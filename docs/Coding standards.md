Coding standard
===============

Common
------

1. Disputable issues MUST be discussed.

1. Character set for __all__ files - __utf-8__ (without BOM). Line ending: __LF__ (git should automatically convert into it).

PHP
---

1. Using __PSR-2 and below__.

1. Naming convention: 

    - __classes__ ONLY in __singular__.
    
    - in same scope MUST not persist variables with and without __`-s`__ (first MUST be renamed).
    
    - variable name SHOULD __not include abbreviations__ (except db properties).

1. Highly recommended margin for code is __80 symbols__.

1. Before commit or making changes use __`Reformat code`__ (default __Ctrl + Alt + L__)

1. Code and comments MUST be written in __english__.

1. To do format: `// TODO: some text.` or `/** @todo: some text. */`.

1. Operations __money__ (and other __floating values__) are carried out by the __BCMath__ extension.

1. __PHPDoc__:

    - __All__ class methods MUST have PHPDoc. If PHPDoc not changed in compare with parent class - write:
      
              /**
               * @inheritdoc
               */

     - __All__ class properties MUST have PHPDoc.

    - __Constant__ MIGHT have not PHPDoc if it clearly named.


GIT
---

1. Before working it is __recommended__ to __pull changes__ to avoid unnecessary merges or even errors. 

1. For __each__ task for more than __1 day__ of work OR __few commits__ MUST be created __new branch__.

1. _Proposed_ branch naming:

        {task number}_{short task name}

1. _Proposed_ commit message:

        {task number} {action type}({module}): {action} {description of the changes}
        {blank line}
        {task number} {task description}
        
    - {action type}: feature, fix, reformat, refactor.
    - {module}: not limited, e.g. docs, docs.rbac, Catalog, Catalog.parameter, User, git, global, config.
    - {action}: create, add, delete, hide, show, update.


DataBase
--------

1. Character set: `utf8_unicode_ci` (for MySQL) or `utf8` (for any other cases, when `unicode_ci` not present).

1. Naming convention:

    - __tables__ MUST be in case (singular or plural) depending on __case of entity in 1 row__ (generally - __singular__).
    
    - Primary key generally named `id`.
     
    - Foreign key: `fk_{entity from}_to_{entity to}`.
    
    - Foreign key column: `{table_name}_id` or `{short table name}_id` (e.g. `user_status` -> `user_status_id` or `status_id`).
    
    - Index: `{prefix}_idx`, where `prefix` consists of column name (for one-column index) or main point of index (for multi-column).

    - Unique index: `{prefix}_uidx`.
    
    - For __abbreviation in column name__ (if not other way) MUST be written __column-comment__.
