# Интеграция сторонних сервисов #
Настройки интеграции хранятся в common\models\Company::$integration. В common\models\employee\Employee::integration() предоставляет доступ к настройкам интеграции текущей компании.


## Facebook ##
Никакие данные на сервере не сохраняются. При запросе /integration/facebook происходит попытка авторизации OAuth и сохранение данных авторизации в куки integrationFBUser-{companyId} и integrationFBToken-{companyId}.
Данные не кешируются.

## AmoCRM ##
- после сохранения настроек AmocrmSettingsForm устанавливает веб-хуки на адрес /integration/amocrm/hook;
- на веб-хуки реагирует AMOcrmHelper::hook(), который создаёт экземпляр класса сущности, и вызывает один из методов HookTrait;
- в момент обработки веб-хука клиент AMOcrm связывается с клиентом сайта через таблицу amocrm_user (заполняется в момент настройки интеграции);
- данные из таблиц amocrm_* связаны с клиентом через поле eployee_id (FOREIGN KEY ON UPDATE CASCADE ON DELETE CASCADE);


## Эвотор ##

### Общие сведения ###
Для интеграции используется приложение в системе Эвотор (создание/редактирование тут: dev.evotor.ru). Клиенты должны установить это приложение, указав при этом логин и пароль доступа к личному кабинету КУБ-24.
Для URL _/integration/evotor/hook_ авторизация должна быть отключена.

При установке приложения, после ввода логина и пароля, присходит удалённая авторизация на сервере КУБ-24 и возврат на сервер Эвотора токена, который будет использоваться при геренации веб-хуков. Этот токен имеет вид **{TOKEN}:{EMPLOYEE_ID}**, подробнее в _frontend\modules\integration\helpers\EvotorHelper::hookAuth()_.

После авторизации сервер Эвотор отправит в КУБ-24 токен доступа REST API, (см _frontend\modules\integration\helpers\EvotorHelper::hookToken()_), который будет использоваться для обращения к API Эвотора, для первоначальной загрузки информации о магазинах, терминалах и сотрудниках. Токен сохраняется в таблице evotor_user.
Токен отправляется на сервер несколько раз: при авторизации приложения, при установке приложения на терминал (если устанавилвается сразу на несколько терминалов, токен будет отправлен один раз) и один раз при удалении приложения с терминала.
API получения номенклатуры доступно только после установки приложения на терминал, а веб-хуки во всех ситуациях отправляются одинаковые, поэтому обработчик пытается загрузить номенклатуру каждый раз.

После этого сервер Эвотор будет отправлять веб-хуки, передавая идентификатор клиента в HTTP-заголовки **Authorization**.
См. _frontend\modules\integration\helpers\EvotorHelper::hook()_.

### Настройки приложения ###
Приложение создаётся/редактируется тут: https://dev.evotor.ru. На вкладках _ОБЗОР_ и _ВИДЕО И СКРИНШОТЫ_ могут содержаться любые данные.
На вкладке _ИНТЕГРАЦИЯ_ должны быть проведены следующие настройки:
* Сотрудники пользователя Эвотор:
    - **URL**: https://kub-24.com/integration/evotor/hook?action=employee&path=
    - **Тип авторизации**: токен пользователя
* Смарт-терминалы пользователя Эвотор:
    - **URL**: https://kub-24.com/integration/evotor/hook?action=device&path=
    - **Тип авторизации**: токен пользователя
* Токен приложения для доступа к REST API Эвотор:
    - **URL**: https://kub-24.com/integration/evotor/hook?action=token&path=
    - **Тип авторизации**: ваш токен
    - **Ваш токен**: должен соответствовать EvotorHelper::APPLICATION_TOKEN
* Магазины пользователя Эвотор:
    - **URL**: https://kub-24.com/integration/evotor/hook?action=store&path=
    - **Тип авторизации**: токен пользователя
* Номенклатура:
    - **URL**: https://kub-24.com/integration/evotor/hook?action=product&path=
* Чеки (ver.2):
    - **URL**: https://kub-24.com/integration/evotor/hook?action=receipt&path=
    - **Ваш токен**: должен соответствовать EvotorHelper::APPLICATION_TOKEN
    - Выберите ниже поля элемента data...: type, deviceId, employeeId, totalDiscount, storeId, totalTax, totalAmount, dateTime
    - Выберите ниже поля элемента items...: отметить все
* Авторизация учётной записи в стороннем сервисе:
    - **URL**: https://kub-24.com/integration/evotor/hook?action=auth
    - **Тип авторизации**: ваш токен
    - **Ваш токен**: должен соответствовать EvotorHelper::APPLICATION_TOKEN
    - Текстовое поле, _FormData name_: login, _Подсказка к полю_: любая
    - Поле с паролем, _FormData name_: password, _Подсказка к полю_: любая
* Регистрация учётной записи в стороннем сервисе:
    - **URL**: https://kub-24.com/integration/evotor/hook?action=auth
    - **Тип авторизации**: ваш токен
    - **Ваш токен**: должен соответствовать EvotorHelper::APPLICATION_TOKEN
    - Текстовое поле, _FormData name_: login, _Подсказка к полю_: любая
    - Поле с паролем, _FormData name_: password, _Подсказка к полю_: любая
* Получить остатки номенклатуры из Облака Эвотор
* Получать номенклатуру из облака Эвотор


## InSales CMS ##
Для интеграции используется приложение в системе InSales. Клиенты должны установить это приложение, после чего из личного кабинета InSales перейти по ссылке приложения для авторизации. В этот момент сохраняются данные подключённого магазина в таблице insales_user.
После установки (в мемент авторизации), через API назначаются веб-хуки, на которые, в свою очередь, реагирует _frontend\modules\integration\helpers\InsalesHelper::hook()_.

### Настройки приложения ###
Приложение создаётся/редактируется тут: https://{your-host}.myinsales.ru/admin2/my_applications.
Важные поля:
* **Секрет** должен соответствовать _frontend\modules\integration\helpers\InsalesHelper::APPLICATION_SECRET_
* **URL установки**: https://kub-24.ru/integration/insales/install
* **URL входа**: https://kub-24.ru/integration/insales/login
* **URL деинсталяции**: https://kub-24.ru/integration/insales/destroy

Для URL _/integration/insales/insall_, _/integration/insales/hook_ и _/integration/insales/destroy_ должна быть отключена авторизация.


## Битрикс 24 ##
Приложение создаётся/редактируется тут: https://vendors.bitrix24.ru/app/ (необходимо зарегистрироваться как "технологический партнёр").
В _frontend\modules\integration\helpers\Bitrx24Helper_ нужно прописать идентификатор созданного приложения (OAuth 2.0 client_id) и секртный ключ (OAuth 2.0 client_secret) в константы __APPLICATION_ID_ и _APPLICATION_SECRET_. Увидеть их можно в vendors.bitrix24.ru, нажав кнопку "Тестировать".
В настройках приложения указать URL приложения: _https://kub-24.ru/integration/bitrix24/token_.

Для URL _/integration/bitrix24/hook_ должна быть отключена авторизация.

Документация по REST: https://dev.1c-bitrix.ru/rest_help
Документация касательно OAuth 2.0: https://dev.1c-bitrix.ru/learning/course/index.php?COURSE_ID=99&LESSON_ID=2486&LESSON_PATH=8771.5380.5379.2486
