# RBAC

All RBAC permissions and rules must be constants and lie in namespace: `frontend\rbac`.

RBAC permission __don't check__ if entity belongs to __current company__.

## Permissions

Common principle for RBAC permissions naming: `<module-name>.<rule-name>`.

## Rules

Common principle - like permission, but with suffix `-rule`.
