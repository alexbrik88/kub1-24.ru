INSTALLATION STEP-BY-STEP INSTRUCTION
=====================================

REQUIREMENTS
------------

The minimum requirement by this application template that your Web server supports PHP 5.6.0.

## CONFIGURE

### NGINX

- Nginx host config see in `nginx.conf`


### PHP

`php.ini`
For php version lower than 5.6 uncomment following line:

    ;mbstring.internal_encoding = UTF-8

Set correct timezone (find `date.timezone`):

    date.timezone = "Europe/Moscow"

### MYSQL

`/etc/mysql/my.cnf`
Set correct timezone:

    [mysqld]
    default_time_zone = '+03:00' # Europe/Moscow

INSTALLATION
------------

- Checkout from VCS

- Run composer:

        php composer.phar install

- Initialize yii project:

        ./init

- Create database with "utf8_general_ci" collation , configure `common/config/main-local.php` and run migrations:

        ./yii migrate

- Run RBAC installation

        ./yii migrate --migrationPath=@yii/rbac/migrations
        ./yii rbac/init

- Set host info in config: (frontend/config/main-local.php)

        'hostInfo' => 'http://kub-24.ru',

- Set additional site address (used in emails (e.g. invoice out)) (frontend/config/params-local.php):

        'serviceSite' => 'http://host-name',

- Install fonts for mPDF. It is needed always on `install` or on `update` if changes was disgard.
Set fonts in file `vendor/mpdf/mpdf/config_fonts.php` in variable `$this->fontdata`

        "arial" => array(
            'R' => "../../../../common/assets/font/arial/Arial.ttf",
            'B' => "../../../../common/assets/font/arial/ArialBD.ttf",
            'I' => "../../../../common/assets/font/arial/ArialLI.ttf",
            'BI' => "../../../../common/assets/font/arial/ArialBI.ttf",
            'useOTL' => 0xFF,
            'useKashida' => 75,
        ),

- Add dictionaries

    - Addresses [link](http://fias.nalog.ru/Public/DownloadPage.aspx).
    Apply dump for `address_dictionary`: unpack and import (it will create `address_dictionary` table (if not exists) and import all rows)

        tar -zxvf address_dictionary.sql.tar.gz
        mysql -uUSER -p DATABASE < address_dictionary.sql

    - Address - new installation:
        - download .dbf
        - unpack
        - import ADDROBJ.dbf (f.e. with Navicat Premium. NOTE: collation type is CP866)
        - create column `FULLNAME` VARCHAR(131) COMMENT "FORMALNAME + ' ' + SHORTNAME"
        - update column `FULLNAME` = CONCAT(FORMALNAME, ' ', SHORTNAME)
        - NOTE: MyISAM engine is prefered (many selects, no DML)
        - create indexes: [AOGUID], [AOLEVEL, ACTSTATUS], [AOLEVEL, ACTSTATUS, FULLNAME], [AOGUID, AOLEVEL, ACTSTATUS, FULLNAME]
        - to update db - read instruction on link above

    - Bik [link](http://www.bik-info.ru/base.html).



### CRON mini-instruction

[Guide](http://kvz.io/blog/2007/07/29/schedule-tasks-on-linux-using-crontab/)

- List of tasks:

        sudo crontab -l

- Edit list of cronjobs:

        sudo crontab -e

- Job period syntax:

        m h  dom mon dow   command

### CRON set tasks

- Currency exchange rates. Before start project, you MUST parse exchange rates at least one time.
At 02:00, 06:00 and 12:00 AM each day with logging into /var/log/script_output.log all messages:

        0 2,6,12 * * * /path/to/project/yii currency-exchange > /var/log/smartplus_currency_exchange.log 2>&1
        0 12 * * * /path/to/project/yii bik/update > /var/log/smartplus_bik_update.log 2>&1
        0 3 * * * /path/to/project/yii service/statistic-update > /var/log/smartplus_service_statistic_update.log 2>&1


## Windows

- Change in `common/config/main-local.php`

        'assetManager' => [
            'forceCopy' => true,
            'linkAssets' => false,
        ],



- Add following php docs to Yii framework classes for smart autocompletion:

        // vendor/yiisoft/yii2/web/User.php
        * @property IdentityInterface|null|common\models\employee\Employee $identity The identity object associated with the currently logged-in