Robokassa installation instruction
==================================

Create an account on [robokasssa.ru](robokasssa.ru).

Create shop and on "Technical preferences" tab set following parameters:

|name|value|
|-|-|
|Result url|domainname/subscribe/online-payment/result|
|Success ulr|domainname/subscribe/online-payment/success|
|Fail ulr|domainname/subscribe/online-payment/fail|
|Request type|dev - __get__, production - __post__|
