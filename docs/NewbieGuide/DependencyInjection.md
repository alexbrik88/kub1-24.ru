# Внедрения зависимостей
https://www.yiiframework.com/doc/guide/2.0/ru/concept-di-container

## Внедрение зависимости через конструктор
**Порядок аргументов в конструкторе класса имеет значение!**

1. Аргументы, которые будут явно переданы конструктору
2. Зависимости, которые будут подставлены автоматически
3. Аргумент коструктора `$params = []` потомков `yii\base\BaseObject`

### Примеры использования

#### Пример №0: Внедряемый ApiComponent
На самом деле, можно использовать любой класс

```php
<?php

namespace example\components;

use yii\base\Component;
use yii\base\InvalidConfigException;

class ApiComponent extends Component
{
    /**
     * @var string
     */
    public $token;

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init(): void
    {
        if (empty($this->token)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @param string $method
     * @param array $params
     * @return array
     */
    public function apiCall(string $method, array $params = []): array
    {
        return []; // TODO
    }
}
```

#### Пример №1: Внедрение зависимости ApiComponent в Controller

```php
<?php

namespace example\controllers;

use example\components\ApiComponent;
use yii\base\Module;
use yii\web\Controller;
use yii\web\Response;

class WebController extends Controller
{
    /** @var int */
    private const PAGES_LIMIT = 10;

    /**
     * @var ApiComponent
     */
    private $component;

    /**
     * @var Response
     */
    private $response;

    /**
     * @param string $id
     * @param Module $module
     * @param ApiComponent $component
     * @param array $config
     */
    public function __construct(string $id, Module $module, ApiComponent $component, array $config = [])
    {
        $this->component = $component;

        parent::__construct($id, $module, $config);
    }

    /**
     * @return Response
     */
    public function actionIndex(): Response
    {
        $result = $this->component->apiCall('pages.get', ['limit' => self::PAGES_LIMIT]);
        $this->response->content = $this->render('index', [
            'result' => $result,
        ]);

        return $this->response;
    }
}
```

#### Пример №2: Внедрение зависимости Response в Action

```php
<?php

namespace example\controllers;

use frontend\controllers\ActionInterface;
use yii\base\Action;
use yii\web\Controller;
use yii\web\Response;

class IndexAction extends Action implements ActionInterface
{
    /**
     * @var Response
     */
    private $response;

    /**
     * @param string $id
     * @param Controller $controller
     * @param Response $response
     * @param array $config
     */
    public function __construct(string $id, Controller $controller, Response $response, array $config = [])
    {
        $this->response = $response;

        parent::__construct($id, $controller, $config);
    }

    /**
     * @return Response
     */
    public function run(): Response
    {
        $this->response->content = $this->controller->render('index');

        return $this->response;
    }
}
```

#### Пример №3: Конфигурация ApiComponent
Его конструктор принимает аргумент `array $params = []`

```php
[
    'container' => [
        'singletons' => [
            'example\components\ApiComponent' => [
                'class' => 'example\components\ApiComponent',
                'token' => '23b12jk4j32hj435j3h5j34mmsdf'
            ],
        ],
    ],
];
```

#### Пример №4: Конфигурация класса ApiService
Его конструктор принимает аргументы `int $accountId`, `string $accountSecret`

```php
<?php

namespace example\services;

class ApiService
{
    /**
     * @var int
     */
    private $accountId;

    /**
     * @var string
     */
    private $accountSecret;

    /**
     * @param int $accountId
     * @param string $accountSecret
     */
    public function __construct(int $accountId, string $accountSecret)
    {
        $this->accountId = $accountId;
        $this->accountSecret = $accountSecret;
    }
}
```

Значения аргументов объявляются строго по порядку и без имен

```php
[
    'container' => [
        'definitions' => [
            'example\services\ApiService' => [
                'class' => 'example\services\ApiService',
                [
                    123456, // accountId
                    'asdasd', // accountSecret
                ],
            ],
        ],
    ],
];
```

#### Пример №5: Фабрика ApiService
Использование метода `Yii::createObject`
```php
<?php

namespace example\factories;

use example\services\ApiService;
use yii\base\InvalidConfigException;
use Yii;

class ApiServiceFactory
{
    /**
     * @return ApiService
     * @throws InvalidConfigException
     */
    public function createApiService(): ApiService
    {
        /** @var ApiService $object */
        $object = Yii::createObject(ApiService::class);

        return $object;
    }
}
```

#### Пример №6: создание экземпляра класса ImportReportCommand
Его конструктор принимает аргументы `string $searchQuery`, `int $limit`, `ApiComponent $component`

```php
<?php

namespace example\commands;

use common\commands\CommandInterface;
use example\components\ApiComponent;

class ImportReportCommand implements CommandInterface
{
    /**
     * @var ApiComponent
     */
    private $component;

    /**
     * @var string
     */
    private $searchQuery;

    /**
     * @var int
     */
    private $limit;

    /**
     * @param string $searchQuery
     * @param int $limit
     * @param ApiComponent $service
     */
    public function __construct(string $searchQuery, int $limit, ApiComponent $component)
    {
        $this->searchQuery = $searchQuery;
        $this->limit = $limit;
        $this->component = $component;
    }

    /**
     * @inheritDoc
     */
    public function run(): void
    {
        $result = $this->component->apiCall('pages.get', [
            'limit' => $this->limit,
            'query' => $this->searchQuery,
        ]);

        foreach ($result as $item) {
            echo $item . PHP_EOL;
        }
    }
}
```

Использование метода `Yii::createObject` для создания объекта ImportReportCommand

```php
<?php

namespace example\commands;

use Yii;

$searchQuery = 'News page';
$limit = 1;

/** @var ImportReportCommand $command */
$command = Yii::createObject(ImportReportCommand::class, [$searchQuery, $limit]);
$command->run();
```
