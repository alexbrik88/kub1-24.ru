<?php

namespace console\controllers;

use console\components\ofd\OfdReceiptAutoload;
use yii\console\Controller;

/**
 * @package console\controllers
 */
class OfdController extends Controller
{
    /**
     * Autoload Ofd Receipts
     */
    public function actionAutoload($company_id = null)
    {
        $autoloader = new OfdReceiptAutoload;
        $autoloader->process($company_id);
    }
}
