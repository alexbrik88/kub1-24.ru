<?php
/**
 * Created by PhpStorm.
 * User: valik
 * Date: 23.11.2015
 * Time: 5:33
 */

namespace console\controllers;

use common\components\date\DateHelper;
use common\models\dictionary\bik\BikDictionary;
use himiklab\thumbnail\FileNotFoundException;
use yii;
use yii\console\Controller;
use yii\helpers\ArrayHelper;

class BikController extends Controller
{
    public static $attributes = [
        'bik',
        'is_active',
        'ks',
        'name',
        'namemini',
        'index',
        'city',
        'address',
        'phone',
        'okato',
        'okpo',
        'regnum',
        'srok',
        'dateadd',
        'datechange',
    ];

    public function actionUpdate()
    {
        try {
            $xml = simplexml_load_file(mb_convert_encoding('http://www.bik-info.ru/base/base.xml', 'UTF-8', 'CP1251'));
            if ($xml === null) {
                throw new FileNotFoundException('File not found');
            }
            $items = (array) ArrayHelper::getValue(json_decode(json_encode($xml), true), 'bik');
            $rows = [];
            foreach ($items as $item) {
                $data = ArrayHelper::getValue($item, '@attributes');
                $bik = ArrayHelper::getValue($data, 'bik');
                $name = ArrayHelper::getValue($data, 'name');
                if ($bik && $name) {
                    $rows[] =[
                        $bik,
                        false,
                        ArrayHelper::getValue($data, 'ks'),
                        $name,
                        ArrayHelper::getValue($data, 'namemini'),
                        ArrayHelper::getValue($data, 'index'),
                        ArrayHelper::getValue($data, 'city'),
                        ArrayHelper::getValue($data, 'address'),
                        ArrayHelper::getValue($data, 'phone'),
                        ArrayHelper::getValue($data, 'okato'),
                        ArrayHelper::getValue($data, 'okpo'),
                        ArrayHelper::getValue($data, 'regnum'),
                        ArrayHelper::getValue($data, 'srok'),
                        ArrayHelper::getValue($data, 'dateadd') ? : null,
                        ArrayHelper::getValue($data, 'datechange') ? : null,
                    ];
                }
            }
            if ($rows) {
                Yii::$app->db->transaction(function ($db) use ($rows) {
                    $db->createCommand()->batchInsert(BikDictionary::tableName(), self::$attributes, $rows)->execute();
                    $db->createCommand()->delete(BikDictionary::tableName(), ['is_active' => true])->execute();
                    $db->createCommand()->update(BikDictionary::tableName(), ['is_active' => true])->execute();
                });
            }
            if ($staticBikDictionary = (array) ArrayHelper::getValue(Yii::$app->params, 'static_bik_dictionary')) {
                foreach ($staticBikDictionary as $bikData) {
                    $bik = [];
                    foreach (self::$attributes as $attribute) {
                        $bik[$attribute] = ArrayHelper::getValue($bikData, $attribute);
                    }
                    if (isset($bik['bik'], $bik['name'], $bik['city']) &&
                        !BikDictionary::find()->where(['bik' => $bik['bik']])->exists()
                    ) {
                        $bik['is_active'] = 1;
                        Yii::$app->db->createCommand()->insert(BikDictionary::tableName(), $bik)->execute();
                    }
                }

            }
        } catch (\Exception $e) {
            throw $e;
        }
    }
}
