<?php

namespace console\controllers;

use frontend\rbac\permissions;
use frontend\rbac\rules;
use frontend\rbac\UserRole;
use Yii;
use yii\console\Controller;
use yii\rbac\DbManager;
use yii\rbac\ManagerInterface;
use yii\rbac\Permission;
use yii\rbac\Role;
use yii\rbac\Rule;

/**
 * Class RbacController
 *
 * Note: employee role `manager` and permission group `manager` - different entities.
 *
 * @package console\controllers
 */
class RbacController extends Controller
{
    /**
     * @var ManagerInterface|DbManager
     */
    private $_auth;

    /**
     * @var \yii\rbac\Role
     */
    private $_guest;
    /**
     * @var \yii\rbac\Role
     */
    private $_authenticated;
    /**
     * @var \yii\rbac\Role
     */
    private $_chief;
    /**
     * @var \yii\rbac\Role
     */
    private $_accountant;
    /**
     * @var \yii\rbac\Role
     */
    private $_assistant;
    /**
     * @var \yii\rbac\Role
     */
    private $_manager;
    /**
     * @var \yii\rbac\Role
     */
    private $_supervisor;
    /**
     * @var \yii\rbac\Role
     */
    private $_supervisor_viewer;
    /**
     * @var \yii\rbac\Role
     */
    private $_sales_manager;
    /**
     * @var \yii\rbac\Role
     */
    private $_sales_supervisor;
    /**
     * @var \yii\rbac\Role
     */
    private $_sales_supervisor_viewer;
    /**
     * @var \yii\rbac\Role
     */
    private $_purchase_supervisor;
    /**
     * @var \yii\rbac\Role
     */
    private $_product_admin;
    /**
     * @var \yii\rbac\Role
     */
    private $_founder;
    /**
     * @var \yii\rbac\Role
     */
    private $_marketer;
    /**
     * @var \yii\rbac\Role
     */
    private $_demo;
    /**
     * @var \yii\rbac\Role
     */
    private $_finance_director;
    /**
     * @var \yii\rbac\Role
     */
    private $_finance_adviser;
    /**
     * @var \yii\rbac\Role
     */
    private $_partner_relations_manager;

    /**
     *  RBAC configuration.
     *
     * guest - операции, разрешённые гостю
     * authenticated - общие операции для залогиненных пользователей
     * chief|accountant|manager|assistant|supervisor - роли пользователей (@see common\models\EmployeeRole)
     */
    public function actionInit()
    {
        // if PhpManager needed, you must rewrite file path rules
        $this->_auth = \Yii::$app->authManager;
        $auth = $this->_auth;
        //$auth->invalidateCache();
        $auth->removeAll();

        //Sysadmin rbac
        $sysadmin = $auth->createRole(UserRole::ROLE_SYSADMIN);
        $sysadminRule = new rules\SysadminRule();
        $auth->add($sysadminRule);
        $sysadmin->ruleName = $sysadminRule->name;
        $auth->add($sysadmin);

        // CREATE ROLES
        $guest = $this->_guest = $auth->createRole(UserRole::ROLE_GUEST);
        $authenticated = $this->_authenticated = $auth->createRole(UserRole::ROLE_AUTHENTICATED);
        $chief = $this->_chief = $auth->createRole(UserRole::ROLE_CHIEF);
        $accountant = $this->_accountant = $auth->createRole(UserRole::ROLE_ACCOUNTANT);
        $manager = $this->_manager = $auth->createRole(UserRole::ROLE_MANAGER);
        $sales_manager = $this->_sales_manager = $auth->createRole(UserRole::ROLE_SALES_MANAGER);
        $assistant = $this->_assistant = $auth->createRole(UserRole::ROLE_ASSISTANT);
        $supervisor = $this->_supervisor = $auth->createRole(UserRole::ROLE_SUPERVISOR);
        $sales_supervisor = $this->_sales_supervisor = $auth->createRole(UserRole::ROLE_SALES_SUPERVISOR);
        $supervisor_viewer = $this->_supervisor_viewer = $auth->createRole(UserRole::ROLE_SUPERVISOR_VIEWER);
        $sales_supervisor_viewer = $this->_sales_supervisor_viewer = $auth->createRole(UserRole::ROLE_SALES_SUPERVISOR_VIEWER);
        $purchase_supervisor = $this->_purchase_supervisor = $auth->createRole(UserRole::ROLE_PURCHASE_SUPERVISOR);
        $product_admin = $this->_product_admin = $auth->createRole(UserRole::ROLE_PRODUCT_ADMIN);
        $founder = $this->_founder = $auth->createRole(UserRole::ROLE_FOUNDER);
        $marketer = $this->_marketer = $auth->createRole(UserRole::ROLE_MARKETER);
        $demo = $this->_demo = $auth->createRole(UserRole::ROLE_DEMO);
        $finance_director = $this->_finance_director = $auth->createRole(UserRole::ROLE_FINANCE_DIRECTOR);
        $finance_adviser  = $this->_finance_adviser = $auth->createRole(UserRole::ROLE_FINANCE_ADVISER);
        $partner_relations_manager  = $this->_partner_relations_manager = $auth->createRole(UserRole::ROLE_PARTNER_RELATIONS_MANAGER);

        // Add all user rule
        $userRule = new rules\UserRule();
        $auth->add($userRule);
        $guest->ruleName = $userRule->name;
        $authenticated->ruleName = $userRule->name;

        // Add employee roles rule
        $employeeRule = new rules\EmployeeRule();
        $auth->add($employeeRule);
        $chief->ruleName = $employeeRule->name;
        $accountant->ruleName = $employeeRule->name;
        $assistant->ruleName = $employeeRule->name;
        $manager->ruleName = $employeeRule->name;
        $supervisor->ruleName = $employeeRule->name;
        $supervisor_viewer->ruleName = $employeeRule->name;
        $sales_manager->ruleName = $employeeRule->name;
        $sales_supervisor->ruleName = $employeeRule->name;
        $sales_supervisor_viewer->ruleName = $employeeRule->name;
        $purchase_supervisor->ruleName = $employeeRule->name;
        $founder->ruleName = $employeeRule->name;
        $marketer->ruleName = $employeeRule->name;
        $demo->ruleName = $employeeRule->name;
        $finance_director->ruleName = $employeeRule->name;
        $finance_adviser->ruleName = $employeeRule->name;
        $partner_relations_manager->ruleName = $employeeRule->name;

        // Add product administrator rule
        $productAdminRule = new rules\ProductAdminRule();
        $auth->add($productAdminRule);
        $product_admin->ruleName = $productAdminRule->name;

        // Add roles in Yii::$app->authManager
        $auth->add($guest);
        $auth->add($authenticated);
        $auth->add($chief);
        $auth->add($accountant);
        $auth->add($assistant);
        $auth->add($manager);
        $auth->add($supervisor);
        $auth->add($supervisor_viewer);
        $auth->add($sales_manager);
        $auth->add($sales_supervisor);
        $auth->add($sales_supervisor_viewer);
        $auth->add($purchase_supervisor);
        $auth->add($product_admin);
        $auth->add($founder);
        $auth->add($marketer);
        $auth->add($demo);
        $auth->add($finance_director);
        $auth->add($finance_adviser);
        $auth->add($partner_relations_manager);


        // GUEST
        $this->createPermission(permissions\User::SIGNUP, $guest);
        $this->createPermission(permissions\User::LOGIN, $guest);
        $this->createPermission(permissions\User::RESET_PASSWORD, $guest);

        // OTHER
        $auth->addChild($chief, $authenticated);
        $auth->addChild($accountant, $authenticated);
        $auth->addChild($assistant, $authenticated);
        $auth->addChild($manager, $authenticated);
        $auth->addChild($supervisor, $authenticated);
        $auth->addChild($supervisor_viewer, $authenticated);
        $auth->addChild($sales_manager, $authenticated);
        $auth->addChild($sales_supervisor, $authenticated);
        $auth->addChild($sales_supervisor_viewer, $authenticated);
        $auth->addChild($purchase_supervisor, $authenticated);
        $auth->addChild($founder, $authenticated);
        $auth->addChild($finance_director, $authenticated);
        $auth->addChild($finance_adviser, $authenticated);


        $this->createServiceRbac();
        $this->createUserRbac();
        $this->createCompanyRbac();
        $this->createContractorRbac();
        $this->createProjectRbac();
        $this->createDocumentRbac();
        $this->createCashRbac();
        $this->createProductRbac();
        $this->createEmployeeRbac();
        $this->createRentRbac();
        $this->createReportRbac();
        $this->createTemplateRbac();
        $this->createHelpArticleRbac();
        $this->createSubscribeRbac();
        $this->createExportRbac();
        $this->createAutoinvoiceRbac();
        $this->createCrmRbac();
        $this->createPaymentReminderRbac();
        $this->createBusinessAnalyticsRbac();
        $this->createReportsRbac();
        $this->createAgreementTemplateRbac();

        // strict mode
        $cashStrictPerm = $this->createRule(new rules\strictMode\Cash(), permissions\Cash::STRICT_MODE);
        $auth->addChild($this->_authenticated, $cashStrictPerm);
        $documentStrictPerm = $this->createRule(new rules\strictMode\Document(), permissions\document\Document::STRICT_MODE);
        $auth->addChild($this->_authenticated, $documentStrictPerm);
    }

    private function createSubscribeRbac()
    {
        $auth = $this->_auth;

        $manager = $this->createPermission(permissions\Subscribe::MANAGER);

        $index = $this->createPermission(permissions\Subscribe::INDEX, $manager);

        // reader
        $auth->addChild($this->_chief, $manager);
        $auth->addChild($this->_founder, $index);
        $auth->addChild($this->_demo, $index);
    }

    private function createExportRbac()
    {
        $auth = $this->_auth;

        $creator = $this->createPermission(permissions\Export::CREATOR);
        $this->createPermission(permissions\Export::INDEX, $creator);
        $this->createPermission(permissions\Export::CREATE, $creator);

        $auth->addChild($this->_chief, $creator);
        $auth->addChild($this->_accountant, $creator);
        $auth->addChild($this->_finance_director, $creator);
        $auth->addChild($this->_finance_adviser, $creator);
    }

    private function createHelpArticleRbac()
    {
        $auth = $this->_auth;

        $reader = $this->createPermission(permissions\HelpArticle::READER);

        $this->createPermission(permissions\HelpArticle::INDEX, $reader);
        $this->createPermission(permissions\HelpArticle::VIEW, $reader);

        // reader
        $auth->addChild($this->_chief, $reader);
        $auth->addChild($this->_accountant, $reader);
        $auth->addChild($this->_manager, $reader);
        $auth->addChild($this->_supervisor, $reader);
        $auth->addChild($this->_supervisor_viewer, $reader);
        $auth->addChild($this->_assistant, $reader);
        $auth->addChild($this->_founder, $reader);
        $auth->addChild($this->_finance_director, $reader);
        $auth->addChild($this->_finance_adviser, $reader);
    }

    private function createTemplateRbac()
    {
        $auth = $this->_auth;

        $reader = $this->createPermission(permissions\Template::READER);

        $this->createPermission(permissions\Template::INDEX, $reader);

        // reader
        $auth->addChild($this->_chief, $reader);
        $auth->addChild($this->_accountant, $reader);
        $auth->addChild($this->_manager, $reader);
        $auth->addChild($this->_supervisor, $reader);
        $auth->addChild($this->_supervisor_viewer, $reader);
        $auth->addChild($this->_assistant, $reader);
        $auth->addChild($this->_founder, $reader);
        $auth->addChild($this->_finance_director, $reader);
        $auth->addChild($this->_finance_adviser, $reader);
    }

    private function createServiceRbac()
    {
        $auth = $this->_auth;

        $this->createPermission(permissions\Service::HOME_ACTIVITIES, $this->_authenticated);
        $home = $this->createPermission(permissions\Service::HOME_PAGE, $this->_authenticated);
        $logo = $this->createPermission(permissions\Service::HOME_LOGO, $this->_authenticated);
        $rate = $this->createPermission(permissions\Service::HOME_EXCHANGE_RATE, $this->_authenticated);

        $proceeds = $this->createPermission(permissions\Service::HOME_PROCEEDS, $this->_chief);
        $expenses = $this->createPermission(permissions\Service::HOME_EXPENSES, $this->_chief);
        $receipts = $this->createPermission(permissions\Service::HOME_RECEIPTS, $this->_chief);
        $flows = $this->createPermission(permissions\Service::HOME_FLOWS, $this->_chief);
        $finances = $this->createPermission(permissions\Service::HOME_FINANCES, $this->_chief);
        $debtsData = $this->createPermission(permissions\Service::DEBTS_HELPER_ALL_DATA, $this->_chief);

        $cash = $this->createPermission(permissions\Service::HOME_CASH);
        $auth->addChild($this->_chief, $cash);
        $auth->addChild($this->_accountant, $cash);
        $auth->addChild($this->_finance_director, $cash);
        $auth->addChild($this->_finance_adviser, $cash);

        $taxCalendar = $this->createPermission(permissions\Service::HOME_TAX_CALENDAR);
        $auth->addChild($this->_chief, $taxCalendar);
        $auth->addChild($this->_accountant, $taxCalendar);
        $auth->addChild($this->_finance_director, $taxCalendar);
        $auth->addChild($this->_finance_adviser, $taxCalendar);

        $auth->addChild($this->_demo, $home);
        $auth->addChild($this->_demo, $logo);
        $auth->addChild($this->_demo, $rate);
        $auth->addChild($this->_demo, $proceeds);
        $auth->addChild($this->_demo, $expenses);
        $auth->addChild($this->_demo, $receipts);
        $auth->addChild($this->_demo, $flows);
        $auth->addChild($this->_demo, $finances);
        $auth->addChild($this->_demo, $debtsData);
        $auth->addChild($this->_demo, $cash);
        $auth->addChild($this->_demo, $taxCalendar);
    }

    private function createProjectRbac()
    {
        $auth = $this->_auth;
        $ruleOwn = new rules\ProjectResponsible();
        $ruleEmployee = new rules\ProjectEmployee();
        $auth->add($ruleOwn);
        $auth->add($ruleEmployee);

        $create = $this->permission(permissions\Project::CREATE);
        $index = $this->permission(permissions\Project::INDEX);
        $indexAll = $this->permission(permissions\Project::INDEX_ALL, null, $index);
        $indexOwn = $this->permission(permissions\Project::INDEX_OWN, null, $index);
        $view = $this->permission(permissions\Project::VIEW);
        $viewOwn = $this->permission(permissions\Project::VIEW_OWN, $ruleOwn->name, $view);
        // $viewEmployee = $this->permission(permissions\Project::VIEW_EMPLOYEE, $ruleEmployee->name, $view);
        $update = $this->permission(permissions\Project::UPDATE);
        $updateOwn = $this->permission(permissions\Project::UPDATE_OWN, $ruleOwn->name, $update);
        $delete = $this->permission(permissions\Project::DELETE);

        $viewer = $this->permission(permissions\Project::VIEWER);
        $manager = $this->permission(permissions\Project::MANAGER);
        $administrator = $this->permission(permissions\Project::ADMINISTRATOR);

        // viewer
        $auth->addChild($viewer, $indexAll);
        $auth->addChild($viewer, $view);

        // manager
        $auth->addChild($manager, $create);
        $auth->addChild($manager, $indexOwn);
        $auth->addChild($manager, $viewOwn);
        // $auth->addChild($manager, $viewEmployee);
        $auth->addChild($manager, $updateOwn);

        // administrator
        $auth->addChild($administrator, $create);
        $auth->addChild($administrator, $indexAll);
        $auth->addChild($administrator, $view);
        $auth->addChild($administrator, $update);
        $auth->addChild($administrator, $delete);

        // roles
        $auth->addChild($this->_assistant, $manager);
        $auth->addChild($this->_manager, $manager);
        $auth->addChild($this->_sales_manager, $manager);
        $auth->addChild($this->_accountant, $manager);

        $auth->addChild($this->_supervisor, $administrator);
        $auth->addChild($this->_sales_supervisor, $administrator);
        $auth->addChild($this->_chief, $administrator);
        $auth->addChild($this->_finance_director, $administrator);
        $auth->addChild($this->_finance_adviser, $administrator);

        $auth->addChild($this->_supervisor_viewer, $viewer);
        $auth->addChild($this->_sales_supervisor_viewer, $viewer);
        $auth->addChild($this->_founder, $viewer);
        $auth->addChild($this->_demo, $viewer);

        // OTHER
        // $auth->addChild($this->_assistant, $viewEmployee);
        // $auth->addChild($this->_manager, $viewEmployee);
        // $auth->addChild($this->_sales_manager, $viewEmployee);
        // $auth->addChild($this->_accountant, $viewEmployee);
        // $auth->addChild($this->_finance_director, $viewEmployee);
        // $auth->addChild($this->_finance_adviser, $viewEmployee);
    }

    /**
     *
     */
    private function createDocumentRbac()
    {
        $this->documentInvoiceRbac();
        $this->documentPaymentOrderRbac();
        $this->documentOtherRbac();
    }

    private function documentInvoiceRbac()
    {
        $auth = $this->_auth;

        // Permission
        $addFlow = $this->createRule(new rules\document\InvoiceAddFlowRule, permissions\document\Invoice::ADD_CASH_FLOW);

        // roles
        $auth->addChild($this->_assistant, $addFlow);
        $auth->addChild($this->_manager, $addFlow);
        $auth->addChild($this->_sales_manager, $addFlow);
        $auth->addChild($this->_supervisor, $addFlow);
        $auth->addChild($this->_sales_supervisor, $addFlow);
        $auth->addChild($this->_supervisor_viewer, $addFlow);
        $auth->addChild($this->_sales_supervisor_viewer, $addFlow);
        $auth->addChild($this->_purchase_supervisor, $addFlow);
        $auth->addChild($this->_accountant, $addFlow);
        $auth->addChild($this->_chief, $addFlow);
        $auth->addChild($this->_finance_director, $addFlow);
        $auth->addChild($this->_finance_adviser, $addFlow);
    }

    private function documentPaymentOrderRbac()
    {
        $auth = $this->_auth;

        $manager = $this->createPermission(permissions\document\PaymentOrder::MANAGER);
        $administrator = $this->createPermission(permissions\document\PaymentOrder::ADMINISTRATOR);

        $create = $this->createPermission(permissions\document\PaymentOrder::CREATE);
        $view = $this->createPermission(permissions\document\PaymentOrder::VIEW);
        $update = $this->createPermission(permissions\document\PaymentOrder::UPDATE);
        $delete = $this->createPermission(permissions\document\PaymentOrder::DELETE);

        // Permissions
        // manager
        $this->createPermission(permissions\document\PaymentOrder::INDEX, $manager);
        $auth->addChild($manager, $create);
        $auth->addChild($manager, $view);
        $auth->addChild($manager, $update);

        // administrator
        $auth->addChild($administrator, $manager);
        $auth->addChild($administrator, $delete);

        // roles
        $auth->addChild($this->_chief, $administrator);
        $auth->addChild($this->_accountant, $manager);
        $auth->addChild($this->_founder, $view);
        $auth->addChild($this->_demo, $view);
        $auth->addChild($this->_finance_director, $administrator);
        $auth->addChild($this->_finance_adviser, $administrator);
    }

    private function documentOtherRbac()
    {
        $auth = $this->_auth;
        $ruleAccess = new rules\document\DocumentAccess();
        $ruleIn = new rules\document\DocumentAccessIn();
        $ruleOut = new rules\document\DocumentAccessOut();
        $ruleOwn = new rules\document\DocumentAccessOwn();
        $ruleContrResp = new rules\document\DocumentContractorResponsible();
        $ruleAuthor = new rules\document\DocumentAuthor();
        $auth->add($ruleAccess);
        $auth->add($ruleIn);
        $auth->add($ruleOut);
        $auth->add($ruleOwn);
        $auth->add($ruleContrResp);
        $auth->add($ruleAuthor);

        $someAccess = $this->permission(permissions\document\Document::SOME_ACCESS);
        $documents = $this->permission(permissions\document\Document::DOCUMENTS);
        $documentsIn = $this->permission(permissions\document\Document::DOCUMENTS_IN, $ruleIn->name, $documents);
        $documentsOut = $this->permission(permissions\document\Document::DOCUMENTS_OUT, $ruleOut->name, $documents);
        $index = $this->permission(permissions\document\Document::INDEX);
        $indexOwn = $this->permission(permissions\document\Document::INDEX_OWN, null, $index);
        $indexIn = $this->permission(permissions\document\Document::INDEX_IN, $ruleIn->name, $index);
        $indexOut = $this->permission(permissions\document\Document::INDEX_OUT, $ruleOut->name, $index);
        $view = $this->permission(permissions\document\Document::VIEW, $ruleAccess->name);
        $viewIn = $this->permission(permissions\document\Document::VIEW_IN, $ruleIn->name, $view);
        $viewOut = $this->permission(permissions\document\Document::VIEW_OUT, $ruleOut->name, $view);
        $viewOwn = $this->permission(permissions\document\Document::VIEW_OWN, $ruleOwn->name, $view);
        $create = $this->permission(permissions\document\Document::CREATE);
        $createIn = $this->permission(permissions\document\Document::CREATE_IN, $ruleIn->name, $create);
        $createOut = $this->permission(permissions\document\Document::CREATE_OUT, $ruleOut->name, $create);
        $createOwn = $this->permission(permissions\document\Document::CREATE_OWN, $ruleOwn->name, $create);
        $update = $this->permission(permissions\document\Document::UPDATE, $ruleAccess->name);
        $updateIn = $this->permission(permissions\document\Document::UPDATE_IN, $ruleIn->name, $update);
        $updateOut = $this->permission(permissions\document\Document::UPDATE_OUT, $ruleOut->name, $update);
        $updateOwn = $this->permission(permissions\document\Document::UPDATE_OWN, $ruleOwn->name, $update);
        $delete = $this->permission(permissions\document\Document::DELETE, $ruleAccess->name);
        $deleteIn = $this->permission(permissions\document\Document::DELETE_IN, $ruleIn->name, $delete);
        $deleteOut = $this->permission(permissions\document\Document::DELETE_OUT, $ruleOut->name, $delete);
        $deleteOun = $this->permission(permissions\document\Document::DELETE_OWN, $ruleOwn->name, $delete);

        $updateStatus = $this->permission(permissions\document\Document::UPDATE_STATUS, $ruleAccess->name);
        $updateStatusIn = $this->permission(permissions\document\Document::UPDATE_STATUS_IN, $ruleIn->name, $updateStatus);
        $updateStatusOut = $this->permission(permissions\document\Document::UPDATE_STATUS_OUT, $ruleOut->name, $updateStatus);
        $updateStatusOwn = $this->permission(permissions\document\Document::UPDATE_STATUS_OWN, $ruleOwn->name, $updateStatus);

        $fileUpload = $this->permission(permissions\document\Document::FILE_UPLOAD, $ruleAccess->name);
        $fileDelete = $this->permission(permissions\document\Document::FILE_DELETE, $ruleAccess->name);

        $indexStrangers = $this->permission(permissions\document\Document::INDEX_STRANGERS);

        $limitedCreator = $this->permission(permissions\document\Document::LIMITED_CREATOR);
        $creator = $this->permission(permissions\document\Document::CREATOR);
        $limitedManager = $this->permission(permissions\document\Document::LIMITED_MANAGER);
        $manager = $this->permission(permissions\document\Document::MANAGER);
        $administrator = $this->permission(permissions\document\Document::ADMINISTRATOR);

        $limitedCreatorIn = $this->permission(permissions\document\Document::LIMITED_CREATOR_IN);
        $creatorIn = $this->permission(permissions\document\Document::CREATOR_IN);
        $limitedManagerIn = $this->permission(permissions\document\Document::LIMITED_MANAGER_IN);
        $managerIn = $this->permission(permissions\document\Document::MANAGER_IN);
        $administratorIn = $this->permission(permissions\document\Document::ADMINISTRATOR_IN);

        $limitedCreatorOut = $this->permission(permissions\document\Document::LIMITED_CREATOR_OUT);
        $creatorOut = $this->permission(permissions\document\Document::CREATOR_OUT);
        $limitedManagerOut = $this->permission(permissions\document\Document::LIMITED_MANAGER_OUT);
        $managerOut = $this->permission(permissions\document\Document::MANAGER_OUT);
        $administratorOut = $this->permission(permissions\document\Document::ADMINISTRATOR_OUT);


        // special permission to create invoice
        $createInvoice = $this->permission(permissions\document\Invoice::CREATE);
        $createInvoiceIn = $this->permission(permissions\document\Invoice::CREATE_IN, $ruleIn->name, $createInvoice);
        $createInvoiceOut = $this->permission(permissions\document\Invoice::CREATE_OUT, $ruleOut->name, $createInvoice);

        $fileUploadOwn = $this->permission(permissions\document\Document::FILE_UPLOAD_OWN, $ruleOwn->name, $fileUpload);
        $fileDeleteOwn = $this->permission(permissions\document\Document::FILE_DELETE_OWN, $ruleOwn->name, $fileDelete);

        $collate = $this->createPermission(permissions\document\Collate::CREATE);

        // Permissions
        // creator limited
        $auth->addChild($limitedCreatorIn, $documentsIn);
        $auth->addChild($limitedCreatorIn, $indexIn);
        $auth->addChild($limitedCreatorIn, $createIn);
        $auth->addChild($limitedCreatorIn, $createInvoiceIn);
        $auth->addChild($limitedCreatorIn, $viewOwn);
        $auth->addChild($limitedCreatorIn, $updateOwn);

        $auth->addChild($limitedCreatorOut, $documentsOut);
        $auth->addChild($limitedCreatorOut, $indexOut);
        $auth->addChild($limitedCreatorOut, $createOut);
        $auth->addChild($limitedCreatorOut, $createInvoiceOut);
        $auth->addChild($limitedCreatorOut, $viewOwn);
        $auth->addChild($limitedCreatorOut, $updateOwn);

        $auth->addChild($limitedCreator, $limitedCreatorIn);
        $auth->addChild($limitedCreator, $limitedCreatorOut);
        $auth->addChild($limitedCreator, $documents);
        $auth->addChild($limitedCreator, $index);
        $auth->addChild($limitedCreator, $create);
        $auth->addChild($limitedCreator, $createInvoice);
        $auth->addChild($limitedCreator, $viewOwn);
        $auth->addChild($limitedCreator, $updateOwn);

        // creator
        $auth->addChild($creatorIn, $limitedCreatorIn);
        $auth->addChild($creatorIn, $createInvoiceIn);
        $auth->addChild($creatorIn, $updateStatusOwn);
        $auth->addChild($creatorIn, $fileUploadOwn);
        $auth->addChild($creatorIn, $fileDeleteOwn);

        $auth->addChild($creatorOut, $limitedCreatorOut);
        $auth->addChild($creatorOut, $createInvoiceOut);
        $auth->addChild($creatorOut, $updateStatusOwn);
        $auth->addChild($creatorOut, $fileUploadOwn);
        $auth->addChild($creatorOut, $fileDeleteOwn);

        $auth->addChild($creator, $limitedCreator);
        $auth->addChild($creator, $creatorIn);
        $auth->addChild($creator, $creatorOut);
        $auth->addChild($creator, $createInvoice);
        $auth->addChild($creator, $updateStatusOwn);
        $auth->addChild($creator, $fileUploadOwn);
        $auth->addChild($creator, $fileDeleteOwn);

        // manager limited
        $auth->addChild($limitedManagerIn, $creatorIn);
        $auth->addChild($limitedManagerIn, $viewIn);
        $auth->addChild($limitedManagerIn, $updateStatusIn);
        $auth->addChild($limitedManagerIn, $indexStrangers);

        $auth->addChild($limitedManagerOut, $creatorOut);
        $auth->addChild($limitedManagerOut, $viewOut);
        $auth->addChild($limitedManagerOut, $updateStatusOut);
        $auth->addChild($limitedManagerOut, $indexStrangers);

        $auth->addChild($limitedManager, $creator);
        $auth->addChild($limitedManager, $limitedManagerIn);
        $auth->addChild($limitedManager, $limitedManagerOut);
        $auth->addChild($limitedManager, $view);
        $auth->addChild($limitedManager, $updateStatus);
        $auth->addChild($limitedManager, $indexStrangers);

        // manager
        $auth->addChild($managerIn, $limitedManagerIn);
        $auth->addChild($managerIn, $updateIn);
        $auth->addChild($managerIn, $fileUpload);
        $auth->addChild($managerIn, $fileDelete);
        $auth->addChild($managerIn, $collate);

        $auth->addChild($managerOut, $limitedManagerOut);
        $auth->addChild($managerOut, $updateOut);
        $auth->addChild($managerOut, $fileUpload);
        $auth->addChild($managerOut, $fileDelete);
        $auth->addChild($managerOut, $collate);

        $auth->addChild($manager, $limitedManager);
        $auth->addChild($manager, $managerIn);
        $auth->addChild($manager, $managerOut);
        $auth->addChild($manager, $update);
        $auth->addChild($manager, $fileUpload);
        $auth->addChild($manager, $fileDelete);
        $auth->addChild($manager, $collate);

        // administrator
        $auth->addChild($administratorIn, $managerIn);
        $auth->addChild($administratorIn, $deleteIn);

        $auth->addChild($administratorOut, $managerOut);
        $auth->addChild($administratorOut, $deleteOut);

        $auth->addChild($administrator, $manager);
        $auth->addChild($administrator, $administratorIn);
        $auth->addChild($administrator, $administratorOut);
        $auth->addChild($administrator, $delete);

        // roles
        $auth->addChild($this->_assistant, $limitedCreatorIn);
        $auth->addChild($this->_supervisor_viewer, $limitedManager);
        $auth->addChild($this->_sales_supervisor_viewer, $limitedManagerOut);
        $auth->addChild($this->_accountant, $manager);
        $auth->addChild($this->_supervisor, $administrator);
        $auth->addChild($this->_sales_supervisor, $administratorOut);
        $auth->addChild($this->_purchase_supervisor, $administratorIn);
        $auth->addChild($this->_chief, $administrator);
        $auth->addChild($this->_founder, $index);
        $auth->addChild($this->_founder, $view);
        $auth->addChild($this->_demo, $index);
        $auth->addChild($this->_demo, $view);
        $auth->addChild($this->_demo, $indexStrangers);
        $auth->addChild($this->_finance_director, $administrator);
        $auth->addChild($this->_finance_adviser, $administrator);

        // roles
        $auth->addChild($this->_assistant, $someAccess);
        $auth->addChild($this->_manager, $someAccess);
        $auth->addChild($this->_sales_manager, $someAccess);
        $auth->addChild($this->_supervisor, $someAccess);
        $auth->addChild($this->_supervisor_viewer, $someAccess);
        $auth->addChild($this->_sales_supervisor, $someAccess);
        $auth->addChild($this->_sales_supervisor_viewer, $someAccess);
        $auth->addChild($this->_purchase_supervisor, $someAccess);
        $auth->addChild($this->_accountant, $someAccess);
        $auth->addChild($this->_chief, $someAccess);
        $auth->addChild($this->_founder, $someAccess);
        $auth->addChild($this->_demo, $someAccess);
        $auth->addChild($this->_finance_director, $someAccess);
        $auth->addChild($this->_finance_adviser, $someAccess);

        /**
         * Manager
         */
        $viewContrResp = $this->permission(permissions\document\Document::VIEW_CONTRACTOR_RESPONSIBLE, $ruleContrResp->name, $view);
        $updateContrResp = $this->permission(permissions\document\Document::UPDATE_CONTRACTOR_RESPONSIBLE, $ruleContrResp->name, $update);
        $fileUploadContrResp = $this->permission(permissions\document\Document::FILE_UPLOAD_CONTRACTOR_RESPONSIBLE, $ruleContrResp->name, $fileUpload);
        $updateStatusContrResp = $this->permission(permissions\document\Document::UPDATE_STATUS_CONTRACTOR_RESPONSIBLE, $ruleContrResp->name, $updateStatus);
        $updateAuthorContrResp = $this->permission(permissions\document\Document::UPDATE_AUTHOR_CONTRACTOR_RESPONSIBLE, $ruleAuthor->name, $updateContrResp);
        $fileUploadAuthorContrResp = $this->permission(permissions\document\Document::FILE_UPLOAD_AUTHOR_CONTRACTOR_RESPONSIBLE, $ruleAuthor->name, $fileUploadContrResp);
        $updateStatusAuthorContrResp = $this->permission(permissions\document\Document::UPDATE_STATUS_AUTHOR_CONTRACTOR_RESPONSIBLE, $ruleAuthor->name, $updateStatusContrResp);
        $auth->addChild($this->_manager, $index);
        $auth->addChild($this->_manager, $create);
        $auth->addChild($this->_manager, $createInvoice);
        $auth->addChild($this->_manager, $viewContrResp);
        $auth->addChild($this->_manager, $updateContrResp);
        $auth->addChild($this->_manager, $fileUploadContrResp);
        $auth->addChild($this->_manager, $updateStatusContrResp);

        /**
         * Sales manager
         */
        $fileUploadOut = $this->permission(permissions\document\Document::FILE_UPLOAD_OUT, $ruleOut->name, $fileUpload);
        $viewContrRespOut = $this->permission(permissions\document\Document::VIEW_CONTRACTOR_RESPONSIBLE_OUT, $ruleContrResp->name, $viewOut);
        $updateContrRespOut = $this->permission(permissions\document\Document::UPDATE_CONTRACTOR_RESPONSIBLE_OUT, $ruleContrResp->name, $updateOut);
        $fileUploadContrRespOut = $this->permission(permissions\document\Document::FILE_UPLOAD_CONTRACTOR_RESPONSIBLE_OUT, $ruleContrResp->name, $fileUploadOut);
        $updateStatusContrRespOut = $this->permission(permissions\document\Document::UPDATE_STATUS_CONTRACTOR_RESPONSIBLE_OUT, $ruleContrResp->name, $updateStatusOut);
        $updateAuthorContrRespOut = $this->permission(permissions\document\Document::UPDATE_AUTHOR_CONTRACTOR_RESPONSIBLE_OUT, $ruleAuthor->name, $updateContrRespOut);
        $fileUploadAuthorContrRespOut = $this->permission(permissions\document\Document::FILE_UPLOAD_AUTHOR_CONTRACTOR_RESPONSIBLE_OUT, $ruleAuthor->name, $fileUploadContrRespOut);
        $updateStatusAuthorContrRespOut = $this->permission(permissions\document\Document::UPDATE_STATUS_AUTHOR_CONTRACTOR_RESPONSIBLE_OUT, $ruleAuthor->name, $updateStatusContrRespOut);
        $auth->addChild($this->_sales_manager, $indexOut);
        $auth->addChild($this->_sales_manager, $createOut);
        $auth->addChild($this->_sales_manager, $createInvoiceOut);
        $auth->addChild($this->_sales_manager, $viewContrRespOut);
        $auth->addChild($this->_sales_manager, $updateContrRespOut);
        $auth->addChild($this->_sales_manager, $fileUploadContrRespOut);
        $auth->addChild($this->_sales_manager, $updateStatusContrRespOut);
    }


    /**
     *
     */
    private function createRentRbac()
    {
        $auth = $this->_auth;

        $viewer = $this->createPermission(permissions\Rent::VIEWER);
        $manager = $this->createPermission(permissions\Rent::MANAGER);
        $administrator = $this->createPermission(permissions\Rent::ADMINISTRATOR);

        // Permissions
        // viewer
        $index = $this->createPermission(permissions\Rent::INDEX, $viewer);
        $view = $this->createPermission(permissions\Rent::VIEW, $viewer);

        // manager
        $auth->addChild($manager, $viewer);
        $this->createPermission(permissions\Rent::CREATE, $manager);
        $this->createPermission(permissions\Rent::UPDATE, $manager);

        // administrator
        $auth->addChild($administrator, $manager);
        $this->createPermission(permissions\Rent::DELETE, $administrator);

        // roles
        $auth->addChild($this->_chief, $administrator);
        $auth->addChild($this->_demo, $viewer);
    }


    /**
     *
     */
    private function createReportRbac()
    {
        $auth = $this->_auth;

        $creator = $this->createPermission(permissions\Report::CREATOR);
        $manager = $this->createPermission(permissions\Report::MANAGER);
        $administrator = $this->createPermission(permissions\Report::ADMINISTRATOR);

        $update = $this->createPermission(permissions\Report::UPDATE);

        $updateOwn = $this->createRule(new rules\report\ReportUpdateOwn(), permissions\Report::UPDATE_OWN, $update);

        // Permissions
        // creator
        $index = $this->createPermission(permissions\Report::INDEX, $creator);
        $view = $this->createPermission(permissions\Report::VIEW, $creator);
        $this->createPermission(permissions\Report::CREATE, $creator);
        $auth->addChild($creator, $updateOwn);

        // manager
        $auth->addChild($manager, $creator);
        $auth->addChild($manager, $update);

        // administrator
        $auth->addChild($administrator, $manager);
        $this->createPermission(permissions\Report::DELETE, $administrator);

        // roles
        $auth->addChild($this->_assistant, $creator);
        $auth->addChild($this->_accountant, $creator);
        $auth->addChild($this->_chief, $administrator);
        $auth->addChild($this->_founder, $index);
        $auth->addChild($this->_founder, $view);
        $auth->addChild($this->_demo, $index);
        $auth->addChild($this->_demo, $view);
        $auth->addChild($this->_finance_director, $administrator);
        $auth->addChild($this->_finance_adviser, $administrator);
    }

    /**
     *
     */
    private function createUserRbac()
    {
        // Permissions
        $this->createPermission(permissions\User::LOGOUT, $this->_authenticated);
        $this->createPermission(permissions\User::PROFILE_EDIT, $this->_authenticated);
    }

    /**
     *
     */
    private function createEmployeeRbac()
    {
        $auth = $this->_auth;

        $completeRegistrationRule = new rules\employee\CompleteRegistration();
        $this->_auth->add($completeRegistrationRule);
        $this->createPermission(permissions\Employee::COMPLETE_REGISTRATION, $this->_authenticated, $completeRegistrationRule->name);

        $creator = $this->createPermission(permissions\Employee::CREATOR);
        $manager = $this->createPermission(permissions\Employee::MANAGER);
        $administrator = $this->createPermission(permissions\Employee::ADMINISTRATOR);

        // Permissions
        // creator
        $index = $this->createPermission(permissions\Employee::INDEX, $creator);
        $view = $this->createPermission(permissions\Employee::VIEW, $creator);
        $this->createPermission(permissions\Employee::CREATE, $creator);

        // manager
        $auth->addChild($manager, $creator);
        $this->createPermission(permissions\Employee::UPDATE, $manager);

        // administrator
        $auth->addChild($administrator, $manager);
        $this->createPermission(permissions\Employee::DELETE, $administrator);

        // roles
        $auth->addChild($this->_chief, $administrator);
        $auth->addChild($this->_founder, $index);
        $auth->addChild($this->_founder, $view);
        $auth->addChild($this->_demo, $index);
        $auth->addChild($this->_demo, $view);
        $auth->addChild($this->_finance_director, $index);
        $auth->addChild($this->_finance_adviser, $index);
        $auth->addChild($this->_finance_director, $view);
        $auth->addChild($this->_finance_adviser, $view);
    }

    /**
     *
     */
    private function createCompanyRbac()
    {
        $auth = $this->_auth;

        $reader = $this->createPermission(permissions\Company::READER);
        $manager = $this->createPermission(permissions\Company::MANAGER);
        $administrator = $this->createPermission(permissions\Company::ADMINISTRATOR);
        $create = $this->createRule(new rules\company\CompanyCreate(), permissions\Company::CREATE);

        // Permissions
        // reader
        $this->createPermission(permissions\Company::PROFILE, $reader);
        $this->createPermission(permissions\Company::VISIT_CARD, $reader);

        // manager
        $auth->addChild($manager, $reader);
        $this->createPermission(permissions\Company::INDEX, $manager);
        $this->createPermission(permissions\Company::UPDATE, $manager);

        // administrator
        $auth->addChild($administrator, $manager);

        // roles
        $auth->addChild($this->_authenticated, $create);
        $auth->addChild($this->_authenticated, $reader);
        $auth->addChild($this->_chief, $administrator);
        $auth->addChild($this->_founder, $reader);
        $auth->addChild($this->_demo, $reader);
        $this->createPermission(permissions\Company::STRICT_MODE_ACCESS, $this->_chief); // allow login and some functionality in strict mode
    }

    /**
     *
     */
    private function createProductRbac()
    {
        $auth = $this->_auth;
        $productPriceInRule = new rules\ProductPriceInRule();
        $auth->add($productPriceInRule);

        $viewer = $this->createPermission(permissions\Product::VIEWER);
        $admin = $this->createPermission(permissions\Product::MANAGER);
        $priceInView = $this->permission(permissions\Product::PRICE_IN_VIEW, $productPriceInRule->name);

        // Permissions
        $auth->addChild($viewer, $priceInView);
        // viewer
        $this->createPermission(permissions\Product::INDEX, $viewer);
        $this->createPermission(permissions\Product::VIEW, $viewer);
        // administrator
        $auth->addChild($admin, $viewer);
        $this->createPermission(permissions\Product::CREATE, $admin);
        $this->createPermission(permissions\Product::UPDATE, $admin);
        $this->createPermission(permissions\Product::DELETE, $admin);

        // roles
        $auth->addChild($this->_chief, $admin);
        $auth->addChild($this->_accountant, $viewer);
        $auth->addChild($this->_manager, $viewer);
        $auth->addChild($this->_supervisor, $viewer);
        $auth->addChild($this->_supervisor_viewer, $viewer);
        $auth->addChild($this->_sales_manager, $viewer);
        $auth->addChild($this->_sales_supervisor, $viewer);
        $auth->addChild($this->_sales_supervisor_viewer, $viewer);
        $auth->addChild($this->_purchase_supervisor, $viewer);
        $auth->addChild($this->_assistant, $viewer);
        $auth->addChild($this->_product_admin, $admin);
        $auth->addChild($this->_founder, $viewer);
        $auth->addChild($this->_demo, $viewer);
        $auth->addChild($this->_finance_director, $admin);
        $auth->addChild($this->_finance_adviser, $admin);

        $this->createProductWriteOffRbac();
    }

    /**
     *
     */
    private function createProductWriteOffRbac()
    {
        $auth = $this->_auth;
        $limitRule = new rules\ProductWriteOffLimitedRule();
        $auth->add($limitRule);

        $writeOff = $this->permission(permissions\Product::WRITE_OFF);
        $writeOffLimited = $this->permission(permissions\Product::WRITE_OFF_LIMITED, $limitRule->name, $writeOff);

        // roles
        foreach (permissions\Product::$writeOffRoles as $roleName) {
            $role = $auth->createRole($roleName);
            $auth->addChild($role, $writeOff);
        }
        foreach (permissions\Product::$writeOffLimitedRoles as $roleName) {
            $role = $auth->createRole($roleName);
            $auth->addChild($role, $writeOffLimited);
        }
    }

    /**
     *
     */
    private function createCashRbac()
    {
        $auth = $this->_auth;

        $creator = $this->createPermission(permissions\Cash::CREATOR);
        $manager = $this->createPermission(permissions\Cash::MANAGER);
        $administrator = $this->createPermission(permissions\Cash::ADMINISTRATOR);

        $create = $this->createPermission(permissions\Cash::CREATE);
        $view = $this->createPermission(permissions\Cash::VIEW);
        $update = $this->createPermission(permissions\Cash::UPDATE);
        $delete = $this->createPermission(permissions\Cash::DELETE);

        // Permissions
        // creator
        $index = $this->createPermission(permissions\Cash::INDEX, $creator);
        $auth->addChild($creator, $create);
        $auth->addChild($creator, $view);

        // manager
        $auth->addChild($manager, $creator);
        $auth->addChild($manager, $update);

        // administrator
        $auth->addChild($administrator, $manager);
        $auth->addChild($administrator, $delete);

        // roles
        $auth->addChild($this->_chief, $administrator);
        $auth->addChild($this->_accountant, $creator);
        $auth->addChild($this->_founder, $index);
        $auth->addChild($this->_founder, $view);
        $auth->addChild($this->_demo, $index);
        $auth->addChild($this->_demo, $view);
        $auth->addChild($this->_finance_director, $administrator);
        $auth->addChild($this->_finance_adviser, $administrator);

        $this->createCashOrderRbac();
        $this->createCashLinkageRbac();
    }

    /**
     *
     */
    private function createCashOrderRbac()
    {
        $auth = $this->_auth;

        $creator = $this->createPermission(permissions\CashOrder::CREATOR);
        // Permissions
        $index = $this->createRule(new rules\cashOrder\CashOrderIndex, permissions\CashOrder::INDEX);
        $view = $this->createRule(new rules\cashOrder\CashOrderView, permissions\CashOrder::VIEW);
        $create = $this->createRule(new rules\cashOrder\CashOrderCreate, permissions\CashOrder::CREATE);
        $update = $this->createRule(new rules\cashOrder\CashOrderUpdate, permissions\CashOrder::UPDATE);
        $delete = $this->createRule(new rules\cashOrder\CashOrderDelete, permissions\CashOrder::DELETE);

        $auth->addChild($creator, $index);
        $auth->addChild($creator, $view);
        $auth->addChild($creator, $create);
        $auth->addChild($creator, $update);
        $auth->addChild($creator, $delete);

        // roles
        $auth->addChild($this->_chief, $creator);
        $auth->addChild($this->_accountant, $creator);
        $auth->addChild($this->_manager, $creator);
        $auth->addChild($this->_supervisor, $creator);
        $auth->addChild($this->_supervisor_viewer, $creator);
        $auth->addChild($this->_sales_manager, $creator);
        $auth->addChild($this->_sales_supervisor, $creator);
        $auth->addChild($this->_sales_supervisor_viewer, $creator);
        $auth->addChild($this->_purchase_supervisor, $creator);
        $auth->addChild($this->_assistant, $creator);
        $auth->addChild($this->_founder, $index);
        $auth->addChild($this->_founder, $view);
        $auth->addChild($this->_demo, $index);
        $auth->addChild($this->_demo, $view);
        $auth->addChild($this->_finance_director, $creator);
        $auth->addChild($this->_finance_adviser, $creator);
    }

    /**
     *
     */
    private function createCashLinkageRbac()
    {
        $auth = $this->_auth;
        $linkage = $this->createPermission(permissions\Cash::LINKAGE);

        $auth->addChild($this->_chief, $linkage);
        $auth->addChild($this->_finance_director, $linkage);
        $auth->addChild($this->_finance_adviser, $linkage);
    }

    /**
     *
     */
    private function createContractorRbac()
    {
        $auth = $this->_auth;
        $ruleSeller = new rules\contractor\ContractorSeller();
        $ruleCustomer = new rules\contractor\ContractorCustomer();
        $ruleOwn = new rules\contractor\ContractorOwn();
        $ruleAccounting = new rules\contractor\ContractorAccounting();
        $ruleNotAccounting = new rules\contractor\ContractorNotAccounting();
        $ruleUpdate = new rules\contractor\ContractorUpdate();
        $ruleDelete = new rules\contractor\ContractorDelete();
        $auth->add($ruleSeller);
        $auth->add($ruleCustomer);
        $auth->add($ruleOwn);
        $auth->add($ruleAccounting);
        $auth->add($ruleNotAccounting);
        $auth->add($ruleUpdate);
        $auth->add($ruleDelete);

        /**
         * Index permission
         */
        $index = $this->permission(permissions\Contractor::INDEX);
        $indexSeller = $this->permission(permissions\Contractor::INDEX_SELLER, $ruleSeller->name, $index);
        $indexCustomer = $this->permission(permissions\Contractor::INDEX_CUSTOMER, $ruleCustomer->name, $index);
        $indexNotAccounting = $this->permission(permissions\Contractor::INDEX_NOT_ACCOUNTING);
        $indexStrangers = $this->permission(permissions\Contractor::INDEX_STRANGERS);
        // roles
        $auth->addChild($this->_chief, $index);
        $auth->addChild($this->_founder, $index);
        $auth->addChild($this->_supervisor, $index);
        $auth->addChild($this->_supervisor_viewer, $index);
        $auth->addChild($this->_accountant, $index);
        $auth->addChild($this->_sales_supervisor, $indexCustomer);
        $auth->addChild($this->_sales_supervisor_viewer, $indexCustomer);
        $auth->addChild($this->_sales_manager, $indexCustomer);
        $auth->addChild($this->_manager, $index);
        $auth->addChild($this->_purchase_supervisor, $indexSeller);
        $auth->addChild($this->_assistant, $indexSeller);
        $auth->addChild($this->_demo, $index);
        $auth->addChild($this->_finance_director, $index);
        $auth->addChild($this->_finance_adviser, $index);
        // index strangers
        $auth->addChild($this->_chief, $indexStrangers);
        $auth->addChild($this->_founder, $indexStrangers);
        $auth->addChild($this->_supervisor, $indexStrangers);
        $auth->addChild($this->_supervisor_viewer, $indexStrangers);
        $auth->addChild($this->_accountant, $indexStrangers);
        $auth->addChild($this->_sales_supervisor, $indexStrangers);
        $auth->addChild($this->_sales_supervisor_viewer, $indexStrangers);
        $auth->addChild($this->_purchase_supervisor, $indexStrangers);
        $auth->addChild($this->_assistant, $indexStrangers);
        $auth->addChild($this->_demo, $indexStrangers);
        $auth->addChild($this->_finance_director, $indexStrangers);
        $auth->addChild($this->_finance_adviser, $indexStrangers);
        // not accounting
        $auth->addChild($this->_chief, $indexNotAccounting);
        $auth->addChild($this->_founder, $indexNotAccounting);
        $auth->addChild($this->_finance_director, $indexNotAccounting);
        $auth->addChild($this->_finance_adviser, $indexNotAccounting);

        /**
         * create permission
         */
        $create = $this->permission(permissions\Contractor::CREATE);
        $createSeller = $this->permission(permissions\Contractor::CREATE_SELLER, $ruleSeller->name, $create);
        $createCustomer = $this->permission(permissions\Contractor::CREATE_CUSTOMER, $ruleCustomer->name, $create);
        // roles
        $auth->addChild($this->_chief, $create);
        $auth->addChild($this->_accountant, $create);
        $auth->addChild($this->_sales_manager, $createCustomer);
        $auth->addChild($this->_sales_supervisor, $createCustomer);
        $auth->addChild($this->_manager, $create);
        $auth->addChild($this->_supervisor, $create);
        $auth->addChild($this->_purchase_supervisor, $createSeller);
        $auth->addChild($this->_assistant, $createSeller);
        $auth->addChild($this->_finance_director, $create);
        $auth->addChild($this->_finance_adviser, $create);

        /**
         * View permission
         */
        $view = $this->permission(permissions\Contractor::VIEW);
        $viewSeller = $this->permission(permissions\Contractor::VIEW_SELLER, $ruleSeller->name, $view);
        $viewCustomer = $this->permission(permissions\Contractor::VIEW_CUSTOMER, $ruleCustomer->name, $view);
        $viewOwn = $this->permission(permissions\Contractor::VIEW_OWN, $ruleOwn->name, $view);
        $viewOwnSeller = $this->permission(permissions\Contractor::VIEW_OWN_SELLER, $ruleOwn->name, $viewSeller);
        $viewOwnCustomer = $this->permission(permissions\Contractor::VIEW_OWN_CUSTOMER, $ruleOwn->name, $viewCustomer);
        $viewAccounting = $this->permission(permissions\Contractor::VIEW_ACCOUNTING, $ruleAccounting->name, $view);
        $viewAccountingSeller = $this->permission(permissions\Contractor::VIEW_ACCOUNTING_SELLER, $ruleAccounting->name, $viewSeller);
        $viewAccountingCustomer = $this->permission(permissions\Contractor::VIEW_ACCOUNTING_CUSTOMER, $ruleAccounting->name, $viewCustomer);
        // roles
        $auth->addChild($this->_chief, $view);
        $auth->addChild($this->_founder, $view);
        $auth->addChild($this->_supervisor, $viewAccounting);
        $auth->addChild($this->_supervisor_viewer, $viewAccounting);
        $auth->addChild($this->_accountant, $viewAccounting);
        $auth->addChild($this->_sales_supervisor, $viewAccountingCustomer);
        $auth->addChild($this->_sales_supervisor_viewer, $viewAccountingCustomer);
        $auth->addChild($this->_purchase_supervisor, $viewAccountingSeller);
        $auth->addChild($this->_assistant, $viewAccountingSeller);
        $auth->addChild($this->_demo, $viewAccounting);
        $auth->addChild($this->_finance_director, $view);
        $auth->addChild($this->_finance_adviser, $view);
        // view own
        $auth->addChild($this->_manager, $viewOwn);
        $auth->addChild($this->_supervisor, $viewOwn);
        $auth->addChild($this->_accountant, $viewOwn);
        $auth->addChild($this->_sales_supervisor, $viewOwnCustomer);
        $auth->addChild($this->_sales_manager, $viewOwnCustomer);
        $auth->addChild($this->_supervisor_viewer, $viewOwn);
        $auth->addChild($this->_sales_supervisor_viewer, $viewOwnCustomer);
        $auth->addChild($this->_purchase_supervisor, $viewOwnSeller);
        $auth->addChild($this->_assistant, $viewOwnSeller);

        /**
         * Update permission
         */
        $update = $this->permission(permissions\Contractor::UPDATE);
        $updateSeller = $this->permission(permissions\Contractor::UPDATE_SELLER, $ruleSeller->name, $update);
        $updateCustomer = $this->permission(permissions\Contractor::UPDATE_CUSTOMER, $ruleCustomer->name, $update);
        $updateOwn = $this->permission(permissions\Contractor::UPDATE_OWN, $ruleOwn->name, $update);
        $updateOwnSeller = $this->permission(permissions\Contractor::UPDATE_OWN_SELLER, $ruleOwn->name, $updateSeller);
        $updateOwnCustomer = $this->permission(permissions\Contractor::UPDATE_OWN_CUSTOMER, $ruleOwn->name, $updateCustomer);
        $updateAccounting = $this->permission(permissions\Contractor::UPDATE_ACCOUNTING, $ruleAccounting->name, $update);
        $updateAccountingSeller = $this->permission(permissions\Contractor::UPDATE_ACCOUNTING_SELLER, $ruleAccounting->name, $updateSeller);
        $updateAccountingCustomer = $this->permission(permissions\Contractor::UPDATE_ACCOUNTING_CUSTOMER, $ruleAccounting->name, $updateCustomer);
        // roles
        $auth->addChild($this->_chief, $update);
        $auth->addChild($this->_supervisor, $updateAccounting);
        $auth->addChild($this->_accountant, $updateAccounting);
        $auth->addChild($this->_sales_supervisor, $updateAccountingSeller);
        $auth->addChild($this->_purchase_supervisor, $updateAccountingCustomer);
        $auth->addChild($this->_finance_director, $update);
        $auth->addChild($this->_finance_adviser, $update);
        // update own
        $auth->addChild($this->_supervisor, $updateOwn);
        $auth->addChild($this->_accountant, $updateOwn);
        $auth->addChild($this->_sales_supervisor, $updateOwnCustomer);
        $auth->addChild($this->_sales_manager, $updateOwnCustomer);
        $auth->addChild($this->_manager, $updateOwn);
        $auth->addChild($this->_purchase_supervisor, $updateOwnSeller);
        $auth->addChild($this->_assistant, $updateOwnSeller);

        /**
         * Delete permission
         */
        $delete = $this->permission(permissions\Contractor::DELETE, $ruleDelete->name);
        $deleteSeller = $this->permission(permissions\Contractor::DELETE_SELLER, $ruleSeller->name, $delete);
        $deleteCustomer = $this->permission(permissions\Contractor::DELETE_CUSTOMER, $ruleCustomer->name, $delete);
        // roles
        $auth->addChild($this->_chief, $delete);
        $auth->addChild($this->_finance_director, $delete);
        $auth->addChild($this->_finance_adviser, $delete);
    }

    /**
     * @param string $name
     * @param Role[] $roles
     * @param Rule|null $rule
     * @throws
     */
    private function addCrmPermission(string $name, array $roles, Rule $rule = null): void
    {
        $permission = $this->_auth->createPermission($name);

        if ($rule) {
            $permission->ruleName = $rule->name;
            $this->_auth->add($rule);
        }

        $this->_auth->add($permission);

        foreach ($roles as $role) {
            $this->_auth->addChild($role, $permission);
        }
    }

    /**
     * @return void
     */
	private function createCrmRbac(): void
	{
        $allRoles = [
            $this->_chief, // Руководитель
            $this->_supervisor, // Руководитель отдела
            $this->_sales_supervisor, // Руководитель отдела продаж
            $this->_supervisor_viewer, // Руководитель отдела (только просмотр)
            $this->_manager, // Менеджер
            $this->_sales_manager, // Менеджер по продажам
            $this->_finance_director,
            $this->_finance_adviser
        ];
        $viewRoles = [
            $this->_chief,
            $this->_supervisor,
            $this->_sales_supervisor,
            $this->_supervisor_viewer,
            $this->_finance_director,
            $this->_finance_adviser,
        ];
        $editRoles = [
            $this->_chief,
            $this->_supervisor,
            $this->_sales_supervisor,
            $this->_manager,
            $this->_sales_manager,
            $this->_finance_director,
            $this->_finance_adviser,
        ];
        $adminRoles = [
            $this->_chief,
            $this->_supervisor,
            $this->_sales_supervisor,
            $this->_finance_director,
            $this->_finance_adviser,
        ];
        $deleteRoles = [
            $this->_chief,
        ];

	    $this->addCrmPermission(permissions\crm\ClientAccess::INDEX, $allRoles);
        $this->addCrmPermission(permissions\crm\ClientAccess::VIEW_ALL, $viewRoles);
	    $this->addCrmPermission(permissions\crm\ClientAccess::VIEW, $allRoles, new rules\crm\ClientViewRule());
        $this->addCrmPermission(permissions\crm\ClientAccess::EDIT_ALL, $adminRoles);
        $this->addCrmPermission(permissions\crm\ClientAccess::EDIT, $editRoles, new rules\crm\ClientEditRule());

        $this->addCrmPermission(permissions\crm\TaskAccess::INDEX, $allRoles);
        $this->addCrmPermission(permissions\crm\TaskAccess::VIEW_ALL, $viewRoles);
        $this->addCrmPermission(permissions\crm\TaskAccess::VIEW, $allRoles, new rules\crm\TaskViewRule());
        $this->addCrmPermission(permissions\crm\TaskAccess::CREATE, $allRoles);
        $this->addCrmPermission(permissions\crm\TaskAccess::EDIT_ALL, $adminRoles);
        $this->addCrmPermission(permissions\crm\TaskAccess::EDIT, $editRoles, new rules\crm\TaskEditRule());
        $this->addCrmPermission(permissions\crm\TaskAccess::DELETE, $deleteRoles);

        $this->addCrmPermission(permissions\crm\DealAccess::INDEX, $allRoles);
        $this->addCrmPermission(permissions\crm\DealAccess::VIEW_ALL, $viewRoles);
        $this->addCrmPermission(permissions\crm\DealAccess::VIEW, $allRoles, new rules\crm\DealViewRule());
        $this->addCrmPermission(permissions\crm\DealAccess::CREATE, $allRoles);
        $this->addCrmPermission(permissions\crm\DealAccess::EDIT_ALL, $adminRoles);
        $this->addCrmPermission(permissions\crm\DealAccess::EDIT, $editRoles, new rules\crm\DealEditRule());

        $this->addCrmPermission(permissions\crm\ClientCheckAccess::CREATE, $adminRoles);
        $this->addCrmPermission(permissions\crm\ClientCheckAccess::EDIT, $adminRoles);
        $this->addCrmPermission(permissions\crm\ClientCheckAccess::VIEW, $allRoles);

        $this->addCrmPermission(permissions\crm\DealTypeAccess::CREATE, $adminRoles);
        $this->addCrmPermission(permissions\crm\DealTypeAccess::EDIT, $adminRoles);
        $this->addCrmPermission(permissions\crm\DealTypeAccess::VIEW, $allRoles);

        $this->addCrmPermission(permissions\crm\ClientPositionAccess::CREATE, $editRoles);
        $this->addCrmPermission(permissions\crm\ClientPositionAccess::EDIT, $adminRoles);
        $this->addCrmPermission(permissions\crm\ClientPositionAccess::VIEW, $allRoles);

        $this->addCrmPermission(permissions\crm\ClientReasonAccess::CREATE, $adminRoles);
        $this->addCrmPermission(permissions\crm\ClientReasonAccess::EDIT, $adminRoles);
        $this->addCrmPermission(permissions\crm\ClientReasonAccess::VIEW, $allRoles);

        $this->addCrmPermission(permissions\crm\ClientStatusAccess::CREATE, $adminRoles);
        $this->addCrmPermission(permissions\crm\ClientStatusAccess::EDIT, $adminRoles);
        $this->addCrmPermission(permissions\crm\ClientStatusAccess::VIEW, $allRoles);

        $this->addCrmPermission(permissions\crm\ClientTypeAccess::CREATE, $adminRoles);
        $this->addCrmPermission(permissions\crm\ClientTypeAccess::EDIT, $adminRoles);
        $this->addCrmPermission(permissions\crm\ClientTypeAccess::VIEW, $allRoles);

        $this->addCrmPermission(permissions\crm\ContractorActivityAccess::CREATE, $editRoles);
        $this->addCrmPermission(permissions\crm\ContractorActivityAccess::EDIT, $adminRoles);
        $this->addCrmPermission(permissions\crm\ContractorActivityAccess::VIEW, $allRoles);

        $this->addCrmPermission(permissions\crm\ContractorCampaignAccess::CREATE, $adminRoles);
        $this->addCrmPermission(permissions\crm\ContractorCampaignAccess::EDIT, $adminRoles);
        $this->addCrmPermission(permissions\crm\ContractorCampaignAccess::VIEW, $allRoles);

        $this->addCrmPermission(permissions\crm\ContactAccess::VIEW, $allRoles, new rules\crm\ContactViewRule());
        $this->addCrmPermission(permissions\crm\ContactAccess::EDIT, $editRoles, new rules\crm\ContactEditRule());

        $this->addCrmPermission(permissions\crm\ConfigAccess::VIEW, $adminRoles);
        $this->addCrmPermission(permissions\crm\ConfigAccess::EDIT, $adminRoles);
	}

    /**
     *
     */
    private function createAutoinvoiceRbac()
    {
        $auth = $this->_auth;

        $manager = $this->createPermission(permissions\document\Autoinvoice::MANAGER);
        $administrator = $this->createPermission(permissions\document\Autoinvoice::ADMINISTRATOR);

        $index = $this->createPermission(permissions\document\Autoinvoice::INDEX);
        $view = $this->createPermission(permissions\document\Autoinvoice::VIEW);
        $create = $this->createPermission(permissions\document\Autoinvoice::CREATE);
        $stop = $this->createPermission(permissions\document\Autoinvoice::STOP);
        $start = $this->createPermission(permissions\document\Autoinvoice::START);
        $update = $this->createPermission(permissions\document\Autoinvoice::UPDATE);
        $delete = $this->createPermission(permissions\document\Autoinvoice::DELETE);

        // Permissions
        // manager
        $auth->addChild($manager, $index);
        $auth->addChild($manager, $view);
        $auth->addChild($manager, $create);
        $auth->addChild($manager, $stop);
        $auth->addChild($manager, $start);
        $auth->addChild($manager, $update);

        // administrator
        $auth->addChild($administrator, $manager);
        $auth->addChild($administrator, $delete);

        // roles
        $auth->addChild($this->_chief, $administrator);
        $auth->addChild($this->_accountant, $manager);
        $auth->addChild($this->_supervisor, $manager);
        $auth->addChild($this->_supervisor_viewer, $manager);
        $auth->addChild($this->_manager, $manager);
        $auth->addChild($this->_sales_supervisor, $manager);
        $auth->addChild($this->_sales_supervisor_viewer, $manager);
        $auth->addChild($this->_sales_manager, $manager);
        $auth->addChild($this->_founder, $index);
        $auth->addChild($this->_founder, $view);
        $auth->addChild($this->_demo, $index);
        $auth->addChild($this->_demo, $view);
        $auth->addChild($this->_finance_director, $administrator);
        $auth->addChild($this->_finance_adviser, $administrator);
    }

    /**
     *
     */
    private function createPaymentReminderRbac()
    {
        $auth = $this->_auth;

        $viewer = $this->createPermission(permissions\PaymentReminder::VIEWER);
        $administrator = $this->createPermission(permissions\PaymentReminder::ADMINISTRATOR);

        $index = $this->createPermission(permissions\PaymentReminder::INDEX);
        $settings = $this->createPermission(permissions\PaymentReminder::SETTINGS);
        $template = $this->createPermission(permissions\PaymentReminder::TEMPLATE);
        $report = $this->createPermission(permissions\PaymentReminder::REPORT);
        $report_own = $this->createPermission(permissions\PaymentReminder::REPORT_OWN);
        $report_all = $this->createPermission(permissions\PaymentReminder::REPORT_ALL);

        $auth->addChild($report_own, $report);
        $auth->addChild($report_all, $report);

        $auth->addChild($viewer, $index);
        $auth->addChild($viewer, $report_own);

        $auth->addChild($administrator, $viewer);
        $auth->addChild($administrator, $settings);
        $auth->addChild($administrator, $template);
        $auth->addChild($administrator, $report_all);

        $auth->addChild($this->_chief, $administrator);
        $auth->addChild($this->_accountant, $viewer);
        $auth->addChild($this->_supervisor, $administrator);
        $auth->addChild($this->_supervisor_viewer, $viewer);
        $auth->addChild($this->_manager, $viewer);
        $auth->addChild($this->_sales_supervisor, $viewer);
        $auth->addChild($this->_sales_supervisor_viewer, $viewer);
        $auth->addChild($this->_sales_manager, $viewer);
        $auth->addChild($this->_founder, $viewer);
        $auth->addChild($this->_finance_director, $administrator);
        $auth->addChild($this->_finance_adviser, $administrator);
    }

    /**
     *
     */
    private function createBusinessAnalyticsRbac()
    {
        $auth = $this->_auth;

        $rule = new rules\AnalyticsStartRule();
        $auth->add($rule);
        $start = $this->permission(permissions\BusinessAnalytics::START, $rule->name);

        $rule = new rules\AnalyticsActivatedRule();
        $auth->add($rule);
        $index = $this->permission(permissions\BusinessAnalytics::INDEX, $rule->name);

        $admin = $this->permission(permissions\BusinessAnalytics::ADMIN, $rule->name);

        $auth->addChild($this->_authenticated, $start);
        $auth->addChild($this->_authenticated, $index);
        $auth->addChild($this->_authenticated, $admin);
        $auth->addChild($this->_demo, $start);
        $auth->addChild($this->_demo, $index);
    }

    /**
     *
     */
    private function createReportsRbac()
    {
        $auth = $this->_auth;

        $view = $this->permission(permissions\Reports::VIEW);
        $finance = $this->permission(permissions\Reports::FINANCE);
        $reports = $this->permission(permissions\Reports::REPORTS);
        $reportsClients = $this->permission(permissions\Reports::REPORTS_CLIENTS);
        $reportsInvoices = $this->permission(permissions\Reports::REPORTS_INVOICES);
        $reportsSuppliers = $this->permission(permissions\Reports::REPORTS_SUPPLIERS);
        $reportsEmployees = $this->permission(permissions\Reports::REPORTS_EMPLOYEES);
        $reportsSales = $this->permission(permissions\Reports::REPORTS_SALES);

        $viewerReportsCI = $this->permission(permissions\Reports::VIEWER_REPORTS_C_I);
        $viewerReportsCIS = $this->permission(permissions\Reports::VIEWER_REPORTS_C_I_S);
        $viewerReportsAll = $this->permission(permissions\Reports::VIEWER_REPORTS_ALL);
        $viewerAll = $this->permission(permissions\Reports::VIEWER_ALL);

        $auth->addChild($viewerReportsCI, $view);
        $auth->addChild($viewerReportsCI, $reports);
        $auth->addChild($viewerReportsCI, $reportsClients);
        $auth->addChild($viewerReportsCI, $reportsInvoices);

        $auth->addChild($viewerReportsCIS, $view);
        $auth->addChild($viewerReportsCIS, $reports);
        $auth->addChild($viewerReportsCIS, $reportsClients);
        $auth->addChild($viewerReportsCIS, $reportsInvoices);
        $auth->addChild($viewerReportsCIS, $reportsSuppliers);

        $auth->addChild($viewerReportsAll, $view);
        $auth->addChild($viewerReportsAll, $reports);
        $auth->addChild($viewerReportsAll, $reportsClients);
        $auth->addChild($viewerReportsAll, $reportsInvoices);
        $auth->addChild($viewerReportsAll, $reportsSuppliers);
        $auth->addChild($viewerReportsAll, $reportsEmployees);
        $auth->addChild($viewerReportsAll, $reportsSales);

        $auth->addChild($viewerAll, $finance);
        $auth->addChild($viewerAll, $viewerReportsAll);

        $auth->addChild($this->_sales_manager, $viewerReportsCI);
        $auth->addChild($this->_sales_supervisor, $viewerReportsCI);
        $auth->addChild($this->_sales_supervisor_viewer, $viewerReportsCI);
        $auth->addChild($this->_manager, $viewerReportsCIS);
        $auth->addChild($this->_supervisor, $viewerReportsCIS);
        $auth->addChild($this->_supervisor_viewer, $viewerReportsCIS);
        $auth->addChild($this->_accountant, $viewerReportsCIS);
        $auth->addChild($this->_chief, $viewerAll);
        $auth->addChild($this->_founder, $viewerAll);
        $auth->addChild($this->_finance_director, $viewerAll);
        $auth->addChild($this->_finance_adviser, $viewerAll);
        $auth->addChild($this->_demo, $viewerAll);
    }

    /**
     *
     */
    private function createAgreementTemplateRbac()
    {
        $auth = $this->_auth;
        $own = new rules\AgreementTemplateOwn();
        $auth->add($own);
        $in = new rules\document\DocumentAccessIn();
        $out = new rules\document\DocumentAccessOut();

        // index
        $index = $this->createPermission(permissions\AgreementTemplate::INDEX, [
            $this->_manager,
            $this->_supervisor,
            $this->_supervisor_viewer,
            $this->_accountant,
            $this->_chief,
            $this->_founder,
            $this->_demo,
            $this->_finance_director,
            $this->_finance_adviser,
        ]);
        $this->createPermission(permissions\AgreementTemplate::INDEX_IN, [
            $this->_purchase_supervisor,
            $this->_assistant,
        ], $in->name, $index);
        $this->createPermission(permissions\AgreementTemplate::INDEX_OUT, [
            $this->_sales_manager,
            $this->_sales_supervisor,
            $this->_sales_supervisor_viewer,
        ], $out->name, $index);

        // view
        $view = $this->createPermission(permissions\AgreementTemplate::VIEW, [
            $this->_manager,
            $this->_supervisor,
            $this->_supervisor_viewer,
            $this->_accountant,
            $this->_chief,
            $this->_founder,
            $this->_demo,
            $this->_finance_director,
            $this->_finance_adviser,
        ]);
        $this->createPermission(permissions\AgreementTemplate::VIEW_IN, [
            $this->_purchase_supervisor,
            $this->_assistant,
        ], $in->name, $view);
        $this->createPermission(permissions\AgreementTemplate::VIEW_OUT, [
            $this->_sales_manager,
            $this->_sales_supervisor,
            $this->_sales_supervisor_viewer,
        ], $out->name, $view);

        // create
        $create = $this->createPermission(permissions\AgreementTemplate::CREATE, [
            $this->_supervisor,
            $this->_chief,
            $this->_finance_director,
            $this->_finance_adviser,
        ]);
        $this->createPermission(permissions\AgreementTemplate::CREATE_IN, [
            $this->_purchase_supervisor,
        ], $out->name, $create);
        $this->createPermission(permissions\AgreementTemplate::CREATE_OUT, [
            $this->_sales_supervisor,
        ], $out->name, $create);

        // update
        $update = $this->createPermission(permissions\AgreementTemplate::UPDATE, [
            $this->_chief,
            $this->_finance_director,
            $this->_finance_adviser,
        ]);
        $this->createPermission(permissions\AgreementTemplate::UPDATE_OWN, [
            $this->_supervisor,
        ], $own->name, $update);
        $updateIn = $this->createPermission(permissions\AgreementTemplate::UPDATE_IN, null, $in->name, $update);
        $this->createPermission(permissions\AgreementTemplate::UPDATE_IN_OWN, [
            $this->_purchase_supervisor,
        ], $in->name, $updateIn);
        $updateOut = $this->createPermission(permissions\AgreementTemplate::UPDATE_OUT, null, $out->name, $update);
        $this->createPermission(permissions\AgreementTemplate::UPDATE_OUT_OWN, [
            $this->_sales_supervisor,
        ], $own->name, $updateOut);

        // delete
        $delete = $this->createPermission(permissions\AgreementTemplate::DELETE, [
            $this->_chief,
            $this->_finance_director,
            $this->_finance_adviser,
        ]);
        $this->createPermission(permissions\AgreementTemplate::DELETE_OWN, [
            $this->_supervisor,
        ], $own->name, $delete);
        $deleteIn = $this->createPermission(permissions\AgreementTemplate::DELETE_IN, null, $in->name, $delete);
        $this->createPermission(permissions\AgreementTemplate::DELETE_IN_OWN, [
            $this->_purchase_supervisor,
        ], $in->name, $deleteIn);
        $deleteOut = $this->createPermission(permissions\AgreementTemplate::DELETE_OUT, null, $out->name, $delete);
        $this->createPermission(permissions\AgreementTemplate::DELETE_OUT_OWN, [
            $this->_sales_supervisor,
        ], $own->name, $deleteOut);
    }

    /**
     * @param $name
     * @param $parent
     * @param null $ruleName
     * @return \yii\rbac\Permission
     */
    private function createPermission($name, $parent = null, $ruleName = null, $child = null)
    {
        $permission = $this->_auth->createPermission($name);
        if ($ruleName !== null) {
            $permission->ruleName = $ruleName;
        }
        $this->_auth->add($permission);
        if ($parent !== null) {
            if (!is_array($parent)) {
                $parent = [$parent];
            }
            foreach ($parent as $item) {
                $this->_auth->addChild($item, $permission);
            }
        }
        if ($child !== null) {
            $this->_auth->addChild($permission, $child);
        }

        return $permission;
    }

    /**
     * @param $name
     * @param $parent
     * @param null $ruleName
     * @return \yii\rbac\Permission
     */
    private function permission($name, $ruleName = null, $child = null)
    {
        $permission = new Permission([
            'name' => $name,
            'ruleName' => $ruleName,
        ]);
        $this->_auth->add($permission);
        if ($child !== null) {
            $this->_auth->addChild($permission, $child);
        }

        return $permission;
    }

    private function createRule($rule, $permissionName, $child = null)
    {
        $this->_auth->add($rule);

        $permission = $this->createPermission($permissionName, null, $rule->name);
        if ($child !== null) {
            $this->_auth->addChild($permission, $child);
        }

        return $permission;
    }
}
