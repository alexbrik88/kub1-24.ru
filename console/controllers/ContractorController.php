<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 26.07.2018
 * Time: 13:44
 */

namespace console\controllers;


use common\models\company\CompanyType;
use common\models\Contractor;
use yii\console\Controller;

/**
 * Class ContractorController
 * @package console\controllers
 */
class ContractorController extends Controller
{
    /**
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     */
    public function actionImportContractor()
    {
        $companyID = 21812;
        $attributeNames = [
            'A' => 'name',
            'B' => 'inn',
            'C' => 'type',
            'D' => 'requisites',
        ];
        $xls = \PHPExcel_IOFactory::load(\Yii::getAlias('@frontend/web/upload/Contractor.xls'));
        $xls->setActiveSheetIndex(0);
        $sheet = $xls->getActiveSheet();
        $rowIterator = $sheet->getRowIterator();
        $contractorData = [];

        foreach ($rowIterator as $key => $row) {
            if ($key == 1) continue;
            $cellIterator = $row->getCellIterator();
            /* @var \PHPExcel_Cell $cell */
            foreach ($cellIterator as $cellKey => $cell) {
                if (isset($attributeNames[$cellKey])) {
                    $contractorData[$attributeNames[$cellKey]] = trim($cell->getFormattedValue());
                }
            }
            if ($contractorData['name']) {
                if ($contractorData['inn']) {
                    /* @var $contractor Contractor */
                    if (Contractor::find()
                        ->byDeleted()
                        ->andWhere(['and',
                            ['ITN' => $contractorData['inn']],
                            ['company_id' => $companyID],
                        ])->exists()
                    ) {
                        echo "Contractor already exists. INN - {$contractorData['inn']} \r\n";
                        continue;
                    }
                    $response = \Yii::$app->dadataSuggestApi->getParty([
                        'query' => $contractorData['inn'],
                        'count' => 1
                    ]);
                    $data = [];
                    if (!$response->isError && !empty($response->data['suggestions'])) {
                        $data = $response->data['suggestions'][0];
                    }
                }
                $contractor = new Contractor();
                $contractor->company_id = $companyID;
                $contractor->type = Contractor::TYPE_CUSTOMER;
                $contractor->ITN = $contractorData['inn'];
                $contractor->status = Contractor::ACTIVE;
                $contractor->chief_accountant_is_director = true;
                $contractor->contact_is_director = true;

                if ($contractorData['type'] == 'Иностран компания') {
                    $contractor->face_type = Contractor::TYPE_FOREIGN_LEGAL_PERSON;
                    $contractor->name = $contractorData['name'];
                    if (empty($contractorData['inn'])) {
                        /* @var $contractor Contractor */
                        if (Contractor::find()
                            ->byDeleted()
                            ->andWhere(['and',
                                ['name' => $contractorData['name']],
                                ['company_id' => $companyID],
                            ])->exists()
                        ) {
                            echo "Contractor already exists. Name - {$contractorData['name']} \r\n";
                            continue;
                        }
                    }
                } elseif ($contractorData['type'] == 'Физ лицо') {
                    $contractor->face_type = Contractor::TYPE_PHYSICAL_PERSON;
                    $fio = explode(' ', $contractorData['name']);
                    foreach (['physical_lastname', 'physical_firstname', 'physical_patronymic'] as $key => $attribute) {
                        $contractor->$attribute = $fio[$key];
                    }
                    if (count($fio) !== 3) {
                        $contractor->physical_no_patronymic = true;
                    }
                    /* @var $contractor Contractor */
                    if (Contractor::find()
                        ->byDeleted()
                        ->andWhere(['and',
                            isset($fio[0]) ? ['physical_lastname' => $fio[0]] : [],
                            isset($fio[1]) ? ['physical_firstname' => $fio[1]] : [],
                            isset($fio[2]) ? ['physical_patronymic' => $fio[2]] : [],
                            ['face_type' => Contractor::TYPE_PHYSICAL_PERSON],
                            ['company_id' => $companyID],
                        ])->exists()
                    ) {
                        echo "Contractor already exists. Name - {$contractorData['name']} \r\n";
                        continue;
                    }

                } elseif ($contractorData['type']) {
                    $contractor->face_type = Contractor::TYPE_LEGAL_PERSON;
                    $contractor->name = $contractorData['name'];

                    /* @var $contractorType CompanyType */
                    $contractorType = CompanyType::find()
                        ->andWhere(['in_contractor' => true])
                        ->andWhere(['name_short' => $contractorData['type']])
                        ->one();
                    if ($contractorType) {
                        $contractor->company_type_id = $contractorType->id;
                        $contractor->name = trim(str_replace($contractorType->name_short, '', $contractor->name));
                    }
                } else {
                    if (isset($data) && $data) {
                        /* @var $contractorType CompanyType */
                        $contractorType = CompanyType::find()
                            ->andWhere(['in_contractor' => true])
                            ->andWhere(['or',
                                ['name_short' => $data['data']['opf']['short']],
                                ['name_full' => $data['data']['opf']['short']],
                            ])->one();
                        if ($contractorType) {
                            $contractor->company_type_id = $contractorType->id;
                        }
                    }
                }

                if (isset($data) && $data) {
                    $contractor->name = isset($data['data']['name']['full']) ? $data['data']['name']['full'] : null;
                    $contractor->PPC = isset($data['data']['kpp']) ? $data['data']['kpp'] : null;
                    $contractor->BIN = isset($data['data']['ogrn']) ? $data['data']['ogrn'] : null;
                    $contractor->legal_address = isset($data['data']['address']['value']) ? $data['data']['address']['value'] : null;
                    $contractor->actual_address = $contractor->legal_address;
                    $contractor->director_name = isset($data['data']['management']['name']) ?
                        $data['data']['management']['name'] :
                        $contractor->name;
                }

                if (isset($contractorData['requisites'])) {
                    $contractor->current_account = mb_substr($contractorData['requisites'], 4, 20);
                    $contractor->BIC = mb_substr($contractorData['requisites'], mb_stripos($contractorData['requisites'], 'БИК:') + 4, 9);
                }

                if ($contractor->save()) {
                    echo "Contractor {$contractor->id} created \r\n";
                } else {
                    echo "Error: \r\n";
                    foreach ($contractor->getErrors() as $attribute => $error) {
                        echo "{$attribute} - {$error[0]} - value - {$contractor->$attribute} \r\n";
                    }
                }
            } else {
                echo "Error: empty params. Name - {$contractorData['name']} \r\n";
            }
        }
    }
}