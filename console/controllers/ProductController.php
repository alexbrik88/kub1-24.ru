<?php

namespace console\controllers;

use common\models\address\Country;
use common\models\product\Product;
use common\models\product\ProductStore;
use common\models\product\ProductUnit;
use common\models\TaxRate;
use Yii;
use yii\console\Controller;

/**
 * @package console\controllers
 */
class ProductController extends Controller
{
    public $layoutWrapperCssClass = '';

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'update-store' => [
                'class' => 'console\components\product\UpdateStoreAction',
            ],
            'update-reserve' => [
                'class' => 'console\components\product\UpdateReserveAction',
            ],
        ];
    }

    /**
     * пересчитать количество на складе согласно выставленных документов
     */
    public function actionCalcStoreCount($company_id = null)
    {
        if ($company_id) {
            ProductStore::recalculateAllByCompanyId($company_id);

            $this->stdout("Done\n");
        } else {
            $this->stdout("Error: Company ID is empty\n");
        }
    }

    /**
     * У компании с id 10516, в связи с действиями черной силы, 42191 товаров были перенесены в левую группу. Необходимо все вернуть как было
     */
    public function actionUpdateGroup()
    {
        $productIDs = Product::find()
            ->byCompany(10516)
            ->byProductionType(Product::PRODUCTION_TYPE_GOODS)
            ->byDeleted()
            ->andWhere(['group_id' => 21230])
            ->column();
        $time = microtime(true);

        Yii::$app->db->createCommand()->update('product', ['group_id' => 21872], ['id' => $productIDs])->execute();

        echo ' done (time: ' . sprintf('%.3f', microtime(true) - $time) . "s)\n";
    }

    /**
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     */
    public function actionImportService()
    {
        $companyID = 21812;
        $attributeNames = [
            'A' => 'name',
            'B' => 'productUnit',
        ];
        $xls = \PHPExcel_IOFactory::load(\Yii::getAlias('@frontend/web/upload/Services.xlsx'));
        $xls->setActiveSheetIndex(0);
        $sheet = $xls->getActiveSheet();
        $rowIterator = $sheet->getRowIterator();
        $service = [];

        foreach ($rowIterator as $key => $row) {
            if ($key == 1) continue;
            $cellIterator = $row->getCellIterator();
            /* @var \PHPExcel_Cell $cell */
            foreach ($cellIterator as $cellKey => $cell) {
                if (isset($attributeNames[$cellKey])) {
                    $service[$attributeNames[$cellKey]] = trim($cell->getFormattedValue());
                }
            }
            if ($service['name'] && $service['productUnit']) {
                /* @var $productUnit ProductUnit */
                if ($service['productUnit'] == 'дн') {
                    $productUnit = ProductUnit::findOne(ProductUnit::UNIT_DAY);
                } else {
                    $productUnit = ProductUnit::find()
                        ->andWhere(['services' => true])
                        ->andWhere(['name' => $service['productUnit']])
                        ->one();
                }
                if ($productUnit) {
                    $product = new Product();
                    $product->company_id = $companyID;
                    $product->production_type = Product::PRODUCTION_TYPE_SERVICE;
                    $product->title = $service['name'];
                    $product->price_for_buy_with_nds = 0;
                    $product->price_for_sell_with_nds = 0;
                    $product->price_for_sell_nds_id = TaxRate::RATE_WITHOUT;
                    $product->price_for_buy_nds_id = TaxRate::RATE_WITHOUT;
                    $product->country_origin_id = Country::COUNTRY_WITHOUT;
                    $product->product_unit_id = $productUnit->id;
                    if ($product->save()) {
                        echo "Product {$product->id} created \r\n";
                    } else {
                        echo "Error: \r\n";
                        foreach ($product->getErrors() as $attribute => $error) {
                            echo "{$attribute} - {$error[0]} \r\n";
                        }
                    }
                } else {
                    echo "Error: productUnit not found. productUnit - {$service['productUnit']} \r\n";
                }
            } else {
                echo "Error: empty params. Name - {$service['name']}, productUnit - {$service['productUnit']} \r\n";
            }
        }
    }
}
