<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 12.09.2017
 * Time: 8:22
 */

namespace console\controllers;

use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use console\components\chat\SocketServer;
use Yii;
use yii\console\Controller;
use yii\helpers\ArrayHelper;

/**
 * Class SocketController
 * @package console\controllers
 */
class SocketController extends Controller
{
    /**
     *
     */
    public function actionStartSocket()
    {
        $params = ArrayHelper::getValue(Yii::$app->params, 'chat', []);

        $server = IoServer::factory(
            new HttpServer(
                new WsServer(
                    new SocketServer()
                )
            ),
            ArrayHelper::getValue($params, 'port', YII_ENV_DEV ? 8085 : 8086),
            ArrayHelper::getValue($params, 'host', YII_ENV_DEV ? '0.0.0.0' : '127.0.0.1')
        );

        $server->run();
    }
}
