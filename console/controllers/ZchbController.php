<?php

namespace console\controllers;

use common\components\zchb\ZCHBAPIException;
use common\components\zchb\ZCHBHelper;
use yii\console\Controller;

/**
 * `./yii zchb/status`
 */
class ZchbController extends Controller
{

    /**
     * Статистика запросов по текущему тарифу
     * @throws ZCHBAPIException
     */
    public function actionStatus()
    {
        $helper = ZCHBHelper::instance(false);
        $result = $helper->request('stats');
        echo 'End of the tariff: ', $result['end_date'], "\n";
        echo 'Today requests: ', $result['sum_request'], "\n";
        echo 'Remaining requests: ', $result['rem_request'], "\n";
        echo "----------------\n";
        foreach (json_decode($result['stats']) as $date => $cnt) {
            echo $date, ': ', $cnt, "\n";
        }
    }
}