<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 02.05.2018
 * Time: 6:04
 */

namespace console\controllers;

use common\components\date\DateHelper;
use common\components\sender\unisender\UniSender;
use common\components\TextHelper;
use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;
use common\models\paymentReminder\PaymentReminderMessage;
use common\models\paymentReminder\PaymentReminderMessageContractor;
use frontend\models\Documents;
use yii\console\Controller;
use Yii;

/**
 * Class PaymentReminderController
 * @package console\controllers
 */
class PaymentReminderController extends Controller
{
    /**
     * @throws \yii\base\Exception
     */
    public function actionSendMessages()
    {
        /**
         * Old controller
         * See ReminderController
         */
        return;

        date_default_timezone_set('Europe/Moscow');
        $currentDate = date(DateHelper::FORMAT_DATE_WITHOUT_SECOND);

        /* @var $paymentReminderMessageContractors PaymentReminderMessageContractor[] */
        $paymentReminderMessageContractors = PaymentReminderMessageContractor::find()
            ->andWhere(['or',
                ['message_1' => true],
                ['message_2' => true],
                ['message_3' => true],
                ['message_4' => true],
                ['message_5' => true],
                ['message_6' => true],
                ['message_7' => true],
                ['message_8' => true],
                ['message_9' => true],
                ['message_10' => true],
            ])
            ->all();
        foreach ($paymentReminderMessageContractors as $paymentReminderMessageContractor) {
            foreach (range(1, 10) as $number) {
                $attribute = 'message_' . $number;
                $messageTemplate = $paymentReminderMessageContractor->getMessageTemplate($number, $paymentReminderMessageContractor->company_id);
                if ($paymentReminderMessageContractor->$attribute &&
                    $paymentReminderMessageContractor->getMessageTemplateStatus($number, $paymentReminderMessageContractor->company_id) &&
                    $messageTemplate
                ) {
                    if ($number == PaymentReminderMessage::MESSAGE_1) {
                        if ($messageTemplate->days) {
                            $time = $messageTemplate->time ?
                                DateHelper::format($messageTemplate->time, 'H:i', 'H:i:s') :
                                '09:45';
                            /* @var $invoices Invoice[] */
                            $invoices = Invoice::find()
                                ->byIOType(Documents::IO_TYPE_OUT)
                                ->byCompany($paymentReminderMessageContractor->company_id)
                                ->byContractorId($paymentReminderMessageContractor->contractor_id)
                                ->byStatus([InvoiceStatus::STATUS_CREATED, InvoiceStatus::STATUS_SEND, InvoiceStatus::STATUS_VIEWED,
                                    InvoiceStatus::STATUS_APPROVED, InvoiceStatus::STATUS_PAYED_PARTIAL,])
                                ->byDeleted()
                                ->all();
                            foreach ($invoices as $invoice) {
                                $sendDate = date(DateHelper::FORMAT_DATE, strtotime('-' . $messageTemplate->days . ' days', strtotime($invoice->payment_limit_date))) . ' ' . $time;
                                if ($sendDate == $currentDate) {
                                    $email = $this->generateEmail($messageTemplate, $paymentReminderMessageContractor);
                                    if (!empty($email)) {
                                        $content = $this->generateContent($paymentReminderMessageContractor, $invoice, $messageTemplate);
                                        $this->sendMessage($messageTemplate, $content, $email, $invoice->documentAuthor->currentEmployeeCompany);
                                        $paymentReminderMessageContractor->setAttribute("last_message_date_{$number}", date(DateHelper::FORMAT_DATE));
                                        $paymentReminderMessageContractor->save(true, ["last_message_date_{$number}"]);
                                    }
                                }
                            }
                        }
                    } elseif (in_array($number, [PaymentReminderMessage::MESSAGE_2, PaymentReminderMessage::MESSAGE_3,
                        PaymentReminderMessage::MESSAGE_4, PaymentReminderMessage::MESSAGE_5,])) {
                        if ($messageTemplate->days) {
                            $time = $messageTemplate->time ?
                                DateHelper::format($messageTemplate->time, 'H:i', 'H:i:s') :
                                '09:45';
                            /* @var $invoices Invoice[] */
                            $invoices = Invoice::find()
                                ->byIOType(Documents::IO_TYPE_OUT)
                                ->byCompany($paymentReminderMessageContractor->company_id)
                                ->byContractorId($paymentReminderMessageContractor->contractor_id)
                                ->byStatus([InvoiceStatus::STATUS_OVERDUE,])
                                ->byDeleted()
                                ->all();
                            foreach ($invoices as $invoice) {
                                $sendDate = date(DateHelper::FORMAT_DATE, strtotime('+' . $messageTemplate->days . ' days', strtotime($invoice->payment_limit_date))) . ' ' . $time;
                                if ($sendDate == $currentDate) {
                                    $email = $this->generateEmail($messageTemplate, $paymentReminderMessageContractor);
                                    if (!empty($email)) {
                                        $content = $this->generateContent($paymentReminderMessageContractor, $invoice, $messageTemplate);
                                        $this->sendMessage($messageTemplate, $content, $email, $invoice->documentAuthor->currentEmployeeCompany);
                                        $paymentReminderMessageContractor->setAttribute("last_message_date_{$number}", date(DateHelper::FORMAT_DATE));
                                        $paymentReminderMessageContractor->save(true, ["last_message_date_{$number}"]);
                                    }
                                }
                            }
                        }
                    } elseif (in_array($number, [PaymentReminderMessage::MESSAGE_6, PaymentReminderMessage::MESSAGE_7,
                        PaymentReminderMessage::MESSAGE_8, PaymentReminderMessage::MESSAGE_9,])) {
                        if ($messageTemplate->debt_sum) {
                            $time = $messageTemplate->time ?
                                DateHelper::format($messageTemplate->time, 'H:i', 'H:i:s') :
                                '09:45';
                            /* @var $invoices Invoice[] */
                            $invoices = Invoice::find()
                                ->byIOType(Documents::IO_TYPE_OUT)
                                ->byCompany($paymentReminderMessageContractor->company_id)
                                ->byContractorId($paymentReminderMessageContractor->contractor_id)
                                ->byStatus(InvoiceStatus::STATUS_OVERDUE)
                                ->byDeleted()
                                ->orderBy(['payment_limit_date' => SORT_ASC])
                                ->all();
                            $debtSum = 0;
                            foreach ($invoices as $invoice) {
                                $debtSum += $invoice->remaining_amount;
                                if ($debtSum >= ($messageTemplate->debt_sum * 100)) {
                                    if ($number == PaymentReminderMessage::MESSAGE_6) {
                                        if ($invoice->payment_limit_date == date(DateHelper::FORMAT_DATE) && date('H:i') == $time) {
                                            $email = $this->generateEmail($messageTemplate, $paymentReminderMessageContractor);
                                            if (!empty($email)) {
                                                $content = $this->generateContent($paymentReminderMessageContractor, $invoice, $messageTemplate);
                                                $this->sendMessage($messageTemplate, $content, $email, $invoice->documentAuthor->currentEmployeeCompany);
                                                $paymentReminderMessageContractor->setAttribute("last_message_date_{$number}", date(DateHelper::FORMAT_DATE));
                                                $paymentReminderMessageContractor->save(true, ["last_message_date_{$number}"]);
                                                break;
                                            }
                                        }
                                    } elseif ($messageTemplate->days) {
                                        $sendDate = date(DateHelper::FORMAT_DATE, strtotime('+' . $messageTemplate->days . ' days', strtotime($invoice->payment_limit_date))) . ' ' . $time;
                                        if ($sendDate == $currentDate) {
                                            $email = $this->generateEmail($messageTemplate, $paymentReminderMessageContractor);
                                            if (!empty($email)) {
                                                $content = $this->generateContent($paymentReminderMessageContractor, $invoice, $messageTemplate);
                                                $this->sendMessage($messageTemplate, $content, $email, $invoice->documentAuthor->currentEmployeeCompany);
                                                $paymentReminderMessageContractor->setAttribute("last_message_date_{$number}", date(DateHelper::FORMAT_DATE));
                                                $paymentReminderMessageContractor->save(true, ["last_message_date_{$number}"]);
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    } else {
                        /* @var $invoices Invoice[] */
                        $invoices = Invoice::find()
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byCompany($paymentReminderMessageContractor->company_id)
                            ->byContractorId($paymentReminderMessageContractor->contractor_id)
                            ->byStatus([InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_PAYED_PARTIAL])
                            ->byDeleted()
                            ->andWhere(['>', 'invoice_status_updated_at', strtotime('- 1 days')])
                            ->all();
                        foreach ($invoices as $invoice) {
                            $paymentType = $invoice->getPaymentType();
                            if (
                                ($messageTemplate->not_send_where_emoney_payment && $paymentType == 3) ||
                                ($messageTemplate->not_send_where_order_payment && $paymentType == 2) ||
                                ($messageTemplate->not_send_where_partial_payment && $invoice->remaining_amount > 0)
                            ) {
                                continue;
                            }
                            if (date(DateHelper::FORMAT_DATE_WITHOUT_SECOND, strtotime('+ 5 minutes', $invoice->invoice_status_updated_at)) == $currentDate) {
                                $email = $this->generateEmail($messageTemplate, $paymentReminderMessageContractor);
                                if (!empty($email)) {
                                    $content = $this->generateContent($paymentReminderMessageContractor, $invoice, $messageTemplate);
                                    $this->sendMessage($messageTemplate, $content, $email, $invoice->documentAuthor->currentEmployeeCompany);
                                    $paymentReminderMessageContractor->setAttribute("last_message_date_{$number}", date(DateHelper::FORMAT_DATE));
                                    $paymentReminderMessageContractor->setAttribute("last_message_contractor_email_{$number}", end($email));
                                    $paymentReminderMessageContractor->save(true, ["last_message_date_{$number}", "last_message_contractor_email_{$number}"]);
                                }
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * @param PaymentReminderMessageContractor $paymentReminderMessageContractor
     * @param Invoice $invoice
     * @param PaymentReminderMessage $messageTemplate
     * @return mixed|string
     */
    public function generateContent(PaymentReminderMessageContractor $paymentReminderMessageContractor, Invoice $invoice, PaymentReminderMessage $messageTemplate)
    {
        $contractorFio = $paymentReminderMessageContractor->contractor->getDirectorFio();
        $contractorName = $paymentReminderMessageContractor->contractor->getTitle(true);
        $invoiceNumber = $invoice->getFullNumber();
        $invoiceDate = DateHelper::format($invoice->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
        $invoiceSum = TextHelper::invoiceMoneyFormat($invoice->total_amount_with_nds, 2);
        $agreement = $invoice->basis_document_number . ' ' .
            (DateHelper::format($invoice->basis_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE));
        $paymentPeriod = date_diff(new \DateTime($invoice->document_date), new \DateTime($invoice->payment_limit_date))->days;
        $overdueDays = date_diff(new \DateTime($invoice->payment_limit_date), new \DateTime())->days;

        $content = strip_tags($messageTemplate->message_template_body, '<br>');
        $content = str_replace('[Имя покупателя]', $contractorFio, $content);
        $content = str_replace('[Название компании покупателя]', $contractorName, $content);
        $content = str_replace('[№ и дата договора]', empty($agreement) ? '' : $agreement, $content);
        $content = str_replace('[Период оплаты]', $paymentPeriod, $content);
        $content = str_replace('[№ счета]', $invoiceNumber, $content);
        $content = str_replace('[Дата счета]', $invoiceDate, $content);
        $content = str_replace('[Сумма по счету]', $invoiceSum, $content);
        $content = str_replace('[Просрочено дней]', $overdueDays, $content);

        return $content;
    }

    /**
     * @param PaymentReminderMessage $messageTemplate
     * @param PaymentReminderMessageContractor $paymentReminderMessageContractor
     * @return array
     */
    public function generateEmail(PaymentReminderMessage $messageTemplate, PaymentReminderMessageContractor $paymentReminderMessageContractor)
    {
        $email = [];
        if (in_array($messageTemplate->number, [PaymentReminderMessage::MESSAGE_5], PaymentReminderMessage::MESSAGE_9)) {
            foreach ($messageTemplate->for_email_list as $oneEmail) {
                if ($oneEmail) {
                    $email[] = $oneEmail;
                }
            }
        } else {
            if (($messageTemplate->send_for_chief || $messageTemplate->send_for_all) && $paymentReminderMessageContractor->contractor->director_email) {
                $email[] = $paymentReminderMessageContractor->contractor->director_email;
            }
            if (($messageTemplate->send_for_contact || $messageTemplate->send_for_all) && $paymentReminderMessageContractor->contractor->contact_email) {
                $email[] = $paymentReminderMessageContractor->contractor->contact_email;
            }
            if ($messageTemplate->send_for_all && $paymentReminderMessageContractor->contractor->chief_accountant_email) {
                $email[] = $paymentReminderMessageContractor->contractor->chief_accountant_email;
            }
        }

        return $email;
    }

    /**
     * @param PaymentReminderMessage $messageTemplate
     * @param $content
     * @param $email
     * @throws \yii\base\Exception
     */
    public function sendMessage(PaymentReminderMessage $messageTemplate, $content, $email, $author)
    {
        $uniSender = new UniSender();
        $uniSender->setEmailTemplateName('payment-reminder')
            ->setFromEmail(Yii::$app->params['emailList']['info'])
            ->setFromName($author->getFio(true))
            ->setParams([
                'supportEmail' => Yii::$app->params['emailList']['support'],
                'subject' => $messageTemplate->message_template_subject,
                'content' => $content,
                'author' => $author,
            ])
            ->setContacts($email)
            ->setSubject($messageTemplate->message_template_subject)
            ->send(false);
    }
}