<?php
/**
 * Created by PhpStorm.
 * User: hp-m6
 * Date: 09.03.2017
 * Time: 18:34
 */

namespace console\controllers;

use common\components\DbTimeZone;
use common\components\date\DateHelper;
use common\components\pdf\PdfRenderer;
use common\models\Company;
use common\models\Contractor;
use common\models\EmployeeCompany;
use common\models\TimeZone;
use common\models\document\Act;
use common\models\document\Autoinvoice;
use common\models\document\Invoice;
use common\models\document\InvoiceAuto;
use common\models\document\status\InvoiceStatus;
use common\models\employee\Employee;
use frontend\models\log\LogDocumentEmail;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use php_rutils\RUtils;
use DateTime;
use Yii;
use yii\console\Controller;
use yii\db\Query;

/**
 * Class AutoinvoiceController
 * @package console\controllers
 */
class AutoinvoiceController extends Controller
{
    public $defaultAction = 'create';

    /**
     * @var string
     */
    public $layoutWrapperCssClass = '';

    /**
     * @param string $date
     */
    public function actionCreate($date = null)
    {
        $this->stdout("start of autoinvoice creation\n");

        Yii::$app->urlManager->baseUrl = '/';
        Yii::$app->urlManager->hostInfo = Yii::$app->params['serviceSiteLk'];
        Yii::setAlias('@webroot', '@frontend/web');
        Yii::setAlias('@web', '@frontend/web');

        $timeZoneArray = TimeZone::find()
            ->rightJoin(Employee::tableName(), '{{time_zone}}.[[id]] = IFNULL({{employee}}.[[time_zone_id]], 57)')
            ->rightJoin(Company::tableName(), '{{employee}}.[[id]] = {{company}}.[[owner_employee_id]]')
            ->andWhere([
                'company.blocked' => Company::UNBLOCKED,
            ])->select([
                'time_zone.id',
                'time_zone.time_zone',
            ])
            ->distinct()
            ->all();

        foreach ($timeZoneArray as $timeZone) {
            $this->create($timeZone, $date);
        }

        $this->stdout("end of autoinvoice creation\n");
    }

    /**
     * @param string $date
     */
    public function create($timeZone, $date = null)
    {
        try {
            date_default_timezone_set($timeZone->time_zone);
            DbTimeZone::sync();
        } catch (\Exception $e) {
            return;
        }

        $nowDate = new DateTime();

        if ($date !== null) {
            $targetDate = date_create($date)->modify('noon');
            $nowDate->modify('noon');
            if (!$targetDate || ($targetDate >= $nowDate)) {
                echo "\"{$date}\" - is not correct date\n";

                return;
            }
        } else {
            if (($h = $nowDate->format('G')) < 6 || $h > 22) {
                return;
            }

            $targetDate = clone $nowDate;
        }

        $this->stdout("creating for {$timeZone->time_zone}\n");

        $this->createMonthly($targetDate, $nowDate, $timeZone);
        $this->createQuarterly($targetDate, $nowDate, $timeZone);
        $this->createHalfyear($targetDate, $nowDate, $timeZone);
        $this->createAnnually($targetDate, $nowDate, $timeZone);
    }

    /**
     * @param string $date
     */
    public function createMonthly($targetDate, $nowDate, $timeZone)
    {
        $this->stdout("start monthly creation\n");

        $currentDateString = $targetDate->format('Y-m-d');
        $currentDayNumber = (int)$targetDate->format('j');
        $currentDaysInMonth = (int)$targetDate->format('t');
        $currentWeekDay = (int)$targetDate->format('N');

        if ($currentWeekDay > 5) {
            return;
        }

        $minDay = $maxDay = $currentDayNumber;

        if ($currentWeekDay == 1) {
            $minDay = max(1, ($currentDayNumber - 2));
        }
        if ($currentWeekDay == 5) {
            $nextMonday = (clone $targetDate)->modify('Monday next week');
            if ($nextMonday->format('m') != $targetDate->format('m')) {
                $maxDay = 31;
            }
        }
        if ($currentDaysInMonth < 31 && $currentDaysInMonth == $currentDayNumber) {
            $maxDay = 31;
        }

        $nextDateObject = clone $targetDate;
        $nextDateObject->modify('first day of + 1 month');
        $nextDaysInMonth = (int) $nextDateObject->format('t');
        $nextDatePart = $nextDateObject->format('Y-m-');

        $query = Autoinvoice::find()
            ->select('autoinvoice.id')
            ->joinWith('company')
            ->joinWith('contractor')
            ->leftJoin(Employee::tableName(), '{{employee}}.[[id]] = {{company}}.[[owner_employee_id]]')
            ->where([
                'and',
                ['autoinvoice.status' => 1],
                ['autoinvoice.period' => Autoinvoice::MONTHLY],
                ['company.blocked' => Company::UNBLOCKED],
                ['employee.time_zone_id' => $timeZone->id],
                ['contractor.status' => Contractor::ACTIVE],
                ['contractor.is_deleted' => Contractor::NOT_DELETED],
                ['<=', 'autoinvoice.date_from', $currentDateString],
                ['>=', 'autoinvoice.date_to', $currentDateString],
                [
                    'or',
                    ['autoinvoice.last_invoice_date' => null],
                    ($targetDate != $nowDate) ?
                        ['not between', 'autoinvoice.last_invoice_date', $currentDateString, $nowDate->format('Y-m-d')] :
                        ['not', ['autoinvoice.last_invoice_date' => $currentDateString]],
                ],
                ($minDay == $maxDay) ?
                    ['autoinvoice.day' => $minDay] :
                    ['between', 'autoinvoice.day', $minDay, $maxDay],
            ]);

        $idArray = $query->column();

        foreach ($idArray as $id) {
            $autoinvoice = InvoiceAuto::find()->byDeleted(false)->andWhere(['id' => $id])->one();

            if ($autoinvoice !== null) {
                try {
                    $model = $autoinvoice->createFromAuto(clone $targetDate);
                    if ($model) {
                        $autoinvoice->auto->updateAttributes([
                            'last_invoice_date' => $nowDate->format('Y-m-d'),
                            'next_invoice_date' => $nextDatePart .
                                str_pad(min($nextDaysInMonth, $autoinvoice->auto->day), 2, '0', STR_PAD_LEFT),
                        ]);

                        $this->sendEmail($model, $autoinvoice);
                    }
                } catch (\Exception $e) {
                    // do nothing
                }
            }
        }

        $this->stdout("end monthly creation\n");

        return;
    }

    /**
     * @param string $date
     */
    public function createQuarterly($targetDate, $nowDate, $timeZone)
    {
        $this->stdout("start quarterly creation\n");

        $month = $targetDate->format('n');
        $quarter = ceil($month / 3);
        $monthOfPeriod = ($month - 1) % 3 + 1;

        $currentDateString = $targetDate->format('Y-m-d');
        $currentDayNumber = (int)$targetDate->format('j');
        $currentDaysInMonth = (int)$targetDate->format('t');
        $currentWeekDay = (int)$targetDate->format('N');

        if ($currentWeekDay > 5) {
            return;
        }

        $minDay = $maxDay = $currentDayNumber;

        if ($currentWeekDay == 1) {
            $minDay = max(1, ($currentDayNumber - 2));
        }
        if ($currentWeekDay == 5) {
            $nextMonday = (clone $targetDate)->modify('Monday next week');
            if ($nextMonday->format('m') != $targetDate->format('m')) {
                $maxDay = 31;
            }
        }
        if ($currentDaysInMonth < 31 && $currentDaysInMonth == $currentDayNumber) {
            $maxDay = 31;
        }

        $nextDateObject = clone $targetDate;
        $nextDateObject->modify('first day of + 3 month');
        $nextDaysInMonth = $nextDateObject->format('t');
        $nextDatePart = $nextDateObject->format('Y-m-');

        $query = Autoinvoice::find()
            ->select('autoinvoice.id')
            ->joinWith('company')
            ->joinWith('contractor')
            ->leftJoin(Employee::tableName(), '{{employee}}.[[id]] = {{company}}.[[owner_employee_id]]')
            ->where([
                'and',
                ['autoinvoice.status' => 1],
                ['autoinvoice.period' => Autoinvoice::QUARTERLY],
                ['autoinvoice.month' => $monthOfPeriod],
                ['company.blocked' => Company::UNBLOCKED],
                ['employee.time_zone_id' => $timeZone->id],
                ['contractor.status' => Contractor::ACTIVE],
                ['contractor.is_deleted' => Contractor::NOT_DELETED],
                ['<=', 'autoinvoice.date_from', $currentDateString],
                ['>=', 'autoinvoice.date_to', $currentDateString],
                [
                    'or',
                    ['autoinvoice.last_invoice_date' => null],
                    ($targetDate != $nowDate) ?
                        ['not between', 'autoinvoice.last_invoice_date', $currentDateString, $nowDate->format('Y-m-d')] :
                        ['not', ['autoinvoice.last_invoice_date' => $currentDateString]],
                ],
                ($minDay == $maxDay) ?
                    ['autoinvoice.day' => $minDay] :
                    ['between', 'autoinvoice.day', $minDay, $maxDay],
            ]);

        $idArray = $query->column();

        foreach ($idArray as $id) {
            $autoinvoice = InvoiceAuto::find()->byDeleted(false)->andWhere(['id' => $id])->one();

            if ($autoinvoice !== null) {
                try {
                    $model = $autoinvoice->createFromAuto(clone $targetDate, $quarter);
                    if ($model) {
                        $autoinvoice->auto->updateAttributes([
                            'last_invoice_date' => $nowDate->format('Y-m-d'),
                            'next_invoice_date' => $nextDatePart .
                                str_pad(min($nextDaysInMonth, $autoinvoice->auto->day), 2, '0', STR_PAD_LEFT),
                        ]);

                        $this->sendEmail($model, $autoinvoice);
                    }
                } catch (\Exception $e) {
                    // do nothing
                }
            }
        }

        $this->stdout("end quarterly creation\n");

        return;
    }

    /**
     * @param string $date
     */
    public function createHalfyear($targetDate, $nowDate, $timeZone)
    {
        $this->stdout("start halfyear creation\n");

        $month = $targetDate->format('n');
        $halfyear = ceil($month / 6);
        $monthOfPeriod = ($month - 1) % 6 + 1;

        $currentDateString = $targetDate->format('Y-m-d');
        $currentDayNumber = (int)$targetDate->format('j');
        $currentDaysInMonth = (int)$targetDate->format('t');
        $currentWeekDay = (int)$targetDate->format('N');

        if ($currentWeekDay > 5) {
            return;
        }

        $minDay = $maxDay = $currentDayNumber;

        if ($currentWeekDay == 1) {
            $minDay = max(1, ($currentDayNumber - 2));
        }
        if ($currentWeekDay == 5) {
            $nextMonday = (clone $targetDate)->modify('Monday next week');
            if ($nextMonday->format('m') != $targetDate->format('m')) {
                $maxDay = 31;
            }
        }
        if ($currentDaysInMonth < 31 && $currentDaysInMonth == $currentDayNumber) {
            $maxDay = 31;
        }

        $nextDateObject = clone $targetDate;
        $nextDateObject->modify('first day of + 6 month');
        $nextDaysInMonth = $nextDateObject->format('t');
        $nextDatePart = $nextDateObject->format('Y-m-');

        $query = Autoinvoice::find()
            ->select('autoinvoice.id')
            ->joinWith('company')
            ->joinWith('contractor')
            ->leftJoin(Employee::tableName(), '{{employee}}.[[id]] = {{company}}.[[owner_employee_id]]')
            ->where([
                'and',
                ['autoinvoice.status' => 1],
                ['autoinvoice.period' => Autoinvoice::HALF_YEAR],
                ['autoinvoice.month' => $monthOfPeriod],
                ['company.blocked' => Company::UNBLOCKED],
                ['employee.time_zone_id' => $timeZone->id],
                ['contractor.status' => Contractor::ACTIVE],
                ['contractor.is_deleted' => Contractor::NOT_DELETED],
                ['<=', 'autoinvoice.date_from', $currentDateString],
                ['>=', 'autoinvoice.date_to', $currentDateString],
                [
                    'or',
                    ['autoinvoice.last_invoice_date' => null],
                    ($targetDate != $nowDate) ?
                        ['not between', 'autoinvoice.last_invoice_date', $currentDateString, $nowDate->format('Y-m-d')] :
                        ['not', ['autoinvoice.last_invoice_date' => $currentDateString]],
                ],
                ($minDay == $maxDay) ?
                    ['autoinvoice.day' => $minDay] :
                    ['between', 'autoinvoice.day', $minDay, $maxDay],
            ]);

        $idArray = $query->column();

        foreach ($idArray as $id) {
            $autoinvoice = InvoiceAuto::find()->byDeleted(false)->andWhere(['id' => $id])->one();

            if ($autoinvoice !== null) {
                try {
                    $model = $autoinvoice->createFromAuto(clone $targetDate, $halfyear);
                    if ($model) {
                        $autoinvoice->auto->updateAttributes([
                            'last_invoice_date' => $nowDate->format('Y-m-d'),
                            'next_invoice_date' => $nextDatePart .
                                str_pad(min($nextDaysInMonth, $autoinvoice->auto->day), 2, '0', STR_PAD_LEFT),
                        ]);

                        $this->sendEmail($model, $autoinvoice);
                    }
                } catch (\Exception $e) {
                    // do nothing
                }
            }
        }

        $this->stdout("end halfyear creation\n");

        return;
    }

    /**
     * @param string $date
     */
    public function createAnnually($targetDate, $nowDate, $timeZone)
    {
        $this->stdout("start annually creation\n");

        $monthOfPeriod = $targetDate->format('n');

        $currentDateString = $targetDate->format('Y-m-d');
        $currentDayNumber = (int)$targetDate->format('j');
        $currentDaysInMonth = (int)$targetDate->format('t');
        $currentWeekDay = (int)$targetDate->format('N');

        if ($currentWeekDay > 5) {
            return;
        }

        $minDay = $maxDay = $currentDayNumber;

        if ($currentWeekDay == 1) {
            $minDay = max(1, ($currentDayNumber - 2));
        }
        if ($currentWeekDay == 5) {
            $nextMonday = (clone $targetDate)->modify('Monday next week');
            if ($nextMonday->format('m') != $targetDate->format('m')) {
                $maxDay = 31;
            }
        }
        if ($currentDaysInMonth < 31 && $currentDaysInMonth == $currentDayNumber) {
            $maxDay = 31;
        }

        $nextDateObject = clone $targetDate;
        $nextDateObject->modify('first day of + 1 year');
        $nextDaysInMonth = $nextDateObject->format('t');
        $nextDatePart = $nextDateObject->format('Y-m-');

        $query = Autoinvoice::find()
            ->select('autoinvoice.id')
            ->joinWith('company')
            ->joinWith('contractor')
            ->leftJoin(Employee::tableName(), '{{employee}}.[[id]] = {{company}}.[[owner_employee_id]]')
            ->where([
                'and',
                ['autoinvoice.status' => 1],
                ['autoinvoice.period' => Autoinvoice::ANNUALLY],
                ['autoinvoice.month' => $monthOfPeriod],
                ['company.blocked' => Company::UNBLOCKED],
                ['employee.time_zone_id' => $timeZone->id],
                ['contractor.status' => Contractor::ACTIVE],
                ['contractor.is_deleted' => Contractor::NOT_DELETED],
                ['<=', 'autoinvoice.date_from', $currentDateString],
                ['>=', 'autoinvoice.date_to', $currentDateString],
                [
                    'or',
                    ['autoinvoice.last_invoice_date' => null],
                    ($targetDate != $nowDate) ?
                        ['not between', 'autoinvoice.last_invoice_date', $currentDateString, $nowDate->format('Y-m-d')] :
                        ['not', ['autoinvoice.last_invoice_date' => $currentDateString]],
                ],
                ($minDay == $maxDay) ?
                    ['autoinvoice.day' => $minDay] :
                    ['between', 'autoinvoice.day', $minDay, $maxDay],
            ]);

        $idArray = $query->column();

        foreach ($idArray as $id) {
            $autoinvoice = InvoiceAuto::find()->byDeleted(false)->andWhere(['id' => $id])->one();

            if ($autoinvoice !== null) {
                try {
                    $model = $autoinvoice->createFromAuto(clone $targetDate);
                    if ($model) {
                        $autoinvoice->auto->updateAttributes([
                            'last_invoice_date' => $nowDate->format('Y-m-d'),
                            'next_invoice_date' => $nextDatePart .
                                str_pad(min($nextDaysInMonth, $autoinvoice->auto->day), 2, '0', STR_PAD_LEFT),
                        ]);

                        $this->sendEmail($model, $autoinvoice);
                    }
                } catch (\Exception $e) {
                    // do nothing
                }
            }
        }

        $this->stdout("end annually creation\n");

        return;
    }

    /**
     *
     */
    public function actionUpdateNextInvoiceDate()
    {
        $autoInvoices = Autoinvoice::find()
            ->select([
                'id',
                'day',
            ])
            ->andWhere(['status' => Autoinvoice::ACTIVE])
            ->andWhere(['period' => Autoinvoice::MONTHLY])
            ->andWhere(['>', 'date_to', date(DateHelper::FORMAT_DATE)])
            ->andWhere(['<', 'next_invoice_date', date(DateHelper::FORMAT_DATE)])
            ->andWhere(['<', 'date_from', date(DateHelper::FORMAT_DATE)])
            ->asArray()
            ->all();
        foreach ($autoInvoices as $autoInvoice) {
            $autoInvoice->setNextDate(null, true);
        }
    }

    /**
     *
     */
    private function sendEmail($model, $autoinvoice)
    {
        $auto = $autoinvoice->auto;
        $contractor = $autoinvoice->contractor;

        $sendTo = [];
        if ($auto->send_to_director || $auto->send_to_chief_accountant || $auto->send_to_contact) {
            if ($auto->send_to_director && $contractor->director_email) {
                $sendTo[] = $contractor->director_email;
            }
            if ($auto->send_to_chief_accountant && $contractor->chief_accountant_email) {
                $sendTo[] = $contractor->chief_accountant_email;
            }
            if ($auto->send_to_contact && $contractor->contact_email) {
                $sendTo[] = $contractor->contact_email;
            }
        } elseif ($email = $model->contractor->someEmail) {
            $sendTo[] = $email;
        }

        $sendTo = array_filter($sendTo, function ($email) {
            return filter_var($email, FILTER_VALIDATE_EMAIL);
        });

        if (count($sendTo) > 0) {
            $sender = EmployeeCompany::findOne([
                'company_id' => $autoinvoice->company_id,
                'employee_id' => $contractor->responsible_employee_id ? : $autoinvoice->document_author_id,
            ]);
            if ($sender) {
                foreach ($sendTo as $toEmail) {
                    $this->_send($model, $sender, $toEmail);
                }
            }
        }
    }

    /**
     *
     */
    private function _send($model, $sender, $toEmail)
    {
        $sendError = 0;
        while ($sendError < 2) {
            if ($sendError > 0) {
                sleep(5);
            }
            try {
                if ($model->sendAsEmail($sender, $toEmail)) {
                    LogDocumentEmail::saveEmail($toEmail, $model->company_id, $model->id, get_class($model));
                }
                Yii::$app->mailer->getTransport()->stop();

                return;
            } catch (\ErrorException $e) {
                Yii::$app->mailer->getTransport()->stop();
                $sendError++;
            }
        }
    }
}
