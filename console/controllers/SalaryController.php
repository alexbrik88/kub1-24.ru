<?php

namespace console\controllers;

use common\models\Company;
use common\models\employee\EmployeeSalarySummary;
use Yii;
use yii\console\Controller;

/**
 * Class SalaryController
 * @package console\controllers
 */
class SalaryController extends Controller
{
    /**
     *
     */
    public function actionCreate($date = null, $company_id = null)
    {
        set_time_limit(6000);
        Yii::$app->db->createCommand('SET session wait_timeout=6000')->execute();
        Yii::$app->db->createCommand('SET session interactive_timeout=6000')->execute();

        $date = new \DateTime($date);
        $date->modify('first day of this month');
        $mysqlDate = $date->format('Y-m-d');
        $idArray = $company_id ? [$company_id] : Company::find()->select('id')->andWhere(['blocked' => false])->column();

        foreach ($idArray as $id) {
            $company = Company::findOne($id);
            $query = EmployeeSalarySummary::find()->andWhere([
                'company_id' => $company->id,
                'date' => $mysqlDate,
            ]);

            if (!$query->exists()) {
                $model = EmployeeSalarySummary::new($company, $date);
                $model->save();
            }
        }
    }
}
