<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use common\models\Company;
use common\models\employee\Employee;
use common\components\getResponse\GetResponseApi;
use common\components\getResponse\GetResponseContact;

/**
 * Фоновые задачи обновления данных в сервисе GetResponse
 *
 * @package console\controllers
 */
class GetResponseController extends Controller
{
    const MAX_EXECUTION_TIME = 30 * 60;
    const MAX_FAILURE_ATTEMPTS = 10;

    /**
     *
     */
    public function actionRefresh()
    {
        Yii::$app->urlManager->baseUrl = '/';
        ini_set('memory_limit', '1024M');
        $time_start = microtime(true);

        ///////////////////////////
        $gr = new GetResponseApi();
        ///////////////////////////

        $grContacts = GetResponseContact::find()
            ->where(['<=', 'update_attempts_count', self::MAX_FAILURE_ATTEMPTS])
            ->orderBy(['updated_at' => SORT_ASC])
            ->select(['employee_id', 'company_id'])
            ->asArray()
            ->all();

        foreach ($grContacts as $grc)
        {
            $employee = Employee::findOne($grc['employee_id']);
            $company = Company::findOne($grc['company_id']);

            if (!$employee || !$company)
                continue;

            $subscribeInfo = GetResponseApi::getCompanySubscribeInfo($company);

            // Update contact in GetResponse service
            $isUpdated = $gr->updateGetResponseContact($employee, [
                'trial_14_days' => ($subscribeInfo['isTrial']) ? GetResponseApi::TRUE : GetResponseApi::FALSE,
                'trial_count' => $subscribeInfo['trialCountDays'],
                'tarif_count' => $subscribeInfo['tariffCountDays'],
                'last_sign_in' => date('Y-m-d H:i:s', $company->last_visit_at)
            ]);

            self::_updateAttempts($employee, $company, $isUpdated);

            if ($isUpdated) {
                echo "{$employee->email} updated\n";
            } else {
                echo "{$employee->email} not updated\n";
            }

            $time_end = microtime(true);

            if ($time_end - $time_start > self::MAX_EXECUTION_TIME) {

                // todo: add2log("GetResponseRefresh: exit by timeout")

                echo 'exit by timeout';
                exit;
            }
        }
    }

    private static function _updateAttempts(Employee $employee, Company $company, $isUpdated)
    {
        if ($isUpdated) {
            $attempt = ' `update_attempts_count` = 0 ';
            $updatedAt = ' `updated_at` = ' . time();
        } else {
            $attempt = ' `update_attempts_count` = `update_attempts_count` + 1 ';
            $updatedAt = ' `updated_at` = ' . time();
        }

        Yii::$app->db->createCommand("
          UPDATE `get_response_contact` 
          SET 
            {$attempt}, 
            {$updatedAt}
          WHERE 
            `employee_id` = {$employee->id} AND 
            `company_id` = {$company->id} LIMIT 1
        ")->execute();
    }
}