<?php

namespace console\controllers;

use frontend\modules\import\models\autoload\OneSAutoloadFile;
use frontend\modules\import\models\OneS;
use Yii;
use common\models\Company;
use common\models\employee\Employee;
use frontend\modules\import\models\autoload\OneSAutoloadEmail;
use frontend\modules\import\models\autoload\OneSAutoloadHelper;
use frontend\modules\import\models\autoload\OneSImportHelper;
use yii\console\Controller;

/**
 * Class OneSAutoloadController
 * @package console\controllers
 */
class OneSAutoloadController extends Controller
{
    // макс. время выполнения скрипта
    const MAX_EXECUTION_TIME = 4 * 60;
    // макс. кол-во параллельных запусков скрипта
    const MAX_IMPORT_THREADS = 1;
    // макс. кол-во почтовых ящиков, которые обрабатывает скрипт за 1 запуск
    const MAX_MAILBOXES_COUNT = 100;

    /**
     *
     */
    public function actionIndex()
    {
        Yii::$app->urlManager->baseUrl = '/';
        ini_set('memory_limit', '4096M');
        $time_start = microtime(true);

        if ($this->getInWorkMailboxesCount() >= self::MAX_IMPORT_THREADS) {
            echo 'no threads';
            exit;
        }

        /** @var $mailbox OneSAutoloadEmail */
        $mailboxes = $this->getMailboxes();
        foreach ($mailboxes as $mailbox) {

            if ($this->isInWork($mailbox))
                continue; // for some threads

            /** @var OneSAutoloadFile $modelFile */
            if ($modelFile = $mailbox->newFile) {

                $file = OneS::getUploadsDir() . DIRECTORY_SEPARATOR . $modelFile->filename;

                $mailbox->updateAttributes(['in_work' => 1]);
                $success = $this->importToDB($file, $modelFile->company);
                $mailbox->updateAttributes(['in_work' => 0]);

                if ($success) {
                    $mailbox->updateAttributes(['uploaded_at' => time()]);
                    $modelFile->updateAttributes(['uploaded_at' => time()]);
                    // todo: temp comment for task 20-329
                    // @unlink($file);
                } else {
                    $modelFile->updateAttributes(['uploaded_at' => -1]);
                    $mailbox->updateAttributes(['uploaded_at' => time(), 'is_exit_by_error' => true]);
                }

            } else {
                // update pos in queue
                $mailbox->updateAttributes(['uploaded_at' => time()]);
            }

            $time_end = microtime(true);

            if ($time_end - $time_start > self::MAX_EXECUTION_TIME) {
                echo 'exit by timeout';
                exit;
            }
        }

        echo 'ok';
    }

    /**
     * @param string $file
     * @param Company $company
     * @return array
     */
    private function importToDB(string $file, Company $company)
    {
        /** @var $employee Employee */
        $employee = $company->ownerEmployee;

        $ret = OneSImportHelper::importFile($file, $company, $employee);

        return $ret['result'] ?? ['result' => null];
    }

    /**
     * @return array
     */
    private function getMailboxes()
    {
        return OneSAutoloadEmail::find()
            ->where([
                'enabled' => 1,
                'in_work' => 0,
                'is_exit_by_error' => 0 // avoid double autoload errors
            ])
            ->limit(self::MAX_MAILBOXES_COUNT)
            ->orderBy(['uploaded_at' => SORT_ASC])
            ->all();
    }

    /**
     * @return int
     */
    private function getInWorkMailboxesCount()
    {
        return OneSAutoloadEmail::find()
            ->where([
                'enabled' => 1,
                'in_work' => 1
            ])->count();
    }

    /**
     * @param OneSAutoloadEmail $mailbox
     * @return bool
     */
    private function isInWork(OneSAutoloadEmail $mailbox)
    {
        return OneSAutoloadEmail::find()
            ->where(['id' => $mailbox->id])
            ->andWhere(['in_work' => 1])
            ->exists();
    }
}