<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 8.12.15
 * Time: 14.50
 * Email: t.kanstantsin@gmail.com
 */
namespace console\controllers;

use common\components\date\DateHelper;
use common\components\excel\Excel;
use common\components\sender\unisender\UniSender;
use common\models\Company;
use common\models\service\Payment;
use common\models\service\PromoCode;
use common\models\service\PromoCodeGroup;
use common\models\Contractor;
use common\models\service\StatisticHelper;
use common\models\service\Subscribe;
use common\models\service\StoreTariff;
use common\models\service\SubscribeHelper;
use common\models\service\SubscribeTariff;
use common\models\service\SubscribeTariffGroup;
use common\models\store\StoreCompanyContractor;
use console\components\service\RemindContractor;
use frontend\modules\analytics\jobs\credits\UpdateCreditJob;
use yii\console\Controller;

/**
 * Class Service
 * @package console\controllers
 */
class ServiceController extends Controller
{
    /**
     * @var string
     */
    public $layoutWrapperCssClass = '';

    /**
     * Updates statistic model
     */
    public function actionStatisticUpdate()
    {
        if (StatisticHelper::isSubscribeChanged()) {
            StatisticHelper::updateStatistics(StatisticHelper::getUpdatedPeriods());
            UpdateCreditJob::dispatchJob();
        }
    }

    /**
     * Remind Contractor for Invoice payment
     */
    public function actionRemindContractor()
    {
        return RemindContractor::process();
    }

    /**
     * search companies for free tariff
     */
    public function actionFreeTariff()
    {
        $companyArray = Company::searchForFreeTariff();
        $emailArray = [];
        foreach ($companyArray as $key => $company) {
            if (!$company->getHasActualSubscription()) {
                $company->applyFreeTariff();
                if ($chief = $company->getEmployeeChief()) {
                    $emailArray[] = [
                        'companyName' => $company->getTitle(),
                        'email' => $chief->email,
                    ];
                }
            }
        }

        if ($emailArray) {
            foreach ($emailArray as $data) {
                \Yii::$app->mailer->compose([
                    'html' => 'system/free-tariff/html',
                    'text' => 'system/free-tariff/text',
                ], [
                    'subject' => 'Тариф БЕСПЛАТНО',
                    'companyName' => $data['companyName'],
                ])
                    ->setFrom([\Yii::$app->params['emailList']['info'] => \Yii::$app->params['emailFromName']])
                    ->setTo($data['email'])
                    ->setSubject('Тариф БЕСПЛАТНО.')
                    ->send();
            }
        }

        return;
    }

    /**
     *
     */
    public function actionGetPaymentXls()
    {
        /* @var $payments Payment */
        $payments = Payment::find()
            ->joinWith('subscribe')
            ->andWhere(['in', Subscribe::tableName() . '.tariff_id', [SubscribeTariff::TARIFF_1, SubscribeTariff::TARIFF_2, SubscribeTariff::TARIFF_3]])
            ->andWhere(['is_confirmed' => 1])
            ->andWhere(['between', 'date(from_unixtime(`payment_date`))', '2016-08-01', '2017-07-31'])
            ->all();

        $excel = new Excel();
        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'models' => $payments,
            'columns' => [
                [
                    'attribute' => 'company.id',
                    'header' => 'ID компании',
                    'format' => 'raw',
                    'value' => function (Payment $model) {
                        return $model->subscribe->company_id;
                    },
                ],
                [
                    'attribute' => 'company.created_at',
                    'header' => 'Дата регистрации',
                    'format' => 'raw',
                    'value' => function (Payment $model) {
                        return date(DateHelper::FORMAT_USER_DATE, $model->subscribe->company->created_at);
                    },
                ],
                [
                    'attribute' => 'payment_date',
                    'header' => 'Дата оплаты',
                    'format' => 'raw',
                    'value' => function (Payment $model) {
                        return date(DateHelper::FORMAT_USER_DATE, $model->payment_date);
                    },
                ],
                [
                    'attribute' => 'sum',
                    'header' => 'Сумма покупки',
                    'format' => 'raw',
                    'value' => function (Payment $model) {
                        return $model->sum;
                    },
                ],
            ],
            'headers' => [
                'Дата регистрации',
                'Дата оплаты',
                'Сумма покупки'
            ],
            'format' => 'Xlsx',
            'fileName' => 'payments.xls',
            'savePath' => __DIR__,
        ]);
    }

    /**
     * Deactivate cabinets where payment expired
     */
    public function actionCheckCabinets()
    {
        $companies = Payment::find()
            ->select(Payment::tableName() . '.company_id')
            ->andWhere(['payment_for' => Payment::FOR_STORE_CABINET])
            ->andWhere(['is_confirmed' => true])
            ->groupBy('company_id')
            ->column();

        foreach ($companies as $companyID) {
            $company = Company::findOne($companyID);
            $availableCabinetsCount = $company->getAvailableCabinetsCount();
            if ($availableCabinetsCount < 0) {
                $company->deactivateCabinets(abs($availableCabinetsCount));
            }
        }
    }

    /**
     * Deactivate links where payment expired
     */
    public function actionCheckLinks()
    {
        $companies = Payment::find()
            ->select(Payment::tableName() . '.company_id')
            ->andWhere(['payment_for' => Payment::FOR_OUT_INVOICE])
            ->andWhere(['is_confirmed' => true])
            ->groupBy('company_id')
            ->column();

        foreach ($companies as $companyID) {
            $company = Company::findOne($companyID);
            $availableLinksCount = $company->getAvailableLinksCount();
            if ($availableLinksCount < 0) {
                $company->deactivateLinks(abs($availableLinksCount));
            }
        }
    }

    /**
     *
     */
    public function actionGeneratePromoCodes()
    {
        for ($i = 0; $i < 500; $i++) {
            $promoCode = new PromoCode();
            $promoCode->name = 'Mail.ru';
            $promoCode->duration_month = 2;
            $promoCode->duration_day = 0;
            $promoCode->tariff_group_id = SubscribeTariffGroup::STANDART;
            $promoCode->group_id = PromoCodeGroup::GROUP_INDIVIDUAL;
            $promoCode->started_at = date(DateHelper::FORMAT_DATE);
            $promoCode->expired_at = '2018-12-31';
            $promoCode->save();
        }
        $excel = new Excel();
        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => PromoCode::find()->andWhere(['name' => 'Mail.ru'])->all(),
            'title' => 'Промо-коды Mail.ru',
            'columns' => [
                [
                    'attribute' => 'id',
                    'value' => function (PromoCode $model) {
                        return $model->id;
                    },
                ],
                [
                    'attribute' => 'code',
                    'value' => function (PromoCode $model) {
                        return $model->code;
                    },
                ],
                [
                    'attribute' => 'name',
                    'value' => function (PromoCode $model) {
                        return $model->name;
                    },
                ],
            ],
            'headers' => [
                'id' => 'ID',
                'code' => 'Код',
                'name' => 'Акция',
            ],
            'format' => 'Xlsx',
            'fileName' => 'PromoCodes_MailRu.xls',
            'savePath' => __DIR__,
        ]);
    }
}
