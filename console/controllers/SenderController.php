<?php
/**
 * Created by PhpStorm.
 * User: crearsdev
 * Date: 25.02.16
 * Time: 13:02
 */
namespace console\controllers;

use backend\modules\company\forms\PromocodeSendForm;
use backend\modules\service\models\PromoCodeStatisticSearch;
use common\components\sender\unisender\UniSender;
use common\models\Company;
use common\models\company\CompanyLastVisit;
use common\models\company\CompanyType;
use common\models\document\status\InvoiceStatus;
use common\models\EmployeeCompany;
use common\models\product\Product;
use common\models\User;
use console\components\sender\Autovoronka;
use console\components\sender\DailyMailing;
use console\components\sender\Findirector;
use console\components\sender\FreeTariff;
use console\components\sender\InactiveCompany;
use console\components\sender\SubscriptionExpires;
use console\components\sender\TaxCalendar;
use console\components\sender\TaxrobotMailing;
use console\components\sender\WorkWithDebtors;
use console\components\sender\PriceList;
use console\models\SubscribeExpired;
use DateTime;
use Yii;
use yii\base\Exception;
use yii\db\Query;
use yii\console\Controller;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\components\sender\Sender;
use common\components\sender\rule\DaysAfterRegistrationRule;
use common\components\sender\rule\DaysAfterLastActivityRule;
use common\components\sender\rule\AllActiveRule;
use common\components\sender\rule\EveryMondayRule;

/**
 * Class Service
 * @package console\controllers
 */
class SenderController extends Controller
{
    /**
     *  max count of companies for statistic
     */
    public $statLimit = 5;

    /**
     *  Update UniSender contacts list
     * @throws Exception
     */
    public function actionSync()
    {
        set_time_limit(0);
        $this->syncContacts();

        echo "Import contacts complete\n";
    }

    /**
     *  Autovoronka email send
     * @throws Exception
     */
    public function actionAutovoronka($email = null, $case = null, $mail = null)
    {
        $autovoronka = new Autovoronka();
        $autovoronka->send($email, $case, $mail);
    }

    /**
     *  PriceList email send
     * @throws Exception
     */
    public function actionPricelist($email = null, $mail = null)
    {
        $autovoronka = new PriceList();
        $autovoronka->send($email, $mail);
    }

    /**
     *  Findirector mailing
     * @throws Exception
     */
    public function actionFindirector($email = null, $number = null)
    {
        if ($email === null) {
            $dailyMailing = new DailyMailing($this);
            $dailyParams = $dailyMailing->getDailyParams();
            $day = end($dailyParams)->day ?? 0;
            Findirector::start($day);
        }
        $sender = new Findirector;
        $sender->send($email, $number);
    }

    /**
     *  Daily email send
     * @throws Exception
     */
    public function actionDaily($email = null, $mailNum = null)
    {
        $dailyMailing = new DailyMailing($this);
        $dailyMailing->send($email, $mailNum);
    }

    /**
     *  Daily email send
     * @throws Exception
     */
    public function actionTaxrobot($email = null, $template = null)
    {
        $dailyMailing = new TaxrobotMailing($this);
        $dailyMailing->send($email, $template);
    }

    /**
     *  Weekly email send
     * @throws Exception
     */
    public function actionWeekly($email = null)
    {
        return; // Отключено

        $result = [];
        $contacts = [];
        $countedContacts = [];
        $query = new Query;
        $query
            ->select(['link.employee_id', 'link.company_id'])
            ->from(['link' => EmployeeCompany::tableName()])
            ->leftJoin(['employee' => Employee::tableName()], 'link.employee_id = employee.id')
            ->leftJoin(['company' => Company::tableName()], 'link.company_id = company.id')
            ->leftJoin([
                'visit' => CompanyLastVisit::tableName(),
            ], '{{visit}}.[[employee_id]]={{link}}.[[employee_id]] AND {{visit}}.[[company_id]]={{link}}.[[company_id]]')
            ->where([
                'employee.notify_new_features' => true,
                'employee.is_active' => Employee::ACTIVE,
                'employee.is_deleted' => Employee::NOT_DELETED,
                'link.employee_role_id' => EmployeeRole::ROLE_CHIEF,
                'link.is_working' => true,
                'company.blocked' => Company::UNBLOCKED,
                'company.strict_mode' => Company::OFF_STRICT_MODE,
            ])
            ->andWhere(['>', 'visit.time', UniSender::getInactiveTimeLimit()]);

        if ($email) {
            if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
                $result = $query->andWhere(['employee.email' => $email])->all();
            } else {
                echo "Email is not correct.\n";
                return;
            }
        } else {
            $result = $query->all();
        }

        if ($result) {
            foreach ($result as $row) {
                $contacts[$row['employee_id']][] = $row['company_id'];
            }
            unset($result);
        }

        if ($contacts) {
            foreach ($contacts as $key => $value) {
                $count = count($value);
                if ($count > $this->statLimit) {
                    $count = $this->statLimit;
                    $value = array_slice($value, 0, $this->statLimit);
                }
                $countedContacts[count($value)][$key] = $value;
            }
            unset($contacts);
        }

        if ($countedContacts) {
            foreach ($countedContacts as $count => $contacts) {
                $uniSender = new UniSender();
                $uniSender->setEmailTemplateName('every-monday-multiple')
                    ->setFromEmail(\Yii::$app->params['emailList']['info'])
                    ->setFromName(\Yii::$app->params['emailFromName'])
                    ->setParams([
                        'supportEmail' => \Yii::$app->params['emailList']['support'],
                        'subject' => 'Итоги вашего бизнеса за прошедшую неделю',
                        'count' => $count,
                    ])
                    ->setSubject('Итоги вашего бизнеса за прошедшую неделю')
                    ->importStatisticData($contacts)
                    ->send(false);
            }
        }
    }

    /**
     *  Monthly email send
     * @throws Exception
     */
    public function actionTaxCalendar($email = null)
    {
        $taxCalendar = new TaxCalendar($this);
        $taxCalendar->send($email);
    }

    /**
     *  Monthly email send
     * @throws Exception
     */
    public function actionExpires($email = null, $letterNumder = null)
    {
        $subscriptionExpires = new SubscriptionExpires($this);
        $subscriptionExpires->send($email, $letterNumder);
        $this->actionSubscribeExpired();
    }

    /**
     * @return void
     */
    public function actionSubscribeExpired(): void
    {
        (new SubscribeExpired)->sendNotifications();
    }

    /**
     *  Monthly email send
     * @throws Exception
     */
    public function actionFreeTariff($email = null)
    {
        $FreeTariff = new FreeTariff;
        $FreeTariff->send($email);
    }

    /**
     *  Monthly email send
     * @throws Exception
     */
    public function actionInactiveCompany($email = null, $number = null)
    {
        $FreeTariff = new InactiveCompany;
        $FreeTariff->send($email, $number);
    }

    /**
     *  Monthly email send
     * @throws Exception
     */
    public function actionDebtors($email = null, $number = null)
    {
        $sender = new WorkWithDebtors;
        $sender->send($email, $number);
    }

    /**
     *  Weekly email send
     * @throws Exception
     */
    public function actionRevise($email = null)
    {
        $date = new DateTime('-1 month');
        $month = mb_strtolower(Yii::$app->formatter->asDate($date, 'LLLL'));
        $subject = "Сверка по документам за {$month}!";
        $date->modify('first day of this month');
        $from = $date->format('Y-m-d');
        $till = $date->modify('last day of this month')->format('Y-m-d');

        $query = new Query;
        $companyIdArray = $query
            ->select(['invoice.company_id'])
            ->from('invoice')
            ->leftJoin('company', '{{company}}.[[id]] = {{invoice}}.[[company_id]]')
            ->leftJoin('company_taxation_type', '{{company_taxation_type}}.[[company_id]] = {{company}}.[[id]]')
            ->leftJoin('act', '{{act}}.[[invoice_id]] = {{invoice}}.[[id]]')
            ->leftJoin('packing_list', '{{packing_list}}.[[invoice_id]] = {{invoice}}.[[id]]')
            ->leftJoin('invoice_facture', '{{invoice_facture}}.[[invoice_id]] = {{invoice}}.[[id]]')
            ->leftJoin('upd', '{{upd}}.[[invoice_id]] = {{invoice}}.[[id]]')
            ->where([
                'invoice.is_deleted' => false,
                'upd.id' => null,
            ])
            ->andWhere(['not', ['invoice.invoice_status_id' => InvoiceStatus::STATUS_REJECTED]])
            ->andWhere(['not', ['invoice.invoice_status_id' => InvoiceStatus::STATUS_AUTOINVOICE]])
            ->andWhere(['between', 'invoice.document_date', $from, $till])
            ->andWhere([
                'or',
                [
                    'and',
                    ['act.id' => null],
                    ['like', 'invoice.production_type', (string) Product::PRODUCTION_TYPE_SERVICE],
                ],
                [
                    'and',
                    ['packing_list.id' => null],
                    ['like', 'invoice.production_type', (string) Product::PRODUCTION_TYPE_GOODS],
                ],
                [
                    'and',
                    ['company_taxation_type.osno' => 1],
                    ['invoice_facture.id' => null],
                ],
            ])
            ->andWhere(['company.blocked' => Company::UNBLOCKED])
            ->groupBy(['invoice.company_id'])
            ->column();

        if ($companyIdArray) {
            $denyList = \common\models\employee\EmailDeny::list();
            foreach ($companyIdArray as $id) {
                $company = Company::findOne($id);
                $company->period = ['from' => $from, 'to' => $till];
                $contacts = Employee::find()
                    ->joinWith('employeeCompany')
                    ->where([
                        'employee.notify_new_features' => true,
                        'employee_company.company_id' => $company->id,
                        'employee_company.employee_role_id' => EmployeeRole::ROLE_CHIEF,
                        'employee_company.is_working' => true,
                    ])
                    ->all();

                if ($contacts) {
                    foreach ($contacts as $employee) {
                        if ($employee->email) {
                            if (isset($denyList[strtolower($employee->email)])) {
                                continue;
                            }
                            Yii::$app->mailer->compose([
                                'html' => 'system/documents/not-created/html',
                                'text' => 'system/documents/not-created/text',
                            ], [
                                'subject' => $subject,
                                'employee' => $employee,
                                'company' => $company,
                                'month' => $month,
                            ])
                                ->setFrom([\Yii::$app->params['emailList']['docs'] => \Yii::$app->params['emailFromName']])
                                ->setTo($employee->email)
                                ->setSubject($subject)
                                ->send();
                        }
                    }
                }
            }
        }
    }

    /**
     * Clear "html_content" field
     *
     * @return $this
     */
    public function actionClearHtml()
    {
        Yii::$app->db->createCommand('SET session wait_timeout=28800')->execute();
        Yii::$app->db->createCommand('SET session interactive_timeout=28800')->execute();

        $uniSender = new UniSender();
        $file = tmpfile();
        echo 'begin clear "html_content" field'. PHP_EOL;
        if ($file) {
            $offset = 0;
            $continue = true;
            do {
                $response = $uniSender->exportContacts([
                    'field_names' => [
                        'email',
                    ],
                    'list_id' => Yii::$app->params['uniSender']['listId'],
                    'offset' => $offset++ * UniSender::EXPORT_ROWS_LIMIT,
                    'limit' => UniSender::EXPORT_ROWS_LIMIT,
                ]);
                $result = json_decode($response, true);
                if (!empty($result['result']['data'])) {
                    foreach ($result['result']['data'] as $item) {
                        fwrite($file, $item[0] . PHP_EOL);
                    }
                } else {
                    $continue = false;
                }
                echo 'export: ' . $offset . PHP_EOL;
            } while ($continue);
            fseek($file, 0);
            $i = 0;
            $count = 0;
            $contacts = [];
            while (($line = fgets($file)) !== false) {
                $contacts[] = trim($line);
                $count++;
                if ($count == UniSender::IMPORT_ROWS_LIMIT) {
                    $uniSender->clearHtmlContent($contacts, Yii::$app->params['uniSender']['listId']);
                    echo 'import: ' . ++$i . PHP_EOL;
                    $count = 0;
                    $contacts = [];
                }
            }
            if ($contacts) {
                $uniSender->clearHtmlContent($contacts, Yii::$app->params['uniSender']['listId']);
                echo 'import: ' . ++$i . PHP_EOL;
            }
            fclose($file);
        }
        echo 'completed'. PHP_EOL;
    }

    /**
     * Mark unsubscribed users
     *
     * @return $this
     */
    public function actionUnsubscribed()
    {
        echo 'begin Mark unsubscribed users'. PHP_EOL;

        $uniSender = new UniSender();
        $requestData = [
            'notify_url' => Yii::$app->urlManager->createAbsoluteUrl(['/webhook/unisender-unsubscribed']),
            'email_status' => 'unsubscribed',
            'field_names' => [
                'email',
                'email_status',
            ]
        ];

        do {
            $response = Json::decode($uniSender->exportContacts($requestData));
            $resultCount = 0;
            if (isset($response['result']['data']) && is_array($response['result']['data'])) {
                $resultCount = count($response['result']['data']);
                $emailArray = [];
                foreach ($response['result']['data'] as $data) {
                    $email = ArrayHelper::getValue($data, 0);
                    $status = ArrayHelper::getValue($data, 1);
                    if ($email && $status == 'unsubscribed') {
                        $emailArray[] = $email;
                    }
                }
                if ($emailArray) {
                    Yii::$app->db->createCommand()->update('{{%employee}}', [
                        'is_mailing_active' => false,
                    ], [
                        'email' => $emailArray,
                    ])->execute();
                    $offset = 0;
                    do {
                        $emails = array_slice($emailArray, $offset, UniSender::IMPORT_ROWS_LIMIT);
                        $offset += UniSender::IMPORT_ROWS_LIMIT;
                        if (count($emails) > 0) {
                            $deleteData = [
                                'field_names' => [
                                    'email',
                                    'delete',
                                ],
                                'data' => [],
                            ];
                            foreach ($emails as $email) {
                                $deleteData['data'][] = [
                                    $email,
                                    1,
                                ];
                            }
                            $r = $uniSender->importContacts($deleteData);
                            file_put_contents(Yii::getAlias('@runtime/logs/unsubscribed.log'), var_export($r, true)."\n\n", FILE_APPEND);
                        }
                    } while (count($emails) == UniSender::IMPORT_ROWS_LIMIT);

                }
            }
        } while ($resultCount > 0);


        echo 'completed'. PHP_EOL;
    }

    /**
     * New design info
     * @throws Exception
     */
    public function actionNewDesign($email)
    {
        return; // Разовая рассылка. Уже выполнена.

        $subject = 'Новый дизайн - мы это сделали!';
        $uniSender = new UniSender();

        $uniSender->setEmailTemplateName('new_design')
            ->setFromEmail(\Yii::$app->params['emailList']['info'])
            ->setFromName('Алексей Кущенко')
            ->setParams([
                'supportEmail' => \Yii::$app->params['emailList']['support'],
                'subject' => $subject,
            ])
            ->setSubject($subject)
            ->setContacts([$email])
            ->send(false, $email == 'all');
    }

    /**
     * Contractor dosier info
     * @throws Exception
     */
    public function actionDossier($email)
    {
        return; // Разовая рассылка. Уже выполнена.

        $subject = 'Заплатит или не заплатит?';
        $uniSender = new UniSender();

        $uniSender->setEmailTemplateName('dossier')
            ->setFromEmail(\Yii::$app->params['emailList']['info'])
            ->setFromName('Арслан Хакимов')
            ->setParams([
                'supportEmail' => \Yii::$app->params['emailList']['support'],
                'subject' => $subject,
            ])
            ->setSubject($subject)
            ->setContacts([$email])
            ->send(false, $email == 'all');
    }

    /**
     *  Update UniSender contacts list
     * @throws Exception
     */
    public function syncContacts()
    {
        if (!empty(Yii::$app->params['uniSender']['listId'])) {
            $uniSender = new UniSender();
            $uniSender->syncContacts(Yii::$app->params['uniSender']['listId']);
        }
    }

    /**
     * @param \yii\base\Action $action
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            set_time_limit(6000);
            Yii::$app->urlManager->baseUrl = '/';
            Yii::setAlias('@webroot', '@frontend/web');
            Yii::setAlias('@web', '@frontend/web');
            Yii::$app->urlManager->hostInfo = Yii::$app->params['serviceSiteLk'];
            Yii::$app->db->createCommand('SET session wait_timeout=28800')->execute();
            Yii::$app->db->createCommand('SET session interactive_timeout=28800')->execute();

            return true;
        }

        return false;
    }
}
