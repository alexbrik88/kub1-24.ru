<?php
namespace console\controllers;

use common\components\date\DateHelper;
use common\models\currency\Currency;
use common\models\currency\CurrencyRate;
use Yii;
use yii\console\Controller;
use yii\web\NotFoundHttpException;

class CurrencyExchangeController extends Controller
{
    public function actionIndex()
    {
        $currentDate = date_create('today');
        $currentDateString = $currentDate->format('Y-m-d');
        $result = CurrencyRate::rateRequest($currentDate);

        if (isset($result['Valute']) && is_array($result['Valute'])) {
            foreach ($result['Valute'] as $data) {
                if (in_array($data['CharCode'], Currency::$nameArray)) {
                    $model = Currency::findOne($data['NumCode']);

                    if ($model === null) {
                        $model = new Currency([
                            'id' => $data['NumCode'],
                            'name' => $data['CharCode'],
                            'label' => $data['Name'],
                        ]);
                    }

                    if ($model->update_date == $currentDateString) {
                        continue;
                    }

                    $value = str_replace(',', '.', $data['Value']);

                    $model->amount = $data['Nominal'];
                    $model->old_value = $model->current_value ? : $value;
                    $model->update_date = $currentDateString;
                    $model->current_value = $value;
                    $model->save();
                }
            }
        }

        CurrencyRate::importRate($result, $currentDate);
    }

    /**
     * Заполнение таблицы курсов валют
     * @param  string $dateFrom  начальная дата
     */
    public function actionInsert(string $dateFrom = '2018-01-01')
    {
        $date = date_create($dateFrom);
        $today = date_create('today');
        $dateExists = CurrencyRate::find()->select('date')->distinct()->where(['>=', 'date', '2018-01-01'])->column();
        $dateExists = array_combine($dateExists, $dateExists);
        while ($date < $today) {
            $dateString = $date->format('Y-m-d');
            if (!isset($dateExists[$dateString])) {
                echo $dateString.PHP_EOL;
                $result = CurrencyRate::rateRequest($date);
                CurrencyRate::importRate($result, $date);
            }
            $date->modify('+1 days');
        }
    }

    /**
     * Установить эквивалент суммы в рублях у валютных операций
     * Если есть не заполненные курсы валют, предварительно выполнить ./yii currency-exchange/insert
     */
    public function actionUpdateFlows()
    {
        $tableArray = [
            '{{%cash_bank_foreign_currency_flows}}',
            '{{%cash_emoney_foreign_currency_flows}}',
            '{{%cash_order_foreign_currency_flows}}',
            '{{%card_operation}}',
        ];

        $sqlTpl = <<<SQL
            UPDATE {{%t}}
            LEFT JOIN {{%currency_rate}} ON {{%currency_rate}}.[[date]] = {{%t}}.[[date]] AND {{%currency_rate}}.[[name]] = {{%t}}.[[currency_name]]
            SET {{%t}}.[[amount_rub]] = IF(
                {{%currency_rate}}.[[name]] IS NULL,
                0,
                ROUND({{%t}}.[[amount]]/{{%currency_rate}}.[[amount]]*{{%currency_rate}}.[[value]])
            )
            WHERE {{%t}}.[[currency_name]] <> "RUB"
SQL;

        foreach ($tableArray as $table) {
            $sql = strtr($sqlTpl, ['{{%t}}' => $table]);
            $command = Yii::$app->db->createCommand($sql);
            echo $command->rawSql.PHP_EOL;
            $command->execute();
        }
    }
}
