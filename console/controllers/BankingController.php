<?php

namespace console\controllers;

use backend\models\Bank;
use console\components\banking\StatementAutoload;
use console\components\banking\TokenRefresh;
use yii\console\Controller;

/**
 * @package console\controllers
 */
class BankingController extends Controller
{
    public $layoutWrapperCssClass = '';

    /**
     * Updates statistic model
     */
    public function actionAutoload($company_id = null)
    {
        $autoloader = new StatementAutoload;
        $autoloader->process($company_id);
    }

    /**
     * Updates statistic model
     */
    public function actionTokenRefresh($company_id = null)
    {
        $autoloader = new TokenRefresh;
        $autoloader->process($company_id);
    }

    /**
     * @throws \yii\base\Exception
     */
    public function actionRemoveLogoBackground()
    {
        $newFileName = DIRECTORY_SEPARATOR . Bank::LOGO_IMAGE . '.png';
        /* @var $banks Bank[] */
        $banks = Bank::find()->all();
        foreach ($banks as $bank) {
            $uploadPath = $bank->getUploadDirectory() . $bank->logo_link;
            if (is_file($uploadPath)) {
                $pathInfo = pathinfo($uploadPath);
                $newPath = $bank->getUploadDirectory() . $newFileName;

                exec("convert \"{$uploadPath}\" -fill none -fuzz 20% -draw \"matte 0,0 floodfill\" -flop  -draw \"matte 0,0 floodfill\" -flop \"{$newPath}\"");
                exec("convert \"{$newPath}\" -fuzz 4% -transparent white \"{$newPath}\"");

                if ($bank->logo_link != $newFileName) {
                    $bank->updateAttributes(['logo_link' => $newFileName]);
                }

                if ($uploadPath != $newPath && is_file($uploadPath)) {
                    unlink($uploadPath);
                }
            } else {
                $bank->updateAttributes(['logo_link' => null]);
            }
        }
    }
}
