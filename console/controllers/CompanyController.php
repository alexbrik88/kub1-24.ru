<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 12.12.2016
 * Time: 3:11
 */

namespace console\controllers;


use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\address\Country;
use common\models\Agreement;
use common\models\cash\CashBankStatementUpload;
use common\models\Company;
use common\models\company\Activities;
use common\models\company\CheckingAccountant;
use common\models\company\CompanyTaxationType;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\dictionary\bik\BikDictionary;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceFacture;
use common\models\document\InvoiceIncomeItem;
use common\models\document\Order;
use common\models\document\OrderHelper;
use common\models\document\PackingList;
use common\models\document\status\InvoiceStatus;
use common\models\document\Upd;
use common\models\employee\Employee;
use common\models\EmployeeCompany;
use common\models\ExpenseItemFlowOfFunds;
use common\models\IncomeItemFlowOfFunds;
use common\models\product\Product;
use common\models\product\ProductUnit;
use common\models\report\ReportState;
use common\models\service\Payment;
use common\models\service\PaymentType;
use common\models\service\Subscribe;
use common\models\service\SubscribeTariff;
use common\components\excel\Excel;
use common\models\ServiceMoreStock;
use common\models\TaxRate;
use frontend\models\Documents;
use frontend\modules\export\models\export\Export;
use frontend\modules\reports\models\FlowOfFundsReportSearch;
use yii\console\Controller;
use Yii;
use Imagine\Imagick\Imagine as Imagick;
use yii\helpers\Console;

/**
 * Class CompanyController
 * @package console\controllers
 */
class CompanyController extends Controller
{
    /**
     * Удаляем компании, которые были созданы с лендинга, для предварительного просмотра счета
     */
    public function actionDeleteCompaniesFromLanding()
    {
        $existCompany = Employee::find()->select(['company_id'])->groupBy('company_id')->column();
        $companiesFromLanding = Company::find()->andWhere(['and',
            ['not in', 'id', $existCompany],
            ['test' => Company::TEST_COMPANY],
        ])->all();
        /* @var $company Company */
        foreach ($companiesFromLanding as $company) {
            $diff = time() - $company->created_at;
            if ((($diff - (int)($diff / 86400) * 86400) / 86400) >= 1) {
                $company->deleteCompanyFromLanding();
            }
        }
    }

    /**
     *
     */
    public function actionUpdateActivationType()
    {
        $companies = Company::find()->isTest(!Company::TEST_COMPANY)->isBlocked(Company::UNBLOCKED)->all();
        /* @var $company Company */
        foreach ($companies as $company) {
            if ($company->strict_mode) {
                $company->activation_type = Company::ACTIVATION_EMPTY_PROFILE;
            } elseif ($company->getOutInvoiceCount() == 0) {
                $company->activation_type = Company::ACTIVATION_NO_INVOICE;
            } elseif ($company->first_send_invoice_date == null) {
                $company->activation_type = Company::ACTIVATION_NOT_SEND_INVOICES;
            } else {
                $company->activation_type = Company::ACTIVATION_ALL_COMPLETE;
            }
            $company->save(true, ['activation_type']);
        }
    }

    /**
     *
     */
    public function actionExportCompaniesByProducts()
    {
        $companies = Company::find()
            ->select([
                Company::tableName() . '.*',
                'COUNT(' . Product::tableName() . '.id) as productCount',
            ])
            ->isTest(!Company::TEST_COMPANY)
            ->isBlocked(Company::UNBLOCKED)
            ->joinWith('products')
            ->andWhere(['is_deleted' => false])
            ->andWhere(['production_type' => Product::PRODUCTION_TYPE_GOODS])
            ->andHaving(['>', 'productCount', 50])
            ->groupBy(Company::tableName() . '.id')
            ->all();

        $excel = new Excel();
        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $companies,
            'rangeHeader' => array_merge(range('A', 'N')),
            'title' => 'Компании c товарами > 50',
            'columns' => [
                [
                    'attribute' => 'id',
                    'value' => function (Company $model) {
                        return $model->id;
                    },
                ],
                [
                    'attribute' => 'name',
                    'value' => function (Company $model) {
                        return $model->getShortName();
                    },
                ],
                [
                    'attribute' => 'chief_firstname',
                    'value' => function (Company $model) {
                        return $model->getChiefFio(true);
                    },
                ],
                [
                    'attribute' => 'phone',
                    'value' => function (Company $model) {
                        return $model->phone;
                    },
                ],
                [
                    'attribute' => 'email',
                    'value' => function (Company $model) {
                        return $model->email;
                    },
                ],
                [
                    'attribute' => 'created_at',
                    'value' => function (Company $model) {
                        return date(DateHelper::FORMAT_USER_DATE, $model->created_at);
                    },
                ],
                [
                    'attribute' => 'last_visit_at',
                    'value' => function (Company $model) {
                        return date(DateHelper::FORMAT_USER_DATE, $model->last_visit_at);
                    },
                ],
                [
                    'attribute' => 'last_visit_at',
                    'value' => function (Company $model) {
                        return date(DateHelper::FORMAT_USER_DATE, $model->last_visit_at);
                    },
                ],
                [
                    'attribute' => 'active_subscribe_id',
                    'value' => function (Company $model) {
                        return $model->activeSubscribe ? $model->activeSubscribe->getTariffName(true) : '-';
                    },
                ],
                [
                    'attribute' => 'products',
                    'value' => function (Company $model) {
                        return $model->getProducts()
                            ->andWhere([Product::tableName() . '.is_deleted' => false])
                            ->andWhere(['production_type' => Product::PRODUCTION_TYPE_GOODS])
                            ->count();
                    },
                ],
                [
                    'attribute' => 'invoices',
                    'value' => function (Company $model) {
                        return $model->getInvoices()
                            ->andWhere([Invoice::tableName() . '.is_deleted' => false])
                            ->andWhere(['type' => Documents::IO_TYPE_OUT])
                            ->count();
                    },
                ],
                [
                    'attribute' => 'contractors',
                    'value' => function (Company $model) {
                        return $model->getContractors()
                            ->andWhere([Contractor::tableName() . '.is_deleted' => false])
                            ->andWhere(['type' => Contractor::TYPE_CUSTOMER])
                            ->count();
                    },
                ],
                [
                    'attribute' => 'productsName',
                    'value' => function (Company $model) {
                        $productsName = $model->getProducts()
                            ->select(Product::tableName() . '.title')
                            ->andWhere([Product::tableName() . '.is_deleted' => false])
                            ->andWhere(['production_type' => Product::PRODUCTION_TYPE_GOODS])
                            ->limit(10)
                            ->groupBy(Product::tableName() . '.id')
                            ->column();

                        return implode(', ', $productsName);
                    },
                ],
            ],
            'headers' => [
                'id' => 'ID',
                'name' => 'Название компании',
                'chief_firstname' => 'ФИО',
                'phone' => 'Телефон',
                'email' => 'E-mail',
                'created_at' => 'Дата регистрации',
                'last_visit_at' => 'Дата последнего входа',
                'active_subscribe_id' => 'Текущий тариф',
                'products' => 'Кол-во товара',
                'invoices' => 'Кол-во исходящих счетов',
                'contractors' => 'Кол-во покупателей',
                'productsName' => 'Название товара',
            ],
            'format' => 'Xlsx',
            'fileName' => 'Companii s tovarami 50.xls',
            'savePath' => __DIR__,
        ]);

    }

    /**
     *
     */
    public function actionGetCompaniesByPromoCode()
    {
        $companies = Company::find()->andWhere(['in', 'id', Payment::find()
            ->select(['service_subscribe.company_id'])
            ->joinWith('subscribe')
            ->andWhere(['promo_code_id' => 38])->column()])->all();

        $excel = new Excel();
        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'models' => $companies,
            'columns' => [
                [
                    'attribute' => 'email',
                    'header' => 'Email',
                    'format' => 'raw',
                    'value' => function (Company $model) {
                        return $model->email;
                    },
                ],
                [
                    'attribute' => 'short_name',
                    'header' => 'Название',
                    'format' => 'raw',
                    'value' => function (Company $model) {
                        return $model->getShortName();
                    },
                ],
                [
                    'attribute' => 'chief_firstname',
                    'header' => 'ФИО',
                    'format' => 'raw',
                    'value' => function (Company $model) {
                        return $model->getChiefFio();
                    },
                ],
            ],
            'headers' => [
                'Email',
                'Название',
                'ФИО'
            ],
            'format' => 'Xlsx',
            'fileName' => 'companies.xls',
            'savePath' => __DIR__,
        ]);

    }

    /**
     *
     */
    public function actionExportFirstPayedCompaniesXls()
    {
        $reportStates = ReportState::find()
            ->joinWith('company')
            ->andWhere([Company::tableName() . '.test' => false])
            ->andWhere(['between', 'date(from_unixtime(' . ReportState::tableName() . '.pay_date))', '2017-06-01', '2017-09-30'])
            ->all();

        $excel = new Excel();
        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $reportStates,
            'title' => 'Первые оплаты',
            'columns' => [
                [
                    'attribute' => 'company.email',
                    'value' => function (ReportState $model) {
                        return $model->company->email;
                    },
                ],
                [
                    'attribute' => 'pay_date',
                    'value' => function (ReportState $model) {
                        return date(DateHelper::FORMAT_USER_DATE, $model->pay_date);
                    },
                ],
            ],
            'headers' => [
                'company.email' => 'E-mail',
                'pay_date' => 'Дата первой оплаты',
            ],
            'format' => 'Xlsx',
            'fileName' => 'Pervie oplaty 01.06.2017 - 31.09.2017.xls',
            'savePath' => __DIR__,
        ]);
    }

    /**
     *
     */
    public function actionExportCompaniesWithFourEmployees()
    {
        $companies = Company::find()
            ->select([
                Company::tableName() . '.*',
                'COUNT(`' . EmployeeCompany::tableName() . '`.`employee_id`) as employeesCount',
            ])
            ->isBlocked(Company::UNBLOCKED)
            ->isTest(!Company::TEST_COMPANY)
            ->joinWith('employeeCompanies')
            ->andWhere(['is_working' => true])
            ->andHaving(['>', 'employeesCount', 3])
            ->groupBy(Company::tableName() . '.id')
            ->all();
        $excel = new Excel();

        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $companies,
            'title' => 'Компании где сотрудников >= 4',
            'columns' => [
                [
                    'attribute' => 'id',
                    'value' => function (Company $model) {
                        return $model->id;
                    },
                ],
                [
                    'attribute' => 'nameShort',
                    'value' => function (Company $model) {
                        return $model->getTitle(true);
                    },
                ],
                [
                    'attribute' => 'employeeCount',
                    'value' => function (Company $model) {
                        return $model->getEmployeeCompanies()
                            ->andWhere(['is_working' => true])
                            ->count();
                    },
                ],
                [
                    'attribute' => 'roles',
                    'value' => function (Company $model) {
                        $result = [];
                        /* @var $employeeCompany EmployeeCompany */
                        foreach ($model->getEmployeeCompanies()->andWhere(['is_working' => true])->all() as $employeeCompany) {
                            $result[$employeeCompany->employeeRole->id] = $employeeCompany->employeeRole->name;
                        }

                        return implode(', ', $result);
                    },
                ],
                [
                    'attribute' => 'positions',
                    'value' => function (Company $model) {
                        $result = [];
                        /* @var $employeeCompany EmployeeCompany */
                        foreach ($model->getEmployeeCompanies()->andWhere(['is_working' => true])->all() as $employeeCompany) {
                            if ($employeeCompany->position) {
                                $result[$employeeCompany->position] = $employeeCompany->position;
                            }
                        }

                        return implode(', ', $result);
                    },
                ],
                [
                    'attribute' => 'created_at',
                    'value' => function (Company $model) {
                        return date(DateHelper::FORMAT_USER_DATE, $model->created_at);
                    },
                ],
                [
                    'attribute' => 'last_visit_at',
                    'value' => function (Company $model) {
                        return date(DateHelper::FORMAT_USER_DATE, $model->last_visit_at);
                    },
                ],
                [
                    'attribute' => 'chiefFio',
                    'value' => function (Company $model) {
                        return $model->getChiefFio(true);
                    },
                ],
                [
                    'attribute' => 'email',
                    'value' => function (Company $model) {
                        return $model->email;
                    },
                ],
                [
                    'attribute' => 'phone',
                    'value' => function (Company $model) {
                        return $model->email;
                    },
                ],
            ],
            'headers' => [
                'id' => 'ID',
                'nameShort' => 'Название компании',
                'employeeCount' => 'Количество сотрудников',
                'roles' => 'Роли сотрудников',
                'positions' => 'Должности сотрудников',
                'created_at' => 'Дата регистрации компании',
                'last_visit_at' => 'Дата последнего входа в компанию',
                'chiefFio' => 'ФИО директора',
                'email' => 'Email',
                'phone' => 'Телефон',
            ],
            'format' => 'Xlsx',
            'fileName' => 'Companies.xls',
            'savePath' => __DIR__,
        ]);
    }

    /**
     *
     */
    public function actionExportBilledInvoicesCompanyToPhysicalPerson()
    {
        $companies = Company::find()
            ->isBlocked(Company::UNBLOCKED)
            ->isTest(!Company::TEST_COMPANY)
            ->joinWith('invoices')
            ->andWhere(['and',
                [Invoice::tableName() . '.is_deleted' => false],
                [Invoice::tableName() . '.type' => Documents::IO_TYPE_OUT]
            ])
            ->leftJoin(Contractor::tableName(), Contractor::tableName() . '.id = ' . Invoice::tableName() . '.contractor_id')
            ->andWhere([Contractor::tableName() . '.face_type' => Contractor::TYPE_PHYSICAL_PERSON])
            ->groupBy(Invoice::tableName() . '.company_id')
            ->all();

        $excel = new Excel();
        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $companies,
            'title' => 'Выставили счета физ.лицам',
            'columns' => [
                [
                    'attribute' => 'name',
                    'value' => function (Company $model) {
                        return $model->getShortName();
                    },
                ],
                [
                    'attribute' => 'invoice_count',
                    'value' => function (Company $model) {
                        return $model->getInvoices()
                            ->andWhere(['and',
                                [Invoice::tableName() . '.is_deleted' => false],
                                [Invoice::tableName() . '.type' => Documents::IO_TYPE_OUT]
                            ])
                            ->leftJoin(Contractor::tableName(), Contractor::tableName() . '.id = ' . Invoice::tableName() . '.contractor_id')
                            ->andWhere([Contractor::tableName() . '.face_type' => Contractor::TYPE_PHYSICAL_PERSON])
                            ->count();
                    },
                ],
                [
                    'attribute' => 'activities_id',
                    'value' => function (Company $model) {
                        return $model->activities ? $model->activities->name : null;
                    },
                ],
                [
                    'attribute' => 'invoice_sum',
                    'value' => function (Company $model) {
                        return TextHelper::invoiceMoneyFormat($model->getInvoices()
                            ->andWhere(['and',
                                [Invoice::tableName() . '.is_deleted' => false],
                                [Invoice::tableName() . '.type' => Documents::IO_TYPE_OUT]
                            ])
                            ->leftJoin(Contractor::tableName(), Contractor::tableName() . '.id = ' . Invoice::tableName() . '.contractor_id')
                            ->andWhere([Contractor::tableName() . '.face_type' => Contractor::TYPE_PHYSICAL_PERSON])
                            ->sum(Invoice::tableName() . '.total_amount_with_nds'), 2);
                    },
                ],
            ],
            'headers' => [
                'name' => 'Название компании',
                'invoice_count' => 'Количество счетов физлицам',
                'activities_id' => 'Вид деятельности компании',
                'invoice_sum' => 'Сумма счетов',
            ],
            'format' => 'Xlsx',
            'fileName' => 'Vistavily scheta fiz licam.xls',
            'savePath' => __DIR__,
        ]);
    }

    /**
     *
     */
    public function actionExportPayedCompaniesByMonth()
    {
        $companies = Company::find()
            ->isBlocked(Company::UNBLOCKED)
            ->isTest(!Company::TEST_COMPANY)
            ->joinWith('activeSubscribe')
            ->andWhere(['in', Subscribe::tableName() . '.tariff_id', SubscribeTariff::paidStandartIds()])
            ->all();

        $excel = new Excel();
        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $companies,
            'title' => 'Компании на платном тарифе',
            'rangeHeader' => array_merge(range('A', 'M')),
            'columns' => [
                [
                    'attribute' => 'name',
                    'value' => function (Company $model) {
                        return $model->getShortName();
                    },
                ],
                [
                    'attribute' => 'january',
                    'value' => function (Company $model) {
                        $invoiceCount = Invoice::find()
                            ->byDeleted()
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byCompany($model->id)
                            ->byDocumentDateRange('2017-01-01', '2017-01-31')
                            ->count();
                        $actCount = Act::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-01-01', '2017-01-31')
                            ->count();
                        $packingListCount = PackingList::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-01-01', '2017-01-31')
                            ->count();
                        $invoiceFactureCount = InvoiceFacture::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-01-01', '2017-01-31')
                            ->count();
                        $updCount = Upd::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-01-01', '2017-01-31')
                            ->count();

                        return "Счета: $invoiceCount | Акты: $actCount | ТН: $packingListCount | СФ: $invoiceFactureCount | УПД: $updCount";
                    },
                ],
                [
                    'attribute' => 'february',
                    'value' => function (Company $model) {
                        $invoiceCount = Invoice::find()
                            ->byDeleted()
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byCompany($model->id)
                            ->byDocumentDateRange('2017-02-01', '2017-02-28')
                            ->count();
                        $actCount = Act::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-02-01', '2017-02-28')
                            ->count();
                        $packingListCount = PackingList::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-02-01', '2017-02-28')
                            ->count();
                        $invoiceFactureCount = InvoiceFacture::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-02-01', '2017-02-28')
                            ->count();
                        $updCount = Upd::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-02-01', '2017-02-28')
                            ->count();

                        return "Счета: $invoiceCount | Акты: $actCount | ТН: $packingListCount | СФ: $invoiceFactureCount | УПД: $updCount";
                    },
                ],
                [
                    'attribute' => 'march',
                    'value' => function (Company $model) {
                        $invoiceCount = Invoice::find()
                            ->byDeleted()
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byCompany($model->id)
                            ->byDocumentDateRange('2017-03-01', '2017-03-31')
                            ->count();
                        $actCount = Act::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-03-01', '2017-03-31')
                            ->count();
                        $packingListCount = PackingList::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-03-01', '2017-03-31')
                            ->count();
                        $invoiceFactureCount = InvoiceFacture::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-03-01', '2017-03-31')
                            ->count();
                        $updCount = Upd::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-03-01', '2017-03-31')
                            ->count();

                        return "Счета: $invoiceCount | Акты: $actCount | ТН: $packingListCount | СФ: $invoiceFactureCount | УПД: $updCount";
                    },
                ],
                [
                    'attribute' => 'april',
                    'value' => function (Company $model) {
                        $invoiceCount = Invoice::find()
                            ->byDeleted()
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byCompany($model->id)
                            ->byDocumentDateRange('2017-04-01', '2017-04-30')
                            ->count();
                        $actCount = Act::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-04-01', '2017-04-30')
                            ->count();
                        $packingListCount = PackingList::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-04-01', '2017-04-30')
                            ->count();
                        $invoiceFactureCount = InvoiceFacture::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-04-01', '2017-04-30')
                            ->count();
                        $updCount = Upd::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-04-01', '2017-04-30')
                            ->count();

                        return "Счета: $invoiceCount | Акты: $actCount | ТН: $packingListCount | СФ: $invoiceFactureCount | УПД: $updCount";
                    },
                ],
                [
                    'attribute' => 'may',
                    'value' => function (Company $model) {
                        $invoiceCount = Invoice::find()
                            ->byDeleted()
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byCompany($model->id)
                            ->byDocumentDateRange('2017-05-01', '2017-05-31')
                            ->count();
                        $actCount = Act::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-05-01', '2017-05-31')
                            ->count();
                        $packingListCount = PackingList::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-05-01', '2017-05-31')
                            ->count();
                        $invoiceFactureCount = InvoiceFacture::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-05-01', '2017-05-31')
                            ->count();
                        $updCount = Upd::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-05-01', '2017-05-31')
                            ->count();

                        return "Счета: $invoiceCount | Акты: $actCount | ТН: $packingListCount | СФ: $invoiceFactureCount | УПД: $updCount";
                    },
                ],
                [
                    'attribute' => 'june',
                    'value' => function (Company $model) {
                        $invoiceCount = Invoice::find()
                            ->byDeleted()
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byCompany($model->id)
                            ->byDocumentDateRange('2017-06-01', '2017-06-30')
                            ->count();
                        $actCount = Act::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-06-01', '2017-06-30')
                            ->count();
                        $packingListCount = PackingList::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-06-01', '2017-06-30')
                            ->count();
                        $invoiceFactureCount = InvoiceFacture::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-06-01', '2017-06-30')
                            ->count();
                        $updCount = Upd::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-06-01', '2017-06-30')
                            ->count();

                        return "Счета: $invoiceCount | Акты: $actCount | ТН: $packingListCount | СФ: $invoiceFactureCount | УПД: $updCount";
                    },
                ],
                [
                    'attribute' => 'july',
                    'value' => function (Company $model) {
                        $invoiceCount = Invoice::find()
                            ->byDeleted()
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byCompany($model->id)
                            ->byDocumentDateRange('2017-07-01', '2017-07-31')
                            ->count();
                        $actCount = Act::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-07-01', '2017-07-31')
                            ->count();
                        $packingListCount = PackingList::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-07-01', '2017-07-31')
                            ->count();
                        $invoiceFactureCount = InvoiceFacture::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-07-01', '2017-07-31')
                            ->count();
                        $updCount = Upd::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-07-01', '2017-07-31')
                            ->count();

                        return "Счета: $invoiceCount | Акты: $actCount | ТН: $packingListCount | СФ: $invoiceFactureCount | УПД: $updCount";
                    },
                ],
                [
                    'attribute' => 'august',
                    'value' => function (Company $model) {
                        $invoiceCount = Invoice::find()
                            ->byDeleted()
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byCompany($model->id)
                            ->byDocumentDateRange('2017-08-01', '2017-08-31')
                            ->count();
                        $actCount = Act::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-08-01', '2017-08-31')
                            ->count();
                        $packingListCount = PackingList::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-08-01', '2017-08-31')
                            ->count();
                        $invoiceFactureCount = InvoiceFacture::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-08-01', '2017-08-31')
                            ->count();
                        $updCount = Upd::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-08-01', '2017-08-31')
                            ->count();

                        return "Счета: $invoiceCount | Акты: $actCount | ТН: $packingListCount | СФ: $invoiceFactureCount | УПД: $updCount";
                    },
                ],
                [
                    'attribute' => 'september',
                    'value' => function (Company $model) {
                        $invoiceCount = Invoice::find()
                            ->byDeleted()
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byCompany($model->id)
                            ->byDocumentDateRange('2017-09-01', '2017-09-30')
                            ->count();
                        $actCount = Act::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-09-01', '2017-09-30')
                            ->count();
                        $packingListCount = PackingList::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-09-01', '2017-09-30')
                            ->count();
                        $invoiceFactureCount = InvoiceFacture::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-01-01', '2017-01-31')
                            ->count();
                        $updCount = Upd::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-09-01', '2017-09-30')
                            ->count();

                        return "Счета: $invoiceCount | Акты: $actCount | ТН: $packingListCount | СФ: $invoiceFactureCount | УПД: $updCount";
                    },
                ],
                [
                    'attribute' => 'october',
                    'value' => function (Company $model) {
                        $invoiceCount = Invoice::find()
                            ->byDeleted()
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byCompany($model->id)
                            ->byDocumentDateRange('2017-10-01', '2017-10-31')
                            ->count();
                        $actCount = Act::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-10-01', '2017-10-31')
                            ->count();
                        $packingListCount = PackingList::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-10-01', '2017-10-31')
                            ->count();
                        $invoiceFactureCount = InvoiceFacture::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-10-01', '2017-10-31')
                            ->count();
                        $updCount = Upd::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-10-01', '2017-10-31')
                            ->count();

                        return "Счета: $invoiceCount | Акты: $actCount | ТН: $packingListCount | СФ: $invoiceFactureCount | УПД: $updCount";
                    },
                ],
                [
                    'attribute' => 'november',
                    'value' => function (Company $model) {
                        $invoiceCount = Invoice::find()
                            ->byDeleted()
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byCompany($model->id)
                            ->byDocumentDateRange('2017-11-01', '2017-11-30')
                            ->count();
                        $actCount = Act::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-11-01', '2017-11-30')
                            ->count();
                        $packingListCount = PackingList::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-11-01', '2017-11-30')
                            ->count();
                        $invoiceFactureCount = InvoiceFacture::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-11-01', '2017-11-30')
                            ->count();
                        $updCount = Upd::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-11-01', '2017-11-30')
                            ->count();

                        return "Счета: $invoiceCount | Акты: $actCount | ТН: $packingListCount | СФ: $invoiceFactureCount | УПД: $updCount";
                    },
                ],
                [
                    'attribute' => 'december',
                    'value' => function (Company $model) {
                        $invoiceCount = Invoice::find()
                            ->byDeleted()
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byCompany($model->id)
                            ->byDocumentDateRange('2017-12-01', '2017-12-31')
                            ->count();
                        $actCount = Act::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-12-01', '2017-12-31')
                            ->count();
                        $packingListCount = PackingList::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-12-01', '2017-12-31')
                            ->count();
                        $invoiceFactureCount = InvoiceFacture::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-12-01', '2017-12-31')
                            ->count();
                        $updCount = Upd::find()
                            ->byCompany($model->id)
                            ->byIOType(Documents::IO_TYPE_OUT)
                            ->byDocumentDateRange('2017-12-01', '2017-12-31')
                            ->count();

                        return "Счета: $invoiceCount | Акты: $actCount | ТН: $packingListCount | СФ: $invoiceFactureCount | УПД: $updCount";
                    },
                ],
            ],
            'headers' => [
                'name' => 'Название компании \ Месяц',
                'january' => 'Январь',
                'february' => 'Февраль',
                'march' => 'Март',
                'april' => 'Апрель',
                'may' => 'Май',
                'june' => 'Июнь',
                'july' => 'Июль',
                'august' => 'Август',
                'september' => 'Сентябрь',
                'october' => 'Октябрь',
                'november' => 'Ноябрь',
                'december' => 'Декабрь',
            ],
            'format' => 'Xlsx',
            'fileName' => 'Companies.xls',
            'savePath' => __DIR__,
        ]);
    }

    /**
     * @throws \PHPExcel_Exception
     */
    public function actionImportInvoicesAndActs()
    {
        $attributeNames = [
            'A' => 'invoiceNumber',
            'B' => 'invoiceDate',
            'C' => 'contractorName',
            'D' => 'contractorINN',
            // 'E' => 'agreement',
            'E' => 'amount',
            'F' => 'serviceName',
            // 'H' => 'employeeLastName',
            'G' => 'actNumber',
            'H' => 'actDate',
        ];
        $company = Company::findOne(20104);

        $xls = \PHPExcel_IOFactory::load(\Yii::getAlias('@frontend/web/upload/invoices.xls'));
        $xls->setActiveSheetIndex(0);
        $sheet = $xls->getActiveSheet();
        $rowIterator = $sheet->getRowIterator();
        $oneInvoice = [];
        $contractorID = null;
        $serviceID = null;

        foreach ($rowIterator as $key => $row) {
            if ($key == 1) continue;
            $cellIterator = $row->getCellIterator();
            /* @var \PHPExcel_Cell $cell */
            foreach ($cellIterator as $cellKey => $cell) {
                if (isset($attributeNames[$cellKey])) {
                    $oneInvoice[$attributeNames[$cellKey]] = trim($cell->getFormattedValue());
                }
            }
            if ($oneInvoice['invoiceNumber'] !== '') {
                $oneInvoice['amount'] = preg_replace("/[^0-9]/", '', $oneInvoice['amount']);
                /* @var $contractor Contractor */
                if (($contractor = Contractor::find()
                        ->byDeleted()
                        ->andWhere(['and',
                            ['ITN' => $oneInvoice['contractorINN']],
                            ['company_id' => $company->id],
                        ])->one()) == null
                ) {
                    $response = \Yii::$app->dadataSuggestApi->getParty([
                        'query' => $oneInvoice['contractorINN'],
                        'count' => 1
                    ]);
                    if (!$response->isError && !empty($response->data['suggestions'])) {
                        $data = $response->data['suggestions'][0];

                        $contractor = new Contractor();
                        $contractor->company_id = $company->id;
                        $contractor->type = Contractor::TYPE_CUSTOMER;
                        $contractor->ITN = $oneInvoice['contractorINN'];
                        /* @var $contractorType CompanyType */
                        $contractorType = CompanyType::find()
                            ->andWhere(['in_contractor' => true])
                            ->andWhere(['or',
                                ['name_short' => $data['data']['opf']['short']],
                                ['name_full' => $data['data']['opf']['short']],
                            ])->one();
                        if ($contractorType) {
                            $contractor->company_type_id = $contractorType->id;
                        }
                        $contractor->name = isset($data['data']['name']['full']) ? $data['data']['name']['full'] : null;
                        $contractor->PPC = isset($data['data']['kpp']) ? $data['data']['kpp'] : null;
                        $contractor->BIN = isset($data['data']['ogrn']) ? $data['data']['ogrn'] : null;
                        $contractor->legal_address = isset($data['data']['address']['value']) ? $data['data']['address']['value'] : null;
                        $contractor->actual_address = $contractor->legal_address;
                        $contractor->director_name = isset($data['data']['management']['name']) ?
                            $data['data']['management']['name'] :
                            $contractor->name;
                        $contractor->save(false);
                    }
                }
                /* @var $service Product */
                if (($service = Product::find()
                        ->byDeleted()
                        ->byCompany($company->id)
                        ->byProductionType(Product::PRODUCTION_TYPE_SERVICE)
                        ->andWhere(['title' => $oneInvoice['serviceName']])
                        ->one()) == null
                ) {
                    $service = new Product();
                    $service->production_type = Product::PRODUCTION_TYPE_SERVICE;
                    $service->company_id = $company->id;
                    $service->title = $oneInvoice['serviceName'];
                    $service->price_for_buy_with_nds = $oneInvoice['amount'];
                    $service->price_for_sell_with_nds = $oneInvoice['amount'];
                    $service->price_for_sell_nds_id = TaxRate::RATE_WITHOUT;
                    $service->price_for_buy_nds_id = TaxRate::RATE_WITHOUT;
                    $service->country_origin_id = Country::COUNTRY_WITHOUT;
                    $service->product_unit_id = ProductUnit::UNIT_COUNT;
                    $service->save();
                }
                $oneInvoice['invoiceNumber'] = explode(' - ', $oneInvoice['invoiceNumber']);
                /* @var $employeeCompany EmployeeCompany */
                $employeeCompany = $company->getChiefEmployeeCompany();

                if ($employeeCompany && $contractor && $service) {
                    $invoice = new Invoice();
                    $invoice->type = Documents::IO_TYPE_OUT;
                    $invoice->invoice_status_id = InvoiceStatus::STATUS_CREATED;
                    $invoice->invoice_status_author_id = $employeeCompany->employee_id;
                    $invoice->document_author_id = $employeeCompany->employee_id;
                    $invoice->document_date = $oneInvoice['invoiceDate'];
                    $invoice->document_number = $oneInvoice['invoiceNumber'][0];
                    $invoice->document_additional_number = isset($oneInvoice['invoiceNumber'][1]) ? $oneInvoice['invoiceNumber'][1] : null;
                    $invoice->payment_limit_date = date(DateHelper::FORMAT_DATE, strtotime('+10 days', strtotime($invoice->document_date)));
                    $invoice->invoice_expenditure_item_id = InvoiceExpenditureItem::ITEM_CONTRACTOR_PAYMENT;
                    $invoice->isAutoinvoice = 0;
                    $invoice->production_type = Product::PRODUCTION_TYPE_SERVICE;

                    $invoice->company_id = $company->id;
                    $invoice->company_inn = $company->inn;
                    $invoice->company_kpp = $company->kpp;
                    $invoice->company_egrip = $company->egrip;
                    $invoice->company_okpo = $company->okpo;
                    $invoice->company_name_full = $company->getTitle();
                    $invoice->company_name_short = $company->getTitle(true, true);
                    $invoice->company_address_legal_full = $company->getAddressLegalFull();
                    $invoice->company_phone = $company->phone;
                    $invoice->company_chief_post_name = $company->chief_post_name;
                    $invoice->company_chief_lastname = $company->chief_lastname;
                    $invoice->company_chief_firstname_initials = $company->chief_firstname_initials;
                    $invoice->company_chief_patronymic_initials = $company->chief_patronymic_initials;
                    $invoice->company_chief_accountant_lastname = $company->chief_accountant_lastname;
                    $invoice->company_chief_accountant_firstname_initials = $company->chief_accountant_firstname_initials;
                    $invoice->company_chief_accountant_patronymic_initials = $company->chief_accountant_patronymic_initials;
                    $invoice->company_print_filename = $company->print_link;
                    $invoice->company_chief_signature_filename = $company->chief_signature_link;

                    $invoice->contractor_id = $contractor->id;
                    $invoice->contractor_name_short = $contractor->getTitle(true);
                    $invoice->contractor_name_full = $contractor->getTitle(false);
                    $invoice->contractor_address_legal_full = ($contractor->face_type == Contractor::TYPE_LEGAL_PERSON ? $contractor->legal_address : $contractor->physical_address);
                    $invoice->contractor_bank_name = $contractor->bank_name;
                    $invoice->contractor_bank_city = $contractor->bank_city;
                    $invoice->contractor_bik = $contractor->BIC;
                    $invoice->contractor_inn = $contractor->ITN;
                    $invoice->contractor_kpp = $contractor->PPC;
                    $invoice->contractor_ks = $contractor->corresp_account;
                    $invoice->contractor_rs = $contractor->current_account;

                    $invoice->total_amount_no_nds = $service->price_for_sell_with_nds;
                    $invoice->total_amount_with_nds = $service->price_for_sell_with_nds;
                    $invoice->total_amount_has_nds = false;
                    $invoice->total_order_count = 1;
                    $invoice->nds_view_type_id = Invoice::NDS_VIEW_WITHOUT;
                    $invoice->company_checking_accountant_id = $company->mainCheckingAccountant->id;
                    $invoice->price_precision = 2;

                    // if ($oneInvoice['agreement']) {
                    //     $basisDate = mb_substr($oneInvoice['agreement'], -10);
                    //     $basisNumber = preg_replace("/[^0-9]/", '', str_replace($basisDate, '', $oneInvoice['agreement']));
                    //     $basisDate = DateHelper::format($basisDate, DateHelper::FORMAT_DATE, DateHelper::FORMAT_USER_DATE);
                    //     $invoice->basis_document_name = 'Договор';
                    //     $invoice->basis_document_number = $basisNumber;
                    //     $invoice->basis_document_date = $basisDate;
                    // }

                    $order = OrderHelper::createOrderByProduct($service, $invoice, 1, $oneInvoice['amount']);
                    $order->product_title = $service->title;
                    $order->number = 1;
                    $invoice->populateRelation('orders', [$order]);
                    if ($invoice->save()) {
                        echo "Invoice - $invoice->id - created \r\n";
                        $order->invoice_id = $invoice->id;
                        if ($order->save()) {
                            echo "Order - $order->id - created \r\n";
                        } else {
                            echo "Order NOT created \r\n";
                        }
                    } else {
                        echo "Invoice NOT created \r\n";
                        echo $invoice->document_number . "\r\n";
                        var_dump($invoice->getErrors());
                    }
                    if ($oneInvoice['actNumber'] && $oneInvoice['actDate']) {
                        if ($invoice->createAct(DateHelper::format($oneInvoice['actDate'], DateHelper::FORMAT_USER_DATE, 'm-d-y'))) {
                            /* @var $act Act */
                            $act = $invoice->getAct()->one();
                            $act->document_number = $oneInvoice['actNumber'];
                            $act->document_author_id = $employeeCompany->employee_id;
                            $act->status_out_author_id = $employeeCompany->employee_id;

                            $act->save(true, ['document_number', 'document_author_id', 'status_out_author_id']);
                            echo "Act - $act->id - created \r\n";
                        } else {
                            echo "Act NOT created \r\n";
                        }
                    }
                }
            }
        }
    }

    /**
     * @throws \PHPExcel_Exception
     * @throws \PHPExcel_Reader_Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionImportInvoice()
    {
        /* @var $company Company */
        $company = Company::findOne(21812);
        $attributeNames = [
            'A' => 'documentDate',
            'B' => 'documentNumber',
            'F' => 'contractorInn',
            'G' => 'serviceName',
            'H' => 'serviceCount',
            'J' => 'servicePrice',
        ];
        $xls = \PHPExcel_IOFactory::load(\Yii::getAlias('@frontend/web/upload/Invoices.xlsx'));
        $xls->setActiveSheetIndex(0);
        $sheet = $xls->getActiveSheet();
        $rowIterator = $sheet->getRowIterator();
        $invoiceData = [];
        $oneInvoiceOrders = [];

        foreach ($rowIterator as $key => $row) {
            if ($key == 1) continue;
            $cellIterator = $row->getCellIterator();
            /* @var \PHPExcel_Cell $cell */
            foreach ($cellIterator as $cellKey => $cell) {
                if (isset($attributeNames[$cellKey])) {
                    $invoiceData[$attributeNames[$cellKey]] = trim($cell->getFormattedValue());
                }
            }
            if ($invoiceData['documentDate'] && $invoiceData['documentNumber'] && $invoiceData['contractorInn'] &&
                $invoiceData['serviceName'] && $invoiceData['serviceCount'] && $invoiceData['servicePrice']) {
                if (empty($oneInvoiceOrders)) {
                    $oneInvoiceOrders = [
                        'documentDate' => $invoiceData['documentDate'],
                        'documentNumber' => $invoiceData['documentNumber'],
                        'contractorInn' => $invoiceData['contractorInn'],
                    ];
                    $oneInvoiceOrders['order'][] = [
                        'serviceName' => $invoiceData['serviceName'],
                        'serviceCount' => $invoiceData['serviceCount'],
                        'servicePrice' => $invoiceData['servicePrice'],
                    ];
                } elseif ($oneInvoiceOrders['documentNumber'] == $invoiceData['documentNumber']) {
                    $oneInvoiceOrders['order'][] = [
                        'serviceName' => $invoiceData['serviceName'],
                        'serviceCount' => $invoiceData['serviceCount'],
                        'servicePrice' => $invoiceData['servicePrice'],
                    ];
                } else {
                    /* @var $contractor Contractor */
                    if (($contractor = Contractor::find()->byDeleted()
                            ->andWhere(['and',
                                ['ITN' => $oneInvoiceOrders['contractorInn']],
                                ['company_id' => $company->id],
                            ])->one()) !== null
                    ) {
                        $invoice = new Invoice();
                        $invoice->type = Documents::IO_TYPE_OUT;
                        $invoice->invoice_status_id = InvoiceStatus::STATUS_CREATED;
                        $invoice->invoice_status_author_id = $company->getChiefEmployeeCompany()->employee_id;
                        $invoice->document_author_id = $company->getChiefEmployeeCompany()->employee_id;
                        $invoice->document_date = DateHelper::format($oneInvoiceOrders['documentDate'], DateHelper::FORMAT_USER_DATE, 'm-d-y');
                        $invoice->document_number = $oneInvoiceOrders['documentNumber'];
                        $invoice->payment_limit_date = date(DateHelper::FORMAT_DATE, strtotime('+10 days', strtotime($invoice->document_date)));
                        $invoice->invoice_expenditure_item_id = InvoiceExpenditureItem::ITEM_CONTRACTOR_PAYMENT;
                        $invoice->isAutoinvoice = 0;
                        $invoice->production_type = Product::PRODUCTION_TYPE_SERVICE;

                        $invoice->company_id = $company->id;
                        $invoice->company_inn = $company->inn;
                        $invoice->company_kpp = $company->kpp;
                        $invoice->company_egrip = $company->egrip;
                        $invoice->company_okpo = $company->okpo;
                        $invoice->company_name_full = $company->getTitle();
                        $invoice->company_name_short = $company->getTitle(true, true);
                        $invoice->company_address_legal_full = $company->getAddressLegalFull();
                        $invoice->company_phone = $company->phone;
                        $invoice->company_chief_post_name = $company->chief_post_name;
                        $invoice->company_chief_lastname = $company->chief_lastname;
                        $invoice->company_chief_firstname_initials = $company->chief_firstname_initials;
                        $invoice->company_chief_patronymic_initials = $company->chief_patronymic_initials;
                        $invoice->company_chief_accountant_lastname = $company->chief_accountant_lastname;
                        $invoice->company_chief_accountant_firstname_initials = $company->chief_accountant_firstname_initials;
                        $invoice->company_chief_accountant_patronymic_initials = $company->chief_accountant_patronymic_initials;
                        $invoice->company_print_filename = $company->print_link;
                        $invoice->company_chief_signature_filename = $company->chief_signature_link;

                        $invoice->contractor_id = $contractor->id;
                        $invoice->contractor_name_short = $contractor->getTitle(true);
                        $invoice->contractor_name_full = $contractor->getTitle(false);
                        $invoice->contractor_address_legal_full = ($contractor->face_type == Contractor::TYPE_LEGAL_PERSON ? $contractor->legal_address : $contractor->physical_address);
                        $invoice->contractor_bank_name = $contractor->bank_name;
                        $invoice->contractor_bank_city = $contractor->bank_city;
                        $invoice->contractor_bik = $contractor->BIC;
                        $invoice->contractor_inn = $contractor->ITN;
                        $invoice->contractor_kpp = $contractor->PPC;
                        $invoice->contractor_ks = $contractor->corresp_account;
                        $invoice->contractor_rs = $contractor->current_account;

                        $invoice->total_amount_has_nds = false;
                        $invoice->nds_view_type_id = Invoice::NDS_VIEW_WITHOUT;
                        $invoice->company_checking_accountant_id = $company->mainCheckingAccountant->id;
                        $invoice->price_precision = 2;
                        $number = 0;
                        $orderArray = [];
                        $totalAmount = 0;
                        foreach ($oneInvoiceOrders['order'] as $oneOrder) {
                            /* @var $service Product */
                            if (($service = Product::find()
                                    ->byProductionType(Product::PRODUCTION_TYPE_SERVICE)
                                    ->byDeleted()
                                    ->andWhere(['title' => $oneOrder['serviceName']])
                                    ->one()) == null
                            ) {
                                $service = new Product();
                                $service->company_id = $company->id;
                                $service->production_type = Product::PRODUCTION_TYPE_SERVICE;
                                $service->title = $oneOrder['serviceName'];
                                $service->price_for_buy_with_nds = 0;
                                $service->price_for_sell_with_nds = 0;
                                $service->price_for_sell_nds_id = TaxRate::RATE_WITHOUT;
                                $service->price_for_buy_nds_id = TaxRate::RATE_WITHOUT;
                                $service->country_origin_id = Country::COUNTRY_WITHOUT;
                                $service->product_unit_id = ProductUnit::UNIT_COUNT;
                                if ($service->save()) {
                                    echo "Product {$service->id} created \r\n";
                                } else {
                                    echo "Error: \r\n";
                                    foreach ($service->getErrors() as $attribute => $error) {
                                        echo "{$attribute} - {$error[0]} \r\n";
                                    }
                                }
                            }
                            $number++;
                            $totalAmount += ($oneOrder['serviceCount'] * TextHelper::parseMoneyInputWithoutComma($oneOrder['servicePrice']));
                            $order = OrderHelper::createOrderByProduct($service, $invoice, $oneOrder['serviceCount'], TextHelper::parseMoneyInputWithoutComma($oneOrder['servicePrice']));
                            $order->product_title = $service->title;
                            $order->number = $number;
                            $orderArray[] = $order;
                        }
                        $invoice->total_amount_no_nds = $totalAmount;
                        $invoice->total_amount_with_nds = $totalAmount;
                        $invoice->total_order_count = $number;
                        $invoice->populateRelation('orders', $orderArray);
                        if ($invoice->save()) {
                            echo "Invoice created - {$invoice->id}: \r\n";
                            foreach ($orderArray as $order) {
                                $order->invoice_id = $invoice->id;
                                if (!$order->save()) {
                                    echo "Error: \r\n";
                                    foreach ($order->getErrors() as $attribute => $error) {
                                        echo "{$attribute} - {$error[0]} - value - {$order->$attribute} \r\n";
                                    }
                                } else {
                                    echo "Order to invoice {$invoice->id} created - {$order->id}: \r\n";
                                }
                            }
                        } else {
                            echo "Error: \r\n";
                            foreach ($invoice->getErrors() as $attribute => $error) {
                                echo "{$attribute} - {$error[0]} - value - {$invoice->$attribute} \r\n";
                            }
                        }
                    } else {
                        echo "Contractor not found: {$oneInvoiceOrders['contractorInn']} \r\n";
                    }
                    $oneInvoiceOrders = [
                        'documentDate' => $invoiceData['documentDate'],
                        'documentNumber' => $invoiceData['documentNumber'],
                        'contractorInn' => $invoiceData['contractorInn'],
                    ];
                    $oneInvoiceOrders['order'][] = [
                        'serviceName' => $invoiceData['serviceName'],
                        'serviceCount' => $invoiceData['serviceCount'],
                        'servicePrice' => $invoiceData['servicePrice'],
                    ];
                }
            }
        }
    }

    /**
     * @throws \PHPExcel_Exception
     */
    public function actionImportActs()
    {
        $attributeNames = [
            'A' => 'actNumber',
            'B' => 'actDate',
            'F' => 'invoiceNumber',
            'G' => 'invoiceDate',
        ];

        $xls = \PHPExcel_IOFactory::load(\Yii::getAlias('@frontend/web/upload/acts-2017.xls'));
        $xls->setActiveSheetIndex(0);
        $sheet = $xls->getActiveSheet();
        $rowIterator = $sheet->getRowIterator();
        $oneAct = [];
        $contractorID = null;
        $serviceID = null;

        foreach ($rowIterator as $key => $row) {
            if ($key == 1) continue;
            $cellIterator = $row->getCellIterator();
            /* @var \PHPExcel_Cell $cell */
            foreach ($cellIterator as $cellKey => $cell) {
                if (isset($attributeNames[$cellKey])) {
                    $oneAct[$attributeNames[$cellKey]] = trim($cell->getFormattedValue());
                }
            }
            if (isset($oneAct['actNumber']) && $oneAct['actNumber']) {
                /* @var $invoice Invoice */
                $invoice = Invoice::find()
                    ->byIOType(Documents::IO_TYPE_OUT)
                    ->byDeleted()
                    ->byCompany(8939)
                    ->andWhere(['and',
                        ['document_date' => DateHelper::format($oneAct['invoiceDate'], DateHelper::FORMAT_DATE, DateHelper::FORMAT_USER_DATE)],
                        ['document_number' => $oneAct['invoiceNumber']],
                    ])->one();

                if ($invoice && $invoice->act == null) {
                    if ($invoice->createAct(DateHelper::format($oneAct['actDate'], DateHelper::FORMAT_USER_DATE, 'd/m/y'))) {
                        /* @var $act Act */
                        $act = $invoice->getAct()->one();
                        $act->document_number = $oneAct['actNumber'];
                        $act->document_author_id = $invoice->document_author_id;
                        $act->status_out_author_id = $invoice->invoice_status_author_id;
                        $act->save(true, ['document_number', 'document_author_id', 'status_out_author_id']);
                    }
                }
            }
        }
    }

    /**
     * @throws \PHPExcel_Exception
     */
    public function actionUpdateAgreement()
    {
        $attributeNames = [
            'A' => 'invoiceNumber',
            'D' => 'contractorINN',
            'E' => 'agreement',
        ];

        $xls = \PHPExcel_IOFactory::load(\Yii::getAlias('@frontend/web/upload/cda.xlsx'));
        $xls->setActiveSheetIndex(0);
        $sheet = $xls->getActiveSheet();
        $rowIterator = $sheet->getRowIterator();
        $oneInvoice = [];
        $contractorID = null;
        $serviceID = null;

        foreach ($rowIterator as $key => $row) {
            if ($key == 1) continue;
            $cellIterator = $row->getCellIterator();
            /* @var \PHPExcel_Cell $cell */
            foreach ($cellIterator as $cellKey => $cell) {
                if (isset($attributeNames[$cellKey])) {
                    $oneInvoice[$attributeNames[$cellKey]] = trim($cell->getFormattedValue());
                }
            }
            if ($oneInvoice['agreement']) {
                $basisDate = mb_substr($oneInvoice['agreement'], -10);
                $basisNumber = preg_replace("/[^0-9]/", '', str_replace($basisDate, '', $oneInvoice['agreement']));
                $basisDate = DateHelper::format($basisDate, DateHelper::FORMAT_DATE, DateHelper::FORMAT_USER_DATE);
                /* @var $contractor Contractor */
                $contractor = Contractor::find()
                    ->byDeleted()
                    ->andWhere(['and',
                        ['ITN' => $oneInvoice['contractorINN']],
                        ['company_id' => 5],
                    ])->one();
                /* @var $invoice Invoice */
                $invoice = Invoice::find()
                    ->byCompany(5)
                    ->byDeleted()
                    ->byContractorId($contractor->id)
                    ->byIOType(Documents::IO_TYPE_OUT)
                    ->byDocumentDateRange('2017-01-01', '2017-12-31')
                    ->andWhere(['document_number' => $oneInvoice['invoiceNumber']])
                    ->one();

                if (!Agreement::find()->andWhere(['and',
                    ['company_id' => 5],
                    ['contractor_id' => $contractor->id],
                    ['document_number' => $basisNumber],
                    ['document_date' => $basisDate],
                    ['document_type_id' => 1],
                    ['is_created' => 1],
                ])->exists()
                ) {
                    $agreement = new Agreement();
                    $agreement->detachBehavior('blameableBehavior');
                    $agreement->company_id = 5;
                    $agreement->contractor_id = $contractor->id;
                    $agreement->document_name = 'Договор';
                    $agreement->document_number = $basisNumber;
                    $agreement->document_date_input = DateHelper::format($basisDate, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                    $agreement->document_type_id = 1;
                    $agreement->created_by = $invoice->document_author_id;
                    $agreement->is_created = 1;
                    $agreement->save();
                }

                if ($invoice) {
                    $invoice->basis_document_name = 'Договор';
                    $invoice->basis_document_number = $basisNumber;
                    $invoice->basis_document_date = $basisDate;
                    $invoice->basis_document_type_id = 1;

                    $invoice->save(false, ['basis_document_name', 'basis_document_number', 'basis_document_date', 'basis_document_type_id']);
                }
            }
        }
    }

    /**
     *
     */
    public function actionExportCompanyInvoicesOnTrial()
    {
        $companies = Company::find()
            ->isBlocked(Company::UNBLOCKED)
            ->isTest(!Company::TEST_COMPANY)
            ->andWhere(['between', 'created_at', strtotime('2018-01-01'), strtotime('2018-12-31')])
            ->all();

        $excel = new Excel();
        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $companies,
            'title' => 'Счета компаний в триал периоде',
            'columns' => [
                [
                    'attribute' => 'id',
                    'value' => function (Company $model) {
                        return $model->id;
                    },
                ],
                [
                    'attribute' => 'name',
                    'value' => function (Company $model) {
                        return $model->getShortName();
                    },
                ],
                [
                    'attribute' => 'out_invoice_count',
                    'value' => function (Company $model) {
                        /* @var $trialSubscribe Subscribe */
                        $trialSubscribe = $model->getSubscribes()
                            ->andWhere(['tariff_id' => SubscribeTariff::TARIFF_TRIAL])
                            ->one();
                        if ($trialSubscribe) {
                            return $model->getInvoices()
                                ->where(['and',
                                    ['type' => Documents::IO_TYPE_OUT],
                                    ['is_deleted' => false],
                                    ['between', 'created_at', $trialSubscribe->activated_at, $trialSubscribe->expired_at],
                                ])->count();
                        }

                        return 0;
                    },
                ],
                [
                    'attribute' => 'in_invoice_count',
                    'value' => function (Company $model) {
                        /* @var $trialSubscribe Subscribe */
                        $trialSubscribe = $model->getSubscribes()
                            ->andWhere(['tariff_id' => SubscribeTariff::TARIFF_TRIAL])
                            ->one();
                        if ($trialSubscribe) {
                            return $model->getInvoices()
                                ->where(['and',
                                    ['type' => Documents::IO_TYPE_IN],
                                    ['is_deleted' => false],
                                    ['between', 'created_at', $trialSubscribe->activated_at, $trialSubscribe->expired_at],
                                ])->count();
                        }

                        return 0;
                    },
                ],
            ],
            'headers' => [
                'id' => 'ID',
                'name' => 'Наименование',
                'out_invoice_count' => 'Количество исходящих счетов',
                'in_invoice_count' => 'Количество входящих счетов',
            ],
            'format' => 'Xlsx',
            'fileName' => 'CompanyInvoicesInTrialSubscribe.xls',
            'savePath' => __DIR__,
        ]);
    }

    /**
     *
     */
    public function actionUpdateInvoiceContractors()
    {
        /* @var $invoices Invoice[] */
        $invoices = Invoice::find()->andWhere(['between', 'created_at', strtotime('-3 days'), strtotime('+1 days')])->all();
        foreach ($invoices as $invoice) {
            if ($invoice->contractor_id) {
                $invoice->setContractor($invoice->contractor);
                $invoice->save();
            }
        }
    }

    /**
     * @param $year
     */
    public function actionExportCompanyByBank($year)
    {
        $models = CheckingAccountant::find()
            ->joinWith('company')
            ->andWhere(['not', [CheckingAccountant::tableName() . '.bank_name' => null]])
            ->andWhere(['not', [CheckingAccountant::tableName() . '.bank_name' => '']])
            ->andWhere(['between', Company::tableName() . '.created_at', strtotime("01.01.{$year}"), strtotime("31.12.{$year}")])
            ->groupBy(CheckingAccountant::tableName() . '.bik')
            ->all();

        $excel = new Excel();
        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $models,
            'title' => "Банки_{$year}",
            'columns' => [
                [
                    'attribute' => 'banks',
                    'value' => function (CheckingAccountant $model) {
                        return $model->bank_name;
                    },
                ],
                [
                    'attribute' => 'company_count',
                    'value' => function (CheckingAccountant $model) use ($year) {
                        return CheckingAccountant::find()
                            ->joinWith('company')
                            ->andWhere([CheckingAccountant::tableName() . '.bik' => $model->bik])
                            ->andWhere([Company::tableName() . '.test' => !Company::TEST_COMPANY])
                            ->andWhere(['between', Company::tableName() . '.created_at', strtotime("01.01.{$year}"), strtotime("31.12.{$year}")])
                            ->groupBy('company_id')
                            ->count();
                    },
                ],
                [
                    'attribute' => 'one_invoice',
                    'value' => function (CheckingAccountant $model) use ($year) {
                        return CheckingAccountant::find()
                            ->select([
                                'COUNT(invoice.id) as invoiceCount',
                            ])
                            ->andWhere([CheckingAccountant::tableName() . '.bik' => $model->bik])
                            ->joinWith('company')
                            ->joinWith('company.invoices')
                            ->andWhere([Invoice::tableName() . '.type' => Documents::IO_TYPE_OUT])
                            ->andWhere([Invoice::tableName() . '.is_deleted' => Invoice::NOT_IS_DELETED])
                            ->andWhere(['not', [Invoice::tableName() . '.invoice_status_id' => InvoiceStatus::STATUS_REJECTED]])
                            ->andWhere(['between', Invoice::tableName() . '.document_date', "{$year}-01-01", "{$year}-12-31"])
                            ->andWhere([Company::tableName() . '.test' => !Company::TEST_COMPANY])
                            ->andHaving(['invoiceCount' => 1])
                            ->groupBy(CheckingAccountant::tableName() . '.company_id')
                            ->count();
                    },
                ],
                [
                    'attribute' => 'two_invoices',
                    'value' => function (CheckingAccountant $model) use ($year) {
                        return CheckingAccountant::find()
                            ->select([
                                'COUNT(invoice.id) as invoiceCount',
                            ])
                            ->andWhere([CheckingAccountant::tableName() . '.bik' => $model->bik])
                            ->joinWith('company')
                            ->joinWith('company.invoices')
                            ->andWhere([Invoice::tableName() . '.type' => Documents::IO_TYPE_OUT])
                            ->andWhere([Invoice::tableName() . '.is_deleted' => Invoice::NOT_IS_DELETED])
                            ->andWhere(['not', [Invoice::tableName() . '.invoice_status_id' => InvoiceStatus::STATUS_REJECTED]])
                            ->andWhere(['between', Invoice::tableName() . '.document_date', "{$year}-01-01", "{$year}-12-31"])
                            ->andWhere([Company::tableName() . '.test' => !Company::TEST_COMPANY])
                            ->andHaving(['invoiceCount' => 2])
                            ->groupBy(CheckingAccountant::tableName() . '.company_id')
                            ->count();
                    },
                ],
                [
                    'attribute' => 'five_invoices',
                    'value' => function (CheckingAccountant $model) use ($year) {
                        return CheckingAccountant::find()
                            ->select([
                                'COUNT(invoice.id) as invoiceCount',
                            ])
                            ->andWhere([CheckingAccountant::tableName() . '.bik' => $model->bik])
                            ->joinWith('company')
                            ->joinWith('company.invoices')
                            ->andWhere([Invoice::tableName() . '.type' => Documents::IO_TYPE_OUT])
                            ->andWhere([Invoice::tableName() . '.is_deleted' => Invoice::NOT_IS_DELETED])
                            ->andWhere(['not', [Invoice::tableName() . '.invoice_status_id' => InvoiceStatus::STATUS_REJECTED]])
                            ->andWhere(['between', Invoice::tableName() . '.document_date', "{$year}-01-01", "{$year}-12-31"])
                            ->andWhere([Company::tableName() . '.test' => !Company::TEST_COMPANY])
                            ->andHaving(['between', 'invoiceCount', 3, 5])
                            ->groupBy(CheckingAccountant::tableName() . '.company_id')
                            ->count();
                    },
                ],
                [
                    'attribute' => 'ten_invoices',
                    'value' => function (CheckingAccountant $model) use ($year) {
                        return CheckingAccountant::find()
                            ->select([
                                'COUNT(invoice.id) as invoiceCount',
                            ])
                            ->andWhere([CheckingAccountant::tableName() . '.bik' => $model->bik])
                            ->joinWith('company')
                            ->joinWith('company.invoices')
                            ->andWhere([Invoice::tableName() . '.type' => Documents::IO_TYPE_OUT])
                            ->andWhere([Invoice::tableName() . '.is_deleted' => Invoice::NOT_IS_DELETED])
                            ->andWhere(['not', [Invoice::tableName() . '.invoice_status_id' => InvoiceStatus::STATUS_REJECTED]])
                            ->andWhere(['between', Invoice::tableName() . '.document_date', "{$year}-01-01", "{$year}-12-31"])
                            ->andWhere([Company::tableName() . '.test' => !Company::TEST_COMPANY])
                            ->andHaving(['between', 'invoiceCount', 6, 10])
                            ->groupBy(CheckingAccountant::tableName() . '.company_id')
                            ->count();
                    },
                ],
                [
                    'attribute' => 'more_ten_invoices',
                    'value' => function (CheckingAccountant $model) use ($year) {
                        return CheckingAccountant::find()
                            ->select([
                                'COUNT(invoice.id) as invoiceCount',
                            ])
                            ->andWhere([CheckingAccountant::tableName() . '.bik' => $model->bik])
                            ->joinWith('company')
                            ->joinWith('company.invoices')
                            ->andWhere([Invoice::tableName() . '.type' => Documents::IO_TYPE_OUT])
                            ->andWhere([Invoice::tableName() . '.is_deleted' => Invoice::NOT_IS_DELETED])
                            ->andWhere(['not', [Invoice::tableName() . '.invoice_status_id' => InvoiceStatus::STATUS_REJECTED]])
                            ->andWhere(['between', Invoice::tableName() . '.document_date', "{$year}-01-01", "{$year}-12-31"])
                            ->andWhere([Company::tableName() . '.test' => !Company::TEST_COMPANY])
                            ->andHaving(['>', 'invoiceCount', 10])
                            ->groupBy(CheckingAccountant::tableName() . '.company_id')
                            ->count();
                    },
                ],
                [
                    'attribute' => 'paid_company_count',
                    'value' => function (CheckingAccountant $model) use ($year) {
                        return CheckingAccountant::find()
                            ->andWhere([CheckingAccountant::tableName() . '.bik' => $model->bik])
                            ->joinWith('company')
                            ->joinWith('company.subscribes')
                            ->andWhere(['not', [Subscribe::tableName() . '.tariff_id' => SubscribeTariff::TARIFF_TRIAL]])
                            ->andWhere(['not', [Subscribe::tableName() . '.payment_type_id' => PaymentType::TYPE_PROMO_CODE]])
                            ->andWhere(['between', Subscribe::tableName() . '.activated_at', strtotime("01.01.{$year}"), strtotime("31.12.{$year}")])
                            ->andWhere([Company::tableName() . '.test' => !Company::TEST_COMPANY])
                            ->andWhere(['between', Company::tableName() . '.created_at', strtotime("01.01.{$year}"), strtotime("31.12.{$year}")])
                            ->groupBy(CheckingAccountant::tableName() . '.company_id')
                            ->count();
                    },
                ],
                [
                    'attribute' => 'act_count',
                    'value' => function (CheckingAccountant $model) use ($year) {
                        return CheckingAccountant::find()
                            ->select([
                                'COUNT(act.id) as actCount',
                            ])
                            ->andWhere([CheckingAccountant::tableName() . '.bik' => $model->bik])
                            ->joinWith('company')
                            ->joinWith('company.invoices')
                            ->joinWith('company.invoices.acts')
                            ->andWhere([Invoice::tableName() . '.type' => Documents::IO_TYPE_OUT])
                            ->andWhere([Invoice::tableName() . '.is_deleted' => Invoice::NOT_IS_DELETED])
                            ->andWhere(['not', [Invoice::tableName() . '.invoice_status_id' => InvoiceStatus::STATUS_REJECTED]])
                            ->andWhere(['between', Act::tableName() . '.document_date', "{$year}-01-01", "{$year}-12-31"])
                            ->andWhere([Company::tableName() . '.test' => !Company::TEST_COMPANY])
                            ->andHaving(['>', 'actCount', 0])
                            ->groupBy(CheckingAccountant::tableName() . '.company_id')
                            ->count();
                    },
                ],
                [
                    'attribute' => 'packing_list_count',
                    'value' => function (CheckingAccountant $model) use ($year) {
                        return CheckingAccountant::find()
                            ->select([
                                'COUNT(packing_list.id) as packingListCount',
                            ])
                            ->andWhere([CheckingAccountant::tableName() . '.bik' => $model->bik])
                            ->joinWith('company')
                            ->joinWith('company.invoices')
                            ->joinWith('company.invoices.packingLists')
                            ->andWhere([Invoice::tableName() . '.type' => Documents::IO_TYPE_OUT])
                            ->andWhere([Invoice::tableName() . '.is_deleted' => Invoice::NOT_IS_DELETED])
                            ->andWhere(['not', [Invoice::tableName() . '.invoice_status_id' => InvoiceStatus::STATUS_REJECTED]])
                            ->andWhere(['between', PackingList::tableName() . '.document_date', "{$year}-01-01", "{$year}-12-31"])
                            ->andWhere([Company::tableName() . '.test' => !Company::TEST_COMPANY])
                            ->andHaving(['>', 'packingListCount', 0])
                            ->groupBy(CheckingAccountant::tableName() . '.company_id')
                            ->count();
                    },
                ],
                [
                    'attribute' => 'import_1c',
                    'value' => function (CheckingAccountant $model) use ($year) {
                        return CheckingAccountant::find()
                            ->andWhere([CheckingAccountant::tableName() . '.bik' => $model->bik])
                            ->joinWith('company')
                            ->joinWith('company.cashBankStatementUploads')
                            ->andWhere([Company::tableName() . '.test' => !Company::TEST_COMPANY])
                            ->andWhere(['between', CashBankStatementUpload::tableName() . '.created_at', strtotime("01.01.{$year}"), strtotime("31.12.{$year}")])
                            ->andWhere([CashBankStatementUpload::tableName() . '.source' => CashBankStatementUpload::SOURCE_FILE])
                            ->groupBy(CheckingAccountant::tableName() . '.company_id')
                            ->count();
                    },
                ],
            ],
            'headers' => [
                'banks' => 'Банки',
                'company_count' => 'Количество пользователей',
                'one_invoice' => '1 сч',
                'two_invoices' => '2 сч',
                'five_invoices' => '3-5 сч',
                'ten_invoices' => '6-10 сч',
                'more_ten_invoices' => '>10 сч',
                'paid_company_count' => 'Кол-во пользователей на платном тарифе',
                'act_count' => 'Выставили акт',
                'packing_list_count' => 'Выставили ТН',
                'import_1c' => 'Загрузили выписку 1С',
            ],
            'format' => 'Xlsx',
            'fileName' => "Banks_{$year}.xls",
            'savePath' => __DIR__,
        ]);
    }

    /**
     * @throws \yii\base\Exception
     */
    public function actionRemoveWhiteBackground()
    {
        /* @var $companies Company[] */
        $companies = Company::find()->andWhere(['or',
            ['not', ['print_link' => null]],
            ['not', ['chief_signature_link' => null]],
        ])->all();
        foreach ($companies as $company) {
            $uploadPath = $company->getUploadPath();

            $signaturePath = $uploadPath . DIRECTORY_SEPARATOR . $company->chief_signature_link;
            $printPath = $uploadPath . DIRECTORY_SEPARATOR . $company->print_link;

            if (file_exists($signaturePath) && $company->chief_signature_link) {
                $signaturePathInfo = pathinfo($signaturePath);
                $newSignatureFileName = DIRECTORY_SEPARATOR . $signaturePathInfo['filename'] . '.png';
                $newSignaturePath = $uploadPath . $newSignatureFileName;

                exec("convert \"{$signaturePath}\" -fill none -fuzz 20% -draw \"matte 0,0 floodfill\" -flop  -draw \"matte 0,0 floodfill\" -flop \"{$newSignaturePath}\"");
                exec("convert \"{$newSignaturePath}\" -fuzz 4% -transparent white \"{$newSignaturePath}\"");

                $company->chief_signature_link = $newSignatureFileName;
                echo "Company ID: {$company->id}. Update signature.\r\n";
            }

            if (file_exists($printPath) && $company->print_link) {
                $printPathInfo = pathinfo($printPath);
                $newPrintFileName = DIRECTORY_SEPARATOR . $printPathInfo['filename'] . '.png';
                $newPrintPath = $uploadPath . $newPrintFileName;

                exec("convert \"{$printPath}\" -fill none -fuzz 20% -draw \"matte 0,0 floodfill\" -flop  -draw \"matte 0,0 floodfill\" -flop \"{$newPrintPath}\"");
                exec("convert \"{$newPrintPath}\" -fuzz 4% -transparent white \"{$newPrintPath}\"");
                $company->print_link = $newPrintFileName;
                echo "Company ID: {$company->id}. Update print.\r\n";
            }
            if ($company->save(true, ['chief_signature_link', 'print_link'])) {
                if (isset($signaturePathInfo['extension']) && $signaturePathInfo['extension'] !== 'png' &&
                    file_exists($signaturePath) && !is_dir($signaturePath)) {
                    unlink($signaturePath);
                }
                if (isset($printPathInfo['extension']) && $printPathInfo['extension'] !== 'png' &&
                    file_exists($printPath) && !is_dir($printPath)) {
                    unlink($printPath);
                }
            }
        }
    }

    /**
     *
     */
    public function actionExportCompaniesBySubscribe($type)
    {
        if ($type == 1) {
            $payedCompanies = Company::find()
                ->isTest(!Company::TEST_COMPANY)
                ->isBlocked(Company::UNBLOCKED)
                ->andWhere(['between', Company::tableName() . '.created_at', strtotime("01.01.2018"), strtotime("31.12.2018")])
                ->joinWith('subscribes')
                ->andWhere(['in', 'tariff_id', SubscribeTariff::paidStandartIds()])
                ->andWhere(['not', ['payment_type_id' => PaymentType::TYPE_PROMO_CODE]])
                ->groupBy(Company::tableName() . '.id')
                ->column();
            $companies = Company::find()
                ->andWhere(['in', Company::tableName() . '.id', $payedCompanies])
                ->joinWith('activeSubscribe')
                ->andWhere(['or',
                    ['<', 'expired_at', time()],
                    [Subscribe::tableName() . '.id' => null],
                ])
                ->all();
            $title = 'Перестали платить';
            $fileName = 'StoppedPaying.xls';
        } elseif ($type == 2) {
            $companies = Company::find()
                ->isTest(!Company::TEST_COMPANY)
                ->isBlocked(Company::UNBLOCKED)
                ->andWhere(['between', Company::tableName() . '.created_at', strtotime("01.01.2018"), strtotime("31.12.2018")])
                ->joinWith('paidSubscriptions')
                ->andWhere([Subscribe::tableName() . '.id' => null])
                ->andWhere(['not', ['free_tariff_start_at' => null]])
                ->andWhere(['>', 'last_visit_at', strtotime('- 10 days')])
                ->groupBy(Company::tableName() . '.id')
                ->all();
            $title = 'Не платили и пользуются';
            $fileName = 'NotPayedAndUse.xls';
        } elseif ($type == 3) {
            $companies = Company::find()
                ->select([
                    Company::tableName() . '.*',
                    'SUM(' . Invoice::tableName() . '.id) as invoiceCount',
                ])
                ->isTest(!Company::TEST_COMPANY)
                ->isBlocked(Company::UNBLOCKED)
                ->andWhere(['between', Company::tableName() . '.created_at', strtotime("01.01.2018"), strtotime("31.12.2018")])
                ->joinWith('paidSubscriptions')
                ->joinWith('invoices')
                ->andWhere(['and',
                    [Invoice::tableName() . '.type' => Documents::IO_TYPE_OUT],
                    [Invoice::tableName() . '.is_deleted' => false],
                ])
                ->andWhere([Subscribe::tableName() . '.id' => null])
                ->andWhere(['not', ['free_tariff_start_at' => null]])
                ->andWhere(['<', 'last_visit_at', strtotime('- 3 month')])
                ->andHaving(['>', 'invoiceCount', 4])
                ->groupBy(Company::tableName() . '.id')
                ->all();
            $title = 'Не платили и ушли';
            $fileName = 'NotPayedAndLeave.xls';
        } else {
            throw new \Exception('Invalid type!');
        }

        $excel = new Excel();
        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $companies,
            'title' => $title,
            'columns' => [
                [
                    'attribute' => 'name',
                    'value' => function (Company $model) {
                        return $model->getShortName();
                    },
                ],
                [
                    'attribute' => 'lastPaymentDate',
                    'value' => function (Company $model) {
                        $lastPayment = $model->getLastPayment()->andWhere(['payment_for' => Payment::FOR_SUBSCRIBE])->one();

                        return $lastPayment ?
                            date(DateHelper::FORMAT_USER_DATE, $lastPayment->payment_date) :
                            '-';
                    },
                ],
                [
                    'attribute' => 'lastPaymentSum',
                    'value' => function (Company $model) {
                        $lastPayment = $model->getLastPayment()->andWhere(['payment_for' => Payment::FOR_SUBSCRIBE])->one();

                        return $lastPayment ? $lastPayment->sum : '-';
                    },
                ],
                [
                    'attribute' => 'lastPaymentExpiredAt',
                    'value' => function (Company $model) {
                        $lastPayment = $model->getLastPayment()->andWhere(['payment_for' => Payment::FOR_SUBSCRIBE])->one();

                        return $lastPayment ?
                            ($lastPayment->subscribe ?
                                date(DateHelper::FORMAT_USER_DATE, $lastPayment->subscribe->expired_at) : '-') : '-';
                    },
                ],
                [
                    'attribute' => 'lastInvoiceDate',
                    'value' => function (Company $model) {
                        $lastInvoiceDocumentDate = $model->getInvoices()
                            ->andWhere(['and',
                                ['type' => Documents::IO_TYPE_OUT],
                                ['is_deleted' => false],
                            ])
                            ->max('document_date');

                        return $lastInvoiceDocumentDate ? DateHelper::format($lastInvoiceDocumentDate, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) : '-';
                    },
                ],
                [
                    'attribute' => 'invoiceCount',
                    'value' => function (Company $model) {
                        return $model->getInvoices()
                            ->andWhere(['and',
                                ['type' => Documents::IO_TYPE_OUT],
                                ['is_deleted' => false],
                            ])->count();
                    },
                ],
                [
                    'attribute' => 'last_visit_at',
                    'value' => function (Company $model) {
                        return date(DateHelper::FORMAT_USER_DATE, $model->last_visit_at);
                    },
                ],
                [
                    'attribute' => 'phone',
                    'value' => function (Company $model) {
                        return $model->phone;
                    },
                ],
                [
                    'attribute' => 'email',
                    'value' => function (Company $model) {
                        return $model->email;
                    },
                ],
                [
                    'attribute' => 'fio',
                    'value' => function (Company $model) {
                        return $model->getChiefFio(true);
                    },
                ],
            ],
            'headers' => [
                'name' => 'Компания',
                'lastPaymentDate' => 'Дата последней оплаты',
                'lastPaymentSum' => 'Сумма последней оплаты',
                'lastPaymentExpiredAt' => 'Дата окончания платной подписки',
                'lastInvoiceDate' => 'Дата последнего созданного счета в КУБе',
                'invoiceCount' => 'Количество созданных счетов',
                'last_visit_at' => 'Дата последнего посещения',
                'phone' => 'Телефон',
                'email' => 'E-mail',
                'fio' => 'ФИО',
            ],
            'format' => 'Xlsx',
            'fileName' => $fileName,
            'savePath' => __DIR__,
        ]);
    }

    /**
     * @param $type
     * @throws \Exception
     */
    public function actionExportCompaniesByBank($type)
    {
        switch ($type) {
            case 1:
                $bik = '044525974';
                break;
            case 2:
                $bik = '044525999';
                break;
            case 3:
                $bik = ['044525092', '045004864'];
                break;
            case 4:
                $bik = '044525593';
                break;
            case 5:
                $bik = '044525297';
                break;
            default:
                throw new \Exception('Invalid type.');
        }
        $companies = Company::find()
            ->isTest(!Company::TEST_COMPANY)
            ->isBlocked(Company::UNBLOCKED)
            ->andWhere(['between', Company::tableName() . '.created_at', strtotime("01.01.2018"), strtotime("31.12.2018")])
            ->joinWith('checkingAccountants')
            ->andWhere([CheckingAccountant::tableName() . '.bik' => $bik])
            ->all();

        if (is_array($bik)) {
            $bikName = implode(', ', $bik);
        } else {
            $bikName = $bik;
        }
        $excel = new Excel();
        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $companies,
            'title' => $bikName,
            'columns' => [
                [
                    'attribute' => 'name',
                    'value' => function (Company $model) {
                        return $model->getShortName();
                    },
                ],
                [
                    'attribute' => 'created_at',
                    'value' => function (Company $model) {
                        return date(DateHelper::FORMAT_USER_DATE, $model->created_at);
                    },
                ],
                [
                    'attribute' => 'lastPaymentDate',
                    'value' => function (Company $model) {
                        $lastPayment = $model->getLastPayment()->andWhere(['payment_for' => Payment::FOR_SUBSCRIBE])->one();

                        return $lastPayment ?
                            date(DateHelper::FORMAT_USER_DATE, $lastPayment->payment_date) :
                            '-';
                    },
                ],
                [
                    'attribute' => 'lastInvoiceDate',
                    'value' => function (Company $model) {
                        $lastInvoiceDocumentDate = $model->getInvoices()
                            ->andWhere(['and',
                                ['type' => Documents::IO_TYPE_OUT],
                                ['is_deleted' => false],
                            ])
                            ->max('document_date');

                        return $lastInvoiceDocumentDate ? DateHelper::format($lastInvoiceDocumentDate, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) : '-';
                    },
                ],
                [
                    'attribute' => 'outInvoiceCount',
                    'value' => function (Company $model) {
                        return $model->getInvoices()
                            ->andWhere(['and',
                                ['type' => Documents::IO_TYPE_OUT],
                                ['is_deleted' => false],
                            ])->count();
                    },
                ],
                [
                    'attribute' => 'inInvoiceCount',
                    'value' => function (Company $model) {
                        return $model->getInvoices()
                            ->andWhere(['and',
                                ['type' => Documents::IO_TYPE_IN],
                                ['is_deleted' => false],
                            ])->count();
                    },
                ],
                [
                    'attribute' => 'last_visit_at',
                    'value' => function (Company $model) {
                        return date(DateHelper::FORMAT_USER_DATE, $model->last_visit_at);
                    },
                ],
                [
                    'attribute' => 'email',
                    'value' => function (Company $model) {
                        return $model->email;
                    },
                ],
                [
                    'attribute' => 'fio',
                    'value' => function (Company $model) {
                        return $model->getChiefFio(true);
                    },
                ],
            ],
            'headers' => [
                'name' => 'Компания',
                'created_at' => 'Дата регистрации',
                'lastPaymentDate' => 'Дата оплаты',
                'lastInvoiceDate' => 'Дата последнего созданного счета',
                'outInvoiceCount' => 'Количество исх.счетов',
                'inInvoiceCount' => 'Количество вх.счетов',
                'last_visit_at' => 'Дата последнего посещения',
                'email' => 'E-mail',
                'fio' => 'ФИО',
            ],
            'format' => 'Xlsx',
            'fileName' => 'Companies ' . $bikName,
            'savePath' => __DIR__,
        ]);
    }

    /**
     *
     */
    public function actionUpdateInvitedCompaniesCount()
    {
        /* @var $company \common\models\Company */
        foreach (Company::find()->all() as $company) {
            $invitedCompaniesCount = $company->getInvitedCompanies()->count();
            if ($company->invited_companies_count == 0 && $invitedCompaniesCount == 0) {
                continue;
            }
            $company->invited_companies_count = $invitedCompaniesCount;
            if ($company->save(true, ['invited_companies_count'])) {
                echo "Company - {$company->id} updated. \r\n";
            } else {
                echo "Error in company save - {$company->id}. \r\n";
            }
        }
    }

    /**
     *
     */
    public function actionExportLeaveCompanies()
    {
        $payedCompanies = Company::find()
            ->isTest(!Company::TEST_COMPANY)
            ->isBlocked(Company::UNBLOCKED)
            ->andWhere(['between', Company::tableName() . '.created_at', strtotime("01.01.2017"), strtotime("31.12.2018")])
            ->joinWith('subscribes')
            ->andWhere(['in', 'tariff_id', SubscribeTariff::paidStandartIds()])
            ->andWhere(['not', ['payment_type_id' => PaymentType::TYPE_PROMO_CODE]])
            ->groupBy(Company::tableName() . '.id')
            ->column();
        $companies = Company::find()
            ->andWhere(['in', Company::tableName() . '.id', $payedCompanies])
            ->joinWith('activeSubscribe')
            ->andWhere(['or',
                ['<', 'expired_at', time()],
                [Subscribe::tableName() . '.id' => null],
            ])
            ->all();

        $excel = new Excel();
        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $companies,
            'title' => 'Перестали платить',
            'rangeHeader' => array_merge(range('A', 'Z'), ['AA', 'AB', 'AC', 'AD', 'AE', 'AF', 'AG', 'AH',
                'AI', 'AJ', 'AK', 'AL', 'AM', 'AN', 'AO', 'AP', 'AQ', 'AR', 'AS', 'AT', 'AU', 'AV',]),
            'columns' => [
                [
                    'attribute' => 'name',
                    'value' => function (Company $model) {
                        return $model->getShortName();
                    },
                ],
                [
                    'attribute' => 'created_at',
                    'value' => function (Company $model) {
                        return date(DateHelper::FORMAT_USER_DATE, $model->created_at);
                    },
                ],
                [
                    'attribute' => 'first_payment_date',
                    'value' => function (Company $model) {
                        return $model->firstPayment ?
                            date(DateHelper::FORMAT_USER_DATE, $model->firstPayment->payment_date) :
                            null;
                    },
                ],
                [
                    'attribute' => 'total_payment_count',
                    'value' => function (Company $model) {
                        return $model->getPayments()
                            ->andWhere(['and',
                                ['in', 'tariff_id', SubscribeTariff::paidStandartIds()],
                                ['promo_code_id' => null],
                                ['is_confirmed' => true],
                            ])->count();
                    },
                ],
                [
                    'attribute' => 'total_payment_amount',
                    'value' => function (Company $model) {
                        return $model->getPayments()
                            ->andWhere(['and',
                                ['in', 'tariff_id', SubscribeTariff::paidStandartIds()],
                                ['promo_code_id' => null],
                                ['is_confirmed' => true],
                            ])->sum('sum');
                    },
                ],
                [
                    'attribute' => 'last_payment_date',
                    'value' => function (Company $model) {
                        return $model->lastPayment ?
                            date(DateHelper::FORMAT_USER_DATE, $model->lastPayment->payment_date) :
                            null;
                    },
                ],
                [
                    'attribute' => 'last_visit_at',
                    'value' => function (Company $model) {
                        return date(DateHelper::FORMAT_USER_DATE, $model->last_visit_at);
                    },
                ],
                [
                    'attribute' => 'city',
                    'value' => function (Company $model) {
                        return $model->address_legal;
                    },
                ],
                [
                    'attribute' => 'specialization',
                    'value' => function (Company $model) {
                        return $model->serviceMoreStock ?
                            ServiceMoreStock::$specializationArray[$model->serviceMoreStock->specialization] :
                            null;
                    },
                ],
                [
                    'attribute' => 'employee_count',
                    'value' => function (Company $model) {
                        return $model->getEmployeeCompanies()
                            ->andWhere(['is_working' => Employee::STATUS_IS_WORKING])
                            ->count();
                    },
                ],
                [
                    'attribute' => 'employee_accountant_fio',
                    'value' => function (Company $model) {
                        return $model->getChiefAccountantFio(true);
                    },
                ],
                [
                    'attribute' => 'product_count',
                    'value' => function (Company $model) {
                        return $model->getProducts()
                            ->andWhere(['production_type' => Product::PRODUCTION_TYPE_GOODS])
                            ->andWhere(['not', [Product::tableName() . '.status' => Product::DELETED]])
                            ->andWhere([Product::tableName() . '.is_deleted' => false])
                            ->count();
                    },
                ],
                [
                    'attribute' => 'service_count',
                    'value' => function (Company $model) {
                        return $model->getProducts()
                            ->andWhere(['production_type' => Product::PRODUCTION_TYPE_SERVICE])
                            ->andWhere(['not', [Product::tableName() . '.status' => Product::DELETED]])
                            ->andWhere([Product::tableName() . '.is_deleted' => false])
                            ->count();
                    },
                ],
                [
                    'attribute' => 'logo_print_signature',
                    'value' => function (Company $model) {
                        $result = null;
                        if ($model->logo_link) {
                            $result .= '1/';
                        } else {
                            $result .= '0/';
                        }
                        if ($model->print_link) {
                            $result .= '1/';
                        } else {
                            $result .= '0/';
                        }
                        if ($model->chief_signature_link) {
                            $result .= '1';
                        } else {
                            $result .= '0';
                        }

                        return $result;
                    },
                ],
                [
                    'attribute' => 'phone',
                    'value' => function (Company $model) {
                        return $model->phone;
                    },
                ],
                [
                    'attribute' => 'email',
                    'value' => function (Company $model) {
                        return $model->email;
                    },
                ],
                [
                    'attribute' => 'email',
                    'value' => function (Company $model) {
                        return $model->email;
                    },
                ],
                [
                    'attribute' => 'employee_chief_fio',
                    'value' => function (Company $model) {
                        return $model->getChiefFio();
                    },
                ],
                [
                    'attribute' => 'invoice_statistic_1_2017',
                    'value' => function (Company $model) {
                        return $model->getInvoices()
                            ->andWhere(['type' => Documents::IO_TYPE_OUT])
                            ->andWhere(['is_deleted' => Invoice::NOT_IS_DELETED])
                            ->andWhere(['between', 'document_date', '2017-01-01', '2017-01-' . cal_days_in_month(CAL_GREGORIAN, '01', 2017)])
                            ->count('id');
                    },
                ],
                [
                    'attribute' => 'invoice_statistic_2_2017',
                    'value' => function (Company $model) {
                        return $model->getInvoices()
                            ->andWhere(['type' => Documents::IO_TYPE_OUT])
                            ->andWhere(['is_deleted' => Invoice::NOT_IS_DELETED])
                            ->andWhere(['between', 'document_date', '2017-02-01', '2017-02-' . cal_days_in_month(CAL_GREGORIAN, '02', 2017)])
                            ->count('id');
                    },
                ],
                [
                    'attribute' => 'invoice_statistic_3_2017',
                    'value' => function (Company $model) {
                        return $model->getInvoices()
                            ->andWhere(['type' => Documents::IO_TYPE_OUT])
                            ->andWhere(['is_deleted' => Invoice::NOT_IS_DELETED])
                            ->andWhere(['between', 'document_date', '2017-03-01', '2017-03-' . cal_days_in_month(CAL_GREGORIAN, '03', 2017)])
                            ->count('id');
                    },
                ],
                [
                    'attribute' => 'invoice_statistic_4_2017',
                    'value' => function (Company $model) {
                        return $model->getInvoices()
                            ->andWhere(['type' => Documents::IO_TYPE_OUT])
                            ->andWhere(['is_deleted' => Invoice::NOT_IS_DELETED])
                            ->andWhere(['between', 'document_date', '2017-04-01', '2017-04-' . cal_days_in_month(CAL_GREGORIAN, '04', 2017)])
                            ->count('id');
                    },
                ],
                [
                    'attribute' => 'invoice_statistic_5_2017',
                    'value' => function (Company $model) {
                        return $model->getInvoices()
                            ->andWhere(['type' => Documents::IO_TYPE_OUT])
                            ->andWhere(['is_deleted' => Invoice::NOT_IS_DELETED])
                            ->andWhere(['between', 'document_date', '2017-05-01', '2017-05-' . cal_days_in_month(CAL_GREGORIAN, '05', 2017)])
                            ->count('id');
                    },
                ],
                [
                    'attribute' => 'invoice_statistic_6_2017',
                    'value' => function (Company $model) {
                        return $model->getInvoices()
                            ->andWhere(['type' => Documents::IO_TYPE_OUT])
                            ->andWhere(['is_deleted' => Invoice::NOT_IS_DELETED])
                            ->andWhere(['between', 'document_date', '2017-06-01', '2017-06-' . cal_days_in_month(CAL_GREGORIAN, '06', 2017)])
                            ->count('id');
                    },
                ],
                [
                    'attribute' => 'invoice_statistic_7_2017',
                    'value' => function (Company $model) {
                        return $model->getInvoices()
                            ->andWhere(['type' => Documents::IO_TYPE_OUT])
                            ->andWhere(['is_deleted' => Invoice::NOT_IS_DELETED])
                            ->andWhere(['between', 'document_date', '2017-07-01', '2017-07-' . cal_days_in_month(CAL_GREGORIAN, '07', 2017)])
                            ->count('id');
                    },
                ],
                [
                    'attribute' => 'invoice_statistic_8_2017',
                    'value' => function (Company $model) {
                        return $model->getInvoices()
                            ->andWhere(['type' => Documents::IO_TYPE_OUT])
                            ->andWhere(['is_deleted' => Invoice::NOT_IS_DELETED])
                            ->andWhere(['between', 'document_date', '2017-08-01', '2017-08-' . cal_days_in_month(CAL_GREGORIAN, '08', 2017)])
                            ->count('id');
                    },
                ],
                [
                    'attribute' => 'invoice_statistic_9_2017',
                    'value' => function (Company $model) {
                        return $model->getInvoices()
                            ->andWhere(['type' => Documents::IO_TYPE_OUT])
                            ->andWhere(['is_deleted' => Invoice::NOT_IS_DELETED])
                            ->andWhere(['between', 'document_date', '2017-09-01', '2017-09-' . cal_days_in_month(CAL_GREGORIAN, '09', 2017)])
                            ->count('id');
                    },
                ],
                [
                    'attribute' => 'invoice_statistic_10_2017',
                    'value' => function (Company $model) {
                        return $model->getInvoices()
                            ->andWhere(['type' => Documents::IO_TYPE_OUT])
                            ->andWhere(['is_deleted' => Invoice::NOT_IS_DELETED])
                            ->andWhere(['between', 'document_date', '2017-10-01', '2017-10-' . cal_days_in_month(CAL_GREGORIAN, '10', 2017)])
                            ->count('id');
                    },
                ],
                [
                    'attribute' => 'invoice_statistic_11_2017',
                    'value' => function (Company $model) {
                        return $model->getInvoices()
                            ->andWhere(['type' => Documents::IO_TYPE_OUT])
                            ->andWhere(['is_deleted' => Invoice::NOT_IS_DELETED])
                            ->andWhere(['between', 'document_date', '2017-11-01', '2017-11-' . cal_days_in_month(CAL_GREGORIAN, '11', 2017)])
                            ->count('id');
                    },
                ],
                [
                    'attribute' => 'invoice_statistic_12_2017',
                    'value' => function (Company $model) {
                        return $model->getInvoices()
                            ->andWhere(['type' => Documents::IO_TYPE_OUT])
                            ->andWhere(['is_deleted' => Invoice::NOT_IS_DELETED])
                            ->andWhere(['between', 'document_date', '2017-12-01', '2017-12-' . cal_days_in_month(CAL_GREGORIAN, '12', 2017)])
                            ->count('id');
                    },
                ],
                [
                    'attribute' => 'total_invoice_statistic_count_2017',
                    'value' => function (Company $model) {
                        return $model->getInvoices()
                            ->andWhere(['type' => Documents::IO_TYPE_OUT])
                            ->andWhere(['is_deleted' => Invoice::NOT_IS_DELETED])
                            ->andWhere(['between', 'document_date', '2017-01-01', '2017-12-' . cal_days_in_month(CAL_GREGORIAN, '12', 2017)])
                            ->count('id');
                    },
                ],
                [
                    'attribute' => 'total_invoice_statistic_amount_2017',
                    'value' => function (Company $model) {
                        return TextHelper::invoiceMoneyFormat($model->getInvoices()
                            ->andWhere(['type' => Documents::IO_TYPE_OUT])
                            ->andWhere(['is_deleted' => Invoice::NOT_IS_DELETED])
                            ->andWhere(['between', 'document_date', '2017-01-01', '2017-12-' . cal_days_in_month(CAL_GREGORIAN, '12', 2017)])
                            ->sum('total_amount_with_nds'), 2);
                    },
                ],
                [
                    'attribute' => 'total_invoice_statistic_average_count_2017',
                    'value' => function (Company $model) {
                        $hasInvoicesMonthCount = 0;
                        $totalCount = $model->getInvoices()
                            ->andWhere(['type' => Documents::IO_TYPE_OUT])
                            ->andWhere(['is_deleted' => Invoice::NOT_IS_DELETED])
                            ->andWhere(['between', 'document_date', '2017-01-01', '2017-12-' . cal_days_in_month(CAL_GREGORIAN, '12', 2017)])
                            ->count('id');
                        foreach (range(1, 12) as $item) {
                            $monthNumber = $item > 9 ? $item : ('0' . $item);
                            $invoiceCount = $model->getInvoices()
                                ->andWhere(['type' => Documents::IO_TYPE_OUT])
                                ->andWhere(['is_deleted' => Invoice::NOT_IS_DELETED])
                                ->andWhere(['between', 'document_date', '2017-' . $monthNumber . '-01', '2017-' . $monthNumber . '-' . cal_days_in_month(CAL_GREGORIAN, $monthNumber, 2017)])
                                ->count('id');
                            if ($invoiceCount > 0) {
                                $hasInvoicesMonthCount += 1;
                            }
                        }

                        return $hasInvoicesMonthCount ? round($totalCount / $hasInvoicesMonthCount, 2) : 0;
                    },
                ],
                [
                    'attribute' => 'invoice_statistic_1_2018',
                    'value' => function (Company $model) {
                        return $model->getInvoices()
                            ->andWhere(['type' => Documents::IO_TYPE_OUT])
                            ->andWhere(['is_deleted' => Invoice::NOT_IS_DELETED])
                            ->andWhere(['between', 'document_date', '2018-01-01', '2018-01-' . cal_days_in_month(CAL_GREGORIAN, '01', 2018)])
                            ->count('id');
                    },
                ],
                [
                    'attribute' => 'invoice_statistic_2_2018',
                    'value' => function (Company $model) {
                        return $model->getInvoices()
                            ->andWhere(['type' => Documents::IO_TYPE_OUT])
                            ->andWhere(['is_deleted' => Invoice::NOT_IS_DELETED])
                            ->andWhere(['between', 'document_date', '2018-02-01', '2018-02-' . cal_days_in_month(CAL_GREGORIAN, '02', 2018)])
                            ->count('id');
                    },
                ],
                [
                    'attribute' => 'invoice_statistic_3_2018',
                    'value' => function (Company $model) {
                        return $model->getInvoices()
                            ->andWhere(['type' => Documents::IO_TYPE_OUT])
                            ->andWhere(['is_deleted' => Invoice::NOT_IS_DELETED])
                            ->andWhere(['between', 'document_date', '2018-03-01', '2018-03-' . cal_days_in_month(CAL_GREGORIAN, '03', 2018)])
                            ->count('id');
                    },
                ],
                [
                    'attribute' => 'invoice_statistic_4_2018',
                    'value' => function (Company $model) {
                        return $model->getInvoices()
                            ->andWhere(['type' => Documents::IO_TYPE_OUT])
                            ->andWhere(['is_deleted' => Invoice::NOT_IS_DELETED])
                            ->andWhere(['between', 'document_date', '2018-04-01', '2018-04-' . cal_days_in_month(CAL_GREGORIAN, '04', 2018)])
                            ->count('id');
                    },
                ],
                [
                    'attribute' => 'invoice_statistic_5_2018',
                    'value' => function (Company $model) {
                        return $model->getInvoices()
                            ->andWhere(['type' => Documents::IO_TYPE_OUT])
                            ->andWhere(['is_deleted' => Invoice::NOT_IS_DELETED])
                            ->andWhere(['between', 'document_date', '2018-05-01', '2018-05-' . cal_days_in_month(CAL_GREGORIAN, '05', 2018)])
                            ->count('id');
                    },
                ],
                [
                    'attribute' => 'invoice_statistic_6_2017',
                    'value' => function (Company $model) {
                        return $model->getInvoices()
                            ->andWhere(['type' => Documents::IO_TYPE_OUT])
                            ->andWhere(['is_deleted' => Invoice::NOT_IS_DELETED])
                            ->andWhere(['between', 'document_date', '2017-06-01', '2017-06-' . cal_days_in_month(CAL_GREGORIAN, '06', 2017)])
                            ->count('id');
                    },
                ],
                [
                    'attribute' => 'invoice_statistic_7_2018',
                    'value' => function (Company $model) {
                        return $model->getInvoices()
                            ->andWhere(['type' => Documents::IO_TYPE_OUT])
                            ->andWhere(['is_deleted' => Invoice::NOT_IS_DELETED])
                            ->andWhere(['between', 'document_date', '2018-07-01', '2018-07-' . cal_days_in_month(CAL_GREGORIAN, '07', 2018)])
                            ->count('id');
                    },
                ],
                [
                    'attribute' => 'invoice_statistic_8_2018',
                    'value' => function (Company $model) {
                        return $model->getInvoices()
                            ->andWhere(['type' => Documents::IO_TYPE_OUT])
                            ->andWhere(['is_deleted' => Invoice::NOT_IS_DELETED])
                            ->andWhere(['between', 'document_date', '2018-08-01', '2018-08-' . cal_days_in_month(CAL_GREGORIAN, '08', 2018)])
                            ->count('id');
                    },
                ],
                [
                    'attribute' => 'invoice_statistic_9_2018',
                    'value' => function (Company $model) {
                        return $model->getInvoices()
                            ->andWhere(['type' => Documents::IO_TYPE_OUT])
                            ->andWhere(['is_deleted' => Invoice::NOT_IS_DELETED])
                            ->andWhere(['between', 'document_date', '2018-09-01', '2018-09-' . cal_days_in_month(CAL_GREGORIAN, '09', 2018)])
                            ->count('id');
                    },
                ],
                [
                    'attribute' => 'invoice_statistic_10_2018',
                    'value' => function (Company $model) {
                        return $model->getInvoices()
                            ->andWhere(['type' => Documents::IO_TYPE_OUT])
                            ->andWhere(['is_deleted' => Invoice::NOT_IS_DELETED])
                            ->andWhere(['between', 'document_date', '2018-10-01', '2018-10-' . cal_days_in_month(CAL_GREGORIAN, '10', 2018)])
                            ->count('id');
                    },
                ],
                [
                    'attribute' => 'invoice_statistic_11_2018',
                    'value' => function (Company $model) {
                        return $model->getInvoices()
                            ->andWhere(['type' => Documents::IO_TYPE_OUT])
                            ->andWhere(['is_deleted' => Invoice::NOT_IS_DELETED])
                            ->andWhere(['between', 'document_date', '2018-11-01', '2018-11-' . cal_days_in_month(CAL_GREGORIAN, '11', 2018)])
                            ->count('id');
                    },
                ],
                [
                    'attribute' => 'invoice_statistic_12_2018',
                    'value' => function (Company $model) {
                        return $model->getInvoices()
                            ->andWhere(['type' => Documents::IO_TYPE_OUT])
                            ->andWhere(['is_deleted' => Invoice::NOT_IS_DELETED])
                            ->andWhere(['between', 'document_date', '2018-12-01', '2018-12-' . cal_days_in_month(CAL_GREGORIAN, '12', 2018)])
                            ->count('id');
                    },
                ],
                [
                    'attribute' => 'total_invoice_statistic_count_2018',
                    'value' => function (Company $model) {
                        return $model->getInvoices()
                            ->andWhere(['type' => Documents::IO_TYPE_OUT])
                            ->andWhere(['is_deleted' => Invoice::NOT_IS_DELETED])
                            ->andWhere(['between', 'document_date', '2018-01-01', '2018-12-' . cal_days_in_month(CAL_GREGORIAN, '12', 2018)])
                            ->count('id');
                    },
                ],
                [
                    'attribute' => 'total_invoice_statistic_amount_2018',
                    'value' => function (Company $model) {
                        return TextHelper::invoiceMoneyFormat($model->getInvoices()
                            ->andWhere(['type' => Documents::IO_TYPE_OUT])
                            ->andWhere(['is_deleted' => Invoice::NOT_IS_DELETED])
                            ->andWhere(['between', 'document_date', '2018-01-01', '2018-12-' . cal_days_in_month(CAL_GREGORIAN, '12', 2018)])
                            ->sum('total_amount_with_nds'), 2);
                    },
                ],
                [
                    'attribute' => 'total_invoice_statistic_average_count_2018',
                    'value' => function (Company $model) {
                        $hasInvoicesMonthCount = 0;
                        $totalCount = $model->getInvoices()
                            ->andWhere(['type' => Documents::IO_TYPE_OUT])
                            ->andWhere(['is_deleted' => Invoice::NOT_IS_DELETED])
                            ->andWhere(['between', 'document_date', '2018-01-01', '2018-12-' . cal_days_in_month(CAL_GREGORIAN, '12', 2018)])
                            ->count('id');
                        foreach (range(1, 12) as $item) {
                            $monthNumber = $item > 9 ? $item : ('0' . $item);
                            $invoiceCount = $model->getInvoices()
                                ->andWhere(['type' => Documents::IO_TYPE_OUT])
                                ->andWhere(['is_deleted' => Invoice::NOT_IS_DELETED])
                                ->andWhere(['between', 'document_date', '2018-' . $monthNumber . '-01', '2018-' . $monthNumber . '-' . cal_days_in_month(CAL_GREGORIAN, $monthNumber, 2018)])
                                ->count('id');
                            if ($invoiceCount > 0) {
                                $hasInvoicesMonthCount += 1;
                            }
                        }

                        return $hasInvoicesMonthCount ? round($totalCount / $hasInvoicesMonthCount, 2) : 0;
                    },
                ],
            ],
            'headers' => [
                'name' => 'Название компании',
                'created_at' => 'Дата регистрации',
                'first_payment_date' => 'Дата 1 оплаты',
                'total_payment_count' => 'Всего оплат. Кол-во',
                'total_payment_amount' => 'Сумма оплат',
                'last_payment_date' => 'Дата последней оплаты',
                'last_visit_at' => 'Дата последнего входа',
                'city' => 'Город',
                'specialization' => 'Отрасль',
                'employee_count' => 'Кол-во сотрудников',
                'employee_accountant_fio' => 'Сотрудник с ролью Бухгалтер',
                'product_count' => 'Кол-во товаров',
                'service_count' => 'Кол-во услуг',
                'logo_print_signature' => 'Лого/печать/подпись',
                'phone' => 'Телефон',
                'email' => 'E-mail',
                'employee_chief_fio' => 'ФИО руководителя полностью',
                'invoice_statistic_1_2017' => 'янв. 2017',
                'invoice_statistic_2_2017' => 'февр. 2017',
                'invoice_statistic_3_2017' => 'март 2017',
                'invoice_statistic_4_2017' => 'апр. 2017',
                'invoice_statistic_5_2017' => 'май 2017',
                'invoice_statistic_6_2017' => 'июнь 2017',
                'invoice_statistic_7_2017' => 'июль 2017',
                'invoice_statistic_8_2017' => 'авг. 2017',
                'invoice_statistic_9_2017' => 'сент. 2017',
                'invoice_statistic_10_2017' => 'окт. 2017',
                'invoice_statistic_11_2017' => 'нояб. 2017',
                'invoice_statistic_12_2017' => 'дек. 2017',
                'total_invoice_statistic_count_2017' => 'Сумма счетов 2017',
                'total_invoice_statistic_amount_2017' => 'Сумма руб. 2017',
                'total_invoice_statistic_average_count_2017' => 'Сред. знач. 2017',
                'invoice_statistic_1_2018' => 'янв. 2018',
                'invoice_statistic_2_2018' => 'февр. 2018',
                'invoice_statistic_3_2018' => 'март 2018',
                'invoice_statistic_4_2018' => 'апр. 2018',
                'invoice_statistic_5_2018' => 'май 2018',
                'invoice_statistic_6_2018' => 'июнь 2018',
                'invoice_statistic_7_2018' => 'июль 2018',
                'invoice_statistic_8_2018' => 'авг. 2018',
                'invoice_statistic_9_2018' => 'сент. 2018',
                'invoice_statistic_10_2018' => 'окт. 2018',
                'invoice_statistic_11_2018' => 'нояб. 2018',
                'invoice_statistic_12_2018' => 'дек. 2018',
                'total_invoice_statistic_count_2018' => 'Сумма счетов 2018',
                'total_invoice_statistic_amount_2018' => 'Сумма руб. 2018',
                'total_invoice_statistic_average_count_2018' => 'Сред. знач. 2018',
            ],
            'format' => 'Xlsx',
            'fileName' => 'Leave Companies',
            'savePath' => __DIR__,
        ]);
    }

    /**
     * @throws \yii\db\Exception
     */
    public function actionUpdateCompanyPaymentsSerialNumber()
    {
        /* @var $company Company */
        foreach (Company::find()->joinWith('payments')
                     ->isTest(!Company::TEST_COMPANY)
                     ->isBlocked(Company::UNBLOCKED)
                     ->andWhere(['and',
                         [Payment::tableName() . '.is_confirmed' => 1],
                         ['not', [Payment::tableName() . '.type_id' => PaymentType::TYPE_PROMO_CODE]],
                         [
                             'or',
                             ['in', Payment::tableName() . '.tariff_id', SubscribeTariff::paidStandartIds()],
                             ['payment_for' => Payment::FOR_TAXROBOT],
                             ['payment_for' => Payment::FOR_STORE_CABINET],
                             ['payment_for' => Payment::FOR_OUT_INVOICE],
                             ['payment_for' => Payment::FOR_ODDS],
                             ['payment_for' => Payment::FOR_ADD_INVOICE],
                         ],
                     ])
                     ->andWhere(['not', [Payment::tableName() . '.id' => null]])
                     ->each() as $company) {
            /* @var $payment Payment */
            foreach ($company->getPayments()
                         ->andWhere(['and',
                             [Payment::tableName() . '.is_confirmed' => 1],
                             ['not', [Payment::tableName() . '.type_id' => PaymentType::TYPE_PROMO_CODE]],
                             [
                                 'or',
                                 ['in', Payment::tableName() . '.tariff_id', SubscribeTariff::paidStandartIds()],
                                 ['payment_for' => Payment::FOR_TAXROBOT],
                                 ['payment_for' => Payment::FOR_STORE_CABINET],
                                 ['payment_for' => Payment::FOR_OUT_INVOICE],
                                 ['payment_for' => Payment::FOR_ODDS],
                                 ['payment_for' => Payment::FOR_ADD_INVOICE],
                             ],
                         ])
                         ->orderBy(['created_at' => SORT_ASC])->all() as $key => $payment) {
                \Yii::$app->db->createCommand()->update('service_payment', ['serial_number' => $key + 1], ['id' => $payment->id])->execute();
            }
        }
    }

    /**
     *
     */
    public function actionExportByActivities()
    {
        $activities = ['Транспортные услуги', 'Аренда транспорта', 'Аренда спецтехники(транспорт, инструменты)'];

        $companies = Company::find()
            ->isBlocked(Company::UNBLOCKED)
            ->isTest(!Company::TEST_COMPANY)
            ->joinWith('activities')
            ->andWhere([Activities::tableName() . '.name' => $activities])
            ->all();

        $excel = new Excel();
        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $companies,
            'title' => 'Компании',
            'columns' => [
                [
                    'attribute' => 'name',
                    'value' => function (Company $model) {
                        return $model->getShortName();
                    },
                ],
                [
                    'attribute' => 'email',
                    'value' => function (Company $model) {
                        return $model->email;
                    },
                ],
                [
                    'attribute' => 'phone',
                    'value' => function (Company $model) {
                        return $model->phone;
                    },
                ],
                [
                    'attribute' => 'chiefFio',
                    'value' => function (Company $model) {
                        return $model->getChiefFio(true);
                    },
                ],
                [
                    'attribute' => 'created_at',
                    'value' => function (Company $model) {
                        return date(DateHelper::FORMAT_USER_DATE, $model->created_at);
                    },
                ],
                [
                    'attribute' => 'activeTariff',
                    'value' => function (Company $model) {
                        $activeSubscribe = $model->getActiveSubscribe()
                            ->andWhere(['tariff_id' => SubscribeTariff::paidStandartIds()])
                            ->exists();

                        return $activeSubscribe ? 'Платный' : 'Бесплатный';
                    },
                ],
                [
                    'attribute' => 'invoiceCountThisYear',
                    'value' => function (Company $model) {
                        return $model->getInvoices()
                            ->andWhere(['type' => Documents::IO_TYPE_OUT])
                            ->andWhere(['is_deleted' => 0])
                            ->andWhere(['between', Invoice::tableName() . '.document_date', '2018-01-01', '2018-12-31'])
                            ->count();

                    },
                ],
                [
                    'attribute' => 'lastInvoiceDate',
                    'value' => function (Company $model) {
                        $lastInvoiceDate = $model->getInvoices()
                            ->andWhere(['type' => Documents::IO_TYPE_OUT])
                            ->andWhere(['is_deleted' => 0])
                            ->max('document_date');

                        return $lastInvoiceDate ?
                            DateHelper::format($lastInvoiceDate, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) :
                            '';
                    },
                ],
                [
                    'attribute' => 'last_visit_at',
                    'value' => function (Company $model) {
                        return date(DateHelper::FORMAT_USER_DATE, $model->last_visit_at);
                    },
                ],
                [
                    'attribute' => 'activities_id',
                    'value' => function (Company $model) {
                        return $model->activities ? $model->activities->name : null;
                    },
                ],
            ],
            'headers' => [
                'name' => 'Название компании',
                'email' => 'Почта',
                'phone' => 'Телефон',
                'chiefFio' => 'ФИО руководителя',
                'created_at' => 'Дата регистрации',
                'activeTariff' => 'Тариф на сегодня',
                'invoiceCountThisYear' => 'Кол-во выставленных счетов за 2018 год',
                'lastInvoiceDate' => 'Дата последнего счета',
                'last_visit_at' => 'Дата последнего посещения КУБа',
                'activities_id' => 'Вид деятельности',
            ],
            'format' => 'Xlsx',
            'fileName' => 'Companies.xls',
            'savePath' => __DIR__,
        ]);
    }

    /**
     *
     */
    public function actionExportCompaniesIp()
    {
        $year = date('Y');
        $companies = Company::find()
            ->isBlocked(Company::UNBLOCKED)
            ->isTest(!Company::TEST_COMPANY)
            ->andWhere([Company::tableName() . '.company_type_id' => CompanyType::TYPE_IP])
            ->joinWith('companyTaxationType')
            ->andWhere([CompanyTaxationType::tableName() . '.usn' => true])
            // ->andWhere([CompanyTaxationType::tableName() . '.usn_type' => CompanyTaxationType::INCOME])
            // ->andWhere([CompanyTaxationType::tableName() . '.usn_percent' => 6])
            ->andWhere(['between', Company::tableName() . '.created_at', strtotime("01.01.{$year}"), strtotime("31.12.{$year}")])
            ->all();

        $excel = new Excel();
        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $companies,
            'title' => 'Компании ИП',
            'rangeHeader' => array_merge(range('A', 'Z'), ['AA', 'AB',]),
            'columns' => [
                [
                    'attribute' => 'id',
                    'value' => function (Company $model) {
                        return $model->id;
                    },
                ],
                [
                    'attribute' => 'name_short',
                    'value' => function (Company $model) {
                        return $model->name_short;
                    },
                ],
                [
                    'attribute' => 'email',
                    'value' => function (Company $model) {
                        return $model->email;
                    },
                ],
                [
                    'attribute' => 'phone',
                    'value' => function (Company $model) {
                        return $model->phone;
                    },
                ],
                [
                    'attribute' => 'fio',
                    'value' => function (Company $model) {
                        return $model->getChiefFio();
                    }
                ],
                [
                    'attribute' => 'created_at',
                    'value' => function (Company $model) {
                        return date(DateHelper::FORMAT_SUER_DATETIME_WITHOUT_SECONDS, $model->created_at);
                    }
                ],
                [
                    'attribute' => 'registrationPageType',
                    'value' => function (Company $model) {
                        return $model->registrationPageType ? $model->registrationPageType->name : '';
                    }
                ],
                [
                    'attribute' => 'inn',
                    'value' => function (Company $model) {
                        return $model->inn;
                    },
                ],
                [
                    'attribute' => 'bankName',
                    'value' => function (Company $model) {
                        return $model->mainCheckingAccountant ? $model->mainCheckingAccountant->bank_name : '(Не задано)';
                    },
                ],
                [
                    'attribute' => 'outInvoiceCount',
                    'value' => function (Company $model) {
                        return $model->getOutInvoiceCount();
                    },
                ],
                [
                    'attribute' => 'InInvoiceCount',
                    'value' => function (Company $model) {
                        return $model->getInInvoiceCount();
                    },
                ],
                [
                    'attribute' => 'outInvoiceSendEmailCount',
                    'value' => function (Company $model) {
                        return $model->getOutInvoiceSendEmailCount();
                    },
                ],
                [
                    'attribute' => 'isFileLogo',
                    'value' => function (Company $model) {
                        return $model->getIsFileLogo();
                    },
                ],
                [
                    'attribute' => 'isFilePrint',
                    'value' => function (Company $model) {
                        return $model->getIsFilePrint();
                    },
                ],
                [
                    'attribute' => 'isFileSignature',
                    'value' => function (Company $model) {
                        return $model->getIsFileSignature();
                    },
                ],
                [
                    'attribute' => 'filesCount',
                    'value' => function (Company $model) {
                        return $model->getFilesCount();
                    },
                ],
                [
                    'attribute' => 'activation_type',
                    'value' => function (Company $model) {
                        return isset(Company::$activationType[$model->activation_type]) ? Company::$activationType[$model->activation_type] : '';
                    },
                ],
                [
                    'attribute' => 'activeTariff',
                    'value' => function (Company $model) {
                        return $model->hasActualSubscription ?
                            $model->activeSubscribe->getTariffName() :
                            ($model->isFreeTariff ? 'Тариф "Бесплатно"' : 'Нет');
                    },
                ],
                [
                    'attribute' => 'activeSubscribeDaysCount',
                    'value' => function (Company $model) {
                        return $model->getActiveSubscribeDaysCount();
                    },
                ],
                [
                    'attribute' => 'customersCount',
                    'value' => function (Company $model) {
                        return $model->getCustomersCount();
                    },
                ],
                [
                    'attribute' => 'sellersCount',
                    'value' => function (Company $model) {
                        return $model->getSellersCount();
                    },
                ],
                [
                    'attribute' => 'autoInvoicesCount',
                    'value' => function (Company $model) {
                        return $model->getAutoInvoicesCount();
                    },
                ],
                [
                    'attribute' => 'outActCount',
                    'value' => function (Company $model) {
                        return $model->getOutActCount();
                    },
                ],
                [
                    'attribute' => 'paymentOrderCount',
                    'value' => function (Company $model) {
                        return $model->getPaymentOrderCount();
                    },
                ],
                [
                    'attribute' => 'import_xls_product_count',
                    'value' => function (Company $model) {
                        return (int)$model->import_xls_product_count;
                    },
                ],
                [
                    'attribute' => 'import_xls_service_count',
                    'value' => function (Company $model) {
                        return (int)$model->import_xls_service_count;
                    },
                ],
                [
                    'attribute' => 'goodsCount',
                    'value' => function (Company $model) {
                        return $model->getGoodsCount();
                    },
                ],
                [
                    'attribute' => 'servicesCount',
                    'value' => function (Company $model) {
                        return $model->getServicesCount();
                    },
                ],
            ],
            'headers' => [
                'id' => 'ID',
                'name_short' => 'Название организации',
                'email' => 'Email',
                'phone' => 'Телефон',
                'fio' => 'ФИО руководителя',
                'created_at' => 'Дата регистрации',
                'registrationPageType' => 'Страница регистрации',
                'inn' => 'ИНН',
                'bankName' => 'Банк',
                'outInvoiceCount' => 'Кол-во ИСХ счетов',
                'InInvoiceCount' => 'Кол-во ВХ счетов',
                'outInvoiceSendEmailCount' => 'Количество отправленных счетов',
                'isFileLogo' => 'Лого',
                'isFilePrint' => 'Печать',
                'isFileSignature' => 'Подпись',
                'filesCount' => 'Итого',
                'activation_type' => 'Статус',
                'activeTariff' => 'Тарифный план',
                'activeSubscribeDaysCount' => 'Осталось дней (Всего)',
                'customersCount' => 'Покупатели',
                'sellersCount' => 'Продавцы',
                'autoInvoicesCount' => 'Автосчета',
                'outActCount' => 'Актсверки',
                'paymentOrderCount' => 'Платежки',
                'import_xls_product_count' => 'Товары из Excel',
                'import_xls_service_count' => 'Услуги из Excel',
                'goodsCount' => 'Товары',
                'servicesCount' => 'Услуги',
            ],
            'format' => 'Xlsx',
            'fileName' => 'Companies IP',
            'savePath' => __DIR__,
        ]);
    }

    /**
     *
     */
    public function actionMaxMonthInvoiceCompanies()
    {
        $models = [];
        $companies = Invoice::find()
            ->select('company_id')
            ->byDocumentDateRange('2018-01-01', '2018-12-31')
            ->byIOType(Documents::IO_TYPE_OUT)
            ->byDeleted()
            ->groupBy('company_id')
            ->column();
        $monthInvoiceCountByCompany = [];
        foreach ($companies as $companyID) {
            foreach (FlowOfFundsReportSearch::$month as $monthNumber => $monthName) {
                $monthInvoiceCountByCompany[] = Invoice::find()
                    ->byCompany($companyID)
                    ->byIOType(Documents::IO_TYPE_OUT)
                    ->byDeleted()
                    ->byDocumentDateRange('2018-' . $monthNumber . '-01', '2018-' . $monthNumber . '-' . cal_days_in_month(CAL_GREGORIAN, $monthNumber, 2018))
                    ->count();
            }
            $maxCount = max($monthInvoiceCountByCompany);
            $monthInvoiceCountByCompany = [];
            if (!isset($models[$maxCount])) {
                $models[$maxCount] = [
                    'invoiceCount' => $maxCount,
                    'companyCount' => 1,
                ];
            } else {
                $models[$maxCount]['companyCount'] += 1;
            }
        }
        ksort($models);

        $excel = new Excel();
        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $models,
            'title' => 'Счета - Компании',
            'rangeHeader' => range('A', 'B'),
            'columns' => [
                [
                    'attribute' => 'invoiceCount',
                    'value' => function ($model) {
                        return $model['invoiceCount'];
                    },
                ],
                [
                    'attribute' => 'companyCount',
                    'value' => function ($model) {
                        return $model['companyCount'];
                    },
                ],
            ],
            'headers' => [
                'invoiceCount' => 'Кол-во счетов',
                'companyCount' => 'Кол-во компаний',
            ],
            'format' => 'Xlsx',
            'fileName' => 'Invoice - Company',
            'savePath' => __DIR__,
        ]);
    }

    /**
     *
     */
    public function actionExportCompaniesByHalfYear()
    {
        $halfYearPeriods = [
            [
                'from' => '-01-01',
                'to' => '-06-30',
            ],
            [
                'from' => '-07-01',
                'to' => '-12-31',
            ],
        ];
        $models = [];
        foreach (range(2016, 2018) as $year) {
            foreach ($halfYearPeriods as $key => $halfYearPeriod) {
                $periodFrom = $year . $halfYearPeriod['from'];
                $periodTo = $year . $halfYearPeriod['to'];
                $models[$periodFrom]['periodName'] = ++$key . ' полугодие ' . $year;
                $models[$periodFrom]['registrationCount'] = Company::find()
                    ->andWhere('`' . Company::tableName() . '`.`id` = `' . Company::tableName() . '`.`main_id`')
                    ->andWhere(['between', 'date(from_unixtime(`' . Company::tableName() . '`.`created_at`))', $periodFrom, $periodTo])
                    ->andWhere(['!=', 'test', Company::TEST_COMPANY])
                    ->andWhere(['blocked' => Company::UNBLOCKED])
                    ->count();
                $models[$periodFrom]['hasOneInvoiceCompaniesCount'] = Company::find()
                    ->joinWith('invoices')
                    ->isTest(!Company::TEST_COMPANY)
                    ->isBlocked(Company::UNBLOCKED)
                    ->andWhere([Invoice::tableName() . '.type' => Documents::IO_TYPE_OUT])
                    ->andWhere([Invoice::tableName() . '.is_deleted' => false])
                    ->andWhere(['between', Invoice::tableName() . '.document_date', $periodFrom, $periodTo])
                    ->andWhere(['between', Company::tableName() . '.created_at', strtotime($periodFrom), strtotime($periodTo)])
                    ->andHaving(['>=', 'COUNT(invoice.id)', 1])
                    ->groupBy(Company::tableName() . '.id')
                    ->count();
                $models[$periodFrom]['firstPayCompaniesCount'] = Company::find()
                    ->select([
                        'pay_at' => 'MIN({{payment}}.[[payment_date]])',
                    ])
                    ->joinWith(['subscribes subscribe', 'subscribes.payment payment'], false)
                    ->andWhere([
                        'company.test' => false,
                        'payment.is_confirmed' => true,
                        'subscribe.tariff_id' => SubscribeTariff::paidStandartIds(),
                    ])
                    ->andWhere(['between', Company::tableName() . '.created_at', strtotime($periodFrom), strtotime($periodTo)])
                    ->having(['between', 'pay_at', strtotime($periodFrom), strtotime($periodTo)])
                    ->groupBy('company.id')
                    ->indexBy('id')
                    ->count();
                $models[$periodFrom]['firstPayCompaniesSum'] = Company::find()
                    ->select([
                        'pay_at' => 'MIN({{payment}}.[[payment_date]])',
                        'pay_sum' => 'SUM({{payment}}.[[sum]])',
                    ])
                    ->joinWith(['subscribes subscribe', 'subscribes.payment payment'], false)
                    ->andWhere([
                        'company.test' => false,
                        'payment.is_confirmed' => true,
                        'subscribe.tariff_id' => SubscribeTariff::paidStandartIds(),
                    ])
                    ->andWhere(['between', Company::tableName() . '.created_at', strtotime($periodFrom), strtotime($periodTo)])
                    ->andHaving(['between', 'pay_at', strtotime($periodFrom), strtotime($periodTo)])
                    ->groupBy('company.id')
                    ->indexBy('id')
                    ->sum('pay_sum');
                $models[$periodFrom]['totalPayCompaniesCount'] = Payment::find()
                    ->alias('payment')
                    ->joinWith('company')
                    ->andWhere(['between', Company::tableName() . '.created_at', strtotime($periodFrom), strtotime($periodTo)])
                    ->andWhere(['and',
                        ['payment.is_confirmed' => 1],
                        ['payment.tariff_id' => SubscribeTariff::paidStandartIds()],
                        ['not', ['type_id' => PaymentType::TYPE_PROMO_CODE]],
                        ['company.test' => false],
                        ['between', 'payment.payment_date', strtotime($periodFrom), strtotime($periodTo)]
                    ])->count();
                $models[$periodFrom]['totalPayCompaniesSum'] = Payment::find()
                    ->alias('payment')
                    ->joinWith('company')
                    ->andWhere(['between', Company::tableName() . '.created_at', strtotime($periodFrom), strtotime($periodTo)])
                    ->andWhere(['and',
                        ['payment.is_confirmed' => 1],
                        ['payment.tariff_id' => SubscribeTariff::paidStandartIds()],
                        ['not', ['type_id' => PaymentType::TYPE_PROMO_CODE]],
                        ['company.test' => false],
                        ['between', 'payment.payment_date', strtotime($periodFrom), strtotime($periodTo)]
                    ])->sum('sum');
                $models[$periodFrom]['statementFromBankCount'] = Company::find()
                    ->joinWith('cashBankStatementUploads')
                    ->isTest(!Company::TEST_COMPANY)
                    ->isBlocked(Company::UNBLOCKED)
                    ->andWhere(['between', Company::tableName() . '.created_at', strtotime($periodFrom), strtotime($periodTo)])
                    ->andWhere(['between', CashBankStatementUpload::tableName() . '.created_at', strtotime($periodFrom), strtotime($periodTo)])
                    ->groupBy(Company::tableName() . '.id')
                    ->count();
                $models[$periodFrom]['companyExport1cCount'] = Company::find()
                    ->joinWith('export')
                    ->isTest(!Company::TEST_COMPANY)
                    ->isBlocked(Company::UNBLOCKED)
                    ->andWhere(['between', Company::tableName() . '.created_at', strtotime($periodFrom), strtotime($periodTo)])
                    ->andWhere(['between', Export::tableName() . '.created_at', strtotime($periodFrom), strtotime($periodTo)])
                    ->groupBy(Company::tableName() . '.id')
                    ->count();
                $models[$periodFrom]['companiesWithMoreThen10Products'] = Company::find()
                    ->select([
                        'product_count' => 'COUNT({{product}}.[[id]])',
                    ])
                    ->joinWith('products')
                    ->isTest(!Company::TEST_COMPANY)
                    ->isBlocked(Company::UNBLOCKED)
                    ->andWhere(['between', Company::tableName() . '.created_at', strtotime($periodFrom), strtotime($periodTo)])
                    ->andWhere(['and',
                        [Product::tableName() . '.is_deleted' => 0],
                        [Product::tableName() . '.production_type' => Product::PRODUCTION_TYPE_GOODS],
                        ['between', Product::tableName() . '.created_at', strtotime($periodFrom), strtotime($periodTo)]
                    ])
                    ->andHaving(['>', 'product_count', 10])
                    ->groupBy(Company::tableName() . '.id')
                    ->count();
                $models[$periodFrom]['companiesWithMoreThen1Employee'] = Company::find()
                    ->select([
                        'employee_count' => 'COUNT({{employee_company}}.[[employee_id]])',
                    ])
                    ->joinWith('employeeCompanies')
                    ->joinWith('employeeCompanies.employee')
                    ->isTest(!Company::TEST_COMPANY)
                    ->isBlocked(Company::UNBLOCKED)
                    ->andWhere(['between', Company::tableName() . '.created_at', strtotime($periodFrom), strtotime($periodTo)])
                    ->andWhere(['between', EmployeeCompany::tableName() . '.created_at', strtotime($periodFrom), strtotime($periodTo)])
                    ->andWhere([Employee::tableName() . '.is_deleted' => false])
                    ->andHaving(['>', 'employee_count', 1])
                    ->groupBy(Company::tableName() . '.id')
                    ->count();
                $models[$periodFrom]['companiesWithMoreThen1ActOrPackingList'] = Company::find()
                    ->select([
                        'act_count' => 'COUNT({{act}}.[[id]])',
                        'packing_list_count' => 'COUNT({{packing_list}}.[[id]])',
                    ])
                    ->joinWith(['invoices', 'invoices.acts', 'invoices.packingLists'])
                    ->isTest(!Company::TEST_COMPANY)
                    ->isBlocked(Company::UNBLOCKED)
                    ->andWhere(['between', Company::tableName() . '.created_at', strtotime($periodFrom), strtotime($periodTo)])
                    ->andWhere(['or',
                        ['and',
                            ['between', Act::tableName() . '.document_date', $periodFrom, $periodTo],
                            [Act::tableName() . '.type' => Documents::IO_TYPE_OUT],
                        ],
                        ['and',
                            ['between', PackingList::tableName() . '.document_date', $periodFrom, $periodTo],
                            [PackingList::tableName() . '.type' => Documents::IO_TYPE_OUT],
                        ],
                    ])
                    ->andHaving(['or',
                        ['>', 'act_count', 1],
                        ['>', 'packing_list_count', 1],
                    ])
                    ->groupBy(Company::tableName() . '.id')
                    ->count();

            }
        }

        $excel = new Excel();
        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $models,
            'title' => 'Компании по полугодиям',
            'rangeHeader' => range('A', 'I'),
            'columns' => [
                [
                    'attribute' => 'periodName',
                    'value' => function ($model) {
                        return $model['periodName'];
                    },
                ],
                [
                    'attribute' => 'registrationCount',
                    'value' => function ($model) {
                        return $model['registrationCount'];
                    },
                ],
                [
                    'attribute' => 'hasOneInvoiceCompaniesCount',
                    'value' => function ($model) {
                        return $model['hasOneInvoiceCompaniesCount'];
                    },
                ],
                [
                    'attribute' => 'firstPayCompaniesCount',
                    'value' => function ($model) {
                        return $model['firstPayCompaniesCount'] . ' / ' . $model['firstPayCompaniesSum'];
                    },
                ],
                [
                    'attribute' => 'totalPayCompaniesCount',
                    'value' => function ($model) {
                        return $model['totalPayCompaniesCount'] . ' / ' . $model['totalPayCompaniesSum'];
                    },
                ],
                [
                    'attribute' => 'statementFromBankCount',
                    'value' => function ($model) {
                        return $model['statementFromBankCount'];
                    },
                ],
                [
                    'attribute' => 'companyExport1cCount',
                    'value' => function ($model) {
                        return $model['companyExport1cCount'];
                    },
                ],
                [
                    'attribute' => 'companiesWithMoreThen10Products',
                    'value' => function ($model) {
                        return $model['companiesWithMoreThen10Products'];
                    },
                ],
                [
                    'attribute' => 'companiesWithMoreThen1Employee',
                    'value' => function ($model) {
                        return $model['companiesWithMoreThen1Employee'];
                    },
                ],
                [
                    'attribute' => 'companiesWithMoreThen1ActOrPackingList',
                    'value' => function ($model) {
                        return $model['companiesWithMoreThen1ActOrPackingList'];
                    },
                ],
            ],
            'headers' => [
                'periodName' => 'Период',
                'registrationCount' => 'Кол-во регистраций',
                'hasOneInvoiceCompaniesCount' => 'Хотя бы один счет',
                'firstPayCompaniesCount' => 'Кол-во первых оплат. Сумма оплат',
                'totalPayCompaniesCount' => 'Кол-во всего оплат. Сумма оплат.',
                'statementFromBankCount' => 'Кол-во загрузивших выписку.',
                'companyExport1cCount' => 'Кол-во выгрузок в 1С',
                'companiesWithMoreThen10Products' => 'Больше 10-и товаров',
                'companiesWithMoreThen1Employee' => 'Больше 1-ого струдника',
                'companiesWithMoreThen1ActOrPackingList' => 'Есть 1 акт или тн',
            ],
            'format' => 'Xlsx',
            'fileName' => 'Companies',
            'savePath' => __DIR__,
        ]);
    }

    public function actionImportDocuments()
    {
        $transaction = Yii::$app->db->beginTransaction();

        $attributeNames = [
            'A' => 'invoiceNumber',
            'B' => 'invoiceDate',
            'C' => 'rsBankName',
            'D' => 'contractorName',
            'E' => 'productArticle',
            'F' => 'productName',
            'G' => 'productCount',
            'J' => 'productPrice',
            'M' => 'packingListNumber',
            'N' => 'packingListDate',
            'O' => 'invoiceFactureNumber',
            'P' => 'invoiceFactureDate',
            'R' => 'companyRs',
            'S' => 'contractorInn',
            'T' => 'author',
        ];
        $parsedData = [];
        $xls = \PHPExcel_IOFactory::load(Yii::getAlias('@frontend/web/upload/out_invoices_53146.xlsx'));
        $xls->setActiveSheetIndex(14);
        $sheet = $xls->getActiveSheet();
        $rowIterator = $sheet->getRowIterator();
        Console::output('Start importing out invoices...');
        foreach ($rowIterator as $key => $row) {
            if ($key == 1) {
                continue;
            }

            $documentNumber = null;
            $productArticle = null;
            $cellIterator = $row->getCellIterator();
            /* @var \PHPExcel_Cell $cell */
            foreach ($cellIterator as $cellKey => $cell) {
                try {
                    $value = trim($cell->getFormattedValue());
                } catch (\Exception $e) {
                    Console::output('Invalid cell format');
                    continue;
                }
                $attribute = $attributeNames[$cellKey] ?? null;
                if ($attribute !== null) {
                    if ($attribute === 'invoiceNumber') {
                        $documentNumber = $value;
                        if (isset($parsedData['invoiceNumber']) && $parsedData['invoiceNumber'] !== $documentNumber) {
                            if (!$this->saveDocuments($parsedData)) {
                                $transaction->rollBack();
                                return;
                            }
                            $parsedData = [];
                        }
                    }

                    if (in_array($attribute, ['productArticle', 'productName', 'productCount', 'productPrice'])) {
                        if ($attribute === 'productArticle') {
                            $productArticle = $value;
                        }

                        $parsedData['products'][$productArticle][$attribute] = $value;
                    } else {
                        $parsedData[$attribute] = $value;
                    }
                }
            }
        }

        $transaction->commit();
    }

    private function saveDocuments($data)
    {
        $company = Company::findOne(53146);
        Console::output("Invoice #{$data['invoiceNumber']} start parsing...");

        /** @var CheckingAccountant $checkingAccountant */
        $checkingAccountant = $company->getCheckingAccountants()
            ->andWhere(['rs' => $data['companyRs']])
            ->one();
        if ($checkingAccountant === null) {
            Console::output("FAILED saving invoice #{$data['invoiceNumber']}. Checking accountant {$data['companyRs']} not found!!!");
            return false;
        }

        /** @var Contractor $contractor */
        $contractor = Contractor::find()
            ->byCompany($company->id)
            ->byDeleted()
            ->andWhere([
                'AND',
                ['type' => Contractor::TYPE_CUSTOMER],
                ['ITN' => $data['contractorInn']],
            ])->one();
        if ($contractor === null) {
            Console::output("FAILED saving invoice #{$data['invoiceNumber']}. Contractor (Customer) {$data['contractorInn']} not found!!!");
            return false;
        }

        $employeeFio = explode(' ', $data['author']);
        $formattedEmployeeFio = [];
        array_map(function ($val) use (&$formattedEmployeeFio) {
            $trim = preg_replace('/\h+/u', '', preg_replace('/\R+/u', '', $val));
            if (!empty($trim)) {
                $formattedEmployeeFio[] = $trim;
            }
        }, $employeeFio);
        /** @var EmployeeCompany $employee */
        $employee = $company->getEmployeeCompanies()
            ->andWhere([
                'AND',
                ['LIKE', 'lastname', $formattedEmployeeFio[0]],
                ['LIKE', 'firstname', $formattedEmployeeFio[1]],
                ['LIKE', 'patronymic', $formattedEmployeeFio[2]],
                ['is_working' => true],
            ])->one();
        if ($employee === null) {
            Console::output("FAILED saving invoice #{$data['invoiceNumber']}. Employee (Customer) {$data['author']} not found!!!");
            return false;
        }

        $productArray = [];
        foreach ($data['products'] as $productData) {
            /** @var Product $product */
            $product = $company->getProducts()
                ->andWhere([
                    'AND',
                    ['production_type' => Product::PRODUCTION_TYPE_GOODS],
                    ['status' => Product::ACTIVE],
                    ['article' => str_replace(',', '', $productData['productArticle'])],
                ])->one();
            if ($product === null) {
                Console::output("FAILED saving invoice #{$data['invoiceNumber']}. Product {$productData['productArticle']} not found!!!");
                return false;
            }

            $productArray[] = [
                'model' => $product,
                'count' => $productData['productCount'],
                'price' => $productData['productPrice'],
            ];
        }

        $invoice = new Invoice();
        $invoice->type = Documents::IO_TYPE_OUT;
        $invoice->invoice_status_id = InvoiceStatus::STATUS_CREATED;
        $invoice->invoice_status_author_id = $employee->employee_id;
        $invoice->document_author_id = $employee->employee_id;
        $invoice->document_date = DateHelper::format($data['invoiceDate'], DateHelper::FORMAT_USER_DATE, 'n/j/Y');
        $invoice->document_number = $data['invoiceNumber'];
        $invoice->payment_limit_date = date(DateHelper::FORMAT_DATE, strtotime('+10 days', strtotime($invoice->document_date)));
        $invoice->isAutoinvoice = 0;
        $invoice->production_type = Product::PRODUCTION_TYPE_GOODS;
        $invoice->company_rs = $checkingAccountant->rs;

        $invoice->company_id = $company->id;
        $invoice->company_inn = $company->inn;
        $invoice->company_kpp = $company->kpp;
        $invoice->company_egrip = $company->egrip;
        $invoice->company_okpo = $company->okpo;
        $invoice->company_name_full = $company->getTitle();
        $invoice->company_name_short = $company->getTitle(true, true);
        $invoice->company_address_legal_full = $company->getAddressLegalFull();
        $invoice->company_phone = $company->phone;
        $invoice->company_chief_post_name = $company->chief_post_name;
        $invoice->company_chief_lastname = $company->chief_lastname;
        $invoice->company_chief_firstname_initials = $company->chief_firstname_initials;
        $invoice->company_chief_patronymic_initials = $company->chief_patronymic_initials;
        $invoice->company_chief_accountant_lastname = $company->chief_accountant_lastname;
        $invoice->company_chief_accountant_firstname_initials = $company->chief_accountant_firstname_initials;
        $invoice->company_chief_accountant_patronymic_initials = $company->chief_accountant_patronymic_initials;
        $invoice->company_print_filename = $company->print_link;
        $invoice->company_chief_signature_filename = $company->chief_signature_link;

        $invoice->contractor_id = $contractor->id;
        $invoice->contractor_name_short = $contractor->getTitle(true);
        $invoice->contractor_name_full = $contractor->getTitle(false);
        $invoice->contractor_address_legal_full = ($contractor->face_type == Contractor::TYPE_LEGAL_PERSON ? $contractor->legal_address : $contractor->physical_address);
        $invoice->contractor_bank_name = $contractor->bank_name;
        $invoice->contractor_bik = $contractor->BIC;
        $invoice->contractor_inn = $contractor->ITN;
        $invoice->contractor_kpp = $contractor->PPC;
        $invoice->contractor_ks = $contractor->corresp_account;
        $invoice->contractor_rs = $contractor->current_account;

        $invoice->total_order_count = count($data['products']);
        $invoice->nds_view_type_id = Invoice::NDS_VIEW_IN;
        $invoice->price_precision = 2;

        /** @var Order[] $orderArray */
        $orderArray = [];
        foreach ($productArray as $key => $oneProduct) {
            /** @var Product $product */
            $product = $oneProduct['model'];
            $order = OrderHelper::createOrderByProduct(
                $oneProduct['model'],
                $invoice,
                $oneProduct['count'],
                $oneProduct['price'] * 100
            );
            $order->product_title = $product->title;
            $order->number = ++$key;
            $orderArray[] = $order;
        }
        $invoice->populateRelation('orders', $orderArray);
        if (!$invoice->save()) {
            Console::output("FAILED saving invoice #{$data['invoiceNumber']}!!!");
            return false;
        }

        foreach ($orderArray as $order) {
            $order->invoice_id = $invoice->id;
            if (!$order->save()) {
                Console::output("FAILED saving invoice #{$data['invoiceNumber']}. FAILED saving order {$order->product->article}!!!");
                return false;
            }
        }

        $packingListDate = DateHelper::format($data['packingListDate'], DateHelper::FORMAT_USER_DATE, 'n/j/Y');
        if (!$invoice->createPackingList($packingListDate, $data['packingListNumber'])) {
            Console::output("FAILED saving invoice #{$data['invoiceNumber']}. FAILED saving packingList!!!");
            return false;
        }

        $invoiceFactureDate = DateHelper::format($data['invoiceFactureDate'], DateHelper::FORMAT_USER_DATE, 'n/j/Y');
        if (!$invoice->createInvoiceFacture($invoiceFactureDate, $data['invoiceFactureNumber'])) {
            Console::output("FAILED saving invoice #{$data['invoiceNumber']}. FAILED saving invoiceFacture!!!");
            return false;
        }

        return true;
    }

    public function actionUpdateFlowOfFundsItems()
    {
        foreach (Company::find()->select(['id'])->column() as $companyId) {
            foreach (InvoiceIncomeItem::find()
                         ->select(['id'])
                         ->andWhere(['IN', 'id', [18, 19, 20, 21]])
                         ->column() as $incomeItemId) {
                if (!IncomeItemFlowOfFunds::find()->andWhere(['and',
                    ['company_id' => $companyId],
                    ['income_item_id' => $incomeItemId],
                ])->exists()
                ) {
                    $incomeItemFlowOfFunds = new IncomeItemFlowOfFunds();
                    $incomeItemFlowOfFunds->company_id = $companyId;
                    $incomeItemFlowOfFunds->income_item_id = $incomeItemId;
                    if ($incomeItemFlowOfFunds->save()) {
                        Console::output("IncomeItemFlowOfFunds companyId={$companyId} itemId={$incomeItemId}");
                    }
                }
            }

            foreach (InvoiceExpenditureItem::find()
                         ->select(['id'])
                         ->andWhere(['IN', 'id', [
                             54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75,
                             76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89,
                         ]])
                         ->column() as $expenseItemId) {
                if (!ExpenseItemFlowOfFunds::find()->andWhere(['and',
                    ['company_id' => $companyId],
                    ['expense_item_id' => $expenseItemId],
                ])->exists()
                ) {
                    $expenseItemFlowOfFunds = new ExpenseItemFlowOfFunds();
                    $expenseItemFlowOfFunds->company_id = $companyId;
                    $expenseItemFlowOfFunds->expense_item_id = $expenseItemId;
                    if ($expenseItemFlowOfFunds->save()) {
                        Console::output("ExpenseItemFlowOfFunds companyId={$companyId} itemId={$expenseItemId}");
                    }
                }
            }
        }
    }
}
