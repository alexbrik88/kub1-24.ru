<?php

namespace console\controllers;

use common\modules\marketing\commands\MarketingAutoImportCommand;
use yii\console\Controller;

class MarketingReportsController extends Controller
{
    /**
     * @return void
     * @throws
     */
    public function actionCreateImportJobs(): void
    {
        $command = new MarketingAutoImportCommand();
        $command->run();
    }
}
