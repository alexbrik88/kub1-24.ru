<?php

namespace console\controllers;

use common\components\date\DateHelper;
use common\components\pdf\PdfRenderer;
use common\models\Company;
use common\models\document\Act;
use common\models\document\Autoact;
use common\models\document\Invoice;
use common\models\document\InvoiceFacture;
use common\models\document\status\ActStatus;
use common\models\document\status\InvoiceFactureStatus;
use common\models\document\status\InvoiceStatus;
use common\models\EmployeeCompany;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\product\Product;
use frontend\models\Documents;
use frontend\modules\documents\forms\InvoiceSendForm;
use DateTime;
use Yii;
use yii\console\Controller;
use yii\db\Query;

/**
 * Class AutoinvoiceController
 * @package console\controllers
 */
class AutoactController extends Controller
{
    /**
     * @var string
     */
    public $layoutWrapperCssClass = '';

    /**
     * Create Acts
     * @param  integer|null $companyId [description]
     * @return mixed
     */
    public function actionCreate($companyId = null)
    {
        $today = new DateTime('today');
        $dateFrom = (clone $today)->modify('first day of this month');
        $dateTo = (clone $dateFrom)->modify('last day of this month');
        $dateFrom2 = (clone $dateFrom)->modify('first day of -1 month');
        $dateTo2 = (clone $dateFrom2)->modify('last day of this month');
        $documentDate = $today->format('d.m.Y');
        if ($companyId) {
            $this->processData([Autoact::LAST_WORK_DAY, Autoact::LAST_MONTH_DAY], $dateFrom, $dateTo, $documentDate, $companyId);
            $this->processData([Autoact::LAST_WORK_DAY_2, Autoact::LAST_MONTH_DAY_2], $dateFrom2, $dateTo2, $documentDate, $companyId);
        } else {
            $lastDay = (clone $today)->modify('last day of this month');

            $lastWorkday = clone $lastDay;
            while ((int) $lastWorkday->format('N') >= 6) {
                $lastWorkday->modify('-1 day');
            }

            if ($today == $lastWorkday) {
                $this->processData(Autoact::LAST_WORK_DAY, $dateFrom, $dateTo, $documentDate);
                $this->processData(Autoact::LAST_WORK_DAY_2, $dateFrom2, $dateTo2, $documentDate);
            }

            if ($today == $lastDay) {
                $this->processData(Autoact::LAST_MONTH_DAY, $dateFrom, $dateTo, $documentDate);
                $this->processData(Autoact::LAST_MONTH_DAY_2, $dateFrom2, $dateTo2, $documentDate);
            }
        }

        return $this->stdout("done\n");
    }

    /**
     * @param integer $rule
     * @param integer $limit
     * @param integer $offset
     * @return array
     */
    public function processData($rule, $dateFrom, $dateTo, $documentDate, $companyId = null)
    {
        $limit = 100;
        $offset = 0;
        $contractorActs = [];
        $sendData = [];
        do {
            $autoactDataArray = $this->getAutoactArray($rule, $companyId, $limit, $offset++ * $limit);
            foreach ($autoactDataArray as $params) {
                $company = Company::findOne($params['company_id']);
                if ($company) {
                    $senderQuery = EmployeeCompany::find()
                        ->joinWith(['employee employee', 'company company'], false)
                        ->where([
                            'employee.is_active' => Employee::ACTIVE,
                            'employee.is_deleted' => Employee::NOT_DELETED,
                            'employee_company.is_working' => true,
                            'employee_company.employee_role_id' => EmployeeRole::ROLE_CHIEF,
                            'company.id' => $company->id,
                        ])
                        ->orderBy(['employee_company.created_at' => SORT_ASC]);
                    if ($sender = $senderQuery->one()) {
                        $sender->populateRelation('company', $company);
                    }
                    $sendData[$company->id]['employee_id'] = $sender ? $sender->employee_id : null;
                    $sendData[$company->id]['send_act'] = $sender && $company->autoact->send_auto;
                    $sendData[$company->id]['send_if'] = $sender && $company->autoinvoicefacture->send_auto;
                    $needSend = $sendData[$company->id]['send_act'] || $sendData[$company->id]['send_if'];
                    $invoiceIdArray = $this->getInvoiceQuery($params, $dateFrom, $dateTo)->select('invoice.id')->distinct()->column();
                    foreach ($invoiceIdArray as $id) {
                        if ($invoice = Invoice::findOne($id)) {
                            $invoice->populateRelation('company', $company);
                            if ($invoice->createAct($documentDate, null, true)) {
                                if ($needSend && !isset($sendData[$company->id]['contractor_list'][$invoice->contractor_id])) {
                                    $sendData[$company->id]['contractor_list'][$invoice->contractor_id] = $invoice->contractor->someEmail ? : false;
                                }
                            }
                        }
                    }
                }
            }
        } while ($autoactDataArray);

        $this->sendDocs($sendData, $documentDate);
    }

    /**
     * @param $dataArray
     */
    public function sendDocs($dataArray, $documentDate)
    {
        $date = date_create_from_format('d.m.Y', $documentDate)->format('Y-m-d');
        foreach ($dataArray as $companyId => $data) {
            if ($data['employee_id'] && isset($data['contractor_list'])) {
                $company = Company::findOne($companyId);
                $sender = EmployeeCompany::find()
                    ->joinWith(['employee employee', 'company company'], false)
                    ->where([
                        'employee.is_active' => Employee::ACTIVE,
                        'employee.is_deleted' => Employee::NOT_DELETED,
                        'employee_company.is_working' => true,
                        'employee_company.employee_role_id' => EmployeeRole::ROLE_CHIEF,
                        'company.id' => $company->id,
                    ])
                    ->orderBy(['employee_company.created_at' => SORT_ASC])
                    ->one();
                if ($sender) {
                    $sender->populateRelation('company', $company);
                }
                foreach ($data['contractor_list'] as $contractorId => $toEmail) {
                    if ($toEmail !== false) {
                        $actArray = $data['send_act'] ? Act::find()->joinWith('invoice', false)->where([
                            'invoice.is_deleted' => false,
                            'invoice.type' => Documents::IO_TYPE_OUT,
                            'invoice.company_id' => $company->id,
                            'invoice.contractor_id' => $contractorId,
                            'act.status_out_id' => ActStatus::STATUS_CREATED,
                            'act.auto_created' => true,
                            'act.document_date' => $date,
                        ])->all() : [];
                        $ifArray = $data['send_if'] ? InvoiceFacture::find()->joinWith('invoice', false)->where([
                            'invoice.is_deleted' => false,
                            'invoice.type' => Documents::IO_TYPE_OUT,
                            'invoice.company_id' => $company->id,
                            'invoice.contractor_id' => $contractorId,
                            'invoice_facture.status_out_id' => InvoiceFactureStatus::STATUS_CREATED,
                            'invoice_facture.auto_created' => true,
                            'invoice_facture.document_date' => $date,
                        ])->all() : [];

                        if ($actArray && $ifArray) {
                            Documents::sendEmail($sender, array_merge($actArray, $ifArray), $toEmail);
                        } elseif ($actArray) {
                            if (count($actArray) > 1) {
                                Act::manySend($sender, $toEmail, Documents::IO_TYPE_OUT, $actArray);
                            } else {
                                current($actArray)->sendAsEmail($sender, $toEmail);
                            }
                        } elseif ($ifArray) {
                            if (count($ifArray) > 1) {
                                InvoiceFacture::manySend($sender, $toEmail, Documents::IO_TYPE_OUT, $ifArray);
                            } else {
                                current($ifArray)->sendAsEmail($sender, $toEmail);
                            }
                        }
                        Yii::$app->mailer->getTransport()->stop();
                    }
                }
            }
        }
    }

    /**
     * @param $contractorActs
     * @throws \MpdfException
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function getAutoactArray($rule, $companyId = null, $limit = null, $offset = null)
    {
        $query = Autoact::find()->select([
            'autoact.company_id',
            'autoact.status',
            'autoact.no_pay_partial',
            'autoact.no_pay_order',
            'autoact.no_pay_emoney',
        ])->where([
            'autoact.rule' => $rule,
        ])
        ->orderBy(['autoact.company_id' => SORT_ASC]);

        if ($companyId) {
            $query->andWhere([
                'autoact.company_id' => $companyId,
            ]);
        } else {
            $query->leftJoin('company', '{{company}}.[[id]] = {{autoact}}.[[company_id]]');
            $query->andWhere([
                'company.blocked' => false,
            ]);
        }
        if ($limit) {
            $query->limit($limit);
        }
        if ($offset) {
            $query->offset($offset);
        }

        return $query->asArray()->all();
    }

    /**
     * @param  array $params
     * @return yii\db\Query
     */
    public function getInvoiceQuery($params, DateTime $dateFrom, DateTime $dateTo)
    {
        $query = Invoice::find()
            ->leftJoin('act', '{{invoice}}.[[id]] = {{act}}.[[invoice_id]]')
            ->leftJoin('order', '{{invoice}}.[[id]] = {{order}}.[[invoice_id]]')
            ->leftJoin('product', '{{product}}.[[id]] = {{order}}.[[product_id]]')
            ->andWhere([
                'invoice.company_id' => $params['company_id'],
                'act.invoice_id' => null,
                'invoice.type' => Documents::IO_TYPE_OUT,
                'invoice.is_deleted' => false,
                'product.production_type' => Product::PRODUCTION_TYPE_SERVICE,
            ])
            ->andWhere(['between', 'invoice.document_date', $dateFrom->format('Y-m-d'), $dateTo->format('Y-m-d')]);

        if ($params['status'] == Autoact::STATUS_PAY) {
            if ($params['no_pay_partial']) {
                $query->andWhere(['invoice.invoice_status_id' => InvoiceStatus::STATUS_PAYED]);
            } else {
                $query->andWhere(['invoice.invoice_status_id' => [
                    InvoiceStatus::STATUS_PAYED,
                    InvoiceStatus::STATUS_PAYED_PARTIAL,
                ]]);
            }

            if ($params['no_pay_order']) {
                $query
                    ->leftJoin(['pay_order' => 'cash_order_flow_to_invoice'], '{{invoice}}.[[id]] = {{pay_order}}.[[invoice_id]]')
                    ->andWhere(['pay_order.invoice_id' => null]);
            }

            if ($params['no_pay_emoney']) {
                $query
                    ->leftJoin(['pay_emoney' => 'cash_emoney_flow_to_invoice'], '{{invoice}}.[[id]] = {{pay_emoney}}.[[invoice_id]]')
                    ->andWhere(['pay_emoney.invoice_id' => null]);
            }
        } else {
            $query->andWhere(['not', ['invoice.invoice_status_id' => [
                InvoiceStatus::STATUS_REJECTED,
            ]]]);
        }

        return $query;
    }

    /**
     * @param \yii\base\Action $action
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            set_time_limit(28800);
            Yii::$app->urlManager->baseUrl = '/';
            Yii::setAlias('@webroot', '@frontend/web');
            Yii::setAlias('@web', '@frontend/web');
            Yii::$app->urlManager->hostInfo = Yii::$app->params['serviceSiteLk'];
            Yii::$app->db->createCommand('SET session wait_timeout=28800')->execute();
            Yii::$app->db->createCommand('SET session interactive_timeout=28800')->execute();

            return true;
        }

        return false;
    }
}
