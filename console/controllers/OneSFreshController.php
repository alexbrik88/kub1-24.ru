<?php

namespace console\controllers;

use frontend\modules\fresh\jobs\ImportDocumentsJob;
use frontend\modules\import\models\Import1c;
use yii\console\Controller;
use Yii;

/**
 * @package console\controllers
 */
class OneSFreshController extends Controller
{
    public $company;

    /**
     * NOT USED!
     */
    public function actionRestartQueue()
    {
        if (!$this->company) {
            echo 'Not enough params. Add param --company=12345' . "\n\n";
            exit;
        }

        $task = Import1c::find()
            ->where(['company_id' => (int)$this->company])
            ->andWhere(['is_completed' => 0])
            ->andWhere(['period' => null])
            ->one();

        if ($task) {
            $success = Yii::$app->queue->push(
                new ImportDocumentsJob(['task_id' => $task->id]));

            echo ($success)
                ? "Task {$task->id} was added."
                : "Task {$task->id} can't be added!";

        } else {

            echo 'Success. No unfinished tasks.';
        }

        echo "\n\n";
    }

    public function options($actionID)
    {
        return array_merge(parent::options($actionID), ['company']);
    }
}