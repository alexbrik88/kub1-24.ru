<?php

namespace console\controllers;

use common\modules\acquiring\commands\YookassaRemoveWebHookCommand;
use yii\console\Controller;

class YookassaController extends Controller
{
    /**
     * @return void
     */
    public function actionRemoveWebHook(): void
    {
        $command = new YookassaRemoveWebHookCommand();
        $command->run();
    }
}
