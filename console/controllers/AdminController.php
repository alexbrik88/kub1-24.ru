<?php

namespace console\controllers;

use \Yii;
use common\models\User;
use yii\console\Controller;

/**
 * @package console\controllers
 */
class AdminController extends Controller
{
    /**
     * Сгенерировать новый пароль для существующего администратора
     */
    public function actionGenPassword($login)
    {
        $user = User::findByLogin($login);
        if ($user !== null) {
            $password = Yii::$app->security->generateRandomString(12);
            $password_hash = Yii::$app->security->generatePasswordHash($password);
            $user->updateAttributes(['password_hash' => $password_hash]);
            echo $password.PHP_EOL;
        } else {
            echo "User not found".PHP_EOL;
        }
    }
}
