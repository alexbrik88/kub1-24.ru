<?php

namespace console\controllers;

use Yii;
use console\components\KubCrmClient\FromInactiveFindirector;
use yii\console\Controller;

class KubCrmClientController extends Controller
{
    public function actionInactiveFindirector()
    {
        $service = new FromInactiveFindirector(\Yii::$app->kubCompany);
        $service->run();
    }
}
