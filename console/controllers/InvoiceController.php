<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 02.03.2018
 * Time: 8:51
 */

namespace console\controllers;


use common\models\Company;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;
use common\models\product\ProductUnit;
use frontend\models\Documents;
use yii\console\Controller;

/**
 * Class InvoiceController
 * @package console\controllers
 */
class InvoiceController extends Controller
{
    /**
     *
     */
    public function actionCheckStatus()
    {
        /* @var $invoice Invoice */
        foreach (Invoice::find()
                     ->joinWith('company')
                     ->andWhere([Company::tableName() . '.blocked' => Company::UNBLOCKED])
                     ->andWhere([Invoice::tableName() . '.has_discount' => true])
                     ->byDeleted()
                     ->byStatus(InvoiceStatus::STATUS_PAYED_PARTIAL)
                     ->all() as $invoice) {
            echo $invoice->id;
            $invoice->checkPaymentStatus();
        }
    }

    /**
     *
     */
    public function actionUpdateOrderUnit()
    {
        $companyID = 20104;
        /* @var $invoice Invoice */
        foreach (Invoice::find()
                     ->byCompany($companyID)
                     ->byIOType(Documents::IO_TYPE_OUT)
                     ->byDeleted()
                     ->all() as $invoice) {
            foreach ($invoice->orders as $order) {
                $order->unit_id = ProductUnit::UNIT_MONTH;
                $order->save(true, ['unit_id']);
            }
        }
    }
}