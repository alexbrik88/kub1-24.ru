<?php

namespace console\controllers;

use FilesystemIterator;
use frontend\modules\integration\helpers\ImapHelper;
use yii\console\Controller;
use yii\helpers\FileHelper;

/**
 * Фоновые задачи интеграции электронной почты
 *
 * `./yii integration-email/clean-attachments` - удаляет старые не актуальные файлы вложений
 */
class IntegrationEmailController extends Controller
{

    const TIMEOUT = 3600 * 24; //Время, после которого считать файл вложения не актуальным

    /**
     * Удаляет старые не актуальные файлы вложений
     */
    public function actionCleanAttachments()
    {
        $time = time() - self::TIMEOUT;
        $files = FileHelper::findFiles(ImapHelper::attachmentPath(), [
            'filter' => function ($fileName) use ($time) {
                return fileatime($fileName) < $time;
            },
        ]);
        $path = [];
        foreach ($files as $item) {
            unlink($item);
            $i = strrpos($item, DIRECTORY_SEPARATOR);
            $item = substr($item, 0, $i);
            if (in_array($item, $path) === false) {
                $path[] = $item;
            }
        }
        foreach ($path as $item) {
            $iterator = new FilesystemIterator($item);
            if ($iterator->valid() === false) {
                rmdir($item);
            }
        }
    }
}