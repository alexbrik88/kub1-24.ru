<?php

namespace console\controllers;

use common\modules\import\commands\AutoImportCommand;
use common\modules\import\models\ImportJobData;
use yii\console\Controller;

class JobsAutoloadController extends Controller
{
    /**
     * @return void
     */
    public function actionReloadNotFinishedJobs(): void // TODO: write command
    {
        /** @var ImportJobData[] $notFinishedJobs */
        $notFinishedJobs = ImportJobData::find()
            ->andWhere(['finished_at' => null])
            ->andWhere(['<', 'created_at', strtotime('-3 hours')])
            ->all();

        foreach ($notFinishedJobs as $job) {
            // TODO:
        }
    }

    /**
     * @return void
     * @throws
     */
    public function actionCreateJobs(): void
    {
        $command = new AutoImportCommand();
        $command->run();
    }
}
