<?php

namespace console\controllers;

use console\components\PaymentReminder;
use yii\console\Controller;
use Yii;

/**
 * Class ReminderController
 * @package console\controllers
 */
class ReminderController extends Controller
{
    /**
     * @throws \yii\base\Exception
     */
    public function actionSend()
    {
        date_default_timezone_set('Europe/Moscow');

        $sender = new PaymentReminder();
        $sender->send();
    }
}
