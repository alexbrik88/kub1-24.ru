<?php

use common\models\Discount;
use common\models\service\PaymentType;
use common\models\service\SubscribeTariff;
use yii\helpers\Inflector;
use yii\helpers\Url;

/* @var $company common\models\Company */
/* @var $subject string */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = Inflector::slug($subject, '_');
$linkParams = "&amp;utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}&amp;utm_campaign={$this->params['utm_campaign']}";

$route = [
    '/subscribe/default/create',
    'id' => $company->id,
    'key' => $company->subscribe_payment_key,
];
$url_1_invoice = Url::to(array_merge($route, ['tariff' => SubscribeTariff::TARIFF_1, 'type' => PaymentType::TYPE_INVOICE]), 'https');
$url_1_online = Url::to(array_merge($route, ['tariff' => SubscribeTariff::TARIFF_1, 'type' => PaymentType::TYPE_ONLINE]), 'https');
$url_2_invoice = Url::to(array_merge($route, ['tariff' => SubscribeTariff::TARIFF_2, 'type' => PaymentType::TYPE_INVOICE]), 'https');
$url_2_online = Url::to(array_merge($route, ['tariff' => SubscribeTariff::TARIFF_2, 'type' => PaymentType::TYPE_ONLINE]), 'https');
$url_3_invoice = Url::to(array_merge($route, ['tariff' => SubscribeTariff::TARIFF_3, 'type' => PaymentType::TYPE_INVOICE]), 'https');
$url_3_online = Url::to(array_merge($route, ['tariff' => SubscribeTariff::TARIFF_3, 'type' => PaymentType::TYPE_ONLINE]), 'https');
?>

<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody>
        <tr>
            <td style="background-color:#ffffff;">
            <table align="left" cellpadding="10" cellspacing="0" style="display:inline; width:180px; margin-right:10px" width="600">
                <tbody>
                    <tr>
                        <td style="padding:0px; text-align:left; width:180px; border-bottom:1px solid #c3e7fa">
                            <img src="https://lk.kub-24.ru/img/unisender/discount/tariff_1_<?= Discount::TYPE_2; ?>.jpg" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="background-color:#ffffff; font-family:Arial,Helvetica,sans-serif; line-height:100%; font-size:12px; padding-top:20px; width:180px; ">
                        <a href="<?= $url_1_invoice . $linkParams ?>" style="color:#ffffff; font-weight:400; text-decoration:none; padding:10px 15px 10px 15px ; background-color:#0077a7;">Оплатить 1 мес.</a><br />
                        <br />
                        <br />
                        <a href="<?= $url_1_online . $linkParams ?>" style="color:#4276a5; font-weight:400px; text-decoration:underline; font-family:Arial,Helvetica,sans-serif;" target="_self"><i>Оплатить картой</i></a></td>
                    </tr>
                </tbody>
            </table>

            <table align="left" cellpadding="10" cellspacing="0" style="display:inline; width:180px; margin-right:10px" width="600">
                <tbody>
                    <tr>
                        <td style="padding:0px; text-align:left; width:180px; border-bottom:1px solid #c3e7fa">
                            <img src="https://lk.kub-24.ru/img/unisender/discount/tariff_2_<?= Discount::TYPE_2; ?>.jpg" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="background-color:#ffffff; font-family:Arial,Helvetica,sans-serif; line-height:100%; font-size:12px; padding-top:20px; width:180px; ">
                        <a href="<?= $url_2_invoice . $linkParams ?>" style="color:#ffffff; font-weight:400; text-decoration:none; padding:10px 15px 10px 15px ; background-color:#0077a7;">Оплатить 4 мес.</a><br />
                        <br />
                        <br />
                        <a href="<?= $url_2_online . $linkParams ?>" style="color:#4276a5; font-weight:400px; text-decoration:underline; font-family:Arial,Helvetica,sans-serif;" target="_self"><i>Оплатить картой</i></a></td>
                    </tr>
                </tbody>
            </table>

            <table align="left" cellpadding="10" cellspacing="0" style="display:inline; width:180px;" width="600">
                <tbody>
                    <tr>
                        <td style="padding:0px; text-align:left; width:180px; border-bottom:1px solid #c3e7fa">
                            <img src="https://lk.kub-24.ru/img/unisender/discount/tariff_3_<?= Discount::TYPE_2; ?>.jpg" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="background-color:#ffffff; font-family:Arial,Helvetica,sans-serif; line-height:100%; font-size:12px; padding-top:20px; width:180px; ">
                        <a href="<?= $url_3_invoice . $linkParams ?>" style="color:#ffffff; font-weight:400; text-decoration:none; padding:10px 13px 10px 13px ; background-color:#0077a7;">Оплатить 12 мес.</a><br />
                        <br />
                        <br />
                        <a href="<?= $url_3_online . $linkParams ?>" style="color:#4276a5; font-weight:400px; text-decoration:underline; font-family:Arial,Helvetica,sans-serif;" target="_self"><i>Оплатить картой</i></a></td>
                    </tr>
                </tbody>
            </table>
            </td>
        </tr>
        <tr>
            <td style="background-color:#ffffff; text-align: center; font-family:Arial,Helvetica,sans-serif;">
                <span style="color: red; font-weight: bold;">БОНУСЫ СГОРЯТ УЖЕ СЕГОДНЯ в 24 часа 00 минут !</span>
            </td>
        </tr>
    </tbody>
</table>