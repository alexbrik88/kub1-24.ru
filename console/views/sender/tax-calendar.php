<?php

/* @var $companyNotifications Array */

$count = count($companyNotifications);
?>

<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody>
        <tr>
            <td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
            <h1 style="margin-top:0; font-size:28px !important; line-height:100% !important; font-weight:600; color:#333333">Добрый день!</h1>
            <?= $subject ?>
            для компаний на
            <?= $companyData['osno'] ? 'ОСНО (с НДС)' : 'УСН' ?>
            <br />
            <br />
            <?php foreach ($companyNotifications as $key => $notification) : ?>
            <table style="width:560px; margin-bottom:20px;<?= (++$key < $count) ? ' padding-bottom: 10px; border-bottom: 1px dashed #cccccc;' : '' ?>">
                <tbody>
                    <tr>
                        <td style="color:#4276a5"><?= Yii::$app->formatter->asDate($notification->event_date, 'long') ?></td>
                    </tr>
                    <tr>
                        <td>
                        <h2 style="margin-top:0; margin-bottom:5px; font-size:20px !important; line-height:100% !important; font-weight:600; color:#333333">
                            <?= $notification->title ?>
                        </h2>
                        <strong style="color:#333333">Срок:</strong> до <?= Yii::$app->formatter->asDate($notification->event_date, 'long') ?><br />
                        <strong style="color:#333333">Кто:</strong> <?= strtr($notification->text, ['<p>' => '<span>', '</p>' => '</span>']) ?><br />
                        <strong style="color:#333333">Штраф:</strong> <?= $notification->fine ?></td>
                    </tr>
                </tbody>
            </table>
            <?php endforeach ?>
        </tr>
    </tbody>
</table>
