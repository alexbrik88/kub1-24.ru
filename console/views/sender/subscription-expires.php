<?php

use common\models\service\PaymentType;
use common\models\service\SubscribeTariff;
use common\models\Discount;
use yii\helpers\Inflector;
use yii\helpers\Url;

/* @var $company common\models\Company */
/* @var $employee common\models\employee\Employee */
/* @var $params array */

$subject = $params['subject'];
$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = Inflector::slug($subject, '_');
$linkParams = "&amp;utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}&amp;utm_campaign={$this->params['utm_campaign']}";

$route = [
    '/subscribe/default/create',
    'id' => $company->id,
    'key' => $company->subscribe_payment_key,
];
$url_1_invoice = Url::to(array_merge($route, ['tariff' => SubscribeTariff::TARIFF_1, 'type' => PaymentType::TYPE_INVOICE]), 'https');
$url_1_online = Url::to(array_merge($route, ['tariff' => SubscribeTariff::TARIFF_1, 'type' => PaymentType::TYPE_ONLINE]), 'https');
$url_2_invoice = Url::to(array_merge($route, ['tariff' => SubscribeTariff::TARIFF_2, 'type' => PaymentType::TYPE_INVOICE]), 'https');
$url_2_online = Url::to(array_merge($route, ['tariff' => SubscribeTariff::TARIFF_2, 'type' => PaymentType::TYPE_ONLINE]), 'https');
$url_3_invoice = Url::to(array_merge($route, ['tariff' => SubscribeTariff::TARIFF_3, 'type' => PaymentType::TYPE_INVOICE]), 'https');
$url_3_online = Url::to(array_merge($route, ['tariff' => SubscribeTariff::TARIFF_3, 'type' => PaymentType::TYPE_ONLINE]), 'https');
$lastSubscribe = null;
if ($params['text'] === 'text3') {
    $lastSubscribe = $company->getSubscribes()->andWhere([
        'status_id' => \common\models\service\SubscribeStatus::STATUS_ACTIVATED,
    ])->orderBy([
        'expired_at' => SORT_DESC,
    ])->one();
}
?>

<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px dashed #cccccc;">
    <tbody><!-- CONTENT PART1-->
        <tr>
            <td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
                <h1 style="margin-top:0; font-size:28px !important; line-height:100% !important; font-weight:500; color:#333333">
                    Добрый день<?= $employee ? ', ' . $employee->firstname : ''; ?>!
                </h1>
                <?php if ($params['text'] === 'text1') : ?>
                    Ваша подписка на КУБ заканчивается через 5 дней.
                    Что бы дальше использовать все возможности КУБа, продлите подписку заранее.
                    Вы можете оплатить счет на подписку с банковского счета вашей компании или оплатить банковской картой.
                <?php elseif ($params['text'] === 'text2') : ?>
                    Напоминаем, что уже завтра Ваша подписка на КУБ закончится.
                    <br>
                    Что бы дальше использовать все возможности КУБа, продлите подписку заранее.
                    Счет на подписку, вы можете оплатить с банковского счета вашей компании или оплатить банковской картой.
                <?php elseif ($params['text'] === 'text3') : ?>
                    Для компании <?= $company->getShortTitle() ?>
                    закончилась подписка
                    <?php if ($lastSubscribe ?? $lastSubscribe->tariffGroup) : ?>
                        по тарифу "<?= $lastSubscribe->tariffGroup->name ?>"
                    <?php endif ?>
                    и ваш КУБ переведен на ограниченный тариф:
                    <ul style="list-style-position: inside; margin: 0 0 15px; padding: 0 0 0 20px;">
                        <li>Не более 5 счетов в месяц</li>
                        <li>Только 1 организация</li>
                        <li>Не больше 3-х сотрудников</li>
                        <li>Место на диске - 1 ГБ</li>
                    </ul>

                    Что бы дальше использовать все возможности КУБа, необходимо оплатить подписку.
                    Счет на подписку, вы можете оплатить с банковского счета вашей компании или оплатить банковской картой.
                <?php elseif ($params['text'] === 'text4') : ?>
                    КУБ подготовил для вас индивидуальное предложение -
                    <span style="color: red; font-weight: bold; font-style: italic;">
                        скидка <?= Discount::TYPE_3 ?>% при оплате подписки на 4 месяца или 12 месяцев
                    </span>.
                    Вы можете оплатить счет на подписку с банковского счета вашей компании или оплатить банковской картой.
                <?php endif ?>
            </td>
        </tr>
        <!-- END CONTENT PART1 -->
    </tbody>
</table>

<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody><!-- line 1 -->
        <tr>
            <td style="background-color:#ffffff;"><!-- Colum 1 -->
            <table align="left" cellpadding="10" cellspacing="0" style="display:inline; width:180px; margin-right:10px" width="600">
                <tbody>
                    <tr>
                        <td style="padding:0px; text-align:left; width:180px; border-bottom:1px solid #c3e7fa">
                            <img src="https://lk.kub-24.ru/img/unisender/subscription-expires/13-3.jpg" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="background-color:#ffffff; font-family:Arial,Helvetica,sans-serif; line-height:100%; font-size:12px; padding-top:20px; width:180px; ">
                        <a href="<?= $url_1_invoice . $linkParams ?>" style="color:#ffffff; font-weight:400; text-decoration:none; padding:10px 15px 10px 15px ; background-color:#0077a7;">Оплатить 1 мес.</a><br />
                        <br />
                        <br />
                        <a href="<?= $url_1_online . $linkParams ?>" style="color:#4276a5; font-weight:400px; text-decoration:underline; font-family:Arial,Helvetica,sans-serif;" target="_self"><i>Оплатить картой</i></a></td>
                    </tr>
                </tbody>
            </table>
            <!-- END Colum 1 --><!-- Colum 1 -->

            <table align="left" cellpadding="10" cellspacing="0" style="display:inline; width:180px; margin-right:10px" width="600">
                <tbody>
                    <tr>
                        <td style="padding:0px; text-align:left; width:180px; border-bottom:1px solid #c3e7fa">
                            <img src="https://lk.kub-24.ru/img/unisender/subscription-expires/13-4<?= $params['discount'] ? '_2' : ''; ?>.jpg" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="background-color:#ffffff; font-family:Arial,Helvetica,sans-serif; line-height:100%; font-size:12px; padding-top:20px; width:180px; ">
                        <a href="<?= $url_2_invoice . $linkParams ?>" style="color:#ffffff; font-weight:400; text-decoration:none; padding:10px 15px 10px 15px ; background-color:#0077a7;">Оплатить 4 мес.</a><br />
                        <br />
                        <br />
                        <a href="<?= $url_2_online . $linkParams ?>" style="color:#4276a5; font-weight:400px; text-decoration:underline; font-family:Arial,Helvetica,sans-serif;" target="_self"><i>Оплатить картой</i></a></td>
                    </tr>
                </tbody>
            </table>
            <!-- END Colum 1 --><!-- Colum 1 -->

            <table align="left" cellpadding="10" cellspacing="0" style="display:inline; width:180px;" width="600">
                <tbody>
                    <tr>
                        <td style="padding:0px; text-align:left; width:180px; border-bottom:1px solid #c3e7fa">
                            <img src="https://lk.kub-24.ru/img/unisender/subscription-expires/13-5<?= $params['discount'] ? '_2' : ''; ?>.jpg" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="background-color:#ffffff; font-family:Arial,Helvetica,sans-serif; line-height:100%; font-size:12px; padding-top:20px; width:180px; ">
                        <a href="<?= $url_3_invoice . $linkParams ?>" style="color:#ffffff; font-weight:400; text-decoration:none; padding:10px 13px 10px 13px ; background-color:#0077a7;">Оплатить 12 мес.</a><br />
                        <br />
                        <br />
                        <a href="<?= $url_3_online . $linkParams ?>" style="color:#4276a5; font-weight:400px; text-decoration:underline; font-family:Arial,Helvetica,sans-serif;" target="_self"><i>Оплатить картой</i></a></td>
                    </tr>
                </tbody>
            </table>
            <!-- END Colum 1 --></td>
        </tr>
        <!-- END LINE 1 -->
        <?php if ($params['discount']) : ?>
            <tr>
                <td style="background-color:#ffffff; text-align: center; font-family:Arial,Helvetica,sans-serif;">
                    <span style="color: red; font-weight: bold;">СКИДКА ДЕЙСТВУЕТ ТОЛЬКО 5 дней!</span>
                </td>
            </tr>
        <?php endif ?>
    </tbody>
</table>