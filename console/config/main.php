<?php

$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

return [
    'id' => 'app-console',
    'basePath' => dirname(__DIR__),
    'bootstrap' => [
        function () {
            set_time_limit(6000);
            Yii::$app->db->createCommand('SET session wait_timeout=28800')->execute();
            Yii::$app->db->createCommand('SET session interactive_timeout=28800')->execute();
        },
        'log',
        'queue',
        'common\modules\import\components\ImportJobHandler',
    ],
    'controllerNamespace' => 'console\controllers',
    'language' => 'ru-RU',
    'sourceLanguage' => 'en-US',
    'components' => [
        'authManager' => [
            'class' => 'yii\rbac\DbManager',
            'cache' => 'frontendCache',
        ],
        'frontendCache' => [
            'class' => 'yii\caching\FileCache',
            'cachePath' => '@frontend/runtime/cache',
        ],
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'except' => [
                        'validation',
                        'robokassa',
                        'banking',
                    ],
                ],
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'categories' => ['validation'],
                    'logFile' => '@runtime/logs/validation.log',
                ],
            ],
        ],
        'dadataSuggestApi' => [
            'class' => 'skeeks\yii2\dadataSuggestApi\DadataSuggestApi',
            'authorization_token' => '78497656dfc90c2b00308d616feb9df60c503f51',
            'timeout' => 12,
        ],
        'urlManager' => [
            'baseUrl' => 'https://lk.kub-24.ru/',
        ],
    ],
    'controllerMap' => [
        'migrate' => [
            'class' => 'yii\console\controllers\MigrateController',
            'templateFile' => '@console/migrations/migration_template.php',
        ],
    ],
    'params' => $params,
];
