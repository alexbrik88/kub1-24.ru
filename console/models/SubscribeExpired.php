<?php

namespace console\models;

use common\models\service\Subscribe;
use common\models\service\SubscribeStatus;
use common\models\service\SubscribeTariffGroup;
use frontend\modules\telegram\components\NotificationDispatcher;
use yii\base\Model;

class SubscribeExpired extends Model
{
    /** @var int */
    public $timeInterval = 86400;

    /** @var int[] Идентификаторы групп тарифов */
    public $tariffGroupId = [
        SubscribeTariffGroup::PRICE_LIST,
        SubscribeTariffGroup::B2B_PAYMENT,
    ];

    /**
     * @var NotificationDispatcher
     */
    private $dispatcher;

    /**
     * @inheritDoc
     */
    public function init()
    {
        $this->dispatcher = new NotificationDispatcher();
    }

    /**
     * @return void
     */
    public function sendNotifications(): void
    {
        foreach ($this->getExpired() as $i => $subscribe) {
            if (!$subscribe->company->blocked
            && !$subscribe->company->getActualSubscription($subscribe->tariff_group_id))
            {
                $notification = new SubscribeExpiredNotification($subscribe);
                $this->dispatcher->sendNotification($notification, $i);
            }
        }
    }

    /**
     * @return Subscribe[]
     */
    private function getExpired(): array
    {
        $min = (time() - $this->timeInterval - 1);
        $max = time();

        return Subscribe::find()
            ->andWhere(['status_id' => SubscribeStatus::STATUS_ACTIVATED])
            ->andWhere(['tariff_group_id' => $this->tariffGroupId])
            ->andWhere(['between', 'expired_at', $min, $max])
            ->with(['company'])
            ->all();
    }
}
