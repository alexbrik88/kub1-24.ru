<?php

namespace console\models;

use common\models\Company;
use common\models\employee\Employee;
use Yii;

/**
 * This is the model class for table "sent_email".
 *
 * @property integer $id
 * @property integer $company_id
 * @property integer $employee_id
 * @property integer $email_type_id
 * @property integer $email_type_number
 * @property integer $created_at
 * @property string $email
 *
 * @property Company $company
 * @property Employee $employee
 * @property SentEmailType $emailType
 */
class SentEmail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sent_email';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'employee_id', 'email_type_id', 'email_type_number', 'created_at'], 'integer'],
            [['email_type_id', 'created_at'], 'required'],
            [['email'], 'string'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['employee_id' => 'id']],
            [['email_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => SentEmailType::className(), 'targetAttribute' => ['email_type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'employee_id' => 'Employee ID',
            'email_type_id' => 'Email Type ID',
            'email_type_number' => 'Type Number',
            'created_at' => 'Created At',
            'email' => 'Email',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmailType()
    {
        return $this->hasOne(SentEmailType::className(), ['id' => 'email_type_id']);
    }
}
