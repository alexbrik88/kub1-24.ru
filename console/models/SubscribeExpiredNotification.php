<?php

namespace console\models;

use common\models\service\Subscribe;
use frontend\modules\telegram\components\NotificationInterface;
use frontend\modules\telegram\models\Identity;

class SubscribeExpiredNotification implements NotificationInterface
{
    /** @var string */
    private const MESSAGE = <<<TXT
У вас закончился оплаченный период.
Чтобы работа КУБ-24 не прекращалась, и вы получали уведомления, нужно оплатить тариф.
TXT;

    /**
     * @var Subscribe Экземпляр подписки
     */
    private $subscribe;

    /**
     * @param Subscribe $subscribe Экземпляр подписки
     */
    public function __construct(Subscribe $subscribe)
    {
        $this->subscribe = $subscribe;
    }

    /**
     * @inheritDoc
     */
    public function isEnabled(): bool {
        if ($this->subscribe->company->getActualSubscription($this->subscribe->tariff_group_id)) {
            return false;
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    public function getCompanyId() : int {
        return $this->subscribe->company_id;
    }

    /**
     * @inheritDoc
     */
    public function getMessage() : string {
        return self::MESSAGE;
    }

    /**
     * @inheritDoc
     */
    public function onDispatch(Identity $identity): bool {
        return true;
    }
}
