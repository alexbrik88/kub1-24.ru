<?php

namespace console\models;

use Yii;
use yii\helpers\Json;

/**
 * This is the model class for table "autovoronka_send_log".
 *
 * @property int $id
 * @property int $for_unread
 * @property string $date
 * @property string $case
 * @property string $mail
 * @property string $subject
 * @property string $list_id
 * @property array $_contacts
 * @property array $_result
 * @property int $created_at
 */
class AutovoronkaSendLog extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\behaviors\TimestampBehavior',
                'updatedAtAttribute' => false,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'autovoronka_send_log';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'for_unread' => 'For Unread',
            'date' => 'Date',
            'case' => 'Case',
            'mail' => 'Mail',
            'subject' => 'Subject',
            'list_id' => 'List ID',
            '_contacts' => 'Contacts',
            '_result' => 'Result',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @param array $value
     */
    public function setContacts($value)
    {
        $this->_contacts = Json::encode($value) ? : Json::encode([]);
    }

    /**
     * @return array
     */
    public function getContacts()
    {
        return Json::decode($this->_contacts) ? : [];
    }

    /**
     * @param array $value
     */
    public function setResult($value)
    {
        $this->_result = Json::encode($value) ? : Json::encode([]);
    }

    /**
     * @return array
     */
    public function getResult()
    {
        return Json::decode($this->_result) ? : [];
    }
}
