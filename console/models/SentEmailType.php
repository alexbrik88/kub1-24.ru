<?php

namespace console\models;

use Yii;

/**
 * This is the model class for table "sent_email_type".
 *
 * @property integer $id
 * @property string $type
 * @property string $description
 *
 * @property SentEmail[] $sentEmails
 */
class SentEmailType extends \common\models\SentEmailType
{
    //
}
