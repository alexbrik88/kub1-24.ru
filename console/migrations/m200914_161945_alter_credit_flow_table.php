<?php

use console\components\db\Migration;

class m200914_161945_alter_credit_flow_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%credit_flow}}';

    /**
     * @inheritDoc
     */
    public function up()
    {
        $this->dropForeignKey('fk_credit_olap_flow_id_wallet_id__olap_flows_id_wallet', self::TABLE_NAME);
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        return false;
    }
}
