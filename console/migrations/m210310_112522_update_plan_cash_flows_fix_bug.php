<?php

use common\models\currency\Currency;
use console\components\db\Migration;
use frontend\modules\analytics\models\PlanCashFlows;
use common\models\company\CheckingAccountant;
use common\models\cash\Cashbox;
use common\models\cash\Emoney;

class m210310_112522_update_plan_cash_flows_fix_bug extends Migration
{
    public function safeUp()
    {
        $companiesIds = PlanCashFlows::find()->select(['company_id'])->distinct()->column();

        foreach ($companiesIds as $companyId) {

            $mainCheckingAccount = CheckingAccountant::findOne([
                'company_id' => $companyId,
                'type' => CheckingAccountant::TYPE_MAIN,
                'currency_id' => Currency::DEFAULT_ID
            ]);

            $mainCashbox = Cashbox::findOne([
                'company_id' => $companyId,
                'is_main' => 1,
                'currency_id' => Currency::DEFAULT_ID
            ]);

            $mainEmoney = Emoney::findOne([
                'company_id' => $companyId,
                'is_main' => 1,
                'currency_id' => Currency::DEFAULT_ID
            ]);

            if ($mainCheckingAccount) {
                $this->execute("UPDATE `plan_cash_flows` SET `checking_accountant_id` = " .$mainCheckingAccount->id. " WHERE `company_id` = {$companyId} AND `payment_type` = 1 AND `checking_accountant_id` IS NULL");
            }
            if ($mainCashbox) {
                $this->execute("UPDATE `plan_cash_flows` SET `cashbox_id` = " .$mainCashbox->id. " WHERE `company_id` = {$companyId} AND `payment_type` = 2 AND `cashbox_id` IS NULL");
            }
            if ($mainEmoney) {
                $this->execute("UPDATE `plan_cash_flows` SET `emoney_id` = " .$mainEmoney->id. " WHERE `company_id` = {$companyId} AND `payment_type` = 3 AND `emoney_id` IS NULL");
            }
        }
    }

    public function safeDown()
    {
        // no down
    }
}
