<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181031_142409_alter_invoice_add_column extends Migration
{
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE `invoice`
                ADD COLUMN `has_waybill` SMALLINT(1) NOT NULL DEFAULT '0' AFTER `has_packing_list`,
                ADD COLUMN `can_add_waybill` SMALLINT(1) NOT NULL DEFAULT '1' AFTER `can_add_packing_list`,
                ADD INDEX `has_waybill` (`has_waybill`);
        ");
    }

    public function safeDown()
    {
        $this->execute("
            ALTER TABLE `invoice`
                DROP COLUMN `has_waybill`,
                DROP COLUMN `can_add_waybill`;
        ");
    }
}


