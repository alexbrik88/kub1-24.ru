<?php

use common\models\amocrm\Contact;
use common\models\employee\Employee;
use console\components\db\Migration;

class m200110_104727_amocrm_contacts_company_id_insert_from_employee_id extends Migration
{
    const TABLE = '{{%amocrm_contacts}}';

    public function safeUp()
    {
        foreach (Contact::find()->all() as $contact) {
            /** @var Contact $contacts */
            /** @noinspection PhpUndefinedFieldInspection */
            $contact->company_id = Employee::findOne(['id' => $contact->employee_id])->company_id;
            $contact->save();
        }
    }

    public function down()
    {
        $this->update(self::TABLE, ['company_id' => 0]);
    }
}
