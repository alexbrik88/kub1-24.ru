<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210730_131832_alter_tables_change_industry_fk extends Migration
{
    public static $docsTables = [
        // RU
        'invoice',
        'goods_cancellation' // списание товара
    ];

    public static $cashTables = [
        // RU
        'cash_bank_flows',
        'cash_emoney_flows',
        'cash_order_flows',
        'acquiring_operation',
        'card_operation',
        // FOREIGN
        'cash_bank_foreign_currency_flows',
        'cash_emoney_foreign_currency_flows',
        'cash_order_foreign_currency_flows',
    ];

    public static $planCashTables = [
        // RU
        'plan_cash_flows',
        // FOREIGN
        'plan_cash_foreign_currency_flows',
    ];

    public static $oldRefTableName = 'company_info_industry';
    public static $newRefTableName = 'company_industry';

    public function safeUp()
    {
        $allTables = array_merge(
            self::$docsTables,
            self::$cashTables,
            self::$planCashTables
        );

        foreach ($allTables as $tableName) {
            $this->dropForeignKey("FK_{$tableName}_to_industry", $tableName);
            $this->addForeignKey("FK_{$tableName}_to_industry", $tableName, 'industry_id', self::$newRefTableName, 'id', 'SET NULL', 'CASCADE');
        }
    }

    public function safeDown()
    {
        $allTables = array_merge(
            self::$docsTables,
            self::$cashTables,
            self::$planCashTables
        );

        foreach ($allTables as $tableName) {
            $this->dropForeignKey("FK_{$tableName}_to_industry", $tableName);
            $this->addForeignKey("FK_{$tableName}_to_industry", $tableName, 'industry_id', self::$oldRefTableName, 'id', 'SET NULL', 'CASCADE');
        }
    }
}
