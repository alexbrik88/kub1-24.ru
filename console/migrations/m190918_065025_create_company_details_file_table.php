<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%company_details_file}}`.
 */
class m190918_065025_create_company_details_file_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%company_details_file}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'type_id' => $this->integer()->notNull(),
            'status_id' => $this->integer(),
            'name' => $this->string(),
            'ext' => $this->string(),
            'size' => $this->integer(),
            'created_by' => $this->integer(),
            'created_at' => $this->integer(),
        ]);

        $this->addForeignKey(
            'FK_company_details_file_company',
            '{{company_details_file}}',
            'company_id',
            '{{company}}',
            'id'
        );

        $this->addForeignKey(
            'FK_company_details_file_company_details_file_type',
            '{{company_details_file}}',
            'type_id',
            '{{company_details_file_type}}',
            'id'
        );

        $this->addForeignKey(
            'FK_company_details_file_company_details_file_status',
            '{{company_details_file}}',
            'status_id',
            '{{company_details_file_status}}',
            'id'
        );

        $this->addForeignKey(
            'FK_company_details_file_employee',
            '{{company_details_file}}',
            'created_by',
            '{{employee}}',
            'id'
        );

        $this->createIndex('status', '{{%company_details_file}}', [
            'company_id',
            'type_id',
            'status_id',
        ], true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%company_details_file}}');
    }
}
