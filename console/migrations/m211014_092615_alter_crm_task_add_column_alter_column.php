<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211014_092615_alter_crm_task_add_column_alter_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%crm_task}}', 'task_number', $this->bigInteger()->null());
        $this->alterColumn('{{%crm_task}}', 'contractor_id', $this->integer()->null());
        $this->alterColumn('{{%crm_task}}', 'employee_id', $this->integer()->null());
        $this->alterColumn('{{%crm_task}}', 'description', $this->text()->null());
        $this->alterColumn('{{%crm_task}}', 'event_type', $this->tinyInteger(3)->null());
        $this->alterColumn('{{%crm_task}}', 'datetime', $this->dateTime()->null());
        $this->alterColumn('{{%crm_task}}', 'result', $this->text()->null());

        $this->createIndex('company_id', '{{%crm_task}}', 'company_id');
    }

    public function safeDown()
    {
        $this->dropIndex('company_id', '{{%crm_task}}');

        $this->alterColumn('{{%crm_task}}', 'task_number', $this->bigInteger()->notNull()->defaultValue());
        $this->alterColumn('{{%crm_task}}', 'contractor_id', $this->integer()->notNull());
        $this->alterColumn('{{%crm_task}}', 'employee_id', $this->integer()->notNull());
        $this->alterColumn('{{%crm_task}}', 'description', $this->text()->notNull());
        $this->alterColumn('{{%crm_task}}', 'event_type', $this->tinyInteger(3)->notNull());
        $this->alterColumn('{{%crm_task}}', 'datetime', $this->dateTime()->notNull());
        $this->alterColumn('{{%crm_task}}', 'result', $this->text()->notNull()->defaultValue(''));
    }
}
