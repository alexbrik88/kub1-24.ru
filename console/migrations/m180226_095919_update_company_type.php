<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180226_095919_update_company_type extends Migration
{
    public function safeUp()
    {
        $this->execute("
            UPDATE {{company_type}}
            SET {{company_type}}.[[in_company]] = 1
            WHERE {{company_type}}.[[id]] = 5
        ");
    }

    public function safeDown()
    {
        $this->execute("
            UPDATE {{company_type}}
            SET {{company_type}}.[[in_company]] = 0
            WHERE {{company_type}}.[[id]] = 5
        ");
    }
}
