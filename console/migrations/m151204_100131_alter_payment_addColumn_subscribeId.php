<?php

use yii\db\Schema;
use yii\db\Migration;

class m151204_100131_alter_payment_addColumn_subscribeId extends Migration
{
    public $tSubscribe = 'service_subscribe';
    public $tPayment = 'service_payment';

    public function safeUp()
    {
        $this->addColumn($this->tPayment, 'subscribe_id', $this->integer()->defaultValue(null) . ' AFTER invoice_number');
        $this->addForeignKey('fk_service_payment_to_subscribe', $this->tPayment, 'subscribe_id', $this->tSubscribe, 'id');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('fk_service_payment_to_subscribe', $this->tPayment);
        $this->dropColumn($this->tPayment, 'subscribe_id');
    }
}
