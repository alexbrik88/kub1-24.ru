<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211220_175155_update_cash_order_flows extends Migration
{
    public function safeUp()
    {
        $this->execute('
            UPDATE {{%cash_order_flows}}
            LEFT JOIN {{%ofd_receipt}} ON {{%ofd_receipt}}.[[uid]] = {{%cash_order_flows}}.[[ofd_receipt_uid]]
            SET {{%cash_order_flows}}.[[flow_type]] = 1
            WHERE {{%cash_order_flows}}.[[ofd_receipt_uid]] IS NOT NULL
            AND {{%ofd_receipt}}.[[operation_type_id]] IN (1, 4)
        ');
        $this->execute('
            UPDATE {{%cash_order_flows}}
            LEFT JOIN {{%ofd_receipt}} ON {{%ofd_receipt}}.[[uid]] = {{%cash_order_flows}}.[[ofd_receipt_uid]]
            SET {{%cash_order_flows}}.[[flow_type]] = 0
            WHERE {{%cash_order_flows}}.[[ofd_receipt_uid]] IS NOT NULL
            AND {{%ofd_receipt}}.[[operation_type_id]] IN (2, 3)
        ');
    }

    public function safeDown()
    {
        //
    }
}
