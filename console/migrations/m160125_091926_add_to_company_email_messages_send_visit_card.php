<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160125_091926_add_to_company_email_messages_send_visit_card extends Migration
{
    public $tCompany = 'company';

    public function safeUp()
    {
        $this->addColumn($this->tCompany, 'email_messages_send_visit_card', $this->integer(11));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tCompany, 'email_messages_send_visit_card');
    }
}


