<?php

use yii\db\Schema;
use yii\db\Migration;

class m150606_152608_alter_Contractor_changeColumn_Date extends Migration
{
    public function safeUp()
    {
        $this->renameColumn('contractor', 'date', 'created_at');
        $this->alterColumn('contractor', 'created_at', Schema::TYPE_INTEGER . ' NOT NULL');
        $this->update('contractor', [
            'created_at' => new \yii\db\Expression('UNIX_TiMESTAMP()'),
        ]);
    }
    
    public function safeDown()
    {
        $this->renameColumn('contractor', 'created_at', 'date');
        $this->alterColumn('contractor', 'date', Schema::TYPE_DATE);
        $this->update('contractor', [
            'date' => new \yii\db\Expression('NOW()'),
        ]);
    }
}
