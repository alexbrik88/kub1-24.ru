<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170208_121741_add_company_type extends Migration
{
    public $tableName = 'company_type';

    public function safeUp()
    {
        $this->insert($this->tableName, [
            'id' => 21,
            'name_short' => 'ГУП',
            'name_full' => 'Государственное унитарное предприятие',
            'in_company' => false,
            'in_contractor' => true,
        ]);
    }

    public function safeDown()
    {
        $this->delete($this->tableName, ['id' => 21]);
    }
}


