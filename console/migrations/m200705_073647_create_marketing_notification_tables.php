<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200705_073647_create_marketing_notification_tables extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }


        //////////// marketing_channel ///////////////////////////////////////////
        $this->createTable('marketing_channel', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' DEFAULT NULL',
            'sort' => Schema::TYPE_TINYINT . ' DEFAULT 0'
        ], $tableOptions);

        $this->batchInsert('marketing_channel', ['id', 'name', 'sort'], [
            [1, 'Яндекс.Директ', 1],
            [2, 'Google Ads', 2],
            [3, 'Facebook', 3],
            [4, 'Instagram', 4],
            [5, 'ВКонтакте', 5],
        ]);
        ///////////////////////////////////////////////////////////////////////////


        //////////// marketing_notification_group /////////////////////////////////
        $this->createTable('marketing_notification_group', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' DEFAULT NULL',
            'rule' => Schema::TYPE_STRING . ' DEFAULT NULL',
            'description' => Schema::TYPE_STRING . ' DEFAULT NULL',
            'threshold_unit' => Schema::TYPE_TINYINT . ' DEFAULT 1',
            'sort' => Schema::TYPE_TINYINT . ' DEFAULT 0'
        ], $tableOptions);

        $this->batchInsert('marketing_notification_group', ['id', 'sort', 'threshold_unit', 'name', 'rule', 'description'], [
            [1, 1, 1, 'Баланс', 'ниже суммы', 'Если сумма баланса меньше указанной суммы, то вы получите уведомление'],
            [2, 2, 2, 'Баланса', 'осталось на', 'Вычисляется прогнозная дата окончания баланса, если дата ближе, чем заданное кол-во дней, то вы получите уведомление'],
            [3, 3, 1, 'Баланс', 'нулевой остаток', 'Если сумма баланса равна нулю, то вы получите уведомление'],
            [4, 4, 0, 'Количество кликов', 'за вчера ниже', 'Если количество меньше указанного значения, то вы получите уведомление'],
            [5, 5, 1, 'Стоимость клика', 'за вчера выше', 'Если стоимость выше указанного значения, то вы получите уведомление'],
            [6, 6, 0, 'Количество лидов', 'за вчера ниже', 'Если количество меньше указанного значения, то вы получите уведомление'],
            [7, 7, 1, 'Стоимость лида', 'за вчера выше', 'Если стоимость больше указанного значения, то вы получите уведомление'],
            [8, 8, 1, 'Количество визитов на сайт', 'за вчера ниже', 'Если количество меньше указанного значения, то вы получите уведомление'],
            [9, 9, 1, 'Расход на рекламу', 'за вчера выше чем', 'Если расход больше указанного значения, то вы получите уведомление'],
            [11, 11, 3, 'Отклонение количества лидов', 'за вчера в меньшую сторону от среднего за неделю на', 'Если количество лидов за вчера меньше среднего значения за 7 дней до вчерашнего, на указанный процент, то вы получите уведомление'],
            [12, 12, 3, 'Отклонение стоимости лида', 'за вчера в большую сторону от среднего за неделю на', 'Если стоимость лида за вчера больше среднего значения за 7 дней до вчерашнего, на указанный процент, то вы получите уведомление'],
            [13, 13, 3, 'Отклонение расхода на рекламу', 'за вчера больше среднего расхода за неделю на', 'Если расход за вчера больше среднего значения за 7 дней до вчерашнего, на указанный процент, то вы получите уведомление'],
        ]);
        ///////////////////////////////////////////////////////////////////////////

        //////////// marketing_notification ///////////////////////////////////////////
        $this->createTable('marketing_notification', [
            'id' => Schema::TYPE_PK,
            'company_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'channel_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'group_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'threshold' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
            'notify_by_informer' => Schema::TYPE_BOOLEAN . ' NOT NULL DEFAULT 0',
            'notify_by_email' => Schema::TYPE_BOOLEAN . ' NOT NULL DEFAULT 0',
            'notify_by_whatsapp' => Schema::TYPE_BOOLEAN . ' NOT NULL DEFAULT 0',
            'notify_by_telegram' => Schema::TYPE_BOOLEAN . ' NOT NULL DEFAULT 0',
            'last_date' => Schema::TYPE_DATE,
            'skip' => Schema::TYPE_BOOLEAN . ' NOT NULL DEFAULT 0',
        ], $tableOptions);

        $this->addForeignKey('marketing_notification_to_company', 'marketing_notification', 'company_id', 'company', 'id');
        $this->addForeignKey('marketing_notification_to_channel', 'marketing_notification', 'channel_id', 'marketing_channel', 'id', 'CASCADE');
        $this->addForeignKey('marketing_notification_to_group', 'marketing_notification', 'group_id', 'marketing_notification_group', 'id', 'CASCADE');
        ///////////////////////////////////////////////////////////////////////////
    }

    public function safeDown()
    {
        $this->dropForeignKey('marketing_notification_to_company', 'marketing_notification');
        $this->dropForeignKey('marketing_notification_to_channel', 'marketing_notification');
        $this->dropForeignKey('marketing_notification_to_group', 'marketing_notification');

        $this->dropTable('marketing_channel');
        $this->dropTable('marketing_notification_group');
        $this->dropTable('marketing_notification');
    }
}
