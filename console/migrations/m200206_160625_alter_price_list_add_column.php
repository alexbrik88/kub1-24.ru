<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200206_160625_alter_price_list_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('price_list', 'last_sended_to', $this->text());
    }

    public function safeDown()
    {
        $this->dropColumn('price_list', 'last_sended_to');
    }
}
