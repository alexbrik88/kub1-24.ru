<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211029_120427_alter_service_payment_order_alter_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%service_payment_order}}', 'discount', $this->decimal(8, 6)->notNull()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->alterColumn('{{%service_payment_order}}', 'discount', $this->decimal(6, 4)->notNull()->defaultValue(0));
    }
}
