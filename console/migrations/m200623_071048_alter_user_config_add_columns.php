<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200623_071048_alter_user_config_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_config', 'report_abc_purchase_price', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn('user_config', 'report_abc_selling_price', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn('user_config', 'report_abc_show_groups', $this->boolean()->notNull()->defaultValue(true));
    }

    public function safeDown()
    {
        $this->dropColumn('user_config', 'report_abc_purchase_price');
        $this->dropColumn('user_config', 'report_abc_selling_price');
        $this->dropColumn('user_config', 'report_abc_show_groups');
    }
}
