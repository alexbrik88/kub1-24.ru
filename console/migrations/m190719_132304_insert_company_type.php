<?php

use common\models\company\CompanyType;
use console\components\db\Migration;
use yii\db\Schema;

class m190719_132304_insert_company_type extends Migration
{
    public function safeUp()
    {
        if (!CompanyType::find()->where('[[id]] = 0')->exists()) {
            $this->insert('{{%company_type}}', [
                'id' => '0',
                'name_short' => '',
                'name_full' => '',
                'in_company' => 0,
                'in_contractor' => 0,
            ]);
            $id = Yii::$app->db->getLastInsertID();
            $this->update('{{%company_type}}', ['id' => 0], ['id' => $id]);
        }
    }

    public function safeDown()
    {
        $this->execute('SET foreign_key_checks = 0');
        $this->delete('{{%company_type}}', [
            'id' => '0',
        ]);
        $this->execute('SET foreign_key_checks = 1');
    }
}
