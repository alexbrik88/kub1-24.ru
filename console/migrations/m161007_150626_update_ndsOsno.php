<?php

use console\components\db\Migration;
use yii\db\Schema;

class m161007_150626_update_ndsOsno extends Migration
{
    public function safeUp()
    {
        $this->update('nds_osno', [
            'name' => 'НДС включен в цену',
        ], [
            'id' => 1,
        ]);
        $this->update('nds_osno', [
            'name' => 'НДС сверху цены',
        ], [
            'id' => 2,
        ]);
    }
    
    public function safeDown()
    {
        $this->update('nds_osno', [
            'name' => 'с НДС',
        ], [
            'id' => 1,
        ]);
        $this->update('nds_osno', [
            'name' => 'без НДС',
        ], [
            'id' => 2,
        ]);
    }
}


