<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201122_154950_alter_config_add_columns extends Migration
{
    public $table = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->table, 'finance_model_tab_help', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table, 'finance_model_tab_chart', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 1');
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'finance_model_tab_help');
        $this->dropColumn($this->table, 'finance_model_tab_chart');
    }
}
