<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200512_083514_alter_contractor_change_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%contractor}}', 'BIN',  $this->string(15));
        $this->alterColumn('{{%contractor}}', 'ITN',  $this->string(12));
    }

    public function safeDown()
    {
        $this->alterColumn('{{%contractor}}', 'BIN',  $this->string(15)->notNull());
        $this->alterColumn('{{%contractor}}', 'ITN',  $this->string(12)->notNull());
    }
}
