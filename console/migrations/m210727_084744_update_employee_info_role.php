<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210727_084744_update_employee_info_role extends Migration
{
    public function safeUp()
    {
        $this->update('{{%employee_info_role}}', [
            'sort' => 15,
        ], [
            'id' => 9,
        ]);
    }

    public function safeDown()
    {
        $this->update('{{%employee_info_role}}', [
            'sort' => 30,
        ], [
            'id' => 9,
        ]);
    }
}
