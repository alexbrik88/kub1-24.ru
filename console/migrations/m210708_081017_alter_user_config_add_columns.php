<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210708_081017_alter_user_config_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%user_config}}', 'crm_task_contractor', $this->boolean()->notNull()->defaultValue('0'));
        $this->addColumn('{{%user_config}}', 'crm_task_contact', $this->boolean()->notNull()->defaultValue('1'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%user_config}}', 'crm_task_contractor');
        $this->dropColumn('{{%user_config}}', 'crm_task_contact');
    }
}
