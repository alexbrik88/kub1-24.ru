<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%company_page_first_time}}`.
 */
class m200309_082558_create_company_page_first_time_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%company_page_first_time}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'action_unique_id' => $this->string()->notNull(),
            'created_date' => $this->date()->notNull(),
            'created_at' => $this->integer()->notNull(),
        ]);

        $this->createIndex('UI_company_id_action_unique_id', '{{%company_page_first_time}}', [
            'company_id',
            'action_unique_id',
        ], true);
        $this->addForeignKey(
            'FK_company_page_first_time_company',
            '{{%company_page_first_time}}',
            'company_id',
            '{{%company}}',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%company_page_first_time}}');
    }
}
