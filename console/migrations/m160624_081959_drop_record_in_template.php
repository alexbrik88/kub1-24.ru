<?php

use yii\db\Migration;

class m160624_081959_drop_record_in_template extends Migration
{
    private $_table = '{{%template}}';
    private $_columnSequence = 'sequence';
    private $_valueColumnSequence = 5;
    private $_template;

    public function safeUp()
    {
        $condition = $this->_columnSequence.'= "'.$this->_valueColumnSequence.'"';
        
     //   $this->delete($this->_table, $condition);
        
    }
    
    public function safeDown()
    {
        $this->_template = [
            $this->_columnSequence  =>  $this->_valueColumnSequence,
            'title' => 'Мои шаблоны',
        ];

        $this->insert($this->_table, $this->_template);
    }
}
