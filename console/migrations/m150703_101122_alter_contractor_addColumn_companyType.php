<?php

use yii\db\Schema;
use yii\db\Migration;

class m150703_101122_alter_contractor_addColumn_companyType extends Migration
{
    public function safeUp()
    {
        $this->addColumn('contractor', 'company_type_id', Schema::TYPE_INTEGER . ' NOT NULL AFTER `status`');
        $this->update('contractor', [
            'company_type_id' => 2, // ООО
        ]);
        $this->addForeignKey('fk_contractor_to_company_type', 'contractor', 'company_type_id', 'company_type', 'id');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('fk_contractor_to_company_type', 'contractor');
        $this->dropColumn('contractor', 'company_type_id');
    }
}
