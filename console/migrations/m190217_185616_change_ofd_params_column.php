<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190217_185616_change_ofd_params_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%ofd_params}}', 'param_value', $this->text());
    }

    public function safeDown()
    {
        $this->alterColumn('{{%ofd_params}}', 'param_value', $this->string(255));
    }
}