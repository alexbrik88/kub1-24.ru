<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210820_165317_alter_config_add_column extends Migration
{
    public $table = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->table, 'cash_pieces_recognition_date', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 1');
        $this->addColumn($this->table, 'cash_pieces_invoices_list', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 1');
        $this->addColumn($this->table, 'cash_pieces_project', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 1');
        $this->addColumn($this->table, 'cash_pieces_sale_point', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table, 'cash_pieces_direction', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'cash_pieces_recognition_date');
        $this->dropColumn($this->table, 'cash_pieces_invoices_list');
        $this->dropColumn($this->table, 'cash_pieces_project');
        $this->dropColumn($this->table, 'cash_pieces_sale_point');
        $this->dropColumn($this->table, 'cash_pieces_direction');
    }
}
