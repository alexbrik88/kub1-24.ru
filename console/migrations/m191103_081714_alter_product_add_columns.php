<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191103_081714_alter_product_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%product}}', 'log_import_product_id', $this->integer()->after('status'));
        $this->addColumn('{{%product}}', 'import_row_id', $this->integer()->after('log_import_product_id'));
        $this->addColumn('{{%product}}', 'import_type_id', $this->tinyInteger(1)->after('import_row_id'));

        $this->addForeignKey(
            'fk_product_log_import_product',
            '{{%product}}',
            'log_import_product_id',
            '{{%log_import_product}}',
            'id'
        );
        $this->createIndex(
            'import_row_id',
            '{{%product}}',
            'import_row_id'
        );
        $this->createIndex(
            'import_type_id',
            '{{%product}}',
            'import_type_id'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_product_log_import_product', '{{%product}}');

        $this->dropColumn('{{%product}}', 'import_type_id');
        $this->dropColumn('{{%product}}', 'import_row_id');
        $this->dropColumn('{{%product}}', 'log_import_product_id');
    }
}
