<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210314_185338_create_table_analytics_article extends Migration
{
    public $table = 'analytics_article';
    public $PK = 'PK_analytics_article';
    public $FK = 'FK_analytics_article_company_id';

    public function safeUp()
    {
        $this->createTable($this->table, [
            'company_id' => $this->integer()->notNull(),
            'type' => $this->tinyInteger()->notNull(),
            'item_id' => $this->integer()->notNull(),
            'profit_and_loss_type' => $this->tinyInteger()->notNull()->defaultValue(0)
        ]);

        $this->addPrimaryKey($this->PK, $this->table, ['company_id', 'type', 'item_id']);
        $this->addForeignKey($this->FK, $this->table, 'company_id', 'company', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey($this->FK, $this->table);
        $this->dropTable($this->table);
    }

    /* Test

    EXPLAIN SELECT
      t.type,
      t.item_id,
      a.profit_and_loss_type,
      DATE_FORMAT(t.recognition_date, '%m') m,
      DATE_FORMAT(t.recognition_date, '%Y') y,
      SUM(t.amount) amount,
      SUM(IF(t.is_accounting, t.amount, 0)) amountTax
    FROM `olap_flows` t
    LEFT JOIN `analytics_article` a ON t.company_id = a.company_id AND t.type = a.type AND t.item_id = a.item_id
    WHERE  t.company_id = 77
      AND t.recognition_date BETWEEN '2020-01-01' AND '2021-12-31'
      AND (t.has_invoice = FALSE OR t.has_doc = FALSE OR t.need_doc = FALSE)
    GROUP BY t.type, t.item_id, YEAR(t.recognition_date), MONTH(t.recognition_date);
    */

    /*
    INSERT INTO `analytics_article` (`company_id`, `type`, `item_id`, `profit_and_loss_type`)
    SELECT `company_id`, 1 AS `type`, `income_item_id`, 1 AS `profit_and_loss_type` FROM `income_item_flow_of_funds`;

    INSERT INTO `analytics_article` (`company_id`, `type`, `item_id`, `profit_and_loss_type`)
    SELECT `company_id`, 0 AS `type`, `expense_item_id`, 1 AS `profit_and_loss_type` FROM `expense_item_flow_of_funds`;
    */
}

