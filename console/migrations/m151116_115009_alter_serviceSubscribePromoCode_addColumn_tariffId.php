<?php

use yii\db\Schema;
use yii\db\Migration;

class m151116_115009_alter_serviceSubscribePromoCode_addColumn_tariffId extends Migration
{
    public $tPromoCode = 'service_payment_promo_code';
    public $tTariff = 'service_subscribe_tariff';

    public function safeUp()
    {
        $this->addColumn($this->tPromoCode, 'tariff_id', $this->integer()->notNull() . ' AFTER id');

        $this->addForeignKey('fk_service_payment_promo_code_to_tariff', $this->tPromoCode, 'tariff_id', $this->tTariff, 'id');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('fk_service_payment_promo_code_to_tariff', $this->tPromoCode);

        $this->dropColumn($this->tPromoCode, 'tariff_id');
    }
}
