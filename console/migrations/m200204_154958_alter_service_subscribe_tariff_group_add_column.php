<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200204_154958_alter_service_subscribe_tariff_group_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%service_subscribe_tariff_group}}', 'icon', $this->string());

        $this->update('{{%service_subscribe_tariff_group}}', [
            'icon' => 'template',
        ], ['id' => 1]);
        $this->update('{{%service_subscribe_tariff_group}}', [
            'icon' => 'delivery',
        ], ['id' => 2]);
        $this->update('{{%service_subscribe_tariff_group}}', [
            'icon' => 'bank',
        ], ['id' => 3]);
        $this->update('{{%service_subscribe_tariff_group}}', [
            'icon' => 'percentage',
        ], ['id' => 4]);
        $this->update('{{%service_subscribe_tariff_group}}', [
            'icon' => 'chart',
        ], ['id' => 5]);
        $this->update('{{%service_subscribe_tariff_group}}', [
            'icon' => 'description',
        ], ['id' => 6]);
        $this->update('{{%service_subscribe_tariff_group}}', [
            'icon' => 'mix',
        ], ['id' => 7]);
        $this->update('{{%service_subscribe_tariff_group}}', [
            'icon' => 'find',
        ], ['id' => 8]);
        $this->update('{{%service_subscribe_tariff_group}}', [
            'icon' => 'recognize',
        ], ['id' => 9]);
    }

    public function safeDown()
    {
        $this->dropColumn('{{%service_subscribe_tariff_group}}', 'icon');
    }
}
