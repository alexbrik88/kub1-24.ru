<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180727_132750_alter_invoice_add_columns extends Migration
{
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE `invoice`
                ADD COLUMN `has_services` SMALLINT(1) NOT NULL DEFAULT '0' AFTER `contract_essence`,
                ADD COLUMN `has_goods` SMALLINT(1) NOT NULL DEFAULT '0' AFTER `has_services`,
                ADD COLUMN `has_act` SMALLINT(1) NOT NULL DEFAULT '0' AFTER `has_goods`,
                ADD COLUMN `has_packing_list` SMALLINT(1) NOT NULL DEFAULT '0' AFTER `has_act`,
                ADD COLUMN `has_invoice_facture` SMALLINT(1) NOT NULL DEFAULT '0' AFTER `has_packing_list`,
                ADD COLUMN `has_upd` SMALLINT(1) NOT NULL DEFAULT '0' AFTER `has_invoice_facture`,
                ADD COLUMN `has_file` SMALLINT(1) NOT NULL DEFAULT '0' AFTER `has_upd`,
                ADD COLUMN `can_add_act` SMALLINT(1) NOT NULL DEFAULT '1' AFTER `has_file`,
                ADD COLUMN `can_add_packing_list` SMALLINT(1) NOT NULL DEFAULT '1' AFTER `can_add_act`,
                ADD COLUMN `can_add_invoice_facture` SMALLINT(1) NOT NULL DEFAULT '1' AFTER `can_add_packing_list`,
                ADD COLUMN `can_add_upd` SMALLINT(1) NOT NULL DEFAULT '1' AFTER `can_add_invoice_facture`,
                ADD INDEX `has_services` (`has_services`),
                ADD INDEX `has_goods` (`has_goods`),
                ADD INDEX `has_act` (`has_act`),
                ADD INDEX `has_packing_list` (`has_packing_list`),
                ADD INDEX `has_invoice_facture` (`has_invoice_facture`),
                ADD INDEX `has_upd` (`has_upd`),
                ADD INDEX `has_file` (`has_file`);
        ");
    }

    public function safeDown()
    {
        $this->execute("
            ALTER TABLE `invoice`
                DROP COLUMN `has_services`,
                DROP COLUMN `has_goods`,
                DROP COLUMN `has_act`,
                DROP COLUMN `has_packing_list`,
                DROP COLUMN `has_invoice_facture`,
                DROP COLUMN `has_upd`,
                DROP COLUMN `has_file`,
                DROP COLUMN `can_add_act`,
                DROP COLUMN `can_add_packing_list`,
                DROP COLUMN `can_add_invoice_facture`,
                DROP COLUMN `can_add_upd`;
        ");
    }
}
