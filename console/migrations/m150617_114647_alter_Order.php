<?php

use yii\db\Schema;
use yii\db\Migration;

class m150617_114647_alter_Order extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('order', 'unit_id', Schema::TYPE_INTEGER . ' DEFAULT NULL');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_order_to_product_unit', 'order');
        $this->alterColumn('order', 'unit_id', Schema::TYPE_INTEGER . ' NOT NULL');
        $this->addForeignKey('order_to_product_unit', 'order', 'unit_id', 'product_unit', 'id');
    }
}
