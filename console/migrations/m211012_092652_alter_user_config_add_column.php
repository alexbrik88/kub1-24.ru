<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211012_092652_alter_user_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%user_config}}', 'crm_task_hide_completed', $this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%user_config}}', 'crm_task_hide_completed');
    }
}
