<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180302_142652_alter_invoice_facture_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('invoice_facture', 'consignor_id', $this->integer());
        $this->addColumn('invoice_facture', 'consignee_id', $this->integer());

        $this->addForeignKey('FK_invoiceFactureConsignor_contractor', 'invoice_facture', 'consignor_id', 'contractor', 'id');
        $this->addForeignKey('FK_invoiceFactureConsignee_contractor', 'invoice_facture', 'consignee_id', 'contractor', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_invoiceFactureConsignor_contractor', 'invoice_facture');
        $this->dropForeignKey('FK_invoiceFactureConsignee_contractor', 'invoice_facture');

        $this->dropColumn('invoice_facture', 'consignor_id');
        $this->dropColumn('invoice_facture', 'consignee_id');
    }
}
