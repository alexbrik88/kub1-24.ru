<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180523_132022_alter_invoice_add_columns extends Migration
{
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE {{invoice}}
                ADD COLUMN [[view_total_base]] BIGINT(20) NOT NULL DEFAULT '0' AFTER [[total_order_count]],
                ADD COLUMN [[view_total_amount]] BIGINT(20) NOT NULL DEFAULT '0' AFTER [[view_total_base]],
                ADD COLUMN [[view_total_no_nds]] BIGINT(20) NOT NULL DEFAULT '0' AFTER [[view_total_amount]],
                ADD COLUMN [[view_total_nds]] BIGINT(20) NOT NULL DEFAULT '0' AFTER [[view_total_no_nds]],
                ADD COLUMN [[view_total_with_nds]] BIGINT(20) NOT NULL DEFAULT '0' AFTER [[view_total_nds]];
        ");

        $this->execute("
            UPDATE {{invoice}}
            INNER JOIN (
                SELECT [[invoice_id]],
                    SUM([[currency_price_no_vat]] * [[quantity]]) AS [[no_nds]],
                    SUM([[currency_price_with_vat]] * [[quantity]]) AS [[with_nds]]
                FROM {{order}}
                GROUP BY [[invoice_id]]
            ) {{order_sum}} ON {{invoice}}.[[id]] = {{order_sum}}.[[invoice_id]]
            SET {{invoice}}.[[view_total_base]] = IF(
                    {{invoice}}.[[nds_view_type_id]] = 1, {{order_sum}}.[[no_nds]], {{order_sum}}.[[with_nds]]
                ),
                {{invoice}}.[[view_total_amount]] = IF(
                    {{invoice}}.[[nds_view_type_id]] = 1,
                    ROUND({{invoice}}.[[total_amount_no_nds]] * {{invoice}}.[[currency_amount]] / {{invoice}}.[[currency_rate]]),
                    ROUND({{invoice}}.[[total_amount_with_nds]] * {{invoice}}.[[currency_amount]] / {{invoice}}.[[currency_rate]])
                ),
                {{invoice}}.[[view_total_no_nds]] = ROUND(
                    {{invoice}}.[[total_amount_no_nds]] * {{invoice}}.[[currency_amount]] / {{invoice}}.[[currency_rate]]
                ),
                {{invoice}}.[[view_total_nds]] = ROUND(
                    {{invoice}}.[[total_amount_nds]] * {{invoice}}.[[currency_amount]] / {{invoice}}.[[currency_rate]]
                ),
                {{invoice}}.[[view_total_with_nds]] = ROUND(
                    {{invoice}}.[[total_amount_with_nds]] * {{invoice}}.[[currency_amount]] / {{invoice}}.[[currency_rate]]
                )
        ");
    }

    public function safeDown()
    {
        $this->execute("
            ALTER TABLE {{invoice}}
                DROP COLUMN [[view_total_base]],
                DROP COLUMN [[view_total_amount]],
                DROP COLUMN [[view_total_no_nds]],
                DROP COLUMN [[view_total_nds]],
                DROP COLUMN [[view_total_with_nds]];
        ");
    }
}
