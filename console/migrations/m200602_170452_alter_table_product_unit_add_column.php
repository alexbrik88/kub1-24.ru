<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200602_170452_alter_table_product_unit_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%product_unit}}', 'intl_short_name', $this->string()->defaultValue('-')->comment('Международное сокращение'));
        $this->addColumn('{{%product_unit}}', 'intl_code', $this->string()->comment('Международный код'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%product_unit}}', 'intl_short_name');
        $this->dropColumn('{{%product_unit}}', 'intl_code');
    }
}
