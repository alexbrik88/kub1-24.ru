<?php

use yii\db\Migration;

class m210517_123621_alter_crm_client_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%crm_client}}';

    /**
     * @inheritDoc
     */
    public function up(): void
    {
        $this->addColumn(self::TABLE_NAME, 'messenger_type', $this->bigInteger()->null()->after('contractor_id'));
        $this->addColumn(self::TABLE_NAME, 'messenger_account', $this->string(255)->notNull()->defaultValue('')->after('messenger_type'));
        $this->addColumn(self::TABLE_NAME, 'additional_phone', $this->string(45)->notNull()->defaultValue('')->after('messenger_account'));
    }

    /**
     * @inheritDoc
     */
    public function down(): void
    {
        $this->dropColumn(self::TABLE_NAME, 'messenger_type');
        $this->dropColumn(self::TABLE_NAME, 'messenger_account');
        $this->dropColumn(self::TABLE_NAME, 'additional_phone');
    }
}
