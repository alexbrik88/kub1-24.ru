<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180120_231600_update_contractor_current_account extends Migration
{
    public $tableName = 'contractor';

    public function safeUp()
    {
        $this->alterColumn($this->tableName, 'current_account', $this->string(35)->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->alterColumn($this->tableName, 'current_account', $this->string(20)->defaultValue(null));
    }
}


