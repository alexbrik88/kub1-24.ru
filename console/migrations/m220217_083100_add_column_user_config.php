<?php

use console\components\db\Migration;
use yii\db\Schema;

class m220217_083100_add_column_user_config extends Migration
{
    public $table = 'user_config';
    public function safeUp()
    {
        $this->addColumn($this->table, 'document_date', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 1');
        $this->addColumn($this->table, 'document_type_id', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 1');
        $this->addColumn($this->table, 'document_name' , Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 1');
        $this->addColumn($this->table, 'agreement_template_id' , Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 1');
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'document_date');
        $this->dropColumn($this->table, 'document_type_id');
        $this->dropColumn($this->table, 'document_name');
        $this->dropColumn($this->table, 'agreement_template_id');
    }
}
