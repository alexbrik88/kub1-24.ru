<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170823_090326_alter_invoice_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%invoice}}', 'contractor_director_post_name', $this->string() . ' AFTER [[contractor_director_name]]');
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%invoice}}', 'contractor_director_post_name');
    }
}
