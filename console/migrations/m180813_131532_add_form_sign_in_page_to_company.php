<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180813_131532_add_form_sign_in_page_to_company extends Migration
{
    public $tableName = 'company';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'form_sign_in_page', $this->string()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'form_sign_in_page');
    }
}


