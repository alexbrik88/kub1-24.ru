<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200312_090125_alter_user_config_add_column extends Migration
{
    public $table = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->table, 'table_view_report_client', $this->integer()->notNull()->defaultValue(0));
        $this->addColumn($this->table, 'table_view_report_employee', $this->integer()->notNull()->defaultValue(0));
        $this->addColumn($this->table, 'table_view_report_invoice', $this->integer()->notNull()->defaultValue(0));
        $this->addColumn($this->table, 'table_view_report_supplier', $this->integer()->notNull()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'table_view_report_client');
        $this->dropColumn($this->table, 'table_view_report_employee');
        $this->dropColumn($this->table, 'table_view_report_invoice');
        $this->dropColumn($this->table, 'table_view_report_supplier');
    }
}
