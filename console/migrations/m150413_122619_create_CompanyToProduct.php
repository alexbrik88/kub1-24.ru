<?php

use yii\db\Schema;
use yii\db\Migration;

class m150413_122619_create_CompanyToProduct extends Migration
{
    public function up()
    {
        $this->createTable('company_to_product', [
            'id' => Schema::TYPE_PK,
            'company_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'product_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);

        $this->addForeignKey('company_rel', 'company_to_product', 'company_id', 'company', 'id');
        $this->addForeignKey('product_rel', 'company_to_product', 'product_id', 'product', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('company_rel', 'company_to_product');
        $this->dropForeignKey('product_rel', 'company_to_product');
        $this->dropTable('company_to_product');
    }
}
