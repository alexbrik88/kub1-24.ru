<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210223_141932_update_product_turnover extends Migration
{
    public function safeUp()
    {
        $this->execute('
            UPDATE `product_turnover`
            LEFT JOIN `product` ON `product_turnover`.`product_id`=`product`.`id`
            SET `product_turnover`.`purchase_price` = IFNULL(`product`.`price_for_buy_with_nds`, 0),
                `product_turnover`.`purchase_amount` = ROUND(`product_turnover`.`quantity` * IFNULL(`product`.`price_for_buy_with_nds`, 0)),
                `product_turnover`.`margin` = `product_turnover`.`total_amount` - ROUND(`product_turnover`.`quantity` * IFNULL(`product`.`price_for_buy_with_nds`, 0))
            WHERE `product_turnover`.`type` = 2
            AND `product_turnover`.`production_type` = 0
        ');
    }

    public function safeDown()
    {
        //
    }
}
