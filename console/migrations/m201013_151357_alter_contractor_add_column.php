<?php

use common\models\Company;
use common\models\Contractor;
use console\components\db\Migration;
use yii\db\Schema;

class m201013_151357_alter_contractor_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('contractor', 'opposite_id', $this->integer());
        $this->addForeignKey('FK_contractor_id_to_opposite_id', 'contractor', 'opposite_id', 'contractor', 'id', 'SET NULL');

        // set opposite_id
        /*foreach (Company::find()->select('id')->column() as $companyId) {

            foreach (Contractor::find()->select('id')->where(['company_id' => $companyId])->column() as $contractorId) {

                if ($contractor = Contractor::findOne($contractorId)) {

                    if ($oppositeType = self::getOppositeType($contractor->type)) {

                        if ($oppositeId = self::getOppositeId($contractor, $oppositeType)) {

                            $this->execute("UPDATE `contractor` SET `opposite_id` = {$oppositeId} WHERE `id` = {$contractorId} LIMIT 1 ");
                        }
                    }
                }
            }
        }*/
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_contractor_id_to_opposite_id', 'contractor');
        $this->dropColumn('contractor', 'opposite_id');
    }

    /////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    /**
     * @param $type
     * @return int|null
     */
    private static function getOppositeType($type)
    {
        if ($type == 1) return 2;
        if ($type == 2) return 1;

        return null;
    }

    /**
     * @param $contractor
     * @param $oppositeType
     * @return int
     */
    private static function getOppositeId($contractor, $oppositeType)
    {
        $query = Contractor::find()->andWhere([
            'company_id' => $contractor->company_id,
            'company_type_id' => $contractor->company_type_id,
            'type' => $oppositeType,
            'is_deleted' => false,
        ]);

        if ($contractor->ITN) {
            $query->andWhere(['ITN' => $contractor->ITN]);
        } else {
            $query->andWhere(['name' => $contractor->name]);
        }

        if (strlen($contractor->PPC) > 0) {
            $query->andWhere(['PPC' => $contractor->PPC]);
        } else {
            $query->andWhere(['or', ['PPC' => ''], ['PPC' => null]]);
        }

        return (int)$query->select('id')->scalar();
    }
}
