<?php

use console\components\db\Migration;

class m200110_105430_amocrm_customer_employee_id_drop extends Migration
{
    const TABLE = '{{%amocrm_customer}}';

    public function safeUp()
    {
        $this->dropForeignKey('amocrm_customer_employee_id', self::TABLE);
        $this->dropColumn(self::TABLE, 'employee_id');
    }

    public function safeDown()
    {
        $this->delete(self::TABLE);
        $this->addColumn(self::TABLE, 'employee_id', $this->string(11)->notNull()->after('id')->comment('ID клиента КУБ'));
        $this->addForeignKey(
            'amocrm_customer_employee_id', self::TABLE, 'employee_id',
            '{{%employee}}', 'id',
            'CASCADE', 'CASCADE'
        );
    }
}
