<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180212_081700_insert_store extends Migration
{
    public function safeUp()
    {
        $this->execute("
            INSERT INTO {{store}} ([[company_id]], [[is_main]], [[name]])
            SELECT [[id]], 1, 'Склад'
            FROM {{company}}
        ");
    }

    public function safeDown()
    {
        $this->execute("
            DELETE FROM {{store}}
        ");
    }
}
