<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210810_055224_alter_user_config_change_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('user_config', 'act_scan', $this->boolean()->notNull()->defaultValue(true));
        $this->execute("
            UPDATE `user_config` SET `act_scan` = 1;
        ");
    }

    public function safeDown()
    {
        // no down
    }
}
