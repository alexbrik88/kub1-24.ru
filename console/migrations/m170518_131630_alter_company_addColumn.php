<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170518_131630_alter_company_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%company}}', 'pdf_act_signed', $this->boolean()->defaultValue(false) . ' AFTER [[pdf_signed]]');
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%company}}', 'pdf_act_signed');
    }
}
