<?php

use yii\db\Schema;
use yii\db\Migration;

class m150618_140247_update_PackingListStatus extends Migration
{

    public function safeUp()
    {
        $this->update('packing_list_status', [
            'name' => 'Подписана',
        ], [
            'id' => 4,
        ]);
    }
    
    public function safeDown()
    {
        $this->update('packing_list_status', [
            'name' => 'Получена-подписана',
        ], [
            'id' => 4,
        ]);
    }
}
