<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211030_172256_alter_finance_plan_items_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('finance_plan_odds_expenditure_item', 'balance_article_id', $this->integer()->after('credit_id'));
        $this->addColumn('finance_plan_odds_income_item', 'balance_article_id', $this->integer()->after('credit_id'));

        $this->addForeignKey('FK_finance_plan_odds_expenditure_item_balance_article', 'finance_plan_odds_expenditure_item', 'balance_article_id', 'finance_plan_balance_article', 'id', 'CASCADE');
        $this->addForeignKey('FK_finance_plan_odds_income_item_balance_article', 'finance_plan_odds_income_item', 'balance_article_id', 'finance_plan_balance_article', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_finance_plan_odds_expenditure_item_balance_article', 'finance_plan_odds_expenditure_item');
        $this->dropForeignKey('FK_finance_plan_odds_income_item_balance_article', 'finance_plan_odds_income_item');

        $this->dropColumn('finance_plan_odds_expenditure_item', 'balance_article_id');
        $this->dropColumn('finance_plan_odds_income_item', 'balance_article_id');
    }
}
