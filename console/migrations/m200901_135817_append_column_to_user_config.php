<?php

use console\components\db\Migration;

class m200901_135817_append_column_to_user_config extends Migration
{
    public $tableName = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'act_stat_by_payment', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'act_stat_by_status', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn($this->tableName, 'act_stat_by_calculate_nds', $this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'act_stat_by_payment');
        $this->dropColumn($this->tableName, 'act_stat_by_status');
        $this->dropColumn($this->tableName, 'act_stat_by_calculate_nds');
    }

}
