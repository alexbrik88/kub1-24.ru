<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180205_185249_insert_cashbox extends Migration
{
    public function safeUp()
    {
        $this->execute("
            INSERT INTO {{cashbox}} ([[company_id]], [[is_main]], [[name]])
            SELECT [[id]], 1, 'Касса'
            FROM {{company}}
        ");
    }

    public function safeDown()
    {
        $this->execute("
            DELETE FROM {{cashbox}}
        ");
    }
}
