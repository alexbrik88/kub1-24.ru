<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191202_131045_alter_online_payment_type_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('online_payment_type', 'site', $this->string()->after('name'));
    }

    public function safeDown()
    {
        $this->dropColumn('online_payment_type', 'site');
    }
}
