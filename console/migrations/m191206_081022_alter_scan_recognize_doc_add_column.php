<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191206_081022_alter_scan_recognize_doc_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('scan_recognize_document', 'document_type', $this->string(16)->after('scan_recognize_id'));
        $this->addColumn('scan_recognize_document', 'io_type', $this->tinyInteger()->after('scan_recognize_id'));
    }

    public function safeDown()
    {
        $this->dropColumn('scan_recognize_document', 'document_type');
        $this->dropColumn('scan_recognize_document', 'io_type');
    }
}
