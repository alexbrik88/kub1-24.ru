<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200427_180358_alter_user_config_add_column extends Migration
{
    public $table = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->table,'report_profit_loss_chart',$this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table,'report_profit_loss_chart');
    }
}
