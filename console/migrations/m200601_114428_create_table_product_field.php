<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200601_114428_create_table_product_field extends Migration
{
    public function safeUp()
    {
        $this->createTable('product_field', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull()->defaultValue(0),
            'title' => $this->string(45)->notNull(),
        ]);

        $this->addForeignKey('FK_product_field_to_company', 'product_field','company_id', 'company', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_product_field_to_company', 'product_field');
        $this->dropTable('product_field');
    }
}
