<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211104_195419_update_act_order_nds extends Migration
{
    public function safeUp()
    {
        $this->addColumn('upd', 'order_nds', $this->bigInteger()->defaultValue(0)->after('orders_sum'));
        $this->addColumn('sales_invoice', 'order_nds', $this->bigInteger()->defaultValue(0)->after('orders_sum'));
        $this->addColumn('packing_list', 'order_nds', $this->bigInteger()->defaultValue(0)->after('orders_sum'));

        $this->execute('
            UPDATE `act` 
            JOIN (
                SELECT ou.act_id, SUM((o.purchase_price_with_vat - o.purchase_price_no_vat) * ou.quantity) AS summ
                FROM `order_act` ou
                LEFT JOIN `order` `o` ON ou.order_id = o.id
                GROUP BY act_id
            ) t ON t.act_id = act.id
            SET `act`.`order_nds` = t.summ
            WHERE act.type = 1
        ');

        $this->execute('
            UPDATE `act` 
            JOIN (
                SELECT ou.act_id, SUM((o.selling_price_with_vat - o.selling_price_no_vat) * ou.quantity) AS summ
                FROM `order_act` ou
                LEFT JOIN `order` `o` ON ou.order_id = o.id
                GROUP BY act_id
            ) t ON t.act_id = act.id
            SET `act`.`order_nds` = t.summ
            WHERE act.type = 2
        ');
    }

    public function safeDown()
    {
        $this->dropColumn('upd', 'order_nds');
        $this->dropColumn('sales_invoice', 'order_nds');
        $this->dropColumn('packing_list', 'order_nds');
    }
}
