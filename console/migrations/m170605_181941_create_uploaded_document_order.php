<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170605_181941_create_uploaded_document_order extends Migration
{
    public $tableName = 'uploaded_document_order';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'uploaded_document_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'count' => $this->integer()->notNull(),
            'product_unit_id' => $this->integer()->defaultValue(null),
            'price' => $this->string()->notNull(),
            'amount' => $this->string()->notNull(),
        ]);

        $this->addForeignKey($this->tableName . '_company_id', $this->tableName, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tableName . '_uploaded_document_id', $this->tableName, 'uploaded_document_id', 'uploaded_documents', 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_company_id', $this->tableName);
        $this->dropForeignKey($this->tableName . '_uploaded_document_id', $this->tableName);

        $this->dropTable($this->tableName);
    }
}


