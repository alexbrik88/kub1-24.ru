<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190527_065503_alter_table_file_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('file', 'is_deleted', Schema::TYPE_BOOLEAN . ' DEFAULT 0');
    }

    public function safeDown()
    {
        $this->dropColumn('file', 'is_deleted');
    }
}
