<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210129_143949_alter_application_to_bank_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%application_to_bank}}', 'kpp', $this->string()->notNull()->defaultValue('')->after('inn'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%application_to_bank}}', 'kpp');
    }
}
