<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210826_101759_alter_config_add_column extends Migration
{
    public $table = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->table, 'bank_column_recognition_date', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'bank_column_recognition_date');
    }
}
