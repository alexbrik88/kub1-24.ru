<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160704_125044_update_company_name extends Migration
{
    public $tCompany = 'company';

    public function safeUp()
    {
        $this->alterColumn($this->tCompany, 'name_short', $this->string()->defaultValue(null));
        $this->alterColumn($this->tCompany, 'name_full', $this->string()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->alterColumn($this->tCompany, 'name_short', $this->string()->notNull());
        $this->alterColumn($this->tCompany, 'name_full', $this->string()->notNull());
    }
}


