<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170131_155748_update_serviceSubscribeTariff extends Migration
{
    public function safeUp()
    {
        $this->update('{{%service_subscribe_tariff}}', ['price' => 600], ['duration_month' =>1]);
        $this->update('{{%service_subscribe_tariff}}', ['price' => 2160, 'duration_month' => 4], ['duration_month' => 3]);
        $this->update('{{%service_subscribe_tariff}}', ['price' => 5760, 'duration_month' => 12], ['duration_month' => 6]);
    }
    
    public function safeDown()
    {
        $this->update('{{%service_subscribe_tariff}}', ['price' => 599], ['duration_month' =>1]);
        $this->update('{{%service_subscribe_tariff}}', ['price' => 1647, 'duration_month' => 3], ['duration_month' => 4]);
        $this->update('{{%service_subscribe_tariff}}', ['price' => 2994, 'duration_month' => 6], ['duration_month' => 12]);
    }
}


