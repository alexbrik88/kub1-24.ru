<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181226_134043_update_invoice_income_items extends Migration
{
    public $tableName = 'invoice_income_item';

    public function safeUp()
    {
        $this->insert($this->tableName, [
            'id' => 12,
            'name' => 'Пополнение карты',
        ]);
        $this->insert($this->tableName, [
            'id' => 13,
            'name' => 'Возврат средств из бюджета',
        ]);
    }

    public function safeDown()
    {
        $this->delete($this->tableName, ['id' => 12]);
        $this->delete($this->tableName, ['id' => 13]);
    }
}
