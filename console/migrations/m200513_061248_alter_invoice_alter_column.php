<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200513_061248_alter_invoice_alter_column extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%invoice}}', 'company_chief_post_name',  $this->string());
        $this->alterColumn('{{%invoice}}', 'company_chief_lastname',  $this->string());
        $this->alterColumn('{{%invoice}}', 'company_chief_accountant_lastname',  $this->string());
        $this->alterColumn('{{%invoice}}', 'contractor_bank_name',  $this->string());
        $this->alterColumn('{{%invoice}}', 'signed_by_name',  $this->string());
    }

    public function down()
    {
        $this->alterColumn('{{%invoice}}', 'company_chief_post_name',  $this->string(45));
        $this->alterColumn('{{%invoice}}', 'company_chief_lastname',  $this->string(45));
        $this->alterColumn('{{%invoice}}', 'company_chief_accountant_lastname',  $this->string(45));
        $this->alterColumn('{{%invoice}}', 'contractor_bank_name',  $this->string(45));
        $this->alterColumn('{{%invoice}}', 'signed_by_name',  $this->string(45));
    }
}
