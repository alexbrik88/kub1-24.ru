<?php

use yii\db\Migration;

/**
 * Handles the creation of table `employee_salary_summary`.
 */
class m181207_073043_create_employee_salary_summary_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('employee_salary_summary', [
            'company_id' => $this->integer()->notNull(),
            'date' => $this->date()->notNull(),
            'days_count' => $this->smallInteger()->notNull(),
            'work_days_count' => $this->smallInteger()->notNull(),
            'employees_count' => $this->integer()->notNull(),
            'prepay_date' => $this->date(),
            'pay_date' => $this->date(),
            'total_amount' => $this->bigInteger(20)->defaultValue(0),
            'total_prepay_sum' => $this->bigInteger(20)->defaultValue(0),
            'total_pay_sum' => $this->bigInteger(20)->defaultValue(0),
            'total_expenses' => $this->bigInteger(20)->defaultValue(0),
            'total_tax_ndfl' => $this->bigInteger(20)->defaultValue(0),
            'total_tax_social' => $this->bigInteger(20)->defaultValue(0),
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', 'employee_salary_summary', ['company_id', 'date']);
        $this->addForeignKey('FK_employee_salary_summary_company', 'employee_salary_summary', 'company_id', 'company', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('employee_salary_summary');
    }
}
