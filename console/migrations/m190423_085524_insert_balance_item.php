<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190423_085524_insert_balance_item extends Migration
{
    public $tableName = 'balance_item';

    public function safeUp()
    {
        $this->insert($this->tableName, [
            'id' => 23,
            'name' => 'Депозит по аренде',
            'sort' => 6,
        ]);
    }

    public function safeDown()
    {
        $this->delete($this->tableName, ['id' => 23]);
    }
}
