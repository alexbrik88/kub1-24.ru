<?php

use yii\db\Migration;

class m210325_005549_rename_jobs_table extends Migration
{
    /**
     * @inheritDoc
     */
    public function up()
    {
        $this->renameTable('{{%jobs}}', '{{%import_job_data}}');
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        $this->renameTable('{{%import_job_data}}', '{{%jobs}}');
    }
}
