<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190131_065658_alter_service_subscribe_tariff_rename_column extends Migration
{
    public function safeUp()
    {
        $this->dropForeignKey('FK_service_subscribe_tariff_tariff_group', 'service_subscribe_tariff');

        $this->renameColumn('service_subscribe_tariff', 'group_id', 'tariff_group_id');

        $this->addForeignKey(
            'FK_service_subscribe_tariff_tariff_group',
            'service_subscribe_tariff',
            'tariff_group_id',
            'service_subscribe_tariff_group',
            'id'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_service_subscribe_tariff_tariff_group', 'service_subscribe_tariff');

        $this->renameColumn('service_subscribe_tariff', 'tariff_group_id', 'group_id');

        $this->addForeignKey(
            'FK_service_subscribe_tariff_tariff_group',
            'service_subscribe_tariff',
            'group_id',
            'service_subscribe_tariff_group',
            'id'
        );
    }
}
