<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170627_133731_alter_company_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{company}}', 'documents_type', $this->smallInteger(1)->notNull()->defaultValue(1) . ' AFTER [[nds]]');
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{company}}', 'documents_type');
    }
}
