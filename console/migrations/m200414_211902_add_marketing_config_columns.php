<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200414_211902_add_marketing_config_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_config', 'marketing_help', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn('user_config', 'marketing_chart', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn('user_config', 'table_view_marketing', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
    }

    public function safeDown()
    {
        $this->dropColumn('user_config', 'marketing_help');
        $this->dropColumn('user_config', 'marketing_chart');
        $this->dropColumn('user_config', 'table_view_marketing');
    }
}
