<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180710_141424_add_is_invoice_contract_to_invoice extends Migration
{
    public $tableName = 'invoice';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'is_invoice_contract', $this->boolean()->defaultValue(false));
        $this->addColumn($this->tableName, 'contract_essence', $this->text()->defaultValue(null));

    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'is_invoice_contract');
        $this->dropColumn($this->tableName, 'contract_essence');
    }
}


