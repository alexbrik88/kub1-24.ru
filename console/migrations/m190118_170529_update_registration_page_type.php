<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190118_170529_update_registration_page_type extends Migration
{
    public $tableName = 'registration_page_type';

    public function safeUp()
    {
        $this->delete($this->tableName, ['id' => 14]);
        $this->delete($this->tableName, ['id' => 13]);
        $this->update($this->tableName, ['id' => 13], ['id' => 15]);
    }

    public function safeDown()
    {
        $this->update($this->tableName, ['id' => 15], ['id' => 13]);
        $this->insert($this->tableName, [
            'id' => 13,
            'name' => 'Прайс',
        ]);
        $this->insert($this->tableName, [
            'id' => 14,
            'name' => 'ТТН',
        ]);
    }
}
