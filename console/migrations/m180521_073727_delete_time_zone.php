<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180521_073727_delete_time_zone extends Migration
{
    public function safeUp()
    {
        $this->delete('time_zone', ['id' => [105, 106, 107]]);
    }

    public function safeDown()
    {
        $this->batchInsert('time_zone', ['id', 'out_time_zone', 'time_zone', 'priority'], [
            ["105", "(UTC+13:00) Нукуалофа", "Pacific/Enderbury", "164"],
            ["106", "(UTC+13:00) Самоа", "Pacific/Fakaofo", "165"],
            ["107", "(UTC+14:00) О-в Киритимати", "Pacific/Kiritimati", "166"],
        ]);
    }
}
