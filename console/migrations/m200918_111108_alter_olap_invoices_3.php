<?php

use console\components\db\Migration;
use yii\db\Expression;
use yii\db\Query;
use yii\db\Schema;

class m200918_111108_alter_olap_invoices_3 extends Migration
{
    //////////////////////////////////
    // OPTIMIZE OLAP_INVOICES TRIGGERS
    //////////////////////////////////

    public function safeUp()
    {
        $this->createTriggersOnInvoice();
        $this->createTriggersOnDocsInsert();
        $this->createTriggersOnDocsUpdate();
        $this->createTriggersOnDocsDelete();
    }

    public function safeDown()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_invoices_insert_invoice`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_invoices_update_invoice`');

        $this->execute('DROP TRIGGER IF EXISTS `olap_invoices_insert_invoice_act`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_invoices_insert_packing_list`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_invoices_insert_invoice_upd`');

        $this->execute('DROP TRIGGER IF EXISTS `olap_invoices_delete_invoice_act`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_invoices_delete_packing_list`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_invoices_delete_invoice_upd`');

        $this->execute('DROP TRIGGER IF EXISTS `olap_invoices_update_act`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_invoices_update_packing_list`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_invoices_update_upd`');
    }

    private function createTriggersOnDocsInsert()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_invoices_insert_invoice_act`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_invoices_insert_packing_list`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_invoices_insert_invoice_upd`');

        $this->execute('
            CREATE TRIGGER `olap_invoices_insert_packing_list`
            AFTER INSERT ON `packing_list`
            FOR EACH ROW
            BEGIN   
              DECLARE _doc_date DATE;
              SET _doc_date = (
                SELECT MAX(p.`document_date`) AS `max_date` FROM `packing_list` p WHERE p.`invoice_id` = NEW.`invoice_id`
                UNION ALL
                SELECT MAX(a.`document_date`) AS `max_date` FROM `act` a LEFT JOIN `invoice_act` r ON a.`id` = r.`act_id` WHERE r.`invoice_id` = NEW.`invoice_id`                   
                ORDER BY `max_date` DESC
                LIMIT 1
              );
              UPDATE `olap_invoices` SET `doc_date` = _doc_date WHERE `olap_invoices`.`id` = NEW.`invoice_id`;
            END');

        $this->execute('
            CREATE TRIGGER `olap_invoices_insert_invoice_act`
            AFTER INSERT ON `invoice_act`
            FOR EACH ROW
            BEGIN
              DECLARE _doc_date DATE;
              SET _doc_date = (
                SELECT MAX(p.`document_date`) AS `max_date` FROM `packing_list` p WHERE p.`invoice_id` = NEW.`invoice_id`
                UNION ALL
                SELECT MAX(a.`document_date`) AS `max_date` FROM `act` a LEFT JOIN `invoice_act` r ON a.`id` = r.`act_id` WHERE r.`invoice_id` = NEW.`invoice_id`
                ORDER BY `max_date` DESC 
                LIMIT 1
              );
              UPDATE `olap_invoices` SET `doc_date` = _doc_date WHERE `olap_invoices`.`id` = NEW.`invoice_id`;
            END');

        $this->execute('
            CREATE TRIGGER `olap_invoices_insert_invoice_upd`
            AFTER INSERT ON `invoice_upd`
            FOR EACH ROW
            BEGIN
              DECLARE _doc_date DATE;
              SET _doc_date = (
                SELECT MAX(u.`document_date`) AS `max_date` FROM `upd` u LEFT JOIN `invoice_upd` r ON u.`id` = r.`upd_id` WHERE r.`invoice_id` = NEW.`invoice_id`
                ORDER BY `max_date` DESC
                LIMIT 1
              );
              UPDATE `olap_invoices` SET `doc_date` = _doc_date WHERE `olap_invoices`.`id` = NEW.`invoice_id`;            
            END');
    }

    private function createTriggersOnDocsDelete()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_invoices_delete_invoice_act`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_invoices_delete_packing_list`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_invoices_delete_invoice_upd`');

        $this->execute('
            CREATE TRIGGER `olap_invoices_delete_packing_list`
            AFTER DELETE ON `packing_list`
            FOR EACH ROW
            BEGIN   
              DECLARE _doc_date DATE;
              SET _doc_date = (
                SELECT MAX(p.`document_date`) AS `max_date` FROM `packing_list` p WHERE p.`invoice_id` = OLD.`invoice_id`
                UNION ALL
                SELECT MAX(a.`document_date`) AS `max_date` FROM `act` a LEFT JOIN `invoice_act` r ON a.`id` = r.`act_id` WHERE r.`invoice_id` = OLD.`invoice_id`                   
                ORDER BY `max_date` DESC
                LIMIT 1
              );
              UPDATE `olap_invoices` SET `doc_date` = _doc_date WHERE `olap_invoices`.`id` = OLD.`invoice_id`;
            END');

        $this->execute('
            CREATE TRIGGER `olap_invoices_delete_invoice_act`
            AFTER DELETE ON `invoice_act`
            FOR EACH ROW
            BEGIN
              DECLARE _doc_date DATE;
              SET _doc_date = (
                SELECT MAX(p.`document_date`) AS `max_date` FROM `packing_list` p WHERE p.`invoice_id` = OLD.`invoice_id`
                UNION ALL
                SELECT MAX(a.`document_date`) AS `max_date` FROM `act` a LEFT JOIN `invoice_act` r ON a.`id` = r.`act_id` WHERE r.`invoice_id` = OLD.`invoice_id`
                ORDER BY `max_date` DESC 
                LIMIT 1
              );
              UPDATE `olap_invoices` SET `doc_date` = _doc_date WHERE `olap_invoices`.`id` = OLD.`invoice_id`;
            END');

        $this->execute('
            CREATE TRIGGER `olap_invoices_delete_invoice_upd`
            AFTER DELETE ON `invoice_upd`
            FOR EACH ROW
            BEGIN
              DECLARE _doc_date DATE;
              SET _doc_date = (
                SELECT MAX(u.`document_date`) AS `max_date` FROM `upd` u LEFT JOIN `invoice_upd` r ON u.`id` = r.`upd_id` WHERE r.`invoice_id` = OLD.`invoice_id`
                ORDER BY `max_date` DESC
                LIMIT 1
              );
              UPDATE `olap_invoices` SET `doc_date` = _doc_date WHERE `olap_invoices`.`id` = OLD.`invoice_id`;            
            END');
    }

    private function createTriggersOnInvoice()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_invoices_insert_invoice`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_invoices_update_invoice`');

        $this->execute('
            CREATE TRIGGER `olap_invoices_insert_invoice`
            AFTER INSERT ON `invoice`
            FOR EACH ROW
            BEGIN
            
              INSERT INTO `olap_invoices` (
                `id`,
                `company_id`,
                `contractor_id`,
                `expenditure_item_id`,                
                `type`,
                `date`,
                `total_amount_with_nds`,
                `payment_partial_amount`,
                `status_id`,
                `status_updated_at`,
                `payment_limit_date`,
                `has_doc`,
                `not_need_doc`,
                `need_all_docs`,
                `doc_date`,
                `is_deleted`
              ) VALUES (
                NEW.`id`,
                NEW.`company_id`,
                NEW.`contractor_id`,
                NEW.`invoice_expenditure_item_id`,                
                NEW.`type`,
                NEW.`document_date`,
                NEW.`total_amount_with_nds`,
                NEW.`payment_partial_amount`,
                NEW.`invoice_status_id`,
                NEW.`invoice_status_updated_at`,
                NEW.`payment_limit_date`,
                IF(IF (NEW.has_act, 1, 0) + IF (NEW.has_packing_list, 1, 0) + IF(NEW.has_upd, 1, 0), 1, 0),
                IF(IF (NEW.need_act, 0, 1) + IF (NEW.need_packing_list, 0, 1) + IF(NEW.need_upd, 0, 1), 1, 0),
                IF(IF (NEW.need_act, 1, 0) * IF (NEW.need_packing_list, 1, 0) * IF(NEW.need_upd, 1, 0), 1, 0),
                NULL,
                NEW.`is_deleted`
              );  
            
            END');

        $this->execute('
            CREATE TRIGGER `olap_invoices_update_invoice`
            AFTER UPDATE ON `invoice`
            FOR EACH ROW
            BEGIN
            
              UPDATE `olap_invoices` SET
                `contractor_id` = NEW.`contractor_id`,
                `expenditure_item_id` = NEW.`invoice_expenditure_item_id`,   
                `date` = NEW.`document_date`,
                `total_amount_with_nds` = NEW.`total_amount_with_nds`,
                `payment_partial_amount` = NEW.`payment_partial_amount`,
                `status_id` = NEW.`invoice_status_id`,
                `status_updated_at` = NEW.`invoice_status_updated_at`,
                `payment_limit_date` = NEW.`payment_limit_date`,
                `has_doc` = IF(IF (NEW.has_act, 1, 0) + IF (NEW.has_packing_list, 1, 0) + IF(NEW.has_upd, 1, 0), 1, 0),
                `not_need_doc` = IF(IF (NEW.need_act, 0, 1) + IF (NEW.need_packing_list, 0, 1) + IF(NEW.need_upd, 0, 1), 1, 0),
                `need_all_docs` = IF(IF (NEW.need_act, 1, 0) * IF (NEW.need_packing_list, 1, 0) * IF(NEW.need_upd, 1, 0), 1, 0),
                `is_deleted` = NEW.`is_deleted`
              WHERE `id` = NEW.`id` LIMIT 1;
            
            END');
    }

    private function createTriggersOnDocsUpdate()
    {

        $this->execute('DROP TRIGGER IF EXISTS `olap_invoices_update_act`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_invoices_update_packing_list`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_invoices_update_upd`');

        $this->execute('
            CREATE TRIGGER `olap_invoices_update_packing_list`
            AFTER UPDATE ON `packing_list`
            FOR EACH ROW
            BEGIN
              DECLARE _doc_date DATE;
              SET _doc_date = (
                SELECT MAX(p.`document_date`) AS `max_date` FROM `packing_list` p WHERE p.`invoice_id` = NEW.`invoice_id`
                UNION ALL
                SELECT MAX(a.`document_date`) AS `max_date` FROM `act` a LEFT JOIN `invoice_act` r ON a.`id` = r.`act_id` WHERE r.`invoice_id` = NEW.`invoice_id`                   
                ORDER BY `max_date` DESC
                LIMIT 1
              );
              UPDATE `olap_invoices` SET `doc_date` = _doc_date WHERE `olap_invoices`.`id` = NEW.`invoice_id` AND _doc_date;            
            END');
        $this->execute('
            CREATE TRIGGER `olap_invoices_update_act`
            AFTER UPDATE ON `act`
            FOR EACH ROW
            BEGIN
              DECLARE _doc_date DATE;
              SET _doc_date = (
                SELECT MAX(p.`document_date`) AS `max_date` FROM `packing_list` p WHERE p.`invoice_id` = NEW.`invoice_id`
                UNION ALL
                SELECT MAX(a.`document_date`) AS `max_date` FROM `act` a LEFT JOIN `invoice_act` r ON a.`id` = r.`act_id` WHERE r.`invoice_id` IN (SELECT `invoice_id` FROM `invoice_act` WHERE `act_id` = NEW.`id`)
                ORDER BY `max_date` DESC 
                LIMIT 1
              );
              UPDATE `olap_invoices` SET `doc_date` = _doc_date WHERE `olap_invoices`.`id` IN (SELECT `invoice_id` FROM `invoice_act` WHERE `act_id` = NEW.`id`);            
            END');
        $this->execute('
            CREATE TRIGGER `olap_invoices_update_upd`
            AFTER UPDATE ON `upd`
            FOR EACH ROW
            BEGIN
              DECLARE _doc_date DATE;
              SET _doc_date = (
                SELECT MAX(u.`document_date`) AS `max_date` FROM `upd` u LEFT JOIN `invoice_upd` r ON u.`id` = r.`upd_id` WHERE r.`invoice_id` IN (SELECT `invoice_id` FROM `invoice_upd` WHERE `upd_id` = NEW.`id`)
                ORDER BY `max_date` DESC
                LIMIT 1
              );
              UPDATE `olap_invoices` SET `doc_date` = _doc_date WHERE `olap_invoices`.`id` IN (SELECT `invoice_id` FROM `invoice_upd` WHERE `upd_id` = NEW.`id`);            
            END');
    }
}