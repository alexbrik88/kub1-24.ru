<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190627_050123_insert_service_subscribe_tariff_group extends Migration
{
    public function safeUp()
    {
        $this->insert('service_subscribe_tariff_group', [
            'id' => 6,
            'name' => 'Нулевая отчетность ООО на ОСНО',
            'is_base' => false,
            'has_base' => false,
            'is_active' => false,
            'description' => 'Автоматическая подготовка отчетности в налоговую|Автоматическая подготовка отчетности по сотрудникам|Подготовка описи|Напоминания о сроках отчетности',
        ]);
    }

    public function safeDown()
    {
        $this->delete('service_subscribe_tariff_group', ['id' => 6]);
    }
}
