<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210329_173721_alter_user_config_change_column_default_value extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('user_config', 'invoice_waybill', $this->boolean()->notNull()->defaultValue(false));
        $this->alterColumn('user_config', 'contr_inv_waybill', $this->boolean()->notNull()->defaultValue(false));

        $this->execute('UPDATE `user_config` SET `invoice_waybill` = 0, `contr_inv_waybill` = 0');
    }

    public function safeDown()
    {
        $this->alterColumn('user_config', 'invoice_waybill', $this->boolean()->notNull()->defaultValue(true));
        $this->alterColumn('user_config', 'contr_inv_waybill', $this->boolean()->notNull()->defaultValue(true));

        $this->execute('UPDATE `user_config` SET `invoice_waybill` = 1, `contr_inv_waybill` = 1');
    }
}
