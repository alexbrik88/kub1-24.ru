<?php

use console\components\db\Migration;

class m160605_145228_alter_colum_current_account_to_contractor extends Migration
{
    public $tContractor = 'contractor';

    public function safeUp()
    {
        $this->alterColumn($this->tContractor, 'current_account', $this->char(20));
    }

    public function safeDown()
    {
        $this->alterColumn($this->tContractor, 'current_account', $this->char(20)->notNull());
    }
}


