<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200708_080223_drop_table_project_expenditure_item extends Migration
{
    public $table = 'project_expenditure_item';

    public function safeUp()
    {
        try {
            $this->dropForeignKey('project_expenditure_item_project_id', $this->table);
            $this->dropForeignKey('project_expenditure_item_expenditure_item_id', $this->table);
        } catch (\Exception $e) {
            echo 'Drop foreign keys fails';
        }
        $this->dropTable($this->table);
    }

    public function safeDown()
    {
        $this->createTable($this->table, [
            'id' => 'pk',
            'project_id' => 'integer',
            'expenditure_item_id' => 'integer',
        ]);
    }
}
