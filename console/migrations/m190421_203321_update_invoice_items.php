<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190421_203321_update_invoice_items extends Migration
{
    public $tInvoiceIncomeItem = 'invoice_income_item';
    public $tInvoiceExpenditureItem = 'invoice_expenditure_item';

    public function safeUp()
    {
        $this->update($this->tInvoiceIncomeItem, ['name' => 'Проценты полученные'], ['id' => 11]);
        $this->update($this->tInvoiceExpenditureItem, ['name' => 'Проценты уплаченные'], ['id' => 38]);
    }

    public function safeDown()
    {
        $this->update($this->tInvoiceIncomeItem, ['name' => 'Полученные проценты'], ['id' => 11]);
        $this->update($this->tInvoiceExpenditureItem, ['name' => 'Уплата процентов'], ['id' => 38]);
    }
}
