<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190619_144452_alter_order_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('order', 'volume', $this->decimal(22, 4)->notNull()->defaultValue(0)->after('markup'));
        $this->addColumn('order', 'weight', $this->decimal(22, 4)->notNull()->defaultValue(0)->after('markup'));
    }

    public function safeDown()
    {
        $this->dropColumn('order', 'volume');
        $this->dropColumn('order', 'weight');
    }
}
