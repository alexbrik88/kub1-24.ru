<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211109_194724_alter_olap_flows_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('olap_flows', 'updated_at', $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')->append('ON UPDATE CURRENT_TIMESTAMP'));
        $this->execute("update `olap_flows` set updated_at = updated_at - INTERVAL (1.8E6 - `id`) SECOND");
        $this->createIndex('IDX_olap_flows_updated_at', 'olap_flows', ['company_id', 'updated_at']);
    }

    public function safeDown()
    {
        $this->dropIndex('IDX_olap_flows_updated_at', 'olap_flows');
        $this->dropColumn('olap_flows', 'updated_at');
    }
}
