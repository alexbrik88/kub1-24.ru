<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190214_172154_alter_invoice_add_column extends Migration
{
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE `invoice`
                ADD COLUMN `has_proxy` SMALLINT(1) NOT NULL DEFAULT '0' AFTER `has_packing_list`,
                ADD COLUMN `can_add_proxy` SMALLINT(1) NOT NULL DEFAULT '1' AFTER `can_add_packing_list`,
                ADD INDEX `has_proxy` (`has_proxy`);
        ");
    }

    public function safeDown()
    {
        $this->execute("
            ALTER TABLE `invoice`
                DROP COLUMN `has_proxy`,
                DROP COLUMN `can_add_proxy`;
        ");
    }
}
