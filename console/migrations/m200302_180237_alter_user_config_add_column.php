<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200302_180237_alter_user_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_config','report_odds_help',$this->boolean()->notNull()->defaultValue(false));
        $this->addColumn('user_config','report_odds_chart',$this->boolean()->notNull()->defaultValue(false));
        $this->addColumn('user_config','report_pc_help',$this->boolean()->notNull()->defaultValue(false));
        $this->addColumn('user_config','report_pc_chart',$this->boolean()->notNull()->defaultValue(false));
        $this->addColumn('user_config','report_expenses_help',$this->boolean()->notNull()->defaultValue(false));
        $this->addColumn('user_config','report_expenses_chart',$this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('user_config','report_odds_help');
        $this->dropColumn('user_config','report_odds_chart');
        $this->dropColumn('user_config','report_pc_help');
        $this->dropColumn('user_config','report_pc_chart');
        $this->dropColumn('user_config','report_expenses_help');
        $this->dropColumn('user_config','report_expenses_chart');
    }
}
