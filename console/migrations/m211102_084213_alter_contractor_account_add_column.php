<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211102_084213_alter_contractor_account_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%contractor_account}}', 'no_mask', $this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%contractor_account}}', 'no_mask');
    }
}
