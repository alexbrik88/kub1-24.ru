<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200222_132821_rework_google_ads_report_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%google_ads_campaign}}', [
            'id' => $this->bigInteger(100)->unsigned()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'status' => $this->string()->notNull(),
            'is_trial_type' => $this->boolean()->defaultValue(false)->notNull(),
        ]);
        $this->addPrimaryKey('id_company', '{{%google_ads_campaign}}', ['id', 'company_id']);
        $this->addForeignKey('FK_google_afs_campaign_to_company', 'google_ads_campaign', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');

        $this->renameTable('{{%google_ads_report}}', '{{%google_ads_campaign_report}}');
        $this->execute("
        INSERT INTO 
                {{%google_ads_campaign}} (
                	[[id]],
                	[[company_id]],
                	[[name]],
                	[[status]]
                )
            SELECT
                {{%google_ads_campaign_report}}.[[campaign_id]],
                {{%google_ads_campaign_report}}.[[company_id]],
                {{%google_ads_campaign_report}}.[[campaign_name]],
                {{%google_ads_campaign_report}}.[[campaign_status]]
            FROM {{%google_ads_campaign_report}}
        GROUP BY {{%google_ads_campaign_report}}.[[campaign_id]]
        ");

        $this->dropColumn('{{%google_ads_campaign_report}}', 'campaign_name');
        $this->dropColumn('{{%google_ads_campaign_report}}', 'campaign_status');

        $this->alterColumn('{{%google_ads_campaign_report}}', 'campaign_id', $this->bigInteger(100)->unsigned()->notNull());
        $this->addForeignKey('FK_google_ads_campaign_report_to_google_ads_campaign', 'google_ads_campaign_report', 'campaign_id', 'google_ads_campaign', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->addColumn('{{%google_ads_campaign_report}}', 'campaign_name', $this->string()->notNull()->after('campaign_id'));
        $this->addColumn('{{%google_ads_campaign_report}}', 'campaign_status', $this->string()->notNull()->after('campaign_name'));
        $this->execute("
            UPDATE {{%google_ads_campaign_report}}
              LEFT JOIN {{%google_ads_campaign}}
                     ON {{%google_ads_campaign}}.[[id]] = {{%google_ads_campaign_report}}.[[campaign_id]]
               SET {{%google_ads_campaign_report}}.[[campaign_name]] = {{%google_ads_campaign}}.[[name]],
                   {{%google_ads_campaign_report}}.[[campaign_status]] = {{%google_ads_campaign}}.[[status]]
        ");
        $this->dropForeignKey('FK_google_ads_campaign_report_to_google_ads_campaign', 'google_ads_campaign_report');
        $this->alterColumn('{{%google_ads_campaign_report}}', 'campaign_id', $this->integer()->notNull());
        $this->renameTable('{{%google_ads_campaign_report}}', '{{%google_ads_report}}');
        $this->dropTable('{{%google_ads_campaign}}');
    }
}
