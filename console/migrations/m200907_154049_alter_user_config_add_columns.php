<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200907_154049_alter_user_config_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_config','report_stocks_help',$this->boolean()->notNull()->defaultValue(false));
        $this->addColumn('user_config','report_stocks_chart',$this->boolean()->notNull()->defaultValue(false));
        $this->addColumn('user_config', 'table_view_finance_stocks', $this->integer()->notNull()->defaultValue(1));
    }

    public function safeDown()
    {
        $this->dropColumn('user_config','report_stocks_help');
        $this->dropColumn('user_config','report_stocks_chart');
        $this->dropColumn('user_config','table_view_finance_stocks');
    }
}
