<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180214_110734_add_number_to_order_document_product extends Migration
{
    public $tableName = 'order_document_product';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'number', $this->integer()->notNull());
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'number');
    }
}


