<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200915_075333_add_column_iotype_to_order_document extends Migration
{
    public $tableName = 'order_document';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'type', $this->integer(1)->defaultValue(2));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'type');
    }
}
