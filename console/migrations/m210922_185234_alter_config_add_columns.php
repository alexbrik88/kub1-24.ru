<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210922_185234_alter_config_add_columns extends Migration
{
    public $table = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->table, 'report_detailing_industry_date', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table, 'report_detailing_industry_type', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'report_detailing_industry_date');
        $this->dropColumn($this->table, 'report_detailing_industry_type');
    }
}
