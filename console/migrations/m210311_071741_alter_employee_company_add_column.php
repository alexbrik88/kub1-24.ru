<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210311_071741_alter_employee_company_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%employee_company}}', 'document_access_own_only', $this->boolean()->notNull()->defaultValue(false));
        $this->update('{{%employee_company}}', [
            'document_access_own_only' => true,
        ], [
            'in', 'employee_role_id', [3,10]
        ]);
    }

    public function safeDown()
    {
        $this->dropColumn('{{%employee_company}}', 'document_access_own_only');
    }
}
