<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210416_093749_alter_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_config', 'contractor_docs_balancesum', $this->boolean(1)->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('user_config', 'contractor_docs_balancesum');
    }
}
