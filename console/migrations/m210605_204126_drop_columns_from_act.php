<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210605_204126_drop_columns_from_act extends Migration
{
    public function safeUp()
    {
        $this->dropIndex('IDX_act_to_company', 'act');
        $this->dropIndex('IDX_act_to_contractor', 'act');

        $this->dropIndex('IDX_upd_to_company', 'upd');
        $this->dropIndex('IDX_upd_to_contractor', 'upd');

        $this->dropIndex('IDX_packing_list_to_company', 'packing_list');
        $this->dropIndex('IDX_packing_list_to_contractor', 'packing_list');

        $this->dropColumn('act', 'contractor_id');
        $this->dropColumn('act', 'company_id');
        $this->dropColumn('act', 'remaining_amount');

        $this->dropColumn('upd', 'contractor_id');
        $this->dropColumn('upd', 'company_id');
        $this->dropColumn('upd', 'remaining_amount');

        $this->dropColumn('packing_list', 'contractor_id');
        $this->dropColumn('packing_list', 'company_id');
        $this->dropColumn('packing_list', 'remaining_amount');
    }

    public function safeDown()
    {
        // no down
    }
}
