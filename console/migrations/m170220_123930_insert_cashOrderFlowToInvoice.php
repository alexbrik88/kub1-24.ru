<?php

use common\models\cash\CashOrderFlows;
use common\models\cash\CashOrderFlowToInvoice;
use common\models\document\Invoice;
use console\components\db\Migration;
use yii\db\Schema;

class m170220_123930_insert_cashOrderFlowToInvoice extends Migration
{
    public function safeUp()
    {
        $B = CashOrderFlows::tableName();
        $F = CashOrderFlowToInvoice::tableName();
        $I = Invoice::tableName();
        $flowArray = CashOrderFlows::find()
            ->select(['id', 'invoice_id'])
            ->where(['not', ['invoice_id' => null]])
            ->asArray()
            ->all();

        if ($flowArray) {
            foreach ($flowArray as $row) {
                $flow = CashOrderFlows::findOne($row['id']);
                $invoice = Invoice::findOne($row['invoice_id']);
                if ($flow && $invoice) {
                    $flow->unlinkInvoice($invoice);
                    $flow->linkInvoice($invoice);
                }
            }
        }
    }
    
    public function safeDown()
    {
        CashOrderFlowToInvoice::deleteAll();
    }
}


