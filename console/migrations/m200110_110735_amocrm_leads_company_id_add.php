<?php

use console\components\db\Migration;

class m200110_110735_amocrm_leads_company_id_add extends Migration
{
    const TABLE = '{{%amocrm_leads}}';

    public function up()
    {
        $this->addColumn(self::TABLE, 'company_id', $this->integer(11)->notNull()->after('id')->comment('Идентификатор компании КУБ'));
    }

    public function down()
    {
        $this->dropColumn(self::TABLE, 'company_id');
    }
}
