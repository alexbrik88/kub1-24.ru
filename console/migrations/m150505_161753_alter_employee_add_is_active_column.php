<?php

use yii\db\Schema;
use yii\db\Migration;

class m150505_161753_alter_employee_add_is_active_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('employee', 'is_active', Schema::TYPE_BOOLEAN . ' COMMENT "1 - активен, 0 - не активен"');
    }

    public function safeDown()
    {
        $this->dropColumn('employee', 'is_active');
    }
}
