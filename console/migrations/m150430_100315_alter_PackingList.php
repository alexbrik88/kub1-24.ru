<?php

use yii\db\Schema;
use yii\db\Migration;

class m150430_100315_alter_PackingList extends Migration
{
    public function safeUp()
    {
        $this->dropTable('packing_list');
        $this->createTable('packing_list', [
            'id' => Schema::TYPE_PK,
            'type' => Schema::TYPE_INTEGER . '(1) NOT NULL COMMENT "2 - out, 1 -in"',
            'invoice_id' => Schema::TYPE_INTEGER . ' NOT NULL',

            'created_at' => Schema::TYPE_DATETIME . ' NOT NULL',
            'document_author_id' => Schema::TYPE_INTEGER . ' NOT NULL',

            'status_out_id' => Schema::TYPE_INTEGER . ' NULL',
            'status_out_updated_at' => Schema::TYPE_DATETIME . ' NULL',
            'status_out_author_id' => Schema::TYPE_INTEGER . ' NULL',

            'document_date' => Schema::TYPE_DATE . ' NOT NULL',
            'document_number' => Schema::TYPE_INTEGER . ' NULL',
            'document_additional_number' => Schema::TYPE_STRING . '(45) NULL',

            'document_filename' => Schema::TYPE_STRING . '(255) NULL',
        ]);

        $this->addForeignKey('fk_packing_list_to_invoice', 'packing_list', 'invoice_id', 'invoice', 'id');
        $this->addForeignKey('fk_packing_list_to_packing_list_status', 'packing_list', 'status_out_id', 'packing_list_status', 'id');
        $this->addForeignKey('fk_packing_list_to_document_author', 'packing_list', 'document_author_id', 'employee', 'id');
        $this->addForeignKey('fk_packing_list_to_status_out_author', 'packing_list', 'status_out_author_id', 'employee', 'id');
    }

    public function safeDown()
    {
        $this->dropTable('packing_list');
        $this->execute("
CREATE TABLE IF NOT EXISTS `packing_list` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `creation_date` date DEFAULT NULL COMMENT 'Дата создания',
  `author` int(11) NOT NULL COMMENT 'Автор (id сотрудника)',
  `invoice_id` int(11) NOT NULL COMMENT 'Счёт',
  `packing_list_status_id` int(11) NOT NULL,
  `status_date` date DEFAULT NULL COMMENT 'Дата возникновения текущего статуса',
  `status_author` int(11) NOT NULL COMMENT 'Автор текущего статуса',
  `packing_list_date` date DEFAULT NULL COMMENT 'Дата обновления акта',
  `additional_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Дополнительный номер акта',
  `file` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Ссылка на файл',
  `type` int(1) NOT NULL COMMENT 'Тип накладной - 2 (исходящая), 1 (входящая)',
  PRIMARY KEY (`id`),
  KEY `packing_list_to_employee_author` (`author`),
  KEY `packing_list_to_employee_author_status` (`status_author`),
  KEY `packing_list_to_act_status` (`packing_list_status_id`),
  KEY `fk_packing_list_to_invoice` (`invoice_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci ;

ALTER TABLE `packing_list`
  ADD CONSTRAINT `fk_packing_list_to_invoice` FOREIGN KEY (`invoice_id`) REFERENCES `invoice` (`id`),
  ADD CONSTRAINT `packing_list_to_act_status` FOREIGN KEY (`packing_list_status_id`) REFERENCES `packing_list_status` (`id`),
  ADD CONSTRAINT `packing_list_to_employee_author` FOREIGN KEY (`author`) REFERENCES `employee` (`id`),
  ADD CONSTRAINT `packing_list_to_employee_author_status` FOREIGN KEY (`status_author`) REFERENCES `employee` (`id`);
        ");
    }
}
