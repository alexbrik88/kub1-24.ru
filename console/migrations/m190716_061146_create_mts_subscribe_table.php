<?php

use yii\db\Migration;

/**
 * Handles the creation of table `mts_subscribe`.
 */
class m190716_061146_create_mts_subscribe_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%mts_subscribe}}', [
            'id' => $this->primaryKey(),
            'status' => $this->string()->notNull(),
            'mts_user_id' => $this->string()->notNull(),
            'tariff_id' => $this->integer()->notNull(),
            'payment_id' => $this->integer(),
            'price_code' => $this->string(),
            'subscription_id' => $this->string(),
            'data' => $this->text(),
            'request_id' => $this->string()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey(
            'FK_mts_subscribe_service_payment',
            '{{%mts_subscribe}}',
            'payment_id',
            '{{%service_payment}}',
            'id'
        );
        $this->createIndex('status', '{{%mts_subscribe}}', 'status');
        $this->createIndex('mts_user_id', '{{%mts_subscribe}}', 'mts_user_id');
        $this->createIndex('tariff_id', '{{%mts_subscribe}}', 'tariff_id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%mts_subscribe}}');
    }
}
