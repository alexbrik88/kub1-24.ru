<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190704_161848_alter_agent_report_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('agent_report', 'status_out_self_id', $this->integer()->defaultValue(1)->after('status_out_id'));
        $this->addForeignKey('FK_agent_report_status_out_self_id', 'agent_report', 'status_out_self_id', 'agent_report_status', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_agent_report_status_out_self_id', 'agent_report');
        $this->dropColumn('agent_report', 'status_out_self_id');
    }
}
