<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201118_065004_update_product_turnover_data_5 extends Migration
{
    public function safeUp()
    {
        $this->execute('
            UPDATE `product_turnover` t 
            JOIN `packing_list` d ON t.document_id = d.id AND t.document_table = "packing_list" 
            SET t.date = d.document_date
        ');
        $this->execute('
            UPDATE `product_turnover` t 
            JOIN `act` d ON t.document_id = d.id AND t.document_table = "act" 
            SET t.date = d.document_date
        ');
        $this->execute('
            UPDATE `product_turnover` t 
            JOIN `upd` d ON t.document_id = d.id AND t.document_table = "upd" 
            SET t.date = d.document_date
        ');
        $this->execute('
            UPDATE `product_turnover` t 
            JOIN `sales_invoice` d ON t.document_id = d.id AND t.document_table = "sales_invoice" 
            SET t.date = d.document_date
        ');
    }

    public function safeDown()
    {
        // no down
    }
}
