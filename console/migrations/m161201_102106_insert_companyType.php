<?php

use console\components\db\Migration;
use yii\db\Schema;

class m161201_102106_insert_companyType extends Migration
{
    public function safeUp()
    {
        $this->insert('company_type', [
            'id' => 15,
            'name_short' => 'ФГБОУ',
            'name_full' => 'Федеральное государственное бюджетное образовательное учреждение',
            'in_company' => 0,
            'in_contractor' => 1,
        ]);
    }
    
    public function safeDown()
    {
        $this->delete('company_type', ['id' => 15]);
    }
}


