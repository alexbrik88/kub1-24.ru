<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180409_135123_update_invoice extends Migration
{
    public function safeUp()
    {
        $this->execute('
            UPDATE {{invoice}}
            LEFT JOIN {{company}} ON {{invoice}}.[[company_id]] = {{company}}.[[id]]
            SET {{invoice}}.[[nds_view_type_id]] = 0
            WHERE {{invoice}}.[[type]] = 1
            AND {{invoice}}.[[nds_view_type_id]] IS NULL
        ');
        $this->execute('
            UPDATE {{invoice}}
            LEFT JOIN {{company}} ON {{invoice}}.[[company_id]] = {{company}}.[[id]]
            SET {{invoice}}.[[nds_view_type_id]] = IF(
                {{invoice}}.[[has_nds]],
                IF({{company}}.[[nds]] = 2, 1, 0),
                2
            )
            WHERE {{invoice}}.[[type]] = 1
        ');
    }

    public function safeDown()
    {
        //
    }
}


