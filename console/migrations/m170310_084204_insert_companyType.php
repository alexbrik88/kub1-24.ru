<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170310_084204_insert_companyType extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%company_type}}', [
            'id' => 29,
            'name_short' => 'ФЧКОО',
            'name_full' => 'Филиал частной компании с ограниченной ответственностью',
            'in_company' => false,
            'in_contractor' => true,
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%company_type}}', ['id' => 29]);
    }
}


