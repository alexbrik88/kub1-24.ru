<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%autovoronka_send_log}}`.
 */
class m191224_081444_create_autovoronka_send_log_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%autovoronka_send_log}}', [
            'id' => $this->primaryKey(),
            'for_unread' => $this->boolean()->notNull()->defaultValue(false),
            'date' => $this->date()->notNull(),
            'case' => $this->string()->notNull(),
            'mail' => $this->string()->notNull(),
            'subject' => $this->string()->notNull(),
            'list_id' => $this->string()->notNull(),
            '_contacts' => $this->text()->notNull(),
            '_result' => $this->text()->notNull(),
            'created_at' => $this->integer()->notNull(),
        ]);

        $this->createIndex('for_unread', '{{%autovoronka_send_log}}', 'for_unread');
        $this->createIndex('date', '{{%autovoronka_send_log}}', 'date');
        $this->createIndex('case', '{{%autovoronka_send_log}}', 'case');
        $this->createIndex('mail', '{{%autovoronka_send_log}}', 'mail');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%autovoronka_send_log}}');
    }
}
