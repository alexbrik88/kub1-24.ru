<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181108_114420_insert_service_store_out_invoice_tariff extends Migration
{
    public $tableName = 'service_store_out_invoice_tariff';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'sort', $this->integer()->defaultValue(null));
        $this->insert($this->tableName, [
            'id' => 5,
            'links_count' => 1,
            'cost_one_month_for_one_link' => 250,
            'total_amount' => 3000,
            'is_visible' => 1,
            'sort' => 10,
        ]);

        $this->update($this->tableName, ['sort' => 20], ['id' => 1]);
        $this->update($this->tableName, ['sort' => 30], ['id' => 2]);
        $this->update($this->tableName, ['sort' => 40], ['id' => 3]);
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'sort');
        $this->delete($this->tableName, ['id' => 5]);
    }
}


