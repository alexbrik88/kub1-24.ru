<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200518_074146_alter_service_subscribe_tariff_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%service_subscribe_tariff}}', 'label', $this->string()->after('price'));

        $this->update('{{%service_subscribe_tariff}}', [
            'label' => 'Простой',
        ], [
            'id' => [40, 41, 42],
        ]);
        $this->update('{{%service_subscribe_tariff}}', [
            'label' => 'Онлайн',
        ], [
            'id' => [43, 44, 45],
        ]);
        $this->update('{{%service_subscribe_tariff}}', [
            'label' => 'Продающий',
        ], [
            'id' => [46, 47, 48],
        ]);
    }

    public function safeDown()
    {
        $this->dropColumn('{{%service_subscribe_tariff}}', 'label');
    }
}
