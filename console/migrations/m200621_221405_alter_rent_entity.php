<?php

use console\components\db\Migration;
use yii\db\Schema;
use yii\db\Query;

class m200621_221405_alter_rent_entity extends Migration
{
    public $table = 'rent_entity';

    public function safeUp()
    {
        $defaultContractorId = Yii::$app->params['service']['contractor_id'];
        $defaultEmployeeId = (new Query())->from('employee')->select('id')->orderBy('id')->limit(1)->scalar();

        $this->addColumn($this->table, 'owner_id', $this->integer()->notNull()->defaultValue($defaultContractorId));
        $this->addColumn($this->table, 'count', $this->integer()->unsigned());
        $this->addColumn($this->table, 'useful_life_in_month', $this->integer());
        $this->addColumn($this->table, 'purchased_at', $this->date());
        $this->addColumn($this->table, 'purchased_price', $this->bigInteger());
        $this->addColumn($this->table, 'deleted', $this->tinyInteger(1)->notNull()->defaultValue(0));
        $this->addColumn($this->table, 'retired', $this->tinyInteger(1)->notNull()->defaultValue(0));
        $this->addColumn($this->table, 'created_at', $this->integer()->notNull());
        $this->addColumn($this->table, 'created_by', $this->integer()->notNull()->defaultValue($defaultEmployeeId));

        $this->alterColumn($this->table, 'owner_id', $this->integer()->notNull());
        $this->alterColumn($this->table, 'created_by', $this->integer()->notNull());

        $this->addForeignKey('FK_rent_entity_owner_id', $this->table, 'owner_id', 'contractor', 'id');
        $this->addForeignKey('FK_rent_entity_created_by', $this->table, 'created_by', 'employee', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_rent_entity_owner_id', $this->table);
        $this->dropForeignKey('FK_rent_entity_created_by', $this->table);
        $this->dropColumn($this->table, 'owner_id');
        $this->dropColumn($this->table, 'count');
        $this->dropColumn($this->table, 'useful_life_in_month');
        $this->dropColumn($this->table, 'purchased_at');
        $this->dropColumn($this->table, 'purchased_price');
        $this->dropColumn($this->table, 'created_at');
        $this->dropColumn($this->table, 'created_by');
        $this->dropColumn($this->table, 'retired');
        $this->dropColumn($this->table, 'deleted');
    }
}
