<?php

use yii\db\Migration;

/**
 * Handles the creation of table `company_infs`.
 */
class m170922_083927_create_company_ifns_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('company_ifns', [
            'company_id' => $this->primaryKey(),
            'ga' => $this->integer()->notNull(),
            'gb' => $this->string(500)->notNull()->defaultValue(''),
            'g1' => $this->string(500)->notNull()->defaultValue(''),
            'g2' => $this->string(500)->notNull()->defaultValue(''),
            'g4' => $this->string(500)->notNull()->defaultValue(''),
            'g6' => $this->string(500)->notNull()->defaultValue(''),
            'g7' => $this->string(500)->notNull()->defaultValue(''),
            'g8' => $this->string(500)->notNull()->defaultValue(''),
            'g9' => $this->string(500)->notNull()->defaultValue(''),
            'g11' => $this->string(500)->notNull()->defaultValue(''),
        ]);

        $this->addCommentOnColumn('company_ifns','ga','Код ИФНС');
        $this->addCommentOnColumn('company_ifns','gb','Полное наименование');
        $this->addCommentOnColumn('company_ifns','g1','Адрес');
        $this->addCommentOnColumn('company_ifns','g2','Телефон');
        $this->addCommentOnColumn('company_ifns','g4','Получатель платежа');
        $this->addCommentOnColumn('company_ifns','g6','ИНН получателя');
        $this->addCommentOnColumn('company_ifns','g7','КПП получателя');
        $this->addCommentOnColumn('company_ifns','g8','Банк получателя');
        $this->addCommentOnColumn('company_ifns','g9','БИК банка получателя');
        $this->addCommentOnColumn('company_ifns','g11','Номер счета получателя');

        $this->addForeignKey('FK_companyIfns_company', 'company_ifns', 'company_id', 'company', 'id', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('company_ifns');
    }
}
