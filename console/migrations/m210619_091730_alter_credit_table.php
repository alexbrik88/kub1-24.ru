<?php

use yii\db\Migration;

class m210619_091730_alter_credit_table extends Migration
{
    private const TABLE_NAME = '{{%credit}}';

    /**
     * @inheritDoc
     */
    public function up(): void
    {
        $this->addColumn(self::TABLE_NAME, 'interest_amount', $this->double(2)->notNull()->defaultValue(0)->after('debt_diff'));
        $this->renameColumn(self::TABLE_NAME, 'debt_interest', 'interest_repaid');
        $this->alterColumn(self::TABLE_NAME, 'interest_repaid', $this->double(2)->notNull()->defaultValue(0)->after('interest_amount'));
        $this->addColumn(self::TABLE_NAME, 'interest_diff', $this->double(2)->notNull()->defaultValue(0)->after('interest_repaid'));
    }

    /**
     * @inheritDoc
     */
    public function down(): bool
    {
        return false;
    }
}
