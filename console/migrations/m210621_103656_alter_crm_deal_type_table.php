<?php

use yii\db\Migration;

class m210621_103656_alter_crm_deal_type_table extends Migration
{
    /**
     * @inheritDoc
     */
    public function up(): void
    {
        $this->dropForeignKey('fk_crm_client__crm_client_deal', '{{%crm_client}}');
        $this->dropForeignKey('fk_crm_client_deal_stage__crm_client_deal', '{{%crm_deal_stage}}');

        $this->renameColumn('{{%crm_deal_type}}', 'client_deal_id', 'deal_type_id');
        $this->renameColumn('{{%crm_client}}', 'client_deal_id', 'deal_type_id');
        $this->renameColumn('{{%crm_deal_stage}}', 'client_deal_id', 'deal_type_id');

        $this->addForeignKey(
            'fk_crm_client__crm_deal_type',
            '{{%crm_client}}',
            ['deal_type_id'],
            '{{%crm_deal_type}}',
            ['deal_type_id'],
            'SET NULL',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_crm_deal_stage__crm_deal_type',
            '{{%crm_deal_stage}}',
            ['deal_type_id'],
            '{{%crm_deal_type}}',
            ['deal_type_id'],
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritDoc
     */
    public function down(): bool
    {
        return false;
    }
}
