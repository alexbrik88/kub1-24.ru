<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190408_215948_create_balance_item_amount extends Migration
{
    public $tableName = '{{%balance_item_amount}}';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'item_id' => $this->integer()->notNull(),
            'month' => $this->string()->notNull(),
            'year' => $this->string()->notNull(),
            'amount' => $this->decimal(38, 2)->defaultValue(null),
        ]);

        $this->addForeignKey('balance_item_amount_item_id', $this->tableName, 'item_id', 'balance_item', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('balance_item_amount_company_id', $this->tableName, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
