<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160127_081511_alter_notification_set_default_value_for_whom extends Migration
{
    public $tNotification = 'notification';

    public function safeUp()
    {
        $this->update($this->tNotification, ['for_whom' => 1], ['for_whom' => 0]);
    }

    public function safeDown()
    {
        echo 'No down migration available';
    }
}


