<?php

use console\components\db\Migration;
use yii\db\Schema;

class m171031_221031_update_invoice_income_items extends Migration
{
    public $tableName = 'invoice_income_item';

    public function safeUp()
    {
        $this->insert($this->tableName, [
            'id' => 10,
            'name' => 'Погашение займа',
        ]);
        $this->insert($this->tableName, [
            'id' => 11,
            'name' => 'Полученные проценты',
        ]);
    }
    
    public function safeDown()
    {
        $this->delete($this->tableName, ['id' => 10]);
        $this->delete($this->tableName, ['id' => 11]);
    }
}


