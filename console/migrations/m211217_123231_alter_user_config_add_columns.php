<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211217_123231_alter_user_config_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%user_config}}', 'debts_cash_config_hide_zero', $this->boolean()->notNull()->defaultValue(0));
        $this->addColumn('{{%user_config}}', 'debts_cash_config_show_by_account', $this->boolean()->notNull()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->addColumn('{{%user_config}}', 'debts_cash_config_hide_zero');
        $this->addColumn('{{%user_config}}', 'debts_cash_config_show_by_account');
    }
}
