<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200710_132704_append_table_project_employee extends Migration
{
    public $tableName = 'project_employee';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => 'pk',
            'company_id' => 'integer',
            'project_id' => 'integer',
            'employee_id' => 'integer'
        ]);

        $this->addForeignKey('fk_project_employers_project',
            $this->tableName,
            'project_id',
            'project',
            'id');

        $this->addForeignKey('fk_project_employers_employee',
            $this->tableName,
            'employee_id',
            'employee',
            'id');

    }

    public function safeDown()
    {
        try {
            $this->dropForeignKey('fk_project_employers_project', $this->tableName);
            $this->dropForeignKey('fk_project_employers_employee', $this->tableName);
        } catch (\Exception $e) {
            echo "Drop foreign key failed";
        }
        $this->dropTable($this->tableName);
    }
}
