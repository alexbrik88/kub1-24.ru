<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180903_134858_add_product_group_to_user_config extends Migration
{
    public $tableName = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'product_group', $this->boolean()->defaultValue(false));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'product_group');
    }
}


