<?php

use console\components\db\Migration;
use yii\db\Schema;
use common\models\document\Act;
use common\models\document\PackingList;
use common\models\document\InvoiceFacture;

class m160713_135159_update_ordinal_document_number extends Migration
{
    public $tAct = 'act';
    public $tPackingList = 'packing_list';
    public $tInvoiceFacture = 'invoice_facture';

    public function safeUp()
    {
        $acts = Act::find()->all();
        $packingLists = PackingList::find()->all();
        $invoiceFactures = InvoiceFacture::find()->all();
        foreach ($acts as $act) {
            /* @var Act $act */
            if ($act->ordinal_document_number == 0) {
                $documentNumber = preg_replace('~\D+~', '', $act->document_number);
                $this->update($this->tAct, ['ordinal_document_number' => $documentNumber], ['id' => $act->id]);
            }
        }
        foreach ($packingLists as $packingList) {
            /* @var PackingList $packingList */
            if ($packingList->ordinal_document_number == 0) {
                $documentNumber = preg_replace('~\D+~', '', $packingList->document_number);
                $this->update($this->tPackingList, ['ordinal_document_number' => $documentNumber], ['id' => $packingList->id]);
            }
        }
        foreach ($invoiceFactures as $invoiceFacture) {
            /* @var InvoiceFacture $invoiceFacture */
            if ($invoiceFacture->ordinal_document_number == 0) {
                $documentNumber = preg_replace('~\D+~', '', $invoiceFacture->document_number);
                $this->update($this->tInvoiceFacture, ['ordinal_document_number' => $documentNumber], ['id' => $invoiceFacture->id]);
            }
        }
    }
    
    public function safeDown()
    {

    }
}


