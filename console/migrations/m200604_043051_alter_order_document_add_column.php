<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200604_043051_alter_order_document_add_column extends Migration
{
    public $tableName = 'order_document';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'has_markup', $this->boolean()->defaultValue(false)->after('has_discount'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'has_markup');
    }
}
