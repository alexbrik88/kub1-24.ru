<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170713_063713_alter_employeeCompany_addColumns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%employee_company}}', 'is_working', $this->boolean()->notNull()->defaultValue(true) . ' AFTER [[company_id]]');
        $this->addColumn('{{%employee_company}}', 'date_hiring', $this->date() . ' AFTER [[is_working]]');
        $this->addColumn('{{%employee_company}}', 'date_dismissal', $this->date() . ' AFTER [[date_hiring]]');
        $this->addColumn('{{%employee_company}}', 'position', $this->string() . ' AFTER [[date_dismissal]]');

        $this->execute("
            UPDATE {{employee_company}}
            LEFT JOIN {{employee}} ON {{employee}}.[[id]] = {{employee_company}}.[[employee_id]]
            SET {{employee_company}}.[[is_working]] = {{employee}}.[[is_working]],
                {{employee_company}}.[[date_hiring]] = {{employee}}.[[date_hiring]],
                {{employee_company}}.[[date_dismissal]] = {{employee}}.[[date_dismissal]],
                {{employee_company}}.[[position]] = {{employee}}.[[position]];
        ");
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%employee_company}}', 'is_working');
        $this->dropColumn('{{%employee_company}}', 'date_hiring');
        $this->dropColumn('{{%employee_company}}', 'date_dismissal');
        $this->dropColumn('{{%employee_company}}', 'position');
    }
}
