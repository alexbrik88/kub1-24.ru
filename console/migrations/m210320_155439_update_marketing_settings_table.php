<?php

use yii\db\Migration;

class m210320_155439_update_marketing_settings_table extends Migration
{
    private const TABLE_NAME = '{{%marketing_settings}}';

    /** @var int[] */
    private const COMMAND_TYPE_MAP = [
        1 => 6,
        2 => 7,
        3 => 9,
        5 => 8,
    ];

    /**
     * @inheritDoc
     * @throws
     */
    public function safeUp()
    {
        foreach (self::COMMAND_TYPE_MAP as $oldValue => $newValue) {
            $this->db
                ->createCommand()
                ->update(self::TABLE_NAME, ['command_type' => $newValue], ['command_type' => $oldValue])
                ->execute();
        }
    }

    /**
     * @inheritDoc
     * @throws
     */
    public function safeDown()
    {
        foreach (self::COMMAND_TYPE_MAP as $newValue => $oldValue) {
            $this->db
                ->createCommand()
                ->update(self::TABLE_NAME, ['command_type' => $newValue], ['command_type' => $oldValue])
                ->execute();
        }
    }
}
