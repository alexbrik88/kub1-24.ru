<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200222_130604_alter_log_create_index extends Migration
{
    public function safeUp()
    {
        $this->createIndex('created_at', '{{%log}}', 'created_at');
    }

    public function safeDown()
    {
        $this->dropIndex('created_at', '{{%log}}');
    }
}
