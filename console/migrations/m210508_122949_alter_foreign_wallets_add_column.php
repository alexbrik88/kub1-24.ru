<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210508_122949_alter_foreign_wallets_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cash_bank_foreign_currency_flows', 'project_id', $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn('cash_bank_foreign_currency_flows', 'project_id');
    }
}