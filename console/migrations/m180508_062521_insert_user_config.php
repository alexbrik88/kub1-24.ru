<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180508_062521_insert_user_config extends Migration
{
    public $tableName = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'payment_reminder_message_contractor_invoice_sum', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn($this->tableName, 'payment_reminder_message_contractor_message_1', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn($this->tableName, 'payment_reminder_message_contractor_message_2', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn($this->tableName, 'payment_reminder_message_contractor_message_3', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn($this->tableName, 'payment_reminder_message_contractor_message_4', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn($this->tableName, 'payment_reminder_message_contractor_message_5', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn($this->tableName, 'payment_reminder_message_contractor_message_6', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn($this->tableName, 'payment_reminder_message_contractor_message_7', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn($this->tableName, 'payment_reminder_message_contractor_message_8', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn($this->tableName, 'payment_reminder_message_contractor_message_9', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn($this->tableName, 'payment_reminder_message_contractor_message_10', $this->boolean()->notNull()->defaultValue(true));

        $this->addCommentOnColumn('user_config', 'payment_reminder_message_contractor_invoice_sum', 'Показ колонки "Задолженность" в списке настройки автоматической рассылки писем');
        $this->addCommentOnColumn('user_config', 'payment_reminder_message_contractor_message_1', 'Показ колонки "Письмо №1" в списке настройки автоматической рассылки писем');
        $this->addCommentOnColumn('user_config', 'payment_reminder_message_contractor_message_2', 'Показ колонки "Письмо №2" в списке настройки автоматической рассылки писем');
        $this->addCommentOnColumn('user_config', 'payment_reminder_message_contractor_message_3', 'Показ колонки "Письмо №3" в списке настройки автоматической рассылки писем');
        $this->addCommentOnColumn('user_config', 'payment_reminder_message_contractor_message_4', 'Показ колонки "Письмо №4" в списке настройки автоматической рассылки писем');
        $this->addCommentOnColumn('user_config', 'payment_reminder_message_contractor_message_5', 'Показ колонки "Письмо №5" в списке настройки автоматической рассылки писем');
        $this->addCommentOnColumn('user_config', 'payment_reminder_message_contractor_message_6', 'Показ колонки "Письмо №6" в списке настройки автоматической рассылки писем');
        $this->addCommentOnColumn('user_config', 'payment_reminder_message_contractor_message_7', 'Показ колонки "Письмо №7" в списке настройки автоматической рассылки писем');
        $this->addCommentOnColumn('user_config', 'payment_reminder_message_contractor_message_8', 'Показ колонки "Письмо №8" в списке настройки автоматической рассылки писем');
        $this->addCommentOnColumn('user_config', 'payment_reminder_message_contractor_message_9', 'Показ колонки "Письмо №9" в списке настройки автоматической рассылки писем');
        $this->addCommentOnColumn('user_config', 'payment_reminder_message_contractor_message_10', 'Показ колонки "Письмо №10" в списке настройки автоматической рассылки писем');
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'payment_reminder_message_contractor_invoice_sum');
        $this->dropColumn($this->tableName, 'payment_reminder_message_contractor_message_1');
        $this->dropColumn($this->tableName, 'payment_reminder_message_contractor_message_2');
        $this->dropColumn($this->tableName, 'payment_reminder_message_contractor_message_3');
        $this->dropColumn($this->tableName, 'payment_reminder_message_contractor_message_4');
        $this->dropColumn($this->tableName, 'payment_reminder_message_contractor_message_5');
        $this->dropColumn($this->tableName, 'payment_reminder_message_contractor_message_6');
        $this->dropColumn($this->tableName, 'payment_reminder_message_contractor_message_7');
        $this->dropColumn($this->tableName, 'payment_reminder_message_contractor_message_8');
        $this->dropColumn($this->tableName, 'payment_reminder_message_contractor_message_9');
        $this->dropColumn($this->tableName, 'payment_reminder_message_contractor_message_10');
    }
}


