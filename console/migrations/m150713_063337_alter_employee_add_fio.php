<?php

use yii\db\Schema;
use yii\db\Migration;

class m150713_063337_alter_employee_add_fio extends Migration
{
    public function safeUp()
    {
        $this->renameColumn('employee', 'name', 'lastname');
        $this->alterColumn('employee', 'lastname', Schema::TYPE_STRING . '(45) NOT NULL');
        $this->addColumn('employee', 'firstname', Schema::TYPE_STRING . '(45) NOT NULL AFTER lastname');
        $this->addColumn('employee', 'firstname_initial', Schema::TYPE_STRING . '(45) NOT NULL AFTER firstname');
        $this->addColumn('employee', 'patronymic', Schema::TYPE_STRING . '(45) NOT NULL AFTER firstname_initial');
        $this->addColumn('employee', 'patronymic_initial', Schema::TYPE_STRING . '(45) NOT NULL AFTER patronymic');
    }
    
    public function safeDown()
    {
        $this->renameColumn('employee', 'lastname', 'name');
        $this->alterColumn('employee', 'name', Schema::TYPE_STRING . '(255) NOT NULL');
        $this->dropColumn('employee', 'firstname');
        $this->dropColumn('employee', 'firstname_initial');
        $this->dropColumn('employee', 'patronymic');
        $this->dropColumn('employee', 'patronymic_initial');
    }
}
