<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%bitrix24_app_host_app}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%bitrix24_client}}`
 * - `{{%bitrix24_purchased_application}}`
 * - `{{%bitrix24_application}}`
 */
class m220111_161038_create_bitrix24_app_host_app_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%bitrix24_app_host_app}}', [
            'id' => $this->primaryKey(),
            'server_domain' => $this->string(100),
            'scope' => $this->string(100),
            'active' => $this->boolean(),
            'app_token' => $this->string(100),
            'company_id' => $this->integer()->notNull(),
            'purch_app_id' => $this->integer()->notNull(),
            'refresh_token' => $this->string(100),
            'user_id' => $this->integer(),
            'client_endpoint' => $this->string(200),
            'expires' => $this->integer(),
            'status' => $this->string(10),
            'server_endpoint' => $this->string(200),
            'expires_in' => $this->integer(),
            'access_token' => $this->string(100),
            'domain' => $this->string(200),
            'app_id' => $this->string(100),
            'app_secret' => $this->string(100),
            'application_id' => $this->integer()->notNull(),
            'member_id' => $this->string(50),
        ]);

        // creates index for column `company_id`
        $this->createIndex(
            '{{%idx-bitrix24_app_host_app-company_id}}',
            '{{%bitrix24_app_host_app}}',
            'company_id'
        );

        // add foreign key for table `{{%bitrix24_client}}`
        $this->addForeignKey(
            '{{%fk-bitrix24_app_host_app-company_id}}',
            '{{%bitrix24_app_host_app}}',
            'company_id',
            '{{%bitrix24_client}}',
            'id',
            'CASCADE'
        );

        // creates index for column `purch_app_id`
        $this->createIndex(
            '{{%idx-bitrix24_app_host_app-purch_app_id}}',
            '{{%bitrix24_app_host_app}}',
            'purch_app_id'
        );

        // add foreign key for table `{{%bitrix24_purchased_application}}`
        $this->addForeignKey(
            '{{%fk-bitrix24_app_host_app-purch_app_id}}',
            '{{%bitrix24_app_host_app}}',
            'purch_app_id',
            '{{%bitrix24_purchased_application}}',
            'id',
            'CASCADE'
        );

        // creates index for column `application_id`
        $this->createIndex(
            '{{%idx-bitrix24_app_host_app-application_id}}',
            '{{%bitrix24_app_host_app}}',
            'application_id'
        );

        // add foreign key for table `{{%bitrix24_application}}`
        $this->addForeignKey(
            '{{%fk-bitrix24_app_host_app-application_id}}',
            '{{%bitrix24_app_host_app}}',
            'application_id',
            '{{%bitrix24_application}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%bitrix24_client}}`
        $this->dropForeignKey(
            '{{%fk-bitrix24_app_host_app-company_id}}',
            '{{%bitrix24_app_host_app}}'
        );

        // drops index for column `company_id`
        $this->dropIndex(
            '{{%idx-bitrix24_app_host_app-company_id}}',
            '{{%bitrix24_app_host_app}}'
        );

        // drops foreign key for table `{{%bitrix24_purchased_application}}`
        $this->dropForeignKey(
            '{{%fk-bitrix24_app_host_app-purch_app_id}}',
            '{{%bitrix24_app_host_app}}'
        );

        // drops index for column `purch_app_id`
        $this->dropIndex(
            '{{%idx-bitrix24_app_host_app-purch_app_id}}',
            '{{%bitrix24_app_host_app}}'
        );

        // drops foreign key for table `{{%bitrix24_application}}`
        $this->dropForeignKey(
            '{{%fk-bitrix24_app_host_app-application_id}}',
            '{{%bitrix24_app_host_app}}'
        );

        // drops index for column `application_id`
        $this->dropIndex(
            '{{%idx-bitrix24_app_host_app-application_id}}',
            '{{%bitrix24_app_host_app}}'
        );

        $this->dropTable('{{%bitrix24_app_host_app}}');
    }
}
