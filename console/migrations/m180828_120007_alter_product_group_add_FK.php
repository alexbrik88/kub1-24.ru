<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180828_120007_alter_product_group_add_FK extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('product_group', 'company_id', $this->integer()->after('title'));

        $this->update('product_group', ['company_id' => null], ['company_id' => 0]);

        $this->addForeignKey('product_group_company', 'product_group', 'company_id', 'company', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('product_group_company', 'product_group');

        $this->update('product_group', ['company_id' => 0], ['company_id' => null]);

        $this->alterColumn('product_group', 'company_id', $this->integer()->defaultValue(0)->after('title'));
    }
}
