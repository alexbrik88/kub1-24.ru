<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190715_120456_alter_company_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%company}}', 'blocked_by_inn', $this->boolean()->defaultValue(false)->after('blocked'));

        $this->createIndex('inn', '{{%company}}', 'inn');
        $this->createIndex('blocked', '{{%company}}', 'blocked');
        $this->createIndex('blocked_by_inn', '{{%company}}', 'blocked_by_inn');
    }

    public function safeDown()
    {
        $this->dropIndex('inn', '{{%company}}');
        $this->dropIndex('blocked', '{{%company}}');
        $this->dropIndex('blocked_by_inn', '{{%company}}');

        $this->dropColumn('{{%company}}', 'blocked_by_inn');
    }
}
