<?php

use yii\db\Schema;
use yii\db\Migration;

class m150709_132147_update_outInvoiceStatus extends Migration
{
    public function safeUp()
    {
        $this->update('out_invoice_status', [
            'name' => 'Оплачен',
        ], 'id = 7');
    }
    
    public function safeDown()
    {
        $this->update('out_invoice_status', [
            'name' => 'Оплачен полностью',
        ], 'id = 7');
    }
}
