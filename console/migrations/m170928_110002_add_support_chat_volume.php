<?php

use console\components\db\Migration;
use yii\db\Schema;
use common\models\EmployeeCompany;
use common\models\employee\Employee;
use common\models\ChatVolume;

class m170928_110002_add_support_chat_volume extends Migration
{
    public $tableName = 'chat_volume';

    public function safeUp()
    {
        $companyId= Employee::find()->where(['id'=>Employee::SUPPORT_KUB_EMPLOYEE_ID])->select('company_id')->asArray()->one()['company_id'];
        foreach (EmployeeCompany::find()->asArray()->all() as $employeeCompany) {
            if (!ChatVolume::find()->andWhere(['and',
                ['company_id' => $employeeCompany['company_id']],
                ['from_employee_id' => $employeeCompany['employee_id']],
                ['to_employee_id' => Employee::SUPPORT_KUB_EMPLOYEE_ID],
            ])->exists()) {
                $chatVolumeWithSupport = new ChatVolume();
                $chatVolumeWithSupport->company_id = $employeeCompany['company_id'];
                $chatVolumeWithSupport->from_employee_id = $employeeCompany['employee_id'];
                $chatVolumeWithSupport->to_employee_id = Employee::SUPPORT_KUB_EMPLOYEE_ID;
                $chatVolumeWithSupport->has_volume = true;
                $chatVolumeWithSupport->save();

                $chatVolumeSupport = new ChatVolume();
                $chatVolumeSupport->company_id = $companyId;
                $chatVolumeSupport->from_employee_id = Employee::SUPPORT_KUB_EMPLOYEE_ID;
                $chatVolumeSupport->to_employee_id = $employeeCompany['employee_id'];
                $chatVolumeSupport->has_volume = true;
                $chatVolumeSupport->save();
            }
        }
    }
    
    public function safeDown()
    {

    }
}


