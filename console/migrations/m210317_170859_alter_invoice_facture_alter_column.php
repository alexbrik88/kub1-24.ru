<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210317_170859_alter_invoice_facture_alter_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%invoice_facture}}', 'ordinal_document_number', $this->string());
    }

    public function safeDown()
    {
        $this->alterColumn('{{%invoice_facture}}', 'ordinal_document_number', $this->integer()->notNull());
    }
}
