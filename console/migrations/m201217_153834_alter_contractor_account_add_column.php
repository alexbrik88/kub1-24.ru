<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201217_153834_alter_contractor_account_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%contractor_account}}', 'currency_id', $this->char(3)->after('contractor_id'));

        $this->execute('
            UPDATE {{%contractor_account}}
            LEFT JOIN {{%contractor}} ON {{%contractor}}.[[id]] = {{%contractor_account}}.[[contractor_id]]
            SET {{%contractor_account}}.[[currency_id]] = {{%contractor}}.[[currency_id]]
            WHERE {{%contractor}}.[[currency_id]] IS NOT NULL
        ');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%contractor_account}}', 'currency_id');
    }
}
