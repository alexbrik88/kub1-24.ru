<?php

use console\components\db\Migration;
use yii\db\Schema;

class m161206_082600_alter_invoiceFacture_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('invoice_facture', 'is_original', $this->boolean()->defaultValue(false));
    }
    
    public function safeDown()
    {
        $this->dropColumn('invoice_facture', 'is_original');
    }
}


