<?php

use yii\db\Schema;
use yii\db\Migration;

class m150515_150851_insert_User extends Migration
{
    public function safeUp()
    {
        $this->execute("
            INSERT
                INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`)
                VALUES
                    (1, 'admin', '', '$2y$13\$VS8TkIF1H9U8w.SX96veXOyIOc54v6bsijuPhf8BsZdiLDha6ZYL.', NULL, 'admin@test.com', 10, 1431700339, 1431700339);");
    }
    
    public function safeDown()
    {
        $this->delete('user', 'id = 1');
    }
}
