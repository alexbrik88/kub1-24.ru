<?php

use console\components\db\Migration;
use yii\db\Schema;

class m151223_070936_alter_set_created_at_and_updated_at_the_begining extends Migration
{
    public $tEmployee = 'employee';
    public $tAct = 'act';
    public $tInvoice = 'invoice';
    public $tInvoiceFacture = 'invoice_facture';
    public $tProduct = 'product';

    public function safeUp()
    {
        $this->update($this->tAct, ['created_at' => NULL], ['created_at' => 0]);
        $this->update($this->tAct, ['status_out_updated_at' => NULL], ['status_out_updated_at' => 0]);
        $this->alterColumn($this->tAct, 'created_at', 'integer(11) AFTER type');
        $this->alterColumn($this->tAct, 'status_out_updated_at', 'integer(11) AFTER created_at');

        $this->update($this->tEmployee, ['created_at' => NULL], ['created_at' => 0]);
        $this->alterColumn($this->tEmployee, 'created_at', 'integer(11) AFTER email');

        $this->update($this->tInvoice, ['created_at' => NULL], ['created_at' => 0]);
        $this->update($this->tInvoice, ['invoice_status_updated_at' => NULL], ['invoice_status_updated_at' => 0]);
        $this->alterColumn($this->tInvoice, 'created_at', 'integer(11) AFTER invoice_status_id');
        $this->alterColumn($this->tInvoice, 'invoice_status_updated_at', 'integer(11) AFTER created_at');

        $this->update($this->tInvoiceFacture, ['created_at' => NULL], ['created_at' => 0]);
        $this->update($this->tInvoiceFacture, ['status_out_updated_at' => NULL], ['status_out_updated_at' => 0]);
        $this->alterColumn($this->tInvoiceFacture, 'created_at', 'integer(11) AFTER type');
        $this->alterColumn($this->tInvoiceFacture, 'status_out_updated_at', 'integer(11) AFTER created_at');

        $this->update($this->tProduct, ['created_at' => NULL], ['created_at' => 0]);
        $this->alterColumn($this->tProduct, 'created_at', 'integer(11) AFTER production_type');
    }
    
    public function safeDown()
    {
        echo 'This migration should not be rolled back';
    }
}


