<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191015_115721_alter_agreement_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%agreement}}', 'title_template_id', $this->integer()->after('status_author_id')->defaultValue(1));

        $this->addForeignKey('fk-agreement-agreement_title_template', '{{%agreement}}', 'title_template_id', '{{%agreement_title_template}}', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk-agreement-agreement_title_template', '{{%agreement}}');

        $this->dropColumn('{{%agreement}}', 'title_template_id');
    }
}
