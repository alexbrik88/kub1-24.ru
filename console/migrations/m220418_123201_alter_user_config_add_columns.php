<?php

use console\components\db\Migration;
use yii\db\Schema;

class m220418_123201_alter_user_config_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%user_config}}', 'project_odds', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn('{{%user_config}}', 'project_opiu', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn('{{%user_config}}', 'project_odds_collapse', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn('{{%user_config}}', 'project_opiu_collapse', $this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%user_config}}', 'project_opiu_collapse');
        $this->dropColumn('{{%user_config}}', 'project_odds_collapse');
        $this->dropColumn('{{%user_config}}', 'project_opiu');
        $this->dropColumn('{{%user_config}}', 'project_odds');
    }
}
