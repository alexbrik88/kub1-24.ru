<?php

use console\components\db\Migration;

class m190919_125827_create_evotor_store extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{evotor_store}}', [
            'id' => "BINARY(16) NOT NULL PRIMARY KEY COMMENT 'UUID Эвотора'",
            'employee_id' => $this->integer()->notNull()->comment('ID покупателя сайта'),
            'name' => $this->string(400)->notNull()->comment('Название магазина'),
            'address' => $this->string(400)->comment('Адрес магазина'),
            'created_at' => $this->integer()->unsigned()->notNull(),
            'updated_at' => $this->integer()->unsigned()->notNull(),
        ], "COMMENT 'Интеграция Эвотор: магазины'");
        $this->addForeignKey(
            'evotor_store_employee_id', '{{evotor_store}}', 'employee_id',
            '{{employee}}', 'id', 'CASCADE', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable('{{evotor_store}}');

    }
}