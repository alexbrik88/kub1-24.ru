<?php

use console\components\db\Migration;
use yii\db\Exception;

class m201014_121748_alter_credit_table extends Migration
{
    /** @var string */
    public const TABLE_NAME = '{{%credit}}';

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function up()
    {
        $this->dropForeignKey('fk_credit_checking_accountant_id__checking_accountant_id', self::TABLE_NAME);
        $this->dropForeignKey('fk_credit_cashbox_id__cashbox_id', self::TABLE_NAME);

        $this->dropColumn(self::TABLE_NAME, 'checking_accountant_id');
        $this->dropColumn(self::TABLE_NAME, 'cashbox_id');

        $this->db->createCommand()->delete(self::TABLE_NAME)->execute();

        $this->addColumn(self::TABLE_NAME, 'wallet_id', $this->integer()->after('payment_amount')->notNull());
        $this->addColumn(self::TABLE_NAME, 'wallet_type', $this->tinyInteger()->after('wallet_id')->notNull());
        $this->addColumn(self::TABLE_NAME, 'contractor_id', $this->integer()->after('employee_id')->notNull());

        $this->createIndex('ck_contractor_id', self::TABLE_NAME, ['contractor_id']);
        $this->addForeignKey(
            'fk_credit_contractor_id__contractor_id',
            self::TABLE_NAME,
            ['contractor_id'],
            '{{%contractor}}',
            ['id'],
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        return false;
    }
}
