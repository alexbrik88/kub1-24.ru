<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190419_143956_invoice_facture_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('invoice_facture', 'add_stamp', $this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('invoice_facture', 'add_stamp');
    }
}
