<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201115_190220_add_has_no_patronymic_driver_column extends Migration
{
    public $tableName = 'driver';

    public function safeUp()
    {
        $this->alterColumn($this->tableName, 'first_name', $this->string(64)->notNull());
        $this->alterColumn($this->tableName, 'last_name', $this->string(64)->notNull());
        $this->alterColumn($this->tableName, 'patronymic', $this->string(64)->notNull());
        $this->addColumn($this->tableName, 'has_no_patronymic', $this->boolean()->defaultValue(false)->after('patronymic'));

        $this->execute('
            UPDATE {{driver}}
            SET {{driver}}.[[has_no_patronymic]] = 1
            WHERE {{driver}}.[[patronymic]] = ""
        ');
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'has_no_patronymic');
        $this->alterColumn($this->tableName, 'first_name', $this->string()->notNull());
        $this->alterColumn($this->tableName, 'last_name', $this->string()->notNull());
        $this->alterColumn($this->tableName, 'patronymic', $this->string()->notNull());
    }
}
