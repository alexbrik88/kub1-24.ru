<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181026_100213_update_address_in_contractor extends Migration
{
    public $tableName = 'contractor';

    public function safeUp()
    {
        $this->alterColumn($this->tableName, 'legal_address', $this->string(255)->defaultValue(null));
        $this->alterColumn($this->tableName, 'actual_address', $this->string(255)->defaultValue(null));
        $this->alterColumn($this->tableName, 'postal_address', $this->string(255)->defaultValue(null));
        $this->alterColumn($this->tableName, 'physical_address', $this->string(255)->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->alterColumn($this->tableName, 'legal_address', $this->string(160)->defaultValue(null));
        $this->alterColumn($this->tableName, 'actual_address', $this->string(160)->defaultValue(null));
        $this->alterColumn($this->tableName, 'postal_address', $this->string(160)->defaultValue(null));
        $this->alterColumn($this->tableName, 'physical_address', $this->string(160)->defaultValue(null));
    }
}


