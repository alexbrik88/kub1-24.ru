<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201107_122734_alter_project_estimate_item_change_columns extends Migration
{
    public $tableName = 'project_estimate_item';

    public function safeUp()
    {
        $this->renameColumn($this->tableName, 'amount', 'count');
        $this->renameColumn($this->tableName, 'nds', 'tax_id');
    }

    public function safeDown()
    {
        $this->renameColumn($this->tableName, 'count', 'amount');
        $this->renameColumn($this->tableName, 'tax_id', 'nds');
    }
}
