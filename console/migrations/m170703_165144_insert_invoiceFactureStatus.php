<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170703_165144_insert_invoiceFactureStatus extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%invoice_facture_status}}', [
            'id' => 4,
            'name' => 'Отменена',
        ]);
    }
    
    public function safeDown()
    {
        $this->delete('{{%invoice_facture_status}}', 'id = 4');
    }
}
