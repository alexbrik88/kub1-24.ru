<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211110_163402_alter_olap_flows_add_index extends Migration
{
    public function safeUp()
    {
        $this->createIndex('idx_olap_flows_recognition_date', 'olap_flows', ['company_id', 'recognition_date']);
    }

    public function safeDown()
    {
        $this->dropIndex('idx_olap_flows_recognition_date', 'olap_flows');
    }
}
