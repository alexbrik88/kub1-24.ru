<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180212_081723_insert_product_store extends Migration
{
    public function safeUp()
    {
        $time = time();
        $this->execute("
            INSERT INTO {{product_store}} (
                [[product_id]],
                [[store_id]],
                [[quantity]],
                [[initial_quantity]],
                [[created_at]],
                [[updated_at]]
            )
            SELECT
                {{product}}.[[id]],
                {{store}}.[[id]],
                {{product}}.[[product_count]],
                {{product}}.[[product_begin_count]],
                {{product}}.[[created_at]],
                {$time}
            FROM {{product}}
            INNER JOIN {{company}} ON {{product}}.[[company_id]] = {{company}}.[[id]]
            LEFT JOIN {{store}} ON {{store}}.[[company_id]] = {{company}}.[[id]]
            WHERE {{product}}.[[production_type]] = 1
        ");
    }

    public function safeDown()
    {
        $this->execute("
            DELETE FROM {{product_store}}
        ");
    }
}
