<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200517_161845_alter_import1c_add_columns extends Migration
{
    public $table = 'import_1c';

    public function safeUp()
    {
        $this->addColumn($this->table, 'period', $this->string()->defaultValue(''));
        $this->addColumn($this->table, 'is_completed', $this->tinyInteger()->defaultValue(0));
        $this->addColumn($this->table, 'total_contractors', $this->integer()->defaultValue(0));
        $this->addColumn($this->table, 'total_products', $this->integer()->defaultValue(0));
        $this->addColumn($this->table, 'total_in_invoices', $this->integer()->defaultValue(0));
        $this->addColumn($this->table, 'total_in_upds', $this->integer()->defaultValue(0));
        $this->addColumn($this->table, 'total_in_invoice_factures', $this->integer()->defaultValue(0));
        $this->addColumn($this->table, 'errors_messages', $this->text());
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'period');
        $this->dropColumn($this->table, 'is_completed');
        $this->dropColumn($this->table, 'total_contractors');
        $this->dropColumn($this->table, 'total_products');
        $this->dropColumn($this->table, 'total_in_invoices');
        $this->dropColumn($this->table, 'total_in_upds');
        $this->dropColumn($this->table, 'total_in_invoice_factures');
        $this->dropColumn($this->table, 'errors_messages');
    }
}
