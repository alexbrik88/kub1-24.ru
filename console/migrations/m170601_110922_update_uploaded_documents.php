<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170601_110922_update_uploaded_documents extends Migration
{
    public $tableName = 'uploaded_documents';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'document_number', $this->string()->defaultValue(null));
        $this->addColumn($this->tableName, 'document_date', $this->date()->defaultValue(null));
        $this->addColumn($this->tableName, 'inn', $this->string(45)->defaultValue(null));
        $this->addColumn($this->tableName, 'contractor_name', $this->string()->defaultValue(null));
        $this->addColumn($this->tableName, 'contractor_type_id', $this->integer()->defaultValue(null));
        $this->addColumn($this->tableName, 'kpp', $this->string(45)->defaultValue(null));
        $this->addColumn($this->tableName, 'rs', $this->string(20)->defaultValue(null));
        $this->addColumn($this->tableName, 'bik', $this->string(9)->defaultValue(null));
        $this->addColumn($this->tableName, 'bank_name', $this->string(45)->defaultValue(null));
        $this->addColumn($this->tableName, 'ks', $this->string(20)->defaultValue(null));
        $this->addColumn($this->tableName, 'chief_firstname', $this->string()->defaultValue(null));
        $this->addColumn($this->tableName, 'chief_lastname', $this->string()->defaultValue(null));
        $this->addColumn($this->tableName, 'chief_patronymic', $this->string()->defaultValue(null));
        $this->addColumn($this->tableName, 'chief_position', $this->string()->defaultValue(null));
        $this->addColumn($this->tableName, 'base', $this->string()->defaultValue(null));
        $this->addColumn($this->tableName, 'base_number', $this->string()->defaultValue(null));
        $this->addColumn($this->tableName, 'base_date', $this->date()->defaultValue(null));

        $this->addForeignKey($this->tableName . '_contractor_type_id', $this->tableName, 'contractor_type_id', 'company_type', 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_contractor_type_id', $this->tableName);

        $this->dropColumn($this->tableName, 'document_number');
        $this->dropColumn($this->tableName, 'document_date');
        $this->dropColumn($this->tableName, 'inn');
        $this->dropColumn($this->tableName, 'contractor_name');
        $this->dropColumn($this->tableName, 'contractor_type_id');
        $this->dropColumn($this->tableName, 'kpp');
        $this->dropColumn($this->tableName, 'rs');
        $this->dropColumn($this->tableName, 'bik');
        $this->dropColumn($this->tableName, 'bank_name');
        $this->dropColumn($this->tableName, 'ks');
        $this->dropColumn($this->tableName, 'chief_firstname');
        $this->dropColumn($this->tableName, 'chief_lastname');
        $this->dropColumn($this->tableName, 'chief_patronymic');
        $this->dropColumn($this->tableName, 'chief_position');
        $this->dropColumn($this->tableName, 'base');
        $this->dropColumn($this->tableName, 'base_number');
        $this->dropColumn($this->tableName, 'base_date');
    }
}


