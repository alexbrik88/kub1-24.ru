<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211118_113812_alter_rent_entity_rent_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%rent_entity_rent}}', 'company_id', $this->integer()->notNull()->after('id'));

        $this->execute('
            UPDATE {{%rent_entity_rent}} {{r}}
            LEFT JOIN {{%rent_entity}} {{e}} ON {{e}}.[[id]] = {{r}}.[[entity_id]]
            SET {{r}}.[[company_id]] = {{e}}.[[company_id]]
        ');

        $this->delete('{{%rent_entity_rent}}', ['company_id' => null]);

        $this->addForeignKey('fk__rent_entity_rent__company_id', '{{%rent_entity_rent}}', 'company_id', '{{%company}}', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk__rent_entity_rent__company_id', '{{%rent_entity_rent}}');

        $this->dropColumn('{{%rent_entity_rent}}', 'company_id');
    }
}
