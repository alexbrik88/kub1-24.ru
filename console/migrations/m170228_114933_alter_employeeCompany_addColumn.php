<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170228_114933_alter_employeeCompany_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%employee_company}}', 'can_sign', $this->integer(1)->notNull()->defaultValue(false));
        $this->addColumn('{{%employee_company}}', 'sign_document_type_id', $this->integer());
        $this->addColumn('{{%employee_company}}', 'sign_document_number', $this->string(50));
        $this->addColumn('{{%employee_company}}', 'sign_document_date', $this->date());
        $this->addColumn('{{%employee_company}}', 'signature_id', $this->integer());

        $this->addForeignKey('FK_employeeCompany_documentType', '{{%employee_company}}', 'sign_document_type_id', '{{%document_type}}', 'id');
        $this->addForeignKey('FK_employeeCompany_employeeSignature', '{{%employee_company}}', 'signature_id', '{{%employee_signature}}', 'id');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('FK_employeeCompany_documentType', '{{%employee_company}}');
        $this->dropForeignKey('FK_employeeCompany_employeeSignature', '{{%employee_company}}');
        $this->dropColumn('{{%employee_company}}', 'can_sign');
        $this->dropColumn('{{%employee_company}}', 'sign_document_type_id');
        $this->dropColumn('{{%employee_company}}', 'sign_document_number');
        $this->dropColumn('{{%employee_company}}', 'sign_document_date');
        $this->dropColumn('{{%employee_company}}', 'signature_id');
    }
}
