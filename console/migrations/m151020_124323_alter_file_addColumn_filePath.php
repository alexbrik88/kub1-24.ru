<?php

use yii\db\Schema;
use yii\db\Migration;

class m151020_124323_alter_file_addColumn_filePath extends Migration
{
    public $tFile = 'file';

    public function safeUp()
    {
        $this->addColumn($this->tFile, 'filepath', $this->string(5000)->notNull() . ' AFTER filename_full');
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tFile, 'filepath');
    }
}
