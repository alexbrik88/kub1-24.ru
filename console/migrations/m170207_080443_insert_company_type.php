<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170207_080443_insert_company_type extends Migration
{
    public $tableName = 'company_type';

    public function safeUp()
    {
        $this->insert($this->tableName, [
            'id' => 20,
            'name_short' => 'ГБКУ',
            'name_full' => 'Государственное бюджетное учреждение культуры',
            'in_company' => false,
            'in_contractor' => true,
        ]);
    }
    
    public function safeDown()
    {
        $this->delete($this->tableName, ['id' => 20]);
    }
}


