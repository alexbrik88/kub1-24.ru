<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190301_000432_update_payment_reminder_message extends Migration
{
    public $tableName = 'payment_reminder_message';

    public function safeUp()
    {
        $this->update($this->tableName, ['event_body' => 'При смене статуса счета на «Оплачен»,<br>
            отправляется письмо Покупателю с благодарность об оплате.'], ['number' => 10]);
    }

    public function safeDown()
    {
        $this->update($this->tableName, ['event_body' => 'До окончания срока оплаты счета осталось "Х" дней.<br>
            Будет отправлено письмо Покупателю с напоминание об оплате.'], ['number' => 10]);
    }
}
