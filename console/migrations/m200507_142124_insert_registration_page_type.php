<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200507_142124_insert_registration_page_type extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%registration_page_type}}', [
            'id' => 30,
            'name' => 'Аналитика',
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%registration_page_type}}', [
            'id' => 30,
        ]);
    }
}
