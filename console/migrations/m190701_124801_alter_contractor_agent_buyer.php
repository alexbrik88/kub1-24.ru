<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190701_124801_alter_contractor_agent_buyer extends Migration
{
    public function safeUp()
    {
        $this->addColumn('contractor_agent_buyer', 'ord', $this->integer(4)->notNull()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('contractor_agent_buyer', 'ord');
    }
}
