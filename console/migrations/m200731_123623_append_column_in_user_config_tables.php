<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200731_123623_append_column_in_user_config_tables extends Migration
{
    public $tableName = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'table_affiliate_affiliate_invoice', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'table_affiliate_affiliate_clients', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'table_affiliate_affiliate_links', $this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'table_affiliate_affiliate_invoice');
        $this->dropColumn($this->tableName, 'table_affiliate_affiliate_clients');
        $this->dropColumn($this->tableName, 'table_affiliate_affiliate_links');
    }
}
