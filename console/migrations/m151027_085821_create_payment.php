<?php

use yii\db\Schema;
use yii\db\Migration;

class m151027_085821_create_payment extends Migration
{
    public $tPayment = 'service_payment';
    public $tPaymentType = 'service_payment_type';
    public $tPaymentPromoCode = 'service_payment_promo_code';

    public function safeUp()
    {
        $this->createTable($this->tPaymentPromoCode, [
            'id' => $this->primaryKey(),
            'code' => 'CHAR(10) NOT NULL',
            'is_used' => $this->boolean()->notNull()->defaultValue(false),
            'created_at' => $this->integer()->notNull(),
            'expired_at' => $this->integer()->notNull(),
        ]);
        $this->createIndex('code_uidx', $this->tPaymentPromoCode, ['code'], true);

        $this->createTable($this->tPaymentType, [
            'id' => $this->primaryKey(),
            'name' => $this->string(45)->notNull(),
        ]);
        $this->batchInsert($this->tPaymentType, ['id', 'name'], [
            [1, 'Онлайн платеж'],
            [2, 'Квитанци сбербанка'],
            [3, 'Промокод'],
        ]);

        $this->createTable($this->tPayment, [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'payment_date' => $this->dateTime()->notNull(),
            'invoice_number' => $this->string(45),
            'sum' => $this->string(45),
            'type_id' => $this->integer()->notNull(),
            'promo_code_id' => $this->integer(),
        ]);
        $this->addForeignKey('fk_payment_to_promo_code', $this->tPayment, 'promo_code_id', $this->tPaymentPromoCode, 'id');
        $this->addForeignKey('fk_payment_to_type', $this->tPayment, 'type_id', $this->tPaymentType, 'id');

    }
    
    public function safeDown()
    {
        $this->dropTable($this->tPayment);
        $this->dropTable($this->tPaymentType);
        $this->dropTable($this->tPaymentPromoCode);
    }
}
