<?php

use console\components\db\Migration;

class m190903_121602_create_amocrm_contacts extends Migration
{
    public function up()
    {
        $this->createTable('{{amocrm_contacts}}', [
            'id' => $this->primaryKey()->unsigned(),
            'name' => $this->string(1000)->notNull(),
            'created_at' => $this->integer()->unsigned()->notNull(),
            'updated_at' => $this->integer()->unsigned()->notNull(),
            'custom_fields' => $this->string(2000),
        ], "COMMENT 'Интеграция AMOcrm: контакты'");

    }

    public function down()
    {
        $this->dropTable('{{amocrm_contacts}}');

    }
}
