<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210604_071525_alter_act_drop_foreign extends Migration
{
    public function safeUp()
    {
        $this->dropForeignKey('FK_act_to_company', 'act');
        $this->dropForeignKey('FK_act_to_contractor', 'act');
        $this->dropForeignKey('FK_upd_to_company', 'upd');
        $this->dropForeignKey('FK_upd_to_contractor', 'upd');
        $this->dropForeignKey('FK_packing_list_to_company', 'packing_list');
        $this->dropForeignKey('FK_packing_list_to_contractor', 'packing_list');
    }

    public function safeDown()
    {

    }
}
