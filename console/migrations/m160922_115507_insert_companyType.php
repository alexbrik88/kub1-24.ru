<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160922_115507_insert_companyType extends Migration
{
    public $tCompanyType = 'company_type';

    public function safeUp()
    {
        $this->insert($this->tCompanyType, [
            'id' => 13,
            'name_short' => 'ЧОУ ДПО',
            'name_full' => 'Частное образовательное учреждение дополнительного профессионального образования',
            'in_company' => 0,
            'in_contractor' => 1,
        ]);
    }

    public function safeDown()
    {
        $this->delete($this->tCompanyType, ['id' => 13]);
    }
}
