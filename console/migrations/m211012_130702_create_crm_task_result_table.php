<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%crm_task_result}}`.
 */
class m211012_130702_create_crm_task_result_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%crm_task_result}}', [
            'task_result_id' => $this->bigPrimaryKey(),
            'company_id' => $this->integer()->notNull(),
            'employee_id' => $this->integer()->null(),
            'status' => $this->boolean()->notNull()->defaultValue(true),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'name' => $this->text()->notNull(),
        ]);

        $this->createIndex('company_id__employee_id', '{{%crm_task_result}}', ['company_id', 'employee_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%crm_task_result}}');
    }
}
