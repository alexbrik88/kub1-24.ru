<?php

use console\components\db\Migration;
use yii\db\Schema;

class m161201_171411_alter_bank_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('bank', 'bik', $this->string(255)->defaultValue('')->append('after `id`'));
    }
    
    public function safeDown()
    {
        $this->dropColumn('bank', 'bik');
    }
}


