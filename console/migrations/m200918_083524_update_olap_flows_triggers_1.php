<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200918_083524_update_olap_flows_triggers_1 extends Migration
{
    public function safeUp()
    {
        $this->createTriggersOnCashBankFlowsToInvoices();
        $this->createTriggersOnCashOrderFlowsToInvoices();
        $this->createTriggersOnCashEmoneyFlowsToInvoices();
    }

    public function safeDown()
    {
        $this->execute('DROP PROCEDURE IF EXISTS `OlapFlowsRecalculateBankByFlow`');
        $this->execute('DROP PROCEDURE IF EXISTS `OlapFlowsRecalculateOrderByFlow`');
        $this->execute('DROP PROCEDURE IF EXISTS `OlapFlowsRecalculateEmoneyByFlow`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_bank_flow_to_invoice`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_delete_cash_bank_flow_to_invoice`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_order_flow_to_invoice`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_delete_cash_order_flow_to_invoice`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_emoney_flow_to_invoice`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_delete_cash_emoney_flow_to_invoice`');
    }

    private function createTriggersOnCashBankFlowsToInvoices()
    {

        $this->execute('DROP PROCEDURE IF EXISTS `OlapFlowsRecalculateBankByFlow`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_bank_flow_to_invoice`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_delete_cash_bank_flow_to_invoice`');

        $this->execute('
            CREATE PROCEDURE `OlapFlowsRecalculateBankByFlow`(flowID INT)
            BEGIN
            
              UPDATE `olap_flows` 
              LEFT JOIN 
                (SELECT 
                    `cash_bank_flow_to_invoice`.`flow_id` AS `flow_id`,
                    IF (COUNT(`invoice`.`id`), 1, 0) AS `has_invoice`,
                    SUM(`invoice`.`total_amount_with_nds`) AS `total_amount_with_nds`,
                    SUM(`product`.`price_for_buy_with_nds` * `order`.`quantity`) AS `amount_in_price_for_buy`,
                    IF (SUM(`invoice`.`has_act`) + SUM(`invoice`.`has_packing_list`) + SUM(`invoice`.`has_upd`), 1, 0) AS `has_doc`,
                    IF (SUM( IF(`invoice`.`need_act`, 0, 1)) + SUM( IF(`invoice`.`need_packing_list`, 0, 1)) + SUM( IF(`invoice`.`need_upd`, 0, 1)), 0, 1) AS `need_doc`	
                  FROM `cash_bank_flow_to_invoice`
                  LEFT JOIN `invoice` ON `cash_bank_flow_to_invoice`.`invoice_id` = `invoice`.`id`
                  LEFT JOIN `order` ON `order`.`invoice_id` = `invoice`.`id`
                  LEFT JOIN `product` ON `order`.`product_id` = `product`.`id`	  
                  WHERE `cash_bank_flow_to_invoice`.`flow_id` = flowID
                  GROUP BY `cash_bank_flow_to_invoice`.`flow_id`
                ) `invoices` ON `olap_flows`.`id` = `invoices`.`flow_id`
                
              SET
            
                `olap_flows`.`has_invoice` = IFNULL(`invoices`.`has_invoice`, 0),
                `olap_flows`.`invoice_amount` = IFNULL(`invoices`.`total_amount_with_nds`, 0),
                `olap_flows`.`invoice_amount_in_price_for_buy` = IFNULL(`invoices`.`amount_in_price_for_buy`, 0),     
                `olap_flows`.`has_doc` = IFNULL(`invoices`.`has_doc`, 0),
                `olap_flows`.`need_doc` = IFNULL(`invoices`.`need_doc`, 0)
              
              WHERE `olap_flows`.`wallet` = "1" AND `olap_flows`.`id` = flowID LIMIT 1;
            
            END');
        $this->execute('
            CREATE TRIGGER `olap_flows_insert_cash_bank_flow_to_invoice`
            AFTER INSERT ON `cash_bank_flow_to_invoice`
            FOR EACH ROW
            BEGIN
            
               CALL `OlapFlowsRecalculateBankByFlow`(NEW.`flow_id`);
              
            END');
        $this->execute('
            CREATE TRIGGER `olap_flows_delete_cash_bank_flow_to_invoice`
            AFTER DELETE ON `cash_bank_flow_to_invoice`
            FOR EACH ROW
            BEGIN
            
               CALL `OlapFlowsRecalculateBankByFlow`(OLD.`flow_id`);
              
            END');
    }

    private function createTriggersOnCashOrderFlowsToInvoices()
    {

        $this->execute('DROP PROCEDURE IF EXISTS `OlapFlowsRecalculateOrderByFlow`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_order_flow_to_invoice`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_delete_cash_order_flow_to_invoice`');

        $this->execute('           
            CREATE PROCEDURE `OlapFlowsRecalculateOrderByFlow`(flowID INT)
            BEGIN
            
              UPDATE `olap_flows` 
              LEFT JOIN 
                (SELECT 
                    `cash_order_flow_to_invoice`.`flow_id` AS `flow_id`,
                    IF (COUNT(`invoice`.`id`), 1, 0) AS `has_invoice`,
                    SUM(`invoice`.`total_amount_with_nds`) AS `total_amount_with_nds`,
                    SUM(`product`.`price_for_buy_with_nds` * `order`.`quantity`) AS `amount_in_price_for_buy`,
                    IF (SUM(`invoice`.`has_act`) + SUM(`invoice`.`has_packing_list`) + SUM(`invoice`.`has_upd`), 1, 0) AS `has_doc`,
                    IF (SUM( IF(`invoice`.`need_act`, 0, 1)) + SUM( IF(`invoice`.`need_packing_list`, 0, 1)) + SUM( IF(`invoice`.`need_upd`, 0, 1)), 0, 1) AS `need_doc`	
                  FROM `cash_order_flow_to_invoice`
                  LEFT JOIN `invoice` ON `cash_order_flow_to_invoice`.`invoice_id` = `invoice`.`id`
                  LEFT JOIN `order` ON `order`.`invoice_id` = `invoice`.`id`
                  LEFT JOIN `product` ON `order`.`product_id` = `product`.`id`	  
                  WHERE `cash_order_flow_to_invoice`.`flow_id` = flowID
                  GROUP BY `cash_order_flow_to_invoice`.`flow_id`
                ) `invoices` ON `olap_flows`.`id` = `invoices`.`flow_id`
                
              SET
            
                `olap_flows`.`has_invoice` = IFNULL(`invoices`.`has_invoice`, 0),
                `olap_flows`.`invoice_amount` = IFNULL(`invoices`.`total_amount_with_nds`, 0),
                `olap_flows`.`invoice_amount_in_price_for_buy` = IFNULL(`invoices`.`amount_in_price_for_buy`, 0),     
                `olap_flows`.`has_doc` = IFNULL(`invoices`.`has_doc`, 0),
                `olap_flows`.`need_doc` = IFNULL(`invoices`.`need_doc`, 0)
              
              WHERE `olap_flows`.`wallet` = "2" AND `olap_flows`.`id` = flowID LIMIT 1;
            
            END');
        $this->execute('
            CREATE TRIGGER `olap_flows_insert_cash_order_flow_to_invoice`
            AFTER INSERT ON `cash_order_flow_to_invoice`
            FOR EACH ROW
            BEGIN
            
               CALL `OlapFlowsRecalculateOrderByFlow`(NEW.`flow_id`);
              
            END');
        $this->execute('
            CREATE TRIGGER `olap_flows_delete_cash_order_flow_to_invoice`
            AFTER DELETE ON `cash_order_flow_to_invoice`
            FOR EACH ROW
            BEGIN
            
               CALL `OlapFlowsRecalculateOrderByFlow`(OLD.`flow_id`);
              
            END');
    }

    private function createTriggersOnCashEmoneyFlowsToInvoices()
    {
        $this->execute('DROP PROCEDURE IF EXISTS `OlapFlowsRecalculateEmoneyByFlow`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_emoney_flow_to_invoice`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_delete_cash_emoney_flow_to_invoice`');

        $this->execute('
            CREATE PROCEDURE `OlapFlowsRecalculateEmoneyByFlow`(flowID INT)
            BEGIN
            
              UPDATE `olap_flows` 
              LEFT JOIN 
                (SELECT 
                    `cash_emoney_flow_to_invoice`.`flow_id` AS `flow_id`,
                    IF (COUNT(`invoice`.`id`), 1, 0) AS `has_invoice`,
                    SUM(`invoice`.`total_amount_with_nds`) AS `total_amount_with_nds`,
                    SUM(`product`.`price_for_buy_with_nds` * `order`.`quantity`) AS `amount_in_price_for_buy`,
                    IF (SUM(`invoice`.`has_act`) + SUM(`invoice`.`has_packing_list`) + SUM(`invoice`.`has_upd`), 1, 0) AS `has_doc`,
                    IF (SUM( IF(`invoice`.`need_act`, 0, 1)) + SUM( IF(`invoice`.`need_packing_list`, 0, 1)) + SUM( IF(`invoice`.`need_upd`, 0, 1)), 0, 1) AS `need_doc`	
                  FROM `cash_emoney_flow_to_invoice`
                  LEFT JOIN `invoice` ON `cash_emoney_flow_to_invoice`.`invoice_id` = `invoice`.`id`
                  LEFT JOIN `order` ON `order`.`invoice_id` = `invoice`.`id`
                  LEFT JOIN `product` ON `order`.`product_id` = `product`.`id`
                  WHERE `cash_emoney_flow_to_invoice`.`flow_id` = flowID 	  
                  GROUP BY `cash_emoney_flow_to_invoice`.`flow_id`
                ) `invoices` ON `olap_flows`.`id` = `invoices`.`flow_id`
                
              SET
            
                `olap_flows`.`has_invoice` = IFNULL(`invoices`.`has_invoice`, 0),
                `olap_flows`.`invoice_amount` = IFNULL(`invoices`.`total_amount_with_nds`, 0),
                `olap_flows`.`invoice_amount_in_price_for_buy` = IFNULL(`invoices`.`amount_in_price_for_buy`, 0),     
                `olap_flows`.`has_doc` = IFNULL(`invoices`.`has_doc`, 0),
                `olap_flows`.`need_doc` = IFNULL(`invoices`.`need_doc`, 0)
              
              WHERE `olap_flows`.`wallet` = "3" AND `olap_flows`.`id` = flowID LIMIT 1;
            
            END');
        $this->execute('
            CREATE TRIGGER `olap_flows_insert_cash_emoney_flow_to_invoice`
            AFTER INSERT ON `cash_emoney_flow_to_invoice`
            FOR EACH ROW
            BEGIN
            
               CALL `OlapFlowsRecalculateEmoneyByFlow`(NEW.`flow_id`);
              
            END');
        $this->execute('
            CREATE TRIGGER `olap_flows_delete_cash_emoney_flow_to_invoice`
            AFTER DELETE ON `cash_emoney_flow_to_invoice`
            FOR EACH ROW
            BEGIN
            
               CALL `OlapFlowsRecalculateEmoneyByFlow`(OLD.`flow_id`);
              
            END');
    }
}
