<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211112_094419_alter_project_customer_add_fk extends Migration
{
    public $table = 'project_customer';

    public function safeUp()
    {
        $table = $this->table;

        $this->execute("DELETE FROM {$table} WHERE NOT project_id IN (SELECT id FROM project)");
        $this->execute("DELETE FROM {$table} WHERE NOT customer_id IN (SELECT id FROM contractor)");

        $this->addForeignKey("FK_{$table}_to_project", $table, 'project_id', 'project', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("FK_{$table}_to_contractor", $table, 'customer_id', 'contractor', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $table = $this->table;

        try {
            $this->dropForeignKey("FK_{$table}_to_project", $table);
            $this->dropForeignKey("FK_{$table}_to_contractor", $table);
        } catch (Throwable $e) {
            echo "\nForeign key not found";
        }
    }
}
