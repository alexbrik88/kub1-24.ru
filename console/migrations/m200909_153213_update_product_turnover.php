<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200909_153213_update_product_turnover extends Migration
{
    public function safeUp()
    {
        $this->truncateTable('{{product_turnover}}');

        $this->execute('
            INSERT INTO `product_turnover` (
                `order_id`,
                `document_id`,
                `document_table`,
                `invoice_id`,
                `contractor_id`,
                `company_id`,
                `date`,
                `type`,
                `production_type`,
                `is_invoice_actual`,
                `is_document_actual`,
                `purchase_price`,
                `price_one`,
                `quantity`,
                `total_amount`,
                `purchase_amount`,
                `margin`,
                `year`,
                `month`,
                `product_group_id`,
                `product_id`
            )
            SELECT
                `order`.`id`,
                `document`.`id`,
                "act",
                `invoice`.`id`,
                `invoice`.`contractor_id`,
                `invoice`.`company_id`,
                `document`.`document_date`,
                `document`.`type`,
                `product`.`production_type`,
                IF(`invoice`.`is_deleted` = 0 AND `invoice`.`invoice_status_id` IN (1, 2, 3, 4, 6, 7, 8), 1, 0),
                IF(`document`.`status_out_id` IN (1, 2, 3, 4), 1, 0),
                (@purchase_price:=IFNULL(`product`.`price_for_buy_with_nds`, 0)),
                (@price:=IF(`document`.`type` = 1, `order`.`purchase_price_with_vat`, `order`.`selling_price_with_vat`)),
                (@quantity:=IFNULL(`document_order`.`quantity`, 0)),
                (@total:=ROUND(@price * @quantity, IF(`invoice`.`price_precision` = 4, 2, 0))),
                (@purchase:=IF(`invoice`.`type` = 2 AND `product`.`production_type` = 1, ROUND(@quantity * @purchase_price), 0)),
                IF(`invoice`.`type` = 2 AND `product`.`production_type` = 1, @total - @purchase, 0),
                YEAR(`document`.`document_date`),
                MONTH(`document`.`document_date`),
                `product`.`group_id`,
                `order`.`product_id`
            FROM `order_act` AS `document_order`
            LEFT JOIN `act` AS `document` ON `document`.`id` = `document_order`.`act_id`
            LEFT JOIN `order` ON `order`.`id` = `document_order`.`order_id`
            LEFT JOIN `invoice` ON `invoice`.`id` = `order`.`invoice_id`
            LEFT JOIN `product` ON `product`.`id` = `order`.`product_id`
            ON DUPLICATE KEY UPDATE
                `date` = VALUES(`date`),
                `type` = VALUES(`type`),
                `production_type` = VALUES(`production_type`),
                `purchase_price` = VALUES(`purchase_price`),
                `price_one` = VALUES(`price_one`),
                `quantity` = VALUES(`quantity`),
                `total_amount` = VALUES(`total_amount`),
                `purchase_amount` = VALUES(`purchase_amount`),
                `margin` = VALUES(`margin`),
                `year` = VALUES(`year`),
                `month` = VALUES(`month`),
                `product_group_id` = VALUES(`product_group_id`),
                `product_id` = VALUES(`product_id`)
        ');
        $this->execute('
            INSERT INTO `product_turnover` (
                `order_id`,
                `document_id`,
                `document_table`,
                `invoice_id`,
                `contractor_id`,
                `company_id`,
                `date`,
                `type`,
                `production_type`,
                `is_invoice_actual`,
                `is_document_actual`,
                `purchase_price`,
                `price_one`,
                `quantity`,
                `total_amount`,
                `purchase_amount`,
                `margin`,
                `year`,
                `month`,
                `product_group_id`,
                `product_id`
            )
            SELECT
                `order`.`id`,
                `document`.`id`,
                "packing_list",
                `invoice`.`id`,
                `invoice`.`contractor_id`,
                `invoice`.`company_id`,
                `document`.`document_date`,
                `document`.`type`,
                `product`.`production_type`,
                IF(`invoice`.`is_deleted` = 0 AND `invoice`.`invoice_status_id` IN (1, 2, 3, 4, 6, 7, 8), 1, 0),
                IF(`document`.`status_out_id` IN (1, 2, 3, 4), 1, 0),
                (@purchase_price:=IFNULL(`product`.`price_for_buy_with_nds`, 0)),
                (@price:=IF(`document`.`type` = 1, `order`.`purchase_price_with_vat`, `order`.`selling_price_with_vat`)),
                (@quantity:=IFNULL(`document_order`.`quantity`, 0)),
                (@total:=ROUND(@price * @quantity, IF(`invoice`.`price_precision` = 4, 2, 0))),
                (@purchase:=IF(`invoice`.`type` = 2 AND `product`.`production_type` = 1, ROUND(@quantity * @purchase_price), 0)),
                IF(`invoice`.`type` = 2 AND `product`.`production_type` = 1, @total - @purchase, 0),
                YEAR(`document`.`document_date`),
                MONTH(`document`.`document_date`),
                `product`.`group_id`,
                `order`.`product_id`
            FROM `order_packing_list` AS `document_order`
            LEFT JOIN `packing_list` AS `document` ON `document`.`id` = `document_order`.`packing_list_id`
            LEFT JOIN `order` ON `order`.`id` = `document_order`.`order_id`
            LEFT JOIN `invoice` ON `invoice`.`id` = `order`.`invoice_id`
            LEFT JOIN `product` ON `product`.`id` = `order`.`product_id`
            ON DUPLICATE KEY UPDATE
                `date` = VALUES(`date`),
                `type` = VALUES(`type`),
                `production_type` = VALUES(`production_type`),
                `purchase_price` = VALUES(`purchase_price`),
                `price_one` = VALUES(`price_one`),
                `quantity` = VALUES(`quantity`),
                `total_amount` = VALUES(`total_amount`),
                `purchase_amount` = VALUES(`purchase_amount`),
                `margin` = VALUES(`margin`),
                `year` = VALUES(`year`),
                `month` = VALUES(`month`),
                `product_group_id` = VALUES(`product_group_id`),
                `product_id` = VALUES(`product_id`)
        ');
        $this->execute('
            INSERT INTO `product_turnover` (
                `order_id`,
                `document_id`,
                `document_table`,
                `invoice_id`,
                `contractor_id`,
                `company_id`,
                `date`,
                `type`,
                `production_type`,
                `is_invoice_actual`,
                `is_document_actual`,
                `purchase_price`,
                `price_one`,
                `quantity`,
                `total_amount`,
                `purchase_amount`,
                `margin`,
                `year`,
                `month`,
                `product_group_id`,
                `product_id`
            )
            SELECT
                `order`.`id`,
                `document`.`id`,
                "upd",
                `invoice`.`id`,
                `invoice`.`contractor_id`,
                `invoice`.`company_id`,
                `document`.`document_date`,
                `document`.`type`,
                `product`.`production_type`,
                IF(`invoice`.`is_deleted` = 0 AND `invoice`.`invoice_status_id` IN (1, 2, 3, 4, 6, 7, 8), 1, 0),
                IF(`document`.`status_out_id` IN (1, 2, 3, 4), 1, 0),
                (@purchase_price:=IFNULL(`product`.`price_for_buy_with_nds`, 0)),
                (@price:=IF(`document`.`type` = 1, `order`.`purchase_price_with_vat`, `order`.`selling_price_with_vat`)),
                (@quantity:=IFNULL(`document_order`.`quantity`, 0)),
                (@total:=ROUND(@price * @quantity, IF(`invoice`.`price_precision` = 4, 2, 0))),
                (@purchase:=IF(`invoice`.`type` = 2 AND `product`.`production_type` = 1, ROUND(@quantity * @purchase_price), 0)),
                IF(`invoice`.`type` = 2 AND `product`.`production_type` = 1, @total - @purchase, 0),
                YEAR(`document`.`document_date`),
                MONTH(`document`.`document_date`),
                `product`.`group_id`,
                `order`.`product_id`
            FROM `order_upd` AS `document_order`
            LEFT JOIN `upd` AS `document` ON `document`.`id` = `document_order`.`upd_id`
            LEFT JOIN `order` ON `order`.`id` = `document_order`.`order_id`
            LEFT JOIN `invoice` ON `invoice`.`id` = `order`.`invoice_id`
            LEFT JOIN `product` ON `product`.`id` = `order`.`product_id`
            ON DUPLICATE KEY UPDATE
                `date` = VALUES(`date`),
                `type` = VALUES(`type`),
                `production_type` = VALUES(`production_type`),
                `purchase_price` = VALUES(`purchase_price`),
                `price_one` = VALUES(`price_one`),
                `quantity` = VALUES(`quantity`),
                `total_amount` = VALUES(`total_amount`),
                `purchase_amount` = VALUES(`purchase_amount`),
                `margin` = VALUES(`margin`),
                `year` = VALUES(`year`),
                `month` = VALUES(`month`),
                `product_group_id` = VALUES(`product_group_id`),
                `product_id` = VALUES(`product_id`)
        ');
    }

    public function safeDown()
    {
        //
    }
}
