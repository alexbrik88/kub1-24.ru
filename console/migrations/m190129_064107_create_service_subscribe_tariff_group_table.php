<?php

use yii\db\Migration;

/**
 * Handles the creation of table `service_subscribe_tariff_group`.
 */
class m190129_064107_create_service_subscribe_tariff_group_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $group1 = [
            'Выставление счетов',
            'Заполнение реквизитов по ИНН',
            'Отправка счетов на e-mail клиентам',
            'Добавление в счет логотипа, печати и подписи',
        ];
        $group2 = [
            'Заявки',
            'Договора-заявки',
            'Договора',
            'ТТН',
            'Путевые листы',
            'Учет транспорта',
        ];
        $group3 = [
            'На вашем сайте',
            'Выставление счетов для ООО и ИП',
            'Прием онлайн платежей от ООО и ИП',
            'Подготовка покупателем Доверенности на получение у Вас оплаченного товара',
        ];
        $group4 = [
            'Автоматический расчёт налогов и подготовка платёжек для их уплаты',
            'Отчётность в налоговую, кроме отчётов за сотрудников',
            'Автоматическая подготовка налоговой декларации и КУДиР',
        ];
        $group5 = [
            'Отчет о Движении Денежных Средств',
            'Платежный календарь',
            'Отчет о Прибылях и Убытках',
            'Баланс',
            'Отчет по продажам',
        ];

        $this->createTable('service_subscribe_tariff_group', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'is_base' => $this->boolean()->notNull(),
            'has_base' => $this->boolean()->notNull(),
            'is_active' => $this->boolean()->notNull(),
            'description' => $this->text()->notNull(),
        ]);

        $this->createIndex('is_base', 'service_subscribe_tariff_group', 'is_base');
        $this->createIndex('has_base', 'service_subscribe_tariff_group', 'has_base');
        $this->createIndex('is_active', 'service_subscribe_tariff_group', 'is_active');

        $this->batchInsert('service_subscribe_tariff_group', [
            'id',
            'name',
            'is_base',
            'has_base',
            'is_active',
            'description',
        ], [
            [1, 'Стандарт', 1, 1, 1, implode('|', $group1)],
            [2, 'Логистика', 0, 1, 0, implode('|', $group2)],
            [3, 'Модуль B2B оплат', 0, 1, 0, implode('|', $group3)],
            [4, 'Бухгалтерия ИП на УСН 6%', 0, 0, 0, implode('|', $group4)],
            [5, 'Аналитика бизнеса', 0, 0, 0, implode('|', $group5)],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('service_subscribe_tariff_group');
    }
}
