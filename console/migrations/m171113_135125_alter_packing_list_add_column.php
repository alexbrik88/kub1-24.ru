<?php

use console\components\db\Migration;
use yii\db\Schema;

class m171113_135125_alter_packing_list_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('packing_list', 'add_stamp', $this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('packing_list', 'add_stamp');
    }
}
