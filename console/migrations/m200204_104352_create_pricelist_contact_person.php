<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200204_104352_create_pricelist_contact_person extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%price_list_contact}}', [
            'id' => Schema::TYPE_PK,
            'price_list_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'lastname' => Schema::TYPE_STRING . ' DEFAULT NULL',
            'firstname' => Schema::TYPE_STRING . ' DEFAULT NULL',
            'patronymic' => Schema::TYPE_STRING . ' DEFAULT NULL',
            'email' => Schema::TYPE_STRING . ' DEFAULT NULL',
            'site' => Schema::TYPE_STRING . ' DEFAULT NULL',
            'phone' => Schema::TYPE_STRING . ' DEFAULT NULL',
            'no_patronymic' => Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0',
        ], $tableOptions);

        $this->addForeignKey('FK_price_list_contact_to_price_list', '{{%price_list_contact}}', 'price_list_id', '{{%price_list}}', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_price_list_contact_to_price_list', '{{%price_list_contact}}');
        $this->dropTable('{{%price_list_contact}}');
    }
}
