<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181004_193848_insert_into_agreement_type extends Migration
{
    public $tableName = 'agreement_type';

    public function safeUp()
    {
        $this->insert($this->tableName, [
            'id' => 8,
            'name' => 'Офферта',
            'name_dative' => 'офферте',
            'sort' => 55,
        ]);
    }
    
    public function safeDown()
    {
        $this->delete($this->tableName, ['id' => 8]);
    }
}


