<?php

use yii\db\Exception;
use yii\db\Migration;

class m210607_120848_update_contractor_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%contractor}}';

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function safeUp(): void
    {
        $rows = $this->db->createCommand("
            SELECT contractor_id, client_activity_id, client_campaign_id
            FROM crm_client
            WHERE ((client_activity_id IS NOT NULL) OR (client_campaign_id IS NOT NULL))
        ")->queryAll();

        array_walk($rows, function ($row): void {
            $this->update(
                self::TABLE_NAME,
                ['activity_id' => $row['client_activity_id'], 'campaign_id' => $row['client_campaign_id']],
                ['id' => $row['contractor_id']]
            );
        });
    }
}
