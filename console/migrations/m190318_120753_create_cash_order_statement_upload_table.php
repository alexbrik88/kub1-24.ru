<?php

use yii\db\Migration;

/**
 * Handles the creation of table `cash_order_statement_upload`.
 */
class m190318_120753_create_cash_order_statement_upload_table extends Migration
{
    public $tStatement = 'cash_order_statement_upload';

    public function safeUp()
    {
        $this->createTable($this->tStatement, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'cashbox_id' => $this->integer()->notNull(),
            'service' => $this->string(),
            'source' => $this->integer()->notNull(),
            'period_from' => $this->date(),
            'period_till' => $this->date(),
            'created_by' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'saved_count' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('FK_order_statement_upload_to_employee', $this->tStatement, 'created_by', 'employee', 'id');
        $this->addForeignKey('FK_order_statement_upload_to_company', $this->tStatement, 'company_id', 'company', 'id');
        $this->addForeignKey('FK_order_statement_upload_to_cashbox', $this->tStatement, 'cashbox_id', 'cashbox', 'id');
        $this->createIndex('created_at', $this->tStatement, 'created_at', false);
        $this->createIndex('service', $this->tStatement, 'service', false);
        $this->createIndex('source', $this->tStatement, 'source', false);
    }

    public function safeDown()
    {
        $this->dropTable($this->tStatement);
    }
}
