<?php

use console\components\db\Migration;

class m160104_142122_alter_promoCode_changeColumns_activatedAt_startedAt extends Migration
{
    public $tPromoCode = 'service_payment_promo_code';

    public function safeUp()
    {
        $this->alterColumn($this->tPromoCode, 'activated_at', $this->integer() . ' AFTER `created_at`');
        $this->alterColumn($this->tPromoCode, 'started_at', $this->integer() . ' AFTER `activated_at`');
    }

    public function safeDown()
    {
        $this->alterColumn($this->tPromoCode, 'activated_at', $this->integer() . ' AFTER `expired_at`');
        $this->alterColumn($this->tPromoCode, 'started_at', $this->integer() . ' AFTER `code`');
    }
}


