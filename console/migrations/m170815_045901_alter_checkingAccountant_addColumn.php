<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170815_045901_alter_checkingAccountant_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%checking_accountant}}', 'autoload_mode_id', $this->integer()->defaultValue(null));

        $this->addForeignKey(
        	'FK_checkingAccountant_bankStatementAutoloadMode',
        	'{{%checking_accountant}}',
        	'autoload_mode_id',
        	'{{%bank_statement_autoload_mode}}',
        	'id'
    	);
    }
    
    public function safeDown()
    {


        $this->dropForeignKey('FK_checkingAccountant_bankStatementAutoloadMode', '{{%checking_accountant}}');

        $this->dropColumn('{{%checking_accountant}}', 'autoload_mode_id');
    }
}
