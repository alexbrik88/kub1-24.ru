<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160113_211250_alter_set_defaut_value_time_zone_id extends Migration
{
    public $tEmployee = 'employee';

    public $tCompany = 'company';

    public function safeUp()
    {
        $oldTimeZoneId = range(108, 500);
        $this->alterColumn($this->tEmployee, 'time_zone_id', $this->integer(11)->defaultValue(57));
        $this->alterColumn($this->tCompany, 'time_zone_id', $this->integer(11)->defaultValue(57));
        $this->update($this->tEmployee, ['time_zone_id' => 57], ['time_zone_id' => $oldTimeZoneId]);
        $this->update($this->tCompany, ['time_zone_id' => 57], ['time_zone_id' => $oldTimeZoneId]);
    }
    
    public function safeDown()
    {
        echo 'This migration should not be rolled back';
    }
}


