<?php

use console\components\db\Migration;
use yii\db\Expression;
use yii\db\Schema;

class m180807_141454_alter_agreement_type_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('agreement_type', 'sort', $this->integer()->notNull()->defaultValue(1000));

        $this->update('agreement_type', ['sort' => new Expression('[[id]] * 10')]);
    }

    public function safeDown()
    {
        $this->dropColumn('agreement_type', 'sort');
    }
}
