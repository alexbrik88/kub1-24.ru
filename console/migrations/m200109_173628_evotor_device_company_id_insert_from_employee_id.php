<?php

use common\models\employee\Employee;
use common\models\evotor\Device;
use console\components\db\Migration;

class m200109_173628_evotor_device_company_id_insert_from_employee_id extends Migration
{
    const TABLE = '{{%evotor_device}}';

    public function safeUp()
    {
        foreach (Device::find()->all() as $device) {
            /** @var Device $device */
            /** @noinspection PhpUndefinedFieldInspection */
            $device->company_id = Employee::findOne(['id' => $device->employee_id])->company_id;
            $device->save();
        }
    }

    public function down()
    {
        $this->update(self::TABLE, ['company_id' => 0]);
    }
}
