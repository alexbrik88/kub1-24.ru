<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200206_154731_alter_price_list_drop_column extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('price_list', 'is_archive');
    }

    public function safeDown()
    {
        $this->addColumn('price_list', 'is_archive', $this->boolean()->notNull()->defaultValue(false));
    }
}
