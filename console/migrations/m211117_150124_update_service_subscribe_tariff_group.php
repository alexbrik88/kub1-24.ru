<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211117_150124_update_service_subscribe_tariff_group extends Migration
{
    public function safeUp()
    {
        $data = [
            [
                'columns' => [
                    'priority' => 3, // 7
                ],
                'condition' => [
                    'id' => 15,
                ],
            ],
            [
                'columns' => [
                    'priority' => 6, // 3
                ],
                'condition' => [
                    'id' => 19,
                ],
            ],
            [
                'columns' => [
                    'priority' => 7, // 8
                ],
                'condition' => [
                    'id' => 17,
                ],
            ],
            [
                'columns' => [
                    'priority' => 8, // 6
                ],
                'condition' => [
                    'id' => 7,
                ],
            ],
            [
                'columns' => [
                    'priority' => 9, // 10
                ],
                'condition' => [
                    'id' => 3,
                ],
            ],
            [
                'columns' => [
                    'priority' => 10, // 12
                ],
                'condition' => [
                    'id' => 10,
                ],
            ],
            [
                'columns' => [
                    'priority' => 11, // 9
                ],
                'condition' => [
                    'id' => 4,
                ],
            ],
            [
                'columns' => [
                    'is_active' => 0,
                ],
                'condition' => [
                    'id' => 6,
                ],
            ],
        ];

        foreach ($data as $item) {
            $this->update('{{%service_subscribe_tariff_group}}', $item['columns'], $item['condition']);
        }
    }

    public function safeDown()
    {
        $data = [
            [
                'columns' => [
                    'priority' => 7
                ],
                'condition' => [
                    'id' => 15,
                ],
            ],
            [
                'columns' => [
                    'priority' => 3
                ],
                'condition' => [
                    'id' => 19,
                ],
            ],
            [
                'columns' => [
                    'priority' => 8
                ],
                'condition' => [
                    'id' => 17,
                ],
            ],
            [
                'columns' => [
                    'priority' => 6
                ],
                'condition' => [
                    'id' => 7,
                ],
            ],
            [
                'columns' => [
                    'priority' => 10
                ],
                'condition' => [
                    'id' => 3,
                ],
            ],
            [
                'columns' => [
                    'priority' => 12
                ],
                'condition' => [
                    'id' => 10,
                ],
            ],
            [
                'columns' => [
                    'priority' => 9
                ],
                'condition' => [
                    'id' => 4,
                ],
            ],
        ];

        foreach ($data as $item) {
            $this->update('{{%service_subscribe_tariff_group}}', $item['columns'], $item['condition']);
        }
    }
}
