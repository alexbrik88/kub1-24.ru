<?php

use console\components\db\Migration;
use yii\db\Schema;

class m171011_125908_alter_service_payment_addColumn extends Migration
{
    public $tableName = 'service_payment';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'payment_for', "ENUM('subscribe', 'taxrobot') NOT NULL DEFAULT 'subscribe' AFTER [[company_id]]");
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'payment_for');
    }
}
