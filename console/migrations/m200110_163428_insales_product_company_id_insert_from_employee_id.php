<?php

use common\models\employee\Employee;
use common\models\insales\Product;
use console\components\db\Migration;

class m200110_163428_insales_product_company_id_insert_from_employee_id extends Migration
{
    const TABLE = '{{%insales_product}}';

    public function safeUp()
    {
        foreach (Product::find()->all() as $product) {
            /** @var Product $product */
            /** @noinspection PhpUndefinedFieldInspection */
            $product->company_id = Employee::findOne(['id' => $product->employee_id])->company_id;
            $product->save();
        }
    }

    public function down()
    {
        $this->update(self::TABLE, ['company_id' => 0]);
    }
}
