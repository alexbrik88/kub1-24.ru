<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190213_210532_alter_contractor_drop_columns extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('contractor', 'current_account');
        $this->dropColumn('contractor', 'bank_name');
        $this->dropColumn('contractor', 'BIC');
        $this->dropColumn('contractor', 'corresp_account');
    }

    public function safeDown()
    {
        $this->addColumn('contractor', 'current_account', $this->string(50)->after('PPC'));
        $this->addColumn('contractor', 'bank_name', $this->string()->after('current_account'));
        $this->addColumn('contractor', 'BIC', $this->string(50)->after('bank_name'));
        $this->addColumn('contractor', 'corresp_account', $this->string(50)->after('BIC'));

        $this->execute('
            UPDATE {{contractor}}
            INNER JOIN {{contractor_account}} ON {{contractor_account}}.[[contractor_id]] = {{contractor}}.[[id]]
            SET {{contractor}}.[[current_account]] = {{contractor_account}}.[[rs]],
                {{contractor}}.[[bank_name]] = {{contractor_account}}.[[bank_name]],
                {{contractor}}.[[BIC]] = {{contractor_account}}.[[bik]],
                {{contractor}}.[[corresp_account]] = {{contractor_account}}.[[ks]]
            WHERE {{contractor_account}}.[[contractor_id]] IS NOT NULL
        ');
    }
}
