<?php

use console\components\db\Migration;
use yii\db\Schema;

class m171201_133502_alter_agreement_changeFK extends Migration
{
    public function up()
    {
        $this->dropForeignKey('fk_agreement_company', '{{%agreement}}');
        $this->dropForeignKey('fk_agreement_contractor', '{{%agreement}}');

        $this->addForeignKey('fk_agreement_company', '{{%agreement}}', 'company_id', '{{%company}}', 'id', 'CASCADE');
        $this->addForeignKey('fk_agreement_contractor', '{{%agreement}}', 'contractor_id', '{{%contractor}}', 'id', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fk_agreement_company', '{{%agreement}}');
        $this->dropForeignKey('fk_agreement_contractor', '{{%agreement}}');

        $this->addForeignKey('fk_agreement_company', '{{%agreement}}', 'company_id', '{{%company}}', 'id');
        $this->addForeignKey('fk_agreement_contractor', '{{%agreement}}', 'contractor_id', '{{%contractor}}', 'id');
    }
}
