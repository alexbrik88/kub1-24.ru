<?php

use yii\db\Schema;
use yii\db\Migration;

class m150420_131718_alter_paking_list_status extends Migration
{
    public function up()
    {
        $this->renameColumn('packing_list_status', 'act_title', 'packing_list_title');
    }

    public function down()
    {
        $this->renameColumn('packing_list_status', 'packing_list_title', 'act_title');
    }

}
