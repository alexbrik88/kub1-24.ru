<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181113_105100_add_out_download_count_to_invoicey extends Migration
{
    public $tableName = 'invoice';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'out_download_count', $this->integer()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'out_download_count');
    }
}


