<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190919_053930_alter_agreement_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%agreement}}', 'print_id', $this->integer()->after('signature_id'));
        $this->addColumn('{{%agreement}}', 'chief_signature_id', $this->integer()->after('print_id'));
        $this->addColumn('{{%agreement}}', 'chief_accountant_signature_id', $this->integer()->after('chief_signature_id'));

        $this->addForeignKey(
            'FK_agreement_company_details_print',
            '{{%agreement}}',
            'print_id',
            '{{%company_details_file}}',
            'id'
        );

        $this->addForeignKey(
            'FK_agreement_company_details_chief_signature',
            '{{%agreement}}',
            'chief_signature_id',
            '{{%company_details_file}}',
            'id'
        );

        $this->addForeignKey(
            'FK_agreement_company_details_chief_accountant_signature',
            '{{%agreement}}',
            'chief_accountant_signature_id',
            '{{%company_details_file}}',
            'id'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_agreement_company_details_print', '{{%agreement}}');
        $this->dropForeignKey('FK_agreement_company_details_chief_signature', '{{%agreement}}');
        $this->dropForeignKey('FK_agreement_company_details_chief_accountant_signature', '{{%agreement}}' );

        $this->dropColumn('{{%agreement}}', 'print_id');
        $this->dropColumn('{{%agreement}}', 'chief_signature_id');
        $this->dropColumn('{{%agreement}}', 'chief_accountant_signature_id');
    }
}
