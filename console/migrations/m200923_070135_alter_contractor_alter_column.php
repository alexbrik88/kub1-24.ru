<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200923_070135_alter_contractor_alter_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%contractor}}', 'decoding_legal_form', $this->string());
    }

    public function safeDown()
    {
        $this->alterColumn('{{%contractor}}', 'decoding_legal_form', $this->string(20));
    }
}
