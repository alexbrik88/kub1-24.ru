<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180903_110233_add_question_to_phone_feedback extends Migration
{
    public $tableName = 'phone_feedback';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'question', $this->text()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'question');
    }
}


