<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201126_100039_rename_table_finance_model_month extends Migration
{
    public function safeUp()
    {
        $this->renameTable('finance_model_month', 'finance_model_revenue');
    }

    public function safeDown()
    {
        $this->renameTable('finance_model_revenue', 'finance_model_month');
    }
}
