<?php

use console\components\db\Migration;
use yii\db\Schema;
use common\models\employee\Employee;

class m170915_100437_add_chat_photo_to_employee extends Migration
{
    public $tableName = 'employee';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'chat_photo', $this->string()->defaultValue(null));
        foreach (Employee::find()->andWhere(['is_registration_completed' => true])->asArray()->all() as $employee) {
            if ($employee['sex'] == Employee::MALE) {
                $this->update($this->tableName,
                    ['chat_photo' => Employee::$chatPhotos[Employee::MALE][array_rand(Employee::$chatPhotos[Employee::MALE])]],
                    ['id' => $employee['id']]);
            } elseif ($employee['sex'] == Employee::FEMALE) {
                $this->update($this->tableName,
                    ['chat_photo' => Employee::$chatPhotos[Employee::FEMALE][array_rand(Employee::$chatPhotos[Employee::FEMALE])]],
                    ['id' => $employee['id']]);
            }
        }
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'chat_photo');
    }
}


