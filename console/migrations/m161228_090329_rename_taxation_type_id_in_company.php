<?php

use console\components\db\Migration;
use yii\db\Schema;

class m161228_090329_rename_taxation_type_id_in_company extends Migration
{
    public $tableName = 'company';

    public function safeUp()
    {
        $this->dropForeignKey('taxation_type_rel', $this->tableName);
        $this->renameColumn($this->tableName, 'taxation_type_id', 'old_taxation_type_id');
    }

    public function safeDown()
    {
        $this->renameColumn($this->tableName, 'old_taxation_type_id', 'taxation_type_id');
        $this->addForeignKey('taxation_type_rel', $this->tableName, 'taxation_type_id', 'taxation_type', 'id', 'RESTRICT', 'CASCADE');
    }
}


