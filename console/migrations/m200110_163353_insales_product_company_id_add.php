<?php

use console\components\db\Migration;

class m200110_163353_insales_product_company_id_add extends Migration
{
    const TABLE = '{{%insales_product}}';

    public function up()
    {
        $this->addColumn(self::TABLE, 'company_id', $this->integer(11)->notNull()->after('id')->comment('Идентификатор компании КУБ'));
    }

    public function down()
    {
        $this->dropColumn(self::TABLE, 'company_id');
    }
}
