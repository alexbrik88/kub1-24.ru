<?php

use console\components\db\Migration;
use yii\db\Schema;

class m171003_180705_add_created_at_to_service_more_stock extends Migration
{
    public $tableName = 'service_more_stock';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'created_at', $this->integer()->notNull());
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'created_at');
    }
}


