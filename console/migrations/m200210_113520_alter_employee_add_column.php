<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200210_113520_alter_employee_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%employee}}', '_page_size', $this->text());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%employee}}', '_page_size', $this->text());
    }
}
