<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170126_072317_insert_companyType extends Migration
{
    public function safeUp()
    {
        $this->batchInsert('{{%company_type}}', ['id', 'name_short', 'name_full', 'in_company', 'in_contractor'], [
            [18, 'МОО', 'Международная Общественная Организация', 0, 1],
            [19, 'МКУ', 'Муниципальное Казённое Учреждение', 0, 1],
        ]);
    }
    
    public function safeDown()
    {
        $this->delete('{{%company_type}}', ['id' => [18, 19]]);
    }
}


