<?php

use console\components\db\Migration;
use yii\db\Schema;
use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;

class m171122_095901_update_payment_partial_amount extends Migration
{
    public $tableName = 'invoice';

    public function safeUp()
    {
        /* @var $invoice Invoice */
        foreach (Invoice::find()->byDeleted()->andWhere(['in', Invoice::tableName() . '.invoice_status_id', [InvoiceStatus::STATUS_PAYED_PARTIAL, InvoiceStatus::STATUS_OVERDUE]])->all() as $invoice) {
            $paymentAmount = $invoice->getPaidAmount();
            if ($invoice->total_amount_with_nds > $paymentAmount && $paymentAmount > 0) {
                $this->update($this->tableName, ['remaining_amount' => $invoice->total_amount_with_nds - $paymentAmount], ['id' => $invoice->id]);
                $this->update($this->tableName, ['payment_partial_amount' => $paymentAmount], ['id' => $invoice->id]);
            } else {
                $this->update($this->tableName, ['payment_partial_amount' => null], ['id' => $invoice->id]);
                $this->update($this->tableName, ['remaining_amount' => null], ['id' => $invoice->id]);
            }
        }
    }

    public function safeDown()
    {

    }
}


