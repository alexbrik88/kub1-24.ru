<?php

use yii\db\Schema;
use yii\db\Migration;

class m150414_081934_alter_Employee_add_authKey extends Migration
{
    public function up()
    {
        $this->addColumn('employee', 'auth_key', Schema::TYPE_STRING . '(32) NOT NULL AFTER `password`');
    }

    public function down()
    {
        $this->dropColumn('employee', 'auth_key');
    }
}
