<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200401_103005_alter_dossier_log_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%dossier_log}}', 'inn', $this->string(12)->after('contractor_id'));

        $this->execute('
            UPDATE {{%dossier_log}}
            LEFT JOIN {{%contractor}} ON {{%contractor}}.[[id]] = {{%dossier_log}}.[[contractor_id]]
            LEFT JOIN {{%company}} ON {{%company}}.[[id]] = {{%dossier_log}}.[[company_id]]
            SET {{%dossier_log}}.[[inn]] = IFNULL({{%contractor}}.[[ITN]], {{%company}}.[[inn]])
        ');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%dossier_log}}', 'inn');
    }
}
