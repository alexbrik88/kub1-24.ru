<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211010_053230_alter_marketing_report_group_item extends Migration
{
    public function safeUp()
    {
        $this->addColumn('marketing_report_group_item', 'source_type', $this->tinyInteger()->notNull()->after('group_id'));
    }

    public function safeDown()
    {
        $this->dropColumn('marketing_report_group_item', 'source_type');
    }
}
