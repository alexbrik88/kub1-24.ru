<?php

use yii\db\Migration;
use yii\db\Schema;

class m160808_150008_add_type_specific_fields_to_export_files extends Migration
{
    public function up()
    {
        $this->addColumn('{{%export_files}}', 'io_type_specific', $this->boolean()->notNull()->defaultValue(0));
        $this->addColumn('{{%export_files}}', 'io_type_specific_items', $this->string(255)->notNull()->defaultValue(''));
    }

    public function down()
    {
        $this->dropColumn('{{%export_files}}', 'io_type_specific');
        $this->dropColumn('{{%export_files}}', 'io_type_specific_items');
    }
}
