<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%google_analytics_profile}}`.
 */
class m200616_112827_create_google_analytics_profile_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%google_analytics_profile}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'access_token' => $this->string()->defaultValue(null),
            'expires_at' => $this->string()->defaultValue(null),
        ]);

        $this->addForeignKey('FK_google_analytics_profile_to_company', 'google_analytics_profile', 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK_google_analytics_profile_to_company', 'google_analytics_profile');
        $this->dropTable('{{%google_analytics_profile}}');
    }
}
