<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190809_062500_create_tables_scan_recognize extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        /// TASK
        $this->createTable('scan_recognize', [
            'id' => $this->primaryKey(),
            'scan_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(),
            'finish_at' => $this->integer(),
            'state' => $this->string(16),
            'is_recognized' => $this->integer(1)->notNull()->defaultValue(0),
            'balance_cost' => $this->integer(),
            'space_guid' => $this->string(36),
            'guid' => $this->string(36),
        ], $tableOptions);

        $this->addForeignKey('FK_scan_recognize_to_scan_document', 'scan_recognize', 'scan_id', 'scan_document', 'id', 'CASCADE');

        /// DOCUMENT
        $this->createTable('scan_recognize_document', [
            'id' => $this->primaryKey(),
            'scan_recognize_id' => $this->integer()->notNull(),
            'document_number' => $this->string(10),
            'document_date' => $this->string(10),
            'guid' => $this->string(36)
        ], $tableOptions);

        $this->addForeignKey('FK_scan_recognize_document_to_scan_recognize', 'scan_recognize_document', 'scan_recognize_id', 'scan_recognize', 'id', 'CASCADE');

        /// PRODUCT
        $this->createTable('scan_recognize_product', [
            'id' => $this->primaryKey(),
            'scan_recognize_document_id' => $this->integer()->notNull(),
            'name' => $this->string(),
            'code' => $this->string(64),
            'unit_name' => $this->string(16),
            'unit_code' => $this->string(16),
            'quantity' => $this->string(16),
            'price' => $this->string(24),
            'price_without_vat' => $this->string(24),
            'vat_rate' => $this->string(16),
            'vat' => $this->string(24),
            'excize' => $this->string(24),
            'amount' => $this->string(24),
            'guid' => $this->string(36)
        ], $tableOptions);

        $this->addForeignKey('FK_scan_recognize_product_to_scan_recognize_document', 'scan_recognize_product', 'scan_recognize_document_id', 'scan_recognize_document', 'id', 'CASCADE');

        /// CONTRACTOR
        $this->createTable('scan_recognize_contractor', [
            'id' => $this->primaryKey(),
            'type' => $this->integer(1)->notNull(),
            'scan_recognize_document_id' => $this->integer()->notNull(),
            'name' => $this->string(),
            'short_name' => $this->string(),
            'inn' => $this->string(12),
            'kpp' => $this->string(9),
            'bank_bic' => $this->string(9),
            'bank_rs' => $this->string(20),
            'guid' => $this->string(36)
        ], $tableOptions);

        $this->addForeignKey('FK_scan_recognize_product_to_scan_recognize_contractor', 'scan_recognize_contractor', 'scan_recognize_document_id', 'scan_recognize_document', 'id', 'CASCADE');

        // PARAMS

        $this->createTable('scan_recognize_params', [
            'id' => $this->primaryKey(),
            'param_name' => $this->string()->notNull(),
            'param_value' => $this->text()->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_scan_recognize_product_to_scan_recognize_contractor', 'scan_recognize_contractor');
        $this->dropForeignKey('FK_scan_recognize_product_to_scan_recognize_document', 'scan_recognize_product');
        $this->dropForeignKey('FK_scan_recognize_document_to_scan_recognize', 'scan_recognize_document');
        $this->dropForeignKey('FK_scan_recognize_to_scan_document', 'scan_recognize');
        $this->dropTable('scan_recognize_params');
        $this->dropTable('scan_recognize_contractor');
        $this->dropTable('scan_recognize_product');
        $this->dropTable('scan_recognize_document');
        $this->dropTable('scan_recognize');
    }
}