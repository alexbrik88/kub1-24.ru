<?php

use common\models\file\File;
use common\models\product\Product;
use console\components\db\Migration;
use yii\db\Schema;
use yii\helpers\FileHelper;

class m180215_110656_delete_file extends Migration
{
    public function safeUp()
    {
        $fileArray = File::find()->where([
            'owner_model' => Product::class,
        ])->all();

        if ($fileArray) {
            foreach ($fileArray as $file) {
                $file->delete();
            }
        }
        $path = Yii::getAlias(Yii::$app->params['uploadDir']) . DIRECTORY_SEPARATOR . Product::$uploadFolder;
        $result = scandir($path);
        $except = [
            '.',
            '..',
            '.gitignore',
        ];
        if ($result) {
            foreach ($result as $item) {
                if (!in_array($item, $except)) {
                    $dir = $path . DIRECTORY_SEPARATOR . $item;
                    FileHelper::removeDirectory($dir);
                }
            }
        }
    }

    public function safeDown()
    {

    }
}
