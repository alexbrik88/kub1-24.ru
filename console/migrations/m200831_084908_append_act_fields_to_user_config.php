<?php

use console\components\db\Migration;

class m200831_084908_append_act_fields_to_user_config extends Migration
{
    public $tableName = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'act_order_sum_without_nds', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'act_order_sum_nds', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'act_contractor_inn', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn($this->tableName, 'act_payment_date', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'act_responsible', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn($this->tableName, 'act_source', $this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'act_order_sum_without_nds');
        $this->dropColumn($this->tableName, 'act_order_sum_nds');
        $this->dropColumn($this->tableName, 'act_contractor_inn');
        $this->dropColumn($this->tableName, 'act_payment_date');
        $this->dropColumn($this->tableName, 'act_responsible');
        $this->dropColumn($this->tableName, 'act_source');
    }

}