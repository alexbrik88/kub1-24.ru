<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191208_155200_alter_scan_recognize_product_change_column extends Migration
{
    public $table = 'scan_recognize_product';

    public function safeUp()
    {
        $this->alterColumn($this->table, 'price', $this->string(16));
        $this->alterColumn($this->table, 'price_without_vat', $this->string(16));
        $this->alterColumn($this->table, 'vat', $this->string(16));
        $this->alterColumn($this->table, 'excize', $this->string(16));
        $this->alterColumn($this->table, 'amount', $this->string(16));
    }

    public function safeDown()
    {
        $this->alterColumn($this->table, 'price', $this->string(24));
        $this->alterColumn($this->table, 'price_without_vat', $this->string(24));
        $this->alterColumn($this->table, 'vat', $this->string(24));
        $this->alterColumn($this->table, 'excize', $this->string(24));
        $this->alterColumn($this->table, 'amount', $this->string(24));
    }
}
