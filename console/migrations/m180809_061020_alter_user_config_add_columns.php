<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180809_061020_alter_user_config_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn(
            'user_config',
            'invoice_upd',
            $this->boolean()->notNull()->defaultValue(true)->after('invoice_invfacture')
        );
        $this->addColumn(
            'user_config',
            'contr_inv_upd',
            $this->boolean()->notNull()->defaultValue(true)->after('contr_inv_invfacture')
        );
        $this->addCommentOnColumn('user_config', 'invoice_upd', 'Показ колонки "Упд" в списке счетов');
        $this->addCommentOnColumn('user_config', 'contr_inv_upd', 'Показ колонки "Упд" в списке счетов контрагента');
    }

    public function safeDown()
    {
        $this->dropColumn('user_config', 'invoice_upd');
        $this->dropColumn('user_config', 'contr_inv_upd');
    }
}
