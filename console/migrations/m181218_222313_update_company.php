<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181218_222313_update_company extends Migration
{
    public function safeUp()
    {
        $this->execute('
            UPDATE {{company}}
            LEFT JOIN {{company_taxation_type}} ON {{company_taxation_type}}.[[company_id]] = {{company}}.[[id]]
            SET {{company}}.[[nds]] = 1
            WHERE {{company_taxation_type}}.[[osno]] = 1 AND {{company}}.[[nds]] IS NULL
        ');
        $this->execute('
            UPDATE {{company}}
            LEFT JOIN {{company_taxation_type}} ON {{company_taxation_type}}.[[company_id]] = {{company}}.[[id]]
            SET {{company}}.[[nds_view_type_id]] = CASE
                    WHEN {{company}}.[[nds]] IS NULL THEN 0
                    WHEN {{company}}.[[nds]] = 1 THEN 0
                    WHEN {{company}}.[[nds]] = 2 THEN 1
                END
            WHERE {{company_taxation_type}}.[[osno]] = 1
        ');
    }

    public function safeDown()
    {
        //
    }
}
