<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170721_051520_alter_serviceSubscribe_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%service_subscribe}}', 'payment_id', $this->integer() . ' AFTER [[tariff_id]]');
        $this->addColumn('{{%service_subscribe}}', 'payment_order_id', $this->integer() . ' AFTER [[payment_id]]');

        $this->addForeignKey('FK_serviceSubscribe_servicePayment',
            '{{%service_subscribe}}', 'payment_id', '{{%service_payment}}', 'id', 'SET NULL');
        $this->addForeignKey('FK_serviceSubscribe_servicePaymentOrder',
            '{{%service_subscribe}}', 'payment_order_id', '{{%service_payment_order}}', 'id', 'SET NULL');

        $this->execute("
            UPDATE
                {{%service_subscribe}}
            LEFT JOIN
                {{%service_payment}} ON {{%service_payment}}.[[subscribe_id]] = {{%service_subscribe}}.[[id]]
            LEFT JOIN
                {{%service_payment_order}} ON {{%service_payment_order}}.[[payment_id]] = {{%service_payment}}.[[id]]
            SET
                {{%service_subscribe}}.[[payment_id]] = {{%service_payment}}.[[id]],
                {{%service_subscribe}}.[[payment_order_id]] = {{%service_payment_order}}.[[id]]
            WHERE
                {{%service_payment}}.[[id]] IS NOT NULL
        ");
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('FK_serviceSubscribe_servicePayment', '{{%service_subscribe}}');
        $this->dropForeignKey('FK_serviceSubscribe_servicePaymentOrder', '{{%service_subscribe}}');

        $this->dropColumn('{{%service_subscribe}}', 'payment_id');
        $this->dropColumn('{{%service_subscribe}}', 'payment_order_id');
    }
}
