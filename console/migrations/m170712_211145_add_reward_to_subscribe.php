<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170712_211145_add_reward_to_subscribe extends Migration
{
    public $tableName = 'service_subscribe';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'reward', $this->integer()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'reward');
    }
}


