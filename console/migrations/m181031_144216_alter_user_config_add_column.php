<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181031_144216_alter_user_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn(
            'user_config',
            'invoice_waybill',
            $this->boolean()->notNull()->defaultValue(true)->after('invoice_paclist')
        );
        $this->addColumn(
            'user_config',
            'contr_inv_waybill',
            $this->boolean()->notNull()->defaultValue(true)->after('contr_inv_paclist')
        );
        $this->addCommentOnColumn('user_config', 'invoice_waybill', 'Показ колонки "ТТН" в списке счетов');
        $this->addCommentOnColumn('user_config', 'contr_inv_waybill', 'Показ колонки "ТТН" в списке счетов контрагента');
    }

    public function safeDown()
    {
        $this->dropColumn('user_config', 'invoice_waybill');
        $this->dropColumn('user_config', 'contr_inv_waybill');
    }
}


