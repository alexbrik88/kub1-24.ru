<?php

use yii\db\Migration;

/**
 * Handles the creation of table `upd_status`.
 */
class m170622_082030_create_upd_status_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('upd_status', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);

        $this->batchInsert('upd_status', ['name'], [
            ['Создан'],
            ['Распечатан'],
            ['Передан'],
            ['Подписан']
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('upd_status');
    }
}
