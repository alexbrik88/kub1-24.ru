<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191220_105859_insert_service_subscribe_tariff_group extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%service_subscribe_tariff_group}}', [
            'id' => 7,
            'name' => 'Налоги ИП на УСН 6%',
            'is_base' => 0,
            'has_base' => 0,
            'is_active' => 1,
            'description' => 'Автоматический расчёт налогов и подготовка платёжек для их уплаты',
            'hint' => '(Бухгалтерия ИП)',
        ]);

        $this->update('{{%service_subscribe_tariff_group}}', [
            'name' => 'Налоги + Декларация ИП на УСН 6%',
        ], [
            'id' => 4,
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%service_subscribe_tariff_group}}', [
            'id' => 7,
        ]);

        $this->update('{{%service_subscribe_tariff_group}}', [
            'name' => 'Налоги и Декларация для ИП на УСН 6%',
        ], [
            'id' => 4,
        ]);
    }
}
