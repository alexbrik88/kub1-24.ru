<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190628_051901_contractor_agent_report extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%contractor_agent_report}}', [
            'id' => Schema::TYPE_PK,

            'uid' => Schema::TYPE_STRING . '(5) DEFAULT NULL',
            'created_at' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'document_author_id' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            
            'agent_id' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'month' => Schema::TYPE_INTEGER. '(2) NOT NULL',
            'year' => Schema::TYPE_INTEGER. '(4) NOT NULL',
        ]);

        $this->createIndex('uid_idx', '{{%contractor_agent_report}}', 'uid');
        $this->createIndex('agent_report_idx', '{{%contractor_agent_report}}', ['agent_id', 'month', 'year']);

        $this->addForeignKey('FK_contractor_agent_report_to_agent', '{{%contractor_agent_report}}', 'agent_id', 'contractor', 'id', 'CASCADE');
        $this->addForeignKey('FK_contractor_agent_report_to_document_author', '{{%contractor_agent_report}}', 'document_author_id', 'employee', 'id');

    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_contractor_agent_report_to_document_author', '{{%contractor_agent_report}}');
        $this->dropForeignKey('FK_contractor_agent_report_to_agent', '{{%contractor_agent_report}}');
        $this->dropTable('{{%contractor_agent_report}}');
    }
}