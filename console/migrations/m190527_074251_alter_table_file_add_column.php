<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190527_074251_alter_table_file_add_column extends Migration
{
    public $tFile = 'file';
    public $tDir  = 'file_dir';

    public function safeUp()
    {
        $this->addColumn($this->tFile, 'directory_id', Schema::TYPE_INTEGER . ' DEFAULT null');
        $this->addForeignKey('fk_directory_id', $this->tFile, 'directory_id', $this->tDir, 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_directory_id', $this->tFile);
        $this->dropColumn($this->tFile, 'directory_id');
    }
}
