<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200415_210148_add_yandex_direct_table_view_config extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_config', 'table_view_yandex_direct_by_campaign', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn('user_config', 'table_view_yandex_direct_by_ad_group', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn('user_config', 'table_view_yandex_direct_by_ad', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
    }

    public function safeDown()
    {
        $this->dropColumn('user_config', 'table_view_yandex_direct_by_campaign');
        $this->dropColumn('user_config', 'table_view_yandex_direct_by_ad_group');
        $this->dropColumn('user_config', 'table_view_yandex_direct_by_ad');
    }
}
