<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210822_151400_alter_config_add_column extends Migration
{
    public $table = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->table, 'report_pc_project', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table, 'report_pc_sale_point', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table, 'report_pc_industry', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'report_pc_project');
        $this->dropColumn($this->table, 'report_pc_sale_point');
        $this->dropColumn($this->table, 'report_pc_industry');
    }
}
