<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210723_052012_change_finance_model_2_tables extends Migration
{
    const PRIME_COST_ITEM = 'finance_plan_prime_cost_item';
    const PRIME_COST_ITEM_AUTOFILL = 'finance_plan_prime_cost_item_autofill';

    public function safeUp()
    {
        /*
         * Drop autofill prime cost item
         */
        $this->dropForeignKey('FK_'.self::PRIME_COST_ITEM_AUTOFILL.'_to_model', self::PRIME_COST_ITEM_AUTOFILL);
        $this->dropForeignKey('FK_'.self::PRIME_COST_ITEM_AUTOFILL.'_to_item', self::PRIME_COST_ITEM_AUTOFILL);
        $this->dropTable(self::PRIME_COST_ITEM_AUTOFILL);

        /*
         * Move calc columns into prime cost
         */
        $this->addColumn(self::PRIME_COST_ITEM, 'action', $this->tinyInteger()->unsigned()->notNull()->defaultValue(1)->after('prime_cost_item_id'));
        $this->addColumn(self::PRIME_COST_ITEM, 'relation_type', $this->tinyInteger()->unsigned()->notNull()->defaultValue(1)->after('prime_cost_item_id'));
    }

    public function safeDown()
    {
        /*
         * Create autofill prime cost item
         */
        $this->createTable(self::PRIME_COST_ITEM_AUTOFILL, [
            'model_id' => $this->integer(11)->notNull(),
            'row_id' => $this->integer()->notNull(),
            'relation_type' => $this->tinyInteger()->unsigned()->notNull(),
            'action' => $this->tinyInteger()->unsigned()->notNull(),
        ]);

        $this->addPrimaryKey('PK_'.self::PRIME_COST_ITEM_AUTOFILL, self::PRIME_COST_ITEM_AUTOFILL, ['model_id', 'row_id']);
        $this->addForeignKey('FK_'.self::PRIME_COST_ITEM_AUTOFILL.'_to_model', self::PRIME_COST_ITEM_AUTOFILL, 'model_id', self::MODEL, 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_'.self::PRIME_COST_ITEM_AUTOFILL.'_to_item', self::PRIME_COST_ITEM_AUTOFILL, 'row_id', self::PRIME_COST_ITEM, 'id', 'CASCADE', 'CASCADE');

        /*
         * Drop calc columns in prime cost
         */
        $this->dropColumn(self::PRIME_COST_ITEM, 'action');
        $this->dropColumn(self::PRIME_COST_ITEM, 'relation_type');
    }
}
