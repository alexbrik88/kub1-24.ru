<?php

use yii\db\Schema;
use yii\db\Migration;

class m150417_063823_drop_incoming_account extends Migration
{
    public function up()
    {
        $this->dropTable('incoming_account');
    }

    public function down()
    {
    }

}
