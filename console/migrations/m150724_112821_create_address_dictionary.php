<?php

use yii\db\Schema;
use yii\db\Migration;

class m150724_112821_create_address_dictionary extends Migration
{
    public function safeUp()
    {
        $this->createTable('address_dictionary', [
            'ACTSTATUS' => 'int(11) NULL',
            'AOGUID' => 'varchar(36) NULL',
            'AOID' => 'varchar(36) NULL',
            'AOLEVEL' => 'int(11) NULL',
            'AREACODE' => 'varchar(3) NULL',
            'AUTOCODE' => 'varchar(1) NULL',
            'CENTSTATUS' => 'int(11) NULL',
            'CITYCODE' => 'varchar(3) NULL',
            'CODE' => 'varchar(17) NULL',
            'CURRSTATUS' => 'int(11) NULL',
            'ENDDATE' => 'date NULL',
            'FORMALNAME' => 'varchar(120) NULL',
            'FULLNAME' => 'varchar(131) NOT NULL COMMENT "FORMALNAME + \'\' + SHORTNAME"',
            'IFNSFL' => 'varchar(4) NULL',
            'IFNSUL' => 'varchar(4) NULL',
            'NEXTID' => 'varchar(36) NULL',
            'OFFNAME' => 'varchar(120) NULL',
            'OKATO' => 'varchar(11) NULL',
            'OKTMO' => 'varchar(11) NULL',
            'OPERSTATUS' => 'int(11) NULL',
            'PARENTGUID' => 'varchar(36) NULL',
            'PLACECODE' => 'varchar(3) NULL',
            'PLAINCODE' => 'varchar(15) NULL',
            'POSTALCODE' => 'varchar(6) NULL',
            'PREVID' => 'varchar(36) NULL',
            'REGIONCODE' => 'varchar(2) NULL',
            'SHORTNAME' => 'varchar(10) NULL',
            'STARTDATE' => 'date NULL',
            'STREETCODE' => 'varchar(4) NULL',
            'TERRIFNSFL' => 'varchar(4) NULL',
            'TERRIFNSUL' => 'varchar(4) NULL',
            'UPDATEDATE' => 'date NULL',
            'CTARCODE' => 'varchar(3) NULL',
            'EXTRCODE' => 'varchar(4) NULL',
            'SEXTCODE' => 'varchar(3) NULL',
            'LIVESTATUS' => 'int(11) NULL',
            'NORMDOC' => 'varchar(36) NULL',
        ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=MyISAM');

        $this->createIndex('aoguid_idx', 'address_dictionary', ['AOGUID']);
        $this->createIndex('city_idx', 'address_dictionary', ['AOLEVEL', 'ACTSTATUS']);
        $this->createIndex('idx1', 'address_dictionary', [
            'AOLEVEL', 'ACTSTATUS', 'FULLNAME',
        ]);
        $this->createIndex('idx2', 'address_dictionary', [
            'AOGUID', 'AOLEVEL', 'ACTSTATUS', 'FULLNAME',
        ]);
    }
    
    public function safeDown()
    {
        $this->dropTable('address_dictionary');
    }
}
