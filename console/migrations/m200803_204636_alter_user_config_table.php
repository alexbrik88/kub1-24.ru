<?php

use console\components\db\Migration;

class m200803_204636_alter_user_config_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%user_config}}';

    /**
     * @inheritDoc
     */
    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'crm_client', $this->boolean()->defaultValue(false));
        $this->addColumn(self::TABLE_NAME, 'crm_client_activity', $this->boolean()->defaultValue(false));
        $this->addColumn(self::TABLE_NAME, 'crm_client_check', $this->boolean()->defaultValue(false));
        $this->addColumn(self::TABLE_NAME, 'crm_client_reason', $this->boolean()->defaultValue(false));
        $this->addColumn(self::TABLE_NAME, 'crm_client_campaign', $this->boolean()->defaultValue(false));
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'crm_client');
        $this->dropColumn(self::TABLE_NAME, 'crm_client_activity');
        $this->dropColumn(self::TABLE_NAME, 'crm_client_check');
        $this->dropColumn(self::TABLE_NAME, 'crm_client_reason');
        $this->dropColumn(self::TABLE_NAME, 'crm_client_campaign');
    }
}
