<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180425_153040_add_new_columns_to_invoice extends Migration
{
    public $tInvoice = 'invoice';
    public $tOrder = 'order_document';

    public function safeUp()
    {
        $this->addColumn($this->tInvoice, 'from_store_cabinet', $this->boolean()->defaultValue(false));
        $this->addColumn($this->tInvoice, 'from_out_invoice', $this->boolean()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tInvoice, 'from_store_cabinet');
        $this->dropColumn($this->tInvoice, 'from_out_invoice');
        $this->dropColumn($this->tOrder, 'from_store_cabinet');
    }
}


