<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181127_095951_create_loading_and_unloading_address extends Migration
{
    public $tLoadingAndUnloadingAddress = 'loading_and_unloading_address';
    public $tLoadingAndUnloadingAddressContactPerson = 'loading_and_unloading_address_contact_person';

    public function safeUp()
    {
        $this->createTable($this->tLoadingAndUnloadingAddress, [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'author_id' => $this->integer()->notNull(),
            'number' => $this->string()->notNull(),
            'date' => $this->date()->notNull(),
            'address_type' => $this->boolean()->defaultValue(0),
            'contractor_id' => $this->integer()->notNull(),
            'city' => $this->string()->notNull(),
            'address' => $this->string()->notNull(),
            'consignor' => $this->string()->defaultValue(null),
            'consignee' => $this->string()->defaultValue(null),
            'status' => $this->integer()->notNull(),
        ]);

        $this->addCommentOnColumn($this->tLoadingAndUnloadingAddress, 'address_type', '0 - Погрузка, 1 - Разгрузка');
        $this->addCommentOnColumn($this->tLoadingAndUnloadingAddress, 'status', '1 - В работе, 2 - Архив');

        $this->addForeignKey($this->tLoadingAndUnloadingAddress . '_company_id', $this->tLoadingAndUnloadingAddress, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tLoadingAndUnloadingAddress . '_author_id', $this->tLoadingAndUnloadingAddress, 'author_id', 'employee', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tLoadingAndUnloadingAddress . '_contractor_id', $this->tLoadingAndUnloadingAddress, 'contractor_id', 'contractor', 'id', 'RESTRICT', 'CASCADE');

        $this->createTable($this->tLoadingAndUnloadingAddressContactPerson, [
            'id' => $this->primaryKey(),
            'address_id' => $this->integer()->notNull(),
            'contact_person' => $this->string()->notNull(),
            'phone_type_id' => $this->integer()->notNull(),
            'phone' => $this->string()->notNull(),
            'is_main' => $this->boolean()->defaultValue(false),
        ]);

        $this->addForeignKey($this->tLoadingAndUnloadingAddressContactPerson . '_address_id', $this->tLoadingAndUnloadingAddressContactPerson, 'address_id', $this->tLoadingAndUnloadingAddress, 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tLoadingAndUnloadingAddressContactPerson . '_phone_type_id', $this->tLoadingAndUnloadingAddressContactPerson, 'phone_type_id', 'phone_type', 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey($this->tLoadingAndUnloadingAddressContactPerson . '_address_id', $this->tLoadingAndUnloadingAddressContactPerson);
        $this->dropForeignKey($this->tLoadingAndUnloadingAddressContactPerson . '_phone_type_id', $this->tLoadingAndUnloadingAddressContactPerson);

        $this->dropForeignKey($this->tLoadingAndUnloadingAddress . '_contractor_id', $this->tLoadingAndUnloadingAddress);
        $this->dropForeignKey($this->tLoadingAndUnloadingAddress . '_company_id', $this->tLoadingAndUnloadingAddress);
        $this->dropForeignKey($this->tLoadingAndUnloadingAddress . '_author_id', $this->tLoadingAndUnloadingAddress);

        $this->dropTable($this->tLoadingAndUnloadingAddressContactPerson);
        $this->dropTable($this->tLoadingAndUnloadingAddress);
    }
}


