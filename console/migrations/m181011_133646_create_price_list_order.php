<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181011_133646_create_price_list_order extends Migration
{
    public $tableName = 'price_list_order';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'price_list_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'article' => $this->string()->defaultValue(null),
            'product_group_id' => $this->integer()->defaultValue(null),
            'quantity' => $this->integer()->defaultValue(null),
            'product_unit_id' => $this->integer()->defaultValue(null),
            'description' => $this->text()->defaultValue(null),
            'price_for_sell' => $this->string()->notNull(),
        ]);

        $this->addForeignKey($this->tableName . '_price_list_id', $this->tableName, 'price_list_id', 'price_list', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tableName . '_product_id', $this->tableName, 'product_id', 'product', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tableName . '_product_group_id', $this->tableName, 'product_group_id', 'product_group', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tableName . '_product_unit_id', $this->tableName, 'product_unit_id', 'product_unit', 'id', 'RESTRICT', 'CASCADE');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_product_id', $this->tableName);
        $this->dropForeignKey($this->tableName . '_price_list_id', $this->tableName);
        $this->dropForeignKey($this->tableName . '_product_group_id', $this->tableName);
        $this->dropForeignKey($this->tableName . '_product_unit_id', $this->tableName);

        $this->dropTable($this->tableName);
    }
}


