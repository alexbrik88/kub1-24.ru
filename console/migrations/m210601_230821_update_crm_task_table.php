<?php

use yii\db\Migration;

class m210601_230821_update_crm_task_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%crm_task}}';

    /**
     * @inheritDoc
     */
    public function safeUp(): void
    {
        $this->delete(self::TABLE_NAME, ['contractor_id' => null]);
    }

    /**
     * @inheritDoc
     */
    public function safeDown(): bool
    {
        return false;
    }
}
