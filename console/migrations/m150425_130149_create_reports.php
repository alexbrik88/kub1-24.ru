<?php

use yii\db\Schema;
use yii\db\Migration;

class m150425_130149_create_reports extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('report', [
            'id' => Schema::TYPE_PK,
            'company_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Компания"',
            'creation_date' => Schema::TYPE_DATETIME . ' COMMENT "Дата создания"',
            'title' => Schema::TYPE_STRING . ' COMMENT "Название"',
            'description' => Schema::TYPE_TEXT . ' COMMENT "Описание"',
        ], $tableOptions);

        $this->addForeignKey('report_to_company', 'report', 'company_id', 'company', 'id');

        $this->createTable('report_file', [
            'id' => Schema::TYPE_PK,
            'file_name' => Schema::TYPE_STRING,
            'report_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ], $tableOptions);

        $this->addForeignKey('report_file_to_report', 'report_file', 'report_id', 'report', 'id');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('report_file_to_report', 'report_file');
        $this->dropTable('report_file');
        $this->dropForeignKey('report_to_company', 'report');
        $this->dropTable('report');
    }
}
