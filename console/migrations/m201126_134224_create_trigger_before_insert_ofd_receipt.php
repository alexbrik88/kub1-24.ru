<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201126_134224_create_trigger_before_insert_ofd_receipt extends Migration
{
    public function safeUp()
    {
        $this->execute('DROP TRIGGER IF EXISTS `TR__ofd_receipt__before_inser`');

        $this->execute('
            CREATE TRIGGER `TR__ofd_receipt__before_inser`
            BEFORE INSERT ON `ofd_receipt`
            FOR EACH ROW
            BEGIN
                SET NEW.`id` = (SELECT IFNULL(MAX(`id`), 0) + 1 FROM `ofd_receipt`);
            END
        ');
    }

    public function safeDown()
    {
        $this->execute('DROP TRIGGER IF EXISTS `TR__ofd_receipt__before_inser`');
    }
}
