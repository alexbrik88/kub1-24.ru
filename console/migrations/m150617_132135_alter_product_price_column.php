<?php

use yii\db\Schema;
use yii\db\Migration;

class m150617_132135_alter_product_price_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%product}}', 'price_for_buy_with_nds', Schema::TYPE_INTEGER . ' COMMENT "цена покупки с НДС"');
        $this->alterColumn('{{%product}}', 'price_for_sell_with_nds', Schema::TYPE_INTEGER . ' COMMENT "цена при продаже c НДС"');
    }
    
    public function safeDown()
    {
        $this->alterColumn('{{%product}}', 'price_for_buy_with_nds', Schema::TYPE_STRING . ' COMMENT "цена покупки с НДС"');
        $this->alterColumn('{{%product}}', 'price_for_sell_with_nds', Schema::TYPE_STRING . ' COMMENT "цена при продаже c НДС"');
    }
}
