<?php

use yii\db\Schema;
use yii\db\Migration;

class m150715_130831_alter_paymentOrder_addColumns_uid extends Migration
{
    public function safeUp()
    {
        $this->addColumn('payment_order', 'uid', 'CHAR(5) DEFAULT NULL COMMENT "unique id for `out` payment orders for quest" AFTER `id` ');
        $this->createIndex('uid_idx', 'payment_order', 'uid');
        $this->update('payment_order', [
            'uid' => new \yii\db\Expression('MD5(RAND())'),
        ]);
    }
    
    public function safeDown()
    {
        $this->dropIndex('uid_idx', 'payment_order');
        $this->dropColumn('payment_order', 'uid');
    }
}
