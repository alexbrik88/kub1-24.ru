<?php

use yii\db\Migration;

class m210610_215954_alter_crm_task_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%crm_task}}';

    /**
     * @inheritDoc
     */
    public function up(): void
    {
        $this->dropForeignKey('fk_crm_task__crm_client_event_client_event_id', self::TABLE_NAME);
        $this->dropColumn(self::TABLE_NAME, 'client_event_id');
        $this->addColumn(self::TABLE_NAME, 'event_type', $this->tinyInteger()->null()->after('description'));
        $this->createIndex('ck_event_type', self::TABLE_NAME, ['event_type']);
        $this->update(self::TABLE_NAME, ['event_type' => 1]);
        $this->alterColumn(self::TABLE_NAME, 'event_type', $this->tinyInteger()->notNull());
    }

    /**
     * @inheritDoc
     */
    public function down(): bool
    {
        return false;
    }
}
