<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181014_153405_alter_agreement_new_add_uid extends Migration
{
    public function safeUp()
    {
		$this->addColumn('{{%agreement_new}}', 'uid', $this->string(5));
    }
    
    public function safeDown()
    {

    }
}
