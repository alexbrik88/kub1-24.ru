<?php

use yii\db\Schema;
use yii\db\Migration;

class m150525_062815_alter_Log extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('log', 'log_entity_type_id', Schema::TYPE_INTEGER . ' NOT NULL AFTER company_id');
        $this->createIndex('company_entity_idx', 'log', [
            'company_id', 'log_entity_type_id',
        ]);

        $this->renameColumn('log', 'log_entity_id', 'model_id');
        $this->addColumn('log', 'model_name', Schema::TYPE_STRING . '(255) NOT NULL AFTER log_entity_type_id');

        $this->dropIndex('model_idx', 'log');
        $this->createIndex('model_idx', 'log', [
            'model_name', 'model_id',
        ]);

        $this->renameColumn('log', 'data', 'attributes_new');
        $this->addColumn('log', 'attributes_old', Schema::TYPE_STRING . '(4096)');
    }
    
    public function safeDown()
    {
        $this->renameColumn('log', 'model_id', 'log_entity_id');

        $this->dropForeignKey('fk_log_to_author', 'log');
        $this->dropForeignKey('fk_log_to_company', 'log');
        $this->dropIndex('company_entity_idx', 'log');
        $this->addForeignKey('fk_log_to_author', 'log', 'author_id', 'employee', 'id');
        $this->addForeignKey('fk_log_to_company', 'log', 'company_id', 'company', 'id');

        $this->dropColumn('log', 'model_name');

        $this->dropIndex('model_idx', 'log');
        $this->createIndex('model_idx', 'log', [
            'log_entity_id',
        ]);

        $this->renameColumn('log', 'attributes_new', 'data');
        $this->dropColumn('log', 'attributes_old');
    }
}
