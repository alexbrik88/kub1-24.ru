<?php

use yii\db\Exception;
use yii\db\Migration;

class m210601_230650_update_project_task_table extends Migration
{
    /**
     * @inheritDoc
     * @throws Exception
     */
    public function safeUp(): void
    {
        $columns = implode(', ', [
            'task_id',
            'task_number',
            'task_name',
            'company_id',
            'employee_id',
            'project_id',
            'description',
            'status',
            'date_begin',
            'date_end',
            'time',
            'created_at',
            'updated_at',
        ]);

        $this->db->createCommand("
            INSERT INTO {{%project_task}} ($columns)
            SELECT $columns FROM {{%crm_task}}
            WHERE project_id IS NOT NULL
        ")->execute();
    }

    /**
     * @inheritDoc
     */
    public function safeDown(): bool
    {
        return false;
    }
}
