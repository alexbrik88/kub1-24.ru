<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200716_130330_alter_invoice_append_project_id extends Migration
{
    public $tableName = 'invoice';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'project_id', $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'project_id');
    }
}
