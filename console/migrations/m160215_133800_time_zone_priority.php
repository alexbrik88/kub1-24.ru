<?php

use console\components\db\Migration;
use yii\db\Query;

/**
 * Class m160215_133800_time_zone_priority
 */
class m160215_133800_time_zone_priority extends Migration
{
    /**
     * @var string
     */
    protected $tableName = 'time_zone';

    /**
     * @var string
     */
    protected $columnName = 'priority';

    /**
     * @inherit
     */
    public function safeUp()
    {
        $this->addColumn($this->tableName, $this->columnName, $this->integer(11)->notNull()->defaultValue(0));

        $timeZoneIds = (new Query())->select('id')->from($this->tableName)->orderBy('out_time_zone ASC')->column();

        if (is_array($timeZoneIds) && sizeof($timeZoneIds) > 0) {
            foreach ($timeZoneIds as $index => $timeZoneId) {
                $this->update($this->tableName, [$this->columnName => (string)$index], 'id = :id', [':id' => $timeZoneId]);
            }

            if ($minTimeZoneId = (new Query())->select($this->columnName)->from($this->tableName)->where(['like', 'out_time_zone', '%-12%', false])->orderBy('out_time_zone DESC')->scalar()) {
                $timeZoneIds = (new Query())->select('id')->from($this->tableName)->where($this->columnName . ' <= :priority', [':priority' => $minTimeZoneId])->orderBy('out_time_zone DESC')->column();

                if (is_array($timeZoneIds) && sizeof($timeZoneIds) > 0) {
                    foreach ($timeZoneIds as $index => $timeZoneId) {
                        $this->update($this->tableName, [$this->columnName => $index], 'id = :id', [':id' => $timeZoneId]);
                    }
                }
            };
        }
    }

    /**
     * @inherit
     */
    public function safeDown()
    {
        $this->dropColumn($this->tableName, $this->columnName);
    }
}


