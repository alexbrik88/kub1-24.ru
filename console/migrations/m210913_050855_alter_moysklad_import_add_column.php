<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210913_050855_alter_moysklad_import_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('moysklad_import', 'store_id' , $this->integer()->after('employee_id'));
        $this->addForeignKey('FK_moysklad_import_to_store', 'moysklad_import', 'store_id', 'store', 'id', 'SET NULL');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_moysklad_import_to_store', 'moysklad_import');
        $this->dropColumn('moysklad_import', 'store_id');
    }
}
