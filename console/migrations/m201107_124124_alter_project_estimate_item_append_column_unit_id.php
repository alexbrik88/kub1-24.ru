<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201107_124124_alter_project_estimate_item_append_column_unit_id extends Migration
{
    public $tableName = 'project_estimate_item';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'unit_id', $this->integer(11)->after('price'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'unit_id');
    }

}
