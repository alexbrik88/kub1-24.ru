<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211125_143251_insert_rent_agreement_invoice extends Migration
{
    public function safeUp()
    {
        $this->execute('
            INSERT INTO
                {{%rent_agreement_invoice}} (
                    [[invoice_id]],
                    [[rent_agreement_id]]
                )
            SELECT
                {{%rent_agreement}}.[[invoice_id]],
                {{%rent_agreement}}.[[id]]
            FROM
                {{%rent_agreement}}
            WHERE
                {{%rent_agreement}}.[[invoice_id]] IS NOT NULL
        ');
    }

    public function safeDown()
    {
        $this->delete('{{%rent_agreement_invoice}}');
    }
}
