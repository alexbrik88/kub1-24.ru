<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181204_210234_create_logistics_request_contract_essence extends Migration
{
    public $tableName = 'logistics_request_contract_essence';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'type' => $this->boolean()->defaultValue(false),
            'company_id' => $this->integer()->notNull(),
            'contractor_id' => $this->integer()->defaultValue(null),
            'text' => $this->text()->defaultValue(null),
            'is_checked' => $this->boolean()->defaultValue(false),
            'for_contractor' => $this->boolean()->defaultValue(false),
        ]);

        $this->addForeignKey($this->tableName . '_company_id', $this->tableName, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tableName . '_contractor_id', $this->tableName, 'contractor_id', 'contractor', 'id', 'RESTRICT', 'CASCADE');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_company_id', $this->tableName);
        $this->dropForeignKey($this->tableName . '_contractor_id', $this->tableName);

        $this->dropTable($this->tableName);
    }
}


