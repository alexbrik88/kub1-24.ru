<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170426_084940_insert_companyType extends Migration
{
    public function safeUp()
    {
        $this->batchInsert('{{%company_type}}', ['id', 'name_short', 'name_full', 'in_company', 'in_contractor'], [
            [30, 'ФГБУ', 'Федеральное Государственное Бюджетное Учреждение', false, true],
            [31, 'ГБУ', 'Государственное Бюджетное Учреждение', false, true],
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%company_type}}', ['in', 'id', [30, 31]]);
    }
}
