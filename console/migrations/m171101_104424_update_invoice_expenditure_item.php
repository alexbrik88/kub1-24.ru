<?php

use console\components\db\Migration;
use yii\db\Schema;

class m171101_104424_update_invoice_expenditure_item extends Migration
{
    public $tableName = 'invoice_expenditure_item';

    public function safeUp()
    {
        $this->update($this->tableName, ['type' => null], ['id' => 14]);
        $this->delete('cash_bank_flows', ['in', 'expenditure_item_id', [33, 34, 35, 40, 41, 42]]);
        $this->delete('cash_emoney_flows', ['in', 'expenditure_item_id', [33, 34, 35, 40, 41, 42]]);
        $this->delete('cash_order_flows', ['in', 'expenditure_item_id', [33, 34, 35, 40, 41, 42]]);

        $this->delete($this->tableName, ['type' => 1]);
        $this->delete($this->tableName, ['type' => 3]);
    }

    public function safeDown()
    {
        echo 'This migration not rolled back';
    }
}


