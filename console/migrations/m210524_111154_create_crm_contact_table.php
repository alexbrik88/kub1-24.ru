<?php

use yii\db\Migration;

class m210524_111154_create_crm_contact_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%crm_contact}}';

    /**
     * @inheritDoc
     */
    public function up(): void
    {
        $this->createTable(self::TABLE_NAME, [
            'contact_id' => $this->bigPrimaryKey()->notNull(),
            'client_id' => $this->bigInteger()->notNull(),
            'client_position_id' => $this->bigInteger()->null(),
            'contact_default' => $this->boolean()->notNull()->defaultValue(true),
            'contact_name' => $this->string()->notNull()->defaultValue(''),
            'email_address' => $this->string()->notNull()->defaultValue(''),
            'skype_account' => $this->string()->notNull()->defaultValue(''),
            'social_account' => $this->string()->notNull()->defaultValue(''),
            'primary_phone_number' => $this->string()->notNull()->defaultValue(''),
            'secondary_phone_number' => $this->string()->notNull()->defaultValue(''),
            'primary_messenger_type' => $this->bigInteger()->null(),
            'secondary_messenger_type' => $this->bigInteger()->null(),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->createIndex('ck_client_id', self::TABLE_NAME, ['client_id']);
        $this->addForeignKey(
            'fk_crm_contact__crm_client',
            self::TABLE_NAME,
            ['client_id'],
            '{{%crm_client}}',
            ['client_id'],
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex('ck_client_position_id', self::TABLE_NAME, ['client_position_id']);
        $this->addForeignKey(
            'fk_crm_contact__crm_client_position',
            self::TABLE_NAME,
            ['client_position_id'],
            '{{%crm_client_position}}',
            ['client_position_id'],
            'CASCADE',
            'SET NULL'
        );
    }

    /**
     * @inheritDoc
     */
    public function down(): void
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
