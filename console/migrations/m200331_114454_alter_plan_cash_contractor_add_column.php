<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200331_114454_alter_plan_cash_contractor_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('plan_cash_contractor', 'rule_id', $this->integer()->after('contractor_id'));
        $this->addForeignKey('FK_plan_cash_contractor_to_plan_cash_rule', 'plan_cash_contractor', 'rule_id', 'plan_cash_rule', 'id', 'SET NULL');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_plan_cash_contractor_to_plan_cash_rule', 'plan_cash_contractor');
        $this->dropColumn('plan_cash_contractor', 'rule_id');
    }
}
