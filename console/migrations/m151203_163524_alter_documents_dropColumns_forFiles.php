<?php

use yii\db\Schema;
use yii\db\Migration;

class m151203_163524_alter_documents_dropColumns_forFiles extends Migration
{
    public $tInvoice = 'invoice';
    public $tInvoiceFacture = 'invoice_facture';
    public $tAct = 'act';
    public $tPackingList = 'packing_list';

    public function safeUp()
    {
        $this->dropColumn($this->tInvoice, 'document_filename');
        $this->dropColumn($this->tInvoiceFacture, 'document_filename');
        $this->dropColumn($this->tAct, 'document_filename');
        $this->dropColumn($this->tPackingList, 'document_filename');
    }
    
    public function safeDown()
    {
        $this->addColumn($this->tInvoice, 'document_filename', $this->string()->defaultValue(null));
        $this->addColumn($this->tInvoiceFacture, 'document_filename', $this->string()->defaultValue(null));
        $this->addColumn($this->tAct, 'document_filename', $this->string()->defaultValue(null));
        $this->addColumn($this->tPackingList, 'document_filename', $this->string()->defaultValue(null));
    }
}
