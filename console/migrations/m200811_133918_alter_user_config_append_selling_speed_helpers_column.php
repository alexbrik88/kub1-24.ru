<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200811_133918_alter_user_config_append_selling_speed_helpers_column extends Migration
{
    public $tableName = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'selling_speed_help', $this->boolean(1)->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'selling_speed_chart', $this->boolean(1)->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'selling_speed_help');
        $this->dropColumn($this->tableName, 'selling_speed_chart');

    }
}
