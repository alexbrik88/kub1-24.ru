<?php

use yii\db\Schema;
use yii\db\Migration;

class m151119_073447_insert_kubcontractor extends Migration
{
    public $tContractor = 'contractor';
    public $tCompany = 'company';

    public function safeUp()
    {
        $this->dropForeignKey('contractor_company_rel', $this->tContractor);
        $this->alterColumn($this->tContractor, 'company_id', $this->integer()->defaultValue(null));
        $this->addForeignKey('contractor_company_rel', $this->tContractor, 'company_id', 'company', 'id');


        $contractor = [
            'company_id' => null,
            'type' => 1, // поставщик
            'status' => 1, // активный
            'company_type_id' => 2, // ООО
            'name' => 'КУБ',
            'director_name' => 'ФИО',
            'director_email' => 'Email',
            'director_phone' => 'Телефон',
            'chief_accountant_is_director' => true,
            'chief_accountant_name' => null,
            'chief_accountant_email' => null,
            'chief_accountant_phone' => null,
            'contact_is_director' => true,
            'contact_name' => null,
            'contact_phone' => null,
            'contact_email' => null,
            'legal_address' => 'Юридический адрес',
            'actual_address' => 'Фактический адрес',
            'BIN' => 'ОГРН',
            'ITN' => 'ИНН',
            'PPC' => 'КПП',
            'current_account' => 'Р/с',
            'bank_name' => 'Наименование банка',
            'corresp_account' => 'К/с',
            'BIC' => 'БИК',

            'created_at' => new \yii\db\Expression('UNIX_TIMESTAMP()'),
            'employee_id' => null,
            'taxation_system' => 0, // без НДС
            'is_deleted' => 0,
        ];

        if ((new \yii\db\Query())->from('contractor')->andWhere(['id' => 1,])->count() > 0) {
            $this->update($this->tContractor, $contractor, 'id = 1');
        } else {
            $contractor['id'] = 1;
            $this->insert($this->tContractor, $contractor);
        }
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('contractor_company_rel', $this->tContractor);
        $this->alterColumn($this->tContractor, 'company_id', $this->integer()->notNull());
        $this->addForeignKey('contractor_company_rel', $this->tContractor, 'company_id', 'company', 'id');
    }
}
