<?php

use common\models\employee\Employee;
use common\models\insales\Client;
use console\components\db\Migration;

class m200110_123541_insales_client_company_id_insert_from_employee_id extends Migration
{
    const TABLE = '{{%insales_client}}';

    public function safeUp()
    {
        foreach (Client::find()->all() as $client) {
            /** @var Client $client */
            /** @noinspection PhpUndefinedFieldInspection */
            $client->company_id = Employee::findOne(['id' => $client->employee_id])->company_id;
            $client->save();
        }
    }

    public function down()
    {
        $this->update(self::TABLE, ['company_id' => 0]);
    }
}
