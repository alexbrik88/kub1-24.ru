<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191203_121631_alter_service_subscribe_tariff_group_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%service_subscribe_tariff_group}}', 'hint', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%service_subscribe_tariff_group}}', 'hint');
    }
}
