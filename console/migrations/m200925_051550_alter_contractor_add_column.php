<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200925_051550_alter_contractor_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%contractor}}', 'guaranty_delay', $this->integer()->notNull()->after('payment_delay')->defaultValue(20));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%contractor}}', 'guaranty_delay');
    }
}
