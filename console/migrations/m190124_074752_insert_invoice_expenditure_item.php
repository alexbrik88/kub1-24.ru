<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190124_074752_insert_invoice_expenditure_item extends Migration
{
    public function safeUp()
    {
        $this->insert('invoice_expenditure_item', [
            'id' => 52,
            'name' => 'Обеспечительный платёж',
        ]);
    }

    public function safeDown()
    {
        $this->delete('invoice_expenditure_item', ['id' => 52]);
    }
}
