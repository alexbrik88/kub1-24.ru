<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160726_185322_add_to_contractor_invoice_expenditure_item_id extends Migration
{
    public $tContractor = 'contractor';

    public function safeUp()
    {
        $this->addColumn($this->tContractor, 'invoice_expenditure_item_id', $this->integer()->defaultValue(null));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tContractor, 'invoice_expenditure_item_id');
    }
}


