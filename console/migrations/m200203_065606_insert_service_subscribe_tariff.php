<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200203_065606_insert_service_subscribe_tariff extends Migration
{
    public function safeUp()
    {
        $this->batchInsert('{{%service_subscribe_tariff}}', [
            'id',
            'tariff_group_id',
            'duration_month',
            'duration_day',
            'price',
            'is_active',
            'proposition',
            'invoice_limit',
        ], [
            [34, 9, 4, 0, 1000, 1, 'Получи скидку 15% при оплате за 10 распознаваний', 50],
            [35, 9, 4, 0, 1700, 1, 'Получи скидку 25% при оплате за 50 распознаваний', 100],
            [36, 9, 4, 0, 3000, 1, 'Получи скидку 40% при оплате за 100 распознаваний', 200],
            [37, 9, 4, 0, 3600, 1, 'Получи скидку 50% при оплате за 500 распознаваний', 300],
            [38, 9, 4, 0, 4000, 1, 'Лучшая цена!', 400],
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%service_subscribe_tariff}}', [
            'id' => [34, 35, 36, 37, 38],
        ]);
    }
}
