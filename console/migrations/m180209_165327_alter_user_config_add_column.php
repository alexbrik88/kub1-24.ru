<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180209_165327_alter_user_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_config', 'invoice_paylimit', $this->boolean()->notNull()->defaultValue(false)->after('invoice_scan'));

        $this->addCommentOnColumn('user_config', 'invoice_paylimit', 'Показ колонки "Оплатить до" в списке счетов');
    }

    public function safeDown()
    {
        $this->dropColumn('user_config', 'invoice_paylimit');
    }
}
