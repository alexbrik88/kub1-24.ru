<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181027_161030_alter_table_waybill extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%waybill}}', 'release_allowed_position', $this->string(45));
        $this->addColumn('{{%waybill}}', 'release_allowed_fullname', $this->string(60));
        $this->addColumn('{{%waybill}}', 'release_produced_position', $this->string(45));
        $this->addColumn('{{%waybill}}', 'release_produced_fullname', $this->string(60));
        $this->addColumn('{{%waybill}}', 'cargo_accepted_position', $this->string(45));
        $this->addColumn('{{%waybill}}', 'cargo_accepted_fullname', $this->string(60));
        $this->addColumn('{{%waybill}}', 'driver_license', $this->string(45));
        $this->addColumn('{{%waybill}}', 'delivery_time', $this->string(45));
        $this->addColumn('{{%waybill}}', 'organization', $this->string(45));
        $this->addColumn('{{%waybill}}', 'car', $this->string(25));
        $this->addColumn('{{%waybill}}', 'car_number', $this->string(25));
        $this->addColumn('{{%waybill}}', 'license_card', $this->string(25));
        $this->addColumn('{{%waybill}}', 'transportation_type', $this->string(25));
        $this->addColumn('{{%waybill}}', 'transportation_route', $this->string(255));
        $this->addColumn('{{%waybill}}', 'trailer1_brand', $this->string(25));
        $this->addColumn('{{%waybill}}', 'trailer1_number', $this->string(25));
        $this->addColumn('{{%waybill}}', 'trailer1_garage_number', $this->string(25));
        $this->addColumn('{{%waybill}}', 'trailer2_brand', $this->string(25));
        $this->addColumn('{{%waybill}}', 'trailer2_number', $this->string(25));
        $this->addColumn('{{%waybill}}', 'trailer2_garage_number', $this->string(25));
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%waybill}}', 'release_allowed_position');
        $this->dropColumn('{{%waybill}}', 'release_allowed_fullname');
        $this->dropColumn('{{%waybill}}', 'release_produced_position');
        $this->dropColumn('{{%waybill}}', 'release_produced_fullname');
        $this->dropColumn('{{%waybill}}', 'cargo_accepted_position');
        $this->dropColumn('{{%waybill}}', 'cargo_accepted_fullname');
        $this->dropColumn('{{%waybill}}', 'driver_license');
        $this->dropColumn('{{%waybill}}', 'delivery_time');
        $this->dropColumn('{{%waybill}}', 'organization');
        $this->dropColumn('{{%waybill}}', 'car');
        $this->dropColumn('{{%waybill}}', 'car_number');
        $this->dropColumn('{{%waybill}}', 'license_card');
        $this->dropColumn('{{%waybill}}', 'transportation_type');
        $this->dropColumn('{{%waybill}}', 'transportation_route');
        $this->dropColumn('{{%waybill}}', 'trailer1_brand');
        $this->dropColumn('{{%waybill}}', 'trailer1_number');
        $this->dropColumn('{{%waybill}}', 'trailer1_garage_number');
        $this->dropColumn('{{%waybill}}', 'trailer2_brand');
        $this->dropColumn('{{%waybill}}', 'trailer2_number');
        $this->dropColumn('{{%waybill}}', 'trailer2_garage_number');
    }
}


