<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200922_084417_alter_contractor_account_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%contractor_account}}', 'corr_bank_name', $this->string());
        $this->addColumn('{{%contractor_account}}', 'corr_bank_address', $this->text());
        $this->addColumn('{{%contractor_account}}', 'corr_bank_swift', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%contractor_account}}', 'corr_bank_name');
        $this->dropColumn('{{%contractor_account}}', 'corr_bank_address');
        $this->dropColumn('{{%contractor_account}}', 'corr_bank_swift');
    }
}
