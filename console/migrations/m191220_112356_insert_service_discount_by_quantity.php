<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191220_112356_insert_service_discount_by_quantity extends Migration
{
    public function safeUp()
    {
        $this->batchInsert('{{%service_discount_by_quantity}}', [
            'tariff_id',
            'quantity',
            'percent',
        ], [
            [27, 2, 8.3333],
            [27, 3, 16.6667],
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%service_discount_by_quantity}}', [
            'tariff_id' => 27,
        ]);
    }
}
