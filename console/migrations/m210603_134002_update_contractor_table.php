<?php

use yii\db\Exception;
use yii\db\Migration;

class m210603_134002_update_contractor_table extends Migration
{
    /**
     * @inheritDoc
     * @throws Exception
     */
    public function up(): void
    {
        $this->db
            ->createCommand("
                UPDATE contractor
                SET type = 4, is_customer = 0
                WHERE id IN(SELECT contractor_id FROM crm_client)
            ")
            ->execute();
    }
}
