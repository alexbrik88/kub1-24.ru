<?php

use yii\db\Migration;

/**
 * Handles the creation of table `visit`.
 */
class m180227_143352_create_company_visit_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('company_visit', [
            'company_id' => $this->integer()->notNull(),
            'number' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);
        $this->addPrimaryKey('PRIMARY_KEY', 'company_visit', ['company_id', 'number']);
        $this->addForeignKey(
            'FK_companyVisit_company',
            'company_visit',
            'company_id',
            'company',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('company_visit');
    }
}
