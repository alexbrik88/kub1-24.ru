<?php

use console\components\db\Migration;
use yii\db\Schema;

class m171031_220010_update_invoice_expenditure_items extends Migration
{
    public $tableName = 'invoice_expenditure_item';

    public function safeUp()
    {
        $this->delete($this->tableName, ['id' => 36]);
        $this->update($this->tableName, ['type' => 2], ['id' => 18]);

        $this->delete($this->tableName, ['id' => 43]);
        $this->update($this->tableName, ['type' => 3], ['id' => 14]);

        $this->insert($this->tableName, [
            'id' => 45,
            'name' => 'Выдача займа',
            'type' => 2,
        ]);
    }
    
    public function safeDown()
    {
        $this->delete($this->tableName, ['id' => 45]);
    }
}


