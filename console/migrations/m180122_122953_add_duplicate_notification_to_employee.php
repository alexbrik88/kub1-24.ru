<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180122_122953_add_duplicate_notification_to_employee extends Migration
{
    public $tableName = 'employee';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'duplicate_notification_to_sms', $this->boolean()->defaultValue(false));
        $this->addColumn($this->tableName, 'duplicate_notification_to_email', $this->boolean()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'duplicate_notification_to_sms');
        $this->dropColumn($this->tableName, 'duplicate_notification_to_email');
    }
}


