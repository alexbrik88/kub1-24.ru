<?php

use yii\db\Schema;
use yii\db\Migration;

class m150417_070152_create_product extends Migration
{

    public function safeUp()
    {
        $this->dropTable('company_to_product');
        $this->dropTable('product');

        $this->createTable('product', [
            'id' => Schema::TYPE_PK,
            'created_at' => Schema::TYPE_DATETIME,
            'creator_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Сотрудник"',
            'company_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'production_type' => Schema::TYPE_BOOLEAN . ' NOT NULL COMMENT "0 - услуга, 1 - товар"',
            'title' => Schema::TYPE_STRING . '(100) NOT NULL',
            'code' => Schema::TYPE_STRING . '(45) NULL',
            'product_unit_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'box_type' => Schema::TYPE_STRING . '(45) NULL',
            'count_in_place' => Schema::TYPE_STRING . '(45) NULL COMMENT "Количество в одном месте"',
            'place_count' => Schema::TYPE_STRING . '(45) NULL COMMENT "Количество мест, штук"',
            'mass_gross' => Schema::TYPE_STRING . '(45) COMMENT "масса брутто"',
            'has_excise' => Schema::TYPE_BOOLEAN . ' NOT NULL COMMENT "тип поля `сумма акциза`, 1 - акциз указывается пользователем, 0 - `без акциза`"',
            'excise' => Schema::TYPE_STRING . '(45) NOT NULL COMMENT "сумма акциза"',
            'price_for_buy' => Schema::TYPE_STRING . '(45) COMMENT "цена покупки без НДС"',
            'price_for_buy_nds_id' => Schema::TYPE_INTEGER . ' NULL COMMENT "НДС при покупке"',
            'price_for_buy_with_nds' => Schema::TYPE_STRING . '(45) COMMENT "цена покупки с НДС"',
            'price_for_sell' => Schema::TYPE_STRING . '(45) COMMENT "цена при продаже без НДС"',
            'price_for_sell_nds_id' => Schema::TYPE_INTEGER . ' NULL COMMENT "НДС при продаже"',
            'price_for_sell_with_nds' => Schema::TYPE_STRING . '(45) COMMENT "цена продажи с НДС"',
            'country_origin_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'customs_declaration_number' => Schema::TYPE_INTEGER . ' NULL',
        ]);

        $this->addForeignKey('product_to_employee_creator', 'product', 'creator_id', 'employee', 'id');
        $this->addForeignKey('product_to_company', 'product', 'company_id', 'company', 'id');
        $this->addForeignKey('product_to_product_unit', 'product', 'product_unit_id', 'product_unit', 'id');
        $this->addForeignKey('product_to_tax_rate1', 'product', 'price_for_buy_nds_id', 'tax_rate', 'id');
        $this->addForeignKey('product_to_tax_rate2', 'product', 'price_for_sell_nds_id', 'tax_rate', 'id');
        $this->addForeignKey('product_to_country_origin_id', 'product', 'country_origin_id', 'country', 'id');
    }
    
    public function safeDown()
    {
        $this->dropTable('product');
        $this->createTable('product', [
            'id' => Schema::TYPE_PK,
            'title' => Schema::TYPE_STRING . '(100) NOT NULL',
        ]);
        $this->createTable('company_to_product', [
            'id' => Schema::TYPE_PK,
            'company_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'product_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);
        $this->addForeignKey('company_rel', 'company_to_product', 'company_id', 'company', 'id');
        $this->addForeignKey('product_rel', 'company_to_product', 'product_id', 'product', 'id');
    }
}
