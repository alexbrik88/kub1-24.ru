<?php

use console\components\db\Migration;

class m200111_121154_bitrix24_employee_id_drop extends Migration
{
    public function safeUp()
    {
        $this->_dropColumn('catalog', 'employee_id');
        $this->_dropColumn('company', 'employee_id');
        $this->_dropColumn('contact', 'employee_id');
        $this->_dropColumn('deal', 'employee_id');
        $this->_dropColumn('deal_position', 'employee_id');
        $this->_dropColumn('invoice', 'employee_id');
        $this->_dropColumn('product', 'employee_id');
        $this->_dropColumn('section', 'employee_id');
        $this->_dropColumn('user', 'employee_id');
        $this->_dropColumn('vat', 'employee_id');
    }

    public function safeDown()
    {
        $this->_addColumn('catalog', 'employee_id', 'Идентификатор клиента КУБ', false);
        $this->_addColumn('company', 'employee_id', 'Идентификатор клиента КУБ', false);
        $this->_addColumn('contact', 'employee_id', 'Идентификатор клиента КУБ', false);
        $this->_addColumn('deal', 'employee_id', 'Идентификатор клиента КУБ', false);
        $this->_addColumn('deal_position', 'employee_id', 'Идентификатор клиента КУБ', false);
        $this->_addColumn('invoice', 'employee_id', 'Идентификатор клиента КУБ', false);
        $this->_addColumn('product', 'employee_id', 'Идентификатор клиента КУБ', false);
        $this->_addColumn('section', 'employee_id', 'Идентификатор клиента КУБ', false);
        $this->_addColumn('user', 'employee_id', 'Идентификатор клиента КУБ', false);
        $this->_addColumn('vat', 'employee_id', 'Идентификатор клиента КУБ', false);
    }

    protected static function tableName(string $table, bool $local = true): string
    {
        if ($local === true) {
            $table = 'bitrix24_' . $table;
        }
        return '{{%' . $table . '}}';
    }

    private function _addColumn(string $table, string $column, string $comment, bool $notNull = true)
    {
        $def = $this->integer(11)->after('id')->comment($comment);
        if ($notNull === true) {
            $def->notNull();
        }
        $this->addColumn(self::tableName($table), $column, $def);
    }

    private function _dropColumn(string $table, string $column = 'company_id')
    {
        $this->dropColumn(self::tableName($table), $column);
    }
}
