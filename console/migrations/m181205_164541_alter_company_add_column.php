<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181205_164541_alter_company_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('company', 'ip_patent_city', $this->string()->after('ip_certificate_date'));
        $this->addColumn('company', 'ip_patent_date', $this->date()->after('ip_patent_city'));
        $this->addColumn('company', 'ip_patent_date_end', $this->date()->after('ip_patent_date'));
    }

    public function safeDown()
    {
        $this->dropColumn('company', 'ip_patent_date_end');
        $this->dropColumn('company', 'ip_patent_date');
        $this->dropColumn('company', 'ip_patent_city');
    }
}


