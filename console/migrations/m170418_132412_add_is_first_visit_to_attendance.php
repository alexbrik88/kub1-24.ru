<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170418_132412_add_is_first_visit_to_attendance extends Migration
{
    public $tableName = 'attendance';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'is_first_visit', $this->boolean()->defaultValue(false));

        foreach (\common\models\company\Attendance::find()->select(['`id`', 'MIN(`date`)'])->groupBy('company_id')->asArray()->all() as $attendanceParams) {
           $this->update($this->tableName, ['is_first_visit' => true], ['id' => $attendanceParams['id']]);
        }
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'is_first_visit');
    }
}


