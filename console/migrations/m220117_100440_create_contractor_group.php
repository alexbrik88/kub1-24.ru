<?php

use console\components\db\Migration;
use common\models\contractor\ContractorGroup;
use yii\db\Schema;

class m220117_100440_create_contractor_group extends Migration
{
    public function safeUp()
    {
        $this->addColumn('contractor', 'seller_group_id', $this->integer()->defaultValue(1));
        $this->update('contractor', ['seller_group_id' => ContractorGroup::ID_WITHOUT]);

        $this->addColumn('contractor', 'customer_group_id', $this->integer()->defaultValue(1));
        $this->update('contractor', ['customer_group_id' => ContractorGroup::ID_WITHOUT]);

        $this->createTable('contractor_group', [
            'id' => $this->primaryKey(),
            'title' => $this->string(60)->notNull(),
            'company_id' => $this->integer(),
            'type' =>$this->integer(4)->notNull(),
        ]);

        $this->insert('contractor_group', [
            'id' => ContractorGroup::ID_WITHOUT,
            'title' => 'Без группы',
            'company_id' => null,
            'type'=>ContractorGroup::TYPE_WITHOUT
        ]);


        $this->createIndex(
            'idx_contractor_seller_group_id',
            'contractor',
            'seller_group_id'
        );

        $this->createIndex(
            'idx_contractor_customer_group_id',
            'contractor',
            'customer_group_id'
        );

        $this->addForeignKey(
            'fk_contractor_seller_group_id',
            'contractor',
            'seller_group_id',
            'contractor_group',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'fk_contractor_customer_group_id',
            'contractor',
            'customer_group_id',
            'contractor_group',
            'id',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_contractor_company_id',
            'contractor_group',
            'company_id',
            'company',
            'id',
            'CASCADE'
        );

    }

    public function safeDown()
    {
        $this->dropColumn('contractor', 'seller_group_id');
        $this->dropColumn('contractor', 'customer_group_id');

        $this->dropForeignKey(
            'fk_contractor_customer_group_id',
            'contractor'
        );
        $this->dropForeignKey(
            'fk_contractor_seller_group_id',
            'contractor'
        );

        $this->dropForeignKey(
            'fk_contractor_company_id',
            'contractor_group'
        );
        $this->dropIndex(
            'idx_contractor_seller_group_id',
            'contractor'
        );
        $this->dropIndex(
            'idx_contractor_customer_group_id',
            'contractor'
        );

        $this->dropTable('contractor_group');

        echo "m220117_100440_create_contractor_group can be reverted.\n";

        return true;
    }
}
