<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200507_061026_alter_company_add_columns extends Migration
{
    public function safeUp()
    {
        $this->execute('ALTER TABLE {{%company}} ROW_FORMAT=DYNAMIC;');
        $this->addColumn('{{%company}}', 'info_place_id', $this->integer()->after('info_industry_id'));
        $this->addColumn('{{%company}}', 'info_sites_count', $this->integer()->after('info_shops_count'));
        $this->addColumn('{{%company}}', 'info_storage_count', $this->integer()->after('info_sites_count'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%company}}', 'info_place_id');
        $this->dropColumn('{{%company}}', 'info_sites_count');
        $this->dropColumn('{{%company}}', 'info_storage_count');
    }
}
