<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170710_134420_alter_productUnit_dropIndex extends Migration
{
    public function safeUp()
    {
        $this->dropIndex('name', '{{%product_unit}}');
    }
    
    public function safeDown()
    {
        $this->createIndex('name', '{{%product_unit}}', 'name', true);
    }
}
