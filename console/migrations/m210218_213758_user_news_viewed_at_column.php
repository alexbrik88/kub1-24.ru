<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210218_213758_user_news_viewed_at_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('employee_company', 'news_viewed_at', $this->integer(11)->unsigned());
    }

    public function safeDown()
    {
        $this->dropColumn('employee_company', 'news_viewed_at');
    }
}
