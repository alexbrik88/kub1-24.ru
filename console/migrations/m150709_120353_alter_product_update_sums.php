<?php

use yii\db\Schema;
use yii\db\Migration;

class m150709_120353_alter_product_update_sums extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('product', 'price_for_buy_with_nds', Schema::TYPE_BIGINT);
        $this->alterColumn('product', 'price_for_sell_with_nds', Schema::TYPE_BIGINT);
    }
    
    public function safeDown()
    {
        $this->alterColumn('product', 'price_for_buy_with_nds', Schema::TYPE_INTEGER);
        $this->alterColumn('product', 'price_for_sell_with_nds', Schema::TYPE_INTEGER);
    }
}
