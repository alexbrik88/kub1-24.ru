<?php

use console\components\db\Migration;

class m200111_120123_bitrix24_pk_drop extends Migration
{
    public function safeUp()
    {
        $this->dropForeignKey('bitrix24_company_employee_id', '{{%bitrix24_company}}');

        $this->dropPK('catalog');
        $this->dropPK('company');
        $this->dropPK('contact');
        $this->dropPK('deal');
        $this->dropPK('deal_position');
        $this->dropPK('invoice');
        $this->dropPK('product');
        $this->dropPK('section');
        $this->dropPK('vat');
    }

    public function safeDown()
    {
        $this->createPK('catalog', 'employeeid');
        $this->createPK('company', 'employeeid');
        $this->createPK('contact', 'employeeid');
        $this->createPK('deal', 'employeeid');
        $this->createPK('deal_position', 'employeeid');
        $this->createPK('invoice', 'employeeid');
        $this->createPK('product', 'employeeid');
        $this->createPK('section', 'employeeid');
        $this->createPK('vat', 'employeeid');
    }

    protected static function tableName(string $table, bool $local = true): string
    {
        if ($local === true) {
            $table = 'bitrix24_' . $table;
        }
        return '{{%' . $table . '}}';
    }

    protected function dropPK(string $table)
    {
        $this->dropPrimaryKey('bitrix24_' . $table . '_pk', self::tableName($table));
    }

    protected function createPK(string $table, string $subColumn)
    {
        $this->addPrimaryKey('bitrix24_' . $table . '_pk', self::tableName($table), [$subColumn, 'id']);
    }
}
