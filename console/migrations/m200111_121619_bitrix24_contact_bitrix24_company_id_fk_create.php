<?php

use console\components\db\Migration;

class m200111_121619_bitrix24_contact_bitrix24_company_id_fk_create extends Migration
{
    const TABLE = '{{%bitrix24_contact}}';

    public function safeUp()
    {
        $this->addForeignKey(
            'bitrix24_contact_bitrix24_company_id', self::TABLE, ['company_id', 'bitrix24_company_id'],
            '{{%bitrix24_company}}', ['company_id', 'id'],
            'CASCADE', 'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('bitrix24_contact_bitrix24_company_id', self::TABLE);
    }
}
