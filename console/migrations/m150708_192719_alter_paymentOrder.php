<?php

use yii\db\Schema;
use yii\db\Migration;

class m150708_192719_alter_paymentOrder extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('payment_order', 'kbk', Schema::TYPE_STRING . '(20)');
        $this->alterColumn('payment_order', 'oktmo_code', Schema::TYPE_STRING . '(20)');
        $this->alterColumn('payment_order', 'ranking_of_payment', Schema::TYPE_STRING . '(20)');
        $this->alterColumn('payment_order', 'sum', Schema::TYPE_BIGINT . ' NOT NULL');
    }

    public function safeDown()
    {
        $this->alterColumn('payment_order', 'kbk', Schema::TYPE_INTEGER);
        $this->alterColumn('payment_order', 'oktmo_code', Schema::TYPE_INTEGER);
        $this->alterColumn('payment_order', 'ranking_of_payment', Schema::TYPE_INTEGER);
        $this->alterColumn('payment_order', 'sum', Schema::TYPE_INTEGER . ' NOT NULL');
    }
}
