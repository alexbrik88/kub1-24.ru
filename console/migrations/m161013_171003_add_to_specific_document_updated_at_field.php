<?php

use console\components\db\Migration;
use yii\db\Schema;

class m161013_171003_add_to_specific_document_updated_at_field extends Migration
{
    public function safeUp()
    {
        $this->addColumn('specific_document', 'updated_at', $this->integer());
    }
    
    public function safeDown()
    {
        $this->dropColumn('specific_document', 'updated_at');
    }
}


