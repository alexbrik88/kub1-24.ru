<?php

use console\components\db\Migration;
use yii\db\Schema;

class m220124_163024_alter_olap_flows_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('olap_flows', 'tin_child_invoice_amount', $this->bigInteger()->after('tin_child_amount'));
    }

    public function safeDown()
    {
        $this->dropColumn('olap_flows', 'tin_child_invoice_amount');
    }
}
