<?php

use console\components\db\Migration;
use yii\db\Schema;

class m161208_152309_alter_employee_addColumns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('employee', 'alert_close_count', $this->integer()->notNull()->defaultValue(0));
        $this->addColumn('employee', 'alert_close_time', $this->integer()->notNull()->defaultValue(0));
    }
    
    public function safeDown()
    {
        $this->dropColumn('employee', 'alert_close_count');
        $this->dropColumn('employee', 'alert_close_time');
    }
}


