<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190129_134448_insert_service_discount_by_quantity extends Migration
{
    public static $items = [
        5 => [
            2 => 25.0,
            3 => 33.3333,
            4 => 40.0,
            5 => 50.0,
        ],
        6 => [
            2 => 25.0,
            3 => 33.3333,
            4 => 40.0,
            5 => 50.0,
        ],
        7 => [
            2 => 25.0,
            3 => 33.3333,
            4 => 40.0,
            5 => 50.0,
        ],
        8 => [
            2 => 25.0,
            3 => 33.3333,
            4 => 35.0,
        ],
        9 => [
            2 => 25.0,
            3 => 33.3333,
            4 => 34.375,
        ],
        10 => [
            2 => 25.0,
            3 => 33.3333,
        ],
        11 => [
            2 => 25.0,
            3 => 33.3333,
            4 => 34.0,
        ],
        12 => [
            2 => 25.0,
            3 => 33.3333,
        ],
        13 => [
            2 => 25.0,
            3 => 33.3333,
            4 => 40.0,
            5 => 50.0,
        ],
        14 => [
            2 => 25.0,
            3 => 33.3333,
            4 => 40.0,
            5 => 50.0,
        ],
        15 => [
            2 => 25.0,
            3 => 33.3333,
            4 => 40.0,
            5 => 50.0,
        ],
    ];

    public function safeUp()
    {
        foreach (self::$items as $id => $data) {
            foreach ($data as $key => $value) {
                $this->insert('service_discount_by_quantity', [
                    'tariff_id' => $id,
                    'quantity' => $key,
                    'percent' => $value,
                ]);
            }
        }
    }

    public function safeDown()
    {
        $this->delete('service_discount_by_quantity', ['tariff_id' => array_keys(self::$items)]);
    }
}
