<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180120_124427_alter_invoice_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('invoice', 'is_by_out_link', $this->boolean()->defaultValue(false));
        $this->addColumn('invoice', 'by_out_link_id', $this->string(50));

        $this->addForeignKey('FK_invoice_outInvoice', 'invoice', 'by_out_link_id', 'out_invoice', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_invoice_outInvoice', 'invoice');

        $this->dropColumn('invoice', 'is_by_out_link');
        $this->dropColumn('invoice', 'by_out_link_id');
    }
}
