<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211206_093936_alter_rent_attribute_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%rent_attribute}}', 'grid_cols', $this->tinyInteger(2)->notNull()->defaultValue(3));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%rent_attribute}}', 'grid_cols');
    }
}
