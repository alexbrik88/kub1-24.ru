<?php

use console\components\db\Migration;

class m200109_185412_evotor_store_company_id_create_fk extends Migration
{
    const TABLE = '{{%evotor_store}}';

    public function up()
    {
        $this->addForeignKey(
            'evotor_store_company_id', self::TABLE, 'company_id',
            '{{%company}}', 'id',
            'CASCADE', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey('evotor_store_company_id', self::TABLE);
    }
}
