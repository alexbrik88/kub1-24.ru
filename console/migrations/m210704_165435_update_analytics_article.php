<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210704_165435_update_analytics_article extends Migration
{
    public function safeUp()
    {
        $INCOME = 1;  // CashFlowsBase
        $PAL_INCOME_OTHER = 41; // AnalyticsArticle
        $ITEM_RETURN = 3; // InvoiceIncomeItem
        $ITEM_BUDGET_RETURN = 13;
        $this->execute("
            UPDATE `analytics_article` 
            SET `profit_and_loss_type` = {$PAL_INCOME_OTHER} 
            WHERE `type` = {$INCOME} 
              AND `item_id` IN ({$ITEM_RETURN}, {$ITEM_BUDGET_RETURN})
              AND `profit_and_loss_type` = 0;
        ");

        $INCOME = 1;  // CashFlowsBase
        $PAL_INCOME_PERCENT = 51; // AnalyticsArticle
        $ITEM_RECEIVED_PERCENTS_BY_DEPOSITS = 21; // InvoiceIncomeItem
        $this->execute("
            UPDATE `analytics_article` 
            SET `profit_and_loss_type` = {$PAL_INCOME_PERCENT} 
            WHERE `type` = {$INCOME} 
              AND `item_id` IN ({$ITEM_RECEIVED_PERCENTS_BY_DEPOSITS})
              AND `profit_and_loss_type` = 0;
        ");

        $INCOME = 1;  // CashFlowsBase
        $PAL_INCOME_UNSET = 0; // AnalyticsArticle
        $ITEM_STARTING_BALANCE = 15; // InvoiceIncomeItem
        $ITEM_INPUT_MONEY = 22;
        $ITEM_CASH_PAYMENT = 2;
        $ITEM_FROM_FOUNDER = 6;
        $ITEM_BORROWING_REDEMPTION = 10;
        $ITEM_BORROWING_DEPOSIT = 19;
        $ITEM_LOAN = 4;
        $ITEM_CREDIT = 5;
        $ITEM_CURRENCY_CONVERSION = 23;
        $ITEM_ENSURE_PAYMENT = 14;
        $ITEM_CARD_REPLENISHMENT  = 12;
        $ITEM_OWN_FUNDS = 9;
        $this->execute("
            UPDATE `analytics_article` 
            SET `profit_and_loss_type` = {$PAL_INCOME_UNSET} 
            WHERE `type` = {$INCOME} 
              AND `item_id` IN (
                {$ITEM_STARTING_BALANCE}, 
                {$ITEM_INPUT_MONEY},
                {$ITEM_CASH_PAYMENT}, 
                {$ITEM_FROM_FOUNDER}, 
                {$ITEM_BORROWING_REDEMPTION}, 
                {$ITEM_BORROWING_DEPOSIT},
                {$ITEM_LOAN}, 
                {$ITEM_CREDIT}, 
                {$ITEM_CURRENCY_CONVERSION},
                {$ITEM_ENSURE_PAYMENT}, 
                {$ITEM_CARD_REPLENISHMENT},
                {$ITEM_OWN_FUNDS}
              )
              AND `profit_and_loss_type` <> 0;
        ");

        $EXPENSE = 0; // CashFlowsBase
        $PAL_INCOME_UNSET = 0; // AnalyticsArticle
        $ITEM_CONTRACTOR_PAYMENT = 13;  // InvoiceExpenditureItem
        $ITEM_RETURN_FOUNDER = 39;
        $ITEM_BORROWING_EXTRADITION = 45;
        $ITEM_DEPOSIT = 62;
        $ITEM_CURRENCY_CONVERSION = 91;
        $ITEM_ENSURE_PAYMENT = 52;
        $ITEM_LOAN_REPAYMENT = 18;
        $ITEM_REPAYMENT_CREDIT = 37;
        $ITEM_OWN_FUNDS = 44;
        $this->execute("
            UPDATE `analytics_article` 
            SET `profit_and_loss_type` = {$PAL_INCOME_UNSET} 
            WHERE `type` = {$EXPENSE} 
              AND `item_id` IN (
                {$ITEM_CONTRACTOR_PAYMENT}, 
                {$ITEM_RETURN_FOUNDER},
                {$ITEM_BORROWING_EXTRADITION}, 
                {$ITEM_DEPOSIT}, 
                {$ITEM_CURRENCY_CONVERSION}, 
                {$ITEM_ENSURE_PAYMENT},
                {$ITEM_LOAN_REPAYMENT}, 
                {$ITEM_REPAYMENT_CREDIT}, 
                {$ITEM_OWN_FUNDS}                   
              )
              AND `profit_and_loss_type` <> 0;
        ");
    }

    public function safeDown()
    {
        // no down
    }
}
