<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200717_074702_alter_employee_company_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%employee_company}}', 'at_business_analytics_now', $this->boolean()->defaultValue(false));

        $this->execute('
            UPDATE {{%employee_company}}
            LEFT JOIN {{%company}} ON {{%company}}.[[id]] = {{%employee_company}}.[[company_id]]
            SET {{%employee_company}}.[[at_business_analytics_now]] = IF({{%company}}.[[registration_page_type_id]] = 30, 1, 0)
        ');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%employee_company}}', 'at_business_analytics_now');
    }
}
