<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181001_092041_alter_employee_alter_columns extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('employee', 'position', $this->string());
        $this->alterColumn('employee', 'date_hiring', $this->date()->defaultValue(null));
    }

    public function safeDown()
    {
        $this->alterColumn('employee', 'position', $this->string()->notNull());
        $this->alterColumn('employee', 'date_hiring', $this->date()->notNull());
    }
}
