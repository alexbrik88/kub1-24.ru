<?php

use console\components\db\Migration;
use yii\db\Expression;
use yii\db\Schema;

class m200831_090214_update_service_subscribe_tariff_group extends Migration
{
    public function safeUp()
    {
        $this->update('{{%service_subscribe_tariff_group}}', [
            'name' => 'Выставление счетов',
            'hint' => null,
        ], [
            'id' => 1,
        ]);
        $this->update('{{%service_subscribe_tariff_group}}', [
            'name' => new Expression('REPLACE([[name]], "BI.", "Бизнес Аналитика. ")'),
            'hint' => null,
        ], [
            'id' => range(11, 18),
        ]);
    }

    public function safeDown()
    {
        $this->update('{{%service_subscribe_tariff_group}}', [
            'name' => 'Стандарт',
            'hint' => '(Выставление счетов)',
        ], [
            'id' => 1,
        ]);
        $this->update('{{%service_subscribe_tariff_group}}', [
            'name' => new Expression('REPLACE([[name]], "Бизнес Аналитика. ", "BI.")'),
            'hint' => '(Бизнес аналитика)',
        ], [
            'id' => range(11, 18),
        ]);
    }
}
