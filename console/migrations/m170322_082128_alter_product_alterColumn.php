<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170322_082128_alter_product_alterColumn extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%product}}', 'product_count', $this->decimal(20, 10)->defaultValue(0));
        $this->alterColumn('{{%product}}', 'product_begin_count', $this->decimal(20, 10));
    }
    
    public function safeDown()
    {
        $this->alterColumn('{{%product}}', 'product_count', $this->integer()->defaultValue(0));
        $this->alterColumn('{{%product}}', 'product_begin_count', $this->integer());
    }
}


