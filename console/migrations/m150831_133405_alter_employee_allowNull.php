<?php

use yii\db\Schema;
use yii\db\Migration;

class m150831_133405_alter_employee_allowNull extends Migration
{
    public $tableEmployee = 'employee';

    public function safeUp()
    {
        $this->alterColumn($this->tableEmployee, 'lastname', $this->string(45));
        $this->alterColumn($this->tableEmployee, 'firstname', $this->string(45));
        $this->alterColumn($this->tableEmployee, 'firstname_initial', $this->string(45));
        $this->alterColumn($this->tableEmployee, 'patronymic', $this->string(45));
        $this->alterColumn($this->tableEmployee, 'patronymic_initial', $this->string(45));
    }
    
    public function safeDown()
    {
        $this->alterColumn($this->tableEmployee, 'lastname', $this->string(45)->notNull());
        $this->alterColumn($this->tableEmployee, 'firstname', $this->string(45)->notNull());
        $this->alterColumn($this->tableEmployee, 'firstname_initial', $this->string(45)->notNull());
        $this->alterColumn($this->tableEmployee, 'patronymic', $this->string(45)->notNull());
        $this->alterColumn($this->tableEmployee, 'patronymic_initial', $this->string(45)->notNull());
    }
}
