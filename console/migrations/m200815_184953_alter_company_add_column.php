<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200815_184953_alter_company_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('company', 'business_analytics_first_visit_at', $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn('company', 'business_analytics_first_visit_at');
    }
}
