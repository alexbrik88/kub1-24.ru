<?php

use console\components\db\Migration;
use yii\db\Schema;
use common\models\employee\Employee;
use common\models\Company;

class m151221_103022_set_default_value_time_zone_id extends Migration
{
    public function safeUp()
    {
        $this->update(Employee::tableName(), ['time_zone_id' => 307], ['time_zone_id' => 'Europe/Moscow']);
        $this->update(Company::tableName(), ['time_zone_id' => 307], ['time_zone_id' => 0]);
    }
    
    public function safeDown()
    {
        $this->update(Employee::tableName(), ['time_zone_id' => 'Europe/Moscow'], ['time_zone_id' => 307]);
        $this->update(Company::tableName(), ['time_zone_id' => 0], ['time_zone_id' => 307]);
    }
}


