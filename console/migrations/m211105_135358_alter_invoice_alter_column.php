<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211105_135358_alter_invoice_alter_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%invoice}}', 'payment_limit_date', $this->date());
    }

    public function safeDown()
    {
        $this->alterColumn('{{%invoice}}', 'payment_limit_date', $this->date()->notNull());
    }
}
