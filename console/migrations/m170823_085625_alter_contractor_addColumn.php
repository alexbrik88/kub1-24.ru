<?php

use common\models\company\CompanyType;
use console\components\db\Migration;
use yii\db\Schema;

class m170823_085625_alter_contractor_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%contractor}}', 'director_post_name', $this->string() . ' AFTER [[director_name]]');

        $this->execute("
			UPDATE {{%contractor}}
			SET 
				[[director_post_name]] = IF([[company_type_id]] IS NOT NULL,
					IF([[company_type_id]] = :type_ip, 'Предприниматель', 'Генеральный директор'), '')
        ", [':type_ip' => CompanyType::TYPE_IP]);
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%contractor}}', 'director_post_name');
    }
}
