<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180517_084950_update_store_out_invoice_tariff extends Migration
{
    public $tableName = 'service_store_out_invoice_tariff';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'is_visible', $this->boolean()->defaultValue(true));
        $this->insert($this->tableName, [
            'id' => 4,
            'links_count' => 10,
            'total_amount' => 10000,
            'is_visible' => false,
        ]);
    }
    
    public function safeDown()
    {
        $this->delete($this->tableName, ['id' => 4]);
        $this->dropColumn($this->tableName, 'is_visible');
    }
}


