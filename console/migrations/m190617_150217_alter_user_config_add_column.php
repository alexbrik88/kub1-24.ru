<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190617_150217_alter_user_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_config', 'invoice_comment', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn('user_config', 'contr_inv_comment', $this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('user_config', 'invoice_comment');
        $this->dropColumn('user_config', 'contr_inv_comment');
    }
}
