<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201004_174704_alter_item_flow_of_funds_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('expense_item_flow_of_funds', 'break_even_block', $this->tinyInteger()->after('flow_of_funds_block'));
    }

    public function safeDown()
    {
        $this->dropColumn('expense_item_flow_of_funds', 'break_even_block');
    }
}
