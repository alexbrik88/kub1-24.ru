<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170703_165157_insert_packingListStatus extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%packing_list_status}}', [
            'id' => 5,
            'name' => 'Отменена',
        ]);
    }
    
    public function safeDown()
    {
        $this->delete('{{%packing_list_status}}', 'id = 5');
    }
}
