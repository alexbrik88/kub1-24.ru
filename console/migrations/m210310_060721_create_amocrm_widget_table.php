<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%amocrm_widget}}`.
 */
class m210310_060721_create_amocrm_widget_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%amocrm_widget}}', [
            'account_id' => $this->integer()->notNull()->comment('ID аккаунта, из которого сделан запрос'),
            'created_at' => $this->integer()->notNull()->comment('Timestamp, когда подключен виджет'),
            'employee_id' => $this->integer()->notNull()->comment('ID сотрудника в КУБ, из под которого подключен виджет'),
            'company_id' => $this->integer()->notNull()->comment('ID компании в КУБ, для которой подключен виджет'),
            'user_id' => $this->integer()->notNull()->comment('ID пользователя, из под которого сделан запрос'),
            'client_uuid' => $this->text()->notNull()->comment('UUID интеграции, которая сделала запрос'),
            'iss' => $this->text()->notNull()->comment('Адрес аккаунта в amoCRM'),
            'aud' => $this->text()->notNull()->comment('Базовый адрес, который сформирован исходя из значения redirect_uri в интеграции'),
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', '{{%amocrm_widget}}', 'account_id');

        $this->addForeignKey('fk_amocrm_widget__employee_company', '{{%amocrm_widget}}', [
            'employee_id',
            'company_id',
        ], '{{%employee_company}}', [
            'employee_id',
            'company_id',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%amocrm_widget}}');
    }
}
