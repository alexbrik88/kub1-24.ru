<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%agreement_title_template}}`.
 */
class m191015_112811_create_agreement_title_template_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%agreement_title_template}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);

        $this->batchInsert('{{%agreement_title_template}}', ['id', 'name'], [
            [1, 'Тип документа + № + дата'],
            [2, 'Название документа + № + дата'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%agreement_title_template}}');
    }
}
