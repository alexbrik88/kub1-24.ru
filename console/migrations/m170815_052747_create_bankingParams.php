<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170815_052747_create_bankingParams extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%banking_params}}', [
            'company_id' => $this->integer()->notNull(),
            'bank_bik' => $this->string(9)->notNull(),
            'param_name' => $this->string()->notNull(),
            'param_value' => $this->string()->notNull(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', '{{%banking_params}}', ['company_id', 'bank_bik', 'param_name']);

        $this->addForeignKey('FK_bankingParams_company', '{{%banking_params}}', 'company_id', '{{%company}}', 'id', 'CASCADE');
        $this->addForeignKey('FK_bankingParams_bank', '{{%banking_params}}', 'bank_bik', '{{%bank}}', 'bik');
        $this->addForeignKey('FK_bankingParams_employee_1', '{{%banking_params}}', 'created_by', '{{%employee}}', 'id');
        $this->addForeignKey('FK_bankingParams_employee_2', '{{%banking_params}}', 'updated_by', '{{%employee}}', 'id');
    }
    
    public function safeDown()
    {
        $this->dropTable('{{%banking_params}}');
    }
}
