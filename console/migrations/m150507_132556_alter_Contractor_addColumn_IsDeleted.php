<?php

use yii\db\Schema;
use yii\db\Migration;

class m150507_132556_alter_Contractor_addColumn_IsDeleted extends Migration
{
    public function safeUp()
    {
        $this->addColumn('contractor', 'is_deleted', Schema::TYPE_BOOLEAN . ' DEFAULT 0');
    }

    public function safeDown()
    {
        $this->dropColumn('contractor', 'is_deleted');
    }
}
