<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170301_001118_alter_act_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%act}}', 'signed_by_employee_id', $this->integer());
        $this->addColumn('{{%act}}', 'signed_by_name', $this->string(50));
        $this->addColumn('{{%act}}', 'sign_document_type_id', $this->integer());
        $this->addColumn('{{%act}}', 'sign_document_number', $this->string(50));
        $this->addColumn('{{%act}}', 'sign_document_date', $this->date());
        $this->addColumn('{{%act}}', 'signature_id', $this->integer());

        $this->addForeignKey('FK_act_sign_employee', '{{%act}}', 'signed_by_employee_id', '{{%employee}}', 'id');
        $this->addForeignKey('FK_act_sign_documentType', '{{%act}}', 'sign_document_type_id', '{{%document_type}}', 'id');
        $this->addForeignKey('FK_act_employeeSignature', '{{%act}}', 'signature_id', '{{%employee_signature}}', 'id');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('FK_act_sign_employee', '{{%act}}');
        $this->dropForeignKey('FK_act_sign_documentType', '{{%act}}');
        $this->dropForeignKey('FK_act_employeeSignature', '{{%act}}');
        $this->dropColumn('{{%act}}', 'signed_by_employee_id');
        $this->dropColumn('{{%act}}', 'signed_by_name');
        $this->dropColumn('{{%act}}', 'sign_document_type_id');
        $this->dropColumn('{{%act}}', 'sign_document_number');
        $this->dropColumn('{{%act}}', 'sign_document_date');
        $this->dropColumn('{{%act}}', 'signature_id');
    }
}
