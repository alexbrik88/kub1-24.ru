<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210730_144320_alter_config_add_columns extends Migration
{
    public $table = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->table, 'cash_column_industry', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table, 'bank_column_industry', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table, 'order_column_industry', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table, 'emoney_column_industry', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table, 'acquiring_column_industry', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table, 'card_column_industry', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'cash_column_industry');
        $this->dropColumn($this->table, 'bank_column_industry');
        $this->dropColumn($this->table, 'order_column_industry');
        $this->dropColumn($this->table, 'emoney_column_industry');
        $this->dropColumn($this->table, 'acquiring_column_industry');
        $this->dropColumn($this->table, 'card_column_industry');
    }
}
