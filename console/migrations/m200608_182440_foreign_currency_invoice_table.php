<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200608_182440_foreign_currency_invoice_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('foreign_currency_invoice', [
            'id' => $this->primaryKey(),
            'uid' => $this->string(255),
            'type' => $this->integer()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'contractor_id' => $this->integer()->notNull(),
            'currency_name' => $this->char(3),
            'document_author_id' => $this->integer(),
            'document_date' => $this->date(),
            'payment_limit_date' => $this->date(),
            'document_number' => $this->string(255)->notNull(),
            'document_additional_number' => $this->string(45),
            'invoice_status_id' => $this->integer()->notNull(),
            'basis_document_type_id' => $this->integer(),
            'basis_document_name' => $this->string(50),
            'basis_document_number' => $this->string(255),
            'basis_document_date' => $this->date(),
            'comment' => $this->string(1000),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey(
            'foreign_currency_invoice_company_fk',
            'foreign_currency_invoice',
            'company_id',
            'company',
            'id',
            'RESTRICT'
        );

        $this->addForeignKey(
            'foreign_currency_invoice_contractor_fk',
            'foreign_currency_invoice',
            'contractor_id',
            'contractor',
            'id',
            'RESTRICT'
        );

        $this->addForeignKey(
            'foreign_currency_invoice_document_author_fk',
            'foreign_currency_invoice',
            'document_author_id',
            'employee',
            'id',
            'RESTRICT'
        );
    }

    public function safeDown()
    {
        $this->dropTable('foreign_currency_invoice');
    }
}
