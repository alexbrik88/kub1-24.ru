<?php

use yii\db\Schema;
use yii\db\Migration;

class m150414_133256_alter_Contractor_table extends Migration
{
    public function up()
    {
        $this->renameColumn('contractor', 'INN', 'ITN');
        $this->renameColumn('contractor', 'checking_account', 'current_account');
        $this->renameColumn('contractor', 'chief_accounted_is_director', 'chief_accountant_is_director');
        $this->renameColumn('contractor', 'chief_accounted_email', 'chief_accountant_email');
        $this->renameColumn('contractor', 'chief_accounted_phone', 'chief_accountant_phone');

        $this->alterColumn('contractor', 'type', Schema::TYPE_INTEGER . ' NOT NULL COMMENT "0 - поставщик, 1 - покупатель"');
        $this->alterColumn('contractor', 'status', Schema::TYPE_BOOLEAN . ' NOT NULL COMMENT "0 - не активно, 1 - активно"');

        $this->addColumn('contractor', 'taxation_system', Schema::TYPE_BOOLEAN . ' NOT NULL COMMENT "0 - без НДС, 1 - с НДС"');
    }

    public function down()
    {

        return false;
    }

}
