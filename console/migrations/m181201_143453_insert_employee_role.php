<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181201_143453_insert_employee_role extends Migration
{
    public function safeUp()
    {
        $this->insert('employee_role', [
            'id' => 7,
            'name' => 'Без доступа в КУБ',
        ]);
    }

    public function safeDown()
    {
        $this->delete('employee_role', [
            'id' => 7,
        ]);
    }
}
