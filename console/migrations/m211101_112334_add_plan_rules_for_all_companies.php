<?php

use common\models\cash\CashFlowsBase;
use common\models\Contractor;
use common\models\document\PackingList;
use console\components\db\Migration;
use frontend\modules\analytics\models\PlanCashRule;
use common\models\Company;
use frontend\modules\analytics\models\PlanCashContractor;

class m211101_112334_add_plan_rules_for_all_companies extends Migration
{
    const INCOME_ITEMS_IDS = [
        1 => 'Оплата от покупателя',
        8 => 'Комиссия'
    ];

    const EXPENDITURE_ITEMS_IDS = [
        13 => 'Оплата поставщику',
        1 => 'Товар',
        24 => 'Доставка',
        10 => 'Мебель',
        11 => 'Оргтехника',
        23 => 'Программное обеспечение',
        19 => 'Услуги',
        8 => 'Хоз. расходы',
    ];

    public function up()
    {
        Yii::$app->db->createCommand('SET session wait_timeout=288000')->execute();
        Yii::$app->db->createCommand('SET session interactive_timeout=288000')->execute();

        echo "\n\n";
        $countCompanies = 0;
        $companiesIds = Company::find()->select('id')->asArray()->column();
        foreach ($companiesIds as $companyId) {

            $transaction = Yii::$app->db->beginTransaction();

            // delete old values
            $this->execute("DELETE FROM `plan_cash_rule` WHERE `company_id` = {$companyId}");
            $this->execute("DELETE FROM `plan_cash_contractor` WHERE `company_id` = {$companyId} AND `plan_type` = 0"); // planning by invoices

            // add plan rules
            $time = time();
            $this->execute("
                INSERT `plan_cash_rule` (`company_id`, `flow_type`, `income_item_id`, `created_at`, `updated_at`)
                VALUES
                ({$companyId}, 1, 1, {$time}, {$time}),
                ({$companyId}, 1, 8, {$time}, {$time});
            ");
            $this->execute("
                INSERT `plan_cash_rule` (`company_id`, `flow_type`, `expenditure_item_id`, `created_at`, `updated_at`)
                VALUES
                ({$companyId}, 0, 1, {$time}, {$time}),
                ({$companyId}, 0, 13, {$time}, {$time}),
                ({$companyId}, 0, 24, {$time}, {$time}),
                ({$companyId}, 0, 10, {$time}, {$time}),
                ({$companyId}, 0, 11, {$time}, {$time}),
                ({$companyId}, 0, 23, {$time}, {$time}),
                ({$companyId}, 0, 19, {$time}, {$time}),
                ({$companyId}, 0, 8, {$time}, {$time});
            ");

            // add plan contractors
            try {
                foreach (array_keys(self::INCOME_ITEMS_IDS) as $incomeId) {
                    if ($rule = $this->findRuleModel($companyId, CashFlowsBase::FLOW_TYPE_INCOME, $incomeId)) {

                        $contractorsIds = Contractor::find()
                            ->where(['company_id' => $companyId])
                            ->andWhere(['type' => Contractor::TYPE_CUSTOMER])
                            ->byIsDeleted(false)
                            ->byStatus(Contractor::ACTIVE)
                            ->andWhere(['invoice_expenditure_item_id' => $incomeId])
                            ->select('id')
                            ->distinct()
                            ->column();

                        foreach ($contractorsIds as $contractorId) {

                            if ($contractor = Contractor::findOne($contractorId)) {

                                $model = new PlanCashContractor([
                                    'company_id' => $companyId,
                                    'contractor_id' => $contractorId,
                                    'plan_type' => PlanCashContractor::PLAN_BY_FUTURE_INVOICES,
                                    'flow_type' => CashFlowsBase::FLOW_TYPE_INCOME,
                                    'payment_type' => PlanCashContractor::PAYMENT_TYPE_BANK,
                                    'item_id' => $incomeId,
                                    'payment_delay' => $contractor->customer_payment_delay,
                                    'created_at' => time(),
                                    'updated_at' => time(),
                                    'rule_id' => $rule->id
                                ]);

                                $model->save();
                            }
                        }

// too slow
//                        if (method_exists($rule, 'addPlanContractors')) {
//                            $rule->addPlanContractors();
//                        } else {
//                            echo "\nNo addPlanContractors method (adding plan contractors skipped) \n";
//                            break;
//                        }
                    }
                }
                foreach (array_keys(self::EXPENDITURE_ITEMS_IDS) as $expenseId) {
                    if ($rule = $this->findRuleModel($companyId, CashFlowsBase::FLOW_TYPE_EXPENSE, $expenseId)) {

                        $contractorsIds = Contractor::find()
                            ->where(['company_id' => $companyId])
                            ->andWhere(['type' => Contractor::TYPE_SELLER])
                            ->byIsDeleted(false)
                            ->byStatus(Contractor::ACTIVE)
                            ->andWhere(['invoice_expenditure_item_id' => $expenseId])
                            ->select('id')
                            ->distinct()
                            ->column();

                        foreach ($contractorsIds as $contractorId) {

                            if ($contractor = Contractor::findOne($contractorId)) {

                                $model = new PlanCashContractor([
                                    'company_id' => $companyId,
                                    'contractor_id' => $contractorId,
                                    'plan_type' => PlanCashContractor::PLAN_BY_FUTURE_INVOICES,
                                    'flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE,
                                    'payment_type' => PlanCashContractor::PAYMENT_TYPE_BANK,
                                    'item_id' => $expenseId,
                                    'payment_delay' => $contractor->seller_payment_delay,
                                    'created_at' => time(),
                                    'updated_at' => time(),
                                    'rule_id' => $rule->id
                                ]);

                                $model->save();
                            }
                        }

// too slow
//                        if (method_exists($rule, 'addPlanContractors')) {
//                            $rule->addPlanContractors();
//                        } else {
//                            echo "\nNo addPlanContractors method (adding plan contractors skipped) \n";
//                            break;
//                        }
                    }
                }
            } catch (Throwable $e) {
                $transaction->rollBack();
                echo "\n" . $e->getMessage() . "\n";
                exit;
            }

            $transaction->commit();

            $countCompanies++;
        }

        echo "Done. Rules updated for companies: $countCompanies\n\n";
    }

    /**
     * @param $companyId
     * @param $flowType
     * @param $itemId
     * @return null|PlanCashRule
     */
    protected function findRuleModel($companyId, $flowType, $itemId)
    {
        /* @var PackingList $model */
        $query = PlanCashRule::find()
            ->where(['company_id' => $companyId])
            ->andWhere(['flow_type' => $flowType]);

        if ($flowType == CashFlowsBase::FLOW_TYPE_EXPENSE) {
            $query->andWhere(['expenditure_item_id' => $itemId]);
        }
        elseif ($flowType == CashFlowsBase::FLOW_TYPE_INCOME) {
            $query->andWhere(['income_item_id' => $itemId]);
        }

        return $query->one();
    }

    public function down()
    {
        // no down;
    }
}
