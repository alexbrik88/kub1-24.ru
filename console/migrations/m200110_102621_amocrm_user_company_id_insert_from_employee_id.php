<?php

use common\models\employee\Employee;
use common\models\amocrm\User;
use console\components\db\Migration;

class m200110_102621_amocrm_user_company_id_insert_from_employee_id extends Migration
{
    const TABLE = '{{%amocrm_user}}';

    public function safeUp()
    {
        foreach (User::find()->all() as $user) {
            /** @var User $user */
            /** @noinspection PhpUndefinedFieldInspection */
            $user->company_id = Employee::findOne(['id' => $user->employee_id])->company_id;
            $user->save();
        }
    }

    public function down()
    {
        $this->update(self::TABLE, ['company_id' => 0]);
    }
}
