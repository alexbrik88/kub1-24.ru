<?php

use console\components\db\Migration;
use yii\db\Schema;

class m220331_024748_alter_packing_list_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%packing_list}}', 'cargo_accepted_position', $this->string(45));
        $this->addColumn('{{%packing_list}}', 'cargo_accepted_fullname', $this->string(90));
        $this->addColumn('{{%packing_list}}', 'cargo_received_position', $this->string(45));
        $this->addColumn('{{%packing_list}}', 'cargo_received_fullname', $this->string(90));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%packing_list}}', 'cargo_accepted_position');
        $this->dropColumn('{{%packing_list}}', 'cargo_accepted_fullname');
        $this->dropColumn('{{%packing_list}}', 'cargo_received_position');
        $this->dropColumn('{{%packing_list}}', 'cargo_received_fullname');
    }
}
