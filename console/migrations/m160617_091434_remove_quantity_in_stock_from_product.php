<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160617_091434_remove_quantity_in_stock_from_product extends Migration
{
    public $tProduct = 'product';

    public function safeUp()
    {
        $this->dropColumn($this->tProduct, 'quantity_in_stock');
    }
    
    public function safeDown()
    {
        $this->addColumn($this->tProduct, 'quantity_in_stock', $this->integer()->defaultValue(0));
    }
}


