<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190522_120335_create_file_dir_employee extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('file_dir_employee', [
            'id' => Schema::TYPE_PK,
            'dir_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Папка"',
            'employee_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Сотрудник"',
        ], $tableOptions);

        $this->addForeignKey('FK_file_dir_employee_to_dir', 'file_dir_employee', 'dir_id', 'file_dir', 'id');
        $this->addForeignKey('FK_file_dir_employee_to_employee', 'file_dir_employee', 'employee_id', 'employee', 'id');
    }

    public function safeDown()
    {
        $this->dropTable('file_dir_employee');
    }
}
