<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order_invoice_facture`.
 */
class m170601_150559_create_order_invoice_facture_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%order_invoice_facture}}', [
            'invoice_facture_id' => $this->integer()->notNull(),
            'order_id' => $this->integer()->notNull(),
            'empty_unit_code' => $this->smallInteger(1)->notNull()->defaultValue(false),
            'empty_unit_name' => $this->smallInteger(1)->notNull()->defaultValue(false),
            'empty_quantity' => $this->smallInteger(1)->notNull()->defaultValue(false),
            'empty_price' => $this->smallInteger(1)->notNull()->defaultValue(false),
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', '{{%order_invoice_facture}}', ['invoice_facture_id', 'order_id']);
        $this->addForeignKey ('FK_orderInvoiceFacture_invoiceFacture', '{{%order_invoice_facture}}', 'invoice_facture_id', '{{%invoice_facture}}', 'id', 'CASCADE');
        $this->addForeignKey ('FK_orderInvoiceFacture_order', '{{%order_invoice_facture}}', 'order_id', '{{%order}}', 'id', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%order_invoice_facture}}');
    }
}
