<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181027_083039_create_waybill_order extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('order_waybill', [
            'id' => Schema::TYPE_PK,
            'order_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Счёт"',
            'waybill_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "ТТН"',
            'product_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Продукция"',
            'quantity' => Schema::TYPE_DECIMAL . '(20,10) NOT NULL'
        ], $tableOptions);

        $this->addForeignKey('FK_order_waybill_to_order', 'order_waybill', 'order_id', 'order', 'id');
        $this->addForeignKey('FK_order_waybill_to_waybill', 'order_waybill', 'waybill_id', 'waybill', 'id');
        $this->addForeignKey('FK_order_waybill_to_product', 'order_waybill', 'product_id', 'product', 'id');
    }
    
    public function safeDown()
    {
		$this->dropTable('order_waybill');
    }
}


