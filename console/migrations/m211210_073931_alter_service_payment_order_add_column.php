<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211210_073931_alter_service_payment_order_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%service_payment_order}}', 'discount_id', $this->integer()->after('tariff_id'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%service_payment_order}}', 'discount_id');
    }
}
