<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190215_115819_employee_add_columns extends Migration
{
    public $tableName = 'employee_company';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'passport_isRf', $this->smallInteger(2)->unsigned()->defaultValue(1));
        $this->addColumn($this->tableName, 'passport_country', $this->string(25));
        $this->addColumn($this->tableName, 'passport_series', $this->string(25));
        $this->addColumn($this->tableName, 'passport_issued_by', $this->string(160));
        $this->addColumn($this->tableName, 'passport_department', $this->string(255));
        $this->addColumn($this->tableName, 'passport_address', $this->string(255));
        $this->addColumn($this->tableName, 'passport_number', $this->string(25));
        $this->addColumn($this->tableName, 'passport_date_output', $this->date());
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'passport_isRf');
        $this->dropColumn($this->tableName, 'passport_country');
        $this->dropColumn($this->tableName, 'passport_series');
        $this->dropColumn($this->tableName, 'passport_issued_by');
        $this->dropColumn($this->tableName, 'passport_department');
        $this->dropColumn($this->tableName, 'passport_address');
        $this->dropColumn($this->tableName, 'passport_number');
        $this->dropColumn($this->tableName, 'passport_date_output');
    }
}