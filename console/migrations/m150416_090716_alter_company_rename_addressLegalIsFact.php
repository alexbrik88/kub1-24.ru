<?php

use yii\db\Schema;
use yii\db\Migration;

class m150416_090716_alter_company_rename_addressLegalIsFact extends Migration
{
    public function up()
    {
        $this->renameColumn('company', 'address_legal_is_fact', 'address_legal_is_actual');
    }

    public function down()
    {
        $this->renameColumn('company', 'address_legal_is_actual', 'address_legal_is_fact');
    }
}
