<?php

use console\components\db\Migration;
use yii\db\Expression;
use yii\db\Query;
use yii\db\Schema;

class m201002_055858_update_olap_flows_2 extends Migration
{
    // Created table
    const TABLE_OLAP_FLOWS = 'olap_flows';

    const TABLE_BANK = 'cash_bank_flows';
    const TABLE_CASHBOX = 'cash_order_flows';
    const TABLE_EMONEY = 'cash_emoney_flows';
    const TABLE_BANK_TO_INVOICE = 'cash_bank_flow_to_invoice';
    const TABLE_CASHBOX_TO_INVOICE = 'cash_order_flow_to_invoice';
    const TABLE_EMONEY_TO_INVOICE = 'cash_emoney_flow_to_invoice';
    const TABLE_INVOICE = 'invoice';
    const TABLE_PRODUCT = 'product';
    const TABLE_ORDER = 'order';

    const WALLET_BANK = 1;
    const WALLET_CASHBOX = 2;
    const WALLET_EMONEY = 3;

    public function safeUp()
    {
        $table = self::TABLE_OLAP_FLOWS;
        $idx = "idx_{$table}";

        $query = $this->getOlapTableQuery();

        $this->execute("DROP TABLE IF EXISTS {$table}");
        $this->execute("CREATE TABLE IF NOT EXISTS {$table} (
            `id` INT,
            `company_id` INT,
            `contractor_id` CHAR(16),
            `wallet` TINYINT,
            `type` TINYINT,
            `is_accounting` TINYINT,
            `item_id` INT,
            `date` DATE,
            `recognition_date` DATE,
            `is_prepaid_expense` TINYINT,
            `amount` BIGINT,
            `has_invoice` TINYINT DEFAULT 0,
            `invoice_amount` BIGINT DEFAULT 0,
            `invoice_amount_in_price_for_buy` BIGINT DEFAULT 0,
            `invoice_quantity` DECIMAL(20,10) DEFAULT 0,            
            `has_doc` TINYINT DEFAULT 0,
            `need_doc` TINYINT DEFAULT 0
        )");
        $this->execute("INSERT INTO {$table} ({$query->createCommand()->rawSql});");
        $this->execute("CREATE INDEX {$idx}_wallet ON {$table} (`company_id`, `date`, `wallet`);");
        $this->execute("CREATE INDEX {$idx}_type ON {$table} (`company_id`, `date`, `type`);");
        $this->execute("CREATE INDEX {$idx}_item ON {$table} (`company_id`, `date`, `item_id`);");
        $this->execute("ALTER TABLE {$table} ADD PRIMARY KEY (`id`, `wallet`);");

        $this->createTriggersOnCashBankFlows();
        $this->createTriggersOnCashOrderFlows();
        $this->createTriggersOnCashEmoneyFlows();
        $this->createTriggersOnCashBankFlowsToInvoices();
        $this->createTriggersOnCashOrderFlowsToInvoices();
        $this->createTriggersOnCashEmoneyFlowsToInvoices();
        $this->createTriggersOnInvoice();
        $this->createTriggersOnProduct();
    }

    public function safeDown()
    {
        $table = self::TABLE_OLAP_FLOWS;
        $this->execute("DROP TABLE IF EXISTS {$table}");
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_bank_flows`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_update_cash_bank_flows`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_delete_cash_bank_flows`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_order_flows`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_update_cash_order_flows`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_delete_cash_order_flows`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_emoney_flows');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_update_cash_emoney_flows`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_delete_cash_emoney_flows`');
        $this->execute('DROP PROCEDURE IF EXISTS `OlapFlowsRecalculateBankByFlow`');
        $this->execute('DROP PROCEDURE IF EXISTS `OlapFlowsRecalculateOrderByFlow`');
        $this->execute('DROP PROCEDURE IF EXISTS `OlapFlowsRecalculateEmoneyByFlow`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_bank_flow_to_invoice`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_delete_cash_bank_flow_to_invoice`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_order_flow_to_invoice`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_delete_cash_order_flow_to_invoice`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_emoney_flow_to_invoice`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_delete_cash_emoney_flow_to_invoice`');
        $this->execute('DROP PROCEDURE IF EXISTS `OlapFlowsRecalculateBankByInvoice`');
        $this->execute('DROP PROCEDURE IF EXISTS `OlapFlowsRecalculateEmoneyByInvoice`');
        $this->execute('DROP PROCEDURE IF EXISTS `OlapFlowsRecalculateOrderByInvoice`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_update_invoice`');
        $this->execute('DROP PROCEDURE IF EXISTS `OlapFlowsRecalculateBankByProduct`');
        $this->execute('DROP PROCEDURE IF EXISTS `OlapFlowsRecalculateOrderByProduct`');
        $this->execute('DROP PROCEDURE IF EXISTS `OlapFlowsRecalculateEmoneyByProduct`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_update_product`');
    }

    private function getOlapTableQuery()
    {
        $queryBank = (new Query())
            ->select(new Expression('
                cbf.id,
                cbf.company_id,
                cbf.contractor_id,
                '.(self::WALLET_BANK).' AS wallet,
                cbf.flow_type AS type,
                1 AS is_accounting,
                IF (cbf.flow_type = 0, cbf.expenditure_item_id, cbf.income_item_id) AS item_id,
                cbf.date,
                cbf.recognition_date,
                cbf.is_prepaid_expense,
                cbf.amount,
                IF (COUNT(invoice.id), 1, 0) AS has_invoice,
                SUM(IFNULL(order.base_price_with_vat * order.quantity, 0)) AS invoice_amount,
                SUM(IFNULL(product.price_for_buy_with_nds * order.quantity, 0)) AS invoice_amount_in_price_for_buy,
                SUM(IFNULL(order.quantity, 0)) AS invoice_quantity,
                IF (SUM(invoice.has_act) + SUM(invoice.has_packing_list) + SUM(invoice.has_upd), 1, 0) AS has_doc,
                IF (SUM( IF(invoice.need_act, 0, 1)) + SUM( IF(invoice.need_packing_list, 0, 1)) + SUM( IF(invoice.need_upd, 0, 1)), 0, 1) AS need_doc
            '))
            ->from(['cbf' => self::TABLE_BANK])
            ->leftJoin(['cbf_to_invoice' => self::TABLE_BANK_TO_INVOICE], 'cbf.id = cbf_to_invoice.flow_id')
            ->leftJoin(['invoice' => self::TABLE_INVOICE], 'invoice.id = cbf_to_invoice.invoice_id')
            ->leftJoin(['order' => self::TABLE_ORDER], 'order.invoice_id = invoice.id')
            ->leftJoin(['product' => self::TABLE_PRODUCT], 'order.product_id = product.id')
            ->groupBy(['cbf.id']);

        $queryOrder = (new Query())
            ->select(new Expression('
                cof.id,
                cof.company_id,
                cof.contractor_id,
                '.(self::WALLET_CASHBOX).' AS wallet,
                cof.flow_type AS type,
                cof.is_accounting,
                IF (cof.flow_type = 0, cof.expenditure_item_id, cof.income_item_id) AS item_id,
                cof.date,
                cof.recognition_date,
                cof.is_prepaid_expense,
                cof.amount,
                IF (COUNT(invoice.id), 1, 0) AS has_invoice,
                SUM(IFNULL(order.base_price_with_vat * order.quantity, 0)) AS invoice_amount,
                SUM(IFNULL(product.price_for_buy_with_nds * order.quantity, 0)) AS invoice_amount_in_price_for_buy,
                SUM(IFNULL(order.quantity, 0)) AS invoice_quantity,
                IF (SUM(invoice.has_act) + SUM(invoice.has_packing_list) + SUM(invoice.has_upd), 1, 0) AS has_doc,
                IF (SUM( IF(invoice.need_act, 0, 1)) + SUM( IF(invoice.need_packing_list, 0, 1)) + SUM( IF(invoice.need_upd, 0, 1)), 0, 1) AS need_doc
            '))
            ->from(['cof' => self::TABLE_CASHBOX])
            ->leftJoin(['cof_to_invoice' => self::TABLE_CASHBOX_TO_INVOICE], 'cof.id = cof_to_invoice.flow_id')
            ->leftJoin(['invoice' => self::TABLE_INVOICE], 'invoice.id = cof_to_invoice.invoice_id')
            ->leftJoin(['order' => self::TABLE_ORDER], 'order.invoice_id = invoice.id')
            ->leftJoin(['product' => self::TABLE_PRODUCT], 'order.product_id = product.id')
            ->groupBy(['cof.id']);

        $queryEmoney = (new Query())
            ->select(new Expression('
                cef.id,
                cef.company_id,
                cef.contractor_id,
                '.(self::WALLET_EMONEY).' AS wallet,
                cef.flow_type AS type,
                cef.is_accounting,
                IF (cef.flow_type = 0, cef.expenditure_item_id, cef.income_item_id) AS item_id,
                cef.date,
                cef.recognition_date,
                cef.is_prepaid_expense,
                cef.amount,
                IF (COUNT(invoice.id), 1, 0) AS has_invoice,
                SUM(IFNULL(order.base_price_with_vat * order.quantity, 0)) AS invoice_amount,
                SUM(IFNULL(product.price_for_buy_with_nds * order.quantity, 0)) AS invoice_amount_in_price_for_buy,
                SUM(IFNULL(order.quantity, 0)) AS invoice_quantity,
                IF (SUM(invoice.has_act) + SUM(invoice.has_packing_list) + SUM(invoice.has_upd), 1, 0) AS has_doc,
                IF (SUM( IF(invoice.need_act, 0, 1)) + SUM( IF(invoice.need_packing_list, 0, 1)) + SUM( IF(invoice.need_upd, 0, 1)), 0, 1) AS need_doc
            '))
            ->from(['cef' => self::TABLE_EMONEY])
            ->leftJoin(['cef_to_invoice' => self::TABLE_EMONEY_TO_INVOICE], 'cef.id = cef_to_invoice.flow_id')
            ->leftJoin(['invoice' => self::TABLE_INVOICE], 'invoice.id = cef_to_invoice.invoice_id')
            ->leftJoin(['order' => self::TABLE_ORDER], 'order.invoice_id = invoice.id')
            ->leftJoin(['product' => self::TABLE_PRODUCT], 'order.product_id = product.id')
            ->groupBy(['cef.id']);

        return $queryBank->union($queryOrder, true)->union($queryEmoney, true);
    }

    private function createTriggersOnCashBankFlows()
    {

        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_bank_flows`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_update_cash_bank_flows`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_delete_cash_bank_flows`');

        $this->execute('
            CREATE TRIGGER `olap_flows_insert_cash_bank_flows`
            AFTER INSERT ON `cash_bank_flows`
            FOR EACH ROW
            BEGIN
            
              INSERT INTO `olap_flows`
              (`id`, `company_id`, `contractor_id`, `wallet`, `type`, `is_accounting`, `item_id`, `date`, `recognition_date`, `is_prepaid_expense`, `amount`, `has_invoice`)
              VALUES
              (
                NEW.`id`, 
                NEW.`company_id`, 
                NEW.`contractor_id`, 
                "1", /* WALLET */
                NEW.`flow_type`, 
                1,
                IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id),
                NEW.`date`,
                NEW.`recognition_date`,
                NEW.`is_prepaid_expense`,
                NEW.`amount`,
                0
              );              
            END');
        $this->execute('
            CREATE TRIGGER `olap_flows_update_cash_bank_flows`
            AFTER UPDATE ON `cash_bank_flows`
            FOR EACH ROW
            BEGIN
            
              UPDATE `olap_flows` SET
                `contractor_id` = NEW.`contractor_id`, 
                `type` = NEW.`flow_type`,
                `date` = NEW.`date`,
                `recognition_date` = NEW.`recognition_date`,
                `is_prepaid_expense` = NEW.`is_prepaid_expense`,
                `amount` = NEW.`amount`,
                `item_id` = IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id)	
              WHERE `id` = NEW.`id` AND `wallet` = "1";
              
            END');
        $this->execute('
            CREATE TRIGGER `olap_flows_delete_cash_bank_flows`
            AFTER DELETE ON `cash_bank_flows`
            FOR EACH ROW
            BEGIN
            
              DELETE FROM `olap_flows` WHERE `id` = OLD.`id` AND `wallet` = "1" LIMIT 1;
              
            END');
    }

    private function createTriggersOnCashOrderFlows()
    {

        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_order_flows`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_update_cash_order_flows`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_delete_cash_order_flows`');

        $this->execute('                     
            CREATE TRIGGER `olap_flows_insert_cash_order_flows`
            AFTER INSERT ON `cash_order_flows`
            FOR EACH ROW
            BEGIN
            
              INSERT INTO `olap_flows`
              (`id`, `company_id`, `contractor_id`, `wallet`, `type`, `is_accounting`, `item_id`, `date`, `recognition_date`, `is_prepaid_expense`, `amount`, `has_invoice`)
              VALUES
              (
                NEW.`id`, 
                NEW.`company_id`, 
                NEW.`contractor_id`, 
                "2", /* WALLET */
                NEW.`flow_type`, 
                NEW.`is_accounting`,
                IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id),
                NEW.`date`,
                NEW.`recognition_date`,
                NEW.`is_prepaid_expense`,
                NEW.`amount`,
                0
              );
              
            END');
        $this->execute('
            CREATE TRIGGER `olap_flows_update_cash_order_flows`
            AFTER UPDATE ON `cash_order_flows`
            FOR EACH ROW
            BEGIN
            
              UPDATE `olap_flows` SET
                `contractor_id` = NEW.`contractor_id`, 
                `type` = NEW.`flow_type`,
                `date` = NEW.`date`,
                `recognition_date` = NEW.`recognition_date`,
                `is_prepaid_expense` = NEW.`is_prepaid_expense`,
                `amount` = NEW.`amount`,
                `item_id` = IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id),
                `is_accounting` = NEW.`is_accounting`
              WHERE `id` = NEW.`id` AND `wallet` = "2";
              
            END');
        $this->execute('
            CREATE TRIGGER `olap_flows_delete_cash_order_flows`
            AFTER DELETE ON `cash_order_flows`
            FOR EACH ROW
            BEGIN
            
              DELETE FROM `olap_flows` WHERE `id` = OLD.`id` AND `wallet` = "2" LIMIT 1;
              
            END');
    }

    private function createTriggersOnCashEmoneyFlows()
    {

        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_emoney_flows`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_update_cash_emoney_flows`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_delete_cash_emoney_flows`');

        $this->execute('        
            CREATE TRIGGER `olap_flows_insert_cash_emoney_flows`
            AFTER INSERT ON `cash_emoney_flows`
            FOR EACH ROW
            BEGIN
            
              INSERT INTO `olap_flows`
              (`id`, `company_id`, `contractor_id`, `wallet`, `type`, `is_accounting`, `item_id`, `date`, `recognition_date`, `is_prepaid_expense`, `amount`, `has_invoice`)
              VALUES
              (
                NEW.`id`, 
                NEW.`company_id`, 
                NEW.`contractor_id`, 
                "3", /* WALLET */
                NEW.`flow_type`, 
                NEW.`is_accounting`,
                IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id),
                NEW.`date`,
                NEW.`recognition_date`,
                NEW.`is_prepaid_expense`,
                NEW.`amount`,
                0
              );             
            END');
        $this->execute('
            CREATE TRIGGER `olap_flows_update_cash_emoney_flows`
            AFTER UPDATE ON `cash_emoney_flows`
            FOR EACH ROW
            BEGIN
            
              UPDATE `olap_flows` SET
                `contractor_id` = NEW.`contractor_id`, 
                `type` = NEW.`flow_type`,
                `date` = NEW.`date`,
                `recognition_date` = NEW.`recognition_date`,
                `is_prepaid_expense` = NEW.`is_prepaid_expense`,
                `amount` = NEW.`amount`,
                `item_id` = IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id),
                `is_accounting` = NEW.`is_accounting`
              WHERE `id` = NEW.`id` AND `wallet` = "3";
              
            END');
        $this->execute('
            CREATE TRIGGER `olap_flows_delete_cash_emoney_flows`
            AFTER DELETE ON `cash_emoney_flows`
            FOR EACH ROW
            BEGIN
            
              DELETE FROM `olap_flows` WHERE `id` = OLD.`id` AND `wallet` = "3" LIMIT 1;
              
            END');
    }


    private function createTriggersOnCashBankFlowsToInvoices()
    {

        $this->execute('DROP PROCEDURE IF EXISTS `OlapFlowsRecalculateBankByFlow`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_bank_flow_to_invoice`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_delete_cash_bank_flow_to_invoice`');

        $this->execute('
            CREATE PROCEDURE `OlapFlowsRecalculateBankByFlow`(flowID INT)
            BEGIN
            
              UPDATE `olap_flows` 
              LEFT JOIN 
                (SELECT 
                    `cash_bank_flow_to_invoice`.`flow_id` AS `flow_id`,
                    IF (COUNT(`invoice`.`id`), 1, 0) AS `has_invoice`,
                    SUM(`order`.`base_price_with_vat` * `order`.`quantity`) AS `amount`,
                    SUM(`product`.`price_for_buy_with_nds` * `order`.`quantity`) AS `amount_in_price_for_buy`,
                    SUM(`order`.`quantity`) AS `quantity`,
                    IF (SUM(`invoice`.`has_act`) + SUM(`invoice`.`has_packing_list`) + SUM(`invoice`.`has_upd`), 1, 0) AS `has_doc`,
                    IF (SUM( IF(`invoice`.`need_act`, 0, 1)) + SUM( IF(`invoice`.`need_packing_list`, 0, 1)) + SUM( IF(`invoice`.`need_upd`, 0, 1)), 0, 1) AS `need_doc`	
                  FROM `cash_bank_flow_to_invoice`
                  LEFT JOIN `invoice` ON `cash_bank_flow_to_invoice`.`invoice_id` = `invoice`.`id`
                  LEFT JOIN `order` ON `order`.`invoice_id` = `invoice`.`id`
                  LEFT JOIN `product` ON `order`.`product_id` = `product`.`id`	  
                  WHERE `cash_bank_flow_to_invoice`.`flow_id` = flowID
                  GROUP BY `cash_bank_flow_to_invoice`.`flow_id`
                ) `invoices` ON `olap_flows`.`id` = `invoices`.`flow_id`
                
              SET
            
                `olap_flows`.`has_invoice` = IFNULL(`invoices`.`has_invoice`, 0),
                `olap_flows`.`invoice_amount` = IFNULL(`invoices`.`amount`, 0),
                `olap_flows`.`invoice_amount_in_price_for_buy` = IFNULL(`invoices`.`amount_in_price_for_buy`, 0),     
                `olap_flows`.`invoice_quantity` = IFNULL(`invoices`.`quantity`, 0),
                `olap_flows`.`has_doc` = IFNULL(`invoices`.`has_doc`, 0),
                `olap_flows`.`need_doc` = IFNULL(`invoices`.`need_doc`, 0)
              
              WHERE `olap_flows`.`wallet` = "1" AND `olap_flows`.`id` = flowID LIMIT 1;
            
            END');
        $this->execute('
            CREATE TRIGGER `olap_flows_insert_cash_bank_flow_to_invoice`
            AFTER INSERT ON `cash_bank_flow_to_invoice`
            FOR EACH ROW
            BEGIN
            
               CALL `OlapFlowsRecalculateBankByFlow`(NEW.`flow_id`);
              
            END');
        $this->execute('
            CREATE TRIGGER `olap_flows_delete_cash_bank_flow_to_invoice`
            AFTER DELETE ON `cash_bank_flow_to_invoice`
            FOR EACH ROW
            BEGIN
            
               CALL `OlapFlowsRecalculateBankByFlow`(OLD.`flow_id`);
              
            END');
    }

    private function createTriggersOnCashOrderFlowsToInvoices()
    {

        $this->execute('DROP PROCEDURE IF EXISTS `OlapFlowsRecalculateOrderByFlow`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_order_flow_to_invoice`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_delete_cash_order_flow_to_invoice`');

        $this->execute('           
            CREATE PROCEDURE `OlapFlowsRecalculateOrderByFlow`(flowID INT)
            BEGIN
            
              UPDATE `olap_flows` 
              LEFT JOIN 
                (SELECT 
                    `cash_order_flow_to_invoice`.`flow_id` AS `flow_id`,
                    IF (COUNT(`invoice`.`id`), 1, 0) AS `has_invoice`,
                    SUM(`order`.`base_price_with_vat` * `order`.`quantity`) AS `amount`,
                    SUM(`product`.`price_for_buy_with_nds` * `order`.`quantity`) AS `amount_in_price_for_buy`,
                    SUM(`order`.`quantity`) AS `quantity`,                    
                    IF (SUM(`invoice`.`has_act`) + SUM(`invoice`.`has_packing_list`) + SUM(`invoice`.`has_upd`), 1, 0) AS `has_doc`,
                    IF (SUM( IF(`invoice`.`need_act`, 0, 1)) + SUM( IF(`invoice`.`need_packing_list`, 0, 1)) + SUM( IF(`invoice`.`need_upd`, 0, 1)), 0, 1) AS `need_doc`	
                  FROM `cash_order_flow_to_invoice`
                  LEFT JOIN `invoice` ON `cash_order_flow_to_invoice`.`invoice_id` = `invoice`.`id`
                  LEFT JOIN `order` ON `order`.`invoice_id` = `invoice`.`id`
                  LEFT JOIN `product` ON `order`.`product_id` = `product`.`id`	  
                  WHERE `cash_order_flow_to_invoice`.`flow_id` = flowID
                  GROUP BY `cash_order_flow_to_invoice`.`flow_id`
                ) `invoices` ON `olap_flows`.`id` = `invoices`.`flow_id`
                
              SET
            
                `olap_flows`.`has_invoice` = IFNULL(`invoices`.`has_invoice`, 0),
                `olap_flows`.`invoice_amount` = IFNULL(`invoices`.`amount`, 0),
                `olap_flows`.`invoice_amount_in_price_for_buy` = IFNULL(`invoices`.`amount_in_price_for_buy`, 0),
                `olap_flows`.`invoice_quantity` = IFNULL(`invoices`.`quantity`, 0),
                `olap_flows`.`has_doc` = IFNULL(`invoices`.`has_doc`, 0),
                `olap_flows`.`need_doc` = IFNULL(`invoices`.`need_doc`, 0)
              
              WHERE `olap_flows`.`wallet` = "2" AND `olap_flows`.`id` = flowID LIMIT 1;
            
            END');
        $this->execute('
            CREATE TRIGGER `olap_flows_insert_cash_order_flow_to_invoice`
            AFTER INSERT ON `cash_order_flow_to_invoice`
            FOR EACH ROW
            BEGIN
            
               CALL `OlapFlowsRecalculateOrderByFlow`(NEW.`flow_id`);
              
            END');
        $this->execute('
            CREATE TRIGGER `olap_flows_delete_cash_order_flow_to_invoice`
            AFTER DELETE ON `cash_order_flow_to_invoice`
            FOR EACH ROW
            BEGIN
            
               CALL `OlapFlowsRecalculateOrderByFlow`(OLD.`flow_id`);
              
            END');
    }

    private function createTriggersOnCashEmoneyFlowsToInvoices()
    {
        $this->execute('DROP PROCEDURE IF EXISTS `OlapFlowsRecalculateEmoneyByFlow`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_emoney_flow_to_invoice`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_delete_cash_emoney_flow_to_invoice`');

        $this->execute('
            CREATE PROCEDURE `OlapFlowsRecalculateEmoneyByFlow`(flowID INT)
            BEGIN
            
              UPDATE `olap_flows` 
              LEFT JOIN 
                (SELECT 
                    `cash_emoney_flow_to_invoice`.`flow_id` AS `flow_id`,
                    IF (COUNT(`invoice`.`id`), 1, 0) AS `has_invoice`,
                    SUM(`order`.`base_price_with_vat` * `order`.`quantity`) AS `amount`,
                    SUM(`product`.`price_for_buy_with_nds` * `order`.`quantity`) AS `amount_in_price_for_buy`,
                    SUM(`order`.`quantity`) AS `quantity`,
                    IF (SUM(`invoice`.`has_act`) + SUM(`invoice`.`has_packing_list`) + SUM(`invoice`.`has_upd`), 1, 0) AS `has_doc`,
                    IF (SUM( IF(`invoice`.`need_act`, 0, 1)) + SUM( IF(`invoice`.`need_packing_list`, 0, 1)) + SUM( IF(`invoice`.`need_upd`, 0, 1)), 0, 1) AS `need_doc`	
                  FROM `cash_emoney_flow_to_invoice`
                  LEFT JOIN `invoice` ON `cash_emoney_flow_to_invoice`.`invoice_id` = `invoice`.`id`
                  LEFT JOIN `order` ON `order`.`invoice_id` = `invoice`.`id`
                  LEFT JOIN `product` ON `order`.`product_id` = `product`.`id`
                  WHERE `cash_emoney_flow_to_invoice`.`flow_id` = flowID 	  
                  GROUP BY `cash_emoney_flow_to_invoice`.`flow_id`
                ) `invoices` ON `olap_flows`.`id` = `invoices`.`flow_id`
                
              SET
            
                `olap_flows`.`has_invoice` = IFNULL(`invoices`.`has_invoice`, 0),
                `olap_flows`.`invoice_amount` = IFNULL(`invoices`.`amount`, 0),
                `olap_flows`.`invoice_amount_in_price_for_buy` = IFNULL(`invoices`.`amount_in_price_for_buy`, 0),
                `olap_flows`.`invoice_quantity` = IFNULL(`invoices`.`quantity`, 0),
                `olap_flows`.`has_doc` = IFNULL(`invoices`.`has_doc`, 0),
                `olap_flows`.`need_doc` = IFNULL(`invoices`.`need_doc`, 0)
              
              WHERE `olap_flows`.`wallet` = "3" AND `olap_flows`.`id` = flowID LIMIT 1;
            
            END');
        $this->execute('
            CREATE TRIGGER `olap_flows_insert_cash_emoney_flow_to_invoice`
            AFTER INSERT ON `cash_emoney_flow_to_invoice`
            FOR EACH ROW
            BEGIN
            
               CALL `OlapFlowsRecalculateEmoneyByFlow`(NEW.`flow_id`);
              
            END');
        $this->execute('
            CREATE TRIGGER `olap_flows_delete_cash_emoney_flow_to_invoice`
            AFTER DELETE ON `cash_emoney_flow_to_invoice`
            FOR EACH ROW
            BEGIN
            
               CALL `OlapFlowsRecalculateEmoneyByFlow`(OLD.`flow_id`);
              
            END');
    }

    private function createTriggersOnInvoice()
    {
        $this->execute('DROP PROCEDURE IF EXISTS `OlapFlowsRecalculateBankByInvoice`');
        $this->execute('DROP PROCEDURE IF EXISTS `OlapFlowsRecalculateOrderByInvoice`');
        $this->execute('DROP PROCEDURE IF EXISTS `OlapFlowsRecalculateEmoneyByInvoice`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_update_invoice`');

        $this->execute('
            CREATE PROCEDURE `OlapFlowsRecalculateBankByInvoice`(invoiceID INT)
            BEGIN
            
              UPDATE `olap_flows` 
              LEFT JOIN 
                (SELECT 
                    `cash_bank_flow_to_invoice`.`flow_id` AS `flow_id`,
                    IF (SUM(`invoice`.`has_act`) + SUM(`invoice`.`has_packing_list`) + SUM(`invoice`.`has_upd`), 1, 0) AS `has_doc`,
                    IF (SUM( IF(`invoice`.`need_act`, 0, 1)) + SUM( IF(`invoice`.`need_packing_list`, 0, 1)) + SUM( IF(`invoice`.`need_upd`, 0, 1)), 0, 1) AS `need_doc`,
                    SUM(`order`.`base_price_with_vat` * `order`.`quantity`) AS `amount`,                    
                    SUM(`product`.`price_for_buy_with_nds` * `order`.`quantity`) AS `amount_in_price_for_buy`,
                    SUM(`order`.`quantity`) AS `quantity`                    	
                  FROM `cash_bank_flow_to_invoice`
                  LEFT JOIN `invoice` ON `cash_bank_flow_to_invoice`.`invoice_id` = `invoice`.`id`
                  LEFT JOIN `order` ON `order`.`invoice_id` = `invoice`.`id`
                  LEFT JOIN `product` ON `order`.`product_id` = `product`.`id`
                  WHERE `cash_bank_flow_to_invoice`.`invoice_id` = invoiceID
                  GROUP BY `cash_bank_flow_to_invoice`.`flow_id`
                ) `invoices` ON `olap_flows`.`id` = `invoices`.`flow_id`
                
              SET
            
                `olap_flows`.`has_doc` = IFNULL(`invoices`.`has_doc`, 0),
                `olap_flows`.`need_doc` = IFNULL(`invoices`.`need_doc`, 0),
				`olap_flows`.`invoice_amount` = IFNULL(`invoices`.`amount`, 0),                
				`olap_flows`.`invoice_amount_in_price_for_buy` = IFNULL(`invoices`.`amount_in_price_for_buy`, 0),
                `olap_flows`.`invoice_quantity` = IFNULL(`invoices`.`quantity`, 0)                
              
              WHERE `olap_flows`.`wallet` = "1" AND `olap_flows`.`id` IN (SELECT `flow_id` FROM `cash_bank_flow_to_invoice` WHERE `invoice_id` = invoiceID);
            
            END');
        $this->execute('
            CREATE PROCEDURE `OlapFlowsRecalculateOrderByInvoice`(invoiceID INT)
            BEGIN
            
              UPDATE `olap_flows` 
              LEFT JOIN 
                (SELECT 
                    `cash_order_flow_to_invoice`.`flow_id` AS `flow_id`,
                    IF (SUM(`invoice`.`has_act`) + SUM(`invoice`.`has_packing_list`) + SUM(`invoice`.`has_upd`), 1, 0) AS `has_doc`,
                    IF (SUM( IF(`invoice`.`need_act`, 0, 1)) + SUM( IF(`invoice`.`need_packing_list`, 0, 1)) + SUM( IF(`invoice`.`need_upd`, 0, 1)), 0, 1) AS `need_doc`,
                    SUM(`order`.`base_price_with_vat` * `order`.`quantity`) AS `amount`,
                    SUM(`product`.`price_for_buy_with_nds` * `order`.`quantity`) AS `amount_in_price_for_buy`,
                    SUM(`order`.`quantity`) AS `quantity`	
                  FROM `cash_order_flow_to_invoice`
                  LEFT JOIN `invoice` ON `cash_order_flow_to_invoice`.`invoice_id` = `invoice`.`id`
                  LEFT JOIN `order` ON `order`.`invoice_id` = `invoice`.`id`
                  LEFT JOIN `product` ON `order`.`product_id` = `product`.`id`	
                  WHERE `cash_order_flow_to_invoice`.`invoice_id` = invoiceID  
                  GROUP BY `cash_order_flow_to_invoice`.`flow_id`
                ) `invoices` ON `olap_flows`.`id` = `invoices`.`flow_id`
                
              SET
            
                `olap_flows`.`has_doc` = IFNULL(`invoices`.`has_doc`, 0),
                `olap_flows`.`need_doc` = IFNULL(`invoices`.`need_doc`, 0),
                `olap_flows`.`invoice_amount` = IFNULL(`invoices`.`amount`, 0),                
				`olap_flows`.`invoice_amount_in_price_for_buy` = IFNULL(`invoices`.`amount_in_price_for_buy`, 0),
                `olap_flows`.`invoice_quantity` = IFNULL(`invoices`.`quantity`, 0)                
              
              WHERE `olap_flows`.`wallet` = "2" AND `olap_flows`.`id` IN (SELECT `flow_id` FROM `cash_order_flow_to_invoice` WHERE `invoice_id` = invoiceID);
            
            END');
        $this->execute('
            CREATE PROCEDURE `OlapFlowsRecalculateEmoneyByInvoice`(invoiceID INT)
            BEGIN
            
              UPDATE `olap_flows` 
              LEFT JOIN 
                (SELECT 
                    `cash_emoney_flow_to_invoice`.`flow_id` AS `flow_id`,
                    IF (SUM(`invoice`.`has_act`) + SUM(`invoice`.`has_packing_list`) + SUM(`invoice`.`has_upd`), 1, 0) AS `has_doc`,
                    IF (SUM( IF(`invoice`.`need_act`, 0, 1)) + SUM( IF(`invoice`.`need_packing_list`, 0, 1)) + SUM( IF(`invoice`.`need_upd`, 0, 1)), 0, 1) AS `need_doc`,
                    SUM(`order`.`base_price_with_vat` * `order`.`quantity`) AS `amount`,                    
                    SUM(`product`.`price_for_buy_with_nds` * `order`.`quantity`) AS `amount_in_price_for_buy`,
                    SUM(`order`.`quantity`) AS `quantity`                    	
                  FROM `cash_emoney_flow_to_invoice`
                  LEFT JOIN `invoice` ON `cash_emoney_flow_to_invoice`.`invoice_id` = `invoice`.`id`
                  LEFT JOIN `order` ON `order`.`invoice_id` = `invoice`.`id`
                  LEFT JOIN `product` ON `order`.`product_id` = `product`.`id`
                  WHERE `cash_emoney_flow_to_invoice`.`invoice_id` = invoiceID 	  
                  GROUP BY `cash_emoney_flow_to_invoice`.`flow_id`
                ) `invoices` ON `olap_flows`.`id` = `invoices`.`flow_id`
                
              SET
            
                `olap_flows`.`has_doc` = IFNULL(`invoices`.`has_doc`, 0),
                `olap_flows`.`need_doc` = IFNULL(`invoices`.`need_doc`, 0),
                `olap_flows`.`invoice_amount` = IFNULL(`invoices`.`amount`, 0),                
				`olap_flows`.`invoice_amount_in_price_for_buy` = IFNULL(`invoices`.`amount_in_price_for_buy`, 0),
                `olap_flows`.`invoice_quantity` = IFNULL(`invoices`.`quantity`, 0)                
              
              WHERE `olap_flows`.`wallet` = "3" AND `olap_flows`.`id` IN (SELECT `flow_id` FROM `cash_emoney_flow_to_invoice` WHERE `invoice_id` = invoiceID);
            
            END');
        $this->execute('
            CREATE TRIGGER `olap_flows_update_invoice`
            AFTER UPDATE ON `invoice`
            FOR EACH ROW
            BEGIN
            
                CALL OlapFlowsRecalculateBankByInvoice(NEW.`id`);
                CALL OlapFlowsRecalculateOrderByInvoice(NEW.`id`);
                CALL OlapFlowsRecalculateEmoneyByInvoice(NEW.`id`);
            
            END');
    }

    private function createTriggersOnProduct()
    {

        $this->execute('DROP PROCEDURE IF EXISTS `OlapFlowsRecalculateBankByProduct`');
        $this->execute('DROP PROCEDURE IF EXISTS `OlapFlowsRecalculateOrderByProduct`');
        $this->execute('DROP PROCEDURE IF EXISTS `OlapFlowsRecalculateEmoneyByProduct`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_update_product`');

        $this->execute('
            CREATE PROCEDURE `OlapFlowsRecalculateBankByProduct`(productID INT)
            BEGIN
            
              UPDATE `olap_flows` 
              LEFT JOIN 
                (SELECT 
                    `cash_bank_flow_to_invoice`.`flow_id` AS `flow_id`,
                    SUM(`product`.`price_for_buy_with_nds` * `order`.`quantity`) AS `amount_in_price_for_buy`
                  FROM `cash_bank_flow_to_invoice`
                  LEFT JOIN `invoice` ON `cash_bank_flow_to_invoice`.`invoice_id` = `invoice`.`id`
                  LEFT JOIN `order` ON `order`.`invoice_id` = `invoice`.`id`
                  LEFT JOIN `product` ON `order`.`product_id` = `product`.`id`
                  WHERE `cash_bank_flow_to_invoice`.`invoice_id` IN (SELECT `invoice_id` FROM `order` WHERE `product_id` = productID)                  	  
                  GROUP BY `cash_bank_flow_to_invoice`.`flow_id`
                ) `invoices` ON `olap_flows`.`id` = `invoices`.`flow_id`
                
              SET
            
                `olap_flows`.`invoice_amount_in_price_for_buy` = IFNULL(`invoices`.`amount_in_price_for_buy`, 0)
              
              WHERE `olap_flows`.`wallet` = "1" AND `olap_flows`.`id` IN (
                SELECT `flow_id` FROM `cash_bank_flow_to_invoice` WHERE `invoice_id` IN (
                  SELECT `invoice_id` FROM `order` WHERE `product_id` = productID
                    
                    ORDER BY `olap_flows`.`id` /* FOR GROUP UPDATE! */
              ));
            
            END');
        $this->execute('
            CREATE PROCEDURE `OlapFlowsRecalculateOrderByProduct`(productID INT)
            BEGIN
            
              UPDATE `olap_flows` 
              LEFT JOIN 
                (SELECT 
                    `cash_order_flow_to_invoice`.`flow_id` AS `flow_id`,
                    SUM(`product`.`price_for_buy_with_nds` * `order`.`quantity`) AS `amount_in_price_for_buy`
                  FROM `cash_order_flow_to_invoice`
                  LEFT JOIN `invoice` ON `cash_order_flow_to_invoice`.`invoice_id` = `invoice`.`id`
                  LEFT JOIN `order` ON `order`.`invoice_id` = `invoice`.`id`
                  LEFT JOIN `product` ON `order`.`product_id` = `product`.`id`	  
                  WHERE `cash_order_flow_to_invoice`.`invoice_id` IN (SELECT `invoice_id` FROM `order` WHERE `product_id` = productID)                  
                  GROUP BY `cash_order_flow_to_invoice`.`flow_id`
                ) `invoices` ON `olap_flows`.`id` = `invoices`.`flow_id`
                
              SET
            
                `olap_flows`.`invoice_amount_in_price_for_buy` = IFNULL(`invoices`.`amount_in_price_for_buy`, 0)
                              
              WHERE `olap_flows`.`wallet` = "2" AND `olap_flows`.`id` IN (
                SELECT `flow_id` FROM `cash_order_flow_to_invoice` WHERE `invoice_id` IN (
                  SELECT `invoice_id` FROM `order` WHERE `product_id` = productID
                  
                    ORDER BY `olap_flows`.`id` /* FOR GROUP UPDATE! */
              ));
            
            END');
        $this->execute('
            CREATE PROCEDURE `OlapFlowsRecalculateEmoneyByProduct`(productID INT)
            BEGIN
            
              UPDATE `olap_flows` 
              LEFT JOIN 
                (SELECT 
                    `cash_emoney_flow_to_invoice`.`flow_id` AS `flow_id`,
                    SUM(`product`.`price_for_buy_with_nds` * `order`.`quantity`) AS `amount_in_price_for_buy`
                  FROM `cash_emoney_flow_to_invoice`
                  LEFT JOIN `invoice` ON `cash_emoney_flow_to_invoice`.`invoice_id` = `invoice`.`id`
                  LEFT JOIN `order` ON `order`.`invoice_id` = `invoice`.`id`
                  LEFT JOIN `product` ON `order`.`product_id` = `product`.`id`
                  WHERE `cash_emoney_flow_to_invoice`.`invoice_id` IN (SELECT `invoice_id` FROM `order` WHERE `product_id` = productID)                  	  
                  GROUP BY `cash_emoney_flow_to_invoice`.`flow_id`
                ) `invoices` ON `olap_flows`.`id` = `invoices`.`flow_id`
                
              SET
            
                `olap_flows`.`invoice_amount_in_price_for_buy` = IFNULL(`invoices`.`amount_in_price_for_buy`, 0)
              
              WHERE `olap_flows`.`wallet` = "3" AND `olap_flows`.`id` IN (
                SELECT `flow_id` FROM `cash_emoney_flow_to_invoice` WHERE `invoice_id` IN (
                  SELECT `invoice_id` FROM `order` WHERE `product_id` = productID
                  
                    ORDER BY `olap_flows`.`id` /* FOR GROUP UPDATE! */
              ));
            
            END');
        $this->execute('
            CREATE TRIGGER `olap_flows_update_product`
            AFTER UPDATE ON `product`
            FOR EACH ROW
            BEGIN
            
              IF (NEW.`price_for_buy_with_nds` <> OLD.`price_for_buy_with_nds`) THEN
            
                CALL OlapFlowsRecalculateBankByProduct(NEW.`id`);
                CALL OlapFlowsRecalculateOrderByProduct(NEW.`id`);
                CALL OlapFlowsRecalculateEmoneyByProduct(NEW.`id`);
              
              END IF;	
              
            END');
    }
}
