<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%ofd_kkt}}`.
 */
class m200520_174033_create_ofd_kkt_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%ofd_kkt}}', [
            'id' => $this->primaryKey(),
            'uid' => $this->string()->notNull()->comment('Идентификатор в базе ОФД'),
            'store_uid' => $this->string()->notNull()->comment('Идентификатор магазина в базе ОФД'),
            'company_id' => $this->integer()->notNull()->comment('ID компании'),
            'ofd_id' => $this->integer()->notNull()->comment('ID ОФД'),
            'factory_number' => $this->string()->comment('Заводской номер'),
            'registration_number' => $this->string()->comment('Регистрационный номер'),
            'registration_status' => $this->string()->comment('Статус регистрации в ФНС'),
            'model' => $this->string()->comment('Модель'),
            'format_fd' => $this->string()->comment('Формат ФД'),
            'name' => $this->string()->notNull()->comment('Название кассы'),
            'address' => $this->string()->comment('адрес (место) расчетов'),
            'place' => $this->string()->comment('место расчетов'),
            'activated_at' => $this->datetime()->comment('Дата и время активации'),
            'created_at' => $this->integer()->comment(''),
        ]);

        $this->addForeignKey('FK_ofd_kkt_company', '{{%ofd_kkt}}', 'company_id', '{{%company}}', 'id');
        $this->addForeignKey('FK_ofd_kkt_ofd', '{{%ofd_kkt}}', 'ofd_id', '{{%ofd}}', 'id');

        $this->createIndex('kkt', '{{%ofd_kkt}}', [
            'company_id',
            'ofd_id',
            'uid',
        ], true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%ofd_kkt}}');
    }
}
