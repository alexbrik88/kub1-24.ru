<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191001_111930_create_table_product_to_supplier extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%product_to_supplier}}', [
            'product_id' => $this->integer()->notNull(),
            'supplier_id' => $this->integer()->notNull(),
            'price' => $this->bigInteger(20)->notNull(),
            'article' => $this->string(32)->notNull(),
        ], $this->tableOptions);

        $this->addPrimaryKey('PRIMARY_KEY', '{{%product_to_supplier}}', ['product_id', 'supplier_id']);
        $this->addForeignKey('fk_product_to_supplier_product', '{{%product_to_supplier}}', 'product_id', '{{%product}}', 'id', 'CASCADE');
        $this->addForeignKey('fk_product_to_supplier_contractor', '{{%product_to_supplier}}', 'supplier_id', '{{%contractor}}', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable('{{%product_to_supplier}}');
    }
}
