<?php

use console\components\db\Migration;
use yii\db\Schema;

class m161129_114758_insert_invoiceExpenditureItem extends Migration
{
    public function safeUp()
    {
        $this->batchInsert('invoice_expenditure_item', ['name', 'sort'], [
            ['НДС', 100],
            ['Налог на прибыль', 100],
            ['АйТи', 100],
            ['Программы', 100],
        ]);
    }
    
    public function safeDown()
    {
        $this->delete('invoice_expenditure_item', ['name' => [
            'НДС',
            'Налог на прибыль',
            'АйТи',
            'Программы',
        ]]);
    }
}


