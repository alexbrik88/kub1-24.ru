<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191203_090753_sale_point_employee extends Migration
{
    public $table = 'sale_point_employee';

    public function safeUp()
    {
        $salePointTable = 'sale_point';
        $employeeTable = 'employee';

        $this->createTable($this->table, [
            'sale_point_id' => $this->integer()->notNull(),
            'employee_id' => $this->integer()->notNull()
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', $this->table, ['sale_point_id', 'employee_id']);
        $this->addForeignKey("FK_{$this->table}_{$salePointTable}", $this->table, 'sale_point_id', $salePointTable, 'id', 'CASCADE');
        $this->addForeignKey("FK_{$this->table}_{$employeeTable}", $this->table, 'employee_id', $employeeTable, 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $salePointTable = 'sale_point';
        $employeeTable = 'employee';

        $this->dropForeignKey("FK_{$this->table}_{$salePointTable}", $this->table);
        $this->dropForeignKey("FK_{$this->table}_{$employeeTable}", $this->table);

        $this->dropTable($this->table);
    }
}
