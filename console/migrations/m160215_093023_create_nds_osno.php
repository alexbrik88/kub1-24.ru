<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160215_093023_create_nds_osno extends Migration
{
    public $tNdsOsno = 'nds_osno';

    public function safeUp()
    {
        $this->createTable($this->tNdsOsno, [
            'id' => $this->primaryKey(11),
            'name' => $this->text(),
        ]);

        $this->batchInsert($this->tNdsOsno, ['id', 'name'], [
            [1, 'с НДС'],
            [2, 'без НДС'],
        ]);
    }
    
    public function safeDown()
    {
        $this->dropTable($this->tNdsOsno);
    }
}


