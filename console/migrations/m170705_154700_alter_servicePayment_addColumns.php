<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170705_154700_alter_servicePayment_addColumns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{service_payment}}', 'price', $this->string(45) . ' AFTER [[subscribe_id]]');
        $this->addColumn('{{service_payment}}', 'discount', $this->integer()->defaultValue(0) . ' AFTER [[price]]');

        $this->execute("
            UPDATE {{service_payment}}
            SET {{service_payment}}.[[price]] = {{service_payment}}.[[sum]];
        ");
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%service_payment}}', 'price');
        $this->dropColumn('{{%service_payment}}', 'discount');
    }
}
