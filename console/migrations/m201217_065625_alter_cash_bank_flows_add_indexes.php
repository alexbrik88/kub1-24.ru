<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201217_065625_alter_cash_bank_flows_add_indexes extends Migration
{
    const TABLE = 'cash_bank_flows';

    public function safeUp()
    {
        $this->dropIndex('is_taxrobot_manual_entry', self::TABLE);
        $this->dropIndex('recognition_date', self::TABLE);
        $this->dropIndex('date', self::TABLE);
        $this->dropIndex('rs', self::TABLE);
        $this->dropIndex('cash_bank_flows_is_taxable', self::TABLE);

        $this->createIndex('date', self::TABLE, ['company_id', 'date']);
        $this->createIndex('recognition_date', self::TABLE, ['company_id', 'recognition_date']);
        $this->createIndex('rs', self::TABLE, ['company_id', 'rs', 'date']);
    }

    public function safeDown()
    {
        $this->dropIndex('date', self::TABLE);
        $this->dropIndex('recognition_date', self::TABLE);
        $this->dropIndex('rs', self::TABLE);

        $this->createIndex('is_taxrobot_manual_entry', self::TABLE, 'is_taxrobot_manual_entry');
        $this->createIndex('recognition_date', self::TABLE, 'recognition_date');
        $this->createIndex('date', self::TABLE, 'date');
        $this->createIndex('rs', self::TABLE, 'rs');
        $this->createIndex('cash_bank_flows_is_taxable', self::TABLE, 'is_taxable');
    }
}
