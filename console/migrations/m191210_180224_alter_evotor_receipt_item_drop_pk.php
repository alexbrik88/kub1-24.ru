<?php

use console\components\db\Migration;
use common\models\evotor\ReceiptItem;

class m191210_180224_alter_evotor_receipt_item_drop_pk extends Migration
{
    public function safeUp()
    {
        $this->dropForeignKey('evotor_receipt_item_product_id', '{{evotor_receipt_item}}');
        $this->dropForeignKey('evotor_receipt_item_receipt_id', '{{evotor_receipt_item}}');
        $this->dropPrimaryKey('evotor_receipt_item_pk', '{{evotor_receipt_item}}');
        $this->addColumn('{{evotor_receipt_item}}', 'id', $this->primaryKey(11)->unsigned()->notNull()->first());
        $this->addForeignKey('evotor_receipt_item_product_id', '{{evotor_receipt_item}}', 'product_id',
            '{{evotor_product}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('evotor_receipt_item_receipt_id', '{{evotor_receipt_item}}', 'receipt_id',
            '{{evotor_receipt}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        ReceiptItem::deleteAll();
        $this->dropColumn('{{evotor_receipt_item}}', 'id');
        $this->dropForeignKey('evotor_receipt_item_product_id', '{{evotor_receipt_item}}');
        $this->dropForeignKey('evotor_receipt_item_receipt_id', '{{evotor_receipt_item}}');
        $this->addPrimaryKey('evotor_receipt_item_pk', '{{evotor_receipt_item}}', ['receipt_id', 'product_id']);
        $this->addForeignKey('evotor_receipt_item_product_id', '{{evotor_receipt_item}}', 'product_id',
            '{{evotor_product}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('evotor_receipt_item_receipt_id', '{{evotor_receipt_item}}', 'receipt_id',
            '{{evotor_receipt}}', 'id', 'CASCADE', 'CASCADE');
    }
}
