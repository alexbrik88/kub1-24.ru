<?php

use console\components\db\Migration;
use yii\db\Schema;
use common\models\company\CompanyNotification;
use common\models\notification\Notification;
use common\models\Company;

class m170823_124807_update_company_notification extends Migration
{
    public function safeUp()
    {
        /* @var $notification Notification */
        foreach (Notification::find()->all() as $notification) {
            /* @var $company Company */
            $company = Company::findOne(4703);
            if ($company) {
                if (!CompanyNotification::find()->andWhere(['and',
                    ['company_id' => $company->id],
                    ['notification_id' => $notification->id],
                ])->exists()
                ) {
                    $companyNotification = new CompanyNotification();
                    $companyNotification->company_id = $company->id;
                    $companyNotification->notification_id = $notification->id;
                    $companyNotification->status = 0;
                    $companyNotification->save();
                }
            }
        }
    }

    public function safeDown()
    {

    }
}


