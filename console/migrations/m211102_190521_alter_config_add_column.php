<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211102_190521_alter_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_config', 'finance_model_other_income', $this->tinyInteger(1)->notNull()->defaultValue(0));
        $this->addColumn('user_config', 'finance_model_other_expense', $this->tinyInteger(1)->notNull()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('user_config', 'finance_model_other_income');
        $this->dropColumn('user_config', 'finance_model_other_expense');
    }
}
