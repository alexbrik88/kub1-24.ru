<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190416_120850_insert_invoice_expenditure_item extends Migration
{
    public $name = 'Фиксированный платеж в ПФР';
    public $kbk = '18210202140061110160';

    public function safeUp()
    {
        $id = Yii::$app->db->createCommand('
            SELECT MAX([[id]])
            FROM {{invoice_expenditure_item}}
            WHERE [[id]] < 100
        ')->queryScalar() + 1;

        $this->insert('invoice_expenditure_item', [
            'id' => $id,
            'group_id' => 1,
            'name' => $this->name,
        ]);

        $this->insert('tax_kbk', [
            'id' => $this->kbk,
            'expenditure_item_id' => $id,
        ]);
    }

    public function safeDown()
    {
        $id = Yii::$app->db->createCommand('
            SELECT [[id]]
            FROM {{invoice_expenditure_item}}
            WHERE [[name]] = :name
        ', [':name' => $this->name])->queryScalar();

        $this->delete('tax_kbk', ['expenditure_item_id' => $id]);

        $this->delete('invoice_expenditure_item', ['id' => $id]);
    }
}
