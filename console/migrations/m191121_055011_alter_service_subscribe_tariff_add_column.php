<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191121_055011_alter_service_subscribe_tariff_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%service_subscribe_tariff}}', 'invoice_limit', $this->integer()->defaultValue(null));

        $this->update('{{%service_subscribe_tariff}}', [
            'invoice_limit' => 10,
        ], [
            'id' => 4,
        ]);
    }

    public function safeDown()
    {
        $this->dropColumn('{{%service_subscribe_tariff}}', 'invoice_limit');
    }
}
