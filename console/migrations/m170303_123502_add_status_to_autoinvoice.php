<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170303_123502_add_status_to_autoinvoice extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%autoinvoice}}','status',$this->integer()->defaultValue(1));
        $this->alterColumn('{{%autoinvoice}}','production_type',$this->string(255));
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%autoinvoice}}','status');
    }
}


