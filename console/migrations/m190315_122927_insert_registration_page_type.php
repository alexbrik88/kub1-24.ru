<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190315_122927_insert_registration_page_type extends Migration
{
    public $tableName = '{{%registration_page_type}}';

    public function safeUp()
    {
        $this->insert($this->tableName, [
            'id' => 19,
            'name' => 'Банк Открытие. Бухгалтерия',
        ]);
    }

    public function safeDown()
    {
        $this->delete($this->tableName, ['id' => 19]);
    }
}
