<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190214_122022_proxy extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('proxy_status', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ], $tableOptions);

        $this->batchInsert('proxy_status', ['id', 'name',], [
            [1, 'Создана',],
            [2, 'Распечатана',],
            [3, 'Передана',],
            [4, 'Получена',],
            [5, 'Отменена']
        ]);

        $this->createTable('proxy', [
            'id' => Schema::TYPE_PK,
            'uid' => Schema::TYPE_STRING . '(5) DEFAULT NULL',
            'type' => Schema::TYPE_INTEGER . '(1) NOT NULL COMMENT "2 - out, 1 -in"',
            'invoice_id' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'document_author_id' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'status_out_id' => Schema::TYPE_INTEGER . '(11) DEFAULT NULL',
            'status_out_author_id' => Schema::TYPE_INTEGER . '(11) DEFAULT NULL',
            'document_date' => Schema::TYPE_DATE . ' NOT NULL',
            'document_number' => Schema::TYPE_INTEGER . '(11) DEFAULT NULL',
            'document_additional_number' => Schema::TYPE_STRING . '(45) DEFAULT NULL',
            'created_at' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'status_out_updated_at' => Schema::TYPE_INTEGER . '(11) DEFAULT NULL',
            'object_guid' => Schema::TYPE_STRING . '(36) NOT NULL DEFAULT "" ',
            'basis_name' => Schema::TYPE_STRING . '(255) DEFAULT NULL',
            'basis_document_number' => Schema::TYPE_STRING . '(50) DEFAULT NULL',
            'basis_document_date' => Schema::TYPE_DATE . ' DEFAULT NULL',
            'basis_document_type_id' => Schema::TYPE_INTEGER . '(11) DEFAULT NULL',
            'consignor_id' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'consignee_id' => Schema::TYPE_INTEGER . '(11) DEFAULT NULL',
            'ordinal_document_number' => Schema::TYPE_INTEGER . '(11) DEFAULT NULL',
            'is_original' => Schema::TYPE_SMALLINT . '(1) DEFAULT 0',
            'is_original_updated_at' => Schema::TYPE_INTEGER . '(11) DEFAULT NULL',
            'orders_sum' => Schema::TYPE_INTEGER . '(11) DEFAULT 0',
            'signed_by_employee_id' => Schema::TYPE_INTEGER . '(11) DEFAULT NULL',
            'signed_by_name' => Schema::TYPE_STRING . '(50) DEFAULT NULL',
            'sign_document_type_id' => Schema::TYPE_INTEGER . '(11) DEFAULT NULL',
            'sign_document_number' => Schema::TYPE_STRING . '(50) DEFAULT NULL',
            'sign_document_date' => Schema::TYPE_DATE . ' DEFAULT NULL',
            'signature_id' => Schema::TYPE_INTEGER . '(11) DEFAULT NULL',
            'add_stamp' => Schema::TYPE_SMALLINT . '(1) DEFAULT 0',
            'has_file' => Schema::TYPE_SMALLINT . '(1) DEFAULT 0',

        ], $tableOptions);

        $this->createIndex('uid_idx', '{{%proxy}}', 'uid');
        $this->createIndex('has_file', '{{%proxy}}', 'has_file');

        $this->addForeignKey('FK_proxy_to_invoice', '{{%proxy}}', 'invoice_id', 'invoice', 'id');
        $this->addForeignKey('FK_proxy_to_proxy_status', '{{%proxy}}', 'status_out_id', 'proxy_status', 'id');
        $this->addForeignKey('FK_proxy_to_document_author', '{{%proxy}}', 'document_author_id', 'employee', 'id');
        $this->addForeignKey('FK_proxy_to_status_out_author', '{{%proxy}}', 'status_out_author_id', 'employee', 'id');

        $this->addForeignKey('FK_proxy_sign_employee', '{{%proxy}}', 'signed_by_employee_id', '{{%employee}}', 'id');
        $this->addForeignKey('FK_proxy_sign_documentType', '{{%proxy}}', 'sign_document_type_id', '{{%document_type}}', 'id');
        $this->addForeignKey('FK_proxy_employeeSignature', '{{%proxy}}', 'signature_id', '{{%employee_signature}}', 'id');
        $this->addForeignKey('FK_proxy_agreementType', '{{%proxy}}', 'basis_document_type_id', '{{%agreement_type}}', 'id');
    }

    public function down()
    {
        $this->dropTable('proxy');
        $this->dropTable('proxy_status');
    }
}
