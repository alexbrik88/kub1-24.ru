<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180727_062442_insert_payment_order_status extends Migration
{
    public function safeUp()
    {
        $this->insert('payment_order_status', [
            'id' => 4,
            'name' => 'Загружено в банк',
        ]);
    }

    public function safeDown()
    {
        $this->delete('payment_order_status', 'id = 4');
    }
}
