<?php

use yii\db\Schema;
use yii\db\Migration;

class m150417_093044_alter_product_dropColumn_priceWithoutNds extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('product', 'price_for_buy');
        $this->dropColumn('product', 'price_for_sell');
    }
    
    public function safeDown()
    {
        $this->addColumn('product', 'price_for_buy', Schema::TYPE_STRING . '(45) COMMENT "цена покупки без НДС" AFTER excise');
        $this->addColumn('product', 'price_for_sell', Schema::TYPE_STRING . '(45) COMMENT "цена покупки без НДС" AFTER price_for_buy_with_nds');
    }
}
