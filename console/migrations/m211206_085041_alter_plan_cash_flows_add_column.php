<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211206_085041_alter_plan_cash_flows_add_column extends Migration
{
    public function safeUp()
    {
        $table = 'plan_cash_flows';
        $this->addColumn($table, 'by_related_document', $this->tinyInteger()->defaultValue(0)->after('invoice_id'));
    }

    public function safeDown()
    {
        $table = 'plan_cash_flows';
        $this->dropColumn($table, 'by_related_document');
    }
}
