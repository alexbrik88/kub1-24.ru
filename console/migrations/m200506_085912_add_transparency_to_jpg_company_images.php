<?php

use common\models\Company;
use common\models\company\DetailsFile;
use common\models\company\DetailsFileType;
use common\models\company\DetailsFileStatus;
use console\components\db\Migration;
use yii\db\Schema;

class m200506_085912_add_transparency_to_jpg_company_images extends Migration
{
    public function safeUp()
    {
        Yii::$app->db->createCommand('SET session wait_timeout=3600')->execute();
        Yii::$app->db->createCommand('SET session interactive_timeout=3600')->execute();

        $filesIds = DetailsFile::find()->where([
            'status_id' => DetailsFileStatus::STATUS_ACTIVE,
            'type_id' => [
                DetailsFileType::TYPE_PRINT,
                DetailsFileType::TYPE_CHIEF_SIGNATURE,
                DetailsFileType::TYPE_CHIEF_ACCOUNTANT_SIGNATURE]
        ])->andWhere(['not', ['ext' => 'png']])->select('id')->asArray()->all();

        $cntFiles = 0;
        foreach ($filesIds as $id) {

            $model = DetailsFile::findOne($id);
            if ($model) {

                $path = $model->getUploadPath();
                $origFile = $path . DIRECTORY_SEPARATOR . $model->name;
                $newName = preg_replace('/\..+$/', '.png', $model->name);
                $newFile = $path . DIRECTORY_SEPARATOR . $newName;

                $im = new \Imagick($origFile);
                $im->transparentPaintImage($im->getImageBackgroundColor(), 0, 2500, false);
                $im->setImageFormat('png');
                $im->writeImage($newFile);
                $im->destroy();

                if (file_exists($newFile)) {
                    $model->updateAttributes([
                        'name' => $newName,
                        'ext' => 'png'
                    ]);

                    @unlink($origFile);

                    $cntFiles++;
                }
            }
        }

        echo 'Images: ' . $cntFiles ."\n";
    }

    public function safeDown()
    {
        // no down;
    }
}
