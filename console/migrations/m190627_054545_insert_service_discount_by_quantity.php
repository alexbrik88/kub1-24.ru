<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190627_054545_insert_service_discount_by_quantity extends Migration
{
    public static $items = [
        16 => [
            2 => 25.0,
            3 => 33.3333,
            4 => 34,
        ],
        17 => [
            2 => 25.0,
            3 => 33.3333,
            4 => 38.8888,
        ],
    ];

    public function safeUp()
    {
        foreach (self::$items as $id => $data) {
            foreach ($data as $key => $value) {
                $this->insert('service_discount_by_quantity', [
                    'tariff_id' => $id,
                    'quantity' => $key,
                    'percent' => $value,
                ]);
            }
        }
    }

    public function safeDown()
    {
        $this->delete('service_discount_by_quantity', ['tariff_id' => array_keys(self::$items)]);
    }
}


