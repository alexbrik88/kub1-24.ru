<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160212_112756_alter_page_type_id_in_prompt extends Migration
{
    public $tPrompt = 'prompt';

    public function safeUp()
    {
        $this->update($this->tPrompt, ['page_type_id' => 3], ['page_type_id' => 1]);
        $this->update($this->tPrompt, ['page_type_id' => 4], ['page_type_id' => 2]);
        $this->update($this->tPrompt, ['page_type_id' => 5], ['page_type_id' => 3]);
        $this->update($this->tPrompt, ['page_type_id' => 10], ['page_type_id' => 4]);
        $this->update($this->tPrompt, ['page_type_id' => 18], ['page_type_id' => 5]);
        $this->update($this->tPrompt, ['page_type_id' => 19], ['page_type_id' => 6]);
        $this->update($this->tPrompt, ['page_type_id' => 20], ['page_type_id' => 7]);
    }
    
    public function safeDown()
    {
        $this->update($this->tPrompt, ['page_type_id' => 1], ['page_type_id' => 3]);
        $this->update($this->tPrompt, ['page_type_id' => 2], ['page_type_id' => 4]);
        $this->update($this->tPrompt, ['page_type_id' => 3], ['page_type_id' => 5]);
        $this->update($this->tPrompt, ['page_type_id' => 4], ['page_type_id' => 10]);
        $this->update($this->tPrompt, ['page_type_id' => 5], ['page_type_id' => 18]);
        $this->update($this->tPrompt, ['page_type_id' => 6], ['page_type_id' => 19]);
        $this->update($this->tPrompt, ['page_type_id' => 7], ['page_type_id' => 20]);
    }
}


