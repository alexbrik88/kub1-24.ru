<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211203_201706_insert_log_event extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%log_event}}', [
            'id' => 7,
            'name' => 'проект изменен',
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%log_event}}', [
            'id' => 7,
        ]);
    }
}
