<?php

use console\components\db\Migration;
use yii\db\Schema;

class m151221_120008_create_servicePaymentPromoCodeGroup extends Migration
{
    public $tPromo = 'service_payment_promo_code';
    public $tPromoGroup = 'service_payment_promo_code_group';
    public $tCompany = 'company';

    public function safeUp()
    {
        $this->createTable($this->tPromoGroup, [
            'id' => $this->primaryKey(),
            'name' => $this->string(45)->notNull(),
        ], $this->tableOptions);

        $this->batchInsert($this->tPromoGroup, ['id', 'name'], [
            [1, 'Личный'],
            [2, 'Групповой'],
        ]);

        $this->addColumn($this->tPromo, 'group_id', $this->integer()->notNull());
        $this->update($this->tPromo, [
            'group_id' => 2, // group
        ]);
        $this->addForeignKey('fk_service_payment_promo_code_to_group', $this->tPromo, 'group_id', $this->tPromoGroup, 'id');

        $this->addColumn($this->tPromo, 'company_id', $this->integer()->defaultValue(null));
        $this->addForeignKey('fk_service_payment_promo_code_to_company', $this->tPromo, 'company_id', $this->tCompany, 'id');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('fk_service_payment_promo_code_to_group', $this->tPromo);
        $this->dropColumn($this->tPromo, 'group_id');

        $this->dropForeignKey('fk_service_payment_promo_code_to_company', $this->tPromo);
        $this->dropColumn($this->tPromo, 'company_id');

        $this->dropTable($this->tPromoGroup);
    }
}


