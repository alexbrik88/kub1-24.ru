<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200818_081207_insertEmployeeRole extends Migration
{
    public function safeUp()
    {
        $this->insert('employee_role', [
            'id' => 16,
            'name' => 'Менеджер по работе с партнерами',
            'sort' => 99000
        ]);
    }

    public function safeDown()
    {
        $this->delete('employee_role', ['id' => 16]);
    }
}
