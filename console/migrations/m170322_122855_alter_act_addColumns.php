<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170322_122855_alter_act_addColumns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%act}}', 'basis_document_name', $this->string(255)->defaultValue(null));
        $this->addColumn('{{%act}}', 'basis_document_number', $this->string(50)->defaultValue(null));
        $this->addColumn('{{%act}}', 'basis_document_date', $this->date()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%act}}', 'basis_document_name');
        $this->dropColumn('{{%act}}', 'basis_document_number');
        $this->dropColumn('{{%act}}', 'basis_document_date');
    }
}
