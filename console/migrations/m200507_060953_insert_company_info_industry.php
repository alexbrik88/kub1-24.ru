<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200507_060953_insert_company_info_industry extends Migration
{
    public function safeUp()
    {
        $this->update('{{%company_info_industry}}', [
            'name' => 'Арендный бизнес',
        ], [
            'id' => 1,
        ]);

        $this->batchInsert('{{%company_info_industry}}', [
            'id',
            'name',
            'need_shop',
            'need_site',
            'need_storage',
        ], [
            [2, 'Оказание услуг', 0, 0, 0],
            [3, 'Оптовая торговля', 0, 0, 0],
            [4, 'Ритейл (Магазины и E-commerce)', 1, 0, 1],
            [5, 'Только E-commerce (интернет)', 0, 1, 1],
            [6, 'Только магазин/магазины', 1, 0, 1],
            [7, 'Производство', 1, 0, 1],
            [8, 'Другое', 0, 0, 0],
        ]);

        $this->update('{{%company}}', [
            'info_industry_id' => 2,
        ], [
            'info_industry_id' => 1,
        ]);
    }

    public function safeDown()
    {
        $this->update('{{%company}}', [
            'info_industry_id' => 1,
        ], [
            'info_industry_id' => 2,
        ]);

        $this->delete('{{%company_info_industry}}', ['>', 'id', 1]);

        $this->update('{{%company_info_industry}}', [
            'name' => 'Услуги',
        ], [
            'id' => 1,
        ]);
    }
}
