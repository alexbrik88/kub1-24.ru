<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211205_132340_alter_invoice_add_column extends Migration
{
    public function safeUp()
    {
        $table = 'invoice';
        $this->addColumn($table, 'payment_limit_rule', $this->tinyInteger()->notNull()->defaultValue(1)->after('document_additional_number'));
        $this->addColumn($table, 'related_document_payment_limit_date', $this->date()->defaultValue(null)->after('payment_limit_date'));
        $this->addColumn($table, 'related_document_payment_limit_percent', $this->tinyInteger()->defaultValue(0)->after('related_document_payment_limit_date'));
        $this->addColumn($table, 'payment_limit_percent', $this->tinyInteger()->defaultValue(100)->after('related_document_payment_limit_date'));
        $this->addColumn($table, 'related_document_payment_limit_days', $this->integer()->unsigned()->after('related_document_payment_limit_date'));
        $this->addColumn($table, 'payment_limit_days', $this->integer()->unsigned()->after('related_document_payment_limit_date'));
    }

    public function safeDown()
    {
        $table = 'invoice';
        $this->dropColumn($table, 'payment_limit_rule');
        $this->dropColumn($table, 'related_document_payment_limit_date');
        $this->dropColumn($table, 'related_document_payment_limit_percent');
        $this->dropColumn($table, 'payment_limit_percent');
        $this->dropColumn($table, 'related_document_payment_limit_days');
        $this->dropColumn($table, 'payment_limit_days');
    }
}
