<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201105_085859_alter_project_estimate_append_addition_number extends Migration
{
    public $tableName = 'project_estimate';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'addition_number', $this->string()->after('number'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'addition_number');
    }
}
