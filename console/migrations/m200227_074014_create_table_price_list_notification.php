<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200227_074014_create_table_price_list_notification extends Migration
{
    public $table = '{{%price_list_notification}}';

    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'uid' => $this->string(16)->notNull(),
            'price_list_id' => $this->integer()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'contractor_id' => $this->integer(),
            'contractor_email' => $this->string(64),
            'is_viewed' => $this->boolean()->notNull()->defaultValue(false),
            'is_notification_viewed' => $this->boolean()->notNull()->defaultValue(false),
            'created_at' => $this->integer()->unsigned(),
            'viewed_at' => $this->integer()->unsigned(),
            'notification_viewed_at' => $this->integer()->unsigned(),
        ]);

        $this->createIndex('IDX_company_id_uid', $this->table, ['company_id', 'uid']);
        $this->addForeignKey('FK_price_list_notification_price_list_id', $this->table, 'price_list_id', 'price_list', 'id', 'CASCADE');
        $this->addForeignKey('FK_price_list_notification_company_id', $this->table, 'company_id', 'company', 'id', 'CASCADE');
        $this->addForeignKey('FK_price_list_notification_contractor_id', $this->table, 'contractor_id', 'contractor', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_price_list_notification_price_list_id', $this->table);
        $this->dropForeignKey('FK_price_list_notification_company_id', $this->table);
        $this->dropForeignKey('FK_price_list_notification_contractor_id', $this->table);
        $this->dropTable($this->table);
    }
}
