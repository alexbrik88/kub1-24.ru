<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210506_140111_alter_card_operation_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('card_operation', 'is_accounting', Schema::TYPE_BOOLEAN . ' NOT NULL DEFAULT 0 COMMENT \'Учёт в бухгалтерии: 0 - нет, 1 - да\'');
    }

    public function safeDown()
    {
        $this->dropColumn('card_operation', 'is_accounting');
    }
}
