<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191127_110845_change_tariff_group_name extends Migration
{
    public function safeUp()
    {
        $this->execute('
            UPDATE `service_subscribe_tariff_group`
                SET `name` = "Бизнес аналитика"
                WHERE id = 5;
        ');
    }

    public function safeDown()
    {
        $this->execute('
            UPDATE `service_subscribe_tariff_group`
                SET `name` = "Аналитика бизнеса"
                WHERE id = 5;
        ');
    }
}