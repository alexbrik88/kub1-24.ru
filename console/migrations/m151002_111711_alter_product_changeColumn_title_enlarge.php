<?php

use yii\db\Schema;
use yii\db\Migration;

class m151002_111711_alter_product_changeColumn_title_enlarge extends Migration
{
    public $tProduct = 'product';

    public function safeUp()
    {
        $this->alterColumn($this->tProduct, 'title', $this->string(200)->notNull());
    }
    
    public function safeDown()
    {
        $this->alterColumn($this->tProduct, 'title', $this->string(100)->notNull());
    }
}
