<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210621_114144_alter_crm_client_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%crm_client}}', 'client_type_id', $this->bigInteger()->after('client_status_id'));

        $this->createIndex('ck_client_type_id', '{{%crm_client}}', 'client_type_id');
        $this->addForeignKey(
            'fk_crm_client__crm_client_type_client_type_id',
            '{{%crm_client}}',
            'client_type_id',
            '{{%crm_client_type}}',
            'client_type_id',
            'SET NULL',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_crm_client__crm_client_type_client_type_id', '{{%crm_client}}');
        $this->dropIndex('ck_client_type_id', '{{%crm_client}}');

        $this->dropColumn('{{%crm_client}}', 'client_type_id');
    }
}
