<?php

use yii\db\Migration;

/**
 * Handles the creation of table `fixed_payments_ip`.
 */
class m190404_111836_create_fixed_payments_ip_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('fixed_payments_ip', [
            'year' => $this->primaryKey(),
            'pfr' => $this->integer(),
            'oms' => $this->integer(),
        ]);

        $this->batchInsert('fixed_payments_ip', ['year', 'pfr', 'oms'], [
            [2018, 2654500, 584000],
            [2019, 2935400, 688400],
            [2020, 3244800, 842600],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('fixed_payments_ip');
    }
}
