<?php

use yii\db\Migration;

/**
 * Handles the creation of table `discount`.
 */
class m171124_170741_create_discount_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('discount', [
            'id' => $this->primaryKey(),
            'type_id' => $this->integer()->defaultValue(null),
            'company_id' => $this->integer()->defaultValue(null),
            'tariff_id' => "SET('1','2','3') NOT NULL DEFAULT '1,2,3'",
            'value' => $this->decimal(7, 4)->notNull(),
            'is_known' => $this->boolean()->defaultValue(false),
            'is_active' => $this->boolean()->defaultValue(true),
            'active_from' => $this->integer()->notNull(),
            'active_to' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('FK_discount_discountType', 'discount', 'type_id', 'discount_type', 'id', 'CASCADE');
        $this->addForeignKey('FK_discount_company', 'discount', 'company_id', 'company', 'id', 'CASCADE');
        $this->createIndex('I_active_from', 'discount', 'active_from');
        $this->createIndex('I_active_to', 'discount', 'active_to');
        $this->createIndex('I_is_active', 'discount', 'is_active');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('discount');
    }
}
