<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191203_085433_create_sale_point extends Migration
{
    public $table = 'sale_point';

    public function safeUp()
    {
        $companyTable = 'company';
        $typeTable = 'sale_point_type';
        $onlinePaymentTable = 'online_payment_type';
        $storeTable = 'store';

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'type_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull()->defaultValue(''),
            'city' => $this->string(),
            'address' => $this->string(),
            'site' => $this->string(),
            'store_id' => $this->integer(),
            'online_payment_type_id' => $this->integer(),
            'sort' => $this->tinyInteger()->notNull()->defaultValue(1)
        ], $tableOptions);

        $this->addForeignKey("FK_{$this->table}_{$companyTable}", $this->table, 'company_id', $companyTable, 'id', 'CASCADE');
        $this->addForeignKey("FK_{$this->table}_{$typeTable}", $this->table, 'type_id', $typeTable, 'id', 'CASCADE');
        $this->addForeignKey("FK_{$this->table}_{$onlinePaymentTable}", $this->table, 'online_payment_type_id', $onlinePaymentTable, 'id', 'CASCADE');
        $this->addForeignKey("FK_{$this->table}_{$storeTable}", $this->table, 'store_id', $storeTable, 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $companyTable = 'company';
        $typeTable = 'sale_point_type';
        $onlinePaymentTable = 'online_payment_type';
        $storeTable = 'store';

        $this->dropForeignKey("FK_{$this->table}_{$storeTable}", $this->table);
        $this->dropForeignKey("FK_{$this->table}_{$onlinePaymentTable}", $this->table);
        $this->dropForeignKey("FK_{$this->table}_{$typeTable}", $this->table);
        $this->dropForeignKey("FK_{$this->table}_{$companyTable}", $this->table);

        $this->dropTable($this->table);
    }
}
