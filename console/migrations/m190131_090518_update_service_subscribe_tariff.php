<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190131_090518_update_service_subscribe_tariff extends Migration
{
    public function safeUp()
    {
        $this->update('service_subscribe_tariff', ['price' => 2000], ['id' => 11]);
        $this->update('service_subscribe_tariff', ['price' => 4500], ['id' => 12]);
    }

    public function safeDown()
    {
        $this->update('service_subscribe_tariff', ['price' => 6400], ['id' => 11]);
        $this->update('service_subscribe_tariff', ['price' => 14400], ['id' => 12]);
    }
}
