<?php

use common\components\date\DateHelper;
use console\components\db\Migration;
use yii\db\Query;

class m200918_094136_init_product_store_daily extends Migration
{
    public function safeUp()
    {
        $this->truncateTable('product_store_daily');
        $this->execute(<<<SQL
            INSERT INTO `product_store_daily` (`date`,
                                                `company_id`,
                                                `product_id`,
                                                `store_id`,
                                                `initial_quantity`,
                                                `quantity`,
                                                `irreducible_quantity`,
                                                `created_at`,
                                                `updated_at`)
            SELECT FROM_UNIXTIME(`updated_at`, '%Y-%m-%d'),
                `store`.`company_id`,
                `product_store`.`product_id`,
                `product_store`.`store_id`,
                `product_store`.`initial_quantity`,
                `product_store`.`quantity`,
                `product_store`.`irreducible_quantity`,
                `product_store`.`created_at`,
                `product_store`.`updated_at`
            FROM `product_store`
            LEFT JOIN `store` ON `store`.`id` = `product_store`.`store_id`
SQL);
    }

    public function safeDown()
    {
        $this->truncateTable('product_store_daily');
    }
}
