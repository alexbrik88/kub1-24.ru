<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210708_105758_alter_plan_cash_flows_add_column extends Migration
{
    public static $cashTables = [
        // RU
        'plan_cash_flows',
        // FOREIGN
        'plan_cash_foreign_currency_flows',
    ];

    public function safeUp()
    {
        foreach (self::$cashTables as $tableName) {
            $this->addColumn($tableName, 'project_id', $this->integer());
            $this->addColumn($tableName, 'sale_point_id', $this->integer()->after('project_id'));
            $this->addForeignKey("FK_{$tableName}_to_sale_point", $tableName, 'sale_point_id', 'sale_point', 'id', 'SET NULL', 'CASCADE');
        }
    }

    public function safeDown()
    {
        foreach (self::$cashTables as $tableName) {
            $this->dropForeignKey("FK_{$tableName}_to_sale_point", $tableName);
            $this->dropColumn($tableName, 'sale_point_id');
            $this->dropColumn($tableName, 'project_id');
        }
    }
}
