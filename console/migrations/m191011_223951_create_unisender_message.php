<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191011_223951_create_unisender_message extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%unisender_message}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'employee_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(11)->unsigned()->notNull(),
            'updated_at' => $this->integer(11)->unsigned()->notNull(),
            'unisender_id' => $this->integer(11)->unsigned()->unique()->notNull(),
            'start_date' => $this->integer(11)->unsigned()->notNull(),
            'status' => $this->string()->notNull(),
            'message_id' => $this->integer(11)->unsigned()->notNull(),
            'list_id' => $this->integer(11)->unsigned()->notNull(),
            'subject' => $this->text()->notNull(),
            'sender_name' => $this->string()->notNull(),
            'sender_email' => $this->string()->notNull(),
            'total_contacts_count' => $this->integer(11)->unsigned()->notNull(),
            'sent_messages_count' => $this->integer(11)->unsigned()->notNull(),
            'delivered_messages_count' => $this->integer(11)->unsigned()->notNull(),
            'read_unique_count' => $this->integer(11)->unsigned()->notNull(),
            'read_count' => $this->integer(11)->unsigned()->notNull(),
            'clicked_unique_count' => $this->integer(11)->unsigned()->notNull(),
            'clicked_count' => $this->integer(11)->unsigned()->notNull(),
            'unsubscribed_contacts_count' => $this->integer(11)->unsigned()->notNull(),
            'spam_contacts_count' => $this->integer(11)->unsigned()->notNull(),
        ]);

        $this->addForeignKey('unisender_message_company_id', '{{%unisender_message}}', 'company_id', '{{%company}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('unisender_message_employee_id', '{{%unisender_message}}', 'employee_id', '{{%employee}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('unisender_message_company_id', '{{%unisender_message}}');
        $this->dropForeignKey('unisender_message_employee_id', '{{%unisender_message}}');

        $this->dropTable('{{%unisender_message}}');
    }
}
