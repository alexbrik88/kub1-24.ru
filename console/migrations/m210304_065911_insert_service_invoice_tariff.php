<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210304_065911_insert_service_invoice_tariff extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%service_invoice_tariff}}', [
            'id' => 3,
            'invoice_count' => 100,
            'total_amount' => 600,
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%service_invoice_tariff}}', [
            'id' => 3,
        ]);
    }
}
