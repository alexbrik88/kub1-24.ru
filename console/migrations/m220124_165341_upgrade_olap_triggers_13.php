<?php

use console\components\db\Migration;
use yii\db\Schema;

/**
 * FULL OLAP_FLOWS TRIGGERS LIST on 24/01/22
 */
class m220124_165341_upgrade_olap_triggers_13 extends Migration
{
    public function safeUp()
    {
        //
        //$this->upTriggersOnCashBankFlows();
        //$this->upTriggersOnCashOrderFlows();
        //$this->upTriggersOnCashEmoneyFlows();
        //$this->upTriggersOnAcquiringOperations();
        //$this->upTriggersOnCardOperations();
        $this->upTriggersOnCashBankFlowsToInvoices(); // + tin_children_invoice_amount
        //$this->upTriggersOnCashOrderFlowsToInvoices();
        //$this->upTriggersOnCashEmoneyFlowsToInvoices();
        //
        //$this->upTriggersOnCashBankForeignCurrencyFlows();
        //$this->upTriggersOnCashOrderForeignCurrencyFlows();
        //$this->upTriggersOnCashEmoneyForeignCurrencyFlows();
        //$this->upTriggersOnCashBankForeignCurrencyFlowsToInvoices();
        //$this->upTriggersOnCashOrderForeignCurrencyFlowsToInvoices();
        //$this->upTriggersOnCashEmoneyForeignCurrencyFlowsToInvoices();
    }

    public function safeDown()
    {
        //
        //$this->downTriggersOnCashBankFlows();
        //$this->downTriggersOnCashOrderFlows();
        //$this->downTriggersOnCashEmoneyFlows();
        //$this->downTriggersOnAcquiringOperations();
        //$this->downTriggersOnCardOperations();
        $this->downTriggersOnCashBankFlowsToInvoices(); // - tin_children_invoice_amount
        //$this->downTriggersOnCashOrderFlowsToInvoices();
        //$this->downTriggersOnCashEmoneyFlowsToInvoices();
        //
        //$this->downTriggersOnCashBankForeignCurrencyFlows();
        //$this->downTriggersOnCashOrderForeignCurrencyFlows();
        //$this->downTriggersOnCashEmoneyForeignCurrencyFlows();
        //$this->downTriggersOnCashBankForeignCurrencyFlowsToInvoices();
        //$this->downTriggersOnCashOrderForeignCurrencyFlowsToInvoices();
        //$this->downTriggersOnCashEmoneyForeignCurrencyFlowsToInvoices();
    }

    private function upTriggersOnCashBankFlows()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_bank_flows`');
        $this->execute('
            CREATE TRIGGER `olap_flows_insert_cash_bank_flows`
            AFTER INSERT ON `cash_bank_flows`
            FOR EACH ROW
            BEGIN
                INSERT INTO `olap_flows` (
                    `id`,
                    `company_id`,
                    `contractor_id`,
                    `account_id`,
                    `project_id`,
                    `sale_point_id`,
                    `industry_id`,
                    `wallet`,
                    `type`,
                    `is_accounting`,
                    `item_id`,
                    `date`,
                    `recognition_date`,
                    `is_prepaid_expense`,
                    `amount`,
                    `original_amount`,
                    `currency_id`,
                    `currency_name`,
                    `has_invoice`,
                    `has_tin_parent`,
                    `has_tin_children`,
                    `tin_child_amount`                    
                )
                VALUES (
                    NEW.`id`,
                    NEW.`company_id`,
                    NEW.`contractor_id`,
                    NEW.`account_id`,
                    NEW.`project_id`,
                    NEW.`sale_point_id`,
                    NEW.`industry_id`,
                    "1", /* WALLET */
                    NEW.`flow_type`,
                    1,
                    IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id),
                    NEW.`date`,
                    NEW.`recognition_date`,
                    NEW.`is_prepaid_expense`,
                    NEW.`amount`,
                    NEW.`amount`,
                    "643",
                    "RUB",
                    0,
                    IF(NEW.parent_id IS NULL, 0, 1), /* has_tin_parent */
                    NEW.`has_tin_children`, /* has_tin_children */
                    NEW.`tin_child_amount` /* tin_child_amount */
                );
            END
        ');

        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_update_cash_bank_flows`');
        $this->execute('
            CREATE TRIGGER `olap_flows_update_cash_bank_flows`
            AFTER UPDATE ON `cash_bank_flows`
            FOR EACH ROW
            BEGIN
                UPDATE `olap_flows`
                SET
                    `contractor_id` = NEW.`contractor_id`,
                    `account_id` = NEW.`account_id`,
                    `project_id` = NEW.`project_id`,
                    `sale_point_id` = NEW.`sale_point_id`,
                    `industry_id` = NEW.`industry_id`,
                    `type` = NEW.`flow_type`,
                    `date` = NEW.`date`,
                    `recognition_date` = NEW.`recognition_date`,
                    `is_prepaid_expense` = NEW.`is_prepaid_expense`,
                    `amount` = NEW.`amount`,
                    `original_amount` = NEW.`amount`,
                    `currency_id` = "643",
                    `currency_name` = "RUB",
                    `item_id` = IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id),
                    `has_tin_parent` = IF(NEW.parent_id IS NULL, 0, 1),
                    `has_tin_children` = NEW.`has_tin_children`,
                    `tin_child_amount` = NEW.`tin_child_amount`
                WHERE `id` = NEW.`id`
                    AND `wallet` = "1";
            END
        ');
    }

    private function upTriggersOnCashOrderFlows()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_order_flows`');
        $this->execute('
            CREATE TRIGGER `olap_flows_insert_cash_order_flows`
            AFTER INSERT ON `cash_order_flows`
            FOR EACH ROW
            BEGIN
                INSERT INTO `olap_flows` (
                    `id`,
                    `company_id`,
                    `contractor_id`,
                    `account_id`,
                    `project_id`,
                    `sale_point_id`,
                    `industry_id`,
                    `wallet`,
                    `type`,
                    `is_accounting`,
                    `item_id`,
                    `date`,
                    `recognition_date`,
                    `is_prepaid_expense`,
                    `amount`,
                    `original_amount`,
                    `currency_id`,
                    `currency_name`,
                    `has_invoice`)
                VALUES (
                    NEW.`id`,
                    NEW.`company_id`,
                    NEW.`contractor_id`,
                    NEW.`cashbox_id`,
                    NEW.`project_id`,
                    NEW.`sale_point_id`,
                    NEW.`industry_id`,
                    "2", /* WALLET */
                    NEW.`flow_type`,
                    NEW.`is_accounting`,
                    IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id),
                    NEW.`date`,
                    NEW.`recognition_date`,
                    NEW.`is_prepaid_expense`,
                    NEW.`amount`,
                    NEW.`amount`,
                    "643",
                    "RUB",
                    0);

            END');

        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_update_cash_order_flows`');
        $this->execute('
            CREATE TRIGGER `olap_flows_update_cash_order_flows`
            AFTER UPDATE ON `cash_order_flows`
            FOR EACH ROW
            BEGIN

                UPDATE `olap_flows`
                SET
                    `contractor_id` = NEW.`contractor_id`,
                    `account_id` = NEW.`cashbox_id`,
                    `project_id` = NEW.`project_id`,
                    `sale_point_id` = NEW.`sale_point_id`,
                    `industry_id` = NEW.`industry_id`,
                    `type` = NEW.`flow_type`,
                    `date` = NEW.`date`,
                    `recognition_date` = NEW.`recognition_date`,
                    `is_prepaid_expense` = NEW.`is_prepaid_expense`,
                    `amount` = NEW.`amount`,
                    `original_amount` = NEW.`amount`,
                    `currency_id` = "643",
                    `currency_name` = "RUB",
                    `item_id` = IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id),
                    `is_accounting` = NEW.`is_accounting`
                WHERE `id` = NEW.`id`
                    AND `wallet` = "2";

            END');
    }

    private function upTriggersOnCashEmoneyFlows()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_emoney_flows`');
        $this->execute('
            CREATE TRIGGER `olap_flows_insert_cash_emoney_flows`
            AFTER INSERT ON `cash_emoney_flows`
            FOR EACH ROW
            BEGIN
                INSERT INTO `olap_flows` (
                    `id`,
                    `company_id`,
                    `contractor_id`,
                    `account_id`,
                    `project_id`,
                    `sale_point_id`,
                    `industry_id`,
                    `wallet`,
                    `type`,
                    `is_accounting`,
                    `item_id`,
                    `date`,
                    `recognition_date`,
                    `is_prepaid_expense`,
                    `amount`,
                    `original_amount`,
                    `currency_id`,
                    `currency_name`,
                    `has_invoice`)
                VALUES (
                    NEW.`id`,
                    NEW.`company_id`,
                    NEW.`contractor_id`,
                    NEW.`emoney_id`,
                    NEW.`project_id`,
                    NEW.`sale_point_id`,
                    NEW.`industry_id`,
                    "3", /* WALLET */
                    NEW.`flow_type`,
                    NEW.`is_accounting`,
                    IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id),
                    NEW.`date`,
                    NEW.`recognition_date`,
                    NEW.`is_prepaid_expense`,
                    NEW.`amount`,
                    NEW.`amount`,
                    "643",
                    "RUB",
                    0);
            END');

        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_update_cash_emoney_flows`');
        $this->execute('
            CREATE TRIGGER `olap_flows_update_cash_emoney_flows`
            AFTER UPDATE ON `cash_emoney_flows`
            FOR EACH ROW
            BEGIN
                UPDATE `olap_flows`
                SET
                    `contractor_id` = NEW.`contractor_id`,
                    `account_id` = NEW.`emoney_id`,
                    `project_id` = NEW.`project_id`,
                    `sale_point_id` = NEW.`sale_point_id`,
                    `industry_id` = NEW.`industry_id`,
                    `type` = NEW.`flow_type`,
                    `date` = NEW.`date`,
                    `recognition_date` = NEW.`recognition_date`,
                    `is_prepaid_expense` = NEW.`is_prepaid_expense`,
                    `amount` = NEW.`amount`,
                    `original_amount` = NEW.`amount`,
                    `currency_id` = "643",
                    `currency_name` = "RUB",
                    `item_id` = IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id),
                    `is_accounting` = NEW.`is_accounting`
                WHERE `id` = NEW.`id`
                    AND `wallet` = "3";

            END');
    }

    private function upTriggersOnAcquiringOperations()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_acquiring_operation`');
        $this->execute('
            CREATE TRIGGER `olap_flows_insert_acquiring_operation`
            AFTER INSERT ON `acquiring_operation`
            FOR EACH ROW
            BEGIN
                INSERT INTO `olap_flows` (
                    `id`,
                    `company_id`,
                    `contractor_id`,
                    `account_id`,
                    `project_id`,
                    `sale_point_id`,
                    `industry_id`,
                    `wallet`,
                    `type`,
                    `is_accounting`,
                    `item_id`,
                    `date`,
                    `recognition_date`,
                    `is_prepaid_expense`,
                    `amount`,
                    `original_amount`,
                    `currency_id`,
                    `currency_name`,
                    `has_invoice`)
                VALUES (
                    NEW.`id`,
                    NEW.`company_id`,
                    NEW.`contractor_id`,
                    NEW.`acquiring_id`,
                    NEW.`project_id`,
                    NEW.`sale_point_id`,
                    NEW.`industry_id`,
                    "4", /* WALLET */
                    NEW.`flow_type`,
                    NEW.`is_accounting`,
                    IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id),
                    NEW.`date`,
                    NEW.`recognition_date`,
                    0, /* is_prepaid_expense */
                    NEW.`amount`,
                    NEW.`amount`,
                    "643",
                    "RUB",
                    0);
            END');

        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_update_acquiring_operation`');
        $this->execute('
            CREATE TRIGGER `olap_flows_update_acquiring_operation`
            AFTER UPDATE ON `acquiring_operation`
            FOR EACH ROW
            BEGIN
                UPDATE `olap_flows`
                SET
                    `contractor_id` = NEW.`contractor_id`,
                    `account_id` = NEW.`acquiring_id`,
                    `project_id` = NEW.`project_id`,
                    `sale_point_id` = NEW.`sale_point_id`,
                    `industry_id` = NEW.`industry_id`,
                    `type` = NEW.`flow_type`,
                    `is_accounting` = NEW.`is_accounting`,
                    `date` = NEW.`date`,
                    `recognition_date` = NEW.`recognition_date`,
                    `amount` = NEW.`amount`,
                    `original_amount` = NEW.`amount`,
                    `currency_id` = "643",
                    `currency_name` = "RUB",
                    `item_id` = IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id)
                WHERE `id` = NEW.`id`
                    AND `wallet` = "4";
            END');
    }

    private function upTriggersOnCardOperations()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_card_operation`');
        $this->execute('
            CREATE TRIGGER `olap_flows_insert_card_operation`
            AFTER INSERT ON `card_operation`
            FOR EACH ROW
            BEGIN
                INSERT INTO `olap_flows` (
                    `id`,
                    `company_id`,
                    `contractor_id`,
                    `account_id`,
                    `project_id`,
                    `sale_point_id`,
                    `industry_id`,
                    `wallet`,
                    `type`,
                    `is_accounting`,
                    `item_id`,
                    `date`,
                    `recognition_date`,
                    `is_prepaid_expense`,
                    `amount`,
                    `original_amount`,
                    `currency_id`,
                    `currency_name`,
                    `has_invoice`)
                VALUES (
                    NEW.`id`,
                    NEW.`company_id`,
                    NEW.`contractor_id`,
                    NEW.`account_id`,
                    NEW.`project_id`,
                    NEW.`sale_point_id`,
                    NEW.`industry_id`,
                    "5", /* WALLET */
                    NEW.`flow_type`,
                    NEW.`is_accounting`,
                    IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id),
                    NEW.`date`,
                    NEW.`recognition_date`,
                    0, /* is_prepaid_expense */
                    NEW.`amount_rub`,
                    NEW.`amount`,
                    NEW.`currency_id`,
                    NEW.`currency_name`,
                    0);
            END');

        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_update_card_operation`');
        $this->execute('
            CREATE TRIGGER `olap_flows_update_card_operation`
            AFTER UPDATE ON `card_operation`
            FOR EACH ROW
            BEGIN
                UPDATE `olap_flows`
                SET
                    `contractor_id` = NEW.`contractor_id`,
                    `account_id` = NEW.`account_id`,
                    `project_id` = NEW.`project_id`,
                    `sale_point_id` = NEW.`sale_point_id`,
                    `industry_id` = NEW.`industry_id`,
                    `type` = NEW.`flow_type`,
                    `is_accounting` = NEW.`is_accounting`,
                    `date` = NEW.`date`,
                    `recognition_date` = NEW.`recognition_date`,
                    `amount` = NEW.`amount_rub`,
                    `original_amount` = NEW.`amount`,
                    `currency_id` = NEW.`currency_id`,
                    `currency_name` = NEW.`currency_name`,
                    `item_id` = IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id)
                WHERE `id` = NEW.`id`
                    AND `wallet` = "5";
            END');
    }

    private function upTriggersOnCashBankFlowsToInvoices()
    {
        $this->execute('DROP PROCEDURE IF EXISTS `OlapFlowsRecalculateBankByFlow`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_bank_flow_to_invoice`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_delete_cash_bank_flow_to_invoice`');

        $this->execute('
            CREATE PROCEDURE `OlapFlowsRecalculateBankByFlow`(flowID INT)
            BEGIN
            
              UPDATE `olap_flows` 
              LEFT JOIN 
                (SELECT 
                    `cash_bank_flow_to_invoice`.`flow_id` AS `flow_id`,
                    IF (COUNT(`invoice`.`id`), 1, 0) AS `has_invoice`,
                    SUM(`cash_bank_flow_to_invoice`.`amount`) AS `amount`,
                    SUM(`cash_bank_flow_to_invoice`.`tin_child_amount`) AS `tin_child_amount`,
                    IF (SUM(`invoice`.`has_act` + `invoice`.`has_packing_list` + `invoice`.`has_upd`) > 0, 1, 0) AS `has_doc`,
                    IF (SUM( IF(`invoice`.`need_act`,0,1) + IF(`invoice`.`need_packing_list`,0,1) + IF(`invoice`.`need_upd`,0,1)) > 0, 0, 1) AS `need_doc`	
                  FROM `cash_bank_flow_to_invoice`
                  LEFT JOIN `invoice` ON `cash_bank_flow_to_invoice`.`invoice_id` = `invoice`.`id`
                  WHERE `cash_bank_flow_to_invoice`.`flow_id` = flowID
                  GROUP BY `cash_bank_flow_to_invoice`.`flow_id`
                ) `invoices` ON `olap_flows`.`id` = `invoices`.`flow_id`
                
              SET
            
                `olap_flows`.`has_invoice` = IFNULL(`invoices`.`has_invoice`, 0),
                `olap_flows`.`invoice_amount` = IFNULL(`invoices`.`amount`, 0),
                `olap_flows`.`tin_child_invoice_amount` = IFNULL(`invoices`.`tin_child_amount`, 0),
                `olap_flows`.`has_doc` = IFNULL(`invoices`.`has_doc`, 0),
                `olap_flows`.`need_doc` = IFNULL(`invoices`.`need_doc`, 0)
              
              WHERE `olap_flows`.`wallet` = "1" AND `olap_flows`.`id` = flowID LIMIT 1;
            
            END');
        $this->execute('
            CREATE TRIGGER `olap_flows_insert_cash_bank_flow_to_invoice`
            AFTER INSERT ON `cash_bank_flow_to_invoice`
            FOR EACH ROW
            BEGIN
            
               CALL `OlapFlowsRecalculateBankByFlow`(NEW.`flow_id`);
              
            END');
        $this->execute('
            CREATE TRIGGER `olap_flows_delete_cash_bank_flow_to_invoice`
            AFTER DELETE ON `cash_bank_flow_to_invoice`
            FOR EACH ROW
            BEGIN
            
               CALL `OlapFlowsRecalculateBankByFlow`(OLD.`flow_id`);
              
            END');
    }

    private function upTriggersOnCashOrderFlowsToInvoices()
    {
        $this->execute('DROP PROCEDURE IF EXISTS `OlapFlowsRecalculateOrderByFlow`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_order_flow_to_invoice`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_delete_cash_order_flow_to_invoice`');

        $this->execute('           
            CREATE PROCEDURE `OlapFlowsRecalculateOrderByFlow`(flowID INT)
            BEGIN
            
              UPDATE `olap_flows` 
              LEFT JOIN 
                (SELECT 
                    `cash_order_flow_to_invoice`.`flow_id` AS `flow_id`,
                    IF (COUNT(`invoice`.`id`), 1, 0) AS `has_invoice`,
                    SUM(`cash_order_flow_to_invoice`.`amount`) AS `amount`,
                    IF (SUM(`invoice`.`has_act` + `invoice`.`has_packing_list` + `invoice`.`has_upd`) > 0, 1, 0) AS `has_doc`,
                    IF (SUM( IF(`invoice`.`need_act`,0,1) + IF(`invoice`.`need_packing_list`,0,1) + IF(`invoice`.`need_upd`,0,1)) > 0, 0, 1) AS `need_doc`	
                  FROM `cash_order_flow_to_invoice`
                  LEFT JOIN `invoice` ON `cash_order_flow_to_invoice`.`invoice_id` = `invoice`.`id`
                  WHERE `cash_order_flow_to_invoice`.`flow_id` = flowID
                  GROUP BY `cash_order_flow_to_invoice`.`flow_id`
                ) `invoices` ON `olap_flows`.`id` = `invoices`.`flow_id`
                
              SET
            
                `olap_flows`.`has_invoice` = IFNULL(`invoices`.`has_invoice`, 0),
                `olap_flows`.`invoice_amount` = IFNULL(`invoices`.`amount`, 0),
                `olap_flows`.`has_doc` = IFNULL(`invoices`.`has_doc`, 0),
                `olap_flows`.`need_doc` = IFNULL(`invoices`.`need_doc`, 0)
              
              WHERE `olap_flows`.`wallet` = "2" AND `olap_flows`.`id` = flowID LIMIT 1;
            
            END');
        $this->execute('
            CREATE TRIGGER `olap_flows_insert_cash_order_flow_to_invoice`
            AFTER INSERT ON `cash_order_flow_to_invoice`
            FOR EACH ROW
            BEGIN
            
               CALL `OlapFlowsRecalculateOrderByFlow`(NEW.`flow_id`);
              
            END');
        $this->execute('
            CREATE TRIGGER `olap_flows_delete_cash_order_flow_to_invoice`
            AFTER DELETE ON `cash_order_flow_to_invoice`
            FOR EACH ROW
            BEGIN
            
               CALL `OlapFlowsRecalculateOrderByFlow`(OLD.`flow_id`);
              
            END');
    }

    private function upTriggersOnCashEmoneyFlowsToInvoices()
    {
        $this->execute('DROP PROCEDURE IF EXISTS `OlapFlowsRecalculateEmoneyByFlow`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_emoney_flow_to_invoice`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_delete_cash_emoney_flow_to_invoice`');

        $this->execute('
            CREATE PROCEDURE `OlapFlowsRecalculateEmoneyByFlow`(flowID INT)
            BEGIN
            
              UPDATE `olap_flows` 
              LEFT JOIN 
                (SELECT 
                    `cash_emoney_flow_to_invoice`.`flow_id` AS `flow_id`,
                    IF (COUNT(`invoice`.`id`), 1, 0) AS `has_invoice`,
                    SUM(`cash_emoney_flow_to_invoice`.`amount`) AS `amount`,
                    IF (SUM(`invoice`.`has_act` + `invoice`.`has_packing_list` + `invoice`.`has_upd`) > 0, 1, 0) AS `has_doc`,
                    IF (SUM( IF(`invoice`.`need_act`,0,1) + IF(`invoice`.`need_packing_list`,0,1) + IF(`invoice`.`need_upd`,0,1)) > 0, 0, 1) AS `need_doc`	
                  FROM `cash_emoney_flow_to_invoice`
                  LEFT JOIN `invoice` ON `cash_emoney_flow_to_invoice`.`invoice_id` = `invoice`.`id`
                  WHERE `cash_emoney_flow_to_invoice`.`flow_id` = flowID 	  
                  GROUP BY `cash_emoney_flow_to_invoice`.`flow_id`
                ) `invoices` ON `olap_flows`.`id` = `invoices`.`flow_id`
                
              SET
            
                `olap_flows`.`has_invoice` = IFNULL(`invoices`.`has_invoice`, 0),
                `olap_flows`.`invoice_amount` = IFNULL(`invoices`.`amount`, 0),
                `olap_flows`.`has_doc` = IFNULL(`invoices`.`has_doc`, 0),
                `olap_flows`.`need_doc` = IFNULL(`invoices`.`need_doc`, 0)
              
              WHERE `olap_flows`.`wallet` = "3" AND `olap_flows`.`id` = flowID LIMIT 1;
            
            END');
        $this->execute('
            CREATE TRIGGER `olap_flows_insert_cash_emoney_flow_to_invoice`
            AFTER INSERT ON `cash_emoney_flow_to_invoice`
            FOR EACH ROW
            BEGIN
            
               CALL `OlapFlowsRecalculateEmoneyByFlow`(NEW.`flow_id`);
              
            END');
        $this->execute('
            CREATE TRIGGER `olap_flows_delete_cash_emoney_flow_to_invoice`
            AFTER DELETE ON `cash_emoney_flow_to_invoice`
            FOR EACH ROW
            BEGIN
            
               CALL `OlapFlowsRecalculateEmoneyByFlow`(OLD.`flow_id`);
              
            END');
    }

    private function upTriggersOnCashBankForeignCurrencyFlows()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_bank_foreign_currency_flows`');
        $this->execute('
            CREATE TRIGGER `olap_flows_insert_cash_bank_foreign_currency_flows`
            AFTER INSERT ON `cash_bank_foreign_currency_flows`
            FOR EACH ROW
            BEGIN
                INSERT INTO `olap_flows` (
                    `id`,
                    `company_id`,
                    `contractor_id`,
                    `account_id`,
                    `project_id`,
                    `sale_point_id`,
                    `industry_id`,
                    `wallet`,
                    `type`,
                    `is_accounting`,
                    `item_id`,
                    `date`,
                    `recognition_date`,
                    `is_prepaid_expense`,
                    `amount`,
                    `original_amount`,
                    `currency_id`,
                    `currency_name`,
                    `has_invoice`)
                VALUES (
                    NEW.`id`,
                    NEW.`company_id`,
                    NEW.`contractor_id`,
                    NEW.`account_id`,
                    NEW.`project_id`,
                    NEW.`sale_point_id`,
                    NEW.`industry_id`,
                    "11", /* WALLET */
                    NEW.`flow_type`,
                    1,
                    IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id),
                    NEW.`date`,
                    NEW.`recognition_date`,
                    NEW.`is_prepaid_expense`,
                    NEW.`amount_rub`,
                    NEW.`amount`,
                    NEW.`currency_id`,
                    NEW.`currency_name`,
                    0);
            END
        ');

        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_update_cash_bank_foreign_currency_flows`');
        $this->execute('
            CREATE TRIGGER `olap_flows_update_cash_bank_foreign_currency_flows`
            AFTER UPDATE ON `cash_bank_foreign_currency_flows`
            FOR EACH ROW
            BEGIN
                UPDATE `olap_flows`
                SET
                    `contractor_id` = NEW.`contractor_id`,
                    `account_id` = NEW.`account_id`,
                    `project_id` = NEW.`project_id`,
                    `sale_point_id` = NEW.`sale_point_id`,
                    `industry_id` = NEW.`industry_id`,
                    `type` = NEW.`flow_type`,
                    `date` = NEW.`date`,
                    `recognition_date` = NEW.`recognition_date`,
                    `is_prepaid_expense` = NEW.`is_prepaid_expense`,
                    `amount` = NEW.`amount_rub`,
                    `original_amount` = NEW.`amount`,
                    `currency_id` = NEW.`currency_id`,
                    `currency_name` = NEW.`currency_name`,
                    `item_id` = IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id)
                WHERE `id` = NEW.`id`
                    AND `wallet` = "11";
            END
        ');

        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_delete_cash_bank_foreign_currency_flows`');
        $this->execute('
            CREATE TRIGGER `olap_flows_delete_cash_bank_foreign_currency_flows`
            AFTER DELETE ON `cash_bank_foreign_currency_flows`
            FOR EACH ROW
            BEGIN
                DELETE FROM `olap_flows` WHERE `id` = OLD.`id` AND `wallet` = "11" LIMIT 1;
            END');
    }

    private function upTriggersOnCashOrderForeignCurrencyFlows()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_order_foreign_currency_flows`');
        $this->execute('
            CREATE TRIGGER `olap_flows_insert_cash_order_foreign_currency_flows`
            AFTER INSERT ON `cash_order_foreign_currency_flows`
            FOR EACH ROW
            BEGIN
                INSERT INTO `olap_flows` (
                    `id`,
                    `company_id`,
                    `contractor_id`,
                    `account_id`,
                    `project_id`,
                    `sale_point_id`,
                    `industry_id`,
                    `wallet`,
                    `type`,
                    `is_accounting`,
                    `item_id`,
                    `date`,
                    `recognition_date`,
                    `is_prepaid_expense`,
                    `amount`,
                    `original_amount`,
                    `currency_id`,
                    `currency_name`,
                    `has_invoice`)
                VALUES (
                    NEW.`id`,
                    NEW.`company_id`,
                    NEW.`contractor_id`,
                    NEW.`cashbox_id`,
                    NEW.`project_id`,
                    NEW.`sale_point_id`,
                    NEW.`industry_id`,
                    "12", /* WALLET */
                    NEW.`flow_type`,
                    NEW.`is_accounting`,
                    IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id),
                    NEW.`date`,
                    NEW.`recognition_date`,
                    NEW.`is_prepaid_expense`,
                    NEW.`amount_rub`,
                    NEW.`amount`,
                    NEW.`currency_id`,
                    NEW.`currency_name`,
                    0);

            END');

        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_update_cash_order_foreign_currency_flows`');
        $this->execute('
            CREATE TRIGGER `olap_flows_update_cash_order_foreign_currency_flows`
            AFTER UPDATE ON `cash_order_foreign_currency_flows`
            FOR EACH ROW
            BEGIN

                UPDATE `olap_flows`
                SET
                    `contractor_id` = NEW.`contractor_id`,
                    `account_id` = NEW.`cashbox_id`,
                    `project_id` = NEW.`project_id`,
                    `sale_point_id` = NEW.`sale_point_id`,
                    `industry_id` = NEW.`industry_id`,
                    `type` = NEW.`flow_type`,
                    `date` = NEW.`date`,
                    `recognition_date` = NEW.`recognition_date`,
                    `is_prepaid_expense` = NEW.`is_prepaid_expense`,
                    `amount` = NEW.`amount_rub`,
                    `original_amount` = NEW.`amount`,
                    `currency_id` = NEW.`currency_id`,
                    `currency_name` = NEW.`currency_name`,
                    `item_id` = IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id),
                    `is_accounting` = NEW.`is_accounting`
                WHERE `id` = NEW.`id`
                    AND `wallet` = "12";

            END');

        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_delete_cash_order_foreign_currency_flows`');
        $this->execute('
            CREATE TRIGGER `olap_flows_delete_cash_order_foreign_currency_flows`
            AFTER DELETE ON `cash_order_foreign_currency_flows`
            FOR EACH ROW
            BEGIN
                DELETE FROM `olap_flows` WHERE `id` = OLD.`id` AND `wallet` = "12" LIMIT 1;
            END');
    }

    private function upTriggersOnCashEmoneyForeignCurrencyFlows()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_emoney_foreign_currency_flows`');
        $this->execute('
            CREATE TRIGGER `olap_flows_insert_cash_emoney_foreign_currency_flows`
            AFTER INSERT ON `cash_emoney_foreign_currency_flows`
            FOR EACH ROW
            BEGIN
                INSERT INTO `olap_flows` (
                    `id`,
                    `company_id`,
                    `contractor_id`,
                    `account_id`,
                    `project_id`,
                    `sale_point_id`,
                    `industry_id`,
                    `wallet`,
                    `type`,
                    `is_accounting`,
                    `item_id`,
                    `date`,
                    `recognition_date`,
                    `is_prepaid_expense`,
                    `amount`,
                    `original_amount`,
                    `currency_id`,
                    `currency_name`,
                    `has_invoice`)
                VALUES (
                    NEW.`id`,
                    NEW.`company_id`,
                    NEW.`contractor_id`,
                    NEW.`emoney_id`,
                    NEW.`project_id`,
                    NEW.`sale_point_id`,
                    NEW.`industry_id`,
                    "13", /* WALLET */
                    NEW.`flow_type`,
                    NEW.`is_accounting`,
                    IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id),
                    NEW.`date`,
                    NEW.`recognition_date`,
                    NEW.`is_prepaid_expense`,
                    NEW.`amount_rub`,
                    NEW.`amount`,
                    NEW.`currency_id`,
                    NEW.`currency_name`,
                    0);
            END');

        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_update_cash_emoney_foreign_currency_flows`');
        $this->execute('
            CREATE TRIGGER `olap_flows_update_cash_emoney_foreign_currency_flows`
            AFTER UPDATE ON `cash_emoney_foreign_currency_flows`
            FOR EACH ROW
            BEGIN
                UPDATE `olap_flows`
                SET
                    `contractor_id` = NEW.`contractor_id`,
                    `account_id` = NEW.`emoney_id`,
                    `project_id` = NEW.`project_id`,
                    `sale_point_id` = NEW.`sale_point_id`,
                    `industry_id` = NEW.`industry_id`,
                    `type` = NEW.`flow_type`,
                    `date` = NEW.`date`,
                    `recognition_date` = NEW.`recognition_date`,
                    `is_prepaid_expense` = NEW.`is_prepaid_expense`,
                    `amount` = NEW.`amount_rub`,
                    `original_amount` = NEW.`amount`,
                    `currency_id` = NEW.`currency_id`,
                    `currency_name` = NEW.`currency_name`,
                    `item_id` = IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id),
                    `is_accounting` = NEW.`is_accounting`
                WHERE `id` = NEW.`id`
                    AND `wallet` = "13";

            END');

        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_delete_cash_emoney_foreign_currency_flows`');
        $this->execute('
            CREATE TRIGGER `olap_flows_delete_cash_emoney_foreign_currency_flows`
            AFTER DELETE ON `cash_emoney_foreign_currency_flows`
            FOR EACH ROW
            BEGIN
                DELETE FROM `olap_flows` WHERE `id` = OLD.`id` AND `wallet` = "13" LIMIT 1;
            END');
    }

    private function upTriggersOnCashBankForeignCurrencyFlowsToInvoices()
    {
        $table = 'cash_bank_foreign_currency_flows_to_invoice';

        $this->execute("DROP TRIGGER IF EXISTS `olap_flows_insert_{$table}`");
        $this->execute("
            CREATE TRIGGER `olap_flows_insert_{$table}`
            AFTER INSERT ON `{$table}`
            FOR EACH ROW
            BEGIN
                UPDATE `olap_flows`
                LEFT JOIN (
                    SELECT `flow_id`, SUM(`amount`) `amount`
                    FROM `{$table}`
                    WHERE `flow_id` = NEW.`flow_id`
                    GROUP BY `flow_id`
                ) `t` ON `t`.`flow_id` = `olap_flows`.`id`
                SET
                    `olap_flows`.`has_invoice` = IF(`t`.`flow_id` IS NULL, 0, 1),
                    `olap_flows`.`invoice_amount` = IFNULL(`t`.`amount`, 0)
                WHERE `olap_flows`.`id` = NEW.`flow_id`
                    AND `olap_flows`.`wallet` = 11;
            END
        ");

        $this->execute("DROP TRIGGER IF EXISTS `olap_flows_delete_{$table}`");
        $this->execute("
            CREATE TRIGGER `olap_flows_delete_{$table}`
            AFTER DELETE ON `{$table}`
            FOR EACH ROW
            BEGIN
                UPDATE `olap_flows`
                LEFT JOIN (
                    SELECT `flow_id`, SUM(`amount`) `amount`
                    FROM `{$table}`
                    WHERE `flow_id` = OLD.`flow_id`
                    GROUP BY `flow_id`
                ) `t` ON `t`.`flow_id` = `olap_flows`.`id`
                SET
                    `olap_flows`.`has_invoice` = IF(`t`.`flow_id` IS NULL, 0, 1),
                    `olap_flows`.`invoice_amount` = IFNULL(`t`.`amount`, 0)
                WHERE `olap_flows`.`id` = OLD.`flow_id`
                    AND `olap_flows`.`wallet` = 11;
            END
        ");
    }

    private function upTriggersOnCashOrderForeignCurrencyFlowsToInvoices()
    {
        $table = 'cash_order_foreign_currency_flows_to_invoice';

        $this->execute("DROP TRIGGER IF EXISTS `olap_flows_insert_{$table}`");
        $this->execute("
            CREATE TRIGGER `olap_flows_insert_{$table}`
            AFTER INSERT ON `{$table}`
            FOR EACH ROW
            BEGIN
                UPDATE `olap_flows`
                LEFT JOIN (
                    SELECT `flow_id`, SUM(`amount`) `amount`
                    FROM `{$table}`
                    WHERE `flow_id` = NEW.`flow_id`
                    GROUP BY `flow_id`
                ) `t` ON `t`.`flow_id` = `olap_flows`.`id`
                SET
                    `olap_flows`.`has_invoice` = IF(`t`.`flow_id` IS NULL, 0, 1),
                    `olap_flows`.`invoice_amount` = IFNULL(`t`.`amount`, 0)
                WHERE `olap_flows`.`id` = NEW.`flow_id`
                    AND `olap_flows`.`wallet` = 12;
            END
        ");

        $this->execute("DROP TRIGGER IF EXISTS `olap_flows_delete_{$table}`");
        $this->execute("
            CREATE TRIGGER `olap_flows_delete_{$table}`
            AFTER DELETE ON `{$table}`
            FOR EACH ROW
            BEGIN
                UPDATE `olap_flows`
                LEFT JOIN (
                    SELECT `flow_id`, SUM(`amount`) `amount`
                    FROM `{$table}`
                    WHERE `flow_id` = OLD.`flow_id`
                    GROUP BY `flow_id`
                ) `t` ON `t`.`flow_id` = `olap_flows`.`id`
                SET
                    `olap_flows`.`has_invoice` = IF(`t`.`flow_id` IS NULL, 0, 1),
                    `olap_flows`.`invoice_amount` = IFNULL(`t`.`amount`, 0)
                WHERE `olap_flows`.`id` = OLD.`flow_id`
                    AND `olap_flows`.`wallet` = 12;
            END
        ");
    }

    private function upTriggersOnCashEmoneyForeignCurrencyFlowsToInvoices()
    {
        $table = 'cash_emoney_foreign_currency_flows_to_invoice';

        $this->execute("DROP TRIGGER IF EXISTS `olap_flows_insert_{$table}`");
        $this->execute("
            CREATE TRIGGER `olap_flows_insert_{$table}`
            AFTER INSERT ON `{$table}`
            FOR EACH ROW
            BEGIN
                UPDATE `olap_flows`
                LEFT JOIN (
                    SELECT `flow_id`, SUM(`amount`) `amount`
                    FROM `{$table}`
                    WHERE `flow_id` = NEW.`flow_id`
                    GROUP BY `flow_id`
                ) `t` ON `t`.`flow_id` = `olap_flows`.`id`
                SET
                    `olap_flows`.`has_invoice` = IF(`t`.`flow_id` IS NULL, 0, 1),
                    `olap_flows`.`invoice_amount` = IFNULL(`t`.`amount`, 0)
                WHERE `olap_flows`.`id` = NEW.`flow_id`
                    AND `olap_flows`.`wallet` = 13;
            END
        ");

        $this->execute("DROP TRIGGER IF EXISTS `olap_flows_delete_{$table}`");
        $this->execute("
            CREATE TRIGGER `olap_flows_delete_{$table}`
            AFTER DELETE ON `{$table}`
            FOR EACH ROW
            BEGIN
                UPDATE `olap_flows`
                LEFT JOIN (
                    SELECT `flow_id`, SUM(`amount`) `amount`
                    FROM `{$table}`
                    WHERE `flow_id` = OLD.`flow_id`
                    GROUP BY `flow_id`
                ) `t` ON `t`.`flow_id` = `olap_flows`.`id`
                SET
                    `olap_flows`.`has_invoice` = IF(`t`.`flow_id` IS NULL, 0, 1),
                    `olap_flows`.`invoice_amount` = IFNULL(`t`.`amount`, 0)
                WHERE `olap_flows`.`id` = OLD.`flow_id`
                    AND `olap_flows`.`wallet` = 13;
            END
        ");
    }

    //

    private function downTriggersOnCashBankFlowsToInvoices()
    {
        $this->execute('DROP PROCEDURE IF EXISTS `OlapFlowsRecalculateBankByFlow`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_bank_flow_to_invoice`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_delete_cash_bank_flow_to_invoice`');

        $this->execute('
            CREATE PROCEDURE `OlapFlowsRecalculateBankByFlow`(flowID INT)
            BEGIN
            
              UPDATE `olap_flows` 
              LEFT JOIN 
                (SELECT 
                    `cash_bank_flow_to_invoice`.`flow_id` AS `flow_id`,
                    IF (COUNT(`invoice`.`id`), 1, 0) AS `has_invoice`,
                    SUM(`cash_bank_flow_to_invoice`.`amount`) AS `amount`,
                    IF (SUM(`invoice`.`has_act` + `invoice`.`has_packing_list` + `invoice`.`has_upd`) > 0, 1, 0) AS `has_doc`,
                    IF (SUM( IF(`invoice`.`need_act`,0,1) + IF(`invoice`.`need_packing_list`,0,1) + IF(`invoice`.`need_upd`,0,1)) > 0, 0, 1) AS `need_doc`	
                  FROM `cash_bank_flow_to_invoice`
                  LEFT JOIN `invoice` ON `cash_bank_flow_to_invoice`.`invoice_id` = `invoice`.`id`
                  WHERE `cash_bank_flow_to_invoice`.`flow_id` = flowID
                  GROUP BY `cash_bank_flow_to_invoice`.`flow_id`
                ) `invoices` ON `olap_flows`.`id` = `invoices`.`flow_id`
                
              SET
            
                `olap_flows`.`has_invoice` = IFNULL(`invoices`.`has_invoice`, 0),
                `olap_flows`.`invoice_amount` = IFNULL(`invoices`.`amount`, 0),
                `olap_flows`.`has_doc` = IFNULL(`invoices`.`has_doc`, 0),
                `olap_flows`.`need_doc` = IFNULL(`invoices`.`need_doc`, 0)
              
              WHERE `olap_flows`.`wallet` = "1" AND `olap_flows`.`id` = flowID LIMIT 1;
            
            END');
        $this->execute('
            CREATE TRIGGER `olap_flows_insert_cash_bank_flow_to_invoice`
            AFTER INSERT ON `cash_bank_flow_to_invoice`
            FOR EACH ROW
            BEGIN
            
               CALL `OlapFlowsRecalculateBankByFlow`(NEW.`flow_id`);
              
            END');
        $this->execute('
            CREATE TRIGGER `olap_flows_delete_cash_bank_flow_to_invoice`
            AFTER DELETE ON `cash_bank_flow_to_invoice`
            FOR EACH ROW
            BEGIN
            
               CALL `OlapFlowsRecalculateBankByFlow`(OLD.`flow_id`);
              
            END');
    }
}
