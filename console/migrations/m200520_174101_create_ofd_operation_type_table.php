<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%ofd_operation_type}}`.
 */
class m200520_174101_create_ofd_operation_type_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%ofd_operation_type}}', [
            'id' => $this->primaryKey()->unsigned(),
            'name' => $this->string()->notNull(),
        ]);

        $this->batchInsert('{{%ofd_operation_type}}', [
            'id',
            'name',
        ], [
            [1, 'Приход'],
            [2, 'Возврат прихода'],
            [3, 'Расход'],
            [4, 'Возврат расхода'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%ofd_operation_type}}');
    }
}
