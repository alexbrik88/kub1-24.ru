<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210815_045604_alter_ofd_kkt_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('ofd_kkt', 'fiscal_number', $this->string(32)->after('factory_number')->comment('Номер фискального накопителя'));
    }

    public function safeDown()
    {
        $this->dropColumn('ofd_kkt', 'fiscal_number');
    }
}
