<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200714_073312_alter_company_alter_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%company}}', 'form_utm', $this->text());
    }

    public function safeDown()
    {
        $this->alterColumn('{{%company}}', 'form_utm', $this->string());
    }
}
