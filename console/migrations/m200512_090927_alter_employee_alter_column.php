<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200512_090927_alter_employee_alter_column extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%employee}}', 'main_company_id',  $this->integer());
        $this->alterColumn('{{%employee}}', 'my_companies',  $this->string());
    }

    public function down()
    {
        $this->alterColumn('{{%employee}}', 'main_company_id',  $this->integer());
        $this->alterColumn('{{%employee}}', 'my_companies',  $this->string());
    }
}
