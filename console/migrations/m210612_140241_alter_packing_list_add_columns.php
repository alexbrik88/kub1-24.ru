<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210612_140241_alter_packing_list_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('packing_list', 'contractor_id', $this->integer()->after('type'));
        $this->addColumn('packing_list', 'company_id', $this->integer()->after('type'));

        $this->execute("
            UPDATE `packing_list` doc
            LEFT JOIN `invoice` ON invoice.id = doc.invoice_id
            SET 
                doc.company_id = invoice.company_id, 
                doc.contractor_id = invoice.contractor_id
        ");

        $this->createIndex('IDX_packing_list_to_company', 'packing_list', 'company_id');
        $this->createIndex('IDX_packing_list_to_contractor', 'packing_list', 'contractor_id');
    }

    public function safeDown()
    {
        $this->dropColumn('packing_list', 'contractor_id');
        $this->dropColumn('packing_list', 'company_id');
    }
}
