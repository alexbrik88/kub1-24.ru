<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200602_074516_alter_invoice_alter_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%invoice}}', 'company_ks', $this->string());
        $this->alterColumn('{{%invoice}}', 'company_rs', $this->string());
        $this->alterColumn('{{%invoice}}', 'contractor_ks', $this->string());
        $this->alterColumn('{{%invoice}}', 'contractor_rs', $this->string());
    }

    public function safeDown()
    {
        $this->alterColumn('{{%invoice}}', 'company_ks', $this->char(20));
        $this->alterColumn('{{%invoice}}', 'company_rs', $this->char(20));
        $this->alterColumn('{{%invoice}}', 'contractor_ks', $this->char(20));
        $this->alterColumn('{{%invoice}}', 'contractor_rs', $this->char(20));
    }
}
