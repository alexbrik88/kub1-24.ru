<?php

use common\models\Company;
use common\models\employee\Employee;
use console\components\db\Migration;
use yii\db\Schema;

class m181023_092622_alter_company_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%company}}', 'owner_employee_id', $this->integer()->notNull()->after('id'));

        $this->execute("
            UPDATE {{%company}}
            SET {{%company}}.[[owner_employee_id]] = (
                SELECT {{%employee}}.[[id]]
                FROM {{%employee_company}}
                LEFT JOIN {{%employee}} ON {{%employee}}.[[id]] = {{%employee_company}}.[[employee_id]]
                WHERE {{%employee_company}}.[[company_id]] = {{%company}}.[[id]]
                AND {{%employee_company}}.[[is_working]] = 1
                AND {{%employee}}.[[is_deleted]] = 0
                ORDER BY {{%employee_company}}.[[employee_role_id]] ASC, {{%employee_company}}.[[created_at]] ASC
                LIMIT 1
            )
        ");

        $query = Company::find()->leftJoin('{{%employee}}', '{{%employee}}.[[id]] = {{%company}}.[[owner_employee_id]]')->andWhere([
            'employee.id' => null,
        ]);
        if ($query->exists()) {
            $n = $this->db->createCommand()->insert('{{%employee}}', [
                'employee_role_id' => 1,
                'is_active' => false,
                'is_deleted' => true,
            ])->execute();
            if ($n > 0) {
                $this->execute("
                    UPDATE {{%company}}
                    LEFT JOIN {{%employee}} ON {{%employee}}.[[id]] = {{%company}}.[[owner_employee_id]]
                    SET {{%company}}.[[owner_employee_id]] = :id
                    WHERE {{%employee}}.[[id]] IS NULL
                ", [':id' => Yii::$app->db->getLastInsertID()]);
            }
        }

        $this->addForeignKey('fk_company_owner_employee', '{{%company}}', 'owner_employee_id', '{{%employee}}', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_company_owner_employee', '{{%company}}');

        $this->dropColumn('{{%company}}', 'owner_employee_id');
    }
}
