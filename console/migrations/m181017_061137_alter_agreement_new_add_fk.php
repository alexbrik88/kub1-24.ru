<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181017_061137_alter_agreement_new_add_fk extends Migration
{
    public function safeUp()
    {
        $this->addForeignKey('fk_agreement_new_company', '{{%agreement_new}}', 'company_id', '{{%company}}', 'id', 'CASCADE');
        $this->addForeignKey('fk_agreement_new_contractor', '{{%agreement_new}}', 'contractor_id', '{{%contractor}}', 'id', 'CASCADE');
        $this->addForeignKey('fk_agreement_new_employee', '{{%agreement_new}}', 'created_by', '{{%employee}}', 'id');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('fk_agreement_new_company', '{{%agreement_new}}');
        $this->dropForeignKey('fk_agreement_new_contractor', '{{%agreement_new}}');
        $this->dropForeignKey('fk_agreement_new_employee', '{{%agreement_new}}');
    }
}


