<?php

use console\components\db\Migration;
use yii\db\Schema;
use common\models\Company;
use common\models\document\Act;
use frontend\models\Documents;
use common\models\document\PackingList;
use common\models\document\InvoiceFacture;

class m160616_072928_add_to_act_packing_list_and_invoice_facture_ordinal_document_number extends Migration
{
    public $tAct = 'act';
    public $tPackingList = 'packing_list';
    public $tInvoiceFacture = 'invoice_facture';

    public function safeUp()
    {
        $this->addColumn($this->tAct, 'ordinal_document_number', $this->integer()->notNull());
        $this->addColumn($this->tPackingList, 'ordinal_document_number', $this->integer()->notNull());
        $this->addColumn($this->tInvoiceFacture, 'ordinal_document_number', $this->integer()->notNull());

        $acts = Act::find()->all();
        $packingLists = PackingList::find()->all();
        $invoiceFactures = InvoiceFacture::find()->all();
        foreach ($acts as $act) {
            /* @var Act $act */
            $documentNumber = preg_replace('~\D+~', '', $act->document_number);
            $this->update($this->tAct, ['ordinal_document_number' => $documentNumber], ['id' => $act->id]);
        }
        foreach ($packingLists as $packingList) {
            /* @var PackingList $packingList */
            $documentNumber = preg_replace('~\D+~', '', $packingList->document_number);
            $this->update($this->tPackingList, ['ordinal_document_number' => $documentNumber], ['id' => $packingList->id]);
        }
        foreach ($invoiceFactures as $invoiceFacture) {
            /* @var InvoiceFacture $invoiceFacture */
            $documentNumber = preg_replace('~\D+~', '', $invoiceFacture->document_number);
            $this->update($this->tInvoiceFacture, ['ordinal_document_number' => $documentNumber], ['id' => $invoiceFacture->id]);
        }
    }

    public function safeDown()
    {
        $this->dropColumn($this->tAct, 'ordinal_document_number');
        $this->dropColumn($this->tPackingList, 'ordinal_document_number');
        $this->dropColumn($this->tInvoiceFacture, 'ordinal_document_number');
    }
}


