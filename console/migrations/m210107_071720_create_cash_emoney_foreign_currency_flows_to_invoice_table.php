<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%cash_emoney_foreign_currency_flows_to_invoice}}`.
 */
class m210107_071720_create_cash_emoney_foreign_currency_flows_to_invoice_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%cash_emoney_foreign_currency_flows_to_invoice}}', [
            'flow_id' => $this->integer()->notNull(),
            'invoice_id' => $this->integer()->notNull(),
            'amount' => $this->bigInteger(20)->notNull(),
            'PRIMARY KEY ([[flow_id]], [[invoice_id]])'
        ]);

        $this->addForeignKey(
            'FK_cash_emoney_foreign_currency_flows_to_invoice__flow',
            '{{%cash_emoney_foreign_currency_flows_to_invoice}}',
            'flow_id',
            '{{%cash_emoney_foreign_currency_flows}}',
            'id'
        );
        $this->addForeignKey(
            'FK_cash_emoney_foreign_currency_flows_to_invoice__invoice',
            '{{%cash_emoney_foreign_currency_flows_to_invoice}}',
            'invoice_id',
            '{{%foreign_currency_invoice}}',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%cash_emoney_foreign_currency_flows_to_invoice}}');
    }
}
