<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180404_160339_add_new_columns_to_company extends Migration
{
    public $tableName = 'company';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'show_popup_expose_other_documents', $this->boolean()->defaultValue(true));
        $this->addColumn($this->tableName, 'show_popup_business_analyse', $this->boolean()->defaultValue(true));
        $this->addColumn($this->tableName, 'show_popup_product', $this->boolean()->defaultValue(true));
        $this->addColumn($this->tableName, 'show_popup_sale_increase', $this->boolean()->defaultValue(true));

        $this->update($this->tableName, ['show_popup_expose_other_documents' => false], ['not', ['id' => null]]);
        $this->update($this->tableName, ['show_popup_business_analyse' => false], ['not', ['id' => null]]);
        $this->update($this->tableName, ['show_popup_product' => false], ['not', ['id' => null]]);
        $this->update($this->tableName, ['show_popup_sale_increase' => false], ['not', ['id' => null]]);
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'show_popup_expose_other_documents');
        $this->dropColumn($this->tableName, 'show_popup_business_analyse');
        $this->dropColumn($this->tableName, 'show_popup_product');
        $this->dropColumn($this->tableName, 'show_popup_sale_increase');
    }
}


