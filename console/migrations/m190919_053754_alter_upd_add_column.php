<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190919_053754_alter_upd_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%upd}}', 'print_id', $this->integer()->after('signature_id'));
        $this->addColumn('{{%upd}}', 'chief_signature_id', $this->integer()->after('print_id'));
        $this->addColumn('{{%upd}}', 'chief_accountant_signature_id', $this->integer()->after('chief_signature_id'));

        $this->addForeignKey(
            'FK_upd_company_details_print',
            '{{%upd}}',
            'print_id',
            '{{%company_details_file}}',
            'id'
        );

        $this->addForeignKey(
            'FK_upd_company_details_chief_signature',
            '{{%upd}}',
            'chief_signature_id',
            '{{%company_details_file}}',
            'id'
        );

        $this->addForeignKey(
            'FK_upd_company_details_chief_accountant_signature',
            '{{%upd}}',
            'chief_accountant_signature_id',
            '{{%company_details_file}}',
            'id'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_upd_company_details_print', '{{%upd}}');
        $this->dropForeignKey('FK_upd_company_details_chief_signature', '{{%upd}}');
        $this->dropForeignKey('FK_upd_company_details_chief_accountant_signature', '{{%upd}}' );

        $this->dropColumn('{{%upd}}', 'print_id');
        $this->dropColumn('{{%upd}}', 'chief_signature_id');
        $this->dropColumn('{{%upd}}', 'chief_accountant_signature_id');
    }
}
