<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201210_160006_insert_cash_contractor_type extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%cash_contractor_type}}', [
            'id' => 7,
            'name' => 'bank_currency',
            'text' => 'Банк, иностранная валюта',
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%cash_contractor_type}}', [
            'id' => 7,
        ]);
    }
}
