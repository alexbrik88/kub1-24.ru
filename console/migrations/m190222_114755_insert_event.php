<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190222_114755_insert_event extends Migration
{
    public function safeUp()
    {
        $this->batchInsert('event', ['id', 'name'], [

            ['89', 'Нулевая декларация. Клик на ссылку'],
            ['90', 'Нулевая декларация. Скачали'],
            ['91', 'Нулевая декларация. Распечатали Опись'],

            ['92', 'Декларация. Клик на ссылку'],
            ['93', 'Декларация. Скачали'],
            ['94', 'Декларация. Распечатали Опись'],
            ['95', 'Декларация. Скачали платежку'],
            ['96', 'Декларация. Распечатали платежку'],

        ]);
    }

    public function safeDown()
    {
        $this->delete('event', ['id' => [89, 90, 91, 92, 93, 94, 95, 96]]);
    }
}