<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210218_212538_rework_news_date_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('news', 'date', $this->date());
    }

    public function safeDown()
    {
        $this->alterColumn('news', 'date', $this->date()->notNull());
    }
}
