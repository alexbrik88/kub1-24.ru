<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200227_073601_alter_company_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('company', 'has_price_list_notifications', $this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('company', 'has_price_list_notifications');
    }
}
