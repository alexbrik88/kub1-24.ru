<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180508_103410_add_show_popup_kub_do_it extends Migration
{
    public $tableName = 'company';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'show_popup_kub_do_it', $this->boolean()->defaultValue(true));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'show_popup_kub_do_it');
    }
}


