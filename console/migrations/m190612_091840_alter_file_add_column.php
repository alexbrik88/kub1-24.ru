<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190612_091840_alter_file_add_column extends Migration
{
    public $tFile = 'file';
    public $tUploadType = 'file_upload_type';

    public function safeUp()
    {
        $this->addColumn($this->tFile, 'upload_type_id', $this->integer()->notNull()->defaultValue(1));
        $this->addForeignKey('FK_file_to_upload_type', $this->tFile, 'upload_type_id', $this->tUploadType, 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_file_to_upload_type', $this->tFile);
        $this->dropColumn($this->tFile, 'upload_type_id');
    }
}
