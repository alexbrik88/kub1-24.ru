<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200210_114033_alter_user_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%user}}', '_page_size', $this->text());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%user}}', '_page_size', $this->text());
    }
}
