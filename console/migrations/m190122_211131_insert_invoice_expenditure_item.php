<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190122_211131_insert_invoice_expenditure_item extends Migration
{
    public $name = 'Транспортный налог';
    public $kbk = '18210604011021000110';

    public function safeUp()
    {
        $id = Yii::$app->db->createCommand('
            SELECT MAX([[id]])
            FROM {{invoice_expenditure_item}}
            WHERE [[id]] < 100
        ')->queryScalar() + 1;

        $this->insert('invoice_expenditure_item', [
            'id' => $id,
            'group_id' => 1,
            'name' => $this->name,
        ]);

        $this->insert('tax_kbk', [
            'id' => $this->kbk,
            'expenditure_item_id' => $id,
        ]);
    }

    public function safeDown()
    {
        $this->delete('tax_kbk', ['id' => $this->kbk]);

        $this->delete('invoice_expenditure_item', ['name' => $this->name]);
    }
}
