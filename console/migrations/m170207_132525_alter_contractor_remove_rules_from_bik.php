<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170207_132525_alter_contractor_remove_rules_from_bik extends Migration
{
    public $tContractor = 'contractor';

    public function safeUp()
    {
        $this->alterColumn($this->tContractor, 'BIC', $this->char(255));
        $this->alterColumn('checking_accountant', 'bik' , $this->char(255));
        $this->alterColumn('bik_dictionary', 'bik' , $this->char(255));
    }

    public function safeDown()
    {
        $this->alterColumn($this->tContractor, 'BIC', $this->char(9));
        $this->alterColumn('checking_accountant', 'bik' , $this->char(9));
        $this->alterColumn('bik_dictionary', 'bik' , $this->char(9));
    }
}


