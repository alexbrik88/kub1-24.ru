<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181128_122309_add_map_width_and_map_longitude_to_loading_and_unloading_address extends Migration
{
    public $tableName = 'loading_and_unloading_address';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'map_width', $this->string()->defaultValue(null));
        $this->addColumn($this->tableName, 'map_longitude', $this->string()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'map_width');
        $this->dropColumn($this->tableName, 'map_longitude');
    }
}


