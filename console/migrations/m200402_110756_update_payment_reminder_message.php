<?php

use console\components\db\Migration;
use yii\db\Query;
use yii\db\Schema;

class m200402_110756_update_payment_reminder_message extends Migration
{
    public function safeUp()
    {
        $query = new Query;
        $query->select(['id', 'for_email_list'])
            ->from('{{%payment_reminder_message}}')
            ->andWhere(['number' => [5, 9]])
            ->andWhere(['not', ['for_email_list' => null]]);

        $limit = 1000;
        $offset = 0;
        $db = Yii::$app->db;
        do {
            $rows = $query->offset($offset)->limit($limit)->all();
            if ($rows) {
                foreach ($rows as $key => $row) {
                    $emails = @unserialize($row['for_email_list']);
                    if (is_array($emails)) {
                        foreach ($emails as $i => $value) {
                            if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                                unset($emails[$i]);
                            }
                        }
                        $emails = implode(',', $emails);
                    } else {
                        $emails = '';
                    }
                    $rows[$key]['for_email_list'] = $emails;
                }
                $sql = $db->queryBuilder->batchInsert('{{%payment_reminder_message}}', ['id', 'for_email_list'], $rows);
                $db->createCommand($sql . ' ON DUPLICATE KEY UPDATE [[for_email_list]] = VALUES([[for_email_list]])')->execute();
            }
            $offset += $limit;
        } while (count($rows) == $limit);
    }

    public function safeDown()
    {
        $query = new Query;
        $query->select(['id', 'for_email_list'])
            ->from('{{%payment_reminder_message}}')
            ->andWhere(['number' => [5, 9]])
            ->andWhere(['not', ['for_email_list' => null]]);

        $limit = 1000;
        $offset = 0;
        $db = Yii::$app->db;
        do {
            $rows = $query->offset($offset)->limit($limit)->all();
            if ($rows) {
                foreach ($rows as $key => $row) {
                    $emails = explode(',', $row['for_email_list']);
                    if (is_array($emails)) {
                        foreach ($emails as $i => $value) {
                            if (!filter_var($value, FILTER_VALIDATE_EMAIL)) {
                                unset($emails[$i]);
                            }
                        }
                        $emails = serialize($emails ? : null);
                    } else {
                        $emails = serialize(null);
                    }
                    $rows[$key]['for_email_list'] = $emails;
                }
                $sql = $db->queryBuilder->batchInsert('{{%payment_reminder_message}}', ['id', 'for_email_list'], $rows);
                $db->createCommand($sql . ' ON DUPLICATE KEY UPDATE [[for_email_list]] = VALUES([[for_email_list]])')->execute();
            }
            $offset += $limit;
        } while (count($rows) == $limit);
    }
}
