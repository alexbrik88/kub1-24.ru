<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210709_122527_alter_crm_task_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%crm_task}}', '__checklist', $this->json());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%crm_task}}', '__checklist');
    }
}
