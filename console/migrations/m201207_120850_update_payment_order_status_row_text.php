<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201207_120850_update_payment_order_status_row_text extends Migration
{
    public function safeUp()
    {
        $this->execute('UPDATE `payment_order_status` SET `name` = "Экспортировано в банк" WHERE `id` = 2');
    }

    public function safeDown()
    {
        $this->execute('UPDATE `payment_order_status` SET `name` = "Импортировано в банк" WHERE `id` = 2');
    }
}
