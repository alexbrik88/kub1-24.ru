<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210930_135612_alter_crm_log_alter_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%crm_log}}', 'employee_id', $this->integer()->null());
    }

    public function safeDown()
    {
        $this->alterColumn('{{%crm_log}}', 'employee_id', $this->integer()->notNull());
    }
}
