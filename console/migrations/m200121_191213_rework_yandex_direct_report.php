<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200121_191213_rework_yandex_direct_report extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%yandex_direct_campaign}}', [
            'id' => $this->bigInteger(100)->notNull(),
            'company_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'status' => $this->string()->defaultValue(null),
            'strategy' => $this->string()->defaultValue(null),
            'showing_place' => $this->string()->defaultValue(null),
            'daily_budget_amount' => $this->float(2)->unsigned()->defaultValue(null),
        ]);
        $this->addPrimaryKey('id_company', '{{%yandex_direct_campaign}}', ['id', 'company_id']);
        $this->addForeignKey('FK_yandex_direct_campaign_to_company', 'yandex_direct_campaign', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
        $this->execute("
            INSERT INTO {{%yandex_direct_campaign}} (id, company_id, name, status, strategy, showing_place, daily_budget_amount)
                 SELECT campaign_id, company_id, campaign_name, status, strategy, showing_place, daily_budget_amount
                   FROM {{%yandex_direct_campaign_report}}
                  GROUP BY campaign_id, company_id
        ");
        $this->dropColumn('{{%yandex_direct_campaign_report}}', 'campaign_name');
        $this->dropColumn('{{%yandex_direct_campaign_report}}', 'status');
        $this->dropColumn('{{%yandex_direct_campaign_report}}', 'strategy');
        $this->dropColumn('{{%yandex_direct_campaign_report}}', 'showing_place');
        $this->dropColumn('{{%yandex_direct_campaign_report}}', 'daily_budget_amount');
        $this->addForeignKey('FK_yandex_direct_campaign_report_to_campaign', 'yandex_direct_campaign_report', 'campaign_id', 'yandex_direct_campaign', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('{{%yandex_direct_ad_group}}', [
            'id' => $this->bigInteger(100)->notNull(),
            'campaign_id' => $this->bigInteger(100)->notNull(),
            'company_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'status' => $this->string()->defaultValue(null),
        ]);
        $this->addPrimaryKey('id_campaign_company', '{{%yandex_direct_ad_group}}', ['id', 'campaign_id', 'company_id']);
        $this->addForeignKey('FK_yandex_direct_ad_group_to_campaign', 'yandex_direct_ad_group', 'campaign_id', 'yandex_direct_campaign', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_yandex_direct_ad_group_to_company', 'yandex_direct_ad_group', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
        $this->execute("
            INSERT INTO {{%yandex_direct_ad_group}} (id, campaign_id, company_id, name, status)
                 SELECT ad_group_id, campaign_id, company_id, ad_group_name, status
                   FROM {{%yandex_direct_ad_group_report}}
                  GROUP BY ad_group_id, campaign_id, company_id
        ");
        $this->dropColumn('{{%yandex_direct_ad_group_report}}', 'ad_group_name');
        $this->dropColumn('{{%yandex_direct_ad_group_report}}', 'status');
        $this->addForeignKey('FK_yandex_direct_ad_group_report_to_ad_group', 'yandex_direct_ad_group_report', 'ad_group_id', 'yandex_direct_ad_group', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_yandex_direct_ad_group_report_to_campaign', 'yandex_direct_ad_group_report', 'campaign_id', 'yandex_direct_campaign', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('{{%yandex_direct_ad}}', [
            'id' => $this->bigInteger(100)->notNull(),
            'ad_group_id' => $this->bigInteger(100)->notNull(),
            'campaign_id' => $this->bigInteger(100)->notNull(),
            'company_id' => $this->integer()->notNull(),
            'name' => $this->string()->defaultValue(null),
            'status' => $this->string()->defaultValue(null),
        ]);
        $this->alterColumn('{{%yandex_direct_ad_report}}', 'ad_id', $this->bigInteger(100)->notNull());
        $this->addPrimaryKey('id_ad_group_campaign_company', '{{%yandex_direct_ad}}', ['id', 'ad_group_id', 'campaign_id', 'company_id']);
        $this->addForeignKey('FK_yandex_direct_ad_to_ad_group', 'yandex_direct_ad', 'ad_group_id', 'yandex_direct_ad_group', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_yandex_direct_ad_to_campaign', 'yandex_direct_ad', 'campaign_id', 'yandex_direct_campaign', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_yandex_direct_ad_to_company', 'yandex_direct_ad', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
        $this->execute("
            INSERT INTO {{%yandex_direct_ad}} (id, ad_group_id, campaign_id, company_id, name, status)
                 SELECT ad_id, ad_group_id, campaign_id, company_id, ad_name, status
                   FROM {{%yandex_direct_ad_report}}
                  GROUP BY ad_id, ad_group_id, campaign_id, company_id
        ");
        $this->dropColumn('{{%yandex_direct_ad_report}}', 'ad_name');
        $this->dropColumn('{{%yandex_direct_ad_report}}', 'status');
        $this->addForeignKey('FK_yandex_direct_ad_report_to_ad', 'yandex_direct_ad_report', 'ad_id', 'yandex_direct_ad', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_yandex_direct_ad_report_to_ad_group', 'yandex_direct_ad_report', 'ad_group_id', 'yandex_direct_ad_group', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_yandex_direct_ad_report_to_campaign', 'yandex_direct_ad_report', 'campaign_id', 'yandex_direct_campaign', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->addColumn('{{%yandex_direct_campaign_report}}', 'campaign_name', $this->string()->notNull()->after('campaign_id'));
        $this->addColumn('{{%yandex_direct_campaign_report}}', 'status', $this->string()->defaultValue(null)->after('campaign_name'));
        $this->addColumn('{{%yandex_direct_campaign_report}}', 'strategy', $this->string()->defaultValue(null)->after('status'));
        $this->addColumn('{{%yandex_direct_campaign_report}}', 'showing_place', $this->string()->defaultValue(null)->after('strategy'));
        $this->addColumn('{{%yandex_direct_campaign_report}}', 'daily_budget_amount', $this->float(2)->unsigned()->defaultValue(null)->after('showing_place'));
        $this->execute("
            UPDATE yandex_direct_campaign_report
              LEFT JOIN yandex_direct_campaign
                     ON yandex_direct_campaign.id = yandex_direct_campaign_report.campaign_id
                    AND yandex_direct_campaign.company_id = yandex_direct_campaign_report.company_id
               SET yandex_direct_campaign_report.campaign_name = yandex_direct_campaign.name,
                   yandex_direct_campaign_report.status = yandex_direct_campaign.status,
                   yandex_direct_campaign_report.strategy = yandex_direct_campaign.strategy,
                   yandex_direct_campaign_report.showing_place = yandex_direct_campaign.showing_place,
                   yandex_direct_campaign_report.daily_budget_amount = yandex_direct_campaign.daily_budget_amount
        ");
        $this->dropForeignKey('FK_yandex_direct_campaign_report_to_campaign', 'yandex_direct_campaign_report');
        $this->dropForeignKey('FK_yandex_direct_ad_group_report_to_campaign', 'yandex_direct_ad_group_report');
        $this->dropForeignKey('FK_yandex_direct_ad_report_to_campaign', 'yandex_direct_ad_report');
        $this->dropForeignKey('FK_yandex_direct_ad_group_to_campaign', 'yandex_direct_ad_group');
        $this->dropForeignKey('FK_yandex_direct_ad_to_campaign', 'yandex_direct_ad');
        $this->dropTable('{{%yandex_direct_campaign}}');

        $this->addColumn('{{%yandex_direct_ad_group_report}}', 'ad_group_name', $this->string()->notNull()->after('campaign_id'));
        $this->addColumn('{{%yandex_direct_ad_group_report}}', 'status', $this->string()->defaultValue(null)->after('ad_group_name'));
        $this->execute("
            UPDATE yandex_direct_ad_group_report
              LEFT JOIN yandex_direct_ad_group
                     ON yandex_direct_ad_group.id = yandex_direct_ad_group_report.ad_group_id
                    AND yandex_direct_ad_group.campaign_id = yandex_direct_ad_group_report.campaign_id
                    AND yandex_direct_ad_group.company_id = yandex_direct_ad_group_report.company_id
               SET yandex_direct_ad_group_report.ad_group_name = yandex_direct_ad_group.name,
                   yandex_direct_ad_group_report.status = yandex_direct_ad_group.status
        ");
        $this->dropForeignKey('FK_yandex_direct_ad_group_report_to_ad_group', 'yandex_direct_ad_group_report');
        $this->dropForeignKey('FK_yandex_direct_ad_report_to_ad_group', 'yandex_direct_ad_report');
        $this->dropForeignKey('FK_yandex_direct_ad_to_ad_group', 'yandex_direct_ad');
        $this->dropTable('{{%yandex_direct_ad_group}}');

        $this->alterColumn('{{%yandex_direct_ad_report}}', 'ad_id', $this->bigInteger(20)->notNull());
        $this->addColumn('{{%yandex_direct_ad_report}}', 'ad_name', $this->string()->defaultValue(null)->after('campaign_id'));
        $this->addColumn('{{%yandex_direct_ad_report}}', 'status', $this->string()->defaultValue(null)->after('ad_name'));
        $this->execute("
            UPDATE yandex_direct_ad_report
              LEFT JOIN yandex_direct_ad
                     ON yandex_direct_ad.id = yandex_direct_ad_report.ad_id
                    AND yandex_direct_ad.ad_group_id = yandex_direct_ad_report.ad_group_id
                    AND yandex_direct_ad.campaign_id = yandex_direct_ad_report.campaign_id
                    AND yandex_direct_ad.company_id = yandex_direct_ad_report.company_id
               SET yandex_direct_ad_report.ad_name = yandex_direct_ad.name,
                   yandex_direct_ad_report.status = yandex_direct_ad.status
        ");
        $this->dropForeignKey('FK_yandex_direct_ad_report_to_ad', 'yandex_direct_ad_report');
        $this->dropTable('{{%yandex_direct_ad}}');
    }
}
