<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%company_api_first_visit}}`.
 */
class m211202_145618_create_company_api_first_visit_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%company_api_first_visit}}', [
            'company_id' => $this->integer()->notNull(),
            'api_partners_id' => $this->integer()->notNull(),
            'visit_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', '{{%company_api_first_visit}}', ['company_id', 'api_partners_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%company_api_first_visit}}');
    }
}
