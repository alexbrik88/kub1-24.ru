<?php

use console\components\db\Migration;
use yii\db\Schema;

class m161003_151840_order_update_selling_price_no_vat extends Migration
{
    public $tOrder = '{{%order}}';

    public function safeUp()
    {
        $this->alterColumn($this->tOrder, 'selling_price_no_vat', $this->bigInteger(20)->defaultValue(0));
        $this->alterColumn($this->tOrder, 'selling_price_with_vat', $this->bigInteger(20)->defaultValue(0));
    }
    
    public function safeDown()
    {
        $this->alterColumn($this->tOrder, 'selling_price_no_vat', $this->bigInteger(20)->notNull());
        $this->alterColumn($this->tOrder, 'selling_price_with_vat', $this->bigInteger(20)->notNull());
    }
}


