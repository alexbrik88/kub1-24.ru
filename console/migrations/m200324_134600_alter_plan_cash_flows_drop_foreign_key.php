<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200324_134600_alter_plan_cash_flows_drop_foreign_key extends Migration
{
    public $tableName = 'plan_cash_flows';

    public function safeUp()
    {
        $this->dropForeignKey($this->tableName . '_first_flow_id', $this->tableName);
        $this->addForeignKey($this->tableName . '_first_flow_id', $this->tableName, 'first_flow_id', $this->tableName, 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_first_flow_id', $this->tableName);
        $this->addForeignKey($this->tableName . '_first_flow_id', $this->tableName, 'first_flow_id', $this->tableName, 'id', 'RESTRICT', 'CASCADE');
    }
}
