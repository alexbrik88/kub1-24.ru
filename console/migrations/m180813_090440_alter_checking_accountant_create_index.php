<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180813_090440_alter_checking_accountant_create_index extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('checking_accountant', 'rs', $this->string(50));
        $this->alterColumn('checking_accountant', 'ks', $this->string(50));
        $this->alterColumn('checking_accountant', 'bik', $this->string(50));

        $this->createIndex('bik', 'checking_accountant', 'bik');
        $this->createIndex('rs', 'checking_accountant', 'rs');
        $this->createIndex('type', 'checking_accountant', 'type');
    }

    public function safeDown()
    {
        $this->dropIndex('bik', 'checking_accountant');
        $this->dropIndex('rs', 'checking_accountant');
        $this->dropIndex('type', 'checking_accountant');

        $this->alterColumn('checking_accountant', 'rs', $this->string());
        $this->alterColumn('checking_accountant', 'ks', $this->string());
        $this->alterColumn('checking_accountant', 'bik', $this->string());
    }
}
