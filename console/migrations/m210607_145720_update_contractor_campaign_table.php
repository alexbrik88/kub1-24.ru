<?php

use yii\db\Exception;
use yii\db\Migration;

class m210607_145720_update_contractor_campaign_table extends Migration
{
    /**
     * @inheritDoc
     * @throws Exception
     */
    public function safeUp(): void
    {
        $this->db->createCommand("
            INSERT IGNORE INTO contractor_campaign (company_id, employee_id, name)
            SELECT company_id, employee_id, source
            FROM contractor
            WHERE source IS NOT NULL
            AND company_id IS NOT NULL
        ")->execute();
    }
}
