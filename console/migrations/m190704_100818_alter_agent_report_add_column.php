<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190704_100818_alter_agent_report_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('agent_report', 'agreement_id', $this->integer()->after('agent_id'));
        $this->addForeignKey('FK_agent_report_agreement', 'agent_report', 'agreement_id', 'agreement', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_agent_report_agreement', 'agent_report');
        $this->dropColumn('agent_report', 'agreement_id');
    }
}
