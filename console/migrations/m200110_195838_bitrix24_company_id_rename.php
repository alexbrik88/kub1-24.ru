<?php

use console\components\db\Migration;

class m200110_195838_bitrix24_company_id_rename extends Migration
{
    public function safeUp()
    {
        $this->_renameColumn('contact', 'company_id', 'bitrix24_company_id');
        $this->_renameColumn('deal', 'company_id', 'bitrix24_company_id');
        $this->_renameColumn('invoice', 'company_id', 'bitrix24_company_id');
    }

    public function safeDown()
    {
        $this->_renameColumn('contact', 'bitrix24_company_id', 'company_id');
        $this->_renameColumn('deal', 'bitrix24_company_id', 'company_id');
        $this->_renameColumn('invoice', 'bitrix24_company_id', 'company_id');
    }

    private function _renameColumn(string $table, string $oldName, string $newName)
    {
        $this->renameColumn(self::tableName($table), $oldName, $newName);
    }

    protected static function tableName(string $table, bool $local = true): string
    {
        if ($local === true) {
            $table = 'bitrix24_' . $table;
        }
        return '{{%' . $table . '}}';
    }

}
