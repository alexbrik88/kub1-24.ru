<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180905_150419_alter_banking_params_alter_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('banking_params', 'param_value', $this->text()->notNull());
    }

    public function safeDown()
    {
        $this->alterColumn('banking_params', 'param_value', $this->string()->notNull());
    }
}
