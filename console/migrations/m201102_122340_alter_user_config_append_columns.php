<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201102_122340_alter_user_config_append_columns extends Migration
{
    public $tableName = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'project_estimate_item_article', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'project_estimate_item_income_price', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'project_estimate_item_income_sum', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'project_estimate_item_sum_diff', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'table_project_estimate_item', $this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'project_estimate_item_article');
        $this->dropColumn($this->tableName, 'project_estimate_item_income_price');
        $this->dropColumn($this->tableName, 'project_estimate_item_income_sum');
        $this->dropColumn($this->tableName, 'project_estimate_item_sum_diff');
        $this->dropColumn($this->tableName, 'table_project_estimate_item');
    }
}
