<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180917_110506_alter_log_alter_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('log', 'attributes_new', $this->text());
        $this->alterColumn('log', 'attributes_old', $this->text());
    }

    public function safeDown()
    {
        $this->alterColumn('log', 'attributes_new', $this->string(4096));
        $this->alterColumn('log', 'attributes_old', $this->string(4096));
    }
}
