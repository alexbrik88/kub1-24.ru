<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160126_141946_update_notification_setDefaultValue_forWhom extends Migration
{
    public $tNotification = 'notification';

    public function safeUp()
    {
        $this->alterColumn($this->tNotification, 'for_whom', $this->integer()->notNull()->defaultValue(0 /*for all*/));
    }
    
    public function safeDown()
    {
        echo 'No down migration available';
    }
}


