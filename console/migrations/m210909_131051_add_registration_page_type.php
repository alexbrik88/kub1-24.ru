<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210909_131051_add_registration_page_type extends Migration
{
    public $tableName = '{{%registration_page_type}}';

    public function safeUp()
    {
        $this->insert($this->tableName, ['id' => 44, 'name' => 'Аналитика / ФинАнализ']);
        $this->insert($this->tableName, ['id' => 45, 'name' => 'Аналитика / Анализ дохода и расхода']);
        $this->insert($this->tableName, ['id' => 46, 'name' => 'Аналитика / Управленка и финансы']);
        $this->insert($this->tableName, ['id' => 47, 'name' => 'Аналитика / ФинКонтроль']);
    }

    public function safeDown()
    {
        $this->delete($this->tableName, ['id' => 47]);
        $this->delete($this->tableName, ['id' => 46]);
        $this->delete($this->tableName, ['id' => 45]);
        $this->delete($this->tableName, ['id' => 44]);
    }
}
