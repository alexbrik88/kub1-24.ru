<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180418_113742_alter_out_invoice_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('out_invoice', 'show_article', $this->boolean()->notNull()->defaultValue(false)->after('return_url'));
    }

    public function safeDown()
    {
        $this->dropColumn('out_invoice', 'show_article');
    }
}
