<?php

use console\components\db\Migration;
use yii\db\Schema;
use common\models\Company;
use common\models\employee\Employee;

class m151221_102557_alter_company_add_time_zone_id extends Migration
{
    public function safeUp()
    {
        $this->addColumn(Company::tableName(), 'time_zone_id', $this->integer()->notNull());
        $this->renameColumn(Employee::tableName(), 'time_zone', 'time_zone_id');
    }
    
    public function safeDown()
    {
        $this->dropColumn(Company::tableName(), 'time_zone_id');
        $this->renameColumn(Employee::tableName(), 'time_zone_id', 'time_zone');
    }
}


