<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180202_091337_alter_out_invoice_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('out_invoice', 'send_with_stamp', $this->boolean()->defaultValue(false)->after('return_url'));
    }

    public function safeDown()
    {
        $this->dropColumn('out_invoice', 'send_with_stamp');
    }
}
