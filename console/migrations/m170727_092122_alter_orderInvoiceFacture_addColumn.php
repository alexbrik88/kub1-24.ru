<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170727_092122_alter_orderInvoiceFacture_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%order_invoice_facture}}', 'quantity', $this->decimal(20, 10)->notNull() . ' AFTER [[order_id]]');

        $this->execute("
            UPDATE
                {{%order_invoice_facture}}
            LEFT JOIN
                {{%order}} ON {{%order}}.[[id]] = {{%order_invoice_facture}}.[[order_id]]
            SET
                {{%order_invoice_facture}}.[[quantity]] = {{%order}}.[[quantity]]
        ");
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%order_invoice_facture}}', 'quantity');
    }
}
