<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%amocrm_widget_user}}`.
 */
class m210422_144704_create_amocrm_widget_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%amocrm_widget_user}}', [
            'account_id' => $this->integer()->notNull()->comment('ID аккаунта, из которого сделан запрос'),
            'user_id' => $this->integer()->notNull()->comment('ID пользователя, из под которого сделан запрос'),
            'token' => $this->char(50)->comment('Токен для доступа из виджета'),
            'employee_id' => $this->integer()->notNull()->comment('ID сотрудника в КУБ'),
            'company_id' => $this->integer()->notNull()->comment('ID компании в КУБ'),
            'client_uuid' => $this->text()->notNull()->comment('UUID интеграции, которая сделала запрос'),
            'jti' => $this->text()->notNull()->comment('UUID токена'),
            'iss' => $this->text()->notNull()->comment('Адрес аккаунта в amoCRM'),
            'aud' => $this->text()->notNull()->comment('Базовый адрес, который сформирован исходя из значения redirect_uri в интеграции'),
            'created_at' => $this->integer()->notNull()->comment('Timestamp, когда подключен пользователь'),
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', '{{%amocrm_widget_user}}', ['account_id', 'user_id']);

        $this->createIndex('token', '{{%amocrm_widget_user}}', 'token', true);

        $this->addForeignKey('fk_amocrm_widget_user__account_id', '{{%amocrm_widget_user}}', 'account_id', '{{%amocrm_widget}}', 'account_id');

        $this->addForeignKey('fk_amocrm_widget_user__employee_company', '{{%amocrm_widget_user}}', [
            'employee_id',
            'company_id',
        ], '{{%employee_company}}', [
            'employee_id',
            'company_id',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%amocrm_widget_user}}');
    }
}
