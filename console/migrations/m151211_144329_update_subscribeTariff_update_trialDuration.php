<?php

use yii\db\Schema;
use yii\db\Migration;

class m151211_144329_update_subscribeTariff_update_trialDuration extends Migration
{
    public $tTariff = 'service_subscribe_tariff';

    public function safeUp()
    {
        $this->update($this->tTariff, [
            'duration_month' => 0,
            'duration_day' => 14,
        ], ['id' => 4, /*trial*/]);
    }
    
    public function safeDown()
    {
        $this->update($this->tTariff, [
            'duration_month' => 1,
            'duration_day' => 0,
        ], ['id' => 4, /*trial*/]);
    }
}
