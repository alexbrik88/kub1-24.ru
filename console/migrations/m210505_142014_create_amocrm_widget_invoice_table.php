<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%amocrm_widget_invoice}}`.
 */
class m210505_142014_create_amocrm_widget_invoice_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%amocrm_widget_invoice}}', [
            'invoice_id' => $this->integer()->notNull(),
            'account_id' => $this->integer()->notNull(),
            'lead_id' => $this->integer()->notNull(),
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', '{{%amocrm_widget_invoice}}', ['invoice_id', 'account_id']);
        $this->createIndex('amo_lead', '{{%amocrm_widget_invoice}}', [
            'account_id',
            'lead_id',
        ]);
        $this->addForeignKey('fk_amocrm_widget_invoice__account_id', '{{%amocrm_widget_invoice}}', 'account_id', '{{%amocrm_widget}}', 'account_id');
        $this->addForeignKey('fk_amocrm_widget_invoice__invoice_id', '{{%amocrm_widget_invoice}}', 'invoice_id', '{{%invoice}}', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%amocrm_widget_invoice}}');
    }
}
