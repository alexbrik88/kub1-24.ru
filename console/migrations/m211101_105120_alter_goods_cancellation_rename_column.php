<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211101_105120_alter_goods_cancellation_rename_column extends Migration
{
    public function safeUp()
    {
        $this->renameColumn('{{%goods_cancellation}}', 'signed_document_date', 'sign_document_date');
    }

    public function safeDown()
    {
        $this->renameColumn('{{%goods_cancellation}}', 'sign_document_date', 'signed_document_date');
    }
}
