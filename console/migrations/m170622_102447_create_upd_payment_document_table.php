<?php

use yii\db\Migration;

/**
 * Handles the creation of table `upd_payment_document`.
 */
class m170622_102447_create_upd_payment_document_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%upd_payment_document}}', [
            'upd_id' => $this->integer()->notNull(),
            'payment_document_number' => $this->string()->notNull(),
            'payment_document_date' => $this->date()->notNull(),
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', '{{%upd_payment_document}}', [
            'upd_id',
            'payment_document_number',
            'payment_document_date',
        ]);
        $this->addForeignKey(
            'FK_updPaymentDocument_upd',
            '{{%upd_payment_document}}',
            'upd_id',
            '{{%upd}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%upd_payment_document}}');
    }
}
