<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180727_131213_alter_scan_document_create_index extends Migration
{
    public function safeUp()
    {
        $this->createIndex('owner', 'scan_document', ['owner_id', 'owner_model']);
    }

    public function safeDown()
    {
        $this->dropIndex('owner', 'scan_document');
    }
}
