<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210822_172913_create_table_payments_request extends Migration
{
    public $table = 'payments_request';
    public $tableStatus = 'payments_request_status';
    public $tablePayments = 'payments_request_order';

    public function safeUp()
    {
        $this->createTable($this->tableStatus, [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL COMMENT "Название"',
        ]);

        $this->batchInsert($this->tableStatus, ['id', 'name'], [
            [1, 'Согласование'],
            [2, 'Подписание'],
            [3, 'Отклонена'],
            [4, 'Подписана'],
        ]);

        $this->createTable($this->table, [
            // заявка
            'id' => Schema::TYPE_PK,
            'company_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'status_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'author_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Автор (инициатор)"',
            'template_id' => Schema::TYPE_INTEGER . ' COMMENT "Шаблон"',
            'document_date' => Schema::TYPE_DATE . ' NOT NULL COMMENT "Дата заявки"',
            'document_number' => Schema::TYPE_STRING . ' NOT NULL COMMENT "Номер заявки"',
            'document_additional_number' => Schema::TYPE_STRING . ' COMMENT "Доп. номер заявки"',
            // платеж
            'payment_date' => Schema::TYPE_DATE . ' NOT NULL COMMENT "Дата платежа"',
            'payment_source' => Schema::TYPE_STRING . ' COMMENT "Р/счета или кассы"',
        ]);

        $this->addForeignKey('FK_' . $this->table . '_to_company', $this->table, 'company_id', 'company', 'id');
        $this->addForeignKey('FK_' . $this->table . '_to_employee_author', $this->table, 'author_id', 'employee', 'id');
        $this->addForeignKey('FK_' . $this->table . '_to_status', $this->table, 'status_id', 'payments_request_status', 'id');


        //////////
        $this->createTable($this->tablePayments, [
            'payments_request_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Заявка"',
            'plan_operation_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Платеж"',
            'number' => Schema::TYPE_INTEGER
        ]);

        $this->addPrimaryKey('PK_' . $this->tablePayments, $this->tablePayments, ['payments_request_id', 'plan_operation_id']);
        $this->addForeignKey('FK_' . $this->tablePayments . '_to_request', $this->tablePayments, 'payments_request_id', $this->table, 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_' . $this->tablePayments . '_to_operation', $this->tablePayments, 'plan_operation_id', 'plan_cash_flows', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_' . $this->tablePayments . '_to_request', $this->tablePayments);
        $this->dropForeignKey('FK_' . $this->tablePayments . '_to_operation', $this->tablePayments);
        $this->dropTable($this->tablePayments);

        $this->dropForeignKey('FK_' . $this->table . '_to_company', $this->table);
        $this->dropForeignKey('FK_' . $this->table . '_to_employee_author', $this->table);
        $this->dropForeignKey('FK_' . $this->table . '_to_status', $this->table);
        $this->dropTable($this->table);

        $this->dropTable($this->tableStatus);
    }
}
