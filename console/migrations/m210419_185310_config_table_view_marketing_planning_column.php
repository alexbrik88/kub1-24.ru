<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210419_185310_config_table_view_marketing_planning_column extends Migration
{
    public $table = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->table, 'table_view_marketing_planning', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'table_view_marketing_planning');
    }
}
