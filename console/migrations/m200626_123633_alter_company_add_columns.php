<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200626_123633_alter_company_add_columns extends Migration
{
    public $tableName = 'company';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'form_google_analytics_id', $this->string()->defaultValue(null));
        $this->addColumn($this->tableName, 'form_utm', $this->string()->defaultValue(null));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'form_utm');
        $this->dropColumn($this->tableName, 'form_google_analytics_id');
    }
}
