<?php

use console\components\db\Migration;
use yii\db\Schema;

class m220124_151028_alter_cash_bank_flows_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cash_bank_flows}}', 'linkage_id', $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%cash_bank_flows}}', 'linkage_id');
    }
}
