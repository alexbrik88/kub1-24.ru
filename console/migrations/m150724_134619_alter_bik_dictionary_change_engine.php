<?php

use yii\db\Schema;
use yii\db\Migration;

class m150724_134619_alter_bik_dictionary_change_engine extends Migration
{
    public function safeUp()
    {
        $this->execute('ALTER TABLE bik_dictionary ENGINE = MYISAM;');
    }
    
    public function safeDown()
    {
        $this->execute('ALTER TABLE bik_dictionary ENGINE = InnoDB;');
    }
}
