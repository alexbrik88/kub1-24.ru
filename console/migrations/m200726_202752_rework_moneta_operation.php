<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200726_202752_rework_moneta_operation extends Migration
{
    public function safeUp()
    {
        $this->renameTable('{{%moneta_operation}}', '{{%acquiring_operation}}');
    }

    public function safeDown()
    {
        $this->renameTable('{{%acquiring_operation}}', '{{%moneta_operation}}');
    }
}
