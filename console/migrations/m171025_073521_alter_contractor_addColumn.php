<?php

use console\components\db\Migration;
use yii\db\Schema;

class m171025_073521_alter_contractor_addColumn extends Migration
{
    public $tableName = 'contractor';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'physical_no_patronymic', $this->boolean()->defaultValue(false)->after('physical_patronymic'));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'physical_no_patronymic');
    }
}
