<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181009_073044_alter_agreement_template extends Migration
{
    public function safeUp()
    {
		$this->alterColumn('agreement_template', 'document_header', 'text');
		$this->alterColumn('agreement_template', 'document_body', 'text');
		$this->alterColumn('agreement_template', 'document_requisites_customer', 'text');
		$this->alterColumn('agreement_template', 'document_requisites_executer', 'text');
		$this->alterColumn('agreement_template', 'other_patterns', 'text');
		$this->alterColumn('agreement_template', 'comment_internal', 'text');
    }
    
    public function safeDown()
    {
		return false;
    }
}


