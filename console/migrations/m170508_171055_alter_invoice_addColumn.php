<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170508_171055_alter_invoice_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%invoice}}', 'basis_document_type_id', $this->integer()->defaultValue(null) . ' AFTER [[basis_document_date]]');

        $this->addForeignKey('FK_invoice_agreementType', '{{%invoice}}', 'basis_document_type_id', '{{%agreement_type}}', 'id');

        $this->update('{{%invoice}}', ['basis_document_type_id' => 1], "
            [[basis_document_name]] IS NOT NULL
            AND TRIM([[basis_document_name]]) <> ''
            AND TRIM([[basis_document_name]]) <> 'Счет'
        ");
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('FK_invoice_agreementType', '{{%invoice}}');
        $this->dropColumn('{{%invoice}}', 'basis_document_type_id');
    }
}
