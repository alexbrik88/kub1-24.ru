<?php

use console\components\db\Migration;
use yii\db\Schema;

class m171114_083554_alter_employee_company_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn(
            'employee_company',
            'is_product_admin',
            $this->boolean()->notNull()->defaultValue(true)->after('employee_role_id')
        );
    }

    public function safeDown()
    {
        $this->dropColumn('employee_company', 'is_product_admin');
    }
}
