<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190805_134836_update_bank_statement_autoload_mode extends Migration
{
    public function safeUp()
    {
        $this->execute('UPDATE {{bank_statement_autoload_mode}} SET [[name]] = "Каждый пн, вт, ср, чт, пт", [[week_days]] = "1,2,3,4,5" WHERE [[id]] = 1');
        $this->execute('UPDATE {{bank_statement_autoload_mode}} SET [[name]] = "Каждый вт, ср, чт, пт, сб", [[week_days]] = "2,3,4,5,6" WHERE [[id]] = 2');
        $this->execute('INSERT INTO {{bank_statement_autoload_mode}} ([[id]], [[name]], [[week_days]]) VALUES (3, "Каждый пн, ср, пт", "1,3,5")');
        $this->execute('UPDATE {{checking_accountant}} SET [[autoload_mode_id]] = 3 WHERE [[autoload_mode_id]] = 2');
    }

    public function safeDown()
    {
        $this->execute('UPDATE {{checking_accountant}} SET [[autoload_mode_id]] = 2 WHERE [[autoload_mode_id]] = 3');
        $this->execute('DELETE FROM {{bank_statement_autoload_mode}} WHERE [[id]] = 3');
        $this->execute('UPDATE {{bank_statement_autoload_mode}} SET [[name]] = "Каждый пн, ср, пт", [[week_days]] = "1,3,5" WHERE [[id]] = 2');
        $this->execute('UPDATE {{bank_statement_autoload_mode}} SET [[name]] = "Каждый день", [[week_days]] = "1,2,3,4,5,6,7" WHERE [[id]] = 1');
    }
}
