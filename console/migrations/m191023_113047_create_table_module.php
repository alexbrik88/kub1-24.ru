<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191023_113047_create_table_module extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('service_module', [
            'id' => $this->primaryKey(),
            'name' => $this->string()
        ], $tableOptions);

        $this->batchInsert('service_module', ['id', 'name'], [
            [1, 'Счета'],
            [2, 'Логистика'],
            [3, 'Модуль B2B'],
            [4, 'Бухгалтерия ИП на УСН 6%'],
            [5, 'Аналитика бизнеса'],
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('service_module');
    }
}
