<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180618_075135_alter_company_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('company', 'ip_certificate_number', $this->string()->after('ip_patronymic_initials'));
        $this->addColumn('company', 'ip_certificate_date', $this->string()->after('ip_certificate_number'));
    }

    public function safeDown()
    {
        $this->dropColumn('company', 'ip_certificate_number');
        $this->dropColumn('company', 'ip_certificate_date');
    }
}
