<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200206_133717_alter_price_list_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('price_list', 'send_count', $this->smallInteger()->notNull()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('price_list', 'send_count');
    }
}
