<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181206_114208_add_request_columns_to_user_config extends Migration
{
    public $tableName = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'logistics_request_unloading_date', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'logistics_request_goods', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'logistics_request_customer_debt', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn($this->tableName, 'logistics_request_carrier_debt', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn($this->tableName, 'logistics_request_ttn', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn($this->tableName, 'logistics_request_author_id', $this->boolean()->notNull()->defaultValue(false));

        $this->addCommentOnColumn($this->tableName, 'logistics_request_unloading_date', 'Показ колонки "Дата разгрузки" в списке заявок');
        $this->addCommentOnColumn($this->tableName, 'logistics_request_goods', 'Показ колонки "Груз" в списке заявок');
        $this->addCommentOnColumn($this->tableName, 'logistics_request_customer_debt', 'Показ колонки "Долг заказчику" в списке заявок');
        $this->addCommentOnColumn($this->tableName, 'logistics_request_carrier_debt', 'Показ колонки "Долг перевозчику" в списке заявок');
        $this->addCommentOnColumn($this->tableName, 'logistics_request_ttn', 'Показ колонки "ТТН" в списке заявок');
        $this->addCommentOnColumn($this->tableName, 'logistics_request_author_id', 'Показ колонки "Ответственный" в списке заявок');
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'logistics_request_unloading_date');
        $this->dropColumn($this->tableName, 'logistics_request_goods');
        $this->dropColumn($this->tableName, 'logistics_request_customer_debt');
        $this->dropColumn($this->tableName, 'logistics_request_carrier_debt');
        $this->dropColumn($this->tableName, 'logistics_request_ttn');
        $this->dropColumn($this->tableName, 'logistics_request_author_id');
    }
}


