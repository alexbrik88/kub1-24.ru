<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200321_154728_create_table_plan_cash_contractor extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('plan_cash_contractor', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'contractor_id' => $this->integer()->notNull(),
            'plan_type' => $this->tinyInteger()->notNull()->comment('0 - по счетам, 1 - по предыдущим периодам'),
            'flow_type' => $this->boolean()->notNull()->comment('0 - Расход, 1 - Приход'),
            'payment_type' => $this->tinyInteger()->notNull()->comment('1 - Банк, 2 - Касса, 3 - E-money'),
            'item_id' => $this->integer()->notNull()->comment('Статья'),
            'payment_delay' => $this->integer()->notNull()->defaultValue(10),
            'created_at' => $this->integer()->unsigned()->notNull(),
            'updated_at' => $this->integer()->unsigned()->notNull(),
        ], $tableOptions);

        $this->addForeignKey('FK_plan_cash_contractor_to_company', 'plan_cash_contractor', 'company_id', 'company', 'id', 'CASCADE');
        $this->addForeignKey('FK_plan_cash_contractor_to_contractor', 'plan_cash_contractor', 'contractor_id', 'contractor', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_plan_cash_contractor_to_contractor', 'plan_cash_contractor');
        $this->dropForeignKey('FK_plan_cash_contractor_to_company', 'plan_cash_contractor');
        $this->dropTable('plan_cash_contractor');
    }
}
