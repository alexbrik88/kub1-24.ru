<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210518_084639_alter_employee_signature_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('employee_signature', 'status_id', $this->tinyInteger());

        $this->execute('UPDATE employee_signature SET status_id = 1');
    }

    public function safeDown()
    {
        $this->dropColumn('employee_signature', 'status_id');
    }
}
