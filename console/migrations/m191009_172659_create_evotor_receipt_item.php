<?php

use common\models\evotor\Product;
use console\components\db\Migration;

class m191009_172659_create_evotor_receipt_item extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{evotor_receipt_item}}', [
            'receipt_id' => "BINARY(16) NOT NULL COMMENT 'ID чека'",
            'product_id' => "BINARY(16) NOT NULL COMMENT 'UUID товара'",
            'name' => $this->string(100)->notNull()->comment('Название товара'),
            'item_type' => "ENUM('" . implode("','", Product::TYPE) . "') NOT NULL DEFAULT '" . Product::TYPE_NORMAL . "' COMMENT 'Тип документа'",
            'measure_name' => $this->string(10)->notNull()->comment('Единица измерения'),
            'quantity' => $this->integer()->unsigned()->notNull()->comment('Количество'),
            'price' => $this->float()->unsigned()->notNull()->defaultValue(0)->comment('Отпускная цена'),
            'cost_price' => $this->float()->unsigned()->notNull()->defaultValue(0)->comment('Закупочная цена'),
            'sum_price' => $this->float()->unsigned()->notNull()->defaultValue(0)->comment('Итоговая сумма позиции'),
            'tax' => $this->float()->unsigned()->notNull()->defaultValue(0)->comment('Сумма налога'),
            'tax_percent' => $this->float()->unsigned()->notNull()->defaultValue(0)->comment('Сумма налога, выраженная в проценах'),
            'discount' => $this->float()->unsigned()->notNull()->defaultValue(0)->comment('Сумма скидки'),
        ], "COMMENT 'Интеграция Эвотор: позиции чека'");
        $this->addPrimaryKey('evotor_receipt_item_pk', '{{evotor_receipt_item}}', ['receipt_id', 'product_id']);
        $this->addForeignKey(
            'evotor_receipt_item_receipt_id', '{{evotor_receipt_item}}', 'receipt_id',
            '{{evotor_receipt}}', 'id', 'CASCADE', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable('{{evotor_receipt_item}}');
    }
}
