<?php

use yii\db\Migration;

class m200807_012358_create_crm_task_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%crm_task}}';

    /**
     * @inheritDoc
     */
    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'task_id' => $this->bigPrimaryKey(),
            'client_id' => $this->bigInteger(),
            'client_event_id' => $this->bigInteger(),
            'company_id' => $this->integer()->notNull(),
            'employee_id' => $this->integer()->notNull(),
            'description' => $this->text()->notNull(),
            'status' => $this->tinyInteger(1)->notNull()->defaultValue(1),
            'date_begin' => $this->date()->notNull(),
            'date_end' => $this->date()->null(),
            'time' => $this->time()->notNull(),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->createForeignKey('crm_client', ['client_id' => 'client_id']);
        $this->createForeignKey('crm_client_event', ['client_event_id' => 'client_event_id']);
        $this->createForeignKey('employee_company', ['employee_id' => 'employee_id', 'company_id' => 'company_id']);

        $this->createIndex('ck_status', self::TABLE_NAME, ['status']);
        $this->createIndex('ck_date_begin', self::TABLE_NAME, ['date_begin']);
        $this->createIndex('ck_date_end', self::TABLE_NAME, ['date_end']);
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }

    /**
     * Создать внешний ключ
     *
     * @param string $table Имя таблицы внешнего ключа
     * @param string[] $columns Названия столбцов текущей таблицы
     * @return void
     */
    private function createForeignKey(string $table, array $columns): void
    {
        $key = implode('_', $columns);
        $this->createIndex('ck_' . $key, self::TABLE_NAME, array_keys($columns));
        $this->addForeignKey(
            "fk_crm_task__{$table}_{$key}",
            self::TABLE_NAME,
            array_keys($columns),
            "{{%{$table}}}",
            array_values($columns),
            'CASCADE',
            'CASCADE'
        );
    }
}
