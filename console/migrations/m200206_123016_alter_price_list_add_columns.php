<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200206_123016_alter_price_list_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%price_list}}', 'auto_update_prices', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn('{{%price_list}}', 'auto_update_quantity', $this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%price_list}}', 'auto_update_prices');
        $this->dropColumn('{{%price_list}}', 'auto_update_quantity');
    }
}
