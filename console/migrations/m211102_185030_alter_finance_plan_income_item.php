<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211102_185030_alter_finance_plan_income_item extends Migration
{
    public function safeUp()
    {
        $table = 'finance_plan_income_item';
        $this->addColumn($table, 'income_item_id', $this->integer()->after('model_id'));
        $this->addForeignKey("FK_{$table}_income_item", $table, 'income_item_id', 'invoice_income_item', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $table = 'finance_plan_income_item';
        $this->dropForeignKey("FK_{$table}_income_item", $table);
        $this->dropColumn($table, 'income_item_id');
    }
}
