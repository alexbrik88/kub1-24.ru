<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190927_100058_insert_registration_page_type extends Migration
{
    public function safeUp()
    {
        $this->update('{{%registration_page_type}}', [
            'name' => 'МТС выставление счетов',
        ], [
            'id' => 23,
        ]);

        $this->batchInsert('{{%registration_page_type}}', ['id', 'name'], [
            [24, 'МТС бухгалтерия ИП'],
            [25, 'МТС модуль В2В'],
        ]);
    }

    public function safeDown()
    {
        $this->update('{{%registration_page_type}}', [
            'name' => 'МТС',
        ], [
            'id' => 23,
        ]);

        $this->delete('{{%registration_page_type}}', ['id' => [24, 25]]);
    }
}
