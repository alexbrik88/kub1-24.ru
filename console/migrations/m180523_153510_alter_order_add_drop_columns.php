<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180523_153510_alter_order_add_drop_columns extends Migration
{
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE {{order}}
                DROP COLUMN [[currency_price_no_vat]],
                DROP COLUMN [[currency_price_with_vat]];
        ");
    }

    public function safeDown()
    {
        $this->execute("
            ALTER TABLE {{order}}
                ADD COLUMN [[currency_price_no_vat]] DECIMAL(22,2) NOT NULL DEFAULT '0.00' AFTER [[mass_gross]],
                ADD COLUMN [[currency_price_with_vat]] DECIMAL(22,2) NOT NULL DEFAULT '0.00' AFTER [[currency_price_no_vat]];
        ");

        $this->execute("
            UPDATE {{order}}
            LEFT JOIN {{invoice}} ON {{invoice}}.[[id]] = {{order}}.[[invoice_id]]
            SET {{order}}.[[currency_price_no_vat]] = ROUND(
                    IF({{invoice}}.[[type]] = 1, {{order}}.[[purchase_price_no_vat]], {{order}}.[[selling_price_no_vat]])
                        * {{invoice}}.[[currency_amount]] / {{invoice}}.[[currency_rate]]
                , 2),
                {{order}}.[[currency_price_with_vat]] = ROUND(
                    IF({{invoice}}.[[type]] = 1, {{order}}.[[purchase_price_with_vat]], {{order}}.[[selling_price_with_vat]])
                        * {{invoice}}.[[currency_amount]] / {{invoice}}.[[currency_rate]]
                , 2);
        ");
    }
}
