<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170717_205700_add_api_key_to_employee extends Migration
{
    public $tableName = 'employee';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'api_key', $this->string()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'api_key');
    }
}


