<?php

use console\components\db\Migration;
use yii\db\Schema;

class m171024_105616_update_chat_volume extends Migration
{
    public $tableName = 'chat_volume';

    public function safeUp()
    {
        $this->dropForeignKey($this->tableName . '_to_employee_id', $this->tableName);
        $this->alterColumn($this->tableName, 'to_employee_id', $this->string()->notNull());
    }
    
    public function safeDown()
    {
        $this->alterColumn($this->tableName, 'to_employee_id', $this->integer()->notNull());
        $this->addForeignKey($this->tableName . '_to_employee_id', $this->tableName, 'to_employee_id', 'user', 'id', 'RESTRICT', 'CASCADE');
    }
}


