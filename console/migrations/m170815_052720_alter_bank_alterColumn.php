<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170815_052720_alter_bank_alterColumn extends Migration
{
    public function safeUp()
    {
    	$this->alterColumn('{{%bank}}', 'bik', $this->string(9)->notNull());

    	$this->createIndex('bik', '{{%bank}}', 'bik', true);
    }
    
    public function safeDown()
    {
    	$this->alterColumn('{{%bank}}', 'bik', $this->string());

    	$this->dropIndex('bik', '{{%bank}}');
    }
}
