<?php

use yii\db\Schema;
use yii\db\Migration;

class m151117_102541_update_servicePaymentType extends Migration
{
    public $tPaymentType = 'service_payment_type';

    public function safeUp()
    {
        $this->update($this->tPaymentType, [
            'id' => 4,
        ], 'id = 3');

        $this->insert($this->tPaymentType, [
            'id' => 3,
            'name' => 'Выставить счёт',
        ]);
    }
    
    public function safeDown()
    {
        $this->delete($this->tPaymentType, 'id = 3');

        $this->update($this->tPaymentType, [
            'id' => 3,
        ], 'id = 4');
    }
}
