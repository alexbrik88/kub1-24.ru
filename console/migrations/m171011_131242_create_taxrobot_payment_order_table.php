<?php

use yii\db\Migration;

/**
 * Handles the creation of table `taxrobot_payment_order`.
 */
class m171011_131242_create_taxrobot_payment_order_table extends Migration
{
    public $table = 'taxrobot_payment_order';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'payment_id' => $this->integer()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'year' => $this->integer(4)->notNull(),
            'quarter' => $this->integer(1)->notNull(),
        ]);

        $this->addForeignKey($this->table . '_payment', $this->table, 'payment_id', 'service_payment', 'id', 'CASCADE');
        $this->addForeignKey($this->table . '_company', $this->table, 'company_id', 'company', 'id');
        $this->createIndex ('KEY_company_year_quarter', $this->table, ['company_id', 'year', 'quarter']);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->table);
    }
}
