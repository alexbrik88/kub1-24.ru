<?php

use yii\db\Schema;
use yii\db\Migration;

class m150423_142410_alter_ActStatus_renameColumn_ActStatus extends Migration
{
    public function up()
    {
        $this->renameColumn('act_status', 'act_title', 'name');
    }

    public function down()
    {
        $this->renameColumn('act_status', 'name', 'act_title');

    }
}
