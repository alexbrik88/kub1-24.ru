<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190624_164031_add_table_agent_payment_type extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('contractor_agent_payment_type', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ], $tableOptions);

        $this->batchInsert('contractor_agent_payment_type', ['id', 'name',], [
            [1, 'Ежемесячный платеж'],
            [2, 'Разовый платеж'],
        ]);
    }

    public function down()
    {
        $this->dropTable('contractor_agent_payment_type');
    }
}
