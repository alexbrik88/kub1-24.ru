<?php

use yii\db\Schema;
use yii\db\Migration;

class m150416_102428_add_Company_column_companyProductTypes extends Migration
{
    public function up()
    {
        $this->addColumn('company', 'company_product_types', Schema::TYPE_STRING . '(45) DEFAULT ""');
        $this->dropTable('company_to_company_product_type');
    }

    public function down()
    {
        $this->dropColumn('company', 'company_product_types');

        $this->createTable('company_to_company_product_type', [
            'id' => Schema::TYPE_PK,
            'company_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'company_product_type_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);
        $this->addForeignKey('company_to_product_type_company', 'company_to_company_product_type', 'company_id', 'company', 'id');
        $this->addForeignKey('company_to_product_type_company_product_type', 'company_to_company_product_type', 'company_product_type_id', 'company_product_type', 'id');
    }
}
