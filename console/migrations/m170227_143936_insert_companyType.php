<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170227_143936_insert_companyType extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%company_type}}', [
            'id' => 27,
            'name_short' => 'СПК',
            'name_full' => 'Сельскохозяйственный производственный кооператив',
            'in_company' => false,
            'in_contractor' => true,
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%company_type}}', ['id' => 27]);
    }
}


