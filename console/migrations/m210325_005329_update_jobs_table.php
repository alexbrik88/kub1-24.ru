<?php

use yii\db\Migration;
use yii\db\Exception;

class m210325_005329_update_jobs_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%jobs}}';

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function safeUp(): void
    {
        if ($this->db->getTableSchema(self::TABLE_NAME, true) === null) {
            return;
        }

        $rows = $this->getRows();

        if (empty($rows)) {
            return;
        }

        array_walk($rows, [$this, 'updateRow']);
    }

    /**
     * @param array $row
     * @return string
     */
    private function createParams(array $row): string
    {
        $params = [];

        if ($row['date_from']) {
            $params['from'] = $row['date_from'] . ' 00:00:00';
        }

        if ($row['date_to']) {
            $params['to'] = $row['date_to'] . ' 23:59:59';
        }

        if ($row['identifier']) {
            $params['identifier'] = $row['identifier'];
        }

        return json_encode($params, JSON_UNESCAPED_UNICODE);
    }

    /**
     * @return array
     * @throws Exception
     */
    private function getRows(): array
    {
        $sql = <<<SQL
            SELECT id, date_from, date_to, identifier
            FROM jobs
            WHERE date_from IS NOT NULL
            OR date_to IS NOT NULL
            OR identifier IS NOT NULL
SQL;

        return $this->db->createCommand($sql)->queryAll();
    }

    /**
     * @param array $row
     * @return void
     */
    private function updateRow(array $row): void
    {
        $this->update(
            self::TABLE_NAME,
            ['params' => $this->createParams($row)],
            ['id' => $row['id']]
        );
    }
}
