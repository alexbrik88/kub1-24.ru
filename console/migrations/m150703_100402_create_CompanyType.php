<?php

use yii\db\Schema;
use yii\db\Migration;

class m150703_100402_create_CompanyType extends Migration
{
    public function safeUp()
    {
        $this->createTable('company_type', [
            'id' => Schema::TYPE_PK,
            'name_short' => Schema::TYPE_STRING . '(45) NOT NULL',
            'name_full' => Schema::TYPE_STRING . '(255) NOT NULL',
        ], ' COMMENT "нумерация здесь не совпадает с company.company_form."');

        $this->batchInsert('company_type', ['id', 'name_short', 'name_full'], [
            [1, 'ИП', 'Индивидуальный предприниматель'],
            [2, 'ООО', 'Общество с ограниченной ответственностью'],
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('company_type');
    }
}
