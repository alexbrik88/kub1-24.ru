<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170721_103615_update_title_in_product extends Migration
{
    public $tProduct = 'product';
    public $tUploadedDocumentOrder = 'uploaded_document_order';
    public $tOrder = 'order';

    public function safeUp()
    {
        $this->alterColumn($this->tProduct, 'title', $this->text()->notNull());
        $this->alterColumn($this->tUploadedDocumentOrder, 'name', $this->text()->notNull());
        $this->alterColumn($this->tOrder, 'product_title', $this->text()->notNull());
    }

    public function safeDown()
    {
        $this->alterColumn($this->tProduct, 'title', $this->string(200)->notNull());
        $this->alterColumn($this->tUploadedDocumentOrder, 'name', $this->string()->notNull());
        $this->alterColumn($this->tOrder, 'product_title', $this->string()->notNull());
    }
}


