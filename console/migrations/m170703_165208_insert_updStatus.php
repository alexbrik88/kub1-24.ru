<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170703_165208_insert_updStatus extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%upd_status}}', [
            'id' => 5,
            'name' => 'Отменён',
        ]);
    }
    
    public function safeDown()
    {
        $this->delete('{{%upd_status}}', 'id = 5');
    }
}
