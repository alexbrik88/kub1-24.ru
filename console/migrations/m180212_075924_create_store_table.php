<?php

use yii\db\Migration;

/**
 * Handles the creation of table `store`.
 */
class m180212_075924_create_store_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('store', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'responsible_employee_id' => $this->integer(),
            'is_main' => $this->boolean()->notNull()->defaultValue(false),
            'is_closed' => $this->boolean()->notNull()->defaultValue(false),
            'name' => $this->string(50)->notNull(),
        ]);

        $this->addForeignKey('FK_store_company', 'store', 'company_id', 'company', 'id');
        $this->addForeignKey('FK_store_employee', 'store', 'responsible_employee_id', 'employee', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('store');
    }
}
