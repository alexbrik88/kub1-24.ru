<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170217_132841_alter_act_add_nds_and_amount_without_nds extends Migration
{
    public function safeUp()
    {
        $this->addColumn('act', 'order_sum_without_nds', $this->integer()->defaultValue(0));
        $this->addColumn('act', 'order_nds', $this->integer()->defaultValue(0));
    }
    
    public function safeDown()
    {
        $this->dropColumn('act', 'order_sum_without_nds');
        $this->dropColumn('act', 'order_nds');
    }
}


