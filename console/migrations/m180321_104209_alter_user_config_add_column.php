<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180321_104209_alter_user_config_add_column extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('user_config', 'contr_inv_scan', $this->boolean(1)->notNull()->defaultValue(false));
        $this->addColumn('user_config', 'contr_inv_paydate', $this->boolean(1)->notNull()->defaultValue(false));
        $this->addColumn('user_config', 'contr_inv_paylimit', $this->boolean(1)->notNull()->defaultValue(true));
        $this->addColumn('user_config', 'contr_inv_act', $this->boolean(1)->notNull()->defaultValue(true));
        $this->addColumn('user_config', 'contr_inv_paclist', $this->boolean(1)->notNull()->defaultValue(true));
        $this->addColumn('user_config', 'contr_inv_author', $this->boolean(1)->notNull()->defaultValue(false));

        $this->addCommentOnColumn('user_config', 'contr_inv_scan', 'Показ колонки "Скан" в списке счетов контрагента');
        $this->addCommentOnColumn('user_config', 'contr_inv_paydate', 'Показ колонки "Дата оплаты" в списке счетов контрагента');
        $this->addCommentOnColumn('user_config', 'contr_inv_paylimit', 'Показ колонки "Оплатить до" в списке счетов контрагента');
        $this->addCommentOnColumn('user_config', 'contr_inv_act', 'Показ колонки "Акт" в списке счетов контрагента');
        $this->addCommentOnColumn('user_config', 'contr_inv_paclist', 'Показ колонки "Товарная накладная" в списке счетов контрагента');
        $this->addCommentOnColumn('user_config', 'contr_inv_author', 'Показ колонки "Ответственный" в списке счетов контрагента');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn('user_config', 'contr_inv_scan');
        $this->dropColumn('user_config', 'contr_inv_paydate');
        $this->dropColumn('user_config', 'contr_inv_paylimit');
        $this->dropColumn('user_config', 'contr_inv_act');
        $this->dropColumn('user_config', 'contr_inv_paclist');
        $this->dropColumn('user_config', 'contr_inv_author');
    }
}
