<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210723_074132_alter_finance_model_2_tables extends Migration
{
    const PRIME_COST_ITEM = 'finance_plan_prime_cost_item';
    const EXPENDITURE_ITEM = 'finance_plan_expenditure_item';

    public function safeUp()
    {
        $this->alterColumn(self::PRIME_COST_ITEM, 'action', $this->float());
        $this->alterColumn(self::PRIME_COST_ITEM, 'relation_type', $this->tinyInteger()->unsigned()->notNull()->defaultValue(0));

        $this->addColumn(self::EXPENDITURE_ITEM, 'action', $this->float()->after('expenditure_item_id'));
        $this->addColumn(self::EXPENDITURE_ITEM, 'relation_type', $this->tinyInteger()->unsigned()->notNull()->defaultValue(0)->after('expenditure_item_id'));
    }

    public function safeDown()
    {
        $this->alterColumn(self::PRIME_COST_ITEM, 'action', $this->tinyInteger()->unsigned()->notNull()->defaultValue(1));

        $this->dropColumn(self::EXPENDITURE_ITEM, 'action');
        $this->dropColumn(self::EXPENDITURE_ITEM, 'relation_type');
    }
}
