<?php

use yii\db\Schema;
use yii\db\Migration;

class m150702_125758_alter_paymentOrder_addColumn_paymentLimitDate extends Migration
{
    public function safeUp()
    {
        $this->addColumn('payment_order', 'payment_limit_date', Schema::TYPE_DATE);
    }
    
    public function safeDown()
    {
        $this->dropColumn('payment_order', 'payment_limit_date');
    }
}
