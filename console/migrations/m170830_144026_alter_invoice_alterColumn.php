<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170830_144026_alter_invoice_alterColumn extends Migration
{
    public function safeUp()
    {
    	$this->alterColumn('{{%invoice}}', 'company_chief_accountant_lastname', $this->string(45));
    }
    
    public function safeDown()
    {
    	$this->alterColumn('{{%invoice}}', 'company_chief_accountant_lastname', $this->string(10));
    }
}
