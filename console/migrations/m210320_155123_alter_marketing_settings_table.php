<?php

use yii\db\Migration;

class m210320_155123_alter_marketing_settings_table extends Migration
{
    private const TABLE_NAME = '{{%marketing_settings}}';

    /**
     * @inheritDoc
     */
    public function up()
    {
        $this->renameColumn(self::TABLE_NAME, 'channel_id', 'command_type');
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        $this->renameColumn(self::TABLE_NAME, 'command_type', 'channel_id');
    }
}
