<?php

use console\components\db\Migration;
use yii\db\Schema;

class m161213_092200_alter_specificDocument_addColumn_companyId extends Migration
{
    public function safeUp()
    {
        $this->addColumn('specific_document', 'company_id', $this->integer()->defaultValue(null));
        $this->addForeignKey('FK_specific_document_company', 'specific_document', 'company_id', 'company', 'id', 'SET NULL', 'CASCADE');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('FK_specific_document_company', 'specific_document');
        $this->dropColumn('specific_document', 'company_id');
    }
}


