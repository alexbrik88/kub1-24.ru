<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210909_145725_alter_proxy_change_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('proxy', 'document_number', $this->string()->notNull());
    }

    public function safeDown()
    {
        $this->alterColumn('proxy', 'document_number', $this->integer()->notNull());
    }
}
