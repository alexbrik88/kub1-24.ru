<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210520_221133_alter_export_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('export', 'source_id', $this->tinyInteger()->defaultValue(1)->after('company_id'));
    }

    public function safeDown()
    {
        $this->dropColumn('export', 'source_id');
    }
}
