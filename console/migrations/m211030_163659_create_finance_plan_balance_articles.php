<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211030_163659_create_finance_plan_balance_articles extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%finance_plan_balance_article}}';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(11),
            'model_id' => $this->integer()->notNull(),
            //'created_at' => $this->integer(11)->unsigned()->notNull(),
            //'updated_at' => $this->integer(11)->unsigned()->notNull(),
            'type' => $this->boolean()->unsigned()->notNull(),
            'status' => $this->tinyInteger()->unsigned()->notNull(),
            'name' => $this->string()->notNull(),
            'category' => $this->tinyInteger()->unsigned()->notNull(),
            'subcategory' => $this->tinyInteger()->unsigned()->notNull(),
            'useful_life_in_month' => $this->integer(11)->unsigned()->defaultValue(null),
            'count' => $this->integer(11)->unsigned()->notNull(),
            'purchased_at' => $this->date()->notNull(),
            'sold_at' => $this->date()->defaultValue(null),
            'written_off_at' => $this->date()->defaultValue(null),
            'amount' => $this->bigInteger(20)->unsigned()->notNull(),
            'description' => $this->text()->defaultValue(null),
        ]);

        $this->addForeignKey('FK_finance_plan_balance_article_to_finance_plan', self::TABLE_NAME, 'model_id', 'finance_plan', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
