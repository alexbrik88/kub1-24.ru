<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%service_subscribe_contains_tariff}}`.
 */
class m200305_063817_create_service_subscribe_contains_tariff_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%service_subscribe_contains_tariff}}', [
            'id' => $this->primaryKey(),
            'tariff_id' => $this->integer()->notNull(),
            'contains_tariff_id' => $this->integer()->notNull(),
        ]);

        $this->createIndex('UI_service_subscribe_contains_tariff', '{{%service_subscribe_contains_tariff}}', [
            'tariff_id',
            'contains_tariff_id',
        ], true);
        $this->addForeignKey(
            'FK_service_subscribe_contains_contains_tariff',
            '{{%service_subscribe_contains_tariff}}',
            'tariff_id',
            '{{%service_subscribe_tariff}}',
            'id'
        );
        $this->addForeignKey(
            'FK_service_subscribe_contains_tariff',
            '{{%service_subscribe_contains_tariff}}',
            'contains_tariff_id',
            '{{%service_subscribe_tariff}}',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%service_subscribe_contains_tariff}}');
    }
}
