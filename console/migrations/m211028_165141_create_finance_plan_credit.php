<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211028_165141_create_finance_plan_credit extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%finance_plan_credit}}';

    public function safeUp()
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->primaryKey(),
            'model_id' => $this->integer()->notNull(),
            'contractor_id' => $this->integer()->notNull(),
            'credit_type' => $this->tinyInteger()->notNull(),
            'credit_status' => $this->tinyInteger()->notNull()->defaultValue(1),
            'credit_amount' => $this->double(2)->notNull(),
            'credit_first_date' => $this->date()->notNull(),
            'credit_last_date' => $this->date()->notNull(),
            'credit_day_count' => $this->integer()->notNull()->defaultValue(0),
            'credit_percent_rate' => $this->double(2)->notNull(),
            'credit_commission_rate' => $this->double(2)->notNull()->defaultValue(0),
            'credit_expiration_rate' => $this->double(2)->notNull()->defaultValue(0),
            'credit_year_length' => $this->smallInteger()->notNull()->defaultValue(0),
            'credit_tranche_depth' => $this->smallInteger()->null(),
            'payment_type' => $this->smallInteger()->notNull(),
            'payment_mode' => $this->tinyInteger()->notNull(),
            'payment_day' => $this->tinyInteger()->notNull()->defaultValue(0),
            'payment_first' => $this->tinyInteger()->notNull()->defaultValue(0),
            'payment_amount' => $this->double(2)->notNull()->defaultValue(0),
            'agreement_date' => $this->date()->notNull(),
            'debt_amount' => $this->double(2)->notNull()->defaultValue(0),
            'debt_repaid' => $this->double(2)->notNull()->defaultValue(0),
            'debt_diff' => $this->double(2)->notNull()->defaultValue(0),
            'interest_amount' => $this->double(2)->notNull()->defaultValue(0),
            'interest_repaid' => $this->double(2)->notNull()->defaultValue(0),
            'interest_diff' => $this->double(2)->notNull()->defaultValue(0),
            //'company_id' => $this->integer()->notNull(),
            //'employee_id' => $this->integer()->notNull(),
            //'checking_accountant_id' => $this->integer()->notNull(),
            //'cashbox_id' => $this->integer()->notNull(),
            //'agreement_number' => $this->bigInteger(),
            //'agreement_name' => $this->string(64)->notNull()->defaultValue(''),
            //'created_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            //'updated_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->addForeignKey('FK_finance_plan_credit_to_finance_plan', self::TABLE_NAME, 'model_id', 'finance_plan', 'id', 'CASCADE');
        $this->addForeignKey('FK_finance_plan_credit_to_contractor', self::TABLE_NAME, 'contractor_id', 'contractor', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
