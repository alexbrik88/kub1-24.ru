<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200826_052756_alter_import_1c_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('import_1c', 'is_autoload', $this->tinyInteger()->defaultValue(0)->after('period'));
    }

    public function safeDown()
    {
        $this->dropColumn('import_1c', 'is_autoload');
    }
}
