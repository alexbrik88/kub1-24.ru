<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160126_122713_add_to_notification_title extends Migration
{
    public $tNotification = 'notification';

    public function safeUp()
    {
        $this->addColumn($this->tNotification, 'title', $this->string(255)->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tNotification, 'title');
    }
}


