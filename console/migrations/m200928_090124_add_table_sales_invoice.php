<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200928_090124_add_table_sales_invoice extends Migration
{
    public $tableName = 'sales_invoice';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'uid' => $this->string(255),
            'type' => $this->integer(1)->notNull(),
            'invoice_id' => $this->integer(),
            'document_author_id' => $this->integer(),
            'status_out_id' => $this->integer(),
            'status_out_author_id' => $this->integer(),
            'document_date' => $this->date(),
            'document_number' => $this->string(255),
            'document_additional_number' => $this->string(45),
            'status_out_updated_at' => $this->integer(),
            'object_guid' => $this->string(36),
            'waybill_number' => $this->string(255),
            'waybill_date' => $this->date(),
            'basis_name' => $this->string(255),
            'basis_document_number' => $this->string(255),
            'basis_document_date' => $this->date(),
            'basis_document_type_id' => $this->integer(),
            'consignor_id' => $this->integer(),
            'consignee_id' => $this->integer(),
            'ordinal_document_number' => $this->integer(),
            'is_original' => $this->integer(1),
            'is_original_updated_at' => $this->integer(11),
            'orders_sum' => $this->integer()->defaultValue(0),
            'signed_by_employee_id' => $this->integer(11),
            'signed_by_name' => $this->string(50),
            'sign_document_type_id' => $this->integer(),
            'sign_document_number' => $this->string(50),
            'sign_document_date' => $this->date(),
            'signature_id' => $this->integer(),
            'print_id' => $this->integer(),
            'chief_signature_id' => $this->integer(),
            'chief_accountant_signature_id' => $this->integer(),
            'proxy_number' => $this->string(50),
            'proxy_date' => $this->date(),
            'add_stamp' => $this->integer(1)->defaultValue(0),
            'has_file' => $this->integer(1)->defaultValue(0),
            'contractor_address' => $this->integer(1)->defaultValue(0),
            'given_out_position' => $this->string(255),
            'given_out_fio' => $this->string(255),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
        ]);

        $this->createIndex($this->tableName . '_has_file_idx', $this->tableName, 'has_file');
        $this->createIndex($this->tableName . '_uid_idx', $this->tableName, 'uid');

        $this->addForeignKey('FK_' . $this->tableName . '_agreementType', $this->tableName, 'basis_document_type_id', 'agreement_type', 'id');
        $this->addForeignKey('FK_' . $this->tableName . '_employeeSignature', $this->tableName, 'signature_id', 'employee_signature', 'id');
        $this->addForeignKey('FK_' . $this->tableName . '_sign_documentType', $this->tableName, 'sign_document_type_id', 'document_type', 'id');
        $this->addForeignKey('FK_' . $this->tableName . '_sign_by_employee', $this->tableName, 'signed_by_employee_id', 'employee', 'id');
        $this->addForeignKey('FK_' . $this->tableName . '_company_details_chief_accountant_signature', $this->tableName, 'chief_accountant_signature_id', 'company_details_file', 'id');
        $this->addForeignKey('FK_' . $this->tableName . '_company_details_chief_signature', $this->tableName, 'chief_signature_id', 'company_details_file', 'id');
        $this->addForeignKey('FK_' . $this->tableName . '_company_details_print', $this->tableName, 'print_id', 'company_details_file', 'id');
        $this->addForeignKey('FK_' . $this->tableName . '_contractor_consignee', $this->tableName, 'consignee_id', 'contractor', 'id');
        $this->addForeignKey('FK_' . $this->tableName . '_contractor_consignor', $this->tableName, 'consignor_id', 'contractor', 'id');
        $this->addForeignKey('FK_' . $this->tableName . '_to_document_author', $this->tableName, 'document_author_id', 'employee', 'id');
        $this->addForeignKey('FK_' . $this->tableName . '_to_invoice', $this->tableName, 'invoice_id', 'invoice', 'id');
        $this->addForeignKey('FK_' . $this->tableName . '_to_status', $this->tableName, 'status_out_id', 'sales_invoice_status', 'id');
        $this->addForeignKey('FK_' . $this->tableName . '_to_status_author', $this->tableName, 'status_out_author_id', 'employee', 'id');
    }

    public function safeDown()
    {
        try {
            $this->dropForeignKey('FK_' . $this->tableName . '_agreementType', $this->tableName);
            $this->dropForeignKey('FK_' . $this->tableName . '_employeeSignature', $this->tableName);
            $this->dropForeignKey('FK_' . $this->tableName . '_sign_documentType', $this->tableName);
            $this->dropForeignKey('FK_' . $this->tableName . '_sign_by_employee', $this->tableName);
            $this->dropForeignKey('FK_' . $this->tableName . '_company_details_chief_accountant_signature', $this->tableName);
            $this->dropForeignKey('FK_' . $this->tableName . '_company_details_chief_signature', $this->tableName);
            $this->dropForeignKey('FK_' . $this->tableName . '_company_details_print', $this->tableName);
            $this->dropForeignKey('FK_' . $this->tableName . '_contractor_consignee', $this->tableName);
            $this->dropForeignKey('FK_' . $this->tableName . '_contractor_consignor', $this->tableName);
            $this->dropForeignKey('FK_' . $this->tableName . '_to_document_author', $this->tableName);
            $this->dropForeignKey('FK_' . $this->tableName . '_to_invoice', $this->tableName);
            $this->dropForeignKey('FK_' . $this->tableName . '_to_status', $this->tableName);
            $this->dropForeignKey('FK_' . $this->tableName . '_to_status_author', $this->tableName);

            $this->dropIndex($this->tableName . '_has_file_idx', $this->tableName);
            $this->dropIndex($this->tableName . '_uid_idx', $this->tableName);
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        $this->dropTable($this->tableName);
    }
}
