<?php

use yii\db\Schema;
use yii\db\Migration;

class m150708_140714_alter_paymentOrder_chnageColumn_uinCode extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('payment_order', 'uin_code', Schema::TYPE_STRING . '(10)');
    }
    
    public function safeDown()
    {
        $this->alterColumn('payment_order', 'uin_code', Schema::TYPE_INTEGER);
    }
}
