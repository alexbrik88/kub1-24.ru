<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181226_074936_tax_declaration_change_columns_type extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('tax_declaration_quarter', 'income_amount', $this->integer());
        $this->alterColumn('tax_declaration_quarter', 'outcome_amount', $this->integer());
        $this->alterColumn('tax_declaration_quarter', 'prepayment_tax_amount', $this->integer());
        $this->alterColumn('tax_declaration_quarter', 'tax_amount', $this->integer());
    }

    public function safeDown()
    {
        $this->alterColumn('tax_declaration_quarter', 'income_amount', Schema::TYPE_DECIMAL . '(22,2) NOT NULL DEFAULT "0.00"');
        $this->alterColumn('tax_declaration_quarter', 'outcome_amount', Schema::TYPE_DECIMAL . '(22,2) NOT NULL DEFAULT "0.00"');
        $this->alterColumn('tax_declaration_quarter', 'prepayment_tax_amount', Schema::TYPE_DECIMAL . '(22,2) NOT NULL DEFAULT "0.00"');
        $this->alterColumn('tax_declaration_quarter', 'tax_amount', Schema::TYPE_DECIMAL . '(22,2) NOT NULL DEFAULT "0.00"');
    }
}
