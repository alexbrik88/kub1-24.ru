<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210614_174247_alter_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_config', 'report_odds_expense_percent_2', $this->tinyInteger(1)->notNull()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('user_config', 'report_odds_expense_percent_2');
    }
}
