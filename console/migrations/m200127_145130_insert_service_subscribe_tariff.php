<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200127_145130_insert_service_subscribe_tariff extends Migration
{
    public function safeUp()
    {
        $this->batchInsert('{{%service_subscribe_tariff}}', [
            'id',
            'tariff_group_id',
            'duration_month',
            'duration_day',
            'price',
            'is_active',
            'proposition',
            'invoice_limit',
        ], [
            [29, 8, 12, 0, 300, 1, 'Получи скидку 16% при оплате за 10 проверок', 5],
            [30, 8, 12, 0, 500, 1, 'Получи скидку 33% при оплате за 50 проверок', 10],
            [31, 8, 12, 0, 2000, 1, 'Получи скидку 50% при оплате за 100 проверок', 50],
            [32, 8, 12, 0, 3000, 1, 'Получи скидку 66% при оплате за 500 проверок', 100],
            [33, 8, 12, 0, 10000, 1, 'Лучшая цена!', 500],
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%service_subscribe_tariff}}', [
            'id' => [29, 30, 31, 32, 33],
        ]);
    }
}
