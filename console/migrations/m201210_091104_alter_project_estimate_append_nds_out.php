<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201210_091104_alter_project_estimate_append_nds_out extends Migration
{
    public $tableName = 'project_estimate';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'nds_out', $this->integer(1)->after('name'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'nds_out');
    }
}