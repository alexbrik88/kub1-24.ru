<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180918_044817_update_payment_order extends Migration
{
    public function safeUp()
    {
        $this->execute('
            UPDATE {{payment_order}}
            LEFT JOIN {{company}} ON {{company}}.[[id]] = {{payment_order}}.[[company_id]]
            LEFT JOIN {{company_type}} ON {{company_type}}.[[id]] = ({{company}}.[[company_type_id]])
            SET
                {{payment_order}}.[[company_name]] = CONCAT(
                    {{company_type}}.[[name_short]], " ", {{company}}.[[name_full]]
                ),
                {{payment_order}}.[[company_inn]] = {{company}}.[[inn]],
                {{payment_order}}.[[company_kpp]] = {{company}}.[[kpp]];
        ');
    }

    public function safeDown()
    {
        $this->execute('
            UPDATE {{payment_order}}
            SET
                {{payment_order}}.[[company_name]] = NULL,
                {{payment_order}}.[[company_inn]] = NULL,
                {{payment_order}}.[[company_kpp]] = NULL;
        ');
    }
}
