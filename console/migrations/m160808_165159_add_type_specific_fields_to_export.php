<?php

use yii\db\Migration;

class m160808_165159_add_type_specific_fields_to_export extends Migration
{
    public function up()
    {
        $this->addColumn('{{%export}}', 'io_type_specific', $this->boolean()->notNull()->defaultValue(0));
        $this->addColumn('{{%export}}', 'io_type_specific_items', $this->string(255)->notNull()->defaultValue(''));
    }

    public function down()
    {
        $this->dropColumn('{{%export}}', 'io_type_specific');
        $this->dropColumn('{{%export}}', 'io_type_specific_items');
    }
}
