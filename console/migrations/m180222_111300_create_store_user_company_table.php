<?php

use yii\db\Migration;

/**
 * Handles the creation of table `store_user_company`.
 */
class m180222_111300_create_store_user_company_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('store_user_company', [
            'store_company_id' => $this->integer()->notNull(),
            'store_user_id' => $this->integer()->notNull(),
            'role_id' => $this->integer()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->integer(),
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', '{{%store_user_company}}', ['store_company_id', 'store_user_id']);

        $this->addForeignKey(
            'FK_storeUserCompany_storeCompany',
            'store_user_company',
            'store_company_id',
            'store_company',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'FK_storeUserCompany_contractor',
            'store_user_company',
            'store_user_id',
            'store_user',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'FK_storeUserCompany_storeUserRole',
            'store_user_company',
            'role_id',
            'store_user_role',
            'id'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('store_user_company');
    }
}
