<?php

use console\components\db\Migration;
use yii\db\Schema;

class m151222_151259_alter_set_default_value_time_zone extends Migration
{
    public $tEmployee = 'employee';

    public $tCompany = 'company';

    public function safeUp()
    {
        $this->alterColumn($this->tEmployee, 'time_zone_id', $this->integer(11)->defaultValue(307));
        $this->alterColumn($this->tCompany, 'time_zone_id', $this->integer(11)->defaultValue(307));
    }
    
    public function safeDown()
    {
        echo 'This migration should not be rolled back';
    }
}


