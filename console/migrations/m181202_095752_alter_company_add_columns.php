<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181202_095752_alter_company_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('company', 'salary_1_pay_day', $this->smallInteger(2)->defaultValue(null));
        $this->addColumn('company', 'salary_1_prepay_day', $this->smallInteger(2)->defaultValue(null));
        $this->addColumn('company', 'bonus_1_pay_day', $this->smallInteger(2)->defaultValue(null));
        $this->addColumn('company', 'bonus_1_prepay_day', $this->smallInteger(2)->defaultValue(null));
        $this->addColumn('company', 'salary_2_pay_day', $this->smallInteger(2)->defaultValue(null));
        $this->addColumn('company', 'salary_2_prepay_day', $this->smallInteger(2)->defaultValue(null));
        $this->addColumn('company', 'bonus_2_pay_day', $this->smallInteger(2)->defaultValue(null));
        $this->addColumn('company', 'bonus_2_prepay_day', $this->smallInteger(2)->defaultValue(null));
    }

    public function safeDown()
    {
        $this->dropColumn('company', 'salary_1_pay_day');
        $this->dropColumn('company', 'salary_1_prepay_day');
        $this->dropColumn('company', 'bonus_1_pay_day');
        $this->dropColumn('company', 'bonus_1_prepay_day');
        $this->dropColumn('company', 'salary_2_pay_day');
        $this->dropColumn('company', 'salary_2_prepay_day');
        $this->dropColumn('company', 'bonus_2_pay_day');
        $this->dropColumn('company', 'bonus_2_prepay_day');
    }
}
