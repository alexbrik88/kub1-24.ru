<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210513_200243_rework_marketing_plan_table extends Migration
{
    public function safeUp()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS = 0');
        $this->truncateTable('marketing_plan_item');
        $this->truncateTable('marketing_plan');
        $this->dropColumn('marketing_plan', 'channel');
        $this->addColumn('marketing_plan_item', 'channel', $this->tinyInteger()->unsigned()->notNull()->after('marketing_plan_id'));
        $this->execute('SET FOREIGN_KEY_CHECKS = 1');

    }

    public function safeDown()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS = 0');
        $this->truncateTable('marketing_plan_item');
        $this->truncateTable('marketing_plan');
        $this->addColumn('marketing_plan', 'channel', $this->tinyInteger()->unsigned()->notNull()->after('company_id'));
        $this->dropColumn('marketing_plan_item', 'channel');
        $this->execute('SET FOREIGN_KEY_CHECKS = 1');
    }
}
