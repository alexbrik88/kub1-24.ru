<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200928_113247_add_transaction_for_table_order_sales_invoice extends Migration
{
    public function safeUp()
    {
        $this->execute(<<<SQL
            DROP TRIGGER IF EXISTS `olap_documents_insert_order_sales_invoice`;
SQL);

        $this->execute(<<<SQL
            CREATE TRIGGER `olap_documents_insert_order_sales_invoice`
              AFTER INSERT ON `order_sales_invoice`
	            FOR EACH ROW 
                  BEGIN
                    INSERT INTO `olap_documents` (`company_id`, `contractor_id`, `by_document`, `id`, `type`, `date`, `amount`, `amount_in_price_for_buy`, `product_amount_in_price_for_buy`, `is_accounting`, `invoice_expenditure_item_id`)
                      SELECT
                        `invoice`.`company_id`,
                        `invoice`.`contractor_id`,
                        "16" AS `by_document`,
                        `document`.`id`,
                        `document`.`type`,
                        `document`.`document_date` AS `date`,
                        SUM(IF(`document`.`status_out_id` = 5, 0, IF(`document`.`type` = 2, `order_main`.`selling_price_with_vat`, `order_main`.`purchase_price_with_vat`) * `order_sales_invoice`.`quantity`)) AS `amount`,
                        SUM(IF(`document`.`status_out_id` = 5, 0, `order_sales_invoice`.`quantity` * `product`.`price_for_buy_with_nds`)) AS `amount_in_price_for_buy`,
                        SUM(IF(`document`.`status_out_id` = 5, 0, IF(`product`.`production_type` = 1, `order_sales_invoice`.`quantity` * `product`.`price_for_buy_with_nds`, 0))) AS `product_amount_in_price_for_buy`,
                        IF (`contractor`.`not_accounting`, 0, 1) AS is_accounting,
                        `invoice`.`invoice_expenditure_item_id`
                      FROM `order_sales_invoice`
                      LEFT JOIN `sales_invoice` `document` ON `document`.`id` = `order_sales_invoice`.`sales_invoice_id`
                      LEFT JOIN `invoice` ON `invoice`.`id` = `document`.`invoice_id`
                      LEFT JOIN `product` ON `product`.`id` = `order_sales_invoice`.`product_id`
                      LEFT JOIN `order` `order_main` ON `order_main`.`id` = `order_sales_invoice`.`order_id`
                      LEFT JOIN `contractor` ON `contractor`.`id` = `invoice`.`contractor_id`
                      WHERE
                        `order_sales_invoice`.`sales_invoice_id` = NEW.`sales_invoice_id`
                      GROUP BY `document`.`id`
        
                      ON DUPLICATE KEY UPDATE
                        `amount` = VALUES(`amount`),
                        `amount_in_price_for_buy` = VALUES(`amount_in_price_for_buy`),
                        `product_amount_in_price_for_buy` = VALUES(`product_amount_in_price_for_buy`);
    
                    END;
SQL);

        $this->execute(<<<SQL
            DROP TRIGGER IF EXISTS `olap_documents_update_order_sales_invoice`
SQL);

        $this->execute(<<<SQL
            CREATE TRIGGER `olap_documents_update_order_sales_invoice`
              AFTER UPDATE ON order_sales_invoice
	            FOR EACH ROW 
                    BEGIN
    
                        INSERT INTO `olap_documents` (`company_id`, `contractor_id`, `by_document`, `id`, `type`, `date`, `amount`, `amount_in_price_for_buy`, `product_amount_in_price_for_buy`, `is_accounting`, `invoice_expenditure_item_id`)
                    
                        SELECT
                            `invoice`.`company_id`,
                            `invoice`.`contractor_id`,
                            "16" AS `by_document`,
                            `document`.`id`,
                            `document`.`type`,
                            `document`.`document_date` AS `date`,
                            SUM(IF(`document`.`status_out_id` = 5, 0, IF(`document`.`type` = 2, `order_main`.`selling_price_with_vat`, `order_main`.`purchase_price_with_vat`) * `order_sales_invoice`.`quantity`)) AS `amount`,
                            SUM(IF(`document`.`status_out_id` = 5, 0, `order_sales_invoice`.`quantity` * `product`.`price_for_buy_with_nds`)) AS `amount_in_price_for_buy`,
                            SUM(IF(`document`.`status_out_id` = 5, 0, IF(`product`.`production_type` = 1, `order_sales_invoice`.`quantity` * `product`.`price_for_buy_with_nds`, 0))) AS `product_amount_in_price_for_buy`,
                            IF (`contractor`.`not_accounting`, 0, 1) AS is_accounting,
                            `invoice`.`invoice_expenditure_item_id`
                        FROM `order_sales_invoice`
                        LEFT JOIN `sales_invoice` `document` ON `document`.`id` = `order_sales_invoice`.`sales_invoice_id`
                        LEFT JOIN `invoice` ON `invoice`.`id` = `document`.`invoice_id`
                        LEFT JOIN `product` ON `product`.`id` = `order_sales_invoice`.`product_id`
                        LEFT JOIN `order` `order_main` ON `order_main`.`id` = `order_sales_invoice`.`order_id`
                        LEFT JOIN `contractor` ON `contractor`.`id` = `invoice`.`contractor_id`
                        WHERE
                            `order_sales_invoice`.`sales_invoice_id` = NEW.`sales_invoice_id`
                        GROUP BY `document`.`id`
                        
                        ON DUPLICATE KEY UPDATE
                            `amount` = VALUES(`amount`),
                            `amount_in_price_for_buy` = VALUES(`amount_in_price_for_buy`),
                            `product_amount_in_price_for_buy` = VALUES(`product_amount_in_price_for_buy`);

                    END;
SQL);

        $this->execute(<<<SQL
            DROP TRIGGER IF EXISTS `product_turnover__order_sales_invoice__after_delete`
SQL);

        $this->execute(<<<SQL
            CREATE TRIGGER `product_turnover__order_sales_invoice__after_delete`
              AFTER DELETE ON `order_sales_invoice`
	            FOR EACH ROW 
                    BEGIN    
                        DELETE FROM `product_turnover`
                        WHERE `order_id` = OLD.`order_id`
                            AND `document_id` = OLD.`sales_invoice_id`
                            AND `document_table` = "sales_invoice";
                    END;
SQL);

        $this->execute(<<<SQL
            DROP TRIGGER IF EXISTS `product_turnover__sales_invoice_list__after_insert`
SQL);

        $this->execute(<<<SQL
            CREATE TRIGGER `product_turnover__sales_invoice_list__after_insert`
              AFTER INSERT ON order_sales_invoice
	            FOR EACH ROW 
                    BEGIN    
                        INSERT INTO `product_turnover` (
                            `order_id`,
                            `document_id`,
                            `document_table`,
                            `invoice_id`,
                            `contractor_id`,
                            `company_id`,
                            `date`,
                            `type`,
                            `production_type`,
                            `is_invoice_actual`,
                            `is_document_actual`,
                            `purchase_price`,
                            `price_one`,
                            `quantity`,
                            `total_amount`,
                            `purchase_amount`,
                            `margin`,
                            `year`,
                            `month`,
                            `product_group_id`,
                            `product_id`
                        ) SELECT
                            NEW.`order_id`,
                            NEW.`sales_invoice_id`,
                            "sales_invoice",
                            `invoice`.`id`,
                            `invoice`.`contractor_id`,
                            `invoice`.`company_id`,
                            `sales_invoice`.`document_date`,
                            `sales_invoice`.`type`,
                            `product`.`production_type`,
                            IF(`invoice`.`is_deleted` = 0 AND `invoice`.`invoice_status_id` IN (1, 2, 3, 4, 6, 7, 8), 1, 0),
                            IF(`sales_invoice`.`status_out_id` IN (1, 2, 3, 4), 1, 0),
                            IFNULL(`product`.`price_for_buy_with_nds`, 0),
                            @price:=IFNULL(IF(`invoice`.`type` = 1, `order`.`purchase_price_with_vat`, `order`.`selling_price_with_vat`), 0),
                            @quantity:=IFNULL(NEW.`quantity`, 0),
                            @total:=ROUND(@price * @quantity, IF(`invoice`.`price_precision` = 4, 2, 0)),
                            @purchase:=IF(`invoice`.`type` = 2 AND `product`.`production_type` = 1, ROUND(@quantity * IFNULL(`product`.`price_for_buy_with_nds`, 0)), 0),
                            IF(`invoice`.`type` = 2 AND `product`.`production_type` = 1, @total - @purchase, 0),
                            YEAR(`sales_invoice`.`document_date`),
                            MONTH(`sales_invoice`.`document_date`),
                            `product`.`group_id`,
                            NEW.`product_id`
                        FROM `order`
                        LEFT JOIN `sales_invoice` ON `sales_invoice`.`id` = NEW.`sales_invoice_id`
                        LEFT JOIN `invoice` ON `invoice`.`id` = `order`.`invoice_id`
                        LEFT JOIN `product` ON `product`.`id` = `order`.`product_id`
                        WHERE `order`.`id` = NEW.`order_id`
                        ON DUPLICATE KEY UPDATE
                            `date` = VALUES(`date`),
                            `type` = VALUES(`type`),
                            `production_type` = VALUES(`production_type`),
                            `price_one` = VALUES(`price_one`),
                            `quantity` = VALUES(`quantity`),
                            `total_amount` = VALUES(`total_amount`),
                            `purchase_amount` = VALUES(`purchase_amount`),
                            `margin` = VALUES(`margin`),
                            `year` = VALUES(`year`),
                            `month` = VALUES(`month`),
                            `product_group_id` = VALUES(`product_group_id`),
                            `product_id` = VALUES(`product_id`);
                    END;
SQL);

        $this->execute(<<<SQL
            DROP TRIGGER IF EXISTS `product_turnover__order_sales_invoice__after_update`
SQL);

        $this->execute(<<<SQL
            CREATE TRIGGER `product_turnover__order_sales_invoice__after_update`
              AFTER UPDATE ON `order_sales_invoice`
	            FOR EACH ROW 
                    BEGIN    
                    INSERT INTO `product_turnover` (
                        `order_id`,
                        `document_id`,
                        `document_table`,
                        `invoice_id`,
                        `contractor_id`,
                        `company_id`,
                        `date`,
                        `type`,
                        `production_type`,
                        `is_invoice_actual`,
                        `is_document_actual`,
                        `purchase_price`,
                        `price_one`,
                        `quantity`,
                        `total_amount`,
                        `purchase_amount`,
                        `margin`,
                        `year`,
                        `month`,
                        `product_group_id`,
                        `product_id`
                    ) SELECT
                        NEW.`order_id`,
                        NEW.`sales_invoice_id`,
                        "sales_invoice",
                        `invoice`.`id`,
                        `invoice`.`contractor_id`,
                        `invoice`.`company_id`,
                        `sales_invoice`.`document_date`,
                        `sales_invoice`.`type`,
                        `product`.`production_type`,
                        IF(`invoice`.`is_deleted` = 0 AND `invoice`.`invoice_status_id` IN (1, 2, 3, 4, 6, 7, 8), 1, 0),
                        IF(`sales_invoice`.`status_out_id` IN (1, 2, 3, 4), 1, 0),
                        IFNULL(`product`.`price_for_buy_with_nds`, 0),
                        @price:=IFNULL(IF(`invoice`.`type` = 1, `order`.`purchase_price_with_vat`, `order`.`selling_price_with_vat`), 0),
                        @quantity:=IFNULL(NEW.`quantity`, 0),
                        @total:=ROUND(@price * @quantity, IF(`invoice`.`price_precision` = 4, 2, 0)),
                        @purchase:=IF(`invoice`.`type` = 2 AND `product`.`production_type` = 1, ROUND(@quantity * IFNULL(`product`.`price_for_buy_with_nds`, 0)), 0),
                        IF(`invoice`.`type` = 2 AND `product`.`production_type` = 1, @total - @purchase, 0),
                        YEAR(`sales_invoice`.`document_date`),
                        MONTH(`sales_invoice`.`document_date`),
                        `product`.`group_id`,
                        NEW.`product_id`
                    FROM `order`
                    LEFT JOIN `sales_invoice` ON `sales_invoice`.`id` = NEW.`sales_invoice_id`
                    LEFT JOIN `invoice` ON `invoice`.`id` = `order`.`invoice_id`
                    LEFT JOIN `product` ON `product`.`id` = `order`.`product_id`
                    WHERE `order`.`id` = NEW.`order_id`
                    ON DUPLICATE KEY UPDATE
                        `date` = VALUES(`date`),
                        `type` = VALUES(`type`),
                        `production_type` = VALUES(`production_type`),
                        `price_one` = VALUES(`price_one`),
                        `quantity` = VALUES(`quantity`),
                        `total_amount` = VALUES(`total_amount`),
                        `purchase_amount` = VALUES(`purchase_amount`),
                        `margin` = VALUES(`margin`),
                        `year` = VALUES(`year`),
                        `month` = VALUES(`month`),
                        `product_group_id` = VALUES(`product_group_id`),
                        `product_id` = VALUES(`product_id`);
                    END;
SQL);

    }

    public function safeDown()
    {
        $this->execute(<<<SQL
            DROP TRIGGER IF EXISTS `olap_documents_insert_order_sales_invoice`;
SQL);

        $this->execute(<<<SQL
            DROP TRIGGER IF EXISTS `olap_documents_update_order_sales_invoice`
SQL);

        $this->execute(<<<SQL
            DROP TRIGGER IF EXISTS `product_turnover__order_sales_invoice__after_delete`
SQL);

        $this->execute(<<<SQL
            DROP TRIGGER IF EXISTS `product_turnover__order_sales_invoice__after_insert`
SQL);

        $this->execute(<<<SQL
            DROP TRIGGER IF EXISTS `product_turnover__order_sales_invoice__after_update`
SQL);
    }
}