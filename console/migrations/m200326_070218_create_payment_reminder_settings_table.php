<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%payment_reminder_settings}}`.
 */
class m200326_070218_create_payment_reminder_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%payment_reminder_settings}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'send_mode' => $this->integer()->notNull(),
            'overdue_invoice' => $this->boolean()->notNull()->defaultValue(false),
            'exceeded_limit' => $this->boolean()->notNull()->defaultValue(false),
            'invoice_paid' => $this->boolean()->notNull()->defaultValue(false),
            'add_exceptions' => $this->boolean()->notNull()->defaultValue(false),
            'exclude_ids' => $this->text(),
        ]);

        $this->addForeignKey(
            'FK_payment_reminder_settings_to_company',
            '{{%payment_reminder_settings}}',
            'company_id',
            '{{%company}}',
            'id'
        );

        $this->createIndex('UI_company_id', '{{%payment_reminder_settings}}', 'company_id', true);
        $this->createIndex('I_send_mode', '{{%payment_reminder_settings}}', 'send_mode');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%payment_reminder_settings}}');
    }
}
