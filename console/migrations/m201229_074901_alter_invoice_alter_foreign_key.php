<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201229_074901_alter_invoice_alter_foreign_key extends Migration
{
    public function safeUp()
    {
        $this->dropForeignKey('invoice_checking_accountant_id', '{{%invoice}}');
        $this->addForeignKey('invoice_checking_accountant_id', '{{%invoice}}', 'company_checking_accountant_id', '{{%checking_accountant}}', 'id', 'SET NULL');
    }

    public function safeDown()
    {
        //
    }
}
