<?php

use common\models\service\Subscribe;
use console\components\db\Migration;
use yii\db\Query;
use yii\db\Schema;

class m190131_155651_taxrobot_subscribes extends Migration
{
    public function safeUp()
    {
        $this->createIndex('payment_for', 'service_payment', 'payment_for');

        $query = new Query;

        $payments = $query->select('service_payment.*')->from('service_payment')
            //->leftJoin('service_payment_order', '{{service_payment}}.[[id]] = {{service_payment_order}}.[[payment_id]]')
            ->leftJoin('taxrobot_payment_order', '{{service_payment}}.[[id]] = {{taxrobot_payment_order}}.[[payment_id]]')
            ->where([
                'or',
                ['service_payment.payment_for' => 'taxrobot'],
                ['not', ['taxrobot_payment_order.id' => null]],
            ])->all();

        foreach ($payments as $pay) {
            $ordQuery = new Query;
            $orderId = $ordQuery->select('id')->from('service_payment_order')->where([
                'payment_id' => $pay['id'],
            ])->scalar();

            if (!$orderId) {
                $this->insert('service_payment_order', [
                    'payment_id' => $pay['id'],
                    'company_id' => $pay['company_id'],
                    'tariff_id' => 11,
                    'price' => 2000,
                    'sum' => 2000,
                ]);

                $orderId = Yii::$app->db->getLastInsertID();
            }

            $subQuery = new Query;
            $subQuery->from('service_subscribe')->where([
                'payment_id' => $pay['id'],
            ]);

            if ($pay['is_confirmed'] && !$subQuery->exists()) {
                $activated_at = $pay['payment_date'] ? : $pay['updated_at'];
                $date = new \DateTime();
                $date->setTimestamp($activated_at)
                    ->modify("-1 day")
                    ->setTime(23, 59, 59)
                    ->modify("+1 month");

                $this->insert('service_subscribe', [
                    'company_id' => $pay['company_id'],
                    'tariff_group_id' => 4,
                    'tariff_id' => 11,
                    'payment_id' => $pay['id'],
                    'payment_order_id' => $orderId,
                    'created_at' => time(),
                    'updated_at' => time(),
                    'duration_month' => 4,
                    'duration_day' => 0,
                    'activated_at' => $activated_at,
                    'expired_at' => $date->getTimestamp(),
                    'payment_type_id' => $pay['type_id'],
                    'status_id' => 3,
                ]);
            }

            $this->update('service_payment', [
                'payment_for' => 'subscribe',
                'tariff_id' => 11,
            ], ['id' => $pay['id']]);
        }
    }

    public function safeDown()
    {
        $this->dropIndex('payment_for', 'service_payment');
    }
}
