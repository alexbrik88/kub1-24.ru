<?php

use common\models\bitrix24\Deal;
use console\components\db\Migration;

class m191116_162409_create_bitrix24_deal_position extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{bitrix24_deal_position}}', [
            'id' => $this->integer()->unsigned()->notNull()->comment('Идентификатор сделки Битрикс24'),
            'employee_id' => $this->integer()->notNull()->comment('ID клиента КУБ'),
            'deal_id' => $this->integer()->unsigned()->notNull()->comment('Идентификатор сделки'),
            'product_id' => $this->integer()->unsigned()->comment('Идентификатор товара'),
            'name' => $this->string(250)->notNull()->comment('Название товара или услуги'),
            'price' => $this->float()->unsigned()->notNull()->defaultValue(0)->comment('Цена'),
            'tax_included' => $this->boolean()->notNull()->defaultValue(true)->comment('НДС включён в цену'),
            'quantity' => $this->float()->unsigned()->notNull()->defaultValue(0)->comment('Количество'),
            'tax_rate' => $this->tinyInteger()->unsigned()->notNull()->defaultValue(0)->comment('Ставка НДС'),
            'discount_sum' => $this->float()->notNull()->comment('Сумма скидки'),
        ], "COMMENT 'Интеграция Битрикс24: товарные позиции сделки'");
        $this->addPrimaryKey('bitrix24_deal_position_id', '{{bitrix24_deal_position}}', ['employee_id', 'id']);
        $this->addForeignKey(
            'bitrix24_deal_position_employee_id', '{{bitrix24_deal_position}}', 'employee_id',
            '{{employee}}', 'id', 'CASCADE', 'CASCADE'
        );
        $this->addForeignKey(
            'bitrix24_deal_position_deal_id', '{{bitrix24_deal_position}}', ['employee_id', 'deal_id'],
            '{{bitrix24_deal}}', ['employee_id', 'id'], 'CASCADE', 'CASCADE'
        );
        $this->addForeignKey(
            'bitrix24_deal_position_product_id', '{{bitrix24_deal_position}}', ['employee_id', 'product_id'],
            '{{bitrix24_product}}', ['employee_id', 'id'], 'RESTRICT', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable('{{bitrix24_deal_position}}');

    }
}
