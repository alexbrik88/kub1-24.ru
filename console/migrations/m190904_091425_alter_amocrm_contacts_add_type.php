<?php

use console\components\db\Migration;
use common\models\amocrm\Contact;

class m190904_091425_alter_amocrm_contacts_add_type extends Migration
{
    public function up()
    {
        $this->addColumn('{{amocrm_contacts}}', 'type', "ENUM('" . implode("','", Contact::TYPE) . "') NOT NULL DEFAULT '" . Contact::TYPE_CONTACT . "' COMMENT 'Тип контакта'");
    }

    public function safeDown()
    {
        $this->dropColumn('{{amocrm_contacts}}', 'type');

    }
}
