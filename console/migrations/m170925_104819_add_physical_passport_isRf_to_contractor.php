<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170925_104819_add_physical_passport_isRf_to_contractor extends Migration
{
    public $tableName = 'contractor';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'physical_passport_isRf', $this->smallInteger(2)->unsigned()->defaultValue(1));
        $this->addColumn($this->tableName, 'physical_passport_country', $this->string(25)->defaultValue(''));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'physical_passport_isRf');
        $this->dropColumn($this->tableName, 'physical_passport_country');
    }
}


