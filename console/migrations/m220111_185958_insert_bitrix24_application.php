<?php

use console\components\db\Migration;
use yii\db\Schema;

class m220111_185958_insert_bitrix24_application extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%bitrix24_application}}', [
            'code' => 'kub24_findirektor',
            'name' => 'Интеграция Битрикс24 с КУБ24',
            'short_descr' => 'Интеграция Битрикс24 с КУБ24',
            'demo_period' => '0',
            'prefix' => 'kub24_findirektor',
            'app_id' => 'app.5ed77ea06e6c59.12270668',
            'active' => true,
            'app_secret' => '9z4UyGSFc9Tg64DtXBqGinF5Fu22nbDW7nP9LSQbgRjUKP22SA',
        ]);
    }

    public function safeDown()
    {
        $this->truncateTable('{{%bitrix24_application}}');
    }
}
