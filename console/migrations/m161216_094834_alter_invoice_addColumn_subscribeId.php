<?php

use console\components\db\Migration;
use yii\db\Schema;
use yii\db\Expression;

class m161216_094834_alter_invoice_addColumn_subscribeId extends Migration
{
    public function safeUp()
    {
        $this->addColumn('invoice', 'subscribe_id', $this->integer()->defaultValue(null)->append(' AFTER is_subscribe_invoice'));
        $this->addForeignKey('FK_invoice_service_subscribe', 'invoice', 'subscribe_id', 'service_subscribe', 'id', 'SET NULL', 'CASCADE');
        $this->execute(
            'UPDATE `invoice` `i`
            LEFT JOIN `service_subscribe` `s` ON `i`.`document_number` = `s`.`id`
            SET `i`.`subscribe_id` = (`i`.`document_number` * 1)
            WHERE `i`.`is_subscribe_invoice` = 1 AND `i`.`is_deleted` = 0 AND `s`.`id` IS NOT NULL'
        );
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('FK_invoice_service_subscribe', 'invoice');
        $this->dropColumn('invoice', 'subscribe_id');
    }
}
