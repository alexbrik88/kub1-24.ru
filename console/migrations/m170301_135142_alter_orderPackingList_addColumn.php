<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170301_135142_alter_orderPackingList_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%order_packing_list}}', 'product_title', $this->string(255)->notNull() . " AFTER `quantity`");

        $this->execute("
            UPDATE {{%order_packing_list}}
            LEFT JOIN {{%product}} ON {{%order_packing_list}}.[[product_id]] = {{%product}}.[[id]]
            SET {{%order_packing_list}}.[[product_title]] = {{%product}}.[[title]]
        ");
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%order_packing_list}}', 'product_title');
    }
}

