<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210802_065905_alter_sale_point_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('sale_point', 'status_id', $this->tinyInteger()->notNull()->defaultValue(1)->after('type_id'));
    }

    public function safeDown()
    {
        $this->dropColumn('sale_point', 'status_id');
    }
}
