<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170216_091812_add_first_send_invoice_date extends Migration
{
    public $tableName = 'company';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'first_send_invoice_date', $this->integer()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'first_send_invoice_date');
    }
}


