<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170705_212806_add_new_for_whom_notification extends Migration
{
    public $tableName = 'for_whom';

    public function safeUp()
    {
        $this->insert($this->tableName, [
            'id' => 4,
            'name' => 'Только для ИП',
        ]);
    }
    
    public function safeDown()
    {
        $this->delete($this->tableName, ['id' => 4]);
    }
}


