<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201104_094328_alter_contractor_add_column extends Migration
{
    const EXPENDITURE_ITEM_TAX = [4,5,20,21,28,29,46,47,48,49,50,51,53,65,66,67,68];
    const EXPENDITURE_ITEM_SALARY = [3];
    const EXPENDITURE_ITEM_CREDIT = [18,37];

    public function safeUp()
    {
        $items = implode(',', array_merge(self::EXPENDITURE_ITEM_TAX, self::EXPENDITURE_ITEM_SALARY, self::EXPENDITURE_ITEM_CREDIT));

        $this->addColumn('contractor', 'payment_priority', $this->tinyInteger()->notNull()->defaultValue(3));
        $this->execute("UPDATE `contractor` SET `payment_priority` = 1 WHERE `invoice_expenditure_item_id` IN ({$items})");
    }

    public function safeDown()
    {
        $this->dropColumn('contractor', 'payment_priority');
    }
}
