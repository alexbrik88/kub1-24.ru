<?php

use yii\db\Schema;
use yii\db\Migration;

class m150702_085753_alter_PaymentOrder extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('payment_order', 'contractor_inn', Schema::TYPE_STRING . '(12)');
        $this->alterColumn('payment_order', 'contractor_bik', 'CHAR(12)');
        $this->alterColumn('payment_order', 'contractor_kpp', 'CHAR(12)');
        $this->alterColumn('payment_order', 'contractor_current_account', 'CHAR(20)');
        $this->alterColumn('payment_order', 'contractor_corresponding_account', 'CHAR(20)');
    }
    
    public function safeDown()
    {
        $this->alterColumn('payment_order', 'contractor_inn', Schema::TYPE_INTEGER);
        $this->alterColumn('payment_order', 'contractor_bik', Schema::TYPE_INTEGER);
        $this->alterColumn('payment_order', 'contractor_kpp', Schema::TYPE_INTEGER);
        $this->alterColumn('payment_order', 'contractor_current_account', Schema::TYPE_INTEGER);
        $this->alterColumn('payment_order', 'contractor_corresponding_account', Schema::TYPE_INTEGER);
    }
}
