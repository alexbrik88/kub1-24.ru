<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180130_121345_update_company_address_actual extends Migration
{
    public function safeUp()
    {
        $this->execute("
            UPDATE {{company}} {{c}}
            LEFT JOIN {{address_house_type}} AS {{t}} ON {{c}}.[[address_actual_house_type_id]] =  {{t}}.[[id]]
            LEFT JOIN {{address_housing_type}} AS {{h}} ON {{c}}.[[address_actual_housing_type_id]] =  {{h}}.[[id]]
            LEFT JOIN {{address_apartment_type}} AS {{a}} ON {{c}}.[[address_actual_apartment_type_id]] =  {{a}}.[[id]]
            SET {{c}}.[[address_actual]] = IF(
                {{c}}.[[address_legal_is_actual]] = true,
                {{c}}.[[address_legal]],
                CONCAT(
                    IF(CHAR_LENGTH({{c}}.[[address_actual_postcode]]) = 6, {{c}}.[[address_actual_postcode]], '000000'),
                    IF({{c}}.[[address_actual_city]] IS NULL OR {{c}}.[[address_actual_city]] = '', '', CONCAT(
                        ', ',
                        {{c}}.[[address_actual_city]]
                    )),
                    IF({{c}}.[[address_actual_street]] IS NULL OR {{c}}.[[address_actual_street]] = '', '', CONCAT(
                        ', ',
                        {{c}}.[[address_actual_street]]
                    )),
                    IF({{c}}.[[address_actual_house]] IS NULL OR {{c}}.[[address_actual_house]] = '', '', CONCAT(
                        ', ',
                        IF({{t}}.[[name]] IS NULL, '', CONCAT(LOWER({{t}}.[[name]]), ' ')),
                        {{c}}.[[address_actual_house]]
                    )),
                    IF({{c}}.[[address_actual_housing]] IS NULL OR {{c}}.[[address_actual_housing]] = '', '', CONCAT(
                        ', ',
                        IF({{h}}.[[name_short]] IS NULL, '', CONCAT({{h}}.[[name_short]], ' ')),
                        {{c}}.[[address_actual_housing]]
                    )),
                    IF({{c}}.[[address_actual_apartment]] IS NULL OR {{c}}.[[address_actual_apartment]] = '', '', CONCAT(
                        ', ',
                        IF({{a}}.[[name_short]] IS NULL, '', CONCAT({{a}}.[[name_short]], ' ')),
                        {{c}}.[[address_actual_apartment]]
                    ))
                )
            )
            WHERE ({{c}}.[[address_actual]] IS NULL OR {{c}}.[[address_actual]] = '');
        ");
    }

    public function safeDown()
    {

    }
}
