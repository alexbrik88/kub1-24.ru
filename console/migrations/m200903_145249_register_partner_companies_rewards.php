<?php

use console\components\db\Migration;
use yii\db\Expression;
use yii\db\Query;
use yii\db\Schema;

class m200903_145249_register_partner_companies_rewards extends Migration
{
    public function safeUp()
    {
        $query = (new Query())
            ->select([
                'invoice.id as id',
                'company.invited_by_company_id as invited_company_id',
                'invoice.company_id as company_id',
                'invoice.total_amount_with_nds as amount',
            ])
            ->from('invoice')
            ->leftJoin('service_payment', 'invoice.service_payment_id = service_payment.id')
            ->leftJoin('company', 'invoice.company_id = company.id')
            ->andWhere(['and',
                ['service_payment.payment_for' => 'subscribe'],
                ['is_confirmed' => 1],
                ['IS NOT', 'company.invited_by_company_id', new Expression('NULL')]
            ])
            ->groupBy('invoice.id')
            ->all();

        foreach ($query as $key => $value) {
            if ($value['invited_company_id'] && $value['company_id'] && !(new Query())
                    ->from('company_affiliate_lead_rewards_payment')
                    ->andWhere(['and',
                        ['invoice_id' => $value['id']],
                        ['company_id' => $value['invited_company_id']],
                        ['lead_company_id' => $value['company_id']],
                    ])
                    ->exists()) {
                $this->insert('company_affiliate_lead_rewards_payment', [
                    'company_id' => $value['invited_company_id'],
                    'lead_company_id' => $value['company_id'],
                    'invoice_id' => $value['id'],
                    'rewards_percent' => 20,
                    'rewards_amount' => ($value['amount'] / 100) * 20,
                    'created_at' => new Expression('UNIX_TIMESTAMP()'),
                    'updated_at' => new Expression('UNIX_TIMESTAMP()'),
                ]);
            }
        }

    }

    public function safeDown()
    {
        return true;
    }
}
