<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190529_090751_alter_contractor_add_column extends Migration
{
    public $tContractor = 'contractor';
    public $tApiPartners = 'api_partners';

    public function safeUp()
    {
        $this->addColumn($this->tContractor, 'create_api_id', $this->integer()->defaultValue(null));
        $this->addForeignKey('FK_contractor_to_api_partners', $this->tContractor, 'create_api_id', $this->tApiPartners, 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_contractor_to_api_partners', $this->tContractor);
        $this->dropColumn($this->tContractor, 'create_api_id');
    }
}
