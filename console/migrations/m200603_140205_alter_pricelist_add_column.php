<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200603_140205_alter_pricelist_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%price_list}}', 'can_checkout_invoice', $this->boolean()->notNull()->defaultValue(false)->after('can_checkout'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%price_list}}', 'can_checkout_invoice');
    }
}
