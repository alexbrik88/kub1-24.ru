<?php

use console\components\db\Migration;
use yii\db\Schema;
use common\models\Company;

class m171003_210920_add_show_service_more_stock_modal extends Migration
{
    public $tableName = 'company';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'show_service_more_stock_modal_first_rule', $this->boolean()->defaultValue(true));
        $this->addColumn($this->tableName, 'show_service_more_stock_modal_second_rule', $this->boolean()->defaultValue(true));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'show_service_more_stock_modal_first_rule');
        $this->dropColumn($this->tableName, 'show_service_more_stock_modal_second_rule');
    }
}


