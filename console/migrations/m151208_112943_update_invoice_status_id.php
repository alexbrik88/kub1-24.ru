<?php

use yii\db\Schema;
use yii\db\Migration;

class m151208_112943_update_invoice_status_id extends Migration
{
    public $tInvoice = 'invoice';

    public function safeUp()
    {
        // 2 - send, 3 - viewed
        $this->update($this->tInvoice, ['invoice_status_id' => 2], ['invoice_status_id' => 3]);
    }

    public function safeDown()
    {
        $this->update($this->tInvoice, ['invoice_status_id' => 3], ['invoice_status_id' => 2]);
    }
}
