<?php

use common\models\Company;
use common\models\ExpenseItemFlowOfFunds;
use console\components\db\Migration;
use yii\db\Schema;

class m210423_054811_alter_company_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('company', 'utm_source', $this->string());

        $cnt = 0;
        foreach (Company::find()->select(['id'])->column() as $id) {
            if ($company = Company::findOne($id)) {
                parse_str($company->form_utm, $params);
                if (isset($params['utm_source'])) {
                    $utm_source = str_replace('"', '', $params['utm_source']);
                    $this->execute("UPDATE `company` SET `utm_source` = \"{$utm_source}\" WHERE `id` = {$id}");
                    $cnt++;
                }
            }
        }

        echo "updated {$cnt} rows\n\n";
    }

    public function safeDown()
    {
        $this->dropColumn('company', 'utm_source');
    }
}
