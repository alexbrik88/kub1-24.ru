<?php

use console\components\db\Migration;
use yii\db\Schema;

class m161205_211027_update_document_author_id_in_invoice extends Migration
{
    public $tInvoice = 'invoice';

    public function safeUp()
    {
        $this->alterColumn($this->tInvoice, 'document_author_id', $this->integer()->defaultValue(null));
        $this->alterColumn($this->tInvoice, 'invoice_status_updated_at', $this->integer()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->alterColumn($this->tInvoice, 'document_author_id', $this->integer()->notNull());
        $this->alterColumn($this->tInvoice, 'invoice_status_updated_at', $this->integer()->notNull());
    }
}


