<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211025_173740_update_income_expense_item_column extends Migration
{
    public $incomeItemsIds = '22, 2, 6, 4, 5, 14, 11, 7, 16, 17, 8';
    public $expenseItemsIds = '22, 2, 25, 39, 89, 90, 45, 61, 62, 15';

    public function safeUp()
    {
        $this->execute("
            UPDATE `invoice_income_item`
            SET `can_be_controlled` = 1
            WHERE `id` IN ({$this->incomeItemsIds})
        ");
        $this->execute("
            UPDATE `invoice_expenditure_item`
            SET `can_be_controlled` = 1
            WHERE `id` IN ({$this->expenseItemsIds})
        ");
    }

    public function safeDown()
    {
        $this->execute("
            UPDATE `invoice_income_item`
            SET `can_be_controlled` = 0
            WHERE `id` IN ({$this->incomeItemsIds})
        ");
        $this->execute("
            UPDATE `invoice_expenditure_item`
            SET `can_be_controlled` = 0
            WHERE `id` IN ({$this->expenseItemsIds})
        ");
    }
}
