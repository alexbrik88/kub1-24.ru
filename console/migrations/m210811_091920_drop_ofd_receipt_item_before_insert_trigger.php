<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210811_091920_drop_ofd_receipt_item_before_insert_trigger extends Migration
{
    public function safeUp()
    {
        $this->execute('DROP TRIGGER IF EXISTS `ofd_receipt_item_before_insert`');
    }

    public function safeDown()
    {
        $this->execute('DROP TRIGGER IF EXISTS `ofd_receipt_item_before_insert`');
        $this->execute('
            CREATE TRIGGER `ofd_receipt_item_before_insert`
            BEFORE INSERT ON `ofd_receipt_item`
            FOR EACH ROW
            BEGIN
                SET NEW.`receipt_id` = (
                    SELECT `id`
                    FROM `ofd_receipt`
                    WHERE `ofd_receipt`.`uid` = NEW.`receipt_uid`
                    LIMIT 1
                );
            END
        ');
    }
}
