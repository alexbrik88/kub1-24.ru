<?php

use console\components\db\Migration;
use yii\db\Schema;

class m151222_141504_alter_packing_list_update_created_at_and_updated_at extends Migration
{
    public $tPackingList = 'packing_list';

    public function safeUp()
    {
        $this->addColumn($this->tPackingList, 'created', $this->integer(11)->notNull());
        $this->update($this->tPackingList, [
            'created' => new \yii\db\Expression('UNIX_TIMESTAMP (created_at)'),
        ]);
        $this->dropColumn($this->tPackingList, 'created_at');
        $this->renameColumn($this->tPackingList, 'created', 'created_at');

        $this->addColumn($this->tPackingList, 'updated', $this->integer(11)->notNull());
        $this->update($this->tPackingList, [
            'updated' => new \yii\db\Expression('UNIX_TIMESTAMP (status_out_updated_at)'),
        ]);
        $this->dropColumn($this->tPackingList, 'status_out_updated_at');
        $this->renameColumn($this->tPackingList, 'updated', 'status_out_updated_at');
        $this->update($this->tPackingList, ['status_out_updated_at' => null], ['status_out_updated_at' => 0]);
    }
    
    public function safeDown()
    {
        echo 'This migration should not be rolled back';
    }
}


