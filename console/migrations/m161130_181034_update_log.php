<?php

use console\components\db\Migration;
use yii\db\Schema;

class m161130_181034_update_log extends Migration
{
    public function safeUp()
    {
        $query = new \yii\db\Query();
        $logArray = $query
            ->select(['id', 'model_name', 'model_id', 'company_id'])
            ->from('log')
            ->where(['=', 'company_id', \Yii::$app->params['service']['company_id']])
            ->andWhere(['>=', 'created_at', strtotime('2016-11-25')])
            ->all();
            

        foreach ($logArray as $log) {
            if (!empty($log['model_name']) && !empty($log['model_id'])) {
                $class = '\\' . $log['model_name'];
                $company = null;
                switch ($log['model_name']) {
                    case 'common\models\document\Act':
                    case 'common\models\document\InvoiceFacture':
                    case 'common\models\document\PackingList':
                        $table = $class::tableName();
                        $query = new \yii\db\Query();
                        $company_id = $query
                            ->select('company_id')
                            ->from('invoice')
                            ->leftJoin($table, $table . '.invoice_id = invoice.id')
                            ->where([$table . '.id' => $log['model_id']])
                            ->scalar();
                        break;

                    case 'common\models\document\Invoice':
                    case 'common\models\document\PaymentOrder':
                    case 'common\models\cash\CashBankFlows':
                    case 'common\models\cash\CashOrderFlows':
                    case 'common\models\cash\CashEmoneyFlows':
                    case 'common\models\cash\form\CashBankFlowsForm':
                    case 'common\models\cash\CashBankStatementUpload':
                        $table = $class::tableName();
                        $query = new \yii\db\Query();
                        $company_id = $query
                            ->select('company_id')
                            ->from($table)
                            ->where(['id' => $log['model_id']])
                            ->scalar();
                        break;
                        break;
                }

                if ($company_id && $company_id != $log['company_id']) {
                    $query = new \yii\db\Query();
                    $author_id = $query
                        ->select('employee_id')
                        ->from('employee_company')
                        ->where(['company_id' => $company_id])
                        ->orderBy(['employee_role_id' => SORT_ASC])
                        ->limit(1)
                        ->scalar();
                    if ($author_id) {
                        $this->update('log', ['company_id' => $company_id, 'author_id' => $author_id], ['id' => $log['id']]);
                    }
                }
            }
        }
    }
    
    public function safeDown()
    {

    }
}
