<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211104_202551_update_sales_invoice_order_nds extends Migration
{
    public function safeUp()
    {
        $this->execute('
            UPDATE `sales_invoice` 
            JOIN (
                SELECT ou.sales_invoice_id, SUM((o.purchase_price_with_vat - o.purchase_price_no_vat) * ou.quantity) AS summ
                FROM `order_sales_invoice` ou
                LEFT JOIN `order` `o` ON ou.order_id = o.id
                GROUP BY sales_invoice_id
            ) t ON t.sales_invoice_id = sales_invoice.id
            SET `sales_invoice`.`order_nds` = t.summ
            WHERE sales_invoice.type = 1
        ');

        $this->execute('
            UPDATE `sales_invoice` 
            JOIN (
                SELECT ou.sales_invoice_id, SUM((o.selling_price_with_vat - o.selling_price_no_vat) * ou.quantity) AS summ
                FROM `order_sales_invoice` ou
                LEFT JOIN `order` `o` ON ou.order_id = o.id
                GROUP BY sales_invoice_id
            ) t ON t.sales_invoice_id = sales_invoice.id
            SET `sales_invoice`.`order_nds` = t.summ
            WHERE sales_invoice.type = 2
        ');
    }

    public function safeDown()
    {
        // no down
    }
}
