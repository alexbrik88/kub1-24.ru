<?php

use yii\db\Migration;

/**
 * Handles adding physical_colums to table `contractor`.
 */
class m160601_091842_add_physical_colums_to_contractor extends Migration
{
    public $tContractor = 'contractor';
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn($this->tContractor, 'physical_firstname', $this->string(45)->notNull());
        $this->addColumn($this->tContractor, 'physical_lastname', $this->string(45)->notNull());
        $this->addColumn($this->tContractor, 'physical_patronymic', $this->string(45)->notNull());
        $this->addColumn($this->tContractor, 'physical_address', $this->string(160));
        $this->addColumn($this->tContractor, 'physical_passport_number', $this->string(6));
        $this->addColumn($this->tContractor, 'physical_passport_series', $this->string(5));
        $this->addColumn($this->tContractor, 'physical_passport_date_output', $this->date());
        $this->addColumn($this->tContractor, 'physical_passport_issued_by', $this->string(160));
        $this->addColumn($this->tContractor, 'physical_passport_department', $this->string(7));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn($this->tContractor, 'physical_firstname');
        $this->dropColumn($this->tContractor, 'physical_lastname');
        $this->dropColumn($this->tContractor, 'physical_patronymic');
        $this->dropColumn($this->tContractor, 'physical_address');
        $this->dropColumn($this->tContractor, 'physical_passport_number');
        $this->dropColumn($this->tContractor, 'physical_passport_series');
        $this->dropColumn($this->tContractor, 'physical_passport_date_output');
        $this->dropColumn($this->tContractor, 'physical_passport_issued_by');
        $this->dropColumn($this->tContractor, 'physical_passport_department');
    }
}
