<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201127_132605_alter_ofd_store_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%ofd_store}}', 'autoload_mode', $this->smallInteger(1)->notNull()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%ofd_store}}', 'autoload_mode');
    }
}
