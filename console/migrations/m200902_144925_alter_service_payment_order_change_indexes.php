<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200902_144925_alter_service_payment_order_change_indexes extends Migration
{
    public function safeUp()
    {
        $this->createIndex('FK_servicePaymentOrder_payment', '{{%service_payment_order}}', 'payment_id');
        $this->createIndex('payment_order', '{{%service_payment_order}}', [
            'payment_id',
            'company_id',
            'tariff_id',
        ], true);
        $this->dropIndex('paymentId_companyId', '{{%service_payment_order}}');
    }

    public function safeDown()
    {
        $this->createIndex('paymentId_companyId', '{{%service_payment_order}}', [
            'payment_id',
            'company_id',
        ], true);
        $this->dropIndex('payment_order', '{{%service_payment_order}}');
        $this->dropIndex('FK_servicePaymentOrder_payment', '{{%service_payment_order}}');
    }
}
