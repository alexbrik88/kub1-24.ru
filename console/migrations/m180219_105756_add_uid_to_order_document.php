<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180219_105756_add_uid_to_order_document extends Migration
{
    public $tableName = 'order_document';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'uid', $this->string(5)->unique()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'uid');
    }
}


