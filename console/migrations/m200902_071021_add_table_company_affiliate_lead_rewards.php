<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200902_071021_add_table_company_affiliate_lead_rewards extends Migration
{
    public $tableName = 'company_affiliate_lead_rewards';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer(11),
            'bottom_line' => $this->double()->unsigned(),
            'top_line' => $this->double()->unsigned(),
            'rewards' => $this->integer()->unsigned(),
            'is_current' => $this->integer(1),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
