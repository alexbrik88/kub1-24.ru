<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181012_130615_add_uid_to_price_list extends Migration
{
    public $tableName = 'price_list';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'uid', $this->string(5)->defaultValue(null));
        $this->addCommentOnColumn($this->tableName, 'uid', 'unique id for `out` price lists for quest');
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'uid');
    }
}


