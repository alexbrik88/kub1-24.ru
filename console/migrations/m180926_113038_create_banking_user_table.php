<?php

use yii\db\Migration;

/**
 * Handles the creation of table `banking_user`.
 */
class m180926_113038_create_banking_user_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('bank_user', [
            'id' => $this->primaryKey(),
            'bank_alias' => $this->string()->notNull(),
            'user_uid' => $this->string()->notNull(),
            'employee_id' => $this->integer(),
        ]);

        $this->addForeignKey('bank_user_employee', 'bank_user', 'employee_id', 'employee', 'id');
        $this->createIndex('bank_alias', 'bank_user', 'bank_alias');
        $this->createIndex('user_uid', 'bank_user', 'user_uid');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('bank_user');
    }
}
