<?php

use yii\db\Schema;
use yii\db\Migration;

class m150511_070002_create_operation_type extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('operation_type', [
            'id' => Schema::TYPE_PK,
            'code' => Schema::TYPE_STRING,
            'name' => Schema::TYPE_TEXT,
        ], $tableOptions);

        $this->batchInsert('operation_type', ['id', 'code', 'name'], [
            [1, '01', '01 – платёжное поручение'],
            [2, '02', '02 – платежное требование'],
            [3, '06', '06 – инкассовое поручение'],
            [4, '16', '16 – платежный ордер'],
            [5, '', 'нет значения'],
        ]);

    }
    
    public function safeDown()
    {
        $this->dropTable('operation_type');
    }
}
