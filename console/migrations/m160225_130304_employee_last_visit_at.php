<?php

use console\components\db\Migration;

/**
 * Class m160225_130304_employee_last_visit_at
 */
class m160225_130304_employee_last_visit_at extends Migration
{
    /**
     * @var string
     */
    public $tableName = 'employee';

    /**
     * @var string
     */
    public $columnName = 'last_visit_at';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn($this->tableName, $this->columnName, $this->integer(11)->notNull()->defaultValue(0));
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn($this->tableName, $this->columnName);
    }
}


