<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190626_073944_alter_autoinvoicefacture_change_column extends Migration
{
    public function safeUp()
    {
        $this->renameColumn('autoinvoicefacture', 'send_by_creation', 'send_auto');
    }

    public function safeDown()
    {
        $this->renameColumn('autoinvoicefacture', 'send_auto', 'send_by_creation');
    }
}
