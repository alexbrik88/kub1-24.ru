<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190607_074202_add_c3b_fields_to_employee_company extends Migration
{
    public $tableName = 'employee_company';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'inn', $this->string()->defaultValue(null));
        $this->addColumn($this->tableName, 'snils', $this->string()->defaultValue(null));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'inn');
        $this->dropColumn($this->tableName, 'snils');
    }
}
