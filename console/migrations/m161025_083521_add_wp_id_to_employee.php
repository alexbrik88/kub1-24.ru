<?php

use console\components\db\Migration;
use yii\db\Schema;

class m161025_083521_add_wp_id_to_employee extends Migration
{
    public $tEmployee = 'employee';

    public function safeUp()
    {
        $this->addColumn($this->tEmployee, 'wp_id', $this->integer()->defaultValue(null));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tEmployee, 'wp_id');
    }
}


