<?php

use console\components\db\Migration;
use yii\db\Expression;
use yii\db\Query;
use yii\db\Schema;

class m200828_131021_alter_olap_invoices extends Migration
{
    // Created table
    const TABLE_OLAP_INVOICES = 'olap_invoices';
    const TABLE_INVOICE = 'invoice';
    const TABLE_PACKING_LIST = 'packing_list';
    const TABLE_ACT = 'act';
    const TABLE_UPD = 'upd';
    const TABLE_INVOICE_ACT = 'invoice_act';
    const TABLE_INVOICE_UPD = 'invoice_upd';

    public function safeUp()
    {
        $table = self::TABLE_OLAP_INVOICES;
        $idx = "idx_{$table}";
        $query = $this->getOlapTableQuery();

        $this->execute("DROP TABLE IF EXISTS {$table}");
        $this->execute("CREATE TABLE IF NOT EXISTS {$table} (
            `id` INT,
            `company_id` INT,
            `contractor_id` INT,
            `type` TINYINT,
            `date` DATE,
            `total_amount_with_nds` BIGINT,
            `payment_partial_amount` BIGINT,
            `status_id` TINYINT,
            `status_updated_at` INT,
            `payment_limit_date` DATE,
            `has_doc` TINYINT,
            `not_need_doc` TINYINT,
            `need_all_docs` TINYINT,
            `doc_date` DATE,
            `is_deleted` TINYINT
        )");
        $this->execute("INSERT INTO {$table} ({$query->createCommand()->rawSql});");
        $this->execute("CREATE INDEX {$idx}_date ON {$table} (`company_id`, `type`, `date`);");
        $this->execute("CREATE INDEX {$idx}_doc_date ON {$table} (`company_id`, `type`, `doc_date`);");
        $this->execute("ALTER TABLE {$table} ADD PRIMARY KEY (`id`);");

        $this->createTriggersOnInvoice();
        $this->createTriggersOnDocsInsert();
        $this->createTriggersOnDocsUpdate();
    }

    public function safeDown()
    {
        $table = self::TABLE_OLAP_INVOICES;
        $this->execute("DROP TABLE IF EXISTS {$table}");
        $this->execute('DROP TRIGGER IF EXISTS `olap_invoices_insert_invoice`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_invoices_update_invoice`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_invoices_update_act`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_invoices_update_packing_list`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_invoices_update_upd`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_invoices_insert_invoice_act`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_invoices_insert_packing_list`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_invoices_insert_invoice_upd`');
    }

    private function getOlapTableQuery()
    {
        return (new Query())
            ->select(new Expression('
                i.id,
                i.company_id,
                i.contractor_id,
                i.type,
                i.document_date AS date,
                i.total_amount_with_nds,
                i.payment_partial_amount,
                i.invoice_status_id AS status_id,
                i.invoice_status_updated_at AS status_updated_at,
                i.payment_limit_date,
                IF(IF (i.has_act, 1, 0) + IF (i.has_packing_list, 1, 0) + IF(i.has_upd, 1, 0), 1, 0) AS has_doc,
                IF(IF (i.need_act, 0, 1) + IF (i.need_packing_list, 0, 1) + IF(i.need_upd, 0, 1), 1, 0) AS not_need_doc,
                IF(IF (i.need_act, 1, 0) * IF (i.need_packing_list, 1, 0) * IF(i.need_upd, 1, 0), 1, 0) AS need_all_docs,
                (
                    SELECT MAX(document_date) AS max_date FROM '.self::TABLE_PACKING_LIST.' p WHERE p.invoice_id = i.id 
                    UNION ALL
                    SELECT MAX(document_date) AS max_date FROM '.self::TABLE_ACT.' a LEFT JOIN '.self::TABLE_INVOICE_ACT.' r ON a.id = r.act_id WHERE r.invoice_id = i.id 
                    UNION ALL
                    SELECT MAX(document_date) AS max_date FROM '.self::TABLE_UPD.' u LEFT JOIN '.self::TABLE_INVOICE_UPD.' r ON u.id = r.upd_id WHERE r.invoice_id = i.id                    
                    ORDER BY max_date DESC 
                    LIMIT 1
                ) AS doc_date,
                i.is_deleted
            '))
            ->from(['i' => self::TABLE_INVOICE])
            ->groupBy(['i.id'])
            ->andWhere(['i.is_deleted' => false]);
    }

    private function createTriggersOnInvoice()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_invoices_insert_invoice`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_invoices_update_invoice`');

        $this->execute('
            CREATE TRIGGER `olap_invoices_insert_invoice`
            AFTER INSERT ON `invoice`
            FOR EACH ROW
            BEGIN
            
              DECLARE _doc_date DATE;
              
              SET _doc_date = NULL;
            
              INSERT INTO `olap_invoices` (
                `id`,
                `company_id`,
                `contractor_id`,
                `type`,
                `date`,
                `total_amount_with_nds`,
                `payment_partial_amount`,
                `status_id`,
                `status_updated_at`,
                `payment_limit_date`,
                `has_doc`,
                `not_need_doc`,
                `need_all_docs`,
                `doc_date`,
                `is_deleted`
              ) VALUES (
                NEW.`id`,
                NEW.`company_id`,
                NEW.`contractor_id`,
                NEW.`type`,
                NEW.`document_date`,
                NEW.`total_amount_with_nds`,
                NEW.`payment_partial_amount`,
                NEW.`invoice_status_id`,
                NEW.`invoice_status_updated_at`,
                NEW.`payment_limit_date`,
                IF(IF (NEW.has_act, 1, 0) + IF (NEW.has_packing_list, 1, 0) + IF(NEW.has_upd, 1, 0), 1, 0),
                IF(IF (NEW.need_act, 0, 1) + IF (NEW.need_packing_list, 0, 1) + IF(NEW.need_upd, 0, 1), 1, 0),
                IF(IF (NEW.need_act, 1, 0) * IF (NEW.need_packing_list, 1, 0) * IF(NEW.need_upd, 1, 0), 1, 0),
                _doc_date,
                NEW.`is_deleted`
              );  
            
            END');
        $this->execute('
            CREATE TRIGGER `olap_invoices_update_invoice`
            AFTER UPDATE ON `invoice`
            FOR EACH ROW
            BEGIN
            
              DECLARE _doc_date DATE;
            
              SET _doc_date = (
                    SELECT MAX(`document_date`) AS `max_date` FROM `packing_list` p WHERE p.`invoice_id` = NEW.`id`
                    UNION ALL
                    SELECT MAX(`document_date`) AS `max_date` FROM `act` a LEFT JOIN `invoice_act` r ON a.`id` = r.`act_id` WHERE r.`invoice_id` = NEW.`id` 
                    UNION ALL
                    SELECT MAX(`document_date`) AS `max_date` FROM `upd` u LEFT JOIN `invoice_upd` r ON u.`id` = r.`upd_id` WHERE r.`invoice_id` = NEW.`id`                    
                    ORDER BY `max_date` DESC 
                    LIMIT 1
                );
            
              UPDATE `olap_invoices` SET
                `contractor_id` = NEW.`contractor_id`,
                `date` = NEW.`document_date`,
                `total_amount_with_nds` = NEW.`total_amount_with_nds`,
                `payment_partial_amount` = NEW.`payment_partial_amount`,
                `status_id` = NEW.`invoice_status_id`,
                `status_updated_at` = NEW.`invoice_status_updated_at`,
                `payment_limit_date` = NEW.`payment_limit_date`,
                `has_doc` = IF(IF (NEW.has_act, 1, 0) + IF (NEW.has_packing_list, 1, 0) + IF(NEW.has_upd, 1, 0), 1, 0),
                `not_need_doc` = IF(IF (NEW.need_act, 0, 1) + IF (NEW.need_packing_list, 0, 1) + IF(NEW.need_upd, 0, 1), 1, 0),
                `need_all_docs` = IF(IF (NEW.need_act, 1, 0) * IF (NEW.need_packing_list, 1, 0) * IF(NEW.need_upd, 1, 0), 1, 0),
                `doc_date` = _doc_date,
                `is_deleted` = NEW.`is_deleted`
              WHERE `id` = NEW.`id` LIMIT 1;
            
            END');
    }
    private function createTriggersOnDocsInsert()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_invoices_insert_invoice_act`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_invoices_insert_packing_list`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_invoices_insert_invoice_upd`');

        $this->execute('
            CREATE TRIGGER `olap_invoices_insert_packing_list`
            AFTER INSERT ON `packing_list`
            FOR EACH ROW
            BEGIN
            
            UPDATE `olap_invoices` SET
                `doc_date` = (
                    SELECT MAX(p.`document_date`) AS `max_date` FROM `packing_list` p WHERE p.`invoice_id` = `olap_invoices`.`id`
                    UNION ALL
                    SELECT MAX(a.`document_date`) AS `max_date` FROM `act` a LEFT JOIN `invoice_act` r ON a.`id` = r.`act_id` WHERE r.`invoice_id` = `olap_invoices`.`id` 
                    UNION ALL
                    SELECT MAX(u.`document_date`) AS `max_date` FROM `upd` u LEFT JOIN `invoice_upd` r ON u.`id` = r.`upd_id` WHERE r.`invoice_id` = `olap_invoices`.`id`                    
                    ORDER BY `max_date` DESC 
                    LIMIT 1
                )
              WHERE `olap_invoices`.`id` = NEW.`invoice_id` ORDER BY `olap_invoices`.`id`;
            
            END');

        $this->execute('
            CREATE TRIGGER `olap_invoices_insert_invoice_act`
            AFTER INSERT ON `invoice_act`
            FOR EACH ROW
            BEGIN
            
            UPDATE `olap_invoices` SET
                `doc_date` = (
                    SELECT MAX(p.`document_date`) AS `max_date` FROM `packing_list` p WHERE p.`invoice_id` = `olap_invoices`.`id`
                    UNION ALL
                    SELECT MAX(a.`document_date`) AS `max_date` FROM `act` a LEFT JOIN `invoice_act` r ON a.`id` = r.`act_id` WHERE r.`invoice_id` = `olap_invoices`.`id` 
                    UNION ALL
                    SELECT MAX(u.`document_date`) AS `max_date` FROM `upd` u LEFT JOIN `invoice_upd` r ON u.`id` = r.`upd_id` WHERE r.`invoice_id` = `olap_invoices`.`id`                    
                    ORDER BY `max_date` DESC 
                    LIMIT 1
                )
              WHERE `olap_invoices`.`id` = NEW.`invoice_id`;
            
            END');
        $this->execute('
            CREATE TRIGGER `olap_invoices_insert_invoice_upd`
            AFTER INSERT ON `invoice_upd`
            FOR EACH ROW
            BEGIN
            
            UPDATE `olap_invoices` SET
                `doc_date` = (
                    SELECT MAX(p.`document_date`) AS `max_date` FROM `packing_list` p WHERE p.`invoice_id` = `olap_invoices`.`id`
                    UNION ALL
                    SELECT MAX(a.`document_date`) AS `max_date` FROM `upd` a LEFT JOIN `invoice_upd` r ON a.`id` = r.`upd_id` WHERE r.`invoice_id` = `olap_invoices`.`id` 
                    UNION ALL
                    SELECT MAX(u.`document_date`) AS `max_date` FROM `upd` u LEFT JOIN `invoice_upd` r ON u.`id` = r.`upd_id` WHERE r.`invoice_id` = `olap_invoices`.`id`                    
                    ORDER BY `max_date` DESC 
                    LIMIT 1
                )
              WHERE `olap_invoices`.`id` = NEW.`invoice_id`;
            
            END');
    }
    private function createTriggersOnDocsUpdate()
    {

        $this->execute('DROP TRIGGER IF EXISTS `olap_invoices_update_act`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_invoices_update_packing_list`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_invoices_update_upd`');

        $this->execute('
            CREATE TRIGGER `olap_invoices_update_packing_list`
            AFTER UPDATE ON `packing_list`
            FOR EACH ROW
            BEGIN
            
            UPDATE `olap_invoices` SET
                `doc_date` = (
                    SELECT MAX(p.`document_date`) AS `max_date` FROM `packing_list` p WHERE p.`invoice_id` = `olap_invoices`.`id`
                    UNION ALL
                    SELECT MAX(a.`document_date`) AS `max_date` FROM `act` a LEFT JOIN `invoice_act` r ON a.`id` = r.`act_id` WHERE r.`invoice_id` = `olap_invoices`.`id` 
                    UNION ALL
                    SELECT MAX(u.`document_date`) AS `max_date` FROM `upd` u LEFT JOIN `invoice_upd` r ON u.`id` = r.`upd_id` WHERE r.`invoice_id` = `olap_invoices`.`id`                    
                    ORDER BY `max_date` DESC 
                    LIMIT 1
                )
              WHERE `olap_invoices`.`id` IN (NEW.`invoice_id`) ORDER BY `olap_invoices`.`id`;
            
            END');
        $this->execute('
            CREATE TRIGGER `olap_invoices_update_act`
            AFTER UPDATE ON `act`
            FOR EACH ROW
            BEGIN
            
            UPDATE `olap_invoices` SET
                `doc_date` = (
                    SELECT MAX(p.`document_date`) AS `max_date` FROM `packing_list` p WHERE p.`invoice_id` = `olap_invoices`.`id`
                    UNION ALL
                    SELECT MAX(a.`document_date`) AS `max_date` FROM `act` a LEFT JOIN `invoice_act` r ON a.`id` = r.`act_id` WHERE r.`invoice_id` = `olap_invoices`.`id` 
                    UNION ALL
                    SELECT MAX(u.`document_date`) AS `max_date` FROM `upd` u LEFT JOIN `invoice_upd` r ON u.`id` = r.`upd_id` WHERE r.`invoice_id` = `olap_invoices`.`id`                    
                    ORDER BY `max_date` DESC 
                    LIMIT 1
                )
              WHERE `olap_invoices`.`id` IN (SELECT `invoice_id` FROM `invoice_act` WHERE `act_id` = NEW.`id` ORDER BY `olap_invoices`.`id`);
            
            END');
        $this->execute('
            CREATE TRIGGER `olap_invoices_update_upd`
            AFTER UPDATE ON `upd`
            FOR EACH ROW
            BEGIN
            
            UPDATE `olap_invoices` SET
                `doc_date` = (
                    SELECT MAX(p.`document_date`) AS `max_date` FROM `packing_list` p WHERE p.`invoice_id` = `olap_invoices`.`id`
                    UNION ALL
                    SELECT MAX(a.`document_date`) AS `max_date` FROM `upd` a LEFT JOIN `invoice_upd` r ON a.`id` = r.`upd_id` WHERE r.`invoice_id` = `olap_invoices`.`id` 
                    UNION ALL
                    SELECT MAX(u.`document_date`) AS `max_date` FROM `upd` u LEFT JOIN `invoice_upd` r ON u.`id` = r.`upd_id` WHERE r.`invoice_id` = `olap_invoices`.`id`                    
                    ORDER BY `max_date` DESC 
                    LIMIT 1
                )
              WHERE `olap_invoices`.`id` IN (SELECT `invoice_id` FROM `invoice_upd` WHERE `upd_id` = NEW.`id` ORDER BY `olap_invoices`.`id`);
            
            END');
    }
}
