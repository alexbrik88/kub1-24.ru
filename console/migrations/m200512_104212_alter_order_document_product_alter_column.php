<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200512_104212_alter_order_document_product_alter_column extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%order_document_product}}', 'purchase_price_no_vat',  $this->bigInteger(20)->defaultValue(0));
        $this->alterColumn('{{%order_document_product}}', 'purchase_price_with_vat',  $this->bigInteger(20)->defaultValue(0));
        $this->alterColumn('{{%order_document_product}}', 'selling_price_no_vat',  $this->bigInteger(20)->defaultValue(0));
        $this->alterColumn('{{%order_document_product}}', 'selling_price_with_vat',  $this->bigInteger(20)->defaultValue(0));
        $this->alterColumn('{{%order_document_product}}', 'amount_purchase_no_vat',  $this->bigInteger(20)->defaultValue(0));
        $this->alterColumn('{{%order_document_product}}', 'amount_purchase_with_vat',  $this->bigInteger(20)->defaultValue(0));
        $this->alterColumn('{{%order_document_product}}', 'amount_sales_no_vat',  $this->bigInteger(20)->defaultValue(0));
        $this->alterColumn('{{%order_document_product}}', 'amount_sales_with_vat',  $this->bigInteger(20)->defaultValue(0));
        $this->alterColumn('{{%order_document_product}}', 'sale_tax',  $this->bigInteger(20)->defaultValue(0));
        $this->alterColumn('{{%order_document_product}}', 'purchase_tax',  $this->bigInteger(20)->defaultValue(0));
    }

    public function down()
    {
        $this->alterColumn('{{%order_document_product}}', 'purchase_price_no_vat',  $this->bigInteger(20)->notNull());
        $this->alterColumn('{{%order_document_product}}', 'purchase_price_with_vat',  $this->bigInteger(20)->notNull());
        $this->alterColumn('{{%order_document_product}}', 'selling_price_no_vat',  $this->bigInteger(20));
        $this->alterColumn('{{%order_document_product}}', 'selling_price_with_vat',  $this->bigInteger(20));
        $this->alterColumn('{{%order_document_product}}', 'amount_purchase_no_vat',  $this->bigInteger(20)->notNull());
        $this->alterColumn('{{%order_document_product}}', 'amount_purchase_with_vat',  $this->bigInteger(20)->notNull());
        $this->alterColumn('{{%order_document_product}}', 'amount_sales_no_vat',  $this->bigInteger(20)->notNull());
        $this->alterColumn('{{%order_document_product}}', 'amount_sales_with_vat',  $this->bigInteger(20)->notNull());
        $this->alterColumn('{{%order_document_product}}', 'sale_tax',  $this->bigInteger(20)->notNull());
        $this->alterColumn('{{%order_document_product}}', 'purchase_tax',  $this->bigInteger(20)->notNull());
    }
}
