<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210910_092027_alter_moysklad_document extends Migration
{
    public $documentType = 'moysklad_document_type';
    public $document = 'moysklad_document';
    public $documentValidation = 'moysklad_document_validation';

    public function safeUp()
    {
        $table = $this->document;

        $this->dropForeignKey("FK_{$table}_to_company", $table);
        $this->dropForeignKey("FK_{$table}_to_document_type", $table);
        $this->dropForeignKey("FK_{$table}_to_contractor", $table);
        $this->dropForeignKey("FK_{$table}_to_act", $table);
        $this->dropForeignKey("FK_{$table}_to_order_document", $table);
        $this->dropForeignKey("FK_{$table}_to_invoice", $table);
        $this->dropForeignKey("FK_{$table}_to_packing_list", $table);
        $this->dropForeignKey("FK_{$table}_to_upd", $table);
        $this->dropIndex("IDX_{$table}_contractor", $table);

        try { $this->dropIndex("FK_{$table}_to_company", $table); } catch (Throwable $e) {}
        try { $this->dropIndex("FK_{$table}_to_document_type", $table); } catch (Throwable $e) {}
        try { $this->dropIndex("FK_{$table}_to_contractor", $table); } catch (Throwable $e) {}
        try { $this->dropIndex("FK_{$table}_to_act", $table); } catch (Throwable $e) {}
        try { $this->dropIndex("FK_{$table}_to_order_document", $table); } catch (Throwable $e) {}
        try { $this->dropIndex("FK_{$table}_to_invoice", $table); } catch (Throwable $e) {}
        try { $this->dropIndex("FK_{$table}_to_packing_list", $table); } catch (Throwable $e) {}
        try { $this->dropIndex("FK_{$table}_to_upd", $table); } catch (Throwable $e) {}

        $this->addForeignKey("FK_{$table}_to_company", $table, 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("FK_{$table}_to_document_type", $table, 'document_type_id', $this->documentType, 'id', 'CASCADE', 'CASCADE');

        $this->addColumn($table, 'invoice_facture_id', $this->integer()->after('upd_id'));
        $this->renameColumn($table, 'object_type', 'document_type');
        $this->renameColumn($table, 'object_date', 'document_date');
        $this->renameColumn($table, 'object_number', 'document_number');
    }

    public function safeDown()
    {
        $table = $this->document;

        $this->dropColumn($table, 'invoice_facture_id');
        $this->renameColumn($table, 'document_type', 'object_type');
        $this->renameColumn($table, 'document_date', 'object_date');
        $this->renameColumn($table, 'document_number', 'object_number');

        $this->dropForeignKey("FK_{$table}_to_company", $table);
        $this->dropForeignKey("FK_{$table}_to_document_type", $table);

        //

        $this->addForeignKey("FK_{$table}_to_company", $table, 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("FK_{$table}_to_document_type", $table, 'document_type_id', $this->documentType, 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("FK_{$table}_to_contractor", $table, 'contractor_id', 'contractor', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("FK_{$table}_to_act", $table, 'act_id', 'act', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("FK_{$table}_to_order_document", $table, 'order_document_id', 'order_document', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("FK_{$table}_to_invoice", $table, 'invoice_id', 'invoice', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("FK_{$table}_to_packing_list", $table, 'packing_list_id', 'packing_list', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("FK_{$table}_to_upd", $table, 'upd_id', 'upd', 'id', 'CASCADE', 'CASCADE');
        $this->createIndex("IDX_{$table}_contractor", $table, ['company_id', 'document_type_id', 'contractor_id']);
    }
}
