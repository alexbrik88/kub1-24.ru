<?php

use console\components\db\Migration;
use yii\db\Query;
use yii\db\Schema;

class m170721_051517_insert_servicePayment extends Migration
{
    public function safeUp()
    {
        $this->execute("
            INSERT INTO
                {{%service_payment}} (
                	company_id,
                    created_at,
                    updated_at,
                    subscribe_id,
                    price,
                    discount,
                    sum,
                    type_id,
                    tariff_id,
                    by_novice,
                    is_confirmed,
                    payment_date
                )
            SELECT
                {{%service_subscribe}}.[[company_id]],
                {{%service_subscribe}}.[[created_at]],
                {{%service_subscribe}}.[[created_at]],
                {{%service_subscribe}}.[[id]],
                {{%service_subscribe_tariff}}.[[price]],
                {{%service_subscribe}}.[[discount]],
                {{%service_subscribe_tariff}}.[[price]] - ROUND({{%service_subscribe_tariff}}.[[price]] * {{%service_subscribe}}.[[discount]] / 100),
                {{%service_subscribe}}.[[payment_type_id]],
                {{%service_subscribe}}.[[tariff_id]],
                IF({{%company}}.[[experience_id]] = 1, 1, 0),
                IF({{%service_subscribe}}.[[status_id]] = 1, 0, 1),
                IF({{%service_subscribe}}.[[status_id]] = 1, NULL, {{%service_subscribe}}.[[created_at]])
            FROM
            	{{%service_subscribe}}
            LEFT JOIN
                {{%service_subscribe_tariff}} ON {{%service_subscribe_tariff}}.[[id]] = {{%service_subscribe}}.[[tariff_id]]
            LEFT JOIN
                {{%company}} ON {{%company}}.[[id]] = {{%service_subscribe}}.[[company_id]]
            LEFT JOIN
                {{%service_payment}} ON {{%service_payment}}.[[subscribe_id]] = {{%service_subscribe}}.[[id]]
            WHERE
                {{%service_subscribe}}.[[tariff_id]] <> 4 AND {{%service_payment}}.[[id]] IS NULL AND {{%company}}.[[id]] IS NOT NULL
            ORDER BY
            	{{%service_subscribe}}.[[created_at]] ASC
        ");
    }
    
    public function safeDown()
    {
    	$query = new Query;
    	$lastOldId = $query
    		->select('{{t1}}.[[id]]')
    		->from(['t1' => '{{%service_payment}}'])
    		->leftJoin(
    			['t2' => '{{%service_payment}}'],
    			't2.id = (SELECT [[id]] FROM {{%service_payment}} WHERE [[id]] > {{t1}}.[[id]] ORDER BY [[id]] ASC LIMIT 1)'
			)
    		->where('{{t1}}.[[created_at]] > {{t2}}.[[created_at]]')
    		->orderBy(['t1.id' => SORT_ASC])
    		->limit(1)
    		->scalar();

    	$this->delete('{{%service_payment}}', ['>', 'id', $lastOldId]);
    }
}
