<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170705_212053_add_fine_to_novelty extends Migration
{
    public $tableName = 'notification';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'fine', $this->string()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'fine');
    }
}


