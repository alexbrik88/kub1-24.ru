<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200415_083628_alter_cash_emoney_flows_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cash_emoney_flows', 'other_emoney_id', $this->integer()->defaultValue(null)->after('emoney_id'));
        $this->addColumn('cash_emoney_flows', 'other_cashbox_id', $this->integer()->defaultValue(null)->after('emoney_id'));
        $this->addColumn('cash_emoney_flows', 'other_rs_id', $this->integer()->defaultValue(null)->after('emoney_id'));
    }

    public function safeDown()
    {
        $this->dropColumn('cash_emoney_flows', 'other_emoney_id');
        $this->dropColumn('cash_emoney_flows', 'other_cashbox_id');
        $this->dropColumn('cash_emoney_flows', 'other_rs_id');
    }
}
