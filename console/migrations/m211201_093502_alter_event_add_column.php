<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211201_093502_alter_event_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%event}}', 'category', $this->integer()->null()->after('id'));
        $this->createIndex('category', '{{%event}}', 'category');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%event}}', 'category');
    }
}
