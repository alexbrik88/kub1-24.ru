<?php

use console\components\db\Migration;
use yii\db\Schema;

class m220222_124817_alter_user_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%user_config}}', 'product_unit_type', $this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%user_config}}', 'product_unit_type');
    }
}
