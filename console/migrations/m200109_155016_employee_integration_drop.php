<?php

use console\components\db\Migration;

class m200109_155016_employee_integration_drop extends Migration
{
    public function up()
    {
        $this->dropColumn('{{%employee}}', 'integration');
    }

    public function down()
    {
        $this->addColumn('{{%employee}}', 'integration', $this->string(2000)->comment('Настройки интеграции'));
    }
}
