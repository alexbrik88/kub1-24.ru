<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201111_165112_alter_user_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%user_config}}', 'what_if_help', $this->boolean()->defaultValue(false));
        $this->addColumn('{{%user_config}}', 'what_if_chart', $this->boolean()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%user_config}}', 'what_if_help');
        $this->dropColumn('{{%user_config}}', 'what_if_chart');
    }
}
