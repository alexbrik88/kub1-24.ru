<?php

use common\models\insales\Client;
use console\components\db\Migration;

class m191016_123503_create_insales_client extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%insales_client}}', [
            'id' => $this->primaryKey()->unsigned()->notNull()->comment('ID клиента в InSales'),
            'employee_id' => $this->integer()->comment('ID клиента на сайте'),
            'type' => "ENUM('" . implode("','", Client::TYPE) . "') NOT NULL DEFAULT '" . Client::TYPE_INDIVIDUAL . "' COMMENT 'Тип клиента'",
            'email' => $this->string(30),
            'phone' => $this->char(15)->comment('Номер телефона'),
            'name' => $this->string(30)->notNull()->comment('Имя клиента'),
            'surname' => $this->string(30)->comment('Фамилия'),
            'middlename' => $this->string(30)->comment('Отчество'),
            'bonus_points' => $this->integer()->unsigned()->notNull()->defaultValue(0),
            'progressive_discount' => $this->smallInteger()->unsigned()->notNull()->defaultValue(0),
            'group_discount' => $this->smallInteger()->unsigned()->notNull()->defaultValue(0),
            'fields_values' => $this->string(1000)->comment('Дополнительные данные по клиенту (JSON)'),
            'created_at' => $this->integer()->unsigned()->notNull(),
            'updated_at' => $this->integer()->unsigned()->notNull(),
        ], "COMMENT 'Интеграция InSales CMS: клиенты'");
        $this->addForeignKey(
            'insales_client_employee_id', '{{%insales_client}}', 'employee_id',
            '{{%employee}}', 'id', 'CASCADE', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable('{{%insales_client}}');

    }
}
