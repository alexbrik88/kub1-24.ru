<?php

use yii\db\Migration;

class m210621_105626_alter_crm_deal_stage_table extends Migration
{
    /**
     * @inheritDoc
     */
    public function up(): void
    {
        $this->dropForeignKey('fk_crm_client__crm_client_deal_stage', '{{%crm_client}}');

        $this->renameColumn('{{%crm_client}}', 'client_deal_stage_id', 'deal_stage_id');
        $this->renameColumn('{{%crm_deal_stage}}', 'client_deal_stage_id', 'deal_stage_id');

        $this->addForeignKey(
            'fk_crm_client__crm_deal_stage',
            '{{%crm_client}}',
            ['deal_stage_id'],
            '{{%crm_deal_stage}}',
            ['deal_stage_id'],
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * @return bool
     */
    public function down(): bool
    {
        return false;
    }
}
