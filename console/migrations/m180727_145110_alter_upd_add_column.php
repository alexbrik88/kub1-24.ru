<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180727_145110_alter_upd_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('upd', 'has_file', $this->boolean()->notNull()->defaultValue(false));
        $this->createIndex('has_file', 'upd', 'has_file');
        $this->execute("
            UPDATE {{upd}} {{t}}
            SET {{t}}.[[has_file]] = IF(
                EXISTS(
                    SELECT {{f}}.[[id]]
                    FROM {{file}} {{f}}
                    WHERE {{f}}.[[owner_id]] = {{t}}.id AND {{f}}.[[owner_model]] = :class
                ),
                1,
                EXISTS(
                    SELECT {{s}}.[[id]]
                    FROM {{scan_document}} {{s}}
                    WHERE {{s}}.[[owner_id]] = {{t}}.id AND {{s}}.[[owner_model]] = :class
                )
            );
        ", [':class' => 'common\models\document\Upd']);
    }

    public function safeDown()
    {
        $this->dropColumn('upd', 'has_file');
    }
}
