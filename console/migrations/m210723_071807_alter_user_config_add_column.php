<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210723_071807_alter_user_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%user_config}}', 'crm_task_last_employee_id', $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%user_config}}', 'crm_task_last_employee_id');
    }
}
