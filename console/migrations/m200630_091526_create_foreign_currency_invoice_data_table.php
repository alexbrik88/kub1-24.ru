<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%foreign_currency_invoice_data}}`.
 */
class m200630_091526_create_foreign_currency_invoice_data_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%foreign_currency_invoice_data}}', [
            'foreign_currency_invoice_id' => $this->integer()->notNull(),
            'key' => $this->string()->notNull(),
            'value' => $this->text(),
            'PRIMARY KEY ([[foreign_currency_invoice_id]], [[key]])',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%foreign_currency_invoice_data}}');
    }
}
