<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200724_081754_alter_employee_company_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%employee_company}}', 'can_ba_all', $this->boolean()->defaultValue(false));
        $this->addColumn('{{%employee_company}}', 'can_ba_finance', $this->boolean()->defaultValue(false));
        $this->addColumn('{{%employee_company}}', 'can_ba_marketing', $this->boolean()->defaultValue(false));
        $this->addColumn('{{%employee_company}}', 'can_ba_product', $this->boolean()->defaultValue(false));
        $this->addColumn('{{%employee_company}}', 'can_ba_sales', $this->boolean()->defaultValue(false));

        $this->execute("
            UPDATE {{%employee_company}}
            SET {{%employee_company}}.[[can_ba_all]] = 1
            WHERE {{%employee_company}}.[[can_business_analytics]] = 1        
        ");
    }

    public function safeDown()
    {
        $this->dropColumn('{{%employee_company}}', 'can_ba_all');
        $this->dropColumn('{{%employee_company}}', 'can_ba_finance');
        $this->dropColumn('{{%employee_company}}', 'can_ba_marketing');
        $this->dropColumn('{{%employee_company}}', 'can_ba_product');
        $this->dropColumn('{{%employee_company}}', 'can_ba_sales');
    }
}
