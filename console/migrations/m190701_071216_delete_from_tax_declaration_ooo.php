<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190701_071216_delete_from_tax_declaration_ooo extends Migration
{
    public function safeUp()
    {
        // update IP param
        $this->execute("
          UPDATE `tax_declaration` SET `single`=0 WHERE `company_id` IN (SELECT `id` FROM `company` WHERE `company_type_id` = 1)
        ");

        // Update old declarations
        $this->execute("
          UPDATE `tax_declaration` SET `tax_quarter` = 1 WHERE (`single`=1 OR `org`=1 OR `nds`=1)
          AND `company_id` IN (SELECT `id` FROM `company` WHERE `company_type_id` > 1)
          AND `tax_quarter` IS NULL
        ");
        // Update old szvm
        $this->execute("
          UPDATE `tax_declaration` SET `tax_quarter` = 1, `tax_month` = 1 WHERE `szvm`=1
          AND `company_id` IN (SELECT `id` FROM `company` WHERE `company_type_id` > 1)
          AND `tax_month` IS NULL
        ");
    }

    public function safeDown()
    {

    }
}
