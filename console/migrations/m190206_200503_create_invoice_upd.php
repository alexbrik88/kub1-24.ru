<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190206_200503_create_invoice_upd extends Migration
{
    public $tableName = '{{%invoice_upd}}';

    public function safeUp()
    {
        $this->dropForeignKey('FK_upd_invoice', '{{%upd}}');
        $this->alterColumn('{{%upd}}', 'invoice_id', $this->integer()->defaultValue(null));
        $this->addForeignKey('FK_upd_invoice', '{{%upd}}', 'invoice_id', '{{%invoice}}', 'id', 'RESTRICT', 'CASCADE');

        $this->createTable($this->tableName, [
            'invoice_id' => $this->integer()->notNull(),
            'upd_id' => $this->integer()->notNull(),
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', $this->tableName, ['invoice_id', 'upd_id']);
        $this->addForeignKey('invoice_upd_invoice_id', $this->tableName, 'invoice_id', '{{%invoice}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('invoice_upd_upd_id', $this->tableName, 'upd_id', '{{%upd}}', 'id', 'CASCADE', 'CASCADE');

        $this->execute("
            INSERT INTO {{%invoice_upd}} (invoice_id, upd_id)
            SELECT {{%upd}}.[[invoice_id]], {{%upd}}.[[id]]
            FROM {{%upd}}
            LEFT JOIN {{%invoice}}
                ON {{%upd}}.[[invoice_id]] = {{%invoice}}.[[id]]
			WHERE {{%invoice}}.[[id]] IS NOT NULL
            AND {{%upd}}.[[invoice_id]] IS NOT NULL;
        ");
    }

    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
