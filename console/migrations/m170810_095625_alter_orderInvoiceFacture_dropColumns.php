<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170810_095625_alter_orderInvoiceFacture_dropColumns extends Migration
{
    public function safeUp()
    {
    	$this->dropColumn('{{%order_invoice_facture}}', 'empty_unit_code');
    	$this->dropColumn('{{%order_invoice_facture}}', 'empty_unit_name');
    	$this->dropColumn('{{%order_invoice_facture}}', 'empty_quantity');
    	$this->dropColumn('{{%order_invoice_facture}}', 'empty_price');
    }
    
    public function safeDown()
    {
        $this->addColumn('{{%order_invoice_facture}}', 'empty_unit_code', $this->smallInteger(1)->defaultValue(false));
        $this->addColumn('{{%order_invoice_facture}}', 'empty_unit_name', $this->smallInteger(1)->defaultValue(false));
        $this->addColumn('{{%order_invoice_facture}}', 'empty_quantity', $this->smallInteger(1)->defaultValue(false));
        $this->addColumn('{{%order_invoice_facture}}', 'empty_price', $this->smallInteger(1)->defaultValue(false));

        $this->execute("
			UPDATE {{%order_invoice_facture}} {{orderIf}}
			LEFT JOIN {{%invoice_facture}} {{iFacture}}
				ON {{orderIf}}.[[invoice_facture_id]] = {{iFacture}}.[[id]]
			LEFT JOIN {{%order}} {{order}}
				ON {{orderIf}}.[[order_id]] = {{order}}.[[id]]
			LEFT JOIN {{%product}} {{product}}
				ON {{order}}.[[product_id]] = {{product}}.[[id]]
			SET 
				{{orderIf}}.[[empty_unit_code]] = 1,
				{{orderIf}}.[[empty_unit_name]] = 1,
				{{orderIf}}.[[empty_quantity]] = 1,
				{{orderIf}}.[[empty_price]] = 1
			WHERE {{iFacture}}.[[type]] = 2
				AND {{iFacture}}.[[show_service_units]] = 0
				AND {{product}}.[[production_type]] = 0
        ");
    }
}
