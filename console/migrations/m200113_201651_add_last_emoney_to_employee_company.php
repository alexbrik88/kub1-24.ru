<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200113_201651_add_last_emoney_to_employee_company extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%employee_company}}', 'last_emoney_id', $this->integer()->defaultValue(null));
        $this->addForeignKey('employee_company_to_emoney', 'employee_company', 'last_emoney_id', 'emoney', 'id', 'SET NULL', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('employee_company_to_emoney', 'employee_company');
        $this->dropColumn('{{%employee_company}}', 'last_emoney_id');
    }
}
