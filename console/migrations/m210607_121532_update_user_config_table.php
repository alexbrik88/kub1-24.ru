<?php

use yii\db\Migration;

class m210607_121532_update_user_config_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%user_config}}';

    /**
     * @inheritDoc
     */
    public function up(): void
    {
        $this->renameColumn(self::TABLE_NAME, 'crm_client_activity',  'contractor_activity');
        $this->renameColumn(self::TABLE_NAME, 'crm_client_campaign',  'contractor_campaign');
    }

    /**
     * @inheritDoc
     */
    public function down(): void
    {
        $this->renameColumn(self::TABLE_NAME, 'contractor_activity', 'crm_client_activity');
        $this->renameColumn(self::TABLE_NAME, 'contractor_campaign', 'crm_client_campaign');
    }
}
