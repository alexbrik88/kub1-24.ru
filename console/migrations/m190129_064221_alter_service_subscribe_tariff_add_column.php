<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190129_064221_alter_service_subscribe_tariff_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('service_subscribe_tariff', 'group_id', $this->integer()->notNull()->after('id'));

        $this->update('service_subscribe_tariff', ['group_id' => 1]);

        $this->addForeignKey(
            'FK_service_subscribe_tariff_tariff_group',
            'service_subscribe_tariff', 'group_id',
            'service_subscribe_tariff_group', 'id'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey(
            'FK_service_subscribe_tariff_tariff_group',
            'service_subscribe_tariff'
        );

        $this->dropColumn('service_subscribe_tariff', 'group_id');
    }
}
