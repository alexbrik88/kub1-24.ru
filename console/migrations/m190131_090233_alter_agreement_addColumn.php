<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190131_090233_alter_agreement_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%agreement}}', 'signed_by_employee_id', $this->integer());
        $this->addColumn('{{%agreement}}', 'signed_by_name', $this->string(50));
        $this->addColumn('{{%agreement}}', 'sign_document_type_id', $this->integer());
        $this->addColumn('{{%agreement}}', 'sign_document_number', $this->string(50));
        $this->addColumn('{{%agreement}}', 'sign_document_date', $this->date());
        $this->addColumn('{{%agreement}}', 'signature_id', $this->integer());

        $this->addForeignKey('FK_agreement_sign_employee', '{{%agreement}}', 'signed_by_employee_id', '{{%employee}}', 'id');
        $this->addForeignKey('FK_agreement_sign_documentType', '{{%agreement}}', 'sign_document_type_id', '{{%document_type}}', 'id');
        $this->addForeignKey('FK_agreement_employeeSignature', '{{%agreement}}', 'signature_id', '{{%employee_signature}}', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_agreement_sign_employee', '{{%agreement}}');
        $this->dropForeignKey('FK_agreement_sign_documentType', '{{%agreement}}');
        $this->dropForeignKey('FK_agreement_employeeSignature', '{{%agreement}}');
        $this->dropColumn('{{%agreement}}', 'signed_by_employee_id');
        $this->dropColumn('{{%agreement}}', 'signed_by_name');
        $this->dropColumn('{{%agreement}}', 'sign_document_type_id');
        $this->dropColumn('{{%agreement}}', 'sign_document_number');
        $this->dropColumn('{{%agreement}}', 'sign_document_date');
        $this->dropColumn('{{%agreement}}', 'signature_id');
    }
}
