<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180807_145022_alter_user_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn(
            'user_config',
            'invoice_invfacture',
            $this->boolean()->notNull()->defaultValue(true)->after('invoice_paclist')
        );
        $this->addColumn(
            'user_config',
            'contr_inv_invfacture',
            $this->boolean()->notNull()->defaultValue(true)->after('contr_inv_paclist')
        );
        $this->addCommentOnColumn('user_config', 'invoice_invfacture', 'Показ колонки "Счет-фактура" в списке счетов');
        $this->addCommentOnColumn('user_config', 'contr_inv_invfacture', 'Показ колонки "Счет-фактура" в списке счетов контрагента');
    }

    public function safeDown()
    {
        $this->dropColumn('user_config', 'invoice_invfacture');
        $this->dropColumn('user_config', 'contr_inv_invfacture');
    }
}
