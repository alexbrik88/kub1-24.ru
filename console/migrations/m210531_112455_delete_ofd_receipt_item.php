<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210531_112455_delete_ofd_receipt_item extends Migration
{
    public function safeUp()
    {
        $this->delete('{{%ofd_receipt_item}}', ['receipt_id' => null]);
    }

    public function safeDown()
    {
        //
    }
}
