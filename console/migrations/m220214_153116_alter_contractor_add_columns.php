<?php

use console\components\db\Migration;
use yii\db\Schema;

class m220214_153116_alter_contractor_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%contractor}}', 'consignee_kpp_same', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn('{{%contractor}}', 'consignee_address_same', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn('{{%contractor}}', 'consignee_kpp', $this->string(50));
        $this->addColumn('{{%contractor}}', 'consignee_address', $this->string(500));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%contractor}}', 'consignee_kpp_same');
        $this->dropColumn('{{%contractor}}', 'consignee_address_same');
        $this->dropColumn('{{%contractor}}', 'consignee_kpp');
        $this->dropColumn('{{%contractor}}', 'consignee_address');
    }
}
