<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181129_122244_update_service_invoice_tariff extends Migration
{
    public $tableName = 'service_invoice_tariff';

    public function safeUp()
    {
        $this->update($this->tableName, ['total_amount' => 150], ['id' => 1]);
        $this->update($this->tableName, ['total_amount' => 300], ['id' => 2]);
    }
    
    public function safeDown()
    {
        $this->update($this->tableName, ['total_amount' => 100], ['id' => 1]);
        $this->update($this->tableName, ['total_amount' => 250], ['id' => 2]);
    }
}


