<?php

use console\components\db\Migration;

class m190907_063923_alter_amocrm_note_add_employee_id extends Migration
{

    public function safeUp()
    {
        $this->truncateTable('{{amocrm_note}}');
        $this->addColumn('{{amocrm_note}}', 'employee_id', $this->integer()->notNull()->comment('ID покупателя на сайте'));
        $this->addForeignKey(
            'amocrm_note_employee_id', '{{amocrm_note}}', 'employee_id',
            '{{employee}}', 'id', 'CASCADE', 'CASCADE'
        );

    }

    public function down()
    {
        $this->dropColumn('{{amocrm_note}}', 'employee_id');
    }

}
