<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210331_121815_clean_project_estimate_name extends Migration
{
    public function safeUp()
    {
        $this->update('project_estimate', ['name' => null]);
    }

    public function safeDown()
    {
    }
}
