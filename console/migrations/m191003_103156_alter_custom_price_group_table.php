<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191003_103156_alter_custom_price_group_table extends Migration
{
    public function safeUp()
    {
        $this->addColumn('custom_price_group', 'show_column', $this->boolean()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('custom_price_group', 'show_column');
    }
}
