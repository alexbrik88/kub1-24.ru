<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%dossier_log}}`.
 */
class m200317_154947_create_dossier_log_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%dossier_log}}', [
            'id' => $this->primaryKey(),
            'employee_id' => $this->integer()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'contractor_id' => $this->integer(),
            'created_at' => $this->integer()->notNull(),
            'expired_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('FK_dossier_log_to_employee', '{{%dossier_log}}', 'employee_id', '{{%employee}}', 'id');
        $this->addForeignKey('FK_dossier_log_to_company', '{{%dossier_log}}', 'company_id', '{{%company}}', 'id');
        $this->addForeignKey('FK_dossier_log_to_contractor', '{{%dossier_log}}', 'contractor_id', '{{%contractor}}', 'id');
        $this->createIndex('created_at', '{{%dossier_log}}', 'created_at');
        $this->createIndex('expired_at', '{{%dossier_log}}', 'expired_at');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%dossier_log}}');
    }
}
