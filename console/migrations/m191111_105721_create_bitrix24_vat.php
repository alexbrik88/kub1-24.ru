<?php

use console\components\db\Migration;

class m191111_105721_create_bitrix24_vat extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{bitrix24_vat}}', [
            'id' => $this->integer()->unsigned()->notNull()->comment('Идентификатор налога Битрикс24'),
            'employee_id' => $this->integer()->notNull()->comment('ID клиента КУБ'),
            'name' => $this->char(20)->notNull()->comment('Название налоговой ставки'),
            'rate' => $this->float()->unsigned()->notNull()->comment('Налоговая ставка'),
        ], "COMMENT 'Интеграция Битрикс24: единицы измерения'");
        $this->addForeignKey(
            'bitrix24_vat_employee_id', '{{bitrix24_vat}}', 'employee_id',
            '{{employee}}', 'id', 'CASCADE', 'CASCADE'
        );
        $this->addPrimaryKey('bitrix24_vat_id','{{bitrix24_vat}}',['employee_id','id']);
    }

    public function down()
    {
        $this->dropTable('{{bitrix24_vat}}');
    }
}
