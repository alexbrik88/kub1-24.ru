<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160212_102452_alter_page_type_id extends Migration
{
    public $tPageType = 'page_type';

    public function safeUp()
    {
        $this->dropTable($this->tPageType);

        $this->createTable($this->tPageType, [
            'id' => $this->primaryKey(11),
            'name' => $this->text(),
        ]);

        $this->batchInsert($this->tPageType, ['id', 'name'], [
            [3, 'Покупатели'],
            [4, 'Поставщики'],
            [5, 'Исходящие документы/Счет'],
            [10, 'Входящие документы/Счет'],
            [18, 'Услуги'],
            [19, 'Товары'],
            [20, 'Сотрудники'],
        ]);
    }
    
    public function safeDown()
    {
        $this->dropTable($this->tPageType);

        $this->createTable($this->tPageType, [
            'id' => $this->primaryKey(11),
            'name' => $this->text(),
        ]);
        $this->batchInsert($this->tPageType, ['id', 'name'], [
            [1, 'Покупатели'],
            [2, 'Поставщики'],
            [3, 'Исходящие документы/Счет'],
            [4, 'Входящие документы/Счет'],
            [5, 'Услуги'],
            [6, 'Товары'],
            [7, 'Сотрудники'],
        ]);
    }
}


