<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200923_085527_alter_contractor_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%contractor}}', 'foreign_legal_form', $this->string()->after('verified'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%contractor}}', 'foreign_legal_form');
    }
}
