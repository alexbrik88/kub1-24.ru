<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211206_095713_update_rent_attribute extends Migration
{
    public function safeUp()
    {
        //спец-техника
        $this->batchInsert('{{%rent_attribute}}', [
            'category_id',
            'alias',
            'title',
            'unit',
            'type',
            'required',
            'sort',
            'default',
            'containerCSS',
            'data',
            'tab',
            'show',
            'grid_cols',
        ], [
            [2, "stateNumber", "Гос номер", NULL, "string", 0, 1, NULL, NULL, NULL, "info", 1, 3],
            [2, "validityPeriod", "Срок действия ТО", NULL, "date", 0, 2, NULL, NULL, NULL, "info", 1, 3],
            [2, "VIN", "VIN / Заводской номер", NULL, "string", 0, 8, NULL, NULL, NULL, "info", 1, 6],
        ]);

        $this->update('{{%rent_attribute}}', [
            'sort' => 3,
            'grid_cols' => 6,
        ], [
            'category_id' => 2,
            'alias' => 'address',
        ]);

        $this->update('{{%rent_attribute}}', [
            'sort' => 4,
        ], [
            'category_id' => 2,
            'alias' => 'carrying',
        ]);

        $this->update('{{%rent_attribute}}', [
            'sort' => 5,
        ], [
            'category_id' => 2,
            'alias' => 'chassis',
        ]);

        $this->update('{{%rent_attribute}}', [
            'sort' => 6,
        ], [
            'category_id' => 2,
            'alias' => 'volume',
        ]);

        $this->update('{{%rent_attribute}}', [
            'sort' => 7,
        ], [
            'category_id' => 2,
            'alias' => 'boomLength',
        ]);

        $this->update('{{%rent_attribute}}', [
            'sort' => 9,
        ], [
            'category_id' => 2,
            'alias' => 'shiftHours',
        ]);

        // вагон-дом
        $this->update('{{%rent_attribute}}', [
            'grid_cols' => 6,
        ], [
            'category_id' => 4,
            'alias' => 'address',
        ]);

        $this->update('{{%rent_attribute}}', [
            'required' => false,
        ], [
            'category_id' => 4,
            'alias' => 'carrying',
        ]);

        $this->update('{{%rent_attribute}}', [
            'sort' => 6,
            'grid_cols' => 6,
        ], [
            'category_id' => 4,
            'alias' => 'VIN',
        ]);

        $this->update('{{%rent_attribute}}', [
            'sort' => 7,
        ], [
            'category_id' => 4,
            'alias' => 'shiftHours',
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%rent_attribute}}', [
            'category_id' => 2,
            'alias' => [
                "stateNumber",
                "validityPeriod",
                "VIN",
            ],
        ]);
    }
}
