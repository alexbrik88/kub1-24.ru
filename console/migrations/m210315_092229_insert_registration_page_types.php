<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210315_092229_insert_registration_page_types extends Migration
{
    public $tableName = '{{%registration_page_type}}';

    public function safeUp()
    {
        $this->batchInsert($this->tableName, ['id', 'name'], [
            [33, 'Аналитика / План-факт'],
            [34, 'Аналитика / Платежный календарь'],
            [35, 'Аналитика / ОПиУ'],
            [36, 'Аналитика / Баланс'],
            [37, 'Аналитика / Финмодель'],
            [38, 'Аналитика / ТочкаБезуб'],
        ]);
    }

    public function safeDown()
    {
        $this->delete($this->tableName, ['id' => [33,34,35,36,37,38]]);
    }
}
