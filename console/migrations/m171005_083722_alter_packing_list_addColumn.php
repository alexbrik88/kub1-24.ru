<?php

use console\components\db\Migration;
use yii\db\Schema;

class m171005_083722_alter_packing_list_addColumn extends Migration
{
    public $tableName = 'packing_list';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'consignee_id', $this->integer()->after('consignor_id'));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'consignee_id');
    }
}
