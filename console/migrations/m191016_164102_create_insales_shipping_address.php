<?php

use console\components\db\Migration;

class m191016_164102_create_insales_shipping_address extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%insales_shipping_address}}', [
            'id' => $this->primaryKey()->unsigned()->notNull()->comment('ID адреса в InSales'),
            'client_id' => $this->integer()->unsigned()->notNull()->comment('ID клиента в InSales'),
            'name' => $this->string(30)->notNull()->comment('Имя получателя'),
            'surname' => $this->string(30)->comment('Фамилия получателя'),
            'middlename' => $this->string(30)->comment('Отчество получателя'),
            'phone' => $this->char(15)->comment('Номер телефона'),
            'full_delivery_address' => $this->string(300)->notNull()->comment('Полный адрес доставки'),
            'latitude' => $this->float()->unsigned(),
            'longitude' => $this->float()->unsigned(),
        ], "COMMENT 'Интеграция InSales CMS: адреса доставки'");
        $this->addForeignKey(
            'insales_shipping_address_client_id', '{{insales_shipping_address}}', 'client_id',
            '{{insales_client}}', 'id', 'CASCADE', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable('{{%insales_shipping_address}}');

    }
}
