<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190626_064245_alter_invoice_facture_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('invoice_facture', 'auto_created', $this->boolean()->defaultValue(false));
        $this->createIndex('auto_created', 'invoice_facture', 'auto_created');
    }

    public function safeDown()
    {
        $this->dropIndex('auto_created', 'invoice_facture');
        $this->dropColumn('invoice_facture', 'auto_created');
    }
}
