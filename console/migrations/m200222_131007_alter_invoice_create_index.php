<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200222_131007_alter_invoice_create_index extends Migration
{
    public function safeUp()
    {
        $this->createIndex('created_at', '{{%invoice}}', 'created_at');
        $this->createIndex('payment_limit_date', '{{%invoice}}', 'payment_limit_date');
        $this->createIndex('invoice_status_updated_at', '{{%invoice}}', 'invoice_status_updated_at');
    }

    public function safeDown()
    {
        $this->dropIndex('created_at', '{{%invoice}}');
        $this->dropIndex('payment_limit_date', '{{%invoice}}');
        $this->dropIndex('invoice_status_updated_at', '{{%invoice}}');
    }
}
