<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211122_102009_alter_rent_agreement_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%rent_agreement}}', 'payment_period', $this->tinyInteger(1));
        $this->addColumn('{{%rent_agreement}}', 'has_discount', $this->boolean()->defaultValue(false));
        $this->addColumn('{{%rent_agreement}}', 'discount_type', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn('{{%rent_agreement}}', 'is_hidden_discount', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn('{{%rent_agreement}}', 'comment', $this->string(500)->defaultValue(''));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%rent_agreement}}', 'payment_period');
        $this->dropColumn('{{%rent_agreement}}', 'has_discount');
        $this->dropColumn('{{%rent_agreement}}', 'discount_type');
        $this->dropColumn('{{%rent_agreement}}', 'is_hidden_discount');
        $this->dropColumn('{{%rent_agreement}}', 'comment');
    }
}
