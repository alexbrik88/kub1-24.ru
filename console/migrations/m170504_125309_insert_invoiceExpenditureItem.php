<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170504_125309_insert_invoiceExpenditureItem extends Migration
{
    public function safeUp()
    {
        $this->batchInsert('{{%invoice_expenditure_item}}', ['name', 'sort'], [
            ['Доставка', 100],
            ['Бухгалтерия', 100],
            ['Услуги Юристов', 100],
            ['Налог по УСН', 100],
            ['Фиксированный платеж в ПФР', 100],
            ['НДФЛ с дивидендов', 100],
            ['Проект 1', 100],
            ['Проект 2', 100],
            ['Проект 3', 100],
        ]);
    }
    
    public function safeDown()
    {
        $this->delete('{{%invoice_expenditure_item}}', ['name' => [
            'Доставка',
            'Бухгалтерия',
            'Услуги Юристов',
            'Налог по УСН',
            'Фиксированный платеж в ПФР',
            'НДФЛ с дивидендов',
            'Проект 1',
            'Проект 2',
            'Проект 3',
        ]]);
    }
}
