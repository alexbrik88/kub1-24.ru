<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201104_084634_alter_plan_cash_flows_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('plan_cash_flows', 'first_date', $this->date()->after('date'));
        $this->execute('UPDATE `plan_cash_flows` SET `first_date` = `date`');
    }

    public function safeDown()
    {
        $this->dropColumn('plan_cash_flows', 'first_date');
    }
}
