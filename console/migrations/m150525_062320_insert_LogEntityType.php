<?php

use yii\db\Schema;
use yii\db\Migration;

class m150525_062320_insert_LogEntityType extends Migration
{
    public function safeUp()
    {
        $this->delete('log_entity_type');
        $this->batchInsert('log_entity_type', ['id', 'name',], [
            [1, 'Документы'],
            [2, 'Деньги'],
        ]);
    }
    
    public function safeDown()
    {
        $this->delete('log_entity_type');

        $this->batchInsert('log_entity_type', ['id', 'name',],  [
            [1, 'счёт'],
            [2, 'акт'],
            [3, 'товарная накладная'],
            [4, 'счёт-фактура'],
            [5, 'платёжное поручение'],
            [6, 'банковская операция'],
            [7, 'перевод e-money'],
            [8, 'кассовый ордер'],
        ]);
    }
}
