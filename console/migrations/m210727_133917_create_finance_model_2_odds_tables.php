<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210727_133917_create_finance_model_2_odds_tables extends Migration
{
    const MODEL = 'finance_plan';
    const ODDS_INCOME_ITEM = 'finance_plan_odds_income_item';
    const ODDS_INCOME_ITEM_AUTOFILL = 'finance_plan_odds_income_item_autofill';

    public function safeUp()
    {
        /*
         * Item
         */
        $this->createTable(self::ODDS_INCOME_ITEM, [
            'id' => $this->primaryKey(),
            'model_id' => $this->integer()->notNull(),
            'income_item_id' => $this->integer()->notNull(),
            'type' => $this->tinyInteger()->unsigned()->notNull()->comment('1 - операционная деятельность, 2 - финансовая, 3 - инвестиционная'),
            'payment_type' => $this->tinyInteger()->unsigned()->comment('1 - предоплата, 2 - доплата'),
            'relation_type' => $this->tinyInteger()->unsigned()->notNull()->defaultValue(0)->comment('0 - нет, 1 - выручка, 2 - кол-во продаж, 3 - ebidta'),
            'action' => $this->float(),
            'data' => $this->text()
        ]);

        $this->addForeignKey('FK_'.self::ODDS_INCOME_ITEM.'_to_model', self::ODDS_INCOME_ITEM, 'model_id', self::MODEL, 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_'.self::ODDS_INCOME_ITEM.'_to_income_items', self::ODDS_INCOME_ITEM, 'income_item_id', 'invoice_income_item', 'id', 'CASCADE', 'CASCADE');

        /*
         * Autofill Item
         */
        $this->createTable(self::ODDS_INCOME_ITEM_AUTOFILL, [
            'model_id' => $this->integer(11)->notNull(),
            'row_id' => $this->integer()->notNull(),
            'start_month' => $this->integer(11)->notNull(),
            'start_amount' => $this->bigInteger(20),
            'amount' => $this->bigInteger(20),
            'limit_amount' => $this->bigInteger(20),
            'action' => $this->tinyInteger()->unsigned()->notNull(),
        ]);


        $this->addPrimaryKey('PK_'.self::ODDS_INCOME_ITEM_AUTOFILL, self::ODDS_INCOME_ITEM_AUTOFILL, ['model_id', 'row_id']);
        $this->addForeignKey('FK_'.self::ODDS_INCOME_ITEM_AUTOFILL.'_to_model', self::ODDS_INCOME_ITEM_AUTOFILL, 'model_id', self::MODEL, 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_'.self::ODDS_INCOME_ITEM_AUTOFILL.'_to_item', self::ODDS_INCOME_ITEM_AUTOFILL, 'row_id', self::ODDS_INCOME_ITEM, 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_'.self::ODDS_INCOME_ITEM.'_to_income_items', self::ODDS_INCOME_ITEM);
        $this->dropForeignKey('FK_'.self::ODDS_INCOME_ITEM.'_to_model', self::ODDS_INCOME_ITEM);
        $this->dropForeignKey('FK_'.self::ODDS_INCOME_ITEM_AUTOFILL.'_to_model', self::ODDS_INCOME_ITEM_AUTOFILL);
        $this->dropForeignKey('FK_'.self::ODDS_INCOME_ITEM_AUTOFILL.'_to_item', self::ODDS_INCOME_ITEM_AUTOFILL);

        $this->dropTable(self::ODDS_INCOME_ITEM);
        $this->dropTable(self::ODDS_INCOME_ITEM_AUTOFILL);
    }
}
