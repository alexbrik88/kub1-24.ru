<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201220_195309_alter_project_estimate_item_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('project_estimate_item', 'title', $this->text()->after('product_id'));

        $this->execute("UPDATE project_estimate_item e LEFT JOIN product p ON e.product_id = p.id SET e.title = p.title");
    }

    public function safeDown()
    {
        $this->dropColumn('project_estimate_item', 'title');
    }
}
