<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201106_074518_alter_project_estimate_item_append_nds_columns extends Migration
{
    public $tableName = 'project_estimate_item';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'hasNds', $this->integer(1)->after('amount'));
        $this->addColumn($this->tableName, 'nds', $this->integer(2)->after('hasNds'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'hasNds');
        $this->dropColumn($this->tableName, 'nds');
    }

}