<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200717_080332_alter_user_config_append_project_tables extends Migration
{
    public $table = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->table, 'table_view_project_money', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT true');
        $this->addColumn($this->table, 'table_view_project_invoice', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT true');
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'table_view_project_money');
        $this->dropColumn($this->table, 'table_view_project_invoice');
    }
}
