<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211113_191614_alter_project_drop_column extends Migration
{
    public function safeUp()
    {
        $this->dropForeignKey('FK_project_to_project_direction', 'project');
        $this->dropColumn('project', 'direction_id');
    }

    public function safeDown()
    {
        $this->addColumn('project', 'direction_id', $this->integer()->defaultValue(1));
        $this->update('project', ['direction_id' => 1]);
        $this->addForeignKey('FK_project_to_project_direction', 'project', 'direction_id', 'project_direction', 'id', 'SET NULL');
    }
}
