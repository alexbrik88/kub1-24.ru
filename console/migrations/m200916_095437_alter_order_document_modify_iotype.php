<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200916_095437_alter_order_document_modify_iotype extends Migration
{
    public $tableName = 'order_document';

    public function safeUp()
    {
        $this->alterColumn($this->tableName, 'type', $this->integer(1));
    }

    public function safeDown()
    {
        $this->alterColumn($this->tableName, 'type', $this->integer(1)->defaultValue(2));
    }
}
