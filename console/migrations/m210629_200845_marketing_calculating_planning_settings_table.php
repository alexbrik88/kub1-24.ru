<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210629_200845_marketing_calculating_planning_settings_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('marketing_calculating_planning_settings', [
            'marketing_plan_id' => $this->integer(11)->unsigned()->notNull(),
            'channel' => $this->tinyInteger()->unsigned()->notNull(),
            'attribute' => $this->string()->notNull(),
            'start_planning_item_id' => $this->integer(11)->notNull(),
            'action' => $this->tinyInteger()->unsigned()->notNull(),
            'start_amount' => $this->bigInteger(20),
            'amount' => $this->bigInteger(20),
            'limit_amount' => $this->bigInteger(20),
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', 'marketing_calculating_planning_settings', ['marketing_plan_id', 'channel', 'attribute']);

        $this->addForeignKey(
            'FK_marketing_calculating_planning_settings_to_marketing_plans',
            'marketing_calculating_planning_settings',
            'marketing_plan_id',
            'marketing_plan',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropTable('marketing_calculating_planning_settings');
    }
}
