<?php

use yii\db\Migration;

/**
 * Handles the creation of table `autoact`.
 */
class m171221_082834_create_autoact_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('autoact', [
            'company_id' => $this->primaryKey(),
            'rule' => $this->smallInteger(1)->notNull()->defaultValue(0),
            'status' => $this->smallInteger(1)->notNull()->defaultValue(0),
            'send_together' => $this->boolean(1)->notNull()->defaultValue(false),
            'send_auto' => $this->boolean(1)->notNull()->defaultValue(false),
            'no_pay_partial' => $this->boolean(1)->notNull()->defaultValue(false),
            'no_pay_order' => $this->boolean(1)->notNull()->defaultValue(false),
            'no_pay_emoney' => $this->boolean(1)->notNull()->defaultValue(false),
        ]);

        $this->addForeignKey('fk_autoact_company', 'autoact', 'company_id', 'company', 'id', 'CASCADE');

        $this->createIndex('I_rule', 'autoact', 'rule');

        $this->addCommentOnColumn('autoact', 'rule', 'Правило выставления');
        $this->addCommentOnColumn('autoact', 'status', 'Выставлять по всем или только оплаченным');
        $this->addCommentOnColumn('autoact', 'send_together', 'Отправлять клиенту Счет и Акт в одном письме');
        $this->addCommentOnColumn('autoact', 'send_auto', 'Автоматическая отправка Акта клиенту, после выставления');
        $this->addCommentOnColumn('autoact', 'no_pay_partial', 'К частично оплаченным счетам акты не выставлять');
        $this->addCommentOnColumn('autoact', 'no_pay_order', 'Акты не выставлять к счетам, оплаченным по кассе');
        $this->addCommentOnColumn('autoact', 'no_pay_emoney', 'Акты не выставлять к счетам, оплаченным по e-money');

        $this->execute("
            INSERT INTO {{autoact}} ([[company_id]], [[rule]])
            SELECT {{company}}.[[id]], 1
            FROM {{company}}
        ");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('autoact');
    }
}
