<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211018_131214_alter_crm_task_comment_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%crm_task_comment}}', 'contractor_id', $this->integer()->after('id'));

        $this->execute('
            UPDATE {{%crm_task_comment}}
            LEFT JOIN {{%crm_task}} ON {{%crm_task}}.[[task_id]] = {{%crm_task_comment}}.[[crm_task_id]]
            SET {{%crm_task_comment}}.[[contractor_id]] = {{%crm_task}}.[[contractor_id]]
        ');

        $this->execute('SET foreign_key_checks = 0');
        $this->addForeignKey('fk_crm_task_comment__contractor_id', '{{%crm_task_comment}}', 'contractor_id', '{{%contractor}}', 'id');
        $this->execute('SET foreign_key_checks = 1');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_crm_task_comment__contractor_id', '{{%crm_task_comment}}');

        $this->dropColumn('{{%crm_task_comment}}', 'contractor_id');
    }
}
