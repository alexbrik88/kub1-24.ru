<?php

use console\components\db\Migration;
use yii\db\Schema;
use common\models\document\Invoice;

class m151222_114254_alter_invoice_created_at_and_update_at extends Migration
{
    public $tInvoice = 'invoice';

    public function safeUp()
    {
        $this->addColumn($this->tInvoice, 'created', $this->integer(11)->notNull());
        $this->update($this->tInvoice, [
            'created' => new \yii\db\Expression('UNIX_TIMESTAMP (created_at)'),
        ]);
        $this->dropColumn($this->tInvoice, 'created_at');
        $this->renameColumn($this->tInvoice, 'created', 'created_at');

        $this->addColumn($this->tInvoice, 'updated', $this->integer(11)->notNull());
        $this->update($this->tInvoice, [
            'updated' => new \yii\db\Expression('UNIX_TIMESTAMP (invoice_status_updated_at)'),
        ]);
        $this->dropColumn($this->tInvoice, 'invoice_status_updated_at');
        $this->renameColumn($this->tInvoice, 'updated', 'invoice_status_updated_at');
        $this->update($this->tInvoice, ['invoice_status_updated_at' => null], ['invoice_status_updated_at' => 0]);
    }
    
    public function safeDown()
    {
        echo 'This migration should not be rolled back';
    }
}


