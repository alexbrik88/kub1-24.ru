<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180209_161410_insert_agreement_type extends Migration
{
    public function safeUp()
    {
        $this->insert('agreement_type', [
            'id' => 5,
            'name' => 'Дополнительное соглашение',
            'name_dative' => 'дополнительному соглашению',
        ]);
    }

    public function safeDown()
    {
        $this->delete('agreement_type', ['id' => 5]);
    }
}
