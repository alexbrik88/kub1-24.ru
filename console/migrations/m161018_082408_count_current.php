<?php

use console\components\db\Migration;
use yii\db\Schema;

class m161018_082408_count_current extends Migration
{
    public function safeUp()
    {
    	$this->addColumn('product', 'product_begin_count', $this->integer());
    }
    
    public function safeDown()
    {
    	$this->dropColumn('product', 'product_begin_count') ;
    }
}


