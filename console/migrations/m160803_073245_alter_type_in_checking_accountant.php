<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160803_073245_alter_type_in_checking_accountant extends Migration
{
    public $tCheckingAccountant = 'checking_accountant';

    public function safeUp()
    {
        $this->alterColumn($this->tCheckingAccountant, 'type', $this->integer(3)->notNull()->comment('0 - основной, 1 - дополнительный, 2 - закрытый'));
    }

    public function safeDown()
    {
        $this->alterColumn($this->tCheckingAccountant, 'type', $this->boolean()->notNull());
    }
}


