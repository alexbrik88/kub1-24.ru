<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211103_143643_alter_banking_email_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('banking_email', 'autoload_mode', $this->boolean()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('banking_email', 'autoload_mode');
    }
}
