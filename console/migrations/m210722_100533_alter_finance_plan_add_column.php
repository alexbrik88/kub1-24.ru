<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210722_100533_alter_finance_plan_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('finance_plan', 'is_main', $this->tinyInteger()->defaultValue(0));
        $this->addColumn('finance_plan', 'sort', $this->integer()->defaultValue(1));
        $this->execute(' UPDATE `finance_plan` SET `is_main` = 1 WHERE `id` IN (SELECT MIN(`id`) FROM `finance_plan` GROUP BY `model_group_id`)');
        $this->execute(' UPDATE `finance_plan` SET `sort` = 2 WHERE `is_main` = 0');
    }

    public function safeDown()
    {
        $this->dropColumn('finance_plan', 'is_main');
        $this->dropColumn('finance_plan', 'sort');
    }
}
