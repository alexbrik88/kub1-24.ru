<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170908_094041_create_chat extends Migration
{
    public $tableName = 'chat';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull(),
            'from_employee_id' => $this->integer()->notNull(),
            'to_employee_id' => $this->integer()->notNull(),
            'message' => $this->text()->notNull(),
        ]);

        $this->addForeignKey($this->tableName . '_from_employee_id', $this->tableName, 'from_employee_id', 'employee', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tableName . '_to_employee_id', $this->tableName, 'to_employee_id', 'employee', 'id', 'RESTRICT', 'CASCADE');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_form_employee_id', $this->tableName);
        $this->dropForeignKey($this->tableName . '_to_employee_id', $this->tableName);

        $this->dropTable($this->tableName);
    }
}


