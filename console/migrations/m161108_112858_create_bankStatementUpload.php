<?php

use console\components\db\Migration;
use yii\db\Schema;

class m161108_112858_create_bankStatementUpload extends Migration
{
    public $tStatement = 'cash_bank_statement_upload';

    public function safeUp()
    {
        $this->createTable($this->tStatement, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'bik' => $this->char(9)->notNull(),
            'rs' => $this->string(255)->notNull(),
            'created_by' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('FK_bank_statement_upload_to_employee', $this->tStatement, 'created_by', 'employee', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('FK_bank_statement_upload_to_company', $this->tStatement, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');
        $this->createIndex('created_at', $this->tStatement, 'created_at', false);
    }
    
    public function safeDown()
    {
        $this->dropTable($this->tStatement);
    }
}
