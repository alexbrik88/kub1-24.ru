<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201102_120923_alter_user_config_append_columns extends Migration
{
    public $tableName = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'project_estimate_number', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'project_estimate_responsible', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'table_project_estimate', $this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'project_estimate_number');
        $this->dropColumn($this->tableName, 'project_estimate_responsible');
        $this->dropColumn($this->tableName, 'table_project_estimate');
    }
}
