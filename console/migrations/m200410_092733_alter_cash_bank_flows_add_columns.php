<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200410_092733_alter_cash_bank_flows_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cash_bank_flows}}', 'is_taxrobot_manual_entry', $this->boolean()->defaultValue(false));
        $this->addColumn('{{%cash_bank_flows}}', 'taxrobot_manual_entry_year', $this->integer(4));
        $this->addColumn('{{%cash_bank_flows}}', 'taxrobot_manual_entry_attribute', $this->string(10));

        $this->createIndex('is_taxrobot_manual_entry', '{{%cash_bank_flows}}', 'is_taxrobot_manual_entry');
        $this->createIndex('taxrobot_manual_entry', '{{%cash_bank_flows}}', [
            'company_id',
            'taxrobot_manual_entry_year',
            'taxrobot_manual_entry_attribute',
        ], true);
    }

    public function safeDown()
    {
        $this->dropIndex('is_taxrobot_manual_entry', '{{%cash_bank_flows}}');
        $this->dropIndex('taxrobot_manual_entry', '{{%cash_bank_flows}}');

        $this->dropColumn('{{%cash_bank_flows}}', 'is_taxrobot_manual_entry');
        $this->dropColumn('{{%cash_bank_flows}}', 'taxrobot_manual_entry_year');
        $this->dropColumn('{{%cash_bank_flows}}', 'taxrobot_manual_entry_attribute');
    }
}
