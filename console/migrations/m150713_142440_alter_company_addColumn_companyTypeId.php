<?php

use yii\db\Schema;
use yii\db\Migration;

class m150713_142440_alter_company_addColumn_companyTypeId extends Migration
{
    public function safeUp()
    {
        $this->addColumn('company', 'company_type_id', Schema::TYPE_INTEGER . ' NOT NULL');
        $this->update('company', [
            'company_type_id' => new \yii\db\Expression('company_form + 1'),
        ]);

        $this->addForeignKey('fk_company_to_company_type', 'company', 'company_type_id', 'company_type', 'id');
        $this->dropColumn('company', 'company_form');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('fk_company_to_company_type', 'company');

        $this->addColumn('company', 'company_form', Schema::TYPE_BOOLEAN . ' NOT NULL COMMENT "0 - ИП, 1 - ООО"');
        $this->update('company', [
            'company_type_id' => '1',
        ], 'company_type_id > 1');
        $this->update('company', [
            'company_form' => new \yii\db\Expression('company_type_id - 1'),
        ]);

        $this->dropColumn('company', 'company_type_id');
    }
}
