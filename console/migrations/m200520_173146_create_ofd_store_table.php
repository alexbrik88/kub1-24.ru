<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%ofd_store}}`.
 */
class m200520_173146_create_ofd_store_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%ofd_store}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'ofd_id' => $this->integer()->notNull(),
            'uid' => $this->string()->notNull()->comment('Идентификатор в базе ОФД'),
            'parent_uid' => $this->string()->comment('Идентификатор в базе ОФД'),
            'store_type_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'local_name' => $this->string()->notNull()->comment('Название в сервисе КУБ'),
            'address' => $this->string(),
            'created_at' => $this->integer(),
        ]);

        $this->addForeignKey('FK_ofd_store_company', '{{%ofd_store}}', 'company_id', '{{%company}}', 'id');
        $this->addForeignKey('FK_ofd_store_ofd', '{{%ofd_store}}', 'ofd_id', '{{%ofd}}', 'id');

        $this->createIndex('store', '{{%ofd_store}}', [
            'company_id',
            'ofd_id',
            'uid',
        ], true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%ofd_store}}');
    }
}
