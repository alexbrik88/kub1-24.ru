<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200205_173555_alter_price_list_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%price_list}}', 'has_discount', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn('{{%price_list}}', 'has_markup', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn('{{%price_list}}', 'discount', $this->decimal(9, 6)->notNull()->defaultValue(0));
        $this->addColumn('{{%price_list}}', 'markup', $this->decimal(9, 6)->notNull()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%price_list}}', 'discount');
        $this->dropColumn('{{%price_list}}', 'markup');
        $this->dropColumn('{{%price_list}}', 'has_discount');
        $this->dropColumn('{{%price_list}}', 'has_markup');
    }
}
