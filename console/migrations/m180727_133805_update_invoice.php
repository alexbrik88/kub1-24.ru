<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180727_133805_update_invoice extends Migration
{
    public function safeUp()
    {
        $this->execute("
            UPDATE {{invoice}} {{t}}
            SET
                {{t}}.[[has_services]] = EXISTS(
                    SELECT {{o}}.[[id]] FROM {{order}} {{o}}
                    LEFT JOIN {{product}} {{p}} ON {{p}}.[[id]] = {{o}}.[[product_id]]
                    WHERE {{o}}.[[invoice_id]] = {{t}}.[[id]] AND {{p}}.[[production_type]] = 0
                ),
                {{t}}.[[has_goods]] = EXISTS(
                    SELECT {{o}}.[[id]] FROM {{order}} {{o}}
                    LEFT JOIN {{product}} {{p}} ON {{p}}.[[id]] = {{o}}.[[product_id]]
                    WHERE {{o}}.[[invoice_id]] = {{t}}.[[id]] AND {{p}}.[[production_type]] = 1
                ),
                {{t}}.[[has_act]] = EXISTS(
                    SELECT {{a}}.[[id]] FROM {{act}} {{a}} WHERE {{a}}.[[invoice_id]] = {{t}}.[[id]]
                ),
                {{t}}.[[has_packing_list]] = EXISTS(
                    SELECT {{p}}.[[id]] FROM {{packing_list}} {{p}} WHERE {{p}}.[[invoice_id]] = {{t}}.[[id]]
                ),
                {{t}}.[[has_invoice_facture]] = EXISTS(
                    SELECT {{i}}.[[id]] FROM {{invoice_facture}} {{i}} WHERE {{i}}.[[invoice_id]] = {{t}}.[[id]]
                ),
                {{t}}.[[has_upd]] = EXISTS(
                    SELECT {{u}}.[[id]] FROM {{upd}} {{u}} WHERE {{u}}.[[invoice_id]] = {{t}}.[[id]]
                ),
                {{t}}.[[has_file]] = IF(
                    EXISTS(SELECT {{f}}.[[id]] FROM {{file}} {{f}} WHERE {{f}}.[[owner_id]] = {{t}}.id AND {{f}}.[[owner_model]] = :class), 1,
                    EXISTS(SELECT {{s}}.[[id]] FROM {{scan_document}} {{s}} WHERE {{s}}.[[owner_id]] = {{t}}.id AND {{s}}.[[owner_model]] = :class)
                )
        ", [':class' => 'common\models\document\Invoice']);
    }

    public function safeDown()
    {
    }
}
