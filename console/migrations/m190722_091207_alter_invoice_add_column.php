<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190722_091207_alter_invoice_add_column extends Migration
{
    public function safeUp()
    {
        Yii::$app->db->createCommand('SET session wait_timeout=28800')->execute();
        Yii::$app->db->createCommand('SET session interactive_timeout=28800')->execute();

        $this->addColumn('invoice', 'store_id' , $this->integer());
        $this->addForeignKey('FK_invoice_to_store', 'invoice', 'store_id', 'store', 'id', 'SET NULL');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('FK_invoice_to_store', 'invoice');
        $this->dropColumn('invoice', 'store_id');
    }
}


