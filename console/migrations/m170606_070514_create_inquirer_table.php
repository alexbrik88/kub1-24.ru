<?php

use yii\db\Migration;

/**
 * Handles the creation of table `inquirer`.
 */
class m170606_070514_create_inquirer_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%inquirer}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'employee_id' => $this->integer()->notNull(),
            'rating' => $this->smallInteger(1),
            'plan_id' => $this->smallInteger(1),
            'difficulty' => $this->string(),
            'problem' => $this->string(),
            'created_at' => $this->integer()->notNull(),
        ]);
        
        $this->addForeignKey ('FK_inquirer_company', '{{%inquirer}}', 'company_id', '{{%company}}', 'id', 'CASCADE');
        $this->addForeignKey ('FK_inquirer_employee', '{{%inquirer}}', 'employee_id', '{{%employee}}', 'id', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%inquirer}}');
    }
}
