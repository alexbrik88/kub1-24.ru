<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210713_071311_alter_user_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%user_config}}', 'crm_client_comment', $this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%user_config}}', 'crm_client_comment');
    }
}
