<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210128_074447_insert_registration_page_type extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%registration_page_type}}', [
            'id' => 32,
            'name' => 'Лендинг Инвойсы',
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%registration_page_type}}', [
            'id' => 32,
        ]);
    }
}
