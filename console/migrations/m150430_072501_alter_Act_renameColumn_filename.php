<?php

use yii\db\Schema;
use yii\db\Migration;

class m150430_072501_alter_Act_renameColumn_filename extends Migration
{
    public function safeUp()
    {
        $this->renameColumn('act', 'filename', 'document_filename');
    }

    public function safeDown()
    {
        $this->renameColumn('act', 'document_filename', 'filename');
    }
}
