<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210415_082530_add_registration_page_type extends Migration
{
    public $tableName = '{{%registration_page_type}}';

    public function safeUp()
    {
        $this->insert($this->tableName, [
            'id' => 40,
            'name' => 'Лендинг Товары',
        ]);
    }

    public function safeDown()
    {
        $this->delete($this->tableName, ['id' => 40]);
    }
}
