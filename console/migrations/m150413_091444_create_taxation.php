<?php

use yii\db\Schema;
use yii\db\Migration;

class m150413_091444_create_taxation extends Migration
{
    public function up()
    {
        $this->createTable('taxation_type', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . '(45) NOT NULL',
        ]);
        $this->batchInsert('taxation_type', ['id', 'name',], [
            [1, 'ОСНО',],
            [2, 'УСН (6%)',],
            [3, 'УСН (15%)',],
            [4, 'ЕНВД',],
        ]);
    }

    public function down()
    {
        $this->dropTable('taxation_type');
    }
}
