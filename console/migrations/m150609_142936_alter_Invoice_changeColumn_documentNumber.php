<?php

use yii\db\Schema;
use yii\db\Migration;

class m150609_142936_alter_Invoice_changeColumn_documentNumber extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('invoice', 'document_number', Schema::TYPE_STRING . '(255) NOT NULL');
    }
    
    public function safeDown()
    {
        $this->alterColumn('invoice', 'document_number', Schema::TYPE_INTEGER . ' NOT NULL');
    }
}
