<?php

use yii\db\Migration;

class m200629_100135_create_telegram_code_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%telegram_code}}';

    /** @var string[] */
    private const FK_COLUMNS = ['employee_id', 'company_id'];

    /**
     * @inheritDoc
     */
    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'code' => $this->char(4),
            'employee_id' => $this->integer(),
            'company_id' => $this->integer(),
            'expired_at' => $this->timestamp(),
        ]);
        $this->addPrimaryKey('pk_code', self::TABLE_NAME, ['code']);
        $this->createIndex('uk_code', self::TABLE_NAME, self::FK_COLUMNS, true);
        $this->addForeignKey(
            'fk_telegram_code__employee_company',
            self::TABLE_NAME,
            self::FK_COLUMNS,
            '{{%employee_company}}',
            self::FK_COLUMNS,
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
