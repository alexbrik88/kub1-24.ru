<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180629_113525_alter_product_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('product', 'barcode', $this->string(30)->defaultValue('')->after('article'));
    }

    public function safeDown()
    {
        $this->dropColumn('product', 'barcode');
    }
}
