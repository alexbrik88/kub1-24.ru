<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191210_170711_alter_employee_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%employee}}', 'is_old_kub_theme', $this->boolean()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%employee}}', 'is_old_kub_theme');
    }
}
