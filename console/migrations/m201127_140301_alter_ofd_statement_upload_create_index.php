<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201127_140301_alter_ofd_statement_upload_create_index extends Migration
{
    public function safeUp()
    {
        $this->createIndex('I__store_id__created_at', '{{%ofd_statement_upload}}', [
            'store_id',
            'created_at',
        ]);
    }

    public function safeDown()
    {
        $this->dropIndex('I__store_id__created_at', '{{%ofd_statement_upload}}');
    }
}
