<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180614_220701_add_new_agreement_type extends Migration
{
    public $tableName = 'agreement_type';

    public function safeUp()
    {
        $this->insert($this->tableName, [
            'id' => 6,
            'name' => 'Приложение',
            'name_dative' => 'приложению',
        ]);
    }

    public function safeDown()
    {
        $this->delete($this->tableName, ['id' => 6]);
    }
}


