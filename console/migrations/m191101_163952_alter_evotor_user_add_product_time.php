<?php

use console\components\db\Migration;

class m191101_163952_alter_evotor_user_add_product_time extends Migration
{
    public function safeUp()
    {
        $this->addColumn(
            '{{evotor_user}}',
            'product_time',
            $this->integer()->unsigned()->notNull()->defaultValue(0)->comment('UNIXTIME последней загрузки товаров')
        );
        $this->addColumn(
            '{{evotor_user}}',
            'product_load_attempts',
            $this->tinyInteger()->unsigned()->notNull()->defaultValue(0)->comment('Количество неудачных попыток загрузить номенклатуру')
        );
    }

    public function safeDown()
    {
        $this->dropColumn('{{evotor_user}}', 'product_time');
        $this->dropColumn('{{evotor_user}}', 'product_load_attempts');
    }
}
