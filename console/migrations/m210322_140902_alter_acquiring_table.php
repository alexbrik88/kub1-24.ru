<?php

use yii\db\Migration;

class m210322_140902_alter_acquiring_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%acquiring}}';

    /**
     * @inheritDoc
     */
    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'is_active', $this->boolean()->unsigned()->notNull()->defaultValue(true));
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'is_active');
    }
}
