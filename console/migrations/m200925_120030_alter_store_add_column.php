<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200925_120030_alter_store_add_column extends Migration
{
    public $tableName = 'store';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'object_guid', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'object_guid');
    }
}
