<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210913_055540_alter_config_add_column extends Migration
{
    public $table = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->table, 'payments_request_order_responsible', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 1');
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'payments_request_order_responsible');
    }
}
