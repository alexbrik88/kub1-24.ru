<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210508_131538_alter_acquiring_operation_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('acquiring_operation', 'project_id', $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn('acquiring_operation', 'project_id');
    }
}
