<?php

use yii\db\Migration;

class m210406_220448_create_zenmoney_identity_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%zenmoney_identity}}';

    /**
     * @inheritDoc
     */
    public function up(): void
    {
        $this->createTable(self::TABLE_NAME, [
            'account_id' => $this->bigInteger()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'access_token' => $this->string()->notNull(),
            'expired_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->addPrimaryKey('pk_account_id', self::TABLE_NAME, ['account_id']);
        $this->createIndex('ck_company_id', self::TABLE_NAME, ['company_id']);
        $this->addForeignKey(
            'fk_zenmoney_identity_company_id__company_id',
            self::TABLE_NAME,
            ['company_id'],
            '{{%company}}',
            ['id'],
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritDoc
     */
    public function down(): void
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
