<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210511_145535_insert_invoice_income_item extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%invoice_income_item}}', [
            'id' => 24,
            'name' => 'Товар',
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%invoice_income_item}}', [
            'id' => 24,
        ]);
    }
}
