<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181014_160019_alter_agreement_new_add_column extends Migration
{
    public function safeUp()
    {
		$this->addColumn('{{%agreement_new}}', 'status_out_updated_at', $this->integer());
    }
    
    public function safeDown()
    {

    }
}


