<?php

use console\components\db\Migration;
use yii\db\Schema;

class m161003_151008_invoice_update extends Migration
{
    public $tInvoice = '{{%invoice}}';

    public function safeUp()
    {
        $this->alterColumn($this->tInvoice, 'contractor_address_legal_full', $this->char(255)->null());
    }
    
    public function safeDown()
    {
        $this->alterColumn($this->tInvoice, 'contractor_address_legal_full', $this->char(255)->notNull());
    }
}


