<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200818_084601_alter_company_append_column_referral_code extends Migration
{
    public $tableName = 'company';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'invited_by_referral_code', $this->string(32));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'invited_by_referral_code');
    }
}
