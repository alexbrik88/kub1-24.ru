<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200831_083815_create_triggers_prevent_invoice_overpayment extends Migration
{
    public function safeUp()
    {
        $this->execute('DROP TRIGGER IF EXISTS `cash_bank_flow_to_invoice__prevent_overpayment`');
        $this->execute('DROP TRIGGER IF EXISTS `cash_order_flow_to_invoice__prevent_overpayment`');
        $this->execute('DROP TRIGGER IF EXISTS `cash_emoney_flow_to_invoice__prevent_overpayment`');

        $triggerBody = '
        
              DECLARE _current_sum BIGINT;
              DECLARE _invoice_sum BIGINT;
              DECLARE _payment_sum BIGINT;
              
              SET _current_sum = NEW.`amount`; 
              SET _invoice_sum = (SELECT `total_amount_with_nds` FROM `invoice` WHERE `id` = NEW.`invoice_id`);                          
              SET _payment_sum = (
                    SELECT SUM(t.`amount`) FROM (
                        SELECT `amount` FROM `cash_bank_flow_to_invoice` WHERE invoice_id = NEW.`invoice_id`
                        UNION ALL
                        SELECT `amount` FROM `cash_order_flow_to_invoice` WHERE  invoice_id = NEW.`invoice_id`
                        UNION ALL
                        SELECT `amount` FROM `cash_emoney_flow_to_invoice` WHERE invoice_id = NEW.`invoice_id`
                    ) t
                );
        
              IF (_payment_sum + _current_sum > _invoice_sum) THEN
              
                SIGNAL SQLSTATE "45000" 
                SET MESSAGE_TEXT = "You can not insert record. Invoice will be overpaid.";
              
              END IF;
        ';

        $this->execute("
            CREATE TRIGGER `cash_bank_flow_to_invoice__prevent_overpayment`
            BEFORE INSERT ON `cash_bank_flow_to_invoice`
            FOR EACH ROW
            BEGIN
            {$triggerBody}
            END");

        $this->execute("
            CREATE TRIGGER `cash_order_flow_to_invoice__prevent_overpayment`
            BEFORE INSERT ON `cash_order_flow_to_invoice`
            FOR EACH ROW
            BEGIN
            {$triggerBody}
            END");

        $this->execute("
            CREATE TRIGGER `cash_emoney_flow_to_invoice__prevent_overpayment`
            BEFORE INSERT ON `cash_emoney_flow_to_invoice`
            FOR EACH ROW
            BEGIN
            {$triggerBody}
            END");
    }

    public function safeDown()
    {
        $this->execute('DROP TRIGGER IF EXISTS `cash_bank_flow_to_invoice__prevent_overpayment`');
        $this->execute('DROP TRIGGER IF EXISTS `cash_order_flow_to_invoice__prevent_overpayment`');
        $this->execute('DROP TRIGGER IF EXISTS `cash_emoney_flow_to_invoice__prevent_overpayment`');
    }
}
