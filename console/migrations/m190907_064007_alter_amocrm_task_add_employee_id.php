<?php

use console\components\db\Migration;

class m190907_064007_alter_amocrm_task_add_employee_id extends Migration
{

    public function safeUp()
    {
        $this->truncateTable('{{amocrm_task}}');
        $this->addColumn('{{amocrm_task}}', 'employee_id', $this->integer()->notNull()->comment('ID покупателя на сайте'));
        $this->addForeignKey(
            'amocrm_task_employee_id', '{{amocrm_task}}', 'employee_id',
            '{{employee}}', 'id', 'CASCADE', 'CASCADE'
        );

    }

    public function down()
    {
        $this->dropColumn('{{amocrm_task}}', 'employee_id');
    }

}
