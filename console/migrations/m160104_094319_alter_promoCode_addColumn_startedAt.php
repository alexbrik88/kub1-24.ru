<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160104_094319_alter_promoCode_addColumn_startedAt extends Migration
{
    public $tPromoCode = 'service_payment_promo_code';

    public function safeUp()
    {
        $this->addColumn($this->tPromoCode, 'started_at', $this->integer() . ' AFTER `code`');
    }

    public function safeDown()
    {
        $this->dropColumn($this->tPromoCode, 'started_at');
    }
}


