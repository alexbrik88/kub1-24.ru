<?php

use console\components\db\Migration;
use yii\db\Schema;
use yii\helpers\ArrayHelper;

class m220120_104736_update_menu_item extends Migration
{
    public function safeUp()
    {
        $demo_company_id = ArrayHelper::getValue(\Yii::$app->params, ['service', 'demo_company_id']);
        $demo_employee_id = ArrayHelper::getValue(\Yii::$app->params, ['service', 'demo_employee_id']);
        if ($demo_company_id && $demo_employee_id) {
            $this->update('{{%menu_item}}', [
                'invoice_item' => 1,
                'b2b_item' => 0,
                'analytics_item' => 0,
                'logistics_item' => 0,
                'accountant_item' => 0,
                'project_item' => 1,
                'product_item' => 0,
                'rent_item' => 1,
                'crm_item' => 0,
                'business_analytics_item' => 0,
            ], [
                'company_id' => $demo_company_id,
                'employee_id' => $demo_employee_id,
            ]);
        }

    }

    public function safeDown()
    {
        //
    }
}
