<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210621_185601_alter_invoice_drop_index extends Migration
{
    public $table = 'invoice';

    public function up()
    {
        try { $this->dropIndex('has_services', $this->table); } catch (Exception $e) { echo $e->getMessage()."\n"; }
        try { $this->dropIndex('has_goods', $this->table); } catch (Exception $e) { echo $e->getMessage()."\n"; }
        try { $this->dropIndex('has_act', $this->table); } catch (Exception $e) { echo $e->getMessage()."\n"; }
        try { $this->dropIndex('has_packing_list', $this->table); } catch (Exception $e) { echo $e->getMessage()."\n"; }
        try { $this->dropIndex('has_invoice_facture', $this->table); } catch (Exception $e) { echo $e->getMessage()."\n"; }
        try { $this->dropIndex('has_upd', $this->table); } catch (Exception $e) { echo $e->getMessage()."\n"; }
        try { $this->dropIndex('has_file', $this->table); } catch (Exception $e) { echo $e->getMessage()."\n"; }
        try { $this->dropIndex('has_proxy', $this->table); } catch (Exception $e) { echo $e->getMessage()."\n"; }
        try { $this->dropIndex('has_waybill', $this->table); } catch (Exception $e) { echo $e->getMessage()."\n"; }
        try { $this->dropIndex('created_at', $this->table); } catch (Exception $e) { echo $e->getMessage()."\n"; }
        try { $this->dropIndex('payment_limit_date', $this->table); } catch (Exception $e) { echo $e->getMessage()."\n"; }
        try { $this->dropIndex('invoice_status_updated_at', $this->table); } catch (Exception $e) { echo $e->getMessage()."\n"; }

    }

    public function down()
    {
        $this->createIndex('has_services', $this->table, 'has_services');
        $this->createIndex('has_goods', $this->table, 'has_goods');
        $this->createIndex('has_act', $this->table, 'has_act');
        $this->createIndex('has_packing_list', $this->table, 'has_packing_list');
        $this->createIndex('has_invoice_facture', $this->table, 'has_invoice_facture');
        $this->createIndex('has_upd', $this->table, 'has_upd');
        $this->createIndex('has_file', $this->table, 'has_file');
        $this->createIndex('has_proxy', $this->table, 'has_proxy');
        $this->createIndex('has_waybill', $this->table, 'has_waybill');
        $this->createIndex('created_at', $this->table, 'created_at');
        $this->createIndex('payment_limit_date', $this->table, 'payment_limit_date');
        $this->createIndex('invoice_status_updated_at', $this->table, 'invoice_status_updated_at');
    }
}
