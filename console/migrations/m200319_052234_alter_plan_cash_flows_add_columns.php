<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200319_052234_alter_plan_cash_flows_add_columns extends Migration
{
    public $table = 'plan_cash_flows';

    public function safeUp()
    {
        $this->addColumn($this->table, 'emoney_id', $this->integer()->after('updated_at'));
        $this->addColumn($this->table, 'cashbox_id', $this->integer()->after('updated_at'));
        $this->addColumn($this->table, 'checking_accountant_id', $this->integer()->after('updated_at'));

        $this->addForeignKey('FK_plan_cash_flows_to_checking_accountant', $this->table, 'checking_accountant_id', 'checking_accountant', 'id', 'CASCADE');
        $this->addForeignKey('FK_plan_cash_flows_to_cashbox', $this->table, 'cashbox_id', 'cashbox', 'id', 'CASCADE');
        $this->addForeignKey('FK_plan_cash_flows_to_emoney', $this->table, 'emoney_id', 'emoney', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_plan_cash_flows_to_checking_accountant', $this->table);
        $this->dropForeignKey('FK_plan_cash_flows_to_cashbox', $this->table);
        $this->dropForeignKey('FK_plan_cash_flows_to_emoney', $this->table);

        $this->dropColumn($this->table, 'checking_accountant_id');
        $this->dropColumn($this->table, 'cashbox_id');
        $this->dropColumn($this->table, 'emoney_id');
    }
}
