<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170823_121404_alter_agreementType_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%agreement_type}}', 'name_dative', $this->string() . ' AFTER [[name]]');

        $this->execute("
            UPDATE {{%agreement_type}}
            SET 
                [[name_dative]] = IF([[id]] = 1, 'договору', IF([[id]] = 2, 'заказу', IF([[id]] = 3, 'спецификации', '')))
        ");
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%agreement_type}}', 'name_dative');
    }
}
