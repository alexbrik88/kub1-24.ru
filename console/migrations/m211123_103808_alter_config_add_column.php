<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211123_103808_alter_config_add_column extends Migration
{
    public $table = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->table, 'report_detailing_industry_income', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 1');
        $this->addColumn($this->table, 'report_detailing_industry_expense', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 1');
        $this->addColumn($this->table, 'report_detailing_industry_revenue', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 1');
        $this->addColumn($this->table, 'report_detailing_industry_margin', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table, 'report_detailing_industry_net', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table, 'report_detailing_industry_profit', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 1');
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'report_detailing_industry_income');
        $this->dropColumn($this->table, 'report_detailing_industry_expense');
        $this->dropColumn($this->table, 'report_detailing_industry_revenue');
        $this->dropColumn($this->table, 'report_detailing_industry_margin');
        $this->dropColumn($this->table, 'report_detailing_industry_net');
        $this->dropColumn($this->table, 'report_detailing_industry_profit');
    }
}
