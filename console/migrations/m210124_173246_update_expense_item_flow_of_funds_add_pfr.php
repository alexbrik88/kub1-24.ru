<?php

use common\models\Company;
use common\models\ExpenseItemFlowOfFunds;
use console\components\db\Migration;

class m210124_173246_update_expense_item_flow_of_funds_add_pfr extends Migration
{
    const EXPENSE_ITEM_PFR = 53;

    public function safeUp()
    {
        $cnt = 0;
        foreach (Company::find()->select(['id'])->column() as $companyId) {
            if (!ExpenseItemFlowOfFunds::find()->andWhere(['and',
                ['company_id' => $companyId],
                ['expense_item_id' => self::EXPENSE_ITEM_PFR],
            ])->exists()
            ) {
                $this->insert('{{%expense_item_flow_of_funds}}', [
                    'company_id' => $companyId,
                    'expense_item_id' => self::EXPENSE_ITEM_PFR,
                ]);
                $cnt++;
            }
        }

        echo "inserted {$cnt} rows\n\n";
    }

    public function safeDown()
    {
        // no down
    }
}
