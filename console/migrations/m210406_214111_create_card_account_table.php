<?php

use yii\db\Migration;

class m210406_214111_create_card_account_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%card_account}}';

    /**
     * @inheritDoc
     */
    public function up(): void
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->bigPrimaryKey()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'account_type' => $this->tinyInteger()->notNull()->unsigned(),
            'identifier' => $this->string()->notNull(),
            'is_active' => $this->boolean()->notNull()->unsigned(),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->createIndex('ck_company_id', self::TABLE_NAME, ['company_id']);
        $this->createIndex('uk_card_account', self::TABLE_NAME, ['company_id', 'account_type', 'identifier'], true);
        $this->addForeignKey(
            'fk_card_account_company_id__company_id',
            self::TABLE_NAME,
            ['company_id'],
            '{{%company}}',
            ['id'],
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritDoc
     */
    public function down(): void
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
