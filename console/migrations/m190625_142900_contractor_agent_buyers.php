<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190625_142900_contractor_agent_buyers extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('contractor_agent_buyer', [
            'agent_id' => $this->integer()->notNull(),
            'buyer_id' => $this->integer()->notNull(),
            'start_date' => $this->date(),
            'created_at' => $this->integer(),
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', '{{%contractor_agent_buyer}}', ['agent_id', 'buyer_id']);

        $this->addForeignKey(
            'FK_contractor_agent',
            'contractor_agent_buyer',
            'agent_id',
            'contractor',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'FK_contractor_agent_buyer',
            'contractor_agent_buyer',
            'buyer_id',
            'contractor',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('contractor_agent_buyer');
    }
}
