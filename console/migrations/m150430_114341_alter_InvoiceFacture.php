<?php

use yii\db\Schema;
use yii\db\Migration;

class m150430_114341_alter_InvoiceFacture extends Migration
{
    public function safeUp()
    {
        $this->dropTable('invoice_facture');
        $this->createTable('invoice_facture', [
            'id' => Schema::TYPE_PK,
            'type' => Schema::TYPE_INTEGER . '(1) NOT NULL COMMENT "2 - out, 1 -in"',
            'invoice_id' => Schema::TYPE_INTEGER . ' NOT NULL',

            'created_at' => Schema::TYPE_DATETIME . ' NOT NULL',
            'document_author_id' => Schema::TYPE_INTEGER . ' NOT NULL',

            'status_out_id' => Schema::TYPE_INTEGER . ' NULL',
            'status_out_updated_at' => Schema::TYPE_DATETIME . ' NULL',
            'status_out_author_id' => Schema::TYPE_INTEGER . ' NULL',

            'document_date' => Schema::TYPE_DATE . ' NOT NULL',
            'document_number' => Schema::TYPE_INTEGER . ' NULL',
            'document_additional_number' => Schema::TYPE_STRING . '(45) NULL',

            'has_to_payment_document' => Schema::TYPE_BOOLEAN . ' NOT NULL',
            'to_payment_document' => Schema::TYPE_STRING . '(100) NULL COMMENT "К платёжно-расчётному документу. Наличие = 0, то значение `---`"',

            'document_filename' => Schema::TYPE_STRING . '(255) NULL',
        ]);

        $this->addForeignKey('fk_invoice_facture_to_invoice', 'invoice_facture', 'invoice_id', 'invoice', 'id');
        $this->addForeignKey('fk_invoice_facture_to_invoice_facture_status', 'invoice_facture', 'status_out_id', 'invoice_facture_status', 'id');
        $this->addForeignKey('fk_invoice_facture_to_document_author', 'invoice_facture', 'document_author_id', 'employee', 'id');
        $this->addForeignKey('fk_invoice_facture_to_status_out_author', 'invoice_facture', 'status_out_author_id', 'employee', 'id');
    }

    public function safeDown()
    {
        $this->execute("

CREATE TABLE IF NOT EXISTS `invoice_facture` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL COMMENT '1 - входящая, 2 - исходящая',
  `invoice_id` int(11) NOT NULL COMMENT 'Номер счёта',
  `invoice_facture_status_id` int(11) DEFAULT NULL COMMENT 'Статус счёт-фактуры. NULL для входящей',
  `invoice_facture_status_change_date` datetime DEFAULT NULL COMMENT 'Дата изменения статуса счёт-фактуры',
  `invoice_facture_status_author_id` int(11) DEFAULT NULL COMMENT 'Автор изменения статуса счёт-фактуры',
  `invoice_facture_date` datetime NOT NULL COMMENT 'Дата создания счёт-фактуры',
  `invoice_number` varchar(100) DEFAULT NULL COMMENT 'Основной номер счёт-фактуры',
  `additional_invoice_number` varchar(100) DEFAULT NULL COMMENT 'Дополнительный номер счёт-фактуры',
  `has_to_payment_document` tinyint(1) DEFAULT NULL COMMENT 'Наличение \"К платёжно-расчётному документу\" ',
  `to_payment_document` varchar(100) DEFAULT NULL COMMENT 'К платёжно-расчётному документу. Наличие = 0, то значение \"---\" ',
  `filename` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `invoice_facture_to_invoice_facture_status` (`invoice_facture_status_id`),
  KEY `fk_invoice_facture_to_invoice` (`invoice_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;

ALTER TABLE `invoice_facture`
  ADD CONSTRAINT `fk_invoice_facture_to_invoice` FOREIGN KEY (`invoice_id`) REFERENCES `invoice` (`id`),
  ADD CONSTRAINT `invoice_facture_to_invoice_facture_status` FOREIGN KEY (`invoice_facture_status_id`) REFERENCES `invoice_facture_status` (`id`);

        ");
    }
}
