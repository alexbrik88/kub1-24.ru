<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210107_085107_insert_cash_contractor_type extends Migration
{
    public function safeUp()
    {
        $this->batchInsert('{{%cash_contractor_type}}', [
            'id',
            'name',
            'text',
        ], [
            [8, 'order_currency', 'Касса, иностранная валюта'],
            [9, 'emoney_currency', 'Emoney, иностранная валюта'],
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%cash_contractor_type}}', [
            'id' => [8, 9],
        ]);
    }
}
