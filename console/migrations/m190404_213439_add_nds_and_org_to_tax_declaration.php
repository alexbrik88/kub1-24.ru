<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190404_213439_add_nds_and_org_to_tax_declaration extends Migration
{
    public $tableName = 'tax_declaration';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'nds', $this->boolean()->defaultValue(false));
        $this->addColumn($this->tableName, 'org', $this->boolean()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'nds');
        $this->dropColumn($this->tableName, 'org');
    }
}
