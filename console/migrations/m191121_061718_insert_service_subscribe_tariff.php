<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191121_061718_insert_service_subscribe_tariff extends Migration
{
    public function safeUp()
    {
        $this->batchInsert('{{%service_subscribe_tariff}}', [
            'id',
            'tariff_group_id',
            'duration_month',
            'duration_day',
            'price',
            'is_active',
            'proposition',
            'invoice_limit',
        ], [
            [18, 1, 1, 0, 600, 1, 'Получи скидку 10% при оплате за 4 месяца', 100],
            [19, 1, 4, 0, 2160, 1, 'Получи скидку 20% при оплате за год', 100],
            [20, 1, 12, 0, 5760, 1, 'Лучшая цена!', 100],
            [21, 1, 1, 0, 1000, 1, 'Получи скидку 10% при оплате за 4 месяца', 250],
            [22, 1, 4, 0, 3600, 1, 'Получи скидку 20% при оплате за год', 250],
            [23, 1, 12, 0, 9600, 1, 'Лучшая цена!', 250],
            [24, 1, 1, 0, 1350, 1, 'Получи скидку 10% при оплате за 4 месяца', null],
            [25, 1, 4, 0, 4860, 1, 'Получи скидку 20% при оплате за год', null],
            [26, 1, 12, 0, 12960, 1, 'Лучшая цена!', null],
        ]);

        $this->update('{{%service_subscribe_tariff}}', ['is_active' => 0], ['id' => [1,2,3]]);
    }

    public function safeDown()
    {
        $this->delete('{{%service_subscribe_tariff}}', [
            'id' => [18, 19, 20, 21, 22, 23, 24, 25, 26],
        ]);

        $this->update('{{%service_subscribe_tariff}}', ['is_active' => 1], ['id' => [1,2,3]]);
    }
}
