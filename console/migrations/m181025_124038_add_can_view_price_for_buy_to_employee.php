<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181025_124038_add_can_view_price_for_buy_to_employee extends Migration
{
    public $tableName = 'employee_company';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'can_view_price_for_buy', $this->boolean()->defaultValue(false));
        $this->update($this->tableName, ['can_view_price_for_buy' => true]);
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'can_view_price_for_buy');
    }
}


