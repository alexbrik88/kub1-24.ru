<?php

use yii\db\Schema;
use yii\db\Migration;

class m150618_140802_drop_OutActStatus extends Migration
{
    public function safeUp()
    {
        $this->dropTable('out_act_status');
    }
    
    public function safeDown()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%out_act_status}}', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' DEFAULT NULL'
        ], $tableOptions);

        $this->batchInsert('{{%out_act_status}}', ['name'], [
            ['Создан'],
            ['Распечатан'],
            ['Передан'],
            ['Получен-подписан']
        ]);
    }
}
