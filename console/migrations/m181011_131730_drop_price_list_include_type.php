<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181011_131730_drop_price_list_include_type extends Migration
{
    public $tableName = 'price_list_include_type';

    public function safeUp()
    {
        $this->dropForeignKey('price_list_include_type_id', 'price_list');
        $this->dropTable($this->tableName);
    }
    
    public function safeDown()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);

        $this->batchInsert($this->tableName, ['id', 'name'], [
            [1, 'Весь товар'],
            [2, 'Товар в наличие'],
            [3, 'Настроить'],
        ]);
        $this->addForeignKey('price_list_include_type_id', 'price_list', 'include_type_id', $this->tableName, 'id', 'RESTRICT', 'CASCADE');
    }
}


