<?php

use console\components\db\Migration;
use yii\db\Schema;
use common\models\employee\Employee;

class m161017_074750_add_my_companies_to_employee extends Migration
{
    public $tEmployee = 'employee';

    public function safeUp()
    {
        $this->addColumn($this->tEmployee, 'my_companies', $this->string()->notNull());
        $this->update($this->tEmployee, ['my_companies' => new \yii\db\Expression('CONCAT(\'a:1:{i:0;i:\',company_id,\';}\')')]);
    }

    public function safeDown()
    {
        $this->dropColumn($this->tEmployee, 'my_companies');
    }
}


