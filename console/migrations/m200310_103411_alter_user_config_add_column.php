<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200310_103411_alter_user_config_add_column extends Migration
{
    public $table = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->table, 'table_view_finance_plan_fact', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table,'report_plan_fact_help',$this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->table,'report_plan_fact_chart',$this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'table_view_finance_plan_fact');
        $this->dropColumn($this->table,'report_plan_fact_help');
        $this->dropColumn($this->table,'report_plan_fact_chart');
    }
}
