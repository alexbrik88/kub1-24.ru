<?php

use yii\db\Schema;
use yii\db\Migration;

class m150617_122055_update_Product_update_ProductUnit extends Migration
{
    public function safeUp()
    {
        $this->update('product', [
            'product_unit_id' => null,
        ], [
            'product_unit_id' => 1,
            'production_type' => 0, // service
        ]);

    }
    
    public function safeDown()
    {
        echo 'No down migration needed.', PHP_EOL;
    }
}
