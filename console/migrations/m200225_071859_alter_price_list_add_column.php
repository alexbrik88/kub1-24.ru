<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200225_071859_alter_price_list_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%price_list}}', 'can_checkout', $this->boolean()->notNull()->defaultValue(false));

        $this->execute('UPDATE `price_list` SET `can_checkout` = 1');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%price_list}}', 'can_checkout');
    }
}
