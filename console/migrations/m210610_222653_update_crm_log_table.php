<?php

use yii\db\Migration;

class m210610_222653_update_crm_log_table extends Migration
{
    /**
     * @inheritDoc
     */
    public function safeUp(): void
    {
        $this->delete('{{%crm_log}}', ['attribute_name' => 'client_event_id']);
    }
}
