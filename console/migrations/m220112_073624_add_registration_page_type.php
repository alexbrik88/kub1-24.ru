<?php

use console\components\db\Migration;
use yii\db\Schema;

class m220112_073624_add_registration_page_type extends Migration
{
    public $tableName = '{{%registration_page_type}}';

    public function safeUp()
    {
        $this->insert($this->tableName, ['id' => 49, 'name' => 'Лэндинг SberUnity']);
        $this->insert($this->tableName, ['id' => 50, 'name' => 'Аналитика SberUnity']);
    }

    public function safeDown()
    {
        $this->delete($this->tableName, ['id' => 49]);
        $this->delete($this->tableName, ['id' => 50]);
    }
}
