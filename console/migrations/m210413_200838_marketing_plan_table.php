<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210413_200838_marketing_plan_table extends Migration
{
    public function safeUp()
    {
        $this->createTable('marketing_plan', [
            'id' => $this->primaryKey(11)->unsigned(),
            'company_id' => $this->integer()->notNull(),
            'channel' => $this->tinyInteger()->unsigned()->notNull(),
            'name' => $this->string()->notNull(),
            'start_date' => $this->string(8)->notNull(),
            'end_date' => $this->string(8)->notNull(),
            'comment' => $this->text(),
            'template' => $this->string(),
            'responsible_employee_id' => $this->integer()->notNull(),
            'financial_result' => $this->bigInteger(20),
            'created_at' => $this->integer(11)->unsigned()->notNull(),
        ]);

        $this->addForeignKey('FK_marketing_plan_to_company', 'marketing_plan', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_marketing_plan_to_responsible_employee', 'marketing_plan', 'responsible_employee_id', 'employee', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('marketing_plan_item', [
            'id' => $this->primaryKey(11)->unsigned(),
            'company_id' => $this->integer()->notNull(),
            'marketing_plan_id' => $this->integer(11)->unsigned()->notNull(),
            'month' => $this->string(2)->notNull(),
            'year' => $this->integer(4)->notNull(),
            'budget_amount' => $this->bigInteger(20)->unsigned(),
            'marketing_service_amount' => $this->bigInteger(20)->unsigned(),
            'additional_expenses_amount' => $this->bigInteger(20)->unsigned(),
            'total_expenses_amount' => $this->bigInteger(20)->unsigned(),
            'clicks_count' => $this->integer(11)->unsigned(),
            'budget_click_amount' => $this->bigInteger(20)->unsigned(),
            'final_costs_click_amount' => $this->bigInteger(20)->unsigned(),
            'registration_conversion' => $this->float(2)->unsigned(),
            'registration_count' => $this->integer(11)->unsigned(),
            'budget_registration_amount' => $this->bigInteger(20)->unsigned(),
            'final_costs_registration_amount' => $this->bigInteger(20)->unsigned(),
            'active_user_conversion' => $this->float(2)->unsigned(),
            'active_users_count' => $this->integer(11)->unsigned(),
            'budget_active_user_amount' => $this->bigInteger(20)->unsigned(),
            'final_costs_user_amount' => $this->bigInteger(20)->unsigned(),
            'purchase_conversion' => $this->float(2)->unsigned(),
            'purchases_count' => $this->integer(11)->unsigned(),
            'budget_sale_amount' => $this->bigInteger(20)->unsigned(),
            'final_costs_sale_amount' => $this->bigInteger(20)->unsigned(),
            'average_check' => $this->bigInteger(20)->unsigned(),
            'proceeds_from_new_amount' => $this->bigInteger(20)->unsigned(),
            'churn_rate' => $this->float(2)->unsigned(),
            'repeated_purchases_amount' => $this->bigInteger(20)->unsigned(),
            'total_proceeds_amount' => $this->bigInteger(20)->unsigned(),
            'financial_result' => $this->bigInteger(20),
            'cumulative_financial_amount' => $this->bigInteger(20),
        ]);

        $this->addForeignKey('FK_marketing_plan_item_to_company', 'marketing_plan_item', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_marketing_plan_item_to_marketing_plan', 'marketing_plan_item', 'marketing_plan_id', 'marketing_plan', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable('marketing_plan_item');
        $this->dropTable('marketing_plan');
    }
}
