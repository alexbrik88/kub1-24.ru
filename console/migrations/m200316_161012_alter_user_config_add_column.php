<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200316_161012_alter_user_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%user_config}}', 'table_view_autoinvoice', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn('{{%user_config}}', 'table_view_invoice_facture', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn('{{%user_config}}', 'turnover_group_id', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn('{{%user_config}}', 'turnover_unit_id', $this->boolean()->notNull()->defaultValue(true));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%user_config}}', 'table_view_autoinvoice');
        $this->dropColumn('{{%user_config}}', 'table_view_invoice_facture');
        $this->dropColumn('{{%user_config}}', 'turnover_group_id');
        $this->dropColumn('{{%user_config}}', 'turnover_unit_id');
    }
}
