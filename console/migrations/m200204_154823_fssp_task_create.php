<?php

use console\components\db\Migration;

class m200204_154823_fssp_task_create extends Migration
{

    const TABLE = '{{%fssp_task}}';

    public function safeUp()
    {
        $this->createTable(self::TABLE, [
            'contractor_id' => $this->primaryKey(11)->notNull()->comment('ID контрагента'),
            'status' => "ENUM('request','error','success') NOT NULL DEFAULT 'request' COMMENT 'Состояние запроса: request - обработка данных, error - данные получить не удалось, success - данные успешно получены'",
            'date' => $this->dateTime()->notNull()->defaultExpression('NOW()')->comment('Дата и время запроса/актуальности данных'),
            'task' => $this->string(36)->comment('Идентификатор задачи на удалённом сервере на загрузку данных (если запрос находитс в обработке)'),
        ], "COMMENT='Сведения о задолженностях контрагентов, полученные из ФССП (задачи загрузки данных'");
        $this->alterColumn(self::TABLE, 'contractor_id', $this->integer(11)->notNull());
        $this->addForeignKey('fssp_task_contractor_id', self::TABLE, 'contractor_id', '{{%contractor}}', 'id',
            'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable(self::TABLE);
    }

}