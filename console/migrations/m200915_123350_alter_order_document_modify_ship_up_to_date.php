<?php

use console\components\db\Migration;
use yii\db\Expression;
use yii\db\Schema;

class m200915_123350_alter_order_document_modify_ship_up_to_date extends Migration
{
    public $tableName = 'order_document';

    public function safeUp()
    {
        $this->alterColumn($this->tableName, 'ship_up_to_date', $this->date());
    }

    public function safeDown()
    {
        $this->update($this->tableName, ['ship_up_to_date' => new Expression('DATE(document_date)')], ['IS', 'ship_up_to_date', new Expression('NULL')]);
        $this->alterColumn($this->tableName, 'ship_up_to_date', $this->date()->notNull());
    }
}
