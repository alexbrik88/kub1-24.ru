<?php

use yii\db\Migration;
use yii\db\Query;

/**
 * Handles the creation of table `invoice_facture_payment_document`.
 */
class m170531_151221_create_invoice_facture_payment_document_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%invoice_facture_payment_document}}', [
            'invoice_facture_id' => $this->integer()->notNull(),
            'payment_document_number' => $this->string()->notNull(),
            'payment_document_date' => $this->date()->notNull(),
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', '{{%invoice_facture_payment_document}}', [
            'invoice_facture_id',
            'payment_document_number',
            'payment_document_date',
        ]);
        $this->addForeignKey(
            'FK_invoiceFacturePaymentDocument_invoiceFacture',
            '{{%invoice_facture_payment_document}}',
            'invoice_facture_id',
            '{{%invoice_facture}}',
            'id',
            'CASCADE'
        );

        $query = new Query;
        $query->select(['id', 'to_payment_document', 'payment_document_date'])
            ->from('invoice_facture')
            ->where([
                'and',
                ['has_to_payment_document' => 1],
                ['not', ['to_payment_document' => null]],
                ['not', ['to_payment_document' => '']],
                ['not', ['to_payment_document' => '---']],
            ]);

        $paymentDocArray = $query->all();

        if ($paymentDocArray) {
            Yii::$app->db->createCommand()
                ->batchInsert('{{%invoice_facture_payment_document}}', [
                    'invoice_facture_id',
                    'payment_document_number',
                    'payment_document_date',
                ], $paymentDocArray)
                ->execute();
        }

        $this->dropColumn('{{%invoice_facture}}', 'has_to_payment_document');
        $this->dropColumn('{{%invoice_facture}}', 'to_payment_document');
        $this->dropColumn('{{%invoice_facture}}', 'payment_document_date');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->addColumn('{{%invoice_facture}}', 'has_to_payment_document', $this->smallInteger(1)->notNull()->defaultValue(false) . ' AFTER [[document_additional_number]]');
        $this->addColumn('{{%invoice_facture}}', 'to_payment_document', $this->string() . ' AFTER [[has_to_payment_document]]');
        $this->addColumn('{{%invoice_facture}}', 'payment_document_date', $this->string() . ' AFTER [[to_payment_document]]');

        $query = new Query;
        $query->select(['*'])
            ->from('invoice_facture_payment_document')
            ->groupBy('invoice_facture_id');

        $paymentDocArray = $query->all();
        foreach ($paymentDocArray as $paymentDoc) {
            Yii::$app->db->createCommand()
                ->update('invoice_facture', [
                    'has_to_payment_document' => 1,
                    'to_payment_document' => $paymentDoc['payment_document_number'],
                    'payment_document_date' => $paymentDoc['payment_document_date'],
                ], ['id' => $paymentDoc['invoice_facture_id']])
                ->execute();
        }

        $this->dropTable('{{%invoice_facture_payment_document}}');
    }
}
