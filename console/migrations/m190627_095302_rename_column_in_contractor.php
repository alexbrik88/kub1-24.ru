<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190627_095302_rename_column_in_contractor extends Migration
{
    public function safeUp()
    {
        $this->update('contractor', [
            'agent_payment_sum' => 0,
        ], [
            'agent_payment_sum' => null,
        ]);

        $this->alterColumn('contractor', 'agent_payment_sum', $this->decimal(6, 4)->notNull()->defaultValue(0));
        $this->renameColumn('contractor', 'agent_payment_sum', 'agent_payment_percent');
    }

    public function safeDown()
    {
        $this->alterColumn('contractor', 'agent_payment_percent', $this->decimal(20, 4));
        $this->renameColumn('contractor', 'agent_payment_percent', 'agent_payment_sum');
    }
}
