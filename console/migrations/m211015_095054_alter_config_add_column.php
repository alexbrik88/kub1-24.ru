<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211015_095054_alter_config_add_column extends Migration
{
    public $table = 'user_config';
    public $columns = [
        'contractor_sale_point',
        'contractor_industry',
    ];

    public function safeUp()
    {
        foreach ($this->columns as $column)
            $this->addColumn($this->table, $column, $this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        foreach ($this->columns as $column)
            $this->dropColumn($this->table, $column);
    }
}
