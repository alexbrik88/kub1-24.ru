<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201223_212906_alter_foreign_currency_invoice_drop_column extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('{{%foreign_currency_invoice}}', 'foreign_currency_account_id');
    }

    public function safeDown()
    {
        $this->addColumn('{{%foreign_currency_invoice}}', 'foreign_currency_account_id', $this->integer()->after('checking_accountant_id'));
    }
}
