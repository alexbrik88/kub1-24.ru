<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200323_105535_alter_plan_cash_contractor_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('plan_cash_contractor', 'amount', $this->decimal(38, 2)->notNull()->defaultValue(0)->after('payment_delay'));
        $this->addColumn('plan_cash_contractor', 'end_plan_date', $this->date()->defaultValue(null)->after('payment_delay'));
        $this->addColumn('plan_cash_contractor', 'plan_date', $this->date()->defaultValue(null)->after('payment_delay'));
    }

    public function safeDown()
    {
        $this->dropColumn('plan_cash_contractor', 'amount');
        $this->dropColumn('plan_cash_contractor', 'end_plan_date');
        $this->dropColumn('plan_cash_contractor', 'plan_date');
    }
}
