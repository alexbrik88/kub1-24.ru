<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190704_162847_change_text_in_agent_report_status extends Migration
{
    public $tableName = 'agent_report_status';
    public function safeUp()
    {
        $this->execute("UPDATE `{$this->tableName}` SET `name` = 'Оплачен' WHERE `id` = 5;");
    }

    public function safeDown()
    {
        $this->execute("UPDATE `{$this->tableName}` SET `name` = 'Оплачен полностью' WHERE `id` = 5;");
    }
}
