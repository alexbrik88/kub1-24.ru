<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200305_082308_insert_service_subscribe_tariff extends Migration
{
    public function safeUp()
    {
        $this->batchInsert('{{%service_subscribe_tariff}}', [
            'id',
            'tariff_group_id',
            'duration_month',
            'duration_day',
            'price',
            'is_active',
            'proposition',
            'invoice_limit',
        ], [
            [39, 10, 0, 5, 0, 1, '', 1],
            [40, 10, 1, 0, 600, 1, 'Получи скидку 10% при оплате за 4 месяца', 3],
            [41, 10, 4, 0, 2160, 1, 'Получи скидку 20% при оплате за год', 3],
            [42, 10, 12, 0, 5760, 1, 'Лучшая цена!', 3],
            [43, 10, 1, 0, 2000, 1, 'Получи скидку 10% при оплате за 4 месяца', 5],
            [44, 10, 4, 0, 7200, 1, 'Получи скидку 20% при оплате за год', 5],
            [45, 10, 12, 0, 19200, 1, 'Лучшая цена!', 5],
            [46, 10, 1, 0, 3500, 1, 'Получи скидку 10% при оплате за 4 месяца', null],
            [47, 10, 4, 0, 12600, 1, 'Получи скидку 20% при оплате за год', null],
            [48, 10, 12, 0, 33600, 1, 'Лучшая цена!', null],
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%service_subscribe_tariff}}', [
            'id' => [
                39,
                40,
                41,
                42,
                43,
                44,
                45,
                46,
                47,
                48,
            ]
        ]);
    }
}
