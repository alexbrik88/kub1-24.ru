<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201216_135213_create_widget_config_account_balance extends Migration
{
    public $table = 'widget_config_account_balance';

    public function safeUp()
    {
        $this->createTable($this->table, [
            'company_id' => $this->integer(),
            'employee_id' => $this->integer(),
            'options' => $this->text()
        ]);

        $this->addPrimaryKey('PK_widget_config_account_balance', $this->table, ['company_id', 'employee_id']);
        $this->addForeignKey('FK_widget_config_account_balance_company', $this->table, 'company_id', 'company', 'id', 'CASCADE');
        $this->addForeignKey('FK_widget_config_account_balance_employee', $this->table, 'employee_id', 'employee', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_widget_config_account_balance_company', $this->table);
        $this->dropForeignKey('FK_widget_config_account_balance_employee', $this->table);
        $this->dropTable($this->table);
    }
}
