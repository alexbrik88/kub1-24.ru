<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200116_181205_rename_yandex_direct_report_table extends Migration
{
    public function safeUp()
    {
        $this->renameTable('yandex_direct_report', 'yandex_direct_campaign_report');
        $this->addColumn('{{%yandex_direct_campaign_report}}', 'status', $this->string()->defaultValue(null)->after('campaign_id'));
        $this->addColumn('{{%yandex_direct_campaign_report}}', 'strategy', $this->string()->defaultValue(null)->after('status'));
        $this->addColumn('{{%yandex_direct_campaign_report}}', 'showing_place', $this->string()->defaultValue(null)->after('strategy'));
        $this->addColumn('{{%yandex_direct_campaign_report}}', 'daily_budget_amount', $this->float(2)->unsigned()->defaultValue(null)->after('showing_place'));
    }

    public function safeDown()
    {
        $this->renameTable('yandex_direct_campaign_report', 'yandex_direct_report');
        $this->dropColumn('{{%yandex_direct_report}}', 'status');
        $this->dropColumn('{{%yandex_direct_report}}', 'strategy');
        $this->dropColumn('{{%yandex_direct_report}}', 'showing_place');
        $this->dropColumn('{{%yandex_direct_report}}', 'daily_budget_amount');
    }
}
