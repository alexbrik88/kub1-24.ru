<?php

use yii\db\Schema;
use yii\db\Migration;

class m150423_142041_alter_PackingListStatus_renameColumn_packingListTitle extends Migration
{
    public function up()
    {
        $this->renameColumn('packing_list_status', 'packing_list_title', 'name');
    }

    public function down()
    {
        $this->renameColumn('packing_list_status', 'name', 'packing_list_title');
    }
}
