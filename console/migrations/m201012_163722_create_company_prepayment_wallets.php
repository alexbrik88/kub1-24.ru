<?php

use common\models\Company;
use console\components\db\Migration;
use yii\db\Schema;

class m201012_163722_create_company_prepayment_wallets extends Migration
{
    public $tableName = 'company_prepayment_wallets';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'bank' => $this->boolean()->notNull()->defaultValue(0),
            'order' => $this->boolean()->notNull()->defaultValue(0),
            'emoney' => $this->boolean()->notNull()->defaultValue(0)
        ]);

        $this->addForeignKey($this->tableName . '_company_id', $this->tableName, 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');

        foreach (Company::find()->select(['id'])->column() as $companyId) {
            $this->execute("
                INSERT INTO `company_prepayment_wallets` (`company_id`, `bank`, `order`, `emoney`)
                VALUES ({$companyId}, 1, 1, 0);
            ");
        }
    }

    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_company_id', $this->tableName);

        $this->dropTable($this->tableName);
    }
}
