<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210927_084644_update_cash_bank_flows_child_description extends Migration
{
    public function safeUp()
    {
        $this->execute("
            UPDATE cash_bank_flows c1
            LEFT JOIN cash_bank_flows c2 ON c2.id = c1.parent_id
            SET c1.description = c2.description
            WHERE c1.parent_id IS NOT NULL
        ");
    }

    public function safeDown()
    {
        // no down
    }
}
