<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200330_091618_alter_user_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%user_config}}', 'table_view', $this->boolean()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%user_config}}', 'table_view');
    }
}
