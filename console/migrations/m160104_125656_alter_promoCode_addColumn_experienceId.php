<?php

use console\components\db\Migration;

class m160104_125656_alter_promoCode_addColumn_experienceId extends Migration
{
    public $tPromoCode = 'service_payment_promo_code';
    public $tExperience = 'company_experience';

    public function safeUp()
    {
        $this->addColumn($this->tPromoCode, 'for_company_experience_id', $this->integer()->defaultValue(null) . ' AFTER `group_id`');

        $this->addForeignKey('fk_service_promo_code_to_company_experience', $this->tPromoCode, 'for_company_experience_id', $this->tExperience, 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_service_promo_code_to_company_experience', $this->tPromoCode);

        $this->dropColumn($this->tPromoCode, 'for_company_experience_id');
    }
}


