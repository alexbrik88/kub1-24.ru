<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170330_083447_alter_autoinvoice_dropColumns extends Migration
{
    public function safeUp()
    {
        $this->delete('{{%autoinvoice}}');

        $this->dropForeignKey('autoinvoice_to_invoice_expenditure_item', '{{%autoinvoice}}');

        $this->execute("ALTER TABLE `autoinvoice` CHANGE COLUMN `id` `id` INT(11) NOT NULL FIRST;");

        $this->dropColumn('{{%autoinvoice}}', 'type');
        $this->dropColumn('{{%autoinvoice}}', 'production_type');
        $this->dropColumn('{{%autoinvoice}}', 'invoice_expenditure_item_id');
        $this->dropColumn('{{%autoinvoice}}', 'all_sum');

        $this->alterColumn('{{%autoinvoice}}', 'date_from', $this->date());
        $this->alterColumn('{{%autoinvoice}}', 'date_to', $this->date());
        $this->alterColumn('{{%autoinvoice}}', 'date_stop', $this->date());
        $this->alterColumn('{{%autoinvoice}}', 'last_invoice_date', $this->date());
        $this->alterColumn('{{%autoinvoice}}', 'next_invoice_date', $this->date());

        $this->addForeignKey('FK_autoinvoice_to_invoice', '{{%autoinvoice}}', 'id', '{{%invoice}}', 'id');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('FK_autoinvoice_to_invoice', '{{%autoinvoice}}');

        $this->execute("ALTER TABLE `autoinvoice` CHANGE COLUMN `id` `id` INT(11) NOT NULL AUTO_INCREMENT FIRST;");

        $this->addColumn('{{%autoinvoice}}', 'type', $this->integer());
        $this->addColumn('{{%autoinvoice}}', 'production_type', $this->string(255));
        $this->addColumn('{{%autoinvoice}}', 'invoice_expenditure_item_id', $this->integer());
        $this->addColumn('{{%autoinvoice}}', 'all_sum', $this->bigInteger(20));

        $this->alterColumn('{{%autoinvoice}}', 'date_from', $this->integer());
        $this->alterColumn('{{%autoinvoice}}', 'date_to', $this->integer());
        $this->alterColumn('{{%autoinvoice}}', 'date_stop', $this->integer());
        $this->alterColumn('{{%autoinvoice}}', 'last_invoice_date', $this->integer());
        $this->alterColumn('{{%autoinvoice}}', 'next_invoice_date', $this->integer());

        $this->addForeignKey('autoinvoice_to_invoice_expenditure_item', '{{%autoinvoice}}', 'invoice_expenditure_item_id', '{{%invoice_expenditure_item}}', 'id');
    }
}
