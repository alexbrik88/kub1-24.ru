<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170810_215125_add_form_field_to_company extends Migration
{
    public $tableName = 'company';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'form_source', $this->string()->defaultValue(null));
        $this->addColumn($this->tableName, 'form_keyword', $this->string()->defaultValue(null));
        $this->addColumn($this->tableName, 'form_region', $this->string()->defaultValue(null));
        $this->addColumn($this->tableName, 'form_date', $this->string()->defaultValue(null));
        $this->addColumn($this->tableName, 'form_hour', $this->string()->defaultValue(null));
        $this->addColumn($this->tableName, 'form_entrance_page', $this->string()->defaultValue(null));
        $this->addColumn($this->tableName, 'form_unique_events_count', $this->integer()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'form_source');
        $this->dropColumn($this->tableName, 'form_keyword');
        $this->dropColumn($this->tableName, 'form_region');
        $this->dropColumn($this->tableName, 'form_date');
        $this->dropColumn($this->tableName, 'form_hour');
        $this->dropColumn($this->tableName, 'form_entrance_page');
        $this->dropColumn($this->tableName, 'form_unique_events_count');
    }
}


