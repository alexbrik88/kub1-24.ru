<?php

use console\components\db\Migration;

class m201016_080259_add_table_project_customer extends Migration
{
    public $tableName = 'project_customer';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'project_id' => $this->integer(11),
            'customer_id' => $this->integer(11),
            'agreement_id' => $this->integer(11),
            'amount' => $this->integer()->unsigned(),
            'date' => $this->date()->defaultValue(new \yii\db\Expression('CURRENT_DATE()')),
            'created_at' => $this->integer(11)->defaultValue(new \yii\db\Expression('CURRENT_TIMESTAMP()')),
            'updated_at' => $this->integer(11)->defaultValue(new \yii\db\Expression('CURRENT_TIMESTAMP()')),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }

}