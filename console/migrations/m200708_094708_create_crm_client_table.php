<?php

use yii\db\Migration;

class m200708_094708_create_crm_client_table extends Migration
{
    /** @var string Имя таблицы */
    private const TABLE_NAME = '{{%crm_client}}';

    /**
     * @inheritDoc
     */
    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'client_id' => $this->bigPrimaryKey(),
            'client_activity_id' => $this->bigInteger()->null(),
            'client_check_id' => $this->bigInteger()->null(),
            'client_event_id' => $this->bigInteger()->null(),
            'client_position_id' => $this->bigInteger()->null(),
            'client_reason_id' => $this->bigInteger()->null(),
            'client_status_id' => $this->bigInteger()->null(),
            'campaign_id' => $this->bigInteger()->null(),
            'company_id' => $this->integer()->notNull(),
            'employee_id' => $this->integer()->notNull(),
            'contractor_id' => $this->integer()->notNull(),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->createForeignKey('crm_client_activity', ['client_activity_id' => 'client_activity_id']);
        $this->createForeignKey('crm_client_check', ['client_check_id' => 'client_check_id']);
        $this->createForeignKey('crm_client_event', ['client_event_id' => 'client_event_id']);
        $this->createForeignKey('crm_client_position', ['client_position_id' => 'client_position_id']);
        $this->createForeignKey('crm_client_reason', ['client_reason_id' => 'client_reason_id']);
        $this->createForeignKey('crm_client_status', ['client_status_id' => 'client_status_id']);
        $this->createForeignKey('crm_campaign', ['campaign_id' => 'campaign_id']);
        $this->createForeignKey('employee_company', ['company_id' => 'company_id', 'employee_id' => 'employee_id']);
        $this->createForeignKey('contractor', ['contractor_id' => 'id']);
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }

    /**
     * Создать внешний ключ
     *
     * @param string $table Имя таблицы внешнего ключа
     * @param string[] $columns Названия столбцов текущей таблицы
     * @return void
     */
    private function createForeignKey(string $table, array $columns): void
    {
        $key = implode('_', $columns);
        $this->createIndex('ck_' . $key, self::TABLE_NAME, array_keys($columns));
        $this->addForeignKey(
            "fk_crm_client__{$table}_{$key}",
            self::TABLE_NAME,
            array_keys($columns),
            "{{%{$table}}}",
            array_values($columns),
            'CASCADE',
            'CASCADE'
        );
    }
}
