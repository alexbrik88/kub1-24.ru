<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170531_105812_alter_employee_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%employee}}', 'need_look_service_video', $this->smallInteger(1)->defaultValue(true));

        $this->update('{{%employee}}', ['need_look_service_video' => false]);
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%employee}}', 'need_look_service_video');
    }
}
