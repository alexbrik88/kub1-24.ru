<?php

use yii\db\Migration;

/**
 * Handles the creation of table `product_store`.
 */
class m180212_080838_create_product_store_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('product_store', [
            'product_id' => $this->integer()->notNull(),
            'store_id' => $this->integer()->notNull(),
            'quantity' => $this->decimal(20, 10)->notNull()->defaultValue(0),
            'initial_quantity' => $this->decimal(20, 10)->notNull()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', '{{product_store}}', ['product_id', 'store_id']);
        $this->addForeignKey ('FK_productStore_product', '{{product_store}}', 'product_id', '{{product}}', 'id', 'CASCADE');
        $this->addForeignKey ('FK_productStore_store', '{{product_store}}', 'store_id', '{{store}}', 'id', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('product_store');
    }
}
