<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210813_070002_update_ofd_row extends Migration
{
    public function safeUp()
    {
        $this->execute('UPDATE `ofd` SET `is_active` = 1 WHERE `alias` = "taxcom" LIMIT 1');
    }

    public function safeDown()
    {
        $this->execute('UPDATE `ofd` SET `is_active` = 0 WHERE `alias` = "taxcom" LIMIT 1');
    }
}
