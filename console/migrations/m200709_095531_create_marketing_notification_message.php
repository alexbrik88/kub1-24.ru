<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200709_095531_create_marketing_notification_message extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('marketing_notification_message', [
            'id' => Schema::TYPE_PK,
            'company_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'channel_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'group_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'message' => Schema::TYPE_TEXT,
            'status' => Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 1',
            'created_at' => Schema::TYPE_INTEGER
        ], $tableOptions);

        $this->addForeignKey('marketing_notification_message_to_company', 'marketing_notification_message', 'company_id', 'company', 'id');
        $this->addForeignKey('marketing_notification_message_to_channel', 'marketing_notification_message', 'channel_id', 'marketing_channel', 'id', 'CASCADE');
        $this->addForeignKey('marketing_notification_message_to_group', 'marketing_notification_message', 'group_id', 'marketing_notification_group', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('marketing_notification_message_to_company', 'marketing_notification_message');
        $this->dropForeignKey('marketing_notification_message_to_channel', 'marketing_notification_message');
        $this->dropForeignKey('marketing_notification_message_to_group', 'marketing_notification_message');

        $this->dropTable('marketing_notification_message');
    }
}
