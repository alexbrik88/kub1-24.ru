<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%payment_reminder_report_invoice}}`.
 */
class m200324_172624_create_payment_reminder_report_invoice_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%payment_reminder_report_invoice}}', [
            'id' => $this->primaryKey(),
            'report_id' => $this->integer()->notNull(),
            'invoice_id' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey(
            'FK_payment_reminder_report_invoice_to_report',
            '{{%payment_reminder_report_invoice}}',
            'report_id',
            '{{%payment_reminder_report}}',
            'id'
        );

        $this->addForeignKey(
            'FK_payment_reminder_report_invoice_to_invoice',
            '{{%payment_reminder_report_invoice}}',
            'invoice_id',
            '{{%invoice}}',
            'id'
        );

        $this->createIndex('UI_report_invoice', '{{%payment_reminder_report_invoice}}', [
            'report_id',
            'invoice_id',
        ], true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%payment_reminder_report_invoice}}');
    }
}
