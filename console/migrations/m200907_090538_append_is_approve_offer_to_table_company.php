<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200907_090538_append_is_approve_offer_to_table_company extends Migration
{
    public $tableName = 'company';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'is_approve_offer', $this->integer(1));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'is_approve_offer');
    }

}
