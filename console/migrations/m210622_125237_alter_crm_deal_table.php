<?php

use yii\db\Migration;

class m210622_125237_alter_crm_deal_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%crm_deal}}';

    /**
     * @inheritDoc
     */
    public function up(): void
    {
        $this->delete(self::TABLE_NAME);
        $this->addColumn(self::TABLE_NAME, 'contact_id', $this->bigInteger()->notNull()->after('contractor_id'));
        $this->createIndex('ck_contact_id', self::TABLE_NAME, ['contact_id']);

        $this->addForeignKey(
            'fk_crm_deal__crm_contact',
            self::TABLE_NAME,
            ['contact_id'],
            '{{%crm_contact}}',
            ['contact_id'],
            'RESTRICT',
            'CASCADE'
        );
    }

    /**
     * @inheritDoc
     */
    public function down(): void
    {
        $this->dropForeignKey('fk_crm_deal__crm_contact', self::TABLE_NAME);
        $this->dropColumn(self::TABLE_NAME, 'contact_id');
    }
}
