<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200203_065549_insert_service_subscribe_tariff_group extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%service_subscribe_tariff_group}}', [
            'id' => 9,
            'name' => 'Распознавание документов',
            'is_base' => 0,
            'has_base' => 0,
            'is_active' => 0,
            'description' => '',
            'priority' => 9,
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%service_subscribe_tariff_group}}', [
            'id' => 9,
        ]);
    }
}
