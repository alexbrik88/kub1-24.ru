<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%cash_flow_linkage}}`.
 */
class m211110_111526_create_cash_flow_linkage_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%cash_flow_linkage}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'number' => $this->integer()->notNull(),
            'status' => $this->tinyInteger(1)->notNull(),
            'is_deleted' => $this->boolean()->notNull(),
            'if_flow_type' => $this->tinyInteger(1)->notNull(),
            'if_wallet_id' => $this->integer()->notNull(),
            'if_contractor_id' => $this->integer()->notNull(),
            'then_flow_type' => $this->tinyInteger(1)->notNull(),
            'then_cashbox_id' => $this->integer()->notNull(),
            'then_contractor_id' => $this->integer()->notNull(),
            'then_date_diff' => $this->integer()->notNull(),
            'then_amount_diff' => $this->decimal(8,6)->notNull(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
        ]);

        $this->addForeignKey('FK_cash_flow_linkage__company_id', '{{%cash_flow_linkage}}', 'company_id', '{{%company}}', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%cash_flow_linkage}}');
    }
}
