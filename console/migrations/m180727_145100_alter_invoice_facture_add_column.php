<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180727_145100_alter_invoice_facture_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('invoice_facture', 'has_file', $this->boolean()->notNull()->defaultValue(false));
        $this->createIndex('has_file', 'invoice_facture', 'has_file');
        $this->execute("
            UPDATE {{invoice_facture}} {{t}}
            SET {{t}}.[[has_file]] = IF(
                EXISTS(
                    SELECT {{f}}.[[id]]
                    FROM {{file}} {{f}}
                    WHERE {{f}}.[[owner_id]] = {{t}}.id AND {{f}}.[[owner_model]] = :class
                ),
                1,
                EXISTS(
                    SELECT {{s}}.[[id]]
                    FROM {{scan_document}} {{s}}
                    WHERE {{s}}.[[owner_id]] = {{t}}.id AND {{s}}.[[owner_model]] = :class
                )
            );
        ", [':class' => 'common\models\document\InvoiceFacture']);
    }

    public function safeDown()
    {
        $this->dropColumn('invoice_facture', 'has_file');
    }
}
