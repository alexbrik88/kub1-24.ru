<?php

use console\components\db\Migration;

class m200208_105002_fssp_task_contractor_id_alter extends Migration
{

    const TABLE = '{{%fssp_task}}';

    public function safeUp()
    {
        $this->dropForeignKey('fssp_task_contractor_id', self::TABLE);
        $this->dropPrimaryKey('contractor_id', self::TABLE);
        $this->alterColumn(self::TABLE, 'contractor_id', $this->integer(11)->comment('ID контрагента, если данные относятся к контрагенту'));
        $this->addForeignKey('fssp_task_contractor_id', self::TABLE, 'contractor_id', '{{%contractor}}', 'id',
            'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->addPrimaryKey('contractor_id', self::TABLE, ['contractor_id']);
    }
}
