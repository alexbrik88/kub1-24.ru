<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170620_080237_alter_order_addColumns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{order}}', 'base_price_no_vat', $this->bigInteger(20)->notNull() . ' AFTER [[mass_gross]]');
        $this->addColumn('{{order}}', 'base_price_with_vat', $this->bigInteger(20)->notNull() . ' AFTER [[base_price_no_vat]]');
        $this->addColumn('{{order}}', 'discount', $this->decimal(4, 2)->defaultValue(0) . ' AFTER [[base_price_with_vat]]');

        $this->execute('
            UPDATE {{order}}
            LEFT JOIN {{invoice}} ON {{invoice}}.[[id]] = {{order}}.[[invoice_id]]
            SET {{order}}.[[base_price_no_vat]] = IF({{invoice}}.[[type]] = 2, {{order}}.[[selling_price_no_vat]], {{order}}.[[purchase_price_no_vat]]),
                {{order}}.[[base_price_with_vat]] = IF({{invoice}}.[[type]] = 2, {{order}}.[[selling_price_with_vat]], {{order}}.[[purchase_price_with_vat]])
        ');
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%order}}', 'price_no_discount');
        $this->dropColumn('{{%order}}', 'discount');
    }
}
