<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180810_072322_alter_order_upd_change_fk extends Migration
{
    public function safeUp()
    {
        $this->dropForeignKey ('FK_orderUpd_upd', '{{%order_upd}}');
        $this->dropForeignKey ('FK_orderUpd_order', '{{%order_upd}}');
        $this->dropForeignKey ('FK_orderUpd_product', '{{%order_upd}}');

        $this->addForeignKey ('FK_orderUpd_upd', '{{%order_upd}}', 'upd_id', '{{%upd}}', 'id');
        $this->addForeignKey ('FK_orderUpd_order', '{{%order_upd}}', 'order_id', '{{%order}}', 'id');
        $this->addForeignKey ('FK_orderUpd_product', '{{%order_upd}}', 'product_id', '{{%product}}', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey ('FK_orderUpd_upd', '{{%order_upd}}');
        $this->dropForeignKey ('FK_orderUpd_order', '{{%order_upd}}');
        $this->dropForeignKey ('FK_orderUpd_product', '{{%order_upd}}');

        $this->addForeignKey ('FK_orderUpd_upd', '{{%order_upd}}', 'upd_id', '{{%upd}}', 'id', 'CASCADE');
        $this->addForeignKey ('FK_orderUpd_order', '{{%order_upd}}', 'order_id', '{{%order}}', 'id', 'CASCADE');
        $this->addForeignKey ('FK_orderUpd_product', '{{%order_upd}}', 'product_id', '{{%product}}', 'id', 'CASCADE');
    }
}
