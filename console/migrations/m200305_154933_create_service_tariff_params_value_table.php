<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%service_tariff_params_value}}`.
 */
class m200305_154933_create_service_tariff_params_value_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%service_tariff_params_value}}', [
            'id' => $this->primaryKey(),
            'tariff_id' => $this->integer()->notNull(),
            'params_id' => $this->integer()->notNull(),
            'value' => $this->string()->notNull(),
            'text' => $this->string()->notNull(),
        ]);

        $this->createIndex('UI_service_tariff_params', '{{%service_tariff_params_value}}', [
            'tariff_id',
            'params_id',
        ], true);
        $this->addForeignKey(
            'FK_service_tariff_params_value_tariff',
            '{{%service_tariff_params_value}}',
            'tariff_id',
            '{{%service_subscribe_tariff}}',
            'id'
        );
        $this->addForeignKey(
            'FK_service_tariff_params_value_params',
            '{{%service_tariff_params_value}}',
            'params_id',
            '{{%service_tariff_params}}',
            'id'
        );

        $this->batchInsert('{{%service_tariff_params_value}}', [
            'tariff_id',
            'params_id',
            'value',
            'text',
        ], [
            [39, 1, '100', 'до 100'],
            [39, 2, '1', 'да'],
            [39, 3, '1', 'да'],
            [39, 4, '1', 'да'],
            [39, 5, '1', 'да'],
            [40, 1, '100', 'до 100'],
            [40, 2, '0', 'нет'],
            [40, 3, '0', 'нет'],
            [40, 4, '0', 'нет'],
            [40, 5, '0', 'нет'],
            [41, 1, '100', 'до 100'],
            [41, 2, '0', 'нет'],
            [41, 3, '0', 'нет'],
            [41, 4, '0', 'нет'],
            [41, 5, '0', 'нет'],
            [42, 1, '100', 'до 100'],
            [42, 2, '0', 'нет'],
            [42, 3, '0', 'нет'],
            [42, 4, '0', 'нет'],
            [42, 5, '0', 'нет'],
            [43, 1, '500', 'до 500'],
            [43, 2, '1', 'да'],
            [43, 3, '1', 'да'],
            [43, 4, '1', 'да'],
            [43, 5, '0', 'нет'],
            [44, 1, '500', 'до 500'],
            [44, 2, '1', 'да'],
            [44, 3, '1', 'да'],
            [44, 4, '1', 'да'],
            [44, 5, '0', 'нет'],
            [45, 1, '500', 'до 500'],
            [45, 2, '1', 'да'],
            [45, 3, '1', 'да'],
            [45, 4, '1', 'да'],
            [45, 5, '0', 'нет'],
            [46, 1, '2000', 'до 2000'],
            [46, 2, '1', 'да'],
            [46, 3, '1', 'да'],
            [46, 4, '1', 'да'],
            [46, 5, '1', 'да'],
            [47, 1, '2000', 'до 2000'],
            [47, 2, '1', 'да'],
            [47, 3, '1', 'да'],
            [47, 4, '1', 'да'],
            [47, 5, '1', 'да'],
            [48, 1, '2000', 'до 2000'],
            [48, 2, '1', 'да'],
            [48, 3, '1', 'да'],
            [48, 4, '1', 'да'],
            [48, 5, '1', 'да'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%service_tariff_params_value}}');
    }
}
