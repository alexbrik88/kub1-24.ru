<?php

use yii\db\Schema;
use yii\db\Migration;

class m150811_114351_alter_invoice_changeColumnSize_contractorName extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('invoice', 'contractor_name_short', Schema::TYPE_STRING . '(255) NOT NULL');
        $this->alterColumn('invoice', 'contractor_name_full', Schema::TYPE_STRING . '(255) NOT NULL');
    }
    
    public function safeDown()
    {
        $this->alterColumn('invoice', 'contractor_name_short', Schema::TYPE_STRING . '(45) NOT NULL');
        $this->alterColumn('invoice', 'contractor_name_full', Schema::TYPE_STRING . '(45) NOT NULL');
    }
}
