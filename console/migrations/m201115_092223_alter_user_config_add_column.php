<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201115_092223_alter_user_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%user_config}}', 'scenario_analysis_help', $this->boolean()->defaultValue(false));
        $this->addColumn('{{%user_config}}', 'scenario_analysis_chart', $this->boolean()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%user_config}}', 'scenario_analysis_help');
        $this->dropColumn('{{%user_config}}', 'scenario_analysis_chart');
    }
}
