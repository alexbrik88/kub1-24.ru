<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201201_173722_alter_ofd_receipt_alter_column extends Migration
{
    public function safeUp()
    {
        $this->execute('DROP TRIGGER IF EXISTS `TR__ofd_receipt__before_inser`');

        $this->alterColumn('{{%ofd_receipt}}', 'id', $this->string(100)->notNull());
    }

    public function safeDown()
    {
        $this->alterColumn('{{%ofd_receipt}}', 'id', $this->bigInteger()->notNull()->unsigned());

        $this->execute('DROP TRIGGER IF EXISTS `TR__ofd_receipt__before_inser`');

        $this->execute('
            CREATE TRIGGER `TR__ofd_receipt__before_inser`
            BEFORE INSERT ON `ofd_receipt`
            FOR EACH ROW
            BEGIN
                SET NEW.`id` = (SELECT IFNULL(MAX(`id`), 0) + 1 FROM `ofd_receipt`);
            END
        ');
    }
}
