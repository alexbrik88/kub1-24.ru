<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201118_064151_update_product_turnover_triggers_5 extends Migration
{
    /**
     * @var string
     */
    public $dropSql = '
        DROP TRIGGER IF EXISTS `product_turnover__act__after_update`;
        DROP TRIGGER IF EXISTS `product_turnover__packing_list__after_update`;
        DROP TRIGGER IF EXISTS `product_turnover__upd__after_update`;
        DROP TRIGGER IF EXISTS `product_turnover__sales_invoice__after_update`;
    ';

    public function safeUp()
    {
        /**
         * Delete exists
         */
        $this->execute($this->dropSql);


        /**
         * `act` triggers
         */
        $this->execute(<<<SQL
CREATE TRIGGER `product_turnover__act__after_update` AFTER UPDATE ON `act`
FOR EACH ROW
BEGIN
    IF
        IFNULL(NEW.`status_out_id`, 1) <> IFNULL(OLD.`status_out_id`, 1) 
        OR NEW.`document_date` <> OLD.`document_date`
    THEN
        UPDATE `product_turnover`
        SET 
        `is_document_actual` = IF(IFNULL(NEW.`status_out_id`, 1) IN (1, 2, 3, 4), 1, 0),
        `date` = NEW.`document_date`
        WHERE `document_id` = NEW.`id`
        AND `document_table` = "act";
    END IF;
END;
SQL);

        /**
         * `packing_list` triggers
         */
        $this->execute(<<<SQL
CREATE TRIGGER `product_turnover__packing_list__after_update` AFTER UPDATE ON `packing_list`
FOR EACH ROW
BEGIN
    IF
        IFNULL(NEW.`status_out_id`, 1) <> IFNULL(OLD.`status_out_id`, 1)
        OR NEW.`document_date` <> OLD.`document_date`
    THEN
        UPDATE `product_turnover`
        SET 
        `is_document_actual` = IF(IFNULL(NEW.`status_out_id`, 1) IN (1, 2, 3, 4), 1, 0),
        `date` = NEW.`document_date`
        WHERE `document_id` = NEW.`id`
        AND `document_table` = "packing_list";
    END IF;
END;
SQL);

        /**
         * `upd` triggers
         */
        $this->execute(<<<SQL
CREATE TRIGGER `product_turnover__upd__after_update` AFTER UPDATE ON `upd`
FOR EACH ROW
BEGIN
    IF
        IFNULL(NEW.`status_out_id`, 1) <> IFNULL(OLD.`status_out_id`, 1)
        OR NEW.`document_date` <> OLD.`document_date`
    THEN
        UPDATE `product_turnover`
        SET 
        `is_document_actual` = IF(IFNULL(NEW.`status_out_id`, 1) IN (1, 2, 3, 4), 1, 0),
        `date` = NEW.`document_date`
        WHERE `document_id` = NEW.`id`
        AND `document_table` = "upd";
    END IF;
END;
SQL);

        /**
         * `sales_invoice` triggers
         */
        $this->execute(<<<SQL
CREATE TRIGGER `product_turnover__sales_invoice__after_update` AFTER UPDATE ON `sales_invoice`
FOR EACH ROW 
BEGIN
    IF
        NEW.`status_out_id` <> OLD.`status_out_id`
        OR NEW.`document_date` <> OLD.`document_date`
    THEN
        UPDATE `product_turnover`
        SET 
        `is_document_actual` = IF(NEW.`status_out_id` IN (1, 2, 3, 4), 1, 0),
        `date` = NEW.`document_date`
        WHERE `document_id` = NEW.`id`
    AND `document_table` = "sales_invoice";
    END IF;
END;
SQL);

    }

    public function safeDown()
    {
        // no down
    }
}
