<?php

use yii\db\Schema;
use yii\db\Migration;

class m151203_150140_update_changeInInvoiceStatusToOutInvoiceStatus extends Migration
{
    public $tInvoice = 'invoice';

    public function safeUp()
    {
        $this->update($this->tInvoice, [
            'invoice_status_id' => 6,
        ], [
            'invoice_status_id' => 2,
        ]);
        $this->update($this->tInvoice, [
            'invoice_status_id' => 7,
        ], [
            'invoice_status_id' => 3,
        ]);
        $this->update($this->tInvoice, [
            'invoice_status_id' => 8,
        ], [
            'invoice_status_id' => 4,
        ]);
    }
    
    public function safeDown()
    {
        $this->update($this->tInvoice, [
            'invoice_status_id' => 2,
        ], [
            'invoice_status_id' => 6,
        ]);
        $this->update($this->tInvoice, [
            'invoice_status_id' => 3,
        ], [
            'invoice_status_id' => 7,
        ]);
        $this->update($this->tInvoice, [
            'invoice_status_id' => 4,
        ], [
            'invoice_status_id' => 48,
        ]);
    }
}
