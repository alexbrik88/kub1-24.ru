<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170620_093744_add_nds_view_type_id_to_uploaded_documents extends Migration
{
    public $tableName = 'uploaded_documents';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'nds_view_type_id', $this->integer()->defaultValue(2));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'nds_view_type_id');
    }
}


