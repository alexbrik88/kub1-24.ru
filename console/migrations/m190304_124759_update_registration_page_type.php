<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190304_124759_update_registration_page_type extends Migration
{
    public $tableName = 'registration_page_type';

    public function safeUp()
    {
        $this->update($this->tableName, ['name' => 'Android'], ['id' => 12]);
        $this->update($this->tableName, ['name' => 'Платежка'], ['id' => 14]);
    }

    public function safeDown()
    {
        $this->update($this->tableName, ['name' => 'Платежка'], ['id' => 12]);
        $this->update($this->tableName, ['name' => 'Моб. приложение'], ['id' => 14]);
    }
}
