<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170418_223659_update_company_id_in_export extends Migration
{
    public $tableName = 'export';

    public function safeUp()
    {
        /* @var $export \frontend\modules\export\models\export\Export */
        foreach (\frontend\modules\export\models\export\Export::find()->all() as $export) {
            $this->update($this->tableName, ['company_id' => $export->employee->company_id], ['id' => $export->id]);
        }
    }

    public function safeDown()
    {

    }
}


