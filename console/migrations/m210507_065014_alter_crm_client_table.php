<?php

use yii\db\Migration;

class m210507_065014_alter_crm_client_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%crm_client}}';

    /**
     * @inheritDoc
     */
    public function up(): void
    {
        $this->addColumn(self::TABLE_NAME, 'client_deal_id', $this->bigInteger()->null()->after('client_id'));
        $this->addColumn(self::TABLE_NAME, 'client_deal_stage_id', $this->bigInteger()->null()->after('client_deal_id'));

        $this->createIndex('ck_client_deal_id', self::TABLE_NAME, 'client_deal_id');
        $this->addForeignKey(
            'fk_crm_client__crm_client_deal',
            self::TABLE_NAME,
            ['client_deal_id'],
            '{{%crm_client_deal}}',
            ['client_deal_id'],
            'SET NULL',
            'CASCADE'
        );

        $this->createIndex('ck_client_deal_stage_id', self::TABLE_NAME, 'client_deal_stage_id');
        $this->addForeignKey(
            'fk_crm_client__crm_client_deal_stage',
            self::TABLE_NAME,
            ['client_deal_stage_id'],
            '{{%crm_client_deal_stage}}',
            ['client_deal_stage_id'],
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * @inheritDoc
     */
    public function down(): void
    {
        $this->dropForeignKey('fk_crm_client__crm_client_deal', self::TABLE_NAME);
        $this->dropForeignKey('fk_crm_client__crm_client_deal_stage', self::TABLE_NAME);
        $this->dropColumn(self::TABLE_NAME, 'client_deal_id');
        $this->dropColumn(self::TABLE_NAME, 'client_deal_stage_id');
    }
}
