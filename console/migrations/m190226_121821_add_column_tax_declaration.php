<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190226_121821_add_column_tax_declaration extends Migration
{
    public $tCompany = '{{%tax_declaration}}';

    public function safeUp()
    {
        $this->addColumn($this->tCompany, 'downloaded_quarter', 'tinyint(1) NOT NULL DEFAULT 0');
    }

    public function safeDown()
    {
        $this->dropColumn($this->tCompany, 'downloaded_quarter');
    }
}
