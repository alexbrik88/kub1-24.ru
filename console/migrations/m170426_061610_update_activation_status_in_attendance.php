<?php

use console\components\db\Migration;
use yii\db\Schema;
use common\models\company\Attendance;
use common\models\document\Invoice;
use frontend\models\Documents;
use common\components\date\DateHelper;
use common\models\Company;

class m170426_061610_update_activation_status_in_attendance extends Migration
{

    public $tAttendance = 'attendance';
    public $tCompany = 'company';

    public function safeUp()
    {
        /* @var $company Company */
        foreach (Company::find()->andWhere(['strict_mode' => 1])->all() as $company) {
            $this->update($this->tCompany, ['strict_mode_date' => null], ['id' => $company->id]);
        }
        foreach (Company::find()->andWhere(['strict_mode_date' => null])->andWhere(['strict_mode' => 0])->all() as $company) {
            $this->update($this->tCompany, ['strict_mode_date' => $company->created_at], ['id' => $company->id]);
        }
        /* @var $attendance Attendance */
        foreach (Attendance::find()->all() as $attendance) {
            $status = null;
            $firstInvoiceDate = Invoice::find()
                ->byCompany($attendance->company_id)
                ->byIOType(Documents::IO_TYPE_OUT)
                ->byDeleted()
                ->min('created_at');
            if ($attendance->company->first_send_invoice_date &&
                $attendance->date > date(DateHelper::FORMAT_DATE, $attendance->company->first_send_invoice_date)
            ) {
                $status = Company::ACTIVATION_ALL_COMPLETE;
            } elseif ($attendance->company->getOutInvoiceCount() && $attendance->date > date(DateHelper::FORMAT_DATE, $firstInvoiceDate)) {
                $status = Company::ACTIVATION_NOT_SEND_INVOICES;
            } elseif ($attendance->company->strict_mode_date && $attendance->date > date(DateHelper::FORMAT_DATE, $attendance->company->strict_mode_date)) {
                $status = Company::ACTIVATION_NO_INVOICE;
            } else {
                $status = Company::ACTIVATION_EMPTY_PROFILE;
            }
            $this->update($this->tAttendance, ['activity_status' => $status], ['id' => $attendance->id]);
        }

    }

    public function safeDown()
    {

    }
}


