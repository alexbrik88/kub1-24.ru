<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191025_082032_alter_employee_company_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('employee_company', 'last_rs', $this->string(20));
    }

    public function safeDown()
    {
        $this->dropColumn('employee_company', 'last_rs');
    }
}
