<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210830_120721_alter_cash_bank_foreign_currency_flows_add_column extends Migration
{
    public function safeUp()  {
        $this->addColumn('cash_bank_foreign_currency_flows', 'account_id', $this->integer()->after('company_id'));
    }

    public function safeDown() {
        $this->dropColumn('cash_bank_foreign_currency_flows', 'account_id');
    }
}
