<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200207_083951_insert_employee_role extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%employee_role}}', [
            'id' => 11,
            'name' => 'Учредитель',
            'sort' => 15000,
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%employee_role}}', [
            'id' => 11,
        ]);
    }
}
