<?php

use console\components\db\Migration;

/**
 * Handles the creation of table `{{%cash_bank_foreign_currency_flows}}`.
 */
class m200710_065638_create_cash_bank_foreign_currency_flows_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->dropTable('IF EXISTS {{%cash_bank_foreign_currency_flows}}');

        $this->createTable('{{%cash_bank_foreign_currency_flows}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'currency_id' => $this->char(3)->notNull(),
            'currency_name' => $this->char(3)->notNull(),
            'income_item_id' => $this->integer(),
            'expenditure_item_id' => $this->integer(),
            'contractor_id' => $this->string(),
            'flow_type' => $this->tinyInteger(1)->notNull()->comment('0 - расход, 1 - приход'),
            'date' => $this->date()->notNull(),
            'recognition_date' => $this->date(),
            'amount' => $this->bigInteger(20)->unsigned()->notNull(),
            'description' => $this->string(),
            'rs' => $this->string(),
            'swift' => $this->string(),
            'bank_name' => $this->string(),
            'bank_address' => $this->string(),
            'payment_order_number' => $this->string(),
            'is_taxable' => $this->boolean(1)->defaultValue(true),
            'has_invoice' => $this->boolean(1)->defaultValue(false)->comment('Связь со счётом: 0 - нет, 1 - да'),
            'is_prepaid_expense' => $this->boolean(1)->defaultValue(false),
            'is_funding_flow' => $this->boolean(1)->defaultValue(false),
            'cash_funding_type_id' => $this->integer(),
            'created_at' => $this->integer(),
        ]);

        $this->addForeignKey(
            'FK_cash_bank_foreign_currency_flows__company',
            '{{%cash_bank_foreign_currency_flows}}',
            'company_id',
            '{{%company}}',
            'id'
        );
        $this->addForeignKey(
            'FK_cash_bank_foreign_currency_flows__invoice_income_item',
            '{{%cash_bank_foreign_currency_flows}}',
            'income_item_id',
            '{{%invoice_income_item}}',
            'id'
        );
        $this->addForeignKey(
            'FK_cash_bank_foreign_currency_flows__invoice_expenditure_item',
            '{{%cash_bank_foreign_currency_flows}}',
            'expenditure_item_id',
            '{{%invoice_expenditure_item}}',
            'id'
        );
        $this->addForeignKey(
            'FK_cash_bank_foreign_currency_flows__cash_funding_type',
            '{{%cash_bank_foreign_currency_flows}}',
            'cash_funding_type_id',
            '{{%cash_funding_type}}',
            'id'
        );

        $this->createIndex(
            'search_by_date',
            '{{%cash_bank_foreign_currency_flows}}',
            [
                'company_id',
                'date',
                'flow_type',
            ]
        );

        $this->createIndex(
            'search_by_recognition_date',
            '{{%cash_bank_foreign_currency_flows}}',
            [
                'company_id',
                'recognition_date',
                'flow_type',
            ]
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%cash_bank_foreign_currency_flows}}');
    }
}
