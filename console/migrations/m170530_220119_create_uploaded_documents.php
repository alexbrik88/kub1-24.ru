<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170530_220119_create_uploaded_documents extends Migration
{
    public $tableName = 'uploaded_documents';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'author_id' => $this->integer()->notNull(),
            'file_name' => $this->string()->defaultValue(null),
            'ext' => $this->string()->defaultValue(null),
            'size' => $this->integer()->defaultValue(null),
            'type' => $this->string()->defaultValue(null),
        ]);

        $this->addForeignKey($this->tableName . '_company_id', $this->tableName, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tableName . '_author_id', $this->tableName, 'author_id', 'employee', 'id', 'RESTRICT', 'CASCADE');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_company_id', $this->tableName);
        $this->dropForeignKey($this->tableName . '_author_id', $this->tableName);
    }
}


