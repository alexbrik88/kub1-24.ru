<?php

use console\components\db\Migration;
use yii\db\Query;
use yii\db\Schema;

class m200724_101241_alter_projects_update_created_at_column extends Migration
{
    public $tableName = 'project';

    /**
     * @return bool|void
     * @throws \yii\db\Exception
     */
    public function safeUp()
    {
        $projects = (new Query())
            ->select(['id', 'created_at', 'updated_at'])
            ->from('project')
            ->all();

        $this->dropColumn($this->tableName, 'created_at');
        $this->addColumn($this->tableName, 'created_at', $this->integer()->unsigned());
        $this->dropColumn($this->tableName, 'updated_at');
        $this->addColumn($this->tableName, 'updated_at', $this->integer()->unsigned());

        foreach ($projects as $project) {
            $query = new Query();
            $query->createCommand()
                ->update($this->tableName, [
                    'created_at' => strtotime($project['created_at']),
                    'updated_at' => strtotime($project['updated_at']),
                ], [
                    'id' => $project['id']
                ])
                ->execute();
        }

    }

    /**
     * @return bool|void
     * @throws \yii\db\Exception
     */
    public function safeDown()
    {
        $projects = (new Query())
            ->select(['id', 'created_at', 'updated_at'])
            ->from('project')
            ->all();

        $this->dropColumn($this->tableName, 'created_at');
        $this->addColumn($this->tableName, 'created_at', $this->dateTime());
        $this->dropColumn($this->tableName, 'updated_at');
        $this->addColumn($this->tableName, 'updated_at', $this->dateTime());

        foreach ($projects as $project) {
            $query = new Query();
            $query->createCommand()->update($this->tableName, [
                    'created_at' => date('Y-m-d H:i:s', $project['created_at']),
                    'updated_at' => date('Y-m-d H:i:s', $project['updated_at']),
                ], [
                    'id' => $project['id']
                ])
                ->execute();
        }

    }
}
