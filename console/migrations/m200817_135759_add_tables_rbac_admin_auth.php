<?php

use console\components\db\Migration;

class m200817_135759_add_tables_rbac_admin_auth extends Migration
{
    private $itemTable = '{{%admin_auth_item}}';
    private $itemChildTable = '{{%admin_auth_item_child}}';
    private $assignmentTable = '{{%admin_auth_assignment}}';
    private $ruleTable = '{{%admin_auth_rule}}';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable($this->ruleTable, [
            'name' => $this->string(64)->notNull(),
            'data' => $this->binary(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'PRIMARY KEY (name)',
        ]);

        $this->createTable($this->itemTable, [
            'name' => $this->string(64)->notNull(),
            'type' => $this->smallInteger()->notNull(),
            'description' => $this->text(),
            'rule_name' => $this->string(64),
            'data' => $this->binary(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
            'PRIMARY KEY (name)',
            'FOREIGN KEY (rule_name) REFERENCES ' . $this->ruleTable . ' (name) ON DELETE SET NULL ON UPDATE CASCADE',
        ]);
        $this->createIndex('idx-retailer_auth_item-type', $this->itemTable, 'type');

        $this->createTable($this->itemChildTable, [
            'parent' => $this->string(64)->notNull(),
            'child' => $this->string(64)->notNull(),
            'PRIMARY KEY (parent, child)',
            'FOREIGN KEY (parent) REFERENCES ' . $this->itemTable . ' (name) ON DELETE CASCADE ON UPDATE CASCADE',
            'FOREIGN KEY (child) REFERENCES ' . $this->itemTable . ' (name) ON DELETE CASCADE ON UPDATE CASCADE',
        ]);

        $this->createTable($this->assignmentTable, [
            'item_name' => $this->string(64)->notNull(),
            'user_id' => $this->string(64)->notNull(),
            'created_at' => $this->integer(),
            'PRIMARY KEY (item_name, user_id)',
            'FOREIGN KEY (item_name) REFERENCES ' . $this->itemTable . ' (name) ON DELETE CASCADE ON UPDATE CASCADE',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->assignmentTable);
        $this->dropTable($this->itemChildTable);
        $this->dropTable($this->itemTable);
        $this->dropTable($this->ruleTable);
    }

}
