<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200610_170451_alter_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%user_config}}', 'product_custom_field', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn('{{%user_config}}', 'product_category_fields', $this->text());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%user_config}}', 'product_custom_field');
        $this->dropColumn('{{%user_config}}', 'product_category_fields');
    }
}
