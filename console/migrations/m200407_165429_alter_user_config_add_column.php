<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200407_165429_alter_user_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_config','report_pc_chart_period',$this->tinyInteger()->notNull()->defaultValue(0));
        $this->addColumn('user_config','report_odds_chart_period',$this->tinyInteger()->notNull()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('user_config','report_pc_chart_period');
        $this->dropColumn('user_config','report_odds_chart_period');
    }
}
