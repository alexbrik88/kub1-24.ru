<?php

use common\models\employee\Employee;
use console\components\db\Migration;
use yii\db\ActiveRecord;

class m200110_202337_bitrix24_company_id_insert_from_employee_id extends Migration
{
    public function safeUp()
    {
        $this->_employee2Company('catalog');
        $this->_employee2Company('company');
        $this->_employee2Company('contact');
        $this->_employee2Company('deal');
        $this->_employee2Company('deal_position');
        $this->_employee2Company('invoice');
        $this->_employee2Company('product');
        $this->_employee2Company('section');
        $this->_employee2Company('user');
        $this->_employee2Company('vat');
    }

    public function safeDown()
    {
        $this->_companyClear('catalog');
        $this->_companyClear('company');
        $this->_companyClear('contact');
        $this->_companyClear('deal');
        $this->_companyClear('deal_position');
        $this->_companyClear('invoice');
        $this->_companyClear('product');
        $this->_companyClear('section');
        $this->_companyClear('user');
        $this->_companyClear('vat');
    }

    private function _employee2Company(string $table)
    {
        /** @var ActiveRecord $class */
        $class = self::_table2Class($table);
        $IDs = [];
        foreach ($class::find()->all() as $model) {
            /** @noinspection PhpUndefinedFieldInspection */
            $employeeId = $model->employee_id;
            if (isset($IDs[$employeeId]) === false) {
                $IDs[$employeeId] = Employee::findOne(['id' => $model->employee_id])->company_id;
            }
            $model->company_id = $IDs[$employeeId];
            $model->save(false, ['company_id']);
        }
    }

    private function _companyClear(string $table)
    {
        /** @var ActiveRecord $class */
        $class = self::_table2Class($table);
        foreach ($class::find()->all() as $model) {
            /** @noinspection PhpUndefinedFieldInspection */
            $model->company_id = 0;
            $model->save(false, ['company_id']);
        }
    }

    private static function _table2Class(string $table): string
    {
        $table = implode('', array_map('ucfirst', explode('_', $table)));
        return 'common\models\bitrix24\\' . $table;
    }
}
