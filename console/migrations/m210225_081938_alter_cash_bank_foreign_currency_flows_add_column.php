<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210225_081938_alter_cash_bank_foreign_currency_flows_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cash_bank_foreign_currency_flows}}', 'source', $this->tinyInteger(1)->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%cash_bank_foreign_currency_flows}}', 'source', $this->tinyInteger(1));
    }
}
