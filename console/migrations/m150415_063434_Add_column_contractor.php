<?php

use yii\db\Schema;
use yii\db\Migration;

class m150415_063434_Add_column_contractor extends Migration
{
    public function up()
    {
        $this->addColumn('contractor', 'chief_accountant_name', Schema::TYPE_STRING);
    }

    public function down()
    {

    }

}
