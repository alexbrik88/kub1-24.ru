<?php

use console\components\db\Migration;
use yii\db\Schema;

class m161206_082505_alter_packingList_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('packing_list', 'is_original', $this->boolean()->defaultValue(false));
    }
    
    public function safeDown()
    {
        $this->dropColumn('packing_list', 'is_original');
    }
}


