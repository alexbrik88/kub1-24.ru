<?php

use console\components\db\Migration;
use yii\db\Schema;

class m151229_143626_update_log_updatemodels extends Migration
{
    public $tLog = 'log';
    
    public function safeUp()
    {
        $this->update($this->tLog, [
            'model_name' => 'common\models\document\Invoice',
        ], [
            'in', 'model_name', [
                'common\models\document\InvoiceIn',
                'common\models\document\InvoiceOut',
            ],
        ]);
        $this->update($this->tLog, [
            'model_name' => 'common\models\document\Act',
        ], [
            'in', 'model_name', [
                'common\models\document\ActIn',
                'common\models\document\ActOut',
            ],
        ]);
        $this->update($this->tLog, [
            'model_name' => 'common\models\document\InvoiceFacture',
        ], [
            'in', 'model_name', [
                'common\models\document\InvoiceFactureIn',
                'common\models\document\InvoiceFactureOut',
            ],
        ]);
        $this->update($this->tLog, [
            'model_name' => 'common\models\document\PackingList',
        ], [
            'in', 'model_name', [
                'common\models\document\PackingListIn',
                'common\models\document\PackingListOut',
            ],
        ]);
    }
    
    public function safeDown()
    {
        echo 'Migration doesn\'t implement down method.';
    }
}


