<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181015_065740_create_registration_page_type extends Migration
{
    public $tRegistrationPageType = 'registration_page_type';
    public $tCompany = 'company';

    public function safeUp()
    {
        $this->createTable($this->tRegistrationPageType, [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);

        $this->insert($this->tRegistrationPageType, [
            'id' => 1,
            'name' => 'Счет',
        ]);

        $this->addColumn($this->tCompany, 'registration_page_type_id', $this->integer()->defaultValue(null));
        $this->addForeignKey($this->tCompany . '_registration_page_type_id', $this->tCompany, 'registration_page_type_id', $this->tRegistrationPageType, 'id', 'RESTRICT', 'CASCADE');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey($this->tCompany . '_registration_page_type_id', $this->tCompany);
        $this->dropColumn($this->tCompany, 'registration_page_type_id');

        $this->dropTable($this->tRegistrationPageType);
    }
}


