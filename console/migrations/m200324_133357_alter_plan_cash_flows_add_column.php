<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200324_133357_alter_plan_cash_flows_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('plan_cash_flows', 'plan_cash_contractor_id', $this->integer()->after('contractor_id'));
        $this->addForeignKey('FK_plan_cash_flows_to_plan_cash_contractor', 'plan_cash_flows', 'plan_cash_contractor_id', 'plan_cash_contractor', 'id', 'SET NULL');
    }

    public function safeDown()
    {
        $this->dropColumn('plan_cash_flows', 'plan_cash_contractor_id');
        $this->dropForeignKey('FK_plan_cash_flows_to_plan_cash_contractor', 'plan_cash_flows');
    }
}
