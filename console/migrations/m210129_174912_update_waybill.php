<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210129_174912_update_waybill extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('waybill', 'consignor_id', $this->integer(11)->defaultValue(null));
    }

    public function safeDown()
    {
        $this->alterColumn('waybill', 'consignor_id', $this->integer(11)->notNull());
    }
}
