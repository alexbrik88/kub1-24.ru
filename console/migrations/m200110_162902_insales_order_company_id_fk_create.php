<?php

use console\components\db\Migration;

class m200110_162902_insales_order_company_id_fk_create extends Migration
{
    const TABLE = '{{%insales_order}}';

    public function up()
    {
        $this->addForeignKey(
            'insales_order_company_id', self::TABLE, 'company_id',
            '{{%company}}', 'id',
            'CASCADE', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey('insales_order_company_id', self::TABLE);
    }
}
