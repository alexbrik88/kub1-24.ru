<?php

use console\components\db\Migration;
use yii\db\Schema;

class m171214_124443_alter_invoice_create_index extends Migration
{
    public function safeUp()
    {
        $this->createIndex('I_type', 'invoice', 'type');
        $this->createIndex('I_invoice_status_id', 'invoice', 'invoice_status_id');
        $this->createIndex('I_is_deleted', 'invoice', 'is_deleted');
    }

    public function safeDown()
    {
        $this->dropIndex('I_type', 'invoice');
        $this->dropIndex('I_invoice_status_id', 'invoice');
        $this->dropIndex('I_is_deleted', 'invoice');
    }
}
