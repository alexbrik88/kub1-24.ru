<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181101_140515_alter_waybill_change_column extends Migration
{
    public function safeUp()
    {
        $this->execute("
            UPDATE {{%waybill}}
            SET {{%waybill}}.[[delivery_time]] = null
        ");

        $this->alterColumn('{{%waybill}}', 'delivery_time', $this->date());
        $this->alterColumn('{{%waybill}}', 'license_card', $this->smallInteger(1));
    }
    
    public function safeDown()
    {
        $this->alterColumn('{{%waybill}}', 'delivery_time', $this->string(25));
        $this->alterColumn('{{%waybill}}', 'license_card', $this->string(45));
    }
}


