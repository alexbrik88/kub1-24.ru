<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160202_090230_add_to_notification_created_at_and_updated_at extends Migration
{
    public $tNotification = 'notification';

    public function safeUp()
    {
        $this->addColumn($this->tNotification, 'created_at', $this->integer(11));
        $this->addColumn($this->tNotification, 'updated_at', $this->integer(11));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tNotification, 'created_at');
        $this->dropColumn($this->tNotification, 'updated_at');
    }
}


