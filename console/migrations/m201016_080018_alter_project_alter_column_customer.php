<?php

use console\components\db\Migration;

class m201016_080018_alter_project_alter_column_customer extends Migration
{
    public $tableName = 'project';

    public function safeUp()
    {
        try {
            $this->dropForeignKey('fk_project_customer', $this->tableName);
            $this->dropIndex('fk_project_customer', $this->tableName);
        } catch (Exception $e) {
            echo $e->getMessage();
        }
        $this->renameColumn($this->tableName, 'customer', 'customer_id');
    }

    public function safeDown()
    {
        $this->renameColumn($this->tableName, 'customer_id', 'customer');
    }
}
