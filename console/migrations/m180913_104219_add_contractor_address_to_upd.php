<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180913_104219_add_contractor_address_to_upd extends Migration
{
    public $tableName = 'upd';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'contractor_address', $this->boolean()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'contractor_address');
    }
}


