<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180820_101204_create_email_signature extends Migration
{
    public $tableName = 'email_signature';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
           'id' => $this->primaryKey(),
           'company_id' => $this->integer()->notNull(),
           'text' => $this->text()->defaultValue(null),
        ]);

        $this->addForeignKey($this->tableName . '_company_id', $this->tableName, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_company_id', $this->tableName);

        $this->dropTable($this->tableName);
    }
}


