<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200519_094834_alter_agreement_change_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('agreement', 'document_date', $this->date());
    }

    public function safeDown()
    {
        $this->alterColumn('agreement', 'document_date', $this->date()->notNull());
    }
}
