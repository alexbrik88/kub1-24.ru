<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190305_170144_alter_out_invoice_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('out_invoice', 'is_demo', $this->boolean()->defaultValue(false)->after('status'));
    }

    public function safeDown()
    {
        $this->dropColumn('out_invoice', 'is_demo');
    }
}
