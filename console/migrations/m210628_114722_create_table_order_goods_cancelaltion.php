<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210628_114722_create_table_order_goods_cancelaltion extends Migration
{
    public function safeUp()
    {
        $this->createTable('order_goods_cancellation', [
            'id' => Schema::TYPE_PK,
            'goods_cancellation_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'product_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'unit_id' => Schema::TYPE_INTEGER . ' NULL',
            'quantity' => Schema::TYPE_DECIMAL . '(20,10) NOT NULL',
            'tax_rate_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'purchase_price_no_vat' => Schema::TYPE_DECIMAL . '(22,2) DEFAULT 0.00',
            'purchase_price_with_vat' => Schema::TYPE_DECIMAL . '(22,2) DEFAULT 0.00',
            'amount_purchase_no_vat' => Schema::TYPE_DECIMAL . '(22,2) DEFAULT 0.00',
            'purchase_tax' => Schema::TYPE_DECIMAL . '(22,2) DEFAULT 0.00',
            'amount_purchase_with_vat' => Schema::TYPE_DECIMAL . '(22,2) DEFAULT 0.00',
            'base_price_no_vat' => Schema::TYPE_DECIMAL . '(22,2) DEFAULT 0.00',
            'base_price_with_vat' => Schema::TYPE_DECIMAL . '(22,2) DEFAULT 0.00',
            'view_price_base' => Schema::TYPE_DECIMAL . '(22,2) DEFAULT 0.00',
            'view_price_one' => Schema::TYPE_DECIMAL . '(22,2) DEFAULT 0.00',
            'view_total_base' => Schema::TYPE_DECIMAL . '(22,2) DEFAULT 0.00',
            'view_total_amount' => Schema::TYPE_DECIMAL . '(22,2) DEFAULT 0.00',
            'view_total_no_nds' => Schema::TYPE_DECIMAL . '(22,2) DEFAULT 0.00',
            'view_total_nds' => Schema::TYPE_DECIMAL . '(22,2) DEFAULT 0.00',
            'view_total_with_nds' => Schema::TYPE_DECIMAL . '(22,2) DEFAULT 0.00',
            'product_title' => Schema::TYPE_TEXT . ' NOT NULL',
            'product_code' => Schema::TYPE_STRING . '(255) NULL',
            'article' => Schema::TYPE_STRING . '(50) NULL',
            'country_id' => Schema::TYPE_INTEGER . ' NULL',
            'custom_declaration_number' => Schema::TYPE_STRING . '(50) NULL',
            'number' => Schema::TYPE_INTEGER . ' NULL',
        ]);

        $this->addForeignKey('fk_order_goods_cancellation_to_goods_cancellation', 'order_goods_cancellation', 'goods_cancellation_id', 'goods_cancellation', 'id', 'CASCADE');
        $this->addForeignKey('fk_order_goods_cancellation_to_product', 'order_goods_cancellation', 'product_id', 'product', 'id', 'CASCADE');
        $this->addForeignKey('fk_order_goods_cancellation_to_product_unit', 'order_goods_cancellation', 'unit_id', 'product_unit', 'id');
        $this->addForeignKey('fk_order_goods_cancellation_to_tax_rate', 'order_goods_cancellation', 'tax_rate_id', 'tax_rate', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_order_goods_cancellation_to_goods_cancellation', 'order_goods_cancellation');
        $this->dropForeignKey('fk_order_goods_cancellation_to_product', 'order_goods_cancellation');
        $this->dropForeignKey('fk_order_goods_cancellation_to_product_unit', 'order_goods_cancellation');
        $this->dropForeignKey('fk_order_goods_cancellation_to_tax_rate', 'order_goods_cancellation');

        $this->dropTable('order_goods_cancellation');
    }
}
