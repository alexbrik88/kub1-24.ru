<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210204_153242_alter_pixel_alter_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%pixel}}', 'email', $this->text());
    }

    public function safeDown()
    {
        $this->alterColumn('{{%pixel}}', 'email', $this->string());
    }
}
