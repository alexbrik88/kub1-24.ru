<?php

use console\components\db\Migration;

class m160606_114156_add_to_product_quantity_in_stock extends Migration
{
    public $tProduct = 'product';

    public function safeUp()
    {
        $this->addColumn($this->tProduct, 'quantity_in_stock', $this->integer()->defaultValue(0));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tProduct, 'quantity_in_stock');
    }
}


