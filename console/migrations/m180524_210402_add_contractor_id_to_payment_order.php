<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180524_210402_add_contractor_id_to_payment_order extends Migration
{
    public $tableName = 'payment_order';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'contractor_id', $this->integer()->defaultValue(null));

        $this->addForeignKey($this->tableName . '_contractor_id', $this->tableName, 'contractor_id', 'contractor', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_contractor_id', $this->tableName);

        $this->dropColumn($this->tableName, 'contractor_id');
    }
}


