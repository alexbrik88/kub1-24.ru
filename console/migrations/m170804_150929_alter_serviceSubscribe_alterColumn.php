<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170804_150929_alter_serviceSubscribe_alterColumn extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%service_subscribe}}', 'discount', $this->decimal(4, 2)->defaultValue(0));
    }
    
    public function safeDown()
    {
        $this->alterColumn('{{%service_subscribe}}', 'discount', $this->integer()->defaultValue(0));
    }
}
