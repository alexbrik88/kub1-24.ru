<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180207_154448_remove_project_date_in_project extends Migration
{
    public $tProject = 'project';
    public $tConfig = 'user_config';

    public function safeUp()
    {
        $this->dropColumn($this->tProject, 'project_date');
        $this->dropColumn($this->tConfig, 'project_date');
    }
    
    public function safeDown()
    {
        echo 'This migration not rolled back';
    }
}


