<?php

use console\components\db\Migration;

class m200110_105859_amocrm_note_company_id_fk_create extends Migration
{
    const TABLE = '{{%amocrm_note}}';

    public function up()
    {
        $this->addForeignKey(
            'amocrm_note_company_id', self::TABLE, 'company_id',
            '{{%company}}', 'id',
            'CASCADE', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey('amocrm_note_company_id', self::TABLE);
    }
}
