<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190512_182301_alter_scan_document_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%scan_document}}', 'contractor_id', $this->integer()->after('owner_id'));
        $this->addColumn('{{%scan_document}}', 'document_type_id', $this->integer()->after('contractor_id'));

        $this->addForeignKey('FK_scan_document_contractor', '{{%scan_document}}', 'contractor_id', '{{%contractor}}', 'id');
        $this->createIndex('idx-document_type_id', '{{%scan_document}}', 'document_type_id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_scan_document_contractor', '{{%scan_document}}');
        $this->dropIndex('idx-document_type_id', '{{%scan_document}}');

        $this->dropColumn('{{%scan_document}}', 'contractor_id');
        $this->dropColumn('{{%scan_document}}', 'document_type_id');
    }
}
