<?php

use yii\db\Migration;

class m210323_064548_drop_last_operation_in_integration_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%last_operation_in_integration}}';

    /**
     * @inheritDoc
     */
    public function up()
    {
        $this->dropTable(self::TABLE_NAME);
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        return false;
    }
}
