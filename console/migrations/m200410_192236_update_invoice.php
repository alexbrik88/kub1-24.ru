<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200410_192236_update_invoice extends Migration
{
    public function safeUp()
    {
        Yii::$app->db->createCommand('SET session wait_timeout=288000')->execute();
        Yii::$app->db->createCommand('SET session interactive_timeout=288000')->execute();

        $this->execute("
            UPDATE {{invoice}} {{t}}
            SET               
                {{t}}.[[has_act]] = EXISTS(
                    SELECT {{ia}}.[[act_id]] FROM {{invoice_act}} {{ia}} WHERE {{ia}}.[[invoice_id]] = {{t}}.[[id]]
                ),
                {{t}}.[[has_packing_list]] = EXISTS(
                    SELECT {{p}}.[[id]] FROM {{packing_list}} {{p}} WHERE {{p}}.[[invoice_id]] = {{t}}.[[id]]
                ),
                {{t}}.[[has_invoice_facture]] = EXISTS(
                    SELECT {{i}}.[[id]] FROM {{invoice_facture}} {{i}} WHERE {{i}}.[[invoice_id]] = {{t}}.[[id]]
                ),
                {{t}}.[[has_upd]] = EXISTS(
                    SELECT {{u}}.[[id]] FROM {{upd}} {{u}} WHERE {{u}}.[[invoice_id]] = {{t}}.[[id]]
                )
        ", [':class' => 'common\models\document\Invoice']);
    }

    public function safeDown()
    {
    }
}
