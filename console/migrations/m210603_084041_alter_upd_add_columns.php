<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210603_084041_alter_upd_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('upd', 'contractor_id', $this->integer()->after('type'));
        $this->addColumn('upd', 'company_id', $this->integer()->after('type'));

        $this->execute("
            UPDATE `upd` doc
            LEFT JOIN `invoice_upd` rel ON rel.upd_id = doc.id
            LEFT JOIN `invoice` ON invoice.id = rel.invoice_id
            SET doc.company_id = invoice.company_id, doc.contractor_id = invoice.contractor_id
        ");

        $this->addForeignKey('FK_upd_to_contractor', 'upd', 'contractor_id', 'contractor', 'id', 'CASCADE');
        $this->addForeignKey('FK_upd_to_company', 'upd', 'company_id', 'company', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_upd_to_company', 'upd');
        $this->dropForeignKey('FK_upd_to_contractor', 'upd');
        $this->dropColumn('upd', 'contractor_id');
        $this->dropColumn('upd', 'company_id');
    }
}
