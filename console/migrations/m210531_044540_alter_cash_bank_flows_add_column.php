<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210531_044540_alter_cash_bank_flows_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cash_bank_flows', 'account_id', $this->integer()->after('company_id'));
        $this->execute('
            UPDATE `cash_bank_flows` cbf
            LEFT JOIN `checking_accountant` a ON a.company_id = cbf.company_id AND a.rs = cbf.rs 
            SET cbf.account_id = a.id
        ');
    }

    public function safeDown()
    {
        $this->dropColumn('cash_bank_flows', 'account_id');
    }
}
