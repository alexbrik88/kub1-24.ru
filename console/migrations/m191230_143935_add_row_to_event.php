<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191230_143935_add_row_to_event extends Migration
{
    public function safeUp()
    {
        $this->batchInsert('event', ['id', 'name'], [

            ['99', 'Робот-бухгалтер. Попап «Уменьшим Ваши налоги»'],

        ]);
    }

    public function safeDown()
    {
        $this->delete('event', ['id' => [99]]);
    }
}
