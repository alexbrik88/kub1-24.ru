<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200422_073758_alter_user_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%user_config}}', 'product_purchase_price',
            $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn('{{%user_config}}', 'product_purchase_amount',
            $this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%user_config}}', 'product_purchase_price');
        $this->dropColumn('{{%user_config}}', 'product_purchase_amount');
    }
}
