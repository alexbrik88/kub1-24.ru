<?php

use console\components\db\Migration;
use yii\db\Schema;

/**
 * Class m170201_121735_alter_company_add_comment
 */
class m170201_121735_alter_company_add_comment extends Migration
{

    /**
     *
     */
    public function safeUp()
    {
        $this->addColumn('{{%company}}', 'comment', $this->text()->null());
    }

    /**
     *
     */
    public function safeDown()
    {
        $this->dropColumn('{{%company}}', 'comment');
    }
}


