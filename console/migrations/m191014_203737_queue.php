<?php

use console\components\db\Migration;
use yii\queue\db\migrations\M161119140200Queue;
use yii\queue\db\migrations\M170307170300Later;
use yii\queue\db\migrations\M170509001400Retry;
use yii\queue\db\migrations\M170601155600Priority;

class m191014_203737_queue extends Migration
{
    /** @var Migration[] */
    private $queueMigrations;

    public function init()
    {
        parent::init();
        foreach ([
            M161119140200Queue::class,
            M170307170300Later::class,
            M170509001400Retry::class,
            M170601155600Priority::class,
        ] as $queueMigration) {
            $this->queueMigrations[] = new $queueMigration();
        }
    }

    public function safeUp()
    {
        foreach ($this->queueMigrations as $queueMigration) {
            $queueMigration->up();
        }
    }

    public function safeDown()
    {
        foreach (array_reverse($this->queueMigrations) as $queueMigration) {
            $queueMigration->down();
        }
    }
}
