<?php

use console\components\db\Migration;
use yii\db\Schema;

class m151222_150123_alter_product_update_created_at extends Migration
{
    public $tProduct = 'product';

    public function safeUp()
    {
        $this->addColumn($this->tProduct, 'created', $this->integer(11)->defaultValue(null));
        $this->update($this->tProduct, [
            'created' => new \yii\db\Expression('UNIX_TIMESTAMP (created_at)'),
        ]);
        $this->dropColumn($this->tProduct, 'created_at');
        $this->renameColumn($this->tProduct, 'created', 'created_at');
    }
    
    public function safeDown()
    {
        echo 'This migration should not be rolled back';
    }
}


