<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201203_095659_alter_finance_model_other_costs extends Migration
{
    public static $table = 'finance_model_other_cost';

    public function safeUp()
    {
        $this->renameColumn(self::$table, 'income_expense', 'income');
        $this->addColumn(self::$table, 'expense', $this->bigInteger()->defaultValue(0)->after('income'));
        $this->addColumn(self::$table, 'amortization', $this->bigInteger()->defaultValue(0)->after('expense'));

        $this->renameColumn(self::$table, 'paid_received_percents', 'received_percents');
        $this->addColumn(self::$table, 'paid_percents', $this->bigInteger()->defaultValue(0)->after('received_percents'));
    }

    public function safeDown()
    {
        $this->dropColumn(self::$table, 'amortization');
        $this->dropColumn(self::$table, 'expense');
        $this->renameColumn(self::$table, 'income', 'income_expense');

        $this->dropColumn(self::$table, 'received_percents');
        $this->renameColumn(self::$table, 'paid_percents', 'paid_received_percents');
    }
}
