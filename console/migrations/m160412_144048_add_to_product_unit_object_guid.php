<?php

use console\components\db\Migration;

class m160412_144048_add_to_product_unit_object_guid extends Migration
{
    public $tProductUnit = 'product_unit';

    public function safeUp()
    {
        $this->addColumn($this->tProductUnit, 'object_guid', $this->string(36));

        $this->update($this->tProductUnit, ['object_guid' => '8C2C54FA-C697-89A4-940C-1C470FCBCA04'], ['id' => 1]);
        $this->update($this->tProductUnit, ['object_guid' => '5D708D82-5B5F-3A7A-2005-DAA3E334AC86'], ['id' => 2]);
        $this->update($this->tProductUnit, ['object_guid' => '8A955A02-BCD5-34B0-37DD-17D59E5FA914'], ['id' => 3]);
        $this->update($this->tProductUnit, ['object_guid' => '7A33DD39-D52F-F42F-1206-AE4839AE1178'], ['id' => 4]);
        $this->update($this->tProductUnit, ['object_guid' => 'DD9ACE77-BDDF-8865-644F-E37CEBE27553'], ['id' => 5]);
        $this->update($this->tProductUnit, ['object_guid' => 'A2213B75-6771-4F18-3547-944C520D0A8F'], ['id' => 6]);
        $this->update($this->tProductUnit, ['object_guid' => '352EE857-A8F2-3A48-3282-C1B3F6F6BFD8'], ['id' => 7]);
        $this->update($this->tProductUnit, ['object_guid' => 'E597162D-66ED-D644-04E0-F5450DB2E5E0'], ['id' => 8]);
        $this->update($this->tProductUnit, ['object_guid' => 'DA7886C1-86FF-C1D4-23BE-96E68EADCF6C'], ['id' => 9]);
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tProductUnit, 'object_guid');
    }
}


