<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170912_123457_add_socket_id_to_employee extends Migration
{
    public $tableName = 'employee';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'socket_id', $this->integer()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'socket_id');
    }
}


