<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210324_141541_insert_invoice_expenditure_item extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%invoice_expenditure_item}}', [
            'id' => 91,
            'name' => 'Конвертация валюты',
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%invoice_expenditure_item}}', [
            'id' => 91,
        ]);
    }
}
