<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191015_182531_create_jobs extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%jobs}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'employee_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(11)->unsigned()->notNull(),
            'finished_at' => $this->integer(11)->unsigned()->defaultValue(null),
            'type' => $this->integer(11)->unsigned()->notNull(),
            'params' => $this->text()->defaultValue(null),
            'result' => $this->text()->notNull(),
        ]);

        $this->addForeignKey('jobs_company_id', '{{%jobs}}', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('jobs_employee_id', '{{%jobs}}', 'employee_id', 'employee', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('jobs_company_id', '{{%jobs}}');
        $this->dropForeignKey('jobs_employee_id', '{{%jobs}}');

        $this->dropTable('{{%jobs}}');
    }
}
