<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191203_121840_update_service_subscribe_tariff_group extends Migration
{
    public function safeUp()
    {
        $this->update('{{%service_subscribe_tariff_group}}', [
            'hint' => '(Выставление счетов)',
        ], ['id' => 1]);
        $this->update('{{%service_subscribe_tariff_group}}', [
            'name' => 'Налоги и Декларация для ИП на УСН 6%',
            'hint' => '(Бухгалтерия ИП)',
        ], ['id' => 4]);
    }

    public function safeDown()
    {
        $this->update('{{%service_subscribe_tariff_group}}', [
            'hint' => null,
        ], ['id' => 1]);
        $this->update('{{%service_subscribe_tariff_group}}', [
            'name' => 'Бухгалтерия ИП на УСН 6%',
            'hint' => null,
        ], ['id' => 4]);
    }
}
