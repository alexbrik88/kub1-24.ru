<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201102_133036_alter_project_estimate_item_append_count extends Migration
{
    public $tableName = 'project_estimate_item';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'amount', $this->integer()->unsigned()->after('product_id'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'amount');
    }
}
