<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180109_115134_update_service_payment_type extends Migration
{
    public function safeUp()
    {
        $this->update('service_payment_type', ['name' => 'Квитанция сбербанка'], ['id' => 2]);
    }

    public function safeDown()
    {
        $this->update('service_payment_type', ['name' => 'Квитанци сбербанка'], ['id' => 2]);
    }
}
