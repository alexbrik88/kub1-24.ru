<?php

use console\components\db\Migration;
use yii\db\Schema;
use common\models\product\Product;

class m180920_144946_add_has_photo_to_product extends Migration
{
    public $tableName = 'product';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'has_photo', $this->boolean()->defaultValue(false));

        $products = [];
        foreach (Product::find()->byProductionType(Product::PRODUCTION_TYPE_GOODS)->each() as $product) {
            if ($product->getUploadedImage()) {
                $products[] = $product->id;
            }
        }

        $this->update($this->tableName, ['has_photo' => true], ['id' => $products]);
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'has_photo');
    }
}


