<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191028_153408_insert_into_event extends Migration
{
    public function safeUp()
    {
        $this->batchInsert('event', ['id', 'name'], [

            ['98', 'Интеграция с банком'],

        ]);
    }

    public function safeDown()
    {
        $this->delete('event', ['id' => [98]]);
    }
}
