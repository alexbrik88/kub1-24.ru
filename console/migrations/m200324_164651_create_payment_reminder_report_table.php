<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%payment_reminder_report}}`.
 */
class m200324_164651_create_payment_reminder_report_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%payment_reminder_report}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'contractor_id' => $this->integer()->notNull(),
            'message_id' => $this->integer()->notNull(),
            'category_id' => $this->integer()->notNull(),
            'date' => $this->date()->notNull(),
            'days_overdue' => $this->integer()->notNull(),
            'debt_amount' => $this->integer(20)->notNull(),
            'invoice' => $this->text()->notNull(),
            'is_sent' => $this->boolean()->notNull(),
            'subject' => $this->string()->notNull(),
            'event_header' => $this->string()->notNull(),
            'event_body' => $this->text()->notNull(),
            'email' => $this->text()->notNull(),
        ]);

        $this->addForeignKey(
            'FK_payment_reminder_report_to_company',
            '{{%payment_reminder_report}}',
            'company_id',
            '{{%company}}',
            'id'
        );

        $this->addForeignKey(
            'FK_payment_reminder_report_to_contractor',
            '{{%payment_reminder_report}}',
            'contractor_id',
            '{{%contractor}}',
            'id'
        );

        $this->addForeignKey(
            'FK_payment_reminder_report_to_message',
            '{{%payment_reminder_report}}',
            'message_id',
            '{{%payment_reminder_message}}',
            'id'
        );

        $this->addForeignKey(
            'FK_payment_reminder_report_to_category',
            '{{%payment_reminder_report}}',
            'category_id',
            '{{%payment_reminder_message_category}}',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%payment_reminder_report}}');
    }
}
