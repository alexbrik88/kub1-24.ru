<?php

use console\components\db\Migration;
use yii\db\Schema;
use common\models\Company;
use common\models\TaxationType;
use common\models\company\CompanyTaxationType;

class m161228_085226_update_company_taxation_type extends Migration
{
    public $tableName = 'company_taxation_type';

    public function safeUp()
    {
        $companiesWithOSNO = (new \yii\db\Query())->select('id')->from('company')->andWhere([
            'taxation_type_id' => TaxationType::TYPE_OSNO,
        ])->column();
        $companiesWithUSN6 = (new \yii\db\Query())->select('id')->from('company')->andWhere([
            'taxation_type_id' => TaxationType::TYPE_USN_6,
        ])->column();
        $companiesWithUSN15 = (new \yii\db\Query())->select('id')->from('company')->andWhere([
            'taxation_type_id' => TaxationType::TYPE_USN_15,
        ])->column();
        $companiesWithENVD = (new \yii\db\Query())->select('id')->from('company')->andWhere([
            'taxation_type_id' => TaxationType::TYPE_ENVD,
        ])->column();

        $rows = [];

        foreach ($companiesWithOSNO as $id) {
            $rows[] = [
                'company_id' => $id,
                'osno' => 1,
                'usn' => 0,
                'envd' => 0,
                'psn' => 0,
                'usn_type' => 0,
                'usn_percent' => null,
            ];
        }
        foreach ($companiesWithUSN6 as $id) {
            $rows[] = [
                'company_id' => $id,
                'osno' => 0,
                'usn' => 1,
                'envd' => 0,
                'psn' => 0,
                'usn_type' => 0,
                'usn_percent' => 6,
            ];
        }
        foreach ($companiesWithUSN15 as $id) {
            $rows[] = [
                'company_id' => $id,
                'osno' => 0,
                'usn' => 1,
                'envd' => 0,
                'psn' => 0,
                'usn_type' => 0,
                'usn_percent' => 15,
            ];
        }
        foreach ($companiesWithENVD as $id) {
            $rows[] = [
                'company_id' => $id,
                'osno' => 0,
                'usn' => 1,
                'envd' => 1,
                'psn' => 0,
                'usn_type' => 0,
                'usn_percent' => null,
            ];
        }

        if ($rows) {
            $this->batchInsert('{{%company_taxation_type}}', [
                'company_id',
                'osno',
                'usn',
                'envd',
                'psn',
                'usn_type',
                'usn_percent',
            ], $rows);
        }
    }

    public function safeDown()
    {
        $this->truncateTable($this->tableName);
    }
}
