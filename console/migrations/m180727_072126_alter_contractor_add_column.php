<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180727_072126_alter_contractor_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('contractor', 'postal_address', $this->string(160)->after('actual_address'));
    }

    public function safeDown()
    {
        $this->dropColumn('contractor', 'postal_address');
    }
}
