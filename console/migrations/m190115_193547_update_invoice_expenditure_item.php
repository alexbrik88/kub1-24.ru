<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190115_193547_update_invoice_expenditure_item extends Migration
{
    public function safeUp()
    {
        $this->update('invoice_expenditure_item', ['name' => 'Страховые взносы за ИП'], ['name' => 'Фиксированный платеж в ПФР']);
    }

    public function safeDown()
    {
        $this->update('invoice_expenditure_item', ['name' => 'Фиксированный платеж в ПФР'], ['name' => 'Страховые взносы за ИП']);
    }
}
