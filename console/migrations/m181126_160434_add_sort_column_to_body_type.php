<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181126_160434_add_sort_column_to_body_type extends Migration
{
    public $tableName = 'body_type';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'sort', $this->integer()->defaultValue(null));

        $this->update($this->tableName, ['sort' => 10], ['id' => 1]);
        $this->update($this->tableName, ['sort' => 20], ['id' => 3]);
        $this->update($this->tableName, ['sort' => 30], ['id' => 4]);
        $this->update($this->tableName, ['sort' => 40], ['id' => 15]);
        $this->update($this->tableName, ['sort' => 50], ['id' => 5]);
        $this->update($this->tableName, ['sort' => 60], ['id' => 6]);
        $this->update($this->tableName, ['sort' => 70], ['id' => 7]);
        $this->update($this->tableName, ['sort' => 80], ['id' => 8]);
        $this->update($this->tableName, ['sort' => 90], ['id' => 19]);

        $this->update($this->tableName, ['sort' => 400], ['sort' => null]);
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'sort');
    }
}


