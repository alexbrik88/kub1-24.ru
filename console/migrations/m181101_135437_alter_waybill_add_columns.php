<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181101_135437_alter_waybill_add_columns extends Migration
{
    public function safeUp()
    {
        // transport-tab1
        $this->addColumn('{{%waybill}}', 'with_packing_list', $this->smallInteger(1));
        $this->addColumn('{{%waybill}}', 'with_invoice', $this->smallInteger(1));
        $this->addColumn('{{%waybill}}', 'with_invoice_facture', $this->smallInteger(1));
        $this->addColumn('{{%waybill}}', 'weight_determining_method', $this->smallInteger(1));
        // transport-tab2
        $this->addColumn('{{%waybill}}', 'loading_operations', $this->string(100));
        $this->addColumn('{{%waybill}}', 'unloading_operations', $this->string(100));
        $this->addColumn('{{%waybill}}', 'loading_method_name', $this->string(50));
        $this->addColumn('{{%waybill}}', 'unloading_method_name', $this->string(50));
        $this->addColumn('{{%waybill}}', 'loading_method_code', $this->string(10));
        $this->addColumn('{{%waybill}}', 'unloading_method_code', $this->string(10));
        $this->addColumn('{{%waybill}}', 'loading_time_come', $this->string(5));
        $this->addColumn('{{%waybill}}', 'loading_time_gone', $this->string(5));
        $this->addColumn('{{%waybill}}', 'loading_time_operations', $this->string(5));
        $this->addColumn('{{%waybill}}', 'unloading_time_come', $this->string(5));
        $this->addColumn('{{%waybill}}', 'unloading_time_gone', $this->string(5));
        $this->addColumn('{{%waybill}}', 'unloading_time_operations', $this->string(5));
        // transport-tab3
        $this->addColumn('{{%waybill}}', 'transportation_distance', $this->integer());
        $this->addColumn('{{%waybill}}', 'transportation_in_city', $this->integer());
        $this->addColumn('{{%waybill}}', 'transportation_group_1', $this->integer());
        $this->addColumn('{{%waybill}}', 'transportation_group_2', $this->integer());
        $this->addColumn('{{%waybill}}', 'transportation_group_3', $this->integer());
        $this->addColumn('{{%waybill}}', 'transportation_cost', $this->integer());
        $this->addColumn('{{%waybill}}', 'transportation_downtime_loading', $this->string(5));
        $this->addColumn('{{%waybill}}', 'transportation_downtime_unloading', $this->string(5));
        // transport
        $this->addColumn('{{%waybill}}', 'taxation', $this->string(50));
        $this->addColumn('{{%waybill}}', 'taximaster', $this->string(50));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%waybill}}', 'with_packing_list');
        $this->dropColumn('{{%waybill}}', 'with_invoice');
        $this->dropColumn('{{%waybill}}', 'with_invoice_facture');
        $this->dropColumn('{{%waybill}}', 'weight_determining_method');
        $this->dropColumn('{{%waybill}}', 'loading_operations');
        $this->dropColumn('{{%waybill}}', 'unloading_operations');
        $this->dropColumn('{{%waybill}}', 'loading_method_name');
        $this->dropColumn('{{%waybill}}', 'unloading_method_name');
        $this->dropColumn('{{%waybill}}', 'loading_method_code');
        $this->dropColumn('{{%waybill}}', 'unloading_method_code');
        $this->dropColumn('{{%waybill}}', 'loading_time_come');
        $this->dropColumn('{{%waybill}}', 'loading_time_gone');
        $this->dropColumn('{{%waybill}}', 'loading_time_operations');
        $this->dropColumn('{{%waybill}}', 'unloading_time_come');
        $this->dropColumn('{{%waybill}}', 'unloading_time_gone');
        $this->dropColumn('{{%waybill}}', 'unloading_time_operations');
        $this->dropColumn('{{%waybill}}', 'transportation_distance');
        $this->dropColumn('{{%waybill}}', 'transportation_in_city');
        $this->dropColumn('{{%waybill}}', 'transportation_group_1');
        $this->dropColumn('{{%waybill}}', 'transportation_group_2');
        $this->dropColumn('{{%waybill}}', 'transportation_group_3');
        $this->dropColumn('{{%waybill}}', 'transportation_cost');
        $this->dropColumn('{{%waybill}}', 'transportation_downtime_loading');
        $this->dropColumn('{{%waybill}}', 'transportation_downtime_unloading');
        $this->dropColumn('{{%waybill}}', 'taxation');
        $this->dropColumn('{{%waybill}}', 'taximaster');
    }
}


