<?php

use yii\db\Migration;

class m210621_102844_rename_crm_client_deal_table extends Migration
{
    /**
     * @inheritDoc
     */
    public function up(): void
    {
        $this->renameTable('{{%crm_client_deal}}', '{{%crm_deal_type}}');
    }

    /**
     * @inheritDoc
     */
    public function down(): bool
    {
        return false;
    }
}
