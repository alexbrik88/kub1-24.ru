<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200621_210258_update_rent_attribute extends Migration
{
    const CATEGORY_REALTY = 1; //ID категории "недвижимость"
    const CATEGORY_TECHNIC = 2; //ID категории "спец.техника"
    const CATEGORY_EQUIPMENT = 3; //ID категории "оборудование"

    public $table = 'rent_attribute';

    public function safeUp()
    {
        $this->update(
            $this->table,
            ['tab' => 'price'],
            ['alias' => ['priceDay', 'priceMonth', 'deposit', 'utilityPayment', 'priceHour', 'priceShift']]
        );

        $this->update(
            $this->table,
            ['show' => '0', 'required' => 0],
            ['alias' => ['rooms', 'priceDay', 'priceMonth', 'deposit', 'utilityPayment', 'mending', 'tech',
                'priceHour', 'priceShift']]
        );

        $this->update($this->table, ['containerCss' => null], ['alias' => 'address']);
        $this->update($this->table, ['title' => 'Площадь м2'], ['alias' => 'area']);

        $this->batchInsert(
            $this->table,
            ['category_id', 'alias', 'title', 'type', 'required', 'default', 'data', 'tab', 'sort'],
            [
                [self::CATEGORY_REALTY, 'unit', 'Ед. измерения', 'enum', 1, 'м2', '["м2", "Объект целиком"]', 'price', 1],
                [self::CATEGORY_EQUIPMENT, 'unit', 'Ед. измерения', 'enum', 1, 'шт', '["шт"]', 'price', 1],
                [self::CATEGORY_TECHNIC, 'unit', 'Ед. измерения', 'enum', 1, 'шт', '["шт"]', 'price', 1],

                [self::CATEGORY_REALTY, 'paymentPeriod', 'Период оплаты', 'enum', 1, null, '["День", "Месяц", "Год"]', 'price', 2],
                [self::CATEGORY_EQUIPMENT, 'paymentPeriod', 'Период оплаты', 'enum', 1, null, '["День", "Месяц", "Год"]', 'price', 2],
                [self::CATEGORY_TECHNIC, 'paymentPeriod', 'Период оплаты', 'enum', 1, null, '["День", "Месяц", "Год"]', 'price', 2],

                [self::CATEGORY_REALTY, 'price', 'Цена за ед. и период', 'float', 1, null, null, 'price', 3],
                [self::CATEGORY_EQUIPMENT, 'price', 'Цена за ед. и период', 'float', 1, null, null, 'price', 3],
                [self::CATEGORY_TECHNIC, 'price', 'Цена за ед. и период', 'float', 1, null, null, 'price', 3],

                [self::CATEGORY_REALTY, 'currency', 'Валюта', 'enum', 1, 'RUB', '["RUB"]', 'price', 4],
                [self::CATEGORY_EQUIPMENT, 'currency', 'Валюта', 'enum', 1, 'RUB', '["RUB"]', 'price', 4],
                [self::CATEGORY_TECHNIC, 'currency', 'Валюта', 'enum', 1, 'RUB', '["RUB"]', 'price', 4],
            ]
        );
    }

    public function safeDown()
    {
        $this->delete($this->table, ['alias' => ['currency', 'unit', 'paymentPeriod', 'price']]);
        $this->update($this->table, ['tab' => 'info', 'show' => 1]);
        $this->update($this->table, ['containerCss' => 'full-width'], ['alias' => 'address']);
        $this->update($this->table, ['title' => 'Площадь'], ['alias' => 'area']);
    }
}
