<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160209_064103_alter_column_payment_date_in_service_payment extends Migration
{
    public $tServicePayment = 'service_payment';

    public function safeUp()
    {
        $this->addColumn($this->tServicePayment, 'unix', $this->integer(11)->notNull());
        $this->update($this->tServicePayment, [
            'unix' => new \yii\db\Expression('UNIX_TIMESTAMP (payment_date)'),
        ]);

        $this->dropColumn($this->tServicePayment, 'payment_date');
        $this->renameColumn($this->tServicePayment, 'unix', 'payment_date');
    }

    public function safeDown()
    {
        $this->addColumn($this->tServicePayment, 'date_time', $this->dateTime());
        $this->update($this->tServicePayment, [
            'date_time' => new \yii\db\Expression('FROM_UNIXTIME (payment_date)'),
        ]);

        $this->dropColumn($this->tServicePayment, 'payment_date');
        $this->renameColumn($this->tServicePayment, 'date_time', 'payment_date');
    }
}


