<?php

use console\components\db\Migration;
use yii\db\Schema;
use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;

class m171129_151129_update_payment_partial_invoices extends Migration
{
    public $tableName = 'invoice';

    public function safeUp()
    {
        /* @var $invoice Invoice */
        foreach (Invoice::find()->byDeleted()->byStatus(InvoiceStatus::STATUS_PAYED_PARTIAL)->all() as $invoice) {
            if ($invoice->getPaidAmount() == $invoice->total_amount_with_nds) {
                $this->update($this->tableName, ['invoice_status_id' => InvoiceStatus::STATUS_PAYED], ['id' => $invoice->id]);
            }
        }
    }

    public function safeDown()
    {

    }
}


