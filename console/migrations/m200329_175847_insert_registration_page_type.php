<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200329_175847_insert_registration_page_type extends Migration
{
    public $tableName = 'registration_page_type';

    public function safeUp()
    {
        $this->insert($this->tableName, [
            'id' => 28,
            'name' => 'Автосбор долгов',
        ]);
    }

    public function safeDown()
    {
        $this->delete($this->tableName, ['id' => 28]);
    }
}
