<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211203_102156_update_rent_attribute extends Migration
{
    public function safeUp()
    {
        $this->update('{{%rent_attribute}}', [
            'title' => 'Адрес постоянного базирования',
        ], [
            'category_id' => 2,
            'alias' => 'address',
        ]);
    }

    public function safeDown()
    {
        $this->update('{{%rent_attribute}}', [
            'title' => 'Адрес постоянной базировки',
        ], [
            'category_id' => 2,
            'alias' => 'address',
        ]);
    }
}
