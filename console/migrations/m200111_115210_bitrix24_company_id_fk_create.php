<?php

use console\components\db\Migration;

class m200111_115210_bitrix24_company_id_fk_create extends Migration
{
    public function safeUp()
    {
        $this->createFK('catalog','company_id');
        $this->createFK('company','company_id');
        $this->createFK('contact','company_id');
        $this->createFK('deal','company_id');
        $this->createFK('deal_position','company_id');
        $this->createFK('invoice','company_id');
        $this->createFK('product','company_id');
        $this->createFK('section','company_id');
        $this->createFK('user','company_id');
        $this->createFK('vat','company_id');
    }

    public function safeDown()
    {
        $this->dropFK('catalog','company_id');
        $this->dropFK('company','company_id');
        $this->dropFK('contact','company_id');
        $this->dropFK('deal','company_id');
        $this->dropFK('deal_position','company_id');
        $this->dropFK('invoice','company_id');
        $this->dropFK('product','company_id');
        $this->dropFK('section','company_id');
        $this->dropFK('user','company_id');
        $this->dropFK('vat','company_id');
    }

    protected static function tableName(string $table, bool $local = true): string
    {
        if ($local === true) {
            $table = 'bitrix24_' . $table;
        }
        return '{{%' . $table . '}}';
    }

    protected static function keyName(string $table, string $column): string
    {
        return 'bitrix24_' . $table . '_' . $column;
    }

    protected function dropFK(string $table, string $column)
    {
        $this->dropForeignKey(self::keyName($table, $column), self::tableName($table));
    }

    /**
     * @param string      $table
     * @param string      $column
     * @param string|null $subColumn
     */
    protected function createFK(string $table, string $column, string $subColumn = null)
    {
        $this->addForeignKey(
            self::keyName($table, $column), self::tableName($table), ($subColumn === null ? $column : [$subColumn, $column]),
            self::tableName(substr($column, 0, -3), $subColumn !== null), ($subColumn === null ? 'id' : [$subColumn, 'id']),
            'CASCADE', 'CASCADE'
        );
    }

}
