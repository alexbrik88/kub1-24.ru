<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190612_091707_create_file_upload_type extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('file_upload_type', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ], $tableOptions);

        $this->batchInsert('file_upload_type', ['id', 'name',], [
            [1, 'С компьютера'],
            [2, 'Моб. прилож-е'],
            [3, 'По e-mail']
        ]);
    }

    public function down()
    {
        $this->dropTable('file_upload_type');
    }
}
