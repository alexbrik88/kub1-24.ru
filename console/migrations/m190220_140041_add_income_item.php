<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190220_140041_add_income_item extends Migration
{
    public $tInvoiceIncomeItem = 'invoice_income_item';
    public $tCashBank = 'cash_bank_flows';
    public $tCashOrder = 'cash_order_flows';
    public $tCashEmoney = 'cash_emoney_flows';

    public function safeUp()
    {
        $this->insert($this->tInvoiceIncomeItem, [
            'id' => 15,
            'name' => 'Баланс начальный',
            'sort' => 100,
        ]);

        $this->execute("
            INSERT INTO 
                {{%income_item_flow_of_funds}} (
                	[[company_id]],
                	[[name]],
                	[[income_item_id]]
                )
            SELECT
                {{%company}}.[[id]],
                'Баланс начальный' as name,
                15 as income_item_id
            FROM 
                {{%company}}
        ");

        $this->update($this->tCashBank, ['income_item_id' => 15], ['contractor_id' => 'balance']);
        $this->update($this->tCashOrder, ['income_item_id' => 15], ['contractor_id' => 'balance']);
        $this->update($this->tCashEmoney, ['income_item_id' => 15], ['contractor_id' => 'balance']);
    }

    public function safeDown()
    {
        $this->update($this->tCashBank, ['income_item_id' => 7], ['contractor_id' => 'balance']);
        $this->update($this->tCashOrder, ['income_item_id' => 7], ['contractor_id' => 'balance']);
        $this->update($this->tCashEmoney, ['income_item_id' => 7], ['contractor_id' => 'balance']);
        $this->delete('{{%income_item_flow_of_funds}}', ['income_item_id' => 15]);
        $this->delete($this->tInvoiceIncomeItem, ['id' => 15]);
    }
}
