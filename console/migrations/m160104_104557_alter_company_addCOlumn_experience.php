<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160104_104557_alter_company_addCOlumn_experience extends Migration
{
    public $tCompany = 'company';
    public $tExperience = 'company_experience';

    public function safeUp()
    {
        $this->addColumn($this->tCompany, 'experience_id', $this->integer()->notNull()->defaultValue(1 /*novice*/) . ' AFTER is_novice');
        $this->update($this->tCompany, [
            'experience_id' => 1, /*novice*/
        ], [
            'is_novice' => 1,
        ]);
        $this->update($this->tCompany, [
            'experience_id' => 2, /*current*/
        ], [
            'is_novice' => 0,
        ]);
        $this->addForeignKey('fk_company_to_experience', $this->tCompany, 'experience_id', $this->tExperience, 'id');

        $this->dropColumn($this->tCompany, 'is_novice');
    }
    
    public function safeDown()
    {
        $this->addColumn($this->tCompany, 'is_novice', $this->boolean()->notNull()->defaultValue(1) . ' AFTER `experience_id`');

        $this->update($this->tCompany, [
            'is_novice' => 1,
        ], [
            'experience_id' => 1, /*novice*/
        ]);
        $this->update($this->tCompany, [
            'is_novice' => 0,
        ], [
            'experience_id' => 2, /*current*/
        ]);

        $this->dropForeignKey('fk_company_to_experience', $this->tCompany);
        $this->dropColumn($this->tCompany, 'experience_id');
    }
}


