<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210601_070628_insert_product_turnover extends Migration
{
    public function up()
    {
        $this->execute('DROP PROCEDURE IF EXISTS insert_product_turnover_FROM_ofd_receipt_item');

        $this->execute('
            CREATE PROCEDURE insert_product_turnover_FROM_ofd_receipt_item()
            BEGIN
            DECLARE done INT DEFAULT 0;
            DECLARE i BIGINT;
            DECLARE cursor_i CURSOR FOR
                SELECT `id`
                FROM `ofd_receipt_item`
                WHERE `product_id` IS NOT NULL AND `product_type` IN(1,2,3,4);
            DECLARE CONTINUE HANDLER FOR SQLSTATE "02000" SET done = 1;

            OPEN cursor_i;

            REPEAT
                FETCH cursor_i INTO i;
                IF NOT done THEN
                    INSERT INTO `product_turnover` (
                        `order_id`,
                        `document_id`,
                        `document_table`,
                        `invoice_id`,
                        `contractor_id`,
                        `company_id`,
                        `date`,
                        `type`,
                        `production_type`,
                        `is_invoice_actual`,
                        `is_document_actual`,
                        `purchase_price`,
                        `price_one`,
                        `quantity`,
                        `total_amount`,
                        `purchase_amount`,
                        `margin`,
                        `year`,
                        `month`,
                        `product_group_id`,
                        `product_id`,
                        `not_for_sale`
                    ) SELECT
                        `ri`.`id`,
                        `ri`.`receipt_id`,
                        "ofd_receipt",
                        null,
                        null,
                        `ri`.`company_id`,
                        DATE(`ri`.`date_time`),
                        2,
                        IFNULL(`pr`.`production_type`, 1),
                        1,
                        1,
                        IFNULL(`pr`.`price_for_buy_with_nds`, 0),
                        IFNULL(`ri`.`price`, 0),
                        IFNULL(`ri`.`quantity`, 0),
                        ROUND(IFNULL(`ri`.`price`, 0) * IFNULL(`ri`.`quantity`, 0), 0),
                        ROUND(IFNULL(`ri`.`quantity`, 0) * IFNULL(`pr`.`price_for_buy_with_nds`, 0)),
                        (ROUND(IFNULL(`ri`.`price`, 0) * IFNULL(`ri`.`quantity`, 0), 0) - ROUND(IFNULL(`ri`.`quantity`, 0) * IFNULL(`pr`.`price_for_buy_with_nds`, 0))),
                        YEAR(DATE(`ri`.`date_time`)),
                        MONTH(DATE(`ri`.`date_time`)),
                        IFNULL(`pr`.`group_id`, 1),
                        `pr`.`id`,
                        0
                    FROM `ofd_receipt_item` `ri`
                    LEFT JOIN `product` `pr` ON `pr`.`id` = `ri`.`product_id`
                    WHERE `ri`.`id` = i
                    ON DUPLICATE KEY UPDATE
                        `date` = VALUES(`date`),
                        `type` = VALUES(`type`),
                        `production_type` = VALUES(`production_type`),
                        `price_one` = VALUES(`price_one`),
                        `quantity` = VALUES(`quantity`),
                        `total_amount` = VALUES(`total_amount`),
                        `purchase_amount` = VALUES(`purchase_amount`),
                        `margin` = VALUES(`margin`),
                        `year` = VALUES(`year`),
                        `month` = VALUES(`month`),
                        `product_group_id` = VALUES(`product_group_id`),
                        `product_id` = VALUES(`product_id`);

                    END IF;
                UNTIL done = 1 END REPEAT;

                CLOSE cursor_i;
            END
        ');

        $this->execute('CALL insert_product_turnover_FROM_ofd_receipt_item()');

        $this->execute('DROP PROCEDURE IF EXISTS insert_product_turnover_FROM_ofd_receipt_item');
    }

    public function down()
    {
        //
    }
}
