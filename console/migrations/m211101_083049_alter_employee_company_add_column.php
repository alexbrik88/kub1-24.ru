<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211101_083049_alter_employee_company_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%employee_company}}', 'can_product_write_off', $this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%employee_company}}', 'can_product_write_off');
    }
}
