<?php

use console\components\db\Migration;

class m200109_174610_evotor_device_company_id_create_fk extends Migration
{
    const TABLE = '{{%evotor_device}}';

    public function up()
    {
        $this->addForeignKey(
            'evotor_device_company_id', self::TABLE, 'company_id',
            '{{%company}}', 'id',
            'CASCADE', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey('evotor_device_company_id', self::TABLE);
    }
}
