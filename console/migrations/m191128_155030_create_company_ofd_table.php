<?php

use yii\db\Migration;

class m191128_155030_create_company_ofd_table extends Migration
{
    public $table = 'ofd_type';
    public $relTable = 'company_to_ofd_type';

    public function safeUp()
    {
        $companyTable = 'company';

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        // ofd types
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer(),
            'name' => $this->string(255)->notNull(),
            'sort' => $this->smallInteger()->defaultValue(100),
        ], $tableOptions);

        $this->batchInsert($this->table, ['id', 'name', 'sort'], [
            [1, "Вымпел-Коммуникации", 1],
            [2, "Калуга Астрал", 1],
            [3, "Мультикарта", 1],
            [4, "Первый ОФД", 1],
            [5, "Петер-Сервис Спецтехнологии (OFD.RU)", 1],
            [6, "Платформа ОФД", 1],
            [7, "СКБ Контур", 1],
            [8, "Такском", 1],
            [9, "Тандер", 1],
            [10, "Тензор", 1],
            [11, "ЭнвижнГруп", 1],
            [12, "Яндекс.ОФД", 1],
            [100, "Ярус", 1],
        ]);

        $this->execute("UPDATE `{$this->table}` set `id` = 13 WHERE `id` = 100");
        $this->addForeignKey("FK_{$this->table}_{$companyTable}", $this->table, 'company_id', $companyTable, 'id', 'CASCADE');

        $this->createTable($this->relTable, [
            'company_id' => $this->integer()->notNull(),
            'ofd_id' => $this->integer()->notNull()
        ], $tableOptions);
        $this->addPrimaryKey('PRIMARY_KEY', $this->relTable, ['company_id', 'ofd_id']);
        $this->addForeignKey("FK_{$this->relTable}_{$companyTable}", $this->relTable, 'company_id', $companyTable, 'id', 'CASCADE');
        $this->addForeignKey("FK_{$this->relTable}_{$this->table}", $this->relTable, 'ofd_id', $this->table, 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $companyTable = 'company';

        $this->dropForeignKey("FK_{$this->table}_{$companyTable}", $this->table);
        $this->dropForeignKey("FK_{$this->relTable}_{$companyTable}", $this->relTable);
        $this->dropForeignKey("FK_{$this->relTable}_{$this->table}", $this->relTable);

        $this->dropTable($this->table);
        $this->dropTable($this->relTable);
    }
}
