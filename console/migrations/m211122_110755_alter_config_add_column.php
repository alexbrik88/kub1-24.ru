<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211122_110755_alter_config_add_column extends Migration
{
    public $tableName = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'order_document_supplier_document_date', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'order_document_shipped', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn($this->tableName, 'order_document_reserve', $this->boolean()->notNull()->defaultValue(true));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'order_document_supplier_document_date');
        $this->dropColumn($this->tableName, 'order_document_shipped');
        $this->dropColumn($this->tableName, 'order_document_reserve');
    }
}
