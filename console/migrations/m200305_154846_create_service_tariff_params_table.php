<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%service_tariff_params}}`.
 */
class m200305_154846_create_service_tariff_params_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%service_tariff_params}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);

        $this->batchInsert('{{%service_tariff_params}}', [
            'id',
            'name',
        ], [
            [1, 'Кол-во товаров в прайс-листе'],
            [2, 'Онлайн обновление цены и остатков'],
            [3, 'Просмотр карточки товара'],
            [4, 'Онлайн оформление заказов'],
            [5, 'Получение уведомлений об открытии'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%service_tariff_params}}');
    }
}
