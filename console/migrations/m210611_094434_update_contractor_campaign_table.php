<?php

use yii\db\Migration;

class m210611_094434_update_contractor_campaign_table extends Migration
{
    /**
     * @inheritDoc
     */
    public function safeUp(): void
    {
        $this->delete('{{%contractor_campaign}}', ['name' => '']);
    }
}
