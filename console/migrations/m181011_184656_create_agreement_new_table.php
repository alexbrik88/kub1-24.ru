<?php

use yii\db\Migration;

/**
 * Handles the creation of table `agreement_new`.
 */
class m181011_184656_create_agreement_new_table extends Migration
{
    public function up()
    {
        $this->createTable('{{%agreement_new}}', [
            'id' => $this->primaryKey(),
			'agreement_template_id' => $this->integer()->notNull(),			
			'contractor_id' => $this->integer()->notNull(),
            'company_id' => $this->integer()->notNull(),
			'company_rs' => $this->string(20),
			'payment_limit_date' => $this->date()->notNull(),			
            'type' => $this->integer()->notNull(),
            'document_number' => $this->string(255)->notNull(),
			'document_additional_number' => $this->string(255),
			'document_date' => $this->date()->notNull(),
            'document_name' => $this->string(255)->notNull(),
			'document_header' => $this->text(),
			'document_body' => $this->text(),
			'document_requisites_customer' => $this->text(),
			'document_requisites_executer' => $this->text(),
			'payment_delay' => $this->integer(),
            'created_at' => $this->integer(),
            'created_by' => $this->integer(),
			'status' => $this->integer()->defaultValue(1),
			'comment_internal' => $this->string()
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%agreement_new}}');
    }
}
