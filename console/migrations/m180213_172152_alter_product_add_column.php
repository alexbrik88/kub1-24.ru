<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180213_172152_alter_product_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('product', 'provider_id', $this->integer());
        $this->addColumn('product', 'comment', $this->string(500));

        $this->addForeignKey ('FK_product_contractor', 'product', 'provider_id', 'contractor', 'id');
    }

    public function safeDown()
    {
        $this->dropColumn('product', 'provider_id');
        $this->dropColumn('product', 'comment');
    }
}
