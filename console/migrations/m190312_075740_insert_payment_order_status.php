<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190312_075740_insert_payment_order_status extends Migration
{
    public function safeUp()
    {
        $this->insert('payment_order_status', [
            'id' => 5,
            'name' => 'Оплачено',
        ]);
    }

    public function safeDown()
    {
        $this->delete('payment_order_status', [
            'id' => 5,
        ]);
    }
}
