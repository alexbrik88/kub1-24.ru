<?php

use yii\db\Schema;
use yii\db\Migration;

class m150714_104124_alter_flows_addColumns_fundingType extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cash_bank_flows', 'is_funding_flow', Schema::TYPE_BOOLEAN);
        $this->addColumn('cash_bank_flows', 'cash_funding_type_id', Schema::TYPE_INTEGER . ' DEFAULT NULL');
        $this->addForeignKey('fk_cash_bank_flows_to_cash_funding_type', 'cash_bank_flows', 'cash_funding_type_id', 'cash_funding_type', 'id');

        $this->addColumn('cash_emoney_flows', 'is_funding_flow', Schema::TYPE_BOOLEAN);
        $this->addColumn('cash_emoney_flows', 'cash_funding_type_id', Schema::TYPE_INTEGER . ' DEFAULT NULL');
        $this->addForeignKey('fk_cash_emoney_flows_to_cash_funding_type', 'cash_emoney_flows', 'cash_funding_type_id', 'cash_funding_type', 'id');

        $this->addColumn('cash_order_flows', 'is_funding_flow', Schema::TYPE_BOOLEAN);
        $this->addColumn('cash_order_flows', 'cash_funding_type_id', Schema::TYPE_INTEGER . ' DEFAULT NULL');
        $this->addForeignKey('fk_cash_order_flows_to_cash_funding_type', 'cash_order_flows', 'cash_funding_type_id', 'cash_funding_type', 'id');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('fk_cash_order_flows_to_cash_funding_type', 'cash_order_flows');
        $this->dropColumn('cash_order_flows', 'cash_funding_type_id');
        $this->dropColumn('cash_order_flows', 'is_funding_flow');

        $this->dropForeignKey('fk_cash_emoney_flows_to_cash_funding_type', 'cash_emoney_flows');
        $this->dropColumn('cash_emoney_flows', 'cash_funding_type_id');
        $this->dropColumn('cash_emoney_flows', 'is_funding_flow');

        $this->dropForeignKey('fk_cash_bank_flows_to_cash_funding_type', 'cash_bank_flows');
        $this->dropColumn('cash_bank_flows', 'cash_funding_type_id');
        $this->dropColumn('cash_bank_flows', 'is_funding_flow');
    }
}
