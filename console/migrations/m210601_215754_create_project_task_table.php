<?php

use yii\db\Migration;

class m210601_215754_create_project_task_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%project_task}}';

    /**
     * @inheritDoc
     */
    public function up(): void
    {
        $this->createTable(self::TABLE_NAME, [
            'task_id' => $this->bigPrimaryKey()->notNull(),
            'task_number' => $this->bigInteger()->notNull()->defaultValue(0),
            'task_name' => $this->string(64)->notNull()->defaultValue(''),
            'company_id' => $this->integer()->notNull(),
            'employee_id' => $this->integer()->notNull(),
            'project_id' => $this->integer()->notNull(),
            'description' => $this->text()->notNull(),
            'status' => $this->tinyInteger(1)->notNull()->defaultValue(1),
            'date_begin' => $this->date()->notNull(),
            'date_end' => $this->date()->null(),
            'time' => $this->time()->notNull(),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->createIndex('ck_employee_id__company_id', self::TABLE_NAME, ['employee_id', 'company_id']);
        $this->createIndex('ck_project_id', self::TABLE_NAME, ['project_id']);
        $this->createIndex('ck_task_number', self::TABLE_NAME, ['task_number']);
        $this->createIndex('ck_task_name', self::TABLE_NAME, ['task_name']);
        $this->createIndex('ck_status', self::TABLE_NAME, ['status']);
        $this->createIndex('ck_date_begin', self::TABLE_NAME, ['date_begin']);
        $this->createIndex('ck_date_end', self::TABLE_NAME, ['date_end']);

        $this->addForeignKey(
            "fk_project_task__employee_company",
            self::TABLE_NAME,
            ['employee_id', 'company_id'],
            '{{%employee_company}}',
            ['employee_id', 'company_id'],
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            "fk_project_task__project",
            self::TABLE_NAME,
            ['project_id'],
            '{{%project}}',
            ['id'],
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritDoc
     */
    public function down(): void
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
