<?php

use yii\db\Migration;

class m190129_094721_alter_service_subscribe_tariff_add_columns extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn('service_subscribe_tariff', 'is_active', $this->boolean()->notNull()->defaultValue(true));

        $this->createIndex('is_active', 'service_subscribe_tariff', 'is_active');

        $this->addColumn('service_subscribe_tariff', 'proposition', $this->string());

        $this->update('service_subscribe_tariff', [
            'proposition' => 'Скидка 10% при оплате за 4 месяца',
        ], ['id' => 1]);

        $this->update('service_subscribe_tariff', [
            'proposition' => 'Скидка 20% при оплате за год',
        ], ['id' => 2]);

        $this->update('service_subscribe_tariff', [
            'proposition' => 'Лучшая цена!',
        ], ['id' => 3]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropIndex('is_active', 'service_subscribe_tariff');
        $this->dropColumn('service_subscribe_tariff', 'is_active');
        $this->dropColumn('service_subscribe_tariff', 'proposition');
    }
}
