<?php

use yii\db\Migration;

class m210521_063737_alter_crm_client_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%crm_client}}';

    /**
     * @inheritDoc
     */
    public function up(): void
    {
        $this->renameColumn(self::TABLE_NAME, 'messenger_account', 'skype_account');
    }

    /**
     * @inheritDoc
     */
    public function down(): void
    {
        $this->renameColumn(self::TABLE_NAME, 'skype_account', 'messenger_account');
    }
}
