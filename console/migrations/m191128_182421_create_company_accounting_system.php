<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191128_182421_create_company_accounting_system extends Migration
{
    public $table = 'accounting_system_type';
    public $relTable = 'company_to_accounting_system_type';

    public function safeUp()
    {
        $companyTable = 'company';

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        // accounting system types
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer(),
            'name' => $this->string(255)->notNull(),
            'sort' => $this->smallInteger()->defaultValue(100),
        ], $tableOptions);

        $this->batchInsert($this->table, ['id', 'name', 'sort'], [
            [1, "1С Бухгалтерия", 1],
            [100, "1C Управление Торговлей", 1],
        ]);

        $this->execute("UPDATE `{$this->table}` set `id` = 2 WHERE `id` = 100");
        $this->addForeignKey("FK_{$this->table}_{$companyTable}", $this->table, 'company_id', $companyTable, 'id', 'CASCADE');

        $this->createTable($this->relTable, [
            'company_id' => $this->integer()->notNull(),
            'accounting_system_id' => $this->integer()->notNull()
        ], $tableOptions);
        $this->addPrimaryKey('PRIMARY_KEY', $this->relTable, ['company_id', 'accounting_system_id']);
        $this->addForeignKey("FK_{$this->relTable}_{$companyTable}", $this->relTable, 'company_id', $companyTable, 'id', 'CASCADE');
        $this->addForeignKey("FK_{$this->relTable}_{$this->table}", $this->relTable, 'accounting_system_id', $this->table, 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $companyTable = 'company';

        $this->dropForeignKey("FK_{$this->table}_{$companyTable}", $this->table);
        $this->dropForeignKey("FK_{$this->relTable}_{$companyTable}", $this->relTable);
        $this->dropForeignKey("FK_{$this->relTable}_{$this->table}", $this->relTable);

        $this->dropTable($this->table);
        $this->dropTable($this->relTable);
    }
}
