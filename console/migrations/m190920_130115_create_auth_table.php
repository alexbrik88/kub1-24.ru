<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%auth}}`.
 */
class m190920_130115_create_auth_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%auth}}', [
            'id' => $this->primaryKey(),
            'provider' => $this->string()->notNull(),
            'user_uid' => $this->string()->notNull(),
            'user_data' => $this->text()->notNull(),
            'employee_id' => $this->integer(),
            'created_at' => $this->integer(),
        ]);

        $this->addForeignKey(
            'fk-auth-employee',
            '{{%auth}}',
            'employee_id',
            '{{%employee}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex(
            'auth_user',
            '{{%auth}}',
            ['provider', 'user_uid'],
            true
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%auth}}');
    }
}
