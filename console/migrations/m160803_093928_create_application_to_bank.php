<?php

use yii\db\Migration;

/**
 * Handles the creation for table `application_to_bank`.
 */
class m160803_093928_create_application_to_bank extends Migration
{
    public $tApplicationToBank = 'application_to_bank';
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->tApplicationToBank, [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'bank_id' => $this->integer()->notNull(),
            'company_name' => $this->string()->notNull(),
            'inn' => $this->string(45)->notNull(),
            'legal_address' => $this->string()->notNull(),
            'fio' => $this->string()->notNull(),
            'contact_phone' => $this->string(45)->notNull(),
        ]);

        $this->addForeignKey($this->tApplicationToBank . '_company_id', $this->tApplicationToBank, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tApplicationToBank . '_bank_id', $this->tApplicationToBank, 'bank_id', 'bank', 'id', 'RESTRICT', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey($this->tApplicationToBank . '_company_id', $this->tApplicationToBank);
        $this->dropForeignKey($this->tApplicationToBank . '_bank_id', $this->tApplicationToBank);

        $this->dropTable($this->tApplicationToBank);
    }
}
