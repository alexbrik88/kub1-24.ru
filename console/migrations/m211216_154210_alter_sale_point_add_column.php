<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211216_154210_alter_sale_point_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%sale_point}}', 'ofd_store_uid', $this->string()->null()->defaultValue(null));

        $this->createIndex('company_id__ofd_store_uid', '{{%sale_point}}', ['company_id', 'ofd_store_uid'], true);
    }

    public function safeDown()
    {
        $this->dropIndex('company_id__ofd_store_uid', '{{%sale_point}}');

        $this->dropColumn('{{%sale_point}}', 'ofd_store_uid');
    }
}
