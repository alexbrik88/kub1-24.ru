<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210917_183621_alter_olap_flows_add_column extends Migration
{
    const TABLE_OLAP_FLOWS = 'olap_flows';

    const WALLET_BANK = 1;
    const WALLET_CASHBOX = 2;
    const WALLET_EMONEY = 3;
    const WALLET_ACQUIRING = 4;
    const WALLET_CARD = 5;

    const TABLE_BANK = 'cash_bank_flows';
    const TABLE_CASHBOX = 'cash_order_flows';
    const TABLE_EMONEY = 'cash_emoney_flows';
    const TABLE_ACQUIRING = 'acquiring_operation';
    const TABLE_CARD = 'card_operation';

    public function safeUp()
    {
        $this->addColumn('olap_flows', 'tin_child_amount', $this->bigInteger()->after('currency_name'));
        $this->addColumn('olap_flows', 'has_tin_children', $this->tinyInteger(1)->notNull()->defaultValue(0)->after('currency_name'));
        $this->addColumn('olap_flows', 'has_tin_parent', $this->tinyInteger(1)->notNull()->defaultValue(0)->after('currency_name'));

        $this->execute("
            UPDATE `olap_flows` o
            SET o.has_tin_children = 1 
            WHERE o.wallet = 1 AND o.id IN (SELECT DISTINCT `parent_id` FROM `cash_bank_flows` WHERE `parent_id` IS NOT NULL) 
        ");

        $this->execute("
            UPDATE `olap_flows` o
            LEFT JOIN `cash_bank_flows` cbf ON o.wallet = 1 AND o.id = cbf.id 
            SET 
              o.tin_child_amount = cbf.tin_child_amount,
              o.has_tin_parent = 1
            WHERE cbf.tin_child_amount IS NOT NULL 
        ");
    }

    public function safeDown()
    {
        $this->dropColumn('olap_flows', 'tin_child_amount');
        $this->dropColumn('olap_flows', 'has_tin_children');
        $this->dropColumn('olap_flows', 'has_tin_parent');
    }
}
