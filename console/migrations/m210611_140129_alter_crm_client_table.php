<?php

use yii\db\Migration;

class m210611_140129_alter_crm_client_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%crm_client}}';

    /**
     * @inheritDoc
     */
    public function up(): void
    {
        $this->addColumn(self::TABLE_NAME, 'is_crm_created', $this->boolean()->defaultValue(false)->after('event_type'));
        $this->update(self::TABLE_NAME, ['is_crm_created' => true]);
    }

    /**
     * @inheritDoc
     */
    public function down(): void
    {
        $this->dropColumn(self::TABLE_NAME, 'is_crm_created');
    }
}
