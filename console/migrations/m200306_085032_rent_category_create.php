<?php

use console\components\db\Migration;
use common\models\rent\Category;

class m200306_085032_rent_category_create extends Migration
{
    const TABLE = '{{%rent_category}}';

    public function safeUp()
    {
        $this->createTable(self::TABLE, [
            'id' => 'SMALLINT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT',
            'title' => $this->string(50)->notNull()->comment('Название')
        ], "COMMENT 'Учёт аренды техники: категории арендуемых объектов'");
        $this->batchInsert(self::TABLE, ['id', 'title'], [
            [Category::ID_REALTY, 'недвижимость'],
            [Category::ID_TECH, 'спец. техника'],
            [Category::ID_DEVICE, 'оборудование']
        ]);
    }

    public function down()
    {
        $this->dropTable(self::TABLE);
    }

}
