<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180501_212016_create_payment_reminder_message extends Migration
{
    public $tPaymentReminderMessage = 'payment_reminder_message';
    public $tPaymentReminderMessageCategory = 'payment_reminder_message_category';

    public function safeUp()
    {
        $this->createTable($this->tPaymentReminderMessageCategory, [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);

        $this->batchInsert($this->tPaymentReminderMessageCategory, ['id', 'name'], [
            [1, 'Работа с должниками'],
            [2, 'Счет оплачен'],
        ]);

        $this->createTable($this->tPaymentReminderMessage, [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'number' => $this->integer()->notNull(),
            'category_id' => $this->integer()->notNull(),
            'event_header' => $this->string()->defaultValue(null),
            'event_body' => $this->text()->defaultValue(null),
            'days' => $this->integer()->defaultValue(null),
            'message_template_subject' => $this->string()->notNull(),
            'message_template_body' => $this->text()->notNull(),
            'status' => $this->boolean()->defaultValue(0),
            'send_for_chief' => $this->boolean()->defaultValue(true),
            'send_for_contact' => $this->boolean()->defaultValue(false),
            'send_for_all' => $this->boolean()->defaultValue(false),
            'time' => $this->time()->defaultValue(null),
            'for_email_list' => $this->text()->defaultValue(null),
            'debt_sum' => $this->string()->defaultValue(null),
            'not_send_where_order_payment' => $this->boolean()->defaultValue(false),
            'not_send_where_emoney_payment' => $this->boolean()->defaultValue(false),
            'not_send_where_partial_payment' => $this->boolean()->defaultValue(false),
        ]);

        $this->addForeignKey($this->tPaymentReminderMessage . '_company_id', $this->tPaymentReminderMessage, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tPaymentReminderMessage . '_category_id', $this->tPaymentReminderMessage, 'category_id', $this->tPaymentReminderMessageCategory, 'id', 'RESTRICT', 'CASCADE');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey($this->tPaymentReminderMessage . '_company_id', $this->tPaymentReminderMessage);
        $this->dropForeignKey($this->tPaymentReminderMessage . '_category_id', $this->tPaymentReminderMessage);

        $this->dropTable($this->tPaymentReminderMessageCategory);
        $this->dropTable($this->tPaymentReminderMessage);
    }
}


