<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170508_165149_create_agreementType extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%agreement_type}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50)->notNull(),
        ]);

        $this->batchInsert('{{%agreement_type}}', ['id', 'name'], [
            [1, 'Договор'],
            [2, 'Заказ'],
            [3, 'Спецификация'],
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%agreement_type}}');
    }
}
