<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180820_141911_create_email_template extends Migration
{
    public $tableName = 'email_template';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'text' => $this->text()->defaultValue(null),
            'status' => $this->boolean()->defaultValue(false),
        ]);

        $this->addForeignKey($this->tableName . '_company_id', $this->tableName, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_company_id', $this->tableName);

        $this->dropTable($this->tableName);
    }
}


