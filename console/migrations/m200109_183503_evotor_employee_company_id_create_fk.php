<?php

use console\components\db\Migration;

class m200109_183503_evotor_employee_company_id_create_fk extends Migration
{
    const TABLE = '{{%evotor_employee}}';

    public function up()
    {
        $this->addForeignKey(
            'evotor_employee_company_id', self::TABLE, 'company_id',
            '{{%company}}', 'id',
            'CASCADE', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey('evotor_employee_company_id', self::TABLE);
    }
}
