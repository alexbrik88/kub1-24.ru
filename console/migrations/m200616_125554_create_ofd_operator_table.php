<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%ofd_operator}}`.
 */
class m200616_125554_create_ofd_operator_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%ofd_operator}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull()->comment('1021 Кассир'),
        ]);

        $this->addForeignKey('FK_ofd_operator_company', '{{%ofd_operator}}', 'company_id', '{{%company}}', 'id');

        $this->createIndex('operator', '{{%ofd_operator}}', [
            'company_id',
            'name',
        ], true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%ofd_operator}}');
    }
}
