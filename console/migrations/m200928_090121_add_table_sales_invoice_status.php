<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200928_090121_add_table_sales_invoice_status extends Migration
{
    public $tableName = 'sales_invoice_status';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);

        $this->batchInsert('sales_invoice_status', ['id', 'name',], [
            [1, 'Создана',],
            [2, 'Распечатана',],
            [3, 'Передана',],
            [4, 'Получена-подписана',],
        ]);

    }

    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
