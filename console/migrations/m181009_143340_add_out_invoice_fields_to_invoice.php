<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181009_143340_add_out_invoice_fields_to_invoice extends Migration
{
    public $tableName = 'invoice';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'out_view_count', $this->integer()->defaultValue(0));
        $this->addColumn($this->tableName, 'out_save_count', $this->integer()->defaultValue(0));
        $this->addColumn($this->tableName, 'out_pay_count', $this->integer()->defaultValue(0));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'out_view_count');
        $this->dropColumn($this->tableName, 'out_save_count');
        $this->dropColumn($this->tableName, 'out_pay_count');
    }
}


