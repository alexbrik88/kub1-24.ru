<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201120_054008_alter_config_add_columns extends Migration
{
    public $table = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->table, 'table_view_finance_model', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table, 'finance_model_revenue', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table, 'finance_model_net_profit', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table, 'finance_model_margin', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table, 'finance_model_sales_profit', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table, 'finance_model_capital_profit', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table, 'finance_model_comment', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 1');
        $this->addColumn($this->table, 'finance_model_employee', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 1');
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'table_view_finance_model');
        $this->dropColumn($this->table, 'finance_model_revenue');
        $this->dropColumn($this->table, 'finance_model_net_profit');
        $this->dropColumn($this->table, 'finance_model_margin');
        $this->dropColumn($this->table, 'finance_model_sales_profit');
        $this->dropColumn($this->table, 'finance_model_capital_profit');
        $this->dropColumn($this->table, 'finance_model_comment');
        $this->dropColumn($this->table, 'finance_model_employee');
    }
}
