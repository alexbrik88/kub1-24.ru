<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191023_113055_create_table_module_visit extends Migration
{
    public function safeUp()
    {
        Yii::$app->db->createCommand('SET session wait_timeout=6000')->execute();
        Yii::$app->db->createCommand('SET session interactive_timeout=6000')->execute();

        $this->createTable('service_module_visit', [
            //'id' => $this->primaryKey(),
            'module_id' => $this->integer()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'date' => $this->date()->notNull(),
        ]);

        $this->addPrimaryKey('service_module_visit_PK', 'service_module_visit', ['date', 'module_id', 'company_id']);
        $this->addForeignKey('service_module_visit' . '_module_id', 'service_module_visit', 'module_id', 'service_module', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('service_module_visit' . '_company_id', 'service_module_visit', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('service_module_visit' . '_company_id', 'service_module_visit');
        $this->dropForeignKey('service_module_visit' . '_module_id', 'service_module_visit');
        $this->dropTable('service_module_visit');
    }
}
