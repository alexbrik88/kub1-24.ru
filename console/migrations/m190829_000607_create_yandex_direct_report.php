<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190829_000607_create_yandex_direct_report extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%yandex_direct_report}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'employee_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'date' => $this->date()->notNull(),
            'campaign_id' => $this->integer()->notNull(),
            'campaign_name' => $this->string()->notNull(),
            'impressions_count' => $this->integer()->notNull(),
            'clicks_count' => $this->integer()->notNull(),
            'ctr' => $this->float(2)->notNull(),
            'cost' => $this->float(2)->notNull(),
            'avg_cpc' => $this->float(2)->notNull(),
            'avg_page_views_count' => $this->float(2)->notNull(),
            'conversion_rate' => $this->float(2)->defaultValue(null),
            'revenue' => $this->float(2)->notNull(),
            'conversions_count' => $this->integer()->defaultValue(null),
            'goals_roi' => $this->float(2)->defaultValue(null),
            'profit' => $this->float(2)->notNull(),
        ]);

        $this->addForeignKey('FK_yandex_direct_report_to_company', '{{%yandex_direct_report}}', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_yandex_direct_report_to_employee', '{{%yandex_direct_report}}', 'employee_id', 'employee', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable('{{%yandex_direct_report}}');
    }
}
