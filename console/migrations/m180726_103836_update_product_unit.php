<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180726_103836_update_product_unit extends Migration
{
    public $tableName = 'product_unit';

    public function safeUp()
    {
        $this->update($this->tableName, ['services' => 1], ['id' => \common\models\product\ProductUnit::UNIT_KG]);
    }
    
    public function safeDown()
    {
        $this->update($this->tableName, ['services' => 0], ['id' => \common\models\product\ProductUnit::UNIT_KG]);
    }
}


