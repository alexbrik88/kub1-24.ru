<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180913_102104_add_contractor_address_to_invoice_facture extends Migration
{
    public $tableName = 'invoice_facture';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'contractor_address', $this->boolean()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'contractor_address');
    }
}


