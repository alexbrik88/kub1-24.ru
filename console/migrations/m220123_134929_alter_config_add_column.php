<?php

use console\components\db\Migration;
use yii\db\Schema;

class m220123_134929_alter_config_add_column extends Migration
{
    public $table = 'user_config';
    
    public function safeUp()
    {
        $this->addColumn($this->table, 'project_money_income_expense', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table, 'project_money_recognition_date', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table, 'project_money_paying', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table, 'project_money_project', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table, 'project_money_sale_point', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table, 'project_money_industry', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'project_money_income_expense');
        $this->dropColumn($this->table, 'project_money_recognition_date');
        $this->dropColumn($this->table, 'project_money_paying');
        $this->dropColumn($this->table, 'project_money_project');
        $this->dropColumn($this->table, 'project_money_sale_point');
        $this->dropColumn($this->table, 'project_money_industry');
    }
}
