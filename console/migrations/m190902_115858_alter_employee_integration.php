<?php

use console\components\db\Migration;
use common\models\employee\Employee;

class m190902_115858_alter_employee_integration extends Migration
{
    public function safeUp()
    {
        $this->renameColumn('{{employee}}', 'integration_email', 'integration');
        foreach (Employee::find()->where(['not', ['integration' => null]])->all() as $item) {
            $item->integration = ['email' => $item->integration];
            $item->save(false, ['integration']);
        }
    }

    public function safeDown()
    {
        $this->renameColumn('{{employee}}', 'integration', 'integration_email');
        foreach (Employee::find()->where(['not', ['integration' => null]])->all() as $item) {
            $item->integration_email = $item->integration_email['email'];
            $item->save(false, ['integration']);
        }
    }
}
