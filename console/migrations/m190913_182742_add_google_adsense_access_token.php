<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190913_182742_add_google_adsense_access_token extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%employee}}', 'google_adsense_access_token', $this->text()->defaultValue(null));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%employee}}', 'google_adsense_access_token');
    }
}
