<?php

use console\components\db\Migration;
use common\models\evotor\Device;
use common\models\evotor\Receipt;

class m191210_115613_alter_evotor_device_uuid extends Migration
{
    public function safeUp()
    {
        Receipt::deleteAll();
        Device::deleteAll();
        $this->addColumn('{{evotor_device}}', 'uuid', $this->char(36)->notNull()->unique()->comment('UUID терминала Эвотор'));
        $this->dropColumn('{{evotor_device}}', 'id');
        $this->addColumn('{{evotor_device}}', 'id', $this->primaryKey(11)->unsigned()->notNull()->first());
        $this->alterColumn('{{evotor_receipt}}', 'device_id', $this->integer(11)->unsigned()->notNull()->comment('ID терминала (evotor_device.id)'));
        $this->addForeignKey('evotor_receipt_device_id', '{{evotor_receipt}}', 'device_id',
            '{{evotor_device}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        Receipt::deleteAll();
        Device::deleteAll();
        $this->dropColumn('{{evotor_device}}', 'uuid');
        $this->dropForeignKey('evotor_receipt_device_id', '{{evotor_receipt}}');
        $this->dropColumn('{{evotor_device}}', 'id');
        $this->addColumn('{{evotor_device}}', 'id', "BINARY(16) NOT NULL PRIMARY KEY COMMENT 'UUID Эвотора'");
        $this->alterColumn('{{evotor_receipt}}', 'device_id', "BINARY(16) NOT NULL COMMENT 'ID терминала'");
    }
}
