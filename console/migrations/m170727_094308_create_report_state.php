<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170727_094308_create_report_state extends Migration
{
    public $tableName = 'report_state';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'subscribe_id' => $this->integer()->notNull(),
            'pay_date' => $this->string()->notNull(),
            'pay_sum' => $this->string()->notNull(),
            'invoice_count' => $this->integer()->defaultValue(0),
            'act_count' => $this->integer()->defaultValue(0),
            'packing_list_count' => $this->integer()->defaultValue(0),
            'invoice_facture_count' => $this->integer()->defaultValue(0),
            'upd_count' => $this->integer()->defaultValue(0),
            'has_logo' => $this->boolean()->defaultValue(false),
            'has_print' => $this->boolean()->defaultValue(false),
            'has_signature' => $this->boolean()->defaultValue(false),
            'product_count' => $this->integer()->defaultValue(0),
            'service_count' => $this->integer()->defaultValue(0),
            'download_statement_count' => $this->integer()->defaultValue(0),
            'download_1c_count' => $this->integer()->defaultValue(0),
        ]);

        $this->addForeignKey($this->tableName . '_company_id', $this->tableName, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tableName . '_subscribe_id', $this->tableName, 'subscribe_id', 'service_subscribe', 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_company_id', $this->tableName);
        $this->dropForeignKey($this->tableName . '_subscribe_id', $this->tableName);

        $this->dropTable($this->tableName);
    }
}


