<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pattern`.
 */
class m180301_103640_create_pattern_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('pattern', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull(),
            'signup_count' => $this->integer()->notNull(),
            'payment_count' => $this->integer()->notNull(),
            'step_count' => $this->integer()->notNull(),
            'date_start' => $this->date()->notNull(),
            'date_end' => $this->date()->notNull(),
            'interval' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('pattern');
    }
}
