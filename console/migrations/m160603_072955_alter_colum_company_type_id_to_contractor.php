<?php

use console\components\db\Migration;

class m160603_072955_alter_colum_company_type_id_to_contractor extends Migration
{
    public $tContractor = 'contractor';

    public function safeUp()
    {
        $this->alterColumn($this->tContractor, 'company_type_id', $this->integer(11));
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('fk_contractor_to_company_type', $this->tContractor);
        $this->alterColumn($this->tContractor, 'company_type_id', $this->integer(11)->notNull());
        $this->addForeignKey('fk_contractor_to_company_type', $this->tContractor, 'company_type_id', 'company_type', 'id');
    }
}


