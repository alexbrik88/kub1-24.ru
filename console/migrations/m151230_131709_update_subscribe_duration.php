<?php

use console\components\db\Migration;
use yii\db\Schema;

class m151230_131709_update_subscribe_duration extends Migration
{
    public $tTariff = 'service_subscribe_tariff';
    public $tSubscribe = 'service_subscribe';

    public function safeUp()
    {
        $this->execute("
            UPDATE {$this->tSubscribe} AS subscribe
                INNER JOIN {$this->tTariff} tariff ON tariff.id = subscribe.tariff_id
                SET subscribe.duration_month = tariff.duration_month,
                    subscribe.duration_day = tariff.duration_day
        ");
    }
    
    public function safeDown()
    {
        echo 'No down migration persists' . PHP_EOL;
    }
}


