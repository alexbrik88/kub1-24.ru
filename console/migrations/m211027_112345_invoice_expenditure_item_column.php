<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211027_112345_invoice_expenditure_item_column extends Migration
{
    public $expenseItemsIds = '24, 6, 10, 11, 18, 37, 23, 30, 31, 32, 38, 12, 7, 1, 19, 8, 26';

    public function safeUp()
    {
        $this->execute("
            UPDATE `invoice_expenditure_item`
            SET `can_be_controlled` = 1
            WHERE `id` IN ({$this->expenseItemsIds})
        ");
    }

    public function safeDown()
    {
        $this->execute("
            UPDATE `invoice_expenditure_item`
            SET `can_be_controlled` = 0
            WHERE `id` IN ({$this->expenseItemsIds})
        ");
    }
}
