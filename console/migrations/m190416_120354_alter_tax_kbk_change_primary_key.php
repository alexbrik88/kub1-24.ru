<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190416_120354_alter_tax_kbk_change_primary_key extends Migration
{
    public function safeUp()
    {
        $this->dropPrimaryKey('PRIMARY_KEY', '{{%tax_kbk}}');
        $this->addPrimaryKey('PRIMARY_KEY', '{{%tax_kbk}}', ['id', 'expenditure_item_id']);
    }

    public function safeDown()
    {
        $this->dropPrimaryKey('PRIMARY_KEY', '{{%tax_kbk}}');
        $this->addPrimaryKey('PRIMARY_KEY', '{{%tax_kbk}}', 'id');
    }
}
