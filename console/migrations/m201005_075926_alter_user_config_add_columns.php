<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201005_075926_alter_user_config_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_config','report_break_even_help',$this->boolean()->notNull()->defaultValue(false));
        $this->addColumn('user_config','report_break_even_chart',$this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('user_config','report_break_even_help');
        $this->dropColumn('user_config','report_break_even_chart');
    }
}
