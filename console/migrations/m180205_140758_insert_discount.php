<?php

use common\components\DbTimeZone;
use common\models\Company;
use common\models\Discount;
use common\models\DiscountType;
use common\models\service\Subscribe;
use common\models\service\SubscribeStatus;
use console\components\db\Migration;
use yii\db\Schema;

class m180205_140758_insert_discount extends Migration
{
    public function safeUp()
    {
        date_default_timezone_set('Europe/Moscow');
        DbTimeZone::sync();
        foreach (range(14, 19) as $days) {
            $date = new DateTime("today -$days days");
            $from = $date->setTime(0, 0, 0)->getTimestamp();
            $till = $date->setTime(23, 59, 59)->getTimestamp();
            $query = Company::find()
                ->alias('company')
                ->addSelect(['company.*', 'MAX({{activated}}.[[expired_at]]) [[expired]]'])
                ->innerJoin(
                    Subscribe::tableName() . ' activated',
                    '{{company}}.[[id]] = {{activated}}.[[company_id]]
                    AND {{activated}}.[[status_id]] = :active
                    AND {{activated}}.[[expired_at]] >= :from',
                    [
                        ':active' => SubscribeStatus::STATUS_ACTIVATED,
                        ':from' => $from,
                    ]
                )
                ->leftJoin(
                    Subscribe::tableName() . ' payed',
                    '{{company}}.[[id]] = {{payed}}.[[company_id]] AND {{payed}}.[[status_id]] = :paid',
                    [':paid' => SubscribeStatus::STATUS_PAYED,]
                )
                ->andWhere(['payed.id' => null])
                ->groupBy('{{company}}.[[id]]')
                ->having(['between', 'expired', $from, $till]);

            $companyArray = $query->all();

            if ($companyArray) {
                $days = $days - 14;
                $dateFrom = new DateTime("today -$days days");
                $dateTill = clone $dateFrom;
                $dateTill->modify('+5 days')->setTime(23, 59, 59);
                foreach ($companyArray as $company) {
                    Discount::createType3($company, $dateFrom, $dateTill);
                }
            }
        }
    }

    public function safeDown()
    {
        Discount::deleteAll(['type_id' => DiscountType::TYPE_3]);
    }
}
