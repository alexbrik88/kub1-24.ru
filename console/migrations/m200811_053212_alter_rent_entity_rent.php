<?php

use console\components\db\Migration;
use yii\db\Schema;
use frontend\models\Documents;

class m200811_053212_alter_rent_entity_rent extends Migration
{
    public $table = 'rent_entity_rent';

    public function safeUp()
    {
        $this->dropForeignKey('rent_entity_rent_created_by', $this->table);
        $this->renameColumn($this->table, 'created_by', 'document_author_id');
        $this->addColumn($this->table, 'type', $this->tinyInteger(1)->notNull()->defaultValue(Documents::IO_TYPE_OUT));
        $this->addForeignKey('rent_entity_rent_document_author_id', $this->table, 'document_author_id', 'employee', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('rent_entity_rent_document_author_id', $this->table);
        $this->dropColumn($this->table, 'type');
        $this->renameColumn($this->table, 'document_author_id', 'created_by');
        $this->addForeignKey('rent_entity_rent_created_by', $this->table, 'created_by', 'employee', 'id');
    }
}
