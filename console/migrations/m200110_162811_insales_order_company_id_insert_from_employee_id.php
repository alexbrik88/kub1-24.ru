<?php

use common\models\employee\Employee;
use common\models\insales\Order;
use console\components\db\Migration;

class m200110_162811_insales_order_company_id_insert_from_employee_id extends Migration
{
    const TABLE = '{{%insales_order}}';

    public function safeUp()
    {
        foreach (Order::find()->all() as $order) {
            /** @var Order $order */
            /** @noinspection PhpUndefinedFieldInspection */
            $order->company_id = Employee::findOne(['id' => $order->employee_id])->company_id;
            $order->save();
        }
    }

    public function down()
    {
        $this->update(self::TABLE, ['company_id' => 0]);
    }
}
