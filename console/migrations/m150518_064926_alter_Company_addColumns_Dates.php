<?php

use yii\db\Schema;
use yii\db\Migration;

class m150518_064926_alter_Company_addColumns_Dates extends Migration
{
    public function safeUp()
    {
        $this->addColumn('company', 'created_at', Schema::TYPE_INTEGER . ' NOT NULL');
        $this->addColumn('company', 'updated_at', Schema::TYPE_INTEGER . ' NOT NULL');

        $this->execute('
            UPDATE company
                SET created_at = UNIX_TIMESTAMP(),
                    updated_at = UNIX_TIMESTAMP()
        ');
    }
    
    public function safeDown()
    {
        $this->dropColumn('company', 'created_at');
        $this->dropColumn('company', 'updated_at');
    }
}
