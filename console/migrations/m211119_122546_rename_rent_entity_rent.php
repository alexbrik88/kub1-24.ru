<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211119_122546_rename_rent_entity_rent extends Migration
{
    public function safeUp()
    {
        $this->renameTable('rent_entity_rent', 'rent_agreement');
    }

    public function safeDown()
    {
        $this->renameTable('rent_agreement', 'rent_entity_rent');
    }
}
