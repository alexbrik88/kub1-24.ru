<?php

use yii\db\Migration;

class m210323_181525_update_autoload_job_table extends Migration
{
    /** @var string[] */
    private const TABLE_NAME = '{{%autoload_job}}';

    /**
     * @inheritDoc
     */
    public function safeUp()
    {
        $rows = $this->getRows();

        array_walk($rows, [$this, 'updateRow']);
    }

    /**
     * @return string[][]
     * @throws
     */
    private function getRows(): array
    {
        return $this->db->createCommand(<<<SQL
            SELECT autoload_job.company_id, autoload_job.command_type, acquiring.identifier FROM autoload_job
            LEFT JOIN acquiring ON (acquiring.company_id = autoload_job.company_id AND acquiring.type = 0)
            WHERE autoload_job.command_type = 2 AND acquiring.identifier IS NOT NULL
SQL
        )->queryAll();
    }

    /**
     * @param string[] $row
     * @return void
     * @throws
     */
    private function updateRow(array $row): void
    {
        $this->update(
            self::TABLE_NAME,
            ['identifier' => $row['identifier']],
            ['company_id' => $row['company_id'], 'command_type' => $row['command_type']]
        );
    }
}
