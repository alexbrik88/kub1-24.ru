<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200512_084717_alter_company_drop_column extends Migration
{
    public function safeUp()
    {
        $this->dropIndex('taxation_type_rel', '{{%company}}');

        $this->dropColumn('{{%company}}', 'old_taxation_type_id');
    }

    public function safeDown()
    {
        $this->addColumn('{{%company}}', 'old_taxation_type_id', $this->integer());
    }
}
