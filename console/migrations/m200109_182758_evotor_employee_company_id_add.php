<?php

use console\components\db\Migration;

class m200109_182758_evotor_employee_company_id_add extends Migration
{

    const TABLE = '{{%evotor_employee}}';

    public function up()
    {
        $this->addColumn(self::TABLE, 'company_id', $this->integer(11)->notNull()->after('id')->comment('Идентификатор компании КУБ'));
    }

    public function down()
    {
        $this->dropColumn(self::TABLE, 'company_id');
    }
}
