<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180225_165459_alter_out_invoice_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('out_invoice', 'data_url', $this->text()->after('return_url'));
    }

    public function safeDown()
    {
        $this->dropColumn('out_invoice', 'data_url');
    }
}
