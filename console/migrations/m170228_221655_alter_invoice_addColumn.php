<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170228_221655_alter_invoice_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%invoice}}', 'signed_by_employee_id', $this->integer());
        $this->addColumn('{{%invoice}}', 'signed_by_name', $this->string(50));
        $this->addColumn('{{%invoice}}', 'sign_document_type_id', $this->integer());
        $this->addColumn('{{%invoice}}', 'sign_document_number', $this->string(50));
        $this->addColumn('{{%invoice}}', 'sign_document_date', $this->date());
        $this->addColumn('{{%invoice}}', 'signature_id', $this->integer());

        $this->addForeignKey('FK_invoice_sign_employee', '{{%invoice}}', 'signed_by_employee_id', '{{%employee}}', 'id');
        $this->addForeignKey('FK_invoice_sign_documentType', '{{%invoice}}', 'sign_document_type_id', '{{%document_type}}', 'id');
        $this->addForeignKey('FK_invoice_employeeSignature', '{{%invoice}}', 'signature_id', '{{%employee_signature}}', 'id');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('FK_invoice_sign_employee', '{{%invoice}}');
        $this->dropForeignKey('FK_invoice_sign_documentType', '{{%invoice}}');
        $this->dropForeignKey('FK_invoice_employeeSignature', '{{%invoice}}');
        $this->dropColumn('{{%invoice}}', 'signed_by_employee_id');
        $this->dropColumn('{{%invoice}}', 'signed_by_name');
        $this->dropColumn('{{%invoice}}', 'sign_document_type_id');
        $this->dropColumn('{{%invoice}}', 'sign_document_number');
        $this->dropColumn('{{%invoice}}', 'sign_document_date');
        $this->dropColumn('{{%invoice}}', 'signature_id');
    }
}
