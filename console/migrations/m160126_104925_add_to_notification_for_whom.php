<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160126_104925_add_to_notification_for_whom extends Migration
{
    public $tNotification = 'notification';

    public function safeUp()
    {
        $this->addColumn($this->tNotification, 'for_whom', $this->string()->notNull());
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tNotification, 'for_whom');
    }
}


