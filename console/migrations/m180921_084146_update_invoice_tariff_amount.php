<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180921_084146_update_invoice_tariff_amount extends Migration
{
    public $tableName = 'service_invoice_tariff';

    public function safeUp()
    {
        $this->update($this->tableName, ['total_amount' => 100], ['id' => 1]);
        $this->update($this->tableName, ['total_amount' => 250], ['id' => 2]);
    }
    
    public function safeDown()
    {
        $this->update($this->tableName, ['total_amount' => 50], ['id' => 1]);
        $this->update($this->tableName, ['total_amount' => 120], ['id' => 2]);
    }
}


