<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200930_060810_alter_user_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%user_config}}', 'product_title_en', $this->boolean()->defaultValue(false)->after('product_comment'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%user_config}}', 'product_title_en');
    }
}
