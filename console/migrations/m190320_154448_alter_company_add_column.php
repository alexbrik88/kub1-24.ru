<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190320_154448_alter_company_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%company}}', 'hide_widget_footer', $this->boolean()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%company}}', 'hide_widget_footer');
    }
}
