<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210629_102731_update_invoice_expenditure_item_add_row extends Migration
{
    public function safeUp()
    {
        $this->insert('invoice_expenditure_item', [
            'id' => 92,
            'name' => 'Списание товара',
            'is_visible' => 0,
            'can_be_controlled' => 0,
        ]);
    }

    public function safeDown()
    {
        $this->delete('invoice_expenditure_item', ['id' => 92]);
    }
}
