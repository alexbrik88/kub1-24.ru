<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190919_053655_alter_packing_list_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%packing_list}}', 'print_id', $this->integer()->after('signature_id'));
        $this->addColumn('{{%packing_list}}', 'chief_signature_id', $this->integer()->after('print_id'));
        $this->addColumn('{{%packing_list}}', 'chief_accountant_signature_id', $this->integer()->after('chief_signature_id'));

        $this->addForeignKey(
            'FK_packing_list_company_details_print',
            '{{%packing_list}}',
            'print_id',
            '{{%company_details_file}}',
            'id'
        );

        $this->addForeignKey(
            'FK_packing_list_company_details_chief_signature',
            '{{%packing_list}}',
            'chief_signature_id',
            '{{%company_details_file}}',
            'id'
        );

        $this->addForeignKey(
            'FK_packing_list_company_details_chief_accountant_signature',
            '{{%packing_list}}',
            'chief_accountant_signature_id',
            '{{%company_details_file}}',
            'id'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_packing_list_company_details_print', '{{%packing_list}}');
        $this->dropForeignKey('FK_packing_list_company_details_chief_signature', '{{%packing_list}}');
        $this->dropForeignKey('FK_packing_list_company_details_chief_accountant_signature', '{{%packing_list}}' );

        $this->dropColumn('{{%packing_list}}', 'print_id');
        $this->dropColumn('{{%packing_list}}', 'chief_signature_id');
        $this->dropColumn('{{%packing_list}}', 'chief_accountant_signature_id');
    }
}
