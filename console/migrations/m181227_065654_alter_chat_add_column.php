<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181227_065654_alter_chat_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('chat', 'is_file', $this->boolean()->notNull()->defaultValue(false)->after('message'));

        $this->execute("
            UPDATE {{chat}}
            SET {{chat}}.[[is_file]] = 1
            WHERE {{chat}}.[[message]] LIKE '<a href=\"/chat/download-file?id=%'
        ");
    }

    public function safeDown()
    {
        $this->dropColumn('chat', 'is_file');
    }
}
