<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201016_180419_update_invoice_expenditure_item_prepayment_articles_2 extends Migration
{
    public function safeUp()
    {
        $this->execute("
            UPDATE `invoice_expenditure_item` 
            SET `is_prepayment` = 1 
            WHERE `id` = 52; 
        ");

        $this->execute("
            UPDATE `expense_item_flow_of_funds` 
            SET `is_prepayment` = 1 
            WHERE `expense_item_id` = 52
        ");

        $this->execute("
            UPDATE `invoice_income_item` 
            SET `is_prepayment` = 1 
            WHERE `id` = 14; 
        ");

        $this->execute("
            UPDATE `income_item_flow_of_funds` 
            SET `is_prepayment` = 1 
            WHERE `income_item_id` = 14
        ");
    }

    public function safeDown()
    {
        // no down
    }
}
