<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191026_055143_alter_payment_add_column extends Migration
{
    public $tPayment = 'service_payment';

    public function safeUp()
    {
        $this->addColumn($this->tPayment, 'by_novice_in_module', $this->boolean()->notNull()->defaultValue(0)->after('by_novice'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tPayment, 'by_novice_in_module');
    }
}
