<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180730_124818_alter_product_unit_drop_index extends Migration
{
    public function safeUp()
    {
        $this->dropIndex('code_okei', 'product_unit');
    }

    public function safeDown()
    {
        $this->createIndex('code_okei', 'product_unit', 'code_okei', true);
    }
}
