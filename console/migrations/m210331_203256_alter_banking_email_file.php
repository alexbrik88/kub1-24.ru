<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210331_203256_alter_banking_email_file extends Migration
{
    public $table = 'banking_email_file';

    public function safeUp()
    {
        $this->dropColumn($this->table, 'is_deleted');
    }

    public function safeDown()
    {
        $this->addColumn($this->table, 'is_deleted', $this->boolean()->defaultValue(false));
    }
}
