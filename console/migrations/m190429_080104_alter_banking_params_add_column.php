<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190429_080104_alter_banking_params_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%banking_params}}', 'bank_alias', $this->string(50)->notNull()->after('company_id'));

        $query = new \yii\db\Query;
        $bikArray = $query->select('bank_bik')->distinct()->from('{{%banking_params}}')->column();
        foreach ($bikArray as $bik) {
            if ($alias = \frontend\modules\cash\modules\banking\components\Banking::aliasByBik($bik)) {
                $this->update('{{%banking_params}}', ['bank_alias' => $alias], ['bank_bik' => $bik]);
            }
        }

        $this->execute('
            DELETE t1
            FROM {{%banking_params}} {{t1}}
            LEFT JOIN {{%company}}
                ON {{company}}.[[id]] = {{t1}}.[[company_id]]
            LEFT JOIN {{%banking_params}} {{t2}}
                ON {{t2}}.[[company_id]] = {{t1}}.[[company_id]]
                AND {{t2}}.[[bank_alias]] = {{t1}}.[[bank_alias]]
                AND {{t2}}.[[param_name]] = {{t1}}.[[param_name]]
                AND {{t2}}.[[updated_at]] > {{t1}}.[[updated_at]]
            WHERE {{t2}}.[[updated_at]] IS NOT NULL
            OR {{t1}}.[[bank_alias]] IS NULL
            OR {{company}}.[[id]] IS NULL
        ');

        $this->dropForeignKey('FK_bankingParams_company', '{{%banking_params}}');
        $this->dropForeignKey('FK_bankingParams_bank', '{{%banking_params}}');

        $this->dropPrimaryKey('PRIMARY_KEY', '{{%banking_params}}');
        $this->addPrimaryKey('PRIMARY_KEY', '{{%banking_params}}', [
            'company_id',
            'bank_alias',
            'param_name',
        ]);

        $this->addForeignKey('FK_bankingParams_company', '{{%banking_params}}', 'company_id', '{{%company}}', 'id', 'CASCADE');

        $this->dropColumn('{{%banking_params}}', 'bank_bik');
    }

    public function safeDown()
    {
        $this->addColumn('{{%banking_params}}', 'bank_bik', $this->string(9)->notNull()->after('company_id'));

        $query = new \yii\db\Query;
        $aliasArray = $query->select('bank_alias')->distinct()->from('{{%banking_params}}')->column();
        foreach ($aliasArray as $alias) {
            $class = "\\frontend\\modules\\cash\\modules\\banking\\modules\\{$alias}\\models\\BankModel";
            if (class_exists($class)) {
                $this->update('{{%banking_params}}', ['bank_bik' => $class::BIK], ['bank_alias' => $alias]);
            }
        }

        $this->delete('{{%banking_params}}', ['bank_bik' => null]);

        $this->dropForeignKey('FK_bankingParams_company', '{{%banking_params}}');

        $this->dropPrimaryKey('PRIMARY_KEY', '{{%banking_params}}');
        $this->addPrimaryKey('PRIMARY_KEY', '{{%banking_params}}', [
            'company_id',
            'bank_bik',
            'param_name',
        ]);

        $this->addForeignKey('FK_bankingParams_company', '{{%banking_params}}', 'company_id', '{{%company}}', 'id', 'CASCADE');
        $this->addForeignKey('FK_bankingParams_bank', '{{%banking_params}}', 'bank_bik', '{{%bank}}', 'bik');

        $this->dropColumn('{{%banking_params}}', 'bank_alias');
    }
}
