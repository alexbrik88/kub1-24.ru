<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160718_134832_insert_in_companyType extends Migration
{
    public $tCompanyType = 'company_type';

    public function safeUp()
    {
        $this->batchInsert($this->tCompanyType, ['id', 'name_short', 'name_full', 'in_company', 'in_contractor'], [
            [8, 'ОАО', 'Открытое акционерное общество', true, true],
        ]);
    }

    public function safeDown()
    {
        $this->delete($this->tCompanyType, 'id IN (8)');
    }
}


