<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181206_184357_add_invoice_status_updated_at_to_logistics_request extends Migration
{
    public $tableName = 'logistics_request';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'status_updated_at', $this->integer()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'status_updated_at');
    }
}


