<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191112_155417_add_registration_page_type extends Migration
{
    public $tableName = '{{%registration_page_type}}';

    public function safeUp()
    {
        $this->insert($this->tableName, [
            'id' => 26,
            'name' => 'Сбербанк лэндинг',
        ]);
    }

    public function safeDown()
    {
        $this->delete($this->tableName, ['id' => 26]);
    }
}
