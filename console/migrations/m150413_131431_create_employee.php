<?php

use yii\db\Schema;
use yii\db\Migration;

class m150413_131431_create_employee extends Migration
{
    public function up()
    {
        $this->createTable('employee_role', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . '(45) NOT NULL',
        ]);
        $this->batchInsert('employee_role', ['id', 'name',], [
            [1, 'Руководитель',],
            [2, 'Бухгалтер',],
            [3, 'Менеджер',],
            [4, 'Помощник',],
        ]);

        $this->createTable('employee', [
            'id' => Schema::TYPE_PK,
            'company_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'email' => Schema::TYPE_STRING . '(255) NOT NULL',
            'password' => Schema::TYPE_STRING . '(255) NOT NULL',
            'created_at' => Schema::TYPE_DATE . ' NOT NULL',
            'name' => Schema::TYPE_STRING . '(255) NOT NULL',
            'sex' => Schema::TYPE_BOOLEAN . ' NOT NULL COMMENT "0 - женский, 1 - мужской"',
            'birthday' => Schema::TYPE_DATE . ' NOT NULL',
            'date_hiring' => Schema::TYPE_DATE . ' NOT NULL',
            'date_dismissal' => Schema::TYPE_DATE . ' DEFAULT NULL',
            'position' => Schema::TYPE_STRING . '(255) NOT NULL',
            'is_working' => Schema::TYPE_BOOLEAN . ' NOT NULL COMMENT "1 - работает, 0 - уволен"',
            'employee_role_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'phone' => Schema::TYPE_STRING . '(20)',
            'notify_nearly_report' => Schema::TYPE_BOOLEAN . ' NOT NULL ',
            'notify_new_features' => Schema::TYPE_BOOLEAN . ' NOT NULL',
        ]);
        $this->addForeignKey('employee_role_rel', 'employee', 'employee_role_id', 'employee_role', 'id');
    }

    public function down()
    {
        $this->dropTable('employee');
        $this->dropTable('employee_role');
    }
}
