<?php

use yii\db\Migration;
use yii\db\Query;

/**
 * Handles the creation of table `{{%cash_emoney_foreign_currency_flows}}`.
 */
class m210106_145253_create_cash_emoney_foreign_currency_flows_table extends Migration
{
    private function getDbName()
    {
        if (preg_match('/dbname=([^;]*)/', Yii::$app->db->dsn, $match)) {
            return $match[1];
        } else {
            return '';
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $collate = (new Query)
            ->select('COLLATION_NAME')
            ->from('information_schema.COLUMNS')
            ->where([
                'TABLE_SCHEMA' => $this->getDbName(),
                'TABLE_NAME' => 'currency',
                'COLUMN_NAME' => 'id',
            ])->scalar() ?: 'utf8_unicode_ci';
        $append = "COLLATE $collate";

        $this->createTable('{{%cash_emoney_foreign_currency_flows}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'emoney_id' => $this->integer()->notNull(),
            'currency_id' => $this->char(3)->notNull()->append($append),
            'currency_name' => $this->char(3)->notNull(),
            'date' => $this->date()->notNull(),
            'date_time' => $this->dateTime()->notNull(),
            'recognition_date' => $this->date(),
            'is_internal_transfer' => $this->boolean()->notNull()->defaultValue(false),
            'internal_transfer_account_table' => $this->string()->null(),
            'internal_transfer_account_id' => $this->integer()->null(),
            'internal_transfer_flow_table' => $this->string()->null(),
            'internal_transfer_flow_id' => $this->integer()->null(),
            'flow_type' => $this->tinyInteger()->notNull()->comment('0 - расход, 1 - приход'),
            'number' => $this->string()->null(),
            'contractor_id' => $this->string()->notNull(),
            'amount' => $this->bigInteger()->notNull()->unsigned(),
            'description' => $this->string()->null(),
            'expenditure_item_id' => $this->integer()->null(),
            'income_item_id' => $this->integer()->null(),
            'cash_funding_type_id' => $this->integer()->null(),
            'project_id' => $this->integer()->null(),
            'moneta_id' => $this->integer()->null(),
            'created_at' => $this->integer()->notNull(),
            'is_accounting' => $this->boolean()->notNull()->defaultValue(true)->comment('Учёт в бухгалтерии: 0 - нет, 1 - да'),
            'has_invoice' => $this->boolean()->notNull()->defaultValue(false)->comment('Связь со счётом: 0 - нет, 1 - да'),
            'is_prepaid_expense' => $this->boolean()->notNull()->defaultValue(false),
            'is_funding_flow' => $this->boolean()->notNull()->defaultValue(false),
        ]);

        $this->createIndex('company_date', '{{%cash_emoney_foreign_currency_flows}}', ['company_id', 'date']);

        $this->addForeignKey('FK_cash_emoney_foreign_currency_flows__company_id', '{{%cash_emoney_foreign_currency_flows}}', 'company_id', '{{%company}}', 'id');
        $this->addForeignKey('FK_cash_emoney_foreign_currency_flows__emoney_id', '{{%cash_emoney_foreign_currency_flows}}', 'emoney_id', '{{%emoney}}', 'id');
        $this->addForeignKey('FK_cash_emoney_foreign_currency_flows__currency_id', '{{%cash_emoney_foreign_currency_flows}}', 'currency_id', '{{%currency}}', 'id');
        $this->addForeignKey('FK_cash_emoney_foreign_currency_flows__expenditure_item_id', '{{%cash_emoney_foreign_currency_flows}}', 'expenditure_item_id', '{{%invoice_expenditure_item}}', 'id');
        $this->addForeignKey('FK_cash_emoney_foreign_currency_flows__income_item_id', '{{%cash_emoney_foreign_currency_flows}}', 'income_item_id', '{{%invoice_income_item}}', 'id');
        $this->addForeignKey('FK_cash_emoney_foreign_currency_flows__cash_funding_type_id', '{{%cash_emoney_foreign_currency_flows}}', 'cash_funding_type_id', '{{%cash_funding_type}}', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%cash_emoney_foreign_currency_flows}}');
    }
}
