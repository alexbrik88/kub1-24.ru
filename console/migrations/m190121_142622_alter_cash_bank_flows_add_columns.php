<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190121_142622_alter_cash_bank_flows_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cash_bank_flows', 'kbk', $this->string(20));
        $this->addColumn('cash_bank_flows', 'tax_period_code', $this->string(10));
    }

    public function safeDown()
    {
        $this->dropColumn('cash_bank_flows', 'kbk');
        $this->dropColumn('cash_bank_flows', 'tax_period_code');
    }
}
