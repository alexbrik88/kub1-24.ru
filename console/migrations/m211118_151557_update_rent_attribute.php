<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211118_151557_update_rent_attribute extends Migration
{
    public function safeUp()
    {
        $this->update('{{%rent_attribute}}', ['sort' => 1], ['alias' => 'price']);
        $this->update('{{%rent_attribute}}', ['sort' => 2], ['alias' => 'currency']);
        $this->update('{{%rent_attribute}}', ['sort' => 3], ['alias' => 'unit']);
        $this->update('{{%rent_attribute}}', ['sort' => 4], ['alias' => 'paymentPeriod']);
    }

    public function safeDown()
    {
        $this->update('{{%rent_attribute}}', ['sort' => 1], ['alias' => 'currency']);
        $this->update('{{%rent_attribute}}', ['sort' => 2], ['alias' => 'unit']);
        $this->update('{{%rent_attribute}}', ['sort' => 3], ['alias' => 'paymentPeriod']);
        $this->update('{{%rent_attribute}}', ['sort' => 4], ['alias' => 'price']);
    }
}
