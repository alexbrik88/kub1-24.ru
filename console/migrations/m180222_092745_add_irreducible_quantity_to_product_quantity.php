<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180222_092745_add_irreducible_quantity_to_product_quantity extends Migration
{
    public $tableName = 'product_store';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'irreducible_quantity', $this->decimal(20, 10)->notNull()->defaultValue(0));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'irreducible_quantity');
    }
}


