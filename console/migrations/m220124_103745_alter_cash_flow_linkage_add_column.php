<?php

use console\components\db\Migration;
use yii\db\Schema;

class m220124_103745_alter_cash_flow_linkage_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cash_flow_linkage}}', 'then_wallet_type', $this->tinyInteger(1)->notNull()->after('then_flow_type'));
        $this->addColumn('{{%cash_flow_linkage}}', 'then_amount_action', $this->tinyInteger(1)->notNull()->after('then_date_diff'));
        $this->renameColumn('{{%cash_flow_linkage}}', 'then_cashbox_id', 'then_wallet_id');
        $this->renameColumn('{{%cash_flow_linkage}}', 'then_amount_diff', 'then_amount_action_value');
        $this->update('{{%cash_flow_linkage}}', [
            'then_wallet_type' => 2,
            'then_amount_action' => 1,
        ]);
    }

    public function safeDown()
    {
        $this->dropColumn('{{%cash_flow_linkage}}', 'then_wallet_type');
        $this->dropColumn('{{%cash_flow_linkage}}', 'then_amount_action');
        $this->renameColumn('{{%cash_flow_linkage}}', 'then_wallet_id', 'then_cashbox_id');
        $this->renameColumn('{{%cash_flow_linkage}}', 'then_amount_action_value', 'then_amount_diff');
    }
}
