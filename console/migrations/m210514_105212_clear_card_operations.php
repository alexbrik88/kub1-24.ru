<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210514_105212_clear_card_operations extends Migration
{
    public function safeUp()
    {
        $this->execute('DELETE FROM `card_operation` WHERE `is_deleted` = 1');
    }

    public function safeDown()
    {
        // no down
    }
}
