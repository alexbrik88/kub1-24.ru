<?php

use console\components\db\Migration;

class m200928_143816_alter_user_config_append_sales_invoice_column extends Migration
{
    public $tableName = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'sales_invoice_order_sum_without_nds', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'sales_invoice_order_sum_nds', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'sales_invoice_contractor_inn', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'sales_invoice_payment_date', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'sales_invoice_responsible', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'sales_invoice_source', $this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'sales_invoice_order_sum_without_nds');
        $this->dropColumn($this->tableName, 'sales_invoice_order_sum_nds');
        $this->dropColumn($this->tableName, 'sales_invoice_contractor_inn');
        $this->dropColumn($this->tableName, 'sales_invoice_payment_date');
        $this->dropColumn($this->tableName, 'sales_invoice_responsible');
        $this->dropColumn($this->tableName, 'sales_invoice_source');
    }

}
