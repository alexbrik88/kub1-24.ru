<?php

use yii\db\Schema;
use yii\db\Migration;

class m150618_070631_update_fix_prices extends Migration
{
    public function safeUp()
    {
        $this->update('product', [
            'price_for_buy_with_nds' => new \yii\db\Expression('price_for_buy_with_nds * 100'),
            'price_for_sell_with_nds' => new \yii\db\Expression('price_for_sell_with_nds * 100'),
        ]);
        $this->update('order', [
            'purchase_price_no_vat' => new \yii\db\Expression('purchase_price_no_vat * 100'),
            'purchase_price_with_vat' => new \yii\db\Expression('purchase_price_with_vat * 100'),
            'selling_price_no_vat' => new \yii\db\Expression('selling_price_no_vat * 100'),
            'selling_price_with_vat' => new \yii\db\Expression('selling_price_with_vat * 100'),
            'excise_price' => new \yii\db\Expression('excise_price * 100'),
            'sale_tax' => new \yii\db\Expression('sale_tax * 100'),
            'purchase_tax' => new \yii\db\Expression('purchase_tax * 100'),
        ]);
        $this->update('invoice', [
            'total_amount_no_nds' => new \yii\db\Expression('total_amount_no_nds * 100'),
            'total_amount_with_nds' => new \yii\db\Expression('total_amount_with_nds * 100'),
            'total_amount_nds' => new \yii\db\Expression('total_amount_nds * 100'),
            'payment_partial_amount' => new \yii\db\Expression('payment_partial_amount * 100'),
        ]);

        $this->update('cash_bank_flows', [
            'amount' => new \yii\db\Expression('amount * 100'),
        ]);
        $this->update('cash_emoney_flows', [
            'amount' => new \yii\db\Expression('amount * 100'),
        ]);
        $this->update('cash_order_flows', [
            'amount' => new \yii\db\Expression('amount * 100'),
        ]);
    }
    
    public function safeDown()
    {
        $this->update('product', [
            'price_for_buy_with_nds' => new \yii\db\Expression('price_for_buy_with_nds / 100'),
            'price_for_sell_with_nds' => new \yii\db\Expression('price_for_sell_with_nds / 100'),
        ]);
        $this->update('order', [
            'purchase_price_no_vat' => new \yii\db\Expression('purchase_price_no_vat / 100'),
            'purchase_price_with_vat' => new \yii\db\Expression('purchase_price_with_vat / 100'),
            'selling_price_no_vat' => new \yii\db\Expression('selling_price_no_vat / 100'),
            'selling_price_with_vat' => new \yii\db\Expression('selling_price_with_vat / 100'),
            'excise_price' => new \yii\db\Expression('excise_price / 100'),
            'sale_tax' => new \yii\db\Expression('sale_tax / 100'),
            'purchase_tax' => new \yii\db\Expression('purchase_tax / 100'),
        ]);
        $this->update('invoice', [
            'total_amount_no_nds' => new \yii\db\Expression('total_amount_no_nds / 100'),
            'total_amount_with_nds' => new \yii\db\Expression('total_amount_with_nds / 100'),
            'total_amount_nds' => new \yii\db\Expression('total_amount_nds / 100'),
            'payment_partial_amount' => new \yii\db\Expression('payment_partial_amount / 100'),
        ]);

        $this->update('cash_bank_flows', [
            'amount' => new \yii\db\Expression('amount / 100'),
        ]);
        $this->update('cash_emoney_flows', [
            'amount' => new \yii\db\Expression('amount / 100'),
        ]);
        $this->update('cash_order_flows', [
            'amount' => new \yii\db\Expression('amount / 100'),
        ]);
    }
}
?>
