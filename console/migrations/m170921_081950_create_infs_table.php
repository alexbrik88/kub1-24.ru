<?php

use yii\db\Migration;

/**
 * Handles the creation of table `infs`.
 */
class m170921_081950_create_infs_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('infs', [
            'ga' => $this->primaryKey(),
            'gb' => $this->string(500)->notNull()->defaultValue(''),
            'g1' => $this->string(500)->notNull()->defaultValue(''),
            'g2' => $this->string(500)->notNull()->defaultValue(''),
            'g3' => $this->string(500)->notNull()->defaultValue(''),
            'g4' => $this->string(500)->notNull()->defaultValue(''),
            'g5' => $this->text(),
            'g6' => $this->string(500)->notNull()->defaultValue(''),
            'g7' => $this->string(500)->notNull()->defaultValue(''),
            'g8' => $this->string(500)->notNull()->defaultValue(''),
            'g9' => $this->string(500)->notNull()->defaultValue(''),
            'g10' => $this->string(500)->notNull()->defaultValue(''),
            'g11' => $this->string(500)->notNull()->defaultValue(''),
            'g12' => $this->string(500)->notNull()->defaultValue(''),
            'g13' => $this->string(500)->notNull()->defaultValue(''),
            'g14' => $this->string(500)->notNull()->defaultValue(''),
            'g15' => $this->string(500)->notNull()->defaultValue(''),
            'g16' => $this->string(500)->notNull()->defaultValue(''),
            'g18' => $this->string(500)->notNull()->defaultValue(''),
            'g19' => $this->string(500)->notNull()->defaultValue(''),
            'g20' => $this->string(500)->notNull()->defaultValue(''),
            'g21' => $this->string(500)->notNull()->defaultValue(''),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('infs');
    }
}
