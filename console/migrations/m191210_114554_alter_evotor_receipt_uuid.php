<?php

use console\components\db\Migration;
use common\models\evotor\ReceiptItem;
use common\models\evotor\Receipt;

class m191210_114554_alter_evotor_receipt_uuid extends Migration
{
    public function safeUp()
    {
        ReceiptItem::deleteAll();
        Receipt::deleteAll();
        $this->addColumn('{{evotor_receipt}}', 'uuid', $this->char(36)->notNull()->unique()->comment('UUID чека Эвотор'));
        $this->dropForeignKey('evotor_receipt_item_receipt_id', '{{evotor_receipt_item}}');
        $this->dropColumn('{{evotor_receipt}}', 'id');
        $this->addColumn('{{evotor_receipt}}', 'id', $this->primaryKey(11)->unsigned()->notNull()->first());
        $this->alterColumn('{{evotor_receipt_item}}', 'receipt_id', $this->integer(11)->unsigned()->notNull()->comment('ID чека (receipt.id)'));
        $this->addForeignKey('evotor_receipt_item_receipt_id', '{{evotor_receipt_item}}', 'receipt_id',
            '{{evotor_receipt}}', 'id', 'CASCADE', 'CASCADE'
        );
    }

    public function safeDown()
    {
        ReceiptItem::deleteAll();
        Receipt::deleteAll();
        $this->dropColumn('{{evotor_receipt}}', 'uuid');
        $this->dropForeignKey('evotor_receipt_item_receipt_id', '{{evotor_receipt_item}}');
        $this->dropColumn('{{evotor_receipt}}', 'id');
        $this->addColumn('{{evotor_receipt}}', 'id', "BINARY(16) NOT NULL PRIMARY KEY COMMENT 'UUID Эвотора' FIRST");
        $this->alterColumn('{{evotor_receipt_item}}', 'receipt_id', "BINARY(16) NOT NULL COMMENT 'ID чека'");
        $this->addForeignKey(
            'evotor_receipt_item_receipt_id', '{{evotor_receipt_item}}', 'receipt_id',
            '{{evotor_receipt}}', 'id', 'CASCADE', 'CASCADE'
        );
    }
}
