<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210826_050211_create_payments_request_template extends Migration
{
    public $templateTable = 'payments_request_sign_template';
    public $templateStepTable = 'payments_request_sign_template_step';
    public $statusTable = 'payments_request_sign_status';

    public function safeUp()
    {
        // Template
        $table = $this->templateTable;
        $this->createTable($table, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'name' => $this->string(255)->notNull(),
            'comment' => $this->string(2048),
            'is_active' => $this->boolean()->defaultValue(true)
        ]);

        $this->addForeignKey("FK_{$table}_to_company", $table, 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');

        // Template Step
        $table = $this->templateStepTable;
        $this->createTable($table, [
            'id' => $this->primaryKey(),
            'template_id' => $this->integer()->notNull(),
            'employee_id' => $this->integer()->notNull(),
            'name' => $this->string(255)->notNull(),
            'step' => $this->integer()->unsigned()->notNull()
        ]);

        $this->addForeignKey("FK_{$table}_to_template", $table, 'template_id', 'payments_request_sign_template', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("FK_{$table}_to_employee", $table, 'employee_id', 'employee', 'id', 'CASCADE', 'CASCADE');

        // Sign Status
        $table = $this->statusTable;
        $this->createTable($table, [
            'request_id' => $this->integer()->notNull(),
            'employee_id' => $this->integer()->notNull(),
            'can_sign' => $this->boolean()->defaultValue(false),
            'can_unsign' => $this->boolean()->defaultValue(false),
            'is_signed' => $this->boolean()->defaultValue(false),
            'signed_at' => $this->integer()->unsigned(),
            'step' => $this->integer()->unsigned()->notNull()
        ]);

        $this->addForeignKey("FK_{$table}_to_request", $table, 'request_id', 'payments_request', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("FK_{$table}_to_employee", $table, 'employee_id', 'employee', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        // Sign Status
        $table = $this->statusTable;
        $this->dropForeignKey("FK_{$table}_to_request", $table);
        $this->dropForeignKey("FK_{$table}_to_employee", $table);
        $this->dropTable($table);

        // Template Signer
        $table = $this->templateStepTable;
        $this->dropForeignKey("FK_{$table}_to_template", $table);
        $this->dropForeignKey("FK_{$table}_to_employee", $table);
        $this->dropTable($table);

        // Template
        $table = $this->templateTable;
        $this->dropForeignKey("FK_{$table}_to_company", $table);
        $this->dropTable($table);
    }
}
