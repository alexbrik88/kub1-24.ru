<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200713_165544_insert_service_subscribe_contains_tariff extends Migration
{
    public function safeUp()
    {
        $this->batchInsert('{{%service_subscribe_contains_tariff}}', [
            'tariff_id',
            'contains_tariff_id',
        ], [
            [52, 18],
            [53, 19],
            [54, 20],
            [67, 18],
            [68, 19],
            [69, 20],
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%service_subscribe_contains_tariff}}', [
            'or',
            [
                'and',
                ['tariff_id' => 52],
                ['contains_tariff_id' => 18],
            ],
            [
                'and',
                ['tariff_id' => 53],
                ['contains_tariff_id' => 19],
            ],
            [
                'and',
                ['tariff_id' => 54],
                ['contains_tariff_id' => 20],
            ],
            [
                'and',
                ['tariff_id' => 67],
                ['contains_tariff_id' => 18],
            ],
            [
                'and',
                ['tariff_id' => 68],
                ['contains_tariff_id' => 19],
            ],
            [
                'and',
                ['tariff_id' => 69],
                ['contains_tariff_id' => 20],
            ],
        ]);
    }
}
