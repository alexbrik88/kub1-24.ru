<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210420_150012_update_upd_column_one_c_imported extends Migration
{
    public function safeUp()
    {
        $this->execute('UPDATE `upd` SET `one_c_imported` = 1 WHERE `object_guid` IS NOT NULL');
    }

    public function safeDown()
    {
        // no down
    }
}
