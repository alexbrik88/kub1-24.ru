<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200512_103201_alter_product_alter_column extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%product}}', 'has_excise',  $this->boolean()->notNull()->defaultValue(false));
    }

    public function down()
    {
        $this->alterColumn('{{%product}}', 'has_excise',  $this->boolean()->notNull());
    }
}
