<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210104_161528_alter_contractor_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%contractor}}', 'seller_discount', $this->decimal(9,6));
        $this->addColumn('{{%contractor}}', 'seller_payment_delay', $this->integer()->notNull()->defaultValue(10));
        $this->addColumn('{{%contractor}}', 'seller_guaranty_delay', $this->integer()->notNull()->defaultValue(20));
        $this->addColumn('{{%contractor}}', 'customer_discount', $this->decimal(9,6));
        $this->addColumn('{{%contractor}}', 'customer_payment_delay', $this->integer()->notNull()->defaultValue(10));
        $this->addColumn('{{%contractor}}', 'customer_guaranty_delay', $this->integer()->notNull()->defaultValue(20));

        $this->execute('
            UPDATE {{%contractor}}
            SET [[seller_discount]] = [[discount]],
                [[seller_payment_delay]] = [[payment_delay]],
                [[seller_guaranty_delay]] = [[guaranty_delay]]
            WHERE [[type]] = 1
        ');
        $this->execute('
            UPDATE {{%contractor}}
            SET [[customer_discount]] = [[discount]],
                [[customer_payment_delay]] = [[payment_delay]],
                [[customer_guaranty_delay]] = [[guaranty_delay]]
            WHERE [[type]] = 2
        ');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%contractor}}', 'seller_discount');
        $this->dropColumn('{{%contractor}}', 'seller_payment_delay');
        $this->dropColumn('{{%contractor}}', 'seller_guaranty_delay');
        $this->dropColumn('{{%contractor}}', 'customer_discount');
        $this->dropColumn('{{%contractor}}', 'customer_payment_delay');
        $this->dropColumn('{{%contractor}}', 'customer_guaranty_delay');
    }
}
