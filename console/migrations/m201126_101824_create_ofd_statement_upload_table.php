<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%ofd_statement_upload}}`.
 */
class m201126_101824_create_ofd_statement_upload_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%ofd_statement_upload}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'ofd_id' => $this->integer()->notNull(),
            'store_id' => $this->integer(),
            'kkt_id' => $this->integer(),
            'source' => $this->integer()->notNull(),
            'period_from' => $this->date(),
            'period_till' => $this->date(),
            'created_by' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'saved_count' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('FK_ofd_statement_upload_to_employee', '{{%ofd_statement_upload}}', 'created_by', 'employee', 'id');
        $this->addForeignKey('FK_ofd_statement_upload_to_company', '{{%ofd_statement_upload}}', 'company_id', 'company', 'id');
        $this->addForeignKey('FK_ofd_statement_upload_to_cashbox', '{{%ofd_statement_upload}}', 'ofd_id', 'ofd', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%ofd_statement_upload}}');
    }
}
