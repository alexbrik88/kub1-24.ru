<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210528_113413_alter_acquiring_operation_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn(
            'acquiring_operation',
            'acquiring_id',
            $this->integer()->after('id')
        );
        $this->execute('
            UPDATE acquiring_operation op 
            LEFT JOIN acquiring a ON op.acquiring_identifier = a.identifier 
            SET op.acquiring_id = a.id 
        ');
        $this->addForeignKey(
            'FK_moneta_operation_to_acquiring',
            'acquiring_operation',
            'acquiring_id',
            'acquiring',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_moneta_operation_to_acquiring', 'acquiring_operation');
        $this->dropColumn('acquiring_operation', 'acquiring_id');
    }
}
