<?php

use console\components\db\Migration;

/**
 * Handles the creation of table `{{%foreign_currency_invoice_order}}`.
 */
class m200630_064943_create_foreign_currency_invoice_order_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%foreign_currency_invoice_order}}', [
            'id' => $this->primaryKey(),
            'foreign_currency_invoice_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'unit_id' => $this->integer()->notNull(),
            'tax_rate_id' => $this->integer()->notNull(),
            'number' => $this->integer()->notNull(),
            'article' => $this->string(50),
            'product_title' => $this->text(),
            'product_code' => $this->string(),
            'quantity' => $this->decimal(20,10)->notNull()->defaultValue(0),
            'reserve' => $this->decimal(20,10)->notNull()->defaultValue(0),
            'tax_rate_value' => $this->tinyInteger(),
            'discount' => $this->decimal(10,6)->notNull()->defaultValue(0),
            'markup' => $this->decimal(10,6)->notNull()->defaultValue(0),
            'price_base' => $this->decimal(22,2)->notNull()->defaultValue(0)->comment('Цена до применения скидки или наценки'),
            'price_one' => $this->decimal(22,2)->notNull()->defaultValue(0)->comment('Цена за единицу'),
            'amount' => $this->decimal(22,2)->notNull()->defaultValue(0),
            'base_amount' => $this->decimal(22,2)->notNull()->defaultValue(0),
            'nds_amount' => $this->decimal(22,2)->notNull()->defaultValue(0)->comment('Сумма НДС'),
            'weight' => $this->decimal(22,4)->notNull()->defaultValue(0)->comment('Вес, кг'),
            'volume' => $this->decimal(22,4)->notNull()->defaultValue(0)->comment('Объем, м'),
            'box_type' => $this->string(50),
            'place_count' => $this->string(50),
            'count_in_place' => $this->string(50),
            'mass_gross' => $this->string(50),
            'excise' => $this->tinyInteger(1),
            'excise_price' => $this->string(),
            'custom_declaration_number' => $this->string(),
            'country_id' => $this->integer(),
        ]);

        $this->addForeignKey(
            'FK_foreign_currency_invoice_order_foreign_currency_invoice',
            '{{%foreign_currency_invoice_order}}',
            'foreign_currency_invoice_id',
            '{{%foreign_currency_invoice}}',
            'id'
        );

        $this->addForeignKey(
            'FK_foreign_currency_invoice_order_product',
            '{{%foreign_currency_invoice_order}}',
            'product_id',
            '{{%product}}',
            'id'
        );

        $this->addForeignKey(
            'FK_foreign_currency_invoice_order_product_unit',
            '{{%foreign_currency_invoice_order}}',
            'unit_id',
            '{{%product_unit}}',
            'id'
        );

        $this->addForeignKey(
            'FK_foreign_currency_invoice_order_tax_rate',
            '{{%foreign_currency_invoice_order}}',
            'tax_rate_id',
            '{{%tax_rate}}',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%foreign_currency_invoice_order}}');
    }
}
