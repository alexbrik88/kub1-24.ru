<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190522_071708_alter_file_dir extends Migration
{
    public $table = 'file_dir';

    public function safeUp()
    {
        $this->addColumn($this->table, 'ord', $this->integer()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'ord');
    }
}
