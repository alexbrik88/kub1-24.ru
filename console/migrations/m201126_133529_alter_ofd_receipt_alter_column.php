<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201126_133529_alter_ofd_receipt_alter_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%ofd_receipt}}', 'id', $this->bigInteger()->unsigned()->notNull());
    }

    public function safeDown()
    {
        $this->alterColumn('{{%ofd_receipt}}', 'id', $this->bigInteger()->notNull()->unsigned() . ' AUTO_INCREMENT');
    }
}
