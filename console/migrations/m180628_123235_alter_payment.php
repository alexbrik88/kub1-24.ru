<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180628_123235_alter_payment extends Migration
{
    public $tServicePayment = 'service_payment';

    public function safeUp()
    {
        $this->alterColumn($this->tServicePayment, 'payment_for', "ENUM('subscribe', 'taxrobot', 'store_cabinet', 'out_invoice', 'odds', 'add_invoice') NOT NULL DEFAULT 'subscribe' AFTER [[company_id]]");
    }

    public function safeDown()
    {
        $this->alterColumn($this->tServicePayment, 'payment_for', "ENUM('subscribe', 'taxrobot', 'store_cabinet', 'out_invoice', 'odds') NOT NULL DEFAULT 'subscribe' AFTER [[company_id]]");
    }
}


