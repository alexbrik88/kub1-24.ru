<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210803_062036_alter_finance_plan_add_fk extends Migration
{
    public function safeUp()
    {
        $this->addForeignKey('FK_finance_plan_to_industry_type', 'finance_plan', 'industry_id', 'company_info_industry', 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_finance_plan_to_industry_type', 'finance_plan');
    }
}
