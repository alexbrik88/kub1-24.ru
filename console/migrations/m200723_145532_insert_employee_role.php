<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200723_145532_insert_employee_role extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%employee_role}}', [
            'id' => 13,
            'name' => 'Демо-сотрудник',
            'sort' => null,
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%employee_role}}', [
            'id' => 13,
        ]);
    }
}
