<?php

use yii\db\Schema;
use yii\db\Migration;

class m150416_083111_create_out_invoice_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%invoice}}', [
            'id' => Schema::TYPE_PK,
            'seller_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Продавец"',
            'out_invoice_status_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Статус"',
            'updated_status' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Дата возникновения текущего статуса"',
            'author_status_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Автор текущего статуса"',
            'created_at' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Дата создания"',
            'author_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Автор"',
            'date_invoice' => Schema::TYPE_DATE . ' DEFAULT NULL COMMENT "Дата счёта"',
            'number' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Основной номер счета"',
            'add_number' => Schema::TYPE_STRING . ' DEFAULT NULL COMMENT "Дополнительный номер счёта"',
            'invoice_number' => Schema::TYPE_STRING . ' DEFAULT NULL COMMENT "Номер исх. счёта"',
            'date_pay' => Schema::TYPE_DATETIME . ' DEFAULT NULL COMMENT "Оплатить до"',
            'seller_bank' => Schema::TYPE_STRING . ' DEFAULT NULL COMMENT "Наименование банка продавца"',
            'seller_inn' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "ИНН продавца"',
            'seller_kpp' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "КПП продавца"',
            'seller_egrip' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "ЕГРИП продавца"',
            'seller_bik' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "БИК продавца"',
            'seller_ks' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "к/с продавца"',
            'seller_ps' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "р/с продавца"',
            'seller_full_name' => Schema::TYPE_STRING . ' DEFAULT NULL COMMENT "Полное название продавца"',
            'seller_name' => Schema::TYPE_STRING . ' DEFAULT NULL COMMENT "Краткое название продавца"',
            'seller_phone' => Schema::TYPE_STRING . ' DEFAULT NULL COMMENT "Телефон продавца"',
            'seller_address' => Schema::TYPE_STRING . ' DEFAULT NULL COMMENT "Юридический адрес продавца"',
            'seller_boss_position' => Schema::TYPE_STRING . ' DEFAULT NULL COMMENT "Должность руководителя продавца"',
            'seller_boss_lname' => Schema::TYPE_STRING . ' DEFAULT NULL COMMENT "Фамилия руководителя продавца"',
            'seller_boss_name' => Schema::TYPE_STRING . ' DEFAULT NULL COMMENT "Имя руководителя продавца (инициал)"',
            'seller_boss_mname' => Schema::TYPE_STRING . ' DEFAULT NULL COMMENT "Отчество руководителя продавца (инициал)"',
            'seller_stamp' => Schema::TYPE_STRING . ' DEFAULT NULL COMMENT "Печать продавца"',
            'seller_signature' => Schema::TYPE_STRING . ' DEFAULT NULL COMMENT "Подпись руководителя продавца"',
            'seller_bookkeeper_lname' => Schema::TYPE_STRING . ' DEFAULT NULL COMMENT "Фамилия главного бухгалтера продавца"',
            'seller_bookkeeper_name' => Schema::TYPE_STRING . ' DEFAULT NULL COMMENT "Имя главного бухгалтера продавца (инициал)"',
            'seller_bookkeeper_mname' => Schema::TYPE_STRING . ' DEFAULT NULL COMMENT "Отчество главного бухгалтера продавца (инициал)"',
            'customer_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Покупатель"',
            'customer_full_name' => Schema::TYPE_STRING . ' DEFAULT NULL COMMENT "Полное название покупателя"',
            'customer_name' => Schema::TYPE_STRING . ' DEFAULT NULL COMMENT "Краткое название покупателя"',
            'customer_inn' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "ИНН покупателя"',
            'customer_kpp' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "КПП покупателя"',
            'customer_address' => Schema::TYPE_STRING . ' DEFAULT NULL COMMENT "Юридический адрес покупателя"',
            'customer_bank' => Schema::TYPE_STRING . ' DEFAULT NULL COMMENT "Наименование банка покупателя"',
            'customer_bik' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "БИК покупателя"',
            'customer_ks' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "к/с покупателя"',
            'customer_ps' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "р/с покупателя"',
            'type' => Schema::TYPE_INTEGER . '(1) NOT NULL COMMENT "Тип счёта"',
            'count' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Итого количество мест, штук"',
            'gross_mass' => Schema::TYPE_FLOAT . ' NOT NULL COMMENT "Итого масса брутто"',
            'price_without_nds' => Schema::TYPE_FLOAT . ' NOT NULL COMMENT "Сумма без учёта НДС"',
            'price_with_nds' => Schema::TYPE_FLOAT . ' NOT NULL COMMENT "Сумма НДС"',
            'total_price' => Schema::TYPE_FLOAT . ' NOT NULL COMMENT "Итого"',
            'avail_nds' => Schema::TYPE_INTEGER . '(1) NOT NULL COMMENT "Наличие строки «В т.ч. НДС»"',
            'including_nds' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "В т.ч. НДС"',
            'amount' => Schema::TYPE_FLOAT . ' NOT NULL COMMENT "Всего к оплате"',
            'amount_string' => Schema::TYPE_STRING . ' DEFAULT NULL COMMENT "Сумма прописью"',
            'pay_type_id' => Schema::TYPE_INTEGER . ' DEFAULT NULL COMMENT "Форма оплаты"',
            'amount_partial' => Schema::TYPE_FLOAT . ' NOT NULL COMMENT "Сумма частичной оплаты"',
            'web_url' => Schema::TYPE_STRING . ' DEFAULT NULL COMMENT "Web-страница счёта"',
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%invoice}}');
    }
}
