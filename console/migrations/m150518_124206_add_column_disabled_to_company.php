<?php

use yii\db\Schema;
use yii\db\Migration;

class m150518_124206_add_column_disabled_to_company extends Migration
{
    public function safeUp()
    {
        $this->addColumn('company', 'blocked', Schema::TYPE_BOOLEAN . ' COMMENT "0 - активна, 1 - заблокирована"');
    }
    
    public function safeDown()
    {
        $this->dropColumn('company', 'blocked');
    }
}
