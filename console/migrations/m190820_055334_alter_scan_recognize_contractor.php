<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190820_055334_alter_scan_recognize_contractor extends Migration
{
    public function safeUp()
    {
        $this->addColumn('scan_recognize_contractor', 'contractor_id', $this->integer()->after('scan_recognize_document_id'));
        $this->addColumn('scan_recognize_product', 'product_id', $this->integer()->after('scan_recognize_document_id'));
        
        $this->addForeignKey('FK_scan_recognize_contractor_to_contractor', 'scan_recognize_contractor', 'contractor_id', 'contractor', 'id', 'CASCADE');
        $this->addForeignKey('FK_scan_recognize_product_to_product', 'scan_recognize_product', 'product_id', 'product', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_scan_recognize_contractor_to_contractor', 'scan_recognize_contractor');
        $this->dropForeignKey('FK_scan_recognize_product_to_product', 'scan_recognize_product');

        $this->dropColumn('scan_recognize_contractor', 'contractor_id');
        $this->dropColumn('scan_recognize_product', 'product_id');
    }
}