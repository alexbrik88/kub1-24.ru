<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211008_085205_add_registration_page_type extends Migration
{
    public $tableName = '{{%registration_page_type}}';

    public function safeUp()
    {
        $this->insert($this->tableName, [
            'id' => 48,
            'name' => 'Согласование платежей',
        ]);
    }

    public function safeDown()
    {
        $this->delete($this->tableName, ['id' => 48]);
    }
}
