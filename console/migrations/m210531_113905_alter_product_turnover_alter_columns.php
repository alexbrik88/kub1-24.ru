<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210531_113905_alter_product_turnover_alter_columns extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%product_turnover}}', 'invoice_id', $this->integer()->null());
        $this->alterColumn('{{%product_turnover}}', 'contractor_id', $this->integer()->null());
    }

    public function safeDown()
    {
        $this->alterColumn('{{%product_turnover}}', 'invoice_id', $this->integer()->notNull());
        $this->alterColumn('{{%product_turnover}}', 'contractor_id', $this->integer()->notNull());
    }
}
