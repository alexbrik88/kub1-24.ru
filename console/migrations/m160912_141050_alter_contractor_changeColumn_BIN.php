<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160912_141050_alter_contractor_changeColumn_BIN extends Migration
{
    public $tContractor = 'contractor';

    public function safeUp()
    {
        $this->alterColumn($this->tContractor, 'BIN', $this->char(15)->notNull());
    }
    
    public function safeDown()
    {
        $this->alterColumn($this->tContractor, 'BIN', $this->char(13)->notNull());
    }
}


