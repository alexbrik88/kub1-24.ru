<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180320_085317_update_company extends Migration
{
    public function safeUp()
    {
        $this->execute("
            UPDATE {{company}}
            SET {{company}}.[[name_full]] = CONCAT('\"', {{company}}.[[name_full]], '\"')
            WHERE {{company}}.[[name_full]] NOT LIKE '%\"%' AND {{company}}.[[company_type_id]] <> 1
        ");
        $this->execute("
            UPDATE {{company}}
            SET {{company}}.[[name_short]] = CONCAT('\"', {{company}}.[[name_short]], '\"')
            WHERE {{company}}.[[name_short]] NOT LIKE '%\"%' AND {{company}}.[[company_type_id]] <> 1
        ");
    }

    public function safeDown()
    {
        $this->execute("
            UPDATE {{company}}
            SET {{company}}.[[name_full]] = TRIM(BOTH '\"' FROM {{company}}.[[name_full]])
            WHERE {{company}}.[[name_full]] LIKE '\"%\"' AND {{company}}.[[company_type_id]] <> 1
        ");
        $this->execute("
            UPDATE {{company}}
            SET {{company}}.[[name_short]] = TRIM(BOTH '\"' FROM {{company}}.[[name_short]])
            WHERE {{company}}.[[name_short]] LIKE '\"%\"' AND {{company}}.[[company_type_id]] <> 1
        ");
    }
}
