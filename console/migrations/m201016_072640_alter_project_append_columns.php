<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201016_072640_alter_project_append_columns extends Migration
{
    public $tableName = 'project';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'addition_number', $this->string(32)->after('number'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'addition_number');
    }
}
