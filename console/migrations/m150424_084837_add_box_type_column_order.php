<?php

use yii\db\Schema;
use yii\db\Migration;

class m150424_084837_add_box_type_column_order extends Migration
{
    public function safeUp()
    {
        $this->addColumn('order', 'box_type', Schema::TYPE_STRING . '(45) NULL');
    }
    
    public function safeDown()
    {
        $this->dropColumn('order', 'box_type');
    }
}
