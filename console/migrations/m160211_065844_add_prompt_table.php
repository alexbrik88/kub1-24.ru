<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160211_065844_add_prompt_table extends Migration
{
    public $tPrompt = 'prompt';

    public function safeUp()
    {
        $this->createTable($this->tPrompt, [
            'id' => $this->primaryKey(11),
            'status' => $this->boolean()->notNull(),
            'page_type_id' => $this->integer(11)->notNull(),
            'for_whom_prompt_id' => $this->integer(11)->notNull(),
            'video_link' => $this->string(),
            'title' => $this->string(255),
            'text' => $this->text(),
        ]);
    }
    
    public function safeDown()
    {
        $this->dropTable($this->tPrompt);
    }
}


