<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190529_070240_alter_invoice_add_column extends Migration
{
    public $tInvoice = 'invoice';
    public $tCreatedType = 'create_type';

    public function safeUp()
    {
        $this->addColumn($this->tInvoice, 'create_type_id', $this->integer()->notNull()->defaultValue(1));
        $this->addForeignKey('FK_invoice_to_create_type', $this->tInvoice, 'create_type_id', $this->tCreatedType, 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_invoice_to_create_type', $this->tInvoice);
        $this->dropColumn($this->tInvoice, 'create_type_id');
    }
}
