<?php

use console\components\db\Migration;
use yii\db\Expression;
use yii\db\Schema;

class m201029_074631_alter_task_append_project_id extends Migration
{
    public $tableName = 'crm_task';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'project_id', $this->integer(11)
            ->after('employee_id')
            ->defaultValue(new Expression('NULL')));

        $this->createIndex($this->tableName . '_project_id_idx', $this->tableName, 'project_id');
    }

    public function safeDown()
    {
        try {
            $this->dropIndex($this->tableName . '_project_id_idx', $this->tableName);
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        $this->dropColumn($this->tableName, 'project_id');
    }
}
