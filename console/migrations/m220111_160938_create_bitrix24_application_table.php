<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%bitrix24_application}}`.
 */
class m220111_160938_create_bitrix24_application_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%bitrix24_application}}', [
            'id' => $this->primaryKey(),
            'code' => $this->string(25),
            'name' => $this->string(75),
            'short_descr' => $this->string(200),
            'demo_period' => $this->integer(),
            'prefix' => $this->string(30),
            'app_id' => $this->string(100),
            'active' => $this->boolean(),
            'app_secret' => $this->string(100),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%bitrix24_application}}');
    }
}
