<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200114_112025_alter_cash_bank_flows_create_index extends Migration
{
    public function safeUp()
    {
        $this->createIndex('date', '{{%cash_bank_flows}}', 'date');
        $this->createIndex('recognition_date', '{{%cash_bank_flows}}', 'recognition_date');
    }

    public function safeDown()
    {
        $this->dropIndex('date', '{{%cash_bank_flows}}');
        $this->dropIndex('recognition_date', '{{%cash_bank_flows}}');
    }
}
