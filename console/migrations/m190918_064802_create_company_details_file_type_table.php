<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%company_details_file_type}}`.
 */
class m190918_064802_create_company_details_file_type_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%company_details_file_type}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);

        $this->batchInsert('{{%company_details_file_type}}', ['id', 'name'], [
            [1, 'Логотип компании'],
            [2, 'Печать компании'],
            [3, 'Подпись руководителя'],
            [4, 'Подпись главного бухгалтера'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%company_details_file_type}}');
    }
}
