<?php

use yii\db\Migration;

/**
 * Handles the creation of table `company_site`.
 */
class m180329_180058_create_company_site_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('company_site', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'url' => $this->string()->notNull(),
            'created_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('FK_companySite_company', 'company_site', 'company_id', 'company', 'id', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('company_site');
    }
}
