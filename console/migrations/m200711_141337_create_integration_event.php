<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200711_141337_create_integration_event extends Migration
{
    public function safeUp()
    {
        $this->createTable('integration_events', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer(11)->unsigned()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'integration_type' => $this->tinyInteger(3)->unsigned()->notNull(),
            'event_type' => $this->tinyInteger(3)->unsigned()->notNull(),
            'message' => $this->text()->notNull(),
        ]);

        $this->addForeignKey('FK_integration_events_to_company', 'integration_events', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_integration_events_to_company', 'integration_events');
        $this->dropTable('integration_events');
    }
}
