<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211110_165941_alter_olap_profit_loss_cache extends Migration
{
    public function safeUp()
    {
        $table = 'olap_profit_and_loss_cache';

        $this->addColumn($table, 'flows', $this->integer()->defaultValue(0)->after('hash'));
        $this->addColumn($table, 'docs', $this->integer()->defaultValue(0)->after('hash'));
    }

    public function safeDown()
    {
        $table = 'olap_profit_and_loss_cache';

        $this->dropColumn($table, 'docs');
        $this->dropColumn($table, 'flows');
    }
}
