<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211029_165855_add_invoice_expenditure_items extends Migration
{
    public function safeUp()
    {
        $this->execute("
            INSERT `invoice_expenditure_item` (`id`, `company_id`, `name`, `is_visible`, `is_prepayment`, `can_be_controlled`)
            VALUES (101, NULL, 'Приобретение НМА', 0, 0, 1)
            ON DUPLICATE KEY UPDATE
                `company_id` = VALUES(`company_id`),
                `name` = VALUES(`name`),
                `is_visible` = VALUES(`is_visible`),
                `is_prepayment` = VALUES(`is_prepayment`),
                `can_be_controlled` = VALUES(`can_be_controlled`);
        ");

        $this->execute("
            DELETE FROM `expense_item_flow_of_funds` WHERE `expense_item_id` IN (101);
        ");

        try {
            $companiesIds = (new \yii\db\Query())
                ->select('company_id')
                ->distinct()
                ->from('expense_item_flow_of_funds')
                ->column();

            if ($companiesIds) {
                foreach ($companiesIds as $companyId) {
                    $this->batchInsert('expense_item_flow_of_funds', ['company_id', 'expense_item_id', 'is_visible', 'is_prepayment'], [
                        [$companyId, 101, 0, 0],
                    ]);
                }
            }
        } catch (Exception $e) {
            echo "\nNot added into expense_item_flow_of_funds!";
        }
    }

    public function safeDown()
    {
        $this->execute("
            DELETE FROM `expense_item_flow_of_funds` WHERE `expense_item_id` IN (101);
        ");
        $this->execute("
            DELETE FROM `invoice_expenditure_item` WHERE `id` IN (101);
        ");
    }
}
