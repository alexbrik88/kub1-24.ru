<?php

use yii\db\Migration;

class m210323_193732_alter_acquiring_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%acquiring}}';

    /**
     * @inheritDoc
     */
    public function up()
    {
        $timestampType = $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP');
        $this->dropColumn(self::TABLE_NAME, 'username');
        $this->dropColumn(self::TABLE_NAME, 'password');
        $this->addColumn(self::TABLE_NAME, 'created_at', $timestampType);
        $this->addColumn(self::TABLE_NAME, 'updated_at', $timestampType);
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        return false;
    }
}
