<?php

use yii\db\Migration;

/**
 * Handles the creation of table `store_user_role`.
 */
class m180222_111250_create_store_user_role extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable('store_user_role', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);

        $this->batchInsert('store_user_role', ['id', 'name'], [
            [1, 'Руководитель'],
            [2, 'Менеджер'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable('store_user_role');
    }
}
