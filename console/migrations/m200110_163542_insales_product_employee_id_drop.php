<?php

use console\components\db\Migration;

class m200110_163542_insales_product_employee_id_drop extends Migration
{
    const TABLE = '{{%insales_product}}';

    public function safeUp()
    {
        $this->dropForeignKey('insales_product_employee_id', self::TABLE);
        $this->dropIndex('insales_product_sku', self::TABLE);
        $this->dropIndex('insales_product_barcode', self::TABLE);
        $this->dropColumn(self::TABLE, 'employee_id');
        $this->createIndex('insales_product_sku', self::TABLE, 'sku', true);
        $this->createIndex('insales_product_barcode', self::TABLE, 'barcode', true);
    }

    public function safeDown()
    {
        $this->delete(self::TABLE);
        $this->addColumn(self::TABLE, 'employee_id', $this->string(11)->notNull()->after('id')->comment('ID клиента КУБ'));
        $this->addForeignKey(
            'insales_product_employee_id', self::TABLE, 'employee_id',
            '{{%employee}}', 'id',
            'CASCADE', 'CASCADE'
        );
        $this->createIndex('insales_product_sku', self::TABLE, ['employee_id', 'sku'], true);
        $this->createIndex('insales_product_barcode', self::TABLE, ['employee_id', 'barcode'], true);
    }
}
