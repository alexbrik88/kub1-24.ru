<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211028_084528_alter_config_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_config', 'finance_model_amortization', $this->tinyInteger(1)->notNull()->defaultValue(0));
        $this->addColumn('user_config', 'finance_model_ebit', $this->tinyInteger(1)->notNull()->defaultValue(0));
        $this->addColumn('user_config', 'finance_model_percent_received', $this->tinyInteger(1)->notNull()->defaultValue(0));
        $this->addColumn('user_config', 'finance_model_percent_paid', $this->tinyInteger(1)->notNull()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('user_config', 'finance_model_amortization');
        $this->dropColumn('user_config', 'finance_model_ebit');
        $this->dropColumn('user_config', 'finance_model_percent_received');
        $this->dropColumn('user_config', 'finance_model_percent_paid');
    }
}
