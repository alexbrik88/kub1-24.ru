<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201221_105728_alter_contractor_account_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{contractor_account}}', 'is_foreign_bank', $this->boolean()->notNull()->defaultValue(false));

        $this->execute('
            UPDATE {{contractor_account}}
            LEFT JOIN {{contractor}} ON {{contractor}}.[[id]] = {{contractor_account}}.[[contractor_id]]
            SET {{contractor_account}}.[[is_foreign_bank]] = IF({{contractor}}.[[face_type]]=2, 1, 0)
        ');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%contractor_account}}', 'is_foreign_bank');
    }
}
