<?php

use console\components\db\Migration;
use yii\db\Schema;

class m220131_151145_alter_bitrix24_widget_invoice_alter_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%bitrix24_widget_invoice}}', 'lead_id', $this->integer()->null());
    }

    public function safeDown()
    {
        $this->alterColumn('{{%bitrix24_widget_invoice}}', 'lead_id', $this->integer()->notNull());
    }
}
