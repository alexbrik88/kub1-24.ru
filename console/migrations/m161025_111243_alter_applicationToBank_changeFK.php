<?php

use console\components\db\Migration;
use yii\db\Schema;

class m161025_111243_alter_applicationToBank_changeFK extends Migration
{
    public $tApplicationToBank = 'application_to_bank';
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->dropForeignKey($this->tApplicationToBank . '_company_id', $this->tApplicationToBank);
        $this->dropForeignKey($this->tApplicationToBank . '_bank_id', $this->tApplicationToBank);

        $this->addForeignKey($this->tApplicationToBank . '_company_id', $this->tApplicationToBank, 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey($this->tApplicationToBank . '_bank_id', $this->tApplicationToBank, 'bank_id', 'bank', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey($this->tApplicationToBank . '_company_id', $this->tApplicationToBank);
        $this->dropForeignKey($this->tApplicationToBank . '_bank_id', $this->tApplicationToBank);

        $this->addForeignKey($this->tApplicationToBank . '_company_id', $this->tApplicationToBank, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tApplicationToBank . '_bank_id', $this->tApplicationToBank, 'bank_id', 'bank', 'id', 'RESTRICT', 'CASCADE');
    }
}


