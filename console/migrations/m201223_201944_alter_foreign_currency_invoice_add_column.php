<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201223_201944_alter_foreign_currency_invoice_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%foreign_currency_invoice}}', 'checking_accountant_id', $this->integer()->after('contractor_id'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%foreign_currency_invoice}}', 'checking_accountant_id');
    }
}
