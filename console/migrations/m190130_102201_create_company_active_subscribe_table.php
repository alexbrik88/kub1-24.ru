<?php

use yii\db\Migration;

/**
 * Handles the creation of table `company_active_subscribe`.
 */
class m190130_102201_create_company_active_subscribe_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('company_active_subscribe', [
            'company_id' => $this->integer()->notNull(),
            'tariff_group_id' => $this->integer()->notNull(),
            'subscribe_id' => $this->integer()->notNull()
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', 'company_active_subscribe', ['company_id', 'tariff_group_id']);

        $this->addForeignKey(
            'FK_company_active_subscribe_company',
            'company_active_subscribe', 'company_id',
            'company', 'id'
        );
        $this->addForeignKey(
            'FK_company_active_subscribe_tariff_group',
            'company_active_subscribe', 'tariff_group_id',
            'service_subscribe_tariff_group', 'id'
        );
        $this->addForeignKey(
            'FK_company_active_subscribe_subscribe',
            'company_active_subscribe', 'subscribe_id',
            'service_subscribe', 'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('company_active_subscribe');
    }
}
