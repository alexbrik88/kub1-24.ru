<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200928_153410_alter_invoice_add_not_for_bookkeping extends Migration
{
    public $tableName = 'invoice';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'not_for_bookkeeping', $this->integer(1)->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'not_for_bookkeeping');
    }
}
