<?php

use yii\db\Migration;

class m200719_193343_create_acquiring_table extends Migration
{

    public function safeUp()
    {
        $this->createTable('{{%acquiring}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'type' => $this->tinyInteger(3)->unsigned()->notNull(),
            'identifier' => $this->string()->notNull(),
            'username' => $this->string()->notNull(),
            'password' => $this->text()->notNull(),
        ]);

        $this->addForeignKey('FK_acquiring_to_company', 'acquiring', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_acquiring_to_company', 'acquiring');
        $this->dropTable('{{%acquiring}}');
    }
}
