<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190603_084154_alter_table_file_add_column extends Migration
{
    public $table = 'file';

    public function safeUp()
    {
        Yii::$app->db->createCommand('SET session wait_timeout=6000')->execute();
        Yii::$app->db->createCommand('SET session interactive_timeout=6000')->execute();

        $this->addColumn($this->table, 'filesize', $this->integer()->notNull()->defaultValue(0));

        $files = \common\models\file\File::find()->select('id')->column();
        foreach ($files as $fileId) {
            $fileModel = \common\models\file\File::findOne($fileId);
            $file = $fileModel->getUploadPath() . DIRECTORY_SEPARATOR . $fileModel->id;
            if (is_file($file))
                $fileModel->updateAttributes(['filesize' => filesize($file)]);
        }
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'filesize');
    }
}
