<?php

use yii\db\Schema;
use yii\db\Migration;

class m150429_092638_alter_Order_changeColumn_ProductId extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('order', 'product_id', Schema::TYPE_INTEGER . ' NULL');
        $this->addForeignKey('fk_order_to_product', 'order', 'product_id', 'product', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_order_to_product', 'order');
        $this->alterColumn('order', 'product_id', Schema::TYPE_INTEGER . ' NOT NULL');
    }
}
