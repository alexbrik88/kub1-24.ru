<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181203_182420_add_loading_and_unloading_data_to_logistics_request extends Migration
{
    public $tableName = 'logistics_request_loading_and_unloading';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'logistics_request_id' => $this->integer()->notNull(),
            'type' => $this->boolean()->defaultValue(false),
            'date' => $this->date()->defaultValue(null),
            'time' => $this->time()->defaultValue(null),
            'city' => $this->string()->defaultValue(null),
            'address' => $this->string()->defaultValue(null),
            'contact_person' => $this->string()->defaultValue(null),
            'method' => $this->string()->defaultValue(null),
            'goods' => $this->string()->defaultValue(null),
            'weight' => $this->string()->defaultValue(null),
            'volume' => $this->string()->defaultValue(null),
            'package' => $this->string()->defaultValue(null),
        ]);
        $this->addCommentOnColumn($this->tableName, 'type', '0 - Погрузка, 1 - Разгрузка');

        $this->addForeignKey($this->tableName . '_logistics_request_id', $this->tableName, 'logistics_request_id', 'logistics_request', 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_logistics_request_id', $this->tableName);

        $this->dropTable($this->tableName);
    }
}


