<?php

use yii\db\Schema;
use yii\db\Migration;

class m150525_065318_create_LogEvent extends Migration
{
    public function safeUp()
    {
        $this->createTable('log_event', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . '(45) NOT NULL',
        ]);

        $this->batchInsert('log_event', ['id', 'name',], [
            [1, 'создано'],
            [2, 'удалено'],
            [3, 'изменено'],
            [4, 'статус изменён'],
        ]);
    }
    
    public function safeDown()
    {
        $this->dropTable('log_event');
    }
}
