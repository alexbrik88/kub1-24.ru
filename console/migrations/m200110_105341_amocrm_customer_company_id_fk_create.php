<?php

use console\components\db\Migration;

class m200110_105341_amocrm_customer_company_id_fk_create extends Migration
{
    const TABLE = '{{%amocrm_customer}}';

    public function up()
    {
        $this->addForeignKey(
            'amocrm_customer_company_id', self::TABLE, 'company_id',
            '{{%company}}', 'id',
            'CASCADE', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey('amocrm_customer_company_id', self::TABLE);
    }
}
