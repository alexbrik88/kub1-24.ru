<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201221_135628_alter_contractor_account_alter_column extends Migration
{
    public function safeUp()
    {
        $this->update('{{contractor_account}}', [
            'currency_id' => '643',
        ], [
            'or',
            ['currency_id' => null],
            ['currency_id' => ''],
        ]);

        $this->alterColumn('{{contractor_account}}', 'currency_id', $this->char(3)->notNull()->defaultValue('643'));
    }

    public function safeDown()
    {
        $this->alterColumn('{{contractor_account}}', 'currency_id', $this->char(3));
    }
}
