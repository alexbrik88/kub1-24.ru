<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211115_080313_alter_cash_flow_linkage_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cash_flow_linkage}}', 'then_description', $this->string()->notNull()->after('then_amount_diff'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%cash_flow_linkage}}', 'then_description');
    }
}
