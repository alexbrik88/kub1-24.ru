<?php

use console\components\db\Migration;
use yii\db\Schema;

class m220124_133125_alter_cash_bank_flow_to_invoice_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cash_bank_flow_to_invoice', 'tin_child_amount', $this->bigInteger());
    }

    public function safeDown()
    {
        $this->dropColumn('cash_bank_flow_to_invoice', 'tin_child_amount');
    }
}
