<?php

use yii\db\Migration;

class m200714_220348_create_yandex_cash_identity_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%yandex_cash_identity}}';

    /** @var string[] */
    private const PK_COLUMNS = ['company_id', 'employee_id'];

    /**
     * @inheritDoc
     */
    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'company_id' => $this->integer()->notNull(),
            'employee_id' => $this->integer()->notNull(),
            'account_id' => $this->bigInteger()->null(),
            'access_token' => $this->string(64)->notNull(),
            'refresh_token' => $this->string(128)->notNull(),
            'expired_at' => $this->timestamp()->notNull()->defaultValue(0),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->addPrimaryKey('pk_identity', self::TABLE_NAME, self::PK_COLUMNS);
        $this->createIndex('ck_account_id', self::TABLE_NAME, ['account_id']);
        $this->addForeignKey(
            'fk_yandex_shop__employee_company',
            self::TABLE_NAME,
            self::PK_COLUMNS,
            '{{%employee_company}}',
            self::PK_COLUMNS,
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
