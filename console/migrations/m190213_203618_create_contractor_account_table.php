<?php

use yii\db\Migration;

/**
 * Handles the creation of table `contractor_account`.
 */
class m190213_203618_create_contractor_account_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('contractor_account', [
            'id' => $this->primaryKey(),
            'contractor_id' => $this->integer()->notNull(),
            'rs' => $this->string(50),
            'bank_name' => $this->string(),
            'bik' => $this->string(50),
            'ks' => $this->string(50),
            'is_main' => $this->boolean()->notNull(),
            'created_at' => $this->integer(),
        ]);

        $this->addForeignKey('FK_contractor_account_contractor', 'contractor_account', 'contractor_id', 'contractor', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('contractor_account');
    }
}
