<?php

use console\components\db\Migration;
use yii\db\Schema;

class m161226_104427_add_has_chief_patronymic_to_company extends Migration
{
    public $tCompany = 'company';

    public function safeUp()
    {
        $this->addColumn($this->tCompany, 'has_chief_patronymic', $this->boolean());
        $this->addColumn($this->tCompany, 'has_chief_accountant_patronymic', $this->boolean());
    }

    public function safeDown()
    {
        $this->dropColumn($this->tCompany, 'has_chief_patronymic', $this->boolean());
        $this->dropColumn($this->tCompany, 'has_chief_accountant_patronymic', $this->boolean());
    }
}


