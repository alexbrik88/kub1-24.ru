<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191119_111856_add_column_to_recognized_items extends Migration
{
    public function safeUp()
    {
        $this->addColumn('scan_recognize_contractor', 'company_id', $this->integer()->after('scan_recognize_document_id'));
        $this->addColumn('scan_recognize_product', 'company_id', $this->integer()->after('scan_recognize_document_id'));

        $this->addForeignKey('scan_recognize_contractor_company_id', 'scan_recognize_contractor', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('scan_recognize_product_company_id', 'scan_recognize_product', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('scan_recognize_contractor_company_id', 'scan_recognize_contractor');
        $this->dropForeignKey('scan_recognize_product_company_id', 'scan_recognize_product');

        $this->dropColumn('scan_recognize_contractor', 'company_id');
        $this->dropColumn('scan_recognize_product', 'company_id');
    }
}