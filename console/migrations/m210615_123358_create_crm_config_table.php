<?php

use yii\db\Migration;

class m210615_123358_create_crm_config_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%crm_config}}';

    /**
     * @inheritDoc
     */
    public function up(): void
    {
        $this->createTable(self::TABLE_NAME, [
            'company_id' => $this->integer()->notNull(),
            'show_customers' => $this->boolean()->notNull()->defaultValue(false),
        ]);

        $this->addPrimaryKey('pk_company_id', self::TABLE_NAME, ['company_id']);
        $this->addForeignKey(
            'fk_crm_config__company',
            self::TABLE_NAME,
            ['company_id'],
            '{{%company}}',
            ['id'],
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritDoc
     */
    public function down(): void
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
