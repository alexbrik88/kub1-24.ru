<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210806_070123_alter_cash_bank_foreign_currency_flows_add_column extends Migration
{
    public function safeUp()
    {
        // $this->addColumn('cash_bank_foreign_currency_flows', 'account_id', $this->integer()->after('company_id'));
        $this->execute('
            UPDATE `cash_bank_foreign_currency_flows` `t`
            LEFT JOIN `checking_accountant` `a` ON `a`.`company_id` = `t`.`company_id` AND `a`.`rs` = `t`.`rs` 
            SET `t`.`account_id` = `a`.`id`
        ');
    }

    public function safeDown()
    {
        // $this->dropColumn('cash_bank_foreign_currency_flows', 'account_id');
    }
}
