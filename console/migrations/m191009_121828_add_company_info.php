<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191009_121828_add_company_info extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('company_info_industry', [
            'id' => $this->primaryKey(),
            'name' => $this->string()
        ], $tableOptions);

        $this->createTable('company_info_site', [
            'id' => $this->primaryKey(),
            'name' => $this->string()
        ], $tableOptions);

        $this->addColumn('company', 'info_industry_id', $this->integer());
        $this->addColumn('company', 'info_site_id', $this->integer());
        $this->addColumn('company', 'info_employers_count', $this->integer(4));
        $this->addColumn('company', 'info_sellers_count', $this->integer(4));
        $this->addColumn('company', 'info_has_shop', $this->boolean());
        $this->addColumn('company', 'info_has_storage', $this->boolean());
        $this->addColumn('company', 'info_shops_count', $this->integer(4));

        $this->addForeignKey('FK_company_to_info_industry', 'company', 'info_industry_id', 'company_info_industry', 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey('FK_company_to_info_site', 'company', 'info_site_id', 'company_info_site', 'id', 'SET NULL', 'CASCADE');

        // TEMP DATA
        $this->batchInsert('company_info_industry', ['id', 'name'], [
            [1, 'Услуги']
        ]);

        // TEMP DATA
        $this->batchInsert('company_info_site', ['id', 'name'], [
            [1, 'Лендинг']
        ]);
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_company_to_info_industry', 'company');
        $this->dropForeignKey('FK_company_to_info_site', 'company');

        $this->dropColumn('company', 'info_industry_id');
        $this->dropColumn('company', 'info_site_id');
        $this->dropColumn('company', 'info_employers_count');
        $this->dropColumn('company', 'info_sellers_count');
        $this->dropColumn('company', 'info_has_shop');
        $this->dropColumn('company', 'info_has_storage');
        $this->dropColumn('company', 'info_shops_count');

        $this->dropTable('company_info_industry');
        $this->dropTable('company_info_site');
    }
}
