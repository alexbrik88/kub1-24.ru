<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181111_105059_alter_user_config_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn(
            'user_config',
            'agreement_scan',
            $this->boolean()->notNull()->defaultValue(true)
        );
        $this->addColumn(
            'user_config',
            'agreement_responsible_employee_id',
            $this->boolean()->notNull()->defaultValue(false)
        );
        $this->addCommentOnColumn('user_config', 'agreement_scan', 'Показ колонки "Скан" в списке договоров');
        $this->addCommentOnColumn('user_config', 'agreement_responsible_employee_id', 'Показ колонки "Ответственный" в списке договоров');
    }

    public function safeDown()
    {
        $this->dropColumn('user_config', 'agreement_scan');
        $this->dropColumn('user_config', 'agreement_responsible_employee_id');
    }
}


