<?php

use console\components\db\Migration;

class m190924_113105_create_evotor_employee extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{evotor_employee}}', [
            'id' => "BINARY(16) NOT NULL PRIMARY KEY COMMENT 'UUID Эвотора'",
            'employee_id' => $this->integer()->notNull()->comment('ID покупателя сайта'),
            'role' => "ENUM('ADMIN','CASHIER','MANUAL') NOT NULL COMMENT 'Роль сотрудника'",
            'name' => $this->string(100)->notNull()->comment('Имя'),
            'last_name' => $this->string(100)->comment('Фамилия'),
            'patronymic_name' => $this->string(100)->comment('Отчество'),
            'phone'=>$this->bigInteger()->unsigned()->comment('Номер телефона'),
            'stores' => $this->string(900)->comment('Список ID магазинов через запятую'),
            'created_at' => $this->integer()->unsigned()->notNull(),
            'updated_at' => $this->integer()->unsigned()->notNull(),
        ], "COMMENT 'Интеграция Эвотор: сотрудники'");
        $this->addForeignKey(
            'evotor_employee_employee_id', '{{evotor_employee}}', 'employee_id',
            '{{employee}}', 'id', 'CASCADE', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable('{{evotor_employee}}');

    }
}
