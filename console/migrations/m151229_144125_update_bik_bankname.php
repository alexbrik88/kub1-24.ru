<?php

use console\components\db\Migration;
use yii\db\Schema;

class m151229_144125_update_bik_bankname extends Migration
{
    public $tCompany = 'company';
    public $tContractor = 'contractor';

    public function safeUp()
    {
        $this->update($this->tCompany, [
            'bank_name' => new \yii\db\Expression('REPLACE (bank_name, "&quot;", "\"") '),
        ]);
        $this->update($this->tContractor, [
            'bank_name' => new \yii\db\Expression('REPLACE (bank_name, "&quot;", "\"") '),
        ]);
    }
    
    public function safeDown()
    {
        $this->update($this->tCompany, [
            'bank_name' => new \yii\db\Expression('REPLACE (bank_name, "\"", "&quot;") '),
        ]);
        $this->update($this->tContractor, [
            'bank_name' => new \yii\db\Expression('REPLACE (bank_name, "\"", "&quot;") '),
        ]);
    }
}


