<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180211_211756_add_discount_to_order_document extends Migration
{
    public $tableName = 'order_document';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'has_discount', $this->boolean()->defaultValue(false));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'has_discount');
    }
}


