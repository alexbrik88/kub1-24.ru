<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200928_143339_alter_invoice_add_can_add_sales_invoice extends Migration
{
    public $tableName = 'invoice';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'can_add_sales_invoice', $this->integer(1)->after('can_add_upd'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'can_add_sales_invoice');
    }
}
