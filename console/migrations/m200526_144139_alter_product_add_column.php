<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200526_144139_alter_product_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%product}}', 'is_import_1c', $this->boolean()->after('import_type_id')->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%product}}', 'is_import_1c');
    }
}
