<?php

use console\components\db\Migration;
use yii\db\Schema;
use yii\db\Expression;

class m180528_082615_add_status_to_payment_order extends Migration
{
    public $tPaymentOrder = 'payment_order';
    public $tPaymentOrderStatus = 'payment_order_status';

    public function safeUp()
    {
        $this->createTable($this->tPaymentOrderStatus, [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);

        $this->batchInsert($this->tPaymentOrderStatus, ['id', 'name'], [
            [1, 'Создано'],
            [2, 'Импортировано в банк'],
            [3, 'Распечатано'],
        ]);

        $this->addColumn($this->tPaymentOrder, 'payment_order_status_id', $this->integer()->defaultValue(1)->notNull());
        $this->addColumn($this->tPaymentOrder, 'payment_order_status_updated_at', $this->integer()->notNull());

        $this->update($this->tPaymentOrder, ['payment_order_status_updated_at' => new Expression('created_at')], ['payment_order_status_updated_at' => 0]);

        $this->addForeignKey($this->tPaymentOrder . '_payment_order_status_id', $this->tPaymentOrder, 'payment_order_status_id', $this->tPaymentOrderStatus, 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey($this->tPaymentOrder . '_payment_order_status_id', $this->tPaymentOrder);

        $this->dropColumn($this->tPaymentOrder, 'payment_order_status_id');
        $this->dropColumn($this->tPaymentOrder, 'payment_order_status_updated_at');

        $this->dropTable($this->tPaymentOrderStatus);
    }
}


