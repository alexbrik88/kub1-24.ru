<?php

use console\components\db\Migration;

class m200110_104423_amocrm_user_employee_id_drop extends Migration
{
    const TABLE = '{{%amocrm_user}}';

    public function safeUp()
    {
        $this->dropForeignKey('amocrm_user_employee_id', self::TABLE);
        $this->dropColumn(self::TABLE, 'employee_id');
    }

    public function safeDown()
    {
        $this->delete(self::TABLE);
        $this->addColumn(self::TABLE, 'employee_id', $this->string(11)->notNull()->after('id')->comment('ID клиента КУБ'));
        $this->addForeignKey(
            'amocrm_user_employee_id', self::TABLE, 'employee_id',
            '{{%employee}}', 'id',
            'CASCADE', 'CASCADE'
        );
    }
}
