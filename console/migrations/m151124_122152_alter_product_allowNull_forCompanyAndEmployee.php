<?php

use yii\db\Schema;
use yii\db\Migration;

class m151124_122152_alter_product_allowNull_forCompanyAndEmployee extends Migration
{
    public $tProduct = 'product';

    public function safeUp()
    {
        $this->dropForeignKey('product_to_employee_creator', $this->tProduct);
        $this->alterColumn($this->tProduct, 'creator_id', $this->integer()->defaultValue(null));
        $this->addForeignKey('product_to_employee_creator', 'product', 'creator_id', 'employee', 'id');

        $this->dropForeignKey('product_to_company', $this->tProduct);
        $this->alterColumn($this->tProduct, 'company_id', $this->integer()->defaultValue(null));
        $this->addForeignKey('product_to_company', 'product', 'company_id', 'company', 'id');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('product_to_employee_creator', $this->tProduct);
        $this->alterColumn($this->tProduct, 'creator_id', $this->integer()->notNull());
        $this->addForeignKey('product_to_employee_creator', 'product', 'creator_id', 'employee', 'id');

        $this->dropForeignKey('product_to_company', $this->tProduct);
        $this->alterColumn($this->tProduct, 'company_id', $this->integer()->notNull());
        $this->addForeignKey('product_to_company', 'product', 'company_id', 'company', 'id');
    }
}
