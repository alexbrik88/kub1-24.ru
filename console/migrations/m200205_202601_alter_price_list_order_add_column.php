<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200205_202601_alter_price_list_order_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%price_list_order}}', 'production_type', $this->tinyInteger()->notNull()->defaultValue(1)->comment('Тип продукции 1 - товар, 0 - услуга'));

        $this->execute('
            UPDATE price_list_order AS plo, product AS p 
            SET plo.production_type = p.production_type 
            WHERE plo.product_id = p.id
        ');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%price_list_order}}', 'production_type');
    }

}
