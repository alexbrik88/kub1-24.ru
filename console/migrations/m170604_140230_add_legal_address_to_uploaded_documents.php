<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170604_140230_add_legal_address_to_uploaded_documents extends Migration
{
    public $tableName = 'uploaded_documents';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'legal_address', $this->string()->defaultValue(null));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'legal_address');
    }
}


