<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160801_112858_update_contractor_id_in_cash extends Migration
{
    public $tBank = 'cash_bank_flows';
    public $tOrder = 'cash_order_flows';
    public $tEmoney = 'cash_emoney_flows';

    public $tTables = [
        'cash_bank_flows',
        'cash_order_flows',
        'cash_emoney_flows',
    ];

    public function safeUp()
    {
        $this->dropForeignKey('cash_bank_flows_contractor_id', $this->tBank);
        $this->dropForeignKey('cash_order_flows_contractor_id', $this->tOrder);
        $this->dropForeignKey('cash_emoney_flows_contractor_id', $this->tEmoney);

        foreach ($this->tTables as $tableName) {
            $this->alterColumn($tableName, 'contractor_id', $this->integer()->defaultValue(null));
        }
        $this->addForeignKey('cash_bank_flows_contractor_id', 'cash_bank_flows', 'contractor_id', 'contractor', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('cash_order_flows_contractor_id', 'cash_order_flows', 'contractor_id', 'contractor', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('cash_emoney_flows_contractor_id', 'cash_emoney_flows', 'contractor_id', 'contractor', 'id', 'RESTRICT', 'CASCADE');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('cash_bank_flows_contractor_id', $this->tBank);
        $this->dropForeignKey('cash_order_flows_contractor_id', $this->tOrder);
        $this->dropForeignKey('cash_emoney_flows_contractor_id', $this->tEmoney);

        foreach ($this->tTables as $tableName) {
            $this->alterColumn($tableName, 'contractor_id', $this->integer()->notNull());
        }
        $this->addForeignKey('cash_bank_flows_contractor_id', 'cash_bank_flows', 'contractor_id', 'contractor', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('cash_order_flows_contractor_id', 'cash_order_flows', 'contractor_id', 'contractor', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('cash_emoney_flows_contractor_id', 'cash_emoney_flows', 'contractor_id', 'contractor', 'id', 'RESTRICT', 'CASCADE');
    }
}


