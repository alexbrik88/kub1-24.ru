<?php

use yii\db\Schema;
use yii\db\Migration;

class m150525_135403_alter_Log_addColumn_message extends Migration
{
    public function safeUp()
    {
        $this->addColumn('log', 'message', Schema::TYPE_STRING . '(2048) NOT NULL DEFAULT "Сообщение не задано"');
    }
    
    public function safeDown()
    {
        $this->dropColumn('log', 'message');
    }
}
