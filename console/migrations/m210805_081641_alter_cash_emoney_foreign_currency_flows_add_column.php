<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210805_081641_alter_cash_emoney_foreign_currency_flows_add_column extends Migration
{
    public function up()
    {
        $this->addColumn('{{%cash_emoney_foreign_currency_flows}}', 'amount_rub', $this->bigInteger()->notNull()->after('amount'));

        $this->execute('
            UPDATE {{%cash_emoney_foreign_currency_flows}}
            LEFT JOIN {{%currency_rate}} ON {{%currency_rate}}.[[date]] = {{%cash_emoney_foreign_currency_flows}}.[[date]] AND {{%currency_rate}}.[[name]] = {{%cash_emoney_foreign_currency_flows}}.[[currency_name]]
            SET {{%cash_emoney_foreign_currency_flows}}.[[amount_rub]] = IF(
                {{%currency_rate}}.[[name]] IS NULL,
                0,
                ROUND({{%cash_emoney_foreign_currency_flows}}.[[amount]]/{{%currency_rate}}.[[amount]]*{{%currency_rate}}.[[value]])
            )
            WHERE {{%cash_emoney_foreign_currency_flows}}.[[currency_name]] <> "RUB"
        ');
    }

    public function down()
    {
        $this->dropColumn('{{%cash_emoney_foreign_currency_flows}}', 'amount_rub', $this->bigInteger());
    }
}
