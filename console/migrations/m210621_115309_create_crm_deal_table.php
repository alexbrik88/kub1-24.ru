<?php

use yii\db\Migration;

class m210621_115309_create_crm_deal_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%crm_deal}}';

    /**
     * @inheritDoc
     */
    public function up(): void
    {
        $this->createTable(self::TABLE_NAME, [
            'deal_id' => $this->bigPrimaryKey()->notNull(),
            'deal_type_id' => $this->bigInteger()->notNull(),
            'deal_stage_id' => $this->bigInteger()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'employee_id' => $this->integer()->notNull(),
            'contractor_id' => $this->integer()->notNull(),
            'currency_id' => $this->char(3)->notNull()->append('COLLATE utf8_unicode_ci'),
            'amount' => $this->bigInteger()->notNull(),
            'name' => $this->string()->notNull()->defaultValue(''),
            'comment' => $this->text()->notNull()->defaultValue(''),
            'staged_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->createIndex('ck_deal_type_id', self::TABLE_NAME, ['deal_type_id']);
        $this->createIndex('ck_deal_stage_id', self::TABLE_NAME, ['deal_stage_id']);
        $this->createIndex('ck_company_id', self::TABLE_NAME, ['company_id']);
        $this->createIndex('ck_company_id__employee_id', self::TABLE_NAME, ['company_id', 'employee_id']);
        $this->createIndex('ck_contractor_id', self::TABLE_NAME, ['contractor_id']);
        $this->createIndex('ck_currency_id', self::TABLE_NAME, ['currency_id']);

        $this->addForeignKey(
            'fk_deal__deal_type',
            self::TABLE_NAME,
            ['deal_type_id'],
            '{{%crm_deal_type}}',
            ['deal_type_id'],
            'RESTRICT',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_deal__deal_stage',
            self::TABLE_NAME,
            ['deal_stage_id'],
            '{{%crm_deal_stage}}',
            ['deal_stage_id'],
            'RESTRICT',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_deal__company',
            self::TABLE_NAME,
            ['company_id'],
            '{{%company}}',
            ['id'],
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_deal__employee_company',
            self::TABLE_NAME,
            ['company_id', 'employee_id'],
            '{{%employee_company}}',
            ['company_id', 'employee_id'],
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_deal__contractor',
            self::TABLE_NAME,
            ['company_id'],
            '{{%contractor}}',
            ['id'],
            'CASCADE',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_deal__currency',
            self::TABLE_NAME,
            ['currency_id'],
            '{{%currency}}',
            ['id'],
            'RESTRICT',
            'CASCADE'
        );
    }

    /**
     * @inheritDoc
     */
    public function down(): void
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
