<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210328_192148_alter_olap_documents_triggers_9 extends Migration
{
    const TABLE_PACKING_LIST = 'packing_list';
    const TABLE_UPD = 'upd';
    const TABLE_ACT = 'act';
    const TABLE_INVOICE = 'invoice';
    const REF_TABLE_INVOICE_UPD = 'invoice_upd';
    const REF_TABLE_INVOICE_ACT = 'invoice_act';

    const DOCUMENT_ACT = 1;
    const DOCUMENT_PACKING_LIST = 4;
    const DOCUMENT_UPD = 8;
    const DOCUMENT_INITIAL_BALANCE = 0;

    public function safeUp()
    {
        $this->createTriggersOnSalesInvoice();
        $this->createTriggersOnPackingList();
        $this->createTriggersOnUpd();
        $this->createTriggersOnAct();
        $this->createTriggersOnInvoice();
        $this->createTriggersOnContractor();
    }

    public function safeDown()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_order_packing_list`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_delete_order_packing_list`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_order_upd`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_delete_order_upd`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_order_act`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_delete_order_act`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_insert_packing_list`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_packing_list`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_delete_packing_list`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_insert_upd`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_upd`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_delete_upd`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_insert_act`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_act`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_delete_act`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_invoice`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_contractor`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_order_sales_invoice`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_delete_order_sales_invoice`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_insert_sales_invoice`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_sales_invoice`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_delete_sales_invoice`;');
    }

    private function createTriggersOnSalesInvoice()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_insert_sales_invoice`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_sales_invoice`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_delete_sales_invoice`');

        $this->execute('
            CREATE TRIGGER `olap_documents_insert_sales_invoice`
            AFTER INSERT ON `sales_invoice`
            FOR EACH ROW
            BEGIN
            
              INSERT INTO `olap_documents` (`company_id`, `contractor_id`, `by_document`, `id`, `type`, `date`, `amount`, `is_accounting`, `invoice_expenditure_item_id`, `invoice_income_item_id`) 
              
              SELECT 
                `invoice`.`company_id`,
                `invoice`.`contractor_id`,
                "16" AS `by_document`, 
                `document`.`id`, 
                `document`.`type`, 
                `document`.`document_date` AS `date`, 
                IF(NEW.`status_out_id` = 5, 0, NEW.`orders_sum`) AS `amount`,
                IF (`contractor`.`not_accounting`, 0, 1) AS is_accounting,
                `invoice`.`invoice_expenditure_item_id`,
                `invoice`.`invoice_income_item_id`
              FROM `sales_invoice` `document`
              LEFT JOIN `invoice` ON `invoice`.`id` = `document`.`invoice_id`  
              LEFT JOIN `contractor` ON `contractor`.`id` = `invoice`.`contractor_id`
              WHERE 
                `document`.`id` = NEW.`id`;
              
            END        
        ');

        $this->execute('
            CREATE TRIGGER `olap_documents_update_sales_invoice`
            AFTER UPDATE ON `sales_invoice`
            FOR EACH ROW
            BEGIN
            
              IF IFNULL(NEW.`status_out_id`, 1) <> IFNULL(OLD.`status_out_id`, 1) OR NEW.`document_date` <> OLD.`document_date`
              THEN
                  UPDATE `olap_documents` 
                  SET 
                    `date` = NEW.`document_date`,
                    `amount` = IF(NEW.`status_out_id` = 5, 0, `amount`) 
                  WHERE `olap_documents`.`by_document` = "16" AND `olap_documents`.`id` = NEW.`id` LIMIT 1;
              END IF;      
                      
            END
        ');

        $this->execute('
            CREATE TRIGGER `olap_documents_delete_sales_invoice`
            AFTER DELETE ON `sales_invoice`
            FOR EACH ROW
            BEGIN
            
              DELETE FROM `olap_documents` WHERE `by_document` = "16" AND `id` = OLD.`id` LIMIT 1;
              
            END  
        ');

        // ORDER_SALES_INVOICE
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_order_sales_invoice`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_delete_order_sales_invoice`');

        $this->execute('
            CREATE TRIGGER `olap_documents_update_order_sales_invoice`
            AFTER UPDATE ON `order_sales_invoice`
            FOR EACH ROW
            BEGIN
             
              DECLARE _amount BIGINT;
              SET _amount = (
                SELECT 
                  SUM(IF(`document`.`status_out_id` = 5, 0, IF(`document`.`type` = 2, `order`.`selling_price_with_vat`, `order`.`purchase_price_with_vat`) * `order_doc`.`quantity`)) AS `amount`
                FROM `sales_invoice` `document`
                LEFT JOIN `order_sales_invoice` `order_doc` ON `document`.`id` = `order_doc`.`sales_invoice_id`
                LEFT JOIN `order` ON `order`.`id` = `order_doc`.`order_id`
                WHERE `document`.`id` = NEW.`sales_invoice_id` 
                GROUP BY `document`.`id`
              );
            
              UPDATE `olap_documents` 
              SET `amount` = _amount
              WHERE `olap_documents`.`by_document` = "16" AND `olap_documents`.`id` = NEW.`sales_invoice_id` LIMIT 1;           
              
            END   
        ');

        $this->execute('
            CREATE TRIGGER `olap_documents_delete_order_sales_invoice`
            AFTER DELETE ON `order_sales_invoice`
            FOR EACH ROW
            BEGIN
             
              DECLARE _amount BIGINT;
              SET _amount = (
                SELECT 
                  SUM(IF(`document`.`status_out_id` = 5, 0, IF(`document`.`type` = 2, `order`.`selling_price_with_vat`, `order`.`purchase_price_with_vat`) * `order_doc`.`quantity`)) AS `amount`
                FROM `sales_invoice` `document`
                LEFT JOIN `order_sales_invoice` `order_doc` ON `document`.`id` = `order_doc`.`sales_invoice_id`
                LEFT JOIN `order` ON `order`.`id` = `order_doc`.`order_id`
                WHERE `document`.`id` = OLD.`sales_invoice_id` 
                GROUP BY `document`.`id`
              );
            
              UPDATE `olap_documents` 
              SET `amount` = _amount
              WHERE `olap_documents`.`by_document` = "16" AND `olap_documents`.`id` = OLD.`sales_invoice_id` LIMIT 1;           
              
            END   
        ');
    }

    private function createTriggersOnPackingList()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_insert_packing_list`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_packing_list`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_delete_packing_list`');

        $this->execute('
            CREATE TRIGGER `olap_documents_insert_packing_list`
            AFTER INSERT ON `packing_list`
            FOR EACH ROW
            BEGIN
            
              INSERT INTO `olap_documents` (`company_id`, `contractor_id`, `by_document`, `id`, `type`, `date`, `amount`, `is_accounting`, `invoice_expenditure_item_id`, `invoice_income_item_id`) 
              
              SELECT 
                `invoice`.`company_id`,
                `invoice`.`contractor_id`,
                "4" AS `by_document`, 
                `document`.`id`, 
                `document`.`type`, 
                `document`.`document_date` AS `date`, 
                IF(NEW.`status_out_id` = 5, 0, NEW.`orders_sum`) AS `amount`,
                IF (`contractor`.`not_accounting`, 0, 1) AS is_accounting,
                `invoice`.`invoice_expenditure_item_id`,
                `invoice`.`invoice_income_item_id`
              FROM `packing_list` `document`
              LEFT JOIN `invoice` ON `invoice`.`id` = `document`.`invoice_id`  
              LEFT JOIN `contractor` ON `contractor`.`id` = `invoice`.`contractor_id`
              WHERE 
                `document`.`id` = NEW.`id`;
              
            END        
        ');

        $this->execute('
            CREATE TRIGGER `olap_documents_update_packing_list`
            AFTER UPDATE ON `packing_list`
            FOR EACH ROW
            BEGIN
            
              IF IFNULL(NEW.`status_out_id`, 1) <> IFNULL(OLD.`status_out_id`, 1) OR NEW.`document_date` <> OLD.`document_date`
              THEN
                  UPDATE `olap_documents` 
                  SET 
                    `date` = NEW.`document_date`,
                    `amount` = IF(NEW.`status_out_id` = 5, 0, `amount`) 
                  WHERE `olap_documents`.`by_document` = "4" AND `olap_documents`.`id` = NEW.`id` LIMIT 1;           
              END IF;
              
            END
        ');

        $this->execute('
            CREATE TRIGGER `olap_documents_delete_packing_list`
            AFTER DELETE ON `packing_list`
            FOR EACH ROW
            BEGIN
            
              DELETE FROM `olap_documents` WHERE `by_document` = "4" AND `id` = OLD.`id` LIMIT 1;
              
            END  
        ');

        // ORDER_PACKING_LIST
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_order_packing_list`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_delete_order_packing_list`');

        $this->execute('
            CREATE TRIGGER `olap_documents_update_order_packing_list`
            AFTER UPDATE ON `order_packing_list`
            FOR EACH ROW
            BEGIN
             
              DECLARE _amount BIGINT;
              SET _amount = (
                SELECT 
                  SUM(IF(`document`.`status_out_id` = 5, 0, IF(`document`.`type` = 2, `order`.`selling_price_with_vat`, `order`.`purchase_price_with_vat`) * `order_doc`.`quantity`)) AS `amount`
                FROM `packing_list` `document`
                LEFT JOIN `order_packing_list` `order_doc` ON `document`.`id` = `order_doc`.`packing_list_id`
                LEFT JOIN `order` ON `order`.`id` = `order_doc`.`order_id`
                WHERE `document`.`id` = NEW.`packing_list_id` 
                GROUP BY `document`.`id`
              );
            
              UPDATE `olap_documents` 
              SET `amount` = _amount
              WHERE `olap_documents`.`by_document` = "4" AND `olap_documents`.`id` = NEW.`packing_list_id` LIMIT 1;           
              
            END   
        ');

        $this->execute('
            CREATE TRIGGER `olap_documents_delete_order_packing_list`
            AFTER DELETE ON `order_packing_list`
            FOR EACH ROW
            BEGIN
             
              DECLARE _amount BIGINT;
              SET _amount = (
                SELECT 
                  SUM(IF(`document`.`status_out_id` = 5, 0, IF(`document`.`type` = 2, `order`.`selling_price_with_vat`, `order`.`purchase_price_with_vat`) * `order_doc`.`quantity`)) AS `amount`
                FROM `packing_list` `document`
                LEFT JOIN `order_packing_list` `order_doc` ON `document`.`id` = `order_doc`.`packing_list_id`
                LEFT JOIN `order` ON `order`.`id` = `order_doc`.`order_id`
                WHERE `document`.`id` = OLD.`packing_list_id` 
                GROUP BY `document`.`id`
              );
            
              UPDATE `olap_documents` 
              SET `amount` = _amount
              WHERE `olap_documents`.`by_document` = "4" AND `olap_documents`.`id` = OLD.`packing_list_id` LIMIT 1;           
              
            END   
        ');
    }

    private function createTriggersOnUpd()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_insert_upd`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_upd`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_delete_upd`');

        $this->execute('
            CREATE TRIGGER `olap_documents_insert_upd`
            AFTER INSERT ON `upd`
            FOR EACH ROW
            BEGIN
            
              INSERT INTO `olap_documents` (`company_id`, `contractor_id`, `by_document`, `id`, `type`, `date`, `amount`, `is_accounting`, `invoice_expenditure_item_id`, `invoice_income_item_id`) 
              
              SELECT 
                `invoice`.`company_id`,
                `invoice`.`contractor_id`,
                "8" AS `by_document`, 
                `document`.`id`, 
                `document`.`type`, 
                `document`.`document_date` AS `date`, 
                IF(NEW.`status_out_id` = 5, 0, NEW.`orders_sum`) AS `amount`,
                IF (`contractor`.`not_accounting`, 0, 1) AS is_accounting,
                `invoice`.`invoice_expenditure_item_id`,
                `invoice`.`invoice_income_item_id`                
              FROM `upd` `document`
              LEFT JOIN `invoice` ON `invoice`.`id` = `document`.`invoice_id`  
              LEFT JOIN `contractor` ON `contractor`.`id` = `invoice`.`contractor_id`
              WHERE 
                `document`.`id` = NEW.`id`;
              
            END        
        ');

        $this->execute('
            CREATE TRIGGER `olap_documents_update_upd`
            AFTER UPDATE ON `upd`
            FOR EACH ROW
            BEGIN
            
              IF IFNULL(NEW.`status_out_id`, 1) <> IFNULL(OLD.`status_out_id`, 1) OR NEW.`document_date` <> OLD.`document_date`
              THEN            
                  UPDATE `olap_documents` 
                  SET 
                    `date` = NEW.`document_date`,
                    `amount` = IF(NEW.`status_out_id` = 5, 0, `amount`)
                  WHERE `olap_documents`.`by_document` = "8" AND `olap_documents`.`id` = NEW.`id` LIMIT 1;
              END IF;           
              
            END   
        ');

        $this->execute('
            CREATE TRIGGER `olap_documents_delete_upd`
            AFTER DELETE ON `upd`
            FOR EACH ROW
            BEGIN
            
              DELETE FROM `olap_documents` WHERE `by_document` = "8" AND `id` = OLD.`id` LIMIT 1;
              
            END        
        ');

        // ORDER_UPD
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_order_upd`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_delete_order_upd`');

        $this->execute('
            CREATE TRIGGER `olap_documents_update_order_upd`
            AFTER UPDATE ON `order_upd`
            FOR EACH ROW
            BEGIN
             
              DECLARE _amount BIGINT;
              SET _amount = (
                SELECT 
                  SUM(IF(`document`.`status_out_id` = 5, 0, IF(`document`.`type` = 2, `order`.`selling_price_with_vat`, `order`.`purchase_price_with_vat`) * `order_doc`.`quantity`)) AS `amount`
                FROM `upd` `document`
                LEFT JOIN `order_upd` `order_doc` ON `document`.`id` = `order_doc`.`upd_id`
                LEFT JOIN `order` ON `order`.`id` = `order_doc`.`order_id`
                WHERE `document`.`id` = NEW.`upd_id` 
                GROUP BY `document`.`id`
              );
            
              UPDATE `olap_documents` 
              SET `amount` = _amount
              WHERE `olap_documents`.`by_document` = "8" AND `olap_documents`.`id` = NEW.`upd_id` LIMIT 1;           
              
            END   
        ');

        $this->execute('
            CREATE TRIGGER `olap_documents_delete_order_upd`
            AFTER DELETE ON `order_upd`
            FOR EACH ROW
            BEGIN
             
              DECLARE _amount BIGINT;
              SET _amount = (
                SELECT 
                  SUM(IF(`document`.`status_out_id` = 5, 0, IF(`document`.`type` = 2, `order`.`selling_price_with_vat`, `order`.`purchase_price_with_vat`) * `order_doc`.`quantity`)) AS `amount`
                FROM `upd` `document`
                LEFT JOIN `order_upd` `order_doc` ON `document`.`id` = `order_doc`.`upd_id`
                LEFT JOIN `order` ON `order`.`id` = `order_doc`.`order_id`
                WHERE `document`.`id` = OLD.`upd_id` 
                GROUP BY `document`.`id`
              );
            
              UPDATE `olap_documents` 
              SET `amount` = _amount
              WHERE `olap_documents`.`by_document` = "8" AND `olap_documents`.`id` = OLD.`upd_id` LIMIT 1;           
              
            END   
        ');
    }

    private function createTriggersOnAct()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_insert_act`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_act`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_delete_act`');

        $this->execute('
            CREATE TRIGGER `olap_documents_insert_act`
            AFTER INSERT ON `act`
            FOR EACH ROW
            BEGIN
            
              INSERT INTO `olap_documents` (`company_id`, `contractor_id`, `by_document`, `id`, `type`, `date`, `amount`, `is_accounting`, `invoice_expenditure_item_id`, `invoice_income_item_id`) 
              
              SELECT 
                `invoice`.`company_id`,
                `invoice`.`contractor_id`,
                "1" AS `by_document`, 
                `document`.`id`, 
                `document`.`type`, 
                `document`.`document_date` AS `date`, 
                IF(NEW.`status_out_id` = 5, 0, NEW.`order_sum`) AS `amount`,               
                IF (`contractor`.`not_accounting`, 0, 1) AS is_accounting,
                `invoice`.`invoice_expenditure_item_id`,
                `invoice`.`invoice_income_item_id`
              FROM `act` `document`
              LEFT JOIN `invoice` ON `invoice`.`id` = `document`.`invoice_id`  
              LEFT JOIN `contractor` ON `contractor`.`id` = `invoice`.`contractor_id`
              WHERE 
                `document`.`id` = NEW.`id`;
              
            END        
        ');

        $this->execute('
            CREATE TRIGGER `olap_documents_update_act`
            AFTER UPDATE ON `act`
            FOR EACH ROW
            BEGIN
           
              IF IFNULL(NEW.`status_out_id`, 1) <> IFNULL(OLD.`status_out_id`, 1) OR NEW.`document_date` <> OLD.`document_date`
              THEN           
                  UPDATE `olap_documents` 
                  SET 
                    `date` = NEW.`document_date`,
                    `amount` = IF(NEW.`status_out_id` = 5, 0, `amount`)
                  WHERE `olap_documents`.`by_document` = "1" AND `olap_documents`.`id` = NEW.`id` LIMIT 1;
              END IF;           
              
            END   
        ');

        $this->execute('
            CREATE TRIGGER `olap_documents_delete_act`
            AFTER DELETE ON `act`
            FOR EACH ROW
            BEGIN
            
              DELETE FROM `olap_documents` WHERE `by_document` = "1" AND `id` = OLD.`id` LIMIT 1;
              
            END        
        ');

        // ORDER_ACT
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_order_act`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_delete_order_act`');

        $this->execute('
            CREATE TRIGGER `olap_documents_update_order_act`
            AFTER UPDATE ON `order_act`
            FOR EACH ROW
            BEGIN
             
              DECLARE _amount BIGINT;
              SET _amount = (
                SELECT 
                  SUM(IF(`document`.`status_out_id` = 5, 0, IF(`document`.`type` = 2, `order`.`selling_price_with_vat`, `order`.`purchase_price_with_vat`) * `order_doc`.`quantity`)) AS `amount`
                FROM `act` `document`
                LEFT JOIN `order_act` `order_doc` ON `document`.`id` = `order_doc`.`act_id`
                LEFT JOIN `order` ON `order`.`id` = `order_doc`.`order_id`
                WHERE `document`.`id` = NEW.`act_id` 
                GROUP BY `document`.`id`
              );
            
              UPDATE `olap_documents` 
              SET `amount` = _amount
              WHERE `olap_documents`.`by_document` = "1" AND `olap_documents`.`id` = NEW.`act_id` LIMIT 1;           
              
            END   
        ');

        $this->execute('
            CREATE TRIGGER `olap_documents_delete_order_act`
            AFTER DELETE ON `order_act`
            FOR EACH ROW
            BEGIN
             
              DECLARE _amount BIGINT;
              SET _amount = (
                SELECT 
                  SUM(IF(`document`.`status_out_id` = 5, 0, IF(`document`.`type` = 2, `order`.`selling_price_with_vat`, `order`.`purchase_price_with_vat`) * `order_doc`.`quantity`)) AS `amount`
                FROM `act` `document`
                LEFT JOIN `order_act` `order_doc` ON `document`.`id` = `order_doc`.`act_id`
                LEFT JOIN `order` ON `order`.`id` = `order_doc`.`order_id`
                WHERE `document`.`id` = OLD.`act_id` 
                GROUP BY `document`.`id`
              );
            
              UPDATE `olap_documents` 
              SET `amount` = _amount
              WHERE `olap_documents`.`by_document` = "1" AND `olap_documents`.`id` = OLD.`act_id` LIMIT 1;           
              
            END   
        ');
    }

    private function createTriggersOnInvoice()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_invoice`');

        $this->execute('
            CREATE TRIGGER `olap_documents_update_invoice`
            AFTER UPDATE ON `invoice`
            FOR EACH ROW
            BEGIN
            
              IF (NEW.`invoice_expenditure_item_id` <> OLD.`invoice_expenditure_item_id`) THEN
            
                /* Packing List */
                UPDATE `olap_documents` SET `invoice_expenditure_item_id` = NEW.`invoice_expenditure_item_id`
                WHERE `olap_documents`.`by_document` = "4" AND `olap_documents`.`id` IN (    
                  
                  SELECT `id` FROM `packing_list` WHERE `invoice_id` = NEW.`id`    
                );
                
                /* Upd */
                UPDATE `olap_documents` SET `invoice_expenditure_item_id` = NEW.`invoice_expenditure_item_id`
                WHERE `olap_documents`.`by_document` = "8" AND `olap_documents`.`id` IN (    
                  
                  SELECT `upd_id` FROM `invoice_upd` WHERE `invoice_id` = NEW.`id`
                );
                
                /* Act */
                UPDATE `olap_documents` SET `invoice_expenditure_item_id` = NEW.`invoice_expenditure_item_id`
                WHERE `olap_documents`.`by_document` = "1" AND `olap_documents`.`id` IN (    
                  
                  SELECT `act_id` FROM `invoice_act` WHERE `invoice_id` = NEW.`id`
                );

              END IF;	

              IF (NEW.`invoice_income_item_id` <> OLD.`invoice_income_item_id`) THEN
            
                /* Packing List */
                UPDATE `olap_documents` SET `invoice_income_item_id` = NEW.`invoice_income_item_id`
                WHERE `olap_documents`.`by_document` = "4" AND `olap_documents`.`id` IN (    
                  
                  SELECT `id` FROM `packing_list` WHERE `invoice_id` = NEW.`id`    
                );
                
                /* Upd */
                UPDATE `olap_documents` SET `invoice_income_item_id` = NEW.`invoice_income_item_id`
                WHERE `olap_documents`.`by_document` = "8" AND `olap_documents`.`id` IN (    
                  
                  SELECT `upd_id` FROM `invoice_upd` WHERE `invoice_id` = NEW.`id`
                );
                
                /* Act */
                UPDATE `olap_documents` SET `invoice_income_item_id` = NEW.`invoice_income_item_id`
                WHERE `olap_documents`.`by_document` = "1" AND `olap_documents`.`id` IN (    
                  
                  SELECT `act_id` FROM `invoice_act` WHERE `invoice_id` = NEW.`id`
                );

              END IF;	

            END        
        ');
    }

    private function createTriggersOnContractor()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_contractor`');

        $this->execute('
            CREATE TRIGGER `olap_documents_update_contractor`
            AFTER UPDATE ON `contractor`
            FOR EACH ROW
            BEGIN
            
              IF (NEW.`not_accounting` <> OLD.`not_accounting`) THEN
            
                UPDATE `olap_documents` 
                SET `is_accounting` = (IF (NEW.`not_accounting`, 0, 1))
                WHERE `olap_documents`.`contractor_id` = NEW.`id`;
                                
              END IF;	
              
            END
        
        ');
    }
}
