<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180921_141414_alter_company_add_column_qiwi_public_key extends Migration
{
    public function safeUp()
    {
        $this->addColumn('company', 'qiwi_public_key', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn('company', 'qiwi_public_key');
    }
}
