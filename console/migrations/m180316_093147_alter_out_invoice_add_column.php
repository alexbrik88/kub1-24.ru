<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180316_093147_alter_out_invoice_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('out_invoice', 'send_result', $this->boolean()->after('data_url'));
        $this->addColumn('out_invoice', 'result_url', $this->text()->after('send_result'));
    }

    public function safeDown()
    {
        $this->dropColumn('out_invoice', 'send_result');
        $this->dropColumn('out_invoice', 'result_url');
    }
}
