<?php

use console\components\db\Migration;

class m190904_103358_create_amocrm_leads extends Migration
{
    public function up()
    {
        $this->createTable('{{amocrm_leads}}', [
            'id' => $this->primaryKey()->unsigned(),
            'status_id' => $this->integer()->unsigned()->notNull()->comment('ID сделки'),
            'pipeline_id' => $this->integer()->unsigned()->comment('ID воронки продаж'),
            'name' => $this->string(250),
            'price' => $this->float()->unsigned()->comment('Сумма сделки'),
            'created_at' => $this->integer()->unsigned()->notNull(),
            'updated_at' => $this->integer()->unsigned()->notNull(),
        ], "COMMENT 'Интеграция AMOcrm: сделки'");
    }

    public function down()
    {
        $this->dropTable('{{amocrm_leads}}');

    }
}
