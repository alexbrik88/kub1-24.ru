<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210817_070003_alter_config_add_column extends Migration
{
    public $table = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->table, 'finance_plan_period', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 1');
        $this->addColumn($this->table, 'finance_plan_view_chart', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table, 'finance_plan_view_help', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'finance_plan_period');
        $this->dropColumn($this->table, 'finance_plan_view_chart');
        $this->dropColumn($this->table, 'finance_plan_view_help');
    }
}
