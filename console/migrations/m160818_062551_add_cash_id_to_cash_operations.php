<?php

use yii\db\Migration;

/**
 * Handles adding cash_id to table `cash_operations`.
 */
class m160818_062551_add_cash_id_to_cash_operations extends Migration
{
    public $tСash = [
        'cash_bank_flows',
        'cash_order_flows',
        'cash_emoney_flows',
    ];
    /**
     * @inheritdoc
     */
    public function up()
    {
        foreach ($this->tСash as $tableName) {
            $this->addColumn($tableName, 'cash_id', $this->integer()->defaultValue(null));
        }
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        foreach ($this->tСash as $tableName) {
            $this->dropColumn($tableName, 'cash_id');
        }
    }
}
