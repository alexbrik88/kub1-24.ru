<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200928_113222_add_transaction_for_table_sales_invoice extends Migration
{
    public function safeUp()
    {
        $this->execute(<<<SQL
            DROP TRIGGER IF EXISTS `olap_documents_delete_sales_invoice`;
SQL);

        $this->execute(<<<SQL
            CREATE TRIGGER `olap_documents_delete_sales_invoice`
	            AFTER DELETE ON sales_invoice
	            FOR EACH ROW 
                    BEGIN
                        DELETE FROM `olap_documents` WHERE `by_document` = "16" AND `id` = OLD.`id` LIMIT 1;
                    END;
SQL);

        $this->execute(<<<SQL
            DROP TRIGGER IF EXISTS `olap_documents_update_sales_invoice`;
SQL);

        $this->execute(<<<SQL
            CREATE TRIGGER `olap_documents_update_sales_invoice`
	            AFTER UPDATE ON sales_invoice
	            FOR EACH ROW 
                    BEGIN
                        INSERT INTO `olap_documents` (`company_id`, `contractor_id`, `by_document`, `id`, `type`, `date`, `amount`, `amount_in_price_for_buy`, `product_amount_in_price_for_buy`, `is_accounting`, `invoice_expenditure_item_id`)
                        SELECT
                            `invoice`.`company_id`,
                            `invoice`.`contractor_id`,
                            "16" AS `by_document`,
                            `document`.`id`,
                            `document`.`type`,
                            `document`.`document_date` AS `date`,
                            SUM(IF(`document`.`status_out_id` = 5, 0, IF(`document`.`type` = 2, `order_main`.`selling_price_with_vat`, `order_main`.`purchase_price_with_vat`) * `order_sales_invoice`.`quantity`)) AS `amount`,
                            SUM(IF(`document`.`status_out_id` = 5, 0, `order_sales_invoice`.`quantity` * `product`.`price_for_buy_with_nds`)) AS `amount_in_price_for_buy`,
                            SUM(IF(`document`.`status_out_id` = 5, 0, IF(`product`.`production_type` = 1, `order_sales_invoice`.`quantity` * `product`.`price_for_buy_with_nds`, 0))) AS `product_amount_in_price_for_buy`,
                            IF (`contractor`.`not_accounting`, 0, 1) AS is_accounting,
                            `invoice`.`invoice_expenditure_item_id`
                        FROM `order_sales_invoice`
                        LEFT JOIN `sales_invoice` `document` ON `document`.`id` = `order_sales_invoice`.`sales_invoice_id`
                        LEFT JOIN `invoice` ON `invoice`.`id` = `document`.`invoice_id`
                        LEFT JOIN `product` ON `product`.`id` = `order_sales_invoice`.`product_id`
                        LEFT JOIN `order` `order_main` ON `order_main`.`id` = `order_sales_invoice`.`order_id`
                        LEFT JOIN `contractor` ON `contractor`.`id` = `invoice`.`contractor_id`
                        WHERE
                            `order_sales_invoice`.`sales_invoice_id` = NEW.`id`
                        GROUP BY `document`.`id`
                        
                        ON DUPLICATE KEY UPDATE
                            `date` = VALUES(`date`);

                    END;
SQL);

        $this->execute(<<<SQL
            DROP TRIGGER IF EXISTS `olap_invoices_insert_sales_invoice`;
SQL);

        $this->execute(<<<SQL
            CREATE TRIGGER `olap_invoices_insert_sales_invoice`
                AFTER INSERT ON sales_invoice
                FOR EACH ROW 
                    BEGIN
                        UPDATE `olap_invoices` SET `doc_date` =
                            (
                                SELECT MAX(s.`document_date`) AS `max_date` FROM `sales_invoice` s WHERE s.`invoice_id` = `olap_invoices`.`id`

                                UNION ALL

                                SELECT MAX(p.`document_date`) AS `max_date` FROM `packing_list` p WHERE p.`invoice_id` = `olap_invoices`.`id`

                                
                                UNION ALL

                                SELECT MAX(a.`document_date`) AS `max_date` FROM `act` a LEFT JOIN `invoice_act` r ON a.`id` = r.`act_id` WHERE r.`invoice_id` = `olap_invoices`.`id`

                                UNION ALL

                                SELECT MAX(u.`document_date`) AS `max_date` FROM `upd` u LEFT JOIN `invoice_upd` r ON u.`id` = r.`upd_id` WHERE r.`invoice_id` = `olap_invoices`.`id`

                                ORDER BY `max_date` DESC
                                LIMIT 1
                            )  WHERE `olap_invoices`.`id` = NEW.`invoice_id` ORDER BY `olap_invoices`.`id`;

                    END;
SQL);

        $this->execute(<<<SQL
            DROP TRIGGER IF EXISTS `olap_invoices_update_sales_invoice`;
SQL);

        $this->execute(<<<SQL
            CREATE TRIGGER `olap_invoices_update_sales_invoice`
	            AFTER UPDATE ON sales_invoice
	            FOR EACH ROW 
                    BEGIN
                        UPDATE `olap_invoices` SET `doc_date` =
                        (
                            SELECT MAX(s.`document_date`) AS `max_date` FROM `sales_invoice` s WHERE s.`invoice_id` = `olap_invoices`.`id`

                            UNION ALL

                            SELECT MAX(p.`document_date`) AS `max_date` FROM `packing_list` p WHERE p.`invoice_id` = `olap_invoices`.`id`

                            UNION ALL

                            
                            SELECT MAX(a.`document_date`) AS `max_date` FROM `act` a LEFT JOIN `invoice_act` r ON a.`id` = r.`act_id` WHERE r.`invoice_id` = `olap_invoices`.`id`

                            UNION ALL

                            SELECT MAX(u.`document_date`) AS `max_date` FROM `upd` u LEFT JOIN `invoice_upd` r ON u.`id` = r.`upd_id` WHERE r.`invoice_id` = `olap_invoices`.`id`

                            ORDER BY `max_date` DESC
                            LIMIT 1
                        )  WHERE `olap_invoices`.`id` IN (NEW.`invoice_id`) ORDER BY `olap_invoices`.`id`;
                    END;
SQL);

        $this->execute(<<<SQL
            DROP TRIGGER IF EXISTS `product_turnover__sales_invoice__after_update`;
SQL);

        $this->execute(<<<SQL
            CREATE TRIGGER `product_turnover__sales_invoice__after_update`
	            AFTER UPDATE ON sales_invoice
	            FOR EACH ROW 
                    BEGIN
                        IF
                            NEW.`status_out_id` <> OLD.`status_out_id`
                        THEN
                            UPDATE `product_turnover`
                            SET `is_document_actual` = IF(NEW.`status_out_id` IN (1, 2, 3, 4), 1, 0)
                            WHERE `document_id` = NEW.`id`
                        AND `document_table` = "sales_invoice";
                        END IF;
                    END;
SQL);

    }

    public function safeDown()
    {
        $this->execute(<<<SQL
            DROP TRIGGER IF EXISTS `olap_documents_delete_sales_invoice`;
SQL);

        $this->execute(<<<SQL
            DROP TRIGGER IF EXISTS `olap_documents_update_sales_invoice`;
SQL);

        $this->execute(<<<SQL
            DROP TRIGGER IF EXISTS `olap_invoices_insert_sales_invoice`;
SQL);

        $this->execute(<<<SQL
            DROP TRIGGER IF EXISTS `olap_invoices_insert_sales_invoice`;
SQL);

        $this->execute(<<<SQL
            DROP TRIGGER IF EXISTS `product_turnover__sales_invoice__after_update`;
SQL);

    }
}
