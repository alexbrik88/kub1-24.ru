<?php

use console\components\db\Migration;

class m200316_184151_user_config_add_columns extends Migration
{
    const TABLE = '{{%user_config}}';

    public function safeUp()
    {
        $this->addColumn(self::TABLE, 'table_view_rent_type', $this->boolean()->notNull()->defaultValue(true)->comment('Учёт аренды: показать/скрыть столбец "Тип"'));
        $this->addColumn(self::TABLE, 'table_view_rent_agreement', $this->boolean()->notNull()->defaultValue(true)->comment('Учёт аренды: показать/скрыть столбец "Договор"'));
        $this->addColumn(self::TABLE, 'table_view_rent_invoice', $this->boolean()->notNull()->defaultValue(true)->comment('Учёт аренды: показать/скрыть столбец "Счёт"'));
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE, 'table_view_rent_type');
        $this->dropColumn(self::TABLE, 'table_view_rent_agreement');
        $this->dropColumn(self::TABLE, 'table_view_rent_invoice');
    }
}
