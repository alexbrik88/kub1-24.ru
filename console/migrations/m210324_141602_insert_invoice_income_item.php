<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210324_141602_insert_invoice_income_item extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%invoice_income_item}}', [
            'id' => 23,
            'name' => 'Конвертация валюты',
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%invoice_income_item}}', [
            'id' => 23,
        ]);
    }
}
