<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%employees_count}}`.
 */
class m200221_055227_create_employee_count_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%employee_count}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);

        $this->batchInsert('{{%employee_count}}', ['id', 'name'], [
            [1, 'Нет сотрудников'],
            [2, 'До 5 сотрудников'],
            [3, 'Более 5 сотрудников'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%employee_count}}');
    }
}
