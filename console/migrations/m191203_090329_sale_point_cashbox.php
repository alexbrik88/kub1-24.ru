<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191203_090329_sale_point_cashbox extends Migration
{
    public $table = 'sale_point_cashbox';
    
    public function safeUp()
    {
        $salePointTable = 'sale_point';
        $cashboxTable = 'cashbox';
        
        $this->createTable($this->table, [
            'sale_point_id' => $this->integer()->notNull(),
            'cashbox_id' => $this->integer()->notNull()
        ]);
        
        $this->addPrimaryKey('PRIMARY_KEY', $this->table, ['sale_point_id', 'cashbox_id']);
        $this->addForeignKey("FK_{$this->table}_{$salePointTable}", $this->table, 'sale_point_id', $salePointTable, 'id', 'CASCADE');
        $this->addForeignKey("FK_{$this->table}_{$cashboxTable}", $this->table, 'cashbox_id', $cashboxTable, 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $salePointTable = 'sale_point';
        $cashboxTable = 'cashbox';
        
        $this->dropForeignKey("FK_{$this->table}_{$salePointTable}", $this->table);
        $this->dropForeignKey("FK_{$this->table}_{$cashboxTable}", $this->table);

        $this->dropTable($this->table);
    }
}
