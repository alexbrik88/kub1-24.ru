<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180404_074233_alter_invoice_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('invoice', 'has_markup', $this->boolean()->notNull()->defaultValue(false)->after('has_discount'));
    }

    public function safeDown()
    {
        $this->dropColumn('invoice', 'has_markup');
    }
}
