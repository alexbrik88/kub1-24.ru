<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%upd_goods_cancellation}}`.
 */
class m220223_105724_create_upd_goods_cancellation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%upd_goods_cancellation}}', [
            'document_id' => $this->integer()->notNull(),
            'cancellation_id' => $this->integer()->notNull(),
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', '{{%upd_goods_cancellation}}', [
            'document_id',
            'cancellation_id',
        ]);

        $this->addForeignKey('fk_upd_goods_cancellation__document_id',
            '{{%upd_goods_cancellation}}',
            'document_id',
            '{{%upd}}',
            'id'
        );

        $this->addForeignKey('fk_upd_goods_cancellation__cancellation_id',
            '{{%upd_goods_cancellation}}',
            'cancellation_id',
            '{{%goods_cancellation}}',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%upd_goods_cancellation}}');
    }
}
