<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210917_054509_alter_moysklad_import_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('moysklad_import', 'document_format_id', $this->tinyInteger()->after('store_id')->defaultValue(1));
    }

    public function safeDown()
    {
        $this->dropColumn('moysklad_import', 'document_format_id');
    }
}
