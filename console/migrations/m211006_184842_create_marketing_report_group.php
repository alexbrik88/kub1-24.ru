<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211006_184842_create_marketing_report_group extends Migration
{
    public $table = 'marketing_report_group';

    public function safeUp()
    {
        $table = $this->table;
        $this->createTable($table, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'source_type' => $this->tinyInteger()->notNull(),
            'title' => $this->string()->notNull()
        ]);

        $this->addForeignKey("FK_{$table}_company", $table, 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $table = $this->table;
        $this->dropForeignKey("FK_{$table}_company", $table);
        $this->dropTable($table);
    }
}
