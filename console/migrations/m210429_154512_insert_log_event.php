<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210429_154512_insert_log_event extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%log_event}}', [
            'id' => 5,
            'name' => 'ответственный изменен',
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%log_event}}', [
            'id' => 5,
        ]);
    }
}
