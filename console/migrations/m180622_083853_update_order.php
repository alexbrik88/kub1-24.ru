<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180622_083853_update_order extends Migration
{
    public function safeUp()
    {
        $this->execute("
            UPDATE {{order}}
            LEFT JOIN {{invoice}} ON {{invoice}}.[[id]] = {{order}}.[[invoice_id]]
            SET {{order}}.[[view_price_base]] = IF(
                    {{invoice}}.[[nds_view_type_id]] = 1,
                    {{order}}.[[base_price_no_vat]],
                    {{order}}.[[base_price_with_vat]]
                ),

                {{order}}.[[view_price_one]] = ROUND(
                    IF(
                        {{invoice}}.[[type]] = 1,
                        IF(
                            {{invoice}}.[[nds_view_type_id]] = 1,
                            {{order}}.[[purchase_price_no_vat]],
                            {{order}}.[[purchase_price_with_vat]]
                        ),
                        IF(
                            {{invoice}}.[[nds_view_type_id]] = 1,
                            {{order}}.[[selling_price_no_vat]],
                            {{order}}.[[selling_price_with_vat]]
                        )
                    ) * {{invoice}}.[[currency_amount]] / {{invoice}}.[[currency_rate]]
                , 2),

                {{order}}.[[view_total_base]] = ROUND({{order}}.[[view_price_base]] * {{order}}.[[quantity]], 2),

                {{order}}.[[view_total_amount]] = ROUND({{order}}.[[view_price_one]] * {{order}}.[[quantity]], 2),

                {{order}}.[[view_total_no_nds]] = ROUND(
                    IF({{invoice}}.[[type]] = 1, {{order}}.[[amount_purchase_no_vat]], {{order}}.[[amount_sales_no_vat]])
                        * {{invoice}}.[[currency_amount]] / {{invoice}}.[[currency_rate]]
                , 2),

                {{order}}.[[view_total_nds]] = ROUND(
                    IF({{invoice}}.[[type]] = 1, {{order}}.[[purchase_tax]], {{order}}.[[sale_tax]])
                        * {{invoice}}.[[currency_amount]] / {{invoice}}.[[currency_rate]]
                , 2),

                {{order}}.[[view_total_with_nds]] = ROUND(
                    IF({{invoice}}.[[type]] = 1, {{order}}.[[amount_purchase_with_vat]], {{order}}.[[amount_sales_with_vat]])
                        * {{invoice}}.[[currency_amount]] / {{invoice}}.[[currency_rate]]
                , 2);
        ");
    }

    public function safeDown()
    {
        echo "This migration can't be rolled back!";
    }
}


