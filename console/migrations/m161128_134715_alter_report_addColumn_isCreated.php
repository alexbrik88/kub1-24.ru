<?php

use console\components\db\Migration;
use yii\db\Schema;

class m161128_134715_alter_report_addColumn_isCreated extends Migration
{
    public function safeUp()
    {
        $this->addColumn('report', 'is_created', $this->smallInteger(1)->notNull()->defaultValue(0));
        $this->update('report', ['is_created' => 1]);
    }
    
    public function safeDown()
    {
        $this->dropColumn('report', 'is_created');
    }
}


