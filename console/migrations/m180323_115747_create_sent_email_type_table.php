<?php

use yii\db\Migration;

/**
 * Handles the creation of table `sent_email_type`.
 */
class m180323_115747_create_sent_email_type_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('sent_email_type', [
            'id' => $this->primaryKey(),
            'type' => $this->char(50)->notNull(),
            'description' => $this->string(),
        ]);

        $this->insert('sent_email_type', [
            'id' => 1,
            'type' => 'INACTIVE_COMPANY',
            'description' => 'По истечению 14 дней, если пользователь не заходит в личный кабинет, высылаем ему письмо. '
                           . 'Каждое следующее письмо высылаем, если пользователь не заходит 3 дня',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('sent_email_type');
    }
}
