<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200922_121836_alter_employee_company_alter_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%employee_company}}', 'last_rs', $this->string());
    }

    public function safeDown()
    {
        $this->alterColumn('{{%employee_company}}', 'last_rs', $this->string(20));
    }
}
