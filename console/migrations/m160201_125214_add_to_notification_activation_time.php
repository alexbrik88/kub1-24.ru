<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160201_125214_add_to_notification_activation_time extends Migration
{
    public $tNotification = 'notification';

    public function safeUp()
    {
        $this->addColumn($this->tNotification, 'activation_time', $this->string(9));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tNotification, 'activation_time');
    }
}


