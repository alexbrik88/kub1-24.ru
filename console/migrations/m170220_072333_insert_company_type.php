<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170220_072333_insert_company_type extends Migration
{
    public $tableName = 'company_type';

    public function safeUp()
    {
        $this->batchInsert($this->tableName, ['id', 'name_short', 'name_full', 'in_company', 'in_contractor'], [
            [22, 'КГБУЗ', 'Краевое Государственное Бюджетное Учреждение Здравоохранения ', false, true],
            [23, 'МУП', 'Муниципальное Унитарное Предприятие', false, true],
            [24, 'КГБОУ', 'Краевое Государственное Бюджетное Образовательное Учреждение', false, true],
            [25, 'МУК', 'Муниципальное Учреждение Культуры', false, true],
            [26, 'КГКУ', 'Краевое Государственное Казенное Учреждение', false, true],
        ]);
    }

    public function safeDown()
    {
        $this->delete($this->tableName, ['in', 'id', [22, 23, 24, 25, 26]]);
    }
}


