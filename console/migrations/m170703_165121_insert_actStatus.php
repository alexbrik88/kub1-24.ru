<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170703_165121_insert_actStatus extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%act_status}}', [
            'id' => 5,
            'name' => 'Отменён',
        ]);
    }
    
    public function safeDown()
    {
        $this->delete('{{%act_status}}', 'id = 5');
    }
}
