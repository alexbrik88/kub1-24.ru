<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180515_152524_alter_invoice_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('invoice', 'currency_name', $this->char(3)->notNull()->defaultValue('RUB'));
        $this->addColumn('invoice', 'currency_amount', $this->integer()->notNull()->defaultValue(1));
        $this->addColumn('invoice', 'currency_rate', $this->decimal(13,4)->notNull()->defaultValue(1));
        $this->addColumn(
            'invoice',
            'currency_rate_type',
            "ENUM('certain_date', 'payment_date', 'custom_rate') NOT NULL DEFAULT 'certain_date'"
        );
        $this->addColumn('invoice', 'currency_rate_date', $this->date()->notNull());
        $this->addColumn('invoice', 'currency_rate_amount', $this->integer()->notNull()->defaultValue(1));
        $this->addColumn('invoice', 'currency_rate_value', $this->decimal(13,4)->notNull()->defaultValue(1));

        $this->createIndex('currency_name', 'invoice', 'currency_name');

        $this->execute("
            UPDATE {{invoice}}
            SET [[currency_rate_date]] = [[document_date]]
        ");
    }

    public function safeDown()
    {
        $this->dropIndex('currency_name', 'invoice');

        $this->dropColumn('invoice', 'currency_name');
        $this->dropColumn('invoice', 'currency_amount');
        $this->dropColumn('invoice', 'currency_rate');
        $this->dropColumn('invoice', 'currency_rate_type');
        $this->dropColumn('invoice', 'currency_rate_date');
        $this->dropColumn('invoice', 'currency_rate_amount');
        $this->dropColumn('invoice', 'currency_rate_value');
    }
}
