<?php

use console\components\db\Migration;

class m191016_202955_create_insales_order_position extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%insales_order_position}}', [
            'id' => $this->primaryKey()->unsigned()->notNull()->comment('Идентификатор позиции товара в InSales'),
            'order_id' => $this->integer()->unsigned()->notNull()->comment('идентификатор заказа'),
            'product_id' => $this->integer()->unsigned()->notNull()->comment('Идентификатор товара'),
            'full_sale_price' => $this->float()->unsigned()->notNull()->defaultValue(0)->comment('Цена с учётом скидки'),
            'discount_amount' => $this->float()->unsigned()->notNull()->defaultValue(0)->comment('Сумма скидки'),
            'quantity' => $this->tinyInteger()->unsigned()->notNull()->defaultValue(1)->comment('Количество'),
            'reserved_quantity' => $this->tinyInteger()->unsigned()->notNull()->defaultValue(1)->comment('Зарезервированное количество'),
            'weight' => $this->float()->unsigned()->comment('Вес'),
            'comment' => $this->string(500),
        ], "COMMENT 'Интеграция InSales CMS: позиции заказа'");
        $this->addForeignKey(
            'insales_order_position_order_id', '{{%insales_order_position}}', 'order_id',
            '{{%insales_order}}', 'id', 'CASCADE', 'CASCADE'
        );
        $this->addForeignKey(
            'insales_order_position_product_id', '{{%insales_order_position}}', 'product_id',
            '{{%insales_product}}', 'id', 'CASCADE', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable('{{%insales_order_position}}');

    }

}
