<?php

use yii\db\Schema;
use yii\db\Migration;

class m150415_070624_alter_company_renameColumns extends Migration
{
    public function up()
    {
        $this->renameColumn('company', 'orgn', 'ogrn');
        $this->renameColumn('company', 'ie_lastname', 'ip_lastname');
        $this->renameColumn('company', 'ie_firstname', 'ip_firstname');
        $this->renameColumn('company', 'ie_firstname_initials', 'ip_firstname_initials');
        $this->renameColumn('company', 'ie_patronymic', 'ip_patronymic');
        $this->renameColumn('company', 'ie_patronymic_initials', 'ip_patronymic_initials');
        $this->renameColumn('company', 'okded', 'okved');
        $this->renameColumn('company', 'pfr_ie', 'pfr_ip');
    }

    public function down()
    {
        $this->renameColumn('company', 'ogrn', 'orgn');
        $this->renameColumn('company', 'ip_lastname', 'ie_lastname');
        $this->renameColumn('company', 'ip_firstname', 'ie_firstname');
        $this->renameColumn('company', 'ip_firstname_initials', 'ie_firstname_initials');
        $this->renameColumn('company', 'ip_patronymic', 'ie_patronymic');
        $this->renameColumn('company', 'ip_patronymic_initials', 'ie_patronymic_initials');
        $this->renameColumn('company', 'okved', 'okded');
        $this->renameColumn('company', 'pfr_ip', 'pfr_ie');
    }
}
