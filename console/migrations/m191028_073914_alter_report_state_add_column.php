<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191028_073914_alter_report_state_add_column extends Migration
{
    public $table = 'report_state';

    public function safeUp()
    {
        $this->addColumn($this->table, 'tariff_id', $this->integer()->after('subscribe_id'));
        $this->addForeignKey($this->table.'_susbscribe_tariff_FK', $this->table, 'tariff_id', 'service_subscribe_tariff', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey($this->table.'_susbscribe_tariff_FK', $this->table);
        $this->dropColumn($this->table, 'tariff_id');
    }
}
