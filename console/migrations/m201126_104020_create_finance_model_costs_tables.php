<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201126_104020_create_finance_model_costs_tables extends Migration
{
    // CREATED TABLES
    const FIXED_COSTS = 'finance_model_fixed_cost';
    const VARIABLE_COSTS = 'finance_model_variable_cost';
    const OTHER_COSTS = 'finance_model_other_cost';

    // REF TABLES
    const MODEL = 'finance_model';
    const SHOP = 'finance_model_shop';
    const EXPENDITURE_ITEM = 'invoice_expenditure_item';

    public function safeUp()
    {
        /*
         * FIXED COSTS
         */
        $this->createTable(self::FIXED_COSTS, [
            'shop_id' => $this->integer()->notNull(),
            'item_id' => $this->integer()->notNull(),
            'year' => $this->smallInteger()->notNull(),
            'month' => $this->smallInteger()->notNull(),
            'amount' => $this->bigInteger()->defaultValue(0),
        ]);

        $this->addForeignKey('FK_'.self::FIXED_COSTS.'_to_shop', self::FIXED_COSTS, 'shop_id', self::SHOP, 'id', 'CASCADE');
        $this->addForeignKey('FK_'.self::FIXED_COSTS.'_to_expenditure_item', self::FIXED_COSTS, 'item_id', self::EXPENDITURE_ITEM, 'id', 'CASCADE');
        $this->addPrimaryKey('PK_'.self::FIXED_COSTS, self::FIXED_COSTS, ['shop_id', 'item_id', 'year', 'month']);

        /*
         * VARIABLE COSTS
         */
        $this->createTable(self::VARIABLE_COSTS, [
            'shop_id' => $this->integer()->notNull(),
            'item_id' => $this->integer()->notNull(),
            'year' => $this->smallInteger()->notNull(),
            'month' => $this->smallInteger()->notNull(),
            'amount' => $this->bigInteger()->defaultValue(0),
        ]);

        $this->addForeignKey('FK_'.self::VARIABLE_COSTS.'_to_shop', self::VARIABLE_COSTS, 'shop_id', self::SHOP, 'id', 'CASCADE');
        $this->addForeignKey('FK_'.self::VARIABLE_COSTS.'_to_expenditure_item', self::VARIABLE_COSTS, 'item_id', self::EXPENDITURE_ITEM, 'id', 'CASCADE');
        $this->addPrimaryKey('PK_'.self::VARIABLE_COSTS, self::VARIABLE_COSTS, ['shop_id', 'item_id', 'year', 'month']);

        /*
         * OTHER COSTS
         */
        $this->createTable(self::OTHER_COSTS, [
            'model_id' => $this->integer()->notNull(),
            'year' => $this->smallInteger()->notNull(),
            'month' => $this->smallInteger()->notNull(),
            'income_expense' => $this->bigInteger()->defaultValue(0),
            'paid_received_percents' => $this->bigInteger()->defaultValue(0),
            'tax_rate' => $this->decimal(6,2)->defaultValue(0),
            'profit_distribution' => $this->bigInteger()->defaultValue(0),
        ]);

        $this->addForeignKey('FK_'.self::OTHER_COSTS.'_to_model', self::OTHER_COSTS, 'model_id', self::MODEL, 'id', 'CASCADE');
        $this->addPrimaryKey('PK_'.self::OTHER_COSTS, self::OTHER_COSTS, ['model_id', 'year', 'month']);
    }

    public function safeDown()
    {
        /*
         * FIXED COSTS
         */        
        $this->dropForeignKey('FK_'.self::FIXED_COSTS.'_to_shop', self::FIXED_COSTS);
        $this->dropForeignKey('FK_'.self::FIXED_COSTS.'_to_expenditure_item', self::FIXED_COSTS);
        $this->dropTable(self::FIXED_COSTS);

        /*
         * VARIABLE COSTS
         */
        $this->dropForeignKey('FK_'.self::VARIABLE_COSTS.'_to_shop', self::VARIABLE_COSTS);
        $this->dropForeignKey('FK_'.self::VARIABLE_COSTS.'_to_expenditure_item', self::VARIABLE_COSTS);
        $this->dropTable(self::VARIABLE_COSTS);

        /*
         * OTHER COSTS
         */
        $this->dropForeignKey('FK_'.self::OTHER_COSTS.'_to_model', self::OTHER_COSTS);
        $this->dropTable(self::OTHER_COSTS);
    }
}
