<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170531_123321_alter_company_addColumns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%company}}', 'pdf_send_signed', $this->smallInteger(1)->notNull()->defaultValue(false) . ' AFTER [[pdf_signed]]');
        $this->addColumn('{{%company}}', 'pdf_act_send_signed', $this->smallInteger(1)->notNull()->defaultValue(false) . ' AFTER [[pdf_act_signed]]');

        $this->update('{{%company}}', ['pdf_send_signed' => true], '[[pdf_signed]] = 1');
        $this->update('{{%company}}', ['pdf_act_send_signed' => true], '[[pdf_act_signed]] = 1');
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%company}}', 'pdf_send_signed');
        $this->dropColumn('{{%company}}', 'pdf_act_send_signed');
    }
}


