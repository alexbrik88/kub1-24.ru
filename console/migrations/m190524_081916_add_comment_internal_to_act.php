<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190524_081916_add_comment_internal_to_act extends Migration
{
    public $tableName = 'act';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'comment_internal', $this->text()->defaultValue(null));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'comment_internal');
    }
}
