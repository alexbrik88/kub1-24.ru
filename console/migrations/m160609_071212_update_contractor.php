<?php

use console\components\db\Migration;

class m160609_071212_update_contractor extends Migration
{
    public $tContractor = 'packing_list';

    public function safeUp()
    {
        $this->addColumn($this->tContractor, 'basis_name', $this->string(255)->notNull());
        $this->addColumn($this->tContractor, 'basis_document_number', $this->integer(11)->defaultValue(null));
        $this->addColumn($this->tContractor, 'basis_document_date', $this->date()->defaultValue(null));
        $this->addColumn($this->tContractor, 'consignor_id', $this->integer(11)->notNull());
    }

    public function safeDown()
    {
        $this->dropColumn($this->tContractor, 'basis_name');
        $this->dropColumn($this->tContractor, 'basis_document_number');
        $this->dropColumn($this->tContractor, 'basis_document_date');
        $this->dropColumn($this->tContractor, 'consignor_id');
    }
}


