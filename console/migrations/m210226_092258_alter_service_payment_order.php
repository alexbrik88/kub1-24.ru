<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210226_092258_alter_service_payment_order extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%service_payment_order}}', 'company_id', $this->integer());
        $this->addColumn('{{%service_payment_order}}', 'employee_id', $this->integer());
        $this->addForeignKey('FK_service_payment_order__employee_id', '{{%service_payment_order}}', 'employee_id', '{{%employee}}', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_service_payment_order__employee_id', '{{%service_payment_order}}');
        $this->alterColumn('{{%service_payment_order}}', 'company_id', $this->integer()->notNull());
        $this->dropColumn('{{%service_payment_order}}', 'employee_id');
    }
}
