<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210805_082902_alter_card_operation_add_column extends Migration
{
    public function up()
    {
        $this->addColumn('{{%card_operation}}', 'currency_id', $this->char(3)->notNull()->after('income_item_id'));
        $this->addColumn('{{%card_operation}}', 'currency_name', $this->char(3)->notNull()->after('currency_id'));
        $this->addColumn('{{%card_operation}}', 'amount_rub', $this->bigInteger()->notNull()->after('amount'));

        $this->execute('
            UPDATE {{%card_operation}}
            LEFT JOIN {{%card_bill}} ON {{%card_bill}}.[[id]] = {{%card_operation}}.[[bill_id]]
            SET {{%card_operation}}.[[currency_id]] = {{%card_bill}}.[[currency_id]],
                {{%card_operation}}.[[currency_name]] = {{%card_bill}}.[[currency_name]],
                {{%card_operation}}.[[amount_rub]] = {{%card_operation}}.[[amount]]
        ');
    }

    public function down()
    {
        $this->dropColumn('{{%card_operation}}', 'currency_id');
        $this->dropColumn('{{%card_operation}}', 'currency_name');
        $this->dropColumn('{{%card_operation}}', 'amount_rub');
    }
}
