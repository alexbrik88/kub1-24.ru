<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191008_173754_add_moneta_id_to_cash_emoney_flows extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cash_emoney_flows}}', 'moneta_id', $this->integer()->unsigned()->defaultValue(null));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%cash_emoney_flows}}', 'moneta_id');
    }
}
