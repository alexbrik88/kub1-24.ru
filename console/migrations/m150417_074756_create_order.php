<?php

use yii\db\Schema;
use yii\db\Migration;

class m150417_074756_create_order extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('order', [
            'id' => Schema::TYPE_PK,
            'invoice_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Счёт"',
            'number' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Номер п/п"',
            'product_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Продукция"',
            'quantity' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Колличество"',
            'product_title' =>Schema::TYPE_STRING . ' NOT NULL COMMENT "Наименование продукции"',
            'product_code' => Schema::TYPE_STRING . ' NOT NULL COMMENT "Код продукта"',
            'unit_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Единица измерения"',
            'count_in_place' => Schema::TYPE_STRING . ' NOT NULL COMMENT "Колличество в одном месте"',
            'place_count' => Schema::TYPE_STRING . '(45) NULL COMMENT "Количество мест, штук"',
            'mass_gross' => Schema::TYPE_STRING . '(45) COMMENT "масса брутто"',
            'purchase_price_no_vat' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Цена покупки без НДС"',
            'purchase_price_with_vat' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Цена покупки с учётом НДС"',
            'selling_price_no_vat' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Цена продажи без НДС"',
            'selling_price_with_vat' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Цена продажи с учётом НДС"',
            'amount_purchase_no_vat' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Сумма покупки без учёта НДС"',
            'amount_purchase_with_vat' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Сумма покупки с учётом НДС"',
            'amount_sales_no_vat' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Сумма продажи без учёта НДС"',
            'amount_sales_with_vat' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Сумма продажи с учётом НДС"',
            'excise' => Schema::TYPE_BOOLEAN . ' NOT NULL COMMENT "1 - с акцизом, 0 - без"',
            'excise_price' => Schema::TYPE_STRING . ' NOT NULL COMMENT "Если нет, то значение - без акциза"',
            'sale_tax' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Сумма налога при продаже"',
            'purchase_tax' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Сумма налога при покупке"',
            'purchase_tax_rate_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "НДС ПОКУПКИ"',
            'sale_tax_rate_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "НДС продажи"',
            'country_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Страна проихождения"',
            'custom_declaration_number' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Номер таможенной декларации"',
        ], $tableOptions);

        $this->addForeignKey('order_to_country', 'order', 'country_id', 'country', 'id');
        $this->addForeignKey('order_to_tax_rate_sale', 'order', 'sale_tax_rate_id', 'tax_rate', 'id');
        $this->addForeignKey('order_to_tax_rate_purchase', 'order', 'purchase_tax_rate_id', 'tax_rate', 'id');
        $this->addForeignKey('order_to_product_unit', 'order', 'unit_id', 'product_unit', 'id');
        $this->addForeignKey('order_to_product', 'order', 'product_id', 'product', 'id');
        $this->addForeignKey('order_to_invoice', 'order', 'invoice_id', 'invoice', 'id');
    }

    public function down()
    {
        $this->dropTable('order');
    }

}
