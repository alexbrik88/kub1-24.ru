<?php

use console\components\db\Migration;
use common\models\FsspTask;

class m200208_114506_fssp_item_task_fk_alter extends Migration
{
    const TABLE = '{{%fssp_item}}';

    public function safeUp()
    {
        $this->dropForeignKey('fssp_item_contractor_id', self::TABLE);
        $this->addColumn(self::TABLE, 'task_id', $this->integer()->unsigned()->notNull()->first()->comment('ID задачи (fssp_task.id)'));
        foreach (FsspTask::find()->asArray()->all() as $item) {
            $this->update(self::TABLE, ['task_id' => $item['id']], ['contractor_id' => $item['contractor_id']]);
        }
        $this->dropColumn(self::TABLE, 'contractor_id');
        $this->addForeignKey(
            'fssp_item_task_id', self::TABLE, 'task_id',
            '{{%fssp_task}}', 'id',
            'CASCADE', 'CASCADE'
        );
        $this->dropColumn(self::TABLE, 'id');
    }

    public function safeDown()
    {
        $this->addColumn(self::TABLE, 'id', $this->primaryKey()->unsigned()->notNull()->first());
        $this->dropForeignKey('fssp_item_task_id', self::TABLE);
        $this->addColumn(self::TABLE, 'contractor_id', $this->integer()->notNull()->after('id'));
        foreach (FsspTask::find()->asArray()->all() as $item) {
            $this->update(self::TABLE, ['contractor_id' => $item['contractor_id']], ['task_id' => $item['id']]);
        }
        $this->dropColumn(self::TABLE, 'task_id');
        $this->addForeignKey('fssp_item_contractor_id', self::TABLE, 'contractor_id', '{{%contractor}}', 'id',
            'CASCADE', 'CASCADE');
    }
}
