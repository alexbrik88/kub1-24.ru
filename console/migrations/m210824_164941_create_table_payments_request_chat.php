<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210824_164941_create_table_payments_request_chat extends Migration
{
    public $tableBody = 'payments_request_chat';
    public $tableUser = 'payments_request_chat_user';

    public function safeUp()
    {
        /// body
        $table = $this->tableBody;
        $this->createTable($table, [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull(),
            'author_id' => $this->integer()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'request_id' => $this->integer()->notNull(),
            'message' => $this->string(2048)
        ]);

        $this->addForeignKey('FK_' . $table . '_author', $table, 'author_id', 'employee', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_' . $table . '_company', $table, 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_' . $table . '_request', $table, 'request_id', 'payments_request', 'id', 'CASCADE', 'CASCADE');

        /// user
        $table = $this->tableUser;
        $this->createTable($table, [
            'request_id' => $this->integer()->notNull(),
            'reader_id' => $this->integer()->notNull(),
            'last_read_at' => $this->integer(),
        ]);

        $this->addPrimaryKey('PK_' . $table, $table, ['request_id', 'reader_id']);
        $this->addForeignKey('FK_' . $table . '_request', $table, 'request_id', 'payments_request', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_' . $table . '_reader', $table, 'reader_id', 'employee', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        /// body
        $table = $this->tableBody;
        $this->dropForeignKey('FK_' . $table . '_author', $table);
        $this->dropForeignKey('FK_' . $table . '_company', $table);
        $this->dropForeignKey('FK_' . $table . '_request', $table);
        $this->dropTable($table);

        /// user
        $table = $this->tableUser;
        $this->dropForeignKey('FK_' . $table . '_request', $table);
        $this->dropForeignKey('FK_' . $table . '_reader', $table);
        $this->dropTable($table);
    }
}
