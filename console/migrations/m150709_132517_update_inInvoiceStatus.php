<?php

use yii\db\Schema;
use yii\db\Migration;

class m150709_132517_update_inInvoiceStatus extends Migration
{
    public function safeUp()
    {
        $this->update('in_invoice_status', [
            'name' => 'Оплачен',
        ], 'id = 3');
    }

    public function safeDown()
    {
        $this->update('in_invoice_status', [
            'name' => 'Оплачен полностью',
        ], 'id = 3');
    }
}
