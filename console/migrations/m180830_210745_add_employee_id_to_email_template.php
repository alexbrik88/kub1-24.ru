<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180830_210745_add_employee_id_to_email_template extends Migration
{
    public $tableName = 'email_template';

    public function safeUp()
    {
        $this->truncateTable($this->tableName);
        $this->addColumn($this->tableName, 'employee_id', $this->integer()->notNull());
        $this->addForeignKey($this->tableName . '_employee_id', $this->tableName, 'employee_id', 'employee', 'id', 'RESTRICT', 'CASCADE');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_employee_id', $this->tableName);
        $this->dropColumn($this->tableName, 'employee_id');
    }
}


