<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180827_142224_add_type_to_email_file extends Migration
{
    public $tableName = 'email_file';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'type', $this->integer()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'type');
    }
}


