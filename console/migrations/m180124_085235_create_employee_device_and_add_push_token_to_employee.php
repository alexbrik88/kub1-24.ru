<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180124_085235_create_employee_device_and_add_push_token_to_employee extends Migration
{
    public $tEmployee = 'employee';
    public $tEmployeeDevice = 'employee_device';

    public function safeUp()
    {
        $this->addColumn($this->tEmployee, 'push_token', $this->string()->defaultValue(null));

        $this->createTable($this->tEmployeeDevice, [
            'id' => $this->primaryKey(),
            'employee_id' => $this->integer()->notNull(),
            'device_id' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey($this->tEmployeeDevice . '_employee_id', $this->tEmployeeDevice, 'employee_id', $this->tEmployee, 'id', 'RESTRICT', 'CASCADE');
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tEmployee, 'push_token');

        $this->dropForeignKey($this->tEmployeeDevice . '_employee_id', $this->tEmployeeDevice);

        $this->dropTable($this->tEmployeeDevice);
    }
}


