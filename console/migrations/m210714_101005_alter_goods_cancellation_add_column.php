<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210714_101005_alter_goods_cancellation_add_column extends Migration
{
    public function safeUp()
    {
        $tableName = 'goods_cancellation';
        $this->addColumn($tableName, 'sale_point_id', $this->integer());
        $this->addForeignKey("FK_{$tableName}_to_sale_point", $tableName, 'sale_point_id', 'sale_point', 'id', 'SET NULL', 'CASCADE');
    }

    public function safeDown()
    {
        $tableName = 'goods_cancellation';
        $this->dropForeignKey("FK_{$tableName}_to_sale_point", $tableName);
        $this->dropColumn($tableName, 'sale_point_id');
    }
}
