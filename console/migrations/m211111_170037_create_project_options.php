<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211111_170037_create_project_options extends Migration
{
    public $table = 'contractor_auto_project';

    public function safeUp()
    {
        $table = $this->table;

        $this->createTable($table, [
            'contractor_id' => $this->integer()->notNull(),
            'project_id' => $this->integer()->notNull(),
            'project_start' => $this->date()->notNull(),
            'project_end' => $this->date()->notNull(),
            'project_status' => $this->tinyInteger()->notNull(),
            'exists_invoices' => $this->boolean()->defaultValue(false),
            'exists_flows' => $this->boolean()->defaultValue(false),
            'new_invoices' => $this->boolean()->defaultValue(false),
            'new_flows' => $this->boolean()->defaultValue(false),
        ]);

        $this->addForeignKey("FK_{$table}_contractor", $table, 'contractor_id', 'contractor', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("FK_{$table}_project", $table, 'project_id', 'project', 'id', 'CASCADE', 'CASCADE');
        $this->addPrimaryKey("PK_{$table}", $table, ['contractor_id', 'project_id']);
    }

    public function safeDown()
    {
        $table = $this->table;

        $this->dropTable($table);
    }
}
