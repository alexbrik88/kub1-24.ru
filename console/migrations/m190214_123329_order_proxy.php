<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190214_123329_order_proxy extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('order_proxy', [
            'id' => Schema::TYPE_PK,
            'order_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Счёт"',
            'proxy_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Доверенность"',
            'product_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Продукция"',
            'quantity' => Schema::TYPE_DECIMAL . '(20,10) NOT NULL'
        ], $tableOptions);

        $this->addForeignKey('FK_order_proxy_to_order', 'order_proxy', 'order_id', 'order', 'id');
        $this->addForeignKey('FK_order_proxy_to_proxy', 'order_proxy', 'proxy_id', 'proxy', 'id');
        $this->addForeignKey('FK_order_proxy_to_product', 'order_proxy', 'product_id', 'product', 'id');
    }

    public function safeDown()
    {
        $this->dropTable('order_proxy');
    }
}
