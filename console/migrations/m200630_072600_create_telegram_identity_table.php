<?php

use yii\db\Migration;

class m200630_072600_create_telegram_identity_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%telegram_identity}}';

    /** @var string[] */
    private const PK_COLUMNS = ['employee_id', 'company_id'];

    /**
     * @inheritDoc
     */
    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'telegram_id' => $this->bigInteger(),
            'employee_id' => $this->integer(),
            'company_id' => $this->integer(),
        ]);

        $this->addPrimaryKey('pk_identity', self::TABLE_NAME, self::PK_COLUMNS);
        $this->createIndex('ck_telegram_id', self::TABLE_NAME, ['telegram_id']);
        $this->addForeignKey(
            'fk_telegram_identity__employee_company',
            self::TABLE_NAME,
            self::PK_COLUMNS,
            '{{%employee_company}}',
            self::PK_COLUMNS,
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
