<?php

use console\components\db\Migration;

class m200110_200438_bitrix24_company_id_add extends Migration
{
    public function safeUp()
    {
        $this->_addColumn('catalog', 'company_id', 'Идентификатор компании КУБ');
        $this->_addColumn('company', 'company_id', 'Идентификатор компании КУБ');
        $this->_addColumn('contact', 'company_id', 'Идентификатор компании КУБ');
        $this->_addColumn('deal', 'company_id', 'Идентификатор компании КУБ');
        $this->_addColumn('deal_position', 'company_id', 'Идентификатор компании КУБ');
        $this->_addColumn('invoice', 'company_id', 'Идентификатор компании КУБ');
        $this->_addColumn('product', 'company_id', 'Идентификатор компании КУБ');
        $this->_addColumn('section', 'company_id', 'Идентификатор компании КУБ');
        $this->_addColumn('user', 'company_id', 'Идентификатор компании КУБ',false);
        $this->_addColumn('vat', 'company_id', 'Идентификатор компании КУБ');
    }

    public function safeDown()
    {
        $this->_dropColumn('catalog');
        $this->_dropColumn('company');
        $this->_dropColumn('contact');
        $this->_dropColumn('deal');
        $this->_dropColumn('deal_position');
        $this->_dropColumn('invoice');
        $this->_dropColumn('product');
        $this->_dropColumn('section');
        $this->_dropColumn('user');
        $this->_dropColumn('vat');
    }

    protected static function tableName(string $table, bool $local = true): string
    {
        if ($local === true) {
            $table = 'bitrix24_' . $table;
        }
        return '{{%' . $table . '}}';
    }

    private function _addColumn(string $table, string $column, string $comment, bool $notNull = true)
    {
        $def = $this->integer(11)->after('id')->comment($comment);
        if ($notNull === true) {
            $def->notNull();
        }
        $this->addColumn(self::tableName($table), $column, $def);
    }

    private function _dropColumn(string $table, string $column = 'company_id')
    {
        $this->dropColumn(self::tableName($table), $column);
    }
}