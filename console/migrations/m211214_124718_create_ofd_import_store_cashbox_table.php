<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%ofd_import_store_cashbox}}`.
 */
class m211214_124718_create_ofd_import_store_cashbox_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%ofd_import_store_cashbox}}', [
            'company_id' => $this->integer()->notNull(),
            'ofd_store_id' => $this->integer()->notNull(),
            'cashbox_id' => $this->integer()->notNull(),
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', '{{%ofd_import_store_cashbox}}', [
            'company_id',
            'ofd_store_id',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%ofd_import_store_cashbox}}');
    }
}
