<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201128_064556_alter_finance_model_variable_cost extends Migration
{
    public function safeUp()
    {
        $this->addColumn('finance_model_revenue', 'amount', $this->bigInteger()->defaultValue(0));
        $this->addColumn('finance_model_variable_cost', 'percent', $this->decimal(6,2)->after('month')->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('finance_model_revenue', 'amount');
        $this->dropColumn('finance_model_variable_cost', 'percent');
    }
}