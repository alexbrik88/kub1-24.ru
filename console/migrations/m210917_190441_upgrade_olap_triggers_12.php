<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210917_190441_upgrade_olap_triggers_12 extends Migration
{
    public function safeUp()
    {
        // add tin_children logic
        ///////////////////////////////////
        $this->upTriggersOnCashBankFlows();
        ///////////////////////////////////

        $this->upTriggersOnCashOrderFlows();
        $this->upTriggersOnCashEmoneyFlows();
        $this->upTriggersOnCashBankForeignCurrencyFlows();
        $this->upTriggersOnCashOrderForeignCurrencyFlows();
        $this->upTriggersOnCashEmoneyForeignCurrencyFlows();
        $this->upTriggersOnAcquiringOperations();
        $this->upTriggersOnCardOperations();
        $this->upTriggersOnCashBankForeignCurrencyFlowsToInvoices();
        $this->upTriggersOnCashOrderForeignCurrencyFlowsToInvoices();
        $this->upTriggersOnCashEmoneyForeignCurrencyFlowsToInvoices();
    }

    public function safeDown()
    {
        $this->downTriggersOnCashBankFlows();
        $this->downTriggersOnCashOrderFlows();
        $this->downTriggersOnCashEmoneyFlows();
        $this->downTriggersOnCashBankForeignCurrencyFlows();
        $this->downTriggersOnCashOrderForeignCurrencyFlows();
        $this->downTriggersOnCashEmoneyForeignCurrencyFlows();
        $this->downTriggersOnAcquiringOperations();
        $this->downTriggersOnCardOperations();
        $this->downTriggersOnCashBankForeignCurrencyFlowsToInvoices();
        $this->downTriggersOnCashOrderForeignCurrencyFlowsToInvoices();
        $this->downTriggersOnCashEmoneyForeignCurrencyFlowsToInvoices();
    }

    private function upTriggersOnCashBankFlows()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_bank_flows`');
        $this->execute('
            CREATE TRIGGER `olap_flows_insert_cash_bank_flows`
            AFTER INSERT ON `cash_bank_flows`
            FOR EACH ROW
            BEGIN
                INSERT INTO `olap_flows` (
                    `id`,
                    `company_id`,
                    `contractor_id`,
                    `account_id`,
                    `project_id`,
                    `sale_point_id`,
                    `industry_id`,
                    `wallet`,
                    `type`,
                    `is_accounting`,
                    `item_id`,
                    `date`,
                    `recognition_date`,
                    `is_prepaid_expense`,
                    `amount`,
                    `original_amount`,
                    `currency_id`,
                    `currency_name`,
                    `has_invoice`,
                    `has_tin_parent`,
                    `has_tin_children`,
                    `tin_child_amount`                    
                )
                VALUES (
                    NEW.`id`,
                    NEW.`company_id`,
                    NEW.`contractor_id`,
                    NEW.`account_id`,
                    NEW.`project_id`,
                    NEW.`sale_point_id`,
                    NEW.`industry_id`,
                    "1", /* WALLET */
                    NEW.`flow_type`,
                    1,
                    IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id),
                    NEW.`date`,
                    NEW.`recognition_date`,
                    NEW.`is_prepaid_expense`,
                    NEW.`amount`,
                    NEW.`amount`,
                    "643",
                    "RUB",
                    0,
                    IF(NEW.parent_id IS NULL, 0, 1), /* has_tin_parent */
                    NEW.`has_tin_children`, /* has_tin_children */
                    NEW.`tin_child_amount` /* tin_child_amount */
                );
            END
        ');

        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_update_cash_bank_flows`');
        $this->execute('
            CREATE TRIGGER `olap_flows_update_cash_bank_flows`
            AFTER UPDATE ON `cash_bank_flows`
            FOR EACH ROW
            BEGIN
                UPDATE `olap_flows`
                SET
                    `contractor_id` = NEW.`contractor_id`,
                    `account_id` = NEW.`account_id`,
                    `project_id` = NEW.`project_id`,
                    `sale_point_id` = NEW.`sale_point_id`,
                    `industry_id` = NEW.`industry_id`,
                    `type` = NEW.`flow_type`,
                    `date` = NEW.`date`,
                    `recognition_date` = NEW.`recognition_date`,
                    `is_prepaid_expense` = NEW.`is_prepaid_expense`,
                    `amount` = NEW.`amount`,
                    `original_amount` = NEW.`amount`,
                    `currency_id` = "643",
                    `currency_name` = "RUB",
                    `item_id` = IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id),
                    `has_tin_parent` = IF(NEW.parent_id IS NULL, 0, 1),
                    `has_tin_children` = NEW.`has_tin_children`,
                    `tin_child_amount` = NEW.`tin_child_amount`
                WHERE `id` = NEW.`id`
                    AND `wallet` = "1";
            END
        ');
    }

    private function upTriggersOnCashOrderFlows()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_order_flows`');
        $this->execute('
            CREATE TRIGGER `olap_flows_insert_cash_order_flows`
            AFTER INSERT ON `cash_order_flows`
            FOR EACH ROW
            BEGIN
                INSERT INTO `olap_flows` (
                    `id`,
                    `company_id`,
                    `contractor_id`,
                    `account_id`,
                    `project_id`,
                    `sale_point_id`,
                    `industry_id`,
                    `wallet`,
                    `type`,
                    `is_accounting`,
                    `item_id`,
                    `date`,
                    `recognition_date`,
                    `is_prepaid_expense`,
                    `amount`,
                    `original_amount`,
                    `currency_id`,
                    `currency_name`,
                    `has_invoice`)
                VALUES (
                    NEW.`id`,
                    NEW.`company_id`,
                    NEW.`contractor_id`,
                    NEW.`cashbox_id`,
                    NEW.`project_id`,
                    NEW.`sale_point_id`,
                    NEW.`industry_id`,
                    "2", /* WALLET */
                    NEW.`flow_type`,
                    NEW.`is_accounting`,
                    IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id),
                    NEW.`date`,
                    NEW.`recognition_date`,
                    NEW.`is_prepaid_expense`,
                    NEW.`amount`,
                    NEW.`amount`,
                    "643",
                    "RUB",
                    0);

            END');

        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_update_cash_order_flows`');
        $this->execute('
            CREATE TRIGGER `olap_flows_update_cash_order_flows`
            AFTER UPDATE ON `cash_order_flows`
            FOR EACH ROW
            BEGIN

                UPDATE `olap_flows`
                SET
                    `contractor_id` = NEW.`contractor_id`,
                    `account_id` = NEW.`cashbox_id`,
                    `project_id` = NEW.`project_id`,
                    `sale_point_id` = NEW.`sale_point_id`,
                    `industry_id` = NEW.`industry_id`,
                    `type` = NEW.`flow_type`,
                    `date` = NEW.`date`,
                    `recognition_date` = NEW.`recognition_date`,
                    `is_prepaid_expense` = NEW.`is_prepaid_expense`,
                    `amount` = NEW.`amount`,
                    `original_amount` = NEW.`amount`,
                    `currency_id` = "643",
                    `currency_name` = "RUB",
                    `item_id` = IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id),
                    `is_accounting` = NEW.`is_accounting`
                WHERE `id` = NEW.`id`
                    AND `wallet` = "2";

            END');
    }

    private function upTriggersOnCashEmoneyFlows()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_emoney_flows`');
        $this->execute('
            CREATE TRIGGER `olap_flows_insert_cash_emoney_flows`
            AFTER INSERT ON `cash_emoney_flows`
            FOR EACH ROW
            BEGIN
                INSERT INTO `olap_flows` (
                    `id`,
                    `company_id`,
                    `contractor_id`,
                    `account_id`,
                    `project_id`,
                    `sale_point_id`,
                    `industry_id`,
                    `wallet`,
                    `type`,
                    `is_accounting`,
                    `item_id`,
                    `date`,
                    `recognition_date`,
                    `is_prepaid_expense`,
                    `amount`,
                    `original_amount`,
                    `currency_id`,
                    `currency_name`,
                    `has_invoice`)
                VALUES (
                    NEW.`id`,
                    NEW.`company_id`,
                    NEW.`contractor_id`,
                    NEW.`emoney_id`,
                    NEW.`project_id`,
                    NEW.`sale_point_id`,
                    NEW.`industry_id`,
                    "3", /* WALLET */
                    NEW.`flow_type`,
                    NEW.`is_accounting`,
                    IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id),
                    NEW.`date`,
                    NEW.`recognition_date`,
                    NEW.`is_prepaid_expense`,
                    NEW.`amount`,
                    NEW.`amount`,
                    "643",
                    "RUB",
                    0);
            END');

        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_update_cash_emoney_flows`');
        $this->execute('
            CREATE TRIGGER `olap_flows_update_cash_emoney_flows`
            AFTER UPDATE ON `cash_emoney_flows`
            FOR EACH ROW
            BEGIN
                UPDATE `olap_flows`
                SET
                    `contractor_id` = NEW.`contractor_id`,
                    `account_id` = NEW.`emoney_id`,
                    `project_id` = NEW.`project_id`,
                    `sale_point_id` = NEW.`sale_point_id`,
                    `industry_id` = NEW.`industry_id`,
                    `type` = NEW.`flow_type`,
                    `date` = NEW.`date`,
                    `recognition_date` = NEW.`recognition_date`,
                    `is_prepaid_expense` = NEW.`is_prepaid_expense`,
                    `amount` = NEW.`amount`,
                    `original_amount` = NEW.`amount`,
                    `currency_id` = "643",
                    `currency_name` = "RUB",
                    `item_id` = IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id),
                    `is_accounting` = NEW.`is_accounting`
                WHERE `id` = NEW.`id`
                    AND `wallet` = "3";

            END');
    }

    private function upTriggersOnCashBankForeignCurrencyFlows()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_bank_foreign_currency_flows`');
        $this->execute('
            CREATE TRIGGER `olap_flows_insert_cash_bank_foreign_currency_flows`
            AFTER INSERT ON `cash_bank_foreign_currency_flows`
            FOR EACH ROW
            BEGIN
                INSERT INTO `olap_flows` (
                    `id`,
                    `company_id`,
                    `contractor_id`,
                    `account_id`,
                    `project_id`,
                    `sale_point_id`,
                    `industry_id`,
                    `wallet`,
                    `type`,
                    `is_accounting`,
                    `item_id`,
                    `date`,
                    `recognition_date`,
                    `is_prepaid_expense`,
                    `amount`,
                    `original_amount`,
                    `currency_id`,
                    `currency_name`,
                    `has_invoice`)
                VALUES (
                    NEW.`id`,
                    NEW.`company_id`,
                    NEW.`contractor_id`,
                    NEW.`account_id`,
                    NEW.`project_id`,
                    NEW.`sale_point_id`,
                    NEW.`industry_id`,
                    "11", /* WALLET */
                    NEW.`flow_type`,
                    1,
                    IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id),
                    NEW.`date`,
                    NEW.`recognition_date`,
                    NEW.`is_prepaid_expense`,
                    NEW.`amount_rub`,
                    NEW.`amount`,
                    NEW.`currency_id`,
                    NEW.`currency_name`,
                    0);
            END
        ');

        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_update_cash_bank_foreign_currency_flows`');
        $this->execute('
            CREATE TRIGGER `olap_flows_update_cash_bank_foreign_currency_flows`
            AFTER UPDATE ON `cash_bank_foreign_currency_flows`
            FOR EACH ROW
            BEGIN
                UPDATE `olap_flows`
                SET
                    `contractor_id` = NEW.`contractor_id`,
                    `account_id` = NEW.`account_id`,
                    `project_id` = NEW.`project_id`,
                    `sale_point_id` = NEW.`sale_point_id`,
                    `industry_id` = NEW.`industry_id`,
                    `type` = NEW.`flow_type`,
                    `date` = NEW.`date`,
                    `recognition_date` = NEW.`recognition_date`,
                    `is_prepaid_expense` = NEW.`is_prepaid_expense`,
                    `amount` = NEW.`amount_rub`,
                    `original_amount` = NEW.`amount`,
                    `currency_id` = NEW.`currency_id`,
                    `currency_name` = NEW.`currency_name`,
                    `item_id` = IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id)
                WHERE `id` = NEW.`id`
                    AND `wallet` = "11";
            END
        ');

        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_delete_cash_bank_foreign_currency_flows`');
        $this->execute('
            CREATE TRIGGER `olap_flows_delete_cash_bank_foreign_currency_flows`
            AFTER DELETE ON `cash_bank_foreign_currency_flows`
            FOR EACH ROW
            BEGIN
                DELETE FROM `olap_flows` WHERE `id` = OLD.`id` AND `wallet` = "11" LIMIT 1;
            END');
    }

    private function upTriggersOnCashOrderForeignCurrencyFlows()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_order_foreign_currency_flows`');
        $this->execute('
            CREATE TRIGGER `olap_flows_insert_cash_order_foreign_currency_flows`
            AFTER INSERT ON `cash_order_foreign_currency_flows`
            FOR EACH ROW
            BEGIN
                INSERT INTO `olap_flows` (
                    `id`,
                    `company_id`,
                    `contractor_id`,
                    `account_id`,
                    `project_id`,
                    `sale_point_id`,
                    `industry_id`,
                    `wallet`,
                    `type`,
                    `is_accounting`,
                    `item_id`,
                    `date`,
                    `recognition_date`,
                    `is_prepaid_expense`,
                    `amount`,
                    `original_amount`,
                    `currency_id`,
                    `currency_name`,
                    `has_invoice`)
                VALUES (
                    NEW.`id`,
                    NEW.`company_id`,
                    NEW.`contractor_id`,
                    NEW.`cashbox_id`,
                    NEW.`project_id`,
                    NEW.`sale_point_id`,
                    NEW.`industry_id`,
                    "12", /* WALLET */
                    NEW.`flow_type`,
                    NEW.`is_accounting`,
                    IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id),
                    NEW.`date`,
                    NEW.`recognition_date`,
                    NEW.`is_prepaid_expense`,
                    NEW.`amount_rub`,
                    NEW.`amount`,
                    NEW.`currency_id`,
                    NEW.`currency_name`,
                    0);

            END');

        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_update_cash_order_foreign_currency_flows`');
        $this->execute('
            CREATE TRIGGER `olap_flows_update_cash_order_foreign_currency_flows`
            AFTER UPDATE ON `cash_order_foreign_currency_flows`
            FOR EACH ROW
            BEGIN

                UPDATE `olap_flows`
                SET
                    `contractor_id` = NEW.`contractor_id`,
                    `account_id` = NEW.`cashbox_id`,
                    `project_id` = NEW.`project_id`,
                    `sale_point_id` = NEW.`sale_point_id`,
                    `industry_id` = NEW.`industry_id`,
                    `type` = NEW.`flow_type`,
                    `date` = NEW.`date`,
                    `recognition_date` = NEW.`recognition_date`,
                    `is_prepaid_expense` = NEW.`is_prepaid_expense`,
                    `amount` = NEW.`amount_rub`,
                    `original_amount` = NEW.`amount`,
                    `currency_id` = NEW.`currency_id`,
                    `currency_name` = NEW.`currency_name`,
                    `item_id` = IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id),
                    `is_accounting` = NEW.`is_accounting`
                WHERE `id` = NEW.`id`
                    AND `wallet` = "12";

            END');

        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_delete_cash_order_foreign_currency_flows`');
        $this->execute('
            CREATE TRIGGER `olap_flows_delete_cash_order_foreign_currency_flows`
            AFTER DELETE ON `cash_order_foreign_currency_flows`
            FOR EACH ROW
            BEGIN
                DELETE FROM `olap_flows` WHERE `id` = OLD.`id` AND `wallet` = "12" LIMIT 1;
            END');
    }

    private function upTriggersOnCashEmoneyForeignCurrencyFlows()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_emoney_foreign_currency_flows`');
        $this->execute('
            CREATE TRIGGER `olap_flows_insert_cash_emoney_foreign_currency_flows`
            AFTER INSERT ON `cash_emoney_foreign_currency_flows`
            FOR EACH ROW
            BEGIN
                INSERT INTO `olap_flows` (
                    `id`,
                    `company_id`,
                    `contractor_id`,
                    `account_id`,
                    `project_id`,
                    `sale_point_id`,
                    `industry_id`,
                    `wallet`,
                    `type`,
                    `is_accounting`,
                    `item_id`,
                    `date`,
                    `recognition_date`,
                    `is_prepaid_expense`,
                    `amount`,
                    `original_amount`,
                    `currency_id`,
                    `currency_name`,
                    `has_invoice`)
                VALUES (
                    NEW.`id`,
                    NEW.`company_id`,
                    NEW.`contractor_id`,
                    NEW.`emoney_id`,
                    NEW.`project_id`,
                    NEW.`sale_point_id`,
                    NEW.`industry_id`,
                    "13", /* WALLET */
                    NEW.`flow_type`,
                    NEW.`is_accounting`,
                    IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id),
                    NEW.`date`,
                    NEW.`recognition_date`,
                    NEW.`is_prepaid_expense`,
                    NEW.`amount_rub`,
                    NEW.`amount`,
                    NEW.`currency_id`,
                    NEW.`currency_name`,
                    0);
            END');

        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_update_cash_emoney_foreign_currency_flows`');
        $this->execute('
            CREATE TRIGGER `olap_flows_update_cash_emoney_foreign_currency_flows`
            AFTER UPDATE ON `cash_emoney_foreign_currency_flows`
            FOR EACH ROW
            BEGIN
                UPDATE `olap_flows`
                SET
                    `contractor_id` = NEW.`contractor_id`,
                    `account_id` = NEW.`emoney_id`,
                    `project_id` = NEW.`project_id`,
                    `sale_point_id` = NEW.`sale_point_id`,
                    `industry_id` = NEW.`industry_id`,
                    `type` = NEW.`flow_type`,
                    `date` = NEW.`date`,
                    `recognition_date` = NEW.`recognition_date`,
                    `is_prepaid_expense` = NEW.`is_prepaid_expense`,
                    `amount` = NEW.`amount_rub`,
                    `original_amount` = NEW.`amount`,
                    `currency_id` = NEW.`currency_id`,
                    `currency_name` = NEW.`currency_name`,
                    `item_id` = IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id),
                    `is_accounting` = NEW.`is_accounting`
                WHERE `id` = NEW.`id`
                    AND `wallet` = "13";

            END');

        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_delete_cash_emoney_foreign_currency_flows`');
        $this->execute('
            CREATE TRIGGER `olap_flows_delete_cash_emoney_foreign_currency_flows`
            AFTER DELETE ON `cash_emoney_foreign_currency_flows`
            FOR EACH ROW
            BEGIN
                DELETE FROM `olap_flows` WHERE `id` = OLD.`id` AND `wallet` = "13" LIMIT 1;
            END');
    }

    private function upTriggersOnAcquiringOperations()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_acquiring_operation`');
        $this->execute('
            CREATE TRIGGER `olap_flows_insert_acquiring_operation`
            AFTER INSERT ON `acquiring_operation`
            FOR EACH ROW
            BEGIN
                INSERT INTO `olap_flows` (
                    `id`,
                    `company_id`,
                    `contractor_id`,
                    `account_id`,
                    `project_id`,
                    `sale_point_id`,
                    `industry_id`,
                    `wallet`,
                    `type`,
                    `is_accounting`,
                    `item_id`,
                    `date`,
                    `recognition_date`,
                    `is_prepaid_expense`,
                    `amount`,
                    `original_amount`,
                    `currency_id`,
                    `currency_name`,
                    `has_invoice`)
                VALUES (
                    NEW.`id`,
                    NEW.`company_id`,
                    NEW.`contractor_id`,
                    NEW.`acquiring_id`,
                    NEW.`project_id`,
                    NEW.`sale_point_id`,
                    NEW.`industry_id`,
                    "4", /* WALLET */
                    NEW.`flow_type`,
                    NEW.`is_accounting`,
                    IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id),
                    NEW.`date`,
                    NEW.`recognition_date`,
                    0, /* is_prepaid_expense */
                    NEW.`amount`,
                    NEW.`amount`,
                    "643",
                    "RUB",
                    0);
            END');

        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_update_acquiring_operation`');
        $this->execute('
            CREATE TRIGGER `olap_flows_update_acquiring_operation`
            AFTER UPDATE ON `acquiring_operation`
            FOR EACH ROW
            BEGIN
                UPDATE `olap_flows`
                SET
                    `contractor_id` = NEW.`contractor_id`,
                    `account_id` = NEW.`acquiring_id`,
                    `project_id` = NEW.`project_id`,
                    `sale_point_id` = NEW.`sale_point_id`,
                    `industry_id` = NEW.`industry_id`,
                    `type` = NEW.`flow_type`,
                    `is_accounting` = NEW.`is_accounting`,
                    `date` = NEW.`date`,
                    `recognition_date` = NEW.`recognition_date`,
                    `amount` = NEW.`amount`,
                    `original_amount` = NEW.`amount`,
                    `currency_id` = "643",
                    `currency_name` = "RUB",
                    `item_id` = IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id)
                WHERE `id` = NEW.`id`
                    AND `wallet` = "4";
            END');
    }

    private function upTriggersOnCardOperations()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_card_operation`');
        $this->execute('
            CREATE TRIGGER `olap_flows_insert_card_operation`
            AFTER INSERT ON `card_operation`
            FOR EACH ROW
            BEGIN
                INSERT INTO `olap_flows` (
                    `id`,
                    `company_id`,
                    `contractor_id`,
                    `account_id`,
                    `project_id`,
                    `sale_point_id`,
                    `industry_id`,
                    `wallet`,
                    `type`,
                    `is_accounting`,
                    `item_id`,
                    `date`,
                    `recognition_date`,
                    `is_prepaid_expense`,
                    `amount`,
                    `original_amount`,
                    `currency_id`,
                    `currency_name`,
                    `has_invoice`)
                VALUES (
                    NEW.`id`,
                    NEW.`company_id`,
                    NEW.`contractor_id`,
                    NEW.`account_id`,
                    NEW.`project_id`,
                    NEW.`sale_point_id`,
                    NEW.`industry_id`,
                    "5", /* WALLET */
                    NEW.`flow_type`,
                    NEW.`is_accounting`,
                    IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id),
                    NEW.`date`,
                    NEW.`recognition_date`,
                    0, /* is_prepaid_expense */
                    NEW.`amount_rub`,
                    NEW.`amount`,
                    NEW.`currency_id`,
                    NEW.`currency_name`,
                    0);
            END');

        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_update_card_operation`');
        $this->execute('
            CREATE TRIGGER `olap_flows_update_card_operation`
            AFTER UPDATE ON `card_operation`
            FOR EACH ROW
            BEGIN
                UPDATE `olap_flows`
                SET
                    `contractor_id` = NEW.`contractor_id`,
                    `account_id` = NEW.`account_id`,
                    `project_id` = NEW.`project_id`,
                    `sale_point_id` = NEW.`sale_point_id`,
                    `industry_id` = NEW.`industry_id`,
                    `type` = NEW.`flow_type`,
                    `is_accounting` = NEW.`is_accounting`,
                    `date` = NEW.`date`,
                    `recognition_date` = NEW.`recognition_date`,
                    `amount` = NEW.`amount_rub`,
                    `original_amount` = NEW.`amount`,
                    `currency_id` = NEW.`currency_id`,
                    `currency_name` = NEW.`currency_name`,
                    `item_id` = IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id)
                WHERE `id` = NEW.`id`
                    AND `wallet` = "5";
            END');
    }

    private function upTriggersOnCashBankForeignCurrencyFlowsToInvoices()
    {
        $table = 'cash_bank_foreign_currency_flows_to_invoice';

        $this->execute("DROP TRIGGER IF EXISTS `olap_flows_insert_{$table}`");
        $this->execute("
            CREATE TRIGGER `olap_flows_insert_{$table}`
            AFTER INSERT ON `{$table}`
            FOR EACH ROW
            BEGIN
                UPDATE `olap_flows`
                LEFT JOIN (
                    SELECT `flow_id`, SUM(`amount`) `amount`
                    FROM `{$table}`
                    WHERE `flow_id` = NEW.`flow_id`
                    GROUP BY `flow_id`
                ) `t` ON `t`.`flow_id` = `olap_flows`.`id`
                SET
                    `olap_flows`.`has_invoice` = IF(`t`.`flow_id` IS NULL, 0, 1),
                    `olap_flows`.`invoice_amount` = IFNULL(`t`.`amount`, 0)
                WHERE `olap_flows`.`id` = NEW.`flow_id`
                    AND `olap_flows`.`wallet` = 11;
            END
        ");

        $this->execute("DROP TRIGGER IF EXISTS `olap_flows_delete_{$table}`");
        $this->execute("
            CREATE TRIGGER `olap_flows_delete_{$table}`
            AFTER DELETE ON `{$table}`
            FOR EACH ROW
            BEGIN
                UPDATE `olap_flows`
                LEFT JOIN (
                    SELECT `flow_id`, SUM(`amount`) `amount`
                    FROM `{$table}`
                    WHERE `flow_id` = OLD.`flow_id`
                    GROUP BY `flow_id`
                ) `t` ON `t`.`flow_id` = `olap_flows`.`id`
                SET
                    `olap_flows`.`has_invoice` = IF(`t`.`flow_id` IS NULL, 0, 1),
                    `olap_flows`.`invoice_amount` = IFNULL(`t`.`amount`, 0)
                WHERE `olap_flows`.`id` = OLD.`flow_id`
                    AND `olap_flows`.`wallet` = 11;
            END
        ");
    }

    private function upTriggersOnCashOrderForeignCurrencyFlowsToInvoices()
    {
        $table = 'cash_order_foreign_currency_flows_to_invoice';

        $this->execute("DROP TRIGGER IF EXISTS `olap_flows_insert_{$table}`");
        $this->execute("
            CREATE TRIGGER `olap_flows_insert_{$table}`
            AFTER INSERT ON `{$table}`
            FOR EACH ROW
            BEGIN
                UPDATE `olap_flows`
                LEFT JOIN (
                    SELECT `flow_id`, SUM(`amount`) `amount`
                    FROM `{$table}`
                    WHERE `flow_id` = NEW.`flow_id`
                    GROUP BY `flow_id`
                ) `t` ON `t`.`flow_id` = `olap_flows`.`id`
                SET
                    `olap_flows`.`has_invoice` = IF(`t`.`flow_id` IS NULL, 0, 1),
                    `olap_flows`.`invoice_amount` = IFNULL(`t`.`amount`, 0)
                WHERE `olap_flows`.`id` = NEW.`flow_id`
                    AND `olap_flows`.`wallet` = 12;
            END
        ");

        $this->execute("DROP TRIGGER IF EXISTS `olap_flows_delete_{$table}`");
        $this->execute("
            CREATE TRIGGER `olap_flows_delete_{$table}`
            AFTER DELETE ON `{$table}`
            FOR EACH ROW
            BEGIN
                UPDATE `olap_flows`
                LEFT JOIN (
                    SELECT `flow_id`, SUM(`amount`) `amount`
                    FROM `{$table}`
                    WHERE `flow_id` = OLD.`flow_id`
                    GROUP BY `flow_id`
                ) `t` ON `t`.`flow_id` = `olap_flows`.`id`
                SET
                    `olap_flows`.`has_invoice` = IF(`t`.`flow_id` IS NULL, 0, 1),
                    `olap_flows`.`invoice_amount` = IFNULL(`t`.`amount`, 0)
                WHERE `olap_flows`.`id` = OLD.`flow_id`
                    AND `olap_flows`.`wallet` = 12;
            END
        ");
    }

    private function upTriggersOnCashEmoneyForeignCurrencyFlowsToInvoices()
    {
        $table = 'cash_emoney_foreign_currency_flows_to_invoice';

        $this->execute("DROP TRIGGER IF EXISTS `olap_flows_insert_{$table}`");
        $this->execute("
            CREATE TRIGGER `olap_flows_insert_{$table}`
            AFTER INSERT ON `{$table}`
            FOR EACH ROW
            BEGIN
                UPDATE `olap_flows`
                LEFT JOIN (
                    SELECT `flow_id`, SUM(`amount`) `amount`
                    FROM `{$table}`
                    WHERE `flow_id` = NEW.`flow_id`
                    GROUP BY `flow_id`
                ) `t` ON `t`.`flow_id` = `olap_flows`.`id`
                SET
                    `olap_flows`.`has_invoice` = IF(`t`.`flow_id` IS NULL, 0, 1),
                    `olap_flows`.`invoice_amount` = IFNULL(`t`.`amount`, 0)
                WHERE `olap_flows`.`id` = NEW.`flow_id`
                    AND `olap_flows`.`wallet` = 13;
            END
        ");

        $this->execute("DROP TRIGGER IF EXISTS `olap_flows_delete_{$table}`");
        $this->execute("
            CREATE TRIGGER `olap_flows_delete_{$table}`
            AFTER DELETE ON `{$table}`
            FOR EACH ROW
            BEGIN
                UPDATE `olap_flows`
                LEFT JOIN (
                    SELECT `flow_id`, SUM(`amount`) `amount`
                    FROM `{$table}`
                    WHERE `flow_id` = OLD.`flow_id`
                    GROUP BY `flow_id`
                ) `t` ON `t`.`flow_id` = `olap_flows`.`id`
                SET
                    `olap_flows`.`has_invoice` = IF(`t`.`flow_id` IS NULL, 0, 1),
                    `olap_flows`.`invoice_amount` = IFNULL(`t`.`amount`, 0)
                WHERE `olap_flows`.`id` = OLD.`flow_id`
                    AND `olap_flows`.`wallet` = 13;
            END
        ");
    }

    private function downTriggersOnCashBankFlows()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_bank_flows`');
        $this->execute('
            CREATE TRIGGER `olap_flows_insert_cash_bank_flows`
            AFTER INSERT ON `cash_bank_flows`
            FOR EACH ROW
            BEGIN

              INSERT INTO `olap_flows`
              (`id`, `company_id`, `contractor_id`, `account_id`, `project_id`, `sale_point_id`, `industry_id`, `wallet`, `type`, `is_accounting`, `item_id`, `date`, `recognition_date`, `is_prepaid_expense`, `amount`, `has_invoice`)
              VALUES
              (
                NEW.`id`,
                NEW.`company_id`,
                NEW.`contractor_id`,
                NEW.`account_id`,
                NEW.`project_id`,
                NEW.`sale_point_id`,
                NEW.`industry_id`,
                "1", /* WALLET */
                NEW.`flow_type`,
                1,
                IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id),
                NEW.`date`,
                NEW.`recognition_date`,
                NEW.`is_prepaid_expense`,
                NEW.`amount`,
                0
              );
            END');

        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_update_cash_bank_flows`');
        $this->execute('
            CREATE TRIGGER `olap_flows_update_cash_bank_flows`
            AFTER UPDATE ON `cash_bank_flows`
            FOR EACH ROW
            BEGIN

              UPDATE `olap_flows` SET
                `contractor_id` = NEW.`contractor_id`,
                `account_id` = NEW.`account_id`,
                `project_id` = NEW.`project_id`,
                `sale_point_id` = NEW.`sale_point_id`,
                `industry_id` = NEW.`industry_id`,
                `type` = NEW.`flow_type`,
                `date` = NEW.`date`,
                `recognition_date` = NEW.`recognition_date`,
                `is_prepaid_expense` = NEW.`is_prepaid_expense`,
                `amount` = NEW.`amount`,
                `item_id` = IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id)
              WHERE `id` = NEW.`id` AND `wallet` = "1";

            END');
    }

    private function downTriggersOnCashOrderFlows()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_order_flows`');
        $this->execute('
            CREATE TRIGGER `olap_flows_insert_cash_order_flows`
            AFTER INSERT ON `cash_order_flows`
            FOR EACH ROW
            BEGIN

              INSERT INTO `olap_flows`
              (`id`, `company_id`, `contractor_id`, `account_id`, `project_id`, `sale_point_id`, `industry_id`, `wallet`, `type`, `is_accounting`, `item_id`, `date`, `recognition_date`, `is_prepaid_expense`, `amount`, `has_invoice`)
              VALUES
              (
                NEW.`id`,
                NEW.`company_id`,
                NEW.`contractor_id`,
                NEW.`cashbox_id`,
                NEW.`project_id`,
                NEW.`sale_point_id`,
                NEW.`industry_id`,
                "2", /* WALLET */
                NEW.`flow_type`,
                NEW.`is_accounting`,
                IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id),
                NEW.`date`,
                NEW.`recognition_date`,
                NEW.`is_prepaid_expense`,
                NEW.`amount`,
                0
              );

            END');

        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_update_cash_order_flows`');
        $this->execute('
            CREATE TRIGGER `olap_flows_update_cash_order_flows`
            AFTER UPDATE ON `cash_order_flows`
            FOR EACH ROW
            BEGIN

              UPDATE `olap_flows` SET
                `contractor_id` = NEW.`contractor_id`,
                `account_id` = NEW.`cashbox_id`,
                `project_id` = NEW.`project_id`,
                `sale_point_id` = NEW.`sale_point_id`,
                `industry_id` = NEW.`industry_id`,
                `type` = NEW.`flow_type`,
                `date` = NEW.`date`,
                `recognition_date` = NEW.`recognition_date`,
                `is_prepaid_expense` = NEW.`is_prepaid_expense`,
                `amount` = NEW.`amount`,
                `item_id` = IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id),
                `is_accounting` = NEW.`is_accounting`
              WHERE `id` = NEW.`id` AND `wallet` = "2";

            END');
    }

    private function downTriggersOnCashEmoneyFlows()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_emoney_flows`');
        $this->execute('
            CREATE TRIGGER `olap_flows_insert_cash_emoney_flows`
            AFTER INSERT ON `cash_emoney_flows`
            FOR EACH ROW
            BEGIN

              INSERT INTO `olap_flows`
              (`id`, `company_id`, `contractor_id`, `account_id`, `project_id`, `sale_point_id`, `industry_id`, `wallet`, `type`, `is_accounting`, `item_id`, `date`, `recognition_date`, `is_prepaid_expense`, `amount`, `has_invoice`)
              VALUES
              (
                NEW.`id`,
                NEW.`company_id`,
                NEW.`contractor_id`,
                NEW.`emoney_id`,
                NEW.`project_id`,
                NEW.`sale_point_id`,
                NEW.`industry_id`,
                "3", /* WALLET */
                NEW.`flow_type`,
                NEW.`is_accounting`,
                IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id),
                NEW.`date`,
                NEW.`recognition_date`,
                NEW.`is_prepaid_expense`,
                NEW.`amount`,
                0
              );
            END');

        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_update_cash_emoney_flows`');
        $this->execute('
            CREATE TRIGGER `olap_flows_update_cash_emoney_flows`
            AFTER UPDATE ON `cash_emoney_flows`
            FOR EACH ROW
            BEGIN

              UPDATE `olap_flows` SET
                `contractor_id` = NEW.`contractor_id`,
                `account_id` = NEW.`emoney_id`,
                `project_id` = NEW.`project_id`,
                `sale_point_id` = NEW.`sale_point_id`,
                `industry_id` = NEW.`industry_id`,
                `type` = NEW.`flow_type`,
                `date` = NEW.`date`,
                `recognition_date` = NEW.`recognition_date`,
                `is_prepaid_expense` = NEW.`is_prepaid_expense`,
                `amount` = NEW.`amount`,
                `item_id` = IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id),
                `is_accounting` = NEW.`is_accounting`
              WHERE `id` = NEW.`id` AND `wallet` = "3";

            END');
    }

    private function downTriggersOnCashBankForeignCurrencyFlows()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_bank_foreign_currency_flows`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_update_cash_bank_foreign_currency_flows`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_delete_cash_bank_foreign_currency_flows`');
    }

    private function downTriggersOnCashOrderForeignCurrencyFlows()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_order_foreign_currency_flows`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_update_cash_order_foreign_currency_flows`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_delete_cash_order_foreign_currency_flows`');
    }

    private function downTriggersOnCashEmoneyForeignCurrencyFlows()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_emoney_foreign_currency_flows`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_update_cash_emoney_foreign_currency_flows`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_delete_cash_emoney_foreign_currency_flows`');
    }

    private function downTriggersOnAcquiringOperations()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_acquiring_operation`');
        $this->execute('
            CREATE TRIGGER `olap_flows_insert_acquiring_operation`
            AFTER INSERT ON `acquiring_operation`
            FOR EACH ROW
            BEGIN

              INSERT INTO `olap_flows`
              (`id`, `company_id`, `contractor_id`, `account_id`, `project_id`, `sale_point_id`, `industry_id`, `wallet`, `type`, `is_accounting`, `item_id`, `date`, `recognition_date`, `is_prepaid_expense`, `amount`, `has_invoice`)
              VALUES
              (
                NEW.`id`,
                NEW.`company_id`,
                NEW.`contractor_id`,
                NEW.`acquiring_id`,
                NEW.`project_id`,
                NEW.`sale_point_id`,
                NEW.`industry_id`,
                "4", /* WALLET */
                NEW.`flow_type`,
                NEW.`is_accounting`,
                IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id),
                NEW.`date`,
                NEW.`recognition_date`,
                0, /* is_prepaid_expense */
                NEW.`amount`,
                0
              );
            END');

        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_update_acquiring_operation`');
        $this->execute('
            CREATE TRIGGER `olap_flows_update_acquiring_operation`
            AFTER UPDATE ON `acquiring_operation`
            FOR EACH ROW
            BEGIN

              UPDATE `olap_flows` SET
                `contractor_id` = NEW.`contractor_id`,
                `account_id` = NEW.`acquiring_id`,
                `project_id` = NEW.`project_id`,
                `sale_point_id` = NEW.`sale_point_id`,
                `industry_id` = NEW.`industry_id`,
                `type` = NEW.`flow_type`,
                `is_accounting` = NEW.`is_accounting`,
                `date` = NEW.`date`,
                `recognition_date` = NEW.`recognition_date`,
                `amount` = NEW.`amount`,
                `item_id` = IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id)
              WHERE `id` = NEW.`id` AND `wallet` = "4";

            END');
    }

    private function downTriggersOnCardOperations()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_card_operation`');
        $this->execute('
            CREATE TRIGGER `olap_flows_insert_card_operation`
            AFTER INSERT ON `card_operation`
            FOR EACH ROW
            BEGIN

              INSERT INTO `olap_flows`
              (`id`, `company_id`, `contractor_id`, `account_id`, `project_id`, `sale_point_id`, `industry_id`, `wallet`, `type`, `is_accounting`, `item_id`, `date`, `recognition_date`, `is_prepaid_expense`, `amount`, `has_invoice`)
              VALUES
              (
                NEW.`id`,
                NEW.`company_id`,
                NEW.`contractor_id`,
                NEW.`account_id`,
                NEW.`project_id`,
                NEW.`sale_point_id`,
                NEW.`industry_id`,
                "5", /* WALLET */
                NEW.`flow_type`,
                NEW.`is_accounting`,
                IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id),
                NEW.`date`,
                NEW.`recognition_date`,
                0, /* is_prepaid_expense */
                NEW.`amount`,
                0
              );
            END');

        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_update_card_operation`');
        $this->execute('
            CREATE TRIGGER `olap_flows_update_card_operation`
            AFTER UPDATE ON `card_operation`
            FOR EACH ROW
            BEGIN

              UPDATE `olap_flows` SET
                `contractor_id` = NEW.`contractor_id`,
                `account_id` = NEW.`account_id`,
                `project_id` = NEW.`project_id`,
                `sale_point_id` = NEW.`sale_point_id`,
                `industry_id` = NEW.`industry_id`,
                `type` = NEW.`flow_type`,
                `is_accounting` = NEW.`is_accounting`,
                `date` = NEW.`date`,
                `recognition_date` = NEW.`recognition_date`,
                `amount` = NEW.`amount`,
                `item_id` = IF (NEW.flow_type = 0, NEW.expenditure_item_id, NEW.income_item_id)
              WHERE `id` = NEW.`id` AND `wallet` = "5";

            END');
    }

    private function downTriggersOnCashBankForeignCurrencyFlowsToInvoices()
    {
        $table = 'cash_bank_foreign_currency_flows_to_invoice';
        $this->execute("DROP TRIGGER IF EXISTS `olap_flows_insert_{$table}`");
        $this->execute("DROP TRIGGER IF EXISTS `olap_flows_delete_{$table}`");
    }

    private function downTriggersOnCashOrderForeignCurrencyFlowsToInvoices()
    {
        $table = 'cash_order_foreign_currency_flows_to_invoice';
        $this->execute("DROP TRIGGER IF EXISTS `olap_flows_insert_{$table}`");
        $this->execute("DROP TRIGGER IF EXISTS `olap_flows_delete_{$table}`");
    }

    private function downTriggersOnCashEmoneyForeignCurrencyFlowsToInvoices()
    {
        $table = 'cash_emoney_foreign_currency_flows_to_invoice';
        $this->execute("DROP TRIGGER IF EXISTS `olap_flows_insert_{$table}`");
        $this->execute("DROP TRIGGER IF EXISTS `olap_flows_delete_{$table}`");
    }
}
