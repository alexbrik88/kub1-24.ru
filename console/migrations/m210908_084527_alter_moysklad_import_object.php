<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210908_084527_alter_moysklad_import_object extends Migration
{
    public $objectType = 'moysklad_object_type';
    public $object = 'moysklad_object';

    public function up()
    {
        // OBJECT
        $table = $this->object;

        $this->dropForeignKey("FK_{$table}_to_product_group", $table);
        $this->dropForeignKey("FK_{$table}_to_product", $table);
        $this->dropForeignKey("FK_{$table}_to_contractor", $table);
        $this->dropForeignKey("FK_{$table}_to_agreement", $table);
        $this->dropForeignKey("FK_{$table}_to_object_type", $table);
        $this->dropForeignKey("FK_{$table}_to_company", $table);

        $this->dropIndex("IDX_{$table}_number", $table);
        $this->dropIndex("IDX_{$table}_inn", $table);

        try {
            $this->dropIndex("FK_{$table}_to_product_group", $table);
            $this->dropIndex("FK_{$table}_to_product", $table);
            $this->dropIndex("FK_{$table}_to_contractor", $table);
            $this->dropIndex("FK_{$table}_to_agreement", $table);
            $this->dropIndex("FK_{$table}_to_object_type", $table);
            $this->dropIndex("FK_{$table}_to_company", $table);
        } catch (Throwable $e) {}

        $this->createIndex("IDX_{$table}_type", $table, ['company_id', 'object_type_id']);
        $this->createIndex("IDX_{$table}_number", $table, ['company_id', 'object_number']);
        $this->createIndex("IDX_{$table}_inn", $table, ['company_id', 'object_inn']);
        $this->createIndex("IDX_{$table}_code", $table, ['company_id', 'object_code']);

        $this->addForeignKey("FK_{$table}_to_company", $table, 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("FK_{$table}_to_object_type", $table, 'object_type_id', $this->objectType, 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $table = $this->object;

        $this->dropForeignKey("FK_{$table}_to_company", $table);
        $this->dropForeignKey("FK_{$table}_to_object_type", $table);

        $this->dropIndex("IDX_{$table}_type", $table);
        $this->dropIndex("IDX_{$table}_number", $table);
        $this->dropIndex("IDX_{$table}_inn", $table);
        $this->dropIndex("IDX_{$table}_code", $table);

        $this->createIndex("IDX_{$table}_number", $table, ['company_id', 'object_type_id', 'object_number']);
        $this->createIndex("IDX_{$table}_inn", $table, ['company_id', 'object_type_id', 'object_inn']);

        $this->addForeignKey("FK_{$table}_to_company", $table, 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("FK_{$table}_to_object_type", $table, 'object_type_id', $this->objectType, 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("FK_{$table}_to_agreement", $table, 'agreement_id', 'agreement', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("FK_{$table}_to_contractor", $table, 'contractor_id', 'contractor', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("FK_{$table}_to_product", $table, 'product_id', 'product', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("FK_{$table}_to_product_group", $table, 'product_group_id', 'product_group', 'id', 'CASCADE', 'CASCADE');
    }
}
