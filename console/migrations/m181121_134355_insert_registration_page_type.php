<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181121_134355_insert_registration_page_type extends Migration
{
    public $tableName = 'registration_page_type';

    public function safeUp()
    {
        $this->insert($this->tableName, [
            'id' => 10,
            'name' => 'Робот бухгалтер',
        ]);
    }

    public function safeDown()
    {
        $this->delete($this->tableName, ['id' => 10]);
    }
}


