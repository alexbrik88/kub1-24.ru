<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181014_154913_alter_agreement_new_add_columns extends Migration
{
    public function safeUp()
    {
  		$this->addColumn('{{%agreement_new}}', 'status_out_id', $this->integer());
		$this->addColumn('{{%agreement_new}}', 'status_out_author_id', $this->integer());
    }
    
    public function safeDown()
    {

    }
}
