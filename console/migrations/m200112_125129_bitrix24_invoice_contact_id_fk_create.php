<?php

use console\components\db\Migration;

class m200112_125129_bitrix24_invoice_contact_id_fk_create extends Migration
{
    const TABLE = '{{%bitrix24_invoice}}';

    public function safeUp()
    {
        $this->addForeignKey(
            'bitrix24_invoice_contact_id', self::TABLE, ['company_id', 'contact_id'],
            '{{%bitrix24_contact}}', ['company_id', 'id'],
            'CASCADE', 'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('bitrix24_invoice_contact_id', self::TABLE);
    }
}
