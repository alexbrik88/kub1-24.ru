<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200305_111855_insert_service_subscribe_contains_tariff extends Migration
{
    public function safeUp()
    {
        $this->batchInsert('{{%service_subscribe_contains_tariff}}', [
            'tariff_id',
            'contains_tariff_id',
        ], [
            [5, 18],
            [6, 19],
            [7, 20],
            [8, 18],
            [9, 19],
            [10, 20],
            [43, 18],
            [44, 19],
            [45, 20],
            [46, 24],
            [47, 25],
            [48, 26],
        ]);
    }

    public function safeDown()
    {
        $this->truncateTable('{{%service_subscribe_contains_tariff}}');
    }
}
