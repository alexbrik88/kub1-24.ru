<?php

use yii\db\Exception;
use yii\db\Migration;

class m210607_150704_update_contractor_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%contractor}}';

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function safeUp(): void
    {
        $rows = $this->db->createCommand("SELECT id, company_id, name FROM contractor_campaign")->queryAll();

        array_walk($rows, function (array $row): void {
            $this->update(
                self::TABLE_NAME,
                ['campaign_id' => $row['id']],
                ['company_id' => $row['company_id'], 'source' => $row['name']]
            );
        });
    }
}
