<?php

use console\components\db\Migration;

class m200124_174858_contractor_verified_add extends Migration
{
    const TABLE = '{{%contractor}}';

    public function up()
    {
        $this->addColumn(self::TABLE, 'verified',
            $this->boolean()->notNull()->defaultValue(false)->comment('Клиент "проверил" контрагента (хотя бы раз заходил на страницу "досье")'));
    }

    public function down()
    {
        $this->dropColumn(self::TABLE, 'verified');
    }
}
