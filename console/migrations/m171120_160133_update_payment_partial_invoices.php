<?php

use console\components\db\Migration;
use yii\db\Schema;
use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;

class m171120_160133_update_payment_partial_invoices extends Migration
{
    public $tableName = 'invoice';

    public function safeUp()
    {
        /* @var $invoice Invoice */
        foreach (Invoice::find()->byDeleted()->byStatus(InvoiceStatus::STATUS_PAYED_PARTIAL)->all() as $invoice) {
            $paymentPartialAmount = $invoice->getPaidAmount();
            if ($invoice->total_amount_with_nds > $paymentPartialAmount) {
                $this->update($this->tableName, ['remaining_amount' => $invoice->total_amount_with_nds - $paymentPartialAmount], ['id' => $invoice->id]);
                $this->update($this->tableName, ['payment_partial_amount' => $paymentPartialAmount], ['id' => $invoice->id]);
            } else {
                $this->update($this->tableName, ['payment_partial_amount' => null], ['id' => $invoice->id]);
            }
        }
    }

    public function safeDown()
    {

    }
}


