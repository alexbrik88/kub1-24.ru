<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160211_090828_create_table_for_whom_prompt extends Migration
{
    public $tForWhomPrompt = 'for_whom_prompt';

    public function safeUp()
    {
        $this->createTable($this->tForWhomPrompt, [
            'id' => $this->primaryKey(11),
            'name' => $this->text(),
        ]);
        $this->batchInsert($this->tForWhomPrompt, ['id', 'name'], [
            [1, 'Всем'],
            [2, 'ИП'],
            [3, 'ООО'],
            [4, 'ЗАО'],
            [5, 'ПАО'],
        ]);
    }
    
    public function safeDown()
    {
        $this->dropTable($this->tForWhomPrompt);
    }
}


