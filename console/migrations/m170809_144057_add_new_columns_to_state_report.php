<?php

use console\components\db\Migration;
use yii\db\Schema;
use common\models\report\ReportState;
use common\models\employee\Employee;

class m170809_144057_add_new_columns_to_state_report extends Migration
{
    public $tableName = 'report_state';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'days_without_payment', $this->integer()->defaultValue(null));
        $this->addColumn($this->tableName, 'sum_company_images', $this->integer()->defaultValue(null));
        $this->addColumn($this->tableName, 'employees_count', $this->integer()->defaultValue(null));
        $this->addColumn($this->tableName, 'company_type_id', $this->integer()->defaultValue(null));
        $this->addColumn($this->tableName, 'company_taxation_type_name', $this->string()->defaultValue(null));

        /* @var $reportState ReportState */
        foreach (ReportState::find()->all() as $reportState) {
            $reportState->days_without_payment = intval(($reportState->pay_date - $reportState->company->created_at) / (60 * 60 * 24));
            $reportState->sum_company_images = (int) ($reportState->has_logo + $reportState->has_print + $reportState->has_signature);
            $reportState->employees_count = Employee::find()
                ->byCompany($reportState->company_id)
                ->byIsActive(Employee::ACTIVE)
                ->byIsDeleted(Employee::NOT_DELETED)
                ->byIsWorking(Employee::STATUS_IS_WORKING)
                ->andWhere(['not', ['email' => 'support@kub-24.ru']])
                ->andWhere(['<=', Employee::tableName() . '.created_at', $reportState->pay_date])
                ->count();
            $reportState->company_type_id = $reportState->company->company_type_id;
            $reportState->company_taxation_type_name = $reportState->company->companyTaxationType->name;
            $reportState->save();

        }


        $this->addForeignKey($this->tableName . '_company_type_id', $this->tableName, 'company_type_id', 'company_type', 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_company_type_id', $this->tableName);

        $this->dropColumn($this->tableName, 'days_without_payment');
        $this->dropColumn($this->tableName, 'sum_company_images');
        $this->dropColumn($this->tableName, 'employees_count');
        $this->dropColumn($this->tableName, 'company_type_id');
        $this->dropColumn($this->tableName, 'company_taxation_type_name');
    }
}


