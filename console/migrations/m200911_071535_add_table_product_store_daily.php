<?php

use console\components\db\Migration;

class m200911_071535_add_table_product_store_daily extends Migration
{
    /**
     * @var string
     */
    public $tableName = 'product_store_daily';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'date' => $this->date(),
            'company_id' => $this->integer(11),
            'product_id' => $this->integer(11),
            'store_id' => $this->integer(11),
            'initial_quantity' => $this->decimal(20, 10)->defaultValue(0),
            'irreducible_quantity' => $this->decimal(20, 10)->defaultValue(0),
            'quantity' => $this->decimal(20, 10)->defaultValue(0),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
        ]);

        $this->createIndex($this->tableName . '_product_store_idx', $this->tableName, ['product_id', 'store_id']);
    }

    public function safeDown()
    {
        try {
            $this->dropIndex($this->tableName . '_product_store_idx', $this->tableName);
            $this->dropTable($this->tableName);
        } catch (Exception $e) {
            echo $e->getMessage(), PHP_EOL;
        }

    }

}
