<?php

use console\components\db\Migration;
use yii\db\Expression;
use yii\db\Schema;

class m190926_115815_alter_cash_emoney_flows_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cash_emoney_flows', 'recognition_date', $this->date()->defaultValue(null)->after('date'));
        $this->addColumn('cash_emoney_flows', 'is_prepaid_expense',  $this->boolean()->defaultValue(false));
        $this->addColumn('cash_emoney_flows', 'number', $this->string()->defaultValue(null)->after('description'));

        $this->update('cash_emoney_flows', ['recognition_date' => new Expression('[[date]]')]);
    }

    public function safeDown()
    {
        $this->dropColumn('cash_emoney_flows', 'is_prepaid_expense');
        $this->dropColumn('cash_emoney_flows', 'recognition_date');
        $this->dropColumn('cash_emoney_flows', 'number');
    }
}
