<?php

use common\models\document\status\InvoiceStatus;
use console\components\db\Migration;
use yii\db\Schema;

class m180412_112908_update_invoice extends Migration
{
    public function safeUp()
    {
        $this->execute("
            UPDATE {{invoice}}
            LEFT JOIN (
                SELECT [[id]], SUM([[amount]]) AS [[sum]]
                FROM (
                    (
                        SELECT {{invoice}}.[[id]], {{flow}}.[[amount]]
                        FROM {{invoice}}
                        INNER JOIN {{cash_bank_flow_to_invoice}} {{link}} ON {{invoice}}.[[id]] = {{link}}.[[invoice_id]]
                        LEFT JOIN {{cash_bank_flows}} {{flow}} ON {{flow}}.[[id]] = {{link}}.[[flow_id]]
                    )
                    UNION ALL
                    (
                        SELECT {{invoice}}.[[id]], {{flow}}.[[amount]]
                        FROM {{invoice}}
                        INNER JOIN {{cash_emoney_flow_to_invoice}} {{link}} ON {{invoice}}.[[id]] = {{link}}.[[invoice_id]]
                        LEFT JOIN {{cash_emoney_flows}} {{flow}} ON {{flow}}.[[id]] = {{link}}.[[flow_id]]
                    )
                    UNION ALL
                    (
                        SELECT {{invoice}}.[[id]], {{flow}}.[[amount]]
                        FROM {{invoice}}
                        INNER JOIN {{cash_order_flow_to_invoice}} {{link}} ON {{invoice}}.[[id]] = {{link}}.[[invoice_id]]
                        LEFT JOIN {{cash_order_flows}} {{flow}} ON {{flow}}.[[id]] = {{link}}.[[flow_id]]
                    )
                ) AS t
                GROUP BY [[id]]
            ) AS {{paid}} ON {{invoice}}.[[id]] = {{paid}}.[[id]]
            SET {{invoice}}.[[payment_partial_amount]] = IF({{paid}}.[[sum]] IS NULL, 0,
                LEAST({{paid}}.[[sum]], {{invoice}}.[[total_amount_with_nds]])
            )
            WHERE {{invoice}}.[[invoice_status_id]] <> :auto
            AND {{invoice}}.[[is_deleted]] = 0
        ", [
            ':auto' => InvoiceStatus::STATUS_AUTOINVOICE,
        ]);

        $this->execute("
            UPDATE {{invoice}}
            SET {{invoice}}.[[remaining_amount]] = ({{invoice}}.[[total_amount_with_nds]] - {{invoice}}.[[payment_partial_amount]])
            WHERE {{invoice}}.[[invoice_status_id]] <> :auto
            AND {{invoice}}.[[is_deleted]] = 0
        ", [
            ':auto' => InvoiceStatus::STATUS_AUTOINVOICE,
        ]);

        $this->execute("
            UPDATE {{invoice}}
            SET {{invoice}}.[[invoice_status_id]] = IF(
                {{invoice}}.[[total_amount_with_nds]] > {{invoice}}.[[payment_partial_amount]],
                :part,
                :paid
            )
            WHERE {{invoice}}.[[payment_partial_amount]] > 0
            AND {{invoice}}.[[invoice_status_id]] NOT IN (:auto, :reject)
            AND {{invoice}}.[[is_deleted]] = 0
        ", [
            ':part' => InvoiceStatus::STATUS_PAYED_PARTIAL,
            ':paid' => InvoiceStatus::STATUS_PAYED,
            ':auto' => InvoiceStatus::STATUS_AUTOINVOICE,
            ':reject' => InvoiceStatus::STATUS_REJECTED,
        ]);
    }

    public function safeDown()
    {
        echo 'This migration not rolled back!';
    }
}
