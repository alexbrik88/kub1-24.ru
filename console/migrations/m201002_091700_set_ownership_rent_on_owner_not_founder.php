<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201002_091700_set_ownership_rent_on_owner_not_founder extends Migration
{
    public function safeUp()
    {
        $this->execute('
            UPDATE `rent_entity`
                INNER JOIN `contractor` ON `contractor`.`id` = `rent_entity`.`owner_id`
            SET `ownership` = "rent"
            WHERE `contractor`.`type` != 3 AND `rent_entity`.`ownership` = "self";
        ');
    }

    public function safeDown()
    {
        return true;
    }
}
