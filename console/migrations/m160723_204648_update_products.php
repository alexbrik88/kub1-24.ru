<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160723_204648_update_products extends Migration
{
    public $tProduct = 'product';

    public function safeUp()
    {
        $this->update($this->tProduct, ['price_for_buy_with_nds' => 0], ['is', 'price_for_buy_with_nds', null]);
        $this->update($this->tProduct, ['price_for_sell_with_nds' => 0], ['is', 'price_for_sell_with_nds', null]);
    }

    public function safeDown()
    {
        $this->update($this->tProduct, ['price_for_buy_with_nds' => null], ['price_for_buy_with_nds' => 0]);
        $this->update($this->tProduct, ['price_for_sell_with_nds' => null], ['price_for_sell_with_nds' => 0]);
    }
}


