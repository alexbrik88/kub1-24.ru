<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210606_195355_alter_user_config_change_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('user_config','order_many_create_recognition_date', $this->tinyInteger()->notNull()->defaultValue(0));
        $this->execute("UPDATE `user_config` SET `order_many_create_recognition_date` = 0");
    }

    public function safeDown()
    {
        // no down
    }
}
