
<?php

use console\components\db\Migration;
use yii\db\Schema;

class m171024_104601_create_crm_user extends Migration
{
    public $tableName = 'crm_user';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'user_fio' => $this->string()->notNull(),
            'user_id' => $this->integer()->notNull(),
            'company_name' => $this->string()->notNull(),
            'chat_photo' => $this->string()->notNull(),
            'is_online' => $this->boolean()->defaultValue(false),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}


