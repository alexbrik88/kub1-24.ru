<?php

use console\components\db\Migration;
use yii\db\Schema;

class m220329_133052_alter_project_customer_upgrade_column_type extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('project_customer', 'amount', $this->bigInteger());
    }

    public function safeDown()
    {
        $this->alterColumn('project_customer', 'amount', $this->integer());
    }
}
