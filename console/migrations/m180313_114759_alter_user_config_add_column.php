<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180313_114759_alter_user_config_add_column extends Migration
{
    public $tableName = 'user_config';

    public function safeUp()
    {
        $this->addColumn('user_config', 'product_image', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn('user_config', 'product_comment', $this->boolean()->notNull()->defaultValue(false));

        $this->addCommentOnColumn('user_config', 'product_image', 'Показ колонки "Картинка" в списке товаров');
        $this->addCommentOnColumn('user_config', 'product_comment', 'Показ колонки "Описание" в списке товаров');
    }

    public function safeDown()
    {
        $this->dropColumn('user_config', 'product_image');
        $this->dropColumn('user_config', 'product_comment');
    }
}
