<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210705_092707_alter_product_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('product', 'is_traceable', $this->tinyInteger()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('product', 'is_traceable');
    }
}
