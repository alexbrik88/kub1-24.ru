<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200407_145336_alter_company_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%company}}', 'requisites_updated_at', $this->integer()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%company}}', 'requisites_updated_at');
    }
}
