<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210928_065758_create_service_contractor extends Migration
{
    public $table = 'service_contractor_item';

    public function safeUp()
    {
        $table = $this->table;

        $this->createTable($table, [
            'company_id' => $this->integer()->notNull(),
            'invoice_income_item_id' => $this->integer(),
            'invoice_expenditure_item_id' => $this->integer(),
        ]);

        $this->addForeignKey("FK_{$table}_to_company", $table, 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("FK_{$table}_to_income_item", $table, 'invoice_income_item_id', 'invoice_income_item', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("FK_{$table}_to_expenditure_item", $table, 'invoice_expenditure_item_id', 'invoice_expenditure_item', 'id', 'CASCADE', 'CASCADE');

        $this->addPrimaryKey("PK_{$table}", $table, ['company_id']);
    }

    public function safeDown()
    {
        $table = $this->table;

        $this->dropForeignKey("FK_{$table}_to_company", $table);
        $this->dropForeignKey("FK_{$table}_to_income_item", $table);
        $this->dropForeignKey("FK_{$table}_to_expenditure_item", $table);

        $this->dropTable($table);
    }
}
