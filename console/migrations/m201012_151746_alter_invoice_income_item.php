<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201012_151746_alter_invoice_income_item extends Migration
{
    public function safeUp()
    {
        $this->addColumn('invoice_income_item', 'is_prepayment', $this->tinyInteger()->notNull()->defaultValue(0));

        $this->execute("
            UPDATE `invoice_income_item` 
            SET `is_prepayment` = 1 
            WHERE `id` IN (1); 
        ");
    }

    public function safeDown()
    {
        $this->dropColumn('invoice_income_item', 'is_prepayment');
    }
}
