<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190624_120432_alter_contractor_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('contractor', 'is_agent', $this->boolean()->notNull()->defaultValue(false));



    }

    public function safeDown()
    {
        $this->dropColumn('contractor', 'is_agent');
    }
}