<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210712_203941_add_currency_marketing_plan_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('marketing_plan', 'currency', $this->tinyInteger(2)->unsigned()->notNull()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('marketing_plan', 'currency');
    }
}
