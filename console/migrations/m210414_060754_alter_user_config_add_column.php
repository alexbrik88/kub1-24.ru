<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210414_060754_alter_user_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_config','order_many_create_recognition_date',$this->tinyInteger()->notNull()->defaultValue(1));
        $this->addColumn('user_config','order_many_create_invoices_list',$this->tinyInteger()->notNull()->defaultValue(1));
        $this->addColumn('user_config','order_many_create_project',$this->tinyInteger()->notNull()->defaultValue(0));
        $this->addColumn('user_config','order_many_create_sale_point',$this->tinyInteger()->notNull()->defaultValue(0));
        $this->addColumn('user_config','order_many_create_direction',$this->tinyInteger()->notNull()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('user_config','order_many_create_recognition_date');
        $this->dropColumn('user_config','order_many_create_invoices_list');
        $this->dropColumn('user_config','order_many_create_project');
        $this->dropColumn('user_config','order_many_create_sale_point');
        $this->dropColumn('user_config','order_many_create_direction');
    }
}