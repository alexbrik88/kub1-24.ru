<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180226_112942_add_store_author_id_to_order_document extends Migration
{
    public $tableName = 'order_document';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'store_author_id', $this->integer()->defaultValue(null));
        $this->addForeignKey($this->tableName . '_store_author_id', $this->tableName, 'store_author_id', 'store_user', 'id', 'RESTRICT', 'CASCADE');

        $this->addColumn($this->tableName, 'status_store_author_id', $this->integer()->defaultValue(null));
        $this->addForeignKey($this->tableName . '_status_store_author_id', $this->tableName, 'status_store_author_id', 'store_user', 'id', 'RESTRICT', 'CASCADE');

        $this->dropForeignKey($this->tableName . '_author_id', $this->tableName);
        $this->alterColumn($this->tableName, 'author_id', $this->integer()->defaultValue(null));
        $this->addForeignKey($this->tableName . '_author_id', $this->tableName, 'author_id', 'employee', 'id', 'RESTRICT', 'CASCADE');

        $this->dropForeignKey($this->tableName . '_status_author_id', $this->tableName);
        $this->alterColumn($this->tableName, 'status_author_id', $this->integer()->defaultValue(null));
        $this->addForeignKey($this->tableName . '_status_author_id', $this->tableName, 'status_author_id', 'employee', 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_author_id', $this->tableName);
        $this->alterColumn($this->tableName, 'author_id', $this->integer()->notNull());
        $this->addForeignKey($this->tableName . '_author_id', $this->tableName, 'author_id', 'employee', 'id', 'RESTRICT', 'CASCADE');

        $this->dropForeignKey($this->tableName . '_store_author_id', $this->tableName);
        $this->dropColumn($this->tableName, 'store_author_id');

        $this->dropForeignKey($this->tableName . '_status_author_id', $this->tableName);
        $this->alterColumn($this->tableName, 'status_author_id', $this->integer()->notNull());
        $this->addForeignKey($this->tableName . '_status_author_id', $this->tableName, 'status_author_id', 'employee', 'id', 'RESTRICT', 'CASCADE');

        $this->dropForeignKey($this->tableName . '_store_author_id', $this->tableName);
        $this->dropColumn($this->tableName, 'store_author_id');
    }
}


