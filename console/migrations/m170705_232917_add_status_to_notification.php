<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170705_232917_add_status_to_notification extends Migration
{
    public $tableName = 'notification';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'status', $this->boolean()->defaultValue(false));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'status');
    }
}


