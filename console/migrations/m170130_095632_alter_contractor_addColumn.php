<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170130_095632_alter_contractor_addColumn extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%contractor}}', 'source', $this->string(255)->defaultValue(null));
        $this->addColumn('{{%contractor}}', 'comment', $this->text()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%contractor}}', 'comment');
    }
}


