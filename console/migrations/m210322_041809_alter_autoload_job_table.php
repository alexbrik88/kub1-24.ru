<?php

use yii\db\Migration;

class m210322_041809_alter_autoload_job_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%autoload_job}}';

    /**
     * @inheritDoc
     */
    public function up()
    {
        $this->renameColumn(self::TABLE_NAME, 'type', 'command_type');
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        $this->renameColumn(self::TABLE_NAME, 'command_type', 'type');
    }
}
