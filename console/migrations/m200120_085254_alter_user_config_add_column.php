<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200120_085254_alter_user_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_config', 'page_size', 'SMALLINT NOT NULL DEFAULT 10');
    }

    public function safeDown()
    {
        $this->dropColumn('user_config', 'page_size');
    }
}
