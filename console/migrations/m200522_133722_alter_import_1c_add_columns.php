<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200522_133722_alter_import_1c_add_columns extends Migration
{
    public $table = 'import_1c';

    public function safeUp()
    {
        $this->addColumn($this->table, 'total_out_invoice_factures', $this->integer()->defaultValue(0)->after('total_in_invoice_factures'));
        $this->addColumn($this->table, 'total_out_upds', $this->integer()->defaultValue(0)->after('total_in_invoice_factures'));
        $this->addColumn($this->table, 'total_out_invoices', $this->integer()->defaultValue(0)->after('total_in_invoice_factures'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'total_out_invoices');
        $this->dropColumn($this->table, 'total_out_upds');
        $this->dropColumn($this->table, 'total_out_invoice_factures');
    }
}
