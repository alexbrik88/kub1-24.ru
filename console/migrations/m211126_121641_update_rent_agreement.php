<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211126_121641_update_rent_agreement extends Migration
{
    public function safeUp()
    {
        $this->execute('
            UPDATE {{%rent_agreement}}
            SET {{%rent_agreement}}.[[amount]] = ROUND({{%rent_agreement}}.[[amount]] * 100)
        ');
    }

    public function safeDown()
    {
        $this->execute('
            UPDATE {{%rent_agreement}}
            SET {{%rent_agreement}}.[[amount]] = {{%rent_agreement}}.[[amount]] / 100
        ');
    }
}
