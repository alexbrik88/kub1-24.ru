<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170713_125137_add_inteded_companies_count_to_company extends Migration
{
    public $tableName = 'company';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'invited_companies_count', $this->integer()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'invited_companies_count');
    }
}


