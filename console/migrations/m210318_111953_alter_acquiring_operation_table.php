<?php

use console\components\db\Migration;

class m210318_111953_alter_acquiring_operation_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%acquiring_operation}}';

    /**
     * @inheritDoc
     */
    public function up()
    {
        $this->alterColumn(self::TABLE_NAME, 'operation_id', $this->string()->notNull());
        $this->alterColumn(self::TABLE_NAME, 'contractor_id', $this->integer()->null());
        $this->addIndex();
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        $this->dropIndex('uk_acquiring_operation', self::TABLE_NAME);
        $this->alterColumn(self::TABLE_NAME, 'contractor_id', $this->integer()->notNull());
        $this->alterColumn(self::TABLE_NAME, 'operation_id', $this->integer()->unsigned()->null());
    }

    private function addIndex()
    {
        $sql = [
            'ALTER IGNORE TABLE `acquiring_operation` ADD UNIQUE INDEX `uk_acquiring_operation`',
            '(`company_id`, `operation_id`, `acquiring_type`, `acquiring_identifier`)',
        ];
        $this->execute(implode(' ', $sql));
    }
}
