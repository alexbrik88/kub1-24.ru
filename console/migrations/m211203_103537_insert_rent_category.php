<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211203_103537_insert_rent_category extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%rent_category}}', [
            'id' => 4,
            'title' => 'Вагон-дом',
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%rent_category}}', [
            'id' => 4,
        ]);
    }
}
