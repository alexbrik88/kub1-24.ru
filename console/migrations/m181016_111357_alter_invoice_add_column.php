<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181016_111357_alter_invoice_add_column extends Migration
{
    public $tableName = 'invoice';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'agreement_new_id', $this->integer());
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'agreement_new_id');
    }
}


