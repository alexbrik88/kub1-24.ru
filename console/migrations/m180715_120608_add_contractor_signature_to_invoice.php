<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180715_120608_add_contractor_signature_to_invoice extends Migration
{
    public $tInvoiceContractorSignature = 'invoice_contractor_signature';
    public $tInvoice = 'invoice';

    public function safeUp()
    {
        $this->createTable($this->tInvoiceContractorSignature, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'is_active' => $this->boolean()->defaultValue(false),
        ]);

        $this->addForeignKey($this->tInvoiceContractorSignature . '_company_id', $this->tInvoiceContractorSignature, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');
        }

    public function safeDown()
    {
        $this->dropForeignKey($this->tInvoiceContractorSignature . '_company_id', $this->tInvoiceContractorSignature);
        $this->dropTable($this->tInvoiceContractorSignature);
    }
}


