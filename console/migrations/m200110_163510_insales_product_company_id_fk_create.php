<?php

use console\components\db\Migration;

class m200110_163510_insales_product_company_id_fk_create extends Migration
{
    const TABLE = '{{%insales_product}}';

    public function up()
    {
        $this->addForeignKey(
            'insales_product_company_id', self::TABLE, 'company_id',
            '{{%company}}', 'id',
            'CASCADE', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey('insales_product_company_id', self::TABLE);
    }
}
