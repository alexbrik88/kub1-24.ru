<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180927_161211_add_to_packing_list_gevin_out_fields extends Migration
{
    public $tableName = 'packing_list';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'given_out_position', $this->string()->defaultValue(null));
        $this->addColumn($this->tableName, 'given_out_fio', $this->string()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'given_out_position');
        $this->dropColumn($this->tableName, 'given_out_fio');
    }
}


