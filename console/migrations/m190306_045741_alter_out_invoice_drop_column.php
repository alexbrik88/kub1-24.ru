<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190306_045741_alter_out_invoice_drop_column extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('out_invoice', 'is_demo');
    }

    public function safeDown()
    {
        $this->addColumn('out_invoice', 'is_demo', $this->boolean()->defaultValue(false)->after('status'));
    }
}
