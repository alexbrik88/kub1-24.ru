<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180521_134320_update_contractor_name_in_invoice_and_order_document extends Migration
{
    public $tInvoice = 'invoice';
    public $tOrderDocument = 'order_document';

    public function safeUp()
    {
        $this->alterColumn($this->tInvoice, 'contractor_name_full', $this->text()->notNull());
        $this->alterColumn($this->tInvoice, 'contractor_name_short', $this->text()->notNull());

        $this->alterColumn($this->tOrderDocument, 'contractor_name_full', $this->text()->notNull());
        $this->alterColumn($this->tOrderDocument, 'contractor_name_short', $this->text()->notNull());
    }
    
    public function safeDown()
    {
        $this->alterColumn($this->tInvoice, 'contractor_name_full', $this->string()->notNull());
        $this->alterColumn($this->tInvoice, 'contractor_name_short', $this->string()->notNull());

        $this->alterColumn($this->tOrderDocument, 'contractor_name_full', $this->string()->notNull());
        $this->alterColumn($this->tOrderDocument, 'contractor_name_short', $this->string()->notNull());
    }
}


