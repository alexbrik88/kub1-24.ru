<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190125_180738_alter_taxrobot_payment_order_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('taxrobot_payment_order', 'price', $this->integer()->notNull()->after('quarter'));
        $this->addColumn('taxrobot_payment_order', 'discount', $this->decimal(6, 4)->notNull()->defaultValue(0)->after('price'));
        $this->addColumn('taxrobot_payment_order', 'sum', $this->integer()->notNull()->after('discount'));
    }

    public function safeDown()
    {
        $this->dropColumn('taxrobot_payment_order', 'price');
        $this->dropColumn('taxrobot_payment_order', 'discount');
        $this->dropColumn('taxrobot_payment_order', 'sum');
    }
}
