<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180410_183258_alter_invoice_drop_column extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('invoice', 'has_nds');
    }

    public function safeDown()
    {
        $this->addColumn('invoice', 'has_nds', $this->boolean()->defaultValue(false)->after('company_checking_accountant_id'));

        $this->execute('
            UPDATE {{invoice}}
            SET {{invoice}}.[[has_nds]] = IF({{invoice}}.[[nds_view_type_id]] = 2, 0, 1)
        ');
    }
}
