<?php

use yii\db\Migration;

class m210323_190602_create_moneta_identity_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%moneta_identity}}';

    /**
     * @inheritDoc
     */
    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'account_id' => $this->bigInteger()->null(),
            'company_id' => $this->integer()->notNull(),
            'username' => $this->string()->notNull(),
            'password' => $this->string()->notNull(),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->addPrimaryKey('pk_account_id', self::TABLE_NAME, ['account_id']);
        $this->createIndex('ck_company_id', self::TABLE_NAME, ['company_id']);
        $this->addForeignKey(
            'fk_moneta_identity_company_id__company_company_id',
            self::TABLE_NAME,
            ['company_id'],
            '{{%company}}',
            ['id'],
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
