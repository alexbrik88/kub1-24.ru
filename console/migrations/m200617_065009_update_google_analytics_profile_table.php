<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200617_065009_update_google_analytics_profile_table extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%google_analytics_profile}}', 'access_token', $this->string()->notNull());
        $this->alterColumn('{{%google_analytics_profile}}', 'expires_at', $this->string()->notNull());
        $this->addColumn('{{%google_analytics_profile}}', 'refresh_token', $this->string()->notNull());
        $this->addColumn('{{%google_analytics_profile}}',  'user_name', $this->string()->notNull()->after('company_id'));
        $this->addColumn('{{%google_analytics_profile}}',  'avatar', $this->string()->defaultValue(null)->after('user_name'));
    }

    public function safeDown()
    {
        $this->alterColumn('{{%google_analytics_profile}}', 'access_token', $this->string()->defaultValue(null));
        $this->alterColumn('{{%google_analytics_profile}}', 'expires_at', $this->string()->defaultValue(null));
    }
}
