<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190629_080411_alter_tax_declaration_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('tax_declaration', 'tax_month', $this->integer(1)->after('tax_year'));

        $this->execute("
            UPDATE {{tax_declaration}}
            SET {{tax_declaration}}.[[tax_month]] = 1
            WHERE {{tax_declaration}}.[[szvm]] = 1
        ");

        $this->execute("
            DELETE FROM {{tax_declaration}}
            WHERE {{tax_declaration}}.[[szvm]] = 1 AND {{tax_declaration}}.[[balance]] = 1 
        ");
    }

    public function safeDown()
    {
        $this->dropColumn('tax_declaration', 'tax_month');
    }
}
