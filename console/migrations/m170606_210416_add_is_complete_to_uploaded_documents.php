<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170606_210416_add_is_complete_to_uploaded_documents extends Migration
{
    public $tableName = 'uploaded_documents';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'is_complete', $this->boolean()->defaultValue(false));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'is_complete');
    }
}


