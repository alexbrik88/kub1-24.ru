<?php

use console\components\db\Migration;
use yii\db\Schema;

class m161124_104940_alter_employeeCompany_createIndex extends Migration
{
    public function safeUp()
    {
        $this->execute('DELETE t1 FROM employee_company t1, employee_company t2 WHERE t1.id < t2.id AND t1.employee_id = t2.employee_id AND t1.company_id = t2.company_id');
        $this->createIndex('employee_company_uidx', 'employee_company', ['employee_id', 'company_id'], true);
    }
    
    public function safeDown()
    {
        $this->dropIndex('employee_company_uidx', 'employee_company');
    }
}
