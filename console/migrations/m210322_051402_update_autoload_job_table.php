<?php

use yii\db\Migration;

class m210322_051402_update_autoload_job_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%autoload_job}}';

    /**
     * @inheritDoc
     * @throws
     */
    public function safeUp()
    {
        $this->db
            ->createCommand()
            ->delete(self::TABLE_NAME, ['!=', 'command_type', 0])
            ->execute();
        $this->db
            ->createCommand()
            ->update(self::TABLE_NAME, ['command_type' => 0], ['command_type' => 2])
            ->execute();
    }

    /**
     * @inheritDoc
     * @throws
     */
    public function safeDown()
    {
        $this->db
            ->createCommand()
            ->update(self::TABLE_NAME, ['command_type' => 2], ['command_type' => 0])
            ->execute();
    }
}
