<?php

use common\models\cash\CashBankStatementUpload;
use common\models\Company;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\InvoiceFacture;
use common\models\document\PackingList;
use common\models\document\Upd;
use common\models\employee\Employee;
use common\models\product\Product;
use common\models\report\ReportState;
use common\models\service\Payment;
use console\components\db\Migration;
use frontend\models\Documents;
use frontend\modules\export\models\export\Export;
use yii\db\Schema;

class m191028_105453_update_report_state_3 extends Migration
{
    public $tableName = 'report_state';
    public $tariffsGroups = [
        [1,2,3], // Invoice
        [5,6,7], // Logistics
        [8,9,10], // B2B
        [11,12], // Declaration USN
        [13,14,15] // Analytics
    ];

    public function safeUp()
    {
        Yii::$app->db->createCommand('SET session wait_timeout=60000')->execute();
        Yii::$app->db->createCommand('SET session interactive_timeout=60000')->execute();

        $this->truncateTable($this->tableName);

        $firstPayments = Payment::find()->andWhere(['and',
            ['not', ['payment_date' => null]],
            ['is_confirmed' => 1],
            ['>', 'invoice_tariff_id', 0],
        ])
            ->orderBy(['payment_date' => SORT_ASC])
            ->groupBy('company_id')
            ->all();

        /* @var $firstPayment Payment */
        foreach ($firstPayments as $firstPayment) {
            $model = new ReportState();
            $model->company_id = $firstPayment->company_id;
            //
            $model->subscribe_id = null;
            $model->tariff_id = null;
            $model->invoice_tariff_id = $firstPayment->invoice_tariff_id;
            //
            $model->pay_date = $firstPayment->payment_date;
            $model->pay_sum = $firstPayment->sum;
            $model->invoice_count = Invoice::find()
                ->byIOType(Documents::IO_TYPE_OUT)
                ->byDeleted()
                ->andWhere(['<=', Invoice::tableName() . '.created_at', $model->pay_date])
                ->byCompany($model->company_id)
                ->count();
            $model->act_count = Act::find()
                ->byIOType(Documents::IO_TYPE_OUT)
                ->andWhere(['<=', Act::tableName() . '.created_at', $model->pay_date])
                ->byCompany($model->company_id)
                ->count();
            $model->packing_list_count = PackingList::find()
                ->byIOType(Documents::IO_TYPE_OUT)
                ->andWhere(['<=', PackingList::tableName() . '.created_at', $model->pay_date])
                ->byCompany($model->company_id)
                ->count();
            $model->invoice_facture_count = InvoiceFacture::find()
                ->byIOType(Documents::IO_TYPE_OUT)
                ->andWhere(['<=', InvoiceFacture::tableName() . '.created_at', $model->pay_date])
                ->byCompany($model->company_id)
                ->count();
            $model->upd_count = Upd::find()
                ->byIOType(Documents::IO_TYPE_OUT)
                ->andWhere(['<=', Upd::tableName() . '.created_at', $model->pay_date])
                ->byCompany($model->company_id)
                ->count();
            $model->has_logo = (bool)$firstPayment->company->logo_link;
            $model->has_print = (bool)$firstPayment->company->print_link;
            $model->has_signature = (bool)$firstPayment->company->chief_signature_link;
            $model->product_count = $firstPayment->company->getProducts()
                ->andWhere(['production_type' => Product::PRODUCTION_TYPE_GOODS])
                ->andWhere(['<=', Product::tableName() . '.created_at', $model->pay_date])
                ->count();
            $model->service_count = $firstPayment->company->getProducts()
                ->andWhere(['production_type' => Product::PRODUCTION_TYPE_SERVICE])
                ->andWhere(['<=', Product::tableName() . '.created_at', $model->pay_date])
                ->count();
            $model->download_statement_count = CashBankStatementUpload::find()
                ->joinWith('company')
                ->andWhere(['<=', CashBankStatementUpload::tableName() . '.created_at', $model->pay_date])
                ->andWhere([
                    Company::tableName() . '.id' => $model->company_id,
                ])->count();
            $model->download_1c_count = Export::find()
                ->joinWith('employee')
                ->leftJoin('company', 'company.id = ' . Employee::tableName() . '.company_id')
                ->andWhere(['<=', Export::tableName() . '.created_at', $model->pay_date])
                ->andWhere([Company::tableName() . '.id' => $model->company_id])
                ->count();

            $company = Company::findOne($firstPayment->company_id);

            $model->days_without_payment = intval(ceil(($model->pay_date - $company->created_at) / (60 * 60 * 24)));
            $model->sum_company_images = (int) ($model->has_logo + $model->has_print + $model->has_signature);
            $model->employees_count = Employee::find()
                ->byCompany($model->company_id)
                ->byIsActive(Employee::ACTIVE)
                ->byIsDeleted(Employee::NOT_DELETED)
                ->byIsWorking(Employee::STATUS_IS_WORKING)
                ->andWhere(['not', ['email' => 'support@kub-24.ru']])
                ->andWhere(['<=', Employee::tableName() . '.created_at', $model->pay_date])
                ->count();
            $model->company_type_id = $company->company_type_id;
            $model->company_taxation_type_name = $company->companyTaxationType->name;
            $model->validate();
            $model->save();
        }

        foreach ($this->tariffsGroups as $tariffs) {

            $firstPayments = Payment::find()->andWhere(['and',
                ['not', ['payment_date' => null]],
                ['is_confirmed' => 1],
                ['in', 'tariff_id', $tariffs],
            ])
                ->orderBy(['payment_date' => SORT_ASC])
                ->groupBy('company_id')
                ->all();

            /* @var $firstPayment Payment */
            foreach ($firstPayments as $firstPayment) {

                if (!$firstPayment->subscribe)
                    continue;

                $model = new ReportState();
                $model->company_id = $firstPayment->company_id;
                $model->subscribe_id = $firstPayment->subscribe->id;
                $model->tariff_id = $firstPayment->tariff_id;
                $model->pay_date = $firstPayment->payment_date;
                $model->pay_sum = $firstPayment->sum;
                $model->invoice_count = Invoice::find()
                    ->byIOType(Documents::IO_TYPE_OUT)
                    ->byDeleted()
                    ->andWhere(['<=', Invoice::tableName() . '.created_at', $model->pay_date])
                    ->byCompany($model->company_id)
                    ->count();
                $model->act_count = Act::find()
                    ->byIOType(Documents::IO_TYPE_OUT)
                    ->andWhere(['<=', Act::tableName() . '.created_at', $model->pay_date])
                    ->byCompany($model->company_id)
                    ->count();
                $model->packing_list_count = PackingList::find()
                    ->byIOType(Documents::IO_TYPE_OUT)
                    ->andWhere(['<=', PackingList::tableName() . '.created_at', $model->pay_date])
                    ->byCompany($model->company_id)
                    ->count();
                $model->invoice_facture_count = InvoiceFacture::find()
                    ->byIOType(Documents::IO_TYPE_OUT)
                    ->andWhere(['<=', InvoiceFacture::tableName() . '.created_at', $model->pay_date])
                    ->byCompany($model->company_id)
                    ->count();
                $model->upd_count = Upd::find()
                    ->byIOType(Documents::IO_TYPE_OUT)
                    ->andWhere(['<=', Upd::tableName() . '.created_at', $model->pay_date])
                    ->byCompany($model->company_id)
                    ->count();
                $model->has_logo = (bool)$firstPayment->company->logo_link;
                $model->has_print = (bool)$firstPayment->company->print_link;
                $model->has_signature = (bool)$firstPayment->company->chief_signature_link;
                $model->product_count = $firstPayment->company->getProducts()
                    ->andWhere(['production_type' => Product::PRODUCTION_TYPE_GOODS])
                    ->andWhere(['<=', Product::tableName() . '.created_at', $model->pay_date])
                    ->count();
                $model->service_count = $firstPayment->company->getProducts()
                    ->andWhere(['production_type' => Product::PRODUCTION_TYPE_SERVICE])
                    ->andWhere(['<=', Product::tableName() . '.created_at', $model->pay_date])
                    ->count();
                $model->download_statement_count = CashBankStatementUpload::find()
                    ->joinWith('company')
                    ->andWhere(['<=', CashBankStatementUpload::tableName() . '.created_at', $model->pay_date])
                    ->andWhere([
                        Company::tableName() . '.id' => $model->company_id,
                    ])->count();
                $model->download_1c_count = Export::find()
                    ->joinWith('employee')
                    ->leftJoin('company', 'company.id = ' . Employee::tableName() . '.company_id')
                    ->andWhere(['<=', Export::tableName() . '.created_at', $model->pay_date])
                    ->andWhere([Company::tableName() . '.id' => $model->company_id])
                    ->count();

                $company = Company::findOne($firstPayment->company_id);

                $model->days_without_payment = intval(ceil(($model->pay_date - $company->created_at) / (60 * 60 * 24)));
                $model->sum_company_images = (int) ($model->has_logo + $model->has_print + $model->has_signature);
                $model->employees_count = Employee::find()
                    ->byCompany($model->company_id)
                    ->byIsActive(Employee::ACTIVE)
                    ->byIsDeleted(Employee::NOT_DELETED)
                    ->byIsWorking(Employee::STATUS_IS_WORKING)
                    ->andWhere(['not', ['email' => 'support@kub-24.ru']])
                    ->andWhere(['<=', Employee::tableName() . '.created_at', $model->pay_date])
                    ->count();
                $model->company_type_id = $company->company_type_id;
                $model->company_taxation_type_name = $company->companyTaxationType->name;

                $model->save();
            }

        }
    }

    public function safeDown()
    {
        $this->execute("DELETE FROM `{$this->tableName}` WHERE invoice_tariff_id > 0");
    }
}
