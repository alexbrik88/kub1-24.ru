<?php

use yii\db\Schema;
use yii\db\Migration;

class m150413_111532_contractor extends Migration
{
    public function up()
    {
        $this->createTable('contractor', [
            'id' => Schema::TYPE_PK,
            'company_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'type' => Schema::TYPE_BOOLEAN . ' NOT NULL',
            'status' => Schema::TYPE_BOOLEAN . ' NOT NULL',
            'name' => Schema::TYPE_STRING . '(45) NOT NULL',
            'director_name' => Schema::TYPE_STRING . '(45) NOT NULL',
            'director_email' => Schema::TYPE_STRING . '(45)',
            'director_phone' => Schema::TYPE_STRING . '(45)',
            'chief_accounted_is_director' => Schema::TYPE_BOOLEAN . ' NOT NULL',
            'chief_accounted_email' => Schema::TYPE_STRING . '(45)',
            'chief_accounted_phone' => Schema::TYPE_STRING . '(45)',
            'legal_address' => Schema::TYPE_STRING . '(45)',
            'actual_address' => Schema::TYPE_STRING . '(45)',
            'BIN' => Schema::TYPE_INTEGER . ' NOT NULL',
            'INN' => Schema::TYPE_INTEGER . ' NOT NULL',
            'PPC' => Schema::TYPE_INTEGER . ' NOT NULL',
            'checking_account' => Schema::TYPE_INTEGER . ' NOT NULL',
            'bank_name' => Schema::TYPE_STRING . '(45)',
            'corresp_account' => Schema::TYPE_INTEGER . ' NOT NULL',
            'BIC' => Schema::TYPE_INTEGER . ' NOT NULL',
            'contact_is_director' => Schema::TYPE_BOOLEAN . ' NOT NULL',
            'contact_name' => Schema::TYPE_STRING . '(45)',
            'contact_phone' => Schema::TYPE_STRING . '(45)',
            'contact_email' => Schema::TYPE_STRING . '(45)',
            'date' => Schema::TYPE_DATE,
            'employee_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);
    }

    public function down()
    {
        $this->dropTable('contractor');
    }
}
