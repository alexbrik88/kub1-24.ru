<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%crm_client_type}}`.
 */
class m210621_101111_create_crm_client_type_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%crm_client_type}}', [
            'client_type_id' => $this->bigPrimaryKey(),
            'company_id' => $this->integer()->notNull(),
            'employee_id' => $this->integer()->null(),
            'name' => $this->string(64)->notNull(),
            'status' => $this->boolean()->notNull()->defaultValue(true),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'value' => $this->tinyInteger(1),
        ]);

        $this->createIndex('ck_company_id', '{{%crm_client_type}}', 'company_id');
        $this->addForeignKey(
            'fk_crm_client_type__company_company_id',
            '{{%crm_client_type}}',
            ['company_id'],
            '{{%company}}',
            ['id'],
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex('ck_client_type_id', '{{%crm_client_type}}', ['employee_id', 'company_id']);
        $this->addForeignKey(
            'fk_crm_client_type__employee_company_employee_id_company_id',
            '{{%crm_client_type}}',
            ['employee_id', 'company_id'],
            '{{%employee_company}}',
            ['employee_id', 'company_id'],
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex('uk_name', '{{%crm_client_type}}', ['company_id', 'name'], true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%crm_client_type}}');
    }
}
