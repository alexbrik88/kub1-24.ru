<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160215_203810_alter_status_in_contractor extends Migration
{
    public $tContractor = 'contractor';

    public function safeUp()
    {
        $this->alterColumn($this->tContractor, 'status', $this->boolean()->notNull()->defaultValue(\common\models\Contractor::ACTIVE));
    }
    
    public function safeDown()
    {
        $this->alterColumn($this->tContractor, 'status', $this->boolean()->notNull());
    }
}


