<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190919_053736_alter_invoice_facture_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%invoice_facture}}', 'print_id', $this->integer()->after('signature_id'));
        $this->addColumn('{{%invoice_facture}}', 'chief_signature_id', $this->integer()->after('print_id'));
        $this->addColumn('{{%invoice_facture}}', 'chief_accountant_signature_id', $this->integer()->after('chief_signature_id'));

        $this->addForeignKey(
            'FK_invoice_facture_company_details_print',
            '{{%invoice_facture}}',
            'print_id',
            '{{%company_details_file}}',
            'id'
        );

        $this->addForeignKey(
            'FK_invoice_facture_company_details_chief_signature',
            '{{%invoice_facture}}',
            'chief_signature_id',
            '{{%company_details_file}}',
            'id'
        );

        $this->addForeignKey(
            'FK_invoice_facture_company_details_chief_accountant_signature',
            '{{%invoice_facture}}',
            'chief_accountant_signature_id',
            '{{%company_details_file}}',
            'id'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_invoice_facture_company_details_print', '{{%invoice_facture}}');
        $this->dropForeignKey('FK_invoice_facture_company_details_chief_signature', '{{%invoice_facture}}');
        $this->dropForeignKey('FK_invoice_facture_company_details_chief_accountant_signature', '{{%invoice_facture}}');

        $this->dropColumn('{{%invoice_facture}}', 'print_id');
        $this->dropColumn('{{%invoice_facture}}', 'chief_signature_id');
        $this->dropColumn('{{%invoice_facture}}', 'chief_accountant_signature_id');
    }
}
