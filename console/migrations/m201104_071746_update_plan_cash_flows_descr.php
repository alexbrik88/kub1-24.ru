<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201104_071746_update_plan_cash_flows_descr extends Migration
{
    public function safeUp()
    {
        $this->execute("UPDATE `plan_cash_flows` SET description = REPLACE(description, 'Создано АвтоПланированием по', 'По') WHERE description like 'Создано АвтоПланированием по%'");
        $this->execute("UPDATE `plan_cash_flows` SET description = CONCAT(description, ' создано АвтоПланированием') WHERE description like 'По Счету%'");
    }

    public function safeDown()
    {
        // no down
    }
}
