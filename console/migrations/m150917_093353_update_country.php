<?php

use yii\db\Schema;
use yii\db\Migration;

class m150917_093353_update_country extends Migration
{
    public $tCountry = 'country';

    public function safeUp()
    {
        $this->update($this->tCountry, [
            'name_short' => new \yii\db\Expression('UPPER(name_short)'),
        ]);
    }
    
    public function safeDown()
    {
        echo 'No down migration needed.' . PHP_EOL;
    }
}
