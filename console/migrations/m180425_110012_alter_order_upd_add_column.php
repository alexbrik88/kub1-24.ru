<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180425_110012_alter_order_upd_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('order_upd', 'product_id', $this->integer()->notNull()->after('order_id'));

        $this->execute("
            UPDATE {{order_upd}}
            LEFT JOIN {{order}} ON {{order_upd}}.[[order_id]] = {{order}}.[[id]]
            SET {{order_upd}}.[[product_id]] = {{order}}.[[product_id]]
        ");

        $this->addForeignKey ('FK_orderUpd_product', 'order_upd', 'product_id', 'product', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey ('FK_orderUpd_product', 'order_upd');

        $this->dropColumn('order_upd', 'product_id');
    }
}
