<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180902_143807_create_user_email extends Migration
{
    public $tableName = 'user_email';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'fio' => $this->string()->notNull(),
            'email' => $this->string()->notNull(),
        ]);

        $this->addForeignKey($this->tableName . '_company_id', $this->tableName, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_company_id', $this->tableName);

        $this->dropTable($this->tableName);
    }
}


