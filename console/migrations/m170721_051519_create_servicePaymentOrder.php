<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170721_051519_create_servicePaymentOrder extends Migration
{
    public $tableName = '{{%service_payment_order}}';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'payment_id' => $this->integer()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'tariff_id' => $this->integer(),
            'price' => $this->integer()->notNull(),
            'discount' => $this->integer()->notNull()->defaultValue(0),
            'sum' => $this->integer()->notNull(),
        ]);

        $this->createIndex('paymentId_companyId', $this->tableName, ['payment_id', 'company_id'], true);
        $this->addForeignKey('FK_servicePaymentOrder_servicePayment', $this->tableName, 'payment_id', 'service_payment', 'id', 'CASCADE');
        $this->addForeignKey('FK_servicePaymentOrder_company', $this->tableName, 'company_id', 'company', 'id', 'CASCADE');
        $this->addForeignKey('FK_servicePaymentOrder_serviceSubscribeTariff', $this->tableName, 'tariff_id', 'service_subscribe_tariff', 'id');

        $this->execute("
            INSERT INTO
			    {$this->tableName} (payment_id, company_id, tariff_id, price, discount, sum)
			SELECT
			    {{%service_payment}}.[[id]],
			    {{%service_payment}}.[[company_id]],
			    {{%service_payment}}.[[tariff_id]],
			    {{%service_payment}}.[[price]],
			    {{%service_payment}}.[[discount]],
			    {{%service_payment}}.[[sum]]
			FROM
			    {{%service_payment}}
			WHERE
				{{%service_payment}}.[[type_id]] <> 4
        ");
    }
    
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
