<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190702_084356_add_table_agent_report_order extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('agent_report_order', [
            'id' => Schema::TYPE_PK,
            'agent_report_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'contractor_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'payments_sum' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
            'agent_percent' => Schema::TYPE_DECIMAL . '(20,4) NOT NULL DEFAULT 0',
            'agent_sum' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 0',
            'is_exclude' => Schema::TYPE_INTEGER . '(1) NOT NULL DEFAULT 0'
        ], $tableOptions);

        $this->addForeignKey('FK_agent_report_order_to_agent_report', 'agent_report_order', 'agent_report_id', 'agent_report', 'id', 'CASCADE');
        $this->addForeignKey('FK_agent_report_order_to_contractor', 'agent_report_order', 'contractor_id', 'contractor', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable('agent_report_order');
    }
}
