<?php

use yii\db\Migration;

/**
 * Handles the creation of table `out_invoice`.
 */
class m180117_083702_create_out_invoice_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('out_invoice', [
            'id' => $this->string(50)->notNull(),
            'company_id' => $this->integer()->notNull(),
            'status' => $this->smallInteger(2)->notNull(),
            'return_url' => $this->text()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', 'out_invoice', 'id');
        $this->addForeignKey('FK_outInvoice_company', 'out_invoice', 'company_id', 'company', 'id', 'CASCADE');
        $this->addForeignKey('FK_outInvoice_createdBy', 'out_invoice', 'created_by', 'employee', 'id');
        $this->addForeignKey('FK_outInvoice_updatedBy', 'out_invoice', 'updated_by', 'employee', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('out_invoice');
    }
}
