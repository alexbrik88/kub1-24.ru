<?php

use console\components\db\Migration;
use yii\db\Schema;

class m171030_113058_update_invoice_expenditure_item extends Migration
{
    public $tableName = 'invoice_expenditure_item';

    public function safeUp()
    {
        $this->update($this->tableName, ['id' => 33], ['id' => 112]);
        $this->update($this->tableName, ['id' => 34], ['id' => 113]);
        $this->update($this->tableName, ['id' => 35], ['id' => 114]);
        $this->update($this->tableName, ['id' => 36], ['id' => 115]);
        $this->update($this->tableName, ['id' => 37], ['id' => 116]);
        $this->update($this->tableName, ['id' => 38], ['id' => 117]);
        $this->update($this->tableName, ['id' => 39], ['id' => 118]);
        $this->update($this->tableName, ['id' => 40], ['id' => 119]);
        $this->update($this->tableName, ['id' => 41], ['id' => 120]);
        $this->update($this->tableName, ['id' => 42], ['id' => 121]);
        $this->update($this->tableName, ['id' => 43], ['id' => 122]);
    }

    public function safeDown()
    {
        echo "This migration not rolled back";
    }
}


