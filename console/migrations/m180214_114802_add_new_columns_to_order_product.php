<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180214_114802_add_new_columns_to_order_product extends Migration
{
    public $tableName = 'order_document';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'total_mass_gross', $this->string()->defaultValue(null));
        $this->addColumn($this->tableName, 'total_place_count', $this->string()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'total_mass_gross');
        $this->dropColumn($this->tableName, 'total_place_count');
    }
}


