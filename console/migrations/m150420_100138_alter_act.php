<?php

use yii\db\Schema;
use yii\db\Migration;

class m150420_100138_alter_act extends Migration
{
    public function up()
    {
        $this->addColumn('act', 'type', Schema::TYPE_INTEGER . '(1) NOT NULL COMMENT "Тип акта - 2 (исходящий), 1 (входящий)"');
    }

    public function down()
    {
        $this->dropColumn('act', 'type');
    }
}
