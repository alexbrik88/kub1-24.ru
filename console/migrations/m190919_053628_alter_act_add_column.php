<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190919_053628_alter_act_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%act}}', 'print_id', $this->integer()->after('signature_id'));
        $this->addColumn('{{%act}}', 'chief_signature_id', $this->integer()->after('print_id'));
        $this->addColumn('{{%act}}', 'chief_accountant_signature_id', $this->integer()->after('chief_signature_id'));

        $this->addForeignKey(
            'FK_act_company_details_print',
            '{{%act}}',
            'print_id',
            '{{%company_details_file}}',
            'id'
        );

        $this->addForeignKey(
            'FK_act_company_details_chief_signature',
            '{{%act}}',
            'chief_signature_id',
            '{{%company_details_file}}',
            'id'
        );

        $this->addForeignKey(
            'FK_act_company_details_chief_accountant_signature',
            '{{%act}}',
            'chief_accountant_signature_id',
            '{{%company_details_file}}',
            'id'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_act_company_details_print', '{{%act}}');
        $this->dropForeignKey('FK_act_company_details_chief_signature', '{{%act}}');
        $this->dropForeignKey('FK_act_company_details_chief_accountant_signature', '{{%act}}' );

        $this->dropColumn('{{%act}}', 'print_id');
        $this->dropColumn('{{%act}}', 'chief_signature_id');
        $this->dropColumn('{{%act}}', 'chief_accountant_signature_id');
    }
}
