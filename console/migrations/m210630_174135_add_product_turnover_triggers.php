<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210630_174135_add_product_turnover_triggers extends Migration
{
    public $dropSql = [
        'DROP TRIGGER IF EXISTS `product_turnover__order_goods_cancellation__after_insert`',
        'DROP TRIGGER IF EXISTS `product_turnover__order_goods_cancellation__after_update`',
        'DROP TRIGGER IF EXISTS `product_turnover__order_goods_cancellation__after_delete`',
        'DROP TRIGGER IF EXISTS `product_turnover__goods_cancellation__after_update`'
    ];

    public $afterInsertOrderSql = <<<SQL

CREATE TRIGGER `product_turnover__order_goods_cancellation__after_insert` AFTER INSERT ON `order_goods_cancellation`
FOR EACH ROW
BEGIN
    INSERT INTO `product_turnover` (
        `order_id`,
        `document_id`,
        `document_table`,
        `invoice_id`,
        `contractor_id`,
        `company_id`,
        `date`,
        `type`,
        `production_type`,
        `is_invoice_actual`,
        `is_document_actual`,
        `purchase_price`,
        `price_one`,
        `quantity`,
        `total_amount`,
        `purchase_amount`,
        `margin`,
        `year`,
        `month`,
        `product_group_id`,
        `product_id`,
        `not_for_sale`
    ) SELECT
        NEW.`id`,
        NEW.`goods_cancellation_id`,
        "goods_cancellation",
        NULL, /* invoice_id */
        `doc`.`contractor_id`,
        `doc`.`company_id`,
        `doc`.`document_date`,
        2, /* io_type = OUT */
        1, /* production_type = GOODS */
        1, /* is_invoice_actual */
        1, /* is_document_actual */
        0, /* purchase_price */
        @price:=IFNULL(`order`.`purchase_price_with_vat`, 0),
        @quantity:=IFNULL(NEW.`quantity`, 0),
        @total:=ROUND(@price * @quantity, IF(`doc`.`price_precision` = 4, 2, 0)),
        0 /* purchase_amount */,
        0 /* margin */,
        YEAR(`doc`.`document_date`),
        MONTH(`doc`.`document_date`),
        `product`.`group_id`,
        NEW.`product_id`,
        `product`.`not_for_sale`
    FROM `order_goods_cancellation` `order`
    LEFT JOIN `goods_cancellation` `doc` ON `doc`.`id` = NEW.`goods_cancellation_id`
    LEFT JOIN `product` ON `product`.`id` = `order`.`product_id`
    WHERE `order`.`id` = NEW.`id`
    ON DUPLICATE KEY UPDATE
        `price_one` = VALUES(`price_one`),
        `quantity` = VALUES(`quantity`),
        `total_amount` = VALUES(`total_amount`),
        `product_group_id` = VALUES(`product_group_id`),
        `product_id` = VALUES(`product_id`);
END;

SQL;

    public $afterUpdateOrderSql = <<<SQL
CREATE TRIGGER `product_turnover__order_goods_cancellation__after_update` AFTER UPDATE ON `order_goods_cancellation`
FOR EACH ROW
BEGIN
    INSERT INTO `product_turnover` (
        `order_id`,
        `document_id`,
        `document_table`,
        `invoice_id`,
        `contractor_id`,
        `company_id`,
        `date`,
        `type`,
        `production_type`,
        `is_invoice_actual`,
        `is_document_actual`,
        `purchase_price`,
        `price_one`,
        `quantity`,
        `total_amount`,
        `purchase_amount`,
        `margin`,
        `year`,
        `month`,
        `product_group_id`,
        `product_id`,
        `not_for_sale`
    ) SELECT
        NEW.`id`,
        NEW.`goods_cancellation_id`,
        "goods_cancellation",
        NULL, /* invoice_id */
        `doc`.`contractor_id`,
        `doc`.`company_id`,
        `doc`.`document_date`,
        2, /* io_type = OUT */
        1, /* production_type = GOODS */
        1, /* is_invoice_actual */
        1, /* is_document_actual */
        0, /* purchase_price */
        @price:=IFNULL(`order`.`purchase_price_with_vat`, 0),
        @quantity:=IFNULL(NEW.`quantity`, 0),
        @total:=ROUND(@price * @quantity, IF(`doc`.`price_precision` = 4, 2, 0)),
        0 /* purchase_amount */,
        0 /* margin */,
        YEAR(`doc`.`document_date`),
        MONTH(`doc`.`document_date`),
        `product`.`group_id`,
        NEW.`product_id`,
        `product`.`not_for_sale`
    FROM `order_goods_cancellation` `order`
    LEFT JOIN `goods_cancellation` `doc` ON `doc`.`id` = NEW.`goods_cancellation_id`
    LEFT JOIN `product` ON `product`.`id` = `order`.`product_id`
    WHERE `order`.`id` = NEW.`id`
    ON DUPLICATE KEY UPDATE
        `price_one` = VALUES(`price_one`),
        `quantity` = VALUES(`quantity`),
        `total_amount` = VALUES(`total_amount`),
        `product_group_id` = VALUES(`product_group_id`),
        `product_id` = VALUES(`product_id`);
END;
END;
    
SQL;

    public $afterDeleteOrderSql = <<<SQL
CREATE TRIGGER `product_turnover__order_goods_cancellation__after_delete` AFTER DELETE ON `order_goods_cancellation`
FOR EACH ROW
BEGIN
    DELETE FROM `product_turnover`
    WHERE `order_id` = OLD.`id`
    AND `document_id` = OLD.`goods_cancellation_id`
    AND `document_table` = "goods_cancellation";
END;
SQL;

    public $afterUpdateSql = <<<SQL
CREATE TRIGGER `product_turnover__goods_cancellation__after_update` AFTER UPDATE ON `goods_cancellation`
FOR EACH ROW
BEGIN
    UPDATE `product_turnover`
    SET 
        `date` = NEW.`document_date`,
        `year` = YEAR(NEW.`document_date`),
        `month` = MONTH(NEW.`document_date`)
    WHERE `document_id` = NEW.`id`
    AND `document_table` = "goods_cancellation";
END;
SQL;

    public function safeUp()
    {
        foreach ($this->dropSql as $sql) {
            $this->execute($sql);
        }

        $this->execute($this->afterInsertOrderSql);
        $this->execute($this->afterUpdateOrderSql);
        $this->execute($this->afterDeleteOrderSql);
        $this->execute($this->afterUpdateSql);
    }

    public function safeDown()
    {
        foreach ($this->dropSql as $sql) {
            $this->execute($sql);
        }
    }
}
