<?php

use common\models\amocrm\Lead;
use common\models\employee\Employee;
use console\components\db\Migration;

class m200110_110847_amocrm_leads_contact_id_insert_from_employee_id extends Migration
{
    const TABLE = '{{%amocrm_leads}}';

    public function safeUp()
    {
        foreach (Lead::find()->all() as $lead) {
            /** @var Lead $leads */
            /** @noinspection PhpUndefinedFieldInspection */
            $lead->company_id = Employee::findOne(['id' => $lead->employee_id])->company_id;
            $lead->save();
        }
    }

    public function down()
    {
        $this->update(self::TABLE, ['company_id' => 0]);
    }
}
