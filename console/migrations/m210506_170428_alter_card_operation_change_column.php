<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210506_170428_alter_card_operation_change_column extends Migration
{
    public function safeUp()
    {
        $this->dropForeignKey('fk_card_operation_contractor_id__contractor', 'card_operation');
        $this->alterColumn('card_operation', 'contractor_id', $this->string()->null());
    }

    public function safeDown()
    {
        $this->alterColumn('card_operation', 'contractor_id', $this->integer()->null());
        $this->addForeignKey('fk_card_operation_contractor_id__contractor', 'card_operation', 'contractor_id', 'contractor', 'id', 'SET NULL');
    }
}
