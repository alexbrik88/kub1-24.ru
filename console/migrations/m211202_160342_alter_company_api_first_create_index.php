<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211202_160342_alter_company_api_first_create_index extends Migration
{
    public function safeUp()
    {
        $this->createIndex('api_partners_id', '{{%company_api_first_visit}}', 'api_partners_id');
    }

    public function safeDown()
    {
        $this->dropIndex('api_partners_id', '{{%company_api_first_visit}}');
    }
}
