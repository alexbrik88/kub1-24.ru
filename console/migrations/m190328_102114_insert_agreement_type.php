<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190328_102114_insert_agreement_type extends Migration
{
    public function safeUp()
    {
        $this->insert('agreement_type', [
            'id' => 11,
            'name' => 'Муниципальный контракт',
            'name_dative' => 'Муниципальному контракту',
            'sort' => 53,
        ]);
    }

    public function safeDown()
    {
        $this->delete('agreement_type', ['id' => 11]);
    }
}
