<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170303_144615_alter_employee_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%employee}}', 'knows_about_sending_invoice', $this->boolean()->notNull()->defaultValue(false));
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%employee}}', 'knows_about_sending_invoice');
    }
}
