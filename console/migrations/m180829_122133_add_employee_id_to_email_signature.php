<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180829_122133_add_employee_id_to_email_signature extends Migration
{
    public $tableName = 'email_signature';

    public function safeUp()
    {
        $this->truncateTable($this->tableName);
        $this->addColumn($this->tableName, 'employee_id', $this->integer()->notNull());
        $this->addForeignKey($this->tableName . '_employee_id', $this->tableName, 'employee_id', 'employee', 'id', 'RESTRICT', 'CASCADE');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_employee_id', $this->tableName);
        $this->dropColumn($this->tableName, 'employee_id');
    }
}


