<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210412_090851_alter_price_list_order_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('price_list_order', 'sort', $this->integer()->notNull()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('price_list_order', 'sort');
    }
}