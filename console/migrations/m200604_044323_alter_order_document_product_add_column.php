<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200604_044323_alter_order_document_product_add_column extends Migration
{
    public $tableName = 'order_document_product';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'markup', $this->decimal(6, 4)->notNull()->defaultValue(0)->after('discount'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'markup');
    }
}
