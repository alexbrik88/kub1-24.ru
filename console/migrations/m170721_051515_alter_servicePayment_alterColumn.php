<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170721_051515_alter_servicePayment_alterColumn extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%service_payment}}', 'payment_date', $this->integer()->defaultValue(null) . ' AFTER [[is_confirmed]]');
    }
    
    public function safeDown()
    {
        $this->alterColumn('{{%service_payment}}', 'payment_date', $this->integer()->notNull() . ' AFTER [[is_confirmed]]');
    }
}
