<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170512_075022_add_activities_id_to_company extends Migration
{
    public $tableName = 'company';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'activities_id', $this->integer()->defaultValue(null));

        $this->addForeignKey($this->tableName . '_activities_id', $this->tableName, 'activities_id', 'activities', 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_activities_id', $this->tableName);

        $this->dropColumn($this->tableName, 'activities_id');
    }
}


