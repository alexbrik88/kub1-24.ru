<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180328_081239_update_product_store extends Migration
{
    public function safeUp()
    {
        $this->execute("
            UPDATE {{product_store}} {{ps}}
            INNER JOIN {{store}} {{s}} ON {{s}}.[[id]] = {{ps}}.[[store_id]] and {{s}}.[[is_main]] = true
            LEFT JOIN (
                SELECT [[product_id]], SUM([[initial_quantity]]) AS [[total]]
                FROM {{product_store}}
                GROUP BY [[product_id]]
            ) {{init}} ON {{init}}.[[product_id]] = {{ps}}.[[product_id]]
            LEFT JOIN (
                SELECT {{ps1}}.[[product_id]], SUM({{ps1}}.[[quantity]]) AS [[total]]
                FROM {{product_store}} {{ps1}}
                LEFT JOIN {{store}} {{s1}} ON {{ps1}}.[[store_id]] = {{s1}}.[[id]]
                WHERE {{s1}}.[[is_main]] = false
                GROUP BY {{ps1}}.[[product_id]]
            ) {{other}} ON {{other}}.[[product_id]] = {{ps}}.[[product_id]]
            LEFT JOIN (
                SELECT {{opl1}}.[[product_id]], SUM({{opl1}}.[[quantity]]) AS [[total]]
                FROM {{order_packing_list}} {{opl1}}
                LEFT JOIN {{packing_list}} {{pl1}} ON {{opl1}}.packing_list_id = {{pl1}}.[[id]]
                LEFT JOIN {{invoice}} {{i1}} ON {{i1}}.[[id]] = {{pl1}}.[[invoice_id]]
                WHERE {{i1}}.[[type]] = 1
                AND {{i1}}.[[is_deleted]] = false
                AND {{i1}}.[[invoice_status_id]] NOT IN (5, 9)
                AND {{pl1}}.[[status_out_id]] <> 5
                GROUP BY {{opl1}}.[[product_id]]
            ) {{buy}} ON {{buy}}.[[product_id]] = {{ps}}.[[product_id]]
            LEFT JOIN (
                SELECT {{opl2}}.[[product_id]], SUM({{opl2}}.[[quantity]]) AS [[total]]
                FROM {{order_packing_list}} {{opl2}}
                LEFT JOIN {{packing_list}} {{pl2}} ON {{opl2}}.packing_list_id = {{pl2}}.[[id]]
                LEFT JOIN {{invoice}} {{i2}} ON {{i2}}.[[id]] = {{pl2}}.[[invoice_id]]
                WHERE {{i2}}.[[type]] = 2
                AND {{i2}}.[[is_deleted]] = false
                AND {{i2}}.[[invoice_status_id]] NOT IN (5, 9)
                AND {{pl2}}.[[status_out_id]] <> 5
                GROUP BY {{opl2}}.[[product_id]]
            ) {{sell}} ON {{sell}}.[[product_id]] = {{ps}}.[[product_id]]
            SET {{ps}}.[[quantity]] = {{init}}.[[total]]
                                    - IFNULL({{other}}.[[total]], 0)
                                    + IFNULL({{buy}}.[[total]], 0)
                                    - IFNULL({{sell}}.[[total]], 0)
            WHERE {{s}}.company_id = 6421
        ");
    }

    public function safeDown()
    {
        //
    }
}
