<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210831_082044_create_payments_request_order_approved extends Migration
{
    public $table = 'payments_request_order_approved';

    public function safeUp()
    {
        $table = $this->table;

        $this->createTable($table, [
            'order_id' => $this->integer(),
            'employee_id' => $this->integer(),
            'approved' => $this->tinyInteger()->defaultValue(0),
            'approved_at' => $this->integer()
        ]);

        $this->addPrimaryKey("PK_{$table}", $table, ['order_id', 'employee_id']);
        $this->addForeignKey("FK_{$table}_to_order", $table, 'order_id', 'payments_request_order', 'id');
        $this->addForeignKey("FK_{$table}_to_employee", $table, 'employee_id', 'employee', 'id');
    }

    public function safeDown()
    {
        $table = $this->table;

        $this->dropForeignKey("FK_{$table}_to_order", $table);
        $this->dropForeignKey("FK_{$table}_to_employee", $table);
        $this->dropTable($table);
    }
}
