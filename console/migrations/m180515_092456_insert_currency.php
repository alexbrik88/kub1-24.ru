<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180515_092456_insert_currency extends Migration
{
    public function safeUp()
    {
        $this->insert('currency', [
            'id' => 643,
            'name' => 'RUB',
            'label' => 'Российский рубль',
            'is_default' => true,
            'amount' => 1,
            'old_value' => 1,
            'current_value' => 1,
            'sort' => 0,
        ]);
    }

    public function safeDown()
    {
        $this->delete('currency', ['id' => 643]);
    }
}
