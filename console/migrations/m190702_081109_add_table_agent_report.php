<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190702_081109_add_table_agent_report extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('agent_report_status', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ], $tableOptions);

        $this->batchInsert('agent_report_status', ['id', 'name'], [
            [1, 'Создан'],
            [2, 'Отправлен'],
            [3, 'Распечатан'],
            [4, 'Оплачен частично'],
            [5, 'Оплачен полностью'],
            [6, 'Просрочен'],
        ]);

        $this->createTable('{{%agent_report}}', [
            'id' => Schema::TYPE_PK,

            'uid' => Schema::TYPE_STRING . '(5) DEFAULT NULL',

            'type' => Schema::TYPE_INTEGER . '(1) NOT NULL DEFAULT 2',
            'invoice_id' => Schema::TYPE_INTEGER . '(11)',
            'created_at' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'document_author_id' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'status_out_id' => Schema::TYPE_INTEGER . '(11) DEFAULT NULL',
            'status_out_updated_at' => Schema::TYPE_INTEGER . '(11) DEFAULT NULL',
            'status_out_author_id' => Schema::TYPE_INTEGER . '(11) DEFAULT NULL',

            'document_date' => Schema::TYPE_DATE . ' NOT NULL',
            'document_number' => Schema::TYPE_INTEGER . '(11) DEFAULT NULL',

            'company_id' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'agent_id' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'total_sum' => Schema::TYPE_INTEGER . '(11) DEFAULT 0',
            'total_sum_without_nds' => Schema::TYPE_INTEGER . '(11) DEFAULT 0',
            'total_nds' => Schema::TYPE_INTEGER . '(11) DEFAULT 0',
            'comment_internal' => Schema::TYPE_TEXT . ' DEFAULT NULL',
            'has_invoice' => Schema::TYPE_INTEGER . '(1) NOT NULL DEFAULT 0',

            'basis_name' => Schema::TYPE_STRING . '(255) DEFAULT NULL',
            'basis_document_number' => Schema::TYPE_STRING . '(50) DEFAULT NULL',
            'basis_document_date' => Schema::TYPE_DATE . ' DEFAULT NULL',
            'basis_document_type_id' => Schema::TYPE_INTEGER . '(11) DEFAULT NULL',

            'signed_by_employee_id' => Schema::TYPE_INTEGER . '(11) DEFAULT NULL',
            'signed_by_name' => Schema::TYPE_STRING . '(50) DEFAULT NULL',
            'sign_document_type_id' => Schema::TYPE_INTEGER . '(11) DEFAULT NULL',
            'sign_document_number' => Schema::TYPE_STRING . '(50) DEFAULT NULL',
            'sign_document_date' => Schema::TYPE_DATE . ' DEFAULT NULL',
            'signature_id' => Schema::TYPE_INTEGER . '(11) DEFAULT NULL',

            'add_stamp' => Schema::TYPE_SMALLINT . '(1) DEFAULT 0',
            'has_file' => Schema::TYPE_SMALLINT . '(1) DEFAULT 0',
        ]);

        $this->createIndex('uid_idx', '{{%agent_report}}', 'uid');
        $this->createIndex('has_file', '{{%agent_report}}', 'has_file');

        $this->addForeignKey('FK_agent_report_to_company', '{{%agent_report}}', 'company_id', 'company', 'id', 'CASCADE');
        $this->addForeignKey('FK_agent_report_to_agent', '{{%agent_report}}', 'agent_id', 'contractor', 'id', 'CASCADE');

        $this->addForeignKey('FK_agent_report_to_invoice', '{{%agent_report}}', 'invoice_id', 'invoice', 'id');
        $this->addForeignKey('FK_agent_report_to_agent_report_status', '{{%agent_report}}', 'status_out_id', 'agent_report_status', 'id');
        $this->addForeignKey('FK_agent_report_to_document_author', '{{%agent_report}}', 'document_author_id', 'employee', 'id');
        $this->addForeignKey('FK_agent_report_to_status_out_author', '{{%agent_report}}', 'status_out_author_id', 'employee', 'id');

        $this->addForeignKey('FK_agent_report_sign_employee', '{{%agent_report}}', 'signed_by_employee_id', '{{%employee}}', 'id');
        $this->addForeignKey('FK_agent_report_sign_documentType', '{{%agent_report}}', 'sign_document_type_id', '{{%document_type}}', 'id');
        $this->addForeignKey('FK_agent_report_employeeSignature', '{{%agent_report}}', 'signature_id', '{{%employee_signature}}', 'id');
        $this->addForeignKey('FK_agent_report_agreementType', '{{%agent_report}}', 'basis_document_type_id', '{{%agreement_type}}', 'id');
    }

    public function safeDown()
    {
        $this->dropTable('{{%agent_report}}');
        $this->dropTable('{{%agent_report_status}}');
    }
}
