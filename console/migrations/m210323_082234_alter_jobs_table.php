<?php

use yii\db\Migration;

class m210323_082234_alter_jobs_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%jobs}}';

    /**
     * @inheritDoc
     */
    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'date_from', $this->date()->null()->after('employee_id'));
        $this->addColumn(self::TABLE_NAME, 'date_to', $this->date()->null()->after('date_from'));
        $this->addColumn(
            self::TABLE_NAME,
            'identifier',
            $this->string()->notNull()->defaultValue('')->after('date_to'),
        );
        $this->addColumn(self::TABLE_NAME, 'count', $this->bigInteger()->null()->after('identifier'));
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'date_from');
        $this->dropColumn(self::TABLE_NAME, 'date_to');
        $this->dropColumn(self::TABLE_NAME, 'identifier');
        $this->dropColumn(self::TABLE_NAME, 'count');
    }
}
