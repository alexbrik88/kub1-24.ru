<?php

use console\components\db\Migration;
use yii\db\Schema;
use common\models\service\Subscribe;
use common\models\service\SubscribeTariff;
use common\models\Company;

class m170824_123258_add_subscribe_id_to_selling_subscribe extends Migration
{
    public $tableName = 'selling_subscribe';

    public function safeUp()
    {
        $this->truncateTable($this->tableName);
        $this->addColumn($this->tableName, 'subscribe_id', $this->integer()->defaultValue(null));
        $this->addForeignKey($this->tableName . '_subscribe_id', $this->tableName, 'subscribe_id', 'service_subscribe', 'id', 'RESTRICT', 'CASCADE');
        $key = 0;
        /* @var $subscribe Subscribe */
        foreach (Subscribe::find()->joinWith('payment')->andWhere(['is_confirmed' => 1])->all() as $key => $subscribe) {
            $key++;
            $type = null;
            $mainType = null;
            $invoiceNumber = null;
            if ($subscribe->tariff_id == SubscribeTariff::TARIFF_TRIAL) {
                $type = 1;
            } elseif ($subscribe->payment && $subscribe->payment->type->id == 4) {
                if ($subscribe->payment->promoCode) {
                    if ($subscribe->payment->promoCode->group_id == \common\models\service\PromoCodeGroup::GROUP_INDIVIDUAL) {
                        $type = 2;
                    } else {
                        $type = 3;
                    }
                } else {
                    continue;
                }
            } elseif ($subscribe->tariff_id == SubscribeTariff::TARIFF_1) {
                $type = 4;
            } elseif ($subscribe->tariff_id == SubscribeTariff::TARIFF_2) {
                $type = 5;
            } elseif ($subscribe->tariff_id == SubscribeTariff::TARIFF_3) {
                $type = 6;
            }
            if ($type !== null) {
                if (in_array($type, [1, 2, 3])) {
                    $mainType = 1;
                } else {
                    $mainType = 2;
                }
            } else {
                continue;
            }
            if (!in_array($type, [1, 2, 3])) {
                if ($subscribe->payment->outInvoice) {
                    $invoiceNumber = $subscribe->payment->outInvoice->getFullNumber();
                }
            }
            $this->insert($this->tableName, [
                'id' => $key,
                'company_id' => $subscribe->company_id,
                'type' => $type,
                'main_type' => $mainType,
                'invoice_number' => $invoiceNumber,
                'creation_date' => $subscribe->created_at,
                'activation_date' => $subscribe->activated_at,
                'end_date' => $subscribe->expired_at,
                'subscribe_id' => $subscribe->id,
            ]);
        }

        /* @var $company Company */
        foreach (Company::find()
                     ->isTest(!Company::TEST_COMPANY)
                     ->isBlocked(Company::UNBLOCKED)
                     ->andWhere(['is_free_tariff_notified' => 1])->all() as $companyKey => $company) {
            $companyKey++;
            $key += $companyKey;

            $this->insert($this->tableName, [
                'id' => $key,
                'company_id' => $company->id,
                'type' => 7,
                'main_type' => 1,
                'invoice_number' => null,
                'creation_date' => $company->free_tariff_start_at,
                'activation_date' => $company->free_tariff_start_at,
                'end_date' => null,
                'subscribe_id' => null,
            ]);
        }
    }

    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_subscribe_id', $this->tableName);
        $this->dropColumn($this->tableName, 'subscribe_id');
    }
}


