<?php

use yii\db\Migration;

class m210621_103321_rename_crm_client_deal_stage_table extends Migration
{
    /**
     * @inheritDoc
     */
    public function up(): void
    {
        $this->renameTable('{{%crm_client_deal_stage}}', '{{%crm_deal_stage}}');
    }

    /**
     * @inheritDoc
     */
    public function down(): bool
    {
        return false;
    }
}
