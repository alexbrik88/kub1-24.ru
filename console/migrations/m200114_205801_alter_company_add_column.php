<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200114_205801_alter_company_add_column extends Migration
{
    public $tableName = 'company';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'show_help_taxrobot_banking', $this->boolean()->defaultValue(true));

        $this->update($this->tableName, ['show_help_taxrobot_banking' => false], ['not', ['id' => null]]);
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'show_help_taxrobot_banking');
    }
}
