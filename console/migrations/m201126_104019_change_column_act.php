<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201126_104019_change_column_act extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('act', 'order_sum_without_nds', $this->bigInteger()->defaultValue(0));
        $this->alterColumn('act', 'order_nds', $this->bigInteger()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->alterColumn('act', 'order_sum_without_nds', $this->integer()->defaultValue(0));
        $this->alterColumn('act', 'order_nds', $this->integer()->defaultValue(0));
    }
}
