<?php

use console\components\db\Migration;

class m200201_141028_address_classifier_create extends Migration
{

    const TABLE = '{{%address_classifier}}';

    public function up()
    {
        $this->createTable(self::TABLE, [
            'postalcode' => $this->primaryKey(6)->unsigned()->notNull()->comment('Почтовый индекс'),
            'regioncode' => $this->tinyInteger()->unsigned()->notNull()->comment('Почтовый индекс')
        ], "COMMENT 'Классификатор адресов: почтовые индексы'");
    }

    public function down()
    {
        $this->dropTable(self::TABLE);
    }
}
