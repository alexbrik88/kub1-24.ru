<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170301_003844_alter_invoiceFacture_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%invoice_facture}}', 'signed_by_employee_id', $this->integer());
        $this->addColumn('{{%invoice_facture}}', 'signed_by_name', $this->string(50));
        $this->addColumn('{{%invoice_facture}}', 'sign_document_type_id', $this->integer());
        $this->addColumn('{{%invoice_facture}}', 'sign_document_number', $this->string(50));
        $this->addColumn('{{%invoice_facture}}', 'sign_document_date', $this->date());
        $this->addColumn('{{%invoice_facture}}', 'signature_id', $this->integer());

        $this->addForeignKey('FK_invoiceFacture_sign_employee', '{{%invoice_facture}}', 'signed_by_employee_id', '{{%employee}}', 'id');
        $this->addForeignKey('FK_invoiceFacture_sign_documentType', '{{%invoice_facture}}', 'sign_document_type_id', '{{%document_type}}', 'id');
        $this->addForeignKey('FK_invoiceFacture_employeeSignature', '{{%invoice_facture}}', 'signature_id', '{{%employee_signature}}', 'id');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('FK_invoiceFacture_sign_employee', '{{%invoice_facture}}');
        $this->dropForeignKey('FK_invoiceFacture_sign_documentType', '{{%invoice_facture}}');
        $this->dropForeignKey('FK_invoiceFacture_employeeSignature', '{{%invoice_facture}}');
        $this->dropColumn('{{%invoice_facture}}', 'signed_by_employee_id');
        $this->dropColumn('{{%invoice_facture}}', 'signed_by_name');
        $this->dropColumn('{{%invoice_facture}}', 'sign_document_type_id');
        $this->dropColumn('{{%invoice_facture}}', 'sign_document_number');
        $this->dropColumn('{{%invoice_facture}}', 'sign_document_date');
        $this->dropColumn('{{%invoice_facture}}', 'signature_id');
    }
}
