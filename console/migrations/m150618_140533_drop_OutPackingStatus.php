<?php

use yii\db\Schema;
use yii\db\Migration;

class m150618_140533_drop_OutPackingStatus extends Migration
{
    public function safeUp()
    {
        $this->dropTable('out_packing_status');
    }
    
    public function safeDown()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%out_packing_status}}', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' DEFAULT NULL'
        ], $tableOptions);

        $this->batchInsert('{{%out_packing_status}}', ['name'], [
            ['Создана'],
            ['Распечатана'],
            ['Передана'],
            ['Получена-подписана']
        ]);
    }
}
