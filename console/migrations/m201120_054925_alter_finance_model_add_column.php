<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201120_054925_alter_finance_model_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('finance_model', 'created_at', $this->timestamp()->defaultExpression('NOW()'));
    }

    public function safeDown()
    {
        $this->dropColumn('finance_model', 'created_at');
    }
}
