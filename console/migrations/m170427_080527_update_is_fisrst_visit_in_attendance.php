<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170427_080527_update_is_fisrst_visit_in_attendance extends Migration
{
    public $tableName = 'attendance';

    public function safeUp()
    {
        $this->update($this->tableName, ['is_first_visit' => false]);
        foreach (\common\models\company\Attendance::find()->select(['`id`', 'MIN(`date`)'])->groupBy('company_id')->asArray()->all() as $attendanceParams) {
            $this->update($this->tableName, ['is_first_visit' => true], ['id' => $attendanceParams['id']]);
        }
    }
    
    public function safeDown()
    {

    }
}


