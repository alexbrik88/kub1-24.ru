<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210621_115550_update_crm_client extends Migration
{
    public function safeUp()
    {
        $this->update('{{%crm_client}}', ['client_type_id' => 1]);
    }

    public function safeDown()
    {
        $this->update('{{%crm_client}}', ['client_type_id' => null]);
    }
}
