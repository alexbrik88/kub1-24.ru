<?php

use console\components\db\Migration;
use yii\db\Schema;

class m161124_150930_alter_serviceSubscribe_addColumns extends Migration
{
    public $tSubscribe = 'service_subscribe';
    public $tPaymentType = 'service_payment_type';

    public function safeUp()
    {
        $this->addColumn($this->tSubscribe, 'payment_type_id', $this->integer()->notNull()->defaultValue(1));
        $this->addForeignKey('fk_service_subscribe_to_service_payment_type', $this->tSubscribe, 'payment_type_id', $this->tPaymentType, 'id');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('fk_service_subscribe_to_service_payment_type', $this->tSubscribe);
        $this->dropColumn($this->tSubscribe, 'payment_type_id');
    }
}
