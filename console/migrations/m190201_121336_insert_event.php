<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190201_121336_insert_event extends Migration
{
    public function safeUp()
    {
        $this->batchInsert('event', ['id', 'name'], [
            [84, 'Просмотрена страница "Робот-бухгалтер"'],
            [85, 'Робот-бухгалтер. Заполнены данные на шаге 1'],
            [86, 'Робот-бухгалтер. Просмотрен шаг 3'],
            [87, 'Робот-бухгалтер. Просмотрен попап с оплатой'],
            [88, 'Робот-бухгалтер оплачен'],
        ]);
    }

    public function safeDown()
    {
        $this->delete('event', ['id' => [84, 85, 86, 87, 88]]);
    }
}
