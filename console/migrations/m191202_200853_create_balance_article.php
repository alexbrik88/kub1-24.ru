<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191202_200853_create_balance_article extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%balance_article}}', [
            'id' => $this->primaryKey(11)->unsigned(),
            'company_id' => $this->integer()->notNull(),
            'created_at' => $this->integer(11)->unsigned()->notNull(),
            'updated_at' => $this->integer(11)->unsigned()->notNull(),
            'type' => $this->boolean()->unsigned()->notNull(),
            'status' => $this->tinyInteger()->unsigned()->notNull(),
            'name' => $this->string()->notNull(),
            'category' => $this->tinyInteger()->unsigned()->notNull(),
            'subcategory' => $this->tinyInteger()->unsigned()->notNull(),
            'useful_life_in_month' => $this->integer(11)->unsigned()->defaultValue(null),
            'count' => $this->integer(11)->unsigned()->notNull(),
            'purchased_at' => $this->date()->notNull(),
            'sold_at' => $this->date()->defaultValue(null),
            'written_off_at' => $this->date()->defaultValue(null),
            'amount' => $this->bigInteger(20)->unsigned()->notNull(),
            'description' => $this->text()->defaultValue(null),
        ]);

        $this->addForeignKey('balance_article_to_company', '{{%balance_article}}', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable('{{%balance_article}}');
    }
}
