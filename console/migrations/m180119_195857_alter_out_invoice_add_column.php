<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180119_195857_alter_out_invoice_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('out_invoice', 'note', $this->text()->after('status'));
    }

    public function safeDown()
    {
        $this->dropColumn('out_invoice', 'note');
    }
}
