<?php

use console\components\db\Migration;

class m200110_123250_insales_account_company_id_fk_create extends Migration
{
    const TABLE = '{{%insales_account}}';

    public function up()
    {
        $this->addForeignKey(
            'insales_account_company_id', self::TABLE, 'company_id',
            '{{%company}}', 'id',
            'CASCADE', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey('insales_account_company_id', self::TABLE);
    }
}
