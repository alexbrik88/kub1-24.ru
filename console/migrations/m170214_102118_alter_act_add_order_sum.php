<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170214_102118_alter_act_add_order_sum extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%act}}', 'order_sum', $this->integer()->null()->defaultValue(0));
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%act}}', 'order_sum');
    }
}


