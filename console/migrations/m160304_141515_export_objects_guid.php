<?php

use console\components\db\Migration;
use frontend\modules\export\models\one_c\OneCExport;

/**
 * Class m160304_141515_export_objects_guid
 */
class m160304_141515_export_objects_guid extends Migration
{
    /**
     * @var string
     */
    public $export = 'export';

    /**
     * @var string
     */
    public $exportGUID = 'export_guid';

    /**
     * @var string
     */
    public $guidColumnName = 'object_guid';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        /**  Add "GUID" column for objects */
        foreach ($this->getObjectTypesWithGUID() as $className => $tableName) {
            $this->addColumn($tableName, $this->guidColumnName, $this->string(36)->notNull()->defaultValue(''));

            foreach ((new \yii\db\Query())->select(['id'])->from($tableName)->orderBy('id')->batch(50) as $contractorIds) {
                foreach ($contractorIds as $contractorId) {
                    $GUIDstr = OneCExport::generateGUID();
                    \Yii::$app->db->createCommand()->update($tableName, [$this->guidColumnName => $GUIDstr], 'id = :id', [':id' => $contractorId['id']])->execute();
                    \Yii::$app->db->createCommand()->insert($this->exportGUID, [
                        'class_id' => $className,
                        'object_id' => $contractorId['id'],
                        'guid' => $GUIDstr,
                    ])->execute();
                }
            };
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        /** Remove "GUID" column */
        foreach ($this->getObjectTypesWithGUID() as $objectTableName) {
            $this->dropColumn($objectTableName, $this->guidColumnName);
        }
    }

    /**
     * @return array
     */
    protected function getObjectTypesWithGUID()
    {
        return [
            \common\models\address\Country::className() => 'country',
        ];
    }
}


