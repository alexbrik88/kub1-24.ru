<?php

use common\models\employee\Employee;
use common\models\evotor\Receipt;
use console\components\db\Migration;

class m200109_191717_evotor_receipt_company_id_insert_from_employee_id extends Migration
{
    const TABLE = '{{%evotor_receipt}}';

    public function safeUp()
    {
        foreach (Receipt::find()->all() as $receipt) {
            /** @var Receipt $receipt */
            /** @noinspection PhpUndefinedFieldInspection */
            $receipt->company_id = Employee::findOne(['id' => $receipt->employee_id])->company_id;
            $receipt->save();
        }
    }

    public function down()
    {
        $this->update(self::TABLE, ['company_id' => 0]);
    }
}
