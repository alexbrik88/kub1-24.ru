<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180920_102517_update_system_invoices extends Migration
{
    public function safeUp()
    {
        $this->execute('
            UPDATE {{invoice}}
            SET
                {{invoice}}.[[can_add_act]] = IF({{invoice}}.[[has_services]] = 1, (
                    SELECT IFNULL(SUM({{order}}.[[quantity]]), 0) FROM {{order}}
                    INNER JOIN {{product}} ON {{order}}.[[product_id]] = {{product}}.[[id]]
                    WHERE {{order}}.[[invoice_id]] = {{invoice}}.[[id]] AND {{product}}.[[production_type]] = :service
                ) > (
                    SELECT IFNULL(SUM({{order_act}}.[[quantity]]), 0) FROM {{order_act}}
                    INNER JOIN {{invoice_act}} ON {{order_act}}.[[act_id]] = {{invoice_act}}.[[act_id]]
                    WHERE {{invoice_act}}.[[invoice_id]] = {{invoice}}.[[id]]
                    AND {{order_act}}.[[invoice_id]] = {{invoice}}.[[id]]
                ), 0)
        ', [
            ':service' => \common\models\product\Product::PRODUCTION_TYPE_SERVICE,
        ]);
    }
    
    public function safeDown()
    {
        echo "This migration not rolled back";
    }
}


