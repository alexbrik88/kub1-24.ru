<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160812_100425_add_to_invoice_company_checking_accountant_id extends Migration
{
    public $tInvoice = 'invoice';

    public function safeUp()
    {
        $this->addColumn($this->tInvoice, 'company_checking_accountant_id', $this->integer()->defaultValue(null));
        $this->addForeignKey($this->tInvoice . '_checking_accountant_id', $this->tInvoice, 'company_checking_accountant_id', 'checking_accountant', 'id', 'RESTRICT', 'CASCADE');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey($this->tInvoice . '_checking_accountant_id', $this->tInvoice);
        $this->dropColumn($this->tInvoice, 'company_checking_accountant_id');
    }
}


