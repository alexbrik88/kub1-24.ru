<?php

use yii\db\Migration;

/**
 * Handles the creation of table `order_upd`.
 */
class m170623_082817_create_order_upd_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%order_upd}}', [
            'upd_id' => $this->integer()->notNull(),
            'order_id' => $this->integer()->notNull(),
            'empty_unit_code' => $this->smallInteger(1)->notNull()->defaultValue(false),
            'empty_unit_name' => $this->smallInteger(1)->notNull()->defaultValue(false),
            'empty_product_code' => $this->smallInteger(1)->notNull()->defaultValue(false),
            'empty_box_type' => $this->smallInteger(1)->notNull()->defaultValue(false),
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', '{{%order_upd}}', ['upd_id', 'order_id']);
        $this->addForeignKey ('FK_orderUpd_upd', '{{%order_upd}}', 'upd_id', '{{%upd}}', 'id', 'CASCADE');
        $this->addForeignKey ('FK_orderUpd_order', '{{%order_upd}}', 'order_id', '{{%order}}', 'id', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%order_upd}}');
    }
}
