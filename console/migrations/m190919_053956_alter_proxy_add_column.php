<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190919_053956_alter_proxy_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%proxy}}', 'print_id', $this->integer()->after('signature_id'));
        $this->addColumn('{{%proxy}}', 'chief_signature_id', $this->integer()->after('print_id'));
        $this->addColumn('{{%proxy}}', 'chief_accountant_signature_id', $this->integer()->after('chief_signature_id'));

        $this->addForeignKey(
            'FK_proxy_company_details_print',
            '{{%proxy}}',
            'print_id',
            '{{%company_details_file}}',
            'id'
        );

        $this->addForeignKey(
            'FK_proxy_company_details_chief_signature',
            '{{%proxy}}',
            'chief_signature_id',
            '{{%company_details_file}}',
            'id'
        );

        $this->addForeignKey(
            'FK_proxy_company_details_chief_accountant_signature',
            '{{%proxy}}',
            'chief_accountant_signature_id',
            '{{%company_details_file}}',
            'id'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_proxy_company_details_print', '{{%proxy}}');
        $this->dropForeignKey('FK_proxy_company_details_chief_signature', '{{%proxy}}');
        $this->dropForeignKey('FK_proxy_company_details_chief_accountant_signature', '{{%proxy}}' );

        $this->dropColumn('{{%proxy}}', 'print_id');
        $this->dropColumn('{{%proxy}}', 'chief_signature_id');
        $this->dropColumn('{{%proxy}}', 'chief_accountant_signature_id');
    }
}
