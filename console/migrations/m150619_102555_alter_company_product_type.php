<?php

use yii\db\Schema;
use yii\db\Migration;

class m150619_102555_alter_company_product_type extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%company_product_type}}', 'production_type', Schema::TYPE_INTEGER . ' COMMENT "Тип продукции 1 - товар, 0 - услуга"');

        $this->update('{{%company_product_type}}', ['production_type' => 1], ['id' => 1]);
        $this->update('{{%company_product_type}}', ['production_type' => 0], ['id' => 2]);
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%company_product_type}}', 'production_type');
    }
}
