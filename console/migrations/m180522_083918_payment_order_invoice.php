<?php

use console\components\db\Migration;
use yii\db\Schema;
use common\models\document\PaymentOrder;

class m180522_083918_payment_order_invoice extends Migration
{
    public $tPaymentOrderInvoice = 'payment_order_invoice';
    public $tPaymentOrder = 'payment_order';

    public function safeUp()
    {
        $this->createTable($this->tPaymentOrderInvoice, [
            'payment_order_id' => $this->integer()->notNull(),
            'invoice_id' => $this->integer()->notNull(),
        ]);
        $this->addPrimaryKey('payment_order_invoice_pk', $this->tPaymentOrderInvoice, ['payment_order_id', 'invoice_id']);
        $this->addForeignKey($this->tPaymentOrderInvoice . '_payment_order_id', $this->tPaymentOrderInvoice, 'payment_order_id', $this->tPaymentOrder, 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tPaymentOrderInvoice . '_invoice_id', $this->tPaymentOrderInvoice, 'invoice_id', 'invoice', 'id', 'RESTRICT', 'CASCADE');

        /* @var $paymentOrder PaymentOrder */
        foreach (PaymentOrder::find()->andWhere(['not', ['invoice_id' => null]])->all() as $paymentOrder) {
            $this->insert($this->tPaymentOrderInvoice, [
                'payment_order_id' => $paymentOrder->id,
                'invoice_id' => $paymentOrder->invoice_id,
            ]);
        }
    }
    
    public function safeDown()
    {
        $this->dropPrimaryKey('payment_order_invoice_pk', $this->tPaymentOrderInvoice);
        $this->dropForeignKey($this->tPaymentOrderInvoice . '_payment_order_id', $this->tPaymentOrderInvoice);
        $this->dropForeignKey($this->tPaymentOrderInvoice . '_invoice_id', $this->tPaymentOrderInvoice);

        $this->dropTable($this->tPaymentOrderInvoice);
    }
}


