<?php

use yii\db\Schema;
use yii\db\Migration;

class m151208_112049_alter_serviceStatistic_addColumn_updatedAt extends Migration
{
    public $tStatistic = 'service_statistic';

    public function safeUp()
    {
        $this->addColumn($this->tStatistic, 'updated_at', $this->integer()->notNull());
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tStatistic, 'updated_at');
    }
}
