<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190226_124724_update_contractor_id_in_plan_cash_flows extends Migration
{
    public $tableName = 'plan_cash_flows';

    public function safeUp()
    {
        $this->dropForeignKey($this->tableName . '_contractor_id', $this->tableName);
        $this->alterColumn($this->tableName, 'contractor_id', $this->string()->notNull());
    }

    public function safeDown()
    {
        $this->alterColumn($this->tableName, 'contractor_id', $this->integer()->notNull());
        $this->addForeignKey($this->tableName . '_contractor_id', $this->tableName, 'contractor_id', 'contractor', 'id', 'RESTRICT', 'CASCADE');
    }
}
