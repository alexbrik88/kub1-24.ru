<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170213_135637_alter_packingList_alterColumn extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%packing_list}}', 'basis_document_number', $this->string(50)->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->alterColumn('{{%packing_list}}', 'basis_document_number', $this->integer()->defaultValue(null));
    }
}
