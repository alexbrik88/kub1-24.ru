<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201005_094223_alter_agreement_template_alter_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%agreement_template}}', 'document_body', "MEDIUMTEXT NULL DEFAULT NULL COLLATE 'utf8_general_ci'");
    }

    public function safeDown()
    {
        $this->alterColumn('{{%agreement_template}}', 'document_body', $this->text());
    }
}
