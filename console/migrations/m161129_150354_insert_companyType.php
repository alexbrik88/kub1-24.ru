<?php

use console\components\db\Migration;
use yii\db\Schema;

class m161129_150354_insert_companyType extends Migration
{
    public function safeUp()
    {
        $this->insert('company_type', [
            'id' => 14,
            'name_short' => 'МОУ',
            'name_full' => 'Муниципальное образовательное учреждение',
            'in_company' => 0,
            'in_contractor' => 1,
        ]);
    }
    
    public function safeDown()
    {
        $this->delete('company_type', ['id' => 14]);
    }
}


