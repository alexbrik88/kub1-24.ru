<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181117_140019_add_little_logo_link_and_out_bank_name_to_bank extends Migration
{
    public $tableName = 'bank';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'little_logo_link', $this->string()->defaultValue(null));
        $this->addColumn($this->tableName, 'out_bank_name', $this->string()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'little_logo_link');
        $this->dropColumn($this->tableName, 'out_bank_name');
    }
}


