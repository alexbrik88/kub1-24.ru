<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170417_171431_add_activitu_statu_to_attendance extends Migration
{
    public $tableName = 'attendance';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'activity_status', $this->integer()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'activity_status');
    }
}


