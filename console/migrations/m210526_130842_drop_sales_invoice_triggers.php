<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210526_130842_drop_sales_invoice_triggers extends Migration
{
    public function safeUp()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_invoices_insert_sales_invoice`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_invoices_delete_sales_invoice`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_invoices_update_sales_invoice`');
    }

    public function safeDown()
    {
        // no down
    }
}
