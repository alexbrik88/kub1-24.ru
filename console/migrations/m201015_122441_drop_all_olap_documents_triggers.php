<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201015_122441_drop_all_olap_documents_triggers extends Migration
{
    public function safeUp()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_product_initial_balance`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_insert_product_initial_balance`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_product_store`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_insert_product_store`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_order_packing_list`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_insert_order_packing_list`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_order_upd`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_insert_order_upd`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_packing_list`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_delete_packing_list`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_upd`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_delete_upd`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_act`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_delete_act`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_product`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_invoice`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_contractor`;');
    }

    public function safeDown()
    {

    }
}
