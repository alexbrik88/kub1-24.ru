<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%ofd_receipt_item}}`.
 */
class m200520_174145_create_ofd_receipt_item_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $p = 0;
        $date = date_create('2016-01-01')->modify('today');
        $maxDate = date_create('first day of this month')->modify('today');
        $options = "PARTITION BY RANGE (TO_DAYS([[date_time]])) (\n\tPARTITION `p{$p}` VALUES LESS THAN (TO_DAYS(\"{$date->format('Y-m-d')}\"))";
        while ($date < $maxDate) {
            $p++;
            $date->modify('+1 month');
            $options .= ",\n\tPARTITION `p{$p}` VALUES LESS THAN (TO_DAYS(\"{$date->format('Y-m-d')}\"))";
        }
        $p++;
        $options .= ",\n\tPARTITION `pMAXVALUE` VALUES LESS THAN MAXVALUE\n)";

        $this->createTable('{{%ofd_receipt_item}}', [
            'id' => $this->bigInteger()->notNull()->unsigned() . ' AUTO_INCREMENT',
            'date_time' => $this->dateTime()->notNull()->comment('1012 дата и время чека'),
            'receipt_id' => $this->bigInteger()->notNull()->comment('id чека'),
            'company_id' => $this->integer()->notNull(),
            'name' => $this->text()->comment('1030 наименование'),
            'barcode' => $this->string()->comment('1107 штриховой код EAN13'),
            'price' => $this->bigInteger()->notNull()->comment('1079 цена за единицу'),
            'quantity' => $this->string()->notNull()->comment('1023 количество'),
            'nds_20' => $this->bigInteger()->notNull()->comment('1102 Сумма НДС по ставке 20%'),
            'nds_10' => $this->bigInteger()->notNull()->comment('1103 Сумма НДС по ставке 10%'),
            'nds_0' => $this->bigInteger()->notNull()->comment('1104 Сумма НДС по ставке 0%'),
            'nds_no' => $this->bigInteger()->notNull()->comment('1105 Сумма без НДС'),
            'nds_calculated_20' => $this->bigInteger()->comment('1106 Сумма НДС по ставке 20/120'),
            'nds_calculated_10' => $this->bigInteger()->comment('1107 Сумма НДС по ставке 10/110'),
            'sum' => $this->bigInteger()->notNull()->comment('1043 общая стоимость позиции с учетом скидок и наценок'),
            'product_type' => $this->integer()->notNull()->comment('1212 признак предмета расчета'),
            'product_code' => $this->integer()->comment('1162 код товарной номенклатуры'),
            '_modifiers' => $this->json()->comment('1112 скидка/наценка'),
            'PRIMARY KEY ([[id]], [[date_time]])',
        ], $options);

        $this->createIndex('receipt_items', '{{%ofd_receipt_item}}', [
            'company_id',
            'date_time',
        ]);

        $this->createIndex('receipt', '{{%ofd_receipt_item}}', [
            'receipt_id',
            'date_time',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%ofd_receipt_item}}');
    }
}
