<?php

use yii\db\Schema;
use yii\db\Migration;

class m150415_162722_alter_Company_dropColumn_AddressFull extends Migration
{
    public function up()
    {
        $this->dropColumn('company', 'address_legal_full');
        $this->dropColumn('company', 'address_actual_full');
    }

    public function down()
    {
        $this->addColumn('company', 'address_legal_full', Schema::TYPE_STRING . '(1024) NULL');
        $this->addColumn('company', 'address_actual_full', Schema::TYPE_STRING . '(1024) NULL');
    }
}
