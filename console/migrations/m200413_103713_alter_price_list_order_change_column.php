<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200413_103713_alter_price_list_order_change_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('price_list_order', 'quantity', $this->decimal(20, 10)->defaultValue(null));
    }

    public function safeDown()
    {
        $this->alterColumn('price_list_order', 'quantity', $this->integer()->defaultValue(null));
    }
}
