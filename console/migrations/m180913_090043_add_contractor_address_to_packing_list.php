<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180913_090043_add_contractor_address_to_packing_list extends Migration
{
    public $tableName = 'packing_list';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'contractor_address', $this->boolean()->defaultValue(false));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'contractor_address');
    }
}


