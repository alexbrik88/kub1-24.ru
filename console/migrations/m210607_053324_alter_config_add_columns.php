<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210607_053324_alter_config_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_config', 'report_odds_row_finance', $this->tinyInteger(1)->notNull()->defaultValue(1));
        $this->addColumn('user_config', 'report_odds_row_investments', $this->tinyInteger(1)->notNull()->defaultValue(1));
        $this->addColumn('user_config', 'report_odds_row_bank', $this->tinyInteger(1)->notNull()->defaultValue(1));
        $this->addColumn('user_config', 'report_odds_row_order', $this->tinyInteger(1)->notNull()->defaultValue(1));
        $this->addColumn('user_config', 'report_odds_row_emoney', $this->tinyInteger(1)->notNull()->defaultValue(1));
        $this->addColumn('user_config', 'report_odds_row_acquiring', $this->tinyInteger(1)->notNull()->defaultValue(1));
        $this->addColumn('user_config', 'report_odds_row_card', $this->tinyInteger(1)->notNull()->defaultValue(1));
        $this->addColumn('user_config', 'report_odds_hide_zeroes', $this->tinyInteger(1)->notNull()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('user_config', 'report_odds_row_finance');
        $this->dropColumn('user_config', 'report_odds_row_investments');
        $this->dropColumn('user_config', 'report_odds_row_bank');
        $this->dropColumn('user_config', 'report_odds_row_order');
        $this->dropColumn('user_config', 'report_odds_row_emoney');
        $this->dropColumn('user_config', 'report_odds_row_acquiring');
        $this->dropColumn('user_config', 'report_odds_row_card');
    }
}
