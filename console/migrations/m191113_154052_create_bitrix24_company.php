<?php

use common\models\bitrix24\Company;
use console\components\db\Migration;

class m191113_154052_create_bitrix24_company extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{bitrix24_company}}', [
            'id' => $this->integer()->unsigned()->notNull()->comment('Идентификатор компании Битрикс24'),
            'employee_id' => $this->integer()->notNull()->comment('ID клиента КУБ'),
            'type' => $this->string(50)->notNull()->defaultValue('OTHER')->comment('Тип компании'),
            'title' => $this->string(300)->notNull()->comment('Название'),
            'logo' => $this->string(500)->comment('URL логотипа'),
            'industry' => $this->string(50)->notNull()->defaultValue('OTHER')->comment('Сфера деятельности'),
            'revenue' => $this->float()->unsigned()->comment('Годовой оборот'),
            'currency' => $this->char(3)->notNull()->defaultValue('RUB')->comment('Валюта'),
            'phone' => $this->string(1000)->comment('Номера телефонов, JSON'),
            'email' => $this->string(1000)->comment('E-mail, JSON'),
            'web' => $this->string(1000)->comment('Сайты, JSON'),
            'im' => $this->string(1000)->comment('Соц. сети, JSON'),
            'created_at' => $this->integer()->unsigned()->notNull()->comment('Дата добавления'),
            'updated_at' => $this->integer()->unsigned()->notNull()->comment('Дата последнего измененеия'),
        ], "COMMENT 'Интеграция Битрикс24: компании'");
        $this->addPrimaryKey('bitrix24_company_id', '{{bitrix24_company}}', ['employee_id', 'id']);
        $this->addForeignKey(
            'bitrix24_company_employee_id', '{{bitrix24_company}}', 'employee_id',
            '{{employee}}', 'id', 'CASCADE', 'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropTable('{{bitrix24_company}}');

    }
}
