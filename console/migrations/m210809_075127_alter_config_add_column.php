<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210809_075127_alter_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_config', 'act_scan', $this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('user_config', 'act_scan');
    }
}
