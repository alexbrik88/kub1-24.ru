<?php

use yii\db\Migration;

/**
 * Handles the creation of table `ifns_upload_log`.
 */
class m171123_111932_create_ifns_upload_log_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('ifns_upload_log', [
            'id' => $this->primaryKey(),
            'file_name' => $this->string()->notNull(),
            'created_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('ifns_upload_log');
    }
}
