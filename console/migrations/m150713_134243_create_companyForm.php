<?php

use yii\db\Schema;
use yii\db\Migration;

class m150713_134243_create_companyForm extends Migration
{
    public function safeUp()
    {
        $this->insert('company_type', [
            'id' => 3,
            'name_short' => 'ЗАО',
            'name_full' => 'Закрытое акционерное общество',
        ]);
    }
    
    public function safeDown()
    {
        $this->delete('company_type', 'id = 3');
    }
}
