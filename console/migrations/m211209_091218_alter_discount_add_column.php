<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211209_091218_alter_discount_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%discount}}', 'is_one_time', $this->boolean());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%discount}}', 'is_one_time');
    }
}
