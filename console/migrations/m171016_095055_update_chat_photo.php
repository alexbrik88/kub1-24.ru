<?php

use console\components\db\Migration;
use yii\db\Schema;
use common\models\EmployeeCompany;
use common\models\employee\Employee;

class m171016_095055_update_chat_photo extends Migration
{
    public $tEmployee = 'employee';

    public $tEmployeeCompany = 'employee_company';

    public function safeUp()
    {
        /* @var $employeeCompany EmployeeCompany */
        foreach (EmployeeCompany::find()->andWhere(['not', ['sex' => null]])->all() as $employeeCompany) {
            if ($employeeCompany->sex == Employee::MALE) {
                $this->update($this->tEmployee,
                    ['chat_photo' => Employee::$chatPhotos[Employee::MALE][array_rand(Employee::$chatPhotos[Employee::MALE])]],
                    ['id' => $employeeCompany->employee_id]);
            } elseif ($employeeCompany->sex == Employee::FEMALE) {
                $this->update($this->tEmployee,
                    ['chat_photo' => Employee::$chatPhotos[Employee::FEMALE][array_rand(Employee::$chatPhotos[Employee::FEMALE])]],
                    ['id' => $employeeCompany->employee_id]);
            }
        }
    }

    public function safeDown()
    {

    }
}


