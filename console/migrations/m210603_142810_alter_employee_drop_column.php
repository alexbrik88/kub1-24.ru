<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210603_142810_alter_employee_drop_column extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('{{%employee}}', 'last_visit_at');
    }

    public function safeDown()
    {
        $this->addColumn('{{%employee}}', 'last_visit_at', $this->integer()->after('view_notification_date'));
    }
}
