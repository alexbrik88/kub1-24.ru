<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210826_152247_alter_payments_request_sign_step extends Migration
{
    public function safeUp()
    {
        $this->addColumn('payments_request_sign_step', 'name', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn('payments_request_sign_step', 'name');
    }
}
