<?php

use yii\db\Migration;

class m210307_123419_rename_yandex_cash_identity_table extends Migration
{
    /**
     * @inheritDoc
     */
    public function up()
    {
        $this->renameTable('{{%yandex_cash_identity}}', '{{%yookassa_identity}}');
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        $this->renameTable('{{%yookassa_identity}}', '{{%yandex_cash_identity}}');
    }
}
