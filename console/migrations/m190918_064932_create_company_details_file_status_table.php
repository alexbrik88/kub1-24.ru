<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%company_details_file_status}}`.
 */
class m190918_064932_create_company_details_file_status_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%company_details_file_status}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);

        $this->batchInsert('{{%company_details_file_status}}', ['id', 'name'], [
            [1, 'Активный'],
            [2, 'Временный'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%company_details_file_status}}');
    }
}
