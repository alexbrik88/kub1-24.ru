<?php

/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 16.12.2016
 * Time: 5:22
 */

use console\components\db\Migration;

class m161216_081456_insert_into_company_type extends Migration
{
    public $tTableName = 'company_type';

    public function safeUp()
    {
        $this->insert($this->tTableName, [
            'id' => 16,
            'name_short' => 'МБОУ',
            'name_full' => 'Муниципальное бюджетное общеобразовательное учреждение',
            'in_company' => 0,
            'in_contractor' => 1,
        ]);

        $this->update($this->tTableName, ['name_short' => 'ФГБОУ ВО'], ['id' => 15]);
        $this->update($this->tTableName, ['name_full' => 'Федеральное государственное бюджетное образовательное учреждение Высшего образования'], ['id' => 15]);
    }

    public function safeDown()
    {
        $this->delete($this->tTableName, ['id' => 16]);
        $this->update($this->tTableName, ['name_short' => 'ФГБОУ'], ['id' => 15]);
        $this->update($this->tTableName, ['name_full' => 'Федеральное государственное бюджетное образовательное учреждение'], ['id' => 15]);
    }
}