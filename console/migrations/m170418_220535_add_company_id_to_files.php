<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170418_220535_add_company_id_to_files extends Migration
{
    public $tableName = 'file';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'company_id', $this->integer()->defaultValue(null));
        $this->addForeignKey($this->tableName . '_company_id', $this->tableName, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');
        /* @var $file \common\models\file\File */
        foreach (\common\models\file\File::find()->all() as $file) {
            $this->update($this->tableName, ['company_id' => $file->createdAtAuthor->company_id], ['id' => $file->id]);
        }
    }

    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_company_id', $this->tableName);
        $this->dropColumn($this->tableName, 'company_id');
    }
}


