<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210902_171301_create_moysklad_import_document extends Migration
{
    public $documentType = 'moysklad_document_type';
    public $document = 'moysklad_document';
    public $documentValidation = 'moysklad_document_validation';

    public function safeUp()
    {
        // DOCUMENT TYPE
        $table = $this->documentType;
        $this->createTable($table, [
            'id' => $this->primaryKey(),
            'name' => $this->string()
        ]);

        $this->batchInsert($table, ['id', 'name'], [
            [11, 'Вх.Счет'],
            [12, 'Вх.УПД'],
            [13, 'Вх.СчетФактура'],
            [21, 'Исх.Счет'],
            [22, 'Исх.УПД'],
            [23, 'Исх.СчетФактура'],
        ]);

        // DOCUMENT
        $table = $this->document;
        $this->createTable($table, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'document_type_id' => $this->integer()->notNull(),

            'object_type' => $this->tinyInteger()->notNull()->comment('1 - in, 2 - out'),
            'object_date' => $this->date(),
            'object_number' => $this->char(64),

            'contractor_id' => $this->integer(),
            'act_id' => $this->integer(),
            'order_document_id' => $this->integer(),
            'invoice_id' => $this->integer(),
            'packing_list_id' => $this->integer(),
            'upd_id' => $this->integer(),

            'is_validated' => $this->boolean(),
            'validated_at' => $this->integer(),
            'one_c_object' => $this->text(),
        ]);

        $this->addForeignKey("FK_{$table}_to_company", $table, 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("FK_{$table}_to_document_type", $table, 'document_type_id', $this->documentType, 'id', 'CASCADE', 'CASCADE');        
        $this->addForeignKey("FK_{$table}_to_contractor", $table, 'contractor_id', 'contractor', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("FK_{$table}_to_act", $table, 'act_id', 'act', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("FK_{$table}_to_order_document", $table, 'order_document_id', 'order_document', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("FK_{$table}_to_invoice", $table, 'invoice_id', 'invoice', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("FK_{$table}_to_packing_list", $table, 'packing_list_id', 'packing_list', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("FK_{$table}_to_upd", $table, 'upd_id', 'upd', 'id', 'CASCADE', 'CASCADE');

        $this->createIndex("IDX_{$table}_contractor", $table, ['company_id', 'document_type_id', 'contractor_id']);

        // DOCUMENT VALIDATION
        $table = $this->documentValidation;
        $this->createTable($table, [
            'import_id' => $this->integer(),
            'document_id' => $this->integer(),
            'errors' => $this->text()
        ]);

        $this->addPrimaryKey("PK_{$table}", $table, ['import_id', 'document_id']);
        $this->addForeignKey("FK_{$table}_to_import", $table, 'import_id', 'moysklad_import', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("FK_{$table}_to_document", $table, 'document_id', 'moysklad_document', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        // DOCUMENT VALIDATION
        $table = $this->documentValidation;
        $this->dropForeignKey("FK_{$table}_to_document", $table);
        $this->dropForeignKey("FK_{$table}_to_import", $table);
        $this->dropTable($table);

        // DOCUMENT
        $table = $this->document;
        $this->dropForeignKey("FK_{$table}_to_company", $table);
        $this->dropForeignKey("FK_{$table}_to_document_type", $table);
        $this->dropForeignKey("FK_{$table}_to_contractor", $table);
        $this->dropForeignKey("FK_{$table}_to_act", $table);
        $this->dropForeignKey("FK_{$table}_to_order_document", $table);
        $this->dropForeignKey("FK_{$table}_to_invoice", $table);
        $this->dropForeignKey("FK_{$table}_to_packing_list", $table);
        $this->dropForeignKey("FK_{$table}_to_upd", $table);
        $this->dropTable($table);

        // DOCUMENT TYPE
        $table = $this->documentType;
        $this->dropTable($table);
    }
}
