<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190410_072735_proxy_set_status_out extends Migration
{
    public function safeUp()
    {
        $this->execute("
            UPDATE
                {{%proxy}}
            SET
                {{%proxy}}.[[status_out_id]] = 1,
                {{%proxy}}.[[status_out_author_id]] = {{%proxy}}.[[document_author_id]],
                {{%proxy}}.[[status_out_updated_at]] = {{%proxy}}.[[created_at]] 
            WHERE
              {{%proxy}}.[[status_out_id]] IS NULL
        ");
    }

    public function safeDown()
    {
        $this->execute("
            UPDATE
                {{%proxy}}
            SET
                {{%proxy}}.[[status_out_id]] = NULL,
                {{%proxy}}.[[status_out_author_id]] = NULL,
                {{%proxy}}.[[status_out_updated_at]] = NULL 
            WHERE
              {{%proxy}}.[[status_out_id]] = 1
        ");
    }
}
