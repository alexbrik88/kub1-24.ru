<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180815_135905_alter_company_change_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('company', 'chief_post_name', $this->string(255));
    }

    public function safeDown()
    {
        $this->alterColumn('company', 'chief_post_name', $this->string(45));
    }
}
