<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170831_085844_alter_employeeCompany_addColumns extends Migration
{
    public function safeUp()
    {
    	$this->alterColumn('{{%employee_company}}', 'id', $this->integer());
    	$this->dropPrimaryKey('PRIMARY_KEY', '{{%employee_company}}');
        $this->createIndex('employee_company_employee_id', '{{%employee_company}}', 'employee_id');
    	$this->dropIndex('employee_company_uidx', '{{%employee_company}}');
    	$this->addPrimaryKey('PRIMARY_KEY', '{{%employee_company}}', ['employee_id', 'company_id']);
    	$this->dropColumn('{{%employee_company}}', 'id');

    	$this->addColumn('{{%employee_company}}', 'lastname', $this->string(45)->after('company_id'));
    	$this->addColumn('{{%employee_company}}', 'firstname', $this->string(45)->after('lastname'));
    	$this->addColumn('{{%employee_company}}', 'patronymic', $this->string(45)->after('firstname'));
    	$this->addColumn('{{%employee_company}}', 'firstname_initial', $this->string(1)->after('patronymic'));
    	$this->addColumn('{{%employee_company}}', 'patronymic_initial', $this->string(1)->after('firstname_initial'));
    	$this->addColumn('{{%employee_company}}', 'sex', $this->smallInteger(1)->after('patronymic_initial'));
    	$this->addColumn('{{%employee_company}}', 'birthday', $this->date()->after('sex'));
    	$this->addColumn('{{%employee_company}}', 'phone', $this->string(20)->after('employee_role_id'));
    	$this->addColumn('{{%employee_company}}', 'time_zone_id', $this->integer()->defaultValue(57)->after('phone'));
    	$this->addColumn('{{%employee_company}}', 'created_at', $this->integer());
    	$this->addColumn('{{%employee_company}}', 'updated_at', $this->integer());

        $this->execute("
			UPDATE {{%employee_company}} {{empl_c}}
			LEFT JOIN {{%employee}} {{empl}}
				ON {{empl_c}}.[[employee_id]] = {{empl}}.[[id]]
			SET
				{{empl_c}}.[[lastname]] = {{empl}}.[[lastname]],
				{{empl_c}}.[[firstname]] = {{empl}}.[[firstname]],
				{{empl_c}}.[[patronymic]] = {{empl}}.[[patronymic]],
				{{empl_c}}.[[firstname_initial]] = LEFT({{empl}}.[[firstname]], 1),
				{{empl_c}}.[[patronymic_initial]] = LEFT({{empl}}.[[patronymic]], 1),
				{{empl_c}}.[[sex]] = {{empl}}.[[sex]],
				{{empl_c}}.[[birthday]] = {{empl}}.[[birthday]],
				{{empl_c}}.[[phone]] = {{empl}}.[[phone]],
				{{empl_c}}.[[time_zone_id]] = {{empl}}.[[time_zone_id]],
				{{empl_c}}.[[created_at]] = {{empl}}.[[created_at]],
				{{empl_c}}.[[updated_at]] = {{empl}}.[[created_at]]
        ");
    }

    public function safeDown()
    {
    	$this->dropPrimaryKey('PRIMARY_KEY', '{{%employee_company}}');
    	$this->createIndex('employee_company_uidx', '{{%employee_company}}', ['employee_id', 'company_id'], true);
    	$this->addColumn('{{%employee_company}}', 'id', $this->primaryKey()->first());

    	$this->dropColumn('{{%employee_company}}', 'lastname');
    	$this->dropColumn('{{%employee_company}}', 'firstname');
    	$this->dropColumn('{{%employee_company}}', 'patronymic');
    	$this->dropColumn('{{%employee_company}}', 'firstname_initial');
    	$this->dropColumn('{{%employee_company}}', 'patronymic_initial');
    	$this->dropColumn('{{%employee_company}}', 'sex');
    	$this->dropColumn('{{%employee_company}}', 'birthday');
    	$this->dropColumn('{{%employee_company}}', 'phone');
    	$this->dropColumn('{{%employee_company}}', 'time_zone_id');
    	$this->dropColumn('{{%employee_company}}', 'created_at');
    	$this->dropColumn('{{%employee_company}}', 'updated_at');
    }
}
