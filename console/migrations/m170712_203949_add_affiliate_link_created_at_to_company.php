<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170712_203949_add_affiliate_link_created_at_to_company extends Migration
{
    public $tableName = 'company';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'affiliate_link_created_at', $this->integer()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'affiliate_link_created_at');
    }
}


