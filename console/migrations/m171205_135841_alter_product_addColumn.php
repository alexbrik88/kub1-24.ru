<?php

use console\components\db\Migration;
use yii\db\Schema;

class m171205_135841_alter_product_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('product', 'not_for_sale', $this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('product', 'not_for_sale');
    }
}
