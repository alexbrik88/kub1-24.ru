<?php

use yii\db\Schema;
use yii\db\Migration;

class m150520_083225_create_currency extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('currency', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING,
            'old_value' => Schema::TYPE_STRING . ' COMMENT "Вчерашний курс"',
            'current_value' => Schema::TYPE_STRING . ' COMMENT "Курс на сегодня"',
            'update_date' => Schema::TYPE_DATE . ' COMMENT "Дата обновления"',
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('currency');
    }
}
