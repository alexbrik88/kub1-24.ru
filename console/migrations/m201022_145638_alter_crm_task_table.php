<?php

use console\components\db\Migration;

class m201022_145638_alter_crm_task_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%crm_task}}';

    /**
     * @inheritDoc
     */
    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'task_number', $this->bigInteger()->notNull()->defaultValue(0)->after('task_id'));
        $this->addColumn(self::TABLE_NAME, 'task_name', $this->string(64)->notNull()->defaultValue('')->after('task_number'));

        $this->createIndex('ck_task_number', self::TABLE_NAME, ['task_number']);
        $this->createIndex('ck_task_name', self::TABLE_NAME, ['task_name']);
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'task_number');
        $this->dropColumn(self::TABLE_NAME, 'task_name');
    }
}
