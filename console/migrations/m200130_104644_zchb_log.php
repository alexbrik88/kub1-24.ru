<?php

use console\components\db\Migration;

class m200130_104644_zchb_log extends Migration
{
    const TABLE = '{{%zchb_log}}';

    public function up()
    {
        $this->createTable(self::TABLE, [
            'contractor_id' => $this->integer(11)->notNull(),
            'date' => $this->dateTime()->notNull()->defaultExpression('NOW()'),
            'method' => $this->char(20)->notNull()->comment('Метод API'),
            'query' => $this->string(500)->notNull()->comment('Параметры запроса'),
            'success' => $this->boolean()->notNull()->defaultValue(true)->comment('Был ли запрос успешно выполнен'),
            'url' => $this->string(1000)
        ], "comment='Лог API-запросов ЗАЧЕСТНЫЙБИЗНЕС'");
        $this->addForeignKey(
            'zchb_log_contractor_id', self::TABLE, 'contractor_id',
            '{{%contractor}}', 'id',
            'RESTRICT', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable(self::TABLE);
    }

}
