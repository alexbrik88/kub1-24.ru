<?php

use yii\db\Schema;
use yii\db\Migration;

class m150416_120548_create_payment_variant extends Migration
{
    public function up()
    {
        $this->createTable('payment_variant',[
            'id' => Schema::TYPE_PK,
            'payment_name' => Schema::TYPE_STRING . ' DEFAULT NULL COMMENT "Название формы оплаты"'
        ]);
    }

    public function down()
    {
        $this->dropTable('payment_variant');
    }

}
