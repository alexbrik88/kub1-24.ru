<?php

use console\components\db\Migration;

class m200110_110951_amocrm_leads_contact_id_fk_create extends Migration
{
    const TABLE = '{{%amocrm_leads}}';

    public function up()
    {
        $this->addForeignKey(
            'amocrm_leads_company_id', self::TABLE, 'company_id',
            '{{%company}}', 'id',
            'CASCADE', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey('amocrm_leads_company_id', self::TABLE);
    }
}
