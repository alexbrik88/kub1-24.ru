<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%sent_email_start}}`.
 */
class m210119_141051_create_sent_email_start_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function up()
    {
        $this->createTable('{{%sent_email_start}}', [
            'employee_id' => $this->integer()->notNull(),
            'sent_email_type_id' => $this->integer()->notNull(),
            'date' => $this->date()->notNull(),
            'PRIMARY KEY ([[employee_id]], [[sent_email_type_id]])'
        ]);

        $this->createIndex('date_type_id', '{{%sent_email_start}}', [
            'date',
            'sent_email_type_id',
        ]);

        $this->addForeignKey('fk_sent_email_start__employee_id', '{{%sent_email_start}}', 'employee_id', '{{%employee}}', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function down()
    {
        $this->dropTable('{{%sent_email_start}}');
    }
}
