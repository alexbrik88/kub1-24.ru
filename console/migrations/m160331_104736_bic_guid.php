<?php

use common\models\dictionary\bik\BikDictionary;
use console\components\db\Migration;
use frontend\modules\export\models\one_c\OneCExport;

/**
 * Class m160331_104736_bic_guid
 */
class m160331_104736_bic_guid extends Migration
{
    /**
     * @var string
     */
    public $tableName = 'bik_dictionary';

    /**
     * @var string
     */
    public $exportGUID = 'export_guid';

    /**
     * @var string
     */
    public $guidColumnName = 'object_guid';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        /**  Add "GUID" column for objects */
        $this->addColumn($this->tableName, $this->guidColumnName, $this->string(36)->notNull()->defaultValue(''));

        foreach ((new \yii\db\Query())->select(['bik'])->from($this->tableName)->orderBy('bik')->batch(50) as $contractorIds) {
            foreach ($contractorIds as $contractorId) {
                $GUIDstr = OneCExport::generateGUID();

                \Yii::$app->db->createCommand()->update($this->tableName, [$this->guidColumnName => $GUIDstr], 'bik = :bik', [':bik' => $contractorId['bik']])->execute();
                \Yii::$app->db->createCommand()->insert($this->exportGUID, [
                    'class_id'  => BikDictionary::className(),
                    'object_id' => $contractorId['bik'],
                    'guid'      => $GUIDstr,
                ])->execute();
            }
        };
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        /** Remove "GUID" column */
        $this->dropColumn($this->tableName, $this->guidColumnName);
    }
}


