<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190805_073200_insert_emoney extends Migration
{
    public function safeUp()
    {
        $this->execute('
            INSERT INTO {{%emoney}} ([[company_id]], [[name]], [[is_main]])
            SELECT [[id]], "E-money", 1
            FROM {{%company}}
        ');
    }

    public function safeDown()
    {
        $this->execute('SET foreign_key_checks = 0;');
        $this->truncateTable('{{%emoney}}');
        $this->execute('SET foreign_key_checks = 1;');
    }
}
