<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210630_080904_alter_config_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_config','goods_cancellation_contractor', $this->tinyInteger()->notNull()->defaultValue(0));
        $this->addColumn('user_config','goods_cancellation_project', $this->tinyInteger()->notNull()->defaultValue(0));
        $this->addColumn('user_config','goods_cancellation_estimate', $this->tinyInteger()->notNull()->defaultValue(0));
        $this->addColumn('user_config','goods_cancellation_pal_type', $this->tinyInteger()->notNull()->defaultValue(0));
        $this->addColumn('user_config','goods_cancellation_responsible', $this->tinyInteger()->notNull()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('user_config','goods_cancellation_responsible');
        $this->dropColumn('user_config','goods_cancellation_pal_type');
        $this->dropColumn('user_config','goods_cancellation_estimate');
        $this->dropColumn('user_config','goods_cancellation_project');
        $this->dropColumn('user_config','goods_cancellation_contractor');
    }
}
