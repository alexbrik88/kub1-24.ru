<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200916_122152_create_triggers_insert_update_product_turnover_for_initial_balance extends Migration
{
    /**
     * @var string
     */
    public $dropSql = '
        DROP TRIGGER IF EXISTS `product_turnover__product_store__after_insert`;
        DROP TRIGGER IF EXISTS `product_turnover__product_store__after_update`;
        DROP TRIGGER IF EXISTS `product_turnover__product_initial_balance__after_insert`;
        DROP TRIGGER IF EXISTS `product_turnover__product_initial_balance__after_update`;
    ';

    public function safeUp()
    {
        /**
         * Delet if exists
         */
        $this->execute($this->dropSql);

        /**
         * `product` triggers
         */
        $this->execute('
CREATE TRIGGER `product_turnover__product_store__after_insert` AFTER INSERT ON `product_store`
FOR EACH ROW
BEGIN
    IF
        NEW.`initial_quantity` <> 0
    THEN
        INSERT INTO `product_turnover` (
            `order_id`,
            `document_id`,
            `document_table`,
            `invoice_id`,
            `contractor_id`,
            `company_id`,
            `date`,
            `type`,
            `production_type`,
            `is_invoice_actual`,
            `is_document_actual`,
            `purchase_price`,
            `price_one`,
            `quantity`,
            `total_amount`,
            `purchase_amount`,
            `margin`,
            `year`,
            `month`,
            `product_group_id`,
            `product_id`
        ) SELECT
            0,
            NEW.`product_id`,
            "initial_balance",
            0,
            0,
            `product`.`company_id`,
            @date:=IFNULL(`product_initial_balance`.`date`, FROM_UNIXTIME(MIN(`product_store`.`created_at`))),
            0,
            `product`.`production_type`,
            0,
            0,
            IFNULL(`product`.`price_for_buy_with_nds`, 0),
            0,
            SUM(`product_store`.`initial_quantity`),
            0,
            0,
            0,
            YEAR(@date),
            MONTH(@date),
            `product`.`group_id`,
            NEW.`product_id`
        FROM `product_store`
        LEFT JOIN `product` ON `product`.`id` = `product_store`.`product_id`
        LEFT JOIN `product_initial_balance` ON `product_initial_balance`.`product_id` = `product_store`.`product_id`
        WHERE `product_store`.`product_id` = NEW.`product_id`
        AND `product`.`production_type`=1
        GROUP BY `product_store`.`product_id`
        ON DUPLICATE KEY UPDATE
            `date` = VALUES(`date`),
            `production_type` = VALUES(`production_type`),
            `quantity` = VALUES(`quantity`),
            `year` = VALUES(`year`),
            `month` = VALUES(`month`),
            `product_group_id` = VALUES(`product_group_id`);
    END IF;
END;
        ');

        /**
         * `product` triggers
         */
        $this->execute('
CREATE TRIGGER `product_turnover__product_store__after_update` AFTER UPDATE ON `product_store`
FOR EACH ROW
BEGIN
    IF
        NEW.`initial_quantity` <> OLD.`initial_quantity`
    THEN
        INSERT INTO `product_turnover` (
            `order_id`,
            `document_id`,
            `document_table`,
            `invoice_id`,
            `contractor_id`,
            `company_id`,
            `date`,
            `type`,
            `production_type`,
            `is_invoice_actual`,
            `is_document_actual`,
            `purchase_price`,
            `price_one`,
            `quantity`,
            `total_amount`,
            `purchase_amount`,
            `margin`,
            `year`,
            `month`,
            `product_group_id`,
            `product_id`
        ) SELECT
            0,
            NEW.`product_id`,
            "initial_balance",
            0,
            0,
            `product`.`company_id`,
            @date:=IFNULL(`product_initial_balance`.`date`, FROM_UNIXTIME(MIN(`product_store`.`created_at`))),
            0,
            `product`.`production_type`,
            0,
            0,
            IFNULL(`product`.`price_for_buy_with_nds`, 0),
            0,
            SUM(`product_store`.`initial_quantity`),
            0,
            0,
            0,
            YEAR(@date),
            MONTH(@date),
            `product`.`group_id`,
            NEW.`product_id`
        FROM `product_store`
        LEFT JOIN `product` ON `product`.`id` = `product_store`.`product_id`
        LEFT JOIN `product_initial_balance` ON `product_initial_balance`.`product_id` = `product_store`.`product_id`
        WHERE `product_store`.`product_id` = NEW.`product_id`
        AND `product`.`production_type`=1
        GROUP BY `product_store`.`product_id`
        ON DUPLICATE KEY UPDATE
            `date` = VALUES(`date`),
            `production_type` = VALUES(`production_type`),
            `quantity` = VALUES(`quantity`),
            `year` = VALUES(`year`),
            `month` = VALUES(`month`),
            `product_group_id` = VALUES(`product_group_id`);
    END IF;
END;
        ');

        /**
         * `product` triggers
         */
        $this->execute('
CREATE TRIGGER `product_turnover__product_initial_balance__after_insert` AFTER INSERT ON `product_initial_balance`
FOR EACH ROW
BEGIN
    IF
        IFNULL(NEW.`date`, "0000-00-00") <> "0000-00-00"
    THEN
        UPDATE `product_turnover`
        SET `product_turnover`.`date` = NEW.`date`
        WHERE `product_turnover`.`product_id` = NEW.`product_id`;
    END IF;
END;
        ');

        /**
         * `product` triggers
         */
        $this->execute('
CREATE TRIGGER `product_turnover__product_initial_balance__after_update` AFTER UPDATE ON `product_initial_balance`
FOR EACH ROW
BEGIN
    IF
        IFNULL(NEW.`date`, "0000-00-00") <> "0000-00-00"
        AND
        IFNULL(NEW.`date`, "0000-00-00") <> IFNULL(OLD.`date`, "0000-00-00")
    THEN
        UPDATE `product_turnover`
        SET `product_turnover`.`date` = NEW.`date`
        WHERE `product_turnover`.`product_id` = NEW.`product_id`;
    END IF;
END;
        ');
    }

    public function safeDown()
    {
        /**
         * Delet if exists
         */
        $this->execute($this->dropSql);
    }
}
