<?php

use console\components\db\Migration;
use yii\db\Schema;

class m151221_101613_alter_promoCode_addColumn_beginAt extends Migration
{
    public $tPromo = 'service_payment_promo_code';

    public function safeUp()
    {
        $this->addColumn($this->tPromo, 'activated_at', $this->integer());

        $this->update($this->tPromo, [
            'activated_at' => new \yii\db\Expression('created_at'),
        ]);
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tPromo, 'activated_at');
    }
}


