<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%autovoronka}}`.
 */
class m191212_074914_create_autovoronka_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%autovoronka}}', [
            'email' => $this->string()->notNull(),
            'is_lm' => $this->boolean()->defaultValue(false),
            'created' => $this->date()->notNull(),
            'repeat' => $this->date()->defaultValue(null),
            'last_mail' => $this->date()->defaultValue(null),
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', '{{%autovoronka}}', 'email');
        $this->createIndex('is_lm', '{{%autovoronka}}', 'is_lm');
        $this->createIndex('created', '{{%autovoronka}}', 'created');
        $this->createIndex('repeat', '{{%autovoronka}}', 'repeat');
        $this->createIndex('last_mail', '{{%autovoronka}}', 'last_mail');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%autovoronka}}');
    }
}
