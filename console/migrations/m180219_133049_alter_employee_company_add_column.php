<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180219_133049_alter_employee_company_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('employee_company', 'can_invoice_add_flow', $this->boolean()->defaultValue(false)->after('can_sign'));
    }

    public function safeDown()
    {
        $this->dropColumn('employee_company', 'can_invoice_add_flow');
    }
}
