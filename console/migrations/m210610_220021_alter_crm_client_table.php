<?php

use yii\db\Migration;

class m210610_220021_alter_crm_client_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%crm_client}}';

    /**
     * @inheritDoc
     */
    public function up(): void
    {
        $this->dropForeignKey('fk_crm_client__crm_client_position_client_position_id', self::TABLE_NAME);
        $this->dropForeignKey('fk_crm_client__crm_client_event_client_event_id', self::TABLE_NAME);
        $this->dropColumn(self::TABLE_NAME, 'client_position_id');
        $this->dropColumn(self::TABLE_NAME, 'client_event_id');
        $this->dropColumn(self::TABLE_NAME, 'messenger_type');
        $this->dropColumn(self::TABLE_NAME, 'skype_account');
        $this->dropColumn(self::TABLE_NAME, 'additional_phone');
    }

    /**
     * @inheritDoc
     */
    public function down(): bool
    {
        return false;
    }
}
