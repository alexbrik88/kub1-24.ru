<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201125_093908_update_upd_orders_sum extends Migration
{
    public function safeUp()
    {
        $this->execute('
            UPDATE `upd` 
            JOIN (
                SELECT ou.upd_id, SUM(o.purchase_price_with_vat * ou.quantity) AS summ
                FROM `order_upd` ou
                LEFT JOIN `order` `o` ON ou.order_id = o.id
                GROUP BY upd_id
            ) t ON t.upd_id = upd.id
            SET `upd`.`orders_sum` = t.summ
            WHERE upd.type = 1
        ');

        $this->execute('
            UPDATE `upd` 
            JOIN (
                SELECT ou.upd_id, SUM(o.selling_price_with_vat * ou.quantity) AS summ
                FROM `order_upd` ou
                LEFT JOIN `order` `o` ON ou.order_id = o.id
                GROUP BY upd_id
            ) t ON t.upd_id = upd.id
            SET `upd`.`orders_sum` = t.summ
            WHERE upd.type = 2
        ');
    }

    public function safeDown()
    {
        // no down
    }
}
