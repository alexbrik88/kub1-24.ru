<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200624_134517_alter_contractor_account_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%contractor_account}}', 'bank_address', $this->text()->after('bank_name'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%contractor_account}}', 'bank_address');
    }
}
