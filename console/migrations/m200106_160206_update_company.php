<?php

use console\components\db\Migration;
use yii\db\Query;
use yii\db\Schema;

class m200106_160206_update_company extends Migration
{
    public function safeUp()
    {
        $ids = (new Query())->select('id')->from('{{%service_subscribe_tariff}}')->where(['>', 'price', 0])->column();
        $ids = implode(',', $ids);

        $this->execute("
            UPDATE {{%company}}
            LEFT JOIN {{%service_subscribe}}
                ON {{%service_subscribe}}.[[company_id]] = {{%company}}.[[id]]
                AND {{%service_subscribe}}.[[status_id]] IN (2,3)
                AND {{%service_subscribe}}.[[tariff_id]] IN ({$ids})
            SET {{%company}}.[[login_redirect_times]] = 5,
                {{%company}}.[[login_redirect_url]] = '/tax/robot/index'
            WHERE {{%company}}.[[company_type_id]] = 1
            AND {{%company}}.[[registration_page_type_id]] IN (10,15,17,18,22,24)
            AND {{%service_subscribe}}.[[id]] IS NULL
        ");
    }

    public function safeDown()
    {
        $this->execute("
            UPDATE {{%company}}
            SET {{%company}}.[[login_redirect_times]] = 0
            WHERE {{%company}}.[[login_redirect_times]] > 0
        ");
        $this->execute("
            UPDATE {{%company}}
            SET {{%company}}.[[login_redirect_url]] = NULL
            WHERE {{%company}}.[[login_redirect_url]] IS NOT NULL
        ");
    }
}
