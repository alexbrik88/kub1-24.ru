<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200826_120006_alter_one_s_autoload_email_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('one_s_autoload_email', 'is_exit_by_error', $this->tinyInteger()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('one_s_autoload_email', 'is_exit_by_error');
    }
}
