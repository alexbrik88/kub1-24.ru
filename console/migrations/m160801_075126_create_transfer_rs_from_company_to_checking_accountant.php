<?php

use yii\db\Migration;
use common\models\Company;

/**
 * Handles the creation for table `transfer_rs_from_company_to_checking_accountant`.
 */
class m160801_075126_create_transfer_rs_from_company_to_checking_accountant extends Migration
{
    public $tCheckingAccountant = 'checking_accountant';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $companies = (new \yii\db\Query())->select([
            'id',
            'bank_name',
            'bik',
            'ks',
            'rs',
        ])->from('company')->all();

        foreach ($companies as $company) {
            /* @var Company $company */
            $this->insert($this->tCheckingAccountant, [
                'created_at' => time(),
                'updated_at' => time(),
                'company_id' => $company['id'],
                'bank_name' => $company['bank_name'],
                'bik' => $company['bik'],
                'ks' => $company['ks'],
                'rs' => $company['rs'],
                'type' => 0,
            ]);
        }
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        echo 'This migration not rolled back';
    }
}
