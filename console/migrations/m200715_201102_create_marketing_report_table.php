<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%marketing_report}}`.
 */
class m200715_201102_create_marketing_report_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%marketing_report}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'source_type' => $this->tinyInteger()->notNull(),
            'date' => $this->date()->notNull(),
            'utm_source' => $this->string()->notNull(),
            'utm_medium' => $this->string()->notNull(),
            'utm_campaign' => $this->string()->notNull(),
            'cost' => $this->double()->defaultValue(0),
            'clicks' => $this->integer()->defaultValue(0),
            'impressions' => $this->integer()->defaultValue(0),
            'google_analytics_leads' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%marketing_report}}');
    }
}
