<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190409_164620_add_capital_to_company extends Migration
{
    public $tableName = '{{%company}}';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'capital', $this->decimal(38,2)->defaultValue(null));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'capital');
    }
}
