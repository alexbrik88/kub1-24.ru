<?php

use console\components\db\Migration;
use yii\db\Schema;

class m220224_085141_update_in_order_document_prices extends Migration
{
    public function safeUp()
    {
        $this->execute("
            UPDATE `order_document_product` odp 
            LEFT JOIN `order_document` o ON o.id = odp.order_document_id
            SET
                odp.purchase_price_no_vat = odp.selling_price_no_vat,
                odp.purchase_price_with_vat = odp.selling_price_with_vat,
                odp.amount_purchase_no_vat = odp.amount_sales_no_vat,
                odp.amount_purchase_with_vat = odp.amount_sales_with_vat,
                odp.purchase_tax = odp.sale_tax,
                odp.purchase_tax_rate_id = odp.sale_tax_rate_id
            WHERE o.`type` = 1;
        ");
    }

    public function safeDown()
    {
        // no down
    }
}
