<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200321_153251_create_table_plan_cash_rule extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('plan_cash_rule', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'flow_type' => $this->tinyInteger()->notNull(),
            'income_item_id' => $this->integer(),
            'expenditure_item_id' => $this->integer(),
            'created_at' => $this->integer()->unsigned()->notNull(),
            'updated_at' => $this->integer()->unsigned()->notNull(),
        ], $tableOptions);

        $this->addForeignKey('FK_plan_cash_rule_to_company', 'plan_cash_rule', 'company_id', 'company', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_plan_cash_rule_to_company', 'plan_cash_rule');
        $this->dropTable('plan_cash_rule');
    }
}
