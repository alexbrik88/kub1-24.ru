<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201015_122909_drop_all_olap_flows_triggers extends Migration
{
    public function safeUp()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_bank_flows`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_update_cash_bank_flows`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_delete_cash_bank_flows`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_order_flows`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_update_cash_order_flows`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_delete_cash_order_flows`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_emoney_flows`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_update_cash_emoney_flows`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_delete_cash_emoney_flows`');
        $this->execute('DROP PROCEDURE IF EXISTS `OlapFlowsRecalculateBankByFlow`');
        $this->execute('DROP PROCEDURE IF EXISTS `OlapFlowsRecalculateOrderByFlow`');
        $this->execute('DROP PROCEDURE IF EXISTS `OlapFlowsRecalculateEmoneyByFlow`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_bank_flow_to_invoice`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_delete_cash_bank_flow_to_invoice`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_order_flow_to_invoice`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_delete_cash_order_flow_to_invoice`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_insert_cash_emoney_flow_to_invoice`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_delete_cash_emoney_flow_to_invoice`');
        $this->execute('DROP PROCEDURE IF EXISTS `OlapFlowsRecalculateBankByInvoice`');
        $this->execute('DROP PROCEDURE IF EXISTS `OlapFlowsRecalculateEmoneyByInvoice`');
        $this->execute('DROP PROCEDURE IF EXISTS `OlapFlowsRecalculateOrderByInvoice`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_update_invoice`');
        $this->execute('DROP PROCEDURE IF EXISTS `OlapFlowsRecalculateBankByProduct`');
        $this->execute('DROP PROCEDURE IF EXISTS `OlapFlowsRecalculateOrderByProduct`');
        $this->execute('DROP PROCEDURE IF EXISTS `OlapFlowsRecalculateEmoneyByProduct`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_flows_update_product`');
    }

    public function safeDown()
    {

    }
}
