<?php

use console\components\db\Migration;
use yii\db\Query;
use common\components\helpers\ArrayHelper;

class m200713_141619_update_rent_attribute_value extends Migration
{
    const CATEGORY_REALTY = 1;
    const CATEGORY_TECHNIC = 2;
    const CATEGORY_EQUIPMENT = 3;

    public $table = 'rent_attribute_value';

    public function safeUp()
    {
        $entities = (new Query())->from('rent_entity')->select(['id', 'category_id'])->all();
        $attributes = [
            self::CATEGORY_REALTY => (new Query())->from('rent_attribute')->select(['id', 'default'])
                ->where(['alias' => ['unit', 'paymentPeriod', 'price', 'currency'], 'category_id' => self::CATEGORY_REALTY])->all(),
            self::CATEGORY_TECHNIC => (new Query())->from('rent_attribute')->select(['id', 'default'])
                ->where(['alias' => ['unit', 'paymentPeriod', 'price', 'currency'], 'category_id' => self::CATEGORY_TECHNIC])->all(),
            self::CATEGORY_EQUIPMENT => (new Query())->from('rent_attribute')->select(['id', 'default'])
                ->where(['alias' => ['unit', 'paymentPeriod', 'price', 'currency'], 'category_id' => self::CATEGORY_EQUIPMENT])->all(),
        ];

        $attributesIds = (new Query())->from('rent_attribute')->where(['alias' => ['unit', 'paymentPeriod', 'price', 'currency']])
            ->select(['id'])->column();
        $this->delete($this->table, ['attribute_id' => $attributesIds]);

        $batch = [];
        foreach ($entities as $entity) {
            foreach ($attributes[$entity['category_id']] as $attribute) {
                $batch[] = [$entity['id'], $attribute['id'], $attribute['default'] ?? ''];
            }
        }

        $this->batchInsert($this->table, ['entity_id', 'attribute_id', 'value'], $batch);
    }

    public function safeDown()
    {
        $attributesIds = (new Query())->from('rent_attribute')->where(['alias' => ['unit', 'paymentPeriod', 'price', 'currency']])
            ->select(['id'])->column();

        $this->delete($this->table, ['attribute_id' => $attributesIds]);
    }
}
