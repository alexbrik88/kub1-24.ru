<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%packing_list_goods_cancellation}}`.
 */
class m220419_110110_create_packing_list_goods_cancellation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%packing_list_goods_cancellation}}', [
            'document_id' => $this->integer()->notNull(),
            'cancellation_id' => $this->integer()->notNull(),
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', '{{%packing_list_goods_cancellation}}', [
            'document_id',
            'cancellation_id',
        ]);

        $this->addForeignKey('fk_packing_list_goods_cancellation__document_id',
            '{{%packing_list_goods_cancellation}}',
            'document_id',
            '{{%packing_list}}',
            'id'
        );

        $this->addForeignKey('fk_packing_list_goods_cancellation__cancellation_id',
            '{{%packing_list_goods_cancellation}}',
            'cancellation_id',
            '{{%goods_cancellation}}',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%packing_list_goods_cancellation}}');
    }
}
