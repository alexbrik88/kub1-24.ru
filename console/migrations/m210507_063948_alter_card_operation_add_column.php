<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210507_063948_alter_card_operation_add_column extends Migration
{
    public $tableName = 'card_operation';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'project_id', $this->integer());
        $this->addForeignKey('fk_card_operation_project_id__project', $this->tableName, 'project_id', 'project', 'id', 'SET NULL');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_card_operation_project_id__project', $this->tableName);
        $this->dropColumn($this->tableName, 'project_id');
    }
}
