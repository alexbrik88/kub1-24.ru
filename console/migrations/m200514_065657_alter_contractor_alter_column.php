<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200514_065657_alter_contractor_alter_column extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%contractor}}', 'director_name',  $this->string()->notNull()->defaultValue(""));
        $this->alterColumn('{{%contractor}}', 'director_email',  $this->string());
        $this->alterColumn('{{%contractor}}', 'chief_accountant_is_director',  $this->boolean()->notNull()->defaultValue(false));
        $this->alterColumn('{{%contractor}}', 'chief_accountant_email',  $this->string());
        $this->alterColumn('{{%contractor}}', 'contact_is_director',  $this->boolean()->notNull()->defaultValue(false));
        $this->alterColumn('{{%contractor}}', 'contact_name',  $this->string());
        $this->alterColumn('{{%contractor}}', 'contact_email',  $this->string());
    }

    public function down()
    {
        $this->alterColumn('{{%contractor}}', 'director_name',  $this->string(45)->notNull());
        $this->alterColumn('{{%contractor}}', 'director_email',  $this->string(45));
        $this->alterColumn('{{%contractor}}', 'chief_accountant_is_director',  $this->boolean()->notNull());
        $this->alterColumn('{{%contractor}}', 'chief_accountant_email',  $this->string(45));
        $this->alterColumn('{{%contractor}}', 'contact_is_director',  $this->boolean()->notNull());
        $this->alterColumn('{{%contractor}}', 'contact_name',  $this->string(45));
        $this->alterColumn('{{%contractor}}', 'contact_email',  $this->string(45));
    }
}
