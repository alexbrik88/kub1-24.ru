<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181104_064005_alter_table_waybill_change_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%waybill}}', 'taxation', $this->string(200));
    }
    
    public function safeDown()
    {
        $this->alterColumn('{{%waybill}}', 'taxation', $this->string(50));
    }
}


