<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210608_115143_insert_registration_page_type extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%registration_page_type}}', [
            'id' => 41,
            'name' => 'AmoCRM',
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%registration_page_type}}', [
            'id' => 41,
        ]);
    }
}
