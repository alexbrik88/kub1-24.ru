<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181218_132731_alter_company_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('company', 'nds_view_type_id', $this->integer()->notNull()->defaultValue(2)->after('nds'));

        $this->addForeignKey('FK_company_ndsViewType', 'company', 'nds_view_type_id', 'nds_view_type', 'id');

        $this->execute('
            UPDATE {{company}}
            LEFT JOIN {{company_taxation_type}} ON {{company_taxation_type}}.[[company_id]] = {{company}}.[[id]]
            SET {{company}}.[[nds_view_type_id]] = CASE
                WHEN {{company}}.[[nds]] = 1 THEN 0
                WHEN {{company}}.[[nds]] = 2 THEN 1
            END
            WHERE {{company_taxation_type}}.[[osno]] = 1
            AND {{company}}.[[nds]] IS NOT NULL
        ');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_company_ndsViewType', 'company');

        $this->dropColumn('company', 'nds_view_type_id');
    }
}
