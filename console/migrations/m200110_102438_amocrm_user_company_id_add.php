<?php

use console\components\db\Migration;

class m200110_102438_amocrm_user_company_id_add extends Migration
{
    const TABLE = '{{%amocrm_user}}';

    public function up()
    {
        $this->addColumn(self::TABLE, 'company_id', $this->integer(11)->notNull()->after('user_id')->comment('Идентификатор компании КУБ'));
    }

    public function down()
    {
        $this->dropColumn(self::TABLE, 'company_id');
    }
}
