<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190213_204250_insert_contractor_account extends Migration
{
    public function safeUp()
    {
        $this->execute('
            INSERT INTO {{contractor_account}} (
              [[contractor_id]],
              [[rs]],
              [[bank_name]],
              [[bik]],
              [[ks]],
              [[is_main]],
              [[created_at]])
            SELECT {{contractor}}.[[id]],
                   {{contractor}}.[[current_account]],
                   {{contractor}}.[[bank_name]],
                   {{contractor}}.[[BIC]],
                   {{contractor}}.[[corresp_account]],
                   1,
                   {{contractor}}.[[created_at]]
            FROM {{contractor}}
            WHERE TRIM(IFNULL({{contractor}}.[[current_account]],"")) <> ""
            AND TRIM(IFNULL({{contractor}}.[[bank_name]],"")) <> ""
            AND TRIM(IFNULL({{contractor}}.[[BIC]],"")) <> ""
        ');
    }

    public function safeDown()
    {
        $this->truncateTable('contractor_account');
    }
}
