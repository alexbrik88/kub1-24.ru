<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211119_111153_order_document_in_to_out extends Migration
{
    public $table = 'order_document_in_to_out';

    public function safeUp()
    {
        $table = $this->table;

        $this->createTable($table, [
            'id' => $this->primaryKey(),
            'out_order_document' => $this->integer()->notNull(),
            'out_order_document_product' => $this->integer()->notNull(),
            'in_order_document' => $this->integer()->notNull(),
            'in_order_document_product' => $this->integer()->notNull()
        ]);

        $this->addForeignKey("FK_{$table}_out_order_document", $table, 'out_order_document', 'order_document', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("FK_{$table}_out_order_document_product", $table, 'out_order_document_product', 'order_document_product', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("FK_{$table}_in_order_document", $table, 'in_order_document', 'order_document', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("FK_{$table}_in_order_document_product", $table, 'in_order_document_product', 'order_document_product', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $table = $this->table;

        $this->dropTable($table);
    }
}
