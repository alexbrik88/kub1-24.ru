<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200526_082318_foreign_currency_account_table extends Migration
{
    /**
     * @return bool|void
     */
    public function safeUp()
    {
        $this->createTable('foreign_currency_account', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer(),
            'currency_id' => $this->char(3),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'type' => $this->integer()->notNull(),

            'rs' => $this->string(50),
            'swift' => $this->string(255),
            'bank_name' => $this->string(255),
            'bank_address' => $this->text(),

            'corr_rs' => $this->string(50),
            'corr_swift' => $this->string(255),
            'corr_bank_name' => $this->string(255),
            'corr_bank_address' => $this->text(),
        ]);

        $this->addForeignKey(
            'foreign_currency_account_company_fk',
            'foreign_currency_account',
            'company_id',
            'company',
            'id',
            'CASCADE'
        );

        $this->addCommentOnTable('foreign_currency_account', 'Таблица валютных расчетных счетов');
    }

    /**
     * @return bool|void
     */
    public function safeDown()
    {
        $this->dropTable('foreign_currency_account');
    }
}
