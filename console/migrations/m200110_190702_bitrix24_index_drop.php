<?php

use console\components\db\Migration;

class m200110_190702_bitrix24_index_drop extends Migration
{
    public function safeUp()
    {
        $this->_dropIndex('contact', 'company_id');
        $this->_dropIndex('deal', 'company_id');
        $this->_dropIndex('deal', 'contact_id');
        $this->_dropIndex('deal_position', 'deal_id');
        $this->_dropIndex('deal_position', 'product_id');
        $this->_dropIndex('invoice', 'deal_id');
        $this->_dropIndex('invoice', 'company_id');
        $this->_dropIndex('invoice', 'contact_id');
        $this->_dropIndex('product', 'section_id');
        $this->_dropIndex('product', 'vat_id');
        $this->_dropIndex('section', 'catalog_id');
        $this->_dropIndex('section', 'parent_id');
    }

    public function safeDown()
    {
        $this->_createIndex('contact', 'company_id');
        $this->_createIndex('deal', 'company_id');
        $this->_createIndex('deal', 'contact_id');
        $this->_createIndex('deal_position', 'deal_id');
        $this->_createIndex('deal_position', 'product_id');
        $this->_createIndex('invoice', 'deal_id');
        $this->_createIndex('invoice', 'company_id');
        $this->_createIndex('invoice', 'contact_id');
        $this->_createIndex('product', 'section_id');
        $this->_createIndex('product', 'vat_id');
        $this->_createIndex('section', 'catalog_id');
        $this->_createIndex('section', 'parent_id');
    }

    private function _dropIndex(string $table, string $column)
    {
        $this->dropIndex('bitrix24_' . $table . '_' . $column, self::tableName($table));
    }

    private function _createIndex(string $table, string $column)
    {
        $this->createIndex('bitrix24_' . $table . '_' . $column, self::tableName($table), ['employee_id', $column]);
    }

    protected static function tableName(string $table, bool $local = true): string
    {
        if ($local === true) {
            $table = 'bitrix24_' . $table;
        }
        return '{{%' . $table . '}}';
    }

}
