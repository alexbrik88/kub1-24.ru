<?php

use yii\db\Migration;

class m210325_005601_rename_autoload_job_table extends Migration
{
    /**
     * @inheritDoc
     */
    public function up()
    {
        $this->renameTable('{{%autoload_job}}', '{{%auto_import_settings}}');
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        $this->renameTable('{{%auto_import_settings}}', '{{%autoload_job}}');
    }
}
