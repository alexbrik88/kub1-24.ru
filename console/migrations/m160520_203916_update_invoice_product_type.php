<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160520_203916_update_invoice_product_type extends Migration
{
    public $tInvoice = 'invoice';

    public function safeUp()
    {
        $this->alterColumn($this->tInvoice, 'production_type', $this->string()->notNull());
    }
    
    public function safeDown()
    {
        $this->alterColumn($this->tInvoice, 'production_type', $this->boolean());
    }
}


