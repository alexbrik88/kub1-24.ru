<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211104_203056_update_packing_list_order_nds extends Migration
{
    public function safeUp()
    {
        $this->execute('
            UPDATE `packing_list` 
            JOIN (
                SELECT ou.packing_list_id, SUM((o.purchase_price_with_vat - o.purchase_price_no_vat) * ou.quantity) AS summ
                FROM `order_packing_list` ou
                LEFT JOIN `order` `o` ON ou.order_id = o.id
                GROUP BY packing_list_id
            ) t ON t.packing_list_id = packing_list.id
            SET `packing_list`.`order_nds` = t.summ
            WHERE packing_list.type = 1
        ');

        $this->execute('
            UPDATE `packing_list` 
            JOIN (
                SELECT ou.packing_list_id, SUM((o.selling_price_with_vat - o.selling_price_no_vat) * ou.quantity) AS summ
                FROM `order_packing_list` ou
                LEFT JOIN `order` `o` ON ou.order_id = o.id
                GROUP BY packing_list_id
            ) t ON t.packing_list_id = packing_list.id
            SET `packing_list`.`order_nds` = t.summ
            WHERE packing_list.type = 2
        ');
    }

    public function safeDown()
    {
        // no down
    }
}
