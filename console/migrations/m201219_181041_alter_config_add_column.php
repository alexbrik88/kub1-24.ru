<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201219_181041_alter_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_config','cash_operations_help',$this->tinyInteger()->notNull()->defaultValue(0));
        $this->addColumn('user_config','cash_operations_chart',$this->tinyInteger()->notNull()->defaultValue(0));
        $this->addColumn('user_config','cash_operations_chart_period',$this->tinyInteger()->notNull()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('user_config','cash_operations_help');
        $this->dropColumn('user_config','cash_operations_chart');
        $this->dropColumn('user_config','cash_operations_chart_period');
    }
}
