<?php

use yii\db\Migration;

class m210507_055542_create_crm_client_deal_stage_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%crm_client_deal_stage}}';

    /**
     * @inheritDoc
     */
    public function up(): void
    {
        $this->createTable(self::TABLE_NAME, [
            'client_deal_stage_id' => $this->bigPrimaryKey()->notNull(),
            'client_deal_id' => $this->bigInteger()->notNull(),
            'order' => $this->bigInteger()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'employee_id' => $this->integer()->null(),
            'name' => $this->string(64)->notNull(),
            'status' => $this->boolean()->notNull()->defaultValue(true),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->createIndex('ck_client_deal_id', self::TABLE_NAME, 'client_deal_id');
        $this->addForeignKey(
            'fk_crm_client_deal_stage__crm_client_deal',
            self::TABLE_NAME,
            ['client_deal_id'],
            '{{%crm_client_deal}}',
            ['client_deal_id'],
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex('ck_company_id', self::TABLE_NAME, 'company_id');
        $this->addForeignKey(
            'fk_crm_client_deal_stage__company',
            self::TABLE_NAME,
            ['company_id'],
            '{{%company}}',
            ['id'],
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex('ck_company_id_employee_id', self::TABLE_NAME, ['company_id', 'employee_id']);
        $this->addForeignKey(
            'fk_crm_client_deal_stage__employee_company',
            self::TABLE_NAME,
            ['company_id', 'employee_id'],
            '{{%employee_company}}',
            ['company_id', 'employee_id'],
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex('ck_order', self::TABLE_NAME, ['order']);
    }

    /**
     * @inheritDoc
     */
    public function down(): void
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
