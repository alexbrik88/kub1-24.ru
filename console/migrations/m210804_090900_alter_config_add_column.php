<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210804_090900_alter_config_add_column extends Migration
{
    public $table = 'user_config';
    public $columns = [
        // invoice
        'invoice_sale_point',
        'invoice_industry',
        'invoice_project',
        // act
        'act_sale_point',
        'act_industry',
        'act_project',
        // packing-list
        'packing_list_sale_point',
        'packing_list_industry',
        'packing_list_project',
        // upd
        'upd_sale_point',
        'upd_industry',
        'upd_project',
        // invoice-facture
        'invoice_facture_sale_point',
        'invoice_facture_industry',
        'invoice_facture_project',
    ];

    public function safeUp()
    {
        foreach ($this->columns as $column)
            $this->addColumn($this->table, $column, $this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        foreach ($this->columns as $column)
            $this->dropColumn($this->table, $column);
    }
}
