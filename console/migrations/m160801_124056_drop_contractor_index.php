<?php

use yii\db\Migration;

/**
 * Handles the dropping for table `contractor_index`.
 */
class m160801_124056_drop_contractor_index extends Migration
{
    public $tTables = [
        'cash_bank_flows',
        'cash_order_flows',
        'cash_emoney_flows',
    ];

    /**
     * @inheritdoc
     */
    public function up()
    {
        foreach ($this->tTables as $tableName) {
            $this->dropForeignKey($tableName . '_contractor_id', $tableName);
            $this->alterColumn($tableName, 'contractor_id', $this->string()->notNull());
        }
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        echo 'This migration not rolled back';
    }
}
