<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210909_152819_alter_proxy_column_collate extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('proxy', 'document_number', $this->string()->notNull() . " COLLATE 'utf8_general_ci'");
    }

    public function safeDown()
    {
        $this->alterColumn('proxy', 'document_number', $this->string()->notNull() . " COLLATE 'utf8_unicode_ci'");
    }
}
