<?php

use yii\db\Schema;
use yii\db\Migration;

class m150617_110206_delete_ProductUnit extends Migration
{

    public function safeUp()
    {
        $this->update('product', [
            'product_unit_id' => 1,
        ], 'product_unit_id = 9'); // UNIT_WITHOUT
        $this->update('order', [
            'unit_id' => 1,
        ], 'unit_id = 9'); // UNIT_WITHOUT
        $this->delete('product_unit', 'id = 9');
    }
    
    public function safeDown()
    {
        $this->insert('product_unit', [
            'id' => 9,
            'name' => '---',
        ]);

        echo 'Returning of product unit with id=9 don\'t returns it in products and orders.', PHP_EOL;
    }
}
