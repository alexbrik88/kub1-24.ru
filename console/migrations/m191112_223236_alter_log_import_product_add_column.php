<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191112_223236_alter_log_import_product_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%log_import_product}}', 'completed_at', $this->integer()->after('created_at'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%log_import_product}}', 'completed_at');
    }
}
