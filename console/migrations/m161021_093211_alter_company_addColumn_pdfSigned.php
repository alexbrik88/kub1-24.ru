<?php

use console\components\db\Migration;
use yii\db\Schema;

class m161021_093211_alter_company_addColumn_pdfSigned extends Migration
{
    public $tCompany = '{{%company}}';

    public function safeUp()
    {
        $this->addColumn($this->tCompany, 'pdf_signed', 'tinyint(1) NOT NULL DEFAULT 0');
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tCompany, 'pdf_signed');
    }
}


