<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200621_193425_update_google_analytics_profile extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('{{%google_analytics_profile}}', 'lead_name');
        $this->dropColumn('{{%google_analytics_profile}}', 'plan_lead_cost');
    }

    public function safeDown()
    {
        $this->addColumn('{{%google_analytics_profile}}', 'lead_name', $this->string()->defaultValue(null));
        $this->addColumn('{{%google_analytics_profile}}', 'plan_lead_cost', $this->integer(11)->unsigned()->defaultValue(null));
    }
}
