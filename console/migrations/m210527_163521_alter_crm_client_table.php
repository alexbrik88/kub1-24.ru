<?php

use yii\db\Migration;

class m210527_163521_alter_crm_client_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%crm_client}}';

    /**
     * @inheritDoc
     */
    public function up(): void
    {
        $this->dropColumn(self::TABLE_NAME, 'client_id');
        $this->addPrimaryKey('pk_contractor_id', self::TABLE_NAME, ['contractor_id']);
    }
}
