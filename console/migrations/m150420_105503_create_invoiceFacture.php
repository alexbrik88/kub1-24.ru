<?php

use yii\db\Schema;
use yii\db\Migration;

class m150420_105503_create_invoiceFacture extends Migration
{
    public function safeUp()
    {
        $this->createTable('invoice_facture', [
            'id' => Schema::TYPE_PK,
            'type' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "1 - входящая, 2 - исходящая"',
            'invoice_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Номер счёта"',
            'invoice_facture_status_id' => Schema::TYPE_INTEGER . ' NULL COMMENT "Статус счёт-фактуры. NULL для входящей"',
            'invoice_facture_status_change_date' => Schema::TYPE_DATETIME . ' NULL COMMENT "Дата изменения статуса счёт-фактуры"',
            'invoice_facture_status_author_id' => Schema::TYPE_INTEGER . ' NULL COMMENT "Автор изменения статуса счёт-фактуры"',
            'invoice_facture_date' => Schema::TYPE_DATETIME . ' NOT NULL COMMENT "Дата создания счёт-фактуры"',
            'invoice_number' => Schema::TYPE_STRING . '(100) COMMENT "Основной номер счёт-фактуры"',
            'additional_invoice_number' => Schema::TYPE_STRING . '(100) NULL COMMENT "Дополнительный номер счёт-фактуры"',
            'has_to_payment_document' => Schema::TYPE_BOOLEAN . ' NULL COMMENT "Наличение \"К платёжно-расчётному документу\" "',
            'to_payment_document' => Schema::TYPE_STRING . '(100) NULL COMMENT "К платёжно-расчётному документу. Наличие = 0, то значение \"---\" "',
        ]);

        $this->createTable('invoice_facture_status', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . '(45) NOT NULL',
        ]);
        $this->batchInsert('invoice_facture_status', ['id', 'name'], [
            [1, 'Создан'],
            [2, 'Распечатан'],
            [3, 'Передан'],
        ]);

        $this->addForeignKey('invoice_facture_to_invoice_facture_status', 'invoice_facture', 'invoice_facture_status_id', 'invoice_facture_status', 'id');
        $this->addForeignKey('invoice_facture_to_invoice', 'invoice_facture', 'invoice_id', 'invoice', 'id');
    }
    
    public function safeDown()
    {
        $this->dropTable('invoice_facture');
        $this->dropTable('invoice_facture_status');
    }
}
