<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201125_091135_alter_invoice_append_column_project_estimate_id extends Migration
{
    public function safeUp()
    {
        $this->addColumn('invoice', 'project_estimate_id', $this->integer(11)->after('project_id'));
    }

    public function safeDown()
    {
        $this->dropColumn('invoice', 'project_estimate_id');
    }
}
