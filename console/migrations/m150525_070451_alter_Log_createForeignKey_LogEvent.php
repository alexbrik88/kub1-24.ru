<?php

use yii\db\Schema;
use yii\db\Migration;

class m150525_070451_alter_Log_createForeignKey_LogEvent extends Migration
{
    public function safeUp()
    {
        $this->addColumn('log', 'log_event_id', Schema::TYPE_INTEGER . ' NOT NULL AFTER log_entity_type_id');
        $this->addForeignKey('fk_log_to_log_event', 'log', 'log_event_id', 'log_event', 'id');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('fk_log_to_log_event', 'log');
        $this->dropColumn('log', 'log_event_id');
    }
}
