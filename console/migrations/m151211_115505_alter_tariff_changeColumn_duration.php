<?php

use yii\db\Schema;
use yii\db\Migration;

class m151211_115505_alter_tariff_changeColumn_duration extends Migration
{
    public $tTariff = 'service_subscribe_tariff';

    public function safeUp()
    {
        $this->addColumn($this->tTariff, 'duration_month', $this->integer()->notNull()->defaultValue(0) . ' AFTER duration');
        $this->addColumn($this->tTariff, 'duration_day', $this->integer()->notNull()->defaultValue(0) . ' AFTER duration_month');

        $this->update($this->tTariff, [
            'duration_month' => new \yii\db\Expression('duration'),
        ]);
        $this->dropColumn($this->tTariff, 'duration');
    }

    public function safeDown()
    {
        $this->addColumn($this->tTariff, 'duration', $this->integer()->notNull()->defaultValue(0) . ' AFTER id');
        $this->update($this->tTariff, [
            'duration' => new \yii\db\Expression('duration_month'),
        ]);

        $this->dropColumn($this->tTariff, 'duration_month');
        $this->dropColumn($this->tTariff, 'duration_day');
    }
}
