<?php

use yii\db\Schema;
use yii\db\Migration;

class m150511_071738_create_payment_type extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('payment_type', [
            'id' => Schema::TYPE_PK,
            'code' => Schema::TYPE_STRING,
            'name' => Schema::TYPE_TEXT,
        ], $tableOptions);

        $this->batchInsert('payment_type', ['id', 'code', 'name'], [
            [1, '0', ''],
            [2, 'ПЕ', 'уплата пени'],
            [3, 'ПЦ', 'уплата процентов'],
        ]);
    }
    
    public function safeDown()
    {
        $this->dropTable('payment_type');
    }
}
