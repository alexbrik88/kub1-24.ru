<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190410_111800_add_prepaid_expense_to_bank_and_order_flows extends Migration
{
    public $tCashBankFlows = 'cash_bank_flows';
    public $tCashOrderFlows = 'cash_order_flows';

    public function safeUp()
    {
        $this->addColumn($this->tCashBankFlows, 'is_prepaid_expense',  $this->boolean()->defaultValue(false));
        $this->alterColumn($this->tCashBankFlows, 'recognition_date', $this->date()->defaultValue(null));

        $this->addColumn($this->tCashOrderFlows, 'is_prepaid_expense',  $this->boolean()->defaultValue(false));
        $this->alterColumn($this->tCashOrderFlows, 'recognition_date', $this->date()->defaultValue(null));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tCashBankFlows, 'is_prepaid_expense');
        $this->alterColumn($this->tCashBankFlows, 'recognition_date', $this->date()->notNull());

        $this->dropColumn($this->tCashOrderFlows, 'is_prepaid_expense');
        $this->alterColumn($this->tCashOrderFlows, 'recognition_date', $this->date()->notNull());
    }
}
