<?php

use yii\db\Schema;
use yii\db\Migration;

class m151126_123508_update_dik_dictionary extends Migration
{
    public $tBik = 'bik_dictionary';


    public function safeUp()
    {
        $this->addColumn($this->tBik, 'is_active', $this->boolean()->notNull() . ' AFTER bik');
        $this->update($this->tBik, ['is_active' => true]);
        $this->dropIndex('bik_u_idx', $this->tBik);
        $this->createIndex('bik_u_idx', $this->tBik, ['is_active', 'bik',], true);
    }

    public function safeDown()
    {
        $this->dropIndex('bik_u_idx', $this->tBik);
        $this->dropColumn($this->tBik, 'is_active');
        $this->createIndex('bik_u_idx', 'bik_dictionary', 'bik', true);
    }
}
