<?php

use console\components\db\Migration;
use yii\db\Schema;

class m161227_074623_create_statistic_range_field_in_employee extends Migration
{
    public $tEmployee = 'employee';

    public function safeUp()
    {
        $this->addColumn($this->tEmployee, 'statistic_range_date_from', $this->string());
        $this->addColumn($this->tEmployee, 'statistic_range_date_to', $this->string());
        $this->addColumn($this->tEmployee, 'statistic_range_name', $this->string());
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tEmployee, 'statistic_range_date_from', $this->string());
        $this->dropColumn($this->tEmployee, 'statistic_range_date_to', $this->string());
        $this->dropColumn($this->tEmployee, 'statistic_range_name', $this->string());
    }
}


