<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190311_100139_update_service_subscribe_tariff extends Migration
{
    public function safeUp()
    {
        $this->execute('
            UPDATE {{service_subscribe_tariff}}
            SET {{service_subscribe_tariff}}.[[proposition]] = REPLACE(
                {{service_subscribe_tariff}}.[[proposition]], "Скидка", "Получи скидку"
            )
            WHERE {{service_subscribe_tariff}}.[[proposition]] LIKE "%Скидка%"
        ');
    }

    public function safeDown()
    {
        $this->execute('
            UPDATE {{service_subscribe_tariff}}
            SET {{service_subscribe_tariff}}.[[proposition]] = REPLACE(
                {{service_subscribe_tariff}}.[[proposition]], "Получи скидку", "Скидка"
            )
            WHERE {{service_subscribe_tariff}}.[[proposition]] LIKE "%Получи скидку%"
        ');
    }
}
