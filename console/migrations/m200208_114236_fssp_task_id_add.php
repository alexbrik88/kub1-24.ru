<?php

use console\components\db\Migration;

class m200208_114236_fssp_task_id_add extends Migration
{

    const TABLE = '{{%fssp_task}}';

    public function safeUp()
    {
        $this->addColumn(self::TABLE, 'id', $this->primaryKey()->unsigned()->notNull()->first());
    }

    public function down()
    {
        $this->dropColumn(self::TABLE, 'id');
    }
}
