<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180906_204155_add_contractor_id_to_user_email extends Migration
{
    public $tableName = 'user_email';

    public function safeUp()
    {
        $this->truncateTable($this->tableName);
        $this->addColumn($this->tableName, 'contractor_id', $this->integer()->notNull());
        $this->addForeignKey($this->tableName . '_contractor_id', $this->tableName, 'contractor_id', 'contractor', 'id', 'RESTRICT', 'CASCADE');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_contractor_id', $this->tableName);
        $this->dropColumn($this->tableName, 'contractor_id');
    }
}


