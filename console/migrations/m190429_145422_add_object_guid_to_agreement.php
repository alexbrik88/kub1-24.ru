<?php

use console\components\db\Migration;
use yii\db\Schema;
use frontend\modules\export\models\one_c\OneCExport;

class m190429_145422_add_object_guid_to_agreement extends Migration
{
    public $tableName = 'agreement';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'object_guid', $this->string(255)->notNull()->defaultValue(''));

        foreach ((new \yii\db\Query())->select(['id'])->from($this->tableName)->orderBy('id')->batch(50) as $contractorIds) {
            foreach ($contractorIds as $contractorId) {
                \Yii::$app->db->createCommand()->update($this->tableName, [
                    'object_guid' => OneCExport::generateGUID()
                ], 'id = :id', [':id' => $contractorId['id']])->execute();
            }
        };
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'object_guid');
    }
}
