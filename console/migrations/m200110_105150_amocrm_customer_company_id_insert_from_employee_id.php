<?php

use common\models\amocrm\Customer;
use common\models\employee\Employee;
use console\components\db\Migration;

class m200110_105150_amocrm_customer_company_id_insert_from_employee_id extends Migration
{
    const TABLE = '{{%amocrm_customer}}';

    public function safeUp()
    {
        foreach (Customer::find()->all() as $customer) {
            /** @var Customer $customer */
            /** @noinspection PhpUndefinedFieldInspection */
            $customer->company_id = Employee::findOne(['id' => $customer->employee_id])->company_id;
            $customer->save();
        }
    }

    public function down()
    {
        $this->update(self::TABLE, ['company_id' => 0]);
    }
}
