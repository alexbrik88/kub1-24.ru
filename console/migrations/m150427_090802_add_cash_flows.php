<?php

use yii\db\mysql\Schema;
use yii\db\Migration;

class m150427_090802_add_cash_flows extends Migration
{
    public function safeUp()
    {
        $schema = new Schema();
        $schema->db = $this->db;
        $this->createTable('cash_bank_flows', [
            'id'                  => Schema::TYPE_PK,
            'company_id'          => $schema->getTableSchema('company')->getColumn('id')->dbType . ' NOT NULL',
            'date'                => Schema::TYPE_DATE . ' NOT NULL',
            'flow_type'           => Schema::TYPE_BOOLEAN . ' NOT NULL COMMENT \'0 - расход, 1 - приход\'',
            'has_invoice'         => Schema::TYPE_BOOLEAN . ' NOT NULL COMMENT \'Связь со счётом: 0 - нет, 1 - да\'',
            'invoice_id'          => $schema->getTableSchema('invoice')->getColumn('id')->dbType,
            'amount'              => Schema::TYPE_INTEGER . ' NOT NULL',
            'description'         => Schema::TYPE_STRING . ' NOT NULL',
            'expenditure_item_id' => $schema->getTableSchema('invoice_expenditure_item')->getColumn('id')->dbType,
            'contractor_id'       => $schema->getTableSchema('contractor')->getColumn('id')->dbType . ' NOT NULL',
            'created_at'          => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);

        $this->addForeignKey('cash_bank_flows_company_id', 'cash_bank_flows', 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('cash_bank_flows_invoice_id', 'cash_bank_flows', 'invoice_id', 'invoice', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('cash_bank_flows_contractor_id', 'cash_bank_flows', 'contractor_id', 'contractor', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('cash_bank_flows_expenditure_item_id', 'cash_bank_flows', 'expenditure_item_id', 'invoice_expenditure_item', 'id', 'RESTRICT', 'CASCADE');

        $this->createTable('cash_emoney_flows', [
            'id'                  => Schema::TYPE_PK,
            'company_id'          => $schema->getTableSchema('company')->getColumn('id')->dbType . ' NOT NULL',
            'date'                => Schema::TYPE_DATE . ' NOT NULL',
            'flow_type'           => Schema::TYPE_BOOLEAN . ' NOT NULL COMMENT \'0 - расход, 1 - приход\'',
            'is_accounting'       => Schema::TYPE_BOOLEAN . ' NOT NULL COMMENT \'Учёт в бухгалтерии: 0 - нет, 1 - да\'',
            'has_invoice'         => Schema::TYPE_BOOLEAN . ' COMMENT \'Связь со счётом: 0 - нет, 1 - да\'',
            'invoice_id'          => $schema->getTableSchema('invoice')->getColumn('id')->dbType,
            'amount'              => Schema::TYPE_INTEGER . ' NOT NULL',
            'description'         => Schema::TYPE_STRING . ' NOT NULL',
            'expenditure_item_id' => $schema->getTableSchema('invoice_expenditure_item')->getColumn('id')->dbType,
            'contractor_id'       => $schema->getTableSchema('contractor')->getColumn('id')->dbType . ' NOT NULL',
            'created_at'          => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);

        $this->addForeignKey('cash_emoney_flows_company_id', 'cash_emoney_flows', 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('cash_emoney_flows_invoice_id', 'cash_emoney_flows', 'invoice_id', 'invoice', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('cash_emoney_flows_contractor_id', 'cash_emoney_flows', 'contractor_id', 'contractor', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('cash_emoney_flows_expenditure_item_id', 'cash_emoney_flows', 'expenditure_item_id', 'invoice_expenditure_item', 'id', 'RESTRICT', 'CASCADE');

        $this->createTable('cash_order_flows', [
            'id'                  => Schema::TYPE_PK,
            'date'                => Schema::TYPE_DATE . ' NOT NULL',
            'author_id'           => $schema->getTableSchema('employee')->getColumn('id')->dbType . ' NOT NULL',
            'company_id'          => $schema->getTableSchema('company')->getColumn('id')->dbType . ' NOT NULL',
            'number'              => Schema::TYPE_STRING . ' NOT NULL',
            'flow_type'           => Schema::TYPE_BOOLEAN . ' NOT NULL COMMENT \'0 - расход, 1 - приход\'',
            'is_accounting'       => Schema::TYPE_BOOLEAN . ' NOT NULL COMMENT \'Учёт в бухгалтерии: 0 - нет, 1 - да\'',
            'has_invoice'         => Schema::TYPE_BOOLEAN . ' COMMENT \'Связь со счётом: 0 - нет, 1 - да\'',
            'invoice_id'          => $schema->getTableSchema('invoice')->getColumn('id')->dbType,
            'contractor_id'       => $schema->getTableSchema('contractor')->getColumn('id')->dbType . ' NOT NULL',
            'amount'              => Schema::TYPE_INTEGER . ' NOT NULL',
            'description'         => Schema::TYPE_STRING,
            'expenditure_item_id' => $schema->getTableSchema('invoice_expenditure_item')->getColumn('id')->dbType,
            'reason_id'           => $schema->getTableSchema('cash_orders_reasons_types')->getColumn('id')->dbType,
            'application'         => Schema::TYPE_STRING,
            'other'               => Schema::TYPE_STRING . ' NOT NULL',
            'created_at'          => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);

        $this->addForeignKey('cash_order_flows_author_id', 'cash_order_flows', 'author_id', 'employee', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('cash_order_flows_company_id', 'cash_order_flows', 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('cash_order_flows_invoice_id', 'cash_order_flows', 'invoice_id', 'invoice', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('cash_order_flows_contractor_id', 'cash_order_flows', 'contractor_id', 'contractor', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('cash_order_flows_expenditure_item_id', 'cash_order_flows', 'expenditure_item_id', 'invoice_expenditure_item', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('cash_order_flows_order_id', 'cash_order_flows', 'reason_id', 'cash_orders_reasons_types', 'id', 'RESTRICT', 'CASCADE');
    }
    
    public function safeDown()
    {
        $this->dropTable('cash_bank_flows');
        $this->dropTable('cash_emoney_flows');
        $this->dropTable('cash_order_flows');
    }
}
