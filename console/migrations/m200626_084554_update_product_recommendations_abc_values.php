<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200626_084554_update_product_recommendations_abc_values extends Migration
{
    public function safeUp()
    {
        $this->execute('UPDATE `product_recommendation_abc` AS p SET p.purchase_price = "Снижать" WHERE p.purchase_price = "Обязательно";');
        $this->execute('UPDATE `product_recommendation_abc` AS p SET p.selling_price = CONCAT("Увеличить на ", p.selling_price) WHERE p.selling_price <> "0%";');
        $this->execute('UPDATE `product_recommendation_abc` AS p SET p.selling_price = "Не менять" WHERE p.selling_price = "0%";');
    }

    public function safeDown()
    {
        echo 'No down migration needed.', PHP_EOL;
    }
}
