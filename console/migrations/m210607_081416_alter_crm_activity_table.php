<?php

use yii\db\Migration;

class m210607_081416_alter_crm_activity_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%crm_client_activity}}';

    /**
     * @inheritDoc
     */
    public function up(): void
    {
        $this->renameColumn(self::TABLE_NAME, 'client_activity_id', 'id');
        $this->renameTable(self::TABLE_NAME, '{{%contractor_activity}}');
    }

    /**
     * @inheritDoc
     */
    public function down(): bool
    {
        return false;
    }
}
