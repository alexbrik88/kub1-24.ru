<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160215_092721_add_to_company_nds extends Migration
{
    public $tCompany = 'company';

    public function safeUp()
    {
        $this->addColumn($this->tCompany, 'nds', $this->integer(11)->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tCompany, 'nds');
    }
}


