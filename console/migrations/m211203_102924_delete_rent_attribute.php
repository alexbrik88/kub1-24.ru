<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211203_102924_delete_rent_attribute extends Migration
{
    public function safeUp()
    {
        $this->execute('
            DELETE {{%rent_attribute_value}}
            FROM {{%rent_attribute_value}}
            LEFT JOIN {{%rent_attribute}} ON {{%rent_attribute}}.[[id]] = {{%rent_attribute_value}}.[[attribute_id]]
            WHERE {{%rent_attribute}}.[[category_id]] = 2 AND {{%rent_attribute}}.[[alias]] = "tenant"
        ');
        $this->delete('{{%rent_attribute}}', [
            'category_id' => 2,
            'alias' => 'tenant',
        ]);
    }

    public function safeDown()
    {
        $this->insert('{{%rent_attribute}}', [
            'id' => 12,
            'category_id' => 2,
            'alias' => 'tenant',
            'title' => 'Арендатор',
            'type' => 'string',
            'sort' => 1,
            'default' => null,
            'data' => null,
            'required' => false,
        ]);
    }
}
