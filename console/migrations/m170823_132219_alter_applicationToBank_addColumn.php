<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170823_132219_alter_applicationToBank_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%application_to_bank}}', 'contact_email', $this->string()->notNull() . ' AFTER [[contact_phone]]');
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%application_to_bank}}', 'contact_email');
    }
}
