<?php

use yii\db\Migration;

/**
 * Handles the creation of table `store_company`.
 */
class m180222_110539_create_store_company_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('store_company', [
            'id' => $this->primaryKey(),
            'company_type_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'inn' => $this->string(12)->notNull(),
            'kpp' => $this->string(9),
            'address' => $this->string(),
            'status' => $this->smallInteger()->notNull()->defaultValue(10),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addForeignKey('FK_storeCompany_companyType', 'store_company', 'company_type_id', 'company_type', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('store_company');
    }
}
