<?php

use console\components\db\Migration;

class m200109_184030_evotor_user_company_id_add extends Migration
{
    const TABLE = '{{%evotor_user}}';

    public function up()
    {
        $this->addColumn(self::TABLE, 'company_id', $this->integer(11)->notNull()->after('id')->comment('Идентификатор компании КУБ'));
    }

    public function down()
    {
        $this->dropColumn(self::TABLE, 'company_id');
    }
}
