<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200924_064316_alter_product_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%product}}', 'title_en', $this->text()->after('title'));
        $this->addColumn('{{%product}}', 'code_tn_ved', $this->string(13)->after('code'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%product}}', 'title_en');
        $this->dropColumn('{{%product}}', 'code_tn_ved');
    }
}
