<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210429_154753_alter_invoice_add_coumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%invoice}}', 'responsible_employee_id', $this->integer()->after('document_author_id'));
        $this->execute('
            UPDATE {{%invoice}}
            SET {{%invoice}}.[[responsible_employee_id]] = {{%invoice}}.[[document_author_id]]
        ');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%invoice}}', 'responsible_employee_id');
    }
}
