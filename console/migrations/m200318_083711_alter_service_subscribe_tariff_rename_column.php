<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200318_083711_alter_service_subscribe_tariff_rename_column extends Migration
{
    public function safeUp()
    {
        $this->renameColumn('{{%service_subscribe_tariff}}', 'invoice_limit', 'tariff_limit');
    }

    public function safeDown()
    {
        $this->renameColumn('{{%service_subscribe_tariff}}', 'tariff_limit', 'invoice_limit');
    }
}
