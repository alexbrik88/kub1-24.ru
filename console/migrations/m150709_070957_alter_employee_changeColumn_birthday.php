<?php

use yii\db\Schema;
use yii\db\Migration;

class m150709_070957_alter_employee_changeColumn_birthday extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('employee', 'birthday', Schema::TYPE_DATE . ' NULL');
    }
    
    public function safeDown()
    {
        $this->alterColumn('employee', 'birthday', Schema::TYPE_DATE . ' NOT NULL');
    }
}
