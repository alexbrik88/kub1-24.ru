<?php

use common\models\Company;
use common\models\document\UploadedDocuments;
use common\models\employee\EmployeeSignature;
use common\models\file\File;
use common\models\report\Report;
use console\components\db\Migration;
use yii\db\Schema;
use yii\helpers\FileHelper;

class m180712_045547_move_company_files extends Migration
{
    public function up()
    {
        Yii::$app->db->createCommand('SET session wait_timeout=28800')->execute();
        Yii::$app->db->createCommand('SET session interactive_timeout=28800')->execute();
        $offset = 0;
        $limit = 1000;
        do {
            $ids = Company::find()
                    ->select('id')
                    ->orderBy(['id' => SORT_ASC])
                    ->offset($offset++ * $limit)
                    ->limit($limit)
                    ->column();

            if ($ids) {
                $uploadPath = Yii::getAlias('@common/uploads');
                foreach ($ids as $id) {
                    $companyDir = Company::fileUploadDir($id);
                    $this->moveProfile($id, $companyDir, $uploadPath);
                    $this->moveDocuments($id, $companyDir, $uploadPath);
                    $this->moveProducts($id, $companyDir, $uploadPath);
                    $this->moveReports($id, $companyDir, $uploadPath);
                    $this->moveSignatures($id, $companyDir, $uploadPath);
                    $this->moveUploadedDocuments($id, $companyDir, $uploadPath);
                }
            }
        } while ($ids);
    }

    public function down()
    {
        echo 'The rollback of this migration has no effect';
    }

    private function moveProfile($id, $companyDir, $uploadPath)
    {
        $oldPath = $uploadPath . DIRECTORY_SEPARATOR . 'company' . DIRECTORY_SEPARATOR . $id;
        if (is_dir($oldPath)) {
            $newPath = $uploadPath . DIRECTORY_SEPARATOR . $companyDir . DIRECTORY_SEPARATOR . 'profile';
            $handle = opendir($oldPath);
            if ($handle !== false) {
                while (($file = readdir($handle)) !== false) {
                    if ($file === '.' || $file === '..') {
                        continue;
                    }
                    $oldFile = $oldPath . DIRECTORY_SEPARATOR . $file;
                    $newFile = $newPath . DIRECTORY_SEPARATOR . $file;
                    if (is_file($oldFile) && !is_file($newFile)) {
                        if (!file_exists($newPath)) {
                            FileHelper::createDirectory($newPath);
                        }
                        copy($oldFile, $newFile);
                    }
                }
                closedir($handle);
            }
        }
    }

    private function moveDocuments($id, $companyDir, $uploadPath)
    {
        $offset = 0;
        $limit = 1000;

        do {
            $rows = File::find()
                    ->select(['id', 'filepath'])
                    ->where(['company_id' => $id])
                    ->orderBy(['id' => SORT_ASC])
                    ->offset($offset++ * $limit)
                    ->limit($limit)
                    ->asArray()
                    ->all();

            if ($rows) {
                foreach ($rows as $row) {
                    $oldFile = $uploadPath . DIRECTORY_SEPARATOR . $row['filepath'] . DIRECTORY_SEPARATOR . $row['id'];
                    $newPath = $uploadPath . DIRECTORY_SEPARATOR . $companyDir . DIRECTORY_SEPARATOR . $row['filepath'];
                    $newFile = $newPath . DIRECTORY_SEPARATOR . $row['id'];
                    if (is_file($oldFile) && !is_file($newFile)) {
                        if (!file_exists($newPath)) {
                            FileHelper::createDirectory($newPath);
                        }
                        copy($oldFile, $newFile);
                    }
                }
            }
        } while ($rows);
    }

    private function moveProducts($id, $companyDir, $uploadPath)
    {
        $oldDir = $uploadPath . DIRECTORY_SEPARATOR . 'product' . DIRECTORY_SEPARATOR . $id;
        $newDir = $uploadPath . DIRECTORY_SEPARATOR . $companyDir . DIRECTORY_SEPARATOR . 'product';

        if (is_dir($oldDir)) {
            $handle = opendir($oldDir);
            if ($handle !== false) {
                while (($dir = readdir($handle)) !== false) {
                    if ($dir === '.' || $dir === '..') {
                        continue;
                    }
                    $oldPath = $oldDir . DIRECTORY_SEPARATOR . $dir;
                    if (is_dir($oldPath)) {
                        $itemHandle = opendir($oldPath);
                        if ($itemHandle !== false) {
                            while (($file = readdir($itemHandle)) !== false) {
                                if ($file === '.' || $file === '..') {
                                    continue;
                                }
                                $oldFile = $oldPath . DIRECTORY_SEPARATOR . $file;
                                $newPath = $newDir . DIRECTORY_SEPARATOR . $dir;
                                $newFile = $newPath . DIRECTORY_SEPARATOR . $file;
                                if (is_file($oldFile) && !is_file($newFile)) {
                                    if (!file_exists($newPath)) {
                                        FileHelper::createDirectory($newPath);
                                    }
                                    copy($oldFile, $newFile);
                                }
                            }
                            closedir($itemHandle);
                        }
                    }
                }
                closedir($handle);
            }
        }
    }

    private function moveReports($id, $companyDir, $uploadPath)
    {
        $ids = Report::find()
            ->select('id')
            ->where(['company_id' => $id])
            ->column();

        if ($ids) {
            $oldDir = $uploadPath . DIRECTORY_SEPARATOR . 'report_file';
            $newDir = $uploadPath . DIRECTORY_SEPARATOR . $companyDir . DIRECTORY_SEPARATOR . 'report';
            foreach ($ids as $reportId) {
                $oldPath = $oldDir . DIRECTORY_SEPARATOR . $reportId;
                $newPath = $newDir . DIRECTORY_SEPARATOR . $reportId;
                if (is_dir($oldPath)) {
                    $handle = opendir($oldPath);
                    if ($handle !== false) {
                        while (($file = readdir($handle)) !== false) {
                            if ($file === '.' || $file === '..') {
                                continue;
                            }
                            $oldFile = $oldPath . DIRECTORY_SEPARATOR . $file;
                            $newFile = $newPath . DIRECTORY_SEPARATOR . $file;
                            if (is_file($oldFile) && !is_file($newFile)) {
                                if (!file_exists($newPath)) {
                                    FileHelper::createDirectory($newPath);
                                }
                                copy($oldFile, $newFile);
                            }
                        }
                        closedir($handle);
                    }
                }
            }
        }
    }

    private function moveSignatures($id, $companyDir, $uploadPath)
    {
        $rows = EmployeeSignature::find()
                ->select(['employee_id', 'file_name'])
                ->where(['company_id' => $id])
                ->asArray()
                ->all();

        if ($rows) {
            $data = [];
            foreach ($rows as $row) {
                $data[$row['employee_id']][] = $row['file_name'];
            }
            foreach ($data as $employee_id => $fileArray) {
                $oldPath = $uploadPath . DIRECTORY_SEPARATOR . 'signature' .
                                         DIRECTORY_SEPARATOR . $id .
                                         DIRECTORY_SEPARATOR . $employee_id;
                $newPath = $uploadPath . DIRECTORY_SEPARATOR . $companyDir .
                                         DIRECTORY_SEPARATOR . 'signature' .
                                         DIRECTORY_SEPARATOR . $employee_id;
                foreach ($fileArray as $file) {
                    $oldFile = $oldPath . DIRECTORY_SEPARATOR . $file;
                    $newFile = $newPath . DIRECTORY_SEPARATOR . $file;
                    if (is_file($oldFile) && !is_file($newFile)) {
                        if (!file_exists($newPath)) {
                            FileHelper::createDirectory($newPath);
                        }
                        copy($oldFile, $newFile);
                    }
                }
            }
        }
    }

    private function moveUploadedDocuments($id, $companyDir, $uploadPath)
    {
        $ids = UploadedDocuments::find()
                ->select('id')
                ->where(['company_id' => $id])
                ->column();

        if ($ids) {
            foreach ($ids as $itemId) {
                $oldPath = $uploadPath . DIRECTORY_SEPARATOR . 'uploaded_documents' .
                                         DIRECTORY_SEPARATOR . $itemId;
                $newPath = $uploadPath . DIRECTORY_SEPARATOR . $companyDir .
                                         DIRECTORY_SEPARATOR . 'uploaded_documents' .
                                         DIRECTORY_SEPARATOR . $itemId;
                if (is_dir($oldPath)) {
                    $handle = opendir($oldPath);
                    if ($handle !== false) {
                        while (($file = readdir($handle)) !== false) {
                            if ($file === '.' || $file === '..') {
                                continue;
                            }
                            $oldFile = $oldPath . DIRECTORY_SEPARATOR . $file;
                            $newFile = $newPath . DIRECTORY_SEPARATOR . $file;
                            if (is_file($oldFile) && !is_file($newFile)) {
                                if (!file_exists($newPath)) {
                                    FileHelper::createDirectory($newPath);
                                }
                                copy($oldFile, $newFile);
                            }
                        }
                        closedir($handle);
                    }
                }
            }
        }
    }
}
