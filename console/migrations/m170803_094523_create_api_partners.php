<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170803_094523_create_api_partners extends Migration
{
    public $tableName = 'api_partners';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'partner_name' => $this->string()->defaultValue(null),
            'partner_site' => $this->string()->defaultValue(null),
            'api_key' => $this->string()->defaultValue(null),
            'start_date' => $this->date()->defaultValue(null),
            'end_date' => $this->date()->defaultValue(null),
            'contract' => $this->string()->defaultValue(null),
            'contact_person_fio' => $this->string()->defaultValue(null),
            'phone' => $this->string()->defaultValue(null),
            'email' => $this->string()->defaultValue(null),
            'is_blocked' => $this->boolean()->defaultValue(false),
        ]);
    }
    
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}


