<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180706_100518_alter_service_store_out_invoice_tariff_unlim extends Migration
{
    public $tableName = 'service_store_out_invoice_tariff';

    public function safeUp()
    {
        $this->update($this->tableName, ['links_count' => 'Без ограничений'], ['id' => 3]);
    }
    
    public function safeDown()
    {
        $this->update($this->tableName, ['links_count' => 'Unlim'], ['id' => 3]);
    }
}


