<?php

use console\components\db\Migration;
use yii\db\Schema;

class m161227_122650_create_statistic_range_fields_in_user extends Migration
{
    public $tEmployee = 'user';

    public function safeUp()
    {
        $this->addColumn($this->tEmployee, 'statistic_range_date_from', $this->string());
        $this->addColumn($this->tEmployee, 'statistic_range_date_to', $this->string());
        $this->addColumn($this->tEmployee, 'statistic_range_name', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn($this->tEmployee, 'statistic_range_date_from', $this->string());
        $this->dropColumn($this->tEmployee, 'statistic_range_date_to', $this->string());
        $this->dropColumn($this->tEmployee, 'statistic_range_name', $this->string());
    }
}


