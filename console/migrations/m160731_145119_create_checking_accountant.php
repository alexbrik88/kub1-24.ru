<?php

use yii\db\Migration;

/**
 * Handles the creation for table `checking_accountant`.
 */
class m160731_145119_create_checking_accountant extends Migration
{
    public $tCheckingAccountant = 'checking_accountant';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->tCheckingAccountant, [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'bank_name' => $this->string(),
            'bik' => $this->string(9),
            'ks' => $this->string(),
            'rs' => $this->string(),
            'type' => $this->boolean()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->tCheckingAccountant);
    }
}
