<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181029_154245_insert_agreement_type extends Migration
{
    public function safeUp()
    {
        $this->batchInsert('{{%agreement_type}}', ['id', 'name', 'name_dative', 'sort'], [
            ['9', 'Договор-заявка', 'договору-заявке', '70'],
            ['10', 'Заявка', 'заявке', '80'],
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%agreement_type}}', ['id' => [9, 10]]);
    }
}
