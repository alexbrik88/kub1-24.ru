<?php

use console\components\db\Migration;
use yii\db\Schema;

class m171004_134955_update_service_more_stock extends Migration
{
    public $tableName = 'service_more_stock';

    public function safeUp()
    {
        $this->dropForeignKey($this->tableName . '_company_type_id', $this->tableName);
        $this->dropColumn($this->tableName, 'company_type_id');

        $this->addColumn($this->tableName, 'activities_id', $this->integer()->defaultValue(null));
        $this->addForeignKey($this->tableName . '_activities_id', $this->tableName, 'activities_id', 'activities', 'id', 'RESTRICT', 'CASCADE');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_activities_id', $this->tableName);
        $this->dropColumn($this->tableName, 'activities_id');

        $this->addColumn($this->tableName, 'company_type_id', $this->integer()->defaultValue(null));
        $this->addForeignKey($this->tableName . '_company_type_id', $this->tableName, 'company_type_id', 'company_type', 'id', 'RESTRICT', 'CASCADE');
    }
}


