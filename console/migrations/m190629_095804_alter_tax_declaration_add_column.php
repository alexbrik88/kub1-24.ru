<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190629_095804_alter_tax_declaration_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('tax_declaration', 'tax_quarter', $this->integer(1)->after('tax_year'));
    }

    public function safeDown()
    {
        $this->dropColumn('tax_declaration', 'tax_quarter');
    }
}
