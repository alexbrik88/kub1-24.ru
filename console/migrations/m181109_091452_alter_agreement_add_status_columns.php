<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181109_091452_alter_agreement_add_status_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%agreement}}', 'status_updated_at', $this->integer().' AFTER [[contractor_id]]');
        $this->addColumn('{{%agreement}}', 'status_author_id', $this->integer().' AFTER [[contractor_id]]');
        $this->addColumn('{{%agreement}}', 'status_id', $this->integer().' AFTER [[contractor_id]]');

        $this->addForeignKey('FK_agreement_to_status', '{{%agreement}}', 'status_id', 'agreement_status', 'id');
        $this->addForeignKey('FK_agreement_to_status_author', '{{%agreement}}', 'status_author_id', 'employee', 'id');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('FK_agreement_to_status_author', '{{%agreement}}');
        $this->dropForeignKey('FK_agreement_to_status', '{{%agreement}}');

        $this->dropColumn('{{%agreement}}', 'status_id');
        $this->dropColumn('{{%agreement}}', 'status_updated_at');
        $this->dropColumn('{{%agreement}}', 'status_author_id');
    }
}


