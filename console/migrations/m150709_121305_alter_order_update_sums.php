<?php

use yii\db\Schema;
use yii\db\Migration;

class m150709_121305_alter_order_update_sums extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('order', 'purchase_price_no_vat', Schema::TYPE_BIGINT . ' NOT NULL');
        $this->alterColumn('order', 'purchase_price_with_vat', Schema::TYPE_BIGINT . ' NOT NULL');

        $this->alterColumn('order', 'selling_price_no_vat', Schema::TYPE_BIGINT . ' NOT NULL');
        $this->alterColumn('order', 'selling_price_with_vat', Schema::TYPE_BIGINT . ' NOT NULL');


        $this->alterColumn('order', 'amount_purchase_no_vat', Schema::TYPE_BIGINT . ' NOT NULL');
        $this->alterColumn('order', 'amount_purchase_with_vat', Schema::TYPE_BIGINT . ' NOT NULL');

        $this->alterColumn('order', 'amount_sales_no_vat', Schema::TYPE_BIGINT . ' NOT NULL');
        $this->alterColumn('order', 'amount_sales_with_vat', Schema::TYPE_BIGINT . ' NOT NULL');

        $this->alterColumn('order', 'purchase_tax', Schema::TYPE_BIGINT . ' NOT NULL');
        $this->alterColumn('order', 'sale_tax', Schema::TYPE_BIGINT . ' NOT NULL');
    }
    
    public function safeDown()
    {
        $this->alterColumn('order', 'purchase_price_no_vat', Schema::TYPE_INTEGER . ' NOT NULL');
        $this->alterColumn('order', 'purchase_price_with_vat', Schema::TYPE_INTEGER . ' NOT NULL');

        $this->alterColumn('order', 'selling_price_no_vat', Schema::TYPE_INTEGER . ' NOT NULL');
        $this->alterColumn('order', 'selling_price_with_vat', Schema::TYPE_INTEGER . ' NOT NULL');


        $this->alterColumn('order', 'amount_purchase_no_vat', Schema::TYPE_INTEGER . ' NOT NULL');
        $this->alterColumn('order', 'amount_purchase_with_vat', Schema::TYPE_INTEGER . ' NOT NULL');

        $this->alterColumn('order', 'amount_sales_no_vat', Schema::TYPE_INTEGER . ' NOT NULL');
        $this->alterColumn('order', 'amount_sales_with_vat', Schema::TYPE_INTEGER . ' NOT NULL');

        $this->alterColumn('order', 'purchase_tax', Schema::TYPE_INTEGER . ' NOT NULL');
        $this->alterColumn('order', 'sale_tax', Schema::TYPE_INTEGER . ' NOT NULL');
    }
}
