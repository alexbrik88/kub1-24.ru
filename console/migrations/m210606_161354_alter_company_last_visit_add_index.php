<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210606_161354_alter_company_last_visit_add_index extends Migration
{
    public function safeUp()
    {
        $this->createIndex('idx_company_last_visit_employee_id', 'company_last_visit', 'employee_id');
    }

    public function safeDown()
    {
        $this->dropIndex('idx_company_last_visit_employee_id', 'company_last_visit');
    }
}
