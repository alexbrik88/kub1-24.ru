<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211019_081048_update_service_subscribe_tariff_group extends Migration
{
    public function safeUp()
    {
        $this->update('{{%service_subscribe_tariff_group}}', [
            'is_active' => 0,
        ], [
            'id' => 6,
        ]);
    }

    public function safeDown()
    {
        $this->update('{{%service_subscribe_tariff_group}}', [
            'is_active' => 1,
        ], [
            'id' => 6,
        ]);
    }
}
