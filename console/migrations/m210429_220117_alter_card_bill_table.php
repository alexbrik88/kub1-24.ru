<?php

use yii\db\Migration;

final class m210429_220117_alter_card_bill_table extends Migration
{
    private const TABLE_NAME = '{{%card_bill}}';

    /**
     * @inheritDoc
     */
    public function up(): void
    {
        $this->addColumn(self::TABLE_NAME, 'number', $this->string()->notNull()->defaultValue('')->after('name'));
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'number');
    }
}
