<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211213_121537_insert_currency extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%currency}}', [
            'id' => '156',
            'name' => 'CNY',
            'label' => 'Китайский юань',
            'old_value' => 0,
            'current_value' => 0,
            'sort' => 7,
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%currency}}', [
            'id' => '156',
        ]);
    }
}
