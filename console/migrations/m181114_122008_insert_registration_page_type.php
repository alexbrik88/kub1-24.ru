<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181114_122008_insert_registration_page_type extends Migration
{
    public function safeUp()
    {
        $this->insert('registration_page_type', [
            'id' => 9,
            'name' => 'Банк Открытие',
        ]);
    }

    public function safeDown()
    {
        $this->delete('registration_page_type', [
            'id' => 9,
        ]);
    }
}
