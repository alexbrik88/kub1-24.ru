<?php

use yii\db\Schema;
use yii\db\Migration;

class m150901_121607_alter_employee_addColumn_email_confirm_hash extends Migration
{
    public $tableEmployee = 'employee';

    public function safeUp()
    {
        $this->addColumn($this->tableEmployee, 'email_confirm_key', 'CHAR(45) AFTER email');
        $this->addColumn($this->tableEmployee, 'is_email_confirmed', $this->boolean()->notNull() . ' DEFAULT 0 AFTER email_confirm_key');
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableEmployee, 'email_confirm_key');
        $this->dropColumn($this->tableEmployee, 'is_email_confirmed');
    }
}
