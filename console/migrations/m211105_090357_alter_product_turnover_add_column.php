<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211105_090357_alter_product_turnover_add_column extends Migration
{
    public $table = 'product_turnover';

    public function safeUp()
    {
        $table = $this->table;

       $this->addColumn($table, 'total_amount_nds', $this->decimal(22, 2)->defaultValue(0)->after('total_amount'));

        $this->execute("
            UPDATE `{$table}` t
            LEFT JOIN `order` o ON t.order_id = o.id
            SET t.total_amount_nds = IF(t.type = 2, o.selling_price_with_vat - o.selling_price_no_vat, o.purchase_price_with_vat - o.purchase_price_no_vat) * t.quantity
            WHERE t.document_table IN ('act', 'packing_list', 'upd', 'sales_invoice')
        ");

        $this->execute("
            UPDATE `{$table}` t
            LEFT JOIN `order_goods_cancellation` o ON t.order_id = o.id
            SET t.total_amount_nds = (o.purchase_price_with_vat - o.purchase_price_no_vat) * t.quantity
            WHERE t.document_table IN ('goods_cancellation')
        ");

        $this->execute("
            UPDATE `{$table}` t
            LEFT JOIN `ofd_receipt_item` o ON t.order_id = o.id
            SET t.total_amount_nds = o.nds_20 + o.nds_10 + o.nds_0 + o.nds_calculated_20 + o.nds_calculated_10  
            WHERE t.document_table IN ('ofd_receipt')
        ");
    }

    public function safeDown()
    {
        $table = $this->table;

        $this->dropColumn($table, 'total_amount_nds');
    }
}
