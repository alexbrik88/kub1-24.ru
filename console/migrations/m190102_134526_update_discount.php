<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190102_134526_update_discount extends Migration
{
    public $tableName = 'discount';

    public function safeUp()
    {
        $this->update($this->tableName, ['tariff_id' => '1'], ['in', 'type_id', [1, 2]]);
    }

    public function safeDown()
    {
        $this->update($this->tableName, ['tariff_id' => '1,2,3'], ['in', 'type_id', [1, 2]]);
    }
}
