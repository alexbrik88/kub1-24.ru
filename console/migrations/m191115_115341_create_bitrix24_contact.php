<?php

use console\components\db\Migration;
use common\models\bitrix24\Contact;

class m191115_115341_create_bitrix24_contact extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{bitrix24_contact}}', [
            'id' => $this->integer()->unsigned()->notNull()->comment('Идентификатор компании Битрикс24'),
            'employee_id' => $this->integer()->notNull()->comment('ID клиента КУБ'),
            'company_id' => $this->integer()->unsigned()->comment('Идентификатор компании'),
            'type' => $this->string(50)->notNull()->defaultValue('OTHER')->comment('Тип'),
            'honorific' => $this->string(50)->notNull()->comment('Обращение'),
            'post' => $this->string(50)->comment('Должность'),
            'name' => $this->string(30)->notNull()->comment('Имя'),
            'last_name' => $this->string(30)->notNull()->comment('Фамилия'),
            'second_name' => $this->string(30)->comment('Отчество'),
            'comment' => $this->string(1000)->comment('Комментарий'),
            'source' => $this->string(50)->notNull()->defaultValue('OTHER')->comment('Источник контакта'),
            'source_description' => $this->string(1000)->comment('Примечание по источнику контакта'),
            'birthdate' => $this->integer()->unsigned()->comment('Дата рождения'),
            'phone' => $this->string(1000)->comment('Номера телефонов, JSON'),
            'email' => $this->string(1000)->comment('E-mail, JSON'),
            'web' => $this->string(1000)->comment('Сайты, JSON'),
            'im' => $this->string(1000)->comment('Соц. сети, JSON'),
            'created_at' => $this->integer()->unsigned()->notNull()->comment('Дата добавления'),
            'updated_at' => $this->integer()->unsigned()->notNull()->comment('Дата последнего изменения'),
        ], "COMMENT 'Интеграция Битрикс24: контакты'");
        $this->addPrimaryKey('bitrix24_contact_id', '{{bitrix24_contact}}', ['employee_id', 'id']);
        $this->addForeignKey(
            'bitrix24_contact_employee_id', '{{bitrix24_contact}}', 'employee_id',
            '{{employee}}', 'id', 'CASCADE', 'CASCADE'
        );
        $this->addForeignKey('bitrix24_contact_company_id', '{{bitrix24_contact}}', ['employee_id', 'company_id'],
            '{{bitrix24_company}}', ['employee_id', 'id'], 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{bitrix24_contact}}');
    }
}
