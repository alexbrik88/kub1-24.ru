<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190226_080426_taxrobot_drop_payment_events extends Migration
{
    public function safeUp()
    {
        // 88 - робот оплачен
        $events = \common\models\company\CompanyFirstEvent::find()->where(['event_id' => 88])->all();
        foreach ($events as $event) {
            $company  = \common\models\Company::findOne(['id' => $event->company_id]);
            if ($company) {
                $taxRobot = new \common\components\TaxRobotHelper($company);
                if (!$taxRobot->isPaid)
                    $event->delete();
            }
        }
    }

    public function safeDown()
    {

    }
}
