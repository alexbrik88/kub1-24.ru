<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211022_080940_create_invoice_in_to_invoice_out extends Migration
{
    public function safeUp()
    {
        $table = 'invoice_in_to_invoice_out';
        $this->createTable($table, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'number' => $this->integer()->notNull(),
            'in_invoice' => $this->integer()->notNull(),
            'out_invoice' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey("FK_{$table}_in", $table, 'in_invoice', 'invoice', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("FK_{$table}_out", $table, 'out_invoice', 'invoice', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("FK_{$table}_company", $table, 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $table = 'invoice_in_to_invoice_out';
        $this->dropTable($table);
    }
}
