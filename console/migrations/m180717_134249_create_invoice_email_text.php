<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180717_134249_create_invoice_email_text extends Migration
{
    public $tableName = 'invoice_email_text';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'text' => $this->text()->defaultValue(null),
            'is_checked' => $this->boolean()->defaultValue(false),
        ]);

        $this->addForeignKey($this->tableName . '_company_id', $this->tableName, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_company_id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}


