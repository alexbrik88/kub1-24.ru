<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170303_081318_add_to_invoice_autoinvoice_field extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%invoice}}', 'autoinvoice_id', $this->integer());
        $this->addColumn('{{%invoice}}', 'show_popup', $this->boolean()->defaultValue(true));
    }
    
    public function safeDown()
    {
        $this->addColumn('{{%invoice}}', 'autoinvoice_id', $this->integer());
        $this->addColumn('{{%invoice}}', 'show_popup', $this->boolean());
    }
}


