<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200302_075228_update_company_activity extends Migration
{
    public function safeUp()
    {
        $this->update('{{%company_activity}}', ['name' => 'Продажа товаров'], ['id' => 2]);
    }

    public function safeDown()
    {
        $this->update('{{%company_activity}}', ['name' => 'Продажа товаров (не магазин)'], ['id' => 2]);
    }
}
