<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170206_085018_alter_packing_list_add_orders_sum extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%packing_list}}', 'orders_sum', $this->integer(11)->defaultValue(0));
        foreach (\common\models\document\PackingList::find()->all() as $packing_list) {
            $sum = 0;
            if (\common\models\document\OrderPackingList::find()->where(['packing_list_id' => $packing_list->id]) != null) {
                foreach (\common\models\document\OrderPackingList::find()->where(['packing_list_id' => $packing_list->id])->all() as $order_packing_list){
                    if ($packing_list->type == 1){
                        $sum .= $order_packing_list->amount_purchase_with_vat;
                    }elseif ($packing_list->type == 2){
                        $sum .= $order_packing_list->amount_sales_with_vat;
                    }
                }
            }
            $packing_list->orders_sum = $sum;
            $packing_list->save(false);
        }
    }

    public function safeDown()
    {
        $this->dropColumn('{{%packing_list}}', 'orders_sum');
    }
}


