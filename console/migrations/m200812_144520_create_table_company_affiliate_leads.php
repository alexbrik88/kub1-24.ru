<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200812_144520_create_table_company_affiliate_leads extends Migration
{
    public $tableName = 'company_affiliate_leads';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer(11),
            'name' => $this->string(128),
            'username' => $this->string(128),
            'email' => $this->string(128),
            'phone' => $this->string(32),
            'comment' => $this->string(500),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
        ]);
    }

    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
