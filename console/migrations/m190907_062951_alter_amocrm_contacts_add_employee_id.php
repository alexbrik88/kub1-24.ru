<?php

use console\components\db\Migration;

class m190907_062951_alter_amocrm_contacts_add_employee_id extends Migration
{
    public function safeUp()
    {
        $this->truncateTable('{{amocrm_contacts}}');
        $this->addColumn('{{amocrm_contacts}}', 'employee_id', $this->integer()->notNull()->comment('ID покупателя на сайте'));
        $this->addForeignKey(
            'amocrm_contacts_employee_id', '{{amocrm_contacts}}', 'employee_id',
            '{{employee}}', 'id', 'CASCADE', 'CASCADE'
        );

    }

    public function down()
    {
        $this->dropColumn('{{amocrm_contacts}}', 'employee_id');
    }

}
