<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210113_082528_alter_expenditure_item_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('invoice_expenditure_item', 'parent_id', $this->integer()->after('id'));
        $this->addForeignKey('FK_invoice_expenditure_item_parent_id_to_id', 'invoice_expenditure_item', 'parent_id', 'invoice_expenditure_item', 'id', 'SET NULL');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_invoice_expenditure_item_parent_id_to_id', 'invoice_expenditure_item');
        $this->dropColumn('invoice_expenditure_item', 'parent_id');
    }
}
