<?php

use console\components\db\Migration;

class m200204_154908_fssp_task_item extends Migration
{
    const TABLE = '{{%fssp_item}}';

    public function safeUp()
    {
        $this->createTable(self::TABLE, [
            'id' => $this->primaryKey()->unsigned()->notNull(),
            'contractor_id' => $this->integer(11)->notNull()->comment('ID контрагента'),
            'name' => $this->string(250)->notNull()->comment('Название контрагента, полученное от ФССП'),
            'production_number' => $this->string(19)->notNull()->comment('Номер исполнительного производства'),
            'production_date' => $this->date()->notNull()->comment('Дата исполнительного производства'),
            'subject' => $this->string(300)->notNull(),
            'detail' => $this->string(1000)->notNull()->comment('Описание документа'),
            'department' => $this->string(300)->notNull()->comment('Отделение ФССП'),
            'bailiff' => $this->string(300)->notNull()->comment('Судебный пристав'),
            'end_date' => $this->date()->comment('Дата завершения производства, если NULL, производство не завершено'),
            'end_base' => $this->string(100)->comment('Основание завершения (статья, пункт, подпункт)'),
        ], "COMMENT='Сведения о задолженностях контрагентов, полученные из ФССП (исполнительные производства)'");
        $this->addForeignKey('fssp_item_contractor_id', self::TABLE, 'contractor_id', '{{%contractor}}', 'id',
            'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable(self::TABLE);
    }
}
