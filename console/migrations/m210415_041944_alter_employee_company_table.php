<?php

use yii\db\Migration;

class m210415_041944_alter_employee_company_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%employee_company}}';

    /**
     * @inheritDoc
     */
    public function up(): void
    {
        $this->addColumn(self::TABLE_NAME, 'last_card_account_id', $this->bigInteger()->null());
        $this->createIndex('ck_last_card_account_id', self::TABLE_NAME, ['last_card_account_id']);
        $this->addForeignKey(
            'fk_last_card_account_id__card_account_id',
            self::TABLE_NAME,
            ['last_card_account_id'],
            '{{%card_account}}',
            ['id'],
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * @inheritDoc
     */
    public function down(): void
    {
        $this->dropForeignKey('fk_last_card_account_id__card_account_id', self::TABLE_NAME);
        $this->dropIndex('ck_last_card_account_id', self::TABLE_NAME);
        $this->dropColumn(self::TABLE_NAME, 'last_card_account_id');
    }
}
