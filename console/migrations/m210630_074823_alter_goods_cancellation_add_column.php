<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210630_074823_alter_goods_cancellation_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('goods_cancellation', 'responsible_employee_id', $this->integer()->defaultValue(null)->after('document_author_id'));
        $this->addForeignKey('fk_goods_cancellation_responsible_employee', 'goods_cancellation', 'responsible_employee_id', 'employee', 'id', 'SET NULL', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_goods_cancellation_responsible_employee', 'goods_cancellation');
        $this->dropColumn('goods_cancellation', 'responsible_employee_id');
    }
}
