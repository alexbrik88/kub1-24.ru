<?php

use common\models\evotor\Product;
use console\components\db\Migration;

class m191009_132705_create_evotor_product extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{evotor_product}}', [
            'id' => "BINARY(16) NOT NULL PRIMARY KEY COMMENT 'UUID Эвотора'",
            'employee_id' => $this->integer()->notNull()->comment('ID покупателя сайта'),
            'store_id' => "BINARY(16) NOT NULL COMMENT 'ID магазина'",
            'parent_id' => "BINARY(16) COMMENT 'ID категории (родительского элемента с group=1)'",
            'group' => $this->boolean()->notNull()->defaultValue(false)->comment('Является ли товар группой'),
            'name' => $this->string(100)->notNull()->comment('Название'),
            'type' => "ENUM('" . implode("','", Product::TYPE) . "') NOT NULL DEFAULT '" . Product::TYPE_NORMAL . "' COMMENT 'Тип товара'",
            'quantity' => $this->integer()->unsigned()->notNull()->defaultValue(0)->comment('Остаток'),
            'measure_name' => "CHAR(10) COMMENT 'Единица измерения'",
            'tax' => "ENUM('" . implode("','", Product::TAX) . "') NOT NULL DEFAULT '" . Product::TAX_VAT_18 . "' COMMENT 'Налоговая ставка'",
            'price' => $this->float()->unsigned()->comment('Отпускная цена'),
            'allow_to_sell' => $this->boolean()->notNull()->defaultValue(true)->comment('Можно ли добавить товар в чек'),
            'cost_price' => $this->float()->unsigned()->comment('Закупочная цена'),
            'description' => $this->string(1000)->comment('Описание товара'),
            'article_number' => $this->string(20)->comment('Артикул'),
            'code' => $this->string(11)->comment('Код товара или группы (категории) товаров'),
            'bar_codes' => $this->string(1000)->comment('Штрихкоды товаров через запятую'),
            'attributes' => $this->string(1000)->comment('Атрибуты товара (JSON)'),
            'created_at' => $this->integer()->unsigned()->notNull(),
            'updated_at' => $this->integer()->unsigned()->notNull(),
        ], "COMMENT 'Интеграция Эвотор: товары'");
        $this->addForeignKey(
            'evotor_product_employee_id', '{{evotor_product}}', 'employee_id',
            '{{employee}}', 'id', 'CASCADE', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable('{{evotor_product}}');

    }
}
