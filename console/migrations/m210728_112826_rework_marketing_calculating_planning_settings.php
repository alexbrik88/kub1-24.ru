<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210728_112826_rework_marketing_calculating_planning_settings extends Migration
{
    public function safeUp()
    {
        $this->truncateTable('marketing_calculating_planning_settings');
        $this->dropColumn('marketing_calculating_planning_settings', 'start_planning_item_id');
        $this->addColumn('marketing_calculating_planning_settings', 'start_month', $this->string(2)->notNull()->after('attribute'));
        $this->addColumn('marketing_calculating_planning_settings', 'start_year', $this->integer(4)->unsigned()->notNull()->after('start_month'));
    }

    public function safeDown()
    {
        $this->truncateTable('marketing_calculating_planning_settings');
        $this->dropColumn('marketing_calculating_planning_settings', 'start_month');
        $this->dropColumn('marketing_calculating_planning_settings', 'start_year');
        $this->addColumn('marketing_calculating_planning_settings', 'start_planning_item_id', $this->integer(11)->unsigned()->notNull()->after('attribute'));
    }
}
