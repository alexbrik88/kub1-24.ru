<?php

use console\components\db\Migration;
use yii\db\Schema;
use common\models\employee\Employee;

class m161108_054142_create_employee_company extends Migration
{
    public $tEmployeeCompany = 'employee_company';

    public function safeUp()
    {
        $this->createTable($this->tEmployeeCompany, [
            'id' => $this->primaryKey(),
            'employee_id' => $this->integer()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'employee_role_id' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey($this->tEmployeeCompany . '_employee_id', $this->tEmployeeCompany, 'employee_id', 'employee', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tEmployeeCompany . '_company_id', $this->tEmployeeCompany, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tEmployeeCompany . '_employee_role_id', $this->tEmployeeCompany, 'employee_role_id', 'employee_role', 'id', 'RESTRICT', 'CASCADE');
        /* @var $employee Employee */
        $all = Employee::find()->select(['id', 'my_companies', 'employee_role_id'])->createCommand()->queryAll();
        foreach ($all as $employee) {
            foreach (unserialize($employee['my_companies']) as $companyId) {
                $this->insert($this->tEmployeeCompany, [
                    'employee_id' => $employee['id'],
                    'company_id' => $companyId,
                    'employee_role_id' => $employee['employee_role_id'],
                ]);
            }
        }
    }

    public function safeDown()
    {
        $this->dropForeignKey($this->tEmployeeCompany . '_employee_id', $this->tEmployeeCompany);
        $this->dropForeignKey($this->tEmployeeCompany . '_company_id', $this->tEmployeeCompany);
        $this->dropForeignKey($this->tEmployeeCompany . '_employee_role_id', $this->tEmployeeCompany);
        $this->dropTable($this->tEmployeeCompany);
    }
}


