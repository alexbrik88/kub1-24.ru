<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200210_141259_alter_price_list_order_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('price_list_order', 'price', $this->string()->notNull()->defaultValue("0")->after('description'));

        $this->execute('UPDATE `price_list_order` SET `price` = `price_for_sell`');
    }

    public function safeDown()
    {
        $this->dropColumn('price_list_order', 'price');
    }
}
