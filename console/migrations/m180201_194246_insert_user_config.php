<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180201_194246_insert_user_config extends Migration
{
    public $tableName = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'project_date', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn($this->tableName, 'project_clients', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn($this->tableName, 'project_end_date', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn($this->tableName, 'project_contractor', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn($this->tableName, 'project_expense', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn($this->tableName, 'project_result', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn($this->tableName, 'project_responsible_employee_id', $this->boolean()->notNull()->defaultValue(true));

        $this->addCommentOnColumn('user_config', 'project_date', 'Показ колонки "Дата проекта" в списке проектов');
        $this->addCommentOnColumn('user_config', 'project_clients', 'Показ колонки "Клиенты" в списке проектов');
        $this->addCommentOnColumn('user_config', 'project_end_date', 'Показ колонки "Окончание" в списке проектов');
        $this->addCommentOnColumn('user_config', 'project_contractor', 'Показ колонки "Поставщик" в списке проектов');
        $this->addCommentOnColumn('user_config', 'project_expense', 'Показ колонки "Расход" в списке проектов');
        $this->addCommentOnColumn('user_config', 'project_result', 'Показ колонки "Результат" в списке проектов');
        $this->addCommentOnColumn('user_config', 'project_responsible_employee_id', 'Показ колонки "Ответственный" в списке проектов');
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'project_date');
        $this->dropColumn($this->tableName, 'project_clients');
        $this->dropColumn($this->tableName, 'project_end_date');
        $this->dropColumn($this->tableName, 'project_contractor');
        $this->dropColumn($this->tableName, 'project_expense');
        $this->dropColumn($this->tableName, 'project_result');
        $this->dropColumn($this->tableName, 'project_responsible_employee_id');
    }
}


