<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210527_121420_alter_ofd_receipt extends Migration
{
    public function safeUp()
    {
        $this->dropPrimaryKey('PRIMARY_KEY', '{{%ofd_receipt_item}}');
        $this->dropPrimaryKey('PRIMARY_KEY', '{{%ofd_receipt}}');

        // `ofd_receipt` table
        $this->renameColumn('{{%ofd_receipt}}', 'id', 'uid');
        $this->addColumn('{{%ofd_receipt}}', 'id', $this->bigPrimaryKey()->first());
        $this->alterColumn('{{%ofd_receipt}}', 'date_time', $this->dateTime()->notNull()->after('company_id')->comment('1012 дата, время'));
        $this->alterColumn('{{%ofd_receipt}}', 'kkt_registration_number', $this->char(20)->notNull()->after('date_time')->comment('1037 регистрационный номер ККТ'));
        $this->alterColumn('{{%ofd_receipt}}', 'document_number', $this->integer()->notNull()->after('kkt_registration_number')->comment('1040 номер фискального документа'));
        $this->alterColumn('{{%ofd_receipt}}', 'uid', $this->char(60)->notNull()->after('document_number')->comment('uid чека'));
        $this->alterColumn('{{%ofd_receipt}}', 'kkt_uid', $this->char(20)->notNull()->comment('Идентификатор ККТ в базе ОФД'));
        $this->alterColumn('{{%ofd_receipt}}', 'kkt_factory_number', $this->char(20)->notNull()->comment('1041 заводской номер ККТ'));

        $this->createIndex('uid', '{{%ofd_receipt}}', 'uid', true);

        // `ofd_receipt_item` table
        $this->dropIndex('receipt', '{{%ofd_receipt_item}}');

        $this->renameColumn('{{%ofd_receipt_item}}', 'id', 'uid');
        $this->renameColumn('{{%ofd_receipt_item}}', 'receipt_id', 'receipt_uid');
        $this->addColumn('{{%ofd_receipt_item}}', 'id', $this->bigPrimaryKey()->first());
        $this->addColumn('{{%ofd_receipt_item}}', 'receipt_id', $this->bigInteger()->after('id'));
        $this->alterColumn('{{%ofd_receipt_item}}', 'date_time', $this->dateTime()->notNull()->after('company_id')->comment('1012 дата, время'));
        $this->alterColumn('{{%ofd_receipt_item}}', 'uid', $this->char(60)->notNull()->after('date_time')->comment('uid позиции'));
        $this->alterColumn('{{%ofd_receipt_item}}', 'receipt_uid', $this->char(60)->notNull()->after('uid')->comment('uid чека'));

        $this->createIndex('uid', '{{%ofd_receipt_item}}', 'uid', true);
        $this->createIndex('receipt_id', '{{%ofd_receipt_item}}', 'receipt_id');
        $this->createIndex('receipt_uid', '{{%ofd_receipt_item}}', 'receipt_uid');

        $this->addForeignKey('fk_ofd_receipt_item__receipt_id', '{{%ofd_receipt_item}}', 'receipt_id', '{{%ofd_receipt}}', 'id');

        $this->execute('
            UPDATE {{%ofd_receipt_item}}
            LEFT JOIN {{%ofd_receipt}} ON {{%ofd_receipt_item}}.[[receipt_uid]] = {{%ofd_receipt}}.[[uid]]
            SET {{%ofd_receipt_item}}.[[receipt_id]] = {{%ofd_receipt}}.[[id]]
        ');
    }

    public function safeDown()
    {
        //
    }
}
