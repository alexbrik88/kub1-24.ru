<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181020_124405_create_agreement_essence extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%agreement_essence}}', [
            'id' => $this->primaryKey(),
			'agreement_id' => $this->integer()->notNull(),
			'document_header' => $this->text(),
			'document_body' => $this->text(),
			'document_requisites_customer' => $this->text(),
			'document_requisites_executer' => $this->text(),
            'created_at' => $this->integer(),
            'created_by' => $this->integer(),
        ]);

        $this->addForeignKey('fk_agreement_essence', 'agreement_essence', 'agreement_id', 'agreement', 'id', 'CASCADE');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('fk_agreement_essence', 'agreement_essence');

        $this->dropTable('{{%agreement_essence}}');
    }
}
