<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190806_104346_alter_cash_emoney_flows_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cash_emoney_flows}}', 'emoney_id', $this->integer()->notNull()->after('company_id'));

        $this->execute('SET foreign_key_checks = 0;');

        $this->addForeignKey('FK_cash_emoney_flows_emoney', '{{%cash_emoney_flows}}', 'emoney_id', '{{%emoney}}', 'id');

        $this->execute('
            UPDATE {{%cash_emoney_flows}}
            LEFT JOIN {{%emoney}} ON {{%cash_emoney_flows}}.[[company_id]] = {{%emoney}}.[[company_id]]
            SET {{%cash_emoney_flows}}.[[emoney_id]] = {{%emoney}}.[[id]]
        ');

        $this->execute('SET foreign_key_checks = 1;');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_cash_emoney_flows_emoney', '{{%cash_emoney_flows}}');

        $this->dropColumn('{{%cash_emoney_flows}}', 'emoney_id');
    }
}
