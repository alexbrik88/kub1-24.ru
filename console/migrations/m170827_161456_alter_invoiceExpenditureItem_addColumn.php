<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170827_161456_alter_invoiceExpenditureItem_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%invoice_expenditure_item}}', 'company_id', $this->integer()->defaultValue(null) . ' AFTER [[id]]');

        $this->addForeignKey('FK_invoiceExpenditureItem_company', '{{%invoice_expenditure_item}}', 'company_id', '{{%company}}', 'id', 'CASCADE');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('FK_invoiceExpenditureItem_company', '{{%invoice_expenditure_item}}');

        $this->dropColumn('{{%invoice_expenditure_item}}', 'company_id');
    }
}
