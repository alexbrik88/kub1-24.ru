<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200717_025419_alter_rent_entity_rent extends Migration
{
    public $table = 'rent_entity_rent';

    public function safeUp()
    {
        $this->addColumn($this->table, 'document_number', $this->string(255)->notNull());
        $this->addColumn($this->table, 'document_additional_number', $this->string(45));
        $this->addColumn($this->table, 'document_date', $this->date()->notNull());
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'document_number');
        $this->dropColumn($this->table, 'document_additional_number');
        $this->dropColumn($this->table, 'document_date');
    }
}
