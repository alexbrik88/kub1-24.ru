<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190704_161325_alter_contractor_alter_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%contractor}}', 'discount', $this->decimal(9, 6));
    }

    public function safeDown()
    {
        $this->alterColumn('{{%contractor}}', 'discount', $this->decimal(5, 2));
    }
}
