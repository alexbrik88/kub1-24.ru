<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180312_133810_alter_order_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('order', 'reserve', $this->decimal(20, 10)->after('quantity'));
    }

    public function safeDown()
    {
        $this->dropColumn('order', 'reserve');
    }
}
