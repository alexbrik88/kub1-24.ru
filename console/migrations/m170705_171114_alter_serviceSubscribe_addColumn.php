<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170705_171114_alter_serviceSubscribe_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%service_subscribe}}', 'discount', $this->integer()->defaultValue(0) . ' AFTER [[tariff_id]]');
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%service_subscribe}}', 'discount');
    }
}
