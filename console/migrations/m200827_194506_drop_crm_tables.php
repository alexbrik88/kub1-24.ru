<?php

use yii\db\Migration;

class m200827_194506_drop_crm_tables extends Migration
{
    /** @var string[] */
    private const TABLE_LIST = [
        '{{%crm_task}}',
        '{{%crm_client}}',
        '{{%crm_campaign}}',
        '{{%crm_campaign_type}}',
        '{{%crm_client_activity}}',
        '{{%crm_client_check}}',
        '{{%crm_client_event}}',
        '{{%crm_client_position}}',
        '{{%crm_client_reason}}',
        '{{%crm_client_status}}',
    ];

    /**
     * @inheritDoc
     */
    public function up()
    {
        foreach (self::TABLE_LIST as $name) {
            $this->dropTable($name);
        }
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        return false;
    }
}
