<?php

use yii\db\Exception;
use yii\db\Migration;

class m200708_094648_create_crm_client_status_table extends Migration
{
    /** @var string Имя таблицы */
    private const TABLE_NAME = '{{%crm_client_status}}';

    /** @var string[] Внешние столбцы */
    private const FK_COLUMNS = ['company_id'];

    /** @var int В работе */
    private const STATUS_IN_WORK = 1;

    /** @var int Отказ */
    private const STATUS_REFUSED = 2;

    /** @var int Активный */
    private const STATUS_ACTIVE = 3;

    /** @var int Неактивный */
    private const STATUS_INACTIVE = 4;

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'client_status_id' => $this->bigPrimaryKey(),
            'company_id' => $this->integer()->null(),
            'name' => $this->string(64)->notNull(),
        ]);

        $this->createIndex('uk_name', self::TABLE_NAME, ['company_id', 'name'], true);
        $this->createIndex('ck_company_id', self::TABLE_NAME, self::FK_COLUMNS);
        $this->addForeignKey(
            'fk_crm_client_status__company',
            self::TABLE_NAME,
            self::FK_COLUMNS,
            '{{%company}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $columns = ['client_status_id', 'name'];
        $this->db->createCommand()->batchInsert(self::TABLE_NAME, $columns, $this->getRows())->execute();
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }

    /**
     * @return string[]
     */
    private function getRows(): array
    {
        return [
            [self::STATUS_IN_WORK, 'В работе'],
            [self::STATUS_REFUSED, 'Отказ'],
            [self::STATUS_ACTIVE, 'Активен'],
            [self::STATUS_INACTIVE, 'Не активен'],
        ];
    }
}
