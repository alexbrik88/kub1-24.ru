<?php

use console\components\db\Migration;
use yii\db\Query;

class m200629_185828_alter_rent_entity_rent extends Migration
{
    public string $table = 'rent_entity_rent';

    public function safeUp()
    {
        $defaultEmployeeId = (new Query())->from('employee')->select('id')->orderBy('id')->limit(1)->scalar();

        $this->addColumn($this->table, 'created_at', $this->integer()->notNull());
        $this->addColumn($this->table, 'created_by', $this->integer()->notNull()->defaultValue($defaultEmployeeId));
        $this->addColumn($this->table, 'deleted', $this->tinyInteger(1)->notNull()->defaultValue(0));
        $this->alterColumn($this->table, 'created_by', $this->integer()->notNull());
        $this->addForeignKey('rent_entity_rent_created_by', $this->table, 'created_by', 'employee', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('rent_entity_rent_created_by', 'rent_entity_rent');
        $this->dropColumn($this->table, 'created_by');
        $this->dropColumn($this->table, 'created_at');
        $this->dropColumn($this->table, 'deleted');
    }
}
