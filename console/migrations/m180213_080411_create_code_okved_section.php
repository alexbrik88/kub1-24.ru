<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180213_080411_create_code_okved_section extends Migration
{
    public $tOkvedCodeSection = 'code_okved_section';
    public $tOkvedCode = 'okved_code';

    public function safeUp()
    {
        $this->createTable($this->tOkvedCodeSection , [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'description' => $this->text()->defaultValue(null),
        ]);

        $this->batchInsert($this->tOkvedCodeSection, ['id', 'name', 'description'], [
            [1, 'A', 'Сельское, лесное хозяйство, охота, рыболовство и рыбоводство'],
            [2, 'B', 'Добыча полезных ископаемых'],
            [3, 'C', 'Обрабатывающие производства'],
            [4, 'D', 'Обеспечение электрической энергией, газом и паром; кондиционирование воздуха'],
            [5, 'E', 'Водоснабжение; водоотведение, организация сбора и утилизации отходов, деятельность по ликвидации загрязнений'],
            [6, 'F', 'Строительство'],
            [7, 'G', 'Торговля оптовая и розничная; ремонт автотранспортных средств и мотоциклов'],
            [8, 'H', 'Транспортировка и хранение'],
            [9, 'I', 'Деятельность гостиниц и предприятий общественного питания'],
            [10, 'J', 'Деятельность в области информации и связи'],
            [11, 'K', 'Деятельность финансовая и страховая'],
            [12, 'L', 'Деятельность по операциям с недвижимым имуществом'],
            [13, 'M', 'Деятельность профессиональная, научная и техническая'],
            [14, 'N', 'Деятельность административная и сопутствующие дополнительные услуги'],
            [15, 'O', 'Государственное управление и обеспечение военной безопасности; социальное обеспечение'],
            [16, 'P', 'Образование'],
            [17, 'Q', 'Деятельность в области здравоохранения и социальных услуг'],
            [18, 'R', 'Деятельность в области культуры, спорта, организации досуга и развлечений'],
            [19, 'S', 'Предоставление прочих видов услуг'],
            [20, 'T', 'Деятельность домашних хозяйств как работодателей; недифференцированная деятельность частных домашних хозяйств по производству товаров и оказанию услуг для собственного потребления'],
            [21, 'U', 'Деятельность экстерриториальных организаций и органов'],
        ]);

        $this->addColumn($this->tOkvedCode, 'section_id', $this->integer()->defaultValue(null));
        $this->addForeignKey($this->tOkvedCode . '_section_id', $this->tOkvedCode, 'section_id', $this->tOkvedCodeSection, 'id', 'RESTRICT', 'CASCADE');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey($this->tOkvedCode . '_section_id', $this->tOkvedCode);
        $this->dropColumn($this->tOkvedCode, 'section_id');

        $this->dropTable($this->tOkvedCodeSection);
    }
}


