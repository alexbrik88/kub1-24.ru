<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211108_055040_update_invoice_expenditure_item_can_controlled extends Migration
{
    public $expenseItemsIds = '94, 1';

    public function safeUp()
    {
        $this->execute("
            UPDATE `invoice_expenditure_item`
            SET `can_be_controlled` = 1
            WHERE `id` IN ({$this->expenseItemsIds})
        ");
    }

    public function safeDown()
    {
        $this->execute("
            UPDATE `invoice_expenditure_item`
            SET `can_be_controlled` = 0
            WHERE `id` IN ({$this->expenseItemsIds})
        ");
    }
}
