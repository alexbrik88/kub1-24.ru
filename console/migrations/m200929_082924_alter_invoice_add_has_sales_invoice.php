<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200929_082924_alter_invoice_add_has_sales_invoice extends Migration
{
    public $tableName = 'invoice';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'has_sales_invoice', $this->integer(1)->defaultValue(0)->after('has_packing_list'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'has_sales_invoice');
    }
}
