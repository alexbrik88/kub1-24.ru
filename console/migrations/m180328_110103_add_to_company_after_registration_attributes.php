<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180328_110103_add_to_company_after_registration_attributes extends Migration
{
    public $tableName = 'company';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'after_registration_expose_invoice_count', $this->integer()->defaultValue(0));
        $this->addColumn($this->tableName, 'after_registration_expose_other_documents_count', $this->integer()->defaultValue(0));
        $this->addColumn($this->tableName, 'after_registration_business_analyse_count', $this->integer()->defaultValue(0));
        $this->addColumn($this->tableName, 'after_registration_employee_plan_count', $this->integer()->defaultValue(0));
        $this->addColumn($this->tableName, 'after_registration_stock_control_count', $this->integer()->defaultValue(0));
        $this->addColumn($this->tableName, 'after_registration_bill_count', $this->integer()->defaultValue(0));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'after_registration_expose_invoice_count');
        $this->dropColumn($this->tableName, 'after_registration_expose_other_documents_count');
        $this->dropColumn($this->tableName, 'after_registration_business_analyse_count');
        $this->dropColumn($this->tableName, 'after_registration_employee_plan_count');
        $this->dropColumn($this->tableName, 'after_registration_stock_control_count');
        $this->dropColumn($this->tableName, 'after_registration_bill_count');
    }
}


