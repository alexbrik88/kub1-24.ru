<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200512_072309_alter_contractor_change_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%contractor}}', 'physical_firstname',  $this->string(45));
        $this->alterColumn('{{%contractor}}', 'physical_lastname',  $this->string(45));
        $this->alterColumn('{{%contractor}}', 'physical_patronymic',  $this->string(45));
    }

    public function safeDown()
    {
        $this->alterColumn('{{%contractor}}', 'physical_firstname',  $this->string(45)->notNull());
        $this->alterColumn('{{%contractor}}', 'physical_lastname',  $this->string(45)->notNull());
        $this->alterColumn('{{%contractor}}', 'physical_patronymic',  $this->string(45)->notNull());
    }
}
