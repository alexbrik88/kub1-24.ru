<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200908_052235_alter_menu_item_add_column extends Migration
{
    public function safeUp()
    {
        if (preg_match('/dbname=([^;]*)/', Yii::$app->db->dsn, $match)) {
            $db_name = $match[1];

            $this->execute("
                IF NOT EXISTS(
                    SELECT NULL
                    FROM INFORMATION_SCHEMA.COLUMNS
                    WHERE TABLE_SCHEMA = '{$db_name}'
                    AND TABLE_NAME = 'menu_item'
                    AND column_name = 'crm_item')
                THEN
                    ALTER TABLE `menu_item` ADD `crm_item` TINYINT(1) NULL DEFAULT '0';
                END IF;
            ");
        }
    }

    public function safeDown()
    {
        //
    }
}
