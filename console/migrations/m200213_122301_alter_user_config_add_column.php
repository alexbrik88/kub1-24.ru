<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200213_122301_alter_user_config_add_column extends Migration
{
    public $table = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->table, 'table_view_finance_odds', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'table_view_finance_odds');
    }
}
