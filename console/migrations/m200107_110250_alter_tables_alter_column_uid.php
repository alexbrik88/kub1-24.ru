<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200107_110250_alter_tables_alter_column_uid extends Migration
{
    public static $tables = [
        "{{%act}}",
        "{{%agent_report}}",
        "{{%agreement}}",
        "{{%invoice}}",
        "{{%invoice_facture}}",
        "{{%order_document}}",
        "{{%packing_list}}",
        "{{%payment_order}}",
        "{{%price_list}}",
        "{{%proxy}}",
        "{{%tax_declaration}}",
        "{{%upd}}",
    ];

    public function safeUp()
    {
        foreach (self::$tables as $table) {
            $this->alterColumn($table, 'uid', $this->string());
        }
    }

    public function safeDown()
    {
        foreach (self::$tables as $table) {
            $this->alterColumn($table, 'uid', $this->char(5));
        }
    }
}
