<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210331_104520_create_banking_email_file extends Migration
{
    public $table = 'banking_email_file';

    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'banking_email_id' => $this->integer()->notNull(),
            'date' => $this->date()->notNull(),
            'period_from' => $this->date(),
            'period_to' => $this->date(),
            'statements_count' => $this->integer(),
            'file_name' => $this->string()->notNull(),
            'is_deleted' => $this->boolean()->defaultValue(false),
            'is_uploaded' => $this->boolean()->defaultValue(false),
            'uploaded_at' => $this->timestamp()->defaultValue(null)
        ]);

        $this->addForeignKey('FK_banking_email_file_to_banking_email', $this->table, 'banking_email_id', 'banking_email', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_banking_email_file_to_banking_email', $this->table);
        $this->dropTable($this->table);
    }
}
