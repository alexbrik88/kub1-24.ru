<?php

use common\models\document\ScanDocument;
use console\components\db\Migration;
use frontend\models\Documents;
use yii\db\Schema;

class m190512_183729_update_scan_document extends Migration
{
    public function safeUp()
    {
        $offset = 0;
        $limit = 100;
        do {
            $scanArray = ScanDocument::find()->andWhere(['not', ['owner_id' => null]])->offset($offset)->limit($limit)->all();
            $offset += $limit;
            foreach ($scanArray as $scan) {
                $update = [];
                $owner = $scan->getOwner();
                if (!empty($owner->contractor)) {
                    $update['contractor_id'] = $owner->contractor->id;
                }
                if ($type = array_search($owner->className(), Documents::$docMap)) {
                    $update['document_type_id'] = $type;
                }
                if ($update) {
                    $scan->updateAttributes($update);
                }
            }
        } while (count($scanArray) == $limit);
    }

    public function safeDown()
    {
        $this-> update('{{%scan_document}}', [
            'contractor_id' => null,
            'document_type_id' => null,
        ], [
            'or',
            ['not', ['contractor_id' => null]],
            ['not', ['document_type_id' => null]],
        ]);
    }
}
