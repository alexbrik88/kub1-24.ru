<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190906_140835_create_amocrm_customer extends Migration
{
    public function up()
    {
        $this->createTable('{{amocrm_customer}}', [
            'id' => $this->primaryKey()->unsigned(),
            'status_id' => $this->integer()->unsigned()->notNull(),
            'name' => $this->string(200)->notNull(),
            'deleted' => $this->boolean()->notNull()->defaultValue(false)->comment('Признак удаления покупателя'),
            'next_price' => $this->float()->unsigned()->notNull()->defaultValue(0)->comment('Планируемая сумма следующей покупки'),
            'periodicity' => $this->tinyInteger()->unsigned()->notNull()->defaultValue(0)->comment('Переодичность'),
            'next_date' => $this->integer()->unsigned()->notNull()->comment('Планируемая дата следующей покупки'),
            'created_at' => $this->integer()->unsigned()->notNull()->comment('Дата создания'),
            'updated_at' => $this->integer()->unsigned()->notNull()->comment('Дата последнего изменения'),
        ], "COMMENT='Интеграция AMOcrm: покупатели'");

    }

    public function down()
    {
        $this->dropTable('{{amocrm_customer}}');

    }
}
