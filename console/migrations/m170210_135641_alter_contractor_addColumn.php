<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170210_135641_alter_contractor_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%contractor}}', 'payment_delay', $this->integer()->notNull()->defaultValue(10));
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%contractor}}', 'payment_delay');
    }
}


