<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201028_080926_alter_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_config', 'report_odds_own_funds', $this->boolean()->notNull()->defaultValue(true));
    }

    public function safeDown()
    {
        $this->dropColumn('user_config', 'report_odds_own_funds');
    }
}
