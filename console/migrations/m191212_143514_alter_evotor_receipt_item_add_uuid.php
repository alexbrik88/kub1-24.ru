<?php

use console\components\db\Migration;

class m191212_143514_alter_evotor_receipt_item_add_uuid extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{evotor_receipt_item}}', 'uuid', $this->char(36)->notNull()->comment('UUID товара'));
        $this->createIndex('evotor_receipt_item_uui', '{{evotor_receipt_item}}', 'uuid');
    }

    public function down()
    {
        $this->dropColumn('{{evotor_receipt_item}}', 'uuid');

    }
}
