<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170214_092217_add_order_act_table extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('order_act', [
            'id' => Schema::TYPE_PK,
            'order_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Заказ"',
            'act_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Акт"',
            'product_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Продукция"',
            'invoice_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Счёт"',
            'quantity' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Количество"',
            'amount_vat' => Schema::TYPE_INTEGER . ' COMMENT "Сумма НДС"',
            'amount_purchase_no_vat' => Schema::TYPE_INTEGER . ' COMMENT "Сумма покупки без учёта НДС"',
            'amount_purchase_with_vat' => Schema::TYPE_INTEGER . ' COMMENT "Сумма покупки с учётом НДС"',
            'amount_sales_no_vat' => Schema::TYPE_INTEGER . ' COMMENT "Сумма продажи без учёта НДС"',
            'amount_sales_with_vat' => Schema::TYPE_INTEGER . ' COMMENT "Сумма продажи с учётом НДС"'
        ], $tableOptions);

        $this->addForeignKey('order_act_to_order', 'order_act', 'order_id', 'order', 'id');
        $this->addForeignKey('order_act_to_act', 'order_act', 'act_id', 'act', 'id');
        $this->addForeignKey('order_act_to_product', 'order_act', 'product_id', 'product', 'id');
        $this->addForeignKey('order_act_to_invoice', 'order_act', 'invoice_id', 'invoice', 'id');
    }
    
    public function safeDown()
    {
        $this->dropTable('order_act');
    }
}


