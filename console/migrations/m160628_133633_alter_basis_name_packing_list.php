<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160628_133633_alter_basis_name_packing_list extends Migration
{
    public $tPackingList = 'packing_list';

    public function safeUp()
    {
        $this->alterColumn($this->tPackingList, 'basis_name', $this->string()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->alterColumn($this->tPackingList, 'basis_name', $this->string()->defaultValue('Договор'));
    }
}


