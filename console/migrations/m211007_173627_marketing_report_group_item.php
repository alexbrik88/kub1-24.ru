<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211007_173627_marketing_report_group_item extends Migration
{
    public $table = 'marketing_report_group_item';

    public function safeUp()
    {
        $table = $this->table;
        $this->createTable($table, [
            'company_id' => $this->integer()->notNull(),
            'group_id' => $this->integer()->notNull(),
            'utm_campaign' => $this->string()->notNull(),
            'campaign_name' => $this->string(),
        ]);

        $this->addPrimaryKey("PK_{$table}", $table, ['company_id', 'group_id', 'utm_campaign']);
        $this->addForeignKey("FK_{$table}_company", $table, 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("FK_{$table}_group", $table, 'group_id', 'marketing_report_group', 'id', 'CASCADE', 'CASCADE');

        // add idx into marketing_report!
        $this->createIndex('IDX_marketing_report_utm_campaign', 'marketing_report', 'utm_campaign');
    }

    public function safeDown()
    {
        $table = $this->table;
        $this->dropForeignKey("FK_{$table}_group", $table);
        $this->dropTable($table);

        // idx in marketing_report
        $this->dropIndex('IDX_marketing_report_utm_campaign', 'marketing_report');
    }
}
