<?php

use console\components\db\Migration;
use yii\db\Schema;
use common\models\selling\SellingSubscribe;
use common\models\service\Subscribe;
use common\models\service\PaymentType;

class m170904_215228_add_type_promo_code_77777_to_selling_subscribe extends Migration
{
    public $tableName = 'selling_subscribe';

    public function safeUp()
    {
        $this->delete($this->tableName, ['in', 'id', SellingSubscribe::find()
            ->andWhere(['in', 'type', [SellingSubscribe::TYPE_PROMO_CODE_INDIVIDUAL, SellingSubscribe::TYPE_PROMO_CODE_GROUPED]])
            ->all()
        ]);

        /* @var $subscribePromoCode Subscribe */
        foreach (Subscribe::find()
                     ->joinWith('payment')
                     ->andWhere(['and',
                         ['is_confirmed' => 1],
                         ['type_id' => PaymentType::TYPE_PROMO_CODE],
                     ])
                     ->all() as $key => $subscribePromoCode) {
            $type = null;
            if ($subscribePromoCode->payment->promoCode) {
                if ($subscribePromoCode->payment->promoCode->code == '77777') {
                    $type = 8;
                } else {
                    if ($subscribePromoCode->payment->promoCode->group_id == \common\models\service\PromoCodeGroup::GROUP_INDIVIDUAL) {
                        $type = 2;
                    } else {
                        $type = 3;
                    }
                }
            } else {
                continue;
            }
            $sellingSubscribe = new SellingSubscribe();
            $sellingSubscribe->company_id = $subscribePromoCode->company_id;
            $sellingSubscribe->subscribe_id = $subscribePromoCode->id;
            $sellingSubscribe->type = $type;
            $sellingSubscribe->main_type = 1;
            $sellingSubscribe->invoice_number = null;
            $sellingSubscribe->creation_date = $subscribePromoCode->created_at;
            $sellingSubscribe->activation_date = $subscribePromoCode->activated_at;
            $sellingSubscribe->end_date = $subscribePromoCode->expired_at;
            $sellingSubscribe->save();
        }
    }

    public function safeDown()
    {

    }
}


