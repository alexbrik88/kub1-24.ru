<?php

use yii\db\Schema;
use yii\db\Migration;

class m150520_134843_alter_Log_addForeignKey_LogEntity extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('log', 'model_name');
        $this->renameColumn('log', 'model_id', 'log_entity_id');
        $this->addColumn('log', 'log_entity_type_id', Schema::TYPE_INTEGER . ' NOT NULL');
        $this->addForeignKey('fk_log_to_log_entity_type', 'log', 'log_entity_type_id', 'log_entity_type', 'id');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('fk_log_to_log_entity_type', 'log');
        $this->dropColumn('log', 'log_entity_type_id');
        $this->renameColumn('log', 'log_entity_id', 'model_id');
        $this->addColumn('log', 'model_name', Schema::TYPE_STRING . '(255) NOT NULL');
    }
}
