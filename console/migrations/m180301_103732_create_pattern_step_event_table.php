<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pattern_step_event`.
 */
class m180301_103732_create_pattern_step_event_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('pattern_step_event', [
            'step_pattern_id' => $this->integer()->notNull(),
            'step_step' => $this->integer()->notNull(),
            'event_id' => $this->integer()->notNull(),
            'event_count' => $this->integer()->notNull(),
        ]);
        $this->addPrimaryKey('PRIMARY_KEY', 'pattern_step_event', [
            'step_pattern_id',
            'step_step',
            'event_id',
        ]);
        $this->addForeignKey(
            'FK_patternStepEvent_patternStep',
            'pattern_step_event',
            ['step_pattern_id', 'step_step'],
            'pattern_step',
            ['pattern_id', 'step'],
            'CASCADE'
        );
        $this->addForeignKey(
            'FK_patternStepEvent_event',
            'pattern_step_event',
            'event_id',
            'event',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('pattern_step_event');
    }
}
