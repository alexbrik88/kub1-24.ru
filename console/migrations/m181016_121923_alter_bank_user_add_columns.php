<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181016_121923_alter_bank_user_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('bank_user', 'company_id', $this->integer()->after('employee_id'));
        $this->addColumn('bank_user', 'inn', $this->string(12)->after('company_id'));
        $this->addColumn('bank_user', 'kpp', $this->string(9)->after('inn'));
        $this->addColumn('bank_user', 'is_chief', $this->boolean()->defaultValue(false)->after('kpp'));

        $this->addForeignKey('FK_bank_user_company', 'bank_user', 'company_id', 'company', 'id', 'CASCADE');

        $this->createIndex('inn', 'bank_user', 'inn');
        $this->createIndex('kpp', 'bank_user', 'kpp');
    }

    public function safeDown()
    {
        $this->dropIndex('inn', 'bank_user');
        $this->dropIndex('kpp', 'bank_user');

        $this->dropForeignKey('FK_bank_user_company', 'bank_user');

        $this->dropColumn('bank_user', 'is_chief');
        $this->dropColumn('bank_user', 'kpp');
        $this->dropColumn('bank_user', 'inn');
        $this->dropColumn('bank_user', 'company_id');
    }
}
