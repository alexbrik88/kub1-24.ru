<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181010_123753_create_price_list extends Migration
{
    public $tPriceList = 'price_list';
    public $tPriceListIncludeType = 'price_list_include_type';

    public function safeUp()
    {
        $this->createTable($this->tPriceListIncludeType, [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);

        $this->batchInsert($this->tPriceListIncludeType, ['id', 'name'], [
            [1, 'Весь товар'],
            [2, 'Товар в наличие'],
            [3, 'Настроить'],
        ]);

        $this->createTable($this->tPriceList, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'author_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'include_type_id' => $this->integer()->notNull(),
            'include_name_column' => $this->boolean()->defaultValue(true)->notNull(),
            'include_article_column' => $this->boolean()->defaultValue(false)->notNull(),
            'include_product_group_column' => $this->boolean()->defaultValue(false)->notNull(),
            'include_reminder_column' => $this->boolean()->defaultValue(true)->notNull(),
            'include_product_unit_column' => $this->boolean()->defaultValue(true)->notNull(),
            'include_description_column' => $this->boolean()->defaultValue(false)->notNull(),
            'include_price_column' => $this->boolean()->defaultValue(true)->notNull(),
            'sort_type' => $this->boolean()->defaultValue(false)->notNull(),
        ]);

        $this->addForeignKey($this->tPriceList . '_company_id', $this->tPriceList, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tPriceList . '_author_id', $this->tPriceList, 'author_id', 'employee', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tPriceList . '_include_type_id', $this->tPriceList, 'include_type_id', $this->tPriceListIncludeType, 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey($this->tPriceList . '_company_id', $this->tPriceList);
        $this->dropForeignKey($this->tPriceList . '_author_id', $this->tPriceList);
        $this->dropForeignKey($this->tPriceList . '_include_type_id', $this->tPriceList);

        $this->dropTable($this->tPriceList);
        $this->dropTable($this->tPriceListIncludeType);
    }
}


