<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200116_193958_create_yandex_direct_ad_group_report extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%yandex_direct_ad_group_report}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'employee_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'date' => $this->date()->notNull(),
            'ad_group_id' => $this->bigInteger(100)->notNull(),
            'campaign_id' => $this->bigInteger(100)->notNull(),
            'ad_group_name' => $this->string()->notNull(),
            'status' => $this->string()->defaultValue(null),
            'impressions_count' => $this->integer()->notNull(),
            'clicks_count' => $this->integer()->notNull(),
            'ctr' => $this->float(2)->notNull(),
            'cost' => $this->float(2)->notNull(),
            'avg_cpc' => $this->float(2)->defaultValue(null),
            'avg_page_views_count' => $this->float(2)->defaultValue(null),
            'conversion_rate' => $this->float(2)->defaultValue(null),
            'bounce_rate' => $this->float(2)->defaultValue(null),
            'revenue' => $this->float(2)->notNull(),
            'conversions_count' => $this->integer()->defaultValue(null),
            'goals_roi' => $this->float(2)->defaultValue(null),
            'profit' => $this->float(2)->notNull(),
            'cost_per_conversion' => $this->float(2)->defaultValue(null),
        ]);

        $this->addForeignKey('FK_yandex_direct_ad_group_report_to_company', '{{%yandex_direct_ad_group_report}}', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_yandex_direct_ad_group_report_to_employee', '{{%yandex_direct_ad_group_report}}', 'employee_id', 'employee', 'id', 'CASCADE', 'CASCADE');

        $this->alterColumn('{{%yandex_direct_campaign_report}}', 'campaign_id', $this->bigInteger(100)->notNull());
    }

    public function safeDown()
    {
        $this->dropTable('{{%yandex_direct_ad_group_report}}');

        $this->alterColumn('{{%yandex_direct_campaign_report}}', 'campaign_id', $this->integer()->notNull());
    }
}
