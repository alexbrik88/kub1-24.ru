<?php

use yii\db\Schema;
use yii\db\Migration;

class m151204_085022_alter_company_addColumn_activeSubscriptionId extends Migration
{
    public $tCompany = 'company';
    public $tSubscribe = 'service_subscribe';

    public function safeUp()
    {
        $this->addColumn($this->tCompany, 'active_subscribe_id', $this->integer()->defaultValue(null));
        $this->addForeignKey('fk_company_to_service_subscribe', $this->tCompany, 'active_subscribe_id', $this->tSubscribe, 'id');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('fk_company_to_service_subscribe', $this->tCompany);
        $this->dropColumn($this->tCompany, 'active_subscribe_id');
    }
}
