<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190201_091637_add_last_message_date_to_payment_reminder_message_contractor extends Migration
{
    public $tableName = '{{%payment_reminder_message_contractor}}';

    public function safeUp()
    {
        foreach (range(1, 10) as $number) {
            $this->addColumn($this->tableName, "last_message_date_{$number}", $this->date()->defaultValue(null)->after("message_{$number}"));
        }
    }

    public function safeDown()
    {
        foreach (range(1, 10) as $number) {
            $this->dropColumn($this->tableName, "last_message_date_{$number}");
        }
    }
}
