<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200110_110602_amocrm_leads_company_id_rename extends Migration
{
    const TABLE = '{{%amocrm_leads}}';

    public function up()
    {
        $this->renameColumn(self::TABLE, 'company_id', 'contact_id');
    }

    public function safeDown()
    {
        $this->renameColumn(self::TABLE, 'contact_id', 'company_id');
    }
}
