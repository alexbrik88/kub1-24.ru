<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201109_105001_alter_foreign_currency_account_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%foreign_currency_account}}', 'iban', $this->string(34)->after('rs'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%foreign_currency_account}}', 'iban');
    }
}
