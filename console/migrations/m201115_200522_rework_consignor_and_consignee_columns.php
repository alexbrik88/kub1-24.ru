<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201115_200522_rework_consignor_and_consignee_columns extends Migration
{
    public $tableName = 'loading_and_unloading_address';

    public function safeUp()
    {
        $this->dropColumn($this->tableName, 'consignor');
        $this->dropColumn($this->tableName, 'consignee');
        $this->addColumn($this->tableName, 'consignor_id', $this->integer()->defaultValue(null)->after('address'));
        $this->addColumn($this->tableName, 'consignee_id', $this->integer()->defaultValue(null)->after('consignor_id'));

        $this->addForeignKey('FK_loadingAndUnloadingAddressConsignor_contractor', $this->tableName, 'consignor_id', 'contractor', 'id');
        $this->addForeignKey('FK_loadingAndUnloadingAddressConsignee_contractor', $this->tableName, 'consignee_id', 'contractor', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_loadingAndUnloadingAddressConsignor_contractor', $this->tableName);
        $this->dropForeignKey('FK_loadingAndUnloadingAddressConsignee_contractor', $this->tableName);
        $this->dropColumn($this->tableName, 'consignor_id');
        $this->dropColumn($this->tableName, 'consignee_id');

        $this->addColumn($this->tableName, 'consignor', $this->string()->defaultValue(null)->after('address'));
        $this->addColumn($this->tableName, 'consignee', $this->string()->defaultValue(null)->after('consignor'));
    }
}
