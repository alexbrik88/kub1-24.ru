<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200715_082014_alter_config_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_config','report_income_help',$this->boolean()->notNull()->defaultValue(false));
        $this->addColumn('user_config','report_income_chart',$this->boolean()->notNull()->defaultValue(false));
        $this->addColumn('user_config', 'table_view_finance_income', $this->tinyInteger()->notNull()->defaultValue(1));
    }

    public function safeDown()
    {
        $this->dropColumn('user_config','report_income_help');
        $this->dropColumn('user_config','report_income_chart');
        $this->dropColumn('user_config', 'table_view_finance_income');
    }
}
