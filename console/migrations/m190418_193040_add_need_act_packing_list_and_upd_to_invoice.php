<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190418_193040_add_need_act_packing_list_and_upd_to_invoice extends Migration
{
    public $tableName = 'invoice';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'need_act', $this->boolean()->defaultValue(true));
        $this->addColumn($this->tableName, 'need_packing_list', $this->boolean()->defaultValue(true));
        $this->addColumn($this->tableName, 'need_upd', $this->boolean()->defaultValue(true));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'need_act');
        $this->dropColumn($this->tableName, 'need_packing_list');
        $this->dropColumn($this->tableName, 'need_upd');
    }
}
