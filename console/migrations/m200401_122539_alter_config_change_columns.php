<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200401_122539_alter_config_change_columns extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('user_config', 'table_view_finance_odds', $this->integer()->notNull()->defaultValue(1));
        $this->alterColumn('user_config', 'table_view_finance_plan_fact', $this->integer()->notNull()->defaultValue(1));
        $this->alterColumn('user_config', 'table_view_report_client', $this->integer()->notNull()->defaultValue(1));
        $this->alterColumn('user_config', 'table_view_report_employee', $this->integer()->notNull()->defaultValue(1));
        $this->alterColumn('user_config', 'table_view_report_invoice', $this->integer()->notNull()->defaultValue(1));
        $this->alterColumn('user_config', 'table_view_report_supplier', $this->integer()->notNull()->defaultValue(1));
        $this->alterColumn('user_config', 'table_view_finance_profit_loss', $this->integer()->notNull()->defaultValue(1));
        $this->alterColumn('user_config', 'table_view_finance_balance', $this->integer()->notNull()->defaultValue(1));
        $this->alterColumn('user_config', 'table_view_finance_expenses', $this->integer()->notNull()->defaultValue(1));
        $this->alterColumn('user_config', 'table_view_finance_pc', $this->integer()->notNull()->defaultValue(1));

        $this->execute('
                UPDATE user_config SET 
                    table_view_finance_odds = 1,
                    table_view_finance_plan_fact = 1,
                    table_view_report_client = 1,
                    table_view_report_employee = 1,
                    table_view_report_invoice = 1,
                    table_view_report_supplier = 1,
                    table_view_finance_profit_loss = 1,
                    table_view_finance_balance = 1,
                    table_view_finance_expenses = 1,
                    table_view_finance_pc = 1            
        ');
    }

    public function safeDown()
    {

    }
}
