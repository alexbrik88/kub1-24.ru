<?php

use common\models\amocrm\Task;
use common\models\employee\Employee;
use console\components\db\Migration;

class m200110_110203_amocrm_task_customer_id_insert_from_employee_id extends Migration
{
    const TABLE = '{{%amocrm_task}}';

    public function safeUp()
    {
        foreach (Task::find()->all() as $task) {
            /** @var Task $task */
            /** @noinspection PhpUndefinedFieldInspection */
            $task->company_id = Employee::findOne(['id' => $task->employee_id])->company_id;
            $task->save();
        }
    }

    public function down()
    {
        $this->update(self::TABLE, ['company_id' => 0]);
    }
}
