<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200202_180834_delete_duplicate_expenditure_item extends Migration
{
    public function safeUp()
    {
        $this->delete('{{%expense_item_flow_of_funds}}', ['expense_item_id' => 89]);
        $this->delete('{{%invoice_expenditure_item}}', ['id' => 89]);
    }

    public function safeDown()
    {
        echo "This migration not rolled back";
        return true;
    }
}
