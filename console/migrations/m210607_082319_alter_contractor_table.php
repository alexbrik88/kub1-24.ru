<?php

use yii\db\Migration;

class m210607_082319_alter_contractor_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%contractor}}';

    /**
     * @inheritDoc
     */
    public function up(): void
    {
        $this->addColumn(self::TABLE_NAME, 'activity_id', $this->bigInteger()->null()->after('company_id'));
        $this->addColumn(self::TABLE_NAME, 'campaign_id', $this->bigInteger()->null()->after('activity_id'));

        $this->createIndex('ck_activity_id', self::TABLE_NAME, ['activity_id']);
        $this->createIndex('ck_campaign_id', self::TABLE_NAME, ['campaign_id']);

        $this->addForeignKey(
            'fk_contractor__contractor_activity',
            self::TABLE_NAME,
            ['activity_id'],
            '{{%contractor_activity}}',
            ['id'],
            'SET NULL',
            'CASCADE'
        );

        $this->addForeignKey(
            'fk_contractor__contractor_campaign',
            self::TABLE_NAME,
            ['campaign_id'],
            '{{%contractor_campaign}}',
            ['id'],
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * @inheritDoc
     */
    public function down(): bool
    {
        return false;
    }
}
