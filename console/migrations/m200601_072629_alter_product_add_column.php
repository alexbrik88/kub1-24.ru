<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200601_072629_alter_product_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('product', 'mass_net', $this->string(45)->after('mass_gross')->comment('масса нетто'));
    }

    public function safeDown()
    {
        $this->dropColumn('product', 'mass_net');
    }
}
