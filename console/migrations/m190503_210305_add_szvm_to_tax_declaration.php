<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190503_210305_add_szvm_to_tax_declaration extends Migration
{
    public $tableName = 'tax_declaration';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'szvm', $this->boolean()->defaultValue(false)->after('balance'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'szvm');
    }
}
