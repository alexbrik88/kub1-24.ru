<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210806_070434_alter_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_config', 'contractor_income_item', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn('user_config', 'contractor_expenditure_item', $this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('user_config', 'contractor_income_item');
        $this->dropColumn('user_config', 'contractor_expenditure_item');
    }
}