<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201119_035357_create_profit_and_loss_model extends Migration
{
    const MODEL = 'finance_model';
    const SHOP = 'finance_model_shop';
    const MONTH = 'finance_model_month';

    public function safeUp()
    {
        /*
         * Model
         */
        $this->createTable(self::MODEL, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'employee_id' => $this->integer()->notNull(),
            'name' => $this->string(255)->notNull(),
            'template_id' => $this->integer(),
            'year_from' => $this->smallInteger(),
            'month_from' => $this->smallInteger(),
            'year_till' => $this->smallInteger(),
            'month_till' => $this->smallInteger(),
            'comment' => $this->text()->defaultValue(''),
            'total_revenue' => $this->integer()->defaultValue(0),
            'total_net_profit' => $this->integer()->defaultValue(0),
            'total_margin' => $this->double(4)->defaultValue(0),
            'total_sales_profit' => $this->double(4)->defaultValue(0),
            'total_capital_profit' => $this->double(4)->defaultValue(0),
        ]);

        $this->addForeignKey('FK_'.self::MODEL.'_to_company', self::MODEL, 'company_id', 'company', 'id');
        $this->addForeignKey('FK_'.self::MODEL.'_to_employee', self::MODEL, 'employee_id', 'employee', 'id');

        /*
         * Shop
         */
        $this->createTable(self::SHOP, [
            'id' => $this->primaryKey(),
            'model_id' => $this->integer()->notNull(),
            'type' => $this->tinyInteger()->notNull()->comment('1 - Оффлайн Магазин, 2 - Интернет Магазин'),
            'name' => $this->string(255)->notNull()
        ]);

        $this->addForeignKey('FK_'.self::SHOP.'_to_model', self::SHOP, 'model_id', self::MODEL, 'id', 'CASCADE');

        /*
         * Month
         */
        $this->createTable(self::MONTH, [
            'shop_id' => $this->integer()->notNull(),
            'year' => $this->smallInteger(),
            'month' => $this->smallInteger(),
            'check_count' => $this->integer()->defaultValue(0),
            'average_check' => $this->integer()->defaultValue(0),
            'visit_count' => $this->integer()->defaultValue(0),
            'conversion' => $this->decimal(8,2)->defaultValue(0),
        ]);

        $this->addForeignKey('FK_'.self::MONTH.'_to_shop', self::MONTH, 'shop_id', self::SHOP, 'id', 'CASCADE');
        $this->addPrimaryKey('PK_'.self::MONTH, self::MONTH, ['shop_id', 'year', 'month']);
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_'.self::MONTH.'_to_shop', self::MONTH);
        $this->dropForeignKey('FK_'.self::SHOP.'_to_model', self::SHOP);
        $this->dropForeignKey('FK_'.self::MODEL.'_to_company', self::MODEL);
        $this->dropForeignKey('FK_'.self::MODEL.'_to_employee', self::MODEL);
        $this->dropTable(self::MONTH);
        $this->dropTable(self::SHOP);
        $this->dropTable(self::MODEL);
    }
}
