<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180119_101150_add_income_item_id_to_cash_order_flows extends Migration
{
    public $tableName = 'cash_order_flows';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'income_item_id', $this->integer()->defaultValue(null));
        $this->addForeignKey($this->tableName . '_income_item_id', $this->tableName, 'income_item_id', 'invoice_income_item', 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_income_item_id', $this->tableName);
        $this->dropColumn($this->tableName, 'income_item_id');
    }
}


