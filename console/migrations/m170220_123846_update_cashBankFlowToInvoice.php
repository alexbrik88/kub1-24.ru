<?php

use common\models\cash\CashBankFlows;
use common\models\cash\CashBankFlowToInvoice;
use common\models\document\Invoice;
use console\components\db\Migration;
use yii\db\Schema;

class m170220_123846_update_cashBankFlowToInvoice extends Migration
{
    public function safeUp()
    {
        $B = CashBankFlows::tableName();
        $F = CashBankFlowToInvoice::tableName();
        $I = Invoice::tableName();
        $flowArray = CashBankFlowToInvoice::find()
            ->select(['flow_id', 'invoice_id'])
            ->leftJoin($I, "$I.id = $F.invoice_id")
            ->orderBy(['document_date' => SORT_ASC])
            ->asArray()
            ->all();

        if ($flowArray) {
            foreach ($flowArray as $row) {
                $flow = CashBankFlows::findOne($row['flow_id']);
                $invoice = Invoice::findOne($row['invoice_id']);
                if ($flow && $invoice) {
                    $flow->unlinkInvoice($invoice);
                    $flow->linkInvoice($invoice);
                }
            }
        }
    }
    
    public function safeDown()
    {

    }
}


