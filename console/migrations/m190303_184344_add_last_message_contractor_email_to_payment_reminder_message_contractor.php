<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190303_184344_add_last_message_contractor_email_to_payment_reminder_message_contractor extends Migration
{
    public $tableName = '{{%payment_reminder_message_contractor}}';

    public function safeUp()
    {
        foreach (range(1, 10) as $number) {
            $this->addColumn($this->tableName, "last_message_contractor_email_{$number}", $this->string()->defaultValue(null)->after("last_message_date_{$number}"));
        }
    }

    public function safeDown()
    {
        foreach (range(1, 10) as $number) {
            $this->dropColumn($this->tableName, "last_message_contractor_email_{$number}");
        }
    }
}
