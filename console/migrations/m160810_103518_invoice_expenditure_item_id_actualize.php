<?php
use console\components\db\Migration;

class m160810_103518_invoice_expenditure_item_id_actualize extends Migration
{
    public function safeUp()
    {
        $this->addColumn('invoice_expenditure_item', 'sort', $this->smallInteger()->defaultValue(100));

        $this->execute("
            UPDATE {{invoice_expenditure_item}} SET [[name]]='Хоз. расходы' WHERE [[name]]='Хозяйственные нужды';
            UPDATE {{invoice_expenditure_item}} SET [[name]]='Прочие расходы' WHERE [[name]]='Прочее';
        ");

        $this->batchInsert('invoice_expenditure_item', ['name', 'sort'], [
            ['Оплата поставщику', 1],
            ['Возврат', 100],
            ['Выплата дивидендов', 100],
            ['Выплата подотчетным лицам', 100],
            ['Комиссия банка', 100],
            ['Погашение займа', 100],
            ['Услуги', 100],
        ]);
    }

    public function safeDown()
    {
        $this->dropColumn('invoice_expenditure_item', 'sort');

        $builder = new \yii\db\QueryBuilder($this->db);
        $params = [];
        $sql = $builder->update('invoice_expenditure_item', ['name' => 'Хозяйственные нужды'], [
            'name' => 'Хоз. расходы',
        ], $params);
        $this->execute($sql, $params);

        $params = [];
        $sql = $builder->update('invoice_expenditure_item', ['name' => 'Прочее'], [
            'name' => 'Прочие расходы',
        ], $params);
        $this->execute($sql, $params);

        $params = [];
        $sql = $builder->delete('invoice_expenditure_item', [
            'name' => [
                'Оплата поставщику', 'Возврат', 'Выплата дивидендов', 'Выплата подотчетным лицам',
                'Комиссия банка', 'Погашение займа', 'Услуги',
            ],
        ], $params);
        $this->execute($sql, $params);
    }
}


