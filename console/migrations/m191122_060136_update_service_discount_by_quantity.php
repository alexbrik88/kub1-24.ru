<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191122_060136_update_service_discount_by_quantity extends Migration
{
    public function safeUp()
    {
        $this->execute('
            UPDATE {{%service_discount_by_quantity}}
            SET {{%service_discount_by_quantity}}.[[percent]] = {{%service_discount_by_quantity}}.[[percent]] + 0.0001
            WHERE {{%service_discount_by_quantity}}.[[percent]] LIKE "%.6666"
        ');
    }

    public function safeDown()
    {
        $this->execute('
            UPDATE {{%service_discount_by_quantity}}
            SET {{%service_discount_by_quantity}}.[[percent]] = {{%service_discount_by_quantity}}.[[percent]] - 0.0001
            WHERE {{%service_discount_by_quantity}}.[[percent]] LIKE "%.6667"
        ');
    }
}