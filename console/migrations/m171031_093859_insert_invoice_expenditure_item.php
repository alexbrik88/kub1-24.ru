<?php

use console\components\db\Migration;
use yii\db\Schema;

class m171031_093859_insert_invoice_expenditure_item extends Migration
{
    public $tableName = 'invoice_expenditure_item';

    public function safeUp()
    {
        $this->insert($this->tableName, [
            'id' => 44,
            'name' => 'Перевод собственных средств',
        ]);
    }
    
    public function safeDown()
    {
        $this->delete($this->tableName, ['id' => 44]);
    }
}
