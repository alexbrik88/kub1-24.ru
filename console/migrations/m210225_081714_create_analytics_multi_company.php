<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210225_081714_create_analytics_multi_company extends Migration
{
    public $table = '{{%analytics_multi_company}}'; 
    
    public function safeUp()
    {
        $this->createTable($this->table, [
            'employee_id' => $this->integer()->notNull(),
            'primary_company_id' => $this->integer()->notNull(),
            'company_id' => $this->integer()->notNull(),
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', $this->table, ['employee_id', 'primary_company_id', 'company_id']);

        $this->addForeignKey('FK_analytics_multi_company_to_employee', $this->table, 'employee_id', 'employee', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_analytics_multi_company_to_company', $this->table, 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_analytics_multi_company_to_primary_company_id', $this->table, 'primary_company_id', 'company', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_analytics_multi_company_to_employee', $this->table);
        $this->dropForeignKey('FK_analytics_multi_company_to_company', $this->table);
        $this->dropForeignKey('FK_analytics_multi_company_to_primary_company_id', $this->table);
        
        $this->dropTable($this->table);
    }
}