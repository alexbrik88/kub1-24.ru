<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170109_182139_alter_packingList_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%packing_list}}', 'is_original_updated_at', $this->integer()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%packing_list}}', 'is_original_updated_at');
    }
}


