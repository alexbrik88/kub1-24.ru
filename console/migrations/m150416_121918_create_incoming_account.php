<?php

use yii\db\Schema;
use yii\db\Migration;

class m150416_121918_create_incoming_account extends Migration
{
    public function up()
    {
        $this->createTable('incoming_account', [
            'id' => Schema::TYPE_PK,
            'company_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Покупатель"',
            'document_path' => Schema::TYPE_STRING . ' Comment "Файл документа"',
            'contractor_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Пожет быть контрагент только с типом 0"',
            'status_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'change_status_date' => Schema::TYPE_DATE . ' NOT NULL COMMENT "Меняется при смене статуса"',
            'change_status_employee_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Присваивается при смене статуса"',
            'creation_date' => Schema::TYPE_DATE,
            'creation_employee_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'incoming_account_date' => Schema::TYPE_DATE,
            'account_number' => Schema::TYPE_INTEGER . ' NOT NULL',
            'pay_to' => Schema::TYPE_DATE . ' NOT NULL COMMENT "Оплатить до"',
            'account_type' => Schema::TYPE_BOOLEAN . ' NOT NULL COMMENT "1 - товар, 0 - услуга"',
            'item_costs' => Schema::TYPE_INTEGER . ' NOT NULL',
            'customer_bank_name' => Schema::TYPE_STRING . ' NOT NULL COMMENT "Наименование банка покупателя"',
            'customer_itn' => Schema::TYPE_INTEGER . ' COMMENT "ИНН покупателя"',
            'customer_ppc' => Schema::TYPE_INTEGER . ' COMMENT "КПП покупателя"',
            'customer_egrip' => Schema::TYPE_INTEGER . ' COMMENT "ЕГРИП покупателя"',
            'customer_okpo' => Schema::TYPE_INTEGER . ' COMMENT "ОКПО покупателя"',
            'customer_bic' => Schema::TYPE_INTEGER . ' COMMENT "БИК покупателя"',
            'customer_corresponding_account' => Schema::TYPE_INTEGER . ' COMMENT "к/с покупателя"',
            'customer_current_account' => Schema::TYPE_INTEGER . ' COMMENT "р/с покупателя"',
            'customer_full_name' => Schema::TYPE_STRING,
            'customer_short_name' => Schema::TYPE_STRING,
            'customer_phone' => Schema::TYPE_STRING,
            'customer_director_position' => Schema::TYPE_STRING . ' COMMENT "Должность руководителя покупателя"',
            'customer_director_surname' => Schema::TYPE_STRING . ' COMMENT "Фамилия руководителя покупателя"',
            'customer_director_name' => Schema::TYPE_STRING . ' COMMENT "Имя руководителя покупателя (инициал)"',
            'customer_director_last_name' => Schema::TYPE_STRING . ' COMMENT "Отчество руководителя покупателя (инициал)"',
            'customer_chief_accountant_surname' => Schema::TYPE_STRING . ' COMMENT "Фамилия главного бухгалтера покупателя"',
            'customer_chief_accountant_name' => Schema::TYPE_STRING . ' COMMENT "Имя главного бухгалтера покупателя(инициал)"',
            'customer_chief_accountant_last_name' => Schema::TYPE_STRING . ' COMMENT "Отчество главного бухгалтера покупателя(инициал)"',
            'vendor_full_name' => Schema::TYPE_STRING . ' COMMENT "Полное название поставщика"',
            'vendor_short_name' => Schema::TYPE_STRING . ' COMMENT "Краткое имя поставщика"',
            'vendor_itn' => Schema::TYPE_INTEGER . ' COMMENT "ИНН поставщика"',
            'vendor_ppc' => Schema::TYPE_INTEGER . ' COMMENT "КПП поставщика"',
            'vendor_legal_address' => Schema::TYPE_STRING . ' COMMENT "Юридический адрес поставщика"',
            'vendor_bank_name' => Schema::TYPE_STRING . ' COMMENT "Наименование банка поставщика"',
            'vendor_bic' => Schema::TYPE_INTEGER . ' COMMENT "Бик поставщика"',
            'vendor_corresponding_account' => Schema::TYPE_INTEGER . ' COMMENT "К/с поставщика"',
            'vendor_current_account' => Schema::TYPE_INTEGER . ' COMMENT "Р/с поставщика"',
            'incoming_account_structure' => Schema::TYPE_STRING . ' NOT NULL COMMENT "Состав входящего счёта"',
            'total_price' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Итого"',
            'vat_exists' => Schema::TYPE_BOOLEAN . ' NOT NULL COMMENT "1 - если есть товар с НДС, 0 - нет"',
            'vat_total' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "В т.ч. НДС"',
            'account_price' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Всего к оплате = Итого"',
            'items_count' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Всего наименований"',
            'sum_in_words' => Schema::TYPE_STRING . ' NOT NULL COMMENT "Сумма прописью"',
            'payment_variant_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Форма оплаты"',
            'partial_payment_size' => Schema::TYPE_INTEGER . ' COMMENT "Присутствует если оплачено частично"'
        ]);

        $this->addForeignKey('account_contractor_rel', 'incoming_account', 'contractor_id', 'contractor', 'id');
        $this->addForeignKey('account_company_rel', 'incoming_account', 'company_id', 'company', 'id');
        $this->addForeignKey('account_employee_change_rel', 'incoming_account', 'change_status_employee_id', 'employee', 'id');
        $this->addForeignKey('account_employee_create_rel', 'incoming_account', 'creation_employee_id', 'employee', 'id');
        $this->addForeignKey('account_payment_variant_rel', 'incoming_account', 'payment_variant_id', 'payment_variant', 'id');
    }

    public function down()
    {
        $this->dropTable('incoming_account');
    }
}
