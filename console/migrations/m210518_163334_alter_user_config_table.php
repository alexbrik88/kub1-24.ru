<?php

use yii\db\Migration;

class m210518_163334_alter_user_config_table extends Migration
{
    private const TABLE_NAME = '{{%user_config}}';

    /**
     * @inheritDoc
     */
    public function up(): void
    {
        $this->addColumn(self::TABLE_NAME, 'crm_task', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn(self::TABLE_NAME, 'crm_task_date_end', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn(self::TABLE_NAME, 'crm_task_number', $this->boolean()->notNull()->defaultValue(true));
    }

    /**
     * @inheritDoc
     */
    public function down(): void
    {
        $this->dropColumn(self::TABLE_NAME, 'crm_task');
        $this->dropColumn(self::TABLE_NAME, 'crm_task_date_end');
        $this->dropColumn(self::TABLE_NAME, 'crm_task_number');
    }
}
