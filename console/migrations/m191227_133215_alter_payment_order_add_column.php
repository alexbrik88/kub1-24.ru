<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191227_133215_alter_payment_order_add_column extends Migration
{
    public function safeUp()
    {
        $this->execute('
            ALTER TABLE {{%payment_order}} CONVERT TO CHARACTER SET "utf8" COLLATE "utf8_general_ci"
        ');

        $this->addColumn('{{%payment_order}}', 'company_bank_city', $this->string()->after('company_bank_name'));
        $this->addColumn('{{%payment_order}}', 'contractor_bank_city', $this->string()->after('contractor_bank_name'));

        $this->execute('
            UPDATE {{%payment_order}}
            LEFT JOIN {{%bik_dictionary}}
                ON {{%bik_dictionary}}.[[bik]] collate utf8_unicode_ci = {{%payment_order}}.[[company_bik]] collate utf8_unicode_ci
            SET {{%payment_order}}.[[company_bank_city]] = {{%bik_dictionary}}.[[city]]
        ');
        $this->execute('
            UPDATE {{%payment_order}}
            LEFT JOIN {{%bik_dictionary}}
                ON {{%bik_dictionary}}.[[bik]] collate utf8_unicode_ci = {{%payment_order}}.[[contractor_bik]] collate utf8_unicode_ci
            SET {{%payment_order}}.[[contractor_bank_city]] = {{%bik_dictionary}}.[[city]]
        ');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%payment_order}}', 'contractor_bank_city');
        $this->dropColumn('{{%payment_order}}', 'company_bank_city');
    }
}
