<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200225_141329_alter_config_add_column extends Migration
{
    public $table = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->table, 'table_view_price_list', $this->integer()->notNull()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'table_view_price_list');
    }
}
