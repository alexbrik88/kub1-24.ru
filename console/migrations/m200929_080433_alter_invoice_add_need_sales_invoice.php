<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200929_080433_alter_invoice_add_need_sales_invoice extends Migration
{
    public $tableName = 'invoice';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'need_sales_invoice', $this->integer(1)->defaultValue(0)->after('need_packing_list'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'need_sales_invoice');
    }
}
