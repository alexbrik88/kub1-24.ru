<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170519_095559_alter_cashBankStatementUpload_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cash_bank_statement_upload}}', 'source', $this->integer()->notNull()->defaultValue(1));
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%cash_bank_statement_upload}}', 'source');
    }
}
