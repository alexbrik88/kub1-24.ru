<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210518_194145_update_marketing_plan_item extends Migration
{
    public function safeUp()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS = 0');
        $this->truncateTable('marketing_plan_item');
        $this->truncateTable('marketing_plan');
        $this->execute('SET FOREIGN_KEY_CHECKS = 1');

        $this->renameColumn('marketing_plan_item', 'total_expenses_amount', 'total_additional_expenses_amount');
        $this->addColumn('marketing_plan_item', 'additional_expenses_amount_for_one_sale', $this->bigInteger(20)->unsigned()->after('total_additional_expenses_amount'));
        $this->renameColumn('marketing_plan_item', 'budget_click_amount', 'click_amount');
        $this->dropColumn('marketing_plan_item', 'final_costs_click_amount');
        $this->renameColumn('marketing_plan_item', 'budget_registration_amount', 'registration_amount');
        $this->dropColumn('marketing_plan_item', 'final_costs_registration_amount');
        $this->renameColumn('marketing_plan_item', 'budget_active_user_amount', 'active_user_amount');
        $this->dropColumn('marketing_plan_item', 'final_costs_user_amount');
        $this->renameColumn('marketing_plan_item', 'budget_sale_amount', 'sale_amount');
        $this->dropColumn('marketing_plan_item', 'final_costs_sale_amount');
    }

    public function safeDown()
    {
        $this->execute('SET FOREIGN_KEY_CHECKS = 0');
        $this->truncateTable('marketing_plan_item');
        $this->truncateTable('marketing_plan');
        $this->execute('SET FOREIGN_KEY_CHECKS = 1');

        $this->renameColumn('marketing_plan_item', 'total_additional_expenses_amount', 'total_expenses_amount');
        $this->dropColumn('marketing_plan_item', 'additional_expenses_amount_for_one_sale');
        $this->renameColumn('marketing_plan_item', 'click_amount', 'budget_click_amount');
        $this->addColumn('marketing_plan_item', 'final_costs_click_amount', $this->bigInteger(20)->unsigned()->after('budget_click_amount'));
        $this->renameColumn('marketing_plan_item', 'registration_amount', 'budget_registration_amount');
        $this->addColumn('marketing_plan_item', 'final_costs_registration_amount', $this->bigInteger(20)->unsigned()->after('budget_registration_amount'));
        $this->renameColumn('marketing_plan_item', 'active_user_amount', 'budget_active_user_amount');
        $this->addColumn('marketing_plan_item', 'final_costs_user_amount', $this->bigInteger(20)->unsigned()->after('budget_active_user_amount'));
        $this->renameColumn('marketing_plan_item', 'sale_amount', 'budget_sale_amount');
        $this->addColumn('marketing_plan_item', 'final_costs_sale_amount', $this->bigInteger(20)->unsigned()->after('budget_sale_amount'));
    }
}
