<?php

use console\components\db\Migration;
use yii\db\Schema;

class m151214_083128_alter_subscribePromoCode extends Migration
{
    public $tType = 'service_payment_promo_code_type';

    public function safeUp()
    {
        $this->createTable($this->tType, [
            'id' => $this->primaryKey(),
            'name' => $this->string(45),
        ], $this->tableOptions);
    }
    
    public function safeDown()
    {
        $this->dropTable($this->tType);
    }
}


