<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181217_142621_alter_taxrobot_payment_order extends Migration
{
    public $table = 'taxrobot_payment_order';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn($this->table, 'payment_order_id', $this->integer()->defaultValue(null));

        $this->addForeignKey($this->table . '_payment_order_id', $this->table, 'payment_order_id', 'payment_order', 'id', 'SET NULL');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey($this->table . '_payment_order_id', $this->table);
        $this->dropColumn($this->table, 'payment_order_id');
    }
}
