<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210408_195040_update_packing_list_order_sum_2 extends Migration
{
    public function safeUp()
    {
        $this->execute('
            UPDATE `packing_list` 
            JOIN (
                SELECT op.packing_list_id, SUM(o.purchase_price_with_vat * op.quantity) AS summ
                FROM `order_packing_list` op
                LEFT JOIN `order` `o` ON op.order_id = o.id
                GROUP BY packing_list_id
            ) t ON t.packing_list_id = packing_list.id
            SET `packing_list`.`orders_sum` = t.summ
            WHERE packing_list.type = 1
        ');

        $this->execute('
            UPDATE `packing_list` 
            JOIN (
                SELECT op.packing_list_id, SUM(o.selling_price_with_vat * op.quantity) AS summ
                FROM `order_packing_list` op
                LEFT JOIN `order` `o` ON op.order_id = o.id
                GROUP BY packing_list_id
            ) t ON t.packing_list_id = packing_list.id
            SET `packing_list`.`orders_sum` = t.summ
            WHERE packing_list.type = 2
        ');
    }

    public function safeDown()
    {
        // no down
    }
}
