<?php

use console\components\db\Migration;
use common\models\evotor\Store;
use common\models\evotor\Device;
use common\models\evotor\EmployeeStore;
use common\models\evotor\Product;
use common\models\evotor\Receipt;

class m191210_121303_alter_evotor_store_uuid extends Migration
{
    public function safeUp()
    {
        Store::deleteAll();
        Device::deleteAll();
        EmployeeStore::deleteAll();
        Product::deleteAll();
        Receipt::deleteAll();
        $this->dropForeignKey('evotor_employee_store_store_id', '{{evotor_employee_store}}');
        $this->dropColumn('{{evotor_store}}', 'id');
        $this->addColumn('{{evotor_store}}', 'uuid', $this->char(36)->notNull()->unique()->comment('UUID магазина Эвтор'));
        $this->addColumn('{{evotor_store}}', 'id', $this->primaryKey(11)->unsigned()->notNull()->first());
        $this->alterColumn('{{evotor_device}}', 'store_id', $this->integer(11)->unsigned()->notNull()->comment('ID магазина (evotor_store.id)'));
        $this->addForeignKey('evotor_device_store_id', '{{evotor_device}}', 'store_id',
            '{{evotor_store}}', 'id', 'CASCADE', 'CASCADE');
        $this->alterColumn('{{evotor_employee_store}}', 'store_id', $this->integer(11)->unsigned()->notNull()->comment('ID магазина (evotor_store.id)'));
        $this->addForeignKey('evotor_employee_store_store_id', '{{evotor_employee_store}}', 'store_id',
            '{{evotor_store}}', 'id', 'CASCADE', 'CASCADE');
        $this->alterColumn('{{evotor_product}}', 'store_id', $this->integer(11)->unsigned()->notNull()->comment('ID магазина (evotor_store.id)'));
        $this->addForeignKey('evotor_product_store_id', '{{evotor_product}}', 'store_id',
            '{{evotor_store}}', 'id', 'CASCADE', 'CASCADE');
        $this->alterColumn('{{evotor_receipt}}', 'store_id', $this->integer(11)->unsigned()->notNull()->comment('ID магазина (evotor_store.id)'));
        $this->addForeignKey('evotor_receipt_store_id', '{{evotor_receipt}}', 'store_id',
            '{{evotor_store}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        Store::deleteAll();
        Device::deleteAll();
        EmployeeStore::deleteAll();
        Product::deleteAll();
        Receipt::deleteAll();
        $this->dropForeignKey('evotor_device_store_id', '{{evotor_device}}');
        $this->dropForeignKey('evotor_employee_store_store_id', '{{evotor_employee_store}}');
        $this->dropForeignKey('evotor_product_store_id', '{{evotor_product}}');
        $this->dropForeignKey('evotor_receipt_store_id', '{{evotor_receipt}}');
        $this->dropColumn('{{evotor_store}}', 'uuid');
        $this->dropColumn('{{evotor_store}}', 'id');
        $this->addColumn('{{evotor_store}}', 'id', "BINARY(16) NOT NULL PRIMARY KEY COMMENT 'UUID Эвотора'");
        $this->alterColumn('{{evotor_device}}', 'store_id', "BINARY(16) NOT NULL COMMENT 'ID магазина'");
        $this->alterColumn('{{evotor_employee_store}}', 'store_id', "BINARY(16) NOT NULL COMMENT 'ID магазина'");
        $this->alterColumn('{{evotor_product}}', 'store_id', "BINARY(16) NOT NULL COMMENT 'ID магазина'");
        $this->alterColumn('{{evotor_receipt}}', 'store_id', "BINARY(16) NOT NULL COMMENT 'ID магазина'");
        $this->addForeignKey('evotor_employee_store_store_id', '{{evotor_employee_store}}', 'store_id',
            '{{evotor_store}}', 'id', 'CASCADE', 'CASCADE');
    }
}
