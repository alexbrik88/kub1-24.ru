<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211203_104648_alter_rent_attribute_alter_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%rent_attribute}}', 'type', "ENUM('integer','float','string','enum','set','date')  NOT NULL COMMENT 'Тип хранимых данных'");
    }

    public function safeDown()
    {
        $this->alterColumn('{{%rent_attribute}}', 'type', "ENUM('integer','float','string','enum','set')  NOT NULL COMMENT 'Тип хранимых данных'");
    }
}
