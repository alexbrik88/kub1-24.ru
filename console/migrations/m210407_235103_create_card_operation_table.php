<?php

use yii\db\Migration;

class m210407_235103_create_card_operation_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%card_operation}}';

    /**
     * @inheritDoc
     */
    public function up(): void
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->bigPrimaryKey()->notNull(),
            'account_id' => $this->bigInteger()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'contractor_id' => $this->integer()->null(),
            'expenditure_item_id' => $this->integer()->null(),
            'income_item_id' => $this->integer()->null(),
            'amount' => $this->bigInteger()->notNull(),
            'flow_type' => $this->tinyInteger(1)->notNull()->unsigned(),
            'is_deleted' => $this->boolean()->notNull()->defaultValue(false),
            'uuid' => $this->string()->notNull(),
            'description' => $this->string()->notNull()->defaultValue(''),
            'recognition_date' => $this->date()->null(),
            'date' => $this->date()->notNull(),
            'time' => $this->time()->notNull(),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->createForeignKey('account_id', 'card_account', 'id', 'CASCADE');
        $this->createForeignKey('company_id', 'company', 'id', 'CASCADE');
        $this->createForeignKey('contractor_id', 'contractor', 'id', 'SET NULL');
        $this->createForeignKey('expenditure_item_id', 'invoice_expenditure_item', 'id', 'SET NULL');
        $this->createForeignKey('income_item_id', 'invoice_income_item', 'id', 'SET NULL');
        $this->createIndex('uk_card_operation', self::TABLE_NAME, ['account_id', 'company_id', 'uuid'], true);
    }

    /**
     * @inheritDoc
     */
    public function down(): void
    {
        $this->dropTable(self::TABLE_NAME);
    }

    /**
     * @param string $column
     * @param string $refTable
     * @param string $refColumn
     * @param string $onDelete
     * @return void
     */
    public function createForeignKey(string $column, string $refTable, string $refColumn, string $onDelete): void
    {
        $this->createIndex(sprintf('ck_%s', $column), self::TABLE_NAME, [$column]);
        $this->addForeignKey(
            sprintf('fk_card_operation_%s__%s', $column, $refTable),
            self::TABLE_NAME,
            [$column],
            "{{%$refTable}}",
            [$refColumn],
            $onDelete,
            'CASCADE',
        );
    }
}
