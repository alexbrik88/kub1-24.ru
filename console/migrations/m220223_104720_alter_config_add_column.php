<?php

use console\components\db\Migration;
use yii\db\Schema;

class m220223_104720_alter_config_add_column extends Migration
{
    public $tableName = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'order_document_upd', $this->boolean()->notNull()->defaultValue(false));
        $this->addCommentOnColumn($this->tableName, 'order_document_upd', 'Показ колонки "УПД" в списке заказов');
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'order_document_upd');
    }
}
