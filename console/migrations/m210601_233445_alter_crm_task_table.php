<?php

use console\components\db\Migration;

class m210601_233445_alter_crm_task_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%crm_task}}';

    /**
     * @inheritDoc
     */
    public function up(): void
    {
        $this->dropColumn(self::TABLE_NAME, 'project_id');
        $this->dropColumn(self::TABLE_NAME, 'task_name');
        $this->dropColumn(self::TABLE_NAME, 'date_end');
        $this->renameColumn(self::TABLE_NAME, 'date_begin', 'date');
        $this->alterColumn(self::TABLE_NAME, 'contractor_id', $this->integer()->notNull());
    }

    /**
     * @inheritDoc
     */
    public function down(): bool
    {
        return false;
    }
}
