<?php

use console\components\db\Migration;
use yii\db\Schema;

class m151222_142328_alter_invoice_facture_update_created_at_and_updated_at extends Migration
{
    public $tInvoiceFacture = 'invoice_facture';

    public function safeUp()
    {
        $this->addColumn($this->tInvoiceFacture, 'created', $this->integer(11)->notNull());
        $this->update($this->tInvoiceFacture, [
            'created' => new \yii\db\Expression('UNIX_TIMESTAMP (created_at)'),
        ]);
        $this->dropColumn($this->tInvoiceFacture, 'created_at');
        $this->renameColumn($this->tInvoiceFacture, 'created', 'created_at');

        $this->addColumn($this->tInvoiceFacture, 'updated', $this->integer(11)->notNull());
        $this->update($this->tInvoiceFacture, [
            'updated' => new \yii\db\Expression('UNIX_TIMESTAMP (status_out_updated_at)'),
        ]);
        $this->dropColumn($this->tInvoiceFacture, 'status_out_updated_at');
        $this->renameColumn($this->tInvoiceFacture, 'updated', 'status_out_updated_at');
        $this->update($this->tInvoiceFacture, ['status_out_updated_at' => null], ['status_out_updated_at' => 0]);
    }

    public function safeDown()
    {
        echo 'This migration should not be rolled back';
    }
}


