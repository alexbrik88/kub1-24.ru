<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190714_174954_alter_scan_upload_email_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('scan_upload_email', 'password', $this->string(16)->notNull()->defaultValue(''));
    }

    public function safeDown()
    {
        $this->dropColumn('scan_upload_email', 'password');
    }
}
