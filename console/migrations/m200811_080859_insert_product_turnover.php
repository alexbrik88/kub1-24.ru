<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200811_080859_insert_product_turnover extends Migration
{
    public function safeUp()
    {
        $this->truncateTable('{{product_turnover}}');

        $this->execute('
            INSERT INTO `product_turnover` (
                `document_id`,
                `order_id`,
                `order_table`,
                `document_table`,
                `invoice_id`,
                `contractor_id`,
                `company_id`,
                `date`,
                `type`,
                `production_type`,
                `is_invoice_actual`,
                `is_document_actual`,
                `purchase_price`,
                `price_one`,
                `quantity`,
                `total_amount`,
                `purchase_amount`,
                `year`,
                `month`,
                `product_id`
            )
            SELECT
                `document`.`id`,
                `order`.`id`,
                "order_act",
                "act",
                `invoice`.`id`,
                `invoice`.`contractor_id`,
                `invoice`.`company_id`,
                `document`.`document_date`,
                `document`.`type`,
                `product`.`production_type`,
                IF(`invoice`.`is_deleted` = 0 AND `invoice`.`invoice_status_id` IN (1, 2, 3, 4, 6, 7, 8), 1, 0),
                IF(`document`.`status_out_id` IN (1, 2, 3, 4), 1, 0),
                IFNULL(`product`.`price_for_buy_with_nds`, 0),
                IF(`document`.`type` = 1, `order`.`purchase_price_with_vat`, `order`.`selling_price_with_vat`),
                IFNULL(`document_order`.`quantity`, 0),
                ROUND(IF(`document`.`type` = 1, `order`.`purchase_price_with_vat`, `order`.`selling_price_with_vat`) * IFNULL(`document_order`.`quantity`, 0), IF(`invoice`.`price_precision` = 4, 2, 0)),
                IF(`invoice`.`type` = 2 AND `product`.`production_type` = 1, ROUND(IFNULL(`document_order`.`quantity`, 0) * IFNULL(`product`.`price_for_buy_with_nds`, 0)), 0),
                YEAR(`document`.`document_date`),
                MONTH(`document`.`document_date`),
                `order`.`product_id`
            FROM `order_act` AS `document_order`
            LEFT JOIN `act` AS `document` ON `document`.`id` = `document_order`.`act_id`
            LEFT JOIN `order` ON `order`.`id` = `document_order`.`order_id`
            LEFT JOIN `invoice` ON `invoice`.`id` = `order`.`invoice_id`
            LEFT JOIN `product` ON `product`.`id` = `order`.`product_id`
            ON DUPLICATE KEY UPDATE
                `company_id` = VALUES(`company_id`),
                `date` = VALUES(`date`),
                `type` = VALUES(`type`),
                `production_type` = VALUES(`production_type`),
                `price_one` = VALUES(`price_one`),
                `quantity` = VALUES(`quantity`),
                `total_amount` = VALUES(`total_amount`),
                `year` = VALUES(`year`),
                `month` = VALUES(`month`),
                `product_id` = VALUES(`product_id`),
                `document_id` = VALUES(`document_id`),
                `document_table` = VALUES(`document_table`)
        ');
        $this->execute('
            INSERT INTO `product_turnover` (
                `document_id`,
                `order_id`,
                `order_table`,
                `document_table`,
                `invoice_id`,
                `contractor_id`,
                `company_id`,
                `date`,
                `type`,
                `production_type`,
                `is_invoice_actual`,
                `is_document_actual`,
                `purchase_price`,
                `price_one`,
                `quantity`,
                `total_amount`,
                `purchase_amount`,
                `year`,
                `month`,
                `product_id`
            )
            SELECT
                `document`.`id`,
                `order`.`id`,
                "order_packing_list",
                "packing_list",
                `invoice`.`id`,
                `invoice`.`contractor_id`,
                `invoice`.`company_id`,
                `document`.`document_date`,
                `document`.`type`,
                `product`.`production_type`,
                IF(`invoice`.`is_deleted` = 0 AND `invoice`.`invoice_status_id` IN (1, 2, 3, 4, 6, 7, 8), 1, 0),
                IF(`document`.`status_out_id` IN (1, 2, 3, 4), 1, 0),
                IFNULL(`product`.`price_for_buy_with_nds`, 0),
                IF(`document`.`type` = 1, `order`.`purchase_price_with_vat`, `order`.`selling_price_with_vat`),
                IFNULL(`document_order`.`quantity`, 0),
                ROUND(IF(`document`.`type` = 1, `order`.`purchase_price_with_vat`, `order`.`selling_price_with_vat`) * IFNULL(`document_order`.`quantity`, 0), IF(`invoice`.`price_precision` = 4, 2, 0)),
                IF(`invoice`.`type` = 2 AND `product`.`production_type` = 1, ROUND(IFNULL(`document_order`.`quantity`, 0) * IFNULL(`product`.`price_for_buy_with_nds`, 0)), 0),
                YEAR(`document`.`document_date`),
                MONTH(`document`.`document_date`),
                `order`.`product_id`
            FROM `order_packing_list` AS `document_order`
            LEFT JOIN `packing_list` AS `document` ON `document`.`id` = `document_order`.`packing_list_id`
            LEFT JOIN `order` ON `order`.`id` = `document_order`.`order_id`
            LEFT JOIN `invoice` ON `invoice`.`id` = `order`.`invoice_id`
            LEFT JOIN `product` ON `product`.`id` = `order`.`product_id`
            ON DUPLICATE KEY UPDATE
                `company_id` = VALUES(`company_id`),
                `date` = VALUES(`date`),
                `type` = VALUES(`type`),
                `production_type` = VALUES(`production_type`),
                `price_one` = VALUES(`price_one`),
                `quantity` = VALUES(`quantity`),
                `total_amount` = VALUES(`total_amount`),
                `year` = VALUES(`year`),
                `month` = VALUES(`month`),
                `product_id` = VALUES(`product_id`),
                `document_id` = VALUES(`document_id`),
                `document_table` = VALUES(`document_table`)
        ');
        $this->execute('
            INSERT INTO `product_turnover` (
                `document_id`,
                `order_id`,
                `order_table`,
                `document_table`,
                `invoice_id`,
                `contractor_id`,
                `company_id`,
                `date`,
                `type`,
                `production_type`,
                `is_invoice_actual`,
                `is_document_actual`,
                `purchase_price`,
                `price_one`,
                `quantity`,
                `total_amount`,
                `purchase_amount`,
                `year`,
                `month`,
                `product_id`
            )
            SELECT
                `document`.`id`,
                `order`.`id`,
                "order_upd",
                "upd",
                `invoice`.`id`,
                `invoice`.`contractor_id`,
                `invoice`.`company_id`,
                `document`.`document_date`,
                `document`.`type`,
                `product`.`production_type`,
                IF(`invoice`.`is_deleted` = 0 AND `invoice`.`invoice_status_id` IN (1, 2, 3, 4, 6, 7, 8), 1, 0),
                IF(`document`.`status_out_id` IN (1, 2, 3, 4), 1, 0),
                IFNULL(`product`.`price_for_buy_with_nds`, 0),
                IF(`document`.`type` = 1, `order`.`purchase_price_with_vat`, `order`.`selling_price_with_vat`),
                IFNULL(`document_order`.`quantity`, 0),
                ROUND(IF(`document`.`type` = 1, `order`.`purchase_price_with_vat`, `order`.`selling_price_with_vat`) * IFNULL(`document_order`.`quantity`, 0), IF(`invoice`.`price_precision` = 4, 2, 0)),
                IF(`invoice`.`type` = 2 AND `product`.`production_type` = 1, ROUND(IFNULL(`document_order`.`quantity`, 0) * IFNULL(`product`.`price_for_buy_with_nds`, 0)), 0),
                YEAR(`document`.`document_date`),
                MONTH(`document`.`document_date`),
                `order`.`product_id`
            FROM `order_upd` AS `document_order`
            LEFT JOIN `upd` AS `document` ON `document`.`id` = `document_order`.`upd_id`
            LEFT JOIN `order` ON `order`.`id` = `document_order`.`order_id`
            LEFT JOIN `invoice` ON `invoice`.`id` = `order`.`invoice_id`
            LEFT JOIN `product` ON `product`.`id` = `order`.`product_id`
            ON DUPLICATE KEY UPDATE
                `company_id` = VALUES(`company_id`),
                `date` = VALUES(`date`),
                `type` = VALUES(`type`),
                `production_type` = VALUES(`production_type`),
                `price_one` = VALUES(`price_one`),
                `quantity` = VALUES(`quantity`),
                `total_amount` = VALUES(`total_amount`),
                `year` = VALUES(`year`),
                `month` = VALUES(`month`),
                `product_id` = VALUES(`product_id`),
                `document_id` = VALUES(`document_id`),
                `document_table` = VALUES(`document_table`)
        ');
    }

    public function safeDown()
    {
        $this->truncateTable('{{product_turnover}}');
    }
}
