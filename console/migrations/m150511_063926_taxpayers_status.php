<?php

use yii\db\Schema;
use yii\db\Migration;

class m150511_063926_taxpayers_status extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('taxpayers_status', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_TEXT,
            'code' => Schema::TYPE_STRING,
        ], $tableOptions);

        $this->batchInsert('taxpayers_status', ['id', 'code', 'name'], [
            [1, '01', '01 - налогоплательщик (плательщик сборов) - юридическое лицо'],
            [2, '02', '02 - налоговый агент'],
            [3, '03', '03 - организация федеральной почтовой связи, составившая распоряжение по каждому платежу физического лица'],
            [4, '04', '04 - налоговый орган'],
            [5, '05', '05 - территориальные органы Федеральной службы судебных приставов'],
            [6, '06', '06 - участник внешнеэкономической деятельности - юридическое лицо'],
            [7, '07', '07 - таможенный орган'],
            [8, '08', '08 - юридическое лицо (индивидуальный предприниматель), уплачивающее страховые взносы и иные платежи'],
            [9, '09', '09 - налогоплательщик (плательщик сборов) – индивидуальный предприниматель'],
            [10, '10', '10 - налогоплательщик (плательщик сборов) – нотариус, занимающийся частной практикой'],
            [11, '11', '11 - налогоплательщик (плательщик сборов) – адвокат, учредивший адвокатский кабинет'],
            [12, '12', '12 - налогоплательщик (плательщик сборов) – глава крестьянского (фермерского) хозяйства'],
            [13, '13', '13 - налогоплательщик (плательщик сборов) – иное физическое лицо – клиент банка (владелец счета)'],
            [14, '14', '14 - налогоплательщик, производящий выплаты физическим лицам'],
            [15, '15', '15 - кредитная организация (филиал кредитной организации), платежный агент, организация федеральной почтовой связи, составившие платежное поручение на общую сумму с реестром'],
            [16, '16', '16 - участник внешнеэкономической деятельности - физическое лицо'],
            [17, '17', '17 - участник внешнеэкономической деятельности - индивидуальный предприниматель'],
            [18, '18', '18 - плательщик таможенных платежей, не являющийся декларантом, на которого законодательством Российской Федерации возложена обязанность по уплате таможенных платежей'],
            [19, '19', '19 - организации, переводящие средства, удержанные из заработной платы на основании исполнительного документа'],
            [20, '20', '20 - кредитная организация (филиал кредитной организации), платежный агент, составившие распоряжение по каждому платежу физического лица'],
            [21, '21', '21 - ответственный участник консолидированной группы налогоплательщиков'],
            [22, '22', '22 - участник консолидированной группы налогоплательщиков'],
            [23, '23', '23 - органы контроля за уплатой страховых взносов'],
            [24, '24', '24 - физическое лицо, уплачивающее страховые взносы и иные платежи'],
            [25, '', '(нет значения)'],
        ]);
    }
    
    public function safeDown()
    {
        $this->dropTable('taxpayers_status');
    }
}
