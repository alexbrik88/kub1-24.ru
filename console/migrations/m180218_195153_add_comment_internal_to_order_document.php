<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180218_195153_add_comment_internal_to_order_document extends Migration
{
    public $tableName = 'order_document';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'comment_internal', $this->text()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'comment_internal');
    }
}


