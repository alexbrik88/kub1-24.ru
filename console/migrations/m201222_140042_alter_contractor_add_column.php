<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201222_140042_alter_contractor_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{contractor}}', 'is_seller', $this->boolean()->notNull()->defaultValue(false)->after('company_id'));
        $this->addColumn('{{contractor}}', 'is_customer', $this->boolean()->notNull()->defaultValue(false)->after('is_seller'));
        $this->addColumn('{{contractor}}', 'is_founder', $this->boolean()->notNull()->defaultValue(false)->after('is_customer'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{contractor}}', 'is_seller');
        $this->dropColumn('{{contractor}}', 'is_customer');
        $this->dropColumn('{{contractor}}', 'is_founder');
    }
}
