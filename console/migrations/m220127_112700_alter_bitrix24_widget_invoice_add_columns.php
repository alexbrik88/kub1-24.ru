<?php

use console\components\db\Migration;
use yii\db\Schema;

class m220127_112700_alter_bitrix24_widget_invoice_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%bitrix24_widget_invoice}}', 'deal_id', $this->integer());
        $this->addColumn('{{%bitrix24_widget_invoice}}', 'contact_id', $this->integer());
        $this->addColumn('{{%bitrix24_widget_invoice}}', 'company_id', $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%bitrix24_widget_invoice}}', 'deal_id');
        $this->dropColumn('{{%bitrix24_widget_invoice}}', 'contact_id');
        $this->dropColumn('{{%bitrix24_widget_invoice}}', 'company_id');
    }
}
