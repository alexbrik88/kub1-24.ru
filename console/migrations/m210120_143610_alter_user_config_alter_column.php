<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210120_143610_alter_user_config_alter_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%user_config}}', 'theme_wide', $this->boolean()->notNull()->defaultValue(true));
        $this->update('{{%user_config}}', ['theme_wide' => 1]);
    }

    public function safeDown()
    {
        $this->alterColumn('{{%user_config}}', 'theme_wide', $this->boolean()->notNull()->defaultValue(false));
    }
}
