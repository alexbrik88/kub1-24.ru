<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210527_072549_alter_ofd_receipt_remove_partitioning extends Migration
{
    public function safeUp()
    {
        $this->execute('ALTER TABLE {{%ofd_receipt}} REMOVE PARTITIONING;');
    }

    public function safeDown()
    {
        //
    }
}
