<?php

use yii\db\Migration;

class m210527_155825_alter_crm_task_table extends Migration
{
    /** @var string  */
    private const TABLE_NAME = '{{%crm_task}}';

    /**
     * @inheritDoc
     */
    public function up(): void
    {
        $this->dropForeignKey('fk_crm_task__crm_client_client_id', self::TABLE_NAME);
        $this->dropIndex('ck_client_id', self::TABLE_NAME);
        $this->renameColumn(self::TABLE_NAME, 'client_id', 'contractor_id');
        $this->alterColumn(self::TABLE_NAME, 'contractor_id', $this->integer()->null());
        $this->createIndex('ck_contractor_id', self::TABLE_NAME, ['contractor_id']);
        $this->addForeignKey(
            'fk_crm_task__contractor',
            self::TABLE_NAME,
            ['contractor_id'],
            '{{%contractor}}',
            ['id'],
            'CASCADE',
            'CASCADE'
        );
    }
}
