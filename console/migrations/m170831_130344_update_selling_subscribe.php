<?php

use console\components\db\Migration;
use yii\db\Schema;
use common\models\selling\SellingSubscribe;
use common\models\service\Subscribe;
use common\models\service\SubscribeTariff;

class m170831_130344_update_selling_subscribe extends Migration
{
    public $tableName = 'selling_subscribe';

    public function safeUp()
    {
        $exists = SellingSubscribe::find()
            ->andWhere(['not', ['subscribe_id' => null]])
            ->select('subscribe_id')
            ->groupBy('subscribe_id')
            ->column();
        /* @var $subscribe Subscribe */
        foreach (Subscribe::find()
                     ->joinWith('payment')
                     ->andWhere(['not', [Subscribe::tableName() . '.id' => $exists]])
                     ->andWhere(['is_confirmed' => 1])
                     ->all() as $key => $subscribe) {
            $type = null;
            $mainType = null;
            $invoiceNumber = null;
            if ($subscribe->tariff_id == SubscribeTariff::TARIFF_TRIAL) {
                $type = 1;
            } elseif ($subscribe->payment && $subscribe->payment->type->id == 4) {
                if ($subscribe->payment->promoCode) {
                    if ($subscribe->payment->promoCode->group_id == \common\models\service\PromoCodeGroup::GROUP_INDIVIDUAL) {
                        $type = 2;
                    } else {
                        $type = 3;
                    }
                } else {
                    continue;
                }
            } elseif ($subscribe->tariff_id == SubscribeTariff::TARIFF_1) {
                $type = 4;
            } elseif ($subscribe->tariff_id == SubscribeTariff::TARIFF_2) {
                $type = 5;
            } elseif ($subscribe->tariff_id == SubscribeTariff::TARIFF_3) {
                $type = 6;
            }
            if ($type !== null) {
                if (in_array($type, [1, 2, 3])) {
                    $mainType = 1;
                } else {
                    $mainType = 2;
                }
            } else {
                continue;
            }
            if (!in_array($type, [1, 2, 3])) {
                if ($subscribe->payment->outInvoice) {
                    $invoiceNumber = $subscribe->payment->outInvoice->getFullNumber();
                }
            }
            $sellingSubscribe = new SellingSubscribe();
            $sellingSubscribe->company_id = $subscribe->company_id;
            $sellingSubscribe->subscribe_id = $subscribe->id;
            $sellingSubscribe->type = $type;
            $sellingSubscribe->main_type = $mainType;
            $sellingSubscribe->invoice_number = $invoiceNumber;
            $sellingSubscribe->creation_date = $subscribe->created_at;
            $sellingSubscribe->activation_date = $subscribe->activated_at;
            $sellingSubscribe->end_date = $subscribe->expired_at;
            $sellingSubscribe->save();
        }
    }

    public function safeDown()
    {

    }
}


