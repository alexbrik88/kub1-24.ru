<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210802_115818_append_log_entity_type extends Migration
{
    public $tableName = 'log_entity_type';

    public function safeUp()
    {
        $this->insert($this->tableName, [
            'id' => 4,
            'name' => 'Направления',
        ]);
        $this->insert($this->tableName, [
            'id' => 5,
            'name' => 'Точки продаж',
        ]);
    }

    public function safeDown()
    {
        $this->delete($this->tableName, [
            'id' => 5,
        ]);
        $this->delete($this->tableName, [
            'id' => 4,
        ]);
    }
}
