<?php

use console\components\db\Migration;
use yii\db\Schema;
use common\models\report\ReportState;

class m170811_152625_add_sum_to_report_state extends Migration
{
    public $tableName = 'report_state';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'sum', $this->string()->defaultValue(0));
        /* @var $reportState ReportState */
        foreach (ReportState::find()->all() as $reportState) {
            $reportState->sum = $reportState->subscribe->payment->sum;
            $reportState->save(true, ['sum']);
        }
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'sum');
    }
}


