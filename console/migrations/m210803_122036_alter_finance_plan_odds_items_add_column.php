<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210803_122036_alter_finance_plan_odds_items_add_column extends Migration
{
    public $tableIncome = 'finance_plan_odds_income_item';
    public $tableExpense = 'finance_plan_odds_expenditure_item';

    public function safeUp()
    {
        $this->addColumn($this->tableIncome, 'payment_delay', $this->tinyInteger(3)->after('payment_type')->comment('Отсрочка оплаты, мес'));
        $this->addColumn($this->tableExpense, 'payment_delay', $this->tinyInteger(3)->after('payment_type')->comment('Отсрочка оплаты, мес'));

        $this->execute(" UPDATE `{$this->tableIncome}` SET `payment_delay` = 0 WHERE payment_type = 1;");
        $this->execute(" UPDATE `{$this->tableIncome}` SET `payment_delay` = 1 WHERE payment_type = 2;");
        $this->execute(" UPDATE `{$this->tableExpense}` SET `payment_delay` = 0 WHERE payment_type = 1;");
        $this->execute(" UPDATE `{$this->tableExpense}` SET `payment_delay` = 1 WHERE payment_type = 2;");
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableIncome, 'payment_delay');
        $this->dropColumn($this->tableExpense, 'payment_delay');
    }
}
