<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180301_102918_insert_event extends Migration
{
    public function safeUp()
    {
        $this->batchInsert('event', ['id', 'name'], [
            [82, 'Выставлен счет на оплату КУБ'],
            [83, 'КУБ оплачен'],
        ]);
    }

    public function safeDown()
    {
        $this->delete('event', ['id' => [82, 83]]);
    }
}
