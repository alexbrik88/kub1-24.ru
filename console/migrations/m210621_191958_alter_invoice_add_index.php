<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210621_191958_alter_invoice_add_index extends Migration
{
    public $table = 'invoice';

    public function safeUp()
    {
        $this->createIndex('I_date', $this->table, ['company_id', 'document_date']);
        $this->createIndex('I_payment_limit_date', $this->table, ['company_id', 'payment_limit_date']);
    }

    public function safeDown()
    {
        $this->dropIndex('I_date', $this->table);
        $this->dropIndex('I_payment_limit_date', $this->table);
    }
}
