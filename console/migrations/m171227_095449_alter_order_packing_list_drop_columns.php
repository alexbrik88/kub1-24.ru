<?php

use console\components\db\Migration;
use yii\db\Schema;

class m171227_095449_alter_order_packing_list_drop_columns extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('order_packing_list', 'amount_sales_with_vat');
        $this->dropColumn('order_packing_list', 'amount_sales_no_vat');
        $this->dropColumn('order_packing_list', 'amount_purchase_with_vat');
        $this->dropColumn('order_packing_list', 'amount_purchase_no_vat');
        $this->dropColumn('order_packing_list', 'product_title');
    }

    public function safeDown()
    {
        $this->addColumn('order_packing_list', 'product_title', $this->string(255)->notNull());
        $this->addColumn('order_packing_list', 'amount_purchase_no_vat', $this->integer()->notNull());
        $this->addColumn('order_packing_list', 'amount_purchase_with_vat', $this->integer()->notNull());
        $this->addColumn('order_packing_list', 'amount_sales_no_vat', $this->integer()->notNull());
        $this->addColumn('order_packing_list', 'amount_sales_with_vat', $this->integer()->notNull());
    }
}
