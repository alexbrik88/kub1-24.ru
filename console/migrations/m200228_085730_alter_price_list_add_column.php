<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200228_085730_alter_price_list_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%price_list}}', 'use_notifications', $this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%price_list}}', 'use_notifications');
    }
}
