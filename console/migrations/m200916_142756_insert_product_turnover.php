<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200916_142756_insert_product_turnover extends Migration
{
    public function safeUp()
    {
        $this->execute('
            INSERT INTO `product_turnover` (
                `order_id`,
                `document_id`,
                `document_table`,
                `invoice_id`,
                `contractor_id`,
                `company_id`,
                `date`,
                `type`,
                `production_type`,
                `is_invoice_actual`,
                `is_document_actual`,
                `purchase_price`,
                `price_one`,
                `quantity`,
                `total_amount`,
                `purchase_amount`,
                `margin`,
                `year`,
                `month`,
                `product_group_id`,
                `product_id`
            ) SELECT
                0,
                `product_store`.`product_id`,
                "initial_balance",
                0,
                0,
                `product`.`company_id`,
                IFNULL(`product_initial_balance`.`date`, FROM_UNIXTIME(MIN(`product_store`.`created_at`))),
                0,
                `product`.`production_type`,
                0,
                0,
                IFNULL(`product`.`price_for_buy_with_nds`, 0),
                0,
                SUM(`product_store`.`initial_quantity`),
                0,
                0,
                0,
                YEAR(IFNULL(`product_initial_balance`.`date`, FROM_UNIXTIME(MIN(`product_store`.`created_at`)))),
                MONTH(IFNULL(`product_initial_balance`.`date`, FROM_UNIXTIME(MIN(`product_store`.`created_at`)))),
                `product`.`group_id`,
                `product_store`.`product_id`
            FROM `product_store`
            LEFT JOIN `product` ON `product`.`id` = `product_store`.`product_id`
            LEFT JOIN `product_initial_balance` ON `product_initial_balance`.`product_id` = `product_store`.`product_id`
            WHERE `product`.`production_type`=1
            GROUP BY `product_store`.`product_id`
            ON DUPLICATE KEY UPDATE
                `date` = VALUES(`date`),
                `quantity` = VALUES(`quantity`),
                `year` = VALUES(`year`),
                `month` = VALUES(`month`),
                `product_group_id` = VALUES(`product_group_id`);
        ');
    }

    public function safeDown()
    {
        $this->delete('{{%product_turnover}}', [
            'order_id' => 0,
            'document_table' => 'initial_balance',
        ]);
    }
}
