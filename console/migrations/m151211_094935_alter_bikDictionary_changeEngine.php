<?php

use yii\db\Schema;
use yii\db\Migration;

class m151211_094935_alter_bikDictionary_changeEngine extends Migration
{
    public $tBik = 'bik_dictionary';

    public function safeUp()
    {
        $this->execute("ALTER TABLE {$this->tBik} ENGINE = InnoDB;");
    }
    
    public function safeDown()
    {
        $this->execute("ALTER TABLE {$this->tBik} ENGINE = MyISAM;");
    }
}
