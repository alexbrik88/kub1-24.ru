<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181012_091207_create_price_list_checked_products extends Migration
{
    public $tableName = 'price_list_checked_products';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'price_list_id' => $this->integer()->notNull(),
            'checked_products' => $this->text()->notNull(),
        ]);

        $this->addForeignKey($this->tableName . '_price_list_id', $this->tableName, 'price_list_id', 'price_list', 'id', 'RESTRICT', 'CASCADE');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_price_list_id', $this->tableName);

        $this->dropTable($this->tableName);
    }
}


