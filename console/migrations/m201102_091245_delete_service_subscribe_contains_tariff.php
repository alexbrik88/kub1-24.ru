<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201102_091245_delete_service_subscribe_contains_tariff extends Migration
{
    public function safeUp()
    {
        $this->delete('{{%service_subscribe_contains_tariff}}', [
            'tariff_id' => [52, 53, 54],
        ]);
    }

    public function safeDown()
    {
        $this->batchInsert('{{%service_subscribe_contains_tariff}}', [
            'tariff_id',
            'contains_tariff_id',
        ], [
            [52, 18],
            [53, 19],
            [54, 20],
        ]);
    }
}
