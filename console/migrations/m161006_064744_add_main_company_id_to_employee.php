<?php

use console\components\db\Migration;
use yii\db\Schema;

class m161006_064744_add_main_company_id_to_employee extends Migration
{
    public $tEmployee = 'employee';

    public function safeUp()
    {
        $this->addColumn($this->tEmployee, 'main_company_id', $this->integer()->notNull());
        $this->update($this->tEmployee, ['main_company_id' => new \yii\db\Expression('company_id')]);

       // $this->addForeignKey($this->tEmployee . '_main_company_id', $this->tEmployee, 'main_company_id', 'company', 'main_id', 'RESTRICT', 'CASCADE');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey($this->tEmployee . '_main_company_id', $this->tEmployee);
        $this->dropColumn($this->tEmployee, 'main_company_id');
    }
}


