<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201009_121218_alter_menu_item_append_item_rent extends Migration
{
    public $tableName = 'menu_item';

    public function safeUp()
    {
        $this->addColumn($this->tableName,'rent_item',$this->boolean()->defaultValue(false)->after('product_item'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'rent_item');
    }
}
