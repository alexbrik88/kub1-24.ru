<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200412_121941_add_table_view_article_config extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%user_config}}', 'table_view_article', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%user_config}}', 'table_view_article');
    }
}
