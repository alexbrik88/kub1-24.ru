<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210129_191151_alter_employee_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%employee}}', 'links_color', $this->tinyInteger()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%employee}}', 'links_color');
    }
}
