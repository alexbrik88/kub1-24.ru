<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170107_003744_update_product_unit extends Migration
{
    public $tableName = 'product_unit';

    public function safeUp()
    {
        $this->update($this->tableName, ['name' => 'ч'], ['id' => 11]);
        $this->insert($this->tableName, [
            'id' => 12,
            'name' => 'мин',
            'code_okei' => 355,
        ]);
    }

    public function safeDown()
    {
        $this->update($this->tableName, ['name' => 'час'], ['id' => 11]);
        $this->delete($this->tableName, ['id' => 12]);
    }
}


