<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180628_123906_create_service_invoice_tariff extends Migration
{
    public $tServiceInvoiceTariff = 'service_invoice_tariff';
    public $tServicePayment = 'service_payment';
    public $tServicePaymentOrder = 'service_payment_order';

    public function safeUp()
    {
        $this->createTable($this->tServiceInvoiceTariff, [
            'id' => $this->primaryKey(),
            'invoice_count' => $this->integer()->defaultValue(null),
            'total_amount' => $this->integer()->defaultValue(null),
        ]);

        $this->batchInsert($this->tServiceInvoiceTariff, ['id', 'invoice_count', 'total_amount'], [
            [1, 1, 50,],
            [2, 3, 120,],
        ]);

        $this->addColumn($this->tServicePayment, 'invoice_tariff_id', $this->integer()->defaultValue(null));

        $this->addColumn($this->tServicePaymentOrder, 'invoice_tariff_id', $this->integer()->defaultValue(null));

        $this->addForeignKey($this->tServicePayment . '_invoice_tariff_id', $this->tServicePayment, 'invoice_tariff_id', $this->tServiceInvoiceTariff, 'id', 'RESTRICT', 'CASCADE');

        $this->addForeignKey($this->tServicePaymentOrder . '_invoice_tariff_id', $this->tServicePaymentOrder, 'invoice_tariff_id', $this->tServiceInvoiceTariff, 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey($this->tServicePayment . '_invoice_tariff_id', $this->tServicePayment);

        $this->dropForeignKey($this->tServicePaymentOrder . '_invoice_tariff_id', $this->tServicePayment);

        $this->dropColumn($this->tServicePayment, 'invoice_tariff_id');

        $this->dropColumn($this->tServicePaymentOrder, 'invoice_tariff_id');

        $this->dropTable($this->tServiceInvoiceTariff);
    }
}
