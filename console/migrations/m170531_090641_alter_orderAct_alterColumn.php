<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170531_090641_alter_orderAct_alterColumn extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%order_act}}', 'quantity', $this->decimal(20, 10));
    }
    
    public function safeDown()
    {
        $this->alterColumn('{{%order_act}}', 'quantity', $this->integer()->notNull());
    }
}
