<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210527_072607_alter_ofd_receipt_item_remove_partitioning extends Migration
{
    public function safeUp()
    {
        $this->execute('ALTER TABLE {{%ofd_receipt_item}} REMOVE PARTITIONING;');
    }

    public function safeDown()
    {
        //
    }
}
