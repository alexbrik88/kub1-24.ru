<?php

use console\components\db\Migration;
use yii\db\Schema;
use \common\models\service\Subscribe;

class m191026_101408_update_dates_in_service_subscribe extends Migration
{
    // Обновление дат активаций подписок по датам оплат (для тарифа ИП УСН 6%)
    public function safeUp()
    {
        Yii::$app->db->createCommand('SET session wait_timeout=28800')->execute();
        Yii::$app->db->createCommand('SET session interactive_timeout=28800')->execute();

        $subscribeIds = Subscribe::find()
            ->where(['tariff_id' => [11,12]])
            ->andWhere(['!=', 'company_id', [16727, 35177, 35364, 39079, 39753, 40007, 49276]]) // компании с 2 оплатами - обновил вручную
            ->select('id')
            ->asArray()
            ->column();

        echo 'Updated ids: ';
        foreach ($subscribeIds as $id) {
            /** @var Subscribe $subscribe */
            if ($subscribe = Subscribe::findOne($id)) {
                // компании с 2 оплатами обновил вручную
                if (Subscribe::find()->where(['company_id' => $subscribe->company_id])->andWhere(['tariff_id' => [11,12]])->count() > 1)
                    continue;

                if ($subscribe->payment) {
                    if (date('Ymd', $subscribe->payment->payment_date) != date('Ymd', $subscribe->activated_at)) {

                        $subscribe->activated_at = (int)$subscribe->payment->payment_date + 1;

                        $date = new \DateTime();
                        $date->setTimestamp($subscribe->activated_at)
                            ->modify("-1 day")
                            ->setTime(23, 59, 59);

                        if ($subscribe->tariff_id == 11)
                            $date->modify("+4 month");
                        elseif ($subscribe->tariff_id == 12)
                            $date->modify("+12 month");
                        else
                            continue;

                        $subscribe->expired_at = $date->getTimestamp();

                        $subscribe->updateAttributes(['activated_at', 'expired_at']);

                        echo $subscribe->id.',';
                    }
                }
            }
        }

        echo "\n";

    }

    public function safeDown()
    {
        echo "This migration not rolled back\n";
    }
}
