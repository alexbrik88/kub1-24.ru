<?php

use console\components\db\Migration;

class m200110_190701_bitrix24_fk_drop extends Migration
{
    public function safeUp()
    {
        $this->dropFK('catalog', 'employee_id');
        $this->dropFK('contact', 'company_id');
        $this->dropFK('contact', 'employee_id');
        $this->dropFK('deal', 'company_id');
        $this->dropFK('deal', 'contact_id');
        $this->dropFK('deal', 'employee_id');
        $this->dropFK('deal_position', 'deal_id');
        $this->dropFK('deal_position', 'employee_id');
        $this->dropFK('deal_position', 'product_id');
        $this->dropFK('invoice', 'company_id');
        $this->dropFK('invoice', 'contact_id');
        $this->dropFK('invoice', 'deal_id');
        $this->dropFK('invoice', 'employee_id');
        $this->dropFK('product', 'section_id');
        $this->dropFK('product', 'vat_id');
        $this->dropFK('section', 'catalog_id');
        $this->dropFK('user', 'employee_id');
        $this->dropFK('vat', 'employee_id');
    }

    public function safeDown()
    {
        $this->createFK('catalog', 'employee_id');
        $this->createFK('contact', 'company_id', 'employee_id');
        $this->createFK('contact', 'employee_id');
        $this->createFK('deal', 'company_id', 'employee_id');
        $this->createFK('deal', 'contact_id', 'employee_id');
        $this->createFK('deal', 'employee_id');
        $this->createFK('deal_position', 'deal_id', 'employee_id');
        $this->createFK('deal_position', 'employee_id');
        $this->createFK('deal_position', 'product_id', 'employee_id');
        $this->createFK('invoice', 'company_id', 'employee_id');
        $this->createFK('invoice', 'contact_id', 'employee_id');
        $this->createFK('invoice', 'deal_id', 'employee_id');
        $this->createFK('invoice', 'employee_id');
        $this->createFK('product', 'section_id', 'employee_id');
        $this->createFK('product', 'vat_id', 'employee_id');
        $this->createFK('section', 'catalog_id', 'employee_id');
        $this->createFK('user', 'employee_id');
        $this->createFK('vat', 'employee_id');
    }

    protected static function tableName(string $table, bool $local = true): string
    {
        if ($local === true) {
            $table = 'bitrix24_' . $table;
        }
        return '{{%' . $table . '}}';
    }

    protected function dropFK(string $table, string $column)
    {
        $this->dropForeignKey(self::keyName($table, $column), self::tableName($table));
    }

    /**
     * @param string      $table
     * @param string      $column
     * @param string|null $subColumn
     */
    protected function createFK(string $table, string $column, string $subColumn = null)
    {
        $this->addForeignKey(
            self::keyName($table, $column), self::tableName($table), ($subColumn === null ? $column : [$subColumn, $column]),
            self::tableName(substr($column, 0, -3), $subColumn !== null), ($subColumn === null ? 'id' : [$subColumn, 'id']),
            'CASCADE', 'CASCADE'
        );
    }

    protected static function keyName(string $table, string $column): string
    {
        return 'bitrix24_' . $table . '_' . $column;
    }

}
