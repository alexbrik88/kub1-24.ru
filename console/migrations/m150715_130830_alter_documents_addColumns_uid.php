<?php

use yii\db\Schema;
use yii\db\Migration;

class m150715_130830_alter_documents_addColumns_uid extends Migration
{
    public function safeUp()
    {
        $this->addColumn('act', 'uid', 'CHAR(5) DEFAULT NULL COMMENT "unique id for `out` acts for quest" AFTER `id` ');
        $this->createIndex('uid_idx', 'act', 'uid');
        $this->update('act', [
            'uid' => new \yii\db\Expression('MD5(RAND())'),
        ], 'type = 2');
        $this->addColumn('packing_list', 'uid', 'CHAR(5) DEFAULT NULL COMMENT "unique id for `out` packing lists for quest" AFTER `id` ');
        $this->createIndex('uid_idx', 'packing_list', 'uid');
        $this->update('packing_list', [
            'uid' => new \yii\db\Expression('MD5(RAND())'),
        ], 'type = 2');
        $this->addColumn('invoice_facture', 'uid', 'CHAR(5) DEFAULT NULL COMMENT "unique id for `out` invoice factures for quest" AFTER `id` ');
        $this->createIndex('uid_idx', 'invoice_facture', 'uid');
        $this->update('invoice_facture', [
            'uid' => new \yii\db\Expression('MD5(RAND())'),
        ], 'type = 2');
    }
    
    public function safeDown()
    {
        $this->dropIndex('uid_idx', 'act');
        $this->dropColumn('act', 'uid');

        $this->dropIndex('uid_idx', 'packing_list');
        $this->dropColumn('packing_list', 'uid');

        $this->dropIndex('uid_idx', 'invoice_facture');
        $this->dropColumn('invoice_facture', 'uid');
    }
}
