<?php

use yii\db\Migration;

/**
 * Handles the creation
 */
class m210728_135003_create_finance_model_2_group_expenses_table extends Migration
{
    const MODEL = 'finance_plan_group';
    const EXPENSE_ITEM = 'finance_plan_group_expenditure_item';
    const EXPENSE_ITEM_AUTOFILL = 'finance_plan_group_expenditure_item_autofill';

    public function safeUp()
    {
        /*
         * Item
         */
        $this->createTable(self::EXPENSE_ITEM, [
            'id' => $this->primaryKey(),
            'model_group_id' => $this->integer()->notNull(),
            'expenditure_item_id' => $this->integer()->notNull(),
            'fill_type' => $this->tinyInteger()->unsigned()->defaultValue(1)->comment('1 - автозаполнение, 2 - зависит от строки'),
            'relation_type' => $this->tinyInteger()->unsigned()->notNull()->defaultValue(0)->comment('0 - нет, 1 - выручка, 2 - кол-во продаж, 3 - ebidta'),
            'action' => $this->float(),
            'data' => $this->text()
        ]);

        $this->addForeignKey('FK_'.self::EXPENSE_ITEM.'_to_model_group', self::EXPENSE_ITEM, 'model_group_id', self::MODEL, 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_'.self::EXPENSE_ITEM.'_to_expenditure_items', self::EXPENSE_ITEM, 'expenditure_item_id', 'invoice_expenditure_item', 'id', 'CASCADE', 'CASCADE');

        /*
         * Autofill Item
         */
        $this->createTable(self::EXPENSE_ITEM_AUTOFILL, [
            'model_group_id' => $this->integer(11)->notNull(),
            'row_id' => $this->integer()->notNull(),
            'start_month' => $this->integer(11)->notNull(),
            'start_amount' => $this->bigInteger(20),
            'amount' => $this->bigInteger(20),
            'limit_amount' => $this->bigInteger(20),
            'action' => $this->tinyInteger()->unsigned()->notNull(),
        ]);

        $this->addPrimaryKey('PK_'.self::EXPENSE_ITEM_AUTOFILL, self::EXPENSE_ITEM_AUTOFILL, ['model_group_id', 'row_id']);
        $this->addForeignKey('FK_'.self::EXPENSE_ITEM_AUTOFILL.'_to_model_group', self::EXPENSE_ITEM_AUTOFILL, 'model_group_id', self::MODEL, 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_'.self::EXPENSE_ITEM_AUTOFILL.'_to_item', self::EXPENSE_ITEM_AUTOFILL, 'row_id', self::EXPENSE_ITEM, 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_'.self::EXPENSE_ITEM.'_to_expenditure_items', self::EXPENSE_ITEM);
        $this->dropForeignKey('FK_'.self::EXPENSE_ITEM.'_to_model_group', self::EXPENSE_ITEM);
        $this->dropForeignKey('FK_'.self::EXPENSE_ITEM_AUTOFILL.'_to_model_group', self::EXPENSE_ITEM_AUTOFILL);
        $this->dropForeignKey('FK_'.self::EXPENSE_ITEM_AUTOFILL.'_to_item', self::EXPENSE_ITEM_AUTOFILL);

        $this->dropTable(self::EXPENSE_ITEM);
        $this->dropTable(self::EXPENSE_ITEM_AUTOFILL);
    }
}
