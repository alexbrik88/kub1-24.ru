<?php

use yii\db\Schema;
use yii\db\Migration;

class m150417_065624_rename_ProductType extends Migration
{
    public function up()
    {
        $this->renameTable('product_type', 'product_unit');
    }

    public function down()
    {
        $this->renameTable('product_unit', 'product_type');
    }
}
