<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211109_211513_create_profit_and_loss_cache extends Migration
{
    public function safeUp()
    {
        $table = 'olap_profit_and_loss_cache';

        $this->createTable($table, [
            'company_id' => $this->integer()->notNull(),
            'year' => $this->integer(4),
            'hash' => $this->char(32)->notNull(),
            'data' => 'MEDIUMBLOB',
            'created_at' => $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')->append('ON UPDATE CURRENT_TIMESTAMP'),
            'expired_at' => $this->timestamp()->defaultExpression('TIMESTAMPADD(MONTH, 1, CURRENT_TIMESTAMP)')
        ]);

        $this->addForeignKey("FK_{$table}_to_company", $table, 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
        $this->addPrimaryKey("PK_{$table}", $table, ['company_id', 'year', 'hash']);
    }

    public function safeDown()
    {
        $this->dropTable('olap_profit_and_loss_cache');
    }
}
