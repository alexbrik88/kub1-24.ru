<?php

use common\models\rent\Attribute;
use console\components\db\Migration;
use yii\db\Schema;
use yii\helpers\Json;

class m211203_104855_insert_rent_attribute extends Migration
{
    public function safeUp()
    {
        $this->batchInsert('{{%rent_attribute}}', [
            'category_id',
            'alias',
            'title',
            'unit',
            'type',
            'required',
            'sort',
            'default',
            'containerCSS',
            'data',
            'tab',
            'show',
        ], [
            [4, "stateNumber", "Гос номер", NULL, "string", 0, 1, NULL, NULL, NULL, "info", 1],
            [4, "validityPeriod", "Срок действия ТО", NULL, "date", 0, 2, NULL, NULL, NULL, "info", 1],
            [4, "address", "Адрес постоянного базирования", NULL, "string", 0, 3, NULL, NULL, NULL, "info", 1],
            [4, "carrying", "Масса", "кг", "float", 1, 4, NULL, NULL, NULL, "info", 1],
            [4, "chassis", "Тип шасси", NULL, "enum", 1, 5, "На шасси", NULL, "[\"На шасси\",\"На раме\"]", "info", 1],
            [4, "shiftHours", "Часов в смене", "ч", "integer", 0, 6, NULL, NULL, NULL, "info", 1],
            [4, "VIN", "VIN / Заводской номер", NULL, "string", 0, 7, NULL, NULL, NULL, "info", 1],
            [4, "priceHour", "Цена за час", NULL, "integer", 0, 8, NULL, NULL, NULL, "price", 0],
            [4, "priceShift", "Цена за смену", NULL, "integer", 0, 9, NULL, NULL, NULL, "price", 0],
            [4, "deposit", "Залог", NULL, "integer", 0, 10, NULL, NULL, NULL, "price", 0],
            [4, "unit", "Ед. измерения", NULL, "enum", 1, 11, "шт", NULL, "[\"шт\"]", "price", 1],
            [4, "paymentPeriod", "Период оплаты", NULL, "enum", 1,12, NULL, NULL, "[\"день\", \"месяц\", \"год\"]", "price", 1],
            [4, "price", "Цена за ед. и период", NULL, "float", 1, 13, NULL, NULL, NULL, "price", 1],
            [4, "currency", "Валюта", NULL, "enum", 1, 14, "руб", NULL, "['руб']", "price", 1],
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%rent_attribute}}', [
            'category_id' => 4,
        ]);
    }
}
