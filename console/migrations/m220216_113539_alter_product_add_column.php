<?php

use console\components\db\Migration;
use yii\db\Schema;

class m220216_113539_alter_product_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%product}}', 'unit_type', $this->tinyInteger(1)->notNull()->defaultValue(1)->after('import_type_id'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%product}}', 'unit_type');
    }
}
