<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200204_153753_update_service_subscribe_tariff extends Migration
{
    public function safeUp()
    {
        $this->update('{{%service_subscribe_tariff}}', [
            'proposition' => 'Получи скидку 15% при оплате за 100 страниц',
        ], ['id' => 34]);
        $this->update('{{%service_subscribe_tariff}}', [
            'proposition' => 'Получи скидку 25% при оплате за 200 страниц',
        ], ['id' => 35]);
        $this->update('{{%service_subscribe_tariff}}', [
            'proposition' => 'Получи скидку 40% при оплате за 300 страниц',
        ], ['id' => 36]);
        $this->update('{{%service_subscribe_tariff}}', [
            'proposition' => 'Получи скидку 50% при оплате за 400 страниц',
        ], ['id' => 37]);
    }

    public function safeDown()
    {
        //
    }
}
