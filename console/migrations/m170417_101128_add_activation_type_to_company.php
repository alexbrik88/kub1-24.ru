<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170417_101128_add_activation_type_to_company extends Migration
{
    public $tableName = 'company';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'activation_type', $this->integer()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'activation_type');
    }
}


