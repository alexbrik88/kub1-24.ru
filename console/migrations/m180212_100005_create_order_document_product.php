<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180212_100005_create_order_document_product extends Migration
{
    public $tableName = 'order_document_product';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'order_document_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'quantity' => $this->decimal(20, 10)->defaultValue(null),
            'product_title' => $this->text()->notNull(),
            'product_code' => $this->string()->defaultValue(null),
            'unit_id' => $this->integer()->defaultValue(null),
            'count_in_place' => $this->string()->defaultValue(null),
            'place_count' => $this->string(45)->defaultValue(null),
            'mass_gross' => $this->string(45)->defaultValue(null),
            'base_price_no_vat' => $this->bigInteger(20)->notNull(),
            'base_price_with_vat' => $this->bigInteger(20)->notNull(),
            'discount' => $this->decimal(6, 4)->notNull()->defaultValue(0),
            'purchase_price_no_vat' => $this->bigInteger(20)->notNull(),
            'purchase_price_with_vat' => $this->bigInteger(20)->notNull(),
            'selling_price_no_vat' => $this->bigInteger(20)->defaultValue(null),
            'selling_price_with_vat' => $this->bigInteger(20)->defaultValue(null),
            'amount_purchase_no_vat' => $this->bigInteger(20)->notNull(),
            'amount_purchase_with_vat' => $this->bigInteger(20)->notNull(),
            'amount_sales_no_vat' => $this->bigInteger(20)->notNull(),
            'amount_sales_with_vat' => $this->bigInteger(20)->notNull(),
            'excise' => $this->boolean()->defaultValue(null),
            'excise_price' => $this->string()->defaultValue(null),
            'sale_tax' => $this->bigInteger(20)->notNull(),
            'purchase_tax' => $this->bigInteger(20)->notNull(),
            'purchase_tax_rate_id' => $this->integer()->notNull(),
            'sale_tax_rate_id' => $this->integer()->notNull(),
            'country_id' => $this->integer()->notNull(),
            'custom_declaration_number' => $this->string(45)->defaultValue(null),
            'box_type' => $this->string(45)->defaultValue(null),
        ]);

        $this->addForeignKey($this->tableName . '_order_document_id', $this->tableName, 'order_document_id', 'order_document', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tableName . '_product_id', $this->tableName, 'product_id', 'product', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tableName . '_unit_id', $this->tableName, 'unit_id', 'product_unit', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tableName . '_country_id', $this->tableName, 'country_id', 'country', 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_order_document_id', $this->tableName);
        $this->dropForeignKey($this->tableName . '_product_id', $this->tableName);
        $this->dropForeignKey($this->tableName . '_unit_id', $this->tableName);
        $this->dropForeignKey($this->tableName . '_country_id', $this->tableName);

        $this->dropTable($this->tableName);
    }
}


