<?php

use console\components\db\Migration;

class m200309_113509_rent_entity_rent_create extends Migration
{

    const TABLE = '{{%rent_entity_rent}}';

    public function safeUp()
    {
        $this->createTable(self::TABLE, [
            'id' => $this->primaryKey()->unsigned()->notNull(),
            'entity_id' => $this->integer()->unsigned()->notNull()->comment('ID арендуемого объекта'),
            'contractor_id' => $this->integer(11)->notNull()->comment('ID компании'),
            'agreement_id' => $this->integer(11)->comment('ID договора'),
            'invoice_id' => $this->integer(11)->comment('ID счёта'),
            'date_begin' => $this->date()->notNull()->comment('Дата начала'),
            'date_end' => $this->date()->notNull()->comment('Дата завершения'),
            'price' => $this->float()->unsigned()->notNull()->comment('Цена'),
            'amount' => $this->float()->unsigned()->notNull()->comment('Стоимость'),
        ], "COMMENT 'Учёт аренды техники: аренды'");
        $this->addForeignKey(
            'rent_entity_rent_entity_id', self::TABLE, 'entity_id',
            '{{%rent_entity}}', 'id', 'CASCADE', 'CASCADE'
        );
        $this->addForeignKey(
            'rent_entity_rent_contractor_id', self::TABLE, 'contractor_id',
            '{{%contractor}}', 'id', 'CASCADE', 'CASCADE'
        );
        $this->addForeignKey(
            'rent_entity_rent_agreement_id', self::TABLE, 'agreement_id',
            '{{%agreement}}', 'id', 'CASCADE', 'CASCADE'
        );
        $this->addForeignKey(
            'rent_entity_rent_invoice_id', self::TABLE, 'invoice_id',
            '{{%invoice}}', 'id', 'CASCADE', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable(self::TABLE);
    }
}
