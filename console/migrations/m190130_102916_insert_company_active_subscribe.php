<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190130_102916_insert_company_active_subscribe extends Migration
{
    public function safeUp()
    {
        $this->execute('
            INSERT INTO {{company_active_subscribe}} ([[company_id]], [[tariff_group_id]], [[subscribe_id]])
                SELECT {{company}}.[[id]], 1, {{company}}.[[active_subscribe_id]]
                FROM {{company}} WHERE {{company}}.[[active_subscribe_id]] IS NOT NULL;
        ');
    }

    public function safeDown()
    {
        $this->truncateTable('company_active_subscribe');
    }
}
