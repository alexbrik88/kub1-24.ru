<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200401_132405_alter_plan_cash_flows_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('plan_cash_flows', 'invoice_id', $this->integer());

        $this->addForeignKey('FK_plan_cash_flows_to_invoice', 'plan_cash_flows', 'invoice_id', 'invoice', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_plan_cash_flows_to_invoice', 'plan_cash_flows');

        $this->dropColumn('plan_cash_flows', 'invoice_id');
    }
}
