<?php

use yii\db\Schema;
use yii\db\Migration;

class m150617_114406_alter_Product extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('product', 'product_unit_id', Schema::TYPE_INTEGER . ' DEFAULT NULL');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('fk_product_to_product_unit', 'product');
        $this->alterColumn('product', 'product_unit_id', Schema::TYPE_INTEGER . ' NOT NULL');
        $this->addForeignKey('product_to_product_unit', 'product', 'product_unit_id', 'product_unit', 'id');
    }
}
