<?php

use yii\db\Migration;

/**
 * Handles the creation of table `tax_kbk`.
 */
class m190115_193714_create_tax_kbk_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('tax_kbk', [
            'id' => $this->char(20)->notNull(),
            'expenditure_item_id' => $this->integer()->notNull(),
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', 'tax_kbk', 'id');

        $this->addForeignKey(
            'FK_tax_kbk_invoice_expenditure_item',
            'tax_kbk', 'expenditure_item_id',
            'invoice_expenditure_item', 'id'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('tax_kbk');
    }
}
