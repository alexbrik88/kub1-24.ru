<?php

use console\components\db\Migration;
use yii\db\Schema;

class m171024_133810_update_chat extends Migration
{
    public $tableName = 'chat';

    public function safeUp()
    {
        $this->dropForeignKey($this->tableName . '_from_employee_id', $this->tableName);
        $this->dropForeignKey($this->tableName . '_to_employee_id', $this->tableName);

        $this->alterColumn($this->tableName, 'to_employee_id', $this->string()->notNull());
        $this->alterColumn($this->tableName, 'from_employee_id', $this->string()->notNull());
    }

    public function safeDown()
    {
        $this->alterColumn($this->tableName, 'to_employee_id', $this->integer()->notNull());
        $this->alterColumn($this->tableName, 'from_employee_id', $this->integer()->notNull());

        $this->addForeignKey($this->tableName . '_from_employee_id', $this->tableName, 'from_employee_id', 'employee', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tableName . '_to_employee_id', $this->tableName, 'to_employee_id', 'employee', 'id', 'RESTRICT', 'CASCADE');
    }
}


