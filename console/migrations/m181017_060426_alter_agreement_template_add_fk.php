<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181017_060426_alter_agreement_template_add_fk extends Migration
{
    public function safeUp()
    {
        $this->addForeignKey('fk_agreement_template_company', '{{%agreement_template}}', 'company_id', '{{%company}}', 'id', 'CASCADE');
        $this->addForeignKey('fk_agreement_template_employee', '{{%agreement_template}}', 'created_by', '{{%employee}}', 'id');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('fk_agreement_template_company', '{{%agreement_template}}');
        $this->dropForeignKey('fk_agreement_template_employee', '{{%agreement_template}}');
    }
}


