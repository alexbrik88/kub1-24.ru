<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170202_111741_add_order_currency_to_contractor extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%contractor}}', 'order_currency', $this->smallInteger(1)->defaultValue(0)->null());
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%contractor}}', 'order_currency');
    }
}


