<?php

use yii\db\Schema;
use yii\db\Migration;

class m151208_091547_create_servicStatistic extends Migration
{
    public $tStatistic = '{{%service_statistic}}';

    public function safeUp()
    {
        $this->createTable($this->tStatistic, [
            'id' => $this->primaryKey(),
            'year' => 'YEAR NOT NULL',
            'month' => $this->smallInteger()->notNull(),
            'invoice_count' => $this->integer()->notNull()->defaultValue(0),
            'payed_sum' => $this->integer()->notNull()->defaultValue(0),
            'payed_invoice_count' => $this->integer()->notNull()->defaultValue(0),
            'payed_sum_by_novice' => $this->integer()->notNull()->defaultValue(0),
            'payed_invoice_count_by_novice' => $this->integer()->notNull()->defaultValue(0),
            'not_payed_sum' => $this->integer()->notNull()->defaultValue(0),
            'not_payed_invoice_count' => $this->integer()->notNull()->defaultValue(0),

            'tariff_1' => $this->integer()->notNull()->defaultValue(0),
            'tariff_2' => $this->integer()->notNull()->defaultValue(0),
            'tariff_3' => $this->integer()->notNull()->defaultValue(0),

            'payment_type_1' => $this->integer()->notNull()->defaultValue(0),
            'payment_type_2' => $this->integer()->notNull()->defaultValue(0),
            'payment_type_3' => $this->integer()->notNull()->defaultValue(0),
        ]);

        $this->createIndex('year_month_uidx', $this->tStatistic, ['year', 'month'], true);
    }
    
    public function safeDown()
    {
        $this->dropTable($this->tStatistic);
    }
}
