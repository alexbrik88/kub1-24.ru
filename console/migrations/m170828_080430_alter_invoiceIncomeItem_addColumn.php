<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170828_080430_alter_invoiceIncomeItem_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%invoice_income_item}}', 'company_id', $this->integer()->defaultValue(null) . ' AFTER [[id]]');

        $this->addForeignKey('FK_invoiceIncomeItem_company', '{{%invoice_income_item}}', 'company_id', '{{%company}}', 'id', 'CASCADE');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('FK_invoiceIncomeItem_company', '{{%invoice_income_item}}');

        $this->dropColumn('{{%invoice_income_item}}', 'company_id');
    }
}
