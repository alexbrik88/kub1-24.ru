<?php

use console\components\db\Migration;
use yii\db\Expression;
use yii\db\Schema;

class m180814_063310_alter_cash_bank_flows_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cash_bank_flows', 'recognition_date', $this->date()->notNull()->after('date'));

        $this->update('cash_bank_flows', ['recognition_date' => new Expression('[[date]]')]);
    }

    public function safeDown()
    {
        $this->dropColumn('cash_bank_flows', 'recognition_date');
    }
}
