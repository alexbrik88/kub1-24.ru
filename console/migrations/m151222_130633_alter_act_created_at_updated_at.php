<?php

use console\components\db\Migration;
use yii\db\Schema;

class m151222_130633_alter_act_created_at_updated_at extends Migration
{
    public $tAct = 'act';

    public function safeUp()
    {
        $this->addColumn($this->tAct, 'created', $this->integer(11)->notNull());
        $this->update($this->tAct, [
           'created' => new \yii\db\Expression('UNIX_TIMESTAMP (created_at)'),
        ]);
        $this->dropColumn($this->tAct, 'created_at');
        $this->renameColumn($this->tAct, 'created', 'created_at');

        $this->addColumn($this->tAct, 'updated', $this->integer(11)->notNull());
        $this->update($this->tAct, [
            'updated' => new \yii\db\Expression('UNIX_TIMESTAMP (status_out_updated_at)'),
        ]);
        $this->dropColumn($this->tAct, 'status_out_updated_at');
        $this->renameColumn($this->tAct, 'updated', 'status_out_updated_at');
        $this->update($this->tAct, ['status_out_updated_at' => null], ['status_out_updated_at' => 0]);
    }

    public function safeDown()
    {
        echo 'This migration should not be rolled back';
    }
}


