<?php

use console\components\db\Migration;
use yii\db\Schema;

class m220125_185430_alter_balance_initial_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('balance_initial', 'fixed_assets_other', $this->bigInteger()->notNull()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('balance_initial', 'fixed_assets_other');
    }
}
