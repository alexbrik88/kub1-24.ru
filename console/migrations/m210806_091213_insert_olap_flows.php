<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210806_091213_insert_olap_flows extends Migration
{
    public function safeUp()
    {
        $this->insertCashBankForeignCurrencyFlows();
        $this->insertCashOrderForeignCurrencyFlows();
        $this->insertCashEmoneyForeignCurrencyFlows();
    }

    public function safeDown()
    {
        $this->delete('{{%olap_flows}}', ['wallet' => [11, 12, 13]]);
    }

    private function insertCashBankForeignCurrencyFlows()
    {
        $this->execute('
            INSERT IGNORE INTO `olap_flows` (
                `id`,
                `company_id`,
                `contractor_id`,
                `account_id`,
                `project_id`,
                `sale_point_id`,
                `industry_id`,
                `wallet`,
                `type`,
                `is_accounting`,
                `item_id`,
                `date`,
                `recognition_date`,
                `is_prepaid_expense`,
                `amount`,
                `original_amount`,
                `currency_id`,
                `currency_name`,
                `has_invoice`,
                `invoice_amount`)
            SELECT
                `t`.`id`,
                `t`.`company_id`,
                `t`.`contractor_id`,
                `t`.`account_id`,
                `t`.`project_id`,
                `t`.`sale_point_id`,
                `t`.`industry_id`,
                11, /* WALLET */
                `t`.`flow_type`,
                1,
                IF (`t`.flow_type = 0, `t`.expenditure_item_id, `t`.income_item_id),
                `t`.`date`,
                `t`.`recognition_date`,
                `t`.`is_prepaid_expense`,
                `t`.`amount_rub`,
                `t`.`amount`,
                `t`.`currency_id`,
                `t`.`currency_name`,
                IF(`t2`.`flow_id` IS NULL, 0, 1),
                IFNULL(SUM(`t2`.`amount`), 0)
            FROM `cash_bank_foreign_currency_flows` `t`
            LEFT JOIN `cash_bank_foreign_currency_flows_to_invoice` `t2` ON `t`.`id` = `t2`.`flow_id`
            GROUP BY `t`.`id`
        ');
    }

    private function insertCashOrderForeignCurrencyFlows()
    {
        $this->execute('
            INSERT IGNORE INTO `olap_flows` (
                `id`,
                `company_id`,
                `contractor_id`,
                `account_id`,
                `project_id`,
                `sale_point_id`,
                `industry_id`,
                `wallet`,
                `type`,
                `is_accounting`,
                `item_id`,
                `date`,
                `recognition_date`,
                `is_prepaid_expense`,
                `amount`,
                `original_amount`,
                `currency_id`,
                `currency_name`,
                `has_invoice`,
                `invoice_amount`)
            SELECT
                `t`.`id`,
                `t`.`company_id`,
                `t`.`contractor_id`,
                `t`.`cashbox_id`,
                `t`.`project_id`,
                `t`.`sale_point_id`,
                `t`.`industry_id`,
                12, /* WALLET */
                `t`.`flow_type`,
                `t`.`is_accounting`,
                IF (`t`.flow_type = 0, `t`.expenditure_item_id, `t`.income_item_id),
                `t`.`date`,
                `t`.`recognition_date`,
                `t`.`is_prepaid_expense`,
                `t`.`amount_rub`,
                `t`.`amount`,
                `t`.`currency_id`,
                `t`.`currency_name`,
                IF(`t2`.`flow_id` IS NULL, 0, 1),
                IFNULL(SUM(`t2`.`amount`), 0)
            FROM `cash_order_foreign_currency_flows` `t`
            LEFT JOIN `cash_order_foreign_currency_flows_to_invoice` `t2` ON `t`.`id` = `t2`.`flow_id`
            GROUP BY `t`.`id`
        ');
    }

    private function insertCashEmoneyForeignCurrencyFlows()
    {
        $this->execute('
            INSERT IGNORE INTO `olap_flows` (
                `id`,
                `company_id`,
                `contractor_id`,
                `account_id`,
                `project_id`,
                `sale_point_id`,
                `industry_id`,
                `wallet`,
                `type`,
                `is_accounting`,
                `item_id`,
                `date`,
                `recognition_date`,
                `is_prepaid_expense`,
                `amount`,
                `original_amount`,
                `currency_id`,
                `currency_name`,
                `has_invoice`,
                `invoice_amount`)
            SELECT
                `t`.`id`,
                `t`.`company_id`,
                `t`.`contractor_id`,
                `t`.`emoney_id`,
                `t`.`project_id`,
                `t`.`sale_point_id`,
                `t`.`industry_id`,
                13, /* WALLET */
                `t`.`flow_type`,
                `t`.`is_accounting`,
                IF (`t`.flow_type = 0, `t`.expenditure_item_id, `t`.income_item_id),
                `t`.`date`,
                `t`.`recognition_date`,
                `t`.`is_prepaid_expense`,
                `t`.`amount_rub`,
                `t`.`amount`,
                `t`.`currency_id`,
                `t`.`currency_name`,
                IF(`t2`.`flow_id` IS NULL, 0, 1),
                IFNULL(SUM(`t2`.`amount`), 0)
            FROM `cash_emoney_foreign_currency_flows` `t`
            LEFT JOIN `cash_emoney_foreign_currency_flows_to_invoice` `t2` ON `t`.`id` = `t2`.`flow_id`
            GROUP BY `t`.`id`
        ');
    }
}
