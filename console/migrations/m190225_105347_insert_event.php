<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190225_105347_insert_event extends Migration
{
    public function safeUp()
    {
        $this->batchInsert('event', ['id', 'name'], [

            ['97', 'Робот бухгалтер. Загрузил выписку'],

        ]);
    }

    public function safeDown()
    {
        $this->delete('event', ['id' => [97]]);
    }
}
