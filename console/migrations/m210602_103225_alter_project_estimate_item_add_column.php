<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210602_103225_alter_project_estimate_item_add_column extends Migration
{
    public function safeUp()
    {
        $this->createIndex('project_estimate_item_estimate_id_idx', 'project_estimate_item', 'id_project_estimate');
    }

    public function safeDown()
    {
        $this->dropIndex('project_estimate_item_estimate_id_idx', 'project_estimate_item');
    }
}
