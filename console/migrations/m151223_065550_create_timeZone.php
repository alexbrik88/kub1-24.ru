<?php

use console\components\db\Migration;
use yii\db\Schema;

class m151223_065550_create_timeZone extends Migration
{
    public $tTimeZone = 'time_zone';

    public function safeUp()
    {
        $this->createTable($this->tTimeZone, [
            'id' => $this->primaryKey(),
            'name' => $this->string(2)->defaultValue(null),
            'time_zone' => $this->string(30)->defaultValue(null),
        ], $this->tableOptions);
    }
    
    public function safeDown()
    {
        $this->dropTable($this->tTimeZone);
    }
}


