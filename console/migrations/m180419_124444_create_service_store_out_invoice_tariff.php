<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180419_124444_create_service_store_out_invoice_tariff extends Migration
{
    public $tableName = 'service_store_out_invoice_tariff';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'links_count' => $this->string()->notNull(),
            'cost_one_month_for_one_link' => $this->integer()->defaultValue(null),
            'total_amount' => $this->integer()->notNull(),
        ]);

        $this->batchInsert($this->tableName, ['id', 'links_count', 'cost_one_month_for_one_link', 'total_amount'], [
            [1, 3, 200, 7200],
            [2, 10, 150, 18000],
            [3, 'Unlim', null, 60000],
        ]);
    }
    
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}


