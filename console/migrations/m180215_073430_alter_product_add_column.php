<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180215_073430_alter_product_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('product', 'status', $this->smallInteger(2)->defaultValue(1)->after('production_type'));
        $this->addColumn('product', 'article', $this->string(50)->after('code'));
        $this->addColumn('product', 'weight', $this->decimal(20, 10));
        $this->addColumn('product', 'volume', $this->decimal(20, 10));

        $this->createIndex('K_status', 'product', 'status');
    }

    public function safeDown()
    {
        $this->dropColumn('product', 'status');
        $this->dropColumn('product', 'article');
        $this->dropColumn('product', 'weight');
        $this->dropColumn('product', 'volume');
    }
}
