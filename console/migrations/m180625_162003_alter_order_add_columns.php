<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180625_162003_alter_order_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('order', 'article', $this->string(50)->after('product_code'));

        $this->execute("
            UPDATE {{order}}
            LEFT JOIN {{product}} ON {{product}}.[[id]] = {{order}}.[[product_id]]
            SET {{order}}.[[article]] = {{product}}.[[article]];
        ");
    }

    public function safeDown()
    {
        $this->dropColumn('order', 'article');
    }
}
