<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210607_085008_alter_user_config_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_config', 'report_odds_income_percent', $this->tinyInteger(1)->notNull()->defaultValue(0));
        $this->addColumn('user_config', 'report_odds_expense_percent', $this->tinyInteger(1)->notNull()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('user_config', 'report_odds_income_percent');
        $this->dropColumn('user_config', 'report_odds_expense_percent');
    }
}
