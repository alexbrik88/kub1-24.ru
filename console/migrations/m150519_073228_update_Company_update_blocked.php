<?php

use yii\db\Schema;
use yii\db\Migration;

class m150519_073228_update_Company_update_blocked extends Migration
{
    public function safeUp()
    {
        $this->update('company', [
            'blocked' => 0,
        ]);
        $this->alterColumn('company', 'blocked', Schema::TYPE_BOOLEAN . ' NOT NULL COMMENT "0 - активна, 1 - заблокирована"');
    }
    
    public function safeDown()
    {
        $this->alterColumn('company', 'blocked', Schema::TYPE_BOOLEAN . ' COMMENT "0 - активна, 1 - заблокирована"');
        $this->update('company', [
            'blocked' => null,
        ]);
    }
}
