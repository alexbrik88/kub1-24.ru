<?php

use yii\db\Schema;
use yii\db\Migration;

class m151211_115506_alter_subscribe_addColumn_duration extends Migration
{
    public $tSubscribe = 'service_subscribe';
    public $tTariff = 'service_subscribe_tariff';

    public function safeUp()
    {
        $this->addColumn($this->tSubscribe, 'duration_month', $this->integer()->notNull()->defaultValue(0) . ' AFTER updated_at');
        $this->addColumn($this->tSubscribe, 'duration_day', $this->integer()->notNull()->defaultValue(0) . ' AFTER duration_month');

        $this->execute("
            UPDATE {$this->tSubscribe} AS subscribe
                LEFT JOIN {$this->tTariff} AS tariff ON tariff.id = subscribe.tariff_id
                SET subscribe.duration_month = tariff.duration_month,
                    subscribe.duration_month = tariff.duration_day
        ");
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tSubscribe, 'duration_month');
        $this->dropColumn($this->tSubscribe, 'duration_day');
    }
}
