<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200716_210039_alter_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_config','report_debtor_help',$this->boolean()->notNull()->defaultValue(false));
        $this->addColumn('user_config','report_debtor_chart',$this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('user_config','report_debtor_help');
        $this->dropColumn('user_config','report_debtor_chart');
    }
}
