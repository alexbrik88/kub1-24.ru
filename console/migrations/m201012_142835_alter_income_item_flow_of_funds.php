<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201012_142835_alter_income_item_flow_of_funds extends Migration
{
    public function safeUp()
    {
        $this->addColumn('income_item_flow_of_funds', 'is_prepayment', $this->tinyInteger()->notNull()->defaultValue(0));

        $this->execute("
            UPDATE `income_item_flow_of_funds` 
            SET `is_prepayment` = 1 
            WHERE `income_item_id` IN (1) 
              OR `income_item_id` > 99; 
        ");
    }

    public function safeDown()
    {
        $this->dropColumn('income_item_flow_of_funds', 'is_prepayment');
    }
}
