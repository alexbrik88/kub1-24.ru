<?php

use console\components\db\Migration;
use common\models\FsspTask;

class m200208_105003_fssp_task_company_id_add extends Migration
{
    const TABLE = '{{%fssp_task}}';

    public function safeUp()
    {
        $this->addColumn(self::TABLE, 'company_id', $this->integer(11)->comment('ID компании, если данные относятся к компании')->first());
        $this->addForeignKey('fssp_task_company_id', self::TABLE, 'company_id', '{{%company}}', 'id',
            'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropForeignKey('fssp_task_company_id', self::TABLE);
        $this->dropColumn(self::TABLE, 'company_id');
        $this->addPrimaryKey('contractor_id', self::TABLE, ['contractor_id']);
    }
}
