<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191203_170857_alter_employee_role_add_column extends Migration
{
    protected $sort = [
        1 => 1000,
        2 => 8000,
        3 => 6000,
        4 => 9000,
        5 => 2000,
        6 => 3000,
        7 => 10000,
        8 => 4000,
        9 => 5000,
        10 => 7000,
    ];

    public function safeUp()
    {
        $this->addColumn('{{%employee_role}}', 'sort', $this->integer());

        foreach ($this->sort as $key => $value) {
            $this->update('{{%employee_role}}', ['sort' => $value], ['id' => $key]);
        }
    }

    public function safeDown()
    {
        $this->dropColumn('{{%employee_role}}', 'sort');
    }
}
