<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180322_075741_add_new_columns_to_okved_code extends Migration
{
    public $tableName = 'okved_code';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'class', $this->string()->defaultValue(null));
        $this->addColumn($this->tableName, 'class_name', $this->string()->defaultValue(null));
        $this->addColumn($this->tableName, 'class_description', $this->text()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'class');
        $this->dropColumn($this->tableName, 'class_name');
        $this->dropColumn($this->tableName, 'class_description');
    }
}


