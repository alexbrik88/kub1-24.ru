<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200220_152344_alter_price_list_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('price_list', 'include_image_column', $this->boolean()->defaultValue(false)->notNull()->after('include_description_column'));
    }

    public function safeDown()
    {
        $this->dropColumn('price_list', 'include_image_column');
    }
}
