<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210204_101717_update_country extends Migration
{
    public function safeUp()
    {
        $this->update('{{%country}}', [
            'name_short' => 'ВЕЛИКОБРИТАНИЯ',
        ], [
            'id' => 196,
        ]);
        $this->update('{{%country}}', [
            'name_short' => 'США',
        ], [
            'id' => 197,
        ]);
    }

    public function safeDown()
    {
        $this->update('{{%country}}', [
            'name_short' => 'СОЕДИНЕННОЕ КОРОЛЕВСТВО',
        ], [
            'id' => 196,
        ]);
        $this->update('{{%country}}', [
            'name_short' => 'СОЕДИНЕННЫЕ ШТАТЫ',
        ], [
            'id' => 197,
        ]);

    }
}
