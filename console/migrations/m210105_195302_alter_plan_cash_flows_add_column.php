<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210105_195302_alter_plan_cash_flows_add_column extends Migration
{
    private $table = '{{%plan_cash_flows}}';

    const BANK_TEXT = 'bank';
    const ORDER_TEXT = 'order';
    const EMONEY_TEXT = 'emoney';

    public function safeUp()
    {
        $this->addColumn($this->table, 'is_internal_transfer', $this->boolean()->notNull()->defaultValue(false));
        $this->update($this->table, ['is_internal_transfer' => 1], ['contractor_id' => [self::BANK_TEXT, self::ORDER_TEXT, self::EMONEY_TEXT]]);
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'is_internal_transfer');
    }
}
