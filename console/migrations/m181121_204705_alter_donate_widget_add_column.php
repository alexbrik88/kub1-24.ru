<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181121_204705_alter_donate_widget_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('donate_widget', 'need_contract', $this->boolean()->defaultValue(false)->after('notify_to_email'));
        $this->addColumn('donate_widget', 'need_contract_sum', $this->integer()->defaultValue(null)->after('need_contract'));
    }

    public function safeDown()
    {
        $this->dropColumn('donate_widget', 'need_contract');
        $this->dropColumn('donate_widget', 'need_contract_sum');
    }
}
