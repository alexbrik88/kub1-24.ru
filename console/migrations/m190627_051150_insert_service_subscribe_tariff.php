<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190627_051150_insert_service_subscribe_tariff extends Migration
{
    public function safeUp()
    {
        $this->batchInsert('service_subscribe_tariff', [
            'id',
            'tariff_group_id',
            'duration_month',
            'duration_day',
            'price',
            'is_active',
            'proposition',
        ], [
            [16, 6, 3, 0, 1500, 1, 'Получи скидку 10% при оплате за год'],
            [17, 6, 12, 0, 5400, 1, 'Лучшая цена!'],
        ]);
    }

    public function safeDown()
    {
        $this->delete('service_subscribe_tariff', ['id' => [16, 17]]);
    }
}
