<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210528_122618_alter_ofd_receipt_item_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%ofd_receipt_item}}', 'product_id', $this->integer()->after('receipt_uid'));
        $this->addForeignKey('fk_ofd_receipt_item__product_id', '{{%ofd_receipt_item}}', 'product_id', '{{%product}}', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_ofd_receipt_item__product_id', '{{%ofd_receipt_item}}');
        $this->dropColumn('{{%ofd_receipt_item}}', 'product_id');
    }
}
