<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180205_191505_alter_cash_order_flows_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cash_order_flows', 'cashbox_id', $this->integer()->after('company_id'));

        $this->addForeignKey('FK_cashOrderFlows_cashbox', 'cash_order_flows', 'cashbox_id', 'cashbox', 'id');

        $this->execute("
            UPDATE {{cash_order_flows}} {{t}}
            LEFT JOIN {{cashbox}} {{c}} ON {{t}}.[[company_id]] = {{c}}.[[company_id]]
            SET {{t}}.[[cashbox_id]] = {{c}}.[[id]]
        ");
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_cashOrderFlows_cashbox', 'cash_order_flows');

        $this->dropColumn('cash_order_flows', 'cashbox_id');
    }
}
