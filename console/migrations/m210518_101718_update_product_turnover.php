<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210518_101718_update_product_turnover extends Migration
{
    public function safeUp()
    {
        $this->execute('
            UPDATE {{%product_turnover}}
            INNER JOIN {{%act}} ON {{%product_turnover}}.[[document_id]] = {{%act}}.[[id]] AND {{%product_turnover}}.[[document_table]] = "act"
            SET {{%product_turnover}}.[[is_document_actual]] = 1
            WHERE {{%act}}.[[type]] = 1
            AND {{%act}}.[[status_out_id]] IS NULL
        ');

        $this->execute('
            UPDATE {{%product_turnover}}
            INNER JOIN {{%packing_list}} ON {{%product_turnover}}.[[document_id]] = {{%packing_list}}.[[id]] AND {{%product_turnover}}.[[document_table]] = "packing_list"
            SET {{%product_turnover}}.[[is_document_actual]] = 1
            WHERE {{%packing_list}}.[[type]] = 1
            AND {{%packing_list}}.[[status_out_id]] IS NULL
        ');

        $this->execute('
            UPDATE {{%product_turnover}}
            INNER JOIN {{%sales_invoice}} ON {{%product_turnover}}.[[document_id]] = {{%sales_invoice}}.[[id]] AND {{%product_turnover}}.[[document_table]] = "sales_invoice"
            SET {{%product_turnover}}.[[is_document_actual]] = 1
            WHERE {{%sales_invoice}}.[[type]] = 1
            AND {{%sales_invoice}}.[[status_out_id]] IS NULL
        ');

        $this->execute('
            UPDATE {{%product_turnover}}
            INNER JOIN {{%upd}} ON {{%product_turnover}}.[[document_id]] = {{%upd}}.[[id]] AND {{%product_turnover}}.[[document_table]] = "upd"
            SET {{%product_turnover}}.[[is_document_actual]] = 1
            WHERE {{%upd}}.[[type]] = 1
            AND {{%upd}}.[[status_out_id]] IS NULL
        ');
    }

    public function safeDown()
    {
        $this->execute('
            UPDATE {{%product_turnover}}
            INNER JOIN {{%act}} ON {{%product_turnover}}.[[document_id]] = {{%act}}.[[id]] AND {{%product_turnover}}.[[document_table]] = "act"
            SET {{%product_turnover}}.[[is_document_actual]] = 0
            WHERE {{%act}}.[[type]] = 1
            AND {{%act}}.[[status_out_id]] IS NULL
        ');

        $this->execute('
            UPDATE {{%product_turnover}}
            INNER JOIN {{%packing_list}} ON {{%product_turnover}}.[[document_id]] = {{%packing_list}}.[[id]] AND {{%product_turnover}}.[[document_table]] = "packing_list"
            SET {{%product_turnover}}.[[is_document_actual]] = 0
            WHERE {{%packing_list}}.[[type]] = 1
            AND {{%packing_list}}.[[status_out_id]] IS NULL
        ');

        $this->execute('
            UPDATE {{%product_turnover}}
            INNER JOIN {{%sales_invoice}} ON {{%product_turnover}}.[[document_id]] = {{%sales_invoice}}.[[id]] AND {{%product_turnover}}.[[document_table]] = "sales_invoice"
            SET {{%product_turnover}}.[[is_document_actual]] = 0
            WHERE {{%sales_invoice}}.[[type]] = 1
            AND {{%sales_invoice}}.[[status_out_id]] IS NULL
        ');

        $this->execute('
            UPDATE {{%product_turnover}}
            INNER JOIN {{%upd}} ON {{%product_turnover}}.[[document_id]] = {{%upd}}.[[id]] AND {{%product_turnover}}.[[document_table]] = "upd"
            SET {{%product_turnover}}.[[is_document_actual]] = 0
            WHERE {{%upd}}.[[type]] = 1
            AND {{%upd}}.[[status_out_id]] IS NULL
        ');
    }
}
