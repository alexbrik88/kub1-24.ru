<?php

use console\components\db\Migration;

class m191015_105910_create_insales_account extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%insales_account}}', [
            'id' => $this->primaryKey()->unsigned()->notNull()->comment('ID магазина InSales CMS'),
            'employee_id' => $this->integer()->comment('ID покупателя сайта'),
            'user_id' => $this->integer()->unsigned()->comment('ID пользователя в InSales CMS'),
            'shop' => $this->string(40)->notNull()->comment('Домен магазина'),
            'password' => $this->string(32)->notNull()->comment('Пароль для доступа к API'),
        ], "COMMENT 'Интеграция InSales CMS: магазины/аккаунты CMS (связь с клиентами сайта)'");
        $this->addForeignKey(
            'insales_account_employee_id', '{{%insales_account}}', 'employee_id',
            '{{%employee}}', 'id', 'CASCADE', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable('{{%insales_account}}');
    }
}
