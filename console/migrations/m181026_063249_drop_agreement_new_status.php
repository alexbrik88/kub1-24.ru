<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181026_063249_drop_agreement_new_status extends Migration
{
    public function safeUp()
    {
        $this->dropTable('{{%agreement_new_status}}');
    }
    
    public function safeDown()
    {

    }
}


