<?php

use console\components\db\Migration;
use yii\db\Schema;

class m161006_062842_add_main_company_id_to_company extends Migration
{
    public $tCompany = 'company';

    public function safeUp()
    {
        $this->addColumn($this->tCompany, 'main_id', $this->integer()->notNull());
        $this->update($this->tCompany, ['main_id' => new \yii\db\Expression('id')]);
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tCompany, 'main_id');
    }
}


