<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200203_155947_create_price_list_status extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%price_list_status}}', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ], $tableOptions);

        $this->batchInsert('{{%price_list_status}}', ['id', 'name'], [
            [1, 'Создан'],
            [2, 'Отправлен']
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%price_list_status}}');
    }
}
