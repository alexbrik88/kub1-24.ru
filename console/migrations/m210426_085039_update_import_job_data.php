<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210426_085039_update_import_job_data extends Migration
{
    public function safeUp()
    {
        $this->execute('UPDATE `import_job_data` SET `is_viewed` = 1');
    }

    public function safeDown()
    {
        // no down
    }
}
