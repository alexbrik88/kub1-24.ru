<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180919_130802_alter_company_drop_column extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('company', 'company_product_types');
    }

    public function safeDown()
    {
        $this->addColumn(
            'company',
            'company_product_types',
            $this->string(45)->defaultValue('[1,2]')->after('chief_accountant_signature_link')
        );
    }
}
