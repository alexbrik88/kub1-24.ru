<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200203_160231_alter_price_list_add_columns extends Migration
{
    public $table = 'price_list';

    public function safeUp()
    {
        $this->addColumn($this->table, 'status_id', $this->integer()->notNull()->defaultValue(1));
        $this->addColumn($this->table, 'status_author_id', $this->integer());
        $this->addColumn($this->table, 'status_updated_at', $this->integer());
        $this->addColumn($this->table, 'is_archive', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->table, 'comment_internal', $this->text()->defaultValue(null));

        $this->addForeignKey('FK_'.$this->table.'_to_status', $this->table, 'status_id', 'price_list_status', 'id');
        $this->addForeignKey('FK_'.$this->table.'_to_employee', $this->table, 'status_author_id', 'employee', 'id', 'SET NULL');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_'.$this->table.'_to_status', $this->table);
        $this->dropForeignKey('FK_'.$this->table.'_to_employee', $this->table);

        $this->dropColumn($this->table, 'status_id');
        $this->dropColumn($this->table, 'status_author_id');
        $this->dropColumn($this->table, 'status_updated_at');
        $this->dropColumn($this->table, 'is_archive');
        $this->dropColumn($this->table, 'comment_internal');
    }
}
