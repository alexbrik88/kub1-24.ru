<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%act_goods_cancellation}}`.
 */
class m220223_105713_create_act_goods_cancellation_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%act_goods_cancellation}}', [
            'document_id' => $this->integer()->notNull(),
            'cancellation_id' => $this->integer()->notNull(),
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', '{{%act_goods_cancellation}}', [
            'document_id',
            'cancellation_id',
        ]);

        $this->addForeignKey('fk_act_goods_cancellation__document_id',
            '{{%act_goods_cancellation}}',
            'document_id',
            '{{%act}}',
            'id'
        );

        $this->addForeignKey('fk_act_goods_cancellation__cancellation_id',
            '{{%act_goods_cancellation}}',
            'cancellation_id',
            '{{%goods_cancellation}}',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%act_goods_cancellation}}');
    }
}
