<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180328_075729_insert_product_store extends Migration
{
    public function safeUp()
    {
        $this->execute("
            SET @t = UNIX_TIMESTAMP();
            INSERT IGNORE INTO {{product_store}}
                ([[product_id]], [[store_id]], [[created_at]], [[updated_at]])

            SELECT {{product}}.[[id]], {{store}}.[[id]], @t, @t
            FROM {{product}} INNER JOIN {{store}} ON {{product}}.[[company_id]] = {{store}}.[[company_id]]
        ");
    }

    public function safeDown()
    {
        //
    }
}
