<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180306_110838_alter_order_alter_columns extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('order', 'base_price_no_vat', $this->decimal(22, 2)->defaultValue(0));
        $this->alterColumn('order', 'base_price_with_vat', $this->decimal(22, 2)->defaultValue(0));
        $this->alterColumn('order', 'purchase_price_no_vat', $this->decimal(22, 2)->defaultValue(0));
        $this->alterColumn('order', 'purchase_price_with_vat', $this->decimal(22, 2)->defaultValue(0));
        $this->alterColumn('order', 'selling_price_no_vat', $this->decimal(22, 2)->defaultValue(0));
        $this->alterColumn('order', 'selling_price_with_vat', $this->decimal(22, 2)->defaultValue(0));
        $this->alterColumn('order', 'amount_purchase_no_vat', $this->decimal(22, 2)->defaultValue(0));
        $this->alterColumn('order', 'amount_purchase_with_vat', $this->decimal(22, 2)->defaultValue(0));
        $this->alterColumn('order', 'amount_sales_no_vat', $this->decimal(22, 2)->defaultValue(0));
        $this->alterColumn('order', 'amount_sales_with_vat', $this->decimal(22, 2)->defaultValue(0));
        $this->alterColumn('order', 'sale_tax', $this->decimal(22, 2)->defaultValue(0));
        $this->alterColumn('order', 'purchase_tax', $this->decimal(22, 2)->defaultValue(0));
    }

    public function safeDown()
    {
        $this->alterColumn('order', 'base_price_no_vat', $this->bigInteger(20)->defaultValue(0));
        $this->alterColumn('order', 'base_price_with_vat', $this->bigInteger(20)->defaultValue(0));
        $this->alterColumn('order', 'purchase_price_no_vat', $this->bigInteger(20)->defaultValue(0));
        $this->alterColumn('order', 'purchase_price_with_vat', $this->bigInteger(20)->defaultValue(0));
        $this->alterColumn('order', 'selling_price_no_vat', $this->bigInteger(20)->defaultValue(0));
        $this->alterColumn('order', 'selling_price_with_vat', $this->bigInteger(20)->defaultValue(0));
        $this->alterColumn('order', 'amount_purchase_no_vat', $this->bigInteger(20)->defaultValue(0));
        $this->alterColumn('order', 'amount_purchase_with_vat', $this->bigInteger(20)->defaultValue(0));
        $this->alterColumn('order', 'amount_sales_no_vat', $this->bigInteger(20)->defaultValue(0));
        $this->alterColumn('order', 'amount_sales_with_vat', $this->bigInteger(20)->defaultValue(0));
        $this->alterColumn('order', 'sale_tax', $this->bigInteger(20)->defaultValue(0));
        $this->alterColumn('order', 'purchase_tax', $this->bigInteger(20)->defaultValue(0));
    }
}
