<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180705_094404_alter_remaining_amount_in_invoice extends Migration
{
    public $tableName = 'invoice';

    public function safeUp()
    {
        $this->alterColumn($this->tableName, 'remaining_amount', $this->bigInteger(20)->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->alterColumn($this->tableName, 'remaining_amount', $this->integer()->defaultValue(null));
    }
}


