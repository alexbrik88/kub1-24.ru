<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201008_111439_alter_invoice_essence_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%invoice_essence}}', 'text_en', $this->text());
        $this->addColumn('{{%invoice_essence}}', 'is_checked_en', $this->boolean()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%invoice_essence}}', 'text_en');
        $this->dropColumn('{{%invoice_essence}}', 'is_checked_en');
    }
}
