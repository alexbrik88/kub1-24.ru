<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170927_195328_add_chat_volume_to_employee extends Migration
{
    public $tableName = 'employee';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'chat_volume', $this->boolean()->defaultValue(true));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'chat_volume');
    }
}


