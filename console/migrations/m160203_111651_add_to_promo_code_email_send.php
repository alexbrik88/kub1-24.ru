<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160203_111651_add_to_promo_code_email_send extends Migration
{
    public $tPromoCode = 'service_payment_promo_code';

    public function safeUp()
    {
        $this->addColumn($this->tPromoCode, 'email_send', $this->boolean()->defaultValue(0));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tPromoCode, 'email_send');
    }
}


