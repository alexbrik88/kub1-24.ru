<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210330_062453_alter_waybill_alter_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%waybill}}', 'driver_license', $this->string(255));
    }

    public function safeDown()
    {
        $this->alterColumn('{{%waybill}}', 'driver_license', $this->string(45));
    }
}
