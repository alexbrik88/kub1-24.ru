<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170317_104333_alter_invoice_addColumns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%invoice}}', 'basis_document_name', $this->string(255)->defaultValue(null));
        $this->addColumn('{{%invoice}}', 'basis_document_number', $this->string(50)->defaultValue(null));
        $this->addColumn('{{%invoice}}', 'basis_document_date', $this->date()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%invoice}}', 'basis_document_name');
        $this->dropColumn('{{%invoice}}', 'basis_document_number');
        $this->dropColumn('{{%invoice}}', 'basis_document_date');
    }
}
