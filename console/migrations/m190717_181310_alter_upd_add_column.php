<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190717_181310_alter_upd_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('upd', 'object_invoice_facture_guid', $this->string(36)->after('object_guid'));
    }

    public function safeDown()
    {
        $this->dropColumn('upd', 'object_invoice_facture_guid');
    }
}


