<?php

use console\components\db\Migration;

class m191010_092549_create_evotor_user extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{evotor_user}}', [
            'id' => "CHAR(18) NOT NULL PRIMARY KEY COMMENT 'ID пользователя Эвотор'",
            'employee_id' => $this->integer()->notNull()->comment('ID покупателя сайта'),
            'token' => $this->char(36)->comment('Токен доступа для REST API'),
        ], "COMMENT 'Интеграция Эвотор: пользователи Эвотор (учётные записей с сайтом)'");
        $this->addForeignKey(
            'evotor_employee_id', '{{evotor_user}}', 'employee_id',
            '{{employee}}', 'id', 'CASCADE', 'CASCADE'
        );
        $this->createIndex('evotor_user_employee_id', '{{evotor_user}}', 'employee_id', true);
        $this->createIndex('evotor_user_token', '{{evotor_user}}', 'token', true);
    }

    public function down()
    {
        $this->dropTable('{{evotor_user}}');
    }
}
