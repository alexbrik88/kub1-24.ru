<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210802_132858_alter_card_bill_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%card_bill}}', 'currency_id', $this->char(3)->notNull()->after('id'));
        $this->addColumn('{{%card_bill}}', 'currency_name', $this->char(3)->notNull()->after('currency_id'));

        $this->update('{{%card_bill}}', [
            'currency_id' => '643',
            'currency_name' => 'RUB',
        ]);

        $this->createIndex('currency_id', '{{%card_bill}}', 'currency_id');
        $this->createIndex('currency_name', '{{%card_bill}}', 'currency_name');
    }

    public function safeDown()
    {
        $this->dropIndex('currency_id', '{{%card_bill}}');
        $this->dropIndex('currency_name', '{{%card_bill}}');

        $this->dropColumn('{{%card_bill}}', 'currency_id');
        $this->dropColumn('{{%card_bill}}', 'currency_name');
    }
}
