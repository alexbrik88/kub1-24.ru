<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200222_131426_alter_contractor_create_index extends Migration
{
    public function safeUp()
    {
        $this->createIndex('type', '{{%contractor}}', 'type');
        $this->createIndex('status', '{{%contractor}}', 'status');
        $this->createIndex('is_deleted', '{{%contractor}}', 'is_deleted');
        $this->createIndex('face_type', '{{%contractor}}', 'face_type');
        $this->createIndex('created_at', '{{%contractor}}', 'created_at');
        $this->createIndex('employee_id', '{{%contractor}}', 'employee_id');
    }

    public function safeDown()
    {
        $this->dropIndex('type', '{{%contractor}}');
        $this->dropIndex('status', '{{%contractor}}');
        $this->dropIndex('is_deleted', '{{%contractor}}');
        $this->dropIndex('face_type', '{{%contractor}}');
        $this->dropIndex('created_at', '{{%contractor}}');
        $this->dropIndex('employee_id', '{{%contractor}}');
    }
}
