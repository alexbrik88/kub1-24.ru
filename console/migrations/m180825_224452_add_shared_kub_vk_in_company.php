<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180825_224452_add_shared_kub_vk_in_company extends Migration
{
    public $tableName = 'company';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'shared_kub_vk', $this->boolean()->defaultValue(false));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'shared_kub_vk');
    }
}


