<?php

use yii\db\Schema;
use yii\db\Migration;

class m150504_084258_update_Invoice_column_InvoiceStatusId extends Migration
{
    public function safeUp()
    {
        $this->update('invoice', [
            'invoice_status_id' => 1,
        ], 'invoice_status_id = 0');
    }
    
    public function safeDown()
    {
        echo 'Hasn\'t down migration.';
    }
}
