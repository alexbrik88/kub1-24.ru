<?php

use console\components\db\Migration;
use yii\db\Schema;

class m220221_084321_add_registration_page_type extends Migration
{
    public $tableName = '{{%registration_page_type}}';

    public function safeUp()
    {
        $this->insert($this->tableName, [
            'id' => 52, // PAGE_TYPE_ALFABANK
            'name' => 'Альфабанк',
        ]);
    }

    public function safeDown()
    {
        $this->delete($this->tableName, ['id' => 52]);
    }
}
