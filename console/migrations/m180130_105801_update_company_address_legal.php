<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180130_105801_update_company_address_legal extends Migration
{
    public function safeUp()
    {
        $this->execute("
            UPDATE {{company}} {{c}}
            LEFT JOIN {{address_house_type}} AS {{t}} ON {{c}}.[[address_legal_house_type_id]] =  {{t}}.[[id]]
            LEFT JOIN {{address_housing_type}} AS {{h}} ON {{c}}.[[address_legal_housing_type_id]] =  {{h}}.[[id]]
            LEFT JOIN {{address_apartment_type}} AS {{a}} ON {{c}}.[[address_legal_apartment_type_id]] =  {{a}}.[[id]]
            SET {{c}}.[[address_legal]] = CONCAT(
                IF(CHAR_LENGTH({{c}}.[[address_legal_postcode]]) = 6, {{c}}.[[address_legal_postcode]], '000000'),
                IF({{c}}.[[address_legal_city]] IS NULL OR {{c}}.[[address_legal_city]] = '', '', CONCAT(
                    ', ',
                    {{c}}.[[address_legal_city]]
                )),
                IF({{c}}.[[address_legal_street]] IS NULL OR {{c}}.[[address_legal_street]] = '', '', CONCAT(
                    ', ',
                    {{c}}.[[address_legal_street]]
                )),
                IF({{c}}.[[address_legal_house]] IS NULL OR {{c}}.[[address_legal_house]] = '', '', CONCAT(
                    ', ',
                    IF({{t}}.[[name]] IS NULL, '', CONCAT(LOWER({{t}}.[[name]]), ' ')),
                    {{c}}.[[address_legal_house]]
                )),
                IF({{c}}.[[address_legal_housing]] IS NULL OR {{c}}.[[address_legal_housing]] = '', '', CONCAT(
                    ', ',
                    IF({{h}}.[[name_short]] IS NULL, '', CONCAT({{h}}.[[name_short]], ' ')),
                    {{c}}.[[address_legal_housing]]
                )),
                IF({{c}}.[[address_legal_apartment]] IS NULL OR {{c}}.[[address_legal_apartment]] = '', '', CONCAT(
                    ', ',
                    IF({{a}}.[[name_short]] IS NULL, '', CONCAT({{a}}.[[name_short]], ' ')),
                    {{c}}.[[address_legal_apartment]]
                ))
            )
            WHERE ({{c}}.[[address_legal]] IS NULL OR {{c}}.[[address_legal]] = '');
        ");
    }

    public function safeDown()
    {

    }
}
