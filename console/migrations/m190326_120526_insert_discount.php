<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190326_120526_insert_discount extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%discount}}', [
            'tariff_id' => '11',
            'value' => 60,
            'is_active' => 1,
            'active_from' => time(),
            'active_to' => strtotime('2019-03-28'),
        ]);
        $this->insert('{{%discount}}', [
            'tariff_id' => '11',
            'value' => 40,
            'is_active' => 1,
            'active_from' => time(),
            'active_to' => strtotime('2019-03-31'),
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%discount}}', [
            'tariff_id' => '11',
            'value' => 60,
            'is_active' => 1,
            'active_to' => strtotime('2019-03-28'),
        ]);
        $this->delete('{{%discount}}', [
            'tariff_id' => '11',
            'value' => 40,
            'is_active' => 1,
            'active_to' => strtotime('2019-03-31'),
        ]);
    }
}
