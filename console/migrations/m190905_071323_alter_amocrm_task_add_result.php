<?php

use console\components\db\Migration;

class m190905_071323_alter_amocrm_task_add_result extends Migration
{
    public function up()
    {
        $this->addColumn('{{amocrm_task}}', 'result', $this->string(1000));

    }

    public function down()
    {
        $this->dropColumn('{{amocrm}}', 'result');

    }
}
