<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210914_115243_insert_moysklad_document_type extends Migration
{
    public function safeUp()
    {
        $this->execute('INSERT INTO `moysklad_document_type` (`id`, `name`) VALUES (14, "Расх.КассовыйОрдер"), (24, "Прих.КассовыйОрдер")');
    }

    public function safeDown()
    {
        $this->execute('DELETE FROM `moysklad_document_type` WHERE `id` IN (14, 24)');
    }
}
