<?php

use console\components\db\Migration;
use yii\db\Schema;

class m171027_201443_inser_invoice_expenditure_items extends Migration
{
    public $tableName = 'invoice_expenditure_item';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'type', $this->integer()->defaultValue(null));

        $this->batchInsert($this->tableName, ['id', 'company_id', 'name', 'sort', 'type'], [
            [112, null, 'Займ', 100, 1],
            [113, null, 'Кредит', 100, 1],
            [114, null, 'От учредителя', 100, 1],

            [115, null, 'Погашение займа', 100, 2],
            [116, null, 'Погашение кредита', 100, 2],
            [117, null, 'Уплата процентов', 100, 2],
            [118, null, 'Возврат учредителю', 100, 2],

            [119, null, 'Оплата от покупателя', 100, 3],
            [120, null, 'Прочие поступления', 100, 3],
            [121, null, 'Взнос наличными', 100, 3],
            [122, null, 'Возврат', 100, 3],
        ]);
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'type');

        $this->delete($this->tableName, ['id' => 112]);
        $this->delete($this->tableName, ['id' => 113]);
        $this->delete($this->tableName, ['id' => 114]);

        $this->delete($this->tableName, ['id' => 115]);
        $this->delete($this->tableName, ['id' => 116]);
        $this->delete($this->tableName, ['id' => 117]);
        $this->delete($this->tableName, ['id' => 118]);

        $this->delete($this->tableName, ['id' => 119]);
        $this->delete($this->tableName, ['id' => 120]);
        $this->delete($this->tableName, ['id' => 121]);
        $this->delete($this->tableName, ['id' => 122]);
    }
}


