<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191223_073103_alter_autovoronka_add_column extends Migration
{
    public function safeUp()
    {
        $this->truncateTable('{{%autovoronka}}');

        $this->dropIndex('is_lm', '{{%autovoronka}}');

        $this->dropColumn('{{%autovoronka}}', 'is_lm');

        $this->addColumn('{{%autovoronka}}', 'case', $this->string()->after('email'));
        $this->addColumn('{{%autovoronka}}', 'list_id', $this->string()->after('case'));

        $this->createIndex('case', '{{%autovoronka}}', 'case');
        $this->createIndex('list_id', '{{%autovoronka}}', 'list_id');
    }

    public function safeDown()
    {
        $this->dropIndex('list_id', '{{%autovoronka}}');
        $this->dropIndex('case', '{{%autovoronka}}');

        $this->dropColumn('{{%autovoronka}}', 'list_id');
        $this->dropColumn('{{%autovoronka}}', 'case');

        $this->addColumn('{{%autovoronka}}', 'is_lm', $this->string()->after('email'));

        $this->createIndex('is_lm', '{{%autovoronka}}', 'is_lm');
    }
}
