<?php

use console\components\db\Migration;
use yii\db\Schema;
use common\models\employee\Employee;

class m151218_120450_alter_employee_add_time_zone extends Migration
{
    public function safeUp()
    {
        $this->addColumn(Employee::tableName(), 'time_zone', $this->string()->notNull()->defaultValue('Europe/Moscow'));
    }
    
    public function safeDown()
    {
        $this->dropColumn(Employee::tableName(), 'time_zone');
    }
}


