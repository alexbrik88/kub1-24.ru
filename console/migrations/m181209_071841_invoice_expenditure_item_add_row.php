<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181209_071841_invoice_expenditure_item_add_row extends Migration
{
    public function up()
    {
        $this->batchInsert('invoice_expenditure_item', ['id', 'name', 'sort'], [
            [46, 'Фиксированный платеж в ОМС', '100'],
        ]);
    }

    public function safeDown()
    {
        $builder = new \yii\db\QueryBuilder($this->db);
        $params = [];
        $sql = $builder->delete('invoice_expenditure_item', [
            'name' => [
                'Фиксированный платеж в ОМС',
            ],
        ], $params);
        $this->execute($sql, $params);
    }
}

