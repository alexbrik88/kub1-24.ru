<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210715_142428_insert_employee_info_role extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%employee_info_role}}', [
            'id' => '9',
            'name' => 'Руководитель и собственник компании',
            //'sort' => '15',
        ]);
    }

    public function safeDown()
    {
        $this->insert('{{%employee_info_role}}', [
            'id' => '9',
        ]);
    }
}
