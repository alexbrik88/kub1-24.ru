<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210225_093328_alter_employee_company_add_column extends Migration
{
    public $table = 'employee_company';
    public $column = 'analytics_multi_company';

    public function safeUp()
    {
        $this->addColumn($this->table, $this->column, $this->boolean()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, $this->column);
    }
}
