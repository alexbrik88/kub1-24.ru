<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191128_181726_create_company_online_payment extends Migration
{
    public $table = 'online_payment_type';
    public $relTable = 'company_to_online_payment_type';

    public function safeUp()
    {
        $companyTable = 'company';

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        // online payment types
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer(),
            'name' => $this->string(255)->notNull(),
            'sort' => $this->smallInteger()->defaultValue(100),
        ], $tableOptions);

        $this->batchInsert($this->table, ['id', 'name', 'sort'], [
            [1, "Монета", 1],
            [2, "PayAnyWay", 1],
            [3, "QIWI.Касса", 1],
            [4, "Робокасса", 1],
            [5, "Яндекс.Касса", 1],
            [6, "Сбербанк.Онлайн", 1],
            [7, "Тинькофф", 1],
            [8, "PayKeeper", 1],
            [9, "PayOnline", 1],
            [10, "Fondy", 1],
            [11, "АльфаБанк", 1],
            [100, "Единая касса", 1],
        ]);

        $this->execute("UPDATE `{$this->table}` set `id` = 12 WHERE `id` = 100");
        $this->addForeignKey("FK_{$this->table}_{$companyTable}", $this->table, 'company_id', $companyTable, 'id', 'CASCADE');

        $this->createTable($this->relTable, [
            'company_id' => $this->integer()->notNull(),
            'payment_type_id' => $this->integer()->notNull()
        ], $tableOptions);
        $this->addPrimaryKey('PRIMARY_KEY', $this->relTable, ['company_id', 'payment_type_id']);
        $this->addForeignKey("FK_{$this->relTable}_{$companyTable}", $this->relTable, 'company_id', $companyTable, 'id', 'CASCADE');
        $this->addForeignKey("FK_{$this->relTable}_{$this->table}", $this->relTable, 'payment_type_id', $this->table, 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $companyTable = 'company';

        $this->dropForeignKey("FK_{$this->table}_{$companyTable}", $this->table);
        $this->dropForeignKey("FK_{$this->relTable}_{$companyTable}", $this->relTable);
        $this->dropForeignKey("FK_{$this->relTable}_{$this->table}", $this->relTable);

        $this->dropTable($this->table);
        $this->dropTable($this->relTable);
    }
}
