<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181213_224545_update_logistics_request_loading_and_unloading extends Migration
{
    public $tableName = 'logistics_request_loading_and_unloading';

    public function safeUp()
    {
        $this->dropColumn($this->tableName, 'goods');
        $this->dropColumn($this->tableName, 'weight');
        $this->dropColumn($this->tableName, 'volume');
        $this->dropColumn($this->tableName, 'package');
        $this->addColumn($this->tableName, 'contact_person_phone', $this->string()->defaultValue(null));
    }

    public function safeDown()
    {
        $this->addColumn($this->tableName, 'goods', $this->string()->defaultValue(null));
        $this->addColumn($this->tableName, 'weight', $this->string()->defaultValue(null));
        $this->addColumn($this->tableName, 'volume', $this->string()->defaultValue(null));
        $this->addColumn($this->tableName, 'package', $this->string()->defaultValue(null));
        $this->dropColumn($this->tableName, 'contact_person_phone');
    }
}
