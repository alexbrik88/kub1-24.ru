<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200512_160231_alter_cash_order_flows_alter_column extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%cash_order_flows}}', 'other',  $this->string());
    }

    public function down()
    {
        $this->alterColumn('{{%cash_order_flows}}', 'other',  $this->string()->notNull());
    }
}
