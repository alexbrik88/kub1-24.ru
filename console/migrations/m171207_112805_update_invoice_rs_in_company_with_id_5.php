<?php

use console\components\db\Migration;
use yii\db\Schema;
use common\models\document\Invoice;
use frontend\models\Documents;
use common\models\Company;

class m171207_112805_update_invoice_rs_in_company_with_id_5 extends Migration
{
    public $tableName = 'invoice';

    public function safeUp()
    {
        /* @var $company Company */
        $company = Company::findOne(5);
        /* @var $invoices Invoice[] */
        $invoices = Invoice::find()
            ->byCompany(5)
            ->byIOType(Documents::IO_TYPE_OUT)
            ->byDeleted()
            ->andWhere(['company_rs' => 2147483647])
            ->all();

        foreach ($invoices as $invoice) {
            $this->update($this->tableName, ['company_rs' => $company->mainCheckingAccountant->rs], ['id' => $invoice->id]);
            $this->update($this->tableName, ['company_ks' => $company->mainCheckingAccountant->ks], ['id' => $invoice->id]);
            $this->update($this->tableName, ['company_bik' => $company->mainCheckingAccountant->bik], ['id' => $invoice->id]);
        }
    }

    public function safeDown()
    {
        echo 'This migration not rolled back';
    }
}


