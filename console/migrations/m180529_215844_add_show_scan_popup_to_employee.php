<?php

use console\components\db\Migration;
use \common\models\employee\Employee;

class m180529_215844_add_show_scan_popup_to_employee extends Migration
{
    public function safeUp()
    {
        $this->addColumn(Employee::tableName(), 'show_scan_popup', 'tinyint(1) NOT NULL DEFAULT 0 AFTER `push_notification_overdue_invoice`');
    }

    public function safeDown()
    {
        $this->dropColumn(Employee::tableName(), 'show_scan_popup');
    }
}


