<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190716_162236_insert_registration_page_type extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%registration_page_type}}', [
            'id' => 23,
            'name' => 'МТС',
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%registration_page_type}}', [
            'id' => 23,
        ]);
    }
}
