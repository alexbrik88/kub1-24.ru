<?php

use yii\db\Schema;
use yii\db\Migration;

class m150413_091220_create_product extends Migration
{
    public function up()
    {
        $this->createTable('product', [
            'id' => Schema::TYPE_PK,
            'title' => Schema::TYPE_STRING . '(100) NOT NULL',
        ]);
    }

    public function down()
    {
        $this->dropTable('product');
    }
}
