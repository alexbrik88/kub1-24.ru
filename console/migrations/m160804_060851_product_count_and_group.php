<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160804_060851_product_count_and_group extends Migration
{
    public function safeUp()
    {
        $this->addColumn('product', 'product_count', $this->integer()->defaultValue(0));
        $this->addColumn('product', 'group_id', $this->integer()->defaultValue(1));
        $this->update('product', ['group_id' => 1]);

        $this->createTable('product_group', [
            'id' => $this->primaryKey(),
            'title' => $this->string(60)->notNull(),
            'company_id' => $this->integer()->defaultValue(0),
        ]);

        $this->insert('product_group', [
            'id' => 1,
            'title' => 'Без группы',
            'company_id' => 0
        ]);

        $this->createIndex(
            'idx-poduct-group_id',
            'product',
            'group_id'
        );
        $this->addForeignKey(
            'fk-poduct-group_id',
            'product',
            'group_id',
            'product_group',
            'id',
            'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropColumn('product', 'product_count');
        $this->dropColumn('product', 'group_id');

        $this->dropForeignKey(
            'fk-poduct-group_id',
            'poduct'
        );
        $this->dropIndex(
            'idx-poduct-group_id',
            'product'
        );

        $this->dropTable('product_group');

        echo "m160804_060851_product_count_and_group can be reverted.\n";

        return true;
    }

    /*
    // Use safeUp/safeDown to run migration code within a transaction
    public function safeUp()
    {
    }

    public function safeDown()
    {
    }
    */
}
