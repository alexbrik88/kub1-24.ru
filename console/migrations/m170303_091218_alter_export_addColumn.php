<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170303_091218_alter_export_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%export}}', 'company_id', $this->integer() . " AFTER `user_id`");

        $this->addForeignKey('FK_export_company', '{{%export}}', 'company_id', '{{%company}}', 'id');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('FK_export_company', '{{%export}}');

        $this->dropColumn('{{%export}}', 'company_id');
    }
}
