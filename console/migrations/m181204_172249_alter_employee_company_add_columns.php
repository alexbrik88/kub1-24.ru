<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181204_172249_alter_employee_company_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('employee_company', 'has_salary_1', $this->boolean()->defaultValue(false));
        $this->addColumn('employee_company', 'has_bonus_1', $this->boolean()->defaultValue(false));
        $this->addColumn('employee_company', 'has_salary_2', $this->boolean()->defaultValue(false));
        $this->addColumn('employee_company', 'has_bonus_2', $this->boolean()->defaultValue(false));
        $this->addColumn('employee_company', 'salary_1_amount', $this->bigInteger(20));
        $this->addColumn('employee_company', 'salary_1_prepay_sum', $this->bigInteger(20));
        $this->addColumn('employee_company', 'bonus_1_amount', $this->bigInteger(20));
        $this->addColumn('employee_company', 'bonus_1_prepay_sum', $this->bigInteger(20));
        $this->addColumn('employee_company', 'salary_2_amount', $this->bigInteger(20));
        $this->addColumn('employee_company', 'salary_2_prepay_sum', $this->bigInteger(20));
        $this->addColumn('employee_company', 'bonus_2_amount', $this->bigInteger(20));
        $this->addColumn('employee_company', 'bonus_2_prepay_sum', $this->bigInteger(20));
    }

    public function safeDown()
    {
        $this->dropColumn('employee_company', 'has_salary_1');
        $this->dropColumn('employee_company', 'has_bonus_1');
        $this->dropColumn('employee_company', 'has_salary_2');
        $this->dropColumn('employee_company', 'has_bonus_2');
        $this->dropColumn('employee_company', 'salary_1_amount');
        $this->dropColumn('employee_company', 'salary_1_prepay_sum');
        $this->dropColumn('employee_company', 'bonus_1_amount');
        $this->dropColumn('employee_company', 'bonus_1_prepay_sum');
        $this->dropColumn('employee_company', 'salary_2_amount');
        $this->dropColumn('employee_company', 'salary_2_prepay_sum');
        $this->dropColumn('employee_company', 'bonus_2_amount');
        $this->dropColumn('employee_company', 'bonus_2_prepay_sum');
    }
}
