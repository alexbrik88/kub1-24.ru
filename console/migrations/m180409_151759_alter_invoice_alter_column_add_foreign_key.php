<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180409_151759_alter_invoice_alter_column_add_foreign_key extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('invoice', 'nds_view_type_id', $this->integer(1)->notNull());

        $this->addForeignKey('FK_invoice_ndsViewType', 'invoice', 'nds_view_type_id', 'nds_view_type', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_invoice_ndsViewType', 'invoice');

        $this->alterColumn('invoice', 'nds_view_type_id', $this->integer(1));
    }
}
