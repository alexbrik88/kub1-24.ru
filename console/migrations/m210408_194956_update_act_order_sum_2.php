<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210408_194956_update_act_order_sum_2 extends Migration
{
    public function safeUp()
    {
        $this->execute('
            UPDATE `act` 
            JOIN (
                SELECT ou.act_id, SUM(o.purchase_price_with_vat * ou.quantity) AS summ
                FROM `order_act` ou
                LEFT JOIN `order` `o` ON ou.order_id = o.id
                GROUP BY act_id
            ) t ON t.act_id = act.id
            SET `act`.`order_sum` = t.summ
            WHERE act.type = 1
        ');

        $this->execute('
            UPDATE `act` 
            JOIN (
                SELECT ou.act_id, SUM(o.selling_price_with_vat * ou.quantity) AS summ
                FROM `order_act` ou
                LEFT JOIN `order` `o` ON ou.order_id = o.id
                GROUP BY act_id
            ) t ON t.act_id = act.id
            SET `act`.`order_sum` = t.summ
            WHERE act.type = 2
        ');
    }

    public function safeDown()
    {
        // no down
    }
}
