<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181212_095818_insert_tax_rate extends Migration
{
    public function safeUp()
    {
        $this->insert('tax_rate', [
            'id' => 5,
            'name' => '20%',
            'rate' => '0.2',
        ]);
    }

    public function safeDown()
    {
        $this->delete('tax_rate', [
            'id' => 5,
        ]);
    }
}
