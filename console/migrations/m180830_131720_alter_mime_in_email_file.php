<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180830_131720_alter_mime_in_email_file extends Migration
{
    public $tableName = 'email_file';

    public function safeUp()
    {
        $this->alterColumn($this->tableName, 'mime', $this->string()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->alterColumn($this->tableName, 'mime', $this->string()->notNull());
    }
}


