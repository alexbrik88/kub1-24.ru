<?php

use console\components\db\Migration;
use yii\db\Schema;
use common\models\document\Invoice;

class m170728_154029_update_payment_partial_amount_in_invoice extends Migration
{
    public $tableName = 'invoice';

    public function safeUp()
    {
        Yii::$app->db->createCommand('SET SESSION wait_timeout = 288000;')->execute();
        /* @var $invoice Invoice */
        foreach (Invoice::find()->byDeleted()->all() as $invoice) {
            if ($invoice->getPaidAmount() < $invoice->total_amount_with_nds)
            $this->update($this->tableName, ['payment_partial_amount' => $invoice->getPaidAmount()],['id' => $invoice->id]);
        }
    }
    
    public function safeDown()
    {
        echo 'This migration not rolled back';
    }
}


