<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210728_095230_alter_config_update_default_column_value extends Migration
{
    public $table = 'user_config';

    public function safeUp()
    {
        $this->alterColumn($this->table, 'table_view_finance_plan', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 1');
        $this->execute("UPDATE `user_config` SET `table_view_finance_plan` = 1");
    }

    public function safeDown()
    {
        // no down
    }
}
