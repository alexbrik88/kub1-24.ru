<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200512_094339_alter_company_alter_column extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%company}}', 'last_visit_at',  $this->integer());
    }

    public function down()
    {
        $this->alterColumn('{{%company}}', 'last_visit_at',  $this->integer()->notNull());
    }
}
