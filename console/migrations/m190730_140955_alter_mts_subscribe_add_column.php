<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190730_140955_alter_mts_subscribe_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%mts_subscribe}}', 'is_applied', $this->boolean()->notNull()->defaultValue(false)->after('tariff_id'));
        $this->addColumn('{{%mts_subscribe}}', 'company_number', $this->integer()->notNull()->after('is_applied'));
        $this->addColumn('{{%mts_subscribe}}', 'purchase_inn', $this->string()->after('payment_id'));

        $this->createIndex('is_applied', '{{%mts_subscribe}}', 'is_applied');
        $this->createIndex('unique_payment_id', '{{%mts_subscribe}}', 'payment_id', true);
    }

    public function safeDown()
    {
        $this->dropIndex('is_applied', '{{%mts_subscribe}}');
        $this->dropIndex('unique_payment_id', '{{%mts_subscribe}}');

        $this->dropColumn('{{%mts_subscribe}}', 'purchase_inn');
        $this->dropColumn('{{%mts_subscribe}}', 'company_number');
        $this->dropColumn('{{%mts_subscribe}}', 'is_applied');
    }
}
