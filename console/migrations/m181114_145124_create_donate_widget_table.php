<?php

use yii\db\Migration;

/**
 * Handles the creation of table `donate_widget`.
 */
class m181114_145124_create_donate_widget_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('donate_widget', [
            'id' => $this->string(50)->notNull(),
            'status' => $this->smallInteger(2)->notNull(),
            'company_id' => $this->integer()->notNull(),
            'product_id' => $this->integer()->notNull(),
            'note' => $this->text(),
            'return_url' => $this->text(),
            'is_autocomplete' => $this->boolean()->defaultValue(false),
            'is_additional_number_before' => $this->boolean()->defaultValue(false),
            'additional_number' => $this->string(45),
            'send_with_stamp' => $this->boolean()->defaultValue(false),
            'send_result' => $this->boolean()->defaultValue(false),
            'result_url' => $this->text(),
            'is_bik_autocomplete' => $this->boolean()->notNull()->defaultValue(false),
            'email_comment' => $this->string()->defaultValue(''),
            'invoice_comment' => $this->string()->defaultValue(''),
            'notify_to_email' => $this->boolean()->notNull()->defaultValue(false),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'created_by' => $this->integer()->notNull(),
            'updated_by' => $this->integer()->notNull(),
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', 'donate_widget', 'id');
        $this->addForeignKey('FK_donateWidget_company', 'donate_widget', 'company_id', 'company', 'id', 'CASCADE');
        $this->addForeignKey('FK_donateWidget_product', 'donate_widget', 'product_id', 'product', 'id');
        $this->addForeignKey('FK_donateWidget_createdBy', 'donate_widget', 'created_by', 'employee', 'id');
        $this->addForeignKey('FK_donateWidget_updatedBy', 'donate_widget', 'updated_by', 'employee', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('donate_widget');
    }
}
