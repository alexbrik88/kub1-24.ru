<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190418_075813_alter_scan_document_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%scan_document}}', 'owner_table', $this->string()->after('owner_model')->defaultValue(null));

        $this->createIndex('owner_table', '{{%scan_document}}', 'owner_table');

        $query = new \yii\db\Query;
        $classArray = $query->select('owner_model')->distinct()->from('{{%scan_document}}')->column();
        foreach ($classArray as $className) {
            if (class_exists($className)) {
                $this->update('{{%scan_document}}', [
                    'owner_table' => $className::tableName(),
                ], [
                    'owner_model' => $className,
                ]);
            }
        }
    }

    public function safeDown()
    {
        $this->dropIndex('owner_table', '{{%scan_document}}');

        $this->dropColumn('{{%scan_document}}', 'owner_table');
    }
}
