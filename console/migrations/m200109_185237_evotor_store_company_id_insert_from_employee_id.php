<?php

use common\models\employee\Employee;
use common\models\evotor\Store;
use console\components\db\Migration;

class m200109_185237_evotor_store_company_id_insert_from_employee_id extends Migration
{
    const TABLE = '{{%evotor_store}}';

    public function safeUp()
    {
        foreach (Store::find()->all() as $store) {
            /** @var Store $store */
            /** @noinspection PhpUndefinedFieldInspection */
            $store->company_id = Employee::findOne(['id' => $store->employee_id])->company_id;
            $store->save();
        }
    }

    public function down()
    {
        $this->update(self::TABLE, ['company_id' => 0]);
    }
}
