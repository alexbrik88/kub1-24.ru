<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210518_101700_alter_product_turnover_trigger extends Migration
{
    public $dropSql = '
        DROP TRIGGER IF EXISTS `product_turnover__order_act__after_insert`;
        DROP TRIGGER IF EXISTS `product_turnover__order_packing_list__after_insert`;
        DROP TRIGGER IF EXISTS `product_turnover__order_upd__after_insert`;
        DROP TRIGGER IF EXISTS `product_turnover__order_sales_invoice__after_insert`;
        DROP TRIGGER IF EXISTS `product_turnover__order_sales_invoice__after_update`;
    ';

    public function safeUp()
    {
        $this->execute($this->dropSql);

        /**
         * `product` triggers
         */
        $this->execute('
CREATE TRIGGER `product_turnover__order_act__after_insert` AFTER INSERT ON `order_act`
FOR EACH ROW
BEGIN
    INSERT INTO `product_turnover` (
        `order_id`,
        `document_id`,
        `document_table`,
        `invoice_id`,
        `contractor_id`,
        `company_id`,
        `date`,
        `type`,
        `production_type`,
        `is_invoice_actual`,
        `is_document_actual`,
        `purchase_price`,
        `price_one`,
        `quantity`,
        `total_amount`,
        `purchase_amount`,
        `margin`,
        `year`,
        `month`,
        `product_group_id`,
        `product_id`,
        `not_for_sale`
    ) SELECT
        NEW.`order_id`,
        NEW.`act_id`,
        "act",
        `invoice`.`id`,
        `invoice`.`contractor_id`,
        `invoice`.`company_id`,
        `act`.`document_date`,
        `act`.`type`,
        `product`.`production_type`,
        IF(`invoice`.`is_deleted` = 0 AND `invoice`.`invoice_status_id` IN (1, 2, 3, 4, 6, 7, 8), 1, 0),
        IF(IFNULL(`act`.`status_out_id`, 1) IN (1, 2, 3, 4), 1, 0),
        IFNULL(`product`.`price_for_buy_with_nds`, 0),
        @price:=IFNULL(IF(`invoice`.`type` = 1, `order`.`purchase_price_with_vat`, `order`.`selling_price_with_vat`), 0),
        @quantity:=IFNULL(NEW.`quantity`, 0),
        @total:=ROUND(@price * @quantity, IF(`invoice`.`price_precision` = 4, 2, 0)),
        @purchase:=IF(`invoice`.`type` = 2, ROUND(@quantity * IFNULL(`product`.`price_for_buy_with_nds`, 0)), 0),
        IF(`invoice`.`type` = 2, @total - @purchase, 0),
        YEAR(`act`.`document_date`),
        MONTH(`act`.`document_date`),
        `product`.`group_id`,
        NEW.`product_id`,
        `product`.`not_for_sale`
    FROM `order`
    LEFT JOIN `act` ON `act`.`id` = NEW.`act_id`
    LEFT JOIN `invoice` ON `invoice`.`id` = `order`.`invoice_id`
    LEFT JOIN `product` ON `product`.`id` = `order`.`product_id`
    WHERE `order`.`id` = NEW.`order_id`
    ON DUPLICATE KEY UPDATE
        `date` = VALUES(`date`),
        `type` = VALUES(`type`),
        `production_type` = VALUES(`production_type`),
        `price_one` = VALUES(`price_one`),
        `quantity` = VALUES(`quantity`),
        `total_amount` = VALUES(`total_amount`),
        `purchase_amount` = VALUES(`purchase_amount`),
        `margin` = VALUES(`margin`),
        `year` = VALUES(`year`),
        `month` = VALUES(`month`),
        `product_group_id` = VALUES(`product_group_id`),
        `product_id` = VALUES(`product_id`);
END;
        ');

        /**
         * `product` triggers
         */
        $this->execute('
CREATE TRIGGER `product_turnover__order_packing_list__after_insert` AFTER INSERT ON `order_packing_list`
FOR EACH ROW
BEGIN
    INSERT INTO `product_turnover` (
        `order_id`,
        `document_id`,
        `document_table`,
        `invoice_id`,
        `contractor_id`,
        `company_id`,
        `date`,
        `type`,
        `production_type`,
        `is_invoice_actual`,
        `is_document_actual`,
        `purchase_price`,
        `price_one`,
        `quantity`,
        `total_amount`,
        `purchase_amount`,
        `margin`,
        `year`,
        `month`,
        `product_group_id`,
        `product_id`,
        `not_for_sale`
    ) SELECT
        NEW.`order_id`,
        NEW.`packing_list_id`,
        "packing_list",
        `invoice`.`id`,
        `invoice`.`contractor_id`,
        `invoice`.`company_id`,
        `packing_list`.`document_date`,
        `packing_list`.`type`,
        `product`.`production_type`,
        IF(`invoice`.`is_deleted` = 0 AND `invoice`.`invoice_status_id` IN (1, 2, 3, 4, 6, 7, 8), 1, 0),
        IF(IFNULL(`packing_list`.`status_out_id`, 1) IN (1, 2, 3, 4), 1, 0),
        IFNULL(`product`.`price_for_buy_with_nds`, 0),
        @price:=IFNULL(IF(`invoice`.`type` = 1, `order`.`purchase_price_with_vat`, `order`.`selling_price_with_vat`), 0),
        @quantity:=IFNULL(NEW.`quantity`, 0),
        @total:=ROUND(@price * @quantity, IF(`invoice`.`price_precision` = 4, 2, 0)),
        @purchase:=IF(`invoice`.`type` = 2, ROUND(@quantity * IFNULL(`product`.`price_for_buy_with_nds`, 0)), 0),
        IF(`invoice`.`type` = 2, @total - @purchase, 0),
        YEAR(`packing_list`.`document_date`),
        MONTH(`packing_list`.`document_date`),
        `product`.`group_id`,
        NEW.`product_id`,
        `product`.`not_for_sale`
    FROM `order`
    LEFT JOIN `packing_list` ON `packing_list`.`id` = NEW.`packing_list_id`
    LEFT JOIN `invoice` ON `invoice`.`id` = `order`.`invoice_id`
    LEFT JOIN `product` ON `product`.`id` = `order`.`product_id`
    WHERE `order`.`id` = NEW.`order_id`
    ON DUPLICATE KEY UPDATE
        `date` = VALUES(`date`),
        `type` = VALUES(`type`),
        `production_type` = VALUES(`production_type`),
        `price_one` = VALUES(`price_one`),
        `quantity` = VALUES(`quantity`),
        `total_amount` = VALUES(`total_amount`),
        `purchase_amount` = VALUES(`purchase_amount`),
        `margin` = VALUES(`margin`),
        `year` = VALUES(`year`),
        `month` = VALUES(`month`),
        `product_group_id` = VALUES(`product_group_id`),
        `product_id` = VALUES(`product_id`);
END;
        ');

        /**
         * `product` triggers
         */
        $this->execute('
CREATE TRIGGER `product_turnover__order_upd__after_insert` AFTER INSERT ON `order_upd`
FOR EACH ROW
BEGIN
    INSERT INTO `product_turnover` (
        `order_id`,
        `document_id`,
        `document_table`,
        `invoice_id`,
        `contractor_id`,
        `company_id`,
        `date`,
        `type`,
        `production_type`,
        `is_invoice_actual`,
        `is_document_actual`,
        `purchase_price`,
        `price_one`,
        `quantity`,
        `total_amount`,
        `purchase_amount`,
        `margin`,
        `year`,
        `month`,
        `product_group_id`,
        `product_id`,
        `not_for_sale`
    ) SELECT
        NEW.`order_id`,
        NEW.`upd_id`,
        "upd",
        `invoice`.`id`,
        `invoice`.`contractor_id`,
        `invoice`.`company_id`,
        `upd`.`document_date`,
        `upd`.`type`,
        `product`.`production_type`,
        IF(`invoice`.`is_deleted` = 0 AND `invoice`.`invoice_status_id` IN (1, 2, 3, 4, 6, 7, 8), 1, 0),
        IF(IFNULL(`upd`.`status_out_id`, 1) IN (1, 2, 3, 4), 1, 0),
        IFNULL(`product`.`price_for_buy_with_nds`, 0),
        @price:=IFNULL(IF(`invoice`.`type` = 1, `order`.`purchase_price_with_vat`, `order`.`selling_price_with_vat`), 0),
        @quantity:=IFNULL(NEW.`quantity`, 0),
        @total:=ROUND(@price * @quantity, IF(`invoice`.`price_precision` = 4, 2, 0)),
        @purchase:=IF(`invoice`.`type` = 2 AND `product`.`production_type` = 1, ROUND(@quantity * IFNULL(`product`.`price_for_buy_with_nds`, 0)), 0),
        IF(`invoice`.`type` = 2 AND `product`.`production_type` = 1, @total - @purchase, 0),
        YEAR(`upd`.`document_date`),
        MONTH(`upd`.`document_date`),
        `product`.`group_id`,
        NEW.`product_id`,
        `product`.`not_for_sale`
    FROM `order`
    LEFT JOIN `upd` ON `upd`.`id` = NEW.`upd_id`
    LEFT JOIN `invoice` ON `invoice`.`id` = `order`.`invoice_id`
    LEFT JOIN `product` ON `product`.`id` = `order`.`product_id`
    WHERE `order`.`id` = NEW.`order_id`
    ON DUPLICATE KEY UPDATE
        `date` = VALUES(`date`),
        `type` = VALUES(`type`),
        `production_type` = VALUES(`production_type`),
        `price_one` = VALUES(`price_one`),
        `quantity` = VALUES(`quantity`),
        `total_amount` = VALUES(`total_amount`),
        `purchase_amount` = VALUES(`purchase_amount`),
        `margin` = VALUES(`margin`),
        `year` = VALUES(`year`),
        `month` = VALUES(`month`),
        `product_group_id` = VALUES(`product_group_id`),
        `product_id` = VALUES(`product_id`);
END;
        ');

        /**
         * `product` triggers
         */
        $this->execute('
CREATE TRIGGER `product_turnover__order_sales_invoice__after_insert` AFTER INSERT ON `order_sales_invoice`
FOR EACH ROW
BEGIN
    INSERT INTO `product_turnover` (
        `order_id`,
        `document_id`,
        `document_table`,
        `invoice_id`,
        `contractor_id`,
        `company_id`,
        `date`,
        `type`,
        `production_type`,
        `is_invoice_actual`,
        `is_document_actual`,
        `purchase_price`,
        `price_one`,
        `quantity`,
        `total_amount`,
        `purchase_amount`,
        `margin`,
        `year`,
        `month`,
        `product_group_id`,
        `product_id`
    ) SELECT
        NEW.`order_id`,
        NEW.`sales_invoice_id`,
        "sales_invoice",
        `invoice`.`id`,
        `invoice`.`contractor_id`,
        `invoice`.`company_id`,
        `sales_invoice`.`document_date`,
        `sales_invoice`.`type`,
        `product`.`production_type`,
        IF(`invoice`.`is_deleted` = 0 AND `invoice`.`invoice_status_id` IN (1, 2, 3, 4, 6, 7, 8), 1, 0),
        IF(IFNULL(`sales_invoice`.`status_out_id`, 1) IN (1, 2, 3, 4), 1, 0),
        IFNULL(`product`.`price_for_buy_with_nds`, 0),
        @price:=IFNULL(IF(`invoice`.`type` = 1, `order`.`purchase_price_with_vat`, `order`.`selling_price_with_vat`), 0),
        @quantity:=IFNULL(NEW.`quantity`, 0),
        @total:=ROUND(@price * @quantity, IF(`invoice`.`price_precision` = 4, 2, 0)),
        @purchase:=IF(`invoice`.`type` = 2, ROUND(@quantity * IFNULL(`product`.`price_for_buy_with_nds`, 0)), 0),
        IF(`invoice`.`type` = 2, @total - @purchase, 0),
        YEAR(`sales_invoice`.`document_date`),
        MONTH(`sales_invoice`.`document_date`),
        `product`.`group_id`,
        NEW.`product_id`
    FROM `order`
    LEFT JOIN `sales_invoice` ON `sales_invoice`.`id` = NEW.`sales_invoice_id`
    LEFT JOIN `invoice` ON `invoice`.`id` = `order`.`invoice_id`
    LEFT JOIN `product` ON `product`.`id` = `order`.`product_id`
    WHERE `order`.`id` = NEW.`order_id`
    ON DUPLICATE KEY UPDATE
        `date` = VALUES(`date`),
        `type` = VALUES(`type`),
        `production_type` = VALUES(`production_type`),
        `price_one` = VALUES(`price_one`),
        `quantity` = VALUES(`quantity`),
        `total_amount` = VALUES(`total_amount`),
        `purchase_amount` = VALUES(`purchase_amount`),
        `margin` = VALUES(`margin`),
        `year` = VALUES(`year`),
        `month` = VALUES(`month`),
        `product_group_id` = VALUES(`product_group_id`),
        `product_id` = VALUES(`product_id`);
END;
        ');

        /**
         * `product` triggers
         */
        $this->execute('
CREATE TRIGGER `product_turnover__order_sales_invoice__after_update` AFTER UPDATE ON `order_sales_invoice`
FOR EACH ROW
BEGIN
    INSERT INTO `product_turnover` (
        `order_id`,
        `document_id`,
        `document_table`,
        `invoice_id`,
        `contractor_id`,
        `company_id`,
        `date`,
        `type`,
        `production_type`,
        `is_invoice_actual`,
        `is_document_actual`,
        `purchase_price`,
        `price_one`,
        `quantity`,
        `total_amount`,
        `purchase_amount`,
        `margin`,
        `year`,
        `month`,
        `product_group_id`,
        `product_id`
    ) SELECT
        NEW.`order_id`,
        NEW.`sales_invoice_id`,
        "sales_invoice",
        `invoice`.`id`,
        `invoice`.`contractor_id`,
        `invoice`.`company_id`,
        `sales_invoice`.`document_date`,
        `sales_invoice`.`type`,
        `product`.`production_type`,
        IF(`invoice`.`is_deleted` = 0 AND `invoice`.`invoice_status_id` IN (1, 2, 3, 4, 6, 7, 8), 1, 0),
        IF(IFNULL(`sales_invoice`.`status_out_id`, 1) IN (1, 2, 3, 4), 1, 0),
        IFNULL(`product`.`price_for_buy_with_nds`, 0),
        @price:=IFNULL(IF(`invoice`.`type` = 1, `order`.`purchase_price_with_vat`, `order`.`selling_price_with_vat`), 0),
        @quantity:=IFNULL(NEW.`quantity`, 0),
        @total:=ROUND(@price * @quantity, IF(`invoice`.`price_precision` = 4, 2, 0)),
        @purchase:=IF(`invoice`.`type` = 2, ROUND(@quantity * IFNULL(`product`.`price_for_buy_with_nds`, 0)), 0),
        IF(`invoice`.`type` = 2, @total - @purchase, 0),
        YEAR(`sales_invoice`.`document_date`),
        MONTH(`sales_invoice`.`document_date`),
        `product`.`group_id`,
        NEW.`product_id`
    FROM `order`
    LEFT JOIN `sales_invoice` ON `sales_invoice`.`id` = NEW.`sales_invoice_id`
    LEFT JOIN `invoice` ON `invoice`.`id` = `order`.`invoice_id`
    LEFT JOIN `product` ON `product`.`id` = `order`.`product_id`
    WHERE `order`.`id` = NEW.`order_id`
    ON DUPLICATE KEY UPDATE
        `date` = VALUES(`date`),
        `type` = VALUES(`type`),
        `production_type` = VALUES(`production_type`),
        `price_one` = VALUES(`price_one`),
        `quantity` = VALUES(`quantity`),
        `total_amount` = VALUES(`total_amount`),
        `purchase_amount` = VALUES(`purchase_amount`),
        `margin` = VALUES(`margin`),
        `year` = VALUES(`year`),
        `month` = VALUES(`month`),
        `product_group_id` = VALUES(`product_group_id`),
        `product_id` = VALUES(`product_id`);
END;
        ');
    }

    public function safeDown()
    {
        $this->execute($this->dropSql);

        /**
         * `product` triggers
         */
        $this->execute('
CREATE TRIGGER `product_turnover__order_act__after_insert` AFTER INSERT ON `order_act`
FOR EACH ROW
BEGIN
    INSERT INTO `product_turnover` (
        `order_id`,
        `document_id`,
        `document_table`,
        `invoice_id`,
        `contractor_id`,
        `company_id`,
        `date`,
        `type`,
        `production_type`,
        `is_invoice_actual`,
        `is_document_actual`,
        `purchase_price`,
        `price_one`,
        `quantity`,
        `total_amount`,
        `purchase_amount`,
        `margin`,
        `year`,
        `month`,
        `product_group_id`,
        `product_id`,
        `not_for_sale`
    ) SELECT
        NEW.`order_id`,
        NEW.`act_id`,
        "act",
        `invoice`.`id`,
        `invoice`.`contractor_id`,
        `invoice`.`company_id`,
        `act`.`document_date`,
        `act`.`type`,
        `product`.`production_type`,
        IF(`invoice`.`is_deleted` = 0 AND `invoice`.`invoice_status_id` IN (1, 2, 3, 4, 6, 7, 8), 1, 0),
        IF(`act`.`status_out_id` IN (1, 2, 3, 4), 1, 0),
        IFNULL(`product`.`price_for_buy_with_nds`, 0),
        @price:=IFNULL(IF(`invoice`.`type` = 1, `order`.`purchase_price_with_vat`, `order`.`selling_price_with_vat`), 0),
        @quantity:=IFNULL(NEW.`quantity`, 0),
        @total:=ROUND(@price * @quantity, IF(`invoice`.`price_precision` = 4, 2, 0)),
        @purchase:=IF(`invoice`.`type` = 2, ROUND(@quantity * IFNULL(`product`.`price_for_buy_with_nds`, 0)), 0),
        IF(`invoice`.`type` = 2, @total - @purchase, 0),
        YEAR(`act`.`document_date`),
        MONTH(`act`.`document_date`),
        `product`.`group_id`,
        NEW.`product_id`,
        `product`.`not_for_sale`
    FROM `order`
    LEFT JOIN `act` ON `act`.`id` = NEW.`act_id`
    LEFT JOIN `invoice` ON `invoice`.`id` = `order`.`invoice_id`
    LEFT JOIN `product` ON `product`.`id` = `order`.`product_id`
    WHERE `order`.`id` = NEW.`order_id`
    ON DUPLICATE KEY UPDATE
        `date` = VALUES(`date`),
        `type` = VALUES(`type`),
        `production_type` = VALUES(`production_type`),
        `price_one` = VALUES(`price_one`),
        `quantity` = VALUES(`quantity`),
        `total_amount` = VALUES(`total_amount`),
        `purchase_amount` = VALUES(`purchase_amount`),
        `margin` = VALUES(`margin`),
        `year` = VALUES(`year`),
        `month` = VALUES(`month`),
        `product_group_id` = VALUES(`product_group_id`),
        `product_id` = VALUES(`product_id`);
END;
        ');

        /**
         * `product` triggers
         */
        $this->execute('
CREATE TRIGGER `product_turnover__order_packing_list__after_insert` AFTER INSERT ON `order_packing_list`
FOR EACH ROW
BEGIN
    INSERT INTO `product_turnover` (
        `order_id`,
        `document_id`,
        `document_table`,
        `invoice_id`,
        `contractor_id`,
        `company_id`,
        `date`,
        `type`,
        `production_type`,
        `is_invoice_actual`,
        `is_document_actual`,
        `purchase_price`,
        `price_one`,
        `quantity`,
        `total_amount`,
        `purchase_amount`,
        `margin`,
        `year`,
        `month`,
        `product_group_id`,
        `product_id`,
        `not_for_sale`
    ) SELECT
        NEW.`order_id`,
        NEW.`packing_list_id`,
        "packing_list",
        `invoice`.`id`,
        `invoice`.`contractor_id`,
        `invoice`.`company_id`,
        `packing_list`.`document_date`,
        `packing_list`.`type`,
        `product`.`production_type`,
        IF(`invoice`.`is_deleted` = 0 AND `invoice`.`invoice_status_id` IN (1, 2, 3, 4, 6, 7, 8), 1, 0),
        IF(`packing_list`.`status_out_id` IN (1, 2, 3, 4), 1, 0),
        IFNULL(`product`.`price_for_buy_with_nds`, 0),
        @price:=IFNULL(IF(`invoice`.`type` = 1, `order`.`purchase_price_with_vat`, `order`.`selling_price_with_vat`), 0),
        @quantity:=IFNULL(NEW.`quantity`, 0),
        @total:=ROUND(@price * @quantity, IF(`invoice`.`price_precision` = 4, 2, 0)),
        @purchase:=IF(`invoice`.`type` = 2, ROUND(@quantity * IFNULL(`product`.`price_for_buy_with_nds`, 0)), 0),
        IF(`invoice`.`type` = 2, @total - @purchase, 0),
        YEAR(`packing_list`.`document_date`),
        MONTH(`packing_list`.`document_date`),
        `product`.`group_id`,
        NEW.`product_id`,
        `product`.`not_for_sale`
    FROM `order`
    LEFT JOIN `packing_list` ON `packing_list`.`id` = NEW.`packing_list_id`
    LEFT JOIN `invoice` ON `invoice`.`id` = `order`.`invoice_id`
    LEFT JOIN `product` ON `product`.`id` = `order`.`product_id`
    WHERE `order`.`id` = NEW.`order_id`
    ON DUPLICATE KEY UPDATE
        `date` = VALUES(`date`),
        `type` = VALUES(`type`),
        `production_type` = VALUES(`production_type`),
        `price_one` = VALUES(`price_one`),
        `quantity` = VALUES(`quantity`),
        `total_amount` = VALUES(`total_amount`),
        `purchase_amount` = VALUES(`purchase_amount`),
        `margin` = VALUES(`margin`),
        `year` = VALUES(`year`),
        `month` = VALUES(`month`),
        `product_group_id` = VALUES(`product_group_id`),
        `product_id` = VALUES(`product_id`);
END;
        ');

        /**
         * `product` triggers
         */
        $this->execute('
CREATE TRIGGER `product_turnover__order_upd__after_insert` AFTER INSERT ON `order_upd`
FOR EACH ROW
BEGIN
    INSERT INTO `product_turnover` (
        `order_id`,
        `document_id`,
        `document_table`,
        `invoice_id`,
        `contractor_id`,
        `company_id`,
        `date`,
        `type`,
        `production_type`,
        `is_invoice_actual`,
        `is_document_actual`,
        `purchase_price`,
        `price_one`,
        `quantity`,
        `total_amount`,
        `purchase_amount`,
        `margin`,
        `year`,
        `month`,
        `product_group_id`,
        `product_id`,
        `not_for_sale`
    ) SELECT
        NEW.`order_id`,
        NEW.`upd_id`,
        "upd",
        `invoice`.`id`,
        `invoice`.`contractor_id`,
        `invoice`.`company_id`,
        `upd`.`document_date`,
        `upd`.`type`,
        `product`.`production_type`,
        IF(`invoice`.`is_deleted` = 0 AND `invoice`.`invoice_status_id` IN (1, 2, 3, 4, 6, 7, 8), 1, 0),
        IF(`upd`.`status_out_id` IN (1, 2, 3, 4), 1, 0),
        IFNULL(`product`.`price_for_buy_with_nds`, 0),
        @price:=IFNULL(IF(`invoice`.`type` = 1, `order`.`purchase_price_with_vat`, `order`.`selling_price_with_vat`), 0),
        @quantity:=IFNULL(NEW.`quantity`, 0),
        @total:=ROUND(@price * @quantity, IF(`invoice`.`price_precision` = 4, 2, 0)),
        @purchase:=IF(`invoice`.`type` = 2 AND `product`.`production_type` = 1, ROUND(@quantity * IFNULL(`product`.`price_for_buy_with_nds`, 0)), 0),
        IF(`invoice`.`type` = 2 AND `product`.`production_type` = 1, @total - @purchase, 0),
        YEAR(`upd`.`document_date`),
        MONTH(`upd`.`document_date`),
        `product`.`group_id`,
        NEW.`product_id`,
        `product`.`not_for_sale`
    FROM `order`
    LEFT JOIN `upd` ON `upd`.`id` = NEW.`upd_id`
    LEFT JOIN `invoice` ON `invoice`.`id` = `order`.`invoice_id`
    LEFT JOIN `product` ON `product`.`id` = `order`.`product_id`
    WHERE `order`.`id` = NEW.`order_id`
    ON DUPLICATE KEY UPDATE
        `date` = VALUES(`date`),
        `type` = VALUES(`type`),
        `production_type` = VALUES(`production_type`),
        `price_one` = VALUES(`price_one`),
        `quantity` = VALUES(`quantity`),
        `total_amount` = VALUES(`total_amount`),
        `purchase_amount` = VALUES(`purchase_amount`),
        `margin` = VALUES(`margin`),
        `year` = VALUES(`year`),
        `month` = VALUES(`month`),
        `product_group_id` = VALUES(`product_group_id`),
        `product_id` = VALUES(`product_id`);
END;
        ');

        /**
         * `product` triggers
         */
        $this->execute('
CREATE TRIGGER `product_turnover__order_sales_invoice__after_insert` AFTER INSERT ON `order_sales_invoice`
FOR EACH ROW
BEGIN
    INSERT INTO `product_turnover` (
        `order_id`,
        `document_id`,
        `document_table`,
        `invoice_id`,
        `contractor_id`,
        `company_id`,
        `date`,
        `type`,
        `production_type`,
        `is_invoice_actual`,
        `is_document_actual`,
        `purchase_price`,
        `price_one`,
        `quantity`,
        `total_amount`,
        `purchase_amount`,
        `margin`,
        `year`,
        `month`,
        `product_group_id`,
        `product_id`
    ) SELECT
        NEW.`order_id`,
        NEW.`sales_invoice_id`,
        "sales_invoice",
        `invoice`.`id`,
        `invoice`.`contractor_id`,
        `invoice`.`company_id`,
        `sales_invoice`.`document_date`,
        `sales_invoice`.`type`,
        `product`.`production_type`,
        IF(`invoice`.`is_deleted` = 0 AND `invoice`.`invoice_status_id` IN (1, 2, 3, 4, 6, 7, 8), 1, 0),
        IF(`sales_invoice`.`status_out_id` IN (1, 2, 3, 4), 1, 0),
        IFNULL(`product`.`price_for_buy_with_nds`, 0),
        @price:=IFNULL(IF(`invoice`.`type` = 1, `order`.`purchase_price_with_vat`, `order`.`selling_price_with_vat`), 0),
        @quantity:=IFNULL(NEW.`quantity`, 0),
        @total:=ROUND(@price * @quantity, IF(`invoice`.`price_precision` = 4, 2, 0)),
        @purchase:=IF(`invoice`.`type` = 2, ROUND(@quantity * IFNULL(`product`.`price_for_buy_with_nds`, 0)), 0),
        IF(`invoice`.`type` = 2, @total - @purchase, 0),
        YEAR(`sales_invoice`.`document_date`),
        MONTH(`sales_invoice`.`document_date`),
        `product`.`group_id`,
        NEW.`product_id`
    FROM `order`
    LEFT JOIN `sales_invoice` ON `sales_invoice`.`id` = NEW.`sales_invoice_id`
    LEFT JOIN `invoice` ON `invoice`.`id` = `order`.`invoice_id`
    LEFT JOIN `product` ON `product`.`id` = `order`.`product_id`
    WHERE `order`.`id` = NEW.`order_id`
    ON DUPLICATE KEY UPDATE
        `date` = VALUES(`date`),
        `type` = VALUES(`type`),
        `production_type` = VALUES(`production_type`),
        `price_one` = VALUES(`price_one`),
        `quantity` = VALUES(`quantity`),
        `total_amount` = VALUES(`total_amount`),
        `purchase_amount` = VALUES(`purchase_amount`),
        `margin` = VALUES(`margin`),
        `year` = VALUES(`year`),
        `month` = VALUES(`month`),
        `product_group_id` = VALUES(`product_group_id`),
        `product_id` = VALUES(`product_id`);
END;
        ');

        /**
         * `product` triggers
         */
        $this->execute('
CREATE TRIGGER `product_turnover__order_sales_invoice__after_update` AFTER UPDATE ON `order_sales_invoice`
FOR EACH ROW
BEGIN
    INSERT INTO `product_turnover` (
        `order_id`,
        `document_id`,
        `document_table`,
        `invoice_id`,
        `contractor_id`,
        `company_id`,
        `date`,
        `type`,
        `production_type`,
        `is_invoice_actual`,
        `is_document_actual`,
        `purchase_price`,
        `price_one`,
        `quantity`,
        `total_amount`,
        `purchase_amount`,
        `margin`,
        `year`,
        `month`,
        `product_group_id`,
        `product_id`
    ) SELECT
        NEW.`order_id`,
        NEW.`sales_invoice_id`,
        "sales_invoice",
        `invoice`.`id`,
        `invoice`.`contractor_id`,
        `invoice`.`company_id`,
        `sales_invoice`.`document_date`,
        `sales_invoice`.`type`,
        `product`.`production_type`,
        IF(`invoice`.`is_deleted` = 0 AND `invoice`.`invoice_status_id` IN (1, 2, 3, 4, 6, 7, 8), 1, 0),
        IF(`sales_invoice`.`status_out_id` IN (1, 2, 3, 4), 1, 0),
        IFNULL(`product`.`price_for_buy_with_nds`, 0),
        @price:=IFNULL(IF(`invoice`.`type` = 1, `order`.`purchase_price_with_vat`, `order`.`selling_price_with_vat`), 0),
        @quantity:=IFNULL(NEW.`quantity`, 0),
        @total:=ROUND(@price * @quantity, IF(`invoice`.`price_precision` = 4, 2, 0)),
        @purchase:=IF(`invoice`.`type` = 2, ROUND(@quantity * IFNULL(`product`.`price_for_buy_with_nds`, 0)), 0),
        IF(`invoice`.`type` = 2, @total - @purchase, 0),
        YEAR(`sales_invoice`.`document_date`),
        MONTH(`sales_invoice`.`document_date`),
        `product`.`group_id`,
        NEW.`product_id`
    FROM `order`
    LEFT JOIN `sales_invoice` ON `sales_invoice`.`id` = NEW.`sales_invoice_id`
    LEFT JOIN `invoice` ON `invoice`.`id` = `order`.`invoice_id`
    LEFT JOIN `product` ON `product`.`id` = `order`.`product_id`
    WHERE `order`.`id` = NEW.`order_id`
    ON DUPLICATE KEY UPDATE
        `date` = VALUES(`date`),
        `type` = VALUES(`type`),
        `production_type` = VALUES(`production_type`),
        `price_one` = VALUES(`price_one`),
        `quantity` = VALUES(`quantity`),
        `total_amount` = VALUES(`total_amount`),
        `purchase_amount` = VALUES(`purchase_amount`),
        `margin` = VALUES(`margin`),
        `year` = VALUES(`year`),
        `month` = VALUES(`month`),
        `product_group_id` = VALUES(`product_group_id`),
        `product_id` = VALUES(`product_id`);
END;
        ');
    }
}
