<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180529_142020_add_phone_to_crm_user extends Migration
{
    public $tableName = 'crm_user';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'phone', $this->string()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'phone');
    }
}


