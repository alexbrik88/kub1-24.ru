<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170116_115057_insert_companyType extends Migration
{
    public function safeUp()
    {
        $this->insert('company_type', [
            'id' => 17,
            'name_short' => 'НП',
            'name_full' => 'Некоммерческое партнёрство',
            'in_company' => 0,
            'in_contractor' => 1,
        ]);
    }
    
    public function safeDown()
    {
        $this->delete('company_type', ['id' => 17]);
    }
}


