<?php

use common\models\bitrix24\Deal;
use console\components\db\Migration;

class m191115_153154_create_bitrix24_deal extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{bitrix24_deal}}', [
            'id' => $this->integer()->unsigned()->notNull()->comment('Идентификатор сделки Битрикс24'),
            'employee_id' => $this->integer()->notNull()->comment('ID клиента КУБ'),
            'company_id' => $this->integer()->unsigned()->comment('Идентификатор компании'),
            'contact_id' => $this->integer()->unsigned()->comment('Идентификатор контакта'),
            'title' => $this->string(120)->notNull()->comment('Название сделки'),
            'type' => $this->string(50)->notNull()->comment('Тип'),
            'stage' => $this->string(50)->notNull()->comment('Этап'),
            'currency' => $this->char(3)->notNull()->defaultValue('RUB')->comment('Валюта'),
            'amount' => $this->float()->unsigned()->notNull()->defaultValue(0)->comment('Сумма сделки'),
            'tax' => $this->float()->unsigned()->notNull()->defaultValue(0)->comment('Сумма налога'),
            'status' => "ENUM('" . implode("','", Deal::STATUS) . "') NOT NULL DEFAULT '" . Deal::STATUS_OPEN . "' COMMENT 'Статус (активна сделка или завершена)'",
            'source' => $this->string(50)->notNull()->defaultValue('OTHER')->comment('Источник'),
            'source_description' => $this->string(1000)->comment('Комментарий к источнику сделки'),
            'begin_date' => $this->integer()->unsigned()->comment('Дата начала сделки'),
            'close_date' => $this->integer()->unsigned()->comment('Дата завершения сделки'),
            'created_at' => $this->integer()->unsigned()->notNull()->comment('Дата добавления сделки'),
            'updated_at' => $this->integer()->unsigned()->notNull()->comment('Дата последнего изменения сделки'),
        ], "COMMENT 'Интеграция Битрикс24: сделки'");
        $this->addPrimaryKey('bitrix24_deal_id', '{{bitrix24_deal}}', ['employee_id', 'id']);
        $this->addForeignKey(
            'bitrix24_deal_employee_id', '{{bitrix24_deal}}', 'employee_id',
            '{{employee}}', 'id', 'CASCADE', 'CASCADE'
        );
        $this->addForeignKey(
            'bitrix24_deal_company_id', '{{bitrix24_deal}}', ['employee_id', 'company_id'],
            '{{bitrix24_company}}', ['employee_id', 'id'], 'CASCADE', 'CASCADE'
        );
        $this->addForeignKey(
            'bitrix24_deal_contact_id', '{{bitrix24_deal}}', ['employee_id', 'contact_id'],
            '{{bitrix24_contact}}', ['employee_id', 'id'], 'CASCADE', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable('{{bitrix24_deal}}');
    }

}
