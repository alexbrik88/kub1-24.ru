<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170303_091236_alter_exportFiles_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%export_files}}', 'company_id', $this->integer() . " AFTER `user_id`");

        $this->addForeignKey('FK_exportFiles_company', '{{%export_files}}', 'company_id', '{{%company}}', 'id');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('FK_exportFiles_company', '{{%export_files}}');

        $this->dropColumn('{{%export_files}}', 'company_id');
    }
}
