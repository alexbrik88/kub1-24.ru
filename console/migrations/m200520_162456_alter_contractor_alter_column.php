<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200520_162456_alter_contractor_alter_column extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%contractor}}', 'ITN',  $this->string());
    }

    public function down()
    {
        $this->alterColumn('{{%contractor}}', 'ITN',  $this->string(12));
    }
}
