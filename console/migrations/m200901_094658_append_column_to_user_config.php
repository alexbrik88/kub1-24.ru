<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200901_094658_append_column_to_user_config extends Migration
{
    public $tableName = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'upd_order_sum_without_nds', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'upd_order_sum_nds', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'upd_contractor_inn', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'upd_payment_date', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'upd_responsible', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'upd_source', $this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'upd_order_sum_without_nds');
        $this->dropColumn($this->tableName, 'upd_order_sum_nds');
        $this->dropColumn($this->tableName, 'upd_contractor_inn');
        $this->dropColumn($this->tableName, 'upd_payment_date');
        $this->dropColumn($this->tableName, 'upd_responsible');
        $this->dropColumn($this->tableName, 'upd_source');
    }
}
