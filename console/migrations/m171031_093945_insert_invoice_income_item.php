<?php

use console\components\db\Migration;
use yii\db\Schema;

class m171031_093945_insert_invoice_income_item extends Migration
{
    public $tableName = 'invoice_income_item';

    public function safeUp()
    {
        $this->insert($this->tableName, [
            'id' => 9,
            'name' => 'Перевод собственных средств',
        ]);
    }
    
    public function safeDown()
    {
        $this->delete($this->tableName, ['id' => 9]);
    }
}
