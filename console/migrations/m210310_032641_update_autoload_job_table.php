<?php

use yii\db\Migration;

class m210310_032641_update_autoload_job_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%autoload_job}}';

    /** @var int */
    private const JOBS_TYPES = [1, 2, 3, 4];

    /**
     * @inheritDoc
     * @throws
     */
    public function safeUp()
    {
        $this->db->createCommand()->delete(self::TABLE_NAME, ['type' => self::JOBS_TYPES])->execute();
    }
}
