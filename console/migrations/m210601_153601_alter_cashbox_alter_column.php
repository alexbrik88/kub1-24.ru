<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210601_153601_alter_cashbox_alter_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%cashbox}}', 'name', $this->string()->notNull());
    }

    public function safeDown()
    {
        $this->alterColumn('{{%cashbox}}', 'name', $this->string(50)->notNull());
    }
}
