<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201030_121851_add_table_project_estimate extends Migration
{
    public $tableName = 'project_estimate';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'project_id' => $this->integer(11),
            'number' => $this->integer(),
            'name' => $this->string(128),
            'date' => $this->integer(11),
            'status' => $this->integer(2),
            'responsible' => $this->integer(11),
        ]);

        $this->createIndex($this->tableName . '_project_id_idx', $this->tableName, 'project_id');
    }

    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
