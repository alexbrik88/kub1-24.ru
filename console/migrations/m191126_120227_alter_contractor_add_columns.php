<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191126_120227_alter_contractor_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('contractor', 'director_in_act', $this->boolean()->notNull()->defaultValue(true)->after('director_phone'));
        $this->addColumn('contractor', 'chief_accountant_in_act', $this->boolean()->notNull()->defaultValue(false)->after('chief_accountant_phone'));
        $this->addColumn('contractor', 'contact_in_act', $this->boolean()->notNull()->defaultValue(false)->after('contact_email'));
    }

    public function safeDown()
    {
        $this->dropColumn('contractor', 'director_in_act');
        $this->dropColumn('contractor', 'chief_accountant_in_act');
        $this->dropColumn('contractor', 'contact_in_act');
    }
}
