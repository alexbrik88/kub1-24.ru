<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170922_093619_rename_infs_to_ifns extends Migration
{
    public function safeUp()
    {
    	$this->execute("RENAME TABLE {{infs}} TO {{ifns}};");
    }
    
    public function safeDown()
    {
    	$this->execute("RENAME TABLE {{ifns}} TO {{infs}};");
    }
}
