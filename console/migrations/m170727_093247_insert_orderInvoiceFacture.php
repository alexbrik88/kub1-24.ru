<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170727_093247_insert_orderInvoiceFacture extends Migration
{
    public function safeUp()
    {
        $this->execute("
            INSERT INTO
                {{%order_invoice_facture}} (
                    [[invoice_facture_id]],
                    [[order_id]],
                    [[quantity]],
                    [[empty_unit_code]],
                    [[empty_unit_name]]
                )

            SELECT
                {{%invoice_facture}}.[[id]],
                {{%order}}.[[id]],
                {{%order}}.[[quantity]],
                IF({{%product}}.[[product_unit_id]] IS NULL, 1, 0),
                IF({{%product}}.[[product_unit_id]] IS NULL, 1, 0)

            FROM
                {{%order}}

            LEFT JOIN
                {{%product}}
                ON {{%product}}.[[id]] = {{%order}}.[[product_id]]

            INNER JOIN
                {{%invoice}}
                ON {{%invoice}}.[[id]] = {{%order}}.[[invoice_id]]

            INNER JOIN
                {{%invoice_facture}}
                ON {{%invoice_facture}}.[[invoice_id]] = {{%invoice}}.[[id]]

            LEFT JOIN
                {{%order_invoice_facture}}
                ON 
                    {{%order_invoice_facture}}.[[invoice_facture_id]] = {{%invoice_facture}}.[[id]]
                    AND
                    {{%order_invoice_facture}}.[[order_id]] = {{%order}}.[[id]]

            WHERE
                {{%order_invoice_facture}}.[[invoice_facture_id]] IS NULL
                AND
                {{%order_invoice_facture}}.[[order_id]] IS NULL
        ");
    }
    
    public function safeDown()
    {
        return;
    }
}
