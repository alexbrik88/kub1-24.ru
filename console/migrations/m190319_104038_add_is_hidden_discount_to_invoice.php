<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190319_104038_add_is_hidden_discount_to_invoice extends Migration
{
    public $tableName = 'invoice';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'is_hidden_discount', $this->boolean()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'is_hidden_discount');
    }
}
