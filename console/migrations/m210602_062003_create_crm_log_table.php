<?php

use yii\db\Migration;

class m210602_062003_create_crm_log_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%crm_log}}';

    /**
     * @inheritDoc
     */
    public function up(): void
    {
        $this->createTable(self::TABLE_NAME, [
            'log_id' => $this->bigPrimaryKey()->notNull(),
            'log_type' => $this->tinyInteger()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'employee_id' => $this->integer()->notNull(),
            'model_id' => $this->bigInteger()->notNull(),
            'model_class' => $this->string()->notNull(),
            'attribute_name' => $this->string()->null(),
            'new_attribute_value' => $this->text()->null(),
            'old_attribute_value' => $this->text()->null(),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->createIndex('ck_company_id', self::TABLE_NAME, ['company_id']);
        $this->createIndex('ck_employee_id', self::TABLE_NAME, ['employee_id']);
        $this->createIndex('ck_model_id', self::TABLE_NAME, ['model_id']);
        $this->createIndex('ck_model_class', self::TABLE_NAME, ['model_class']);
        $this->createIndex('ck_created_at', self::TABLE_NAME, ['created_at']);

        $this->addForeignKey(
            'fk_crm_log__company',
            self::TABLE_NAME,
            ['company_id'],
            '{{%company}}',
            'id',
            'CASCADE',
            'CASCADE',
        );

        $this->addForeignKey(
            'fk_crm_log__employee',
            self::TABLE_NAME,
            ['employee_id'],
            '{{%employee}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritDoc
     */
    public function down(): void
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
