<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210519_123528_alter_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_config','retail_sales_help',$this->boolean()->notNull()->defaultValue(false));
        $this->addColumn('user_config','retail_sales_chart',$this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('user_config','retail_sales_help');
        $this->dropColumn('user_config','retail_sales_chart');
    }
}