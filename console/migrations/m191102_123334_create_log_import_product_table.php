<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%log_import_product}}`.
 */
class m191102_123334_create_log_import_product_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%log_import_product}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'employee_id' => $this->integer()->notNull(),
            'production_type' => $this->tinyInteger(1)->notNull(),
            'created_at' => $this->integer()->notNull(),
            'inserted_rows' => $this->integer()->notNull()->defaultValue(0),
            'updated_rows' => $this->integer()->notNull()->defaultValue(0),
        ]);

        $this->addForeignKey(
            'fk_log_import_product_employee',
            '{{%log_import_product}}',
            'employee_id',
            '{{%employee}}',
            'id'
        );

        $this->addForeignKey(
            'fk_log_import_product_company',
            '{{%log_import_product}}',
            'company_id',
            '{{%company}}',
            'id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%log_import_product}}');
    }
}
