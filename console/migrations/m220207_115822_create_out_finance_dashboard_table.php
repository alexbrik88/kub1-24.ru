<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%out_finance_dashboard}}`.
 */
class m220207_115822_create_out_finance_dashboard_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%out_finance_dashboard}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'uid' => $this->string(16)->notNull()
        ]);

        $this->addForeignKey('FK_out_finance_dashboard_to_company', 'out_finance_dashboard', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
        $this->createIndex('IDX_out_finance_dashboard_uid', 'out_finance_dashboard', 'uid', true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%out_finance_dashboard}}');
    }
}
