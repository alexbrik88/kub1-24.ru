<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180822_104814_create_email_file extends Migration
{
    public $tableName = 'email_file';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'file_name' => $this->string()->notNull(),
            'ext' => $this->string()->defaultValue(null),
            'mime' => $this->string()->notNull(),
            'size' => $this->string()->notNull(),
        ]);

        $this->addForeignKey($this->tableName . '_company_id', $this->tableName, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_company_id', $this->tableName);

        $this->dropTable($this->tableName);
    }
}


