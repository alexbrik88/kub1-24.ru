<?php

use yii\db\Migration;
use yii\db\Exception;

class m200708_093745_create_crm_campaign_type_table extends Migration
{
    /** @var string Имя таблицы */
    private const TABLE_NAME = '{{%crm_campaign_type}}';

    /** @var string[] Внешние столбцы */
    private const FK_COLUMNS = ['company_id'];

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'campaign_type_id' => $this->bigPrimaryKey(),
            'company_id' => $this->integer()->null(),
            'name' => $this->string(64)->notNull(),
        ]);

        $this->createIndex('ck_company_id', self::TABLE_NAME, self::FK_COLUMNS);
        $this->addForeignKey(
            'fk_crm_campaign_type__company',
            self::TABLE_NAME,
            self::FK_COLUMNS,
            '{{%company}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->db->createCommand()->batchInsert(self::TABLE_NAME, ['name'], $this->getRows())->execute();
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }

    /**
     * @return string[]
     */
    private function getRows(): array
    {
        return [
            ['Яндекс.Директ'],
            ['Google.AdWords'],
            ['Facebook'],
            ['Instagram'],
            ['ВКонтакте'],
            ['Рекомендация партнера'],
            ['Рекомендация клиента'],
            ['Реклама'],
            ['Холодный звонок'],
        ];
    }
}
