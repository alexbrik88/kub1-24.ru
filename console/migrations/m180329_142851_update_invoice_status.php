<?php

use console\components\db\Migration;
use yii\db\Schema;
use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;

class m180329_142851_update_invoice_status extends Migration
{
    public function safeUp()
    {
        foreach (Invoice::find()
                     ->byStatus(InvoiceStatus::STATUS_PAYED_PARTIAL)
                     ->andWhere('total_amount_with_nds = payment_partial_amount')->all() as $invoice) {
            $this->update(Invoice::tableName(), ['invoice_status_id' => InvoiceStatus::STATUS_PAYED], ['id' => $invoice->id]);
        }
    }

    public function safeDown()
    {
        echo 'This migration not rolled back!';
    }
}


