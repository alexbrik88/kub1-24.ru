<?php

use yii\db\Migration;

class m200721_195551_create_moneta_operation_table extends Migration
{

    public function safeUp()
    {
        $this->createTable('{{%moneta_operation}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'flow_type' => $this->tinyInteger(1)->unsigned()->notNull(),
            'acquiring_identifier' => $this->string()->notNull(),
            'date' => $this->date()->notNull(),
            'datetime' => $this->dateTime()->defaultValue(null),
            'recognition_date' => $this->date()->defaultValue(null),
            'operation_id' => $this->integer(11)->unsigned()->defaultValue(null),
            'contractor_id' => $this->integer()->notNull(),
            'amount' => $this->bigInteger(20)->unsigned()->notNull(),
            'description' => $this->string()->notNull(),
            'expenditure_item_id' => $this->integer()->defaultValue(null),
            'income_item_id' => $this->integer()->defaultValue(null),
        ]);

        $this->addForeignKey('FK_moneta_operation_to_company', 'moneta_operation', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_moneta_operation_to_contractor', 'moneta_operation', 'contractor_id', 'contractor', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_moneta_operation_to_expenditure_item', 'moneta_operation', 'expenditure_item_id', 'invoice_expenditure_item', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_moneta_operation_to_income_item', 'moneta_operation', 'income_item_id', 'invoice_income_item', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_moneta_operation_to_company', 'moneta_operation');
        $this->dropForeignKey('FK_moneta_operation_to_contractor', 'moneta_operation');
        $this->dropForeignKey('FK_moneta_operation_to_expenditure_item', 'moneta_operation');
        $this->dropForeignKey('FK_moneta_operation_to_income_item', 'moneta_operation');
        $this->dropTable('{{%moneta_operation}}');
    }
}
