<?php

use console\components\db\Migration;
use yii\db\Schema;

class m220202_160450_insert_employee_role extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%employee_role}}', [
            'id' => 17,
            'name' => 'Руководитель отдела закупки',
            'sort' => 7500,
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%employee_role}}', [
            'id' => 17,
        ]);
    }
}
