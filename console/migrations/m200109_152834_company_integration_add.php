<?php

use console\components\db\Migration;

class m200109_152834_company_integration_add extends Migration
{
    public function up()
    {
        $this->addColumn('{{%company}}', 'integration', $this->string(2000)->comment('Настройки интеграции'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%company}}', 'integration');
    }
}
