<?php

use console\components\db\Migration;
use yii\db\Schema;

class m171228_105117_update_employee_company_initials extends Migration
{
    public function safeUp()
    {
        $this->execute("
            UPDATE {{employee_company}}
            SET
                [[firstname_initial]] = LEFT([[firstname]], 1),
                [[patronymic_initial]] = LEFT([[patronymic]], 1)
        ");
    }

    public function safeDown()
    {
        return true;
    }
}
