<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190115_193739_insert_tax_kbk extends Migration
{
    public function safeUp()
    {
        $items = [
            'Налог на прибыль' => [
                '18210101011011000110',
                '18210101012021000110',
            ],
            'Налог УСН 15%' => [
                '18210501021011000110',
            ],
            'Налог УСН 6%' => [
                '18210501011011000110',
            ],
            'Налоги' => [],
            'Налоги на ЗП' => [
                '18210102010011000110',
                '18210202010061010160',
                '18210202090071010160',
                '18210202101081013160',
                '39310202050071000160',
            ],
            'НДС' => [
                '18210301000011000110',
                '18210401000011000110',
                '15310401000011000110',
            ],
            'НДФЛ с дивидендов' => [],
            'Патент' => [
                '18210504010021000110',
            ],
            'Страховые взносы за ИП' => [
                '18210202140061110160',
                '18210202103081013160',
            ],
            'Торговый сбор' => [
                '18210505010021000110',
            ],
        ];

        foreach ($items as $name => $kbkArray) {
            if ($id = self::getIdByName($name)) {
                $this->update('invoice_expenditure_item', ['group_id' => 1], ['id' => $id]);
            } else {
                $id = self::getNewId();
                $this->insert('invoice_expenditure_item', [
                    'id' => $id,
                    'group_id' => 1,
                    'name' => $name,
                ]);
            }

            foreach ($kbkArray as $kbk) {
                $this->insert('tax_kbk', [
                    'id' => $kbk,
                    'expenditure_item_id' => $id,
                ]);
            }
        }
    }

    public function safeDown()
    {
        $this->truncateTable('tax_kbk');
    }

    protected static function getNewId()
    {
        return Yii::$app->db->createCommand('
            SELECT MAX([[id]])
            FROM {{invoice_expenditure_item}}
            WHERE [[id]] < 100
        ')->queryScalar() + 1;
    }

    protected static function getIdByName($name)
    {
        return Yii::$app->db->createCommand('
            SELECT [[id]]
            FROM {{invoice_expenditure_item}}
            WHERE [[name]] = :name
        ', [':name' => $name])->queryScalar();
    }
}
