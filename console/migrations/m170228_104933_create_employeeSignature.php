<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170228_104933_create_employeeSignature extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%employee_signature}}', [
            'id' => $this->primaryKey(),
            'employee_id' => $this->integer()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'file_name' => $this->string(50)->notNull(),
            'created_at' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('FK_employeeSignature_employee', '{{%employee_signature}}', 'employee_id', '{{%employee}}', 'id');
        $this->addForeignKey('FK_employeeSignature_company', '{{%employee_signature}}', 'company_id', '{{%company}}', 'id');
    }

    public function safeDown()
    {
        $this->dropTable('{{%employee_signature}}');
    }
}
