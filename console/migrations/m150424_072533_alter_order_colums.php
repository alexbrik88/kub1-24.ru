<?php

use yii\db\Schema;
use yii\db\Migration;

class m150424_072533_alter_order_colums extends Migration
{

    public function safeUp()
    {
        $this->alterColumn('order', 'product_code', Schema::TYPE_STRING . ' COMMENT "Код продукта"');
        $this->alterColumn('order', 'unit_id', Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Единица измерения"');
        $this->alterColumn('order', 'count_in_place', Schema::TYPE_STRING . ' COMMENT "Колличество в одном месте"');
        $this->alterColumn('order', 'excise', Schema::TYPE_BOOLEAN . ' COMMENT "1 - с акцизом, 0 - без"');
        $this->alterColumn('order', 'excise_price', Schema::TYPE_STRING . ' COMMENT "Если нет, то значение - без акциза"');
    }
    
    public function safeDown()
    {
        $this->alterColumn('order', 'product_code', Schema::TYPE_STRING . ' NOT NULL COMMENT "Код продукта"');
        $this->alterColumn('order', 'unit_id', Schema::TYPE_INTEGER . ' COMMENT "Единица измерения"');
        $this->alterColumn('order', 'count_in_place', Schema::TYPE_STRING . ' NOT NULL COMMENT "Колличество в одном месте"');
        $this->alterColumn('order', 'excise', Schema::TYPE_BOOLEAN . ' COMMENT "1 - с акцизом, 0 - без"');
        $this->alterColumn('order', 'excise_price', Schema::TYPE_STRING . ' NOT NULL COMMENT "Если нет, то значение - без акциза"');
    }
}
