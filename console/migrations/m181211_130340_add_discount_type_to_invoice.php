<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181211_130340_add_discount_type_to_invoice extends Migration
{
    public $tInvoice = 'invoice';
    public $tOrder = 'order';

    public function safeUp()
    {
        $this->addColumn($this->tInvoice, 'discount_type', $this->boolean()->defaultValue(false));
        $this->alterColumn($this->tOrder, 'discount', $this->decimal(22, 4)->defaultValue(0.0000));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tInvoice, 'discount_type');
        $this->alterColumn($this->tOrder, 'discount', $this->decimal(6, 4)->defaultValue(0.0000));
    }
}


