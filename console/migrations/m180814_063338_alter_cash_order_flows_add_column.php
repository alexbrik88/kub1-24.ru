<?php

use console\components\db\Migration;
use yii\db\Expression;
use yii\db\Schema;

class m180814_063338_alter_cash_order_flows_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cash_order_flows', 'recognition_date', $this->date()->notNull()->after('date'));

        $this->update('cash_order_flows', ['recognition_date' => new Expression('[[date]]')]);
    }

    public function safeDown()
    {
        $this->dropColumn('cash_order_flows', 'recognition_date');
    }
}
