<?php

use console\components\db\Migration;
use yii\db\Schema;
use common\models\document\Invoice;

class m171221_145435_update_partial_payment_amount_in_invoice extends Migration
{
    public $tableName = 'invoice';

    public function safeUp()
    {
        /* @var $invoice Invoice */
        foreach (Invoice::find()->byDeleted()->andWhere(['<', 'payment_partial_amount', 0])->all() as $invoice) {
            if ($invoice->getPaidAmount() < $invoice->total_amount_with_nds) {
                $this->update($this->tableName, ['payment_partial_amount' => $invoice->getPaidAmount()], ['id' => $invoice->id]);
                $this->update($this->tableName, ['remaining_amount' => $invoice->total_amount_with_nds - $invoice->getPaidAmount()], ['id' => $invoice->id]);
            }
        }
    }

    public function safeDown()
    {

    }
}


