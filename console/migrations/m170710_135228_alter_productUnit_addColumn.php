<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170710_135228_alter_productUnit_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%product_unit}}', 'title', $this->string(50)->notNull() . ' AFTER [[name]]');

        $this->update('{{%product_unit}}', [
            'title' => new \yii\db\Expression('[[name]]'),
        ]);
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%product_unit}}', 'title');
    }
}
