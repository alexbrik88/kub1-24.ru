<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%google_analytics_profile_goal}}`.
 */
class m200729_200858_create_google_analytics_profile_goal_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%google_analytics_profile_goal}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer(),
            'profile_id' => $this->integer(),
            'external_id' => $this->integer(),
            'name' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%google_analytics_profile_goal}}');
    }
}
