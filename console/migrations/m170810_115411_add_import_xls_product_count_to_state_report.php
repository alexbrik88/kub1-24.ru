<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170810_115411_add_import_xls_product_count_to_state_report extends Migration
{
    public $tableName = 'report_state';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'import_xls_product_count', $this->integer()->defaultValue(0));
        $this->addColumn($this->tableName, 'import_xls_service_count', $this->integer()->defaultValue(0));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'import_xls_product_count');
        $this->dropColumn($this->tableName, 'import_xls_service_count');
    }
}


