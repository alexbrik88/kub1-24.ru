<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211129_182455_alter_config_add_column extends Migration
{
    /** @var string */
    private const TABLE_NAME = 'user_config';

    /**
     * @inheritDoc
     */
    public function up()
    {
        $type = $this->boolean()->notNull()->defaultValue(false);
        $this->addColumn(self::TABLE_NAME, 'credits_interest_repaid', $type);
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'credits_interest_repaid');
    }
}
