<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200122_213437_rework_autoload_moneta extends Migration
{
    public function safeUp()
    {
        $this->renameTable('{{%autoload_moneta}}', '{{%autoload_job}}');
        $this->renameColumn('{{%autoload_job}}', 'type', 'period_type');
        $this->addColumn('{{%autoload_job}}', 'type', $this->tinyInteger(3)->unsigned()->notNull()->after('company_id'));
        $this->update('{{%autoload_job}}', ['type' => 0]);
        $this->createIndex('temporary_index', '{{%autoload_job}}', 'company_id', true);
        $this->dropPrimaryKey('PRIMARY', '{{%autoload_job}}');
        $this->addPrimaryKey('company_id_type', '{{%autoload_job}}', ['company_id', 'type']);
        $this->dropIndex('temporary_index', '{{%autoload_job}}');
    }

    public function safeDown()
    {
        $this->renameTable('{{%autoload_job}}', '{{%autoload_moneta}}');
        $this->createIndex('temporary_index', '{{%autoload_moneta}}', 'company_id', true);
        $this->dropPrimaryKey('PRIMARY', '{{%autoload_moneta}}');
        $this->addPrimaryKey('company_id', '{{%autoload_moneta}}', 'company_id');
        $this->dropIndex('temporary_index', '{{%autoload_moneta}}');
        $this->dropColumn('{{%autoload_moneta}}', 'type');
        $this->renameColumn('{{%autoload_moneta}}', 'period_type', 'type');
    }
}
