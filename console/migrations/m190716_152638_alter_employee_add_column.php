<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190716_152638_alter_employee_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%employee}}', 'is_mts_user', $this->boolean()->defaultValue(false));
        $this->createIndex('is_mts_user', '{{%employee}}', 'is_mts_user');
    }

    public function safeDown()
    {
        $this->dropIndex('is_mts_user', '{{%employee}}');
        $this->dropColumn('{{%employee}}', 'is_mts_user');
    }
}
