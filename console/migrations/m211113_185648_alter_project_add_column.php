<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211113_185648_alter_project_add_column extends Migration
{
    public $table = 'project';

    public function safeUp()
    {
        $table = $this->table;

        $this->addColumn($table, 'industry_id', $this->integer()->after('direction_id'));
        $this->addForeignKey("FK_{$table}_company_industry", $table, 'industry_id', 'company_industry', 'id', 'SET NULL');
    }

    public function safeDown()
    {
        $table = $this->table;

        $this->dropForeignKey("FK_{$table}_company_industry", $table);
        $this->dropColumn($table, 'industry_id');
    }
}
