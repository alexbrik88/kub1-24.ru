<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170710_104313_alter_upd_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%upd}}', 'state_contract', $this->string(50) . ' AFTER [[signature_id]]');
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%upd}}', 'state_contract');
    }
}
