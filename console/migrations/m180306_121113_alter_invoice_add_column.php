<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180306_121113_alter_invoice_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('invoice', 'price_precision', "ENUM('2', '4') NOT NULL DEFAULT '2'");
    }

    public function safeDown()
    {
        $this->dropColumn('invoice', 'price_precision');
    }
}
