<?php

use console\components\db\Migration;
use yii\db\Schema;

class m171010_144909_add_is_viewed_to_chat extends Migration
{
    public $tableName = 'chat';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'is_viewed', $this->boolean()->defaultValue(true));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'is_viewed');
    }
}


