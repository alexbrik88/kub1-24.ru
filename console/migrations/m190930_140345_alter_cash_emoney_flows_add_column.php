<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190930_140345_alter_cash_emoney_flows_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cash_emoney_flows', 'date_time', $this->dateTime()->after('date'));
        $this->execute("UPDATE `cash_emoney_flows` SET `date_time` = `date`");
    }

    public function safeDown()
    {
        $this->dropColumn('cash_emoney_flows', 'date_time');
    }
}
