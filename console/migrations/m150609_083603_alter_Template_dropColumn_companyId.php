<?php

use yii\db\Schema;
use yii\db\Migration;

class m150609_083603_alter_Template_dropColumn_companyId extends Migration
{
    public function safeUp()
    {
        $this->dropForeignKey('template_to_company', 'template');
        $this->dropColumn('template', 'company_id');
    }
    
    public function safeDown()
    {
        $this->addColumn('template', 'company_id', Schema::TYPE_INTEGER);
        $this->addForeignKey('template_to_company', 'template', 'company_id', 'company', 'id');
    }
}
