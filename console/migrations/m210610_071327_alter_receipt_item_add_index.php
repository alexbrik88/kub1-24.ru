<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210610_071327_alter_receipt_item_add_index extends Migration
{
    public function safeUp()
    {
        $this->createIndex('idx_ofd_receipt_operator', 'ofd_receipt', ['company_id', 'operator_id', 'date_time']);
    }

    public function safeDown()
    {
        $this->dropIndex('idx_ofd_receipt_operator', 'ofd_receipt');
    }
}
