<?php

use yii\db\Migration;

/**
 * Handles the creation of table `discount`.
 */
class m171124_170700_create_discount_type_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('discount_type', [
            'id' => $this->primaryKey(),
            'description' => $this->text()->notNull(),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('discount_type');
    }
}
