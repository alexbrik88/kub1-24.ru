<?php

use yii\db\Schema;
use yii\db\Migration;

class m150714_103644_create_cashFundingType extends Migration
{
    public function safeUp()
    {
        $this->createTable('cash_funding_type', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . '(45) NOT NULL',
        ]);

        $this->batchInsert('cash_funding_type', ['name'], [
            ['Личные деньги'],
            ['Заим от учредителя'],
            ['Кредит в банке'],
        ]);
    }
    
    public function safeDown()
    {
        $this->dropTable('cash_funding_type');
    }
}
