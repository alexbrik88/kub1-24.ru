<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200112_204350_add_is_viewed_column_to_jobs extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%jobs}}', 'is_viewed', $this->boolean()->unsigned()->notNull()->defaultValue(false));
        $this->update('{{%jobs}}', ['is_viewed' => true]);
    }

    public function safeDown()
    {
        $this->dropColumn('{{%jobs}}', 'is_viewed');
    }
}
