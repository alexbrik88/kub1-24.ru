<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210811_081629_alter_ofd_receipt_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%ofd_receipt}}', 'ofd_statement_upload_id', $this->integer()->after('document_number'));
        $this->createIndex('ofd_statement_upload_id', '{{%ofd_receipt}}', 'ofd_statement_upload_id');
    }

    public function safeDown()
    {
        $this->dropIndex('ofd_statement_upload_id', '{{%ofd_receipt}}');
        $this->dropColumn('{{%ofd_receipt}}', 'ofd_statement_upload_id');
    }
}
