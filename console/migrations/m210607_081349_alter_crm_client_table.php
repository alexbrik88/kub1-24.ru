<?php

use yii\db\Migration;

class m210607_081349_alter_crm_client_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%crm_client}}';

    /**
     * @inheritDoc
     */
    public function up(): void
    {
        $this->dropForeignKey('fk_crm_client__crm_client_activity_client_activity_id', self::TABLE_NAME);
        $this->dropForeignKey('fk_crm_client__crm_client_campaign_client_campaign_id', self::TABLE_NAME);
    }

    /**
     * @inheritDoc
     */
    public function down(): bool
    {
        return false;
    }
}
