
<?php

use yii\db\Schema;
use yii\db\Migration;

class m150417_080535_alter_product_column_excise extends Migration
{
    public function up()
    {
        $this->alterColumn('product', 'excise', Schema::TYPE_STRING . '(45) NULL COMMENT "сумма акциза"');
    }

    public function down()
    {
        $this->alterColumn('product', 'excise', Schema::TYPE_STRING . '(45) NOT NULL COMMENT "сумма акциза"');
    }
}
