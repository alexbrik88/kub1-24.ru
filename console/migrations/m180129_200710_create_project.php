<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180129_200710_create_project extends Migration
{
    public $tProject = 'project';
    public $tProjectCompany = 'project_company';
    public $tProjectContractor = 'project_contractor';
    public $tProjectExpenditureItem = 'project_expenditure_item';


    public function safeUp()
    {
        $this->createTable($this->tProject, [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'project_number' => $this->string()->notNull(),
            'project_date' => $this->date()->notNull(),
            'name' => $this->string()->notNull(),
            'description' => $this->text()->defaultValue(null),
            'start_date' => $this->date()->notNull(),
            'end_date' => $this->date()->defaultValue(null),
            'responsible_employee_id' => $this->integer()->notNull(),
            'additional_expenses' => $this->integer()->defaultValue(null),
        ]);

        $this->addForeignKey($this->tProject . '_responsible_employee_id', $this->tProject, 'responsible_employee_id', 'employee', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tProject . '_company_id', $this->tProject, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');

        $this->createTable($this->tProjectCompany, [
            'id' => $this->primaryKey(),
            'project_id' => $this->integer()->notNull(),
            'company_id' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey($this->tProjectCompany . '_project_id', $this->tProjectCompany, 'project_id', 'project', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tProjectCompany . '_company_id', $this->tProjectCompany, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');

        $this->createTable($this->tProjectContractor, [
            'id' => $this->primaryKey(),
            'project_id' => $this->integer()->notNull(),
            'contractor_id' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey($this->tProjectContractor . '_project_id', $this->tProjectContractor, 'project_id', 'project', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tProjectContractor . '_contractor_id', $this->tProjectContractor, 'contractor_id', 'contractor', 'id', 'RESTRICT', 'CASCADE');

        $this->createTable($this->tProjectExpenditureItem, [
            'id' => $this->primaryKey(),
            'project_id' => $this->integer()->notNull(),
            'expenditure_item_id' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey($this->tProjectExpenditureItem . '_project_id', $this->tProjectExpenditureItem, 'project_id', 'project', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tProjectExpenditureItem . '_expenditure_item_id', $this->tProjectExpenditureItem, 'expenditure_item_id', 'invoice_expenditure_item', 'id', 'RESTRICT', 'CASCADE');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey($this->tProjectExpenditureItem . '_project_id', $this->tProjectExpenditureItem);
        $this->dropForeignKey($this->tProjectExpenditureItem . '_expenditure_item_id', $this->tProjectExpenditureItem);

        $this->dropTable($this->tProjectExpenditureItem);

        $this->dropForeignKey($this->tProjectContractor . '_project_id', $this->tProjectContractor);
        $this->dropForeignKey($this->tProjectContractor . '_contractor_id', $this->tProjectContractor);

        $this->dropTable($this->tProjectContractor);

        $this->dropForeignKey($this->tProjectCompany . '_project_id', $this->tProjectCompany);
        $this->dropForeignKey($this->tProjectCompany . '_company_id', $this->tProjectCompany);

        $this->dropTable($this->tProjectCompany);

        $this->dropForeignKey($this->tProject . '_responsible_employee_id', $this->tProject);
        $this->dropForeignKey($this->tProject . '_company_id', $this->tProject);

        $this->dropTable($this->tProject);
    }
}


