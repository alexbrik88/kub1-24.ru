<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170301_003801_alter_packingList_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%packing_list}}', 'signed_by_employee_id', $this->integer());
        $this->addColumn('{{%packing_list}}', 'signed_by_name', $this->string(50));
        $this->addColumn('{{%packing_list}}', 'sign_document_type_id', $this->integer());
        $this->addColumn('{{%packing_list}}', 'sign_document_number', $this->string(50));
        $this->addColumn('{{%packing_list}}', 'sign_document_date', $this->date());
        $this->addColumn('{{%packing_list}}', 'signature_id', $this->integer());

        $this->addForeignKey('FK_packingList_sign_employee', '{{%packing_list}}', 'signed_by_employee_id', '{{%employee}}', 'id');
        $this->addForeignKey('FK_packingList_sign_documentType', '{{%packing_list}}', 'sign_document_type_id', '{{%document_type}}', 'id');
        $this->addForeignKey('FK_packingList_employeeSignature', '{{%packing_list}}', 'signature_id', '{{%employee_signature}}', 'id');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('FK_packingList_sign_employee', '{{%packing_list}}');
        $this->dropForeignKey('FK_packingList_sign_documentType', '{{%packing_list}}');
        $this->dropForeignKey('FK_packingList_employeeSignature', '{{%packing_list}}');
        $this->dropColumn('{{%packing_list}}', 'signed_by_employee_id');
        $this->dropColumn('{{%packing_list}}', 'signed_by_name');
        $this->dropColumn('{{%packing_list}}', 'sign_document_type_id');
        $this->dropColumn('{{%packing_list}}', 'sign_document_number');
        $this->dropColumn('{{%packing_list}}', 'sign_document_date');
        $this->dropColumn('{{%packing_list}}', 'signature_id');
    }
}
