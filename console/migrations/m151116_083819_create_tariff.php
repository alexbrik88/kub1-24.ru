<?php

use yii\db\Schema;
use yii\db\Migration;

class m151116_083819_create_tariff extends Migration
{
    public $tTariff = 'service_subscribe_tariff';

    public function safeUp()
    {
        $this->createTable($this->tTariff, [
            'id' => $this->primaryKey(),
            'duration' => $this->integer()->notNull() . ' COMMENT "Number of months"',
            'price' => $this->integer()->notNull(),
        ]);

        $this->batchInsert($this->tTariff, ['id', 'duration', 'price'], [
            [1, 1, 599],
            [2, 3, 1647],
            [3, 6, 2994],
        ]);
    }
    
    public function safeDown()
    {
        $this->dropTable($this->tTariff);
    }
}
