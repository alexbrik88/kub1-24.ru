<?php
use console\components\db\Migration;

class m160810_103156_add_cash_bank_flow_income_item extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cash_bank_flows', 'income_item_id', $this->integer()->defaultValue(null));
        $this->addForeignKey('fk_cash_bank_flows_income_item_id', 'cash_bank_flows', 'income_item_id', 'invoice_income_item', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_cash_bank_flows_income_item_id', 'cash_bank_flows');
        $this->dropColumn('cash_bank_flows', 'income_item_id');
    }
}


