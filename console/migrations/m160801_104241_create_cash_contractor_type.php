<?php

use yii\db\Migration;

/**
 * Handles the creation for table `cash_contractor_type`.
 */
class m160801_104241_create_cash_contractor_type extends Migration
{
    public $tCashContractorType = 'cash_contractor_type';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->tCashContractorType, [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'text' => $this->string()->notNull(),
        ]);

        $this->batchInsert($this->tCashContractorType, ['id', 'name', 'text'], [
            [1, 'bank', 'Банк'],
            [2, 'order', 'Касса'],
            [3, 'emoney', 'E-money'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->tCashContractorType);
    }
}
