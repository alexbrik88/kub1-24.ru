<?php

use yii\db\Schema;
use yii\db\Migration;

class m150708_155844_insert_paymentDetails_0 extends Migration
{
    public function safeUp()
    {
        $this->insert('payment_details', [
            'id' => 14,
            'code' => '0',
            'name' => '0',
        ]);
    }
    
    public function safeDown()
    {
        $this->delete('payment_details', 'id = 1');
    }
}
