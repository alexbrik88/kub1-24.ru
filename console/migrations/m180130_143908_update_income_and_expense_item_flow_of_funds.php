<?php

use console\components\db\Migration;
use yii\db\Schema;
use common\models\Company;
use common\models\document\InvoiceIncomeItem;
use common\models\document\InvoiceExpenditureItem;
use common\models\IncomeItemFlowOfFunds;
use common\models\ExpenseItemFlowOfFunds;

class m180130_143908_update_income_and_expense_item_flow_of_funds extends Migration
{
    public $tIncomeItemFlowOfFunds = 'income_item_flow_of_funds';
    public $tExpenseItemFlowOfFunds = 'expense_item_flow_of_funds';

    public function safeUp()
    {
        $query = (new \yii\db\Query())->select('id')->from('company');

        foreach ($query->all() as $company) {
            /* @var $incomeItem InvoiceIncomeItem */
            foreach (InvoiceIncomeItem::find()->all() as $incomeItem) {
                if (!IncomeItemFlowOfFunds::find()->andWhere(['and',
                    ['company_id' => $company['id']],
                    ['income_item_id' => $incomeItem->id],
                ])->exists()
                ) {
                    if ($incomeItem->company_id == null || $incomeItem->company_id == $company['id']) {
                        $model = new IncomeItemFlowOfFunds();
                        $model->company_id = $company['id'];
                        $model->name = $incomeItem->name;
                        $model->income_item_id = $incomeItem->id;
                        $model->save();
                    }
                }
            }
            /* @var $expenseItem InvoiceExpenditureItem */
            foreach (InvoiceExpenditureItem::find()->all() as $expenseItem) {
                if (!ExpenseItemFlowOfFunds::find()->andWhere(['and',
                    ['company_id' => $company['id']],
                    ['expense_item_id' => $expenseItem->id],
                ])->exists()
                ) {
                    if ($expenseItem->company_id == null || $expenseItem->company_id == $company['id']) {
                        $model = new ExpenseItemFlowOfFunds();
                        $model->company_id = $company['id'];
                        $model->name = $expenseItem->name;
                        $model->expense_item_id = $expenseItem->id;
                        $model->save();
                    }
                }
            }
        }
    }

    public function safeDown()
    {

    }
}


