<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191129_104523_alter_company_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%company}}', 'self_employed', $this->boolean()->defaultValue(false)->after('id'));
        $this->createIndex('self_employed', '{{%company}}', 'self_employed');
    }

    public function safeDown()
    {
        $this->dropIndex('self_employed', '{{%company}}');
        $this->dropColumn('{{%company}}', 'self_employed');
    }
}
