<?php

use console\components\db\Migration;
use yii\db\Schema;

class m220222_094337_alter_order_document_add_column extends Migration
{
    // see Invoice model
    const NDS_VIEW_IN = 0;
    const NDS_VIEW_OUT = 1;
    const NDS_VIEW_WITHOUT = 2;

    public function safeUp()
    {
        $this->addColumn('order_document', 'nds_view_type_id', $this->tinyInteger()->notNull()->after('has_nds'));
        $this->update('order_document', ['nds_view_type_id' => self::NDS_VIEW_IN], ['has_nds' => 1]);
        $this->update('order_document', ['nds_view_type_id' => self::NDS_VIEW_WITHOUT], ['has_nds' => 0]);
    }

    public function safeDown()
    {
        $this->dropColumn('order_document', 'nds_view_type_id');
    }
}
