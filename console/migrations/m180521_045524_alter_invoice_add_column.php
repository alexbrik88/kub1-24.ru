<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180521_045524_alter_invoice_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('invoice', 'show_paylimit_info', $this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('invoice', 'show_paylimit_info');
    }
}
