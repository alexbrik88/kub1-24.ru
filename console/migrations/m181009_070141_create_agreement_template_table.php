<?php

use yii\db\Migration;

/**
 * Handles the creation of table `agreement_template`.
 */
class m181009_070141_create_agreement_template_table extends Migration
{
    public function up()
    {
        $this->createTable('{{%agreement_template}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'type' => $this->integer()->notNull(),
            'document_number' => $this->string(255)->notNull(),
			'document_additional_number' => $this->string(255),
			'document_date' => $this->date()->notNull(),
            'document_name' => $this->string(255)->notNull(),
			'document_header' => $this->string()->notNull(),
			'document_body' => $this->string()->notNull(),
			'document_requisites_customer' => $this->string()->notNull(),
			'document_requisites_executer' => $this->string()->notNull(),
			'payment_delay' => $this->integer()->notNull(),
			'documents_created' => $this->integer()->defaultValue(0),
			'other_patterns' => $this->string(),
            'created_at' => $this->integer(),
            'created_by' => $this->integer(),
			'status' => $this->integer()->defaultValue(1),
			'comment_internal' => $this->string()
        ]);

    }

    public function down()
    {
        $this->dropTable('{{%agreement_template}}');
    }
}
