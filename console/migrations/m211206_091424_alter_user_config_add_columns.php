<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211206_091424_alter_user_config_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%user_config}}', 'rent_entity_id', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn('{{%user_config}}', 'rent_entity_created', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn('{{%user_config}}', 'rent_entity_category', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn('{{%user_config}}', 'rent_entity_additional_status', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn('{{%user_config}}', 'rent_entity_ownership', $this->boolean()->notNull()->defaultValue(true));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%user_config}}', 'rent_entity_id');
        $this->dropColumn('{{%user_config}}', 'rent_entity_created');
        $this->dropColumn('{{%user_config}}', 'rent_entity_category');
        $this->dropColumn('{{%user_config}}', 'rent_entity_additional_status');
        $this->dropColumn('{{%user_config}}', 'rent_entity_ownership');
    }
}
