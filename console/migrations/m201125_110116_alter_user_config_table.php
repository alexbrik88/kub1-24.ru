<?php

use console\components\db\Migration;

class m201125_110116_alter_user_config_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = 'user_config';

    /**
     * @inheritDoc
     */
    public function up()
    {
        $type = $this->boolean()->notNull()->defaultValue(false);
        $this->addColumn(self::TABLE_NAME, 'credits_credit_table', $type);
        $this->addColumn(self::TABLE_NAME, 'credits_flow_table', $type);
        $this->addColumn(self::TABLE_NAME, 'credits_debt_table', $type);
        $type = $this->boolean()->notNull()->defaultValue(true);
        $this->addColumn(self::TABLE_NAME, 'credits_credit_first_date', $type);
        $this->addColumn(self::TABLE_NAME, 'credits_credit_amount', $type);
        $this->addColumn(self::TABLE_NAME, 'credits_contractor_id', $type);
        $this->addColumn(self::TABLE_NAME, 'credits_credit_type', $type);
        $this->addColumn(self::TABLE_NAME, 'credits_debt_amount', $type);
        $this->addColumn(self::TABLE_NAME, 'credits_debt_repaid', $type);
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'credits_credit_table');
        $this->dropColumn(self::TABLE_NAME, 'credits_flow_table');
        $this->dropColumn(self::TABLE_NAME, 'credits_debt_table');

        $this->dropColumn(self::TABLE_NAME, 'credits_credit_first_date');
        $this->dropColumn(self::TABLE_NAME, 'credits_credit_amount');
        $this->dropColumn(self::TABLE_NAME, 'credits_contractor_id');
        $this->dropColumn(self::TABLE_NAME, 'credits_credit_type');
        $this->dropColumn(self::TABLE_NAME, 'credits_debt_amount');
        $this->dropColumn(self::TABLE_NAME, 'credits_debt_repaid');
    }
}
