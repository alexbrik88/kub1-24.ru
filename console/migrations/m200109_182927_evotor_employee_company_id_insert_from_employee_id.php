<?php

use common\models\employee\Employee;
use common\models\evotor\Employee as EvotorEmployee;
use console\components\db\Migration;

class m200109_182927_evotor_employee_company_id_insert_from_employee_id extends Migration
{
    const TABLE = '{{%evotor_employee}}';

    public function safeUp()
    {
        foreach (EvotorEmployee::find()->all() as $employee) {
            /** @var EvotorEmployee $employee */
            /** @noinspection PhpUndefinedFieldInspection */
            $employee->company_id = Employee::findOne(['id' => $employee->employee_id])->company_id;
            $employee->save();
        }
    }

    public function down()
    {
        $this->update(self::TABLE, ['company_id' => 0]);
    }
}
