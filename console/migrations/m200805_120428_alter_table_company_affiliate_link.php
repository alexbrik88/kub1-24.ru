<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200805_120428_alter_table_company_affiliate_link extends Migration
{
    public $tableName = 'company_affiliate_link';

    public function safeUp()
    {
        try {
            $this->dropForeignKey('fk_company_affiliate_link_service_module', $this->tableName);
            $this->dropIndex($this->tableName . '_service_module_id_idx', $this->tableName);
        } catch (\Exception $e) {
            echo $e->getTraceAsString();
        }

        $this->truncateTable($this->tableName);

        $this->alterColumn($this->tableName, 'service_module_id', $this->string(16));
    }

    public function safeDown()
    {
        $this->alterColumn($this->tableName, 'service_module_id', $this->integer(11));
        $this->createIndex($this->tableName . '_service_module_id_idx', $this->tableName, 'service_module_id');

        $this->addForeignKey('fk_company_affiliate_link_service_module',
            $this->tableName,
            'service_module_id',
            'service_module',
            'id');
    }
}
