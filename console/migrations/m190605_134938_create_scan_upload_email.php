<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190605_134938_create_scan_upload_email extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('scan_upload_email', [
            'id' => $this->primaryKey(),
            'email' => $this->string(255),
            'company_id' => $this->integer()->notNull(),
            'employee_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'enabled' => $this->boolean()->notNull()->defaultValue(0)
        ]);

        $this->addForeignKey ('FK_scanUploadEmail_company', 'scan_upload_email', 'company_id', 'company', 'id', 'CASCADE');
        $this->addForeignKey ('FK_scanUploadEmail_employee', 'scan_upload_email', 'employee_id', 'employee', 'id');
        $this->createIndex('IDX_email', 'scan_upload_email', 'email');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('scan_upload_email');
    }
}
