<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180201_075002_alter_company_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('company', 'is_additional_number_before', $this->boolean()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('company', 'is_additional_number_before');
    }
}
