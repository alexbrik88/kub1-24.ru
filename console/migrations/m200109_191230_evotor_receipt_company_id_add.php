<?php

use console\components\db\Migration;

class m200109_191230_evotor_receipt_company_id_add extends Migration
{
    const TABLE = '{{%evotor_receipt}}';

    public function up()
    {
        $this->addColumn(self::TABLE, 'company_id', $this->integer(11)->notNull()->after('id')->comment('Идентификатор компании КУБ'));
    }

    public function down()
    {
        $this->dropColumn(self::TABLE, 'company_id');
    }
}
