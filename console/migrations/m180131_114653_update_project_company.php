<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180131_114653_update_project_company extends Migration
{
    public $tProjectCompany = 'project_company';

    public function safeUp()
    {
        $this->dropForeignKey($this->tProjectCompany . '_company_id', $this->tProjectCompany);

        $this->renameColumn($this->tProjectCompany, 'company_id', 'contractor_id');

        $this->addForeignKey($this->tProjectCompany . '_contractor_id', $this->tProjectCompany, 'contractor_id', 'contractor', 'id', 'RESTRICT', 'CASCADE');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey($this->tProjectCompany . '_contractor_id', $this->tProjectCompany);

        $this->renameColumn($this->tProjectCompany, 'contractor_id', 'company_id');

        $this->addForeignKey($this->tProjectCompany . '_company_id', $this->tProjectCompany, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');
    }
}


