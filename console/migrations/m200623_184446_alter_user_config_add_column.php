<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200623_184446_alter_user_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_config','product_abc_help',$this->boolean()->notNull()->defaultValue(false));
        $this->addColumn('user_config','product_abc_chart',$this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('user_config','product_abc_help');
        $this->dropColumn('user_config','product_abc_chart');
    }
}
