<?php

use console\components\db\Migration;

class m191016_165804_create_insales_order extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%insales_order}}', [
            'id' => $this->primaryKey()->unsigned()->notNull()->comment('ID заказа в InSales'),
            'employee_id' => $this->integer()->comment('ID клиента на сайте'),
            'client_id' => $this->integer()->unsigned()->comment('ID клиента в InSales'),
            'shipping_address_id' => $this->integer()->unsigned()->comment('ID адреса доставки в InSales'),
            'number' => $this->char(20)->notNull()->comment('Номер заказа'),
            'key' => $this->string(32),
            'total_price' => $this->float()->unsigned()->notNull()->defaultValue(0)->comment('Итоговая сумма заказа'),
            'items_price' => $this->float()->unsigned()->notNull()->defaultValue(0)->comment('Итоговая сумма товаров'),
            'currency_code' => $this->char(3)->notNull()->defaultValue('RUR')->comment('Валюта'),
            'archived' => $this->boolean()->notNull()->defaultValue(false),
            'delivery_title' => $this->string(100)->comment('Краткое название способа доставки'),
            'delivery_description' => $this->string(500)->comment('Развёрнутое название способа доставки'),
            'delivery_date' => $this->integer()->unsigned()->comment('Дата доставки'),
            'delivery_from_hour' => $this->tinyInteger()->unsigned()->comment('Время доставки (не раньше)'),
            'delivery_to_hour' => $this->tinyInteger()->unsigned()->comment('Время доставки (не позже)'),
            'delivery_price' => $this->float()->unsigned()->notNull()->defaultValue(0),
            'full_delivery_price' => $this->float()->unsigned()->notNull()->defaultValue(0),
            'paid_at' => $this->integer()->unsigned(),
            'payment_title' => $this->string(100)->comment('Краткое название способа оплаты'),
            'payment_description' => $this->string(500)->comment('Развёрнутое название способа доставки'),
            'fulfillment_status' => $this->string(30)->comment('Код статуса заказа'),
            'custom_status' => $this->string(100),
            'comment' => $this->string(1000)->comment('Комментарий клиента'),
            'manager_comment' => $this->string(1000)->comment('Комментарий менеджера'),
            'created_at' => $this->integer()->unsigned()->notNull(),
            'updated_at' => $this->integer()->unsigned()->notNull(),
        ], "COMMENT 'Интеграция InSales CMS: заказы'");
        $this->addForeignKey(
            'insales_order_employee_id', '{{%insales_order}}', 'employee_id',
            '{{%employee}}', 'id', 'CASCADE', 'CASCADE'
        );
        $this->addForeignKey(
            'insales_order_client_id', '{{%insales_order}}', 'client_id',
            '{{%insales_client}}', 'id', 'CASCADE', 'CASCADE'
        );
        $this->addForeignKey(
            'insales_order_shipping_address_id', '{{%insales_order}}', 'shipping_address_id',
            '{{%insales_shipping_address}}', 'id', 'CASCADE', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable('{{%insales_order}}');
    }
}
