<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180212_084124_alter_product_drop_column extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('product', 'product_count');
        $this->dropColumn('product', 'product_begin_count');
    }

    public function safeDown()
    {
        $this->addColumn('product', 'product_count',
            $this->decimal(20, 10)->notNull()->defaultValue(0)->after('object_guid'));
        $this->addColumn('product', 'product_begin_count',
            $this->decimal(20, 10)->notNull()->defaultValue(0)->after('group_id'));

        $this->execute("
            UPDATE {{product}}
            LEFT JOIN (
                SELECT [[product_id]], SUM([[quantity]]) [[count]], SUM([[initial_quantity]]) [[init_count]]
                FROM {{product_store}}
                GROUP BY [[product_id]]
            ) {{t}} ON {{t}}.[[product_id]] = {{product}}.[[id]]
            SET
                {{product}}.[[product_count]] = IFNULL({{t}}.[[count]], 0),
                {{product}}.[[product_begin_count]] = IFNULL({{t}}.[[init_count]], 0)
        ");
    }
}
