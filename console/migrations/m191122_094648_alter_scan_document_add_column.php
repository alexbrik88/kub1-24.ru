<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191122_094648_alter_scan_document_add_column extends Migration
{
    public $table = 'scan_document';

    public function safeUp()
    {
        $this->addColumn($this->table, 'from_email', $this->string(64)->notNull()->defaultValue(''));
        $this->addColumn($this->table, 'is_from_employee', $this->boolean()->notNull()->defaultValue(0));
        $this->addColumn($this->table, 'is_from_contractor', $this->boolean()->notNull()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'from_email');
        $this->dropColumn($this->table, 'is_from_employee');
        $this->dropColumn($this->table, 'is_from_contractor');
    }
}
