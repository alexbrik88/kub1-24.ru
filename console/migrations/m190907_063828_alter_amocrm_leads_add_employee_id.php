<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190907_063828_alter_amocrm_leads_add_employee_id extends Migration
{

    public function safeUp()
    {
        $this->truncateTable('{{amocrm_leads}}');
        $this->addColumn('{{amocrm_leads}}', 'employee_id', $this->integer()->notNull()->comment('ID покупателя на сайте'));
        $this->addForeignKey(
            'amocrm_leads_employee_id', '{{amocrm_leads}}', 'employee_id',
            '{{employee}}', 'id', 'CASCADE', 'CASCADE'
        );

    }

    public function down()
    {
        $this->dropColumn('{{amocrm_leads}}', 'employee_id');
    }

}
