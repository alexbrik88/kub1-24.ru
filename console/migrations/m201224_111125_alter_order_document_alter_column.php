<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201224_111125_alter_order_document_alter_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%order_document}}', 'document_additional_number', $this->text());
    }

    public function safeDown()
    {
        $this->alterColumn('{{%order_document}}', 'document_additional_number', $this->string());
    }
}
