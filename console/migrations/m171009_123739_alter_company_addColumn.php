<?php

use console\components\db\Migration;
use yii\db\Schema;

class m171009_123739_alter_company_addColumn extends Migration
{
    public $tableName = 'company';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'ifns_ga', $this->integer(4)->unsigned()->defaultValue(null)->after('oktmo'));

        $this->execute("
            UPDATE {{company}}
            INNER JOIN {{company_ifns}}
                ON {{company}}.[[id]] = {{company_ifns}}.[[company_id]]
            INNER JOIN {{ifns}}
                ON {{company_ifns}}.[[ga]] = {{ifns}}.[[ga]]
            SET {{company}}.[[ifns_ga]] = {{ifns}}.[[ga]]
        ");
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'ifns_ga');
    }

}
