<?php

use yii\db\Schema;
use yii\db\Migration;

class m150519_104254_create_Log extends Migration
{
    public function safeUp()
    {
        $this->createTable('log', [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull(),
            'author_id' => $this->integer()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'model_name' => $this->string(255)->notNull(),
            'model_id' => $this->integer()->notNull(),
            'data' => $this->text(),
        ]);

        $this->createIndex('model_idx', 'log', [
            'model_name', 'model_id',
        ]);

        $this->addForeignKey('fk_log_to_author', 'log', 'author_id', 'employee', 'id');
        $this->addForeignKey('fk_log_to_company', 'log', 'company_id', 'company', 'id');
    }

    public function safeDown()
    {
        $this->dropTable('log');
    }
}
