<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%crm_task_comment}}`.
 */
class m210712_072515_create_crm_task_comment_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%crm_task_comment}}', [
            'id' => $this->bigPrimaryKey(),
            'crm_task_id' => $this->bigInteger()->notNull(),
            'employee_id' => $this->integer()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'text' => $this->string()->notNull(),
        ]);

        $this->createIndex('employee_company', '{{%crm_task_comment}}', [
            'employee_id',
            'company_id',
        ]);

        $this->addForeignKey('fk_crm_task_comment__crm_task', '{{%crm_task_comment}}', 'crm_task_id', '{{%crm_task}}', 'task_id');

        $this->addForeignKey('fk_crm_task_comment__employee_company', '{{%crm_task_comment}}', [
            'employee_id',
            'company_id',
        ], '{{%employee_company}}', [
            'employee_id',
            'company_id',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%crm_task_comment}}');
    }
}
