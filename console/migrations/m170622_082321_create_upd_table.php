<?php

use yii\db\Migration;

/**
 * Handles the creation of table `upd`.
 */
class m170622_082321_create_upd_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('upd', [
            'id' => $this->primaryKey(),
            'uid' => $this->string(5),
            'type' => $this->smallInteger(1)->notNull(),
            'invoice_id' => $this->integer()->notNull(),
            'document_author_id' => $this->integer()->notNull(),
            'status_out_id' => $this->integer()->notNull(),
            'status_out_author_id' => $this->integer()->notNull(),
            'document_date' => $this->date()->notNull(),
            'document_number' => $this->string()->notNull(),
            'document_additional_number' => $this->string(),
            'waybill_number' => $this->string(),
            'waybill_date' => $this->date(),
            'object_guid' => $this->string(36),
            'basis_document_name' => $this->string(),
            'basis_document_number' => $this->string(50),
            'basis_document_date' => $this->date(),
            'basis_document_type_id' => $this->integer(),
            'consignor_id' => $this->integer()->notNull(),
            'ordinal_document_number' => $this->integer()->notNull(),
            'is_original' => $this->smallInteger(1)->notNull()->defaultValue(false),
            'is_original_updated_at' => $this->integer(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'status_out_updated_at' => $this->integer(),
            'signed_by_employee_id' => $this->integer(),
            'signed_by_name' => $this->string(50),
            'sign_document_type_id' => $this->integer(),
            'sign_document_number' => $this->string(50),
            'sign_document_date' => $this->date(),
            'signature_id' => $this->integer(),
        ]);

        $this->addForeignKey('FK_upd_invoice', 'upd', 'invoice_id', 'invoice', 'id');
        $this->addForeignKey('FK_upd_employee_author', 'upd', 'document_author_id', 'employee', 'id');
        $this->addForeignKey('FK_upd_updStatus', 'upd', 'status_out_id', 'upd_status', 'id');
        $this->addForeignKey('FK_upd_employee_statusAuthor', 'upd', 'status_out_author_id', 'employee', 'id');
        $this->addForeignKey('FK_upd_employee_signedBy', 'upd', 'signed_by_employee_id', 'employee', 'id');
        $this->addForeignKey('FK_upd_documentType', 'upd', 'sign_document_type_id', 'document_type', 'id');
        $this->addForeignKey('FK_upd_employeeSignature', 'upd', 'signature_id', 'employee_signature', 'id');
        $this->addForeignKey('FK_upd_agreementType', 'upd', 'basis_document_type_id', 'agreement_type', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('upd');
    }
}
