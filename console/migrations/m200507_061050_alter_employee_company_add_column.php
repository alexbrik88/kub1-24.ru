<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200507_061050_alter_employee_company_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%employee_company}}', 'info_role_id', $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%employee_company}}', 'info_role_id');
    }
}
