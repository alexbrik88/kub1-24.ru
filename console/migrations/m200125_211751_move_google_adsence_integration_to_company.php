<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200125_211751_move_google_adsence_integration_to_company extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('{{%employee}}', 'google_adsense_access_token');
    }

    public function safeDown()
    {
        $this->addColumn('{{%employee}}', 'google_adsense_access_token', $this->text()->defaultValue(null));
    }
}
