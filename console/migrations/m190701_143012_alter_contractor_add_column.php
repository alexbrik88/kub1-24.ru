<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190701_143012_alter_contractor_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('contractor', 'agent_agreement_id', $this->integer());
        $this->addForeignKey('fk_contractor_agent_agreement', 'contractor', 'agent_agreement_id', 'agreement', 'id', 'SET NULL');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_contractor_agent_agreement', 'contractor');
        $this->dropColumn('contractor', 'agent_agreement');
    }
}
