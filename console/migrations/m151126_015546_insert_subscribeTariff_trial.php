<?php

use yii\db\Schema;
use yii\db\Migration;

class m151126_015546_insert_subscribeTariff_trial extends Migration
{
    public $tTariff = 'service_subscribe_tariff';

    public function safeUp()
    {
        $this->insert($this->tTariff, [
            'id' => 4,
            'duration' => 1,
            'price' => 0,
        ]);
    }
    
    public function safeDown()
    {
        $this->delete($this->tTariff, 'id = 4');
    }
}
