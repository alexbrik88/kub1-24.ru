<?php

use yii\db\Migration;

class m210619_165830_alter_user_config_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%user_config}}';

    /**
     * @inheritDoc
     */
    public function up(): void
    {
        $this->addColumn(self::TABLE_NAME, 'credits_currency_id', $this->boolean()->notNull()->defaultValue(true));
    }

    /**
     * @inheritDoc
     */
    public function down(): void
    {
        $this->dropColumn(self::TABLE_NAME, 'credits_currency_id');
    }
}
