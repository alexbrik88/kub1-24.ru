<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%product_turnover}}`.
 */
class m200810_100317_create_product_turnover_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%product_turnover}}', [
            'order_id' => $this->integer()->notNull(),
            'order_table' => $this->string(50)->notNull(),
            'document_id' => $this->integer()->notNull(),
            'document_table' => $this->string(50)->notNull(),
            'company_id' => $this->integer()->notNull(),
            'date' => $this->date()->notNull(),
            'invoice_id' => $this->integer()->notNull(),
            'contractor_id' => $this->integer()->notNull(),
            'type' => $this->tinyInteger(1)->notNull(),
            'production_type' => $this->tinyInteger(1),
            'is_invoice_actual' => $this->boolean(),
            'is_document_actual' => $this->boolean(),
            'purchase_price' => $this->decimal(20)->notNull(),
            'price_one' => $this->decimal(22, 2)->notNull(),
            'quantity' => $this->decimal(20, 10)->notNull(),
            'total_amount' => $this->decimal(22, 2)->notNull(),
            'purchase_amount' => $this->decimal(20)->notNull(),
            'year' => $this->smallInteger(4)->notNull(),
            'month' => $this->tinyInteger(2)->notNull(),
            'product_id' => $this->integer()->notNull(),
            'PRIMARY KEY (order_id, order_table)',
        ]);

        $this->createIndex('document_id__document_table', '{{%product_turnover}}', [
            'document_id',
            'document_table',
        ]);

        $this->createIndex('company_id__date', '{{%product_turnover}}', [
            'company_id',
            'date',
        ]);

        $this->createIndex('invoice_id', '{{%product_turnover}}', 'invoice_id');
        $this->createIndex('product_id', '{{%product_turnover}}', 'product_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%product_turnover}}');
    }
}
