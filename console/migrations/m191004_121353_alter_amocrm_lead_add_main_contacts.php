<?php

use console\components\db\Migration;

class m191004_121353_alter_amocrm_lead_add_main_contacts extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{amocrm_leads}}', 'main_contact_id', $this->integer()->unsigned()->comment('ID Основного контакта (amocrm_contacts.id)'));
        $this->addColumn('{{amocrm_leads}}', 'company_id', $this->integer()->unsigned()->comment('ID Компании (amocrm_contacts.id)'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{amocrm_leads}}', 'main_contact_id');
        $this->dropColumn('{{amocrm_leads}}', 'company_id');
    }
}
