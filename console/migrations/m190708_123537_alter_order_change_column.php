<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190708_123537_alter_order_change_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%order}}', 'discount', $this->decimal(22, 6));
    }

    public function safeDown()
    {
        $this->alterColumn('{{%order}}', 'discount', $this->decimal(9, 6));
    }
}