<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180209_102353_alter_cash_order_flows_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cash_order_flows', 'other_rs_id', $this->integer()->defaultValue(null)->after('other_cashbox_id'));
    }

    public function safeDown()
    {
        $this->dropColumn('cash_order_flows', 'other_rs_id');
    }
}
