<?php

use yii\db\Migration;

class m200827_194552_create_crm_client_type_tables extends Migration
{
    /** @var string[] */
    private const TABLE_LIST = [
        '{{%crm_client_campaign}}' => 'client_campaign_id',
        '{{%crm_client_activity}}' => 'client_activity_id',
        '{{%crm_client_check}}' => 'client_check_id',
        '{{%crm_client_event}}' => 'client_event_id',
        '{{%crm_client_position}}' => 'client_position_id',
        '{{%crm_client_reason}}' => 'client_reason_id',
        '{{%crm_client_status}}' => 'client_status_id',
    ];

    /**
     * @inheritDoc
     */
    public function up()
    {
        foreach (self::TABLE_LIST as $table => $column) {
            $this->addTable($table, $column);
        }
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        foreach (self::TABLE_LIST as $table => $column) {
            $this->dropTable($table);
        }
    }

    /**
     * @param string $table
     * @param string $column
     * @return void
     */
    private function addTable(string $table, string $column): void
    {
        $prefix = str_replace(['{{%', '}}'], '', $table);

        $this->createTable($table, [
            $column => $this->bigPrimaryKey(),
            'company_id' => $this->integer()->notNull(),
            'employee_id' => $this->integer()->null(),
            'name' => $this->string(64)->notNull(),
            'status' => $this->boolean()->notNull()->defaultValue(true),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->createIndex('ck_company_id', $table, 'company_id');
        $this->addForeignKey(
            'fk_' . $prefix . '__company_company_id',
            $table,
            ['company_id'],
            '{{%company}}',
            ['id'],
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex('ck_' . $column, $table, ['company_id', 'employee_id']);
        $this->addForeignKey(
            'fk_' . $prefix . '__employee_company_employee_id_company_id',
            $table,
            ['company_id', 'employee_id'],
            '{{%employee_company}}',
            ['company_id', 'employee_id'],
            'CASCADE',
            'CASCADE'
        );

        $this->createIndex('uk_name', $table, ['company_id', 'name'], true);
    }
}
