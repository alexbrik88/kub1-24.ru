<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170810_085338_alter_invoiceFacture_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%invoice_facture}}', 'show_service_units', $this->smallInteger(1)->defaultValue(false));

        $this->execute("
			UPDATE {{%invoice_facture}} {{iFacture}}
			LEFT JOIN (
				SELECT {{orderIf}}.[[invoice_facture_id]],
					SUM({{orderIf}}.[[empty_unit_code]]) [[unit_code]],
					SUM({{orderIf}}.[[empty_unit_name]]) [[unit_name]],
					SUM({{orderIf}}.[[empty_quantity]]) [[quantity]],
					SUM({{orderIf}}.[[empty_price]]) [[price]]
				FROM {{%order_invoice_facture}} {{orderIf}}
				INNER JOIN {{%order}} {{order}}
					ON {{orderIf}}.[[order_id]] = {{order}}.[[id]]
				INNER JOIN {{%product}} {{product}}
					ON {{order}}.[[product_id]] = {{product}}.[[id]]
				WHERE {{product}}.[[production_type]] = 0
				GROUP BY {{orderIf}}.[[invoice_facture_id]]
			) {{orders}} ON {{iFacture}}.[[id]] = {{orders}}.[[invoice_facture_id]]
			SET {{iFacture}}.[[show_service_units]] = 1
			WHERE {{iFacture}}.[[type]] = 2
				AND {{orders}}.[[invoice_facture_id]] IS NOT NULL
				AND {{orders}}.[[unit_code]] = 0
				AND {{orders}}.[[unit_name]] = 0
				AND {{orders}}.[[quantity]] = 0
				AND {{orders}}.[[price]] = 0
        ");
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%invoice_facture}}', 'show_service_units');
    }
}
