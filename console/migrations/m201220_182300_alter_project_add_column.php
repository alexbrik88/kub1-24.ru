<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201220_182300_alter_project_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('project', 'direction_id', $this->integer()->defaultValue(1));
        $this->update('project', ['direction_id' => 1]);

        $this->createTable('project_direction', [
            'id' => $this->primaryKey(),
            'title' => $this->string(255)->notNull(),
            'company_id' => $this->integer()->defaultValue(0),
        ]);

        $this->insert('project_direction', [
            'id' => 1,
            'title' => 'Без направления',
            'company_id' => 0
        ]);

        $this->addForeignKey('FK_project_to_project_direction', 'project', 'direction_id', 'project_direction', 'id', 'SET NULL');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_project_to_project_direction', 'project');
        $this->dropColumn('project', 'direction_id');
        $this->dropTable('project_direction');
    }
}
