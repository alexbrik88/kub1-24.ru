<?php

use console\components\db\Migration;
use yii\db\Schema;

class m151216_145646_update_invoice_status extends Migration
{

    public $tInvoice = 'invoice';

    public function safeUp()
    {
        // 2 - send, 3 - viewed
        $this->update($this->tInvoice, ['invoice_status_id' => 2], ['invoice_status_id' => 3]);
    }

    public function safeDown()
    {
        echo 'Hasn\'t down migration.';
    }
}


