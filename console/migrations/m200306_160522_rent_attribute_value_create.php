<?php

use console\components\db\Migration;

class m200306_160522_rent_attribute_value_create extends Migration
{
    const TABLE = '{{%rent_attribute_value}}';

    public function safeUp()
    {
        $this->createTable(self::TABLE, [
            'entity_id' => $this->integer()->unsigned()->notNull()->comment('ID объекта'),
            'attribute_id' => "MEDIUMINT UNSIGNED NOT NULL COMMENT 'ID атрибута'",
            'value' => $this->string(500)->notNull()->comment('Значение')
        ], "COMMENT 'Учёт аренды техники: значения дополнительных атрибутов объектов аренды'");
        $this->addForeignKey(
            'rent_entity_attribute_entity_id', self::TABLE, 'entity_id',
            '{{%rent_entity}}', 'id', 'CASCADE', 'CASCADE'
        );
        $this->addForeignKey(
            'rent_entity_attribute_attribute_id', self::TABLE, 'attribute_id',
            '{{%rent_attribute}}', 'id', 'CASCADE', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable(self::TABLE);
    }
}
