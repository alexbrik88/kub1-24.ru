<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190820_205839_add_yandex_access_token_to_employee extends Migration
{
    public $tableName = '{{%employee}}';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'yandex_access_token', $this->string()->defaultValue(null));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'yandex_access_token');
    }
}
