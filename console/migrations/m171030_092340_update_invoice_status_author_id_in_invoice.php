<?php

use console\components\db\Migration;
use yii\db\Schema;

class m171030_092340_update_invoice_status_author_id_in_invoice extends Migration
{
    public $tableName = 'invoice';

    public function safeUp()
    {
        $this->dropForeignKey('fk_ivoice_to_status_author', $this->tableName);

        $this->alterColumn($this->tableName, 'invoice_status_author_id', $this->integer()->defaultValue(null));

        $this->addForeignKey('fk_ivoice_to_status_author', $this->tableName, 'invoice_status_author_id', 'employee', 'id', 'RESTRICT', 'CASCADE');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('fk_ivoice_to_status_author', $this->tableName);

        $this->alterColumn($this->tableName, 'invoice_status_author_id', $this->integer()->notNull());

        $this->addForeignKey('fk_ivoice_to_status_author', $this->tableName, 'invoice_status_author_id', 'employee', 'id', 'RESTRICT', 'CASCADE');
    }
}


