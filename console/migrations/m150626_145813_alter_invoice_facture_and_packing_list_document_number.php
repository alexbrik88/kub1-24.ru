<?php

use yii\db\Schema;
use yii\db\Migration;

class m150626_145813_alter_invoice_facture_and_packing_list_document_number extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('packing_list', 'document_number', Schema::TYPE_STRING);
        $this->alterColumn('invoice_facture', 'document_number', Schema::TYPE_STRING);
    }
    
    public function safeDown()
    {
        $this->alterColumn('packing_list', 'document_number', Schema::TYPE_INTEGER);
        $this->alterColumn('invoice_facture', 'document_number', Schema::TYPE_INTEGER);
    }
}
