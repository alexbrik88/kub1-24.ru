<?php

use yii\db\Migration;

/**
 * Handles the creation of table `emoney`.
 */
class m190805_071739_create_emoney_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%emoney}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'name' => $this->char(50)->notNull(),
            'is_main' => $this->boolean()->notNull()->defaultValue(false),
            'is_accounting' => $this->boolean()->notNull()->defaultValue(false),
            'is_closed' => $this->boolean()->notNull()->defaultValue(false),
        ]);

        $this->addForeignKey('FK_emoney_company', '{{%emoney}}', 'company_id', '{{%company}}', 'id');
        $this->createIndex('name', '{{%emoney}}', ['company_id', 'name'], true);
        $this->createIndex('is_main', '{{%emoney}}', 'is_main');
        $this->createIndex('is_accounting', '{{%emoney}}', 'is_accounting');
        $this->createIndex('is_closed', '{{%emoney}}', 'is_closed');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('{{%emoney}}');
    }
}
