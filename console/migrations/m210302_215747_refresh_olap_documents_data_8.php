<?php

use console\components\db\Migration;
use yii\db\Expression;
use yii\db\Query;
use yii\db\Schema;

class m210302_215747_refresh_olap_documents_data_8 extends Migration
{
    // Created table
    const TABLE_OLAP_DOCS = 'olap_documents';

    const TABLE_PACKING_LIST = 'packing_list';
    const TABLE_UPD = 'upd';
    const TABLE_ACT = 'act';
    const TABLE_INVOICE = 'invoice';
    const REF_TABLE_INVOICE_UPD = 'invoice_upd';
    const REF_TABLE_INVOICE_ACT = 'invoice_act';

    const DOCUMENT_ACT = 1;
    const DOCUMENT_PACKING_LIST = 4;
    const DOCUMENT_UPD = 8;
    const DOCUMENT_INITIAL_BALANCE = 0;

    public function safeUp()
    {
        $table = self::TABLE_OLAP_DOCS;
        $idx = "idx_{$table}";

        $query = $this->getOlapTableQuery();

        $this->execute("DROP TABLE IF EXISTS {$table}");
        $this->execute("CREATE TABLE IF NOT EXISTS {$table} (
            `company_id` INT NOT NULL,
            `contractor_id` INT DEFAULT NULL,
            `by_document` TINYINT NOT NULL,
            `id` INT NOT NULL,
            `type` TINYINT NOT NULL,
            `date` DATE NOT NULL,
            `amount` BIGINT DEFAULT 0,
            `is_accounting` TINYINT DEFAULT 1,
            `invoice_expenditure_item_id` INT
        )");

        $this->execute("INSERT INTO {$table} ({$query->createCommand()->rawSql});");
        $this->execute("CREATE INDEX {$idx}_date ON {$table} (`company_id`, `date`);");
        $this->execute("CREATE INDEX {$idx}_contractor ON {$table} (`contractor_id`);");
        $this->execute("ALTER TABLE {$table} ADD PRIMARY KEY (`id`, `by_document`);");
    }

    public function safeDown()
    {
        // no down
    }

    private function getOlapTableQuery()
    {
        $select = [
            'invoice.company_id',
            'invoice.contractor_id',
            'by_document',
            'document.id',
            'document.type',
            'document.document_date AS date',
            'SUM( IF(document.type = 2, `order_main`.selling_price_with_vat, `order_main`.purchase_price_with_vat) * `order`.quantity) AS amount',
            'IF (contractor.not_accounting, 0, 1) AS is_accounting',
            'invoice.invoice_expenditure_item_id',
        ];

        // оборот по товарным накладным
        $query1 = (new Query)
            ->select($select)
            ->addSelect(['by_document' => new Expression('"' . self::DOCUMENT_PACKING_LIST . '"')])
            ->from(['order' => 'order_packing_list'])
            ->leftJoin(['document' => self::TABLE_PACKING_LIST], '{{document}}.[[id]] = {{order}}.[[packing_list_id]]')
            ->leftJoin(['invoice' => self::TABLE_INVOICE], '{{invoice}}.[[id]] = {{document}}.[[invoice_id]]')
            ->leftJoin(['order_main' => 'order'], '{{order_main}}.[[id]] = {{order}}.[[order_id]]')
            ->leftJoin(['contractor' => 'contractor'], 'contractor.id = invoice.contractor_id')
            ->andWhere([
                'invoice.is_deleted' => false,
                'invoice.invoice_status_id' => [1, 2, 3, 4, 6, 7, 8], // valid statuses
            ])
            ->andWhere([
                'or',
                ['document.status_out_id' => [1, 2, 3, 4]], // valid statuses
                ['document.status_out_id' => null],
            ])
            ->groupBy('document.id');

        // оборот по УПД
        $query2 = (new Query)
            ->select($select)
            ->addSelect(['by_document' => new Expression('"' . self::DOCUMENT_UPD . '"')])
            ->from(['order' => 'order_upd'])
            ->leftJoin(['document' => self::TABLE_UPD], '{{document}}.[[id]] = {{order}}.[[upd_id]]')
            ->leftJoin(['invoice_upd' => self::REF_TABLE_INVOICE_UPD], '{{invoice_upd}}.[[upd_id]] = {{document}}.[[id]]')
            ->leftJoin(['invoice' => self::TABLE_INVOICE], '{{invoice}}.[[id]] = {{invoice_upd}}.[[invoice_id]]')
            ->leftJoin(['order_main' => 'order'], '{{order_main}}.[[id]] = {{order}}.[[order_id]]')
            ->leftJoin(['contractor' => 'contractor'], 'contractor.id = invoice.contractor_id')
            ->andWhere([
                'invoice.is_deleted' => false,
                'invoice.invoice_status_id' => [1, 2, 3, 4, 6, 7, 8],
            ])
            ->andWhere([
                'or',
                ['document.status_out_id' => [1, 2, 3, 4]],
                ['document.status_out_id' => null],
            ])
            ->groupBy('invoice.id, document.id');

        // оборот по Актам
        $query3 = (new Query)
            ->select($select)
            ->addSelect(['by_document' => new Expression('"' . self::DOCUMENT_ACT . '"')])
            ->from(['order' => 'order_act'])
            ->leftJoin(['document' => self::TABLE_ACT], '{{document}}.[[id]] = {{order}}.[[act_id]]')
            ->leftJoin(['invoice_act' => self::REF_TABLE_INVOICE_ACT], '{{invoice_act}}.[[act_id]] = {{document}}.[[id]]')
            ->leftJoin(['invoice' => self::TABLE_INVOICE], '{{invoice}}.[[id]] = {{invoice_act}}.[[invoice_id]]')
            ->leftJoin(['order_main' => 'order'], '{{order_main}}.[[id]] = {{order}}.[[order_id]]')
            ->leftJoin(['contractor' => 'contractor'], 'contractor.id = invoice.contractor_id')
            ->andWhere([
                'invoice.is_deleted' => false,
                'invoice.invoice_status_id' => [1, 2, 3, 4, 6, 7, 8],
            ])
            ->andWhere([
                'or',
                ['document.status_out_id' => [1, 2, 3, 4]],
                ['document.status_out_id' => null],
            ])
            ->groupBy('invoice.id, document.id');

        return $query1->union($query2)->union($query3); // not "all" for prevent double primary
    }
}
