<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200728_133720_create_table_company_affiliate_link extends Migration
{
    public $tableName = 'company_affiliate_link';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer(11),
            'name' => $this->string(),
            'link' => $this->string(255),
            'service_module_id' => $this->integer(11),
            'percent_reward' => $this->integer()->unsigned()->defaultValue(20),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
        ]);

        $this->createIndex($this->tableName . '_company_id_idx', $this->tableName, 'company_id');
        $this->createIndex($this->tableName . '_service_module_id_idx', $this->tableName, 'service_module_id');

        $this->addForeignKey('fk_company_affiliate_link_company',
            $this->tableName,
            'company_id',
            'company',
            'id');

        $this->addForeignKey('fk_company_affiliate_link_service_module',
            $this->tableName,
            'service_module_id',
            'service_module',
            'id');
    }

    public function safeDown()
    {
        try {
            $this->dropForeignKey('fk_company_affiliate_link_company', $this->tableName);
            $this->dropIndex($this->tableName . '_company_id_idx', $this->tableName);
        } catch (\Exception $e) {
            echo "Drop foreign and index fails";
        }

        $this->dropTable($this->tableName);
    }

}
