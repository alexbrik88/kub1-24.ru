<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181015_053213_alter_agreements_new_add_column extends Migration
{
    public function safeUp()
    {
		$this->addColumn('{{%agreement_new}}', 'invoice_id', $this->integer());
    }
    
    public function safeDown()
    {

    }
}


