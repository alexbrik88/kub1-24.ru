<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160921_110804_insert_company_type extends Migration
{
    public $tCompanyType = 'company_type';
    public $tCompany = 'company';
    public $tContractor = 'contractor';

    public function safeUp()
    {
    //    $this->dropForeignKey('fk_company_to_company_type', $this->tCompany);
    //    $this->dropForeignKey('fk_contractor_to_company_type', $this->tContractor);
//
    //    $this->delete($this->tCompanyType, ['in', 'id', [8, 9]]);
    //    $this->update($this->tCompanyType, ['id' => 8], ['id' => 10]);
//
    //    $this->batchInsert($this->tCompanyType, ['id', 'name_short', 'name_full', 'in_company', 'in_contractor'], [
    //        [9, 'ННО', 'Негосударственная некоммерческая организация', 0, 1],
    //        [10, 'ФГКУ', 'Федеральное Государственное Казённое Учрежден', 0, 1],
    //        [11, 'НО', 'Некоммерческая Организация', 0, 1],
    //    ]);
//
    //    $this->update($this->tCompany, ['company_type_id' => 15], ['company_type_id' => 10]);
    //    $this->update($this->tContractor, ['company_type_id' => 15], ['company_type_id' => 10]);
//
    //    $this->update($this->tCompany, ['company_type_id' => 10], ['company_type_id' => 9]);
    //    $this->update($this->tContractor, ['company_type_id' => 10], ['company_type_id' => 9]);
//
    //    $this->update($this->tCompany, ['company_type_id' => 9], ['company_type_id' => 8]);
    //    $this->update($this->tContractor, ['company_type_id' => 9], ['company_type_id' => 8]);
//
    //    $this->update($this->tCompany, ['company_type_id' => 8], ['company_type_id' => 15]);
    //    $this->update($this->tContractor, ['company_type_id' => 8], ['company_type_id' => 15]);
//
    //    $this->addForeignKey('fk_company_to_company_type', $this->tCompany, 'company_type_id', $this->tCompanyType, 'id', 'RESTRICT', 'CASCADE');
    //    $this->addForeignKey('fk_contractor_to_company_type', $this->tContractor, 'company_type_id', $this->tCompanyType, 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown()
    {
        echo 'this migration not rolled back';
    }
}


