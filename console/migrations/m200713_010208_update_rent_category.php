<?php

use console\components\db\Migration;
use yii\db\Query;
use yii\db\Expression;

class m200713_010208_update_rent_category extends Migration
{
    public $table = 'rent_category';

    public function safeUp()
    {
        $titles = (new Query())->from($this->table)->select('title')->column();
        $expr = new Expression('concat(upper(substr(title, 1, 1)), substr(title, 2))');
        foreach ($titles as $title) {
            $this->update($this->table, ['title' => $expr], ['title' => $title]);
        }
    }

    public function safeDown()
    {
    }
}
