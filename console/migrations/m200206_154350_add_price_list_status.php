<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200206_154350_add_price_list_status extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->batchInsert('{{%price_list_status}}', ['id', 'name'], [
            [3, 'В архиве'],
        ]);
    }

    public function safeDown()
    {
        $this->execute('DELETE FROM `price_list_status` WHERE `id` = 3 LIMIT 1');
    }
}
