<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211026_181034_add_invoice_expenditure_items extends Migration
{
    public function safeUp()
    {
        $this->insert('invoice_expenditure_item', [
            'id' => 98,
            'name' => 'Себестоимость товара',
            'is_visible' => 0,
            'can_be_controlled' => 0,
        ]);
        $this->insert('invoice_expenditure_item', [
            'id' => 99,
            'name' => 'Себестоимость услуг',
            'is_visible' => 0,
            'can_be_controlled' => 0,
        ]);

        try {
            $primeCostPalType = 70; // AnalyticsArticle::PAL_EXPENSES_PRIME_COST
            $companiesIds = (new \yii\db\Query())
                ->select('company_id')
                ->distinct()
                ->from('analytics_article')
                ->column();

            if ($companiesIds) {
                foreach ($companiesIds as $companyId) {
                    $this->batchInsert('analytics_article', ['company_id', 'type', 'item_id', 'profit_and_loss_type'], [
                        [$companyId, 0, 98, $primeCostPalType],
                        [$companyId, 0, 99, $primeCostPalType],
                    ]);
                }
            }
        } catch (Exception $e) {
            echo "\nNot added into analytics_article!";
        }
    }

    public function safeDown()
    {
        $this->delete('analytics_article', ['item_id' => 98]);
        $this->delete('analytics_article', ['item_id' => 99]);

        $this->delete('invoice_expenditure_item', ['id' => 98]);
        $this->delete('invoice_expenditure_item', ['id' => 99]);
    }
}
