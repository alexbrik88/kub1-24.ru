<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190111_153908_alter_cash_order_flows_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cash_order_flows', 'is_taxable', $this->boolean()->notNull()->defaultValue(false));

        $this->execute('
            UPDATE {{cash_order_flows}}
            SET {{cash_order_flows}}.[[is_taxable]] = 1
            WHERE {{cash_order_flows}}.[[is_accounting]] = 1
        ');
    }

    public function safeDown()
    {
        $this->dropColumn('cash_order_flows', 'is_taxable');
    }
}
