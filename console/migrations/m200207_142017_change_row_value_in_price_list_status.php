<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200207_142017_change_row_value_in_price_list_status extends Migration
{
    public function safeUp()
    {
        $this->execute('UPDATE `price_list_status` SET `name` = "Действующий" WHERE `id` = 1 LIMIT 1');
    }

    public function safeDown()
    {
        $this->execute('UPDATE `price_list_status` SET `name` = "Создан" WHERE `id` = 1 LIMIT 1');
    }
}
