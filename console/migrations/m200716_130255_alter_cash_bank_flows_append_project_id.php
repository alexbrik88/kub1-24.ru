<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200716_130255_alter_cash_bank_flows_append_project_id extends Migration
{
    public $tableName = 'cash_bank_flows';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'project_id', $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'project_id');
    }
}
