<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210715_143822_delete_company_info_place extends Migration
{
    public function safeUp()
    {
        $this->execute("SET foreign_key_checks = 0;");
        $this->update('{{%company}}', ['info_place_id' => 1], ['info_place_id' => 43]);
        $this->delete('{{%company_info_place}}', ['id' => 43]);
        $this->execute("SET foreign_key_checks = 1;");
    }

    public function safeDown()
    {
        $this->insert('{{%company_info_place}}', [
            'id' => 43,
            'name' => 'Россия',
        ]);
    }
}
