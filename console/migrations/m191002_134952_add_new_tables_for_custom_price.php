<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191002_134952_add_new_tables_for_custom_price extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%custom_price_group}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'name' => $this->string(),
        ], $tableOptions);

        $this->addForeignKey('FK_custom_price_group_company', '{{custom_price_group}}', 'company_id', '{{company}}', 'id');

        $this->createTable('{{%custom_price}}', [
            'product_id' => $this->integer()->notNull(),
            'price_group_id' => $this->integer()->notNull(),
            'price' => $this->bigInteger(20)->notNull(),
            'nds_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addPrimaryKey('PRIMARY_KEY', '{{%custom_price}}', ['product_id', 'price_group_id']);
        $this->addForeignKey('FK_custom_price_to_product', '{{%custom_price}}', 'product_id', '{{%product}}', 'id', 'CASCADE');
        $this->addForeignKey('FK_custom_price_to_custom_price_group', '{{%custom_price}}', 'price_group_id', '{{%custom_price_group}}', 'id', 'CASCADE');
        $this->addForeignKey('FK_custom_price_to_tax_rate', '{{%custom_price}}', 'nds_id', 'tax_rate', 'id');
    }

    public function safeDown()
    {
        $this->dropTable('{{%custom_price}}');
        $this->dropTable('{{%custom_price_group}}');
    }
}
