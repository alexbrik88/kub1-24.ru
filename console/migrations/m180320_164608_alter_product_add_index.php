<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180320_164608_alter_product_add_index extends Migration
{
    public function safeUp()
    {
        $this->createIndex('production_type', 'product', 'production_type');
        $this->createIndex('is_deleted', 'product', 'is_deleted');
        $this->createIndex('not_for_sale', 'product', 'not_for_sale');
    }

    public function safeDown()
    {
        $this->dropIndex('production_type', 'product');
        $this->dropIndex('is_deleted', 'product');
        $this->dropIndex('not_for_sale', 'product');
    }
}
