<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190912_083123_alter_company_type_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%company_type}}', 'like_ip', $this->boolean()->defaultValue(false));

        $this->update('{{%company_type}}', ['like_ip' => 1], ['id' => 1]);
    }

    public function safeDown()
    {
        $this->dropColumn('{{%company_type}}', 'like_ip');
    }
}
