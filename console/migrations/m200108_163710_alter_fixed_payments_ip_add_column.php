<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200108_163710_alter_fixed_payments_ip_add_column extends Migration
{
    public static $limits = [
        2018 => 21236000,
        2019 => 23483200,
        2020 => 25958400,
    ];

    public function safeUp()
    {
        $this->addColumn('{{%fixed_payments_ip}}', 'limit', $this->integer());

        foreach (self::$limits as $year => $limit) {
            $this->update('{{%fixed_payments_ip}}', ['limit' => $limit], ['year' => $year]);
        }
    }

    public function safeDown()
    {
        $this->addColumn('{{%fixed_payments_ip}}', 'limit');
    }
}
