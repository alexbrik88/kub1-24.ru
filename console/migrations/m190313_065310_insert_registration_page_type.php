<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190313_065310_insert_registration_page_type extends Migration
{
    public $tableName = '{{%registration_page_type}}';

    public function safeUp()
    {
        $this->insert($this->tableName, [
            'id' => 18,
            'name' => 'TaxCom ОФД',
        ]);
    }

    public function safeDown()
    {
        $this->delete($this->tableName, ['id' => 18]);
    }
}
