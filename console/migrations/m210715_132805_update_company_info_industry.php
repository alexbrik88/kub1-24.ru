<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210715_132805_update_company_info_industry extends Migration
{
    public function safeUp()
    {
        $table = '{{%company_info_industry}}';
        $this->execute("UPDATE {$table} SET [[name]] = 'Интернет магазины и оффлайн магазины' WHERE [[id]] = 4;");
        $this->execute("UPDATE {$table} SET [[name]] = 'Интернет магазин' WHERE [[id]] = 5;");
        $this->execute("UPDATE {$table} SET [[name]] = 'Оффлайн магазин/магазины' WHERE [[id]] = 6;");
        $this->batchInsert($table, [
            'id',
            'name',
            'need_shop',
            'need_site',
            'need_storage',
        ], [
            [9, "Онлайн-сервис", 0, 1, 0,],
            [10, "Оптовая и розничная торговля", 1, 0, 1,],
            [11, "Транспортная компания", 0, 0, 0,],
            [12, "Строительство и ремонт", 0, 0, 0,],
        ]);
    }

    public function safeDown()
    {
        $table = '{{%company_info_industry}}';
        $this->execute("UPDATE {$table} SET [[name]] = 'Ритейл (Магазины и E-commerce)' WHERE [[id]] = 4;");
        $this->execute("UPDATE {$table} SET [[name]] = 'Только E-commerce (интернет)' WHERE [[id]] = 5;");
        $this->execute("UPDATE {$table} SET [[name]] = 'Только магазин/магазины' WHERE [[id]] = 6;");
        $this->delete($table, ['id' => [9, 10, 11, 12]]);
    }
}
