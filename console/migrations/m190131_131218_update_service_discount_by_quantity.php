<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190131_131218_update_service_discount_by_quantity extends Migration
{
    public function safeUp()
    {
        $this->update('service_discount_by_quantity', ['percent' => 40], [
            'tariff_id' => [1, 2, 3],
            'quantity' => 4,
        ]);
    }

    public function safeDown()
    {
        $this->update('service_discount_by_quantity', ['percent' => 41.6666], [
            'tariff_id' => [1, 2, 3],
            'quantity' => 4,
        ]);
    }
}
