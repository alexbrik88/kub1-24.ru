<?php

use yii\db\Migration;

/**
 * Handles adding type_face to table `contractor`.
 */
class m160531_101059_add_type_face_to_contractor extends Migration
{
    public $tContractor = 'contractor';
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->addColumn($this->tContractor, 'face_type', $this->boolean()->notNull()->comment('1 - physical person, 0 - legal person')->defaultValue('0'));
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropColumn($this->tContractor, 'face_type');
    }
}
