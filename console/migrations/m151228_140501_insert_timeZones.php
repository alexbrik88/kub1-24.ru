<?php

use console\components\db\Migration;

class m151228_140501_insert_timeZones extends Migration
{
    public $tTimeZone = 'time_zone';

    public function safeUp()
    {
        $this->batchInsert($this->tTimeZone, ['id', 'name', 'time_zone'], [
            [1, 'AD', 'Europe/Andorra'],
            [2, 'AE', 'Asia/Dubai'],
            [3, 'AF', 'Asia/Kabul'],
            [4, 'AG', 'America/Antigua'],
            [5, 'AI', 'America/Anguilla'],
            [6, 'AL', 'Europe/Tirane'],
            [7, 'AM', 'Asia/Yerevan'],
            [8, 'AO', 'Africa/Luanda'],
            [9, 'AQ', 'Antarctica/McMurdo'],
            [10, 'AQ', 'Antarctica/Rothera'],
            [11, 'AQ', 'Antarctica/Palmer'],
            [12, 'AQ', 'Antarctica/Mawson'],
            [13, 'AQ', 'Antarctica/Davis'],
            [14, 'AQ', 'Antarctica/Casey'],
            [15, 'AQ', 'Antarctica/Vostok'],
            [16, 'AQ', 'Antarctica/DumontDUrville'],
            [17, 'AQ', 'Antarctica/Syowa'],
            [18, 'AQ', 'Antarctica/Troll'],
            [19, 'AR', 'America/Argentina/Buenos_Aires'],
            [20, 'AR', 'America/Argentina/Cordoba'],
            [21, 'AR', 'America/Argentina/Salta'],
            [22, 'AR', 'America/Argentina/Jujuy'],
            [23, 'AR', 'America/Argentina/Tucuman'],
            [24, 'AR', 'America/Argentina/Catamarca'],
            [25, 'AR', 'America/Argentina/La_Rioja'],
            [26, 'AR', 'America/Argentina/San_Juan'],
            [27, 'AR', 'America/Argentina/Mendoza'],
            [28, 'AR', 'America/Argentina/San_Luis'],
            [29, 'AR', 'America/Argentina/Rio_Gallegos'],
            [30, 'AR', 'America/Argentina/Ushuaia'],
            [31, 'AS', 'Pacific/Pago_Pago'],
            [32, 'AT', 'Europe/Vienna'],
            [33, 'AU', 'Australia/Lord_Howe'],
            [34, 'AU', 'Antarctica/Macquarie'],
            [35, 'AU', 'Australia/Hobart'],
            [36, 'AU', 'Australia/Currie'],
            [37, 'AU', 'Australia/Melbourne'],
            [38, 'AU', 'Australia/Sydney'],
            [39, 'AU', 'Australia/Broken_Hill'],
            [40, 'AU', 'Australia/Brisbane'],
            [41, 'AU', 'Australia/Lindeman'],
            [42, 'AU', 'Australia/Adelaide'],
            [43, 'AU', 'Australia/Darwin'],
            [44, 'AU', 'Australia/Perth'],
            [45, 'AU', 'Australia/Eucla'],
            [46, 'AW', 'America/Aruba'],
            [47, 'AX', 'Europe/Mariehamn'],
            [48, 'AZ', 'Asia/Baku'],
            [49, 'BA', 'Europe/Sarajevo'],
            [50, 'BB', 'America/Barbados'],
            [51, 'BD', 'Asia/Dhaka'],
            [52, 'BE', 'Europe/Brussels'],
            [53, 'BF', 'Africa/Ouagadougou'],
            [54, 'BG', 'Europe/Sofia'],
            [55, 'BH', 'Asia/Bahrain'],
            [56, 'BI', 'Africa/Bujumbura'],
            [57, 'BJ', 'Africa/Porto-Novo'],
            [58, 'BL', 'America/St_Barthelemy'],
            [59, 'BM', 'Atlantic/Bermuda'],
            [60, 'BN', 'Asia/Brunei'],
            [61, 'BO', 'America/La_Paz'],
            [62, 'BQ', 'America/Kralendijk'],
            [63, 'BR', 'America/Noronha'],
            [64, 'BR', 'America/Belem'],
            [65, 'BR', 'America/Fortaleza'],
            [66, 'BR', 'America/Recife'],
            [67, 'BR', 'America/Araguaina'],
            [68, 'BR', 'America/Maceio'],
            [69, 'BR', 'America/Bahia'],
            [70, 'BR', 'America/Sao_Paulo'],
            [71, 'BR', 'America/Campo_Grande'],
            [72, 'BR', 'America/Cuiaba'],
            [73, 'BR', 'America/Santarem'],
            [74, 'BR', 'America/Porto_Velho'],
            [75, 'BR', 'America/Boa_Vista'],
            [76, 'BR', 'America/Manaus'],
            [77, 'BR', 'America/Eirunepe'],
            [78, 'BR', 'America/Rio_Branco'],
            [79, 'BS', 'America/Nassau'],
            [80, 'BT', 'Asia/Thimphu'],
            [81, 'BW', 'Africa/Gaborone'],
            [82, 'BY', 'Europe/Minsk'],
            [83, 'BZ', 'America/Belize'],
            [84, 'CA', 'America/St_Johns'],
            [85, 'CA', 'America/Halifax'],
            [86, 'CA', 'America/Glace_Bay'],
            [87, 'CA', 'America/Moncton'],
            [88, 'CA', 'America/Goose_Bay'],
            [89, 'CA', 'America/Blanc-Sablon'],
            [90, 'CA', 'America/Toronto'],
            [91, 'CA', 'America/Nipigon'],
            [92, 'CA', 'America/Thunder_Bay'],
            [93, 'CA', 'America/Iqaluit'],
            [94, 'CA', 'America/Pangnirtung'],
            [95, 'CA', 'America/Resolute'],
            [96, 'CA', 'America/Atikokan'],
            [97, 'CA', 'America/Rankin_Inlet'],
            [98, 'CA', 'America/Winnipeg'],
            [99, 'CA', 'America/Rainy_River'],
            [100, 'CA', 'America/Regina'],
            [101, 'CA', 'America/Swift_Current'],
            [102, 'CA', 'America/Edmonton'],
            [103, 'CA', 'America/Cambridge_Bay'],
            [104, 'CA', 'America/Yellowknife'],
            [105, 'CA', 'America/Inuvik'],
            [106, 'CA', 'America/Creston'],
            [107, 'CA', 'America/Dawson_Creek'],
            [108, 'CA', 'America/Fort_Nelson'],
            [109, 'CA', 'America/Vancouver'],
            [110, 'CA', 'America/Whitehorse'],
            [111, 'CA', 'America/Dawson'],
            [112, 'CC', 'Indian/Cocos'],
            [113, 'CD', 'Africa/Kinshasa'],
            [114, 'CD', 'Africa/Lubumbashi'],
            [115, 'CF', 'Africa/Bangui'],
            [116, 'CG', 'Africa/Brazzaville'],
            [117, 'CH', 'Europe/Zurich'],
            [118, 'CI', 'Africa/Abidjan'],
            [119, 'CK', 'Pacific/Rarotonga'],
            [120, 'CL', 'America/Santiago'],
            [121, 'CL', 'Pacific/Easter'],
            [122, 'CM', 'Africa/Douala'],
            [123, 'CN', 'Asia/Shanghai'],
            [124, 'CN', 'Asia/Urumqi'],
            [125, 'CO', 'America/Bogota'],
            [126, 'CR', 'America/Costa_Rica'],
            [127, 'CU', 'America/Havana'],
            [128, 'CV', 'Atlantic/Cape_Verde'],
            [129, 'CW', 'America/Curacao'],
            [130, 'CX', 'Indian/Christmas'],
            [131, 'CY', 'Asia/Nicosia'],
            [132, 'CZ', 'Europe/Prague'],
            [133, 'DE', 'Europe/Berlin'],
            [134, 'DE', 'Europe/Busingen'],
            [135, 'DJ', 'Africa/Djibouti'],
            [136, 'DK', 'Europe/Copenhagen'],
            [137, 'DM', 'America/Dominica'],
            [138, 'DO', 'America/Santo_Domingo'],
            [139, 'DZ', 'Africa/Algiers'],
            [140, 'EC', 'America/Guayaquil'],
            [141, 'EC', 'Pacific/Galapagos'],
            [142, 'EE', 'Europe/Tallinn'],
            [143, 'EG', 'Africa/Cairo'],
            [144, 'EH', 'Africa/El_Aaiun'],
            [145, 'ER', 'Africa/Asmara'],
            [146, 'ES', 'Europe/Madrid'],
            [147, 'ES', 'Africa/Ceuta'],
            [148, 'ES', 'Atlantic/Canary'],
            [149, 'ET', 'Africa/Addis_Ababa'],
            [150, 'FI', 'Europe/Helsinki'],
            [151, 'FJ', 'Pacific/Fiji'],
            [152, 'FK', 'Atlantic/Stanley'],
            [153, 'FM', 'Pacific/Chuuk'],
            [154, 'FM', 'Pacific/Pohnpei'],
            [155, 'FM', 'Pacific/Kosrae'],
            [156, 'FO', 'Atlantic/Faroe'],
            [157, 'FR', 'Europe/Paris'],
            [158, 'GA', 'Africa/Libreville'],
            [159, 'GB', 'Europe/London'],
            [160, 'GD', 'America/Grenada'],
            [161, 'GE', 'Asia/Tbilisi'],
            [162, 'GF', 'America/Cayenne'],
            [163, 'GG', 'Europe/Guernsey'],
            [164, 'GH', 'Africa/Accra'],
            [165, 'GI', 'Europe/Gibraltar'],
            [166, 'GL', 'America/Godthab'],
            [167, 'GL', 'America/Danmarkshavn'],
            [168, 'GL', 'America/Scoresbysund'],
            [169, 'GL', 'America/Thule'],
            [170, 'GM', 'Africa/Banjul'],
            [171, 'GN', 'Africa/Conakry'],
            [172, 'GP', 'America/Guadeloupe'],
            [173, 'GQ', 'Africa/Malabo'],
            [174, 'GR', 'Europe/Athens'],
            [175, 'GS', 'Atlantic/South_Georgia'],
            [176, 'GT', 'America/Guatemala'],
            [177, 'GU', 'Pacific/Guam'],
            [178, 'GW', 'Africa/Bissau'],
            [179, 'GY', 'America/Guyana'],
            [180, 'HK', 'Asia/Hong_Kong'],
            [181, 'HN', 'America/Tegucigalpa'],
            [182, 'HR', 'Europe/Zagreb'],
            [183, 'HT', 'America/Port-au-Prince'],
            [184, 'HU', 'Europe/Budapest'],
            [185, 'ID', 'Asia/Jakarta'],
            [186, 'ID', 'Asia/Pontianak'],
            [187, 'ID', 'Asia/Makassar'],
            [188, 'ID', 'Asia/Jayapura'],
            [189, 'IE', 'Europe/Dublin'],
            [190, 'IL', 'Asia/Jerusalem'],
            [191, 'IM', 'Europe/Isle_of_Man'],
            [192, 'IN', 'Asia/Kolkata'],
            [193, 'IO', 'Indian/Chagos'],
            [194, 'IQ', 'Asia/Baghdad'],
            [195, 'IR', 'Asia/Tehran'],
            [196, 'IS', 'Atlantic/Reykjavik'],
            [197, 'IT', 'Europe/Rome'],
            [198, 'JE', 'Europe/Jersey'],
            [199, 'JM', 'America/Jamaica'],
            [200, 'JO', 'Asia/Amman'],
            [201, 'JP', 'Asia/Tokyo'],
            [202, 'KE', 'Africa/Nairobi'],
            [203, 'KG', 'Asia/Bishkek'],
            [204, 'KH', 'Asia/Phnom_Penh'],
            [205, 'KI', 'Pacific/Tarawa'],
            [206, 'KI', 'Pacific/Enderbury'],
            [207, 'KI', 'Pacific/Kiritimati'],
            [208, 'KM', 'Indian/Comoro'],
            [209, 'KN', 'America/St_Kitts'],
            [210, 'KP', 'Asia/Pyongyang'],
            [211, 'KR', 'Asia/Seoul'],
            [212, 'KW', 'Asia/Kuwait'],
            [213, 'KY', 'America/Cayman'],
            [214, 'KZ', 'Asia/Almaty'],
            [215, 'KZ', 'Asia/Qyzylorda'],
            [216, 'KZ', 'Asia/Aqtobe'],
            [217, 'KZ', 'Asia/Aqtau'],
            [218, 'KZ', 'Asia/Oral'],
            [219, 'LA', 'Asia/Vientiane'],
            [220, 'LB', 'Asia/Beirut'],
            [221, 'LC', 'America/St_Lucia'],
            [222, 'LI', 'Europe/Vaduz'],
            [223, 'LK', 'Asia/Colombo'],
            [224, 'LR', 'Africa/Monrovia'],
            [225, 'LS', 'Africa/Maseru'],
            [226, 'LT', 'Europe/Vilnius'],
            [227, 'LU', 'Europe/Luxembourg'],
            [228, 'LV', 'Europe/Riga'],
            [229, 'LY', 'Africa/Tripoli'],
            [230, 'MA', 'Africa/Casablanca'],
            [231, 'MC', 'Europe/Monaco'],
            [232, 'MD', 'Europe/Chisinau'],
            [233, 'ME', 'Europe/Podgorica'],
            [234, 'MF', 'America/Marigot'],
            [235, 'MG', 'Indian/Antananarivo'],
            [236, 'MH', 'Pacific/Majuro'],
            [237, 'MH', 'Pacific/Kwajalein'],
            [238, 'MK', 'Europe/Skopje'],
            [239, 'ML', 'Africa/Bamako'],
            [240, 'MM', 'Asia/Rangoon'],
            [241, 'MN', 'Asia/Ulaanbaatar'],
            [242, 'MN', 'Asia/Hovd'],
            [243, 'MN', 'Asia/Choibalsan'],
            [244, 'MO', 'Asia/Macau'],
            [245, 'MP', 'Pacific/Saipan'],
            [246, 'MQ', 'America/Martinique'],
            [247, 'MR', 'Africa/Nouakchott'],
            [248, 'MS', 'America/Montserrat'],
            [249, 'MT', 'Europe/Malta'],
            [250, 'MU', 'Indian/Mauritius'],
            [251, 'MV', 'Indian/Maldives'],
            [252, 'MW', 'Africa/Blantyre'],
            [253, 'MX', 'America/Mexico_City'],
            [254, 'MX', 'America/Cancun'],
            [255, 'MX', 'America/Merida'],
            [256, 'MX', 'America/Monterrey'],
            [257, 'MX', 'America/Matamoros'],
            [258, 'MX', 'America/Mazatlan'],
            [259, 'MX', 'America/Chihuahua'],
            [260, 'MX', 'America/Ojinaga'],
            [261, 'MX', 'America/Hermosillo'],
            [262, 'MX', 'America/Tijuana'],
            [263, 'MX', 'America/Santa_Isabel'],
            [264, 'MX', 'America/Bahia_Banderas'],
            [265, 'MY', 'Asia/Kuala_Lumpur'],
            [266, 'MY', 'Asia/Kuching'],
            [267, 'MZ', 'Africa/Maputo'],
            [268, 'NA', 'Africa/Windhoek'],
            [269, 'NC', 'Pacific/Noumea'],
            [270, 'NE', 'Africa/Niamey'],
            [271, 'NF', 'Pacific/Norfolk'],
            [272, 'NG', 'Africa/Lagos'],
            [273, 'NI', 'America/Managua'],
            [274, 'NL', 'Europe/Amsterdam'],
            [275, 'NO', 'Europe/Oslo'],
            [276, 'NP', 'Asia/Kathmandu'],
            [277, 'NR', 'Pacific/Nauru'],
            [278, 'NU', 'Pacific/Niue'],
            [279, 'NZ', 'Pacific/Auckland'],
            [280, 'NZ', 'Pacific/Chatham'],
            [281, 'OM', 'Asia/Muscat'],
            [282, 'PA', 'America/Panama'],
            [283, 'PE', 'America/Lima'],
            [284, 'PF', 'Pacific/Tahiti'],
            [285, 'PF', 'Pacific/Marquesas'],
            [286, 'PF', 'Pacific/Gambier'],
            [287, 'PG', 'Pacific/Port_Moresby'],
            [288, 'PG', 'Pacific/Bougainville'],
            [289, 'PH', 'Asia/Manila'],
            [290, 'PK', 'Asia/Karachi'],
            [291, 'PL', 'Europe/Warsaw'],
            [292, 'PM', 'America/Miquelon'],
            [293, 'PN', 'Pacific/Pitcairn'],
            [294, 'PR', 'America/Puerto_Rico'],
            [295, 'PS', 'Asia/Gaza'],
            [296, 'PS', 'Asia/Hebron'],
            [297, 'PT', 'Europe/Lisbon'],
            [298, 'PT', 'Atlantic/Madeira'],
            [299, 'PT', 'Atlantic/Azores'],
            [300, 'PW', 'Pacific/Palau'],
            [301, 'PY', 'America/Asuncion'],
            [302, 'QA', 'Asia/Qatar'],
            [303, 'RE', 'Indian/Reunion'],
            [304, 'RO', 'Europe/Bucharest'],
            [305, 'RS', 'Europe/Belgrade'],
            [306, 'RU', 'Europe/Kaliningrad'],
            [307, 'RU', 'Europe/Moscow'],
            [308, 'RU', 'Europe/Simferopol'],
            [309, 'RU', 'Europe/Volgograd'],
            [310, 'RU', 'Europe/Samara'],
            [311, 'RU', 'Asia/Yekaterinburg'],
            [312, 'RU', 'Asia/Omsk'],
            [313, 'RU', 'Asia/Novosibirsk'],
            [314, 'RU', 'Asia/Novokuznetsk'],
            [315, 'RU', 'Asia/Krasnoyarsk'],
            [316, 'RU', 'Asia/Irkutsk'],
            [317, 'RU', 'Asia/Chita'],
            [318, 'RU', 'Asia/Yakutsk'],
            [319, 'RU', 'Asia/Khandyga'],
            [320, 'RU', 'Asia/Vladivostok'],
            [321, 'RU', 'Asia/Sakhalin'],
            [322, 'RU', 'Asia/Ust-Nera'],
            [323, 'RU', 'Asia/Magadan'],
            [324, 'RU', 'Asia/Srednekolymsk'],
            [325, 'RU', 'Asia/Kamchatka'],
            [326, 'RU', 'Asia/Anadyr'],
            [327, 'RW', 'Africa/Kigali'],
            [328, 'SA', 'Asia/Riyadh'],
            [329, 'SB', 'Pacific/Guadalcanal'],
            [330, 'SC', 'Indian/Mahe'],
            [331, 'SD', 'Africa/Khartoum'],
            [332, 'SE', 'Europe/Stockholm'],
            [333, 'SG', 'Asia/Singapore'],
            [334, 'SH', 'Atlantic/St_Helena'],
            [335, 'SI', 'Europe/Ljubljana'],
            [336, 'SJ', 'Arctic/Longyearbyen'],
            [337, 'SK', 'Europe/Bratislava'],
            [338, 'SL', 'Africa/Freetown'],
            [339, 'SM', 'Europe/San_Marino'],
            [340, 'SN', 'Africa/Dakar'],
            [341, 'SO', 'Africa/Mogadishu'],
            [342, 'SR', 'America/Paramaribo'],
            [343, 'SS', 'Africa/Juba'],
            [344, 'ST', 'Africa/Sao_Tome'],
            [345, 'SV', 'America/El_Salvador'],
            [346, 'SX', 'America/Lower_Princes'],
            [347, 'SY', 'Asia/Damascus'],
            [348, 'SZ', 'Africa/Mbabane'],
            [349, 'TC', 'America/Grand_Turk'],
            [350, 'TD', 'Africa/Ndjamena'],
            [351, 'TF', 'Indian/Kerguelen'],
            [352, 'TG', 'Africa/Lome'],
            [353, 'TH', 'Asia/Bangkok'],
            [354, 'TJ', 'Asia/Dushanbe'],
            [355, 'TK', 'Pacific/Fakaofo'],
            [356, 'TL', 'Asia/Dili'],
            [357, 'TM', 'Asia/Ashgabat'],
            [358, 'TN', 'Africa/Tunis'],
            [359, 'TO', 'Pacific/Tongatapu'],
            [360, 'TR', 'Europe/Istanbul'],
            [361, 'TT', 'America/Port_of_Spain'],
            [362, 'TV', 'Pacific/Funafuti'],
            [363, 'TW', 'Asia/Taipei'],
            [364, 'TZ', 'Africa/Dar_es_Salaam'],
            [365, 'UA', 'Europe/Kiev'],
            [366, 'UA', 'Europe/Uzhgorod'],
            [367, 'UA', 'Europe/Zaporozhye'],
            [368, 'UG', 'Africa/Kampala'],
            [369, 'UM', 'Pacific/Johnston'],
            [370, 'UM', 'Pacific/Midway'],
            [371, 'UM', 'Pacific/Wake'],
            [372, 'US', 'America/New_York'],
            [373, 'US', 'America/Detroit'],
            [374, 'US', 'America/Kentucky/Louisville'],
            [375, 'US', 'America/Kentucky/Monticello'],
            [376, 'US', 'America/Indiana/Indianapolis'],
            [377, 'US', 'America/Indiana/Vincennes'],
            [378, 'US', 'America/Indiana/Winamac'],
            [379, 'US', 'America/Indiana/Marengo'],
            [380, 'US', 'America/Indiana/Petersburg'],
            [381, 'US', 'America/Indiana/Vevay'],
            [382, 'US', 'America/Chicago'],
            [383, 'US', 'America/Indiana/Tell_City'],
            [384, 'US', 'America/Indiana/Knox'],
            [385, 'US', 'America/Menominee'],
            [386, 'US', 'America/North_Dakota/Center'],
            [387, 'US', 'America/North_Dakota/New_Salem'],
            [388, 'US', 'America/North_Dakota/Beulah'],
            [389, 'US', 'America/Denver'],
            [390, 'US', 'America/Boise'],
            [391, 'US', 'America/Phoenix'],
            [392, 'US', 'America/Los_Angeles'],
            [393, 'US', 'America/Metlakatla'],
            [394, 'US', 'America/Anchorage'],
            [395, 'US', 'America/Juneau'],
            [396, 'US', 'America/Sitka'],
            [397, 'US', 'America/Yakutat'],
            [398, 'US', 'America/Nome'],
            [399, 'US', 'America/Adak'],
            [400, 'US', 'Pacific/Honolulu'],
            [401, 'UY', 'America/Montevideo'],
            [402, 'UZ', 'Asia/Samarkand'],
            [403, 'UZ', 'Asia/Tashkent'],
            [404, 'VA', 'Europe/Vatican'],
            [405, 'VC', 'America/St_Vincent'],
            [406, 'VE', 'America/Caracas'],
            [407, 'VG', 'America/Tortola'],
            [408, 'VI', 'America/St_Thomas'],
            [409, 'VN', 'Asia/Ho_Chi_Minh'],
            [410, 'VU', 'Pacific/Efate'],
            [411, 'WF', 'Pacific/Wallis'],
            [412, 'WS', 'Pacific/Apia'],
            [413, 'YE', 'Asia/Aden'],
            [414, 'YT', 'Indian/Mayotte'],
            [415, 'ZA', 'Africa/Johannesburg'],
            [416, 'ZM', 'Africa/Lusaka'],
            [417, 'ZW', 'Africa/Harare'],
        ]);
    }

    public function safeDown()
    {
        $this->delete($this->tTimeZone);
    }
}


