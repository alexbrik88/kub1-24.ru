<?php

use common\models\service\ServiceModule;
use console\components\db\Migration;
use yii\db\Query;

class m200817_143929_alter_company_affiliate_link_add_id_custom_link extends Migration
{
    public $tableName = 'company_affiliate_link';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'product', $this->string(64));
        $this->addColumn($this->tableName, 'custom_link_id', $this->string(16));

        $links = (new Query())
            ->from($this->tableName)
            ->all();

        foreach($links as $key => $value) {
            $this->update($this->tableName,
                ['product' => ServiceModule::$serviceModuleLabel[$value['service_module_id']]],
                ['id' => $value['id']],
            );
        }
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'product');
        $this->dropColumn($this->tableName, 'custom_link_id');
    }

}
