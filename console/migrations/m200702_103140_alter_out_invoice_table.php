<?php

use console\components\db\Migration;

class m200702_103140_alter_out_invoice_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%out_invoice}}';

    /**
     * @inheritDoc`
     */
    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'notify_to_telegram', $this->boolean()->notNull()->defaultValue(false));
        $this->createIndex('ck_notify_to_telegram', self::TABLE_NAME, ['notify_to_telegram']);
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'notify_to_telegram');
    }
}
