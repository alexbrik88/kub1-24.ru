<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170315_224508_add_to_autoinvoice_payment_delay extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%autoinvoice}}', 'payment_delay', $this->integer()->defaultValue(10));
        $this->addColumn('{{%autoinvoice}}', 'date_stop', $this->integer()->null());
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%autoinvoice}}', 'payment_delay');
        $this->dropColumn('{{%autoinvoice}}', 'date_stop');
    }
}


