<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%email_deny}}`.
 */
class m210419_134914_create_email_deny_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%email_deny}}', [
            'id' => $this->primaryKey(),
            'email' => $this->text()->notNull(),
            'created_at' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%email_deny}}');
    }
}
