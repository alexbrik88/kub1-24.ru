<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210521_105111_alter_import_1c_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('import_1c', 'source_id', $this->tinyInteger()->defaultValue(1)->after('company_id'));
        $this->addColumn('import_1c', 'filename', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn('import_1c', 'source_id');
        $this->dropColumn('import_1c', 'filename');
    }
}
