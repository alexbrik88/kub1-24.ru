<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181015_080855_insert_into_registration_page_type extends Migration
{
    public $tableName = 'registration_page_type';

    public function safeUp()
    {
        $this->batchInsert($this->tableName, ['id', 'name'], [
            [2, 'Счет-фактура'],
            [3, 'Акт'],
            [4, 'ТТН'],
            [5, 'ТН'],
            [6, 'УПД'],
            [7, 'Счет-договор'],
        ]);
    }

    public function safeDown()
    {
        $this->delete($this->tableName, ['id' => [2, 3, 4, 5, 6, 7]]);
    }
}


