<?php

use console\components\db\Migration;

class m200302_182255_insales_account_company_id_modify extends Migration
{
    const TABLE = '{{%insales_account}}';

    public function up()
    {
        $this->alterColumn(self::TABLE, 'company_id', $this->integer(11)->comment('Идентификатор компании КУБ'));
    }

    public function down()
    {
        $this->alterColumn(self::TABLE, 'company_id', $this->integer(11)->notNull()->comment('Идентификатор компании КУБ'));
    }
}
