<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170809_081600_alter_invoice_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%invoice}}', 'remind_contractor', $this->smallInteger(1)->defaultValue(false) . ' AFTER [[contractor_rs]]');
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%invoice}}', 'remind_contractor');
    }
}
