<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%product_composite_map}}`.
 */
class m220216_135752_create_product_composite_map_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%product_composite_map}}', [
            'composite_product_id' => $this->integer()->notNull(),
            'simple_product_id' => $this->integer()->notNull(),
            'quantity' => $this->decimal(20, 10)->notNull(),
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', '{{%product_composite_map}}', [
            'composite_product_id',
            'simple_product_id',
        ]);

        $this->createIndex('simple_product_id', '{{%product_composite_map}}', 'simple_product_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%product_composite_map}}');
    }
}
