<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190312_143720_alter_cash_bank_flows_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cash_bank_flows', 'taxpayers_status_id', $this->integer()->after('is_taxable'));
        $this->addColumn('cash_bank_flows', 'oktmo_code', $this->string()->after('kbk'));
        $this->addColumn('cash_bank_flows', 'payment_details_id', $this->integer()->after('oktmo_code'));
        $this->addColumn('cash_bank_flows', 'document_number_budget_payment', $this->string()->after('tax_period_code'));
        $this->addColumn('cash_bank_flows', 'document_date_budget_payment', $this->date()->after('document_number_budget_payment'));
        $this->addColumn('cash_bank_flows', 'payment_type_id', $this->integer()->after('document_date_budget_payment'));
        $this->addColumn('cash_bank_flows', 'uin_code', $this->string()->after('payment_type_id'));

        $this->addForeignKey('fk_cash_bank_flows_taxpayers_status', 'cash_bank_flows', 'taxpayers_status_id', 'taxpayers_status', 'id');
        $this->addForeignKey('fk_cash_bank_flows_payment_details', 'cash_bank_flows', 'payment_details_id', 'payment_details', 'id');
        $this->addForeignKey('fk_cash_bank_flows_payment_type', 'cash_bank_flows', 'payment_type_id', 'payment_type', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_cash_bank_flows_taxpayers_status', 'cash_bank_flows');
        $this->dropForeignKey('fk_cash_bank_flows_payment_details', 'cash_bank_flows');
        $this->dropForeignKey('fk_cash_bank_flows_payment_type', 'cash_bank_flows');

        $this->dropColumn('cash_bank_flows', 'taxpayers_status_id');
        $this->dropColumn('cash_bank_flows', 'oktmo_code');
        $this->dropColumn('cash_bank_flows', 'payment_details_id');
        $this->dropColumn('cash_bank_flows', 'document_number_budget_payment');
        $this->dropColumn('cash_bank_flows', 'document_date_budget_payment');
        $this->dropColumn('cash_bank_flows', 'payment_type_id');
        $this->dropColumn('cash_bank_flows', 'uin_code');
    }
}
