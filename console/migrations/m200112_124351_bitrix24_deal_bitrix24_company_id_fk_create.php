<?php

use console\components\db\Migration;

class m200112_124351_bitrix24_deal_bitrix24_company_id_fk_create extends Migration
{
    const TABLE = '{{%bitrix24_deal}}';

    public function safeUp()
    {
        $this->addForeignKey(
            'bitrix24_deal_bitrix24_company_id', self::TABLE, ['company_id', 'bitrix24_company_id'],
            '{{%bitrix24_company}}', ['company_id', 'id'],
            'CASCADE', 'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('bitrix24_deal_bitrix24_company_id', self::TABLE);
    }
}
