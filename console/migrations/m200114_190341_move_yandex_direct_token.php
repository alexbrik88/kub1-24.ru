<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200114_190341_move_yandex_direct_token extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('{{%employee}}', 'yandex_access_token');
    }

    public function safeDown()
    {
        $this->addColumn('{{%employee}}', 'yandex_access_token', $this->string()->defaultValue(null));
    }
}
