<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180531_063935_alter_contractor_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('contractor', 'not_accounting', $this->boolean()->notNull()->defaultValue(false));

        $this->addCommentOnColumn(
            'contractor',
            'not_accounting',
            'все документы по этому клиенты мы не учитываем в выгрузке для 1с и не показываем никому кроме руководителя'
        );
    }

    public function safeDown()
    {
        $this->dropColumn('contractor', 'not_accounting');
    }
}
