<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180411_103317_create_service_store_tariff extends Migration
{
    public $tableName = 'service_store_tariff';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'cabinets_count' => $this->integer()->notNull(),
            'cost_one_month_for_one_cabinet' => $this->integer()->notNull(),
            'total_amount' => $this->integer()->notNull(),
        ]);

        $this->batchInsert($this->tableName, ['id', 'cabinets_count', 'cost_one_month_for_one_cabinet', 'total_amount'], [
            [1, 1, 250, 3000],
            [2, 2, 250, 6000],
            [3, 5, 200, 12000],
            [4, 10, 150, 18000],
            [5, 20, 120, 28800],
            [6, 50, 100, 60000],
            [7, 100, 90, 108000],
        ]);
    }

    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}


