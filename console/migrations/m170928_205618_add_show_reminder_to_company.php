<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170928_205618_add_show_reminder_to_company extends Migration
{
    public $tableName = 'company';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'show_reminder_7', $this->boolean()->defaultValue(true));
        $this->addColumn($this->tableName, 'show_reminder_5', $this->boolean()->defaultValue(true));
        $this->addColumn($this->tableName, 'show_reminder_3', $this->boolean()->defaultValue(true));
        $this->addColumn($this->tableName, 'show_reminder_1', $this->boolean()->defaultValue(true));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'show_reminder_7');
        $this->dropColumn($this->tableName, 'show_reminder_5');
        $this->dropColumn($this->tableName, 'show_reminder_3');
        $this->dropColumn($this->tableName, 'show_reminder_1');
    }
}


