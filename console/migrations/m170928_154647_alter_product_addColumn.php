<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170928_154647_alter_product_addColumn extends Migration
{
    public function safeUp()
    {
		$this->addColumn('{{product}}', 'item_type_code', $this->string(45)->after('code'));
    }
    
    public function safeDown()
    {
    	$this->dropColumn('{{product}}', 'item_type_code');
    }
}
