<?php

use yii\db\Schema;
use yii\db\Migration;

class m150713_075556_alter_employee_addColumn_isDeleted extends Migration
{
    public function safeUp()
    {
        $this->addColumn('employee', 'is_deleted', Schema::TYPE_BOOLEAN . ' NOT NULL');
        $this->update('employee', [
            'is_deleted' => 0,
        ]);
    }
    
    public function safeDown()
    {
        $this->dropColumn('employee', 'is_deleted');
    }
}
