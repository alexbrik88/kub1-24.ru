<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211021_101328_insert_service_invoice_tariff extends Migration
{
    public function safeUp()
    {
        $this->batchInsert('{{%service_invoice_tariff}}', [
            'id',
            'invoice_count',
            'total_amount',
        ], [
            [4, 1, 200],
            [5, 3, 360],
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%service_invoice_tariff}}', ['id' => [4, 5]]);
    }
}
