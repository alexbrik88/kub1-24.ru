<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210520_083530_alter_checking_accountant_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%checking_accountant}}', 'name', $this->string()->after('is_foreign_bank'));
        $this->update('{{%checking_accountant}}', [
            'name' => new yii\db\Expression('IF(LENGTH(IFNULL([[bank_name]], "")) > 0, [[bank_name]], [[bank_name_en]])'),
        ]);
    }

    public function safeDown()
    {
        $this->dropColumn('{{%checking_accountant}}', 'name');
    }
}
