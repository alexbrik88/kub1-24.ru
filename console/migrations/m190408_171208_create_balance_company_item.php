<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190408_171208_create_balance_company_item extends Migration
{
    public $tableName = '{{%balance_company_item}}';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'company_id' => $this->integer()->notNull(),
            'item_id' => $this->integer()->notNull(),
            'sort' => $this->integer()->notNull(),
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', $this->tableName, ['company_id', 'item_id']);
        $this->addForeignKey('balance_company_item_company_id', $this->tableName, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('balance_company_item_item_id', $this->tableName, 'item_id', 'balance_item', 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
