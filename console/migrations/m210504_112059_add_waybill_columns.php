<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210504_112059_add_waybill_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('waybill', 'consignment_short_description', $this->string());
        $this->addColumn('waybill', 'consignment_packaging_type', $this->string());
        $this->addColumn('waybill', 'consignment_places_count', $this->string());
        $this->addColumn('waybill', 'consignment_code', $this->string());
        $this->addColumn('waybill', 'consignment_container_number', $this->string());
        $this->addColumn('waybill', 'consignment_classification', $this->string());
        $this->addColumn('waybill', 'consignment_gross_weight', $this->float(2));
        $this->addColumn('waybill', 'loading_contractor', $this->string());
        $this->addColumn('waybill', 'unloading_contractor', $this->string());
        $this->addColumn('waybill', 'completed_per_ton', $this->string());
        $this->addColumn('waybill', 'completed_per_ton_km', $this->string());
        $this->addColumn('waybill', 'completed_loading_and_unloading_works', $this->string());
        $this->addColumn('waybill', 'completed_underload', $this->string());
        $this->addColumn('waybill', 'completed_forwarding', $this->string());
        $this->addColumn('waybill', 'completed_excess_simple_loading', $this->string());
        $this->addColumn('waybill', 'completed_excess_simple_unloading', $this->string());
        $this->addColumn('waybill', 'completed_urgency', $this->string());
        $this->addColumn('waybill', 'completed_special_transport', $this->string());
        $this->addColumn('waybill', 'completed_another_surcharge', $this->string());
        $this->addColumn('waybill', 'completed_total', $this->string());
        $this->addColumn('waybill', 'rate_per_ton', $this->string());
        $this->addColumn('waybill', 'rate_per_ton_km', $this->string());
        $this->addColumn('waybill', 'rate_loading_and_unloading_works', $this->string());
        $this->addColumn('waybill', 'rate_underload', $this->string());
        $this->addColumn('waybill', 'rate_forwarding', $this->string());
        $this->addColumn('waybill', 'rate_excess_simple_loading', $this->string());
        $this->addColumn('waybill', 'rate_excess_simple_unloading', $this->string());
        $this->addColumn('waybill', 'rate_urgency', $this->string());
        $this->addColumn('waybill', 'rate_special_transport', $this->string());
        $this->addColumn('waybill', 'rate_another_surcharge', $this->string());
        $this->addColumn('waybill', 'rate_total', $this->string());
        $this->addColumn('waybill', 'to_pay_per_ton', $this->string());
        $this->addColumn('waybill', 'to_pay_per_ton_km', $this->string());
        $this->addColumn('waybill', 'to_pay_loading_and_unloading_works', $this->string());
        $this->addColumn('waybill', 'to_pay_underload', $this->string());
        $this->addColumn('waybill', 'to_pay_forwarding', $this->string());
        $this->addColumn('waybill', 'to_pay_excess_simple_loading', $this->string());
        $this->addColumn('waybill', 'to_pay_excess_simple_unloading', $this->string());
        $this->addColumn('waybill', 'to_pay_urgency', $this->string());
        $this->addColumn('waybill', 'to_pay_special_transport', $this->string());
        $this->addColumn('waybill', 'to_pay_another_surcharge', $this->string());
        $this->addColumn('waybill', 'to_pay_total', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn('waybill', 'consignment_short_description');
        $this->dropColumn('waybill', 'consignment_packaging_type');
        $this->dropColumn('waybill', 'consignment_places_count');
        $this->dropColumn('waybill', 'consignment_code');
        $this->dropColumn('waybill', 'consignment_container_number');
        $this->dropColumn('waybill', 'consignment_classification');
        $this->dropColumn('waybill', 'consignment_gross_weight');
        $this->dropColumn('waybill', 'loading_contractor');
        $this->dropColumn('waybill', 'unloading_contractor');
        $this->dropColumn('waybill', 'completed_per_ton');
        $this->dropColumn('waybill', 'completed_per_ton_km');
        $this->dropColumn('waybill', 'completed_loading_and_unloading_works');
        $this->dropColumn('waybill', 'completed_underload');
        $this->dropColumn('waybill', 'completed_forwarding');
        $this->dropColumn('waybill', 'completed_excess_simple_loading');
        $this->dropColumn('waybill', 'completed_excess_simple_unloading');
        $this->dropColumn('waybill', 'completed_urgency');
        $this->dropColumn('waybill', 'completed_special_transport');
        $this->dropColumn('waybill', 'completed_another_surcharge');
        $this->dropColumn('waybill', 'completed_total');
        $this->dropColumn('waybill', 'rate_per_ton');
        $this->dropColumn('waybill', 'rate_per_ton_km');
        $this->dropColumn('waybill', 'rate_loading_and_unloading_works');
        $this->dropColumn('waybill', 'rate_underload');
        $this->dropColumn('waybill', 'rate_forwarding');
        $this->dropColumn('waybill', 'rate_excess_simple_loading');
        $this->dropColumn('waybill', 'rate_excess_simple_unloading');
        $this->dropColumn('waybill', 'rate_urgency');
        $this->dropColumn('waybill', 'rate_special_transport');
        $this->dropColumn('waybill', 'rate_another_surcharge');
        $this->dropColumn('waybill', 'rate_total');
        $this->dropColumn('waybill', 'to_pay_per_ton');
        $this->dropColumn('waybill', 'to_pay_per_ton_km');
        $this->dropColumn('waybill', 'to_pay_loading_and_unloading_works');
        $this->dropColumn('waybill', 'to_pay_underload');
        $this->dropColumn('waybill', 'to_pay_forwarding');
        $this->dropColumn('waybill', 'to_pay_excess_simple_loading');
        $this->dropColumn('waybill', 'to_pay_excess_simple_unloading');
        $this->dropColumn('waybill', 'to_pay_urgency');
        $this->dropColumn('waybill', 'to_pay_special_transport');
        $this->dropColumn('waybill', 'to_pay_another_surcharge');
        $this->dropColumn('waybill', 'to_pay_total');
    }
}
