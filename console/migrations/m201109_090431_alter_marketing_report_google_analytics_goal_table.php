<?php

use console\components\db\Migration;
use yii\db\Exception;

class m201109_090431_alter_marketing_report_google_analytics_goal_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%marketing_report_google_analytics_goal}}';

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function up()
    {
        $this->db->createCommand("
            DELETE t1 FROM marketing_report_google_analytics_goal t1
            INNER JOIN marketing_report_google_analytics_goal t2
            WHERE t1.id < t2.id
            AND t1.company_id = t2.company_id
            AND t1.source_type = t2.source_type
            AND t1.date = t2.date
            AND t1.utm_source = t2.utm_source
            AND t1.utm_medium = t2.utm_medium
            AND t1.utm_campaign = t2.utm_campaign
            AND t1.goal_id = t2.goal_id
        ")->execute();

        $this->createIndex(
            'uk_marketing_report_google_analytics_goal',
            self::TABLE_NAME,
            ['company_id', 'source_type', 'date', 'utm_source', 'utm_medium', 'utm_campaign', 'goal_id'],
            true
        );
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        $this->dropIndex('uk_marketing_report_google_analytics_goal', self::TABLE_NAME);
    }
}
