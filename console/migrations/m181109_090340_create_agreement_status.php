<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181109_090340_create_agreement_status extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('agreement_status', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ], $tableOptions);

        $this->batchInsert('agreement_status', ['id', 'name',], [
            [1, 'Создан',],
            [2, 'Распечатан',],
            [3, 'Передан',],
            [4, 'Получен',],
            [5, 'Отменен',],
            [6, 'Окончен',],
        ]);
    }
    
    public function safeDown()
    {

    }
}


