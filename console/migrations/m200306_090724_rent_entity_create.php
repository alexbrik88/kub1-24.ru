<?php

use console\components\db\Migration;
use common\models\rent\Entity;

class m200306_090724_rent_entity_create extends Migration
{
    const TABLE = '{{%rent_entity}}';

    public function up()
    {
        $this->createTable(self::TABLE, [
            'id' => $this->primaryKey()->unsigned()->notNull(),
            'company_id' => $this->integer(11)->notNull()->comment('ID компании'),
            'category_id' => $this->smallInteger()->unsigned()->notNull()->comment('ID категории'),
            'product_id' => $this->integer(11)->comment('ID товара (услуги) для счёта'),
            'title' => $this->string(150)->notNull()->comment('Название объекта'),
            'ownership' => "ENUM('" . implode("','", Entity::OWNERSHIP_TYPE) . "') NOT NULL DEFAULT '" . Entity::OWNERSHIP_TYPE_SELF . "' COMMENT 'Тип собственности'",
            'archive' => $this->boolean()->notNull()->defaultValue(false)->comment('Признак, что объект находится в архиве'),
            'description' => $this->text()->comment('Описание'),
            'comment' => $this->text()->comment('Комментарий')
        ], "COMMENT 'Учёт аренды техники: объекты аренды'");
        $this->addForeignKey(
            'rent_entity_company_id', self::TABLE, 'company_id',
            '{{%company}}', 'id', 'CASCADE', 'CASCADE'
        );
        $this->addForeignKey(
            'rent_entity_category_id', self::TABLE, 'category_id',
            '{{%rent_category}}', 'id', 'CASCADE', 'CASCADE'
        );
        $this->addForeignKey(
            'rent_entity_product_id', self::TABLE, 'product_id',
            '{{%product}}', 'id', 'SET NULL', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable(self::TABLE);
    }
}
