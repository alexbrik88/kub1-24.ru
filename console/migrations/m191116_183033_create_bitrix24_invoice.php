<?php

use console\components\db\Migration;

class m191116_183033_create_bitrix24_invoice extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{bitrix24_invoice}}', [
            'id' => $this->integer()->unsigned()->notNull()->comment('Идентификатор счёта Битрикс24'),
            'employee_id' => $this->integer()->notNull()->comment('ID клиента КУБ'),
            'deal_id' => $this->integer()->unsigned()->comment('Идентификатор сделки'),
            'company_id' => $this->integer()->unsigned()->comment('Идентификатор компании'),
            'contact_id' => $this->integer()->unsigned()->comment('Идентификатор контакта'),
            'number' => $this->string(30)->notNull()->comment('Номер счёта'),
            'status' => $this->string(50)->notNull()->comment('Статус'),
            'amount' => $this->float()->unsigned()->notNull()->defaultValue(0)->comment('Сумма'),
            'currency' => $this->char(3)->notNull()->defaultValue('RUB')->comment('Валюта'),
            'tax' => $this->float()->unsigned()->notNull()->defaultValue(0)->comment('Сумма налога'),
            'comment' => $this->string(1000),
            'comment_manager' => $this->string(1000),
            'date_paid' => $this->integer()->unsigned()->comment('Дата и время оплаты'),
            'created_at' => $this->integer()->unsigned()->notNull()->comment('Дата и время создания'),
            'updated_at' => $this->integer()->unsigned()->notNull()->comment('Дата и время последнего обновления'),
        ], "COMMENT 'Интеграция Битрикс24: счета'");
        $this->addPrimaryKey('bitrix24_inoice_id', '{{bitrix24_invoice}}', ['employee_id', 'id']);
        $this->addForeignKey(
            'bitrix24_invoice_employee_id', '{{bitrix24_invoice}}', 'employee_id',
            '{{employee}}', 'id', 'CASCADE', 'CASCADE'
        );
        $this->addForeignKey(
            'bitrix24_invoice_deal_id', '{{bitrix24_invoice}}', ['employee_id','deal_id'],
            '{{bitrix24_deal}}', ['employee_id','id'], 'CASCADE', 'CASCADE'
        );
        $this->addForeignKey(
            'bitrix24_invoice_company_id', '{{bitrix24_invoice}}', ['employee_id','company_id'],
            '{{bitrix24_company}}', ['employee_id','id'], 'CASCADE', 'CASCADE'
        );
        $this->addForeignKey(
            'bitrix24_invoice_contact_id', '{{bitrix24_invoice}}', ['employee_id','contact_id'],
            '{{bitrix24_contact}}', ['employee_id','id'], 'CASCADE', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable('{{bitrix24_invoice}}');
    }
}
