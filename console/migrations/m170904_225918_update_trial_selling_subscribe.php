<?php

use console\components\db\Migration;
use yii\db\Schema;
use common\models\selling\SellingSubscribe;
use common\models\service\Subscribe;
use common\models\service\SubscribeTariff;

class m170904_225918_update_trial_selling_subscribe extends Migration
{
    public $tableName = 'selling_subscribe';

    public function safeUp()
    {

        $this->delete($this->tableName, ['in', 'id', SellingSubscribe::find()->andWhere(['type' => SellingSubscribe::TYPE_TRIAL])->column()]);

        foreach (Subscribe::find()
                     ->andWhere(['tariff_id' => SubscribeTariff::TARIFF_TRIAL])
                     ->all() as $key => $subscribe) {
            $sellingSubscribe = new SellingSubscribe();
            $sellingSubscribe->company_id = $subscribe->company_id;
            $sellingSubscribe->subscribe_id = $subscribe->id;
            $sellingSubscribe->type = SellingSubscribe::TYPE_TRIAL;
            $sellingSubscribe->main_type = SellingSubscribe::TYPE_MAIN_FREE;
            $sellingSubscribe->invoice_number = null;
            $sellingSubscribe->creation_date = $subscribe->created_at;
            $sellingSubscribe->activation_date = $subscribe->activated_at;
            $sellingSubscribe->end_date = $subscribe->expired_at;
            $sellingSubscribe->save();
        }
    }
    
    public function safeDown()
    {

    }
}


