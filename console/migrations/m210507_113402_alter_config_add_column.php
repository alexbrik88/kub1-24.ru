<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210507_113402_alter_config_add_column extends Migration
{
    public $table = 'user_config';

    public $columns = [
        'bank_column_income_expense' => 0,
        'bank_column_project' => 0,
        'bank_column_paying' => 1,

        'order_column_income_expense' => 0,
        'order_column_project' => 0,
        'order_column_paying' => 1,

        'emoney_column_income_expense' => 0,
        'emoney_column_project' => 0,
        'emoney_column_paying' => 1,

        'card_column_income_expense' => 0,
        'card_column_project' => 0,

        'acquiring_column_income_expense' => 0,
        'acquiring_column_project' => 0,
    ];

    public function safeUp()
    {
        foreach ($this->columns as $column => $default)
            $this->addColumn($this->table, $column, $this->tinyInteger()->notNull()->defaultValue($default));
    }

    public function safeDown()
    {
        foreach ($this->columns as $column => $default)
            $this->dropColumn($this->table, $column);
    }
}