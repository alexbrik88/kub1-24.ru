<?php

use console\components\db\Migration;
use yii\db\Schema;

class m220124_151055_alter_cash_order_flows_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cash_order_flows}}', 'linkage_id', $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%cash_order_flows}}', 'linkage_id');
    }
}
