<?php

use console\components\db\Migration;
use yii\db\Schema;

class m220113_075750_alter_config_add_column extends Migration
{
    public function safeUp()
    {
        $table = 'user_config';
        $type = $this->boolean()->defaultValue(1);
        $this->addColumn($table, 'balance_article_purchased_at', $type);
        $this->addColumn($table, 'balance_article_category', $type);
        $this->addColumn($table, 'balance_article_amount', $type);
        $this->addColumn($table, 'balance_article_useful_life_in_month', $type);
        $this->addColumn($table, 'balance_article_month_in_use', $type);
        $this->addColumn($table, 'balance_article_month_left', $type);
        $this->addColumn($table, 'balance_article_write_off_date', $type);
        $this->addColumn($table, 'balance_article_status', $type);
    }

    public function safeDown()
    {
        $table = 'user_config';
        $this->dropColumn($table, 'balance_article_purchased_at');
        $this->dropColumn($table, 'balance_article_category');
        $this->dropColumn($table, 'balance_article_amount');
        $this->dropColumn($table, 'balance_article_useful_life_in_month');
        $this->dropColumn($table, 'balance_article_month_in_use');
        $this->dropColumn($table, 'balance_article_month_left');
        $this->dropColumn($table, 'balance_article_write_off_date');
        $this->dropColumn($table, 'balance_article_status');
    }
}
