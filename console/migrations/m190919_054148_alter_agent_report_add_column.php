<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190919_054148_alter_agent_report_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%agent_report}}', 'print_id', $this->integer()->after('signature_id'));
        $this->addColumn('{{%agent_report}}', 'chief_signature_id', $this->integer()->after('print_id'));
        $this->addColumn('{{%agent_report}}', 'chief_accountant_signature_id', $this->integer()->after('chief_signature_id'));

        $this->addForeignKey(
            'FK_agent_report_company_details_print',
            '{{%agent_report}}',
            'print_id',
            '{{%company_details_file}}',
            'id'
        );

        $this->addForeignKey(
            'FK_agent_report_company_details_chief_signature',
            '{{%agent_report}}',
            'chief_signature_id',
            '{{%company_details_file}}',
            'id'
        );

        $this->addForeignKey(
            'FK_agent_report_company_details_chief_accountant_signature',
            '{{%agent_report}}',
            'chief_accountant_signature_id',
            '{{%company_details_file}}',
            'id'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_agent_report_company_details_print', '{{%agent_report}}');
        $this->dropForeignKey('FK_agent_report_company_details_chief_signature', '{{%agent_report}}');
        $this->dropForeignKey('FK_agent_report_company_details_chief_accountant_signature', '{{%agent_report}}' );

        $this->dropColumn('{{%agent_report}}', 'print_id');
        $this->dropColumn('{{%agent_report}}', 'chief_signature_id');
        $this->dropColumn('{{%agent_report}}', 'chief_accountant_signature_id');
    }
}
