<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200708_080158_drop_table_project_contractor extends Migration
{
    public $table = 'project_contractor';

    public function safeUp()
    {
        try {
            $this->dropForeignKey('project_contractor_project_id', $this->table);
            $this->dropForeignKey('project_contractor_contractor_id', $this->table);
        } catch (\Exception $e) {
            echo 'Drop foreign keys fails';
        }
        $this->dropTable($this->table);
    }

    public function safeDown()
    {
        $this->createTable($this->table, [
            'id' => 'pk',
            'project_id' => 'integer',
            'contractor_id' => 'integer',
        ]);
    }
}
