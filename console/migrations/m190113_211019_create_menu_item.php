<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190113_211019_create_menu_item extends Migration
{
    public $tableName = 'menu_item';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->unique()->notNull(),
            'invoice_item' => $this->boolean()->defaultValue(false),
            'b2b_item' => $this->boolean()->defaultValue(false),
            'analytics_item' => $this->boolean()->defaultValue(false),
            'logistics_item' => $this->boolean()->defaultValue(false),
            'accountant_item' => $this->boolean()->defaultValue(false),
            'project_item' => $this->boolean()->defaultValue(false),
            'product_item' => $this->boolean()->defaultValue(false),
        ]);

        $this->addForeignKey($this->tableName . '_company_id', $this->tableName, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');

        $this->execute("
            INSERT INTO 
                {{%menu_item}} (
                	[[company_id]],
                	[[invoice_item]],
                	[[accountant_item]]
                )
            SELECT
                {{%company}}.[[id]],
                IF({{%company}}.[[registration_page_type_id]] = 10, 0, 1),
                IF({{%company}}.[[registration_page_type_id]] = 10, 1, 0)
            FROM 
                {{%company}}
        ");
    }

    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_company_id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}
