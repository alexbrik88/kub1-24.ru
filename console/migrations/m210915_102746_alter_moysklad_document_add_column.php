<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210915_102746_alter_moysklad_document_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('moysklad_document', 'document_amount', $this->char(32)->after('document_number'));
    }

    public function safeDown()
    {
        $this->dropColumn('moysklad_document', 'document_amount');
    }
}
