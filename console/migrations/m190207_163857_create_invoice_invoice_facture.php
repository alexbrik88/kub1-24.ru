<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190207_163857_create_invoice_invoice_facture extends Migration
{
    public $tableName = '{{%invoice_invoice_facture}}';

    public function safeUp()
    {
        $this->dropForeignKey('fk_invoice_facture_to_invoice', '{{%invoice_facture}}');
        $this->alterColumn('{{%invoice_facture}}', 'invoice_id', $this->integer()->defaultValue(null));
        $this->addForeignKey('fk_invoice_facture_to_invoice', '{{%invoice_facture}}', 'invoice_id', '{{%invoice}}', 'id', 'RESTRICT', 'CASCADE');

        $this->createTable($this->tableName, [
            'invoice_id' => $this->integer()->notNull(),
            'invoice_facture_id' => $this->integer()->notNull(),
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', $this->tableName, ['invoice_id', 'invoice_facture_id']);
        $this->addForeignKey('invoice_invoice_facture_to_invoice', $this->tableName, 'invoice_id', '{{%invoice}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('invoice_invoice_facture_to_invoice_facture', $this->tableName, 'invoice_facture_id', '{{%invoice_facture}}', 'id', 'CASCADE', 'CASCADE');

        $this->execute("
            INSERT INTO {{%invoice_invoice_facture}} (invoice_id, invoice_facture_id)
            SELECT {{%invoice_facture}}.[[invoice_id]], {{%invoice_facture}}.[[id]]
            FROM {{%invoice_facture}}
            LEFT JOIN {{%invoice}}
                ON {{%invoice_facture}}.[[invoice_id]] = {{%invoice}}.[[id]]
			WHERE {{%invoice}}.[[id]] IS NOT NULL
            AND {{%invoice_facture}}.[[invoice_id]] IS NOT NULL;
        ");
    }

    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
