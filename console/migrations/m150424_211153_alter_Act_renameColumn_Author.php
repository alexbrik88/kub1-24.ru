<?php

use yii\db\Schema;
use yii\db\Migration;

class m150424_211153_alter_Act_renameColumn_Author extends Migration
{
    public function up()
    {
        $this->dropForeignKey('act_to_employee_author', 'act');
        $this->renameColumn('act', 'author', 'author_id');
        $this->addForeignKey('act_to_employee_author', 'act', 'author_id', 'employee', 'id');

        $this->dropForeignKey('act_to_employee_author_status', 'act');
        $this->renameColumn('act', 'status_author', 'status_author_id');
        $this->addForeignKey('act_to_employee_author_status', 'act', 'status_author_id', 'employee', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('act_to_employee_author', 'act');
        $this->renameColumn('act', 'author_id', 'author');
        $this->addForeignKey('act_to_employee_author', 'act', 'author', 'employee', 'id');

        $this->dropForeignKey('act_to_employee_author_status', 'act');
        $this->renameColumn('act', 'status_author_id', 'status_author');
        $this->addForeignKey('act_to_employee_author_status', 'act', 'status_author', 'employee', 'id');
    }
}
