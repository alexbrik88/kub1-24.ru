<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211006_065156_create_marketing_user_config extends Migration
{
    public $table = 'marketing_user_config';

    // chart type:
    // 1 - CTR
    // 2 - Клики
    // 3 - CPC
    // 4 - Конверсия
    // 5 - Лиды
    // 6 - CPL
    // 7 - Расходы

    public function safeUp()
    {
        $table = $this->table;
        $this->createTable($table, [
            'employee_id' => $this->primaryKey(),
            'statistics_chart_type_1' => $this->tinyInteger()->notNull()->defaultValue(2),
            'statistics_chart_type_2' => $this->tinyInteger()->notNull()->defaultValue(5),
            'statistics_chart_type_3' => $this->tinyInteger()->notNull()->defaultValue(7),
            'dynamics_chart_type' =>  $this->tinyInteger()->notNull()->defaultValue(1),
            'dynamics_chart_period' => $this->tinyInteger()->notNull()->defaultValue(1),
        ]);

        $this->addForeignKey("FK_{$table}_employee", $table, 'employee_id', 'employee', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $table = $this->table;
        $this->dropForeignKey("FK_{$table}_employee", $table);
        $this->dropTable($table);
    }
}
