<?php

use yii\db\Schema;
use yii\db\Migration;

class m151015_094446_create_file extends Migration
{
    public $tFile = 'file';
    public $tEmployee = 'employee';

    public function safeUp()
    {
        $this->createTable($this->tFile, [
            'id' => $this->primaryKey(),
            'filename' => $this->string(1023)->notNull(),
            'ext' => $this->string(255),
            'filename_full' => $this->string(1278)->notNull(),
            'hash' => $this->string(255)->notNull(),
            'created_at' => $this->integer()->notNull(),
            'created_at_author_id' => $this->integer()->notNull(),
            'owner_model' => $this->string(255)->notNull(),
            'owner_id' => $this->integer()->notNull(),
        ]);

        $this->createIndex('owner_model_idx', $this->tFile, ['owner_model', 'owner_id']);
        $this->addForeignKey('fk_', $this->tFile, 'created_at_author_id', $this->tEmployee, 'id');
    }
    
    public function safeDown()
    {
        $this->dropTable($this->tFile);
    }
}
