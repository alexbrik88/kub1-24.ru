<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200311_211434_create_vk_ads_tables extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%vk_ads_campaign}}', [
            'id' => $this->bigInteger(100)->unsigned()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'type' => $this->string()->notNull(),
            'name' => $this->string()->notNull(),
            'status' => $this->tinyInteger(3)->unsigned()->notNull(),
            'day_limit' => $this->integer(11)->unsigned()->defaultValue(null),
            'all_limit' => $this->integer(11)->unsigned()->defaultValue(null),
            'start_time' => $this->integer(11)->unsigned()->defaultValue(null),
            'stop_time' => $this->integer(11)->unsigned()->defaultValue(null),
        ]);
        $this->addPrimaryKey('id_company', '{{%vk_ads_campaign}}', ['id', 'company_id']);
        $this->addForeignKey('FK_vk_ads_campaign_to_company', 'vk_ads_campaign', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('{{%vk_ads_ad}}', [
            'id' => $this->bigInteger(100)->unsigned()->notNull(),
            'campaign_id' => $this->bigInteger(100)->unsigned()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'format' => $this->tinyInteger(3)->unsigned()->notNull(),
            'cost_type' => $this->tinyInteger(3)->unsigned()->notNull(),
            'cpc' => $this->integer(11)->unsigned()->defaultValue(null),
            'cpm' => $this->integer(11)->unsigned()->defaultValue(null),
            'ocpm' => $this->integer(11)->unsigned()->defaultValue(null),
            'goal_type' => $this->tinyInteger(3)->unsigned()->notNull(),
            'impressions_limit' => $this->integer(11)->unsigned()->defaultValue(null),
            'is_impressions_limited' => $this->boolean()->unsigned(),
            'ad_platform' => $this->string()->defaultValue(null),
            'is_ad_platform_no_wall' => $this->boolean()->unsigned(),
            'is_ad_platform_no_ad_network' => $this->boolean()->unsigned(),
            'all_limit' => $this->integer(11)->unsigned()->defaultValue(null),
            'day_limit' => $this->integer(11)->unsigned()->defaultValue(null),
            'autobidding_max_cost' => $this->integer(11)->unsigned()->defaultValue(null),
            'status' => $this->tinyInteger(3)->unsigned()->notNull(),
            'name' => $this->string()->notNull(),
            'approved' => $this->tinyInteger(3)->unsigned()->notNull(),
            'is_video' => $this->boolean()->unsigned(),
        ]);
        $this->addPrimaryKey('id_campaign_company', '{{%vk_ads_ad}}', ['id', 'campaign_id', 'company_id']);
        $this->addForeignKey('FK_vk_ads_ad_to_vk_ads_campaign', 'vk_ads_ad', ['campaign_id', 'company_id'], 'vk_ads_campaign', ['id', 'company_id'], 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_vk_ads_ad_to_company', 'vk_ads_ad', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('{{%vk_ads_report}}', [
            'id' => $this->primaryKey(11)->unsigned()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'ad_id' => $this->bigInteger(100)->unsigned()->notNull(),
            'campaign_id' => $this->bigInteger(100)->unsigned()->notNull(),
            'date' => $this->date()->notNull(),
            'spent_amount' => $this->decimal(10, 2)->unsigned()->notNull(),
            'impressions_count' => $this->integer(11)->unsigned()->notNull(),
            'clicks_count' => $this->integer(11)->unsigned()->notNull(),
            'reach' => $this->integer(11)->unsigned()->notNull(),
        ]);
        $this->addForeignKey('FK_vk_ads_report_to_company', 'vk_ads_report', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_vk_ads_report_to_vk_ads_campaign', 'vk_ads_report', 'campaign_id', 'vk_ads_campaign', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_vk_ads_report_to_vk_ads_ad', 'vk_ads_report', 'ad_id', 'vk_ads_ad', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_vk_ads_report_to_vk_ads_campaign', 'vk_ads_report');
        $this->dropForeignKey('FK_vk_ads_report_to_vk_ads_ad', 'vk_ads_report');
        $this->dropTable('{{%vk_ads_report}}');

        $this->dropForeignKey('FK_vk_ads_ad_to_vk_ads_campaign', 'vk_ads_ad');
        $this->dropTable('{{%vk_ads_ad}}');

        $this->dropTable('{{%vk_ads_campaign}}');
    }
}
