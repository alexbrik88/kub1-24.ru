<?php

use console\components\db\Migration;
use yii\db\Schema;
use common\models\service\Payment;
use common\models\service\SubscribeTariff;
use common\models\service\PaymentType;
use common\models\Company;

class m170826_120743_add_serial_number_to_payment extends Migration
{
    public $tableName = 'service_payment';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'serial_number', $this->integer()->defaultValue(null));
        /* @var $company Company */
        foreach (Company::find()->joinWith('payments')
                     ->isTest(!Company::TEST_COMPANY)
                     ->isBlocked(Company::UNBLOCKED)
                     ->andWhere(['and',
                         [Payment::tableName() . '.is_confirmed' => 1],
                         ['in', Payment::tableName() . '.tariff_id', [SubscribeTariff::TARIFF_1, SubscribeTariff::TARIFF_2, SubscribeTariff::TARIFF_3]],
                         ['not', [Payment::tableName() . '.type_id' => PaymentType::TYPE_PROMO_CODE]],
                     ])
                     ->andWhere(['not', [Payment::tableName() . '.id' => null]])
                     ->all() as $company) {
            /* @var $payment Payment */
            foreach ($company->getPayments()
                         ->andWhere(['and',
                             [Payment::tableName() . '.is_confirmed' => 1],
                             ['in', Payment::tableName() . '.tariff_id', [SubscribeTariff::TARIFF_1, SubscribeTariff::TARIFF_2, SubscribeTariff::TARIFF_3]],
                             ['not', [Payment::tableName() . '.type_id' => PaymentType::TYPE_PROMO_CODE]],
                         ])
                         ->orderBy(['created_at' => SORT_ASC])->all() as $key => $payment) {
                $this->update($this->tableName, ['serial_number' => $key + 1], ['id' => $payment->id]);
            }
        }
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'serial_number');
    }
}


