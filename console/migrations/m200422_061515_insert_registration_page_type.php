<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200422_061515_insert_registration_page_type extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%registration_page_type}}', [
            'id' => 29,
            'name' => 'Эвотор',
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%registration_page_type}}', [
            'id' => 29,
        ]);
    }
}
