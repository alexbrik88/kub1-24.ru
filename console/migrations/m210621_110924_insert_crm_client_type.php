<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210621_110924_insert_crm_client_type extends Migration
{
    public function safeUp()
    {
        $this->execute('
            INSERT INTO
                {{%crm_client_type}} (
                    [[company_id]],
                    [[name]],
                    [[status]],
                    [[created_at]],
                    [[updated_at]],
                    [[value]]
                )

            SELECT
                {{%crm_client_status}}.[[company_id]],
                IF({{%crm_client_status}}.[[value]]=1, "Клиент", "Партнер"),
                "1",
                {{%crm_client_status}}.[[created_at]],
                {{%crm_client_status}}.[[created_at]],
                {{%crm_client_status}}.[[value]]

            FROM
                {{%crm_client_status}}

            WHERE
                {{%crm_client_status}}.[[value]] IN(1, 2)
        ');
    }

    public function safeDown()
    {
        $this->truncateTable('{{%crm_client_type}}');
    }
}
