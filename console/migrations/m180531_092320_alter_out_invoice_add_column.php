<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180531_092320_alter_out_invoice_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn(
            'out_invoice',
            'is_bik_autocomplete',
            $this->boolean()->notNull()->defaultValue(false)->after('is_autocomplete')
        );
        $this->addColumn(
            'out_invoice',
            'add_comment',
             $this->string()->defaultValue('')->after('is_additional_number_before')
        );
        $this->addColumn(
            'out_invoice',
            'notify_to_email',
            $this->boolean()->notNull()->defaultValue(false)->after('add_comment')
        );
    }

    public function safeDown()
    {
        $this->dropColumn('out_invoice', 'is_bik_autocomplete');
        $this->dropColumn('out_invoice', 'add_comment');
        $this->dropColumn('out_invoice', 'notify_to_email');
    }
}
