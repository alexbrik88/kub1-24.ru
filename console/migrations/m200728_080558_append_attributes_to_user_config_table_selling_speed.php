<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200728_080558_append_attributes_to_user_config_table_selling_speed extends Migration
{
    public $tableName = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'report_selling_speed_unit', $this->boolean(1)->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'report_selling_speed_article', $this->boolean(1)->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'report_selling_speed_group', $this->boolean(1)->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'report_selling_speed_storage', $this->boolean(1)->notNull()->defaultValue(true));

        $this->addColumn($this->tableName, 'table_selling_speed', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'report_selling_speed_unit');
        $this->dropColumn($this->tableName, 'report_selling_speed_article');
        $this->dropColumn($this->tableName, 'report_selling_speed_group');
        $this->dropColumn($this->tableName, 'report_selling_speed_storage');

        $this->dropColumn($this->tableName, 'table_selling_speed');
    }
}
