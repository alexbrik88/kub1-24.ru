<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200305_174323_alter_service_subscribe_create_index extends Migration
{
    public function safeUp()
    {
        $this->createIndex('activated_at', '{{%service_subscribe}}', 'activated_at');
        $this->createIndex('expired_at', '{{%service_subscribe}}', 'expired_at');
    }

    public function safeDown()
    {
        $this->dropIndex('activated_at', '{{%service_subscribe}}');
        $this->dropIndex('expired_at', '{{%service_subscribe}}');
    }
}
