<?php

use console\components\db\Migration;

class m200110_162705_insales_order_company_id_add extends Migration
{
    const TABLE = '{{%insales_order}}';

    public function up()
    {
        $this->addColumn(self::TABLE, 'company_id', $this->integer(11)->notNull()->after('id')->comment('Идентификатор компании КУБ'));
    }

    public function down()
    {
        $this->dropColumn(self::TABLE, 'company_id');
    }
}
