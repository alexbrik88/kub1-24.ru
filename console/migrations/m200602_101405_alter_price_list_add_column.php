<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200602_101405_alter_price_list_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('price_list', 'include_product_category_columns', $this->string()->after('include_count_in_package_column'));
    }

    public function safeDown()
    {
        $this->dropColumn('price_list', 'include_product_category_columns');
    }
}
