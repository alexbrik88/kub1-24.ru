<?php

use yii\db\Migration;

class m210607_123017_alter_crm_client_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%crm_client}}';

    /**
     * @inheritDoc
     */
    public function up(): void
    {
        $this->dropColumn(self::TABLE_NAME, 'client_activity_id');
        $this->dropColumn(self::TABLE_NAME, 'client_campaign_id');
    }

    /**
     * @inheritDoc
     */
    public function down(): bool
    {
        return false;
    }
}
