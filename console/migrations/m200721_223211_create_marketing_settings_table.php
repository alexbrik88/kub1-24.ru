<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%marketing_channel_settings}}`.
 */
class m200721_223211_create_marketing_settings_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%marketing_settings}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer(),
            'channel_id' => $this->integer(),
            'auto_import_type' => $this->tinyInteger()->defaultValue(0),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%marketing_settings}}');
    }
}
