<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191230_092737_alter_service_subscribe_tariff_group_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%service_subscribe_tariff_group}}', 'priority', $this->integer());

        $priority = [1, 7, 4, 3, 5, 2, 6];

        foreach ($priority as $key => $value) {
            $this->update('{{%service_subscribe_tariff_group}}', ['priority' => $key], ['id' => $value]);
        }
    }

    public function safeDown()
    {
        $this->dropColumn('{{%service_subscribe_tariff_group}}', 'priority');
    }
}
