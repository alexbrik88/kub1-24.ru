<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190124_145256_alter_bank_user_add_kolumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('bank_user', 'created_at', $this->timestamp()->notNull());
    }

    public function safeDown()
    {
        $this->dropColumn('bank_user', 'created_at');
    }
}
