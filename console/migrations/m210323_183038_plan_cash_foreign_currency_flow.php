<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210323_183038_plan_cash_foreign_currency_flow extends Migration
{
    public $tableName = 'plan_cash_foreign_currency_flows';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'currency_id' => $this->char(3)->notNull(),
            'currency_name' => $this->char(3)->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'checking_accountant_id' => $this->integer(),
            'cashbox_id' => $this->integer(),
            'emoney_id' => $this->integer(),
            'contractor_id' => $this->string()->notNull(),
            'plan_cash_contractor_id' => $this->integer(),
            'date' => $this->date()->notNull(),
            'first_date' => $this->date(),
            'flow_type' => $this->boolean()->defaultValue(1)->comment('0 - расход, 1 - приход'),
            'payment_type' => $this->integer()->defaultValue(1)->comment('1 - банк, 2 - касса, 3 - emoney'),
            'amount' => $this->decimal(38, 2)->notNull(),
            'income_item_id' => $this->integer()->defaultValue(null),
            'expenditure_item_id' => $this->integer()->defaultValue(null),
            'description' => $this->text()->notNull(),
            'is_repeated' => $this->boolean()->notNull()->defaultValue(false),
            'first_flow_id' => $this->integer()->defaultValue(null),
            'invoice_id' => $this->integer(),
            'is_internal_transfer' => $this->boolean()->notNull()->defaultValue(false)
        ]);

        $this->addForeignKey('FK_plan_cash_foreign_currency_flows_to_company_id', $this->tableName, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('FK_plan_cash_foreign_currency_flows_to_income_item_id', $this->tableName, 'income_item_id', 'invoice_income_item', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('FK_plan_cash_foreign_currency_flows_to_expenditure_item_id', $this->tableName, 'expenditure_item_id', 'invoice_expenditure_item', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('FK_plan_cash_foreign_currency_flows_to_first_flow_id', $this->tableName, 'first_flow_id', $this->tableName, 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_plan_cash_foreign_currency_flows_to_checking_accountant', $this->tableName, 'checking_accountant_id', 'checking_accountant', 'id', 'CASCADE');
        $this->addForeignKey('FK_plan_cash_foreign_currency_flows_to_cashbox', $this->tableName, 'cashbox_id', 'cashbox', 'id', 'CASCADE');
        $this->addForeignKey('FK_plan_cash_foreign_currency_flows_to_emoney', $this->tableName, 'emoney_id', 'emoney', 'id', 'CASCADE');
        $this->addForeignKey('FK_plan_cash_foreign_currency_flows_to_plan_cash_contractor', $this->tableName, 'plan_cash_contractor_id', 'plan_cash_contractor', 'id', 'SET NULL');
        $this->addForeignKey('FK_plan_cash_foreign_currency_flows_to_invoice', $this->tableName, 'invoice_id', 'foreign_currency_invoice', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_plan_cash_foreign_currency_flows_to_company_id', $this->tableName);
        $this->dropForeignKey('FK_plan_cash_foreign_currency_flows_to_income_item_id', $this->tableName);
        $this->dropForeignKey('FK_plan_cash_foreign_currency_flows_to_expenditure_item_id', $this->tableName);
        $this->dropForeignKey('FK_plan_cash_foreign_currency_flows_to_first_flow_id', $this->tableName);
        $this->dropForeignKey('FK_plan_cash_foreign_currency_flows_to_checking_accountant', $this->tableName);
        $this->dropForeignKey('FK_plan_cash_foreign_currency_flows_to_cashbox', $this->tableName);
        $this->dropForeignKey('FK_plan_cash_foreign_currency_flows_to_emoney', $this->tableName);
        $this->dropForeignKey('FK_plan_cash_foreign_currency_flows_to_plan_cash_contractor', $this->tableName);
        $this->dropForeignKey('FK_plan_cash_foreign_currency_flows_to_invoice', $this->tableName);

        $this->dropTable($this->tableName);
    }
}
