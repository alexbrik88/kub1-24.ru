<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170223_085558_create_table_autoinvoice extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%autoinvoice}}', [
            'id' => $this->primaryKey(),
            'period' => $this->smallInteger(),
            'day' => $this->integer(2),
            'date_from' => $this->integer(),
            'date_to' => $this->integer(),
            'add_month_and_year' => $this->boolean(),
            'type' => $this->integer(),
            'company_id' => $this->integer(),
            'document_author_id' => $this->integer(),
            'production_type' => $this->integer(),
            'contractor_id' => $this->integer(),
            'invoice_expenditure_item_id' => $this->integer(),
        ], $this->tableOptions);
        $this->addForeignKey('autoinvoice_to_company', '{{%autoinvoice}}', 'company_id', '{{%company}}', 'id');
        $this->addForeignKey('autoinvoice_to_contractor', '{{%autoinvoice}}', 'contractor_id', '{{%contractor}}', 'id');
        $this->addForeignKey('autoinvoice_to_invoice_expenditure_item', '{{%autoinvoice}}', 'invoice_expenditure_item_id', '{{%invoice_expenditure_item}}', 'id');
    }
    
    public function safeDown()
    {
        $this->dropTable('{{%autoinvoice}}');
    }
}


