<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180302_092116_alter_cash_bank_statement_upload_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cash_bank_statement_upload', 'saved_count', $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn('cash_bank_statement_upload', 'saved_count');
    }
}
