<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190114_173357_add_employee_id_to_menu_item extends Migration
{
    public $tableName = 'menu_item';

    public function safeUp()
    {
        $this->truncateTable($this->tableName);

        $this->addColumn($this->tableName, 'employee_id', $this->integer()->notNull()->after('company_id'));
        $this->addForeignKey($this->tableName . '_employee_id', $this->tableName, 'employee_id', 'employee', 'id', 'RESTRICT', 'CASCADE');

        $this->dropForeignKey($this->tableName . '_company_id', $this->tableName);
        $this->dropIndex('company_id', $this->tableName);
        $this->alterColumn($this->tableName, 'company_id', $this->integer()->notNull());
        $this->addForeignKey($this->tableName . '_company_id', $this->tableName, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');

        $this->alterColumn($this->tableName, 'id', $this->integer());
        $this->dropPrimaryKey('PRIMARY_KEY', $this->tableName);
        $this->dropColumn($this->tableName, 'id');
        $this->addPrimaryKey('PRIMARY_KEY', $this->tableName, ['employee_id', 'company_id']);

        $this->execute("
            INSERT INTO 
                {{%menu_item}} (
                    [[company_id]],
                	[[employee_id]],
                	[[invoice_item]],
                	[[accountant_item]]
                )
            SELECT
                {{%employee_company}}.[[company_id]],
                {{%employee_company}}.[[employee_id]],
                IF({{%company}}.[[registration_page_type_id]] = 10, 0, 1),
                IF({{%company}}.[[registration_page_type_id]] = 10, 1, 0)
            FROM 
                {{%employee_company}}
            LEFT JOIN
                {{%company}}
                ON {{%company}}.[[id]] = {{%employee_company}}.[[company_id]]
        ");
    }

    public function safeDown()
    {
        $this->truncateTable($this->tableName);
        $this->dropForeignKey($this->tableName . '_employee_id', $this->tableName);
        $this->dropColumn($this->tableName, 'employee_id');
    }
}
