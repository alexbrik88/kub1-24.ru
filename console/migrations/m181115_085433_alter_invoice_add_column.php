<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181115_085433_alter_invoice_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('invoice', 'by_donate_link_id', $this->string(50)->after('by_out_link_id'));

        $this->addForeignKey('FK_invoice_donateWidget', 'invoice', 'by_donate_link_id', 'donate_widget', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_invoice_donateWidget', 'invoice');

        $this->dropColumn('invoice', 'by_donate_link_id');
    }
}
