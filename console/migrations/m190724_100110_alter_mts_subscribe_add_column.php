<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190724_100110_alter_mts_subscribe_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%mts_subscribe}}', 'product_code', $this->string()->after('payment_id'));
        $this->alterColumn('{{%mts_subscribe}}', 'tariff_id', $this->integer());

        $this->addForeignKey(
            'FK_mts_subscribe_service_subscribe_tariff',
            '{{%mts_subscribe}}',
            'tariff_id',
            '{{%service_subscribe_tariff}}',
            'id'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey(
            'FK_mts_subscribe_service_subscribe_tariff',
            '{{%mts_subscribe}}'
        );

        $this->dropColumn('{{%mts_subscribe}}', 'product_code');
        $this->alterColumn('{{%mts_subscribe}}', 'tariff_id', $this->integer()->notNull());
    }
}
