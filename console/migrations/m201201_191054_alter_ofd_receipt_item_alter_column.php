<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201201_191054_alter_ofd_receipt_item_alter_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%ofd_receipt_item}}', 'id', $this->string(100)->notNull());
        $this->alterColumn('{{%ofd_receipt_item}}', 'receipt_id', $this->string(100)->notNull());
    }

    public function safeDown()
    {
        $this->alterColumn('{{%ofd_receipt_item}}', 'id', $this->bigInteger()->notNull()->unsigned()." AUTO_INCREMENT");
        $this->alterColumn('{{%ofd_receipt_item}}', 'receipt_id', $this->bigInteger()->notNull());
    }
}
