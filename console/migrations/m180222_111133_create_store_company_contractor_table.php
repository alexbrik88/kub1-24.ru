<?php

use yii\db\Migration;

/**
 * Handles the creation of table `store_company_contractor`.
 */
class m180222_111133_create_store_company_contractor_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('store_company_contractor', [
            'store_company_id' => $this->integer()->notNull(),
            'contractor_id' => $this->integer()->notNull(),
            'status' => $this->smallInteger()->notNull()->defaultValue(1),
            'created_at' => $this->integer(),
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', '{{%store_company_contractor}}', ['store_company_id', 'contractor_id']);

        $this->addForeignKey(
            'FK_storeCompanyContractor_storeCompany',
            'store_company_contractor',
            'store_company_id',
            'store_company',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'FK_storeCompanyContractor_contractor',
            'store_company_contractor',
            'contractor_id',
            'contractor',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('store_company_contractor');
    }
}
