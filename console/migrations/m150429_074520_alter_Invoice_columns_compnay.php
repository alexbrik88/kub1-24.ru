<?php

use yii\db\Schema;
use yii\db\Migration;

class m150429_074520_alter_Invoice_columns_compnay extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('invoice', 'company_address_legal_full', Schema::TYPE_STRING . '(255) NULL');
        $this->alterColumn('invoice', 'company_chief_post_name', Schema::TYPE_STRING . '(45) NULL');
        $this->alterColumn('invoice', 'company_chief_lastname', Schema::TYPE_STRING . '(45) NULL');
        $this->alterColumn('invoice', 'company_chief_firstname_initials', Schema::TYPE_STRING . '(10) NULL');
        $this->alterColumn('invoice', 'company_chief_patronymic_initials', Schema::TYPE_STRING . '(10) NULL');
        $this->alterColumn('invoice', 'company_chief_accountant_lastname', Schema::TYPE_STRING . '(10) NULL');
        $this->alterColumn('invoice', 'company_chief_accountant_firstname_initials', Schema::TYPE_STRING . '(10) NULL');
        $this->alterColumn('invoice', 'company_chief_accountant_patronymic_initials', Schema::TYPE_STRING . '(10) NULL');
    }

    public function safeDown()
    {
        $this->alterColumn('invoice', 'company_address_legal_full', Schema::TYPE_STRING . '(255) NOT NULL');
        $this->alterColumn('invoice', 'company_chief_post_name', Schema::TYPE_STRING . '(45) NOT NULL');
        $this->alterColumn('invoice', 'company_chief_lastname', Schema::TYPE_STRING . '(45) NOT NULL');
        $this->alterColumn('invoice', 'company_chief_firstname_initials', Schema::TYPE_STRING . '(10) NOT NULL');
        $this->alterColumn('invoice', 'company_chief_patronymic_initials', Schema::TYPE_STRING . '(10) NOT NULL');
        $this->alterColumn('invoice', 'company_chief_accountant_lastname', Schema::TYPE_STRING . '(10) NOT NULL');
        $this->alterColumn('invoice', 'company_chief_accountant_firstname_initials', Schema::TYPE_STRING . '(10) NOT NULL');
        $this->alterColumn('invoice', 'company_chief_accountant_patronymic_initials', Schema::TYPE_STRING . '(10) NOT NULL');
    }
}
