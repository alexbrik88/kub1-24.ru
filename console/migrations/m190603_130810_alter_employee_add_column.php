<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190603_130810_alter_employee_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%employee}}', 'is_mailing_active', $this->boolean()->defaultValue(true));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%employee}}', 'is_mailing_active');
    }
}
