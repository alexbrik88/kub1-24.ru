<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190130_083942_alter_service_subscribe_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('service_subscribe', 'group_id', $this->integer()->notNull()->after('company_id'));

        $this->update('service_subscribe', ['group_id' => 1]);

        $this->addForeignKey(
            'FK_service_subscribe_tariff_group',
            'service_subscribe', 'group_id',
            'service_subscribe_tariff_group', 'id'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_service_subscribe_tariff_group', 'service_subscribe');

        $this->dropColumn('service_subscribe', 'group_id');
    }
}
