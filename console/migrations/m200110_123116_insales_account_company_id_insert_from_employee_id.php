<?php

use common\models\insales\Account;
use common\models\employee\Employee;
use console\components\db\Migration;

class m200110_123116_insales_account_company_id_insert_from_employee_id extends Migration
{
    const TABLE = '{{%insales_account}}';

    public function safeUp()
    {
        foreach (Account::find()->all() as $account) {
            /** @var Account $account */
            /** @noinspection PhpUndefinedFieldInspection */
            $account->company_id = Employee::findOne(['id' => $account->employee_id])->company_id;
            $account->save();
        }
    }

    public function down()
    {
        $this->update(self::TABLE, ['company_id' => 0]);
    }
}
