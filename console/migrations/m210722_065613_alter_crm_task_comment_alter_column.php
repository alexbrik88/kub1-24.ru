<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210722_065613_alter_crm_task_comment_alter_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%crm_task_comment}}', 'text', $this->text()->notNull());
    }

    public function safeDown()
    {
        $this->alterColumn('{{%crm_task_comment}}', 'text', $this->string()->notNull());
    }
}
