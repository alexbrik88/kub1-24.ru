<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210601_064130_update_ofd_receipt_item extends Migration
{
    public function up()
    {
        $this->execute('
            UPDATE `ofd_receipt_item` ri
                LEFT JOIN (
                    SELECT id, company_id, title FROM product t
                    WHERE t.is_deleted = FALSE
                    AND t.company_id IS NOT NULL
                    GROUP BY company_id, title
                ) pr ON ri.`company_id` = pr.`company_id` AND ri.`name` = pr.`title`
            SET ri.`product_id` = pr.`id`
            WHERE ri.`product_id` IS NULL
                AND pr.`id` IS NOT NULL
        ');
    }

    public function down()
    {
        //
    }
}
