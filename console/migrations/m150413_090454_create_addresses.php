<?php

use yii\db\Schema;
use yii\db\Migration;

class m150413_090454_create_addresses extends Migration
{
    public function up()
    {
        $this->createTable('address_house_type', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . '(45) NOT NULL',
        ]);
        $this->batchInsert('address_house_type', ['id', 'name',], [
            [1, 'Дом',],
            [2, 'Владение',],
            [3, 'Домовладение',],
        ]);

        $this->createTable('address_housing_type', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . '(45) NOT NULL',
            'name_short' => Schema::TYPE_STRING . '(45) NOT NULL',
        ]);
        $this->batchInsert('address_housing_type', ['id', 'name', 'name_short',], [
            [1, 'Корпус', 'корп.',],
            [2, 'Строение', 'стр.',],
            [3, 'Сооружение', 'сооруж.',],
            [4, 'Литер', 'литер',],
        ]);

        $this->createTable('address_apartment_type', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . '(45) NOT NULL',
            'name_short' => Schema::TYPE_STRING . '(45) NOT NULL',
        ]);
        $this->batchInsert('address_apartment_type', ['id', 'name', 'name_short',], [
            [1, 'Квартира', 'кв.',],
            [2, 'Комната', 'комн.',],
            [3, 'Офис', 'оф.',],
        ]);
    }

    public function down()
    {
        $this->dropTable('address_house_type');
        $this->dropTable('address_housing_type');
        $this->dropTable('address_apartment_type');
    }
}
