<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190123_123431_insert_new_registration_page_type extends Migration
{
    public $tableName = 'registration_page_type';

    public function safeUp()
    {
        $this->insert($this->tableName, [
            'id' => 14,
            'name' => 'Моб. приложение',
        ]);
    }
    
    public function safeDown()
    {
        $this->delete($this->tableName, ['id' => 14]);
    }
}


