<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211109_210641_alter_olap_docs_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('olap_documents', 'updated_at', $this->timestamp()->defaultExpression('CURRENT_TIMESTAMP')->append('ON UPDATE CURRENT_TIMESTAMP'));
        $this->execute("update `olap_documents` set updated_at = updated_at - INTERVAL (0.5E6 - `id`) SECOND");
        $this->createIndex('IDX_olap_documents_updated_at', 'olap_documents', ['company_id', 'updated_at']);
    }

    public function safeDown()
    {
        $this->dropIndex('IDX_olap_documents_updated_at', 'olap_documents');
        $this->dropColumn('olap_documents', 'updated_at');
    }
}
