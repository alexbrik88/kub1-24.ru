<?php

use console\components\db\Migration;
use common\models\amocrm\Task;

class m190904_093005_create_amocrm_task extends Migration
{
    public function up()
    {
        $this->createTable('{{amocrm_task}}', [
            'id' => $this->primaryKey()->unsigned(),
            'element_id' => $this->integer()->unsigned(),
            'element_type' => "ENUM('" . implode("','", Task::ELEMENT_TYPE) . "') COMMENT 'Тип связанной с задачей сущности'",
            'task_type' => "ENUM('" . implode("','", Task::TYPE) . "') NOT NULL COMMENT 'Тип задачи'",
            'text' => $this->string(1000),
            'status' => $this->boolean()->notNull()->defaultValue(false)->comment('Признак, что задача завершена'),
            'complete_before' => $this->integer()->unsigned()->notNull(),
            'complete_till' => $this->integer()->unsigned()->notNull(),
            'created_at' => $this->integer()->unsigned()->notNull(),
            'updated_at' => $this->integer()->unsigned()->notNull(),
        ], "COMMENT 'Интеграция AMOcrm: задачи'");

    }

    public function down()
    {
        $this->dropTable('{{amocrm_task}}');

    }
}
