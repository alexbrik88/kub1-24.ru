<?php

use common\models\Company;
use common\models\company\CompanyAffiliateLink;
use console\components\db\Migration;
use yii\db\Expression;
use yii\db\Query;
use yii\db\Schema;

class m200818_094135_set_affiliate_link_to_company_table extends Migration
{
    public function safeUp()
    {
        $companies = (new Query())
            ->from(Company::tableName())
            ->andWhere(['and',
                ['IS NOT', 'invited_by_company_id', new Expression('NULL')],
                ['IS', 'invited_by_referral_code', new Expression('NULL')],
            ])
            ->all();

        foreach ($companies as $company) {
            $link = (new Query())
                ->from(CompanyAffiliateLink::tableName())
                ->andWhere(['and',
                    ['company_id' => $company['invited_by_company_id']],
                    ['module_id' => $company['invited_by_product_id']],
                    ['IS', 'custom_link_id', new Expression('NULL')],
                ])
                ->one();

            if (!$link) {
                continue;
            }

            $this->update(Company::tableName(),
                ['invited_by_referral_code' => $link['link']],
                ['id' => $company['id']]
            );
        }
    }

    public function safeDown()
    {
        $this->update(Company::tableName(),
            ['invited_by_referral_code' => new Expression('NULL')],
            ['>', 'id', new Expression(0)]
        );
    }
}
