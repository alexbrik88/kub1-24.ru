<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170303_094730_add_fields_to_autoinvoice extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%autoinvoice}}', 'created_at', $this->integer());
        $this->addColumn('{{%autoinvoice}}', 'document_number', $this->integer());
        $this->addColumn('{{%autoinvoice}}', 'all_sum', $this->bigInteger());
        $this->addColumn('{{%autoinvoice}}', 'last_invoice_date', $this->integer());
        $this->addColumn('{{%autoinvoice}}', 'next_invoice_date', $this->integer());
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%autoinvoice}}', 'created_at');
        $this->dropColumn('{{%autoinvoice}}', 'document_number');
        $this->dropColumn('{{%autoinvoice}}', 'all_sum');
        $this->dropColumn('{{%autoinvoice}}', 'last_invoice_date');
        $this->dropColumn('{{%autoinvoice}}', 'next_invoice_date');
    }
}


