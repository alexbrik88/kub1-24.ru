<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190611_092246_insert_employee_role extends Migration
{
    public function safeUp()
    {
        $this->batchInsert('{{%employee_role}}', [
            'id',
            'name',
        ], [
            [8, 'Руководитель отдела продаж'],
            [9, 'Руководитель отдела продаж (только просмотр)'],
            [10, 'Менеджер по продажам'],
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%employee_role}}', [
            'id' => [8, 9, 10],
        ]);
    }
}
