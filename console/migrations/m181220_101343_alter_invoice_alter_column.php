<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181220_101343_alter_invoice_alter_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('invoice', 'is_invoice_contract', $this->boolean()->notNull()->defaultValue(false));

        $this->update('invoice', ['is_invoice_contract' => false], ['is_invoice_contract' => null]);
    }

    public function safeDown()
    {
        $this->alterColumn('invoice', 'is_invoice_contract', $this->boolean()->defaultValue(false));
    }
}
