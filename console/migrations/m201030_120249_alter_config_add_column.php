<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201030_120249_alter_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_config', 'report_plan_fact_expanded', $this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('user_config', 'report_plan_fact_expanded');
    }
}
