<?php

use yii\db\Schema;
use yii\db\Migration;

class m150507_110931_alter_Invoice_addColumn_IsDeleted extends Migration
{
    public function safeUp()
    {
        $this->addColumn('invoice', 'is_deleted', Schema::TYPE_BOOLEAN . ' DEFAULT 0');
    }
    
    public function safeDown()
    {
        $this->dropColumn('invoice', 'is_deleted');
    }
}
