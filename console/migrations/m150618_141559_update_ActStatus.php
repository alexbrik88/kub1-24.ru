<?php

use yii\db\Schema;
use yii\db\Migration;

class m150618_141559_update_ActStatus extends Migration
{
    public function safeUp()
    {
        $this->update('act_status', [
            'name' => 'Подписан',
        ], [
            'id' => 4,
        ]);
    }
    
    public function safeDown()
    {
        $this->update('act_status', [
            'name' => 'Получен-подписан',
        ], [
            'id' => 4,
        ]);
    }
}
