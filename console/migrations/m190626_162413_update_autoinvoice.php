<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190626_162413_update_autoinvoice extends Migration
{
    public function safeUp()
    {
        $this->execute('
            UPDATE {{autoinvoice}}
            LEFT JOIN {{invoice}} ON {{invoice}}.[[id]] = {{autoinvoice}}.[[id]]
            LEFT JOIN {{contractor}} ON {{contractor}}.[[id]] = {{invoice}}.[[contractor_id]]
            SET {{autoinvoice}}.[[send_to_director]] = IF({{contractor}}.[[director_email]] LIKE "%@%", 1, 0),
                {{autoinvoice}}.[[send_to_contact]] = IF({{contractor}}.[[contact_email]] LIKE "%@%", 1, 0)
            WHERE {{autoinvoice}}.[[send_to_director]] = 0
            AND {{autoinvoice}}.[[send_to_chief_accountant]] = 0
            AND {{autoinvoice}}.[[send_to_contact]] = 0
        ');
    }

    public function safeDown()
    {

    }
}
