<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201125_100614_alter_packing_list_change_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('packing_list', 'orders_sum', $this->bigInteger()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->alterColumn('packing_list', 'orders_sum', $this->integer()->defaultValue(0));
    }
}
