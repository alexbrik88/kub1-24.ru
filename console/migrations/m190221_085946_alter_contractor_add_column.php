<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190221_085946_alter_contractor_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('contractor', 'invoice_income_item_id', $this->integer()->after('invoice_expenditure_item_id'));

        $this->execute('
            UPDATE {{contractor}}
            LEFT JOIN {{invoice_expenditure_item}}
                ON {{invoice_expenditure_item}}.[[id]] = {{contractor}}.[[invoice_expenditure_item_id]]
            SET {{contractor}}.[[invoice_expenditure_item_id]] = NULL
            WHERE {{contractor}}.[[invoice_expenditure_item_id]] IS NOT NULL
            AND {{invoice_expenditure_item}}.[[id]] IS NULL

        ');

        $this->addForeignKey(
            'FK_contractor_invoice_expenditure_item',
            'contractor', 'invoice_expenditure_item_id',
            'invoice_expenditure_item', 'id',
            'SET NULL'
        );
        $this->addForeignKey(
            'FK_contractor_invoice_income_item',
            'contractor', 'invoice_income_item_id',
            'invoice_income_item', 'id',
            'SET NULL'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_contractor_invoice_expenditure_item', 'contractor');
        $this->dropForeignKey('FK_contractor_invoice_income_item', 'contractor');
        $this->dropColumn('contractor', 'invoice_income_item_id');
    }
}
