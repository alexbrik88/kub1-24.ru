<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190130_092034_alter_service_payment_promo_code_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('service_payment_promo_code', 'tariff_group_id', $this->integer()->notNull()->after('company_id'));

        $this->update('service_payment_promo_code', ['tariff_group_id' => 1]);

        $this->addForeignKey(
            'FK_service_payment_promo_code_tariff_group',
            'service_payment_promo_code', 'tariff_group_id',
            'service_subscribe_tariff_group', 'id'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_service_payment_promo_code_tariff_group', 'service_payment_promo_code');

        $this->dropColumn('service_payment_promo_code', 'tariff_group_id');
    }
}
