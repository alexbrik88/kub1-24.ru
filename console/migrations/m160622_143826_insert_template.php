<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160622_143826_insert_template extends Migration
{
    private $_table = '{{%template}}';
    private $_columnSequence = 'sequence';
    private $_valueColumnSequence = 5;
    private $_template;

    public function safeUp()
    {
        $this->_template = [
            $this->_columnSequence  =>  $this->_valueColumnSequence,
            'title' => 'Мои шаблоны',
        ];

        $this->insert($this->_table, $this->_template);
    }
    
    public function safeDown()
    {
        $condition = $this->_columnSequence.'= "'.$this->_valueColumnSequence.'"';
        
        $this->delete($this->_table, $condition);
    }
}


