<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201125_100606_alter_act_change_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('act', 'order_sum', $this->bigInteger()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->alterColumn('act', 'order_sum', $this->integer()->defaultValue(0));
    }
}
