<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180501_045904_update_invoice extends Migration
{
    public function safeUp()
    {
        $this->execute('
            UPDATE {{invoice}}
            LEFT JOIN {{company}} ON {{invoice}}.[[company_id]] = {{company}}.[[id]]
            LEFT JOIN {{company_taxation_type}} ON {{company_taxation_type}}.[[company_id]] = {{company}}.[[id]]
            LEFT JOIN {{invoice_facture}} ON {{invoice_facture}}.[[invoice_id]] = {{invoice}}.[[id]]
            SET {{invoice}}.[[nds_view_type_id]] = IF(
                {{company_taxation_type}}.[[osno]],
                IF({{company}}.[[nds]] = 2, 1, 0),
                IF(
                    {{company_taxation_type}}.[[usn]],
                    IF({{invoice_facture}}.[[id]] IS NULL, 2, 0),
                    2
                )
            )
            WHERE {{invoice}}.[[type]] = 2
        ');
    }

    public function safeDown()
    {
        echo "This migration can't be rolled back!";
    }
}
