<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170808_115332_alter_servicePaymentOrder_alterColumn extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%service_payment_order}}', 'discount', $this->decimal(6, 4)->notNull()->defaultValue(0));
    }
    
    public function safeDown()
    {
        $this->alterColumn('{{%service_payment_order}}', 'discount', $this->integer(4, 2)->notNull()->defaultValue(0));
    }
}
