<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190302_123641_alter_cashbox_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cashbox', 'reg_number', $this->string(16)->defaultValue(''));

        $this->createIndex('reg_number', 'cashbox', 'reg_number');
    }

    public function safeDown()
    {
        $this->dropIndex('reg_number', 'cashbox');

        $this->dropColumn('cashbox', 'reg_number');
    }
}