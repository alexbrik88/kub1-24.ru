<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210825_123215_alter_config_add_column extends Migration
{
    public $table = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->table, 'report_pc_description', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 1');
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'report_pc_description');
    }
}
