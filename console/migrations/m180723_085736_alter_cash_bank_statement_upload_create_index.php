<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180723_085736_alter_cash_bank_statement_upload_create_index extends Migration
{
    public function safeUp()
    {
        $this->createIndex('source', 'cash_bank_statement_upload', 'source');
    }

    public function safeDown()
    {
        $this->dropIndex('source', 'cash_bank_statement_upload');
    }
}
