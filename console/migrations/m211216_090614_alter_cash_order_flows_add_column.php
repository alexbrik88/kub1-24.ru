<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211216_090614_alter_cash_order_flows_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cash_order_flows}}', 'ofd_receipt_uid', $this->string(60)->null()->defaultValue(null));

        $this->createIndex('ofd_receipt_uid', '{{%cash_order_flows}}', 'ofd_receipt_uid', true);
    }

    public function safeDown()
    {
        $this->dropIndex('ofd_receipt_uid', '{{%cash_order_flows}}');

        $this->dropColumn('{{%cash_order_flows}}', 'ofd_receipt_uid');
    }
}
