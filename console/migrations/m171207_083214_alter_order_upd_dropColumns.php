<?php

use console\components\db\Migration;
use yii\db\Schema;

class m171207_083214_alter_order_upd_dropColumns extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('{{order_upd}}', 'empty_unit_code');
        $this->dropColumn('{{order_upd}}', 'empty_unit_name');
        $this->dropColumn('{{order_upd}}', 'empty_product_code');
        $this->dropColumn('{{order_upd}}', 'empty_box_type');
    }

    public function safeDown()
    {
        $this->addColumn('{{order_upd}}', 'empty_unit_code', $this->smallInteger(1)->defaultValue(false));
        $this->addColumn('{{order_upd}}', 'empty_unit_name', $this->smallInteger(1)->defaultValue(false));
        $this->addColumn('{{order_upd}}', 'empty_product_code', $this->smallInteger(1)->defaultValue(false));
        $this->addColumn('{{order_upd}}', 'empty_box_type', $this->smallInteger(1)->defaultValue(false));

        $this->execute("
            UPDATE {{order_upd}}
            LEFT JOIN {{upd}}
                ON {{order_upd}}.[[upd_id]] = {{upd}}.[[id]]
            LEFT JOIN {{order}}
                ON {{order_upd}}.[[order_id]] = {{order}}.[[id]]
            LEFT JOIN {{product}} {{product}}
                ON {{order}}.[[product_id]] = {{product}}.[[id]]
            SET
                {{order_upd}}.[[empty_unit_code]] = 1,
                {{order_upd}}.[[empty_unit_name]] = 1,
                {{order_upd}}.[[empty_product_code]] = 1,
                {{order_upd}}.[[empty_box_type]] = 1
            WHERE {{upd}}.[[type]] = 2
                AND {{upd}}.[[show_service_units]] = 0
                AND {{product}}.[[production_type]] = 0
        ");
    }
}
