<?php

use console\components\db\Migration;

class m200109_192308_evotor_receipt_employee_id_drop extends Migration
{
    const TABLE = '{{%evotor_receipt}}';

    public function safeUp()
    {
        $this->dropForeignKey('evotor_receipt_employee_id', self::TABLE);
        $this->dropColumn(self::TABLE, 'employee_id');
    }

    public function safeDown()
    {
        $this->delete(self::TABLE);
        $this->addColumn(self::TABLE, 'employee_id', $this->string(11)->notNull()->after('id')->comment('ID клиента КУБ'));
        $this->addForeignKey(
            'evotor_receipt_employee_id', self::TABLE, 'employee_id',
            '{{%employee}}', 'id',
            'CASCADE', 'CASCADE'
        );
    }
}
