<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200518_181726_add_agreement_template_user_config_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn(
            'user_config',
            'agreement_template_responsible_employee_id',
            $this->boolean()->notNull()->defaultValue(false)->comment('Показ колонки "Ответственный" в списке шаблонов договоров')
        );
    }

    public function safeDown()
    {
        $this->dropColumn('user_config', 'agreement_template_responsible_employee_id');
    }
}
