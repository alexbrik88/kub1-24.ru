<?php

use yii\db\Schema;
use yii\db\Migration;

class m150713_125404_alter_employee_addColumn_author_id extends Migration
{
    public function safeUp()
    {
        $this->addColumn('employee', 'author_id', Schema::TYPE_INTEGER . ' DEFAULT NULL AFTER created_at');
        $this->addForeignKey('fk_employee_to_author', 'employee', 'author_id', 'employee', 'id');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('fk_employee_to_author', 'employee');
        $this->dropColumn('employee', 'author_id');
    }
}
