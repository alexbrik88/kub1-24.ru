<?php

use yii\db\Schema;
use yii\db\Migration;

class m150606_154135_alter_Report_changeColumn_Date extends Migration
{
    public function safeUp()
    {
        $this->renameColumn('report', 'creation_date', 'created_at');
        $this->alterColumn('report', 'created_at', Schema::TYPE_INTEGER . ' NOT NULL');
        $this->update('report', [
            'created_at' => new \yii\db\Expression('UNIX_TiMESTAMP()'),
        ]);
    }

    public function safeDown()
    {
        $this->renameColumn('report', 'created_at', 'creation_date');
        $this->alterColumn('report', 'creation_date', Schema::TYPE_DATE);
        $this->update('report', [
            'creation_date' => new \yii\db\Expression('NOW()'),
        ]);
    }
}
