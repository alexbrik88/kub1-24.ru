<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211209_092318_insert_discount_type extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%discount_type}}', [
            'id' => 4,
            'description' => 'Тариф "Налоги + Декларация ИП на УСН 6%" на год с 50% скидкой для новых ИП',
        ]);
    }

    public function safeDown()
    {
        $this->вудуеу('{{%discount_type}}', [
            'id' => 4,
        ]);
    }
}
