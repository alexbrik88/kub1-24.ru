<?php

use console\components\db\Migration;
use yii\db\Schema;

class m161025_095759_add_test_to_company extends Migration
{
    public $tCompany = 'company';

    public function safeUp()
    {
        $this->addColumn($this->tCompany, 'test', $this->boolean()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tCompany, 'test');
    }
}


