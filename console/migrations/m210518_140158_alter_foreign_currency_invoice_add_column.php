<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210518_140158_alter_foreign_currency_invoice_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%foreign_currency_invoice}}', 'responsible_employee_id', $this->integer()->after('document_author_id'));
        $this->execute('
            UPDATE {{%foreign_currency_invoice}}
            SET {{%foreign_currency_invoice}}.[[responsible_employee_id]] = {{%foreign_currency_invoice}}.[[document_author_id]]
        ');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%foreign_currency_invoice}}', 'responsible_employee_id');
    }
}
