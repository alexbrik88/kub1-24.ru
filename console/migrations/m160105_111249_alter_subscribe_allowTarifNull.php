<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160105_111249_alter_subscribe_allowTarifNull extends Migration
{
    public $tSubscribe = 'service_subscribe';
    public $tTariff = 'service_subscribe_tariff';

    public function safeUp()
    {
        $this->dropForeignKey('fk_service_subscribe_to_tariff', $this->tSubscribe);

        $this->alterColumn($this->tSubscribe, 'tariff_id', $this->integer()->defaultValue(null));

        $this->addForeignKey('fk_service_subscribe_to_tariff', $this->tSubscribe, 'tariff_id', $this->tTariff, 'id');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('fk_service_subscribe_to_tariff', $this->tSubscribe);

        $this->alterColumn($this->tSubscribe, 'tariff_id', $this->integer()->notNull());

        $this->addForeignKey('fk_service_subscribe_to_tariff', $this->tSubscribe, 'tariff_id', $this->tTariff, 'id');
    }
}


