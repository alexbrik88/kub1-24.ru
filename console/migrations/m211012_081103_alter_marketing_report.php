<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211012_081103_alter_marketing_report extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%marketing_report}}';

    public function safeUp()
    {
        $this->dropIndex('uk_marketing_report', self::TABLE_NAME);
        $this->createIndex('uk_marketing_report',
            self::TABLE_NAME,
            ['company_id', 'source_type', 'date',  'utm_campaign', 'utm_source', 'utm_medium'],
            true
        );

        $this->dropIndex('IDX_marketing_report_utm_campaign', 'marketing_report');
        $this->createIndex('idx_marketing_report',
            self::TABLE_NAME,
            ['company_id', 'source_type', 'utm_campaign'],
            false
        );
    }

    public function safeDown()
    {
        $this->dropIndex('uk_marketing_report', self::TABLE_NAME);
        $this->createIndex('uk_marketing_report',
            self::TABLE_NAME,
            ['company_id', 'source_type', 'date', 'utm_source', 'utm_medium', 'utm_campaign'],
            true
        );

        $this->dropIndex('idx_marketing_report', self::TABLE_NAME);
        $this->createIndex('IDX_marketing_report_utm_campaign', 'marketing_report', 'utm_campaign', false);
    }
}
