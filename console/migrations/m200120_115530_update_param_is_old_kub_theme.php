<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200120_115530_update_param_is_old_kub_theme extends Migration
{
    public function safeUp()
    {
        $this->execute('
            UPDATE `employee`
            SET `is_old_kub_theme` = 1
        ');
    }

    public function safeDown()
    {
        echo 'No down migration.', PHP_EOL;
    }
}
