<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200909_143600_alter_product_turnover_add_column extends Migration
{
    public function safeUp()
    {
        $this->dropPrimaryKey('PRIMARY_KEY', '{{%product_turnover}}');
        $this->addPrimaryKey('PRIMARY_KEY', '{{%product_turnover}}', [
            'order_id',
            'document_id',
            'document_table',
        ]);
        $this->dropColumn('{{%product_turnover}}', 'order_table');
        $this->alterColumn('{{%product_turnover}}', 'product_id', $this->integer()->notNull()->after('date'));
        $this->addColumn('{{%product_turnover}}', 'margin', $this->bigInteger()->notNull()->defaultValue(0)->after('purchase_amount'));
        $this->addColumn('{{%product_turnover}}', 'product_group_id', $this->integer()->after('product_id'));
    }

    public function safeDown()
    {
        $this->addColumn('{{%product_turnover}}', 'order_table', $this->string(50)->notNull()->after('order_id'));
        $this->dropColumn('{{%product_turnover}}', 'product_group_id');
        $this->dropColumn('{{%product_turnover}}', 'margin');
        $this->alterColumn('{{%product_turnover}}', 'product_id', $this->integer()->notNull()->after('month'));

        $this->execute('
            UPDATE `product_turnover`
            SET `product_turnover`.`order_table` = CONCAT("order_", `product_turnover`.`document_table`)
        ');

        $this->dropPrimaryKey('PRIMARY_KEY', '{{%product_turnover}}');

        $this->execute('
            ALTER IGNORE TABLE {{%product_turnover}} ADD PRIMARY KEY([[order_id]], [[order_table]])
        ');
    }
}
