<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200629_154512_alter_foreign_currency_invoice_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%foreign_currency_invoice}}', 'foreign_currency_account_id', $this->integer()->after('contractor_id'));
        $this->addColumn('{{%foreign_currency_invoice}}', 'nds_view_type_id', $this->integer()->after('foreign_currency_account_id'));
        $this->addColumn('{{%foreign_currency_invoice}}', 'agreement_id', $this->integer()->after('nds_view_type_id'));
        $this->addColumn('{{%foreign_currency_invoice}}', 'is_additional_number_before', $this->boolean()->defaultValue(false)->after('document_additional_number'));
        $this->addColumn('{{%foreign_currency_invoice}}', 'invoice_status_author_id', $this->integer()->after('invoice_status_id'));
        $this->addColumn('{{%foreign_currency_invoice}}', 'invoice_status_updated_at', $this->integer()->after('invoice_status_author_id'));
        $this->addColumn('{{%foreign_currency_invoice}}', 'invoice_expenditure_item_id', $this->integer()->comment('статья расходов'));
        $this->addColumn('{{%foreign_currency_invoice}}', 'signed_by_employee_id', $this->integer());
        $this->addColumn('{{%foreign_currency_invoice}}', 'sign_document_type_id', $this->integer());
        $this->addColumn('{{%foreign_currency_invoice}}', 'signature_id', $this->integer());
        $this->addColumn('{{%foreign_currency_invoice}}', 'chief_signature_id', $this->integer());
        $this->addColumn('{{%foreign_currency_invoice}}', 'chief_accountant_signature_id', $this->integer());
        $this->addColumn('{{%foreign_currency_invoice}}', 'print_id', $this->integer());
        $this->addColumn('{{%foreign_currency_invoice}}', 'store_id', $this->integer());
        $this->addColumn('{{%foreign_currency_invoice}}', 'total_discount', $this->bigInteger(20)->notNull());
        $this->addColumn('{{%foreign_currency_invoice}}', 'total_amount', $this->bigInteger(20)->notNull());
        $this->addColumn('{{%foreign_currency_invoice}}', 'total_nds', $this->bigInteger(20));
        $this->addColumn('{{%foreign_currency_invoice}}', 'total_amount_with_nds', $this->bigInteger(20)->notNull());
        $this->addColumn('{{%foreign_currency_invoice}}', 'paid_amount', $this->bigInteger(20)->notNull());
        $this->addColumn('{{%foreign_currency_invoice}}', 'remaining_amount', $this->bigInteger(20)->notNull());
        $this->addColumn('{{%foreign_currency_invoice}}', 'is_deleted', $this->boolean()->defaultValue(false));
        $this->addColumn('{{%foreign_currency_invoice}}', 'remind_contractor', $this->boolean()->defaultValue(false));
        $this->addColumn('{{%foreign_currency_invoice}}', 'has_services', $this->boolean()->defaultValue(false));
        $this->addColumn('{{%foreign_currency_invoice}}', 'has_goods', $this->boolean()->defaultValue(false));
        $this->addColumn('{{%foreign_currency_invoice}}', 'has_file', $this->boolean()->defaultValue(false));
        $this->addColumn('{{%foreign_currency_invoice}}', 'has_discount', $this->boolean()->defaultValue(false));
        $this->addColumn('{{%foreign_currency_invoice}}', 'has_markup', $this->boolean()->defaultValue(false));
        $this->addColumn('{{%foreign_currency_invoice}}', 'has_weight', $this->boolean()->defaultValue(false));
        $this->addColumn('{{%foreign_currency_invoice}}', 'has_volume', $this->boolean()->defaultValue(false));
        $this->addColumn('{{%foreign_currency_invoice}}', 'discount_type', $this->boolean()->defaultValue(false));
        $this->addColumn('{{%foreign_currency_invoice}}', 'is_hidden_discount', $this->boolean()->defaultValue(false));
        $this->addColumn('{{%foreign_currency_invoice}}', 'show_article', $this->boolean()->defaultValue(false));
        $this->addColumn('{{%foreign_currency_invoice}}', 'show_paylimit_info', $this->boolean()->defaultValue(false));
        $this->addColumn('{{%foreign_currency_invoice}}', 'price_precision', "ENUM('2', '4') NOT NULL DEFAULT '2'");
        $this->addColumn('{{%foreign_currency_invoice}}', 'total_order_count', $this->integer());
        $this->addColumn('{{%foreign_currency_invoice}}', 'total_place_count', $this->string());
        $this->addColumn('{{%foreign_currency_invoice}}', 'total_mass_gross', $this->string());
        $this->addColumn('{{%foreign_currency_invoice}}', 'comment_internal', $this->text());
        $this->addColumn('{{%foreign_currency_invoice}}', 'email_messages', $this->integer()->defaultValue(0));

        $this->createIndex(
            'search_invoice',
            '{{%foreign_currency_invoice}}',
            [
                'company_id',
                'document_date',
                'type',
                'invoice_status_id',
            ]
        );
    }

    public function safeDown()
    {
        $this->dropIndex('search_invoice', '{{%foreign_currency_invoice}}');

        $this->dropColumn('{{%foreign_currency_invoice}}', 'foreign_currency_account_id');
        $this->dropColumn('{{%foreign_currency_invoice}}', 'nds_view_type_id');
        $this->dropColumn('{{%foreign_currency_invoice}}', 'agreement_id');
        $this->dropColumn('{{%foreign_currency_invoice}}', 'is_additional_number_before');
        $this->dropColumn('{{%foreign_currency_invoice}}', 'invoice_status_author_id');
        $this->dropColumn('{{%foreign_currency_invoice}}', 'invoice_status_updated_at');
        $this->dropColumn('{{%foreign_currency_invoice}}', 'invoice_expenditure_item_id');
        $this->dropColumn('{{%foreign_currency_invoice}}', 'signed_by_employee_id');
        $this->dropColumn('{{%foreign_currency_invoice}}', 'sign_document_type_id');
        $this->dropColumn('{{%foreign_currency_invoice}}', 'signature_id');
        $this->dropColumn('{{%foreign_currency_invoice}}', 'chief_signature_id');
        $this->dropColumn('{{%foreign_currency_invoice}}', 'chief_accountant_signature_id');
        $this->dropColumn('{{%foreign_currency_invoice}}', 'print_id');
        $this->dropColumn('{{%foreign_currency_invoice}}', 'store_id');
        $this->dropColumn('{{%foreign_currency_invoice}}', 'total_discount');
        $this->dropColumn('{{%foreign_currency_invoice}}', 'total_amount');
        $this->dropColumn('{{%foreign_currency_invoice}}', 'total_nds');
        $this->dropColumn('{{%foreign_currency_invoice}}', 'total_amount_with_nds');
        $this->dropColumn('{{%foreign_currency_invoice}}', 'paid_amount');
        $this->dropColumn('{{%foreign_currency_invoice}}', 'remaining_amount');
        $this->dropColumn('{{%foreign_currency_invoice}}', 'is_deleted');
        $this->dropColumn('{{%foreign_currency_invoice}}', 'remind_contractor');
        $this->dropColumn('{{%foreign_currency_invoice}}', 'has_services');
        $this->dropColumn('{{%foreign_currency_invoice}}', 'has_goods');
        $this->dropColumn('{{%foreign_currency_invoice}}', 'has_file');
        $this->dropColumn('{{%foreign_currency_invoice}}', 'has_discount');
        $this->dropColumn('{{%foreign_currency_invoice}}', 'has_markup');
        $this->dropColumn('{{%foreign_currency_invoice}}', 'has_weight');
        $this->dropColumn('{{%foreign_currency_invoice}}', 'has_volume');
        $this->dropColumn('{{%foreign_currency_invoice}}', 'discount_type');
        $this->dropColumn('{{%foreign_currency_invoice}}', 'is_hidden_discount');
        $this->dropColumn('{{%foreign_currency_invoice}}', 'show_article');
        $this->dropColumn('{{%foreign_currency_invoice}}', 'show_paylimit_info');
        $this->dropColumn('{{%foreign_currency_invoice}}', 'price_precision');
        $this->dropColumn('{{%foreign_currency_invoice}}', 'total_order_count');
        $this->dropColumn('{{%foreign_currency_invoice}}', 'total_place_count');
        $this->dropColumn('{{%foreign_currency_invoice}}', 'total_mass_gross');
        $this->dropColumn('{{%foreign_currency_invoice}}', 'comment_internal');
        $this->dropColumn('{{%foreign_currency_invoice}}', 'email_messages');
    }
}
