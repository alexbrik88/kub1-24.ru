<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181019_205748_add_is_deleted_to_price_list extends Migration
{
    public $tableName = 'price_list';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'is_deleted', $this->boolean()->notNull());
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'is_deleted');
    }
}


