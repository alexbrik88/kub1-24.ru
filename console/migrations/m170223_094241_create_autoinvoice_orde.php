<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170223_094241_create_autoinvoice_orde extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%autoorder}}', [
            'id' => $this->primaryKey(),
            'autoinvoice_id' => $this->integer(),
            'number' => $this->integer(),
            'product_id' => $this->integer(),
            'quantity' => $this->integer(),
            'product_title' => $this->string(255),
            'product_code' => $this->string(255),
            'unit_id' => $this->integer(),
            'count_in_place' => $this->string(255),
            'place_count' => $this->string(45),
            'mass_gross' => $this->string(45),
            'purchase_price_no_vat' => $this->bigInteger(20),
            'purchase_price_with_vat' => $this->bigInteger(20),
            'selling_price_no_vat' => $this->bigInteger(20),
            'selling_price_with_vat' => $this->bigInteger(20),
            'amount_purchase_no_vat' => $this->bigInteger(20),
            'amount_purchase_with_vat' => $this->bigInteger(20),
            'amount_sales_no_vat' => $this->bigInteger(20),
            'amount_sales_with_vat' => $this->bigInteger(20),
            'excise' => $this->boolean(),
            'excise_price' => $this->string(255),
            'sale_tax' => $this->bigInteger(20),
            'purchase_tax' => $this->bigInteger(20),
            'purchase_tax_rate_id' => $this->integer(),
            'sale_tax_rate_id' => $this->integer(),
            'country_id' => $this->integer(),
            'custom_declaration_number' => $this->string(45),
            'box_type' => $this->string(45),

        ], $this->tableOptions);
        $this->addForeignKey('autoorder_to_autoinvoice', '{{%autoorder}}', 'autoinvoice_id', '{{%autoinvoice}}', 'id');
        $this->addForeignKey('autoorder_to_product', '{{%autoorder}}', 'product_id', '{{%product}}', 'id');

    }
    
    public function safeDown()
    {
        $this->dropTable('{{%autoorder}}');
    }
}


