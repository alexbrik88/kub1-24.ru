<?php

use console\components\db\Migration;
use yii\db\Schema;
use common\models\document\Act;

class m160713_120137_update_old_act_and_packing_list_number extends Migration
{
    public $tArray = [
        'act',
        'packing_list',
        'invoice_facture',
    ];
    public $tInvoice = 'invoice';

    public function safeUp()
    {
        foreach ($this->tArray as $tableName) {
            Yii::$app->db->createCommand('UPDATE ' . $tableName . ' t1,
                                    (SELECT id, document_number FROM ' . $this->tInvoice . ') t2
                                    SET t1.document_number = t2.document_number + 0
                                    WHERE t1.document_number is null
                                    AND t1.invoice_id = t2.id')->execute();
            Yii::$app->db->createCommand('UPDATE ' . $tableName . ' t1,
                                    (SELECT id, document_number FROM ' . $this->tInvoice . ') t2
                                    SET t1.ordinal_document_number = t2.document_number + 0
                                    WHERE t1.document_number is null
                                    AND t1.invoice_id = t2.id')->execute();
        }
    }

    public function safeDown()
    {

    }
}


