<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160104_104313_create_companyExperience extends Migration
{
    public $tExperience = 'company_experience';

    public function safeUp()
    {
        $this->createTable($this->tExperience, [
            'id' => $this->primaryKey(),
            'name' => $this->string(45)->notNull(),
        ], $this->tableOptions);

        $this->batchInsert($this->tExperience, ['id', 'name'], [
            [1, 'Новая'],
            [2, 'Текущая'],
        ]);
    }
    
    public function safeDown()
    {
        $this->dropTable($this->tExperience);
    }
}


