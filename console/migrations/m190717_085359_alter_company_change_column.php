<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190717_085359_alter_company_change_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%company}}', 'new_template_order', $this->boolean()->defaultValue(true));
        $this->update('{{%company}}', ['new_template_order' => 1], ['not', ['new_template_order' => 1]]);
    }

    public function safeDown()
    {
        $this->alterColumn('{{%company}}', 'new_template_order', $this->boolean()->defaultValue(false));
    }
}
