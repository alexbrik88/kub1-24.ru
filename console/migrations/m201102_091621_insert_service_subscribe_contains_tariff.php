<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201102_091621_insert_service_subscribe_contains_tariff extends Migration
{
    public function safeUp()
    {
        $this->batchInsert('{{%service_subscribe_contains_tariff}}', [
            'tariff_id',
            'contains_tariff_id',
        ], [
            [49, 18],
            [50, 19],
            [51, 20],
            [52, 21],
            [53, 22],
            [54, 23],
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%service_subscribe_contains_tariff}}', [
            'tariff_id' => [49, 50, 51, 52, 53, 54],
        ]);
    }
}
