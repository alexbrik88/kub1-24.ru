<?php

use common\models\rent\Attribute;
use console\components\db\Migration;

class m200306_114601_rent_attribute_create extends Migration
{
    const TABLE = '{{%rent_attribute}}';

    const CATEGORY_REALTY = 1; //ID категории "недвижимость"
    const CATEGORY_TECHNIC = 2; //ID категории "спец.техника"
    const CATEGORY_EQUIPMENT = 3; //ID категории "оборудование"

    public function safeUp()
    {
        $this->createTable(self::TABLE, [
            'id' => 'MEDIUMINT UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT',
            'category_id' => $this->smallInteger()->unsigned()->notNull()->comment('ID категории объекта'),
            'alias' => $this->char(20)->notNull()->comment('Псевдоним атрибута'),
            'title' => $this->string(30)->notNull()->comment('Название атрибута'),
            'type' => "ENUM('" . implode("','", Attribute::TYPE) . "') NOT NULL COMMENT 'Тип хранимых данных'",
            'required' => $this->boolean()->notNull()->defaultValue(false)->comment('Обязательый атрибут'),
            'sort' => $this->tinyInteger()->unsigned()->notNull()->defaultValue(0)->comment('Индекс сортировки'),
            'default' => $this->string(50)->comment('Значение по умолчанию'),
            'containerCSS' => $this->string(20)->comment('CSS-класс контейнера'),
            'data' => $this->string(1000)->comment('Дополнительне атрибута (AJAX)')
        ], "COMMENT 'Учёт аренды техники: дополнительные атрибуты объектов аренды'");
        $this->addForeignKey(
            'rent_attribute_category_id', self::TABLE, 'category_id',
            '{{%rent_category}}', 'id', 'CASCADE', 'CASCADE'
        );
        $this->batchInsert(self::TABLE, ['category_id', 'alias', 'title', 'type', 'sort', 'default', 'data', 'required', 'containerCSS'], [
            [self::CATEGORY_REALTY, 'type', 'Тип', Attribute::TYPE_ENUM, 1, 'коммерческая', json_encode(['коммерческая', 'жилая'], JSON_UNESCAPED_UNICODE), true, null],
            [self::CATEGORY_REALTY, 'lessor', 'Арендодатель / № Кадастра', Attribute::TYPE_STRING, 2, null, null, false, null],
            [self::CATEGORY_REALTY, 'address', 'Адрес', Attribute::TYPE_STRING, 3, null, null, true, 'full-width'],
            [self::CATEGORY_REALTY, 'area', 'Площадь', Attribute::TYPE_FLOAT, 4, null, null, true, null],
            [self::CATEGORY_REALTY, 'rooms', 'Комнат', Attribute::TYPE_ENUM, 5, null, json_encode(['студия', 1, 2, 3, 4, 5], JSON_UNESCAPED_UNICODE), true, null],
            [self::CATEGORY_REALTY, 'priceDay', 'Цена за день', Attribute::TYPE_INTEGER, 6, null, null, false, null],
            [self::CATEGORY_REALTY, 'priceMonth', 'Цена за месяц', Attribute::TYPE_INTEGER, 7, null, null, false, null],
            [self::CATEGORY_REALTY, 'deposit', 'Обеспечительный платеж/Залог', Attribute::TYPE_INTEGER, 8, 0, null, false, null],
            [self::CATEGORY_REALTY, 'utilityPayment', 'Коммунальные платежи', Attribute::TYPE_ENUM, 9, 'включены', json_encode(['включены', 'не включены'], JSON_UNESCAPED_UNICODE), true, null],
            [self::CATEGORY_REALTY, 'mending', 'Ремонт', Attribute::TYPE_ENUM, 10, 'косметический', json_encode(['косметический', 'евро', 'эксклюзив'], JSON_UNESCAPED_UNICODE), true, null],
            [self::CATEGORY_REALTY, 'tech', 'Техника', Attribute::TYPE_SET, 11, null, json_encode(['телевизор', 'холодильник'], JSON_UNESCAPED_UNICODE), true, null]
        ]);
        $this->batchInsert(self::TABLE, ['category_id', 'alias', 'title', 'type', 'sort', 'default', 'data', 'required'], [
            [self::CATEGORY_TECHNIC, 'tenant', 'Арендатор', Attribute::TYPE_STRING, 1, null, null, false],
            [self::CATEGORY_TECHNIC, 'address', 'Адрес постоянной базировки', Attribute::TYPE_STRING, 2, null, null, false],
            [self::CATEGORY_TECHNIC, 'carrying', 'Грузоподъемность', Attribute::TYPE_FLOAT, 3, null, null, true],
            [self::CATEGORY_TECHNIC, 'volume', 'Объем (ковша/кузова)', Attribute::TYPE_FLOAT, 4, null, null, true],
            [self::CATEGORY_TECHNIC, 'chassis', 'Тип шасси', Attribute::TYPE_ENUM, 5, 'колёса', json_encode(['колёса', 'гусеничный'], JSON_UNESCAPED_UNICODE), true],
            [self::CATEGORY_TECHNIC, 'boomLength', 'Длина стрелы', Attribute::TYPE_FLOAT, 6, null, null, false],
            [self::CATEGORY_TECHNIC, 'priceHour', 'Цена за час', Attribute::TYPE_INTEGER, 7, null, null, false],
            [self::CATEGORY_TECHNIC, 'priceShift', 'Цена за смену', Attribute::TYPE_INTEGER, 8, null, null, false],
            [self::CATEGORY_TECHNIC, 'shiftHours', 'Часов в смене', Attribute::TYPE_INTEGER, 9, null, null, false],
            [self::CATEGORY_TECHNIC, 'deposit', 'Залог', Attribute::TYPE_INTEGER, 10, null, null, false],
        ]);
        $this->batchInsert(self::TABLE, ['category_id', 'alias', 'title', 'type', 'sort', 'default', 'data', 'required'], [
            [self::CATEGORY_EQUIPMENT, 'priceHour', 'Цена за час', Attribute::TYPE_INTEGER, 1, null, null, false],
            [self::CATEGORY_EQUIPMENT, 'priceShift', 'Цена за смену', Attribute::TYPE_INTEGER, 2, null, null, false],
            [self::CATEGORY_EQUIPMENT, 'shiftHours', 'Часов в смене', Attribute::TYPE_INTEGER, 3, null, null, false],
            [self::CATEGORY_EQUIPMENT, 'deposit', 'Залог', Attribute::TYPE_INTEGER, 4, null, null, false],
        ]);
    }

    public function down()
    {
        $this->dropTable(self::TABLE);
    }

}
