<?php

use yii\db\Migration;

/**
 * Handles the creation of table `employee_salary`.
 */
class m181207_073408_create_employee_salary_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('employee_salary', [
            'employee_id' => $this->integer()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'date' => $this->date()->notNull(),
            'days_worked' => $this->smallInteger(),
            'salary_1_amount' => $this->bigInteger(20)->defaultValue(0),
            'bonus_1_amount' => $this->bigInteger(20)->defaultValue(0),
            'salary_2_amount' => $this->bigInteger(20)->defaultValue(0),
            'bonus_2_amount' => $this->bigInteger(20)->defaultValue(0),
            'salary_1_prepay_sum' => $this->bigInteger(20)->defaultValue(0),
            'bonus_1_prepay_sum' => $this->bigInteger(20)->defaultValue(0),
            'salary_2_prepay_sum' => $this->bigInteger(20)->defaultValue(0),
            'bonus_2_prepay_sum' => $this->bigInteger(20)->defaultValue(0),
            'salary_1_pay_sum' => $this->bigInteger(20)->defaultValue(0),
            'bonus_1_pay_sum' => $this->bigInteger(20)->defaultValue(0),
            'salary_2_pay_sum' => $this->bigInteger(20)->defaultValue(0),
            'bonus_2_pay_sum' => $this->bigInteger(20)->defaultValue(0),
            'salary_1_expenses' => $this->bigInteger(20)->defaultValue(0),
            'bonus_1_expenses' => $this->bigInteger(20)->defaultValue(0),
            'salary_2_expenses' => $this->bigInteger(20)->defaultValue(0),
            'bonus_2_expenses' => $this->bigInteger(20)->defaultValue(0),
            'salary_1_prepay_date' => $this->date()->defaultValue(null),
            'bonus_1_prepay_date' => $this->date()->defaultValue(null),
            'salary_2_prepay_date' => $this->date()->defaultValue(null),
            'bonus_2_prepay_date' => $this->date()->defaultValue(null),
            'salary_1_pay_date' => $this->date()->defaultValue(null),
            'bonus_1_pay_date' => $this->date()->defaultValue(null),
            'salary_2_pay_date' => $this->date()->defaultValue(null),
            'bonus_2_pay_date' => $this->date()->defaultValue(null),
            'total_amount' => $this->bigInteger(20)->defaultValue(0),
            'total_prepay_sum' => $this->bigInteger(20)->defaultValue(0),
            'total_pay_sum' => $this->bigInteger(20)->defaultValue(0),
            'total_expenses' => $this->bigInteger(20)->defaultValue(0),
            'total_tax_ndfl' => $this->bigInteger(20)->defaultValue(0),
            'total_tax_social' => $this->bigInteger(20)->defaultValue(0),
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', 'employee_salary', ['employee_id', 'company_id', 'date']);
        $this->addForeignKey('FK_employee_salary_employee_company', 'employee_salary', [
            'employee_id',
            'company_id',
        ], 'employee_company', [
            'employee_id',
            'company_id',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('employee_salary');
    }
}
