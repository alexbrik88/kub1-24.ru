<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170208_122605_update_product_unit extends Migration
{
    public $tableName = 'product_unit';

    public function safeUp()
    {
        $this->update($this->tableName, ['name' => 'тн'], ['id' => 9]);
    }
    
    public function safeDown()
    {
        $this->update($this->tableName, ['name' => 'Тонна'], ['id' => 9]);
    }
}


