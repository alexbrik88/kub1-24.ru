<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170508_171153_alter_packingList_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%packing_list}}', 'basis_document_type_id', $this->integer()->defaultValue(null) . ' AFTER [[basis_document_date]]');

        $this->addForeignKey('FK_packingList_agreementType', '{{%packing_list}}', 'basis_document_type_id', '{{%agreement_type}}', 'id');

        $this->update('{{%packing_list}}', ['basis_document_type_id' => 1], "
            [[basis_name]] IS NOT NULL
            AND TRIM([[basis_name]]) <> ''
            AND TRIM([[basis_name]]) <> 'Счет'
        ");
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('FK_packingList_agreementType', '{{%packing_list}}');
        $this->dropColumn('{{%packing_list}}', 'basis_document_type_id');
    }
}
