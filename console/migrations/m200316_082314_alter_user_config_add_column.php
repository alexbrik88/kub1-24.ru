<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200316_082314_alter_user_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%user_config}}', 'table_view_payment_order', $this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%user_config}}', 'table_view_payment_order');
    }
}
