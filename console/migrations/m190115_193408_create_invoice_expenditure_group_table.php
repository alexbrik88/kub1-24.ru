<?php

use yii\db\Migration;

/**
 * Handles the creation of table `invoice_expenditure_group`.
 */
class m190115_193408_create_invoice_expenditure_group_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('invoice_expenditure_group', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50)->notNull(),
        ]);

        $this->insert('invoice_expenditure_group', [
            'id' => 1,
            'name' => 'Налоги',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('invoice_expenditure_group');
    }
}
