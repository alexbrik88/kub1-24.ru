<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181211_101920_add_uid_to_logistics_request extends Migration
{
    public $tableName = 'logistics_request';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'uid', 'CHAR(5) DEFAULT NULL COMMENT "unique id for logistic requests for quest" AFTER `id` ');
        $this->createIndex('uid_idx', $this->tableName, 'uid');

        foreach (\common\models\logisticsRequest\LogisticsRequest::find()->column() as $id) {
            do {
                $randString = \common\components\TextHelper::randString(5);
            } while (\common\models\logisticsRequest\LogisticsRequest::find()->andWhere(['uid' => $randString])->exists());
            $this->update($this->tableName, ['uid' => $randString], ['id' => $id]);
        }
    }
    
    public function safeDown()
    {
        $this->dropIndex('uid_idx', $this->tableName);
        $this->dropColumn($this->tableName, 'uid');
    }
}


