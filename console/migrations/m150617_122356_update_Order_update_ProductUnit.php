<?php

use yii\db\Schema;
use yii\db\Migration;

class m150617_122356_update_Order_update_ProductUnit extends Migration
{
    public function safeUp()
    {
        $this->execute('
            UPDATE `order` AS o
                JOIN product ON product.id = o.product_id
                SET o.unit_id = NULL
                WHERE product.product_unit_id IS NULL;
        ');
    }

    public function safeDown()
    {
        echo 'No down migration needed.', PHP_EOL;
    }
}
