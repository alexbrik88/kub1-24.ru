<?php
use console\components\db\Migration;

class m160802_095052_add_to_cash_bank_flows_reason_id extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cash_bank_flows', 'reason_id', $this->integer()->defaultValue(null));
        $this->addForeignKey('fk_cash_bank_flows_reason_id', 'cash_bank_flows', 'reason_id', 'cash_bank_reason_type', 'id');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('fk_cash_bank_flows_reason_id', 'cash_bank_flows');
        $this->dropColumn('cash_bank_flows', 'reason_id');
    }
}


