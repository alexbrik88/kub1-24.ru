<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210722_094331_alter_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_config', 'finance_model_round_numbers', $this->tinyInteger(1)->notNull()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('user_config', 'finance_model_round_numbers');
    }
}
