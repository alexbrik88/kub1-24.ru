<?php

use yii\db\Migration;

/**
 * Handles the creation of table `company_event`.
 */
class m180227_140908_create_company_first_event_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('company_first_event', [
            'company_id' => $this->integer()->notNull(),
            'event_id' => $this->integer()->notNull(),
            'step' => $this->integer()->notNull(),
            'visit' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
        ]);
        $this->addPrimaryKey('PRIMARY_KEY', 'company_first_event', ['company_id', 'event_id']);
        $this->addForeignKey(
            'FK_companyFirstEvent_company',
            'company_first_event',
            'company_id',
            'company',
            'id',
            'CASCADE'
        );
        $this->addForeignKey(
            'FK_storeUserCompany_event',
            'company_first_event',
            'event_id',
            'event',
            'id',
            'CASCADE'
        );
        $this->createIndex('step', 'company_first_event', 'step');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('company_first_event');
    }
}
