<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170127_102829_create_agreement extends Migration
{
    public function up()
    {
        $this->createTable('{{%agreement}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'contractor_id' => $this->integer()->notNull(),
            'document_date' => $this->date()->notNull(),
            'document_number' => $this->string(50)->notNull(),
            'document_name' => $this->string(255)->notNull(),
            'is_created' => $this->boolean()->notNull()->defaultValue(false),
            'created_at' => $this->integer()->notNull(),
            'created_by' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('fk_agreement_company', '{{%agreement}}', 'company_id', '{{%company}}', 'id');
        $this->addForeignKey('fk_agreement_contractor', '{{%agreement}}', 'contractor_id', '{{%contractor}}', 'id');
        $this->addForeignKey('fk_agreement_employee', '{{%agreement}}', 'created_by', '{{%employee}}', 'id');
    }

    public function down()
    {
        $this->dropTable('{{%agreement}}');
    }
}


