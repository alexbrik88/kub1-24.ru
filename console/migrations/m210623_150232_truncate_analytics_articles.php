<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210623_150232_truncate_analytics_articles extends Migration
{
    public function safeUp()
    {
        $this->execute('TRUNCATE TABLE `analytics_article`');
    }

    public function safeDown()
    {
        // no down
    }
}
