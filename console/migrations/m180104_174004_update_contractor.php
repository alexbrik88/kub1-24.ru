<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180104_174004_update_contractor extends Migration
{
    public function safeUp()
    {
        $this->execute("
            UPDATE {{contractor}} {{c}}
            INNER JOIN {{invoice}} {{i}}
            ON {{i}}.[[id]] = (
                SELECT {{t}}.[[id]]
                FROM {{invoice}} {{t}}
                WHERE {{t}}.[[company_id]] = {{c}}.[[company_id]]
                AND {{t}}.[[contractor_id]] = {{c}}.[[id]]
                AND {{t}}.[[type]] = {{c}}.[[type]]
                ORDER BY {{t}}.[[created_at]] DESC
                LIMIT 1
            )
            INNER JOIN {{employee_company}} {{e}}
            ON {{i}}.[[document_author_id]] = {{e}}.[[employee_id]] AND {{c}}.[[company_id]] = {{e}}.[[company_id]]
            SET {{c}}.[[responsible_employee_id]] = {{e}}.[[employee_id]]
            WHERE {{c}}.[[company_id]] IS NOT NULL
            AND {{c}}.[[responsible_employee_id]] IS NULL
            AND {{i}}.[[document_author_id]] IS NOT NULL
        ");
    }

    public function safeDown()
    {
        return true;
    }
}
