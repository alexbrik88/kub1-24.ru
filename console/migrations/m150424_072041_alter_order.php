<?php

use yii\db\Schema;
use yii\db\Migration;

class m150424_072041_alter_order extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('order', 'custom_declaration_number', Schema::TYPE_STRING . ' NOT NULL COMMENT "Номер таможенной декларации"');
    }
    
    public function safeDown()
    {
        $this->alterColumn('order', 'custom_declaration_number', Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Номер таможенной декларации"');
    }
}
