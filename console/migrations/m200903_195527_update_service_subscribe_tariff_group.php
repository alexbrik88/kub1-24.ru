<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200903_195527_update_service_subscribe_tariff_group extends Migration
{
    public $prioriry = [
        1, 11, 12,
        7, 13, 16,
        4, 15, 17,
        10, 3, 8,
        9
    ];

    public function safeUp()
    {
        $count = count($this->prioriry);
        $this->update('{{%service_subscribe_tariff_group}}', [
            'priority' => $count,
        ], [
            '<',
            'priority',
            $count,
        ]);

        foreach ($this->prioriry as $key => $id) {
            $this->update('{{%service_subscribe_tariff_group}}', [
                'priority' => $key,
            ], [
                'id' => $id,
            ]);
        }
    }

    public function safeDown()
    {
        //
    }
}
