<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180607_091006_alter_payment_reminder_message_contractor_alter_FK extends Migration
{
    public $tableName = 'payment_reminder_message_contractor';

    public function safeUp()
    {
        $this->dropForeignKey($this->tableName . '_company_id', $this->tableName);
        $this->dropForeignKey($this->tableName . '_contractor_id', $this->tableName);
        $this->addForeignKey($this->tableName . '_company_id', $this->tableName, 'company_id', 'company', 'id', 'CASCADE');
        $this->addForeignKey($this->tableName . '_contractor_id', $this->tableName, 'contractor_id', 'contractor', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_company_id', $this->tableName);
        $this->dropForeignKey($this->tableName . '_contractor_id', $this->tableName);
        $this->addForeignKey($this->tableName . '_company_id', $this->tableName, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tableName . '_contractor_id', $this->tableName, 'contractor_id', 'contractor', 'id', 'RESTRICT', 'CASCADE');
    }
}
