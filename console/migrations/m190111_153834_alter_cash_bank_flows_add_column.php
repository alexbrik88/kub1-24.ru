<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190111_153834_alter_cash_bank_flows_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cash_bank_flows', 'is_taxable', $this->boolean()->notNull()->defaultValue(true));

        $this->createIndex('cash_bank_flows_is_taxable', 'cash_bank_flows', 'is_taxable');
    }

    public function safeDown()
    {
        $this->dropColumn('cash_bank_flows', 'is_taxable');
    }
}
