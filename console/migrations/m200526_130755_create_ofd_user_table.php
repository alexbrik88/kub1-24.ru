<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%ofd_user}}`.
 */
class m200526_130755_create_ofd_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%ofd_user}}', [
            'employee_id' => $this->integer()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'ofd_id' => $this->integer()->notNull(),
            'auth_key' => $this->string(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'user_uid' => $this->string(),
            'company_uid' => $this->string(),
            '_data' => $this->json(),
            'PRIMARY KEY ([[employee_id]], [[company_id]], [[ofd_id]])',
        ]);

        $this->createIndex('ofd_user', '{{%ofd_user}}', ['user_uid', 'ofd_id'], true);
        $this->createIndex('auth_key', '{{%ofd_user}}', 'auth_key', true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%ofd_user}}');
    }
}
