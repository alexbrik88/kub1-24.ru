<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211205_183959_alter_invoice_add_column_2 extends Migration
{
    public function safeUp()
    {
        $table = 'invoice';
        $this->addColumn($table, 'max_related_document_date', $this->date()->defaultValue(null));
    }

    public function safeDown()
    {
        $table = 'invoice';
        $this->dropColumn($table, 'max_related_document_date');
    }
}
