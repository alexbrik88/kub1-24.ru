<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200928_090153_add_table_order_sales_invoice extends Migration
{
    public $tableName = 'order_sales_invoice';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'order_id' => $this->integer(),
	        'sales_invoice_id' => $this->integer(),
	        'product_id' => $this->integer(),
	        'quantity' => $this->decimal(20, 10),
        ]);

        $this->addForeignKey('FK_' . $this->tableName . '_to_order', $this->tableName, 'order_id', 'order', 'id');
        $this->addForeignKey('FK_' . $this->tableName . '_to_sales_invoice', $this->tableName, 'sales_invoice_id', 'sales_invoice', 'id');
        $this->addForeignKey('FK_' . $this->tableName . '_to_product', $this->tableName, 'product_id', 'product', 'id');
    }

    public function safeDown()
    {
        try {
            $this->dropForeignKey('FK_' . $this->tableName . '_to_order', $this->tableName);
            $this->dropForeignKey('FK_' . $this->tableName . '_to_sales_invoice', $this->tableName);
            $this->dropForeignKey('FK_' . $this->tableName . '_to_product', $this->tableName);
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        $this->dropTable($this->tableName);
    }
}
