<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160926_160216_alter_invoice_addColumn_ndsViewTypeId extends Migration
{
    public $tInvoice = '{{%invoice}}';

    public function safeUp()
    {
        $this->addColumn($this->tInvoice, 'nds_view_type_id', $this->integer(1));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tInvoice, 'nds_view_type_id');
    }
}


