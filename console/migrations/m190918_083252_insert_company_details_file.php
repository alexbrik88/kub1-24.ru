<?php

use console\components\db\Migration;
use yii\db\Query;
use yii\db\Schema;
use yii\helpers\FileHelper;

class m190918_083252_insert_company_details_file extends Migration
{
    public function safeUp()
    {
        $this->execute('SET foreign_key_checks = 0;');
        $this->truncateTable('{{%company_details_file}}');
        $this->execute('SET foreign_key_checks = 1;');

        $nameList = [
            1 => 'logo',
            2 => 'print',
            3 => 'chiefSignature',
            4 => 'chiefAccountantSignature',
        ];
        $dirList = [
            1 => 'logo',
            2 => 'print',
            3 => 'chief_signature',
            4 => 'chief_accountant_signature',
        ];

        $insert = [];

        $companyDirList = [];
        $uploadDir = \Yii::getAlias('@common/uploads/company_files');
        foreach (FileHelper::findDirectories($uploadDir, ['recursive' => false]) as $dir1) {
            foreach (FileHelper::findDirectories($dir1, ['recursive' => false]) as $dir2) {
                foreach (FileHelper::findDirectories($dir2, ['recursive' => false]) as $dir3) {
                    $companyDirList[] = $dir3;
                }
            }
        }
        $id = 1;
        foreach ($companyDirList as $basePath) {
            $companyId = basename($basePath);
            if ((new Query)->from('{{%company}}')->where(['id' => $companyId])->exists()) {
                $path = $basePath . DIRECTORY_SEPARATOR . 'profile';
                if (is_dir($path)) {
                    foreach ($nameList as $typeId => $fileName) {
                        $files = FileHelper::findFiles($path, [
                            'only' => ["/{$fileName}.*"],
                            'recursive' => false
                        ]);
                        $file = reset($files);
                        if ($file) {
                            $fileInfo = pathinfo($file);
                            $name = $fileInfo['basename'];
                            $size = filesize($file);
                            $time = filemtime($file);
                            $newPath = $basePath . DIRECTORY_SEPARATOR . $dirList[$typeId] . DIRECTORY_SEPARATOR . $id;
                            $newFile = $newPath . DIRECTORY_SEPARATOR . $name;
                            if (is_file($newPath)) {
                                unlink($newPath);
                            }
                            FileHelper::createDirectory($newPath);
                            copy($file, $newFile);
                            $insert[] = [
                                $id,
                                $companyId,
                                $typeId,
                                1,
                                $name,
                                isset($fileInfo['extension']) ? $fileInfo['extension'] : null,
                                $size,
                                $time,
                            ];
                            $id++;
                        }
                    }
                }
            }
        }

        if (!empty($insert)) {
            $this->batchInsert('{{%company_details_file}}', [
                'id',
                'company_id',
                'type_id',
                'status_id',
                'name',
                'ext',
                'size',
                'created_at',
            ], $insert);
        }
    }

    public function safeDown()
    {
        $this->execute('SET foreign_key_checks = 0;');
        $this->truncateTable('{{%company_details_file}}');
        $this->execute('SET foreign_key_checks = 1;');
    }
}
