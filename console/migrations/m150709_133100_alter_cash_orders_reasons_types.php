<?php

use yii\db\Schema;
use yii\db\Migration;

class m150709_133100_alter_cash_orders_reasons_types extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cash_orders_reasons_types', 'flow_type', Schema::TYPE_INTEGER . ' COMMENT "null - both, 0 - expense, 1 - income"');
        $this->dropForeignKey('cash_order_flows_order_id', 'cash_order_flows');

        $this->delete('cash_orders_reasons_types');
        $this->batchInsert('cash_orders_reasons_types', ['id', 'name', 'flow_type'], [
            [1, 'Оплата поставщику', 0],
            [2, 'Выдача наличных под отчёт', 0],
            [3, 'Выдача заработной платы', 0],
            [4, 'Сдача наличных в банк', 0],
            [5, 'Выдача займа', 0],
            [6, 'Возврат займа', 0],
            [7, 'Расчёт с учредителями', 0],
            [8, 'Компенсация за использование личного транспорта', 0],
            [9, 'Выручка', 1],
            [10, 'Розничная выручка', 1],
            [11, 'Возврат денег от клиента', 1],
            [12, 'Возврат денег из-под отчёта', 1],
            [13, 'Получение займа', 1],
            [14, 'Возврат займа', 1],
            [15, 'Взнос в Уставный капитал', 1],
            [16, 'Прочее', null],
        ]);
        $this->update('cash_order_flows', [
            'reason_id' => 16,
        ], 'reason_id = 8');

        $this->addForeignKey('cash_order_flows_order_id', 'cash_order_flows', 'reason_id', 'cash_orders_reasons_types', 'id', 'RESTRICT', 'CASCADE');
    }
    
    public function safeDown()
    {
        $this->dropColumn('cash_orders_reasons_types', 'flow_type');
        $this->dropForeignKey('cash_order_flows_order_id', 'cash_order_flows');
        $this->delete('cash_orders_reasons_types');
        $this->batchInsert('cash_orders_reasons_types', ['id', 'name'], [
            [1, 'Сдача наличных в банк'],
            [2, 'Выдача наличных под отчёт'],
            [3, 'Выдача заработной платы'],
            [4, 'Выручка'],
            [5, 'Розничная выручка'],
            [6, 'Возврат денег от клиента'],
            [7, 'Возврат денег из-под отчёта'],
            [8, 'Прочее'],
        ]);
        $this->update('cash_order_flows', [
            'reason_id' => 8,
        ], 'reason_id > 8');
        $this->addForeignKey('cash_order_flows_order_id', 'cash_order_flows', 'reason_id', 'cash_orders_reasons_types', 'id', 'RESTRICT', 'CASCADE');
    }
}
