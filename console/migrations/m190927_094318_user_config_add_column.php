<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190927_094318_user_config_add_column extends Migration
{
    public $tableName = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'product_zeroes_quantity', $this->boolean()->defaultValue(true));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'product_zeroes_quantity');
    }
}
