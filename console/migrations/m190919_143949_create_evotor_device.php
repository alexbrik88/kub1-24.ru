<?php

use console\components\db\Migration;

class m190919_143949_create_evotor_device extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{evotor_device}}', [
            'id' => "BINARY(16) NOT NULL PRIMARY KEY COMMENT 'UUID Эвотора'",
            'employee_id' => $this->integer()->notNull()->comment('ID покупателя сайта'),
            'store_id' => "BINARY(16) NOT NULL COMMENT 'ID магазина'",
            'name' => $this->string(400)->notNull()->comment('Название магазина'),
            'timezone_offset' => $this->integer()->unsigned()->notNull()->defaultValue(10800000)->comment('Временная зона'),
            'imei' => $this->bigInteger()->unsigned()->comment('IMEI устройства'),
            'firmware_version' => $this->string(11)->comment('Версия ПО'),
            'latitude' => $this->float()->unsigned()->comment('Координаты терминала (широта)'),
            'longitude' => $this->float()->unsigned()->comment('Координаты терминала (долгота)'),
            'created_at' => $this->integer()->unsigned(),
            'updated_at' => $this->integer()->unsigned(),
        ], "COMMENT 'Интеграция Эвотор: смарт-терминалы'");
        $this->addForeignKey(
            'evotor_device_employee_id', '{{evotor_device}}', 'employee_id',
            '{{employee}}', 'id', 'CASCADE', 'CASCADE'
        );
        $this->createIndex('evotor_device_store_id', '{{evotor_device}}', 'store_id');
    }

    public function down()
    {
        $this->dropTable('{{evotor_device}}');

    }
}
