<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200724_092048_append_log_entity_type_projects extends Migration
{
    public $tableName = 'log_entity_type';

    public function safeUp()
    {
        $this->insert($this->tableName, [
            'id' => '3',
            'name' => 'Проекты',
        ]);
    }

    public function safeDown()
    {
        $this->delete($this->tableName, [
            'id' => 3,
        ]);
    }
}
