<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210831_082043_alter_payments_request_order_add_column extends Migration
{
    public $table = 'payments_request';
    public $tablePayments = 'payments_request_order';

    public function safeUp()
    {
        $this->dropForeignKey('FK_' . $this->tablePayments . '_to_request', $this->tablePayments);
        $this->dropForeignKey('FK_' . $this->tablePayments . '_to_operation', $this->tablePayments);
        $this->dropPrimaryKey('PK_' . $this->tablePayments, $this->tablePayments);

        $this->execute("ALTER TABLE {$this->tablePayments} ADD `id` INT NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST");
        $this->addForeignKey('FK_' . $this->tablePayments . '_to_request', $this->tablePayments, 'payments_request_id', $this->table, 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_' . $this->tablePayments . '_to_request', $this->tablePayments);
        $this->execute("ALTER TABLE {$this->tablePayments} DROP COLUMN `id`");

        $this->addPrimaryKey('PK_' . $this->tablePayments, $this->tablePayments, ['payments_request_id', 'plan_operation_id']);
        $this->addForeignKey('FK_' . $this->tablePayments . '_to_request', $this->tablePayments, 'payments_request_id', $this->table, 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_' . $this->tablePayments . '_to_operation', $this->tablePayments, 'plan_operation_id', 'plan_cash_flows', 'id', 'CASCADE', 'CASCADE');
    }
}
