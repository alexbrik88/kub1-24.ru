<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%crm_client_refusal}}`.
 */
class m220204_102910_create_crm_client_refusal_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%crm_client_refusal}}', [
            'id' => $this->bigPrimaryKey(),
            'company_id' => $this->integer()->notNull(),
            'contractor_id' => $this->integer()->notNull(),
            'refusal_id' => $this->bigInteger(),
            'employee_id' => $this->integer()->notNull(),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'details' => $this->text(),
        ]);

        $this->createIndex('company_id', '{{%crm_client_refusal}}', 'company_id');
        $this->createIndex('contractor_id', '{{%crm_client_refusal}}', 'contractor_id');
        $this->createIndex('refusal_id', '{{%crm_client_refusal}}', 'refusal_id');
        $this->createIndex('employee_id', '{{%crm_client_refusal}}', 'employee_id');

        $this->addForeignKey('fk_crm_client_refusal__company_id', '{{%crm_client_refusal}}', 'company_id', '{{%company}}', 'id');
        $this->addForeignKey('fk_crm_client_refusal__contractor_id', '{{%crm_client_refusal}}', 'contractor_id', '{{%contractor}}', 'id');
        $this->addForeignKey('fk_crm_client_refusal__refusal_id', '{{%crm_client_refusal}}', 'refusal_id', '{{%crm_refusal}}', 'refusal_id');
        $this->addForeignKey('fk_crm_client_refusal__employee_id', '{{%crm_client_refusal}}', 'employee_id', '{{%employee}}', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%crm_client_refusal}}');
    }
}
