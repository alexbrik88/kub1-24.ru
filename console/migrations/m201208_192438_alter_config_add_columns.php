<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201208_192438_alter_config_add_columns extends Migration
{
    public $table = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->table, 'cash_column_paying', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 1');
        $this->addColumn($this->table, 'cash_column_income_expense', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'cash_column_paying');
        $this->dropColumn($this->table, 'cash_column_income_expense');
    }
}
