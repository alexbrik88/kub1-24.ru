<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pixel`.
 */
class m180214_091549_create_pixel_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('pixel', [
            'id' => $this->string(36)->notNull(),
            'el' => $this->string(36)->notNull(),
            'email' => $this->string(255),
            'company_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', 'pixel', 'id');
        $this->addForeignKey ('FK_pixel_company', 'pixel', 'company_id', 'company', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('pixel');
    }
}
