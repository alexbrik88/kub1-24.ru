<?php

use yii\db\Migration;

class m210628_060511_alter_crm_deal_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%crm_deal}}';

    /**
     * @inheritDoc
     */
    public function up(): void
    {
        $this->alterColumn(self::TABLE_NAME, 'contact_id', $this->bigInteger()->null());
    }

    /**
     * @inheritDoc
     */
    public function down(): void
    {
        $this->alterColumn(self::TABLE_NAME, 'contact_id', $this->bigInteger()->notNull());
    }
}
