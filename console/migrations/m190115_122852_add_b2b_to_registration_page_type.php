<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190115_122852_add_b2b_to_registration_page_type extends Migration
{
    public $tableName = 'registration_page_type';

    public function safeUp()
    {
        $this->insert($this->tableName, [
           'id' => 11,
           'name' => 'B2B модуль',
        ]);
    }

    public function safeDown()
    {
        $this->delete($this->tableName, ['id' => 11]);
    }
}
