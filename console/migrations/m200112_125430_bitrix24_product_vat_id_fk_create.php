<?php

use console\components\db\Migration;

class m200112_125430_bitrix24_product_vat_id_fk_create extends Migration
{
    const TABLE = '{{%bitrix24_product}}';

    public function safeUp()
    {
        $this->addForeignKey(
            'bitrix24_product_vat_id', self::TABLE, ['company_id', 'vat_id'],
            '{{%bitrix24_vat}}', ['company_id', 'id'],
            'CASCADE', 'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('bitrix24_product_vat_id', self::TABLE);
    }
}
