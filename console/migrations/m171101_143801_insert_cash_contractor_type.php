<?php

use console\components\db\Migration;
use yii\db\Schema;

class m171101_143801_insert_cash_contractor_type extends Migration
{
    public $tableName = 'cash_contractor_type';

    public function safeUp()
    {
        $this->insert($this->tableName, [
            'id' => 5,
            'name' => 'company',
            'text' => 'Компания',
        ]);
    }
    
    public function safeDown()
    {
        $this->delete($this->tableName, ['id' => 5]);
    }
}
