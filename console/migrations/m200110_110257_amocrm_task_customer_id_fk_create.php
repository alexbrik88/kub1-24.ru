<?php

use console\components\db\Migration;

class m200110_110257_amocrm_task_customer_id_fk_create extends Migration
{
    const TABLE = '{{%amocrm_task}}';

    public function up()
    {
        $this->addForeignKey(
            'amocrm_task_company_id', self::TABLE, 'company_id',
            '{{%company}}', 'id',
            'CASCADE', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey('amocrm_task_company_id', self::TABLE);
    }
}
