<?php

use console\components\db\Migration;
use yii\db\Schema;

class m220123_073858_alter_config_add_column extends Migration
{
    public $table = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->table, 'contr_inv_income_item', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table, 'contr_inv_expenditure_item', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table, 'contr_inv_industry', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table, 'contr_inv_sale_point', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table, 'contr_inv_project', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'contr_inv_income_item');
        $this->dropColumn($this->table, 'contr_inv_expenditure_item');
        $this->dropColumn($this->table, 'contr_inv_industry');
        $this->dropColumn($this->table, 'contr_inv_sale_point');
        $this->dropColumn($this->table, 'contr_inv_project');
    }
}
