<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201001_144306_alter_user_config_append_order_document_sales_invoice extends Migration
{
    public $tableName = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'order_document_sales_invoice', $this->boolean()->defaultValue(false)->after('order_document_packing_list'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'order_document_sales_invoice');
    }

}
