<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191121_071109_insert_service_discount_by_quantity extends Migration
{
    protected static $discount_data = [
        18 => [
            1 => 0,
            2 => 16.6666,
            3 => 25,
            4 => 33.3333,
            5 => 41.6666,
        ],
        19 => [
            1 => 0,
            2 => 16.6666,
            3 => 25,
            4 => 33.3333,
            5 => 41.6666,
        ],
        20 => [
            1 => 0,
            2 => 16.6666,
            3 => 25,
            4 => 33.3333,
            5 => 41.6666,
        ],
        21 => [
            1 => 0,
            2 => 10,
            3 => 20,
            4 => 25,
            5 => 35,
        ],
        22 => [
            1 => 0,
            2 => 10,
            3 => 20,
            4 => 25,
            5 => 35,
        ],
        23 => [
            1 => 0,
            2 => 10,
            3 => 20,
            4 => 25,
            5 => 35,
        ],
        24 => [
            1 => 0,
            2 => 11.1111,
            3 => 18.5185,
            4 => 25.9259,
            5 => 33.3333,
        ],
        25 => [
            1 => 0,
            2 => 11.1111,
            3 => 18.5185,
            4 => 25.9259,
            5 => 33.3333,
        ],
        26 => [
            1 => 0,
            2 => 11.1111,
            3 => 18.5185,
            4 => 25.9259,
            5 => 33.3333,
        ],
    ];

    public function safeUp()
    {
        $insert = [];

        foreach (self::$discount_data as $tariff_id => $value) {
            foreach ($value as $quantity => $percent) {
                $insert[] = [$tariff_id, $quantity, $percent];
            }
        }

        $this->batchInsert('{{%service_discount_by_quantity}}', [
            'tariff_id',
            'quantity',
            'percent',
        ], $insert);
    }

    public function safeDown()
    {
        $this->delete('{{%service_discount_by_quantity}}', [
            'tariff_id' => array_keys(self::$discount_data),
        ]);
    }
}
