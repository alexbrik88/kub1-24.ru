<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201023_075540_alter_invoice_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('invoice', 'is_surcharge', $this->boolean()->notNull()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('invoice', 'is_surcharge');
    }
}
