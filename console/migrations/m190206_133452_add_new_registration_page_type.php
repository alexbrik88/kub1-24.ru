<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190206_133452_add_new_registration_page_type extends Migration
{
    public $tableName = '{{%registration_page_type}}';

    public function safeUp()
    {
        $this->insert($this->tableName, [
            'id' => 15,
            'name' => 'Шаблон декларация ИП',
        ]);
    }

    public function safeDown()
    {
        $this->delete($this->tableName, ['id' => 15]);
    }
}
