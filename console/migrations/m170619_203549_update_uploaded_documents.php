<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170619_203549_update_uploaded_documents extends Migration
{
    public $tableName = 'uploaded_documents';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'total_amount_has_nds', $this->boolean()->defaultValue(1));
        $this->addColumn($this->tableName, 'total_amount_no_nds', $this->string()->defaultValue(null));
        $this->addColumn($this->tableName, 'total_amount_with_nds', $this->string()->defaultValue(null));
        $this->addColumn($this->tableName, 'total_amount_nds', $this->string()->defaultValue(null));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'total_amount_has_nds');
        $this->dropColumn($this->tableName, 'total_amount_no_nds');
        $this->dropColumn($this->tableName, 'total_amount_with_nds');
        $this->dropColumn($this->tableName, 'total_amount_nds');
    }
}


