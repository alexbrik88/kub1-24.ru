<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200625_135738_create_product_recommendation_abc extends Migration
{

    public function safeUp()
    {
        $tableOptions = null;
        if('mysql' == $this->db->driverName) $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

        $this->createTable('product_recommendation_abc', [
            'id' => $this->primaryKey(),
            'key' => $this->string(4)->notNull(),
            
            'irreducible_quantity' => $this->string()->defaultValue(''),
            'advertising' => $this->string()->defaultValue(''),
            'discount' => $this->string()->defaultValue(''),
            'action' => $this->string()->defaultValue(''),
            'selling_price' => $this->string()->defaultValue(''),
            'out_from_range' => $this->string()->defaultValue(''),
            'purchase_price' => $this->string()->defaultValue(''),

            'irreducible_quantity_txt' => $this->text(),
            'advertising_txt' => $this->text(),
            'discount_txt' => $this->text(),
            'action_txt' => $this->text(),
            'selling_price_txt' => $this->text(),
            'out_from_range_txt' => $this->text(),
            'purchase_price_txt' => $this->text(),

            'created_at' => $this->integer()->notNull()->unsigned(),
            'updated_at' => $this->integer()->notNull()->unsigned(),            

        ],$tableOptions);

        $this->createIndex('product_recommendation_abc_key_idx', 'product_recommendation_abc', 'key', true);
    }

    public function safeDown()
    {
        $this->dropTable('product_recommendation_abc');
    }
}
