<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201012_142846_alter_expense_item_flow_of_funds extends Migration
{
    public function safeUp()
    {
        $this->addColumn('expense_item_flow_of_funds', 'is_prepayment', $this->tinyInteger()->notNull()->defaultValue(0));

        $this->execute("
            UPDATE `expense_item_flow_of_funds` 
            SET `is_prepayment` = 1 
            WHERE `expense_item_id` IN (1,2,6,7,8,9,10,11,12,13,19,22,23,24,25,26,27,30,31,32,33,34,35,36,40,41,42,43,52,54,55,56,57,58,59,60,63,69,70,71,72,73,74,75,76,77,78,79,80,81,82,83,87,88)
              OR `expense_item_id` > 99
        ");
    }

    public function safeDown()
    {
        $this->dropColumn('expense_item_flow_of_funds', 'is_prepayment');
    }
}
