<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191026_062505_update_payment_table extends Migration
{
    public function safeUp()
    {
        Yii::$app->db->createCommand('SET session wait_timeout=60000')->execute();
        Yii::$app->db->createCommand('SET session interactive_timeout=60000')->execute();

        // Счета
        $this->execute("
			UPDATE service_payment t1
			LEFT JOIN service_payment t2
				ON t2.company_id = t1.company_id AND t2.payment_date < t1.payment_date AND t2.tariff_id IN (1,2,3) AND t2.is_confirmed = 1
			SET t1.by_novice_in_module = IF(t2.payment_date IS NULL, 1, 0) WHERE t1.tariff_id IN (1,2,3)        
        ");
        // Логистика
        $this->execute("
			UPDATE service_payment t1
			LEFT JOIN service_payment t2
				ON t2.company_id = t1.company_id AND t2.payment_date < t1.payment_date AND t2.tariff_id IN (5,6,7) AND t2.is_confirmed = 1
			SET t1.by_novice_in_module = IF(t2.payment_date IS NULL, 1, 0) WHERE t1.tariff_id IN (5,6,7)        
        ");
        // B2B
        $this->execute("
			UPDATE service_payment t1
			LEFT JOIN service_payment t2
				ON t2.company_id = t1.company_id AND t2.payment_date < t1.payment_date AND t2.tariff_id IN (8,9,10) AND t2.is_confirmed = 1
			SET t1.by_novice_in_module = IF(t2.payment_date IS NULL, 1, 0) WHERE t1.tariff_id IN (8,9,10)       
        ");
        // Бухгалтерия ИП
        $this->execute("
			UPDATE service_payment t1
			LEFT JOIN service_payment t2
				ON t2.company_id = t1.company_id AND t2.payment_date < t1.payment_date AND t2.tariff_id IN (11,12) AND t2.is_confirmed = 1
			SET t1.by_novice_in_module = IF(t2.payment_date IS NULL, 1, 0) WHERE t1.tariff_id IN (11,12)        
        ");
        // Аналитика
        $this->execute("
			UPDATE service_payment t1
			LEFT JOIN service_payment t2
				ON t2.company_id = t1.company_id AND t2.payment_date < t1.payment_date AND t2.tariff_id IN (13,14,15) AND t2.is_confirmed = 1
			SET t1.by_novice_in_module = IF(t2.payment_date IS NULL, 1, 0) WHERE t1.tariff_id IN (13,14,15)       
        ");
        // Нулевая отчетность ООО
        $this->execute("
			UPDATE service_payment t1
			LEFT JOIN service_payment t2
				ON t2.company_id = t1.company_id AND t2.payment_date < t1.payment_date AND t2.tariff_id IN (16,17) AND t2.is_confirmed = 1
			SET t1.by_novice_in_module = IF(t2.payment_date IS NULL, 1, 0) WHERE t1.tariff_id IN (16,17)
        ");
    }

    public function safeDown()
    {
        echo "This migration not rolled back\n";
    }
}
