<?php

use console\components\db\Migration;
use yii\db\Schema;

class m171108_083754_alter_order_upd_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('order_upd', 'quantity', $this->decimal(20, 10)->notNull()->after('order_id'));

        $this->execute("
            UPDATE {{order_upd}}
            INNER JOIN {{order}}
                ON {{order}}.[[id]] = {{order_upd}}.[[order_id]]
            SET {{order_upd}}.[[quantity]] = {{order}}.[[quantity]]
        ");

        $this->execute("
            INSERT INTO {{order_upd}} (
                upd_id,
                order_id,
                quantity,
                empty_unit_code,
                empty_unit_name,
                empty_product_code,
                empty_box_type
            )
            SELECT
                {{upd}}.[[id]],
                {{order}}.[[id]],
                {{order}}.[[quantity]],
                IF({{product}}.[[production_type]] = 0, 1, 0),
                IF({{product}}.[[production_type]] = 0, 1, 0),
                IF({{product}}.[[production_type]] = 0, 1, 0),
                IF({{product}}.[[production_type]] = 0, 1, 0)
            FROM {{upd}}
            LEFT JOIN {{invoice}}
                ON {{invoice}}.[[id]] = {{upd}}.[[invoice_id]]
            LEFT JOIN {{order}}
                ON {{order}}.[[invoice_id]] = {{invoice}}.[[id]]
            LEFT JOIN {{order_upd}}
                ON {{order_upd}}.[[order_id]] = {{order}}.[[id]]
            LEFT JOIN {{product}}
                ON {{product}}.[[id]] = {{order}}.[[product_id]]
            WHERE
                {{order_upd}}.[[order_id]] IS NULL
        ");
    }
    
    public function safeDown()
    {
        $this->dropColumn('order_upd', 'quantity');
    }
}
