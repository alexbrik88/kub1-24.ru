<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170619_103810_add_real_file_name_to_uploaded_documents extends Migration
{
    public $tableName = 'uploaded_documents';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'real_file_name', $this->string()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'real_file_name');
    }
}


