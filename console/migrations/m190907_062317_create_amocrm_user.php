<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190907_062317_create_amocrm_user extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{amocrm_user}}', [
            'user_id' => $this->integer()->unsigned()->notNull()->unique()->comment('ID пользователя AMOcrm'),
            'employee_id' => $this->integer()->notNull()->comment('ID полкупателя сайта'),
        ], "COMMENT 'Интеграция AMOcrm: связь пользователей AMO с клиентами сайта'");
        $this->addForeignKey(
            'amocrm_user_employee_id',
            '{{amocrm_user}}',
            'employee_id',
            '{{employee}}',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable('{{amocrm_user}}');

    }
}
