<?php

use yii\db\Migration;

class m210415_170710_alter_user_config_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%user_config}}';

    /**
     * @inheritDoc
     */
    public function up(): void
    {
        $this->addColumn(self::TABLE_NAME, 'card_operation_table', $this->boolean()->notNull()->defaultValue(false));
    }

    /**
     * @inheritDoc
     */
    public function down(): void
    {
        $this->dropColumn(self::TABLE_NAME, 'card_operation_table');
    }
}
