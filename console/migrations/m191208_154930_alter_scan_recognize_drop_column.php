<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191208_154930_alter_scan_recognize_drop_column extends Migration
{
    public $table = 'scan_recognize';

    public function safeUp()
    {
        $this->dropColumn($this->table, 'state');
    }

    public function safeDown()
    {
        $this->addColumn($this->table, 'state', $this->string(16));
    }
}
