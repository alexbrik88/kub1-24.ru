<?php

use yii\db\Exception;
use yii\db\Migration;

class m200708_094531_create_crm_client_activity_table extends Migration
{
    /** @var string Имя таблицы */
    private const TABLE_NAME = '{{%crm_client_activity}}';

    /** @var string[] Внешние столбцы */
    private const FK_COLUMNS = ['company_id'];

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'client_activity_id' => $this->bigPrimaryKey(),
            'company_id' => $this->integer()->null(),
            'name' => $this->string(64)->notNull(),
        ]);

        $this->createIndex('uk_name', self::TABLE_NAME, ['company_id', 'name'], true);
        $this->createIndex('ck_company_id', self::TABLE_NAME, self::FK_COLUMNS);
        $this->addForeignKey(
            'fk_crm_client_activity__company',
            self::TABLE_NAME,
            self::FK_COLUMNS,
            '{{%company}}',
            'id',
            'CASCADE',
            'CASCADE'
        );

        $this->db->createCommand()->batchInsert(self::TABLE_NAME, ['name'], $this->getRows())->execute();
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }

    /**
     * @return string[]
     */
    private function getRows(): array
    {
        return [
            ['Оптовая торговля'],
            ['Информация и связь'],
            ['Недвижимое имущество'],
            ['Транспортировка'],
            ['Хранение'],
            ['Деятельность в области спорта, отдыха и развлечений'],
            ['Реклама'],
            ['Творческая деятельность'],
            ['Маркетинг'],
            ['Медицина и здравоохранение'],
            ['Общепит'],
            ['Гостиничный бизнес'],
            ['Издательство'],
            ['Радиовещание и телевидение'],
            ['Производство'],
            ['Строительство'],
            ['Другое'],
        ];
    }
}
