<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201124_233048_alter_upd_change_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('upd', 'orders_sum', $this->bigInteger()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->alterColumn('upd', 'orders_sum', $this->integer()->defaultValue(0));
    }
}
