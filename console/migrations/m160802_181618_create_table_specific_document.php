<?php
use yii\db\Schema;
use yii\db\Migration;

class m160802_181618_create_table_specific_document extends Migration
{
    public function up()
    {
        $this->createTable('{{%specific_document}}', [
            'id' => Schema::TYPE_PK,
            'created_at' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Время создания записи"',
            'document_author_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "id создателя записи"',
            'document_date' => Schema::TYPE_DATE . ' COMMENT "Дата"',
            'name' => Schema::TYPE_STRING . '(100) NOT NULL COMMENT "Название"',
            'description' => Schema::TYPE_STRING . '(255) NOT NULL COMMENT "Описание"',
        ]);
    }

    public function down()
    {
        $this->dropTable('{{%specific_document}}');
    }
}
