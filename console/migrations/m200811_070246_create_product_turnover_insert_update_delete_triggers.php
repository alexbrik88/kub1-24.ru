<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200811_070246_create_product_turnover_insert_update_delete_triggers extends Migration
{
    /**
     * @var string
     */
    public $dropSql = '
        DROP TRIGGER IF EXISTS `product_turnover__order_act__after_insert`;
        DROP TRIGGER IF EXISTS `product_turnover__order_act__after_update`;
        DROP TRIGGER IF EXISTS `product_turnover__order_act__after_delete`;
        DROP TRIGGER IF EXISTS `product_turnover__order_packing_list__after_insert`;
        DROP TRIGGER IF EXISTS `product_turnover__order_packing_list__after_update`;
        DROP TRIGGER IF EXISTS `product_turnover__order_packing_list__after_delete`;
        DROP TRIGGER IF EXISTS `product_turnover__order_upd__after_insert`;
        DROP TRIGGER IF EXISTS `product_turnover__order_upd__after_update`;
        DROP TRIGGER IF EXISTS `product_turnover__order_upd__after_delete`;
        DROP TRIGGER IF EXISTS `product_turnover__invoice__after_update`;
        DROP TRIGGER IF EXISTS `product_turnover__act__after_update`;
        DROP TRIGGER IF EXISTS `product_turnover__packing_list__after_update`;
        DROP TRIGGER IF EXISTS `product_turnover__upd__after_update`;
        DROP TRIGGER IF EXISTS `product_turnover__product__after_update`;
    ';

    public function safeUp()
    {
        /**
         * Delet if exists
         */
        $this->execute($this->dropSql);

        /**
         * `order_act` triggers
         */
        $this->execute('
CREATE TRIGGER `product_turnover__order_act__after_insert` AFTER INSERT ON `order_act`
FOR EACH ROW
BEGIN
    INSERT INTO `product_turnover` (
        `document_id`,
        `order_id`,
        `order_table`,
        `document_table`,
        `invoice_id`,
        `contractor_id`,
        `company_id`,
        `date`,
        `type`,
        `production_type`,
        `is_invoice_actual`,
        `is_document_actual`,
        `purchase_price`,
        `price_one`,
        `quantity`,
        `total_amount`,
        `purchase_amount`,
        `year`,
        `month`,
        `product_id`
    ) SELECT
        NEW.`act_id`,
        NEW.`order_id`,
        "order_act",
        "act",
        `invoice`.`id`,
        `invoice`.`contractor_id`,
        `invoice`.`company_id`,
        `act`.`document_date`,
        `act`.`type`,
        `product`.`production_type`,
        IF(`invoice`.`is_deleted` = 0 AND `invoice`.`invoice_status_id` IN (1, 2, 3, 4, 6, 7, 8), 1, 0),
        IF(`act`.`status_out_id` IN (1, 2, 3, 4), 1, 0),
        IFNULL(`product`.`price_for_buy_with_nds`, 0),
        IF(`act`.`type` = 1, `order`.`purchase_price_with_vat`, `order`.`selling_price_with_vat`),
        IFNULL(NEW.`quantity`, 0),
        ROUND(IF(`act`.`type` = 1, `order`.`purchase_price_with_vat`, `order`.`selling_price_with_vat`) * NEW.`quantity`, IF(`invoice`.`price_precision` = 4, 2, 0)),
        IF(`invoice`.`type` = 2 AND `product`.`production_type` = 1, ROUND(NEW.`quantity` * IFNULL(`product`.`price_for_buy_with_nds`, 0)), 0),
        YEAR(`act`.`document_date`),
        MONTH(`act`.`document_date`),
        NEW.`product_id`
    FROM `order`
    LEFT JOIN `act` ON `act`.`id` = NEW.`act_id`
    LEFT JOIN `invoice` ON `invoice`.`id` = `order`.`invoice_id`
    LEFT JOIN `product` ON `product`.`id` = `order`.`product_id`
    WHERE `order`.`id` = NEW.`order_id`
    ON DUPLICATE KEY UPDATE
        `company_id` = VALUES(`company_id`),
        `date` = VALUES(`date`),
        `type` = VALUES(`type`),
        `production_type` = VALUES(`production_type`),
        `price_one` = VALUES(`price_one`),
        `quantity` = VALUES(`quantity`),
        `total_amount` = VALUES(`total_amount`),
        `year` = VALUES(`year`),
        `month` = VALUES(`month`),
        `product_id` = VALUES(`product_id`);
END;
        ');
        $this->execute('
CREATE TRIGGER `product_turnover__order_act__after_update` AFTER UPDATE ON `order_act`
FOR EACH ROW
BEGIN
    INSERT INTO `product_turnover` (
        `document_id`,
        `order_id`,
        `order_table`,
        `document_table`,
        `invoice_id`,
        `contractor_id`,
        `company_id`,
        `date`,
        `type`,
        `production_type`,
        `is_invoice_actual`,
        `is_document_actual`,
        `purchase_price`,
        `price_one`,
        `quantity`,
        `total_amount`,
        `purchase_amount`,
        `year`,
        `month`,
        `product_id`
    ) SELECT
        NEW.`act_id`,
        NEW.`order_id`,
        "order_act",
        "act",
        `invoice`.`id`,
        `invoice`.`contractor_id`,
        `invoice`.`company_id`,
        `act`.`document_date`,
        `act`.`type`,
        `product`.`production_type`,
        IF(`invoice`.`is_deleted` = 0 AND `invoice`.`invoice_status_id` IN (1, 2, 3, 4, 6, 7, 8), 1, 0),
        IF(`act`.`status_out_id` IN (1, 2, 3, 4), 1, 0),
        IFNULL(`product`.`price_for_buy_with_nds`, 0),
        IF(`act`.`type` = 1, `order`.`purchase_price_with_vat`, `order`.`selling_price_with_vat`),
        IFNULL(NEW.`quantity`, 0),
        ROUND(IF(`act`.`type` = 1, `order`.`purchase_price_with_vat`, `order`.`selling_price_with_vat`) * NEW.`quantity`, IF(`invoice`.`price_precision` = 4, 2, 0)),
        IF(`invoice`.`type` = 2 AND `product`.`production_type` = 1, ROUND(NEW.`quantity` * IFNULL(`product`.`price_for_buy_with_nds`, 0)), 0),
        YEAR(`act`.`document_date`),
        MONTH(`act`.`document_date`),
        NEW.`product_id`
    FROM `order`
    LEFT JOIN `act` ON `act`.`id` = NEW.`act_id`
    LEFT JOIN `invoice` ON `invoice`.`id` = `order`.`invoice_id`
    LEFT JOIN `product` ON `product`.`id` = `order`.`product_id`
    WHERE `order`.`id` = NEW.`order_id`
    ON DUPLICATE KEY UPDATE
        `company_id` = VALUES(`company_id`),
        `date` = VALUES(`date`),
        `type` = VALUES(`type`),
        `production_type` = VALUES(`production_type`),
        `price_one` = VALUES(`price_one`),
        `quantity` = VALUES(`quantity`),
        `total_amount` = VALUES(`total_amount`),
        `year` = VALUES(`year`),
        `month` = VALUES(`month`),
        `product_id` = VALUES(`product_id`);
END;
        ');
        $this->execute('
CREATE TRIGGER `product_turnover__order_act__after_delete` AFTER DELETE ON `order_act`
FOR EACH ROW
BEGIN
    DELETE FROM `product_turnover`
    WHERE `document_id` = OLD.`act_id`
    AND `order_id` = OLD.`order_id`
    AND `order_table` = "order_act";
END;
        ');

        /**
         * `order_packing_list` triggers
         */
        $this->execute('
CREATE TRIGGER `product_turnover__order_packing_list__after_insert` AFTER INSERT ON `order_packing_list`
FOR EACH ROW
BEGIN
    INSERT INTO `product_turnover` (
        `document_id`,
        `order_id`,
        `order_table`,
        `document_table`,
        `invoice_id`,
        `contractor_id`,
        `company_id`,
        `date`,
        `type`,
        `production_type`,
        `is_invoice_actual`,
        `is_document_actual`,
        `purchase_price`,
        `price_one`,
        `quantity`,
        `total_amount`,
        `purchase_amount`,
        `year`,
        `month`,
        `product_id`
    ) SELECT
        NEW.`packing_list_id`,
        NEW.`order_id`,
        "order_packing_list",
        "packing_list",
        `invoice`.`id`,
        `invoice`.`contractor_id`,
        `invoice`.`company_id`,
        `packing_list`.`document_date`,
        `packing_list`.`type`,
        `product`.`production_type`,
        IF(`invoice`.`is_deleted` = 0 AND `invoice`.`invoice_status_id` IN (1, 2, 3, 4, 6, 7, 8), 1, 0),
        IF(`packing_list`.`status_out_id` IN (1, 2, 3, 4), 1, 0),
        IFNULL(`product`.`price_for_buy_with_nds`, 0),
        IF(`packing_list`.`type` = 1, `order`.`purchase_price_with_vat`, `order`.`selling_price_with_vat`),
        IFNULL(NEW.`quantity`, 0),
        ROUND(IF(`packing_list`.`type` = 1, `order`.`purchase_price_with_vat`, `order`.`selling_price_with_vat`) * NEW.`quantity`, IF(`invoice`.`price_precision` = 4, 2, 0)),
        IF(`invoice`.`type` = 2 AND `product`.`production_type` = 1, ROUND(NEW.`quantity` * IFNULL(`product`.`price_for_buy_with_nds`, 0)), 0),
        YEAR(`packing_list`.`document_date`),
        MONTH(`packing_list`.`document_date`),
        NEW.`product_id`
    FROM `order`
    LEFT JOIN `packing_list` ON `packing_list`.`id` = NEW.`packing_list_id`
    LEFT JOIN `invoice` ON `invoice`.`id` = `order`.`invoice_id`
    LEFT JOIN `product` ON `product`.`id` = `order`.`product_id`
    WHERE `order`.`id` = NEW.`order_id`
    ON DUPLICATE KEY UPDATE
        `company_id` = VALUES(`company_id`),
        `date` = VALUES(`date`),
        `type` = VALUES(`type`),
        `production_type` = VALUES(`production_type`),
        `price_one` = VALUES(`price_one`),
        `quantity` = VALUES(`quantity`),
        `total_amount` = VALUES(`total_amount`),
        `year` = VALUES(`year`),
        `month` = VALUES(`month`),
        `product_id` = VALUES(`product_id`);
END;
        ');
        $this->execute('
CREATE TRIGGER `product_turnover__order_packing_list__after_update` AFTER UPDATE ON `order_packing_list`
FOR EACH ROW
BEGIN
    INSERT INTO `product_turnover` (
        `document_id`,
        `order_id`,
        `order_table`,
        `document_table`,
        `invoice_id`,
        `contractor_id`,
        `company_id`,
        `date`,
        `type`,
        `production_type`,
        `is_invoice_actual`,
        `is_document_actual`,
        `purchase_price`,
        `price_one`,
        `quantity`,
        `total_amount`,
        `purchase_amount`,
        `year`,
        `month`,
        `product_id`
    ) SELECT
        NEW.`packing_list_id`,
        NEW.`order_id`,
        "order_packing_list",
        "packing_list",
        `invoice`.`id`,
        `invoice`.`contractor_id`,
        `invoice`.`company_id`,
        `packing_list`.`document_date`,
        `packing_list`.`type`,
        `product`.`production_type`,
        IF(`invoice`.`is_deleted` = 0 AND `invoice`.`invoice_status_id` IN (1, 2, 3, 4, 6, 7, 8), 1, 0),
        IF(`packing_list`.`status_out_id` IN (1, 2, 3, 4), 1, 0),
        IFNULL(`product`.`price_for_buy_with_nds`, 0),
        IF(`packing_list`.`type` = 1, `order`.`purchase_price_with_vat`, `order`.`selling_price_with_vat`),
        IFNULL(NEW.`quantity`, 0),
        ROUND(IF(`packing_list`.`type` = 1, `order`.`purchase_price_with_vat`, `order`.`selling_price_with_vat`) * NEW.`quantity`, IF(`invoice`.`price_precision` = 4, 2, 0)),
        IF(`invoice`.`type` = 2 AND `product`.`production_type` = 1, ROUND(NEW.`quantity` * IFNULL(`product`.`price_for_buy_with_nds`, 0)), 0),
        YEAR(`packing_list`.`document_date`),
        MONTH(`packing_list`.`document_date`),
        NEW.`product_id`
    FROM `order`
    LEFT JOIN `packing_list` ON `packing_list`.`id` = NEW.`packing_list_id`
    LEFT JOIN `invoice` ON `invoice`.`id` = `order`.`invoice_id`
    LEFT JOIN `product` ON `product`.`id` = `order`.`product_id`
    WHERE `order`.`id` = NEW.`order_id`
    ON DUPLICATE KEY UPDATE
        `company_id` = VALUES(`company_id`),
        `date` = VALUES(`date`),
        `type` = VALUES(`type`),
        `production_type` = VALUES(`production_type`),
        `price_one` = VALUES(`price_one`),
        `quantity` = VALUES(`quantity`),
        `total_amount` = VALUES(`total_amount`),
        `year` = VALUES(`year`),
        `month` = VALUES(`month`),
        `product_id` = VALUES(`product_id`);
END;
        ');
        $this->execute('
CREATE TRIGGER `product_turnover__order_packing_list__after_delete` AFTER DELETE ON `order_packing_list`
FOR EACH ROW
BEGIN
    DELETE FROM `product_turnover`
    WHERE `document_id` = OLD.`packing_list_id`
    AND `order_id` = OLD.`order_id`
    AND `order_table` = "order_packing_list";
END;
        ');

        /**
         * `order_upd` triggers
         */
        $this->execute('
CREATE TRIGGER `product_turnover__order_upd__after_insert` AFTER INSERT ON `order_upd`
FOR EACH ROW
BEGIN
    INSERT INTO `product_turnover` (
        `document_id`,
        `order_id`,
        `order_table`,
        `document_table`,
        `invoice_id`,
        `contractor_id`,
        `company_id`,
        `date`,
        `type`,
        `production_type`,
        `is_invoice_actual`,
        `is_document_actual`,
        `purchase_price`,
        `price_one`,
        `quantity`,
        `total_amount`,
        `purchase_amount`,
        `year`,
        `month`,
        `product_id`
    ) SELECT
        NEW.`upd_id`,
        NEW.`order_id`,
        "order_upd",
        "upd",
        `invoice`.`id`,
        `invoice`.`contractor_id`,
        `invoice`.`company_id`,
        `upd`.`document_date`,
        `upd`.`type`,
        `product`.`production_type`,
        IF(`invoice`.`is_deleted` = 0 AND `invoice`.`invoice_status_id` IN (1, 2, 3, 4, 6, 7, 8), 1, 0),
        IF(`upd`.`status_out_id` IN (1, 2, 3, 4), 1, 0),
        IFNULL(`product`.`price_for_buy_with_nds`, 0),
        IF(`upd`.`type` = 1, `order`.`purchase_price_with_vat`, `order`.`selling_price_with_vat`),
        IFNULL(NEW.`quantity`, 0),
        ROUND(IF(`upd`.`type` = 1, `order`.`purchase_price_with_vat`, `order`.`selling_price_with_vat`) * NEW.`quantity`, IF(`invoice`.`price_precision` = 4, 2, 0)),
        IF(`invoice`.`type` = 2 AND `product`.`production_type` = 1, ROUND(NEW.`quantity` * IFNULL(`product`.`price_for_buy_with_nds`, 0)), 0),
        YEAR(`upd`.`document_date`),
        MONTH(`upd`.`document_date`),
        NEW.`product_id`
    FROM `order`
    LEFT JOIN `upd` ON `upd`.`id` = NEW.`upd_id`
    LEFT JOIN `invoice` ON `invoice`.`id` = `order`.`invoice_id`
    LEFT JOIN `product` ON `product`.`id` = `order`.`product_id`
    WHERE `order`.`id` = NEW.`order_id`
    ON DUPLICATE KEY UPDATE
        `company_id` = VALUES(`company_id`),
        `date` = VALUES(`date`),
        `type` = VALUES(`type`),
        `production_type` = VALUES(`production_type`),
        `price_one` = VALUES(`price_one`),
        `quantity` = VALUES(`quantity`),
        `total_amount` = VALUES(`total_amount`),
        `year` = VALUES(`year`),
        `month` = VALUES(`month`),
        `product_id` = VALUES(`product_id`);
END;
        ');
        $this->execute('
CREATE TRIGGER `product_turnover__order_upd__after_update` AFTER UPDATE ON `order_upd`
FOR EACH ROW
BEGIN
    INSERT INTO `product_turnover` (
        `document_id`,
        `order_id`,
        `order_table`,
        `document_table`,
        `invoice_id`,
        `contractor_id`,
        `company_id`,
        `date`,
        `type`,
        `production_type`,
        `is_invoice_actual`,
        `is_document_actual`,
        `purchase_price`,
        `price_one`,
        `quantity`,
        `total_amount`,
        `purchase_amount`,
        `year`,
        `month`,
        `product_id`
    ) SELECT
        NEW.`upd_id`,
        NEW.`order_id`,
        "order_upd",
        "upd",
        `invoice`.`id`,
        `invoice`.`contractor_id`,
        `invoice`.`company_id`,
        `upd`.`document_date`,
        `upd`.`type`,
        `product`.`production_type`,
        IF(`invoice`.`is_deleted` = 0 AND `invoice`.`invoice_status_id` IN (1, 2, 3, 4, 6, 7, 8), 1, 0),
        IF(`upd`.`status_out_id` IN (1, 2, 3, 4), 1, 0),
        IFNULL(`product`.`price_for_buy_with_nds`, 0),
        IF(`upd`.`type` = 1, `order`.`purchase_price_with_vat`, `order`.`selling_price_with_vat`),
        IFNULL(NEW.`quantity`, 0),
        ROUND(IF(`upd`.`type` = 1, `order`.`purchase_price_with_vat`, `order`.`selling_price_with_vat`) * NEW.`quantity`, IF(`invoice`.`price_precision` = 4, 2, 0)),
        IF(`invoice`.`type` = 2 AND `product`.`production_type` = 1, ROUND(NEW.`quantity` * IFNULL(`product`.`price_for_buy_with_nds`, 0)), 0),
        YEAR(`upd`.`document_date`),
        MONTH(`upd`.`document_date`),
        NEW.`product_id`
    FROM `order`
    LEFT JOIN `upd` ON `upd`.`id` = NEW.`upd_id`
    LEFT JOIN `invoice` ON `invoice`.`id` = `order`.`invoice_id`
    LEFT JOIN `product` ON `product`.`id` = `order`.`product_id`
    WHERE `order`.`id` = NEW.`order_id`
    ON DUPLICATE KEY UPDATE
        `company_id` = VALUES(`company_id`),
        `date` = VALUES(`date`),
        `type` = VALUES(`type`),
        `production_type` = VALUES(`production_type`),
        `price_one` = VALUES(`price_one`),
        `quantity` = VALUES(`quantity`),
        `total_amount` = VALUES(`total_amount`),
        `year` = VALUES(`year`),
        `month` = VALUES(`month`),
        `product_id` = VALUES(`product_id`);
END;
        ');
        $this->execute('
CREATE TRIGGER `product_turnover__order_upd__after_delete` AFTER DELETE ON `order_upd`
FOR EACH ROW
BEGIN
    DELETE FROM product_turnover
    WHERE `document_id` = OLD.`upd_id`
    AND `order_id` = OLD.`order_id`
    AND `order_table` = "order_upd";
END;
        ');

        /**
         * `invoice` triggers
         */
        $this->execute('
CREATE TRIGGER `product_turnover__invoice__after_update` AFTER UPDATE ON `invoice`
FOR EACH ROW
BEGIN
    UPDATE `product_turnover`
    SET `is_invoice_actual` = IF(NEW.`is_deleted` = 0 AND NEW.`invoice_status_id` IN (1, 2, 3, 4, 6, 7, 8), 1, 0)
    WHERE `invoice_id` = NEW.`id`;
END;
        ');

        /**
         * `act` triggers
         */
        $this->execute('
CREATE TRIGGER `product_turnover__act__after_update` AFTER UPDATE ON `act`
FOR EACH ROW
BEGIN
    UPDATE `product_turnover`
    SET `is_document_actual` = IF(NEW.`status_out_id` IN (1, 2, 3, 4), 1, 0)
    WHERE `document_id` = NEW.`id`
    AND `document_table` = "act";
END;
        ');

        /**
         * `packing_list` triggers
         */
        $this->execute('
CREATE TRIGGER `product_turnover__packing_list__after_update` AFTER UPDATE ON `packing_list`
FOR EACH ROW
BEGIN
    UPDATE `product_turnover`
    SET `is_document_actual` = IF(NEW.`status_out_id` IN (1, 2, 3, 4), 1, 0)
    WHERE `document_id` = NEW.`id`
    AND `document_table` = "packing_list";
END;
        ');

        /**
         * `upd` triggers
         */
        $this->execute('
CREATE TRIGGER `product_turnover__upd__after_update` AFTER UPDATE ON `upd`
FOR EACH ROW
BEGIN
    UPDATE `product_turnover`
    SET `is_document_actual` = IF(NEW.`status_out_id` IN (1, 2, 3, 4), 1, 0)
    WHERE `document_id` = NEW.`id`
    AND `document_table` = "upd";
END;
        ');

        /**
         * `product` triggers
         */
        $this->execute('
CREATE TRIGGER `product_turnover__product__after_update` AFTER UPDATE ON `product`
FOR EACH ROW
BEGIN
    IF
        IFNULL(NEW.`price_for_buy_with_nds`, 0) > 0
        AND (IFNULL(OLD.`price_for_buy_with_nds`, 0) <> IFNULL(NEW.`price_for_buy_with_nds`, 0))
    THEN
        UPDATE `product_turnover`
        SET `purchase_price` = NEW.`price_for_buy_with_nds`,
            `purchase_amount` = ROUND(`quantity` * NEW.`price_for_buy_with_nds`)
        WHERE `product_id` = NEW.`id`
        AND `type` = 2
        AND `production_type` = 1
        AND `purchase_price` = 0;
    END IF;
END;
        ');
    }

    public function safeDown()
    {
        $this->execute($this->dropSql);
    }
}
