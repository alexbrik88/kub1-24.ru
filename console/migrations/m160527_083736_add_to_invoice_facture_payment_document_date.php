<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160527_083736_add_to_invoice_facture_payment_document_date extends Migration
{
    public $tInvoiceFacture = 'invoice_facture';

    public function safeUp()
    {
        $this->addColumn($this->tInvoiceFacture, 'payment_document_date', $this->string());
        $this->update($this->tInvoiceFacture, [
            'payment_document_date' => new \yii\db\Expression('DATE (document_date)'),
        ]);
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tInvoiceFacture, 'payment_document_date');
    }
}


