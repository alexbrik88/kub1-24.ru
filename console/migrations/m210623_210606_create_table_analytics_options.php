<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210623_210606_create_table_analytics_options extends Migration
{
    public $table = 'analytics_options';
    public $PK = 'PK_analytics_options';
    public $FK = 'FK_analytics_options_company_id';

    public function safeUp()
    {
        $this->createTable($this->table, [
            'company_id' => $this->integer()->notNull(),
            'key' => $this->integer()->notNull(),
            'value' => $this->char(16),
        ]);

        $this->addPrimaryKey($this->PK, $this->table, ['company_id', 'key']);
        $this->addForeignKey($this->FK, $this->table, 'company_id', 'company', 'id', 'CASCADE');

        $this->execute("INSERT INTO `{$this->table}` (`company_id`, `key`, `value`) SELECT DISTINCT company_id, 1 AS `key`, 0 AS `value` FROM `analytics_article`");
    }

    public function safeDown()
    {
        $this->dropForeignKey($this->FK, $this->table);
        $this->dropTable($this->table);
    }

}
