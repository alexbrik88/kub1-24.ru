<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190607_072816_alter_payment_order_add_column extends Migration
{
    public $tablePayments = 'payment_order';
    public function safeUp()
    {
        $this->addColumn($this->tablePayments, 'banking_guid', $this->string(36)->notNull()->defaultValue(''));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tablePayments, 'banking_guid');
    }
}
