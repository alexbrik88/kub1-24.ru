<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200617_162130_add_registration_page_type extends Migration
{
    public $tableName = '{{%registration_page_type}}';

    public function safeUp()
    {
        $this->insert($this->tableName, [
            'id' => 31,
            'name' => 'Прайс-лист. Лэндинг',
        ]);
    }

    public function safeDown()
    {
        $this->delete($this->tableName, ['id' => 31]);
    }
}
