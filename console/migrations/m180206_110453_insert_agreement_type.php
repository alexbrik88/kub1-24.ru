<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180206_110453_insert_agreement_type extends Migration
{
    public function safeUp()
    {
        $this->insert('agreement_type', [
            'id' => 4,
            'name' => 'Контракт',
            'name_dative' => 'контракту',
        ]);
    }

    public function safeDown()
    {
        $this->delete('agreement_type', ['id' => 4]);
    }
}
