<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210719_122755_create_table_finance_model_2 extends Migration
{
    const MODEL_GROUP = 'finance_plan_group';
    const MODEL = 'finance_plan';
    const INCOME_ITEM  = 'finance_plan_income_item';
    const EXPENDITURE_ITEM = 'finance_plan_expenditure_item';
    const INCOME_ITEM_AUTOFILL = 'finance_plan_income_item_autofill';
    const EXPENDITURE_ITEM_AUTOFILL = 'finance_plan_expenditure_item_autofill';

    public function safeUp()
    {
        /*
         * Finance Model Group
         */
        $this->createTable(self::MODEL_GROUP, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'employee_id' => $this->integer()->notNull(),
            'name' => $this->string(255)->notNull(),
            'year_from' => $this->smallInteger(),
            'month_from' => $this->smallInteger(),
            'year_till' => $this->smallInteger(),
            'month_till' => $this->smallInteger(),
            'currency_id' => $this->string(3)->notNull()->defaultValue('643'),
            'created_at' =>  $this->timestamp()->defaultExpression('NOW()'),
            'comment' => $this->text()->defaultValue('')
        ]);

        $this->addForeignKey('FK_'.self::MODEL_GROUP.'_to_company', self::MODEL_GROUP, 'company_id', 'company', 'id');
        $this->addForeignKey('FK_'.self::MODEL_GROUP.'_to_employee', self::MODEL_GROUP, 'employee_id', 'employee', 'id');

        /*
         * Finance Model
         */
        $this->createTable(self::MODEL, [
            'id' => $this->primaryKey(),
            'model_group_id' => $this->integer()->notNull(),
            'industry_id' => $this->integer()->notNull(),
            'name' => $this->string(255)->notNull(),
        ]);

        $this->addForeignKey('FK_'.self::MODEL.'_to_model_group', self::MODEL, 'model_group_id', self::MODEL_GROUP, 'id', 'CASCADE', 'CASCADE');

        /*
         * Income Item
         */
        $this->createTable(self::INCOME_ITEM, [
            'id' => $this->primaryKey(),
            'model_id' => $this->integer()->notNull(),
            'type' => $this->tinyInteger()->notNull()->comment('1 - новые продажи, 2 - повторные продажи, 3 - средний чек'),
            'data' => $this->text()
        ]);

        $this->addForeignKey('FK_'.self::INCOME_ITEM.'_to_model', self::INCOME_ITEM, 'model_id', self::MODEL, 'id', 'CASCADE', 'CASCADE');

        /*
         * Expenditure Item
         */
        $this->createTable(self::EXPENDITURE_ITEM, [
            'id' => $this->primaryKey(),
            'model_id' => $this->integer()->notNull(),
            'expenditure_item_id' => $this->integer()->notNull(),
            'type' => $this->tinyInteger()->notNull()->comment('1 - переменные расходы, 2 - постоянные расходы, 3 - себестоимость'),
            'data' => $this->text()
        ]);

        $this->addForeignKey('FK_'.self::EXPENDITURE_ITEM.'_to_model', self::EXPENDITURE_ITEM, 'model_id', self::MODEL, 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_'.self::EXPENDITURE_ITEM.'_to_expenditure_item', self::EXPENDITURE_ITEM, 'expenditure_item_id', 'invoice_expenditure_item', 'id', 'CASCADE', 'CASCADE');

        /*
         * Autofill income item
         */
        $this->createTable(self::INCOME_ITEM_AUTOFILL, [
            'model_id' => $this->integer(11)->notNull(),
            'row_id' => $this->integer()->notNull(),
            'start_month' => $this->integer(11)->notNull(),
            'start_amount' => $this->bigInteger(20),
            'amount' => $this->bigInteger(20),
            'limit_amount' => $this->bigInteger(20),
            'action' => $this->tinyInteger()->unsigned()->notNull(),
        ]);

        $this->addPrimaryKey('PK_'.self::INCOME_ITEM_AUTOFILL, self::INCOME_ITEM_AUTOFILL, ['model_id', 'row_id']);
        $this->addForeignKey('FK_'.self::INCOME_ITEM_AUTOFILL.'_to_model', self::INCOME_ITEM_AUTOFILL, 'model_id', self::MODEL, 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_'.self::INCOME_ITEM_AUTOFILL.'_to_item', self::INCOME_ITEM_AUTOFILL, 'row_id', self::INCOME_ITEM, 'id', 'CASCADE', 'CASCADE');

        /*
         * Autofill expenditure item
         */
        $this->createTable(self::EXPENDITURE_ITEM_AUTOFILL, [
            'model_id' => $this->integer(11)->notNull(),
            'row_id' => $this->integer()->notNull(),
            'start_month' => $this->integer(11)->notNull(),
            'start_amount' => $this->bigInteger(20),
            'amount' => $this->bigInteger(20),
            'limit_amount' => $this->bigInteger(20),
            'action' => $this->tinyInteger()->unsigned()->notNull(),
        ]);

        $this->addPrimaryKey('PK_'.self::EXPENDITURE_ITEM_AUTOFILL, self::EXPENDITURE_ITEM_AUTOFILL, ['model_id', 'row_id']);
        $this->addForeignKey('FK_'.self::EXPENDITURE_ITEM_AUTOFILL.'_to_model', self::EXPENDITURE_ITEM_AUTOFILL, 'model_id', self::MODEL, 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_'.self::EXPENDITURE_ITEM_AUTOFILL.'_to_item', self::EXPENDITURE_ITEM_AUTOFILL, 'row_id', self::EXPENDITURE_ITEM, 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_'.self::EXPENDITURE_ITEM.'_to_expenditure_item', self::EXPENDITURE_ITEM);
        $this->dropForeignKey('FK_'.self::EXPENDITURE_ITEM.'_to_model', self::EXPENDITURE_ITEM);
        $this->dropForeignKey('FK_'.self::INCOME_ITEM.'_to_model', self::INCOME_ITEM);
        $this->dropForeignKey('FK_'.self::MODEL.'_to_model_group', self::MODEL);
        $this->dropForeignKey('FK_'.self::MODEL_GROUP.'_to_company', self::MODEL_GROUP);
        $this->dropForeignKey('FK_'.self::MODEL_GROUP.'_to_employee', self::MODEL_GROUP);
        $this->dropForeignKey('FK_'.self::INCOME_ITEM_AUTOFILL.'_to_model', self::INCOME_ITEM_AUTOFILL);
        $this->dropForeignKey('FK_'.self::INCOME_ITEM_AUTOFILL.'_to_item', self::INCOME_ITEM_AUTOFILL);
        $this->dropForeignKey('FK_'.self::EXPENDITURE_ITEM_AUTOFILL.'_to_model', self::EXPENDITURE_ITEM_AUTOFILL);
        $this->dropForeignKey('FK_'.self::EXPENDITURE_ITEM_AUTOFILL.'_to_item', self::EXPENDITURE_ITEM_AUTOFILL);

        $this->dropTable(self::EXPENDITURE_ITEM);
        $this->dropTable(self::INCOME_ITEM);
        $this->dropTable(self::MODEL);
        $this->dropTable(self::MODEL_GROUP);
        $this->dropTable(self::INCOME_ITEM_AUTOFILL);
        $this->dropTable(self::EXPENDITURE_ITEM_AUTOFILL);
    }
}
