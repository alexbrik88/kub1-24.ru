<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201223_094017_insert_checking_accountant extends Migration
{
    public function safeUp()
    {
        $this->execute('
            INSERT INTO {{checking_accountant}} (
                [[created_at]],
                [[updated_at]],
                [[company_id]],
                [[currency_id]],
                [[is_foreign_bank]],
                [[bank_name]],
                [[bank_address]],
                [[rs]],
                [[type]],
                [[iban]],
                [[swift]],
                [[bank_name_en]],
                [[bank_address_en]],
                [[corr_rs]],
                [[corr_swift]],
                [[corr_bank_name]],
                [[corr_bank_address]]
            )
            SELECT
                {{foreign_currency_account}}.[[created_at]],
                {{foreign_currency_account}}.[[updated_at]],
                {{foreign_currency_account}}.[[company_id]],
                {{foreign_currency_account}}.[[currency_id]],
                {{foreign_currency_account}}.[[is_foreign_bank]],
                {{foreign_currency_account}}.[[bank_name]],
                {{foreign_currency_account}}.[[bank_address]],
                {{foreign_currency_account}}.[[rs]],
                {{foreign_currency_account}}.[[type]],
                {{foreign_currency_account}}.[[rs]],
                {{foreign_currency_account}}.[[swift]],
                {{foreign_currency_account}}.[[bank_name]],
                {{foreign_currency_account}}.[[bank_address]],
                {{foreign_currency_account}}.[[corr_rs]],
                {{foreign_currency_account}}.[[corr_swift]],
                {{foreign_currency_account}}.[[corr_bank_name]],
                {{foreign_currency_account}}.[[corr_bank_address]]
            FROM {{foreign_currency_account}}
        ');
    }

    public function safeDown()
    {
        //
    }
}
