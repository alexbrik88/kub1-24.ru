<?php

use yii\db\Migration;

class m210420_224053_create_card_bill_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%card_bill}}';

    /**
     * @inheritDoc
     */
    public function up(): void
    {
        $this->createTable(self::TABLE_NAME, [
            'id' => $this->bigPrimaryKey()->notNull(),
            'account_id' => $this->bigInteger()->notNull(),
            'uuid' => $this->string()->notNull(),
            'name' => $this->string()->notNull(),
        ]);

        $this->createIndex('ck_account_id', self::TABLE_NAME, ['account_id']);
        $this->createIndex('uk_card_invoice', self::TABLE_NAME, ['account_id', 'uuid']);
        $this->addForeignKey(
            'fk_card_invoice__card_account',
            self::TABLE_NAME,
            ['account_id'],
            '{{%card_account}}',
            ['id'],
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritDoc
     */
    public function down(): void
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
