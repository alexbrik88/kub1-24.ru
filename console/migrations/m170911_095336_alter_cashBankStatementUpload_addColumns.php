<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170911_095336_alter_cashBankStatementUpload_addColumns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cash_bank_statement_upload}}', 'account_id', $this->integer()->defaultValue(null)->after('company_id'));
        $this->addColumn('{{%cash_bank_statement_upload}}', 'period_from', $this->date()->defaultValue(null)->after('rs'));
        $this->addColumn('{{%cash_bank_statement_upload}}', 'period_till', $this->date()->defaultValue(null)->after('period_from'));

        $this->dropForeignKey('FK_bank_statement_upload_to_employee', '{{%cash_bank_statement_upload}}');
        $this->dropForeignKey('FK_bank_statement_upload_to_company', '{{%cash_bank_statement_upload}}');

        $this->addForeignKey('FK_bank_statement_upload_to_employee', '{{%cash_bank_statement_upload}}', 'created_by', '{{%employee}}', 'id', 'CASCADE');
        $this->addForeignKey('FK_bank_statement_upload_to_company', '{{%cash_bank_statement_upload}}', 'company_id', '{{%company}}', 'id', 'CASCADE');
        $this->addForeignKey('FK_bankStatementUpload_checkingAccountant', '{{%cash_bank_statement_upload}}', 'account_id', '{{%checking_accountant}}', 'id', 'SET NULL');



        $this->execute("
            UPDATE {{%cash_bank_statement_upload}} {{statement}}
            LEFT JOIN {{%checking_accountant}} {{account}}
                ON {{statement}}.[[company_id]] = {{account}}.[[company_id]]
                AND {{statement}}.[[bik]] = {{account}}.[[bik]]
                AND {{statement}}.[[rs]] = {{account}}.[[rs]]
            SET {{statement}}.[[account_id]] = {{account}}.[[id]]
        ");
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('FK_bankStatementUpload_checkingAccountant', '{{%cash_bank_statement_upload}}');
        $this->dropForeignKey('FK_bank_statement_upload_to_employee', '{{%cash_bank_statement_upload}}');
        $this->dropForeignKey('FK_bank_statement_upload_to_company', '{{%cash_bank_statement_upload}}');

        $this->addForeignKey('FK_bank_statement_upload_to_employee', '{{%cash_bank_statement_upload}}', 'created_by', '{{%employee}}', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('FK_bank_statement_upload_to_company', '{{%cash_bank_statement_upload}}', 'company_id', '{{%company}}', 'id', 'RESTRICT', 'CASCADE');

        $this->dropColumn('{{%cash_bank_statement_upload}}', 'account_id');
        $this->dropColumn('{{%cash_bank_statement_upload}}', 'period_from');
        $this->dropColumn('{{%cash_bank_statement_upload}}', 'period_till');
    }
}
