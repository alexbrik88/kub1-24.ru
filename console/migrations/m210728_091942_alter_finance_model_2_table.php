<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210728_091942_alter_finance_model_2_table extends Migration
{
    const TABLE = 'finance_plan_group';

    public function safeUp()
    {
        $this->addColumn(self::TABLE, 'total_revenue', $this->bigInteger());
        $this->addColumn(self::TABLE, 'total_net_profit', $this->bigInteger());
        $this->addColumn(self::TABLE, 'total_marginality', $this->float());
    }

    public function safeDown()
    {
        $this->dropColumn(self::TABLE, 'total_revenue');
        $this->dropColumn(self::TABLE, 'total_marginality');
        $this->dropColumn(self::TABLE, 'total_net_profit');
    }
}
