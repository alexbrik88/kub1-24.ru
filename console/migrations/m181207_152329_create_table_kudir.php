<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181207_152329_create_table_kudir extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        // kudir_status
        $this->createTable('{{%kudir_status}}', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ], $tableOptions);

        $this->batchInsert('{{%kudir_status}}', ['id', 'name'], [
            [1, 'Создана'],
            [2, 'Распечатана'],
            [3, 'Передана'],
            [4, 'Принята'],
            [5, 'Подписана'],
        ]);

        // kudir
        $this->createTable('{{%kudir}}', [
            'id' => Schema::TYPE_PK,

            'company_id' => Schema::TYPE_INTEGER . '(11) NOT NULL',

            'created_at' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'document_author_id' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'status_id' => Schema::TYPE_INTEGER . '(11) DEFAULT NULL',
            'status_updated_at' => Schema::TYPE_INTEGER . '(11) DEFAULT NULL',
            'status_author_id' => Schema::TYPE_INTEGER . '(11) DEFAULT NULL',

            'document_date' => Schema::TYPE_DATE . ' NOT NULL',
            'tax_year' => Schema::TYPE_INTEGER . '(4) NOT NULL',
        ]);

        $this->addForeignKey('FK_kudir_to_company', '{{%kudir}}', 'company_id', 'company', 'id');
        $this->addForeignKey('FK_kudir_to_kudir_status', '{{%kudir}}', 'status_id', 'kudir_status', 'id');
        $this->addForeignKey('FK_kudir_to_document_author', '{{%kudir}}', 'document_author_id', 'employee', 'id');
        $this->addForeignKey('FK_kudir_to_status_author', '{{%kudir}}', 'status_author_id', 'employee', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_kudir_to_company', '{{%kudir}}');
        $this->dropForeignKey('FK_kudir_to_kudir_status', '{{%kudir}}');
        $this->dropForeignKey('FK_kudir_to_document_author', '{{%kudir}}');
        $this->dropForeignKey('FK_kudir_to_status_author', '{{%kudir}}');


        $this->dropTable('{{%kudir}}');
        $this->dropTable('{{%kudir_status}}');
    }
}


