<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190209_131447_create_ofdParams extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%ofd_params}}', [
            'company_id' => $this->integer()->notNull(),
            'ofd_alias' => $this->string(16)->notNull(),
            'param_name' => $this->string()->notNull(),
            'param_value' => $this->string(),
            'created_by' => $this->integer(),
            'updated_by' => $this->integer(),
            'created_at' => $this->integer(),
            'updated_at' => $this->integer(),
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', '{{%ofd_params}}', ['company_id', 'ofd_alias', 'param_name']);

        $this->addForeignKey('FK_ofdParams_company', '{{%ofd_params}}', 'company_id', '{{%company}}', 'id', 'CASCADE');
        $this->addForeignKey('FK_ofdParams_employee_1', '{{%ofd_params}}', 'created_by', '{{%employee}}', 'id');
        $this->addForeignKey('FK_ofdParams_employee_2', '{{%ofd_params}}', 'updated_by', '{{%employee}}', 'id');
    }

    public function safeDown()
    {
        $this->dropTable('{{%ofd_params}}');
    }
}
