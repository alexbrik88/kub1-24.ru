<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211126_122103_alter_rent_agreement_alter_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%rent_agreement}}', 'amount', $this->bigInteger());
    }

    public function safeDown()
    {
        $this->alterColumn('{{%rent_agreement}}', 'amount', $this->float());
    }
}
