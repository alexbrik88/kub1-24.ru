<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160819_105617_insert_into_product_unit extends Migration
{
    public $tProductUnit = 'product_unit';

    public function safeUp()
    {
        $this->insert($this->tProductUnit, [
            'id' => YII_ENV_PROD ? 11 : 10,
            'name' => 'пог.м',
            'code_okei' => '018',
            'object_guid' => \frontend\modules\export\models\one_c\OneCExport::generateGUID(),
        ]);
    }

    public function safeDown()
    {
        $this->delete($this->tProductUnit, ['id' => 10]);
    }
}


