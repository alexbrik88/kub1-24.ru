<?php

use console\components\db\Migration;

class m201125_173310_alter_credit_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%credit}}';

    /** @var string */
    private const DEFAULT_CURRENCY_VALUE = '643';

    /**
     * @inheritDoc
     */
    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'payment_debt', $this->double(2)->notNull()->defaultValue(0)->after('payment_amount'));
        $this->addColumn(self::TABLE_NAME, 'payment_date', $this->date()->null()->after('payment_debt'));
        $this->addColumn(self::TABLE_NAME, 'currency_id', $this
            ->char(3)
            ->notNull()
            ->defaultValue(self::DEFAULT_CURRENCY_VALUE)
            ->append('COLLATE "utf8_unicode_ci"')
            ->after('contractor_id'));

        $this->createIndex('ck_currency_id', self::TABLE_NAME, ['currency_id']);
        $this->addForeignKey(
            'fk_credit_currency_id__currency_id',
            self::TABLE_NAME,
            ['currency_id'],
            '{{%currency}}',
            ['id'],
            'CASCADE',
            'RESTRICT'
        );
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        $this->dropForeignKey('fk_credit_currency_id__currency_id', self::TABLE_NAME);
        $this->dropColumn(self::TABLE_NAME, 'currency_id');
        $this->dropColumn(self::TABLE_NAME, 'payment_date');
        $this->dropColumn(self::TABLE_NAME, 'payment_debt');
    }
}
