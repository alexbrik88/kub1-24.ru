<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200609_124259_create_product_initial_balance extends Migration
{
    public function safeUp()
    {
        $this->createTable('product_initial_balance', [
            'product_id' => $this->primaryKey(),
            'product_unit_id' => $this->integer(),
            'price' => $this->integer()->defaultValue(0),
            'date' => $this->date()
        ]);

        $this->execute('
            INSERT INTO `product_initial_balance` (`product_id`, `product_unit_id`, `price`, `date`)
                SELECT `id`, `product_unit_id`, 0, FROM_UNIXTIME(`created_at`) FROM `product`
        ');

        $this->addForeignKey('FK_product_initial_balance_to_product', 'product_initial_balance', 'product_id', 'product', 'id', 'CASCADE');
        $this->addForeignKey('FK_product_initial_balance_to_product_unit', 'product_initial_balance', 'product_unit_id', 'product_unit', 'id', 'SET NULL');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_product_initial_balance_to_product', 'product_initial_balance');
        $this->dropForeignKey('FK_product_initial_balance_to_product_unit', 'product_initial_balance');
        $this->dropTable('product_initial_balance');
    }
}
