<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190405_173751_add_new_column_to_company extends Migration
{
    public $tableName = 'company';
    
    public function safeUp()
    {
        $this->addColumn($this->tableName, 'show_popup_import_products', $this->boolean()->defaultValue(true));

        $this->update($this->tableName, ['show_popup_import_products' => false], ['not', ['id' => null]]);
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'show_popup_import_products');
    }
}
