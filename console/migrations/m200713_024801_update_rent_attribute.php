<?php

use console\components\db\Migration;
use yii\db\Query;
use yii\db\Expression;


class m200713_024801_update_rent_attribute extends Migration
{
    const CATEGORY_REALTY = 1; //ID категории "недвижимость"
    const CATEGORY_TECHNIC = 2; //ID категории "спец.техника"
    const CATEGORY_EQUIPMENT = 3; //ID категории "оборудование"

    public $rentAttribute = 'rent_attribute';
    public $rentAttributeValue = 'rent_attribute_value';

    public function safeUp()
    {
        $realtyTypeId = (new Query())->from($this->rentAttribute)->where(['alias' => 'type', 'category_id' => self::CATEGORY_REALTY])->select('id')->scalar();
        $techChassisId = (new Query())->from($this->rentAttribute)->where(['alias' => 'chassis', 'category_id' => self::CATEGORY_TECHNIC])->select('id')->scalar();

        $this->update($this->rentAttribute, ['data' => '["Коммерческая","Жилая"]', 'default' => 'Коммерческая'], ['id' => $realtyTypeId]);
        $this->update($this->rentAttribute, ['data' => '["Колёса","Гусеничный"]', 'default' => 'Колёса'], ['id' => $techChassisId]);

        $expr = new Expression('concat(upper(substr(value, 1, 1)), substr(value, 2))');
        $this->update($this->rentAttributeValue, ['value' => $expr], ['attribute_id' => [$realtyTypeId, $techChassisId]]);

        $this->update($this->rentAttribute, ['sort' => 1], ['alias' => 'currency']);
        $this->update($this->rentAttribute, ['sort' => 2], ['alias' => 'unit']);
        $this->update($this->rentAttribute, ['sort' => 3], ['alias' => 'paymentPeriod']);
        $this->update($this->rentAttribute, ['sort' => 4], ['alias' => 'price']);
    }

    public function safeDown()
    {
    }
}
