<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180806_163025_alter_checking_accountant_add_FK extends Migration
{
    public function safeUp()
    {
        $this->execute('
            DELETE {{checking_accountant}} FROM {{checking_accountant}}
            LEFT JOIN {{company}} ON {{checking_accountant}}.[[company_id]] = {{company}}.[[id]]
            WHERE {{company}}.[[id]] IS NULL
        ');
        $this->addForeignKey('FK_checkingAccountant_company', 'checking_accountant', 'company_id', 'company', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_checkingAccountant_company', 'checking_accountant');
    }
}
