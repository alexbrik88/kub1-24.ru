<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181114_102801_add_certificate_issued_by_to_company extends Migration
{
    public $tableName = 'company';

    public function safeUp()
    {
        $this->addColumn($this->tableName ,'ip_certificate_issued_by', $this->string()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName ,'ip_certificate_issued_by');
    }
}


