<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201001_141247_alter_cash_bank_flows_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%cash_bank_flows}}', 'source', $this->tinyInteger(1));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%cash_bank_flows}}', 'source');
    }
}
