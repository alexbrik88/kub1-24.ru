<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200706_102652_alter_user_config_add_column extends Migration
{
    public $tableName = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'contractor_payment_delay', $this->boolean()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'contractor_payment_delay');
    }
}
