<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170504_103253_insert_cashOrdersReasonsTypes extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%cash_orders_reasons_types}}', [
            'id' => 17,
            'name' => 'Получение денег с расчетного счета',
            'flow_type' => 1,
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%cash_orders_reasons_types}}', ['id' => 17]);
    }
}
