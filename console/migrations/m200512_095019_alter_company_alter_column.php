<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200512_095019_alter_company_alter_column extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%company}}', 'main_id',  $this->integer());
    }

    public function down()
    {
        $this->alterColumn('{{%company}}', 'main_id',  $this->integer()->notNull());
    }
}
