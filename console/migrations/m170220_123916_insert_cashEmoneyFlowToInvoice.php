<?php

use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashEmoneyFlowToInvoice;
use common\models\document\Invoice;
use console\components\db\Migration;
use yii\db\Schema;

class m170220_123916_insert_cashEmoneyFlowToInvoice extends Migration
{
    public function safeUp()
    {
        $B = CashEmoneyFlows::tableName();
        $F = CashEmoneyFlowToInvoice::tableName();
        $I = Invoice::tableName();
        $flowArray = CashEmoneyFlows::find()
            ->select(['id', 'invoice_id'])
            ->where(['not', ['invoice_id' => null]])
            ->asArray()
            ->all();

        if ($flowArray) {
            foreach ($flowArray as $row) {
                $flow = CashEmoneyFlows::findOne($row['id']);
                $invoice = Invoice::findOne($row['invoice_id']);
                if ($flow && $invoice) {
                    $flow->unlinkInvoice($invoice);
                    $flow->linkInvoice($invoice);
                }
            }
        }
    }
    
    public function safeDown()
    {
        CashEmoneyFlowToInvoice::deleteAll();
    }
}


