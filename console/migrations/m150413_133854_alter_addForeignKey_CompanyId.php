<?php

use yii\db\Schema;
use yii\db\Migration;

class m150413_133854_alter_addForeignKey_CompanyId extends Migration
{
    public function up()
    {
        $this->addForeignKey('contractor_company_rel', 'contractor', 'company_id', 'company', 'id');

        $this->addForeignKey('employee_company_rel', 'employee', 'company_id', 'company', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('contractor_company_rel', 'contractor');

        $this->dropForeignKey('employee_company_rel', 'employee');
    }
}
