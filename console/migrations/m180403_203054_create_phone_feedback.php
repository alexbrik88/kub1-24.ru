<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180403_203054_create_phone_feedback extends Migration
{
    public $tableName = 'phone_feedback';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'employee_id' => $this->integer()->notNull(),
            'popup_type' => $this->integer()->notNull(),
            'phone' => $this->string()->notNull(),
        ]);

        $this->addForeignKey($this->tableName . '_company_id', $this->tableName, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tableName . '_employee_id', $this->tableName, 'employee_id', 'employee', 'id', 'RESTRICT', 'CASCADE');
    }
    
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}


