<?php

use yii\db\Schema;
use yii\db\Migration;

class m150414_090250_alter_Company_allowNullForAddress extends Migration
{
    public function up()
    {
        $this->alterColumn('company', 'address_legal_house_type_id', Schema::TYPE_INTEGER . ' NULL');
        $this->alterColumn('company', 'address_legal_housing_type_id', Schema::TYPE_INTEGER . ' NULL');
        $this->alterColumn('company', 'address_legal_apartment_type_id', Schema::TYPE_INTEGER . ' NULL');

        $this->alterColumn('company', 'address_actual_house_type_id', Schema::TYPE_INTEGER . ' NULL');
        $this->alterColumn('company', 'address_actual_housing_type_id', Schema::TYPE_INTEGER . ' NULL');
        $this->alterColumn('company', 'address_actual_apartment_type_id', Schema::TYPE_INTEGER . ' NULL');
    }

    public function down()
    {
        $this->alterColumn('company', 'address_legal_house_type_id', Schema::TYPE_INTEGER . ' NOT NULL');
        $this->alterColumn('company', 'address_legal_housing_type_id', Schema::TYPE_INTEGER . ' NOT NULL');
        $this->alterColumn('company', 'address_legal_apartment_type_id', Schema::TYPE_INTEGER . ' NOT NULL');

        $this->alterColumn('company', 'address_actual_house_type_id', Schema::TYPE_INTEGER . ' NOT NULL');
        $this->alterColumn('company', 'address_actual_housing_type_id', Schema::TYPE_INTEGER . ' NOT NULL');
        $this->alterColumn('company', 'address_actual_apartment_type_id', Schema::TYPE_INTEGER . ' NOT NULL');
    }
}
