<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180305_125505_alter_out_invoice_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('out_invoice', 'strict_data', $this->boolean()->after('return_url'));
    }

    public function safeDown()
    {
        $this->dropColumn('out_invoice', 'strict_data');
    }
}
