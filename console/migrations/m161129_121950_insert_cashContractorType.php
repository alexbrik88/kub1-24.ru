<?php

use console\components\db\Migration;
use yii\db\Schema;

class m161129_121950_insert_cashContractorType extends Migration
{
    public function safeUp()
    {
        $this->insert('cash_contractor_type', [
            'id' => 4,
            'name' => 'balance',
            'text' => 'Баланс начальный',
        ]);
    }
    
    public function safeDown()
    {
        $this->delete('cash_contractor_type', ['id' => 4]);
    }
}
