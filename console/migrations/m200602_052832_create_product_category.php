<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200602_052832_create_product_category extends Migration
{
    public function safeUp()
    {
        $this->createTable('product_category', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'sort' => $this->tinyInteger()->defaultValue(0)
        ]);

        $this->createTable('product_category_field', [
            'id' => $this->primaryKey(),
            'category_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'sort' => $this->tinyInteger()->defaultValue(0)
        ]);

        $this->createTable('product_category_item', [
            'product_id' => $this->integer(),
            'field_id' => $this->integer(),
            'value' => $this->string(),
        ]);

        $this->addForeignKey('FK_product_category_field_to_category', 'product_category_field', 'category_id', 'product_category', 'id', 'CASCADE');
        $this->addForeignKey('FK_product_category_item_to_field', 'product_category_item', 'field_id', 'product_category_field', 'id', 'CASCADE');
        $this->addForeignKey('FK_product_category_item_to_product', 'product_category_item', 'product_id', 'product', 'id', 'CASCADE');
        $this->addPrimaryKey('PRIMARY_KEY', 'product_category_item', ['product_id', 'field_id']);

        $this->batchInsert('product_category', ['id', 'name', 'sort'], [
            [1, 'Издательство книг', 10]
        ]);

        $this->batchInsert('product_category_field', ['category_id', 'name', 'sort'], [
            [1, 'ISBN', 10],
            [1, 'Автор', 20],
            [1, 'Год издания', 30],
            [1, 'Издательство', 40],
            [1, 'Формат', 50],
            [1, 'Переплет', 60],
        ]);

        // link to product
        $this->addColumn('product', 'category_id', $this->integer());
        $this->addForeignKey('FK_product_to_category', 'product', 'category_id', 'product_category', 'id', 'SET NULL');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_product_to_category', 'product');
        $this->dropForeignKey('FK_product_category_item_to_product', 'product_category_item');
        $this->dropForeignKey('FK_product_category_item_to_field', 'product_category_item');
        $this->dropForeignKey('FK_product_category_field_to_category', 'product_category_field');

        $this->dropTable('product_category_item');
        $this->dropTable('product_category_field');
        $this->dropTable('product_category');
    }
}
