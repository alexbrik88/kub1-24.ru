<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180327_082712_update_contractor extends Migration
{
    public function safeUp()
    {
        $this->execute("
            UPDATE {{contractor}}
            SET {{contractor}}.[[name]] = CONCAT('\"', {{contractor}}.[[name]], '\"')
            WHERE {{contractor}}.[[name]] NOT LIKE '%\"%' AND {{contractor}}.[[company_type_id]] > 1
        ");
    }

    public function safeDown()
    {
        $this->execute("
            UPDATE {{contractor}}
            SET {{contractor}}.[[name]] = TRIM(BOTH '\"' FROM {{contractor}}.[[name]])
            WHERE {{contractor}}.[[name]] LIKE '\"%\"' AND {{contractor}}.[[company_type_id]] > 1
        ");
    }
}
