<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190307_130534_insert_invoice_income_item extends Migration
{
    public function safeUp()
    {
        $this->batchInsert('invoice_income_item', ['id', 'name'], [
            [16, 'Торговая выручка'],
            [17, 'Торговый эквайринг'],
        ]);
    }

    public function safeDown()
    {
        $this->delete('invoice_income_item', ['id' => [16, 17]]);
    }
}
