<?php

use yii\db\Migration;

class m210622_203810_alter_employee_company_table extends Migration
{
    private const TABLE_NAME = '{{%employee_company}}';

    /**
     * @inheritDoc
     */
    public function up(): void
    {
        $this->addColumn(self::TABLE_NAME, 'crm_deal_type_id', $this->bigInteger()->null());
    }

    /**
     * @inheritDoc
     */
    public function down(): void
    {
        $this->dropColumn(self::TABLE_NAME, 'crm_deal_type_id');
    }
}
