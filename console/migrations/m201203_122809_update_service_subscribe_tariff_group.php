<?php

use console\components\db\Migration;
use yii\db\Expression;
use yii\db\Schema;

class m201203_122809_update_service_subscribe_tariff_group extends Migration
{
    public function safeUp()
    {
        $this->update('{{%service_subscribe_tariff_group}}', [
            'name' => new Expression('REPLACE([[name]], "Бизнес Аналитика", "ФинДиректор")'),
            'description' => new Expression('REPLACE([[description]], "Бизнес Аналитика", "ФинДиректор")'),
        ], [
            'id' => range(11, 18),
        ]);
    }

    public function safeDown()
    {
        $this->update('{{%service_subscribe_tariff_group}}', [
            'name' => new Expression('REPLACE([[name]], "ФинДиректор", "Бизнес Аналитика")'),
            'description' => new Expression('REPLACE([[description]], "ФинДиректор", "Бизнес Аналитика")'),
        ], [
            'id' => range(11, 18),
        ]);
    }
}
