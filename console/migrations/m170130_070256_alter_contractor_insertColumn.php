<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170130_070256_alter_contractor_insertColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%contractor}}', 'responsible_employee_id', $this->integer()->defaultValue(null));
        $this->addColumn('{{%contractor}}', 'source', $this->text()->defaultValue(null));

        $this->addForeignKey('fk_contractor_employee_company', '{{%contractor}}', 'responsible_employee_id',
            '{{%employee_company}}', 'employee_id', 'SET NULL', 'CASCADE');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('fk_contractor_employee_company', '{{%contractor}}');

        $this->dropColumn('{{%contractor}}', 'responsible_employee_id');
        $this->dropColumn('{{%contractor}}', 'source');
    }
}


