<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160802_143532_alter_rs_and_ks_checking_accountant extends Migration
{
    public $tCheckingAccountant = 'checking_accountant';

    public function safeUp()
    {
        $this->alterColumn($this->tCheckingAccountant, 'bik', $this->string(9)->defaultValue(null));
        $this->alterColumn($this->tCheckingAccountant, 'rs', $this->string()->defaultValue(null));
        $this->alterColumn($this->tCheckingAccountant, 'ks', $this->string()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->alterColumn($this->tCheckingAccountant, 'bik', $this->integer()->defaultValue(null));
        $this->alterColumn($this->tCheckingAccountant, 'rs', $this->integer()->defaultValue(null));
        $this->alterColumn($this->tCheckingAccountant, 'ks', $this->integer()->defaultValue(null));
    }
}


