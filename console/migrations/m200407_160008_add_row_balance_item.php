<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200407_160008_add_row_balance_item extends Migration
{
    public $tableName = '{{%balance_item}}';

    public function safeUp()
    {
        $this->batchInsert($this->tableName, ['id', 'name', 'sort'], [
            [24, 'Незавершенные проекты', 1],
        ]);
    }

    public function safeDown()
    {
        $this->execute('DELETE FROM `balance_item` WHERE `id` = 24 LIMIT 1');
    }
}
