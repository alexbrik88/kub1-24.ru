<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200724_060338_insert_employee_role extends Migration
{
    public function safeUp()
    {
        $this->insert('employee_role', [
            'id' => 14,
            'name' => 'Финансовый директор',
            'sort' => 17000
        ]);
        $this->insert('employee_role', [
            'id' => 15,
            'name' => 'Финансовый консультант',
            'sort' => 18000
        ]);
    }

    public function safeDown()
    {
        $this->delete('employee_role', ['id' => 15]);
        $this->delete('employee_role', ['id' => 14]);
    }
}
