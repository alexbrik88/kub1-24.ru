<?php

use yii\db\Schema;
use yii\db\Migration;

class m151112_143751_alter_promoCode_changeColumnType_expiredAt extends Migration
{
    public $tPromoCode = 'service_payment_promo_code';

    public function safeUp()
    {
        $this->alterColumn($this->tPromoCode, 'expired_at', $this->dateTime()->notNull());
    }
    
    public function safeDown()
    {
        $this->alterColumn($this->tPromoCode, 'expired_at', 'DATE NOT NULL');
    }
}
