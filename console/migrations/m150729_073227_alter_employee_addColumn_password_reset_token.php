<?php

use yii\db\Schema;
use yii\db\Migration;

class m150729_073227_alter_employee_addColumn_password_reset_token extends Migration
{
    public function safeUp()
    {
        $this->addColumn('employee', 'password_reset_token', Schema::TYPE_STRING . '(255) NULL AFTER password');
    }
    
    public function safeDown()
    {
        $this->dropColumn('employee', 'password_reset_token');
    }
}
