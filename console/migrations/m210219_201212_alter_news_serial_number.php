<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210219_201212_alter_news_serial_number extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('news', 'serial_number', $this->integer(11)->unsigned());
    }

    public function safeDown()
    {
        $this->alterColumn('news', 'serial_number', $this->integer(11)->unsigned()->notNull());

    }
}
