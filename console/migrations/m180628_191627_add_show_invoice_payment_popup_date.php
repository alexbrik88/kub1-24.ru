<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180628_191627_add_show_invoice_payment_popup_date extends Migration
{
    public $tableName = 'company';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'show_invoice_payment_popup_date', $this->integer()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'show_invoice_payment_popup_date');
    }
}


