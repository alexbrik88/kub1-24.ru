<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210914_120914_alter_moysklad_document extends Migration
{
    public $table = 'moysklad_document';

    public function safeUp()
    {
        $this->addColumn($this->table, 'contractor_kpp', $this->char(16)->after('contractor_id'));
        $this->addColumn($this->table, 'contractor_inn', $this->char(16)->after('contractor_id'));
        $this->addColumn($this->table, 'contractor_name', $this->char(255)->after('contractor_id'));
        $this->dropColumn('moysklad_document', 'contractor_id');
        $this->addColumn($this->table, 'cash_order_flows_id', $this->integer()->after('invoice_facture_id'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'cash_order_flows_id');
        $this->addColumn('moysklad_document', 'contractor_id', $this->integer()->after('document_number'));
        $this->dropColumn($this->table, 'contractor_name');
        $this->dropColumn($this->table, 'contractor_inn');
        $this->dropColumn($this->table, 'contractor_kpp');
    }
}
