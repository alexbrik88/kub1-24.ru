<?php

use yii\db\Schema;
use yii\db\Migration;

class m150507_120111_alter_Product_addColumn_isDeleted extends Migration
{
    public function safeUp()
    {
        $this->addColumn('product', 'is_deleted', Schema::TYPE_BOOLEAN . ' DEFAULT 0');
    }
    
    public function safeDown()
    {
        $this->dropColumn('product', 'is_deleted');
    }
}
