<?php

use yii\db\Migration;

/**
 * Handles the creation of table `get_response_contact`.
 */
class m210921_170906_create_get_response_contact_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('get_response_contact', [
            'id' => $this->primaryKey(),
            'employee_id' => $this->integer()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'update_attempts_count' => $this->integer()->notNull()->defaultValue(0)
        ]);

        $this->addForeignKey('FK_get_response_contact_to_company', 'get_response_contact', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_get_response_contact_to_employee', 'get_response_contact', 'employee_id', 'employee', 'id', 'CASCADE', 'CASCADE');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey('FK_get_response_contact_to_company', 'get_response_contact');
        $this->dropForeignKey('FK_get_response_contact_to_employee', 'get_response_contact');
        $this->dropTable('get_response_contact');
    }
}
