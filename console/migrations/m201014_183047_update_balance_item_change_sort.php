<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201014_183047_update_balance_item_change_sort extends Migration
{
    public function safeUp()
    {
        $this->execute("UPDATE `balance_item` SET `sort` = 2 WHERE `id` = 23");
        $this->execute("UPDATE `balance_company_item` SET `sort` = 2 WHERE `item_id` = 23");
    }

    public function safeDown()
    {
        $this->execute("UPDATE `balance_item` SET `sort` = 6 WHERE `id` = 23");
        $this->execute("UPDATE `balance_company_item` SET `sort` = 6 WHERE `item_id` = 23");
    }
}
