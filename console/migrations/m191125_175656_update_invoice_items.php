<?php

use common\models\Company;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\ExpenseItemFlowOfFunds;
use common\models\IncomeItemFlowOfFunds;
use console\components\db\Migration;
use yii\db\Schema;
use yii\helpers\Console;

class m191125_175656_update_invoice_items extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('{{%income_item_flow_of_funds}}', 'name');
        $this->batchInsert('{{%invoice_income_item}}', ['id', 'name', 'sort'], [
            [18, 'Агентское вознаграждение', 100],
            [19, 'Возврат депозита', 100],
            [20, 'Продажа Основных Средств', 100],
            [21, 'Проценты полученные по депозитам', 100],
        ]);
         foreach (Company::find()->select(['id'])->column() as $companyId) {
             foreach (InvoiceIncomeItem::find()->select(['id'])->column() as $incomeItemId) {
                 if (!IncomeItemFlowOfFunds::find()->andWhere(['and',
                     ['company_id' => $companyId],
                     ['income_item_id' => $incomeItemId],
                 ])->exists()
                 ) {
                     $this->insert('{{%income_item_flow_of_funds}}', [
                         'company_id' => $companyId,
                         'income_item_id' => $incomeItemId,
                     ]);
                 }
             }
         }
        $this->update('{{%invoice_income_item}}', ['name' => 'Взнос от учредителя'], ['id' => 6]);
        $this->update('{{%invoice_income_item}}', ['name' => 'Проценты полученные по займам'], ['id' => 11]);
        $this->update('{{%invoice_income_item}}', ['name' => 'Прочие доходы'], ['id' => 7]);

        $this->addColumn('{{%invoice_income_item}}', 'is_visible', $this->boolean()->unsigned()->notNull()->defaultValue(true));
        $this->addColumn('{{%income_item_flow_of_funds}}', 'is_visible', $this->boolean()->unsigned()->notNull()->defaultValue(true));

        $this->addColumn('{{%invoice_income_item}}', 'can_be_controlled', $this->boolean()->unsigned()->notNull()->defaultValue(false));
        $this->update('{{%invoice_income_item}}', ['can_be_controlled' => true], ['id' => 18]);
        $this->update('{{%invoice_income_item}}', ['can_be_controlled' => true], ['id' => 19]);
        $this->update('{{%invoice_income_item}}', ['can_be_controlled' => true], ['id' => 12]);
        $this->update('{{%invoice_income_item}}', ['can_be_controlled' => true], ['id' => 20]);
        $this->update('{{%invoice_income_item}}', ['can_be_controlled' => true], ['id' => 21]);

        $this->dropColumn('{{%expense_item_flow_of_funds}}', 'name');
        $this->batchInsert('{{%invoice_expenditure_group}}', ['id', 'name'], [
            [2, 'Аренда'],
            [3, 'Реклама'],
            [4, 'Сотрудники'],
        ]);
        $this->batchInsert('{{%invoice_expenditure_item}}', ['id', 'group_id', 'name', 'sort'], [
            [54, null, 'Агентское вознаграждение', 100],
            [55, 2, 'Аренда автотранспорта', 100],
            [56, 2, 'Аренда офиса', 100],
            [57, 2, 'Аренда склада', 100],
            [58, 2, 'Аренда оборудования', 100],
            [59, 2, 'Коммунальные расходы', 100],
            [60, 2, 'Электроэнергия', 100],
            [61, null, 'Госпошлины', 100],
            [62, null, 'Депозит', 100],
            [63, null, 'Канцелярские товары', 100],
            [64, null, 'Лизинг', 100],
            [65, 1, 'Налог ЕНВД', 100],
            [66, 1, 'Налог ЕСХН', 100],
            [67, 1, 'Пени', 100],
            [68, 1, 'Штрафы', 100],
            [69, null, 'Подарки контрагентам', 100],
            [70, null, 'Почта', 100],
            [71, null, 'Приобретение Основных Средств', 100],
            [72, null, 'Проезд', 100],
            [73, 3, 'Рекламная продукция', 100],
            [74, 3, 'Реклама PR', 100],
            [75, null, 'Ремонт', 100],
            [76, null, 'Ремонт автотранспорта', 100],
            [77, null, 'Ремонт офиса', 100],
            [78, 4, 'Услуги Кадрового Агентства', 100],
            [79, 4, 'Обучение', 100],
            [80, 4, 'Командировочные расходы', 100],
            [81, 4, 'Подарки сотрудникам', 100],
            [82, 4, 'Корпоративы', 100],
            [83, 4, 'Материальная помощь', 100],
            [84, null, 'Страхование', 100],
            [85, null, 'Страхование автотранспорта', 100],
            [86, null, 'Страхование ДМС', 100],
            [87, null, 'Таможенные услуги', 100],
            [88, null, 'Топливо', 100],
            [89, null, 'Хоз. расходы', 100],
        ]);

        foreach (Company::find()->select(['id'])->column() as $companyId) {
            foreach (InvoiceExpenditureItem::find()->select(['id'])->column() as $expenseItemId) {
                if (!ExpenseItemFlowOfFunds::find()->andWhere(['and',
                    ['company_id' => $companyId],
                    ['expense_item_id' => $expenseItemId],
                ])->exists()
                ) {
                    $this->insert('{{%expense_item_flow_of_funds}}', [
                        'company_id' => $companyId,
                        'expense_item_id' => $expenseItemId,
                    ]);
                }
            }
        }

        $this->update('{{%invoice_expenditure_item}}', ['group_id' => 2], ['id' => 2]);
        $this->update('{{%invoice_expenditure_item}}', ['group_id' => 2], ['id' => 52]);
        $this->update('{{%invoice_expenditure_item}}', ['group_id' => 3], ['id' => 9]);

        $this->update('{{%invoice_expenditure_item}}', ['name' => 'Бухгалтерские услуги'], ['id' => 25]);
        $this->update('{{%invoice_expenditure_item}}', ['name' => 'Дивиденды'], ['id' => 15]);
        $this->update('{{%invoice_expenditure_item}}', ['name' => 'Программное обеспечение'], ['id' => 23]);
        $this->update('{{%invoice_expenditure_item}}', ['name' => 'Юридические услуги'], ['id' => 26]);

        $this->addColumn('{{%invoice_expenditure_item}}', 'is_visible', $this->boolean()->unsigned()->notNull()->defaultValue(true));
        $this->addColumn('{{%expense_item_flow_of_funds}}', 'is_visible', $this->boolean()->unsigned()->notNull()->defaultValue(true));

        $this->addColumn('{{%invoice_expenditure_item}}', 'can_be_controlled', $this->boolean()->unsigned()->notNull()->defaultValue(false));
        $this->update('{{%invoice_expenditure_item}}', ['can_be_controlled' => true], ['id' => 54]);
        $this->update('{{%invoice_expenditure_item}}', ['can_be_controlled' => true], ['id' => 55]);
        $this->update('{{%invoice_expenditure_item}}', ['can_be_controlled' => true], ['id' => 56]);
        $this->update('{{%invoice_expenditure_item}}', ['can_be_controlled' => true], ['id' => 57]);
        $this->update('{{%invoice_expenditure_item}}', ['can_be_controlled' => true], ['id' => 58]);
        $this->update('{{%invoice_expenditure_item}}', ['can_be_controlled' => true], ['id' => 52]);
        $this->update('{{%invoice_expenditure_item}}', ['can_be_controlled' => true], ['id' => 59]);
        $this->update('{{%invoice_expenditure_item}}', ['can_be_controlled' => true], ['id' => 60]);
        $this->update('{{%invoice_expenditure_item}}', ['can_be_controlled' => true], ['id' => 16]);
        $this->update('{{%invoice_expenditure_item}}', ['can_be_controlled' => true], ['id' => 61]);
        $this->update('{{%invoice_expenditure_item}}', ['can_be_controlled' => true], ['id' => 63]);
        $this->update('{{%invoice_expenditure_item}}', ['can_be_controlled' => true], ['id' => 64]);
        $this->update('{{%invoice_expenditure_item}}', ['can_be_controlled' => true], ['id' => 20]);
        $this->update('{{%invoice_expenditure_item}}', ['can_be_controlled' => true], ['id' => 49]);
        $this->update('{{%invoice_expenditure_item}}', ['can_be_controlled' => true], ['id' => 50]);
        $this->update('{{%invoice_expenditure_item}}', ['can_be_controlled' => true], ['id' => 51]);
        $this->update('{{%invoice_expenditure_item}}', ['can_be_controlled' => true], ['id' => 65]);
        $this->update('{{%invoice_expenditure_item}}', ['can_be_controlled' => true], ['id' => 66]);
        $this->update('{{%invoice_expenditure_item}}', ['can_be_controlled' => true], ['id' => 69]);
        $this->update('{{%invoice_expenditure_item}}', ['can_be_controlled' => true], ['id' => 70]);
        $this->update('{{%invoice_expenditure_item}}', ['can_be_controlled' => true], ['id' => 71]);
        $this->update('{{%invoice_expenditure_item}}', ['can_be_controlled' => true], ['id' => 72]);
        $this->update('{{%invoice_expenditure_item}}', ['can_be_controlled' => true], ['id' => 73]);
        $this->update('{{%invoice_expenditure_item}}', ['can_be_controlled' => true], ['id' => 74]);
        $this->update('{{%invoice_expenditure_item}}', ['can_be_controlled' => true], ['id' => 75]);
        $this->update('{{%invoice_expenditure_item}}', ['can_be_controlled' => true], ['id' => 76]);
        $this->update('{{%invoice_expenditure_item}}', ['can_be_controlled' => true], ['id' => 77]);
        $this->update('{{%invoice_expenditure_item}}', ['can_be_controlled' => true], ['id' => 78]);
        $this->update('{{%invoice_expenditure_item}}', ['can_be_controlled' => true], ['id' => 79]);
        $this->update('{{%invoice_expenditure_item}}', ['can_be_controlled' => true], ['id' => 80]);
        $this->update('{{%invoice_expenditure_item}}', ['can_be_controlled' => true], ['id' => 83]);
        $this->update('{{%invoice_expenditure_item}}', ['can_be_controlled' => true], ['id' => 84]);
        $this->update('{{%invoice_expenditure_item}}', ['can_be_controlled' => true], ['id' => 85]);
        $this->update('{{%invoice_expenditure_item}}', ['can_be_controlled' => true], ['id' => 86]);
        $this->update('{{%invoice_expenditure_item}}', ['can_be_controlled' => true], ['id' => 87]);
        $this->update('{{%invoice_expenditure_item}}', ['can_be_controlled' => true], ['id' => 88]);

        $companiesWithInvoices = Yii::$app->db->createCommand('
            SELECT invoice.company_id 
              FROM invoice
              LEFT JOIN company_taxation_type
                     ON company_taxation_type.company_id = invoice.company_id
             WHERE invoice.invoice_expenditure_item_id = 27
             GROUP BY invoice.company_id;       
        ')->queryColumn();

        $companiesWithContractor = Yii::$app->db->createCommand('
            SELECT contractor.company_id 
              FROM contractor
              LEFT JOIN company_taxation_type
                     ON company_taxation_type.company_id = contractor.company_id
             WHERE contractor.invoice_expenditure_item_id = 27
             GROUP BY contractor.company_id;       
        ')->queryColumn();

        $companiesWithBankFlows = Yii::$app->db->createCommand('
            SELECT cash_bank_flows.company_id 
              FROM cash_bank_flows
              LEFT JOIN company_taxation_type
                     ON company_taxation_type.company_id = cash_bank_flows.company_id
             WHERE cash_bank_flows.expenditure_item_id = 27
             GROUP BY cash_bank_flows.company_id;       
        ')->queryColumn();

        $companiesWithOrderFlows = Yii::$app->db->createCommand('
            SELECT cash_order_flows.company_id 
              FROM cash_order_flows
              LEFT JOIN company_taxation_type
                     ON company_taxation_type.company_id = cash_order_flows.company_id
             WHERE cash_order_flows.expenditure_item_id = 27
             GROUP BY cash_order_flows.company_id;       
        ')->queryColumn();

        $companiesWithEmoneyFlows = Yii::$app->db->createCommand('
            SELECT cash_emoney_flows.company_id 
              FROM cash_emoney_flows
              LEFT JOIN company_taxation_type
                     ON company_taxation_type.company_id = cash_emoney_flows.company_id
             WHERE cash_emoney_flows.expenditure_item_id = 27
             GROUP BY cash_emoney_flows.company_id;       
        ')->queryColumn();

        $companiesWithPlanFlows = Yii::$app->db->createCommand('
            SELECT plan_cash_flows.company_id 
              FROM plan_cash_flows
              LEFT JOIN company_taxation_type
                     ON company_taxation_type.company_id = plan_cash_flows.company_id
             WHERE plan_cash_flows.expenditure_item_id = 27
             GROUP BY plan_cash_flows.company_id;       
        ')->queryColumn();

        $companies = array_unique(array_merge(
            $companiesWithInvoices,
            $companiesWithContractor,
            $companiesWithBankFlows,
            $companiesWithOrderFlows,
            $companiesWithEmoneyFlows,
            $companiesWithPlanFlows
        ));
        foreach ($companies as $companyId) {
            $companyTaxationType = Yii::$app->db->createCommand("
                SELECT usn, usn_percent
                  FROM company_taxation_type
                 WHERE company_taxation_type.company_id = {$companyId}
            ")->queryOne();
            if ($companyTaxationType['usn'] == true && in_array($companyTaxationType['usn_percent'], [6, 15])) {
                if ($companyTaxationType['usn_percent'] == 6) {
                    $expenditureItemId = 48;
                } else {
                    $expenditureItemId = 47;
                }
            } else {
                $expenditureItemId = Yii::$app->db->createCommand('
                    SELECT MAX(invoice_expenditure_item.id)
                      FROM invoice_expenditure_item;       
                ')->queryScalar() + 1;
                $this->insert('{{%invoice_expenditure_item}}', [
                    'id' => $expenditureItemId,
                    'company_id' => $companyId,
                    'name' => 'Налог по УСН',
                    'sort' => 100,
                    'can_be_controlled' => true,
                ]);
            }

            $this->update('{{%invoice}}', ['invoice_expenditure_item_id' => $expenditureItemId], [
                'AND',
                ['invoice_expenditure_item_id' => 27],
                ['company_id' => $companyId],
            ]);
            $this->update('{{%contractor}}', ['invoice_expenditure_item_id' => $expenditureItemId], [
                'AND',
                ['invoice_expenditure_item_id' => 27],
                ['company_id' => $companyId],
            ]);
            $this->update('{{%cash_bank_flows}}', ['expenditure_item_id' => $expenditureItemId], [
                'AND',
                ['expenditure_item_id' => 27],
                ['company_id' => $companyId],
            ]);
            $this->update('{{%cash_order_flows}}', ['expenditure_item_id' => $expenditureItemId], [
                'AND',
                ['expenditure_item_id' => 27],
                ['company_id' => $companyId],
            ]);
            $this->update('{{%cash_emoney_flows}}', ['expenditure_item_id' => $expenditureItemId], [
                'AND',
                ['expenditure_item_id' => 27],
                ['company_id' => $companyId],
            ]);
            $this->update('{{%plan_cash_flows}}', ['expenditure_item_id' => $expenditureItemId], [
                'AND',
                ['expenditure_item_id' => 27],
                ['company_id' => $companyId],
            ]);
        }

        $this->delete('{{%expense_item_flow_of_funds}}', ['expense_item_id' => 27]);
        $this->delete('{{%invoice_expenditure_item}}', ['id' => 27]);
    }

    public function safeDown()
    {
        Console::output('This migration not rolled back.');
        return false;
    }
}
