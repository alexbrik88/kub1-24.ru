<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190610_205127_insert_registration_page_type extends Migration
{
    public $tableName = 'registration_page_type';

    public function safeUp()
    {
        $this->insert($this->tableName, [
            'id' => 20,
            'name' => 'Финансы',
        ]);
    }

    public function safeDown()
    {
        $this->delete($this->tableName, ['id' => 20]);
    }
}
