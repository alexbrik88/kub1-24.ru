<?php

use yii\db\Schema;
use yii\db\Migration;

class m150623_155722_alter_Cash_updateMaxAmount extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('cash_bank_flows', 'amount', Schema::TYPE_BIGINT . ' UNSIGNED NOT NULL');
        $this->alterColumn('cash_emoney_flows', 'amount', Schema::TYPE_BIGINT . ' UNSIGNED NOT NULL');
        $this->alterColumn('cash_order_flows', 'amount', Schema::TYPE_BIGINT . ' UNSIGNED NOT NULL');
    }
    
    public function safeDown()
    {
        $this->alterColumn('cash_bank_flows', 'amount', Schema::TYPE_INTEGER . ' NOT NULL');
        $this->alterColumn('cash_emoney_flows', 'amount', Schema::TYPE_INTEGER . ' NOT NULL');
        $this->alterColumn('cash_order_flows', 'amount', Schema::TYPE_INTEGER . ' NOT NULL');
    }
}
