<?php

use console\components\db\Migration;

class m200111_120750_bitrix24_pk_add extends Migration
{
    public function safeUp()
    {
        $this->createPK('catalog', 'company_id');
        $this->createPK('company', 'company_id');
        $this->createPK('contact', 'company_id');
        $this->createPK('deal', 'company_id');
        $this->createPK('deal_position', 'company_id');
        $this->createPK('invoice', 'company_id');
        $this->createPK('product', 'company_id');
        $this->createPK('section', 'company_id');
        $this->createPK('vat', 'company_id');
    }

    public function safeDown()
    {
        $this->dropPK('catalog');
        $this->dropPK('company');
        $this->dropPK('contact');
        $this->dropPK('deal');
        $this->dropPK('dealposition');
        $this->dropPK('invoice');
        $this->dropPK('product');
        $this->dropPK('section');
        $this->dropPK('vat');
    }

    protected static function tableName(string $table, bool $local = true): string
    {
        if ($local === true) {
            $table = 'bitrix24_' . $table;
        }
        return '{{%' . $table . '}}';
    }

    protected function dropPK(string $table)
    {
        $this->dropPrimaryKey('bitrix24_' . $table . '_pk', self::tableName($table));
    }

    protected function createPK(string $table, string $subColumn)
    {
        $this->addPrimaryKey('bitrix24_' . $table . '_pk', self::tableName($table), [$subColumn, 'id']);
    }
}