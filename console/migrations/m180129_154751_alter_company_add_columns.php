<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180129_154751_alter_company_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('company', 'address_legal', $this->string()->after('name_short'));
        $this->addColumn('company', 'address_actual', $this->string()->after('address_legal'));
    }

    public function safeDown()
    {
        $this->dropColumn('company', 'address_legal');
        $this->dropColumn('company', 'address_actual');
    }
}
