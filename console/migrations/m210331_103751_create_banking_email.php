<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210331_103751_create_banking_email extends Migration
{
    public $table = 'banking_email';

    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'checking_accountant_id' => $this->integer()->notNull(),            
            'email' => $this->string()->notNull(),
            'password' => $this->string(16)->notNull()
        ]);

        $this->addForeignKey('FK_banking_email_to_company', $this->table, 'company_id', 'company', 'id', 'CASCADE');
        $this->addForeignKey('FK_banking_email_to_checking_accountant', $this->table, 'checking_accountant_id', 'checking_accountant', 'id', 'CASCADE');

    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_banking_email_to_checking_accountant', $this->table);
        $this->dropForeignKey('FK_banking_email_to_company', $this->table);
        $this->dropTable($this->table);
    }
}
