<?php

use yii\db\Migration;

/**
 * Handles the creation of table `autoinvoicefacture`.
 */
class m190624_140149_create_autoinvoicefacture_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('autoinvoicefacture', [
            'company_id' => $this->integer()->notNull(),
            'rule' => $this->smallInteger(1)->notNull()->defaultValue(1),
            'send_by_creation' => $this->boolean()->notNull()->defaultValue(false),
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', 'autoinvoicefacture', 'company_id');

        $this->addForeignKey(
            'FK_autoinvoicefacture_company',
            'autoinvoicefacture', 'company_id',
            'company', 'id',
            'CASCADE'
        );

        $this->execute('
            INSERT INTO {{autoinvoicefacture}} ([[company_id]])
            SELECT [[id]] FROM {{company}}
        ');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('autoinvoicefacture');
    }
}
