<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210502_204844_alter_user_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_config', 'marketing_plan_by_budget', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn('user_config', 'marketing_plan_by_total_expenses', $this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('user_config', 'marketing_plan_by_budget');
        $this->dropColumn('user_config', 'marketing_plan_by_total_expenses');
    }
}
