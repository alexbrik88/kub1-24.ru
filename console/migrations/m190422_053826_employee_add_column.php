<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190422_053826_employee_add_column extends Migration
{
    public $tableName = 'employee';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'has_no_patronymic', $this->boolean()->defaultValue(false)->after('patronymic'));

        $this->execute('
            UPDATE {{employee}}
            SET {{employee}}.[[has_no_patronymic]] = 1
            WHERE {{employee}}.[[patronymic]] = ""
        ');
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'has_no_patronymic');
    }
}
