<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200925_091251_alter_product_group_add_column extends Migration
{
    public $tableName = 'product_group';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'object_guid', $this->string());
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'object_guid');
    }
}
