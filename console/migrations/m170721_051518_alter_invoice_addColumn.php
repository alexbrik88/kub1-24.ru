<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170721_051518_alter_invoice_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%invoice}}', 'service_payment_id', $this->integer()->defaultValue(null) . ' AFTER [[subscribe_id]]');

        $this->addForeignKey('FK_invoice_servicePayment', '{{%invoice}}', 'service_payment_id', '{{%service_payment}}', 'id', 'SET NULL');

        $this->execute("
            UPDATE
                {{%invoice}}
            LEFT JOIN
                {{%service_subscribe}} ON {{%service_subscribe}}.[[id]] = {{%invoice}}.[[subscribe_id]]
            LEFT JOIN
                {{%service_payment}} ON {{%service_payment}}.[[subscribe_id]] = {{%service_subscribe}}.[[id]]
            SET
                {{%invoice}}.[[service_payment_id]] = {{%service_payment}}.[[id]]
            WHERE
                {{%invoice}}.[[is_subscribe_invoice]] = 1 AND {{%service_payment}}.[[id]] IS NOT NULL;
        ");
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('FK_invoice_servicePayment', '{{%invoice}}');

        $this->dropColumn('{{%invoice}}', 'service_payment_id');
    }
}
