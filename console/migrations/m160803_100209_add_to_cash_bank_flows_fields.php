<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160803_100209_add_to_cash_bank_flows_fields extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cash_bank_flows', 'rs', $this->string(255));
        $this->addColumn('cash_bank_flows', 'bank_name', $this->string(255));
        $this->addColumn('cash_bank_flows', 'payment_order_number', $this->string(255));

        $this->alterColumn('cash_bank_flows', 'description', $this->string(255)->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->dropColumn('cash_bank_flows', 'rs');
        $this->dropColumn('cash_bank_flows', 'bank_name');
        $this->dropColumn('cash_bank_flows', 'payment_order_number');

        Yii::$app->db->createCommand('
          UPDATE cash_bank_flows 
          SET description = \'\'
          WHERE description IS NULL')->execute();

        $this->alterColumn('cash_bank_flows', 'description', $this->string(255)->notNull());
    }
}


