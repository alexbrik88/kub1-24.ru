<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191227_125824_alter_checking_accountant_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%checking_accountant}}', 'bank_city', $this->string()->after('bank_name'));

        $this->execute('
            UPDATE {{%checking_accountant}}
            LEFT JOIN {{%bik_dictionary}} ON {{%bik_dictionary}}.[[bik]] = {{%checking_accountant}}.[[bik]]
            SET {{%checking_accountant}}.[[bank_city]] = {{%bik_dictionary}}.[[city]]
        ');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%checking_accountant}}', 'bank_city');
    }
}
