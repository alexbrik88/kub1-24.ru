<?php

use console\components\db\Migration;
use yii\db\Schema;

class m220401_155335_add_registration_page_type extends Migration
{
    public $tableName = '{{%registration_page_type}}';

    public function safeUp()
    {
        $this->insert($this->tableName, [
            'id' => 53, // PAGE_TYPE_ANALYTICS_DASHBOARD
            'name' => 'Аналитика / Дашборды',
        ]);
    }

    public function safeDown()
    {
        $this->delete($this->tableName, ['id' => 53]);
    }
}
