<?php

use yii\db\Schema;
use yii\db\Migration;

class m150424_080318_alter_act_and_packing_list_date extends Migration
{
    public function safeUp()
    {
        $this->renameColumn('packing_list', 'update_date', 'packing_list_date');
        $this->renameColumn('act', 'update_date', 'act_date');
    }
    
    public function safeDown()
    {
        $this->renameColumn('packing_list', 'packing_list_date', 'update_date');
        $this->renameColumn('act', 'act_date', 'update_date');
    }
}
