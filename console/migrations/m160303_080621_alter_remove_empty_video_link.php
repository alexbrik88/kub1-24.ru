<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160303_080621_alter_remove_empty_video_link extends Migration
{
    public $tPrompt = 'prompt';

    public function safeUp()
    {
        $this->update($this->tPrompt, ['video_link' => null], ['video_link' => 'https://player.vimeo.com/video/?title=0&byline=0&portrait=0']);
    }
    
    public function safeDown()
    {
        echo 'this migration not roll back';
    }
}


