<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190529_065302_create_type extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('create_type', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ], $tableOptions);

        $this->batchInsert('create_type', ['id', 'name',], [
            [1, 'WEB'],
            [2, 'API'],
            [3, 'Модуль b2b']
        ]);
    }

    public function down()
    {
        $this->dropTable('create_type');
    }
}
