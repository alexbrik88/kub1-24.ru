<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190123_105243_update_tax_kbk extends Migration
{
    public function safeUp()
    {
        $this->update('tax_kbk', ['expenditure_item_id' => 46], ['id' => '18210202103081013160']);
    }

    public function safeDown()
    {
        $this->update('tax_kbk', ['expenditure_item_id' => 28], ['id' => '18210202103081013160']);
    }
}
