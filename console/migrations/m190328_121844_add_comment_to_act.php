<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190328_121844_add_comment_to_act extends Migration
{
    public $tAct = 'act';
    public $tActEssence = 'act_essence';

    public function safeUp()
    {
        $this->addColumn($this->tAct, 'comment', $this->text()->defaultValue(null));
        $this->update($this->tAct, ['comment' => 'Вышеперечисленные услуги выполнены полностью и в срок. Заказчик претензий по объему, качеству и срокам оказания услуг не имеет.']);
        $this->createTable($this->tActEssence, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'text' => $this->text()->defaultValue(null),
            'is_checked' => $this->boolean()->defaultValue(false),
        ]);

        $this->addForeignKey($this->tActEssence . '_company_id', $this->tActEssence, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');

    }

    public function safeDown()
    {
        $this->dropColumn($this->tAct, 'comment');

        $this->dropForeignKey($this->tActEssence . '_company_id', $this->tActEssence);
        $this->dropTable($this->tActEssence);
    }
}
