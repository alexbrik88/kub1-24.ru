<?php

use yii\db\Schema;
use yii\db\Migration;

class m150713_085506_alter_invoice_addColumn_uid extends Migration
{
    public function safeUp()
    {
        $this->addColumn('invoice', 'uid', 'CHAR(5) DEFAULT NULL COMMENT "unique id for `out` invoices for quest" AFTER `id` ');
        $this->createIndex('uid_idx', 'invoice', 'uid');
        $this->update('invoice', [
            'uid' => new \yii\db\Expression('MD5(RAND())'),
        ], 'type = 2');
    }
    
    public function safeDown()
    {
        $this->dropIndex('uid_idx', 'invoice');
        $this->dropColumn('invoice', 'uid');
    }
}
