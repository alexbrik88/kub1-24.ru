<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200930_152037_alter_rent_attribute_append_unit extends Migration
{
    public $tableName = 'rent_attribute';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'unit', $this->string(16)->after('title'));

        $this->update($this->tableName, ['unit' => 'кг'], ['alias' => 'carrying']);
        $this->update($this->tableName, ['unit' => 'м<sup>3</sup>'], ['alias' => 'volume']);
        $this->update($this->tableName, ['unit' => 'м'], ['alias' => 'boomLength']);
        $this->update($this->tableName, ['unit' => 'ч'], ['alias' => 'shiftHours']);
    }

    public function down()
    {
        $this->dropColumn($this->tableName, 'unit');
    }

}
