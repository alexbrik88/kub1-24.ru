<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170315_095906_alter_orderPackingList_alterColumns extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%order_packing_list}}', 'quantity', $this->decimal(20, 10)->notNull());
        $this->alterColumn('{{%order_packing_list}}', 'amount_purchase_no_vat', $this->bigInteger(20)->notNull());
        $this->alterColumn('{{%order_packing_list}}', 'amount_purchase_with_vat', $this->bigInteger(20)->notNull());
        $this->alterColumn('{{%order_packing_list}}', 'amount_sales_no_vat', $this->bigInteger(20)->notNull());
        $this->alterColumn('{{%order_packing_list}}', 'amount_sales_with_vat', $this->bigInteger(20)->notNull());
    }
    
    public function safeDown()
    {
        $this->alterColumn('{{%order_packing_list}}', 'quantity', $this->integer());
        $this->alterColumn('{{%order_packing_list}}', 'amount_purchase_no_vat', $this->integer()->notNull());
        $this->alterColumn('{{%order_packing_list}}', 'amount_purchase_with_vat', $this->integer()->notNull());
        $this->alterColumn('{{%order_packing_list}}', 'amount_sales_no_vat', $this->integer()->notNull());
        $this->alterColumn('{{%order_packing_list}}', 'amount_sales_with_vat', $this->integer()->notNull());
    }
}
