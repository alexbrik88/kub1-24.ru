<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210426_072301_alter_employee_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('employee', 'has_finished_unviewed_jobs', $this->boolean()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('employee', 'has_finished_unviewed_jobs');
    }
}