<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210428_091714_alter_plan_cash_foreign_flows_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('plan_cash_foreign_currency_flows', 'rs', $this->string());

        $this->execute('
            UPDATE `plan_cash_foreign_currency_flows` p
            LEFT JOIN `checking_accountant` c ON c.id = p.checking_accountant_id
            SET p.rs = c.rs
            WHERE p.payment_type = 1
        ');
    }

    public function safeDown()
    {
        $this->dropColumn('plan_cash_foreign_currency_flows', 'rs');
    }
}
