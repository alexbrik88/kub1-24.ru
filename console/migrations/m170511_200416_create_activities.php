<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170511_200416_create_activities extends Migration
{
    public $tableName = 'activities';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);
    }
    
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}


