<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200603_142256_alter_order_document_add_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('order_document', 'contractor_director_name', $this->text());
        $this->alterColumn('order_document', 'contractor_director_post_name', $this->text());
        $this->alterColumn('order_document', 'contractor_address_legal_full', $this->text());
        $this->alterColumn('order_document', 'contractor_bank_name', $this->text());
        $this->alterColumn('order_document', 'contractor_bank_city', $this->text());
        $this->alterColumn('order_document', 'contractor_bik', $this->text());
        $this->alterColumn('order_document', 'contractor_inn', $this->text());
        $this->alterColumn('order_document', 'contractor_kpp', $this->text());
        $this->alterColumn('order_document', 'contractor_ks', $this->text());
        $this->alterColumn('order_document', 'contractor_rs', $this->text());
        $this->alterColumn('order_document', 'total_mass_gross', $this->text());
        $this->alterColumn('order_document', 'total_place_count', $this->text());
        $this->addColumn('order_document', 'price_list_id', $this->integer()->after('status_updated_at'));
        $this->addForeignKey('FK_order_document_to_price_list', 'order_document', 'price_list_id', 'price_list', 'id', 'SET NULL');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_order_document_to_price_list', 'order_document');
        $this->dropColumn('order_document', 'price_list_id');
    }
}
