<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%ofd_receipt}}`.
 */
class m200520_174121_create_ofd_receipt_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $p = 0;
        $date = date_create('2016-01-01')->modify('today');
        $maxDate = date_create('first day of this month')->modify('today');
        $options = "PARTITION BY RANGE (TO_DAYS([[date_time]])) (\n\tPARTITION `p{$p}` VALUES LESS THAN (TO_DAYS(\"{$date->format('Y-m-d')}\"))";
        while ($date < $maxDate) {
            $p++;
            $date->modify('+1 month');
            $options .= ",\n\tPARTITION `p{$p}` VALUES LESS THAN (TO_DAYS(\"{$date->format('Y-m-d')}\"))";
        }
        $p++;
        $options .= ",\n\tPARTITION `pMAXVALUE` VALUES LESS THAN MAXVALUE\n)";

        $this->createTable('{{%ofd_receipt}}', [
            'id' => $this->bigInteger()->notNull()->unsigned() . ' AUTO_INCREMENT',
            'company_id' => $this->integer()->notNull(),
            'ofd_id' => $this->integer()->notNull(),
            'kkt_id' => $this->integer()->notNull(),
            'kkt_registration_number' => $this->string()->notNull()->comment('1037 регистрационный номер ККТ'),
            'kkt_uid' => $this->string()->notNull()->comment('Идентификатор ККТ в базе ОФД'),
            'kkt_factory_number' => $this->string()->notNull()->comment('1041 заводской номер ККТ'),
            'operation_type_id' => $this->integer()->notNull(),
            'date_time' => $this->dateTime()->notNull()->comment('1012 дата, время'),
            'document_number' => $this->string()->notNull()->comment('1040 номер фискального документа'),
            'shift_number' => $this->integer()->notNull()->comment('1038 Номер смены'),
            'number' => $this->integer()->notNull()->comment('1042 Номер чека за смену'),
            'operation_type_id' => $this->integer()->notNull(),
            'address' => $this->string()->notNull()->comment('1009 Адрес расчетов'),
            'place' => $this->string()->notNull()->comment('1187 Место расчетов'),
            'total_sum' => $this->bigInteger()->notNull()->comment('1020 Сумма расчета'),
            'cash_sum' => $this->bigInteger()->notNull()->comment('1031 Сумма по чеку наличными'),
            'cashless_sum' => $this->bigInteger()->notNull()->comment('1081 Сумма по чеку безналичными'),
            'nds_20' => $this->bigInteger()->notNull()->comment('1102 Сумма НДС чека по ставке 20%'),
            'nds_10' => $this->bigInteger()->notNull()->comment('1103 Сумма НДС чека по ставке 10%'),
            'nds_0' => $this->bigInteger()->notNull()->comment('1104 Сумма НДС чека по ставке 0%'),
            'nds_no' => $this->bigInteger()->notNull()->comment('1105 Сумма расчета по чеку без НДС'),
            'nds_calculated_20' => $this->bigInteger()->comment('1106 Сумма НДС чека по ставке 20/120'),
            'nds_calculated_10' => $this->bigInteger()->comment('1107 Сумма НДС чека по ставке 10/110'),
            'operator_name' => $this->string()->comment('1021 Кассир'),
            'operator_inn' => $this->string()->comment('1203 ИНН кассира'),
            'fiscal_tag' => $this->string()->comment('1077 Фискальный признак документа'),
            'contractor_name' => $this->string()->comment('1227 Наименование покупателя или клиента'),
            'contractor_inn' => $this->string()->comment('1228 ИНН покупателя или клиента'),
            '_modifiers' => $this->json()->comment('1112 скидка/наценка'),
            'created_at' => $this->integer(),
            'PRIMARY KEY ([[id]], [[date_time]])',
        ], $options);

        $this->createIndex('receipt_unique', '{{%ofd_receipt}}', [
            'company_id',
            'date_time',
            'kkt_registration_number',
            'document_number',
        ], true);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%ofd_receipt}}');
    }
}
