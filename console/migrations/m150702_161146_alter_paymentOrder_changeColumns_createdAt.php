<?php

use yii\db\Schema;
use yii\db\Migration;

class m150702_161146_alter_paymentOrder_changeColumns_createdAt extends Migration
{
    public function safeUp()
    {
        $this->renameColumn('payment_order', 'created_at', 'created_at_old');
        $this->addColumn('payment_order', 'created_at', Schema::TYPE_INTEGER . ' NOT NULL');
        $this->update('payment_order', [
            'created_at' => new \yii\db\Expression('UNIX_TIMESTAMP(created_at_old)'),
        ]);
        $this->dropColumn('payment_order', 'created_at_old');
    }
    
    public function safeDown()
    {
        $this->renameColumn('payment_order', 'created_at', 'created_at_old');
        $this->addColumn('payment_order', 'created_at', Schema::TYPE_DATETIME . ' NOT NULL');
        $this->update('payment_order', [
            'created_at' => new \yii\db\Expression('FROM_UNIXTIME(created_at_old)'),
        ]);
        $this->dropColumn('payment_order', 'created_at_old');
    }
}
