<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181207_165902_alter_company_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('company', 'okud', $this->string()->after('oktmo'));
    }

    public function safeDown()
    {
        $this->dropColumn('company', 'okud');
    }
}


