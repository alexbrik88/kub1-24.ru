<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210723_141421_update_service_subscribe_tariff_group extends Migration
{
    public function safeUp()
    {
        $this->update('{{%service_subscribe_tariff_group}}', ['is_active' => 0], ['id' => 8]);
    }

    public function safeDown()
    {
        $this->update('{{%service_subscribe_tariff_group}}', ['is_active' => 1], ['id' => 8]);
    }
}
