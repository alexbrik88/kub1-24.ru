<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170704_081815_insert_product_unit extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%product_unit}}', [
            'id' => 15,
            'name' => 'км',
            'code_okei' => '008',
            'object_guid' => \frontend\modules\export\models\one_c\OneCExport::generateGUID(),
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%product_unit}}', ['id' => 15]);
    }
}
