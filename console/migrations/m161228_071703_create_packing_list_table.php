<?php

use yii\db\Migration;
use yii\db\Schema;

/**
 * Handles the creation of table `packing_list`.
 */
class m161228_071703_create_packing_list_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('order_packing_list', [
            'id' => Schema::TYPE_PK,
            'order_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Счёт"',
            'packing_list_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Накладная"',
            'product_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Продукция"',
            'quantity' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Количество"',
            'amount_purchase_no_vat' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Сумма покупки без учёта НДС"',
            'amount_purchase_with_vat' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Сумма покупки с учётом НДС"',
            'amount_sales_no_vat' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Сумма продажи без учёта НДС"',
            'amount_sales_with_vat' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Сумма продажи с учётом НДС"'
        ], $tableOptions);

        $this->addForeignKey('order_packing_list_to_order', 'order_packing_list', 'order_id', 'order', 'id');
        $this->addForeignKey('order_packing_list_to_packing_list', 'order_packing_list', 'packing_list_id', 'packing_list', 'id');
        $this->addForeignKey('order_packing_list_to_product', 'order_packing_list', 'product_id', 'product', 'id');
    }
    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('order_packing_list');
    }
}
