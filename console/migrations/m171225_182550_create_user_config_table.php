<?php

use yii\db\Migration;

/**
 * Handles the creation of table `user_config`.
 */
class m171225_182550_create_user_config_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('user_config', [
            'employee_id' => $this->primaryKey(),
            'invoice_scan' => $this->boolean(1)->notNull()->defaultValue(true),
            'invoice_paydate' => $this->boolean(1)->notNull()->defaultValue(false),
            'invoice_act' => $this->boolean(1)->notNull()->defaultValue(true),
            'invoice_paclist' => $this->boolean(1)->notNull()->defaultValue(true),
            'invoice_author' => $this->boolean(1)->notNull()->defaultValue(false),
        ]);

        $this->addForeignKey('FK_user_config_employee', 'user_config', 'employee_id', 'employee', 'id', 'CASCADE');

        $this->addCommentOnColumn('user_config', 'invoice_scan', 'Показ колонки "Скан" в списке счетов');
        $this->addCommentOnColumn('user_config', 'invoice_paydate', 'Показ колонки "Дата оплаты" в списке счетов');
        $this->addCommentOnColumn('user_config', 'invoice_act', 'Показ колонки "Акт" в списке счетов');
        $this->addCommentOnColumn('user_config', 'invoice_paclist', 'Показ колонки "Товарная накладная" в списке счетов');
        $this->addCommentOnColumn('user_config', 'invoice_author', 'Показ колонки "Ответственный" в списке счетов');

        $this->execute("
            INSERT INTO {{user_config}} ([[employee_id]])
            SELECT {{employee}}.[[id]]
            FROM {{employee}}
        ");
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('user_config');
    }
}
