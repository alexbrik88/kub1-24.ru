<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180814_074116_update_agreement_type extends Migration
{
    public $tableName = 'agreement_type';

    public function safeUp()
    {
        $this->update($this->tableName, ['sort' => 20], ['id' => 5]);
        $this->update($this->tableName, ['sort' => 30], ['id' => 7]);
        $this->update($this->tableName, ['sort' => 40], ['id' => 2]);
        $this->update($this->tableName, ['sort' => 50], ['id' => 4]);
        $this->update($this->tableName, ['sort' => 60], ['id' => 6]);
        $this->update($this->tableName, ['sort' => 70], ['id' => 3]);
    }
    
    public function safeDown()
    {
        $this->update($this->tableName, ['sort' => 50], ['id' => 5]);
        $this->update($this->tableName, ['sort' => 45], ['id' => 7]);
        $this->update($this->tableName, ['sort' => 20], ['id' => 2]);
        $this->update($this->tableName, ['sort' => 40], ['id' => 4]);
        $this->update($this->tableName, ['sort' => 60], ['id' => 6]);
        $this->update($this->tableName, ['sort' => 30], ['id' => 3]);
    }
}


