<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180221_211755_add_comment_to_product extends Migration
{
    public $tableName = 'product';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'comment_photo', $this->text()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'comment_photo');
    }
}


