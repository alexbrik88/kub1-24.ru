<?php

use console\components\db\Migration;

class m191108_111524_create_bitrix24_user extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{bitrix24_user}}', [
            'id' => $this->primaryKey()->unsigned()->notNull(),
            'member_id' => 'CHAR(32) NOT NULL COMMENT "ID пользователя Битрикс24"',
            'employee_id' => $this->integer()->comment('ID клиента КУБ'),
        ], "COMMENT 'Интеграция Битрикс24: связь пользователей с клиентами КУБ'");
        $this->createIndex('bitrix24_user_member_id', '{{bitrix24_user}}', 'member_id', true);
        $this->addForeignKey(
            'bitrix24_user_employee_id', '{{bitrix24_user}}', 'employee_id',
            '{{employee}}', 'id', 'CASCADE', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable('{{bitrix24_user}}');

    }
}
