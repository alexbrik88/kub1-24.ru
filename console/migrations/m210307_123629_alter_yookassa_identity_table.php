<?php

use yii\db\Migration;

class m210307_123629_alter_yookassa_identity_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%yookassa_identity}}';

    /**
     * @inheritDoc
     */
    public function up()
    {
        $this->dropForeignKey('fk_yandex_shop__employee_company', self::TABLE_NAME);
        $this->dropPrimaryKey('yookassa_identity', self::TABLE_NAME);
        $this->dropIndex('ck_account_id', self::TABLE_NAME);
        $this->dropColumn(self::TABLE_NAME, 'employee_id');
        $this->dropColumn(self::TABLE_NAME, 'refresh_token');
        $this->alterColumn(self::TABLE_NAME, 'access_token', $this->string(128)->notNull());
        $this->alterColumn(self::TABLE_NAME, 'company_id', $this->integer()->notNull()->after('account_id'));
        $this->createPrimaryKey();
        $this->createIndex('ck_company_id', self::TABLE_NAME, ['company_id']);
        $this->addForeignKey(
            'fk_yookassa_identity__company',
            self::TABLE_NAME,
            ['company_id'],
            '{{%company}}',
            ['id'],
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        return false;
    }

    /**
     * @return void
     */
    private function createPrimaryKey(): void
    {
        $sql = 'ALTER IGNORE TABLE {{%yookassa_identity}} ADD CONSTRAINT `pk_account_id` PRIMARY KEY (`account_id`)';
        $this->execute($sql);
    }
}
