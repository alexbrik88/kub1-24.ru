<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190215_132700_proxy_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%proxy}}', 'proxy_person_id', $this->integer()->after('invoice_id'));
        $this->addColumn('{{%proxy}}', 'limit_date', Schema::TYPE_DATE);

        $this->addForeignKey('FK_proxy_to_proxy_person', '{{%proxy}}', 'proxy_person_id', 'employee', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey(
            'FK_proxy_to_proxy_person',
            '{{%proxy}}'
        );

        $this->dropColumn('{{%proxy}}', 'limit_date');
        $this->dropColumn('{{%proxy}}', 'proxy_person_id');
    }
}
