<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170523_080058_insert_employeeRole extends Migration
{
    public function safeUp()
    {
        $this->insert('employee_role', [
            'id' => 6,
            'name' => 'Руководитель отдела (только просмотр)',
        ]);
    }
    
    public function safeDown()
    {
        $this->delete('employee_role', ['id' => 6]);
    }
}
