<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180515_152538_alter_order_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('order', 'currency_price_no_vat', $this->decimal(22, 2)->notNull()->defaultValue(0)->after('mass_gross'));
        $this->addColumn('order', 'currency_price_with_vat', $this->decimal(22, 2)->notNull()->defaultValue(0)->after('currency_price_no_vat'));

        $this->execute("
            UPDATE {{order}}
            SET {{order}}.[[currency_price_no_vat]] = {{order}}.[[base_price_no_vat]],
                {{order}}.[[currency_price_with_vat]] = {{order}}.[[base_price_with_vat]]
        ");
    }

    public function safeDown()
    {
        $this->dropColumn('order', 'currency_price_no_vat');
        $this->dropColumn('order', 'currency_price_with_vat');
    }
}
