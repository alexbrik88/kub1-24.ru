<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180201_075018_alter_invoice_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('invoice', 'is_additional_number_before', $this->boolean()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('invoice', 'is_additional_number_before');
    }
}
