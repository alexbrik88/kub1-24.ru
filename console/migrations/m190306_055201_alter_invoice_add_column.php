<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190306_055201_alter_invoice_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('invoice', 'from_demo_out_invoice', $this->boolean()->defaultValue(false)->after('from_out_invoice'));
        $this->createIndex('from_demo_out_invoice', 'invoice', ['company_id', 'from_demo_out_invoice']);
    }

    public function safeDown()
    {
        $this->dropIndex('from_demo_out_invoice', 'invoice');
        $this->dropColumn('invoice', 'from_demo_out_invoice');
    }
}
