<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180210_150829_create_order_document extends Migration
{
    public $tOrderDocument = 'order_document';
    public $tOrderDocumentStatus = 'order_document_status';

    public function safeUp()
    {
        $this->createTable($this->tOrderDocument, [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'author_id' => $this->integer()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'contractor_id' => $this->integer()->notNull(),
            'status_id' => $this->integer()->notNull(),
            'status_author_id' => $this->integer()->defaultValue(null),
            'status_updated_at' => $this->integer()->defaultValue(null),
            'production_type' => $this->string()->notNull(),
            'document_number' => $this->string()->notNull(),
            'document_additional_number' => $this->string(0)->defaultValue(null),
            'document_date' => $this->date()->notNull(),
            'basis_document_number' => $this->string(50)->defaultValue(null),
            'basis_document_name' => $this->string()->defaultValue(null),
            'basis_document_date' => $this->date()->defaultValue(null),
            'basis_document_type_id' => $this->integer()->defaultValue(null),
            'ship_up_to_date' => $this->date()->notNull(),
            'is_deleted' => $this->boolean()->defaultValue(false),
            'comment' => $this->text()->defaultValue(null),
            'has_nds' => $this->boolean()->defaultValue(false),
            'email_messages_count' => $this->integer()->defaultValue(0),
            'total_amount' => $this->bigInteger(20)->defaultValue(null),
            'total_mass' => $this->integer()->defaultValue(null),
            'total_order_count' => $this->integer()->defaultValue(null),
            'contractor_name_short' => $this->string()->notNull(),
            'contractor_name_full' => $this->string()->notNull(),
            'contractor_director_name' => $this->string()->defaultValue(null),
            'contractor_director_post_name' => $this->string()->defaultValue(null),
            'contractor_address_legal_full' => $this->string()->defaultValue(null),
            'contractor_bank_name' => $this->string(45)->defaultValue(null),
            'contractor_bik' => $this->string(9)->defaultValue(null),
            'contractor_inn' => $this->string(45)->defaultValue(null),
            'contractor_kpp' => $this->string(45)->defaultValue(null),
            'contractor_ks' => $this->string(20)->defaultValue(null),
            'contractor_rs' => $this->string(20)->defaultValue(null),
        ]);

        $this->addForeignKey($this->tOrderDocument . '_author_id', $this->tOrderDocument, 'author_id', 'employee', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tOrderDocument . '_company_id', $this->tOrderDocument, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tOrderDocument . '_contractor_id', $this->tOrderDocument, 'contractor_id', 'contractor', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tOrderDocument . '_status_author_id', $this->tOrderDocument, 'status_author_id', 'employee', 'id', 'RESTRICT', 'CASCADE');

        $this->createTable($this->tOrderDocumentStatus, [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'color' => $this->string()->defaultValue(null),
        ]);

        $this->batchInsert($this->tOrderDocumentStatus, ['id', 'name', 'color'], [
            [1, 'Новый', '#dfba49',],
            [2, 'Получен', '#dfba49',],
            [3, 'Отправлен', '#dfba49',],
            [4, 'Подтвержден', '#c8d046',],
            [5, 'В обработке', '#c49f47',],
            [6, 'Собран', '#E87E04',],
            [7, 'Отгружен', '#4B77BE',],
            [8, 'Доставлен', '#26C281',],
            [9, 'Оплачен', '#45b6af',],
            [10, 'Возврат', '#f3565d',],
            [11, 'Отменен', '#808080',],
        ]);

        $this->addForeignKey($this->tOrderDocument . '_status_id', $this->tOrderDocument, 'status_id', $this->tOrderDocumentStatus, 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey($this->tOrderDocument . '_status_id', $this->tOrderDocument);
        $this->dropForeignKey($this->tOrderDocument . '_author_id', $this->tOrderDocument);
        $this->dropForeignKey($this->tOrderDocument . '_company_id', $this->tOrderDocument);
        $this->dropForeignKey($this->tOrderDocument . '_contractor_id', $this->tOrderDocument);
        $this->dropForeignKey($this->tOrderDocument . '_status_author_id', $this->tOrderDocument);

        $this->dropTable($this->tOrderDocumentStatus);
        $this->dropTable($this->tOrderDocument);
    }
}


