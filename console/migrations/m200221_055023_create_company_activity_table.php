<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%company_activity}}`.
 */
class m200221_055023_create_company_activity_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%company_activity}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);

        $this->batchInsert('{{%company_activity}}', ['id', 'name'], [
            [1, 'Оказание услуг'],
            [2, 'Продажа товаров (не магазин)'],
            [3, 'Магазин'],
            [4, 'Интернет магазин'],
            [5, 'Строительство'],
            [6, 'Производство'],
            [7, 'Другое'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%company_activity}}');
    }
}
