<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210404_202122_alter_1c_objects_add_column extends Migration
{
    public static $columnImport = 'one_c_imported';
    public static $columnExport = 'one_c_exported';

    public static $tables = [
        'invoice',
        'agreement',
        'act',
        'packing_list',
        'invoice_facture',
        'upd',
        'sales_invoice',
        'product',
        'contractor'
    ];

    public function safeUp()
    {
        foreach (self::$tables as $table) {
            $this->addColumn($table, self::$columnImport, $this->tinyInteger()->defaultValue(0));
            $this->addColumn($table, self::$columnExport, $this->tinyInteger()->defaultValue(0));
        }
    }

    public function safeDown()
    {
        foreach (self::$tables as $table) {
            $this->dropColumn($table, self::$columnImport);
            $this->dropColumn($table, self::$columnExport);
        }
    }
}
