<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210601_072244_create_product_turnover_trigger extends Migration
{
    public $dropSql = '
        DROP TRIGGER IF EXISTS `product_turnover__ofd_receipt_item__after_update`;
        DROP TRIGGER IF EXISTS `product_turnover__ofd_receipt_item__after_delete`;
    ';

    public function safeUp()
    {
        $this->execute($this->dropSql);

        $this->execute('
CREATE TRIGGER `product_turnover__ofd_receipt_item__after_update` AFTER UPDATE ON `ofd_receipt_item`
FOR EACH ROW
BEGIN
    IF NEW.`product_id` IS NOT NULL THEN
        INSERT INTO `product_turnover` (
            `order_id`,
            `document_id`,
            `document_table`,
            `invoice_id`,
            `contractor_id`,
            `company_id`,
            `date`,
            `type`,
            `production_type`,
            `is_invoice_actual`,
            `is_document_actual`,
            `purchase_price`,
            `price_one`,
            `quantity`,
            `total_amount`,
            `purchase_amount`,
            `margin`,
            `year`,
            `month`,
            `product_group_id`,
            `product_id`,
            `not_for_sale`
        ) SELECT
            NEW.`id`,
            NEW.`receipt_id`,
            "ofd_receipt",
            null,
            null,
            NEW.`company_id`,
            @date:=DATE(NEW.`date_time`),
            2,
            IFNULL(`product`.`production_type`, 1),
            1,
            1,
            IFNULL(`product`.`price_for_buy_with_nds`, 0),
            @price:=IFNULL(NEW.`price`, 0),
            @quantity:=IFNULL(NEW.`quantity`, 0),
            @total:=ROUND(@price * @quantity, 0),
            @purchase:=ROUND(@quantity * IFNULL(`product`.`price_for_buy_with_nds`, 0)),
            (@total - @purchase),
            YEAR(@date),
            MONTH(@date),
            IFNULL(`product`.`group_id`, 1),
            `product`.`id`,
            0
        FROM `ofd_receipt_item`
        LEFT JOIN `product` ON `product`.`id` = NEW.`product_id`
        ON DUPLICATE KEY UPDATE
            `date` = VALUES(`date`),
            `type` = VALUES(`type`),
            `production_type` = VALUES(`production_type`),
            `price_one` = VALUES(`price_one`),
            `quantity` = VALUES(`quantity`),
            `total_amount` = VALUES(`total_amount`),
            `purchase_amount` = VALUES(`purchase_amount`),
            `margin` = VALUES(`margin`),
            `year` = VALUES(`year`),
            `month` = VALUES(`month`),
            `product_group_id` = VALUES(`product_group_id`),
            `product_id` = VALUES(`product_id`);
    ELSE
        DELETE FROM `product_turnover`
        WHERE `order_id` = OLD.`id`
        AND `document_id` = OLD.`receipt_id`
        AND `document_table` = "ofd_receipt";
    END IF;
END;
        ');

        $this->execute('
CREATE TRIGGER `product_turnover__ofd_receipt_item__after_delete` AFTER DELETE ON `ofd_receipt_item`
FOR EACH ROW
BEGIN
    DELETE FROM `product_turnover`
    WHERE `order_id` = OLD.`id`
    AND `document_id` = OLD.`receipt_id`
    AND `document_table` = "ofd_receipt";
END;
        ');
    }

    public function safeDown()
    {
        $this->execute($this->dropSql);
    }
}
