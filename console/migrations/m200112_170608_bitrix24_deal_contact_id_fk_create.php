<?php

use console\components\db\Migration;

class m200112_170608_bitrix24_deal_contact_id_fk_create extends Migration
{
    const TABLE = '{{%bitrix24_deal}}';

    public function safeUp()
    {
        $this->addForeignKey(
            'bitrix24_deal_contact_id', self::TABLE, ['company_id', 'contact_id'],
            '{{%bitrix24_contact}}', ['company_id', 'id'],
            'CASCADE', 'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('bitrix24_deal_contact_id', self::TABLE);
    }
}
