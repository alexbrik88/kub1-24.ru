<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201015_155631_alter_upd_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%upd}}', 'orders_sum', $this->integer(11)->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%upd}}', 'orders_sum');
    }
}
