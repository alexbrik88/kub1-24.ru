<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191128_180636_create_company_acquiring extends Migration
{
    public $relTable = 'company_to_acquiring';

    public function safeUp()
    {
        $companyTable = 'company';
        $accountTable = 'checking_accountant';

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->relTable, [
            'company_id' => $this->integer()->notNull(),
            'account_id' => $this->integer()->notNull()
        ], $tableOptions);
        $this->addPrimaryKey('PRIMARY_KEY', $this->relTable, ['company_id', 'account_id']);
        $this->addForeignKey("FK_{$this->relTable}_{$companyTable}", $this->relTable, 'company_id', $companyTable, 'id', 'CASCADE');
        $this->addForeignKey("FK_{$this->relTable}_{$accountTable}", $this->relTable, 'account_id', $accountTable, 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $companyTable = 'company';
        $accountTable = 'checking_accountant';

        $this->dropForeignKey("FK_{$this->relTable}_{$companyTable}", $this->relTable);
        $this->dropForeignKey("FK_{$this->relTable}_{$accountTable}", $this->relTable);

        $this->dropTable($this->relTable);
    }
}
