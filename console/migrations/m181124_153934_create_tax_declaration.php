<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181124_153934_create_tax_declaration extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        // tax_declaration_status
        $this->createTable('{{%tax_declaration_status}}', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ], $tableOptions);

        $this->batchInsert('{{%tax_declaration_status}}', ['id', 'name'], [
            [1, 'Создана'],
            [2, 'Распечатана'],
            [3, 'Передана'],
            [4, 'Принята'],
            [5, 'Скорректирована'],
        ]);

        // tax_declaration_quarter
        $this->createTable('{{%tax_declaration_quarter}}', [
            'id' => Schema::TYPE_PK,

            'tax_declaration_id' => Schema::TYPE_INTEGER . '(11) NOT NULL',

            'quarter_number' => Schema::TYPE_SMALLINT . '(1) NOT NULL',

            'oktmo' => Schema::TYPE_STRING . '(11) NOT NULL DEFAULT ""',
            'income_amount' => Schema::TYPE_DECIMAL . '(22,2) NOT NULL DEFAULT "0.00"',
            'outcome_amount' => Schema::TYPE_DECIMAL . '(22,2) NOT NULL DEFAULT "0.00"',
            'tax_rate' => Schema::TYPE_DECIMAL . '(3,2) DEFAULT 0.0',
            'prepayment_tax_amount' => Schema::TYPE_DECIMAL . '(22,2) NOT NULL DEFAULT "0.00"',
            'tax_amount' => Schema::TYPE_DECIMAL . '(22,2) NOT NULL DEFAULT "0.00"',
            'created_at' => Schema::TYPE_INTEGER . '(11) NOT NULL',
        ]);

        // tax_declaration
        $this->createTable('{{%tax_declaration}}', [
            'id' => Schema::TYPE_PK,

            'uid' => Schema::TYPE_STRING . '(5) DEFAULT NULL',

            'company_id' => Schema::TYPE_INTEGER . '(11) NOT NULL',

            'created_at' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'document_author_id' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'status_id' => Schema::TYPE_INTEGER . '(11) DEFAULT NULL',
            'status_updated_at' => Schema::TYPE_INTEGER . '(11) DEFAULT NULL',
            'status_author_id' => Schema::TYPE_INTEGER . '(11) DEFAULT NULL',

            'document_date' => Schema::TYPE_DATE . ' NOT NULL',
            'document_correction_number' => Schema::TYPE_INTEGER . '(4) DEFAULT 0',
            'document_knd' => Schema::TYPE_STRING . '(7) NOT NULL DEFAULT "" ',

            'tax_period' => Schema::TYPE_STRING . '(2) NOT NULL DEFAULT "" ',
            'tax_year' => Schema::TYPE_INTEGER . '(4) NOT NULL',
            'tax_service_ifns' => Schema::TYPE_STRING . '(4) NOT NULL DEFAULT "" ',
            'tax_service_location' => Schema::TYPE_STRING . '(3) NOT NULL DEFAULT "" ',

            'declaration_maker_code' => Schema::TYPE_STRING . '(1) NOT NULL DEFAULT "1" ',
            'declaration_maker_fio' => Schema::TYPE_STRING . '(60) NOT NULL DEFAULT "" ',
            'declaration_maker_organization' => Schema::TYPE_STRING . '(160) NOT NULL DEFAULT "" ',

            'taxpayer_sign' => Schema::TYPE_SMALLINT . '(1) NOT NULL DEFAULT 1', // Признак налогоплательщика

            'add_signature' => Schema::TYPE_SMALLINT . '(1) DEFAULT 0',
        ]);

        $this->addForeignKey('FK_tax_declaration_to_company', '{{%tax_declaration}}', 'company_id', 'company', 'id');
        $this->addForeignKey('FK_tax_declaration_to_tax_declaration_status', '{{%tax_declaration}}', 'status_id', 'tax_declaration_status', 'id');
        $this->addForeignKey('FK_tax_declaration_to_document_author', '{{%tax_declaration}}', 'document_author_id', 'employee', 'id');
        $this->addForeignKey('FK_tax_declaration_to_status_out_author', '{{%tax_declaration}}', 'status_author_id', 'employee', 'id');

        $this->addForeignKey('FK_tax_declaration_quarter_to_tax_declaration', '{{%tax_declaration_quarter}}', 'tax_declaration_id', 'tax_declaration', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_tax_declaration_to_company', '{{%tax_declaration}}');
        $this->dropForeignKey('FK_tax_declaration_to_tax_declaration_status', '{{%tax_declaration}}');
        $this->dropForeignKey('FK_tax_declaration_to_document_author', '{{%tax_declaration}}');
        $this->dropForeignKey('FK_tax_declaration_to_status_out_author', '{{%tax_declaration}}');
        $this->dropForeignKey('FK_tax_declaration_quarter_to_tax_declaration', '{{%tax_declaration_quarter}}');


        $this->dropTable('{{%tax_declaration}}');
        $this->dropTable('{{%tax_declaration_status}}');
        $this->dropTable('{{%tax_declaration_quarter}}');
    }
}


