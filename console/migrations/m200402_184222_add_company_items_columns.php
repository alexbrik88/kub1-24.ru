<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200402_184222_add_company_items_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%company}}', 'default_contractor_expenditure_item_id', $this->integer()->defaultValue(13));
        $this->addForeignKey('FK_company_to_invoice_expenditure_item', 'company', 'default_contractor_expenditure_item_id', 'invoice_expenditure_item', 'id', 'CASCADE', 'CASCADE');
        $this->addColumn('{{%company}}', 'default_contractor_income_item_id', $this->integer()->defaultValue(1));
        $this->addForeignKey('FK_company_to_invoice_income_item', 'company', 'default_contractor_income_item_id', 'invoice_income_item', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_company_to_invoice_income_item', 'company');
        $this->dropColumn('{{%company}}', 'default_contractor_income_item_id');
        $this->dropForeignKey('FK_company_to_invoice_expenditure_item', 'company');
        $this->dropColumn('{{%company}}', 'default_contractor_expenditure_item_id');
    }
}
