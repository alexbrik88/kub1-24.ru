<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181026_063240_drop_agreement_new extends Migration
{
    public function safeUp()
    {
        $this->dropForeignKey('fk_agreement_new_company', '{{%agreement_new}}');
        $this->dropForeignKey('fk_agreement_new_contractor', '{{%agreement_new}}');
        $this->dropForeignKey('fk_agreement_new_employee', '{{%agreement_new}}');
        $this->dropTable('{{%agreement_new}}');
    }
    
    public function safeDown()
    {

    }
}


