<?php

use console\components\db\Migration;
use yii\db\Schema;

class m220215_075855_insert_registration_page_type extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%registration_page_type}}', [
            'id' => 51,
            'name' => 'Банк Открытие. Финдиректор',
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%registration_page_type}}', [
            'id' => 51,
        ]);
    }
}
