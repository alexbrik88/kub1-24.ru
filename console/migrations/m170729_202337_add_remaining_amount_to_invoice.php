<?php

use console\components\db\Migration;
use yii\db\Schema;
use common\models\document\Invoice;

class m170729_202337_add_remaining_amount_to_invoice extends Migration
{
    public $tableName = 'invoice';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'remaining_amount', $this->integer()->defaultValue(null));
        /* @var $invoice Invoice */
        foreach (Invoice::find()->byDeleted()->andWhere(['not', ['payment_partial_amount' => null]])->all() as $invoice) {
            if ($invoice->payment_partial_amount >= $invoice->total_amount_with_nds || $invoice->payment_partial_amount == 0) {
                $this->update($this->tableName, ['payment_partial_amount' => null], ['id' => $invoice->id]);
            } else {
                $this->update($this->tableName, ['remaining_amount' => $invoice->total_amount_with_nds - $invoice->payment_partial_amount], ['id' => $invoice->id]);
            }
        }
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'remaining_amount');
    }
}


