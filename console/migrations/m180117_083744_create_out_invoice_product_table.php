<?php

use yii\db\Migration;

/**
 * Handles the creation of table `out_invoice_product`.
 */
class m180117_083744_create_out_invoice_product_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('out_invoice_product', [
            'out_invoice_id' => $this->string(50)->notNull(),
            'product_id' => $this->integer()->notNull(),
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', 'out_invoice_product', ['out_invoice_id', 'product_id']);
        $this->addForeignKey('FK_outInvoiceProduct_outInvoice', 'out_invoice_product', 'out_invoice_id', 'out_invoice', 'id', 'CASCADE');
        $this->addForeignKey('FK_outInvoiceProduct_product', 'out_invoice_product', 'product_id', 'product', 'id', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('out_invoice_product');
    }
}
