<?php

use yii\db\Migration;

/**
 * Handles the creation of table `sent_email`.
 */
class m180323_115802_create_sent_email_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('sent_email', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer(),
            'employee_id' => $this->integer(),
            'email_type_id' => $this->integer()->notNull(),
            'email_type_number' => $this->integer()->notNull()->defaultValue(1),
            'created_at' => $this->integer()->notNull(),
            'email' => $this->text(),
        ]);

        $this->addForeignKey('FK_sentEmail_company', 'sent_email', 'company_id', 'company', 'id', 'SET NULL');
        $this->addForeignKey('FK_sentEmail_employee', 'sent_email', 'employee_id', 'employee', 'id', 'SET NULL');
        $this->addForeignKey('FK_sentEmail_sentEmailType', 'sent_email', 'email_type_id', 'sent_email_type', 'id');

        $this->createIndex('email_type_number', 'sent_email', 'email_type_number');
        $this->createIndex('created_at', 'sent_email', 'created_at');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('sent_email');
    }
}
