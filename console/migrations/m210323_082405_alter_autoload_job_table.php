<?php

use yii\db\Migration;

class m210323_082405_alter_autoload_job_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%autoload_job}}';

    /**
     * @inheritDoc
     * @throws
     */
    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'identifier', $this->string()->notNull()->defaultValue(''));
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'identifier');
    }
}
