<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191208_154147_alter_scan_recognize_document_add_column extends Migration
{
    public $table = 'scan_recognize_document';

    public function safeUp()
    {
        $this->addColumn($this->table, 'vat', $this->string(16)->notNull()->defaultValue('')->after('document_date'));
        $this->addColumn($this->table, 'amount_without_vat', $this->string(16)->notNull()->defaultValue('')->after('document_date'));
        $this->addColumn($this->table, 'amount', $this->string(16)->notNull()->defaultValue('')->after('document_date'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'amount');
        $this->dropColumn($this->table, 'amount_without_vat');
        $this->dropColumn($this->table, 'vat');
    }
}
