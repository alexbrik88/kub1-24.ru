<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201210_093437_alter_cash_bank_foreign_currency_flows_add_column extends Migration
{
    private $table = '{{%cash_bank_foreign_currency_flows}}';

    public function safeUp()
    {
        $this->addColumn($this->table, 'is_internal_transfer', $this->boolean()->notNull()->defaultValue(false)->after('company_id'));
        $this->addColumn($this->table, 'internal_transfer_account_table', $this->string()->null()->after('is_internal_transfer'));
        $this->addColumn($this->table, 'internal_transfer_account_id', $this->integer()->null()->after('internal_transfer_account_table'));
        $this->addColumn($this->table, 'internal_transfer_flow_table', $this->string()->null()->after('internal_transfer_account_id'));
        $this->addColumn($this->table, 'internal_transfer_flow_id', $this->integer()->null()->after('internal_transfer_flow_table'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'is_internal_transfer');
        $this->dropColumn($this->table, 'internal_transfer_account_table');
        $this->dropColumn($this->table, 'internal_transfer_account_id');
        $this->dropColumn($this->table, 'internal_transfer_flow_table');
        $this->dropColumn($this->table, 'internal_transfer_flow_id');
    }
}
