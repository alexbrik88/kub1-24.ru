<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200825_133112_create_one_s_autoload_email extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('one_s_autoload_email', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'email' => $this->string(255)->notNull(),
            'password' => $this->string(16)->notNull()->defaultValue(''),
            'created_at' => $this->integer()->notNull(),
            'uploaded_at' => $this->integer(),
            'enabled' => $this->boolean()->notNull()->defaultValue(0),
            'in_work' => $this->tinyInteger()->notNull()->defaultValue(0),
        ]);

        $this->addForeignKey('FK_one_s_autoload_email_company', 'one_s_autoload_email', 'company_id', 'company', 'id', 'CASCADE');
        $this->createIndex('IDX_email', 'one_s_autoload_email', 'email', true);
        $this->createIndex('IDX_in_work', 'one_s_autoload_email', ['enabled', 'in_work']);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('FK_one_s_autoload_email_company', 'one_s_autoload_email');
        $this->dropTable('one_s_autoload_email');
    }
}
