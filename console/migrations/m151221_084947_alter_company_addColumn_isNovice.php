<?php

use console\components\db\Migration;
use yii\db\Schema;

class m151221_084947_alter_company_addColumn_isNovice extends Migration
{
    public $tCompany = 'company';

    public function safeUp()
    {
        $this->addColumn($this->tCompany, 'is_novice', $this->boolean()->defaultValue(true));

        $this->execute("
            UPDATE {$this->tCompany} AS company
                LEFT JOIN (
                    SELECT service_subscribe.company_id, COUNT(*) AS subscribe_count
                        FROM service_subscribe
                        WHERE tariff_id <> 4
                        GROUP BY service_subscribe.company_id
                ) AS stat ON stat.company_id = company.id
                SET is_novice = COALESCE(stat.subscribe_count = 0, 1)
        ");
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tCompany, 'is_novice');
    }
}


