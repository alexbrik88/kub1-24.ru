<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160926_081904_alter_contractor_addColumn_ndsViewTypeId extends Migration
{
    public $tContractor = '{{%contractor}}';

    public function safeUp()
    {
        $this->addColumn($this->tContractor, 'nds_view_type_id', $this->integer(1));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tContractor, 'nds_view_type_id');
    }
}


