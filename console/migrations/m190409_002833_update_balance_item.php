<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190409_002833_update_balance_item extends Migration
{
    public $tableName = '{{%balance_item}}';

    public function safeUp()
    {
        $this->update($this->tableName, ['name' => 'Оборудование, офисная техника'], ['id' => 2]);
    }

    public function safeDown()
    {
        $this->update($this->tableName, ['name' => 'Торговое оборудование, офисная техника'], ['id' => 2]);
    }
}
