<?php

use console\components\db\Migration;

class m160605_144731_alter_colum_bin_to_contractor extends Migration
{
    public $tContractor = 'contractor';

    public function safeUp()
    {
        $this->alterColumn($this->tContractor, 'BIN', $this->char(13));
    }

    public function safeDown()
    {
        $this->alterColumn($this->tContractor, 'BIN', $this->char(13)->notNull());
    }
}


