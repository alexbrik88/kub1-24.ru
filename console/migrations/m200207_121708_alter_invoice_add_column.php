<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200207_121708_alter_invoice_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%invoice}}', 'contractor_passport', $this->text()->after('contractor_rs'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%invoice}}', 'contractor_passport');
    }
}
