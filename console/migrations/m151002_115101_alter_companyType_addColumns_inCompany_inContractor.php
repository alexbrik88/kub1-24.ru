<?php

use yii\db\Schema;
use yii\db\Migration;

class m151002_115101_alter_companyType_addColumns_inCompany_inContractor extends Migration
{
    public $tCompanyType = 'company_type';

    public function safeUp()
    {
        $this->addColumn($this->tCompanyType, 'in_company', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tCompanyType, 'in_contractor', $this->boolean()->notNull()->defaultValue(false));

        $this->update($this->tCompanyType, [
            'in_company' => true,
        ], 'id IN (1,2,3,4)');

        $this->update($this->tCompanyType, [
            'in_contractor' => true,
        ]);
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tCompanyType, 'in_company');
        $this->dropColumn($this->tCompanyType, 'in_contractor');
    }
}
