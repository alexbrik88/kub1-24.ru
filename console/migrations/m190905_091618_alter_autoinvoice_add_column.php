<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190905_091618_alter_autoinvoice_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%autoinvoice}}', 'month', $this->smallInteger(1)->after('period'));

        $this->execute('
            UPDATE {{%autoinvoice}}
            SET [[month]] = (@month:=(([[day]]-1) DIV 31) + 1),
                [[day]] = [[day]] - ((@month - 1) * 31)
            WHERE [[period]] = 2
        ');
    }

    public function safeDown()
    {
        $this->execute('
            UPDATE {{%autoinvoice}}
            SET [[day]] = [[day]] + (([[month]] - 1) * 31)
            WHERE [[period]] = 2
        ');

        $this->dropColumn('{{%autoinvoice}}', 'month');
    }
}
