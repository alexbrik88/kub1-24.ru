<?php

use console\components\db\Migration;
use yii\db\Schema;

class m220223_171501_alter_config_add_column extends Migration
{
    public $tableName = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'selling_report_search_by', $this->tinyInteger()->notNull()->defaultValue(0));
        $this->addColumn($this->tableName, 'selling_report_type', $this->tinyInteger()->notNull()->defaultValue(0));
        $this->addColumn($this->tableName, 'selling_report_by_contractor', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'selling_report_percent', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'selling_report_percent_total', $this->boolean()->notNull()->defaultValue(true));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'selling_report_search_by');
        $this->dropColumn($this->tableName, 'selling_report_type');
        $this->dropColumn($this->tableName, 'selling_report_by_contractor');
        $this->dropColumn($this->tableName, 'selling_report_percent');
        $this->dropColumn($this->tableName, 'selling_report_percent_total');
    }
}
