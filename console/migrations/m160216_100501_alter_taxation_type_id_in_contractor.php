<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160216_100501_alter_taxation_type_id_in_contractor extends Migration
{
    public $tContractor = 'contractor';

    public function safeUp()
    {
        $this->alterColumn($this->tContractor, 'taxation_system', $this->boolean());
    }
    
    public function safeDown()
    {
        $this->alterColumn($this->tContractor, 'taxation_system', $this->boolean()->notNull());
    }
}


