<?php

use yii\db\Migration;

class m210514_120132_alter_user_config_table extends Migration
{
    private const TABLE_NAME = '{{%user_config}}';

    /**
     * @inheritDoc
     */
    public function up(): void
    {
        $this->addColumn(self::TABLE_NAME, 'crm_client_created', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn(self::TABLE_NAME, 'crm_contact_name', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn(self::TABLE_NAME, 'crm_client_position', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn(self::TABLE_NAME, 'crm_contact_email', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn(self::TABLE_NAME, 'crm_client_deal', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn(self::TABLE_NAME, 'crm_client_deal_stage', $this->boolean()->notNull()->defaultValue(true));
    }

    /**
     * @inheritDoc
     */
    public function down(): void
    {
        $this->dropColumn(self::TABLE_NAME, 'crm_client_created');
        $this->dropColumn(self::TABLE_NAME, 'crm_contact_name');
        $this->dropColumn(self::TABLE_NAME, 'crm_client_position');
        $this->dropColumn(self::TABLE_NAME, 'crm_contact_email');
        $this->dropColumn(self::TABLE_NAME, 'crm_client_deal');
        $this->dropColumn(self::TABLE_NAME, 'crm_client_deal_stage');
    }
}
