<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200529_093114_alter_company_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('company', 'name_short_en', $this->string(255));
        $this->addColumn('company', 'name_full_en', $this->string(255));
        $this->addColumn('company', 'form_legal_en', $this->string(45));
        $this->addColumn('company', 'address_legal_en', $this->string(255));
        $this->addColumn('company', 'address_actual_en', $this->string(255));
        $this->addColumn('company', 'chief_post_name_en', $this->string(255));
        $this->addColumn('company', 'lastname_en', $this->string(45));
        $this->addColumn('company', 'firstname_en', $this->string(45));
        $this->addColumn('company', 'patronymic_en', $this->string(45));
        $this->addColumn('company', 'not_has_patronymic_en', $this->integer(1));
    }

    public function safeDown()
    {
        $this->dropColumn('company', 'name_short_en');
        $this->dropColumn('company', 'name_full_en');
        $this->dropColumn('company', 'form_legal_en');
        $this->dropColumn('company', 'address_legal_en');
        $this->dropColumn('company', 'address_actual_en');
        $this->dropColumn('company', 'chief_post_name_en');
        $this->dropColumn('company', 'lastname_en');
        $this->dropColumn('company', 'firstname_en');
        $this->dropColumn('company', 'patronymic_en');
        $this->dropColumn('company', 'not_has_patronymic_en');
    }
}
