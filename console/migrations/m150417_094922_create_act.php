<?php

use yii\db\Schema;
use yii\db\Migration;

class m150417_094922_create_act extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('act_status', [
            'id' => Schema::TYPE_PK,
            'act_title' => Schema::TYPE_STRING . ' NOT NULL COMMENT "Название"',
        ], $tableOptions);

        $this->batchInsert('act_status', ['id', 'act_title',], [
            [1, 'Создан',],
            [2, 'Распечатан',],
            [3, 'Передан',],
            [4, 'Получен-подписан',],
        ]);

        $this->createTable('act', [
            'id' => Schema::TYPE_PK,
            'creation_date' => Schema::TYPE_DATE . ' COMMENT "Дата создания"',
            'author' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Автор (id сотрудника)"',
            'invoice_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Счёт"',
            'act_status_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'status_date' => Schema::TYPE_DATE . ' COMMENT "Дата возникновения текущего статуса"',
            'status_author' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Автор текущего статуса"',
            'update_date' => Schema::TYPE_DATE . ' COMMENT "Дата обновления акта"',
            'additional_number' => Schema::TYPE_STRING . ' COMMENT "Дополнительный номер акта"',
            'file' => Schema::TYPE_STRING . ' COMMENT "Ссылка на файл"',
        ], $tableOptions);

        $this->addForeignKey('act_to_employee_author', 'act', 'author', 'employee', 'id');
        $this->addForeignKey('act_to_employee_author_status', 'act', 'status_author', 'employee', 'id');
        $this->addForeignKey('act_to_invoice', 'act', 'invoice_id', 'invoice', 'id');
        $this->addForeignKey('act_to_act_status', 'act', 'act_status_id', 'act_status', 'id');
    }

    public function down()
    {
        $this->dropTable('act');
        $this->dropTable('act_status');
    }

}
