<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200318_071356_alter_user_config_add_columns extends Migration
{
    public $table = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->table, 'table_view_finance_profit_loss', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table, 'table_view_finance_balance', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table, 'table_view_finance_expenses', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table,'report_profit_loss_help',$this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->table,'report_balance_help',$this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'table_view_finance_profit_loss');
        $this->dropColumn($this->table, 'table_view_finance_balance');
        $this->dropColumn($this->table, 'table_view_finance_expenses');
        $this->dropColumn($this->table,'report_profit_loss_help');
        $this->dropColumn($this->table,'report_balance_help');
    }
}
