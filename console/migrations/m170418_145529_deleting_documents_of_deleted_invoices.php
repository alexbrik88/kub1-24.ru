<?php

use common\models\document\Act;
use common\models\document\InvoiceFacture;
use common\models\document\OrderAct;
use common\models\document\OrderPackingList;
use common\models\document\PackingList;
use common\models\document\PaymentOrder;
use console\components\db\Migration;
use yii\db\Schema;
use yii\db\Query;

class m170418_145529_deleting_documents_of_deleted_invoices extends Migration
{
    public function safeUp()
    {
        $idArray = (new Query)->from('{{act}} {{doc}}')
            ->select(['doc.id'])
            ->leftJoin('invoice', '{{invoice}}.[[id]] = {{doc}}.[[invoice_id]]')
            ->andWhere(['invoice.is_deleted' => true])
            ->column();
        if (count($idArray) > 0) {
            OrderAct::deleteAll(['act_id' => $idArray]);
            Act::deleteAll(['id' => $idArray]);
        }

        $idArray = (new Query)->from('{{packing_list}} {{doc}}')
            ->select(['doc.id'])
            ->leftJoin('invoice', '{{invoice}}.[[id]] = {{doc}}.[[invoice_id]]')
            ->andWhere(['invoice.is_deleted' => true])
            ->column();
        if (count($idArray) > 0) {
            OrderPackingList::deleteAll(['packing_list_id' => $idArray]);
            PackingList::deleteAll(['id' => $idArray]);
        }

        $idArray = (new Query)->from('{{invoice_facture}} {{doc}}')
            ->select(['doc.id'])
            ->leftJoin('invoice', '{{invoice}}.[[id]] = {{doc}}.[[invoice_id]]')
            ->andWhere(['invoice.is_deleted' => true])
            ->column();
        if (count($idArray) > 0) {
            InvoiceFacture::deleteAll(['id' => $idArray]);
        }

        $idArray = (new Query)->from('{{payment_order}} {{doc}}')
            ->select(['doc.id'])
            ->leftJoin('invoice', '{{invoice}}.[[id]] = {{doc}}.[[invoice_id]]')
            ->andWhere(['invoice.is_deleted' => true])
            ->column();
        if (count($idArray) > 0) {
            PaymentOrder::deleteAll(['id' => $idArray]);
        }
    }

    public function safeDown()
    {

    }
}
