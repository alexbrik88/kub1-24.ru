<?php

use yii\db\Migration;

class m210326_051544_alter_import_job_data_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%import_job_data}}';

    /**
     * @inheritDoc
     */
    public function up()
    {
        $this->dropColumn(self::TABLE_NAME, 'date_from');
        $this->dropColumn(self::TABLE_NAME, 'date_to');
        $this->dropColumn(self::TABLE_NAME, 'identifier');
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        $this->addColumn(self::TABLE_NAME, 'date_from', $this->date()->null());
        $this->addColumn(self::TABLE_NAME, 'date_to', $this->date()->null());
        $this->addColumn(self::TABLE_NAME, 'identifier', $this->string()->notNull()->defaultValue(''));
    }
}
