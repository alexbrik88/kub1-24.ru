<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201211_120457_insert_invoice_items extends Migration
{
    const INCOME_ITEM_INPUT_MONEY = 22;
    const EXPENSE_ITEM_OUTPUT_MONEY = 89;
    const EXPENSE_ITEM_OUTPUT_PROFIT = 90;

    public function safeUp()
    {
        $this->execute("
            INSERT INTO `invoice_income_item` (`id`, `company_id`, `name`, `sort`, `is_visible`, `can_be_controlled`, `is_prepayment`)
            VALUES (22, null, 'Ввод денег', 100, 1, 0, 0);
        ");
        $this->execute("
            INSERT INTO `invoice_expenditure_item` (`id`, `group_id`, `company_id`, `name`, `sort`, `is_visible`, `can_be_controlled`, `is_prepayment`)
            VALUES (89, null, null, 'Вывод денег', 100, 1, 0, 0);
        ");
        $this->execute("
            INSERT INTO `invoice_expenditure_item` (`id`, `group_id`, `company_id`, `name`, `sort`, `is_visible`, `can_be_controlled`, `is_prepayment`)
            VALUES (90, null, null, 'Вывод прибыли', 100, 1, 0, 0);
        ");

        ///

        $this->execute("
            INSERT INTO `income_item_flow_of_funds` (`company_id`, `income_item_id`)
            SELECT `id`, 22 FROM `company`;
        ");
        $this->execute("
            INSERT INTO `expense_item_flow_of_funds` (`company_id`, `expense_item_id`)
            SELECT `id`, 89 FROM `company`;
        ");
        $this->execute("
            INSERT INTO `expense_item_flow_of_funds` (`company_id`, `expense_item_id`)
            SELECT `id`, 90 FROM `company`;
        ");
    }

    public function safeDown()
    {
        $this->execute("DELETE FROM `income_item_flow_of_funds` WHERE `income_item_id` = 22");
        $this->execute("DELETE FROM `expense_item_flow_of_funds` WHERE `expense_item_id` = 89");
        $this->execute("DELETE FROM `expense_item_flow_of_funds` WHERE `expense_item_id` = 90");

        ///

        $this->execute("DELETE FROM `invoice_income_item` WHERE `id` = 22");
        $this->execute("DELETE FROM `invoice_expenditure_item` WHERE `id` = 89");
        $this->execute("DELETE FROM `invoice_expenditure_item` WHERE `id` = 90");
    }
}
