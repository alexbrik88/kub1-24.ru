<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211012_084254_alter_marketing_report_goal extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%marketing_report_google_analytics_goal}}';

    public function safeUp()
    {
        $this->dropIndex('uk_marketing_report_google_analytics_goal', self::TABLE_NAME);
        $this->createIndex(
            'uk_marketing_report_google_analytics_goal',
            self::TABLE_NAME,
            ['company_id', 'source_type', 'utm_campaign', 'goal_id', 'date', 'utm_source', 'utm_medium'],
            true
        );
    }

    public function safeDown()
    {
        $this->dropIndex('uk_marketing_report_google_analytics_goal', self::TABLE_NAME);
        $this->createIndex(
            'uk_marketing_report_google_analytics_goal',
            self::TABLE_NAME,
            ['company_id', 'source_type', 'date', 'utm_source', 'utm_medium', 'utm_campaign', 'goal_id'],
            true
        );
    }
}
