<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%rent_agreement_invoice}}`.
 */
class m211125_093558_create_rent_agreement_invoice_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%rent_agreement_invoice}}', [
            'invoice_id' => $this->integer()->notNull(),
            'rent_agreement_id' => $this->integer()->notNull(),
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', '{{%rent_agreement_invoice}}', 'invoice_id');
        $this->createIndex('rent_agreement_id', '{{%rent_agreement_invoice}}', 'rent_agreement_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%rent_agreement_invoice}}');
    }
}
