<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211008_125521_alter_user_config_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%user_config}}', 'crm_task_description', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn('{{%user_config}}', 'crm_task_result', $this->boolean()->notNull()->defaultValue(true));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%user_config}}', 'crm_task_description');
        $this->dropColumn('{{%user_config}}', 'crm_task_result');
    }
}
