<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190603_122808_add_is_repeated_to_plan_cash_flows extends Migration
{
    public $tableName = 'plan_cash_flows';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'is_repeated', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'first_flow_id', $this->integer()->defaultValue(null));

        $this->addForeignKey($this->tableName . '_first_flow_id', $this->tableName, 'first_flow_id', $this->tableName, 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_first_flow_id', $this->tableName);

        $this->dropColumn($this->tableName, 'first_flow_id');
        $this->dropColumn($this->tableName, 'is_repeated');
    }
}
