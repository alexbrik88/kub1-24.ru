<?php

use console\components\db\Migration;

class m200110_104318_amocrm_user_company_id_fk_create extends Migration
{
    const TABLE = '{{%amocrm_user}}';

    public function up()
    {
        $this->addForeignKey(
            'amocrm_user_company_id', self::TABLE, 'company_id',
            '{{%company}}', 'id',
            'CASCADE', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey('amocrm_user_company_id', self::TABLE);
    }
}
