<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200716_141040_alter_user_config_drop_project_fields extends Migration
{
    public $tableName = 'user_config';

    public function safeUp()
    {
        $this->dropColumn($this->tableName, 'project_clients');
        $this->dropColumn($this->tableName, 'project_end_date');
        $this->dropColumn($this->tableName, 'project_contractor');
        $this->dropColumn($this->tableName, 'project_expense');
        $this->dropColumn($this->tableName, 'project_result');
        $this->dropColumn($this->tableName, 'project_responsible_employee_id');
    }

    public function safeDown()
    {
        $this->addColumn($this->tableName, 'project_clients', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn($this->tableName, 'project_end_date', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn($this->tableName, 'project_contractor', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn($this->tableName, 'project_expense', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn($this->tableName, 'project_result', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn($this->tableName, 'project_responsible_employee_id', $this->boolean()->notNull()->defaultValue(true));

        $this->addCommentOnColumn('user_config', 'project_clients', 'Показ колонки "Клиенты" в списке проектов');
        $this->addCommentOnColumn('user_config', 'project_end_date', 'Показ колонки "Окончание" в списке проектов');
        $this->addCommentOnColumn('user_config', 'project_contractor', 'Показ колонки "Поставщик" в списке проектов');
        $this->addCommentOnColumn('user_config', 'project_expense', 'Показ колонки "Расход" в списке проектов');
        $this->addCommentOnColumn('user_config', 'project_result', 'Показ колонки "Результат" в списке проектов');
        $this->addCommentOnColumn('user_config', 'project_responsible_employee_id', 'Показ колонки "Ответственный" в списке проектов');
    }

}
