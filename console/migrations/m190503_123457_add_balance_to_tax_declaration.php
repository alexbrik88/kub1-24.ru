<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190503_123457_add_balance_to_tax_declaration extends Migration
{
    public $tableName = 'tax_declaration';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'balance', $this->boolean()->defaultValue(false)->after('org'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'balance');
    }
}
