<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210824_121810_create_table_payments_request_history extends Migration
{
    public $table = 'payments_request_history';

    public function safeUp()
    {
        $table = $this->table;
        $this->createTable($table, [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull(),
            'author_id' => $this->integer()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'request_id' => $this->integer()->notNull(),
            'event_id' => $this->tinyInteger(),
            'message' => $this->string(2048)
        ]);

        $this->addForeignKey('FK_' . $table . '_author', $table, 'author_id', 'employee', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_' . $table . '_company', $table, 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_' . $table . '_request', $table, 'request_id', 'payments_request', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $table = $this->table;
        $this->dropForeignKey('FK_' . $table . '_author', $table);
        $this->dropForeignKey('FK_' . $table . '_company', $table);
        $this->dropForeignKey('FK_' . $table . '_request', $table);
        $this->dropTable($table);
    }
}
