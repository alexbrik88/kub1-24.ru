<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191225_102757_insert_service_subscribe_tariff extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%service_subscribe_tariff}}', [
            'id' => 28,
            'tariff_group_id' => 4,
            'duration_month' => 12,
            'duration_day' => 0,
            'price' => 2000,
            'is_active' => 1,
            'proposition' => 'Доплата до тарифа с декларацией',
            'is_pay_extra' => 1,
            'pay_extra_for' => 27,
        ]);
    }

    public function safeDown()
    {

    }
}
