<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201211_162256_update_cash_flows extends Migration
{
    const BANK_TEXT = 'bank';
    const ORDER_TEXT = 'order';
    const EMONEY_TEXT = 'emoney';

    public function safeUp()
    {
        $innerContractors = [
            'bank',
            'order',
            'emoney',
        ];

        $this->update('{{cash_bank_flows}}', ['is_internal_transfer' => 1], ['contractor_id' => $innerContractors]);
        $this->update('{{cash_order_flows}}', ['is_internal_transfer' => 1], ['contractor_id' => $innerContractors]);
        $this->update('{{cash_emoney_flows}}', ['is_internal_transfer' => 1], ['contractor_id' => $innerContractors]);

        $this->execute('
            UPDATE {{cash_bank_flows}}, {{cash_order_flows}}
            SET {{cash_bank_flows}}.[[internal_transfer_account_table]] = "cashbox",
                {{cash_bank_flows}}.[[internal_transfer_account_id]] = {{cash_order_flows}}.[[cashbox_id]],
                {{cash_bank_flows}}.[[internal_transfer_flow_table]] = "cash_order_flows",
                {{cash_bank_flows}}.[[internal_transfer_flow_id]] = {{cash_order_flows}}.[[id]],
                {{cash_order_flows}}.[[internal_transfer_account_table]] = "checking_accountant",
                {{cash_order_flows}}.[[internal_transfer_account_id]] = {{cash_order_flows}}.[[other_rs_id]],
                {{cash_order_flows}}.[[internal_transfer_flow_table]] = "cash_bank_flows",
                {{cash_order_flows}}.[[internal_transfer_flow_id]] = {{cash_bank_flows}}.[[id]]
            WHERE (
                (
                    {{cash_bank_flows}}.[[id]] = {{cash_order_flows}}.[[cash_id]]
                    AND
                    ({{cash_bank_flows}}.[[cash_id]] = {{cash_order_flows}}.[[id]] OR {{cash_bank_flows}}.[[cash_id]] IS NULL)
                )
                OR
                (
                    {{cash_order_flows}}.[[id]] = {{cash_bank_flows}}.[[cash_id]]
                    AND
                    ({{cash_order_flows}}.[[cash_id]] = {{cash_bank_flows}}.[[id]] OR {{cash_order_flows}}.[[cash_id]] IS NULL)
                )
            )
            AND {{cash_bank_flows}}.[[contractor_id]] = "order"
            AND {{cash_order_flows}}.[[contractor_id]] = "bank"
            AND {{cash_bank_flows}}.[[company_id]] = {{cash_order_flows}}.[[company_id]]
        ');

        $this->execute('
            UPDATE {{cash_bank_flows}}, {{cash_emoney_flows}}
            SET {{cash_bank_flows}}.[[internal_transfer_account_table]] = "emoney",
                {{cash_bank_flows}}.[[internal_transfer_account_id]] = {{cash_emoney_flows}}.[[emoney_id]],
                {{cash_bank_flows}}.[[internal_transfer_flow_table]] = "cash_emoney_flows",
                {{cash_bank_flows}}.[[internal_transfer_flow_id]] = {{cash_emoney_flows}}.[[id]],
                {{cash_emoney_flows}}.[[internal_transfer_account_table]] = "checking_accountant",
                {{cash_emoney_flows}}.[[internal_transfer_account_id]] = {{cash_emoney_flows}}.[[other_rs_id]],
                {{cash_emoney_flows}}.[[internal_transfer_flow_table]] = "cash_bank_flows",
                {{cash_emoney_flows}}.[[internal_transfer_flow_id]] = {{cash_bank_flows}}.[[id]]
            WHERE (
                (
                    {{cash_bank_flows}}.[[id]] = {{cash_emoney_flows}}.[[cash_id]]
                    AND
                    ({{cash_bank_flows}}.[[cash_id]] = {{cash_emoney_flows}}.[[id]] OR {{cash_bank_flows}}.[[cash_id]] IS NULL)
                )
                OR
                (
                    {{cash_emoney_flows}}.[[id]] = {{cash_bank_flows}}.[[cash_id]]
                    AND
                    ({{cash_emoney_flows}}.[[cash_id]] = {{cash_bank_flows}}.[[id]] OR {{cash_emoney_flows}}.[[cash_id]] IS NULL)
                )
            )
            AND {{cash_bank_flows}}.[[contractor_id]] = "emoney"
            AND {{cash_emoney_flows}}.[[contractor_id]] = "bank"
            AND {{cash_bank_flows}}.[[company_id]] = {{cash_emoney_flows}}.[[company_id]]
        ');

        $this->execute('
            UPDATE {{cash_order_flows}}, {{cash_emoney_flows}}
            SET {{cash_order_flows}}.[[internal_transfer_account_table]] = "emoney",
                {{cash_order_flows}}.[[internal_transfer_account_id]] = {{cash_emoney_flows}}.[[emoney_id]],
                {{cash_order_flows}}.[[internal_transfer_flow_table]] = "cash_emoney_flows",
                {{cash_order_flows}}.[[internal_transfer_flow_id]] = {{cash_emoney_flows}}.[[id]],
                {{cash_emoney_flows}}.[[internal_transfer_account_table]] = "cashbox",
                {{cash_emoney_flows}}.[[internal_transfer_account_id]] = {{cash_order_flows}}.[[cashbox_id]],
                {{cash_emoney_flows}}.[[internal_transfer_flow_table]] = "cash_order_flows",
                {{cash_emoney_flows}}.[[internal_transfer_flow_id]] = {{cash_order_flows}}.[[id]]
            WHERE (
                (
                    {{cash_order_flows}}.[[id]] = {{cash_emoney_flows}}.[[cash_id]]
                    AND
                    ({{cash_order_flows}}.[[cash_id]] = {{cash_emoney_flows}}.[[id]] OR {{cash_order_flows}}.[[cash_id]] IS NULL)
                )
                OR
                (
                    {{cash_emoney_flows}}.[[id]] = {{cash_order_flows}}.[[cash_id]]
                    AND
                    ({{cash_emoney_flows}}.[[cash_id]] = {{cash_order_flows}}.[[id]] OR {{cash_emoney_flows}}.[[cash_id]] IS NULL)
                )
            )
            AND {{cash_order_flows}}.[[contractor_id]] = "emoney"
            AND {{cash_emoney_flows}}.[[contractor_id]] = "order"
            AND {{cash_emoney_flows}}.[[company_id]] = {{cash_order_flows}}.[[company_id]]
        ');

        $this->execute('
            UPDATE {{cash_order_flows}}
            INNER JOIN {{cash_order_flows}} AS {{flow}} ON (
                (
                    {{cash_order_flows}}.[[id]] = {{flow}}.[[cash_id]]
                    AND
                    ({{cash_order_flows}}.[[cash_id]] = {{flow}}.[[id]] OR {{cash_order_flows}}.[[cash_id]] IS NULL)
                )
                OR
                (
                    {{flow}}.[[id]] = {{cash_order_flows}}.[[cash_id]]
                    AND
                    ({{flow}}.[[cash_id]] = {{cash_order_flows}}.[[id]] OR {{flow}}.[[cash_id]] IS NULL)
                )
            )
            SET {{cash_order_flows}}.[[internal_transfer_account_table]] = "cashbox",
                {{cash_order_flows}}.[[internal_transfer_account_id]] = {{flow}}.[[cashbox_id]],
                {{cash_order_flows}}.[[internal_transfer_flow_table]] = "cash_order_flows",
                {{cash_order_flows}}.[[internal_transfer_flow_id]] = {{flow}}.[[id]]
            WHERE {{cash_order_flows}}.[[contractor_id]] = "order"
            AND {{flow}}.[[contractor_id]] = "order"
            AND {{cash_order_flows}}.[[company_id]] = {{flow}}.[[company_id]]
        ');

        $this->execute('
            UPDATE {{cash_emoney_flows}}
            INNER JOIN {{cash_emoney_flows}} AS {{flow}} ON (
                (
                    {{cash_emoney_flows}}.[[id]] = {{flow}}.[[cash_id]]
                    AND
                    ({{cash_emoney_flows}}.[[cash_id]] = {{flow}}.[[id]] OR {{cash_emoney_flows}}.[[cash_id]] IS NULL)
                )
                OR
                (
                    {{flow}}.[[id]] = {{cash_emoney_flows}}.[[cash_id]]
                    AND
                    ({{flow}}.[[cash_id]] = {{cash_emoney_flows}}.[[id]] OR {{flow}}.[[cash_id]] IS NULL)
                )
            )
            SET {{cash_emoney_flows}}.[[internal_transfer_account_table]] = "emoney",
                {{cash_emoney_flows}}.[[internal_transfer_account_id]] = {{flow}}.[[emoney_id]],
                {{cash_emoney_flows}}.[[internal_transfer_flow_table]] = "cash_emoney_flows",
                {{cash_emoney_flows}}.[[internal_transfer_flow_id]] = {{flow}}.[[id]]
            WHERE {{cash_emoney_flows}}.[[contractor_id]] = "emoney"
            AND {{flow}}.[[contractor_id]] = "emoney"
            AND {{cash_emoney_flows}}.[[company_id]] = {{flow}}.[[company_id]]
        ');
    }

    public function safeDown()
    {
        $this->update('{{cash_bank_flows}}', [
            'is_internal_transfer' => 0,
            'internal_transfer_account_table' => null,
            'internal_transfer_account_id' => null,
            'internal_transfer_flow_table' => null,
            'internal_transfer_flow_id' => null,
        ], ['is_internal_transfer' => 1]);

        $this->update('{{cash_order_flows}}', [
            'is_internal_transfer' => 0,
            'internal_transfer_account_table' => null,
            'internal_transfer_account_id' => null,
            'internal_transfer_flow_table' => null,
            'internal_transfer_flow_id' => null,
        ], ['is_internal_transfer' => 1]);

        $this->update('{{cash_emoney_flows}}', [
            'is_internal_transfer' => 0,
            'internal_transfer_account_table' => null,
            'internal_transfer_account_id' => null,
            'internal_transfer_flow_table' => null,
            'internal_transfer_flow_id' => null,
        ], ['is_internal_transfer' => 1]);
    }
}
