<?php

use common\models\Company;
use console\components\db\Migration;
use yii\db\Schema;
use yii\helpers\FileHelper;

class m170915_120256_create_companyImageUploadLog extends Migration
{
    public $tableName = 'company_image_upload_log';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'type' => $this->string()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'created_by' => $this->integer(),
        ]);

        $this->addForeignKey('FK_' . $this->tableName . '_company', $this->tableName, 'company_id', '{{%company}}', 'id', 'CASCADE');
        $this->addForeignKey('FK_' . $this->tableName . '_employee', $this->tableName, 'created_by', '{{%employee}}', 'id', 'SET NULL');

        $fileList = FileHelper::findFiles(\Yii::getAlias('@common/uploads/company'), [
            'except' => ['tmp/', '.gitignore', '*-resize.*'],
        ]);
        $rowArray = [];
        foreach ($fileList as $file) {
            $time = filemtime($file);
            preg_match('~company\/(\d+)\/([a-zA-Z]+)\.~', $file, $matches);
            if ($time && isset($matches[1], $matches[2])) {
                if (Company::find()->where(['id' => $matches[1]])->exists()) {
                    $rowArray[] = [
                        'company_id' => $matches[1],
                        'type' => $matches[2],
                        'created_at' => $time,
                    ];
                }
            }
            unset($matches);
        }

        if ($rowArray) {
            $this->batchInsert($this->tableName, ['company_id', 'type', 'created_at'], $rowArray);
        }
    }
    
    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
