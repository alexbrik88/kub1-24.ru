<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%mts_user}}`.
 */
class m190716_061136_create_mts_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%mts_user}}', [
            'id' => $this->string()->notNull(),
            'employee_id' => $this->integer(),
            'firstname' => $this->string(),
            'lastname' => $this->string(),
            'email' => $this->string(),
            'phone' => $this->string(),
            'groups' => $this->string(),
            'created_at' => $this->integer(),
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', '{{%mts_user}}', 'id');

        $this->addForeignKey(
            'FK_mts_user_employee',
            '{{%mts_user}}',
            'employee_id',
            '{{%employee}}',
            'id'
        );

        $this->createIndex(
            'employee_id',
            '{{%mts_user}}',
            'employee_id',
            true
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%mts_user}}');
    }
}
