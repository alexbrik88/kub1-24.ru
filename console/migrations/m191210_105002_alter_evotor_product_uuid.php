<?php

use console\components\db\Migration;
use common\models\evotor\Product;
use common\models\evotor\ReceiptItem;

class m191210_105002_alter_evotor_product_uuid extends Migration
{
    public function up()
    {
        ReceiptItem::deleteAll();
        Product::deleteAll();
        $this->addColumn('{{evotor_product}}', 'uuid', $this->char(36)->notNull()->unique()->comment('UUID товара'));
        $this->dropColumn('{{evotor_product}}', 'id');
        $this->addColumn('{{evotor_product}}', 'id', $this->primaryKey(11)->unsigned()->notNull()->first()->comment('ID'));
        $this->alterColumn('{{evotor_product}}', 'parent_id', $this->integer(11)->unsigned()->comment('ID родительского товара (группы товаров)'));
        $this->alterColumn('{{evotor_receipt_item}}', 'product_id', $this->integer(11)->unsigned()->comment('ID товара (evotor_product.id)'));
        $this->addForeignKey('evotor_receipt_item_product_id', '{{evotor_receipt_item}}', 'product_id',
            '{{evotor_product}}', 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown()
    {
        ReceiptItem::deleteAll();
        Product::deleteAll();
        $this->dropForeignKey('evotor_receipt_item_product_id', '{{evotor_receipt_item}}');
        $this->dropColumn('{{evotor_product}}', 'uuid');
        $this->alterColumn('{{evotor_product}}', 'parent_id', "BINARY(16) NOT NULL COMMENT 'UUID товара'");
        $this->dropColumn('{{evotor_product}}', 'id');
        $this->addColumn('{{evotor_product}}', 'id', "BINARY(16) NOT NULL PRIMARY KEY COMMENT 'UUID Эвотора' FIRST");
        $this->alterColumn('{{evotor_receipt_item}}', 'product_id', "BINARY(16) NOT NULL COMMENT 'UUID товара'");
    }

}
