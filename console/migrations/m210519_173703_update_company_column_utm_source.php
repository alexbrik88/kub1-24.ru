<?php

use common\models\Company;
use console\components\db\Migration;
use yii\db\Schema;

class m210519_173703_update_company_column_utm_source extends Migration
{
    public function safeUp()
    {
        $cnt = 0;
        foreach (Company::find()->select(['id'])->andWhere(['not', ['form_utm' => null]])->andWhere(['utm_source' => null])->column() as $id) {
            if ($company = Company::findOne($id)) {
                parse_str($company->form_utm, $params);
                if (isset($params['utm_source'])) {
                    $utm_source = str_replace('"', '', $params['utm_source']);
                    $this->execute("UPDATE `company` SET `utm_source` = \"{$utm_source}\" WHERE `id` = {$id}");
                    $cnt++;
                }
            }
        }

        echo "updated {$cnt} rows\n\n";
    }

    public function safeDown()
    {
        // no down
    }
}
