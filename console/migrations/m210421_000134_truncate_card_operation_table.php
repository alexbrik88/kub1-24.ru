<?php

use yii\db\Migration;

class m210421_000134_truncate_card_operation_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%card_operation}}';

    /**
     * @inheritDoc
     */
    public function safeUp(): void
    {
        $this->truncateTable(self::TABLE_NAME);
    }
}
