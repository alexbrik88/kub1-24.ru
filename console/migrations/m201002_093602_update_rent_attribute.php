<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201002_093602_update_rent_attribute extends Migration
{
    public function safeUp()
    {
        $this->update('rent_attribute', ['default' => 'руб', 'data' => "['руб']"], ['default' => 'RUB', 'alias' => 'currency']);
        $this->update('rent_attribute', ['data' => '["день", "месяц", "год"]'], ['alias' => 'paymentPeriod']);

        $this->update('rent_attribute_value', ['value' => 'день'], ['value' => 'День']);
        $this->update('rent_attribute_value', ['value' => 'месяц'], ['value' => 'Месяц']);
        $this->update('rent_attribute_value', ['value' => 'год'], ['value' => 'Год']);
        $this->update('rent_attribute_value', ['value' => 'руб'], ['value' => 'RUB']);
    }

    public function safeDown()
    {
        $this->update('rent_attribute', ['default' => 'RUB', 'data' => "['RUB']"], ['default' => 'руб', 'alias' => 'currency']);
        $this->update('rent_attribute', ['data' => '["День", "Месяц", "Год"]'], ['alias' => 'paymentPeriod']);

        $this->update('rent_attribute_value', ['value' => 'День'], ['value' => 'день']);
        $this->update('rent_attribute_value', ['value' => 'Месяц'], ['value' => 'месяц']);
        $this->update('rent_attribute_value', ['value' => 'Год'], ['value' => 'год']);
        $this->update('rent_attribute_value', ['value' => 'RUB'], ['value' => 'руб']);
    }
}
