<?php

use console\components\db\Migration;

class m200109_190923_evotor_product_company_id_create_fk extends Migration
{
    const TABLE = '{{%evotor_product}}';

    public function up()
    {
        $this->addForeignKey(
            'evotor_product_company_id', self::TABLE, 'company_id',
            '{{%company}}', 'id',
            'CASCADE', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey('evotor_product_company_id', self::TABLE);
    }
}
