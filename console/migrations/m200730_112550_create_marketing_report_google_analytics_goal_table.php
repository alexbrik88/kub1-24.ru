<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%marketing_report_google_analytics_goal}}`.
 */
class m200730_112550_create_marketing_report_google_analytics_goal_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%marketing_report_google_analytics_goal}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer(),
            'source_type' => $this->integer(),
            'date' => $this->date(),
            'campaign_name' => $this->string(),
            'utm_source' => $this->string(),
            'utm_medium' => $this->string(),
            'utm_campaign' => $this->string(),
            'goal_id' => $this->integer(),
            'total' => $this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%marketing_report_google_analytics_goal}}');
    }
}
