<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210701_183659_update_marketing_plan_template extends Migration
{
    public function safeUp()
    {
        $this->update('marketing_plan', ['template' => 0]);
    }

    public function safeDown()
    {

    }
}
