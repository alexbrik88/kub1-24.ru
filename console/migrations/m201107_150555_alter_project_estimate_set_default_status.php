<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201107_150555_alter_project_estimate_set_default_status extends Migration
{
    public $tableName = 'project_estimate';

    public function safeUp()
    {
        $this->alterColumn($this->tableName, 'status', $this->integer(1)->defaultValue(1));
    }

    public function safeDown()
    {
        $this->alterColumn($this->tableName, 'status', $this->integer(1));
    }
}
