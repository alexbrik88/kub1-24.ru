<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201104_172239_alter_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_config','report_pc_priority',$this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('user_config','report_pc_priority');
    }
}
