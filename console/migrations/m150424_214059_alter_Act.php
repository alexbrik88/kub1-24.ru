<?php

use yii\db\Schema;
use yii\db\Migration;

class m150424_214059_alter_Act extends Migration
{
    public function up()
    {
        $this->dropTable('act');
        $this->createTable('act', [
            'id' => Schema::TYPE_PK,
            'type' => Schema::TYPE_INTEGER . '(1) NOT NULL COMMENT "2 - out, 1 -in"',
            'created_at' => Schema::TYPE_DATETIME . ' NOT NULL',
            'document_author_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'invoice_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'status_out_id' => Schema::TYPE_INTEGER . ' NULL',
            'status_out_updated_at' => Schema::TYPE_DATETIME . ' NULL',
            'status_out_author_id' => Schema::TYPE_INTEGER . ' NULL',
            'document_date' => Schema::TYPE_DATE . ' NOT NULL',
            'document_number' => Schema::TYPE_STRING . '(45) NULL',
            'document_additional_number' => Schema::TYPE_STRING . '(45) NULL',
            'filename' => Schema::TYPE_STRING . '(45) NULL',
        ]);

        $this->addForeignKey('act_to_document_author', 'act', 'document_author_id', 'employee', 'id');
        $this->addForeignKey('act_to_employee_author_status', 'act', 'status_out_author_id', 'employee', 'id');
        $this->addForeignKey('act_to_invoice', 'act', 'invoice_id', 'invoice', 'id');
        $this->addForeignKey('act_to_act_status', 'act', 'status_out_id', 'act_status', 'id');
    }

    public function down()
    {
        $this->dropTable('act');
        $this->createTable('act', [
            'id' => Schema::TYPE_PK,
            'author_id' => Schema::TYPE_INTEGER . ' NULL COMMENT "Дата создания"',
            'invoice_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Счёт"',
            'act_status_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'status_date' => Schema::TYPE_DATE . ' NULL COMMENT "Дата вознинкновения статуса"',
            'status_author_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Автор текущего статуса"',
            'act_date' => Schema::TYPE_DATE . ' NULL COMMENT "Дата обновления статуса"',
            'additional_number' => Schema::TYPE_STRING . ' NULL COMMENT "Дополнительный номер акта"',
            'file' => Schema::TYPE_STRING . '(255) NULL COMMENT "Ссылка на файл"',
            'type' => Schema::TYPE_INTEGER . '(1) NOT NULL COMMENT "Тип акта - 2 (исходящий), 1 (входящий)"',
        ]);

        $this->addForeignKey('act_to_employee_author', 'act', 'author_id', 'employee', 'id');
        $this->addForeignKey('act_to_employee_author_status', 'act', 'status_author_id', 'employee', 'id');
        $this->addForeignKey('act_to_invoice', 'act', 'invoice_id', 'invoice', 'id');
        $this->addForeignKey('act_to_act_status', 'act', 'act_status_id', 'act_status', 'id');
    }
}