<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201222_141527_update_contractor extends Migration
{
    public function safeUp()
    {
        $this->execute('
            UPDATE {{contractor}}
            SET [[is_seller]] = IF([[type]]=1, 1, 0),
                [[is_customer]] = IF([[type]]=2, 1, 0),
                [[is_founder]] = IF([[type]]=3, 1, 0)
        ');
    }

    public function safeDown()
    {
        $this->execute('
            UPDATE {{contractor}}
            SET [[is_seller]] = 0,
                [[is_customer]] = 0,
                [[is_founder]] = 0
        ');
    }
}
