<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190417_075243_alter_payment_order_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%payment_order}}', 'cash_bank_flows_id', $this->integer()->defaultValue(null));
        $this->addColumn('{{%payment_order}}', 'created_by_taxrobot', $this->boolean()->defaultValue(false));
        $this->addColumn('{{%payment_order}}', 'sum_changed_by_user', $this->boolean()->defaultValue(false));

        $this->createIndex('cash_bank_flows_id', '{{%payment_order}}', 'cash_bank_flows_id', true);

        $this->addForeignKey(
            'fk_payment_order_cash_bank_flows',
            '{{%payment_order}}',
            'cash_bank_flows_id',
            '{{%cash_bank_flows}}',
            'id',
            'SET NULL'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_payment_order_cash_bank_flows', '{{%payment_order}}');

        $this->dropIndex('cash_bank_flows_id', '{{%payment_order}}');

        $this->dropColumn('{{%payment_order}}', 'sum_changed_by_user');
        $this->dropColumn('{{%payment_order}}', 'created_by_taxrobot');
        $this->dropColumn('{{%payment_order}}', 'cash_bank_flows_id');
    }
}
