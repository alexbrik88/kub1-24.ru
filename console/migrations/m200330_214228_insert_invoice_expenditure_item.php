<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200330_214228_insert_invoice_expenditure_item extends Migration
{
    public function safeUp()
    {
        $this->update('{{%invoice_expenditure_item}}', ['is_visible' => 0], [
            'company_id' => null,
            'can_be_controlled' => 1,
        ]);
        $this->update('{{%invoice_income_item}}', ['is_visible' => 0], [
            'company_id' => null,
            'can_be_controlled' => 1,
        ]);
        $this->execute('
            UPDATE expense_item_flow_of_funds
             INNER JOIN invoice_expenditure_item 
                     ON invoice_expenditure_item.id = expense_item_flow_of_funds.expense_item_id
                    AND invoice_expenditure_item.company_id IS NULL
                    AND invoice_expenditure_item.can_be_controlled = 1
            SET expense_item_flow_of_funds.is_visible = invoice_expenditure_item.is_visible
        ');
        $this->execute('
            UPDATE income_item_flow_of_funds
             INNER JOIN invoice_income_item 
                     ON invoice_income_item.id = income_item_flow_of_funds.income_item_id
                    AND invoice_income_item.company_id IS NULL
                    AND invoice_income_item.can_be_controlled = 1
            SET income_item_flow_of_funds.is_visible = invoice_income_item.is_visible
        ');
    }

    public function safeDown()
    {
        $this->update('{{%invoice_expenditure_item}}', ['is_visible' => 1], [
            'company_id' => null,
            'can_be_controlled' => 1,
        ]);
        $this->update('{{%invoice_income_item}}', ['is_visible' => 1], [
            'company_id' => null,
            'can_be_controlled' => 1,
        ]);
        $this->execute('
            UPDATE expense_item_flow_of_funds
             INNER JOIN invoice_expenditure_item
                     ON invoice_expenditure_item.id = expense_item_flow_of_funds.expense_item_id
                    AND invoice_expenditure_item.company_id IS NULL
                    AND invoice_expenditure_item.can_be_controlled = 1
            SET expense_item_flow_of_funds.is_visible = invoice_expenditure_item.is_visible
        ');
        $this->execute('
            UPDATE income_item_flow_of_funds
             INNER JOIN invoice_income_item
                     ON invoice_income_item.id = income_item_flow_of_funds.income_item_id
                    AND invoice_income_item.company_id IS NULL
                    AND invoice_income_item.can_be_controlled = 1
            SET income_item_flow_of_funds.is_visible = invoice_expenditure_item.is_visible
        ');
    }
}
