<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200616_135834_alter_ofd_receipt_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%ofd_receipt}}', 'store_id', $this->integer()->after('ofd_id'));
        $this->addColumn('{{%ofd_receipt}}', 'operator_id', $this->integer()->after('operator_inn'));
        $this->addColumn('{{%ofd_receipt}}', 'payment_type_id', $this->integer(1)->after('cashless_sum'));
        $this->alterColumn('{{%ofd_receipt}}', 'operation_type_id', $this->integer(1)->notNull());
    }

    public function safeDown()
    {
        $this->alterColumn('{{%ofd_receipt}}', 'operation_type_id', $this->integer()->notNull());
        $this->dropColumn('{{%ofd_receipt}}', 'payment_type_id');
        $this->dropColumn('{{%ofd_receipt}}', 'operator_id');
        $this->dropColumn('{{%ofd_receipt}}', 'store_id');
    }
}
