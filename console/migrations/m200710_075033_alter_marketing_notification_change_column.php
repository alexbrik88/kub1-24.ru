<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200710_075033_alter_marketing_notification_change_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('marketing_notification', 'threshold', $this->float(2)->defaultValue(null));
    }

    public function safeDown()
    {
        $this->alterColumn('marketing_notification', 'threshold', $this->integer()->notNull()->defaultValue(0));
    }
}
