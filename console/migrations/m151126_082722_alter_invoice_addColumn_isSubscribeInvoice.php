<?php

use yii\db\Schema;
use yii\db\Migration;

class m151126_082722_alter_invoice_addColumn_isSubscribeInvoice extends Migration
{
    public $tInvoice = 'invoice';

    public function safeUp()
    {
        $this->addColumn($this->tInvoice, 'is_subscribe_invoice', $this->boolean()->notNull()->defaultValue(0));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tInvoice, 'is_subscribe_invoice');
    }
}
