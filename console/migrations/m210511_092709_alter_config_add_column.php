<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210511_092709_alter_config_add_column extends Migration
{
    public $table = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->table, 'cash_index_hide_plan', $this->tinyInteger()->notNull()->defaultValue(1));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'cash_index_hide_plan');
    }
}
