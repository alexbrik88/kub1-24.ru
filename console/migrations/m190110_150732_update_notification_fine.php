<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190110_150732_update_notification_fine extends Migration
{
    public $tableName = 'notification';

    public function safeUp()
    {
        $this->alterColumn($this->tableName, 'fine', $this->text()->defaultValue(null));
    }

    public function safeDown()
    {
        $this->alterColumn($this->tableName, 'fine', $this->string()->defaultValue(null));
    }
}
