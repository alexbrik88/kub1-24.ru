<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210117_180651_alter_config_add_column extends Migration
{
    public $tableName = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'project_number', $this->boolean()->notNull()->defaultValue(true)->after('report_debtor_chart'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'project_number');
    }
}
