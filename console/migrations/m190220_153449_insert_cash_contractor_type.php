<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190220_153449_insert_cash_contractor_type extends Migration
{
    public $tableName = 'cash_contractor_type';

    public function safeUp()
    {
        $this->insert($this->tableName, [
            'id' => 6,
            'name' => 'customers',
            'text' => 'Физ.лица',
        ]);
    }

    public function safeDown()
    {
        $this->delete($this->tableName, ['id' => 6]);
    }
}
