<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180910_122125_alter_user_fio_in_user_email extends Migration
{
    public $tableName = 'user_email';

    public function safeUp()
    {
        $this->alterColumn($this->tableName, 'fio', $this->string()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->alterColumn($this->tableName, 'fio', $this->string()->notNull());
    }
}


