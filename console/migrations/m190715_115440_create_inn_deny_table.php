<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%inn_deny}}`.
 */
class m190715_115440_create_inn_deny_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%inn_deny}}', [
            'id' => $this->primaryKey(),
            'inn' => $this->string(),
            'created_at' => $this->integer(),
        ]);

        $this->createIndex('inn', '{{%inn_deny}}', 'inn');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%inn_deny}}');
    }
}
