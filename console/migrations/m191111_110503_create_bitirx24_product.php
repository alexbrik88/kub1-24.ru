<?php

use console\components\db\Migration;

class m191111_110503_create_bitirx24_product extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{bitrix24_product}}', [
            'id' => $this->integer()->unsigned()->notNull()->comment('Идентификатор товара Битрикс24'),
            'employee_id' => $this->integer()->notNull()->comment('ID клиента КУБ'),
            'section_id' => $this->integer()->unsigned()->comment('Идентификатор товарной группы'),
            'vat_id' => $this->integer()->unsigned()->comment('Идентификатор налоговой группы'),
            'status' => $this->boolean()->notNull()->defaultValue(true),
            'name' => $this->string(250)->notNull()->comment('Название товара'),
            'vat_included' => $this->boolean()->notNull()->defaultValue(true)->comment('Включён ли налог в цену'),
            'picture' => $this->string(1000)->comment('URL изображения товара'),
            'sort' => $this->smallInteger()->unsigned()->notNull()->defaultValue(0),
            'created_at' => $this->integer()->unsigned()->notNull()->comment('Дата создания'),
            'price' => $this->float()->unsigned(),
            'description' => $this->text(),
            'currency' => $this->char(3)->notNull()->defaultValue('RUB'),
        ], "COMMENT 'Интеграция Битрикс24: товары'");
        $this->addPrimaryKey('bitrix24_product_id', '{{bitrix24_product}}', ['employee_id', 'id']);
        $this->addForeignKey(
            'bitrix24_product_section_id', '{{bitrix24_product}}', ['employee_id', 'section_id'],
            '{{bitrix24_section}}', ['employee_id', 'id']
        );
        $this->addForeignKey(
            'bitrix24_product_vat_id', '{{bitrix24_product}}', ['employee_id', 'vat_id'],
            '{{bitrix24_vat}}', ['employee_id', 'id']
        );
    }

    public function down()
    {
        $this->dropTable('{{bitrix24_product}}');
    }
}
