<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210605_043557_alter_act_add_idx extends Migration
{
    public function safeUp()
    {
        $this->createIndex('IDX_act_to_company', 'act', 'company_id');
        $this->createIndex('IDX_act_to_contractor', 'act', 'contractor_id');
        $this->createIndex('IDX_upd_to_company', 'upd', 'company_id');
        $this->createIndex('IDX_upd_to_contractor', 'upd', 'contractor_id');
        $this->createIndex('IDX_packing_list_to_company', 'packing_list', 'company_id');
        $this->createIndex('IDX_packing_list_to_contractor', 'packing_list', 'contractor_id');
    }

    public function safeDown()
    {
        $this->dropIndex('IDX_act_to_company', 'act');
        $this->dropIndex('IDX_act_to_contractor', 'act');
        $this->dropIndex('IDX_upd_to_company', 'upd');
        $this->dropIndex('IDX_upd_to_contractor', 'upd');
        $this->dropIndex('IDX_packing_list_to_company', 'packing_list');
        $this->dropIndex('IDX_packing_list_to_contractor', 'packing_list');
    }
}
