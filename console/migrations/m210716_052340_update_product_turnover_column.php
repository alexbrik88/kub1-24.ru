<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210716_052340_update_product_turnover_column extends Migration
{
    public function up()
    {
        $this->execute('
            UPDATE `product_turnover` t
            LEFT JOIN `product` p ON p.id = t.product_id
            SET t.product_group_id = p.group_id
        ');
    }

    public function down()
    {
        // no down
    }
}
