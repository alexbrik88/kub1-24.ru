<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210930_153009_alter_product_turnover_add_column extends Migration
{
    public $table = 'product_turnover';

    public function safeUp()
    {
        $table = $this->table;

        $this->addColumn($table, 'project_id', $this->integer()->after('contractor_id'));
        $this->addColumn($table, 'sale_point_id', $this->integer()->after('contractor_id'));
        $this->addColumn($table, 'industry_id', $this->integer()->after('contractor_id'));

        $this->execute("
            UPDATE `{$table}` t
            LEFT JOIN `invoice` i ON t.invoice_id = i.id
            SET 
              t.industry_id = i.industry_id,
              t.sale_point_id = i.sale_point_id,
              t.project_id = i.project_id  
            WHERE t.invoice_id IS NOT NULL;    
        ");
    }

    public function safeDown()
    {
        $table = $this->table;

        $this->dropColumn($table, 'project_id');
        $this->dropColumn($table, 'sale_point_id');
        $this->dropColumn($table, 'industry_id');
    }
}
