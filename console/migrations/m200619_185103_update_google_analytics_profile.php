<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200619_185103_update_google_analytics_profile extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%google_analytics_profile}}', 'analytics_account_id', $this->integer(11)->unsigned()->defaultValue(null));
        $this->addColumn('{{%google_analytics_profile}}', 'analytics_account_name', $this->string()->defaultValue(null));
        $this->addColumn('{{%google_analytics_profile}}', 'analytics_property_id', $this->string()->defaultValue(null));
        $this->addColumn('{{%google_analytics_profile}}', 'analytics_property_name', $this->string()->defaultValue(null));
        $this->addColumn('{{%google_analytics_profile}}', 'analytics_view_id', $this->integer(11)->unsigned()->defaultValue(null));
        $this->addColumn('{{%google_analytics_profile}}', 'analytics_view_name', $this->string()->defaultValue(null));
        $this->addColumn('{{%google_analytics_profile}}', 'analytics_goal_id', $this->integer(11)->unsigned()->defaultValue(null));
        $this->addColumn('{{%google_analytics_profile}}', 'analytics_goal_name', $this->string()->defaultValue(null));
        $this->addColumn('{{%google_analytics_profile}}', 'lead_name', $this->string()->defaultValue(null));
        $this->addColumn('{{%google_analytics_profile}}', 'plan_lead_cost', $this->integer(11)->unsigned()->defaultValue(null));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%google_analytics_profile}}', 'analytics_account_id');
        $this->dropColumn('{{%google_analytics_profile}}', 'analytics_account_name');
        $this->dropColumn('{{%google_analytics_profile}}', 'analytics_property_id');
        $this->dropColumn('{{%google_analytics_profile}}', 'analytics_property_name');
        $this->dropColumn('{{%google_analytics_profile}}', 'analytics_view_id');
        $this->dropColumn('{{%google_analytics_profile}}', 'analytics_view_name');
        $this->dropColumn('{{%google_analytics_profile}}', 'analytics_goal_id');
        $this->dropColumn('{{%google_analytics_profile}}', 'analytics_goal_name');
        $this->dropColumn('{{%google_analytics_profile}}', 'lead_name');
        $this->dropColumn('{{%google_analytics_profile}}', 'plan_lead_cost');
    }
}
