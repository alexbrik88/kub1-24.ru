<?php

use console\components\db\Migration;
use yii\db\Schema;

class m171006_110105_insert_invoice_income_item extends Migration
{
    public function safeUp()
    {
		$this->insert('invoice_income_item', [
            'id' => 8,
            'name' => 'Комиссия',
        ]);
    }
    
    public function safeDown()
    {
    	$this->delete('invoice_income_item', ['id' => 8]);
    }
}
