<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190226_101809_insert_registration_page_type extends Migration
{
    public $tableName = '{{%registration_page_type}}';

    public function safeUp()
    {
        $this->insert($this->tableName, [
            'id' => 17,
            'name' => 'Шаблон декларации ИП - Нулевая',
        ]);
    }

    public function safeDown()
    {
        $this->delete($this->tableName, ['id' => 17]);
    }
}
