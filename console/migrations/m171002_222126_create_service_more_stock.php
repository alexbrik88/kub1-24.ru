<?php

use console\components\db\Migration;
use yii\db\Schema;

class m171002_222126_create_service_more_stock extends Migration
{
    public $tableName = 'service_more_stock';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'company_type_id' => $this->integer()->defaultValue(null),
            'company_type_text' => $this->string()->defaultValue(null),
            'specialization' => $this->integer()->defaultValue(null),
            'average_invoice_count' => $this->integer()->defaultValue(null),
            'employees_count' => $this->integer()->defaultValue(null),
            'accountant_id' => $this->integer()->defaultValue(null),
            'has_logo' => $this->boolean()->defaultValue(false),
        ]);

        $this->addForeignKey($this->tableName . '_company_id', $this->tableName, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tableName . '_company_type_id', $this->tableName, 'company_type_id', 'company_type', 'id', 'RESTRICT', 'CASCADE');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_company_id', $this->tableName);
        $this->dropForeignKey($this->tableName . '_company_type_id', $this->tableName);

        $this->dropTable($this->tableName);
    }
}


