<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200423_115057_alter_service_payment_promo_code_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn(
            '{{%service_payment_promo_code}}',
            'days_from_registration',
            $this->integer(3)->defaultValue(null)->after('expired_at')
        );
    }

    public function safeDown()
    {
        $this->dropColumn('{{%service_payment_promo_code}}', 'days_from_registration');
    }
}
