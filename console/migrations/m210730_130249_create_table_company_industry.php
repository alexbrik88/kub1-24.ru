<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210730_130249_create_table_company_industry extends Migration
{
    public static $table = 'company_industry';

    public function safeUp()
    {
        $this->createTable(self::$table, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'employee_id' => $this->integer()->notNull(),
            'industry_type_id' => $this->integer()->notNull(),
            'name' => $this->string(255)->notNull(),
            'created_at' =>  $this->timestamp()->defaultExpression('NOW()'),
            'comment' => $this->text()->defaultValue('')
        ]);

        $this->addForeignKey('FK_'.self::$table.'_to_company', self::$table, 'company_id', 'company', 'id');
        $this->addForeignKey('FK_'.self::$table.'_to_employee', self::$table, 'employee_id', 'employee', 'id');
        $this->addForeignKey('FK_'.self::$table.'_to_industry_type', self::$table, 'industry_type_id', 'company_info_industry', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_'.self::$table.'_to_company', self::$table);
        $this->dropForeignKey('FK_'.self::$table.'_to_employee', self::$table);
        $this->dropForeignKey('FK_'.self::$table.'_to_industry_type', self::$table);
        $this->dropTable(self::$table);
    }
}
