<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180419_210827_add_out_invoice_tariff_id_to_payment extends Migration
{
    public $tServicePayment = 'service_payment';
    public $tServicePaymentOrder = 'service_payment_order';

    public function safeUp()
    {
        $this->alterColumn($this->tServicePayment, 'payment_for', "ENUM('subscribe', 'taxrobot', 'store_cabinet', 'out_invoice') NOT NULL DEFAULT 'subscribe' AFTER [[company_id]]");

        $this->addColumn($this->tServicePayment, 'out_invoice_tariff_id', $this->integer()->defaultValue(null));
        $this->addForeignKey($this->tServicePayment . '_out_invoice_tariff_id', $this->tServicePayment, 'out_invoice_tariff_id', 'service_store_out_invoice_tariff', 'id', 'RESTRICT', 'CASCADE');

        $this->addColumn($this->tServicePaymentOrder, 'out_invoice_tariff_id', $this->integer()->defaultValue(null));
        $this->addForeignKey($this->tServicePaymentOrder . '_out_invoice_tariff_id', $this->tServicePaymentOrder, 'out_invoice_tariff_id', 'service_store_out_invoice_tariff', 'id', 'RESTRICT', 'CASCADE');
    }
    
    public function safeDown()
    {
        $this->alterColumn($this->tServicePayment, 'payment_for', "ENUM('subscribe', 'taxrobot', 'store_cabinet') NOT NULL DEFAULT 'subscribe' AFTER [[company_id]]");

        $this->dropForeignKey($this->tServicePayment . '_out_invoice_tariff_id', $this->tServicePayment);
        $this->dropColumn($this->tServicePayment, 'out_invoice_tariff_id');

        $this->dropForeignKey($this->tServicePaymentOrder . '_out_invoice_tariff_id', $this->tServicePaymentOrder);
        $this->dropColumn($this->tServicePaymentOrder, 'out_invoice_tariff_id');
    }
}


