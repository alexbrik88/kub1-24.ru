<?php

use yii\db\Exception;
use yii\db\Migration;

class m210603_045057_alter_crm_task_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%crm_task}}';

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function up(): void
    {
        $this->addColumn(self::TABLE_NAME, 'datetime', $this->dateTime()->null()->after('time'));
        $this->db->createCommand("UPDATE crm_task SET datetime = TIMESTAMP (date, time)")->execute();
        $this->alterColumn(self::TABLE_NAME, 'datetime', $this->dateTime()->notNull());
        $this->createIndex('ck_datetime', self::TABLE_NAME, ['datetime']);
        $this->dropColumn(self::TABLE_NAME, 'date');
        $this->dropColumn(self::TABLE_NAME, 'time');
    }

    /**
     * @inheritDoc
     */
    public function down(): bool
    {
        return false;
    }
}
