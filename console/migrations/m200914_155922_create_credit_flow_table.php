<?php

use yii\db\Migration;

class m200914_155922_create_credit_flow_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%credit_flow}}';

    /**
     * @inheritDoc
     */
    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'credit_flow_id' => $this->bigPrimaryKey()->notNull(),
            'credit_id' => $this->bigInteger()->notNull(),
            'cash_bank_flow_id' => $this->integer()->null(),
            'cash_order_flow_id' => $this->integer()->null(),
            'cash_emoney_flow_id' => $this->integer()->null(),
            'olap_flow_id' => $this->integer()->notNull(),
            'wallet_id' => $this->tinyInteger()->notNull(),
        ]);

        $this->createIndex('ck_cash_bank_flow_id', self::TABLE_NAME, ['cash_bank_flow_id'], true);
        $this->createIndex('ck_cash_order_flow_id', self::TABLE_NAME, ['cash_order_flow_id'], true);
        $this->createIndex('ck_cash_emoney_flow_id', self::TABLE_NAME, ['cash_emoney_flow_id'], true);
        $this->createIndex('ck_credit_id', self::TABLE_NAME, ['credit_id']);
        $this->createIndex('uk_credit_flow', self::TABLE_NAME, ['olap_flow_id', 'wallet_id'], true);

        $this->createForeignKey('cash_bank_flows', ['cash_bank_flow_id' => 'id']);
        $this->createForeignKey('cash_order_flows', ['cash_order_flow_id' => 'id']);
        $this->createForeignKey('cash_emoney_flows', ['cash_emoney_flow_id' => 'id']);
        $this->createForeignKey('credit', ['credit_id' => 'credit_id']);
        $this->createForeignKey('olap_flows', ['olap_flow_id' => 'id', 'wallet_id' => 'wallet']);
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }

    /**
     * @param string $table
     * @param string[] $columns
     * @return void
     */
    private function createForeignKey(string $table, array $columns): void
    {
        $internal = array_keys($columns);
        $foreign = array_values($columns);

        $this->addForeignKey(
            sprintf('fk_credit_%s__%s_%s', implode('_', $internal), $table, implode('_', $foreign)),
            self::TABLE_NAME,
            $internal,
            '{{%' . $table . '}}',
            $foreign,
            'CASCADE',
            'CASCADE'
        );
    }
}
