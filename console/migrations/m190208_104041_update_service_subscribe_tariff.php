<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190208_104041_update_service_subscribe_tariff extends Migration
{
    public function safeUp()
    {
        $this->update('service_subscribe_tariff', ['price' => 1200], ['id' => 11]);
        $this->update('service_subscribe_tariff', ['price' => 3000], ['id' => 12]);
    }

    public function safeDown()
    {
        $this->update('service_subscribe_tariff', ['price' => 2000], ['id' => 11]);
        $this->update('service_subscribe_tariff', ['price' => 4500], ['id' => 12]);
    }
}
