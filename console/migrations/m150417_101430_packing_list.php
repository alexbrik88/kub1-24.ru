<?php

use yii\db\Schema;
use yii\db\Migration;

class m150417_101430_packing_list extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('packing_list_status', [
            'id' => Schema::TYPE_PK,
            'act_title' => Schema::TYPE_STRING . ' NOT NULL COMMENT "Название"',
        ], $tableOptions);

        $this->batchInsert('packing_list_status', ['id', 'act_title',], [
            [1, 'Создана',],
            [2, 'Распечатана',],
            [3, 'Передана',],
            [4, 'Получена-подписана',],
        ]);

        $this->createTable('packing_list', [
            'id' => Schema::TYPE_PK,
            'creation_date' => Schema::TYPE_DATE . ' COMMENT "Дата создания"',
            'author' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Автор (id сотрудника)"',
            'invoice_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Счёт"',
            'packing_list_status_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'status_date' => Schema::TYPE_DATE . ' COMMENT "Дата возникновения текущего статуса"',
            'status_author' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Автор текущего статуса"',
            'update_date' => Schema::TYPE_DATE . ' COMMENT "Дата обновления акта"',
            'additional_number' => Schema::TYPE_STRING . ' COMMENT "Дополнительный номер акта"',
            'file' => Schema::TYPE_STRING . ' COMMENT "Ссылка на файл"',
        ], $tableOptions);

        $this->addForeignKey('packing_list_to_employee_author', 'packing_list', 'author', 'employee', 'id');
        $this->addForeignKey('packing_list_to_employee_author_status', 'packing_list', 'status_author', 'employee', 'id');
        $this->addForeignKey('packing_list_to_invoice', 'packing_list', 'invoice_id', 'invoice', 'id');
        $this->addForeignKey('packing_list_to_act_status', 'packing_list', 'packing_list_status_id', 'packing_list_status', 'id');
    }

    public function down()
    {
        $this->dropTable('packing_list');
        $this->dropTable('packing_list_status');
    }
}
