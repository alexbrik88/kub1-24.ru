<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200903_104127_create_table_one_s_autoload_file extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('one_s_autoload_file', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'email_id' => $this->integer()->notNull(),
            'filename' => $this->string()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'uploaded_at' => $this->integer(),
        ]);

        $this->addForeignKey('FK_one_s_autoload_file_company', 'one_s_autoload_file', 'company_id', 'company', 'id', 'CASCADE');
        $this->addForeignKey('FK_one_s_autoload_file_email', 'one_s_autoload_file', 'email_id', 'one_s_autoload_email', 'id', 'CASCADE');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropForeignKey('FK_one_s_autoload_file_company', 'one_s_autoload_file');
        $this->dropForeignKey('FK_one_s_autoload_file_email', 'one_s_autoload_file');
        $this->dropTable('one_s_autoload_file');
    }
}
