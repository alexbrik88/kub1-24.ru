<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170508_165740_alter_agreement_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%agreement}}', 'document_type_id', $this->integer()->notNull()->defaultValue(1) . ' AFTER [[document_name]]');

        $this->addForeignKey('FK_agreement_agreementType', '{{%agreement}}', 'document_type_id', '{{%agreement_type}}', 'id');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('FK_agreement_agreementType', '{{%agreement}}');

        $this->dropColumn('{{%agreement}}', 'document_type_id');
    }
}
