<?php

use console\components\db\Migration;

class m200109_184652_evotor_user_company_id_insert_create_fk extends Migration
{
    const TABLE = '{{%evotor_user}}';

    public function up()
    {
        $this->addForeignKey(
            'evotor_user_company_id', self::TABLE, 'company_id',
            '{{%company}}', 'id',
            'CASCADE', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey('evotor_user_company_id', self::TABLE);
    }
}
