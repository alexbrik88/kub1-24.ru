<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200507_060947_alter_company_info_industry_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%company_info_industry}}', 'need_shop', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn('{{%company_info_industry}}', 'need_site', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn('{{%company_info_industry}}', 'need_storage', $this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%company_info_industry}}', 'need_shop');
        $this->dropColumn('{{%company_info_industry}}', 'need_site');
        $this->dropColumn('{{%company_info_industry}}', 'need_storage');
    }
}
