<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160907_140423_alter_order_changeColumn_quantity extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%order}}', 'quantity', $this->decimal(20, 10));
    }
    
    public function safeDown()
    {
        $this->alterColumn('{{%order}}', 'quantity', $this->integer());
    }
}


