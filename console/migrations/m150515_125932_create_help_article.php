<?php

use yii\db\Schema;
use yii\db\Migration;

class m150515_125932_create_help_article extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('help_article', [
            'id' => Schema::TYPE_PK,
            'sequence' => Schema::TYPE_INTEGER . ' COMMENT "Порядок следования (чем больше число, тем выше в выдаче находится)"',
            'title' => Schema::TYPE_STRING,
            'text' => Schema::TYPE_TEXT,
        ], $tableOptions);
    }
    
    public function safeDown()
    {
        $this->dropTable('help_article');
    }
}
