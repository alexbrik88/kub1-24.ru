<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180411_103209_alter_invoice_add_index extends Migration
{
    public function safeUp()
    {
        $this->createIndex('I_document_date', 'invoice', 'document_date');
    }

    public function safeDown()
    {
        $this->dropIndex('I_document_date', 'invoice');
    }
}
