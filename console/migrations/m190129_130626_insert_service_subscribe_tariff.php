<?php

use console\components\db\Migration;
use yii\db\Schema;
use yii\helpers\ArrayHelper;

class m190129_130626_insert_service_subscribe_tariff extends Migration
{
    public static $items = [
        [5, 2, 1, 1000, 'Скидка 10% при оплате за 4 месяца',],
        [6, 2, 4, 3600, 'Скидка 20% при оплате за год',],
        [7, 2, 12, 9600, 'Лучшая цена!',],
        [8, 3, 1, 2000, 'Скидка 20% при оплате за 4 месяца',],
        [9, 3, 4, 6400, 'Скидка 40% при оплате за год',],
        [10, 3, 12, 14400, 'Лучшая цена!',],
        [11, 4, 4, 6400, 'Скидка 25% при оплате за год',],
        [12, 4, 12, 14400, 'Лучшая цена!',],
        [13, 5, 1, 2000, 'Скидка 15% при оплате за 4 месяца',],
        [14, 5, 4, 6800, 'Скидка 25% при оплате за год',],
        [15, 5, 12, 18000, 'Лучшая цена!',],
    ];

    public function safeUp()
    {
        $this->batchInsert('service_subscribe_tariff', [
            'id',
            'group_id',
            'duration_month',
            'price',
            'proposition'
        ], self::$items);
    }

    public function safeDown()
    {
        $this->delete('service_subscribe_tariff', ['id' => ArrayHelper::getColumn(self::$items, 0)]);
    }
}
