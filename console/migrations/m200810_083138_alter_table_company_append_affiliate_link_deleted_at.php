<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200810_083138_alter_table_company_append_affiliate_link_deleted_at extends Migration
{
    public $tableName = "company";

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'affiliate_link_deleted_at', $this->integer(11));
        $this->addColumn($this->tableName, 'affiliate_link_updated_at', $this->integer(11));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'affiliate_link_deleted_at');
        $this->dropColumn($this->tableName, 'affiliate_link_updated_at');
    }
}
