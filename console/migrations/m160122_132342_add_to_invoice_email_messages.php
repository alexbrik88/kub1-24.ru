<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160122_132342_add_to_invoice_email_messages extends Migration
{
    public $tInvoice = 'invoice';

    public function safeUp()
    {
        $this->addColumn($this->tInvoice, 'email_messages', $this->integer(11));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tInvoice, 'email_messages');
    }
}


