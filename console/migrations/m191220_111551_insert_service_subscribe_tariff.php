<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191220_111551_insert_service_subscribe_tariff extends Migration
{
    public function safeUp()
    {
        $this->update('{{%service_subscribe_tariff}}', [
            'is_active' => 0,
        ], [
            'id' => 11,
        ]);
        $this->insert('{{%service_subscribe_tariff}}', [
            'id' => 27,
            'tariff_group_id' => 7,
            'duration_month' => 4,
            'duration_day' => 0,
            'price' => 1200,
            'is_active' => 1,
            'proposition' => 'Лучшая цена!',
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%service_subscribe_tariff}}', [
            'id' => 27,
        ]);
        $this->update('{{%service_subscribe_tariff}}', [
            'is_active' => 1,
        ], [
            'id' => 11,
        ]);
    }
}
