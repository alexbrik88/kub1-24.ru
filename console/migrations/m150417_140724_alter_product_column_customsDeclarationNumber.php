<?php

use yii\db\Schema;
use yii\db\Migration;

class m150417_140724_alter_product_column_customsDeclarationNumber extends Migration
{
    public function up()
    {
        $this->alterColumn('product', 'customs_declaration_number', Schema::TYPE_STRING . '(45) NULL');
    }

    public function down()
    {
        $this->alterColumn('product', 'customs_declaration_number', Schema::TYPE_INTEGER . ' NULL');
    }
}
