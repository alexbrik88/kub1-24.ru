<?php

use yii\db\Schema;
use yii\db\Migration;

class m150423_130352_alter_InvoiceFacture_addColumn_filename extends Migration
{
    public function up()
    {
        $this->addColumn('invoice_facture', 'filename', Schema::TYPE_STRING . '(100) NULL');
    }

    public function down()
    {
        $this->dropColumn('invoice_facture', 'filename');
    }
}
