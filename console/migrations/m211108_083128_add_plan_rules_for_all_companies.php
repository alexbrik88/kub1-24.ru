<?php

use common\models\cash\CashFlowsBase;
use common\models\document\PackingList;
use console\components\db\Migration;
use frontend\modules\analytics\models\PlanCashRule;
use common\models\Company;

class m211108_083128_add_plan_rules_for_all_companies extends Migration
{
    const ALL_INCOME_ITEMS = 'NULL';
    const ALL_EXPENDITURE_ITEMS = 'NULL';

    public function up()
    {
        Yii::$app->db->createCommand('SET session wait_timeout=288000')->execute();
        Yii::$app->db->createCommand('SET session interactive_timeout=288000')->execute();

        $allIncomeItems = self::ALL_INCOME_ITEMS;
        $allExpenditureItems = self::ALL_EXPENDITURE_ITEMS;

        echo "\n\n";
        $countCompanies = 0;
        $companiesIds = Company::find()->select('id')->asArray()->column();
        foreach ($companiesIds as $companyId) {

            $transaction = Yii::$app->db->beginTransaction();

            // add plan rules
            $time = time();
            $this->execute("
                INSERT IGNORE `plan_cash_rule` (`company_id`, `flow_type`, `income_item_id`, `created_at`, `updated_at`)
                VALUES 
                    ({$companyId}, 1, {$allIncomeItems}, {$time}, {$time}),
                    ({$companyId}, 0, {$allExpenditureItems}, {$time}, {$time});
            ");

            $transaction->commit();

            $countCompanies++;
        }

        echo "Done. Rules updated for companies: $countCompanies\n\n";
    }

    public function down()
    {
        $this->execute(" DELETE FROM `plan_cash_rule` WHERE `flow_type` = 1 AND `income_item_id` IS NULL");
        $this->execute(" DELETE FROM `plan_cash_rule` WHERE `flow_type` = 0 AND `expenditure_item_id` IS NULL");
    }
}
