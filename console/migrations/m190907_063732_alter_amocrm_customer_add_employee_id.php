<?php

use console\components\db\Migration;

class m190907_063732_alter_amocrm_customer_add_employee_id extends Migration
{

    public function safeUp()
    {
        $this->truncateTable('{{amocrm_customer}}');
        $this->addColumn('{{amocrm_customer}}', 'employee_id', $this->integer()->notNull()->comment('ID покупателя на сайте'));
        $this->addForeignKey(
            'amocrm_customer_employee_id', '{{amocrm_customer}}', 'employee_id',
            '{{employee}}', 'id', 'CASCADE', 'CASCADE'
        );

    }

    public function down()
    {
        $this->dropColumn('{{amocrm_customer}}', 'employee_id');
    }

}
