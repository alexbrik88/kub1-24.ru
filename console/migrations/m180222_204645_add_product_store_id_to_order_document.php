<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180222_204645_add_product_store_id_to_order_document extends Migration
{
    public $tableName = 'order_document';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'store_id', $this->integer()->defaultValue(null));
        $this->addForeignKey($this->tableName . '_store_id', $this->tableName, 'store_id', 'store', 'id', 'RESTRICT', 'CASCADE');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_store_id', $this->tableName);
        $this->dropColumn($this->tableName, 'store_id');
    }
}


