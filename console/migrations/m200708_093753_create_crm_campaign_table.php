<?php

use yii\db\Migration;

class m200708_093753_create_crm_campaign_table extends Migration
{
    /** @var string Имя таблицы */
    private const TABLE_NAME = '{{%crm_campaign}}';

    /** @var string[] Внешние столбцы */
    private const FK_COLUMNS = ['company_id', 'employee_id'];

    /**
     * @inheritDoc
     */
    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'campaign_id' => $this->bigPrimaryKey(),
            'campaign_type_id' => $this->bigInteger()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'employee_id' => $this->integer()->notNull(),
            'status' => $this->boolean()->notNull()->defaultValue(true),
            'created_at' => $this->timestamp()->notNull()->defaultValue(0),
            'updated_at' => $this->timestamp()->notNull()->defaultValue(0),
        ]);

        $this->createIndex('ck_crm_campaign', self::TABLE_NAME, self::FK_COLUMNS);
        $this->addForeignKey(
            'fk_crm_campaign__employee_company',
            self::TABLE_NAME,
            self::FK_COLUMNS,
            '{{%employee_company}}',
            self::FK_COLUMNS,
            'CASCADE',
            'CASCADE'
        );
        $this->createIndex('ck_crm_campaign_type_id', self::TABLE_NAME, 'campaign_type_id');
        $this->addForeignKey(
            'fk_crm_campaign__crm_campaign_type',
            self::TABLE_NAME,
            'campaign_type_id',
            '{{%crm_campaign_type}}',
            'campaign_type_id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }
}
