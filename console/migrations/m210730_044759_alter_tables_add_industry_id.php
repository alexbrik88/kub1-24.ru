<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210730_044759_alter_tables_add_industry_id extends Migration
{
    public static $docsTables = [
        // RU
        'invoice',
        'goods_cancellation' // списание товара
    ];
    
    public static $cashTables = [
        // RU
        'cash_bank_flows',
        'cash_emoney_flows',
        'cash_order_flows',
        'acquiring_operation',
        'card_operation',
        // FOREIGN
        'cash_bank_foreign_currency_flows',
        'cash_emoney_foreign_currency_flows',
        'cash_order_foreign_currency_flows',
    ];
    
    public static $planCashTables = [
        // RU
        'plan_cash_flows',
        // FOREIGN
        'plan_cash_foreign_currency_flows',
    ];
    
    public static $refTableName = 'company_info_industry';

    public function safeUp()
    {
        $this->addToDocsTable();
        $this->addToFlowsTables();
        $this->addToPlanFlowsTables();
    }

    public function safeDown()
    {
        $this->dropFromDocsTable();
        $this->dropFromFlowsTables();
        $this->dropFromPlanFlowsTables();
    }

    private function addToDocsTable()
    {
        foreach (self::$docsTables as $tableName) {
            $this->addColumn($tableName, 'industry_id', $this->integer());
            $this->addForeignKey("FK_{$tableName}_to_industry", $tableName, 'industry_id', self::$refTableName, 'id', 'SET NULL', 'CASCADE');
        }
    }

    private function addToFlowsTables()
    {
        foreach (self::$cashTables as $tableName) {
            $this->addColumn($tableName, 'industry_id', $this->integer()->after('sale_point_id'));
            $this->addForeignKey("FK_{$tableName}_to_industry", $tableName, 'industry_id', self::$refTableName, 'id', 'SET NULL', 'CASCADE');
        }
    }

    private function addToPlanFlowsTables()
    {
        foreach (self::$planCashTables as $tableName) {
            $this->addColumn($tableName, 'industry_id', $this->integer()->after('sale_point_id'));
            $this->addForeignKey("FK_{$tableName}_to_industry", $tableName, 'industry_id', self::$refTableName, 'id', 'SET NULL', 'CASCADE');
        }
    }

    private function dropFromDocsTable()
    {
        foreach (self::$docsTables as $tableName) {
            $this->dropForeignKey("FK_{$tableName}_to_industry", $tableName);
            $this->dropColumn($tableName, 'industry_id');
        }
    }

    private function dropFromFlowsTables()
    {
        foreach (self::$cashTables as $tableName) {
            $this->dropForeignKey("FK_{$tableName}_to_industry", $tableName);
            $this->dropColumn($tableName, 'industry_id');
        }
    }

    private function dropFromPlanFlowsTables()
    {
        foreach (self::$planCashTables as $tableName) {
            $this->dropForeignKey("FK_{$tableName}_to_industry", $tableName);
            $this->dropColumn($tableName, 'industry_id');
        }
    }

}
