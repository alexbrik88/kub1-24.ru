<?php

use console\components\db\Migration;
use yii\db\Schema;
use yii\db\Query;

class m201223_075623_alter_checking_accountant_add_columns extends Migration
{
    private function getDbName()
    {
        if (preg_match('/dbname=([^;]*)/', Yii::$app->db->dsn, $match)) {
            return $match[1];
        } else {
            return '';
        }
    }

    public function safeUp()
    {
        $collate = (new Query)
            ->select('COLLATION_NAME')
            ->from('information_schema.COLUMNS')
            ->where([
                'TABLE_SCHEMA' => $this->getDbName(),
                'TABLE_NAME' => 'currency',
                'COLUMN_NAME' => 'id',
            ])->scalar() ?: 'utf8_unicode_ci';
        $append = "COLLATE $collate";

        $this->addColumn('{{%checking_accountant}}', 'currency_id', $this->char(3)->notNull()->after('company_id')->append($append));
        $this->addColumn('{{%checking_accountant}}', 'is_foreign_bank', $this->boolean()->notNull()->after('currency_id')->defaultValue(false));
        $this->addColumn('{{%checking_accountant}}', 'bank_address', $this->text()->notNull()->after('bank_city')->defaultValue(''));
        $this->addColumn('{{%checking_accountant}}', 'iban', $this->text());
        $this->addColumn('{{%checking_accountant}}', 'swift', $this->text());
        $this->addColumn('{{%checking_accountant}}', 'bank_name_en', $this->text());
        $this->addColumn('{{%checking_accountant}}', 'bank_address_en', $this->text());
        $this->addColumn('{{%checking_accountant}}', 'corr_rs', $this->text());
        $this->addColumn('{{%checking_accountant}}', 'corr_swift', $this->text());
        $this->addColumn('{{%checking_accountant}}', 'corr_bank_name', $this->text());
        $this->addColumn('{{%checking_accountant}}', 'corr_bank_address', $this->text());

        $this->update('{{%checking_accountant}}', ['currency_id' => '643']);

        $this->createIndex('currency_id', '{{%checking_accountant}}', 'currency_id');

        $this->addForeignKey('FK_checking_accountant__currency_id', '{{%checking_accountant}}', 'currency_id', '{{%currency}}', 'id');

    }

    public function safeDown()
    {
        $this->dropIndex('currency_id', '{{%checking_accountant}}');
        $this->dropForeignKey('FK_checking_accountant__currency_id', '{{%checking_accountant}}');

        $this->dropColumn('{{%checking_accountant}}', 'currency_id');
        $this->dropColumn('{{%checking_accountant}}', 'is_foreign_bank');
        $this->dropColumn('{{%checking_accountant}}', 'bank_address');
        $this->dropColumn('{{%checking_accountant}}', 'iban');
        $this->dropColumn('{{%checking_accountant}}', 'swift');
        $this->dropColumn('{{%checking_accountant}}', 'bank_name_en');
        $this->dropColumn('{{%checking_accountant}}', 'bank_address_en');
        $this->dropColumn('{{%checking_accountant}}', 'corr_rs');
        $this->dropColumn('{{%checking_accountant}}', 'corr_swift');
        $this->dropColumn('{{%checking_accountant}}', 'corr_bank_name');
        $this->dropColumn('{{%checking_accountant}}', 'corr_bank_address');
    }
}
