<?php

use console\components\db\Migration;
use yii\db\Exception;

class m201210_203902_update_marketing_report_table extends Migration
{
    public const TABLE_NAME = '{{%marketing_report}}';

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function up()
    {
        $this->db
            ->createCommand()
            ->delete(self::TABLE_NAME, 'utm_campaign = :utm_campaign', [':utm_campaign' => 'default_kub_campaign'])
            ->execute();
    }

    /**
     * @inheritDoc
     */
    public function down()
    {

    }
}
