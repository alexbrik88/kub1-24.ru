<?php

use yii\db\Schema;
use yii\db\Migration;

class m150415_133700_create_statuses_tables extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%out_invoice_status}}', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' DEFAULT NULL'
        ], $tableOptions);

        $this->batchInsert('{{%out_invoice_status}}', ['name'], [
            ['Создан'],
            ['Отправлен'],
            ['Просмотрен'],
            ['Утвержден'],
            ['Отменён'],
            ['Оплачен частично'],
            ['Оплачен полностью'],
            ['Просрочен'],
        ]);

        $this->createTable('{{%out_act_status}}', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' DEFAULT NULL'
        ], $tableOptions);

        $this->batchInsert('{{%out_act_status}}', ['name'], [
            ['Создан'],
            ['Распечатан'],
            ['Передан'],
            ['Получен-подписан']
        ]);

        $this->createTable('{{%out_packing_status}}', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' DEFAULT NULL'
        ], $tableOptions);

        $this->batchInsert('{{%out_packing_status}}', ['name'], [
            ['Создана'],
            ['Распечатана'],
            ['Передана'],
            ['Получена-подписана']
        ]);

        $this->createTable('{{%out_through_invoice_status}}', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' DEFAULT NULL'
        ], $tableOptions);

        $this->batchInsert('{{%out_through_invoice_status}}', ['name'], [
            ['Создан'],
            ['Распечатан'],
            ['Передан']
        ]);

        $this->createTable('{{%in_invoice_status}}', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' DEFAULT NULL'
        ], $tableOptions);

        $this->batchInsert('{{%in_invoice_status}}', ['name'], [
            ['Создан'],
            ['Оплачен частично'],
            ['Оплачен полностью'],
            ['Просрочен']
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%out_invoice_status}}');
        $this->dropTable('{{%out_act_status}}');
        $this->dropTable('{{%out_packing_status}}');
        $this->dropTable('{{%out_through_invoice_status}}');
        $this->dropTable('{{%in_invoice_status}}');
    }
}
