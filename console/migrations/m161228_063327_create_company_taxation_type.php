<?php

use console\components\db\Migration;
use yii\db\Schema;

class m161228_063327_create_company_taxation_type extends Migration
{
    public $tableName = 'company_taxation_type';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'osno' => $this->boolean()->defaultValue(0)->notNull(),
            'usn' => $this->boolean()->defaultValue(0)->notNull(),
            'envd' => $this->boolean()->defaultValue(0)->notNull(),
            'psn' => $this->boolean()->defaultValue(0)->notNull(),
            'usn_type' => $this->boolean()->defaultValue(0)->notNull(),
            'usn_percent' => $this->integer()->defaultValue(null),
        ]);

        $this->addForeignKey($this->tableName . '_company_id', $this->tableName, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_company_id', $this->tableName);
        $this->dropTable($this->tableName);
    }
}


