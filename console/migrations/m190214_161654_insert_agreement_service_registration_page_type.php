<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190214_161654_insert_agreement_service_registration_page_type extends Migration
{
    public $tableName = '{{%registration_page_type}}';

    public function safeUp()
    {
        $this->insert($this->tableName, [
            'id' => 16,
            'name' => 'Договор оказания услуг',
        ]);
    }

    public function safeDown()
    {
        $this->delete($this->tableName, ['id' => 16]);
    }
}
