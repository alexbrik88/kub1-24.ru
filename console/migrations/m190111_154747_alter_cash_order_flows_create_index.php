<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190111_154747_alter_cash_order_flows_create_index extends Migration
{
    public function safeUp()
    {
        $this->createIndex('cash_order_flows_is_accounting', 'cash_order_flows', 'is_accounting');
        $this->createIndex('cash_order_flows_is_taxable', 'cash_order_flows', 'is_taxable');
    }

    public function safeDown()
    {
        $this->dropIndex('cash_order_flows_is_accounting', 'cash_order_flows');
        $this->dropIndex('cash_order_flows_is_taxable', 'cash_order_flows');
    }
}
