<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190703_150953_alter_agent_report_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('agent_report', 'pay_up_date',Schema::TYPE_DATE . ' NOT NULL');
    }

    public function safeDown()
    {
        $this->dropColumn('agent_report', 'pay_up_date');
    }
}
