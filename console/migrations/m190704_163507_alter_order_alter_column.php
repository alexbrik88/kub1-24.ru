<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190704_163507_alter_order_alter_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%order}}', 'discount', $this->decimal(9, 6));
    }

    public function safeDown()
    {
        $this->alterColumn('{{%order}}', 'discount', $this->decimal(5, 2));
    }
}
