<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170721_051516_alter_servicePayment_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%service_payment}}', 'company_id', $this->integer()->notNull() . ' AFTER [[id]]');
        $this->addColumn('{{%service_payment}}', 'tariff_id', $this->integer() . ' AFTER [[type_id]]');

        $this->execute("
            UPDATE
                {{%service_payment}}
            LEFT JOIN
                {{%service_subscribe}} ON {{%service_subscribe}}.[[id]] = {{%service_payment}}.[[subscribe_id]]
            SET
                {{%service_payment}}.[[company_id]] = {{%service_subscribe}}.[[company_id]],
                {{%service_payment}}.[[tariff_id]] = {{%service_subscribe}}.[[tariff_id]]
        ");

        $this->execute("
            DELETE
                {{%service_payment}}
            FROM
                {{%service_payment}}
            LEFT JOIN
                {{%company}} ON {{%company}}.[[id]] = {{%service_payment}}.[[company_id]]
            WHERE {{%company}}.[[id]] IS NULL
        ");
        
        $this->addForeignKey('FK_servicePayment_company', '{{%service_payment}}', 'company_id', '{{%company}}', 'id');
        $this->addForeignKey('FK_servicePayment_serviceSubscribeTariff', '{{%service_payment}}', 'tariff_id', '{{%service_subscribe_tariff}}', 'id');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('FK_servicePayment_company', '{{%service_payment}}');
        $this->dropForeignKey('FK_servicePayment_serviceSubscribeTariff', '{{%service_payment}}');
        
        $this->dropColumn('{{%service_payment}}', 'company_id');
        $this->dropColumn('{{%service_payment}}', 'tariff_id');
    }
}
