<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180309_094814_alter_store_user_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('store_user', 'contractor_id', $this->integer()->after('store_company_id'));
    }

    public function safeDown()
    {
        $this->dropColumn('store_user', 'contractor_id');
    }
}
