<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%crm_kub_responsible}}`.
 */
class m211210_154533_create_crm_kub_responsible_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%crm_kub_responsible}}', [
            'employee_id' => $this->integer()->notNull(),
            'last_time_at' => $this->decimal(18, 8)->notNull(),
            'is_responsible' => $this->boolean()->notNull()->defaultValue(false),
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', '{{%crm_kub_responsible}}', 'employee_id');

        if (isset(Yii::$app->params['service']['crm_responsible_employee_id'])) {
            $this->insert('{{%crm_kub_responsible}}', [
                'employee_id' => Yii::$app->params['service']['crm_responsible_employee_id'],
                'last_time_at' => 0,
                'is_responsible' => true,
            ]);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%crm_kub_responsible}}');
    }
}
