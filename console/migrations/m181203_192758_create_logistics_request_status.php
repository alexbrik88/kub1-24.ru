<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181203_192758_create_logistics_request_status extends Migration
{
    public $tableName = 'logistics_request_status';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);

        $this->batchInsert($this->tableName, ['id', 'name'], [
            [1, 'На погрузке'],
            [2, 'Погрузились'],
            [3, 'На разгрузке'],
            [4, 'Разгрузились'],
            [5, 'Отменена'],
            [6, 'Проблема'],
        ]);

        $this->addColumn('logistics_request', 'status_id', $this->integer()->notNull());

        $this->addForeignKey('logistics_request_status_id', 'logistics_request', 'status_id', $this->tableName, 'id', 'RESTRICT', 'CASCADE');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('logistics_request_status_id', 'logistics_request');

        $this->dropColumn('logistics_request', 'status_id');

        $this->dropTable($this->tableName);
    }
}


