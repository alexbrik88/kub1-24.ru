<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160202_111045_alter_employee_set_default_value_to_notify extends Migration
{
    public $tEmployee = 'employee';

    public function safeUp()
    {
        $this->alterColumn($this->tEmployee, 'notify_nearly_report', $this->boolean()->notNull()->defaultValue(1));
        $this->alterColumn($this->tEmployee, 'notify_new_features', $this->boolean()->notNull()->defaultValue(1));
    }
    
    public function safeDown()
    {
        $this->alterColumn($this->tEmployee, 'notify_nearly_report', $this->boolean()->notNull());
        $this->alterColumn($this->tEmployee, 'notify_new_features', $this->boolean()->notNull());
    }
}


