<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180905_232442_delete_bug_export_one_c extends Migration
{
    public $tableName = 'export';

    public function safeUp()
    {
        $this->delete($this->tableName, ['and',
            ['period_start_date' => null],
            ['period_end_date' => null],
            ['io_type_in' => 0],
            ['io_type_out' => 0],
            ['product_and_service' => 0],
            ['contractor' => 0],
            ['total_objects' => 1],
            ['status' => 1],
        ]);
    }

    public function safeDown()
    {
        echo "This migration not rolled back";
    }
}


