<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181012_095510_add_is_finished_to_price_list extends Migration
{
    public $tableName = 'price_list';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'is_finished', $this->boolean()->defaultValue(false));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'is_finished');
    }
}


