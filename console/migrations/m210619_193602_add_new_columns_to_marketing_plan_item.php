<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210619_193602_add_new_columns_to_marketing_plan_item extends Migration
{
    public function safeUp()
    {
        $this->addColumn('marketing_plan_item', 'repeated_purchases_count', $this->integer(11)->unsigned());
        $this->addColumn('marketing_plan_item', 'total_purchases_count', $this->integer(11)->unsigned());
    }

    public function safeDown()
    {
        $this->dropColumn('marketing_plan_item', 'repeated_purchases_count');
        $this->dropColumn('marketing_plan_item', 'total_purchases_count');
    }
}
