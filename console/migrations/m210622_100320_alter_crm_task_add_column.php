<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210622_100320_alter_crm_task_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%crm_task}}', 'completed_at', $this->timestamp()->null());
        $this->addColumn('{{%crm_task}}', 'result', $this->text()->notNull()->defaultValue(''));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%crm_task}}', 'result');
        $this->dropColumn('{{%crm_task}}', 'completed_at');
    }
}
