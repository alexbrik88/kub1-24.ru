<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170808_115353_alter_serviceSubscribe_alterColumn extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%service_subscribe}}', 'discount', $this->decimal(6, 4)->notNull()->defaultValue(0));
    }
    
    public function safeDown()
    {
        $this->alterColumn('{{%service_subscribe}}', 'discount', $this->integer(4, 2)->notNull()->defaultValue(0));
    }
}
