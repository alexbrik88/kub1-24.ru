<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181023_092531_alter_employee_alter_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%employee}}', 'company_id', $this->integer());
    }

    public function safeDown()
    {
        //
    }
}
