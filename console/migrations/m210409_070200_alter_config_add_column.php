<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210409_070200_alter_config_add_column extends Migration
{
    public $table = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->table, 'report_debtor_period_type', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 2');
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'report_debtor_period_type');
    }
}
