<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190529_085902_alter_invoice_add_column extends Migration
{
    public $tInvoice = 'invoice';
    public $tApiPartners = 'api_partners';

    public function safeUp()
    {
        $this->addColumn($this->tInvoice, 'create_api_id', $this->integer()->defaultValue(null));
        $this->addForeignKey('FK_invoice_to_api_partners', $this->tInvoice, 'create_api_id', $this->tApiPartners, 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_invoice_to_api_partners', $this->tInvoice);
        $this->dropColumn($this->tInvoice, 'create_api_id');
    }
}