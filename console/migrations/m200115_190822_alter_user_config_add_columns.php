<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200115_190822_alter_user_config_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn(
            'user_config',
            'yandex_direct_status',
            $this->boolean()->notNull()->defaultValue(true)
        );
        $this->addColumn(
            'user_config',
            'yandex_direct_strategy',
            $this->boolean()->notNull()->defaultValue(false)
        );
        $this->addColumn(
            'user_config',
            'yandex_direct_placement',
            $this->boolean()->notNull()->defaultValue(true)
        );
        $this->addColumn(
            'user_config',
            'yandex_direct_budget',
            $this->boolean()->notNull()->defaultValue(false)
        );
    }

    public function safeDown()
    {
        $this->dropColumn('user_config', 'yandex_direct_status');
        $this->dropColumn('user_config', 'yandex_direct_strategy');
        $this->dropColumn('user_config', 'yandex_direct_placement');
        $this->dropColumn('user_config', 'yandex_direct_budget');
    }
}
