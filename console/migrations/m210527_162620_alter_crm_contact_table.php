<?php

use yii\db\Migration;

class m210527_162620_alter_crm_contact_table extends Migration
{
    /** @var string  */
    private const TABLE_NAME = '{{%crm_contact}}';

    /**
     * @inheritDoc
     */
    public function up(): void
    {
        $this->dropForeignKey('fk_crm_contact__crm_client', self::TABLE_NAME);
        $this->dropIndex('ck_client_id', self::TABLE_NAME);
        $this->renameColumn(self::TABLE_NAME, 'client_id', 'contractor_id');
        $this->alterColumn(self::TABLE_NAME, 'contractor_id', $this->integer()->notNull());
        $this->createIndex('ck_contractor_id', self::TABLE_NAME, ['contractor_id']);
        $this->addForeignKey(
            'fk_crm_contact__contractor',
            self::TABLE_NAME,
            ['contractor_id'],
            '{{%contractor}}',
            ['id'],
            'CASCADE',
            'CASCADE'
        );
    }
}
