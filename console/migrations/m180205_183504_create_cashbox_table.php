<?php

use yii\db\Migration;

/**
 * Handles the creation of table `cashbox`.
 */
class m180205_183504_create_cashbox_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('cashbox', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'responsible_employee_id' => $this->integer(),
            'is_accounting' => $this->boolean()->notNull()->defaultValue(true),
            'is_main' => $this->boolean()->notNull()->defaultValue(false),
            'is_closed' => $this->boolean()->notNull()->defaultValue(false),
            'name' => $this->string(50)->notNull(),
        ]);

        $this->addForeignKey('FK_cashbox_company', 'cashbox', 'company_id', 'company', 'id');
        $this->addForeignKey('FK_cashbox_employee', 'cashbox', 'responsible_employee_id', 'employee', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('cashbox');
    }
}
