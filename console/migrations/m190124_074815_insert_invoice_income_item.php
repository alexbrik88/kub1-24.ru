<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190124_074815_insert_invoice_income_item extends Migration
{
    public function safeUp()
    {
        $this->insert('invoice_income_item', [
            'id' => 14,
            'name' => 'Обеспечительный платёж',
        ]);
    }

    public function safeDown()
    {
        $this->delete('invoice_income_item', ['id' => 14]);
    }
}
