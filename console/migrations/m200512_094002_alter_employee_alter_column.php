<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200512_094002_alter_employee_alter_column extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%employee}}', 'auth_key',  $this->string());
    }

    public function down()
    {
        $this->alterColumn('{{%employee}}', 'auth_key',  $this->string(32)->notNull());
    }
}
