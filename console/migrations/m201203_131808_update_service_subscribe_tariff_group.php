<?php

use console\components\db\Migration;
use yii\db\Expression;
use yii\db\Schema;

class m201203_131808_update_service_subscribe_tariff_group extends Migration
{
    public function safeUp()
    {
        $this->update('{{%service_subscribe_tariff_group}}', [
            'name' => new Expression('REPLACE([[name]], "ФинДиректор.Финансы +", "ФинДиректор PRO")'),
            'description' => new Expression('REPLACE([[description]], "ФинДиректор.Финансы +", "ФинДиректор PRO")'),
        ], [
            'id' => range(11, 18),
        ]);
        $this->update('{{%service_subscribe_tariff_group}}', [
            'name' => new Expression('REPLACE([[name]], "ФинДиректор.Финансы", "ФинДиректор")'),
            'description' => new Expression('REPLACE([[description]], "ФинДиректор.Финансы", "ФинДиректор")'),
        ], [
            'id' => range(11, 18),
        ]);
        $this->update('{{%service_subscribe_tariff_group}}', [
            'name' => new Expression('REPLACE([[name]], "ФинДиректор+", "ФинДиректор PRO")'),
            'description' => new Expression('REPLACE([[description]], "ФинДиректор+", "ФинДиректор PRO")'),
        ], [
            'id' => range(11, 18),
        ]);
    }

    public function safeDown()
    {

    }
}
