<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170330_085155_insert_outInvoiceStatus extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%out_invoice_status}}', [
            'id' => 9,
            'name' => 'Автосчет',
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%out_invoice_status}}', ['id' => 9]);
    }
}
