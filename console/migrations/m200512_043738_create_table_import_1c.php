<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200512_043738_create_table_import_1c extends Migration
{
    public $table = 'import_1c';

    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'employee_id' => $this->integer()->notNull(),
        ], $this->tableOptions);

        $this->addForeignKey('FK_import_1c_to_company', 'import_1c', 'company_id', 'company', 'id', 'CASCADE');
        $this->addForeignKey('FK_import_1c_to_employee', 'import_1c', 'employee_id', 'employee', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_import_1c_to_company', 'import_1c');
        $this->dropForeignKey('FK_import_1c_to_employee', 'import_1c');
        $this->dropTable($this->table);
    }
}
