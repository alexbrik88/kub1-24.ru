<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170109_182218_alter_invoiceFacture_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%invoice_facture}}', 'is_original_updated_at', $this->integer()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%invoice_facture}}', 'is_original_updated_at');
    }
}


