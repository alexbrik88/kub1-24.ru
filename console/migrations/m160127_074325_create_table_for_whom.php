<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160127_074325_create_table_for_whom extends Migration
{
    public $tForWhom = 'for_whom';

    public function safeUp()
    {
        $this->createTable($this->tForWhom, [
            'id' => $this->integer(11)->unique()->notNull(),
            'name' => $this->string(255)->defaultValue(null),
        ]);

        $this->batchInsert($this->tForWhom, ['id', 'name',], [
            [1, 'Всем'],
            [2, 'Только ОСНО'],
            [3, 'Только УСН, ЕНВД'],
        ]);
    }

    public function safeDown()
    {
        $this->dropTable($this->tForWhom);
    }
}


