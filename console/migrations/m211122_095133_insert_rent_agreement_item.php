<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211122_095133_insert_rent_agreement_item extends Migration
{
    public function safeUp()
    {
        $this->execute('
            INSERT INTO
                {{%rent_agreement_item}} (
                    [[rent_agreement_id]],
                    [[rent_entity_id]],
                    [[number]],
                    [[price]],
                    [[discount]],
                    [[amount]]
                )
            SELECT
                {{%rent_agreement}}.[[id]],
                {{%rent_agreement}}.[[entity_id]],
                1,
                ROUND({{%rent_agreement}}.[[amount]] * 100),
                0,
                ROUND({{%rent_agreement}}.[[amount]] * 100)
            FROM
                {{%rent_agreement}}
            WHERE
                {{%rent_agreement}}.[[entity_id]] IS NOT NULL
        ');
    }

    public function safeDown()
    {
        $this->delete('{{%rent_agreement_item}}');
    }
}
