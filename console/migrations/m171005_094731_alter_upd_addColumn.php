<?php

use console\components\db\Migration;
use yii\db\Schema;

class m171005_094731_alter_upd_addColumn extends Migration
{
    public $tableName = 'upd';

    public function safeUp()
    {
        $this->renameColumn($this->tableName, 'consignor_id', 'consignee_id');
        $this->alterColumn($this->tableName, 'consignee_id', $this->integer());
        $this->update($this->tableName, ['consignee_id' => null], ['consignee_id' => 0]);
        $this->addColumn($this->tableName, 'consignor_id', $this->integer()->after('basis_document_type_id'));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'consignor_id');
        $this->renameColumn($this->tableName, 'consignee_id', 'consignor_id');
    }
}
