<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190218_184207_create_ofd_taxcom_shift extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%ofd_taxcom_shift}}', [
            'id' => Schema::TYPE_PK,
            'kktRegNumber' => Schema::TYPE_STRING . '(21) NOT NULL',
            'fnFactoryNumber' => Schema::TYPE_STRING . '(21) NOT NULL',
            'shiftNumber' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'group' => Schema::TYPE_STRING . '(21) NOT NULL',
            'openDateTime' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'closeDateTime' => Schema::TYPE_INTEGER . '(11) NOT NULL',
            'total' => Schema::TYPE_INTEGER . '(11)',
            'cash' => Schema::TYPE_INTEGER . '(11)',
            'electronic' => Schema::TYPE_INTEGER . '(11)',
            'receiptCount' => Schema::TYPE_INTEGER . '(11)',
            'nds0Total' => Schema::TYPE_INTEGER . '(11)',
            'noNDSTotal' => Schema::TYPE_INTEGER . '(11)',
            'nds10' => Schema::TYPE_INTEGER . '(11)',
            'nds18' => Schema::TYPE_INTEGER . '(11)',
            'nds10_110' => Schema::TYPE_INTEGER . '(11)',
            'nds18_118' => Schema::TYPE_INTEGER . '(11)',
            'prepayment' => Schema::TYPE_INTEGER . '(11)',
            'credit' => Schema::TYPE_INTEGER . '(11)',
            'exchange' => Schema::TYPE_INTEGER . '(11)',
        ], $tableOptions);

        $this->createIndex('kktRegNumber', '{{%ofd_taxcom_shift}}', 'kktRegNumber');
        $this->createIndex('fnFactoryNumber', '{{%ofd_taxcom_shift}}', 'fnFactoryNumber');
        $this->createIndex('kktRegNumber_group', '{{%ofd_taxcom_shift}}', ['kktRegNumber', 'group']);
    }

    public function down()
    {
        $this->dropTable('{{%ofd_taxcom_shift}}');
    }
}
