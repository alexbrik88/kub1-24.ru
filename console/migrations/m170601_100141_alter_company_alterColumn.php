<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170601_100141_alter_company_alterColumn extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%company}}', 'pdf_send_signed', $this->smallInteger(1)->notNull()->defaultValue(true));
        $this->update('{{%company}}', ['pdf_send_signed' => true]);
    }
    
    public function safeDown()
    {
        $this->alterColumn('{{%company}}', 'pdf_send_signed', $this->smallInteger(1)->notNull()->defaultValue(false));
    }
}


