<?php

use yii\db\Schema;
use yii\db\Migration;

class m150708_141122_alter_paymentOrder_changeColumn_document_date_budget_payment extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('payment_order', 'document_date_budget_payment', Schema::TYPE_DATE);
    }
    
    public function safeDown()
    {
        $this->alterColumn('payment_order', 'document_date_budget_payment', Schema::TYPE_DATETIME);
    }
}
