<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211230_111556_alter_user_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%user_config}}', 'invoice_expenditure_item', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn('{{%user_config}}', 'invoice_income_item', $this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%user_config}}', 'invoice_expenditure_item');
        $this->dropColumn('{{%user_config}}', 'invoice_income_item');
    }
}
