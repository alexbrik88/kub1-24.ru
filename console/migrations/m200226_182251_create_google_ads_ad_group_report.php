<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200226_182251_create_google_ads_ad_group_report extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%google_ads_ad_group}}', [
            'id' => $this->bigInteger(100)->unsigned()->notNull(),
            'campaign_id' => $this->bigInteger(100)->unsigned()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'status' => $this->string()->notNull(),
            'type' => $this->string()->notNull(),
        ]);

        $this->addPrimaryKey('id_campaign_company', '{{%google_ads_ad_group}}', ['id', 'campaign_id', 'company_id']);
        $this->addForeignKey('FK_google_ads_ad_group_to_company', 'google_ads_ad_group', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_google_ads_ad_group_to_google_ads_campaign', 'google_ads_ad_group', 'campaign_id', 'google_ads_campaign', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('{{%google_ads_ad_group_report}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'employee_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'date' => $this->date()->notNull(),
            'campaign_id' => $this->bigInteger(100)->unsigned()->notNull(),
            'ad_group_id' => $this->bigInteger(100)->unsigned()->notNull(),
            'impressions_count' => $this->integer()->notNull(),
            'clicks_count' => $this->integer()->notNull(),
            'ctr' => $this->float(2)->notNull(),
            'cost' => $this->float(2)->notNull(),
            'avg_cpc' => $this->float(2)->notNull(),
            'avg_page_views_count' => $this->float(2)->notNull(),
            'conversion_rate' => $this->float(2)->notNull(),
            'cost_per_conversion' => $this->float(2)->notNull(),
            'conversions_count' => $this->float(2)->notNull(),
        ]);

        $this->addForeignKey('FK_google_ads_ad_group_report_to_company', '{{%google_ads_ad_group_report}}', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_google_ads_ad_group_report_to_employee', '{{%google_ads_ad_group_report}}', 'employee_id', 'employee', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_google_ads_ad_group_report_to_google_ads_campaign', '{{%google_ads_ad_group_report}}', 'campaign_id', 'google_ads_campaign', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_google_ads_ad_group_report_to_google_ads_ad_group', '{{%google_ads_ad_group_report}}', 'ad_group_id', 'google_ads_ad_group', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('{{%google_ads_keyword}}', [
            'id' => $this->bigInteger(100)->unsigned()->notNull(),
            'campaign_id' => $this->bigInteger(100)->unsigned()->notNull(),
            'ad_group_id' => $this->bigInteger(100)->unsigned()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'status' => $this->string()->notNull(),
        ]);

        $this->addPrimaryKey('id_campaign_group_company', '{{%google_ads_keyword}}', ['id', 'campaign_id', 'ad_group_id', 'company_id']);
        $this->addForeignKey('FK_google_ads_keyword_to_company', 'google_ads_keyword', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_google_ads_keyword_to_google_ads_campaign', 'google_ads_keyword', 'campaign_id', 'google_ads_campaign', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_google_ads_keyword_to_google_ads_ad_group', 'google_ads_keyword', 'ad_group_id', 'google_ads_ad_group', 'id', 'CASCADE', 'CASCADE');

        $this->createTable('{{%google_ads_keyword_report}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'employee_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'date' => $this->date()->notNull(),
            'campaign_id' => $this->bigInteger(100)->unsigned()->notNull(),
            'ad_group_id' => $this->bigInteger(100)->unsigned()->notNull(),
            'keyword_id' => $this->bigInteger(100)->unsigned()->notNull(),
            'impressions_count' => $this->integer()->notNull(),
            'clicks_count' => $this->integer()->notNull(),
            'ctr' => $this->float(2)->notNull(),
            'cost' => $this->float(2)->notNull(),
            'avg_cpc' => $this->float(2)->notNull(),
            'avg_page_views_count' => $this->float(2)->notNull(),
            'conversion_rate' => $this->float(2)->notNull(),
            'cost_per_conversion' => $this->float(2)->notNull(),
            'conversions_count' => $this->float(2)->notNull(),
        ]);

        $this->addForeignKey('FK_google_ads_keyword_report_to_company', '{{%google_ads_keyword_report}}', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_google_ads_keyword_report_to_employee', '{{%google_ads_keyword_report}}', 'employee_id', 'employee', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_google_ads_keyword_report_to_google_ads_campaign', '{{%google_ads_keyword_report}}', 'campaign_id', 'google_ads_campaign', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_google_ads_keyword_report_to_google_ads_ad_group', '{{%google_ads_keyword_report}}', 'ad_group_id', 'google_ads_ad_group', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_google_ads_keyword_report_to_google_ads_keyword', '{{%google_ads_keyword_report}}', 'keyword_id', 'google_ads_keyword', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable('{{%google_ads_keyword_report}}');
        $this->dropTable('{{%google_ads_keyword}}');
        $this->dropTable('{{%google_ads_ad_group_report}}');
        $this->dropTable('{{%google_ads_ad_group}}');
    }
}
