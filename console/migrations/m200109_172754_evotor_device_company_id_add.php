<?php

use console\components\db\Migration;

class m200109_172754_evotor_device_company_id_add extends Migration
{
    const TABLE = '{{%evotor_device}}';

    public function up()
    {
        $this->addColumn(self::TABLE, 'company_id', $this->integer(11)->notNull()->after('id')->comment('Идентификатор компании КУБ'));
    }

    public function down()
    {
        $this->dropColumn(self::TABLE, 'company_id');
    }
}
