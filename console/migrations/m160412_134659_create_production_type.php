<?php

use yii\db\Migration;

class m160412_134659_create_production_type extends Migration
{
    public $tProductionType = 'production_type';

    public function up()
    {
        $this->createTable($this->tProductionType, [
            'id' => $this->primaryKey(),
            'name' => $this->text(),
            'object_guid' => $this->string(36),
        ]);

        $this->batchInsert($this->tProductionType, ['id', 'name', 'object_guid'], [
            [1,'Услуга', '39362A62-9F92-CDA7-FC9E-A9A06C5D1116'],
            [2, 'Товар', 'EA83276F-9BFF-CDEC-4356-294871C343B1'],
        ]);
    }

    public function down()
    {
        $this->dropTable('production_type');
    }
}
