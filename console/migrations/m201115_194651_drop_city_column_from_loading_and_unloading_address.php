<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201115_194651_drop_city_column_from_loading_and_unloading_address extends Migration
{
    public $tableName = 'loading_and_unloading_address';

    public function safeUp()
    {
        $this->dropColumn($this->tableName, 'city');
    }

    public function safeDown()
    {
        $this->addColumn($this->tableName, 'city', $this->string()->notNull());
    }
}
