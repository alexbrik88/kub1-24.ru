<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201204_120319_alter_user_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%user_config}}', 'theme_wide', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn('{{%user_config}}', 'minimize_side_menu', $this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%user_config}}', 'theme_wide');
        $this->dropColumn('{{%user_config}}', 'minimize_side_menu');
    }
}
