<?php

use yii\db\Migration;

/**
 * Handles the creation of table `session_frontend`.
 */
class m190218_105735_create_session_frontend_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('{{%session_frontend}}', [
            'id' => $this->char(40)->notNull(),
            'employee_id' => $this->integer(),
            'last' => $this->integer(),
            'expire' => $this->integer(),
            'data' => 'LONGBLOB',
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', '{{%session_frontend}}', 'id');
        $this->addForeignKey('FK_session_employee', '{{%session_frontend}}', 'employee_id', '{{%employee}}', 'id');
        $this->createIndex('session_last_index', '{{%session_frontend}}', 'last');
        $this->createIndex('session_expire_index', '{{%session_frontend}}', 'expire');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('session_frontend');
    }
}
