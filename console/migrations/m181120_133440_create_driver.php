<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181120_133440_create_driver extends Migration
{
    public $tDriver = 'driver';
    public $tIdentificationType = 'identification_type';
    public $tPhoneType = 'phone_type';
    public $tDriverPhone = 'driver_phone';

    public function safeUp()
    {
        $this->createTable($this->tDriver, [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'author_id' => $this->integer()->notNull(),
            'first_name' => $this->string()->notNull(),
            'last_name' => $this->string()->notNull(),
            'patronymic' => $this->string()->defaultValue(null),
            'contractor_id' => $this->integer()->notNull(),
            'vehicle_id' => $this->integer()->defaultValue(null),
            'birthday_date' => $this->date()->defaultValue(null),
            'identification_type_id' => $this->integer()->notNull(),
            'identification_series' => $this->string()->notNull(),
            'identification_number' => $this->string()->notNull(),
            'identification_date' => $this->date()->notNull(),
            'identification_issued_by' => $this->string()->notNull(),
            'driver_license_series' => $this->string()->notNull(),
            'driver_license_number' => $this->string()->notNull(),
            'driver_license_date' => $this->date()->notNull(),
            'driver_license_issued_by' => $this->string()->notNull(),
            'home_town' => $this->string()->notNull(),
            'residential_address' => $this->string()->notNull(),
            'registration_address' => $this->string()->notNull(),
            'comment' => $this->text()->defaultValue(null),
        ]);

        $this->createTable($this->tIdentificationType, [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);

        $this->batchInsert($this->tIdentificationType, ['id', 'name'], [
            [1, 'Военный билет'],
            [2, 'Временное удостоверение личности гражданина РФ'],
            [3, 'Загранпаспорт'],
            [4, 'Иностранный паспорт'],
            [5, 'Иные документы, выдаваемые органами МВД'],
            [6, 'Паспорт'],
            [7, 'Свидетельство о регистрации ходатайства иммигранта о признании его беженцем'],
            [8, 'Свидетельство о рождении, выданное уполномоченным органом иностранного государства'],
            [9, 'Справка об освобождении'],
            [10, 'Удостоверение беженца'],
        ]);

        $this->addForeignKey($this->tDriver . '_company_id', $this->tDriver, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tDriver . '_author_id', $this->tDriver, 'author_id', 'employee', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tDriver . '_contractor_id', $this->tDriver, 'contractor_id', 'contractor', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tDriver . '_identification_type_id', $this->tDriver, 'identification_type_id', $this->tIdentificationType, 'id', 'RESTRICT', 'CASCADE');

        $this->createTable($this->tPhoneType, [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);

        $this->batchInsert($this->tPhoneType, ['id', 'name'], [
            [1, 'Сотовый'],
            [2, 'Домашний'],
            [3, 'Рабочий'],
        ]);

        $this->createTable($this->tDriverPhone, [
            'id' => $this->primaryKey(),
            'driver_id' => $this->integer()->notNull(),
            'phone_type_id' => $this->integer()->notNull(),
            'phone' => $this->string()->notNull(),
            'is_main' => $this->boolean()->defaultValue(false),
        ]);

        $this->addForeignKey($this->tDriverPhone . '_driver_id', $this->tDriverPhone, 'driver_id', $this->tDriver, 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tDriverPhone . '_phone_type_id', $this->tDriverPhone, 'phone_type_id', $this->tPhoneType, 'id', 'RESTRICT', 'CASCADE');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey($this->tDriver . '_company_id', $this->tDriver);
        $this->dropForeignKey($this->tDriver . '_author_id', $this->tDriver);
        $this->dropForeignKey($this->tDriver . '_contractor_id', $this->tDriver);
        $this->dropForeignKey($this->tDriver . '_identification_type_id', $this->tDriver);

        $this->dropForeignKey($this->tDriverPhone . '_driver_id', $this->tDriverPhone);
        $this->dropForeignKey($this->tDriverPhone . '_phone_type_id', $this->tDriverPhone);

        $this->dropTable($this->tIdentificationType);
        $this->dropTable($this->tDriver);

        $this->dropTable($this->tDriverPhone);
        $this->dropTable($this->tPhoneType);
    }
}


