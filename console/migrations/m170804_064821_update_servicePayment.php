<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170804_064821_update_servicePayment extends Migration
{
    public function safeUp()
    {
        $this->execute("
			UPDATE {{%service_payment}} t1
			LEFT JOIN {{%service_payment}} t2
				ON t2.company_id = t1.company_id AND t2.created_at < t1.created_at AND t2.tariff_id IN(1,2,3) AND t2.is_confirmed = 1
			SET t1.by_novice = IF(t2.created_at IS NULL, 1, 0)
        ");
    }
    
    public function safeDown()
    {
    	echo "This migration not rolled back\n";
    }
}
