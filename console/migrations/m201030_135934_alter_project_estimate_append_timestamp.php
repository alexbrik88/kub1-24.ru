<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201030_135934_alter_project_estimate_append_timestamp extends Migration
{
    public $tableName = 'project_estimate';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'created_at', $this->integer(11));
        $this->addColumn($this->tableName, 'updated_at', $this->integer(11));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'created_at');
        $this->dropColumn($this->tableName, 'updated_at');
    }

}
