<?php

use yii\db\Schema;
use yii\db\Migration;

class m150424_091309_alter_Order_changeColumn_CustomDeclarationNumber extends Migration
{
    public function up()
    {
        $this->alterColumn('order', 'custom_declaration_number', Schema::TYPE_STRING . '(45) NULL');
    }

    public function down()
    {
        $this->alterColumn('order', 'custom_declaration_number', Schema::TYPE_STRING . '(45) NOT NULL');
    }
}
