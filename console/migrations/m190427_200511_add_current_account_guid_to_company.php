<?php

use console\components\db\Migration;
use yii\db\Schema;
use frontend\modules\export\models\one_c\OneCExport;

class m190427_200511_add_current_account_guid_to_company extends Migration
{
    public $tableName = 'company';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'current_account_guid', $this->string(255)->notNull()->defaultValue('')->after('object_guid'));

        foreach ((new \yii\db\Query())->select(['id'])->from($this->tableName)->orderBy('id')->batch(50) as $contractorIds) {
            foreach ($contractorIds as $contractorId) {
                Yii::$app->db->createCommand()->update($this->tableName, [
                    'current_account_guid' => OneCExport::generateGUID()
                ], 'id = :id', [':id' => $contractorId['id']])->execute();
            }
        };
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'current_account_guid');
    }
}
