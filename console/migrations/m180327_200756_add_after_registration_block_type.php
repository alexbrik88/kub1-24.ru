<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180327_200756_add_after_registration_block_type extends Migration
{
    public $tableName = 'company';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'after_registration_block_type', $this->integer()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'after_registration_block_type');
    }
}


