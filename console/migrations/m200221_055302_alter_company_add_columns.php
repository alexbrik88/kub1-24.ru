<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200221_055302_alter_company_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%company}}', 'company_activity_id', $this->integer());
        $this->addColumn('{{%company}}', 'employee_count_id', $this->integer());


        $this->addForeignKey('FK_company_company_activity', '{{%company}}', 'company_activity_id', '{{%company_activity}}', 'id');
        $this->addForeignKey('FK_company_employee_count', '{{%company}}', 'employee_count_id', '{{%employee_count}}', 'id');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%company}}', 'company_activity_id');
        $this->dropColumn('{{%company}}', 'employee_count_id');
    }
}
