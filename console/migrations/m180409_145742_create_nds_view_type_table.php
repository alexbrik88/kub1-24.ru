<?php

use yii\db\Migration;

/**
 * Handles the creation of table `nds_view_type`.
 */
class m180409_145742_create_nds_view_type_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('nds_view_type', [
            'id' => $this->integer(1)->notNull(),
            'name' => $this->string(24),
            'title' => $this->string(24),
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', 'nds_view_type', 'id');

        $this->batchInsert('nds_view_type', ['id', 'name', 'title'], [
            [0, 'В том числе НДС', 'В том числе НДС'],
            [1, 'НДС', 'НДС сверху'],
            [2, 'Без налога (НДС)', 'Без налога (НДС)'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('nds_view_type');
    }
}
