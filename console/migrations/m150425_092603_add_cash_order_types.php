<?php

use yii\db\Schema;
use yii\db\Migration;

class m150425_092603_add_cash_order_types extends Migration
{
    public function safeUp()
    {
        $this->createTable('cash_orders_reasons_types', [
            'id'   => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ]);

        $this->batchInsert('cash_orders_reasons_types', ['name'], [
            ['Сдача наличных в банк'],
            ['Выдача наличных под отчёт'],
            ['Выдача заработной платы'],
            ['Выручка'],
            ['Розничная выручка'],
            ['Возврат денег от клиента'],
            ['Возврат денег из-под отчёта'],
            ['Прочее'],
        ]);

        $this->createTable('cash_operations_types', [
            'id'   => Schema::TYPE_PK,
            'code' => Schema::TYPE_INTEGER . ' NOT NULL',
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ]);

        $this->batchInsert('cash_operations_types', ['code', 'name'], [
            [1,  '01 - платёжное поручение'],
            [2,  '02 - платежное требование'],
            [6,  '06 - инкассовое поручение'],
            [16, '16 - платежный ордер'],
        ]);
    }
    
    public function safeDown()
    {
        $this->dropTable('cash_orders_reasons_types');
        $this->dropTable('cash_operations_types');
    }
}
