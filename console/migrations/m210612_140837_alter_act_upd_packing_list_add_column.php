<?php

use common\models\document\Invoice;
use console\components\db\Migration;
use yii\db\Schema;

class m210612_140837_alter_act_upd_packing_list_add_column extends Migration
{
    public function up()
    {
        $this->addColumn('act', 'remaining_amount', $this->bigInteger()->defaultValue(0));
        $this->addColumn('upd', 'remaining_amount', $this->bigInteger()->defaultValue(0));
        $this->addColumn('packing_list', 'remaining_amount', $this->bigInteger()->defaultValue(0));

        $this->_refresh();
    }

    public function down()
    {
        $this->dropColumn('act', 'remaining_amount');
        $this->dropColumn('upd', 'remaining_amount');
        $this->dropColumn('packing_list', 'remaining_amount');
    }

    private function _refresh()
    {
        /* @var $invoice Invoice */
        $invoice = new Invoice;

        if (method_exists($invoice, 'updateDocumentsPaid')) {
            try {
                $unpaidInvoicesIds = Invoice::find()
                    ->byDeleted()
                    ->andWhere(['>', 'remaining_amount', 0])
                    ->select('id')
                    ->column();

                echo "\n";
                foreach ($unpaidInvoicesIds as $i => $id) {
                    if ($invoice = Invoice::findOne($id)) {
                        $invoice->updateDocumentsPaid();

                        if ($i % 1000 === 0)
                            echo "{$i}\n";
                    }
                }
            } catch (\Throwable $e) {
                echo "no payment invoices refreshed!" . "\r\n";
                echo "error: " . $e->getMessage() . "\r\n";
            }
            if (isset($i))
                echo "{$i}\n";

        } else {

            echo "\n\nUpdate skipped!\n\n";
        }
    }
}
