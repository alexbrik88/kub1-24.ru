<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181113_111655_create_emloyee_click extends Migration
{
    public $tableName = 'employee_click';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'employee_id' => $this->integer()->notNull(),
            'click_type' => $this->integer()->notNull(),
            'click_date' => $this->date()->notNull(),
        ]);

        $this->addForeignKey($this->tableName . '_employee_id', $this->tableName, 'employee_id', 'employee', 'id', 'RESTRICT', 'CASCADE');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_employee_id', $this->tableName);

        $this->dropTable($this->tableName);
    }
}


