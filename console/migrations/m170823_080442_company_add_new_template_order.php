<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170823_080442_company_add_new_template_order extends Migration
{
    public $tableName = 'company';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'new_template_order', $this->smallInteger(1)->defaultValue(0));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'new_template_order');
    }
}


