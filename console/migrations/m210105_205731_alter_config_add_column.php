<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210105_205731_alter_config_add_column extends Migration
{
    public $table = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->table, 'cash_column_wallet', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'cash_column_wallet');
    }
}
