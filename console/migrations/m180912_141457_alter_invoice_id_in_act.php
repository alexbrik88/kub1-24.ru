<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180912_141457_alter_invoice_id_in_act extends Migration
{
    public $tableName = 'act';

    public function safeUp()
    {
        $this->dropForeignKey('fk_act_to_invoice', $this->tableName);
        $this->alterColumn($this->tableName, 'invoice_id', $this->integer()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->alterColumn($this->tableName, 'invoice_id', $this->integer()->notNull());
        $this->addForeignKey('fk_act_to_invoice', $this->tableName, 'invoice_id', 'invoice', 'id', 'RESTRICT', 'CASCADE');
    }
}


