<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201202_091217_alter_finance_model extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('finance_model', 'total_net_profit', $this->bigInteger()->defaultValue(0));
        $this->alterColumn('finance_model', 'total_revenue', $this->bigInteger()->defaultValue(0));
        $this->renameColumn('finance_model', 'total_margin', 'total_marginality');
        $this->addColumn('finance_model', 'total_average_check', $this->bigInteger()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('finance_model', 'total_average_check');
        $this->renameColumn('finance_model', 'total_marginality', 'total_margin');
        $this->alterColumn('finance_model', 'total_revenue', $this->integer()->defaultValue(0));
        $this->alterColumn('finance_model', 'total_net_profit', $this->integer()->defaultValue(0));
    }
}
