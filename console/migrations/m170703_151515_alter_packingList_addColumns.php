<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170703_151515_alter_packingList_addColumns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{packing_list}}', 'proxy_number', $this->string(50) . ' AFTER [[signature_id]]');
        $this->addColumn('{{packing_list}}', 'proxy_date', $this->date() . ' AFTER [[proxy_number]]');
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{packing_list}}', 'proxy_number');
        $this->dropColumn('{{packing_list}}', 'proxy_date');
    }
}
