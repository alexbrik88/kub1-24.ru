<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170608_223308_update_uploaded_documents extends Migration
{
    public $tableName = 'uploaded_documents';

    public function safeUp()
    {
        $this->dropColumn($this->tableName, 'chief_firstname');
        $this->dropColumn($this->tableName, 'chief_lastname');
        $this->dropColumn($this->tableName, 'chief_patronymic');

        $this->addColumn($this->tableName, 'chief_name', $this->string()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->addColumn($this->tableName, 'chief_firstname', $this->string()->defaultValue(null));
        $this->addColumn($this->tableName, 'chief_lastname', $this->string()->defaultValue(null));
        $this->addColumn($this->tableName, 'chief_patronymic', $this->string()->defaultValue(null));

        $this->dropColumn($this->tableName, 'chief_name');
    }
}


