<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%crm_client_wish}}`.
 */
class m211011_084122_create_crm_client_wish_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%crm_client_wish}}', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'contractor_id' => $this->integer()->notNull(),
            'number' => $this->integer()->notNull(),
            'responsible_employee_id' => $this->integer()->notNull(),
            'status_id' => $this->tinyInteger(1)->notNull(),
            'date' => $this->date()->notNull(),
            'text' => $this->text()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%crm_client_wish}}');
    }
}
