<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180518_093809_add_new_columns_to_service_payment_promo_code extends Migration
{
    public $tableName = 'service_payment_promo_code';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'type', $this->integer()->defaultValue(1));
        $this->addColumn($this->tableName, 'cabinets_count', $this->integer()->defaultValue(null));
        $this->addColumn($this->tableName, 'out_invoice_count', $this->integer()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'type');
        $this->dropColumn($this->tableName, 'cabinets_count');
        $this->dropColumn($this->tableName, 'out_invoice_count');
    }
}


