<?php

use yii\db\Schema;
use yii\db\Migration;

class m150512_131123_alter_payment_order extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('payment_order', 'document_date', Schema::TYPE_DATE);
    }
    
    public function safeDown()
    {
        $this->alterColumn('payment_order', 'document_date', Schema::TYPE_DATETIME);
    }
}
