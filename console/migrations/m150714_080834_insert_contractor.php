<?php

use common\models\Company;
use yii\db\Schema;
use yii\db\Migration;

class m150714_080834_insert_contractor extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('contractor', 'type', 'TINYINT NOT NULL COMMENT "1 - поставщик, 2 - покупатель, 3 - учредитель"');

        $this->alterColumn('contractor', 'employee_id', Schema::TYPE_INTEGER . ' DEFAULT NULL');

        /* @var Company[] $companyArray */
        $companyArray = (new \yii\db\Query())->select(['id', 'company_type_id'])->from('company')->all();
        foreach ($companyArray as $company) {
            $this->insert('contractor', [
                'company_id' => $company['id'],
                'type' => '3', // учредитель
                'status' => '1', // active
                'company_type_id' => $company['company_type_id'],
                'name' => 'Учредитель',
                'created_at' => time(),
                'taxation_system' => '0',
                'is_deleted' => '0',
                'employee_id' => null,
                'director_name' => '',
                'BIN' => '',
                'ITN' => '',
                'PPC' => '',
                'BIC' => '',
                'taxation_system' => 0,
                'current_account' => '',
                'corresp_account' => '',
                'chief_accountant_is_director' => false,
                'contact_is_director' => false,
            ]);
        }
    }

    public function safeDown()
    {
        $this->delete('contractor', 'type = 3');

        $this->alterColumn('contractor', 'type', Schema::TYPE_INTEGER . ' NOT NULL COMMENT "0 - поставщик, 1 - покупатель"');

        $this->alterColumn('contractor', 'employee_id', Schema::TYPE_INTEGER . ' NOT NULL');
    }
}
