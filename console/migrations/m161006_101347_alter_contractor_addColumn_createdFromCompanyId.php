<?php

use console\components\db\Migration;
use yii\db\Schema;

class m161006_101347_alter_contractor_addColumn_createdFromCompanyId extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%contractor}}', 'created_from_company_id', $this->integer()->null());
        $this->addForeignKey('fk_contractor_from_company_to_company', '{{%contractor}}', 'created_from_company_id', '{{%company}}', 'id');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('fk_contractor_from_company_to_company', '{{%contractor}}');
        $this->dropColumn('{{%contractor}}', 'created_from_company_id');
    }
}


