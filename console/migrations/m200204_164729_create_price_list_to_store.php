<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200204_164729_create_price_list_to_store extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%price_list_to_store}}', [
            'price_list_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'store_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ], $tableOptions);

        $this->addPrimaryKey('PRIMARY_KEY', '{{%price_list_to_store}}', ['price_list_id', 'store_id']);
        $this->addForeignKey('fk_price_list_to_store_price', '{{%price_list_to_store}}', 'price_list_id', '{{%price_list}}', 'id', 'CASCADE');
        $this->addForeignKey('fk_price_list_to_store_store', '{{%price_list_to_store}}', 'store_id', '{{%store}}', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_price_list_to_store_price', '{{%price_list_to_store}}');
        $this->dropForeignKey('fk_price_list_to_store_store', '{{%price_list_to_store}}');
        $this->dropTable('{{%price_list_to_store}}');
    }
}
