<?php

use yii\db\Migration;

class m200914_151829_create_credit_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%credit}}';

    /**
     * @inheritDoc
     */
    public function up()
    {
        $this->createTable(self::TABLE_NAME, [
            'credit_id' => $this->bigPrimaryKey(),
            'credit_type' => $this->tinyInteger()->notNull(),
            'credit_status' => $this->tinyInteger()->notNull()->defaultValue(1),
            'credit_amount' => $this->double(2)->notNull(),
            'credit_first_date' => $this->date()->notNull(),
            'credit_last_date' => $this->date()->notNull(),
            'credit_day_count' => $this->integer()->notNull()->defaultValue(0),
            'credit_percent_rate' => $this->double(2)->notNull(),
            'credit_commission_rate' => $this->double(2)->notNull()->defaultValue(0),
            'credit_expiration_rate' => $this->double(2)->notNull()->defaultValue(0),
            'credit_year_length' => $this->smallInteger()->notNull()->defaultValue(0),
            'credit_tranche_depth' => $this->smallInteger()->null(),
            'payment_type' => $this->smallInteger()->notNull(),
            'payment_mode' => $this->tinyInteger()->notNull(),
            'payment_day' => $this->tinyInteger()->notNull()->defaultValue(0),
            'payment_first' => $this->tinyInteger()->notNull()->defaultValue(0),
            'payment_amount' => $this->double(2)->notNull()->defaultValue(0),
            'company_id' => $this->integer()->notNull(),
            'employee_id' => $this->integer()->notNull(),
            'checking_accountant_id' => $this->integer()->notNull(),
            'cashbox_id' => $this->integer()->notNull(),
            'agreement_number' => $this->bigInteger(),
            'agreement_name' => $this->string(64)->notNull()->defaultValue(''),
            'agreement_date' => $this->date()->notNull(),
            'debt_amount' => $this->double(2)->notNull()->defaultValue(0),
            'debt_repaid' => $this->double(2)->notNull()->defaultValue(0),
            'debt_interest' => $this->double(2)->notNull()->defaultValue(0),
            'debt_diff' => $this->double(2)->notNull()->defaultValue(0),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
        ]);

        $this->createForeignKey(['company_id'], 'company', ['id']);
        $this->createForeignKey(['employee_id'], 'employee', ['id']);
        $this->createForeignKey(['cashbox_id'], 'cashbox', ['id']);
        $this->createForeignKey(['checking_accountant_id'], 'checking_accountant', ['id']);
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        $this->dropTable(self::TABLE_NAME);
    }

    /**
     * Создать внешний ключ
     *
     * @param string[] $columns
     * @param string $foreignTable
     * @param string[] $foreignColumns
     * @return void
     */
    private function createForeignKey(array $columns, string $foreignTable, array $foreignColumns): void
    {
        $this->createIndex('ck_' . implode('_', $columns), self::TABLE_NAME, $columns);
        $this->addForeignKey(
            sprintf('fk_credit_%s__%s_%s', implode('_', $columns), $foreignTable, implode('_', $foreignColumns)),
            self::TABLE_NAME,
            $columns,
            '{{%' . $foreignTable . '}}',
            $foreignColumns,
            'CASCADE',
            'CASCADE'
        );
    }
}
