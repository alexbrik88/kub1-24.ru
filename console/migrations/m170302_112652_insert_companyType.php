<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170302_112652_insert_companyType extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%company_type}}', [
            'id' => 28,
            'name_short' => 'ЖСК',
            'name_full' => 'Жилищно-строительный кооператив',
            'in_company' => false,
            'in_contractor' => true,
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%company_type}}', ['id' => 28]);
    }
}


