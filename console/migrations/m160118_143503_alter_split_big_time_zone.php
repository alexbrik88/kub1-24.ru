<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160118_143503_alter_split_big_time_zone extends Migration
{
    public $tTimeZone = 'time_zone';

    public function safeUp()
    {
        $this->dropIndex('time_zone', $this->tTimeZone);

        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC-07:00) Ла-Пас'], ['out_time_zone' => '(UTC-07:00) Ла-Пас, Мазатлан, Чихуахуа']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC-06:00) Гвадалахара'], ['out_time_zone' => '(UTC-06:00) Гвадалахара, Мехико, Монтеррей']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC-05:00) Богота'], ['out_time_zone' => '(UTC-05:00) Богота, Кито, Лима, Рио-Бранко']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC-04:00) Джорджтаун'], ['out_time_zone' => '(UTC-04:00) Джорджтаун, Ла-Пас, Манаус, Сан-Хуан']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC-03:00) Кайенна'], ['out_time_zone' => '(UTC-03:00) Кайенна, Форталеза']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC) Дублин'], ['out_time_zone' => '(UTC) Дублин, Лиссабон, Лондон, Эдинбург']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC) Монровия'], ['out_time_zone' => '(UTC) Монровия, Рейкьявик']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+01:00) Амстердам'], ['out_time_zone' => '(UTC+01:00) Амстердам, Берлин, Берн, Вена, Рим, Стокгольм']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+01:00) Белград'], ['out_time_zone' => '(UTC+01:00) Белград, Братислава, Будапешт, Любляна, Прага']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+01:00) Брюссель'], ['out_time_zone' => '(UTC+01:00) Брюссель, Копенгаген, Мадрид, Париж']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+01:00) Варшава'], ['out_time_zone' => '(UTC+01:00) Варшава, Загреб, Сараево, Скопье']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+02:00) Афины'], ['out_time_zone' => '(UTC+02:00) Афины, Бухарест']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+02:00) Вильнюс'], ['out_time_zone' => '(UTC+02:00) Вильнюс, Киев, Рига, София, Таллин, Хельсинки']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+02:00) Хараре'], ['out_time_zone' => '(UTC+02:00) Хараре, Претория']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+03:00) Москва'], ['out_time_zone' => '(UTC+03:00) Волгоград, Москва, Санкт-Петербург (RTZ 2)']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+04:00) Абу-Даби'], ['out_time_zone' => '(UTC+04:00) Абу-Даби, Мускат']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+04:00) Ижевск'], ['out_time_zone' => '(UTC+04:00) Ижевск, Самара (RTZ 3)']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+05:00) Ашхабад'], ['out_time_zone' => '(UTC+05:00) Ашхабад, Ташкент']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+05:00) Исламабад'], ['out_time_zone' => '(UTC+05:00) Исламабад, Карачи']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+05:30) Колката'], ['out_time_zone' => '(UTC+05:30) Колката, Мумбаи, Нью-Дели, Ченнай']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+07:00) Бангкок'], ['out_time_zone' => '(UTC+07:00) Бангкок, Джакарта, Ханой']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+08:00) Гонконг'], ['out_time_zone' => '(UTC+08:00) Гонконг, Пекин, Урумчи, Чунцин']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+08:00) Куала-Лумпур'], ['out_time_zone' => '(UTC+08:00) Куала-Лумпур, Сингапур']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+09:00) Осака'], ['out_time_zone' => '(UTC+09:00) Осака, Саппоро, Токио']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+10:00) Владивосток'], ['out_time_zone' => '(UTC+10:00) Владивосток, Магадан (RTZ 9)']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+10:00) Гуам'], ['out_time_zone' => '(UTC+10:00) Гуам, Порт-Морсби']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+10:00) Канберра'], ['out_time_zone' => '(UTC+10:00) Канберра, Мельбурн, Сидней']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+11:00) Соломоновы о-ва'], ['out_time_zone' => '(UTC+11:00) Соломоновы о-ва, Нов. Каледония']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+12:00) Анадырь'], ['out_time_zone' => '(UTC+12:00) Анадырь, Петропаловск-Камчатский (RTZ 11)']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+12:00) Веллигтон'], ['out_time_zone' => '(UTC+12:00) Веллигтон, Окленд']);

        $this->batchInsert($this->tTimeZone, ['id', 'out_time_zone', 'time_zone'], [
            [108, '(UTC-07:00) Мазатлан', 'America/Mazatlan'],
            [109, '(UTC-07:00) Чихуахуа', 'America/Mazatlan'],

            [110, '(UTC-06:00) Мехико', 'America/Mexico_City'],
            [111, '(UTC-06:00) Монтеррей', 'America/Mexico_City'],

            [112, '(UTC-05:00) Кито', 'America/Bogota'],
            [113, '(UTC-05:00) Лима', 'America/Bogota'],
            [114, '(UTC-05:00) Рио-Бранко', 'America/Bogota'],

            [115, '(UTC-04:00) Ла-Пас', 'America/Manaus'],
            [116, '(UTC-04:00) Манаус', 'America/Manaus'],
            [117, '(UTC-04:00) Сан-Хуан', 'America/Manaus'],

            [118, '(UTC-03:00) Форталеза', 'America/Cayenne'],

            [119, '(UTC) Лиссабон', 'Europe/Dublin'],
            [120, '(UTC) Лондон', 'Europe/Dublin'],
            [121, '(UTC) Эдинбург', 'Europe/Dublin'],

            [122, '(UTC) Рейкьявик', 'Africa/Monrovia'],

            [123, '(UTC+01:00) Берлин', 'Europe/Amsterdam'],
            [124, '(UTC+01:00) Берн', 'Europe/Amsterdam'],
            [125, '(UTC+01:00) Вена', 'Europe/Amsterdam'],
            [126, '(UTC+01:00) Рим', 'Europe/Amsterdam'],
            [127, '(UTC+01:00) Стокгольм', 'Europe/Amsterdam'],

            [128, '(UTC+01:00) Братислава', 'Europe/Belgrade'],
            [129, '(UTC+01:00) Будапешт', 'Europe/Belgrade'],
            [130, '(UTC+01:00) Любляна', 'Europe/Belgrade'],
            [131, '(UTC+01:00) Прага', 'Europe/Belgrade'],

            [132, '(UTC+01:00) Копенгаген', 'Europe/Brussels'],
            [133, '(UTC+01:00) Мадрид', 'Europe/Brussels'],
            [134, '(UTC+01:00) Париж', 'Europe/Brussels'],

            [135, '(UTC+01:00) Загреб', 'Europe/Warsaw'],
            [136, '(UTC+01:00) Сараево', 'Europe/Warsaw'],
            [137, '(UTC+01:00) Скопье', 'Europe/Warsaw'],

            [138, '(UTC+02:00) Бухарест', 'Europe/Athens'],

            [139, '(UTC+02:00) Киев', 'Europe/Vilnius'],
            [140, '(UTC+02:00) Рига', 'Europe/Vilnius'],
            [141, '(UTC+02:00) София', 'Europe/Vilnius'],
            [142, '(UTC+02:00) Таллин', 'Europe/Vilnius'],
            [143, '(UTC+02:00) Хельсинки', 'Europe/Vilnius'],

            [144, '(UTC+02:00) Претория', 'Africa/Harare'],

            [145, '(UTC+03:00) Волгоград', 'Europe/Moscow'],
            [146, '(UTC+03:00) Санкт-Петербург', 'Europe/Moscow'],

            [147, '(UTC+04:00) Мускат', 'Africa/Addis_Ababa'],

            [148, '(UTC+04:00) Самара', 'Europe/Samara'],

            [149, '(UTC+05:00) Ташкент', 'Asia/Ashgabat'],

            [150, '(UTC+05:00) Карачи', 'Asia/Karachi'],

            [151, '(UTC+05:30) Мумбаи', 'Asia/Kolkata'],
            [152, '(UTC+05:30) Нью-Дели', 'Asia/Kolkata'],
            [153, '(UTC+05:30) Ченнай', 'Asia/Kolkata'],

            [154, '(UTC+07:00) Джакарта', 'Asia/Bangkok'],
            [155, '(UTC+07:00) Ханой', 'Asia/Bangkok'],

            [156, '(UTC+08:00) Пекин', 'Asia/Hong_Kong'],
            [157, '(UTC+08:00) Урумчи', 'Asia/Hong_Kong'],
            [158, '(UTC+08:00) Чунцин', 'Asia/Hong_Kong'],

            [159, '(UTC+08:00) Сингапур', 'Asia/Singapore'],

            [160, '(UTC+09:00) Саппоро', 'Asia/Tokyo'],
            [161, '(UTC+09:00) Токио', 'Asia/Tokyo'],

            [163, '(UTC+10:00) Порт-Морсби', 'Pacific/Guam'],

            [164, '(UTC+10:00) Мельбурн', 'Australia/Canberra'],
            [165, '(UTC+10:00) Сидней', 'Australia/Canberra'],

            [166, '(UTC+11:00) Нов. Каледония', 'Pacific/Guadalcanal'],

            [167, '(UTC+12:00) Петропаловск-Камчатский', 'Asia/Anadyr'],

            [168, '(UTC+12:00) Окленд', 'Pacific/Auckland'],

        ]);
    }
    
    public function safeDown()
    {
        $splitTimeZones = range(108, 168);

        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC-07:00) Ла-Пас, Мазатлан, Чихуахуа'], ['out_time_zone' => '(UTC-07:00) Ла-Пас']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC-06:00) Гвадалахара, Мехико, Монтеррей'], ['out_time_zone' => '(UTC-06:00) Гвадалахара']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC-05:00) Богота, Кито, Лима, Рио-Бранко'], ['out_time_zone' => '(UTC-05:00) Богота']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC-04:00) Джорджтаун, Ла-Пас, Манаус, Сан-Хуан'], ['out_time_zone' => '(UTC-04:00) Джорджтаун']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC-03:00) Кайенна, Форталеза'], ['out_time_zone' => '(UTC-03:00) Кайенна']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC) Дублин, Лиссабон, Лондон, Эдинбург'], ['out_time_zone' => '(UTC) Дублин']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC) Монровия, Рейкьявик'], ['out_time_zone' => '(UTC) Монровия']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+01:00) Амстердам, Берлин, Берн, Вена, Рим, Стокгольм'], ['out_time_zone' => '(UTC+01:00) Амстердам']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+01:00) Белград, Братислава, Будапешт, Любляна, Прага'], ['out_time_zone' => '(UTC+01:00) Белград']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+01:00) Брюссель, Копенгаген, Мадрид, Париж'], ['out_time_zone' => '(UTC+01:00) Брюссель']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+01:00) Варшава, Загреб, Сараево, Скопье'], ['out_time_zone' => '(UTC+01:00) Варшава']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+02:00) Афины, Бухарест'], ['out_time_zone' => '(UTC+02:00) Афины']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+02:00) Вильнюс, Киев, Рига, София, Таллин, Хельсинки'], ['out_time_zone' => '(UTC+02:00) Вильнюс']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+02:00) Хараре, Претория'], ['out_time_zone' => '(UTC+02:00) Хараре']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+03:00) Волгоград, Москва, Санкт-Петербург (RTZ 2)'], ['out_time_zone' => '(UTC+03:00) Москва']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+04:00) Абу-Даби, Мускат'], ['out_time_zone' => '(UTC+04:00) Абу-Даби']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+04:00) Ижевск, Самара (RTZ 3)'], ['out_time_zone' => '(UTC+04:00) Ижевск']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+05:00) Ашхабад, Ташкент'], ['out_time_zone' => '(UTC+05:00) Ашхабад']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+05:00) Исламабад, Карачи'], ['out_time_zone' => '(UTC+05:00) Исламабад']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+05:30) Колката, Мумбаи, Нью-Дели, Ченнай'], ['out_time_zone' => '(UTC+05:30) Колката']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+07:00) Бангкок, Джакарта, Ханой'], ['out_time_zone' => '(UTC+07:00) Бангкок']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+08:00) Гонконг, Пекин, Урумчи, Чунцин'], ['out_time_zone' => '(UTC+08:00) Гонконг']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+08:00) Куала-Лумпур, Сингапур'], ['out_time_zone' => '(UTC+08:00) Куала-Лумпур']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+09:00) Осака, Саппоро, Токио'], ['out_time_zone' => '(UTC+09:00) Осака']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+10:00) Владивосток, Магадан (RTZ 9)'], ['out_time_zone' => '(UTC+10:00) Владивосток']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+10:00) Гуам, Порт-Морсби'], ['out_time_zone' => '(UTC+10:00) Гуам']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+10:00) Канберра, Мельбурн, Сидней'], ['out_time_zone' => '(UTC+10:00) Канберра']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+11:00) Соломоновы о-ва, Нов. Каледония'], ['out_time_zone' => '(UTC+11:00) Соломоновы о-ва']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+12:00) Анадырь, Петропаловск-Камчатский (RTZ 11)'], ['out_time_zone' => '(UTC+12:00) Анадырь']);
        $this->update($this->tTimeZone, ['out_time_zone' => '(UTC+12:00) Веллигтон, Окленд'], ['out_time_zone' => '(UTC+12:00) Веллигтон']);

        $this->delete($this->tTimeZone, ['id' => $splitTimeZones]);

        $this->alterColumn($this->tTimeZone, 'time_zone', $this->string()->notNull()->unique());
    }
}


