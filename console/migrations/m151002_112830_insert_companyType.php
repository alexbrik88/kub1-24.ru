<?php

use yii\db\Schema;
use yii\db\Migration;

class m151002_112830_insert_companyType extends Migration
{
    public $tCompanyType = 'company_type';

    public function safeUp()
    {
        $this->batchInsert($this->tCompanyType, ['id', 'name_short', 'name_full'], [
            [4, 'ПАО', 'Публичное акционерное общество'],
            [5, 'АО', 'Акционерное общество'],
            [6, 'АНО', 'Автономная некоммерческая организация'],
            [7, 'ФГУП', 'Федеральное государственное унитарное предприятие'],
        ]);
    }
    
    public function safeDown()
    {
        $this->delete($this->tCompanyType, 'id IN (4,5,6,7)');
    }
}
