<?php

use yii\db\Schema;
use yii\db\Migration;

class m150513_063550_alter_Company_addColumn_StrictMode extends Migration
{
    public function safeUp()
    {
        $this->addColumn('company', 'strict_mode', Schema::TYPE_BOOLEAN . ' COMMENT "0 - off, 1 - on" AFTER `id`');
    }
    
    public function safeDown()
    {
        $this->dropColumn('company', 'strict_mode');
    }
}
