<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180523_143335_alter_order_add_columns extends Migration
{
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE {{order}}
                ADD COLUMN [[view_price_base]] DECIMAL(22,2) NOT NULL DEFAULT '0.00' AFTER [[markup]],
                ADD COLUMN [[view_price_one]] DECIMAL(22,2) NOT NULL DEFAULT '0.00' AFTER [[view_price_base]],
                ADD COLUMN [[view_total_base]] DECIMAL(22,2) NOT NULL DEFAULT '0.00' AFTER [[view_price_one]],
                ADD COLUMN [[view_total_amount]] DECIMAL(22,2) NOT NULL DEFAULT '0.00' AFTER [[view_total_base]],
                ADD COLUMN [[view_total_no_nds]] DECIMAL(22,2) NOT NULL DEFAULT '0.00' AFTER [[view_total_amount]],
                ADD COLUMN [[view_total_nds]] DECIMAL(22,2) NOT NULL DEFAULT '0.00' AFTER [[view_total_no_nds]],
                ADD COLUMN [[view_total_with_nds]] DECIMAL(22,2) NOT NULL DEFAULT '0.00' AFTER [[view_total_nds]];
        ");

        $this->execute("
            UPDATE {{order}}
            LEFT JOIN {{invoice}} ON {{invoice}}.[[id]] = {{order}}.[[invoice_id]]
            SET {{order}}.[[view_price_base]] = IF(
                    {{invoice}}.[[nds_view_type_id]] = 1,
                    {{order}}.[[currency_price_no_vat]],
                    {{order}}.[[currency_price_with_vat]]
                ),

                {{order}}.[[view_price_one]] = ROUND(
                    IF(
                        {{invoice}}.[[type]] = 1,
                        IF(
                            {{invoice}}.[[nds_view_type_id]] = 1,
                            {{order}}.[[purchase_price_no_vat]],
                            {{order}}.[[purchase_price_with_vat]]
                        ),
                        IF(
                            {{invoice}}.[[nds_view_type_id]] = 1,
                            {{order}}.[[selling_price_no_vat]],
                            {{order}}.[[selling_price_with_vat]]
                        )
                    ) * {{invoice}}.[[currency_amount]] / {{invoice}}.[[currency_rate]]
                , 2),

                {{order}}.[[view_total_base]] = ROUND({{order}}.[[view_price_base]] * {{order}}.[[quantity]], 2),

                {{order}}.[[view_total_amount]] = ROUND({{order}}.[[view_price_one]] * {{order}}.[[quantity]], 2),

                {{order}}.[[view_total_no_nds]] = ROUND(
                    IF({{invoice}}.[[type]] = 1, {{order}}.[[amount_purchase_no_vat]], {{order}}.[[amount_sales_no_vat]])
                        * {{invoice}}.[[currency_amount]] / {{invoice}}.[[currency_rate]]
                , 2),

                {{order}}.[[view_total_nds]] = ROUND(
                    IF({{invoice}}.[[type]] = 1, {{order}}.[[purchase_tax]], {{order}}.[[sale_tax]])
                        * {{invoice}}.[[currency_amount]] / {{invoice}}.[[currency_rate]]
                , 2),

                {{order}}.[[view_total_with_nds]] = ROUND(
                    IF({{invoice}}.[[type]] = 1, {{order}}.[[amount_purchase_with_vat]], {{order}}.[[amount_sales_with_vat]])
                        * {{invoice}}.[[currency_amount]] / {{invoice}}.[[currency_rate]]
                , 2);
        ");
    }

    public function safeDown()
    {
        $this->execute("
            ALTER TABLE {{order}}
                DROP COLUMN [[view_price_base]],
                DROP COLUMN [[view_price_one]],
                DROP COLUMN [[view_total_base]],
                DROP COLUMN [[view_total_amount]],
                DROP COLUMN [[view_total_no_nds]],
                DROP COLUMN [[view_total_nds]],
                DROP COLUMN [[view_total_with_nds]];
        ");
    }
}
