<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180215_134621_add_document_author_id_to_order_document extends Migration
{
    public $tableName = 'order_document';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'document_author_id', $this->integer()->notNull());
        $this->addForeignKey($this->tableName . '_document_author_id', $this->tableName, 'document_author_id', 'employee', 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_document_author_id', $this->tableName);
        $this->dropColumn($this->tableName, 'document_author_id');
    }
}


