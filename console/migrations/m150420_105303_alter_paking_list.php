<?php

use yii\db\Schema;
use yii\db\Migration;

class m150420_105303_alter_paking_list extends Migration
{
    public function up()
    {
        $this->addColumn('packing_list', 'type', Schema::TYPE_INTEGER . '(1) NOT NULL COMMENT "Тип накладной - 2 (исходящая), 1 (входящая)"');
    }

    public function down()
    {
        $this->dropColumn('packing_list', 'type');
    }
}
