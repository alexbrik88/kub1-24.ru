<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180704_112934_alter_product_create_index extends Migration
{
    public function safeUp()
    {
        $this->execute("ALTER TABLE {{product}} ADD FULLTEXT INDEX [[title]] ([[title]]);");
    }

    public function safeDown()
    {
        $this->execute("ALTER TABLE {{product}} DROP INDEX [[title]];");
    }
}
