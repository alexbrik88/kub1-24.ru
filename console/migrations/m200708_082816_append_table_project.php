<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200708_082816_append_table_project extends Migration
{
    public $table = 'project';

    public function safeUp()
    {
        $this->createTable($this->table, [
            'id' => 'pk',
            'company_id' => 'integer',
            'number' => 'string',
            'responsible' => 'integer',
            'customer' => 'integer',
            'document' => 'integer',
            'name' => 'string',
            'status' => 'integer',
            'description' => 'text',
            'start_date' => 'date',
            'end_date' => 'date',
            'money_amount' => 'integer',
            'money_expense' => 'integer',
            'created_at' => 'timestamp',
            'updated_at' => 'timestamp',
        ]);

        $this->addForeignKey('fk_project_responsible',
            $this->table,
            'responsible',
            'employee',
            'id'
        );

        $this->addForeignKey('fk_project_customer',
            $this->table,
            'customer',
            'contractor',
            'id'
        );

        $this->addForeignKey('fk_project_document',
            $this->table,
            'document',
            'agreement',
            'id'
        );
    }

    public function safeDown()
    {
        try {
            $this->dropForeignKey('fk_project_responsible', $this->table);
            $this->dropForeignKey('fk_project_customer', $this->table);
            $this->dropForeignKey('fk_project_document', $this->table);
        } catch(\Exception $e) {
            echo 'Drop foreign key fail';
        }
        $this->dropTable($this->table);
    }
}
