<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180222_123605_alter_store_user_add_columns extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('store_user', 'username');
        $this->addColumn('store_user', 'lastname', $this->string()->after('status'));
        $this->addColumn('store_user', 'firstname', $this->string()->after('lastname'));
        $this->addColumn('store_user', 'patronymic', $this->string()->after('firstname'));
    }

    public function safeDown()
    {
        $this->dropColumn('store_user', 'lastname');
        $this->dropColumn('store_user', 'firstname');
        $this->dropColumn('store_user', 'patronymic');
        $this->addColumn('username', 'lastname', $this->string()->after('status'));
    }
}
