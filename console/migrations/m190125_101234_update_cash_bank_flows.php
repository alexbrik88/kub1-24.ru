<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190125_101234_update_cash_bank_flows extends Migration
{
    public function safeUp()
    {
        $this->update('cash_bank_flows', ['is_taxable' => false], [
            'flow_type' => 1,
            'income_item_id' => [
                2, // Взнос наличными
                3, // Возврат
                4, // Займ
                5, // Кредит
                6, // От учредителя
                9, // Перевод собственных средств
                10, // Возврат займа
                13, // Возврат средств из бюджета
                14, // Обеспечительный платёж
            ],
        ]);
    }

    public function safeDown()
    {
        //
    }
}
