<?php

use console\components\db\Migration;

class m191029_171110_create_evotor_employee_store extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{evotor_employee_store}}', [
            'employee_id' => "BINARY(16) NOT NULL COMMENT 'UUID сотрудника Эвотора'",
            'store_id' => "BINARY(16) NOT NULL COMMENT 'UUID магазина Эвотора'",
        ], "COMMENT 'Интеграция Эвотор: связь сотрудников с магазинами'");
        $this->createIndex('employee_store_employee_id_store_id', '{{evotor_employee_store}}', ['employee_id', 'store_id'],true);
        $this->addForeignKey('evotor_employee_store_employee_id', '{{evotor_employee_store}}', 'employee_id',
            '{{evotor_employee}}', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('evotor_employee_store_store_id', '{{evotor_employee_store}}', 'store_id',
            '{{evotor_store}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function down()
    {
        $this->dropTable('{{evotor_employee_store}}');
    }

}
