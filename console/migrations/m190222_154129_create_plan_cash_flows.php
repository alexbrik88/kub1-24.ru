<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190222_154129_create_plan_cash_flows extends Migration
{
    public $tableName = 'plan_cash_flows';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'contractor_id' => $this->integer()->notNull(),
            'date' => $this->date()->notNull(),
            'flow_type' => $this->boolean()->defaultValue(1)->comment('0 - расход, 1 - приход'),
            'payment_type' => $this->integer()->defaultValue(1)->comment('1 - банк, 2 - касса, 3 - emoney'),
            'amount' => $this->decimal(38, 2)->notNull(),
            'income_item_id' => $this->integer()->defaultValue(null),
            'expenditure_item_id' => $this->integer()->defaultValue(null),
            'description' => $this->text()->notNull(),
        ]);

        $this->addForeignKey($this->tableName . '_company_id', $this->tableName, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tableName . '_contractor_id', $this->tableName, 'contractor_id', 'contractor', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tableName . '_income_item_id', $this->tableName, 'income_item_id', 'invoice_income_item', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tableName . '_expenditure_item_id', $this->tableName, 'expenditure_item_id', 'invoice_expenditure_item', 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_company_id', $this->tableName);
        $this->dropForeignKey($this->tableName . '_contractor_id', $this->tableName);
        $this->dropForeignKey($this->tableName . '_income_item_id', $this->tableName);
        $this->dropForeignKey($this->tableName . '_expenditure_item_id', $this->tableName);

        $this->dropTable($this->tableName);
    }
}
