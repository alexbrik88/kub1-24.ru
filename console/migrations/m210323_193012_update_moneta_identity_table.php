<?php

use yii\db\Migration;

class m210323_193012_update_moneta_identity_table extends Migration
{
    /**
     * @inheritDoc
     * @throws
     */
    public function safeUp()
    {
        $this->db->createCommand(<<<SQL
            INSERT IGNORE INTO moneta_identity (account_id, company_id, username, password)
            SELECT identifier, company_id, username, password FROM acquiring
            WHERE type = 0
SQL
        )->execute();
    }
}
