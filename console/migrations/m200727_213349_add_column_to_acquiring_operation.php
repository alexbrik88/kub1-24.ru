<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200727_213349_add_column_to_acquiring_operation extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%acquiring_operation}}', 'acquiring_type', $this->tinyInteger(3)->unsigned()->notNull());
        $this->update('{{%acquiring_operation}}', ['acquiring_type' => 0]);
    }

    public function safeDown()
    {
        $this->dropColumn('{{%acquiring_operation}}', 'acquiring_type');
    }
}
