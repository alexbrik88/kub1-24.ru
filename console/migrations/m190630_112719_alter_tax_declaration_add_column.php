<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190630_112719_alter_tax_declaration_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('tax_declaration', 'single', $this->integer(1)->defaultValue(0)->after('downloaded_quarter'));

        $this->execute("
          UPDATE `tax_declaration` SET `single`=1 WHERE `org`=0 AND `nds`=0 AND `szvm`=0 AND `balance`=0
          AND `company_id` IN (SELECT `company_id` FROM `company` WHERE `company_type_id` > 1)
        ");
    }

    public function safeDown()
    {
        $this->dropColumn('tax_declaration', 'single');
    }
}
