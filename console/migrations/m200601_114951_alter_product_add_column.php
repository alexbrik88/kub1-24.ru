<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200601_114951_alter_product_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('product', 'custom_field_value', $this->string(45));
    }

    public function safeDown()
    {
        $this->dropColumn('product', 'custom_field_value');
    }
}
