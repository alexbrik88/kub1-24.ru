<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200622_085509_alter_ofd_kkt_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%ofd_kkt}}', 'store_id', $this->integer()->after('id'));

        $this->addForeignKey('FK_ofd_kkt_ofd_store', '{{%ofd_kkt}}', 'store_id', '{{%ofd_store}}', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_ofd_kkt_ofd_store', '{{%ofd_kkt}}');

        $this->dropColumn('{{%ofd_kkt}}', 'store_id');
    }
}
