<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%ofd}}`.
 */
class m200520_171935_create_ofd_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%ofd}}', [
            'id' => $this->primaryKey(),
            'alias' => $this->string()->notNull(),
            'name' => $this->string()->notNull(),
            'url' => $this->string()->notNull(),
            'is_active' => $this->boolean()->notNull()->defaultValue(false),
            'logo' => $this->string(),
        ]);

        $this->createIndex('alias', '{{%ofd}}', 'alias', true);

        $this->batchInsert('{{%ofd}}', [
            'id',
            'alias',
            'name',
            'url',
            'is_active',
            'logo',
        ], [
            [1, 'astral', 'Астрал', 'https://ofd.astralnalog.ru/', 0, 'astral.jpg'],
            [2, 'initpro', 'ИнитПро', 'https://ofd-initpro.ru/', 0, 'initpro.jpg'],
            [3, 'kontur', 'Контур', 'https://kontur.ru/', 0, 'kontur.jpg'],
            [4, 'ofdru', 'OFD.ru', 'https://ofd.ru/', 0, 'ofdru.jpg'],
            [5, 'perviy', 'Первый ОФД', 'https://www.1-ofd.ru/', 0, 'perviy.jpg'],
            [6, 'platforma', 'Платформа ОФД', 'https://platformaofd.ru/', 1, 'platforma.jpg'],
            [7, 'sbis', 'СБИС', 'https://sbis.ru/', 0, 'sbis.jpg'],
            [8, 'taxcom', 'Такском', 'https://taxcom.ru/', 0, 'taxcom.jpg'],
            [9, 'yandex', 'Яндекс.ОФД', 'https://ofd.yandex.ru/', 0, 'yandex.jpg'],
            [10, 'yarus', 'Ярус', 'https://ofd-ya.ru/', 0, 'yarus.jpg'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%ofd}}');
    }
}
