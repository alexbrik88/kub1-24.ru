<?php

use console\components\db\Migration;

class m200804_145054_table_notes extends Migration
{
    public $tableName = 'notes';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'index' => $this->integer(11)->unsigned(),
            'company_id' => $this->integer(11),
            'employee_id' => $this->integer(11),
            'title' => $this->string(255),
            'description' => $this->string(500),
        ]);

        $this->createIndex($this->tableName . '_company_id_idx', $this->tableName, 'company_id');
        $this->createIndex($this->tableName . '_employee_id_idx', $this->tableName, 'employee_id');
    }

    public function safeDown()
    {
        try {
            $this->dropIndex($this->tableName . '_company_id_idx', $this->tableName);
            $this->dropIndex($this->tableName . '_employee_id_idx', $this->tableName);
        } catch (\Exception $e) {
            echo $e->getTraceAsString();
        }

        $this->dropTable($this->tableName);
    }

}
