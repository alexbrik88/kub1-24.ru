<?php

use console\components\db\Migration;
use yii\db\Schema;

class m220402_081054_alter_config_add_column extends Migration
{
    public $tableName = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'act_comment', $this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'act_comment');
    }
}
