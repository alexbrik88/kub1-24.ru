<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190620_162244_alter_autoinvoice_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('autoinvoice', 'updated_at', $this->integer()->after('created_at'));
        $this->addColumn('autoinvoice', 'send_to_director', $this->boolean()->defaultValue(false));
        $this->addColumn('autoinvoice', 'send_to_chief_accountant', $this->boolean()->defaultValue(false));
        $this->addColumn('autoinvoice', 'send_to_contact', $this->boolean()->defaultValue(false));

        $this->execute('
            UPDATE {{autoinvoice}}
            SET [[updated_at]] = [[created_at]];
        ');
    }

    public function safeDown()
    {
        $this->dropColumn('autoinvoice', 'updated_at');
        $this->dropColumn('autoinvoice', 'send_to_director');
        $this->dropColumn('autoinvoice', 'send_to_chief_accountant');
        $this->dropColumn('autoinvoice', 'send_to_contact');
    }
}
