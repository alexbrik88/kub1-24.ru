<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200511_071213_update_balance_item_name extends Migration
{
    public function safeUp()
    {
        $this->execute(' UPDATE `balance_item` SET `name` = "Задолженность перед поставщиками" WHERE `id` = 19 LIMIT 1');
    }

    public function safeDown()
    {
        $this->execute(' UPDATE `balance_item` SET `name` = "Счета поставщиков к оплате" WHERE `id` = 19  LIMIT 1');
    }
}
