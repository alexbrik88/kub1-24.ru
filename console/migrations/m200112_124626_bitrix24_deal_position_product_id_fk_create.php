<?php

use console\components\db\Migration;

class m200112_124626_bitrix24_deal_position_product_id_fk_create extends Migration
{
    const TABLE = '{{%bitrix24_deal_position}}';

    public function safeUp()
    {
        $this->addForeignKey(
            'bitrix24_deal_position_product_id', self::TABLE, ['company_id', 'product_id'],
            '{{%bitrix24_product}}', ['company_id', 'id'],
            'CASCADE', 'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('bitrix24_deal_position_product_id', self::TABLE);
    }
}
