<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190115_193535_alter_invoice_expenditure_item_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('invoice_expenditure_item', 'group_id', $this->integer()->after('id'));

        $this->addForeignKey(
            'FK_invoice_expenditure_item_group',
            'invoice_expenditure_item', 'group_id',
            'invoice_expenditure_group', 'id'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey(
            'FK_invoice_expenditure_item_group',
            'invoice_expenditure_item'
        );

        $this->dropColumn('invoice_expenditure_item', 'group_id');
    }
}
