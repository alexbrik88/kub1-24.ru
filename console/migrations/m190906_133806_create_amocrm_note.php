<?php

use common\models\amocrm\Note;
use console\components\db\Migration;

class m190906_133806_create_amocrm_note extends Migration
{
    public function up()
    {
        $this->createTable('{{amocrm_note}}', [
            'id' => $this->primaryKey()->unsigned(),
            'element_id' => $this->integer()->unsigned()->notNull()->comment('ID сущности, к которой относится примечание'),
            'element_type' => "ENUM('" . implode("','", Note::ELEMENT_TYPE) . "') NOT NULL COMMENT 'Тип сущности, к которой относится примечание'",
            'note_type' => "ENUM('" . implode("','", Note::TYPE) . "') COMMENT 'Тип примечания'",
            'text' => $this->string(1000)->notNull()->comment('Текст примечания'),
            'attachement' => $this->string(1000),
            'created_at' => $this->integer()->unsigned()->notNull()->comment('Дата создания'),
            'updated_at' => $this->integer()->unsigned()->notNull()->comment('Дата последнего изменения'),
        ], "COMMENT='Интеграция AMOcrm: примечания'");
    }

    public function down()
    {
        $this->dropTable('{{amocrm_note}}');

    }
}
