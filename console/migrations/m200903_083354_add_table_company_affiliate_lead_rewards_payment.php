<?php

use console\components\db\Migration;

class m200903_083354_add_table_company_affiliate_lead_rewards_payment extends Migration
{
    public $tableName = 'company_affiliate_lead_rewards_payment';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer(11),
            'lead_company_id' => $this->integer(11),
            'invoice_id' => $this->integer(11),
            'rewards_percent' => $this->integer()->unsigned(),
            'rewards_amount' => $this->double()->unsigned(),
            'created_at' => $this->integer(11),
            'updated_at' => $this->integer(11),
        ]);

        $this->createIndex($this->tableName . '_company_id_idx', $this->tableName, 'company_id');
        $this->createIndex($this->tableName . '_lead_company_id_idx', $this->tableName, 'lead_company_id');
        $this->createIndex($this->tableName . '_invoice_id_idx', $this->tableName, 'invoice_id');
    }

    public function safeDown()
    {
        try {
            $this->dropIndex($this->tableName . '_company_id_idx', $this->tableName);
            $this->dropIndex($this->tableName . '_lead_company_id_idx', $this->tableName);
            $this->dropIndex($this->tableName . '_invoice_id_idx', $this->tableName);
        } catch (Exception $e) {
            echo $e->getMessage();
        }

        $this->dropTable($this->tableName);
    }

}