<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170815_045157_create_bankStatementAutoload extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%bank_statement_autoload_mode}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
            'week_days' => $this->string()->notNull(),
        ]);

        $this->batchInsert('{{%bank_statement_autoload_mode}}', ['id', 'name', 'week_days'], [
            [1, 'Каждый день', '1,2,3,4,5,6,7'],
            [2, 'Каждый пн, ср, пт', '1,3,5'],
        ]);
    }
    
    public function safeDown()
    {
        $this->dropTable('{{%bank_statement_autoload_mode}}');
    }
}


