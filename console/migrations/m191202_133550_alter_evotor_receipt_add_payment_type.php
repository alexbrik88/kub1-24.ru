<?php

use common\models\insales\Client;
use console\components\db\Migration;
use yii\db\Schema;

class m191202_133550_alter_evotor_receipt_add_payment_type extends Migration
{
    public function up()
    {
        $this->addColumn('{{evotor_receipt}}', 'cash', $this->boolean()->notNull()->defaultValue(false)->comment('Наличная оплата'));
    }

    public function down()
    {
        $this->dropColumn('{{evotor_receipt}}', 'cash');
    }
}
