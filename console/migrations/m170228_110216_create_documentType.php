<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170228_110216_create_documentType extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%document_type}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(50)->notNull(),
            'name2' => $this->string(50)->notNull(),
        ]);

        $this->insert('{{%document_type}}', [
            'id' => 1,
            'name' => 'Доверенность',
            'name2' => 'Доверенности',
        ]);
    }

    public function safeDown()
    {
        $this->dropTable('{{%document_type}}');
    }
}
