<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200118_180352_create_last_operation_in_integration extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%last_operation_in_integration}}', [
            'company_id' => $this->integer()->notNull(),
            'type' => $this->tinyInteger(3)->unsigned()->notNull(),
            'date' => $this->integer(11)->unsigned()->notNull(),
        ]);

        $this->addPrimaryKey('company_id_type', '{{%last_operation_in_integration}}', ['company_id', 'type']);

        $this->addForeignKey('FK_last_operation_in_integration_to_comany', 'last_operation_in_integration', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable('{{%last_operation_in_integration}}');
    }
}
