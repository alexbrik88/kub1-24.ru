<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210806_065421_alter_olap_flows_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%olap_flows}}', 'original_amount', $this->bigInteger()->notNull()->after('amount'));
        $this->addColumn('{{%olap_flows}}', 'currency_id', $this->char(3)->notNull()->after('original_amount'));
        $this->addColumn('{{%olap_flows}}', 'currency_name', $this->char(3)->notNull()->after('currency_id'));

        $this->update('{{%olap_flows}}', [
            'original_amount' => new \yii\db\Expression('[[amount]]'),
            'currency_id' => '643',
            'currency_name' => 'RUB',
        ]);
    }

    public function safeDown()
    {
        $this->dropColumn('{{%olap_flows}}', 'original_amount');
        $this->dropColumn('{{%olap_flows}}', 'currency_id');
        $this->dropColumn('{{%olap_flows}}', 'currency_name');
    }
}
