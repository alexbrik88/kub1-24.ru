<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210826_141308_alter_payments_request_change_column extends Migration
{
    public $table = 'payments_request';

    public function safeUp()
    {
        $table = $this->table;
        $this->renameColumn($table, 'template_id', 'sign_template_id');
        $this->alterColumn($table, 'sign_template_id', $this->integer()->comment('Шаблон подписания'));
        $this->addForeignKey("FK_{$table}_to_sign_template", $table, 'sign_template_id', 'payments_request_sign_template', 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown()
    {
        $table = $this->table;
        $this->dropForeignKey("FK_{$table}_to_sign_template", $table);
        $this->renameColumn($table, 'sign_template_id', 'template_id');
        $this->alterColumn($table, 'template_id', $this->integer()->comment('Шаблон'));
    }
}
