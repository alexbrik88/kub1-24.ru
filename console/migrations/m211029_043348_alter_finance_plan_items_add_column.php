<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211029_043348_alter_finance_plan_items_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('finance_plan_odds_expenditure_item', 'credit_id', $this->integer()->after('expenditure_item_id'));
        $this->addColumn('finance_plan_odds_income_item', 'credit_id', $this->integer()->after('income_item_id'));
        $this->addColumn('finance_plan_odds_expenditure_item', 'can_edit', $this->boolean()->defaultValue(1)->after('action'));
        $this->addColumn('finance_plan_odds_income_item', 'can_edit', $this->boolean()->defaultValue(1)->after('action'));

        $this->addForeignKey('FK_finance_plan_odds_expenditure_item_credit', 'finance_plan_odds_expenditure_item', 'credit_id', 'finance_plan_credit', 'id', 'CASCADE');
        $this->addForeignKey('FK_finance_plan_odds_income_item_credit', 'finance_plan_odds_income_item', 'credit_id', 'finance_plan_credit', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_finance_plan_odds_expenditure_item_credit', 'finance_plan_odds_expenditure_item');
        $this->dropForeignKey('FK_finance_plan_odds_income_item_credit', 'finance_plan_odds_income_item');

        $this->dropColumn('finance_plan_odds_expenditure_item', 'credit_id');
        $this->dropColumn('finance_plan_odds_income_item', 'credit_id');
        $this->dropColumn('finance_plan_odds_expenditure_item', 'can_edit');
        $this->dropColumn('finance_plan_odds_income_item', 'can_edit');
    }
}
