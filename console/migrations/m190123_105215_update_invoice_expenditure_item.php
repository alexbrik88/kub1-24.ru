<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190123_105215_update_invoice_expenditure_item extends Migration
{
    public function safeUp()
    {
        $this->update('invoice_expenditure_item', ['group_id' => 1], ['id' => 46]);
    }

    public function safeDown()
    {
        $this->update('invoice_expenditure_item', ['group_id' => null], ['id' => 46]);
    }
}
