<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211215_200536_alter_config_add_column extends Migration
{
    public $tableName = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'project_profitability', $this->boolean()->notNull()->defaultValue(true));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'project_profitability');
    }
}
