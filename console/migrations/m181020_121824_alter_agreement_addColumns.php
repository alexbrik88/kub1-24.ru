<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181020_121824_alter_agreement_addColumns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%agreement}}', 'document_author_id', $this->integer());
        $this->addColumn('{{%agreement}}', 'agreement_template_id', $this->integer());
        $this->addColumn('{{%agreement}}', 'type', 'tinyint(1) NOT NULL DEFAULT 2 COMMENT "Тип договора - 2 (исходящий), 1 (входящий)" AFTER `id`');
        $this->addColumn('{{%agreement}}', 'uid', $this->string(5) . ' AFTER [[id]]');
        $this->addColumn('{{%agreement}}', 'document_additional_number', $this->string(50) . ' AFTER [[document_number]]');
        $this->addColumn('{{%agreement}}', 'is_completed', 'tinyint(1) NOT NULL DEFAULT 0 AFTER `is_created`');
        $this->addColumn('{{%agreement}}', 'comment_internal', $this->string(255) .' AFTER [[document_type_id]] ');

        $this->addColumn('{{%agreement}}', 'payment_delay', $this->integer());
        $this->addColumn('{{%agreement}}', 'payment_limit_date', $this->date());
        $this->addColumn('{{%agreement}}', 'company_rs', $this->string(20));

        $this->alterColumn('{{%agreement}}','contractor_id','integer DEFAULT NULL');
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%agreement}}', 'document_author_id');
        $this->dropColumn('{{%agreement}}', 'agreement_template_id');
        $this->dropColumn('{{%agreement}}', 'type');
        $this->dropColumn('{{%agreement}}', 'uid');
        $this->dropColumn('{{%agreement}}', 'document_additional_number');
        $this->dropColumn('{{%agreement}}', 'is_completed');
        $this->dropColumn('{{%agreement}}', 'comment_internal');

        $this->dropColumn('{{%agreement}}', 'payment_delay');
        $this->dropColumn('{{%agreement}}', 'payment_limit_date');
        $this->dropColumn('{{%agreement}}', 'company_rs');

        $this->alterColumn('{{%agreement}}','contractor_id','integer NOT NULL');
    }
}
