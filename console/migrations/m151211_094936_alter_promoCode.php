<?php

use yii\db\Schema;
use yii\db\Migration;

class m151211_094936_alter_promoCode extends Migration
{
    public $tPromoCode = 'service_payment_promo_code';
    public $tTariff = 'service_subscribe_tariff';

    public function safeUp()
    {
        $this->dropForeignKey('fk_service_payment_promo_code_to_tariff', $this->tPromoCode);
        $this->dropColumn($this->tPromoCode, 'tariff_id');

        $this->addColumn($this->tPromoCode, 'duration_month', $this->integer()->notNull()->defaultValue(0) . ' AFTER code');
        $this->update($this->tPromoCode, [
            'duration_month' => 1,
        ]);
        $this->addColumn($this->tPromoCode, 'duration_day', $this->integer()->notNull()->defaultValue(0) . ' AFTER duration_month');
    }
    
    public function safeDown()
    {
        $this->addColumn($this->tPromoCode, 'tariff_id', $this->integer()->notNull() . ' AFTER id');
        $this->update($this->tPromoCode, [
            'tariff_id' => 1, // 1 month
        ]);
        $this->addForeignKey('fk_service_payment_promo_code_to_tariff', $this->tPromoCode, 'tariff_id', $this->tTariff, 'id');

        $this->dropColumn($this->tPromoCode, 'duration_month');
        $this->dropColumn($this->tPromoCode, 'duration_day');
    }
}
