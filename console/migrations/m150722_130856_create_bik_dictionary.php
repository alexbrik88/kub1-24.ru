<?php

use yii\db\Schema;
use yii\db\Migration;

class m150722_130856_create_bik_dictionary extends Migration
{
    public function safeUp()
    {
        $this->createTable('bik_dictionary', [
            'bik' => 'CHAR(9) NOT NULL',
            'ks' => Schema::TYPE_STRING . '(20) NULL',
            'name' => Schema::TYPE_STRING . '(255) DEFAULT NULL',
            'namemini' => Schema::TYPE_STRING . '(255) DEFAULT NULL',
            'index' => Schema::TYPE_STRING . '(255) DEFAULT NULL',
            'city' => Schema::TYPE_STRING . '(255) DEFAULT NULL',
            'address' => Schema::TYPE_STRING . '(255) DEFAULT NULL',
            'phone' => Schema::TYPE_STRING . '(255) DEFAULT NULL',
            'okato' => Schema::TYPE_STRING . '(255) DEFAULT NULL',
            'okpo' => Schema::TYPE_STRING . '(255) DEFAULT NULL',
            'regnum' => Schema::TYPE_STRING . '(255) DEFAULT NULL',
            'srok' => Schema::TYPE_STRING . '(255) DEFAULT NULL',
            'dateadd' => Schema::TYPE_DATE . ' NULL',
            'datechange' => Schema::TYPE_DATE . ' NULL',
        ]);

        $this->createIndex('bik_u_idx', 'bik_dictionary', 'bik', true);
    }

    public function safeDown()
    {
        $this->dropTable('bik_dictionary');
    }
}
