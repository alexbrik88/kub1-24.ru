<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180404_074331_alter_order_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('order', 'markup', $this->decimal(8, 4)->notNull()->defaultValue(0)->after('discount'));
    }

    public function safeDown()
    {
        $this->dropColumn('order', 'markup');
    }
}
