<?php
use console\components\db\Migration;
use frontend\modules\export\models\one_c\OneCExport;

/**
 * Class m160401_094051_current_account_guid
 */
class m160401_094051_current_account_guid extends Migration
{
    /**
     * @var string
     */
    public $tableName = 'contractor';

    /**
     * @var string
     */
    public $column = 'current_account_guid';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->addColumn($this->tableName, $this->column, $this->string(255)->notNull()->defaultValue(''));

        foreach ((new \yii\db\Query())->select(['id'])->from($this->tableName)->orderBy('id')->batch(50) as $contractorIds) {
            foreach ($contractorIds as $contractorId) {
                \Yii::$app->db->createCommand()->update($this->tableName, [
                    $this->column => OneCExport::generateGUID()
                ], 'id = :id', [':id' => $contractorId['id']])->execute();
            }
        };
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropColumn($this->tableName, $this->column);
    }
}


