<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201221_090226_alter_foreign_currency_account_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%foreign_currency_account}}', 'is_foreign_bank', $this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%foreign_currency_account}}', 'is_foreign_bank');
    }
}
