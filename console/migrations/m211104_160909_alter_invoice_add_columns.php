<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211104_160909_alter_invoice_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%invoice}}', 'price_round_rule', $this->tinyInteger(1)->notNull()->defaultValue(1));
        $this->addColumn('{{%invoice}}', 'price_round_precision', $this->tinyInteger(1)->notNull()->defaultValue(1));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%invoice}}', 'price_round_rule');
        $this->dropColumn('{{%invoice}}', 'price_round_precision');
    }
}
