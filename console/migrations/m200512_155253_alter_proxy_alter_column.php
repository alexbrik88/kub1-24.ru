<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200512_155253_alter_proxy_alter_column extends Migration
{
    public function up()
    {
        $this->alterColumn('{{%proxy}}', 'consignor_id',  $this->integer());
    }

    public function down()
    {
        $this->alterColumn('{{%proxy}}', 'consignor_id',  $this->integer()->notNull());
    }
}
