<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201217_050900_update_invoice_items_name extends Migration
{
    public function safeUp()
    {
        $this->execute("
            UPDATE `invoice_income_item` SET `name` = 'Перевод между счетами' WHERE `id` = 9;
        ");
        $this->execute("
            UPDATE `invoice_expenditure_item` SET `name` = 'Перевод между счетами' WHERE `id` = 44;
        ");
    }

    public function safeDown()
    {
        $this->execute("
            UPDATE `invoice_income_item` SET `name` = 'Перевод собственных средств' WHERE `id` = 9;
        ");
        $this->execute("
            UPDATE `invoice_expenditure_item` SET `name` = 'Перевод собственных средств' WHERE `id` = 44;
        ");
    }
}
