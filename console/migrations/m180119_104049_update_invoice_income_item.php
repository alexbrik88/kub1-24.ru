<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180119_104049_update_invoice_income_item extends Migration
{
    public $tableName = 'invoice_income_item';

    public function safeUp()
    {
        $this->update($this->tableName, ['name' => 'Возврат займа'], ['id' => 10]);
    }
    
    public function safeDown()
    {
        $this->update($this->tableName, ['name' => 'Погашение займа'], ['id' => 10]);
    }
}


