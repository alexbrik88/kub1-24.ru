<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160117_132207_update_time_zone extends Migration
{
    public $tTimeZone = 'time_zone';

    public function safeUp()
    {
        $this->update($this->tTimeZone, ['time_zone' => 'Australia/Darwin'], ['time_zone' => '	Australia/Darwin']);
        $this->update($this->tTimeZone, ['time_zone' => 'Pacific/Enderbury'], ['time_zone' => '	Pacific/Enderbury']);
        $this->update($this->tTimeZone, ['time_zone' => 'Asia/Tbilisi'], ['time_zone' => '	Asia/Tbilisi']);
        $this->update($this->tTimeZone, ['time_zone' => 'Asia/Baku'], ['time_zone' => '	Asia/Baku']);
    }
    
    public function safeDown()
    {
        echo 'This migration not rolled back';
    }
}


