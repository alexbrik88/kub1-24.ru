<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191227_131850_alter_invoice_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%invoice}}', 'company_bank_city', $this->string()->after('company_bank_name'));
        $this->addColumn('{{%invoice}}', 'contractor_bank_city', $this->string()->after('contractor_bank_name'));

        $this->execute('
            UPDATE {{%invoice}}
            LEFT JOIN {{%bik_dictionary}} ON {{%bik_dictionary}}.[[bik]] = {{%invoice}}.[[company_bik]]
            SET {{%invoice}}.[[company_bank_city]] = {{%bik_dictionary}}.[[city]]
        ');
        $this->execute('
            UPDATE {{%invoice}}
            LEFT JOIN {{%bik_dictionary}} ON {{%bik_dictionary}}.[[bik]] = {{%invoice}}.[[contractor_bik]]
            SET {{%invoice}}.[[contractor_bank_city]] = {{%bik_dictionary}}.[[city]]
        ');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%invoice}}', 'contractor_bank_city');
        $this->dropColumn('{{%invoice}}', 'company_bank_city');
    }
}
