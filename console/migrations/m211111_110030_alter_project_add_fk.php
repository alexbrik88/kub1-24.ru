<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211111_110030_alter_project_add_fk extends Migration
{
    public function safeUp()
    {
        $table = 'project';
        $this->addForeignKey("FK_{$table}_to_company", $table, 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $table = 'project';
        $this->dropForeignKey("FK_{$table}_to_company", $table);
    }
}
