<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191203_084904_create_sale_point_type extends Migration
{
    public $table = 'sale_point_type';

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->table, [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'sort' => $this->tinyInteger()->notNull()->defaultValue(1)
        ], $tableOptions);

        $this->batchInsert($this->table, ['id', 'name', 'sort'], [
            [1, 'Отдел продаж', 1],
            [2, 'Магазин', 2],
            [3, 'Интернет-магазин', 3],
            [4, 'Кафе/ресторан', 4],
            [5, 'Филиал', 5],
        ]);
    }

    public function safeDown()
    {
        $this->dropTable($this->table);
    }
}
