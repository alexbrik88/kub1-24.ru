<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180217_134533_add_invoice_id_to_order_document extends Migration
{
    public $tableName = 'order_document';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'invoice_id', $this->integer()->defaultValue(null));
        $this->addForeignKey($this->tableName . '_invoice_id', $this->tableName, 'invoice_id', 'invoice', 'id', 'RESTRICT', 'CASCADE');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_invoice_id', $this->tableName);
        $this->dropColumn($this->tableName, 'invoice_id');
    }
}


