<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200514_100901_alter_company_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%company}}', 'analytics_module_activated', $this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%company}}', 'analytics_module_activated');
    }
}
