<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200908_114344_insert_service_subscribe_tariff extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%service_subscribe_tariff}}', [
            'id' => 71,
            'tariff_group_id' => 6,
            'duration_month' => 4,
            'duration_day' => 0,
            'price' => 2000,
            'label' => null,
            'is_active' => 1,
            'proposition' => 'Получи скидку 10% при оплате за год',
            'tariff_limit' => null,
            'is_pay_extra' => 0,
            'pay_extra_for' => null,
        ]);

        $this->update('{{%service_subscribe_tariff}}', [
            'is_active' => 0,
        ], [
            'id' => 16,
        ]);

        $this->execute('
            INSERT INTO {{service_discount_by_quantity}} (
                [[tariff_id]],
                [[quantity]],
                [[percent]]
            )
            SELECT
                71,
                {{t}}.[[quantity]],
                {{t}}.[[percent]]
            FROM {{service_discount_by_quantity}} AS {{t}}
            WHERE {{t}}.[[tariff_id]] = 16
        ');
    }

    public function safeDown()
    {
        $this->delete('{{%service_discount_by_quantity}}', [
            'tariff_id' => 71,
        ]);

        $this->delete('{{%service_subscribe_tariff}}', [
            'id' => 71,
        ]);

        $this->update('{{%service_subscribe_tariff}}', [
            'is_active' => 1,
        ], [
            'id' => 16,
        ]);
    }
}
