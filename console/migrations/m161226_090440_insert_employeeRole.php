<?php

use console\components\db\Migration;
use yii\db\Schema;

class m161226_090440_insert_employeeRole extends Migration
{
    public function safeUp()
    {
        $this->insert('employee_role', [
            'id' => 5,
            'name' => 'Руководитель отдела',
        ]);
    }
    
    public function safeDown()
    {
        $this->delete('employee_role', ['id' => 5]);
    }
}


