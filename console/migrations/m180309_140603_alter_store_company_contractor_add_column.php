<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180309_140603_alter_store_company_contractor_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('store_company_contractor', 'notified', $this->boolean()->defaultValue(false)->after('status'));
    }

    public function safeDown()
    {
        $this->dropColumn('store_company_contractor', 'notified');
    }
}
