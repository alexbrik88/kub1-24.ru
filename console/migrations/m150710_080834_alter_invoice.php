<?php

use yii\db\Schema;
use yii\db\Migration;

class m150710_080834_alter_invoice extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('invoice', 'total_amount_no_nds', Schema::TYPE_BIGINT);
        $this->alterColumn('invoice', 'total_amount_with_nds', Schema::TYPE_BIGINT);
        $this->alterColumn('invoice', 'total_amount_nds', Schema::TYPE_BIGINT);
        $this->alterColumn('invoice', 'payment_partial_amount', Schema::TYPE_BIGINT);
    }
    
    public function safeDown()
    {
        $this->alterColumn('invoice', 'total_amount_no_nds', Schema::TYPE_BIGINT);
        $this->alterColumn('invoice', 'total_amount_with_nds', Schema::TYPE_BIGINT);
        $this->alterColumn('invoice', 'total_amount_nds', Schema::TYPE_BIGINT);
        $this->alterColumn('invoice', 'payment_partial_amount', Schema::TYPE_BIGINT);
    }
}
