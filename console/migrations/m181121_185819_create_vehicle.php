<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181121_185819_create_vehicle extends Migration
{
    public $tVehicle = 'vehicle';
    public $tVehicleType = 'vehicle_type';
    public $tFuelType = 'fuel_type';
    public $tBodyType = 'body_type';

    public function safeUp()
    {
        $this->createTable($this->tVehicleType, [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);

        $this->batchInsert($this->tVehicleType, ['id', 'name'], [
            [1, 'Тягач'],
            [2, 'Полуприцеп'],
            [3, 'Фургон'],
            [4, 'Прицеп'],
        ]);

        $this->createTable($this->tFuelType, [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);

        $this->batchInsert($this->tFuelType, ['id', 'name'], [
            [1, 'АИ-80'],
            [2, 'АИ-92'],
            [3, 'АИ-95'],
            [4, 'АИ-98'],
            [5, 'ДТ'],
            [6, 'Метан'],
            [7, 'Пропан'],
            [8, 'Пропан-Бутан'],
        ]);

        $this->createTable($this->tBodyType, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->defaultValue(null),
            'name' => $this->string()->notNull(),
        ]);

        $this->batchInsert($this->tBodyType, ['id', 'company_id', 'name'], [
            [1, null, 'Борт'],
            [2, null, 'Изометрический'],
            [3, null, 'Контейнер'],
            [4, null, 'Рефрижератор'],
            [5, null, 'Тент бок'],
            [6, null, 'Тент верх'],
            [7, null, 'Тент задняя загрузка'],
            [8, null, 'Тент полная растентовка'],
            [9, null, 'Трал'],
            [10, null, 'Фургон'],
            [11, null, 'Цельнометалический'],
            [12, null, 'Легковой автомобиль'],
            [13, null, 'Самосвал'],
            [14, null, 'Кран-борт'],
            [15, null, 'Тент'],
            [16, null, 'Цистерна'],
            [17, null, 'Низкорамная платформа'],
            [18, null, 'Седельный тягач'],
            [19, null, 'Тент зад и бок'],
            [20, null, 'Автовоз'],
            [21, null, 'Эвакуатор'],
            [22, null, 'Коневоз'],
            [23, null, 'Тушевоз'],
            [24, null, 'Контейнерная площадка'],
            [25, null, 'Кран'],
            [26, null, 'Погрузчик'],
            [27, null, 'Экскаватор-погрузчик'],
            [28, null, 'Шаланда'],
            [29, null, 'Бортовая'],
            [30, null, 'Вышка'],
            [31, null, 'Манипулятор'],
            [32, null, 'Гидробур'],
            [33, null, 'Каток'],
            [34, null, 'Гидромолот'],
            [35, null, 'Ассенизаторская машина'],
            [36, null, 'Миксер'],
            [37, null, 'Сцепка'],
            [38, null, 'Площадка'],
            [39, null, 'Тонар'],
            [40, null, 'Панелевоз'],
        ]);

        $this->addForeignKey($this->tBodyType . '_company_id', $this->tBodyType, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');

        $this->createTable($this->tVehicle, [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'author_id' => $this->integer()->notNull(),
            'number' => $this->string()->notNull(),
            'date' => $this->date()->notNull(),
            'is_own' => $this->boolean()->defaultValue(false),
            'vehicle_type_id' => $this->integer()->notNull(),
            'fuel_type_id' => $this->integer()->defaultValue(null),
            'fuel_consumption' => $this->string()->defaultValue(null),
            'initial_mileage_date' => $this->date()->defaultValue(null),
            'initial_mileage' => $this->string()->defaultValue(null),
            'current_mileage' => $this->string()->defaultValue(null),
            'contractor_id' => $this->integer()->defaultValue(null),
            'driver_id' => $this->integer()->defaultValue(null),
            'model' => $this->string()->notNull(),
            'state_number' => $this->string()->notNull(),
            'is_state_number_russian' => $this->boolean()->defaultValue(false),
            'color' => $this->string()->defaultValue(null),
            'semitrailer_type_id' => $this->integer()->defaultValue(null),
            'trailer_type_id' => $this->integer()->defaultValue(null),
            'body_type_id' => $this->integer()->defaultValue(null),
            'tonnage' => $this->string()->defaultValue(null),
            'weight' => $this->string()->defaultValue(null),
            'length' => $this->string()->defaultValue(null),
            'width' => $this->string()->defaultValue(null),
            'height' => $this->string()->defaultValue(null),
            'volume' => $this->string()->defaultValue(null),
            'comment' => $this->text()->defaultValue(null),
        ]);

        $this->addForeignKey($this->tVehicle . '_company_id', $this->tVehicle, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tVehicle . '_author_id', $this->tVehicle, 'author_id', 'employee', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tVehicle . '_vehicle_type_id', $this->tVehicle, 'vehicle_type_id', $this->tVehicleType, 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tVehicle . '_fuel_type_id', $this->tVehicle, 'fuel_type_id', $this->tFuelType, 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tVehicle . '_contractor_id', $this->tVehicle, 'contractor_id', 'contractor', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tVehicle . '_driver_id', $this->tVehicle, 'driver_id', 'driver', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tVehicle . '_semitrailer_type_id', $this->tVehicle, 'semitrailer_type_id', $this->tVehicle, 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tVehicle . '_trailer_type_id', $this->tVehicle, 'trailer_type_id', $this->tVehicle, 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tVehicle . '_body_type_id', $this->tVehicle, 'body_type_id', $this->tBodyType, 'id', 'RESTRICT', 'CASCADE');

        $this->addForeignKey('driver_vehicle_id', 'driver', 'vehicle_id', $this->tVehicle, 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey($this->tVehicle . '_company_id', $this->tVehicle);
        $this->dropForeignKey($this->tVehicle . '_author_id', $this->tVehicle);
        $this->dropForeignKey($this->tVehicle . '_vehicle_type_id', $this->tVehicle);
        $this->dropForeignKey($this->tVehicle . '_fuel_type_id', $this->tVehicle);
        $this->dropForeignKey($this->tVehicle . '_contractor_id', $this->tVehicle);
        $this->dropForeignKey($this->tVehicle . '_driver_id', $this->tVehicle);
        $this->dropForeignKey($this->tVehicle . '_semitrailer_type_id', $this->tVehicle);
        $this->dropForeignKey($this->tVehicle . '_trailer_type_id', $this->tVehicle);
        $this->dropForeignKey($this->tVehicle . '_body_type_id', $this->tVehicle);

        $this->dropForeignKey('driver_vehicle_id', 'driver');

        $this->dropTable($this->tVehicle);
        $this->dropTable($this->tVehicleType);
        $this->dropTable($this->tFuelType);
        $this->dropTable($this->tBodyType);
    }
}


