<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210719_143903_update_company_info_industry extends Migration
{
    public function safeUp()
    {
        $this->update('{{%company_info_industry}}', [
            'name' => 'Интернет магазин и оффлайн магазин',
        ], [
            'id' => 4,
        ]);
    }

    public function safeDown()
    {
        $this->update('{{%company_info_industry}}', [
            'name' => 'Интернет магазины и оффлайн магазины',
        ], [
            'id' => 4,
        ]);
    }
}
