<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200616_174622_alter_user_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_config', 'table_view_report_abc', $this->integer()->notNull()->defaultValue(1));
        $this->addColumn('user_config', 'report_abc_article', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn('user_config', 'report_abc_cost_price', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn('user_config', 'report_abc_group_margin', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn('user_config', 'report_abc_number_of_sales', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn('user_config', 'report_abc_average_check', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn('user_config', 'report_abc_comment', $this->boolean()->notNull()->defaultValue(true));
    }

    public function safeDown()
    {
        $this->dropColumn('user_config', 'table_view_report_abc');
        $this->dropColumn('user_config', 'report_abc_article');
        $this->dropColumn('user_config', 'report_abc_cost_price');
        $this->dropColumn('user_config', 'report_abc_group_margin');
        $this->dropColumn('user_config', 'report_abc_number_of_sales');
        $this->dropColumn('user_config', 'report_abc_average_check');
        $this->dropColumn('user_config', 'report_abc_comment');
    }
}
