<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180831_134637_alter_employee_create_index extends Migration
{
    public function safeUp()
    {
        $this->renameColumn('employee', 'api_key', 'access_token');
        $this->createIndex('access_token', 'employee', 'access_token', true);
    }

    public function safeDown()
    {
        $this->dropIndex('access_token', 'employee');
        $this->renameColumn('employee', 'access_token', 'api_key');
    }
}
