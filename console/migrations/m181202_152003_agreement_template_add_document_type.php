<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181202_152003_agreement_template_add_document_type extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%agreement_template}}', 'document_name', $this->string(255));
        $this->alterColumn('{{%agreement_template}}', 'document_number', $this->string(255));
        $this->alterColumn('{{%agreement_template}}', 'payment_delay', $this->integer(11));

        $this->addColumn('{{%agreement_template}}', 'document_type_id', $this->integer()->notNull()->defaultValue(1) . ' AFTER [[document_name]]');

        $this->addForeignKey('FK_agreementTemplate_agreementType', '{{%agreement_template}}', 'document_type_id', '{{%agreement_type}}', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_agreementTemplate_agreementType', '{{%agreement_template}}');

        $this->dropColumn('{{%agreement_template}}', 'document_type_id');

        $this->alterColumn('{{%agreement_template}}', 'document_name', $this->string(255)->notNull());
        $this->alterColumn('{{%agreement_template}}', 'document_number', $this->string(255)->notNull());
        $this->alterColumn('{{%agreement_template}}', 'payment_delay', $this->integer(11)->notNull());
    }
}


