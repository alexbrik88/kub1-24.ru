<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200727_220004_add_last_acquiring_column_to_company extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%employee_company}}', 'last_acquiring_id', $this->integer()->defaultValue(null));
        $this->addForeignKey('employee_company_to_acquiring', 'employee_company', 'last_acquiring_id', 'acquiring', 'id', 'SET NULL', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('employee_company_to_acquiring', 'employee_company');
        $this->dropColumn('{{%employee_company}}', 'last_acquiring_id');
    }
}
