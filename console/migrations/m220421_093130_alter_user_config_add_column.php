<?php

use console\components\db\Migration;
use yii\db\Schema;

class m220421_093130_alter_user_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%user_config}}', 'packing_list_scan', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn('{{%user_config}}', 'upd_scan', $this->boolean()->notNull()->defaultValue(true));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%user_config}}', 'upd_scan');
        $this->dropColumn('{{%user_config}}', 'packing_list_scan');
    }
}
