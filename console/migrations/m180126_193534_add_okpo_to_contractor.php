<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180126_193534_add_okpo_to_contractor extends Migration
{
    public $tableName = 'contractor';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'okpo', $this->string()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'okpo');
    }
}


