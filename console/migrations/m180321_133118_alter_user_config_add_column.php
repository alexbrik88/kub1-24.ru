<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180321_133118_alter_user_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_config', 'product_article', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn('user_config', 'invoice_form_article', $this->boolean()->notNull()->defaultValue(false));

        $this->addCommentOnColumn('user_config', 'product_article', 'Показ колонки "Артикул" в списке товаров');
        $this->addCommentOnColumn('user_config', 'invoice_form_article', 'Показ колонки "Артикул" в форме счета');
    }

    public function safeDown()
    {
        $this->dropColumn('user_config', 'product_article');
        $this->dropColumn('user_config', 'invoice_form_article');
    }
}
