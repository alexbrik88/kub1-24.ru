<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180206_141418_add_additional_expenses_month extends Migration
{
    public $tableName = 'project';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'additional_expenses_month', $this->integer()->defaultValue(null));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'additional_expenses_month');
    }
}


