<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170606_215815_add_production_type_to_order_document_order extends Migration
{
    public $tableName = 'uploaded_document_order';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'production_type_id', $this->integer()->defaultValue(null));
        $this->addForeignKey($this->tableName . '_production_type_id', $this->tableName, 'production_type_id', 'production_type', 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_production_type_id', $this->tableName);
        $this->dropColumn($this->tableName, 'production_type_id');
    }
}


