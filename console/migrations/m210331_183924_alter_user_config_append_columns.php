<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210331_183924_alter_user_config_append_columns extends Migration
{
    public $tableName = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'project_estimate_name', $this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'project_estimate_name');
    }
}
