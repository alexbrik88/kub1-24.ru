<?php

use yii\db\Schema;
use yii\db\Migration;

class m150729_093055_alter_report_addColumn_author_id extends Migration
{
    public function safeUp()
    {
        $this->addColumn('report', 'author_id', Schema::TYPE_INTEGER . ' NULL AFTER created_at');

        $this->addForeignKey('fk_report_to_author', 'report', 'author_id', 'employee', 'id');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('fk_report_to_author', 'report');
        $this->dropColumn('report', 'author_id');
    }
}
