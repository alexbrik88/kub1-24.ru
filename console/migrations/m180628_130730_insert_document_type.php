<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180628_130730_insert_document_type extends Migration
{
    public function safeUp()
    {
        $this->insert('document_type', [
            'id' => 2,
            'name' => 'Приказ',
            'name2' => 'Приказу',
        ]);
    }

    public function safeDown()
    {
        $this->delete('document_type', ['id' => 2]);
    }
}
