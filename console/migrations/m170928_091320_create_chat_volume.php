<?php

use console\components\db\Migration;
use yii\db\Schema;
use common\models\employee\Employee;
use common\models\EmployeeCompany;

class m170928_091320_create_chat_volume extends Migration
{
    public $tableName = 'chat_volume';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'from_employee_id' => $this->integer()->notNull(),
            'to_employee_id' => $this->integer()->notNull(),
            'has_volume' => $this->boolean()->defaultValue(true),
        ]);

        $this->addForeignKey($this->tableName . '_company_id', $this->tableName, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tableName . '_from_employee_id', $this->tableName, 'from_employee_id', 'employee', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tableName . '_to_employee_id', $this->tableName, 'to_employee_id', 'employee', 'id', 'RESTRICT', 'CASCADE');
        $id = 0;
        /* @var $employeeCompany EmployeeCompany */
        foreach (EmployeeCompany::find()->all() as $employeeCompany) {
            /* @var $employeeCompanyFriend EmployeeCompany */
            foreach (EmployeeCompany::find()
                         ->andWhere(['company_id' => $employeeCompany->company_id])
                         ->andWhere(['not', ['employee_id' => $employeeCompany->employee_id]])
                         ->all()
                     as $employeeCompanyFriend) {
                $id++;
                $this->insert($this->tableName, [
                    'id' => $id,
                    'company_id' => $employeeCompany->company_id,
                    'from_employee_id' => $employeeCompany->employee_id,
                    'to_employee_id' => $employeeCompanyFriend->employee_id,
                    'has_volume' => true,
                ]);
            }
        }
    }

    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_company_id', $this->tableName);
        $this->dropForeignKey($this->tableName . '_from_employee_id', $this->tableName);
        $this->dropForeignKey($this->tableName . '_to_employee_id', $this->tableName);

        $this->dropTable($this->tableName);
    }
}


