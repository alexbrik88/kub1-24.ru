<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210628_112750_create_table_goods_cancellation extends Migration
{
    public function safeUp()
    {
        // status table
        $this->createTable('goods_cancellation_status', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . ' NOT NULL',
        ]);

        $this->batchInsert('goods_cancellation_status', ['id', 'name'], [[1, 'Создан'], [2, 'Распечатан'], [3, 'Передан']]);

        // table
        $this->createTable('goods_cancellation', [
            'id' => Schema::TYPE_PK,

            'company_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'store_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'contractor_id' => Schema::TYPE_INTEGER . ' NULL',
            'project_id' => Schema::TYPE_INTEGER . ' NULL',
            'project_estimate_id' => Schema::TYPE_INTEGER . ' NULL',
            'expenditure_item_id' => Schema::TYPE_INTEGER . ' NULL',
            'profit_loss_type' => Schema::TYPE_INTEGER . ' NOT NULL',

            'document_date' => Schema::TYPE_DATE . ' NOT NULL',
            'document_number' => Schema::TYPE_INTEGER . ' NULL',
            'document_additional_number' => Schema::TYPE_STRING . '(45) NULL',

            'status_id' => Schema::TYPE_INTEGER . ' NOT NULL DEFAULT 1',
            'nds_view_type_id' => Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 2',
            'price_precision' => "ENUM('2', '4') NOT NULL DEFAULT '2'",
            'view_total_base' => Schema::TYPE_BIGINT . '(20) DEFAULT 0',
            'view_total_amount' => Schema::TYPE_BIGINT . '(20) DEFAULT 0',
            'view_total_no_nds' => Schema::TYPE_BIGINT . '(20) DEFAULT 0',
            'view_total_nds' => Schema::TYPE_BIGINT . '(20) DEFAULT 0',
            'view_total_with_nds' => Schema::TYPE_BIGINT . '(20) DEFAULT 0',
            'total_amount_with_nds' => Schema::TYPE_BIGINT . '(20) DEFAULT 0',
            'total_amount_nds' => Schema::TYPE_BIGINT . '(20) DEFAULT 0',
            'total_amount_no_nds' => Schema::TYPE_BIGINT . '(20) DEFAULT 0',
            'total_amount_has_nds' => Schema::TYPE_TINYINT . '(1) DEFAULT 0',
            'total_order_count' => Schema::TYPE_INTEGER . '(11) DEFAULT 0',

            'signed_by_employee_id' => Schema::TYPE_INTEGER . ' NULL',
            'signed_by_name' => Schema::TYPE_STRING . '(255) NULL',
            'sign_document_type_id' => Schema::TYPE_INTEGER . ' NULL',
            'sign_document_number' => Schema::TYPE_STRING . '(50) NULL',
            'signed_document_date' => Schema::TYPE_DATE . ' NULL',
            'signature_id' => Schema::TYPE_INTEGER . ' NULL',
            'print_id' => Schema::TYPE_INTEGER . ' NULL',
            'chief_signature_id' => Schema::TYPE_INTEGER . ' NULL',
            'chief_accountant_signature_id' => Schema::TYPE_INTEGER . ' NULL',

            'document_author_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'created_at' => Schema::TYPE_INTEGER . ' NOT NULL',

            'comment' => Schema::TYPE_STRING . '(1000) NULL'
        ]);

        // fk: company + store + contractor + project + author + expenditureItem
        $this->addForeignKey('fk_goods_cancellation_to_company', 'goods_cancellation', 'company_id', 'company', 'id', 'CASCADE');
        $this->addForeignKey('fk_goods_cancellation_to_store', 'goods_cancellation', 'store_id', 'store', 'id', 'CASCADE');
        $this->addForeignKey('fk_goods_cancellation_to_contractor', 'goods_cancellation', 'contractor_id', 'contractor', 'id', 'CASCADE');
        $this->addForeignKey('fk_goods_cancellation_to_project', 'goods_cancellation', 'project_id', 'project', 'id', 'SET NULL');
        $this->addForeignKey('fk_goods_cancellation_to_project_estimate', 'goods_cancellation', 'project_estimate_id', 'project_estimate', 'id', 'SET NULL');
        $this->addForeignKey('fk_goods_cancellation_to_document_author', 'goods_cancellation', 'document_author_id', 'employee', 'id');
        $this->addForeignKey('fk_goods_cancellation_to_invoice_expenditure_item', 'goods_cancellation', 'expenditure_item_id', 'invoice_expenditure_item', 'id', 'CASCADE', 'CASCADE');

        // fk: signature + print
        $this->addForeignKey('fk_goods_cancellation_sign_employee', 'goods_cancellation', 'signed_by_employee_id', 'employee', 'id');
        $this->addForeignKey('fk_goods_cancellation_sign_document_type', 'goods_cancellation', 'sign_document_type_id', 'document_type', 'id');
        $this->addForeignKey('fk_goods_cancellation_employee_signature', 'goods_cancellation', 'signature_id', 'employee_signature', 'id');
        $this->addForeignKey('fk_goods_cancellation_company_details_chief_signature', 'goods_cancellation', 'chief_signature_id', 'company_details_file', 'id');
        $this->addForeignKey('fk_goods_cancellation_company_details_chief_accountant_signature', 'goods_cancellation', 'chief_accountant_signature_id', 'company_details_file', 'id');
        $this->addForeignKey('fk_goods_cancellation_company_details_print', 'goods_cancellation', 'print_id', 'company_details_file', 'id');

        // fk: status
        $this->addForeignKey('fk_goods_cancellation_status', 'goods_cancellation', 'status_id', 'goods_cancellation_status', 'id');
    }


    public function safeDown()
    {
        // fk: signature + print
        $this->dropForeignKey('fk_goods_cancellation_sign_employee', 'goods_cancellation');
        $this->dropForeignKey('fk_goods_cancellation_sign_document_type', 'goods_cancellation');
        $this->dropForeignKey('fk_goods_cancellation_employee_signature', 'goods_cancellation');
        $this->dropForeignKey('fk_goods_cancellation_company_details_chief_signature', 'goods_cancellation');
        $this->dropForeignKey('fk_goods_cancellation_company_details_chief_accountant_signature', 'goods_cancellation');
        $this->dropForeignKey('fk_goods_cancellation_company_details_print', 'goods_cancellation');

        // fk: company + store + contractor + project + author
        $this->dropForeignKey('fk_goods_cancellation_to_company', 'goods_cancellation');
        $this->dropForeignKey('fk_goods_cancellation_to_store', 'goods_cancellation');
        $this->dropForeignKey('fk_goods_cancellation_to_contractor', 'goods_cancellation');
        $this->dropForeignKey('fk_goods_cancellation_to_project', 'goods_cancellation');
        $this->dropForeignKey('fk_goods_cancellation_to_project_estimate', 'goods_cancellation');
        $this->dropForeignKey('fk_goods_cancellation_to_document_author', 'goods_cancellation');
        $this->dropForeignKey('fk_goods_cancellation_to_invoice_expenditure_item', 'goods_cancellation');

        // fk: status
        $this->dropForeignKey('fk_goods_cancellation_status', 'goods_cancellation');

        // table
        $this->dropTable('goods_cancellation');

        // status table
        $this->dropTable('goods_cancellation_status');
    }
}
