<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210804_081008_alter_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_config', 'table_view_detailing', $this->boolean()->notNull()->defaultValue(true));
    }

    public function safeDown()
    {
        $this->dropColumn('user_config', 'table_view_detailing');
    }
}
