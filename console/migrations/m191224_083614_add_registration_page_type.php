<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191224_083614_add_registration_page_type extends Migration
{
    public $tableName = '{{%registration_page_type}}';

    public function safeUp()
    {
        $this->insert($this->tableName, [
            'id' => 27,
            'name' => 'Лэндинг бухгалтерия ИП/Сбербанк',
        ]);
    }

    public function safeDown()
    {
        $this->delete($this->tableName, ['id' => 27]);
    }
}
