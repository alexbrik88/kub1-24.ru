<?php

use yii\db\Migration;

/**
 * Handles the creation for table `bank`.
 */
class m160729_205152_create_bank extends Migration
{
    public $tBank = 'bank';

    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable($this->tBank, [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'bank_name' => $this->string()->notNull(),
            'description' => $this->text()->defaultValue(null),
            'url' => $this->string()->defaultValue(null),
            'email' => $this->string()->defaultValue(null),
            'phone' => $this->string(130)->defaultValue(null),
            'contact_person' => $this->string()->defaultValue(null),
            'logo_link' => $this->string()->defaultValue(null),
            'request_count' => $this->integer()->defaultValue(0),
            'is_blocked' => $this->boolean()->defaultValue(0),
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable($this->tBank);
    }
}
