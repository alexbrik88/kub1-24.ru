<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190626_114905_insert_registration_page_type extends Migration
{
    public function safeUp()
    {
        $this->batchInsert('registration_page_type', ['id', 'name'], [
            [21, 'Нулевка шаблоны'],
            [22, 'Нулевка лендинг'],
        ]);
    }

    public function safeDown()
    {
        $this->delete('registration_page_type', ['id' => [21, 22]]);
    }
}
