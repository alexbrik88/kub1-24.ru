<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180918_044748_alter_payment_order_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('payment_order', 'company_name', $this->string()->after('invoice_id'));
        $this->addColumn('payment_order', 'company_inn', $this->string()->after('company_name'));
        $this->addColumn('payment_order', 'company_kpp', $this->string()->after('company_inn'));
    }

    public function safeDown()
    {
        $this->dropColumn('payment_order', 'company_name');
        $this->dropColumn('payment_order', 'company_inn');
        $this->dropColumn('payment_order', 'company_kpp');
    }
}
