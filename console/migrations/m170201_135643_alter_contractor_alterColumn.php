<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170201_135643_alter_contractor_alterColumn extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%contractor}}', 'name', $this->string(255)->notNull());
    }
    
    public function safeDown()
    {
        $this->alterColumn('{{%contractor}}', 'name', $this->string(45)->notNull());
    }
}


