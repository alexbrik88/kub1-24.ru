<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210513_172250_alter_config_add_columns extends Migration
{
    public $table = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->table, 'table_retail_sales_view', $this->tinyInteger()->notNull()->defaultValue(0));
        $this->addColumn($this->table, 'table_retail_sales_sort', $this->tinyInteger()->notNull()->defaultValue(0));
        $this->addColumn($this->table, 'table_retail_sales_zeroes', $this->tinyInteger()->notNull()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'table_retail_sales_view');
        $this->dropColumn($this->table, 'table_retail_sales_sort');
        $this->dropColumn($this->table, 'table_retail_sales_zeroes');
    }
}