<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160921_112519_insert_company_type extends Migration
{
    public $tCompanyType = 'company_type';

    public function safeUp()
    {
        $this->delete($this->tCompanyType, ['id' => 9]);

        $this->batchInsert($this->tCompanyType, ['id', 'name_short', 'name_full', 'in_company', 'in_contractor'], [
            [9, 'ННО', 'Негосударственная некоммерческая организация', 0, 1],
            [10, 'ФГКУ', 'Федеральное Государственное Казённое Учрежден', 0, 1],
            [11, 'НО', 'Некоммерческая Организация', 0, 1],
            [12, 'УФК', 'Управление Федерального Казначейства', 0, 0],
        ]);
    }

    public function safeDown()
    {
        $this->delete($this->tCompanyType, ['in', 'id', [9, 10, 11]]);
    }
}


