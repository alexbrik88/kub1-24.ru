<?php

use common\models\company\CompanyAffiliateLink;
use common\models\service\ServiceModule;
use console\components\db\Migration;
use yii\db\Expression;
use yii\db\Query;
use yii\db\Schema;

class m200812_115807_company_affiliate_link_recalculate_module_id extends Migration
{
    public $tableName = 'company_affiliate_link';

    public function safeUp()
    {
        $companyAffiliateLinks = (new Query())
            ->select([
                'id',
                'service_module_id',
                'module_id',
            ])
            ->from(CompanyAffiliateLink::tableName())
            ->andWhere(['IS', 'module_id', new Expression('NULL')])
            ->all();

        foreach ($companyAffiliateLinks as $companyAffiliateLink) {
            $moduleIndex = array_search($companyAffiliateLink['service_module_id'], ServiceModule::$serviceModuleArray);
            $this->update($this->tableName, ['module_id' => $moduleIndex], ['id' => $companyAffiliateLink['id']]);
        }
    }

    public function safeDown()
    {
        return true;
    }
}
