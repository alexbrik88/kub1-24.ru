<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170712_215413_add_affiliate_additional_fields extends Migration
{
    public $tableName = 'company';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'total_affiliate_sum', $this->integer()->defaultValue(0));
        $this->addColumn($this->tableName, 'payed_reward_for_kub', $this->integer()->defaultValue(0));
        $this->addColumn($this->tableName, 'payed_reward_for_rs', $this->integer()->defaultValue(0));
        $this->addColumn($this->tableName, 'payed_reward_for_card', $this->integer()->defaultValue(0));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'total_affiliate_sum');
        $this->dropColumn($this->tableName, 'payed_reward_for_kub');
        $this->dropColumn($this->tableName, 'payed_reward_for_rs');
        $this->dropColumn($this->tableName, 'payed_reward_for_card');
    }
}


