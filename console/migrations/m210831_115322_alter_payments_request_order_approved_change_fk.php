<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210831_115322_alter_payments_request_order_approved_change_fk extends Migration
{
    public $table = 'payments_request_order_approved';

    public function safeUp()
    {
        $table = $this->table;
        $this->dropForeignKey("FK_{$table}_to_order", $table);
        $this->addForeignKey("FK_{$table}_to_order", $table, 'order_id', 'payments_request_order', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $table = $this->table;
        $this->dropForeignKey("FK_{$table}_to_order", $table);
        $this->addForeignKey("FK_{$table}_to_order", $table, 'order_id', 'payments_request_order', 'id');

    }
}
