<?php

use common\models\evotor\Receipt;
use console\components\db\Migration;

class m191009_160100_create_evotor_receipt extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{evotor_receipt}}', [
            'id' => "BINARY(16) NOT NULL PRIMARY KEY COMMENT 'UUID Эвотора'",
            'employee_id' => $this->integer()->notNull()->comment('ID покупателя сайта'),
            'device_id' => "BINARY(16) NOT NULL COMMENT 'ID терминала'",
            'store_id' => "BINARY(16) NOT NULL COMMENT 'ID магазина'",
            'date_time' => $this->integer()->unsigned()->notNull(),
            'type' => "ENUM('" . implode("','", Receipt::TYPE) . "') NOT NULL DEFAULT '" . Receipt::TYPE_SELL . "' COMMENT 'Тип документа'",
            'evotor_employee_id' => "BINARY(16) COMMENT 'UUID клиента Эвотор'",
            'total_tax' => $this->float()->unsigned()->notNull()->defaultValue(0)->comment('Сумма налога'),
            'total_discount' => $this->float()->unsigned()->notNull()->defaultValue(0)->comment('Сумма скидки'),
            'total_amount' => $this->float()->unsigned()->notNull()->defaultValue(0)->comment('Сумма документа'),
        ], "COMMENT 'Интеграция Эвотор: чеки'");
        $this->addForeignKey(
            'evotor_receipt_employee_id', '{{evotor_receipt}}', 'employee_id',
            '{{employee}}', 'id', 'CASCADE', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropTable('{{evotor_receipt}}');
    }
}
