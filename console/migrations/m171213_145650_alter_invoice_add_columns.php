<?php

use console\components\db\Migration;
use yii\db\Schema;

class m171213_145650_alter_invoice_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('invoice', 'comment', $this->string(1000));
        $this->addColumn('invoice', 'comment_internal', $this->string(255));
    }

    public function safeDown()
    {
        $this->dropColumn('invoice', 'comment');
        $this->dropColumn('invoice', 'comment_internal');
    }
}
