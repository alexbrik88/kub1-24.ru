<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200621_182618_alter_rent_attribute_add_column extends Migration
{
    public $table = 'rent_attribute';

    public function safeUp()
    {
        $this->addColumn($this->table, 'tab', $this->char(20)->notNull());
        $this->addColumn($this->table, 'show', $this->tinyInteger(1)->notNull()->defaultValue(1));

        $this->update($this->table, ['tab' => 'info']);
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'show');
        $this->dropColumn($this->table, 'tab');
    }
}
