<?php

use console\components\db\Migration;
use yii\db\Expression;
use yii\db\Schema;

class m200817_151650_set_empty_value_name_in_company_affiliate_link extends Migration
{
    public $tableName = 'company_affiliate_link';

    public function safeUp()
    {
        $this->update($this->tableName, ['name' => (new Expression('NULL'))]);
    }

    public function safeDown()
    {
        return true;
    }
}
