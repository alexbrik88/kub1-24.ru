<?php

use yii\db\Schema;
use yii\db\Migration;

class m150519_111032_create_notification extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('notification', [
            'id' => Schema::TYPE_PK,
            'notification_type' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "1 - налоговое, 0 - системное"',
            'event_date' => Schema::TYPE_DATE . ' COMMENT "Дата наступления события"',
            'activation_date' => Schema::TYPE_DATE . ' COMMENT "Дата активизиции уведомления"',
            'text' => Schema::TYPE_TEXT,
        ], $tableOptions);
    }
    
    public function safeDown()
    {
        $this->dropTable('notification');
    }
}
