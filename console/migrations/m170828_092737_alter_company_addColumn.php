<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170828_092737_alter_company_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%company}}', 'chief_accountant_signature_link', $this->string()->defaultValue(null) . ' AFTER [[chief_signature_link]]');
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%company}}', 'chief_accountant_signature_link');
    }
}
