<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210328_181118_alter_invoice_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%invoice}}', 'invoice_income_item_id', $this->integer()->comment('статья приходов'));

        $this->execute('
            UPDATE `invoice`
            LEFT JOIN `contractor` ON `invoice`.`contractor_id` = `contractor`.`id`
            SET `invoice`.`invoice_income_item_id` = `contractor`.`invoice_income_item_id`
            WHERE `invoice`.`type` = 2
        ');

        $this->addForeignKey('invoice_to_invoice_income_item', '{{%invoice}}', 'invoice_income_item_id', 'invoice_income_item', 'id', 'SET NULL');
    }

    public function safeDown()
    {
        $this->dropForeignKey('invoice_to_invoice_income_item', '{{%invoice}}');
        $this->dropColumn('{{%invoice}}', 'invoice_income_item_id');
    }
}
