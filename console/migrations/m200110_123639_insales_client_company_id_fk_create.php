<?php

use console\components\db\Migration;

class m200110_123639_insales_client_company_id_fk_create extends Migration
{
    const TABLE = '{{%insales_client}}';

    public function up()
    {
        $this->addForeignKey(
            'insales_client_company_id', self::TABLE, 'company_id',
            '{{%company}}', 'id',
            'CASCADE', 'CASCADE'
        );
    }

    public function down()
    {
        $this->dropForeignKey('insales_client_company_id', self::TABLE);
    }
}
