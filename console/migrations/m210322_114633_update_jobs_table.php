<?php

use yii\db\Migration;

class m210322_114633_update_jobs_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%jobs}}';

    /** @var int[] */
    private const UNUSED_TYPES = [3, 4, 5];

    /**
     * @inheritDoc
     * @throws
     */
    public function safeUp()
    {
        $this->db->createCommand()->delete(self::TABLE_NAME, ['in', 'type', self::UNUSED_TYPES])->execute();
    }
}
