<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190521_092825_file_dir extends Migration
{
    public $tFile = 'file_dir';
    public $tEmployee = 'employee';
    public $tCompany = 'company';

    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable($this->tFile, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'name' => $this->string(255)->notNull(),
            'created_at' => $this->integer()->notNull(),
            'created_at_author_id' => $this->integer()->notNull(),
        ], $tableOptions);

        $this->addForeignKey('fk_company_id', $this->tFile, 'company_id', $this->tCompany, 'id');
        $this->addForeignKey('fk_author_id', $this->tFile, 'created_at_author_id', $this->tEmployee, 'id');
    }

    public function safeDown()
    {
        $this->dropTable($this->tFile);
    }
}
