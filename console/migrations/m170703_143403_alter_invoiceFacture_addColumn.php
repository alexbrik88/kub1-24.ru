<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170703_143403_alter_invoiceFacture_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{invoice_facture}}', 'state_contract', $this->string(50) . ' AFTER [[signature_id]]');
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{invoice_facture}}', 'state_contract');
    }
}


