<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180411_222004_update_payment extends Migration
{
    public $tServicePayment = 'service_payment';
    public $tServicePaymentOrder = 'service_payment_order';

    public function safeUp()
    {
        $this->alterColumn($this->tServicePayment, 'payment_for', "ENUM('subscribe', 'taxrobot', 'store_cabinet') NOT NULL DEFAULT 'subscribe' AFTER [[company_id]]");

        $this->addColumn($this->tServicePayment, 'store_tariff_id', $this->integer()->defaultValue(null));
        $this->addForeignKey($this->tServicePayment . '_store_tariff_id', $this->tServicePayment, 'store_tariff_id', 'service_store_tariff', 'id', 'RESTRICT', 'CASCADE');

        $this->addColumn($this->tServicePaymentOrder, 'store_tariff_id', $this->integer()->defaultValue(null));
        $this->addForeignKey($this->tServicePaymentOrder . '_store_tariff_id', $this->tServicePaymentOrder, 'store_tariff_id', 'service_store_tariff', 'id', 'RESTRICT', 'CASCADE');
    }
    
    public function safeDown()
    {
        $this->alterColumn($this->tServicePayment, 'payment_for', "ENUM('subscribe', 'taxrobot') NOT NULL DEFAULT 'subscribe' AFTER [[company_id]]");

        $this->dropForeignKey($this->tServicePayment . '_store_tariff_id', $this->tServicePayment);
        $this->dropColumn($this->tServicePayment, 'store_tariff_id');

        $this->dropForeignKey($this->tServicePaymentOrder . '_store_tariff_id', $this->tServicePaymentOrder);
        $this->dropColumn($this->tServicePaymentOrder, 'store_tariff_id');
    }
}


