<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210208_185417_add_show_waybill_button_in_invoice_column_to_company extends Migration
{
    public function safeUp()
    {
        $this->addColumn('company', 'show_waybill_button_in_invoice', $this->boolean()->unsigned()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('company', 'show_waybill_button_in_invoice');
    }
}
