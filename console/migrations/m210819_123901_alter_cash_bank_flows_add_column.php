<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210819_123901_alter_cash_bank_flows_add_column extends Migration
{
    public $table = 'cash_bank_flows';
    public $table2 = 'cash_bank_foreign_currency_flows';

    public function safeUp()
    {
        $this->addColumn($this->table, 'tin_child_amount', $this->bigInteger()->unsigned()->after('amount'));
        $this->addColumn($this->table, 'parent_id', $this->integer()->after('id'));
        $this->addForeignKey('FK_'.$this->table.'_to_parent', $this->table, 'parent_id', $this->table, 'id', 'CASCADE', 'CASCADE');

        $this->addColumn($this->table2, 'tin_child_amount', $this->bigInteger()->unsigned()->after('amount'));
        $this->addColumn($this->table2, 'parent_id', $this->integer()->after('id'));
        $this->addForeignKey('FK_'.$this->table2.'_to_parent', $this->table2, 'parent_id', $this->table2, 'id', 'CASCADE', 'CASCADE');        
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_'.$this->table.'_to_parent', $this->table);
        $this->dropColumn($this->table, 'parent_id');
        $this->dropColumn($this->table, 'tin_child_amount');

        $this->dropForeignKey('FK_'.$this->table2.'_to_parent', $this->table2);
        $this->dropColumn($this->table2, 'parent_id');
        $this->dropColumn($this->table2, 'tin_child_amount');        
    }
}
