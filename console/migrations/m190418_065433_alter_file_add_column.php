<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190418_065433_alter_file_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%file}}', 'owner_table', $this->string()->notNull()->after('owner_model'));

        $this->createIndex('owner_table', '{{%file}}', 'owner_table');

        $query = new \yii\db\Query;
        $classArray = $query->select('owner_model')->distinct()->from('{{%file}}')->column();
        foreach ($classArray as $className) {
            if (class_exists($className)) {
                $this->update('{{%file}}', [
                    'owner_table' => $className::tableName(),
                ], [
                    'owner_model' => $className,
                ]);
            }
        }
    }

    public function safeDown()
    {
        $this->dropIndex('owner_table', '{{%file}}');

        $this->dropColumn('{{%file}}', 'owner_table');
    }
}
