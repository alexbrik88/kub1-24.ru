<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180523_141832_alter_invoice_add_column extends Migration
{
    public function safeUp()
    {
        $this->execute("
            ALTER TABLE {{invoice}}
                ADD COLUMN [[view_total_discount]] BIGINT(20) NOT NULL DEFAULT '0' AFTER [[view_total_base]];
        ");

        $this->execute("
            UPDATE {{invoice}}
            SET {{invoice}}.[[view_total_discount]] = {{invoice}}.[[view_total_base]] - {{invoice}}.[[view_total_amount]];
        ");
    }

    public function safeDown()
    {
        $this->execute("
            ALTER TABLE {{invoice}}
                DROP COLUMN [[view_total_discount]];
        ");
    }
}
