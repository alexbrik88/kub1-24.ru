<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181128_211535_create_logistics_request extends Migration
{
    public $tLogisticsRequest = 'logistics_request';
    public $tLogisticsRequestForm = 'logistics_request_from';
    public $tLogisticsRequestCondition = 'logistics_request_condition';
    public $tLogisticsRequestAdditionalExpenses = 'logistics_request_additional_expenses';

    public function safeUp()
    {
        $this->createTable($this->tLogisticsRequest, [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'author_id' => $this->integer()->notNull(),
            'document_number' => $this->string()->notNull(),
            'document_date' => $this->date()->notNull(),

            'customer_id' => $this->integer()->notNull(),
            'customer_contact_person' => $this->string()->defaultValue(null),
            'customer_agreement_document_name' => $this->string()->defaultValue(null),
            'customer_agreement_document_number' => $this->string()->defaultValue(null),
            'customer_agreement_document_date' => $this->date()->defaultValue(null),
            'customer_agreement_document_type_id' => $this->integer()->defaultValue(null),
            'customer_request_number' => $this->string()->defaultValue(null),
            'customer_request_date' => $this->date()->defaultValue(null),
            'customer_rate' => $this->string()->notNull(),
            'customer_form_id' => $this->integer()->defaultValue(null),
            'customer_condition_id' => $this->integer()->defaultValue(null),
            'customer_delay' => $this->integer()->defaultValue(null),
            'customer_additional_expenses_id' => $this->integer()->defaultValue(null),
            'customer_amount' => $this->string()->defaultValue(null),
            'customer_request_essence' => $this->text()->defaultValue(null),

            'carrier_id' => $this->integer()->defaultValue(null),
            'carrier_contact_person' => $this->string()->defaultValue(null),
            'carrier_agreement_document_name' => $this->string()->defaultValue(null),
            'carrier_agreement_document_number' => $this->string()->defaultValue(null),
            'carrier_agreement_document_date' => $this->date()->defaultValue(null),
            'carrier_agreement_document_type_id' => $this->integer()->defaultValue(null),
            'carrier_request_number' => $this->string()->defaultValue(null),
            'carrier_request_date' => $this->date()->defaultValue(null),
            'driver_id' => $this->integer()->notNull(),
            'vehicle_id' => $this->integer()->notNull(),
            'carrier_rate' => $this->string()->defaultValue(null),
            'carrier_form_id' => $this->integer()->defaultValue(null),
            'carrier_condition_id' => $this->integer()->defaultValue(null),
            'carrier_delay' => $this->integer()->defaultValue(null),
            'carrier_additional_expenses_id' => $this->integer()->defaultValue(null),
            'carrier_amount' => $this->string()->defaultValue(null),
            'carrier_request_essence' => $this->text()->defaultValue(null),
        ]);

        $this->createTable($this->tLogisticsRequestForm, [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);

        $this->batchInsert($this->tLogisticsRequestForm, ['id', 'name'], [
            [1, 'Безналичный расчет, в т.ч. НДС'],
            [2, 'Безналичный расчет, без НДС'],
            [3, 'Наличный расчет'],
        ]);

        $this->createTable($this->tLogisticsRequestCondition, [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);

        $this->batchInsert($this->tLogisticsRequestCondition, ['id', 'name'], [
            [1, 'По факту погрузки'],
            [2, 'По факту разгрузки'],
            [3, 'Получение копии ТН, ТТН'],
            [4, 'Получение копии Акта'],
            [5, 'Получение оригинала ТН, ТТН'],
            [6, 'Получение оригинала Акта'],
        ]);

        $this->createTable($this->tLogisticsRequestAdditionalExpenses, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->defaultValue(null),
            'name' => $this->string()->notNull(),
        ]);

        $this->batchInsert($this->tLogisticsRequestAdditionalExpenses, ['id', 'company_id', 'name'], [
            [1, null, 'Простой машины на Погрузке'],
            [2, null, 'Простой машины на Разгрузке'],
            [3, null, 'Услуги грузчиков на Погрузке'],
            [4, null, 'Услуги грузчиков на Разгрузке'],
            [5, null, 'Опоздание'],
            [6, null, 'Страхование груза'],
            [7, null, 'Экспресс почта'],
        ]);

        $this->addForeignKey($this->tLogisticsRequestAdditionalExpenses . '_company_id', $this->tLogisticsRequestAdditionalExpenses, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');

        $this->addForeignKey($this->tLogisticsRequest . '_company_id', $this->tLogisticsRequest, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tLogisticsRequest . '_author_id', $this->tLogisticsRequest, 'author_id', 'employee', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tLogisticsRequest . '_customer_id', $this->tLogisticsRequest, 'customer_id', 'contractor', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tLogisticsRequest . '_customer_agreement_document_type_id', $this->tLogisticsRequest, 'customer_agreement_document_type_id', 'agreement_type', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tLogisticsRequest . '_customer_form_id', $this->tLogisticsRequest, 'customer_form_id', $this->tLogisticsRequestForm, 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tLogisticsRequest . '_customer_condition_id', $this->tLogisticsRequest, 'customer_condition_id', $this->tLogisticsRequestCondition, 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tLogisticsRequest . '_customer_additional_expenses_id', $this->tLogisticsRequest, 'customer_additional_expenses_id', $this->tLogisticsRequestAdditionalExpenses, 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tLogisticsRequest . '_carrier_id', $this->tLogisticsRequest, 'carrier_id', 'contractor', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tLogisticsRequest . '_carrier_agreement_document_type_id', $this->tLogisticsRequest, 'carrier_agreement_document_type_id', 'agreement_type', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tLogisticsRequest . '_carrier_form_id', $this->tLogisticsRequest, 'carrier_form_id', $this->tLogisticsRequestForm, 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tLogisticsRequest . '_carrier_condition_id', $this->tLogisticsRequest, 'carrier_condition_id', $this->tLogisticsRequestCondition, 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tLogisticsRequest . '_carrier_additional_expenses_id', $this->tLogisticsRequest, 'carrier_additional_expenses_id', $this->tLogisticsRequestAdditionalExpenses, 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey($this->tLogisticsRequestAdditionalExpenses . '_company_id', $this->tLogisticsRequestAdditionalExpenses);

        $this->dropForeignKey($this->tLogisticsRequest . '_company_id', $this->tLogisticsRequest);
        $this->dropForeignKey($this->tLogisticsRequest . '_author_id', $this->tLogisticsRequest);
        $this->dropForeignKey($this->tLogisticsRequest . '_customer_id', $this->tLogisticsRequest);
        $this->dropForeignKey($this->tLogisticsRequest . '_customer_agreement_document_type_id', $this->tLogisticsRequest);
        $this->dropForeignKey($this->tLogisticsRequest . '_customer_form_id', $this->tLogisticsRequest);
        $this->dropForeignKey($this->tLogisticsRequest . '_customer_condition_id', $this->tLogisticsRequest);
        $this->dropForeignKey($this->tLogisticsRequest . '_customer_additional_expenses_id', $this->tLogisticsRequest);
        $this->dropForeignKey($this->tLogisticsRequest . '_carrier_id', $this->tLogisticsRequest);
        $this->dropForeignKey($this->tLogisticsRequest . '_carrier_agreement_document_type_id', $this->tLogisticsRequest);
        $this->dropForeignKey($this->tLogisticsRequest . '_carrier_form_id', $this->tLogisticsRequest);
        $this->dropForeignKey($this->tLogisticsRequest . '_carrier_condition_id', $this->tLogisticsRequest);
        $this->dropForeignKey($this->tLogisticsRequest . '_carrier_additional_expenses_id', $this->tLogisticsRequest);

        $this->dropTable($this->tLogisticsRequest);
        $this->dropTable($this->tLogisticsRequestForm);
        $this->dropTable($this->tLogisticsRequestCondition);
        $this->dropTable($this->tLogisticsRequestAdditionalExpenses);
    }
}


