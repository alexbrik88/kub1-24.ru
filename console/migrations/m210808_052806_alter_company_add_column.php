<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210808_052806_alter_company_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('company', 'invoice_facture_print_template', $this->tinyInteger()->defaultValue(1));
    }

    public function safeDown()
    {
        $this->dropColumn('company', 'invoice_facture_print_template');
    }
}
