<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200213_075533_alter_packing_list_add_fk extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%packing_list}}', 'consignor_id', $this->integer()->defaultValue(null));
        $this->execute("
            UPDATE {{%packing_list}}
            LEFT JOIN {{%contractor}} ON {{%packing_list}}.[[consignor_id]] = {{%contractor}}.[[id]]
            SET {{%packing_list}}.[[consignor_id]] = {{%contractor}}.[[id]]
        ");
        $this->execute("
            UPDATE {{%packing_list}}
            LEFT JOIN {{%contractor}} ON {{%packing_list}}.[[consignee_id]] = {{%contractor}}.[[id]]
            SET {{%packing_list}}.[[consignee_id]] = {{%contractor}}.[[id]]
        ");
        $this->addForeignKey(
            'fk_packing_list_contractor_consignor',
            '{{%packing_list}}',
            'consignor_id',
            '{{%contractor}}',
            'id'
        );
        $this->addForeignKey(
            'fk_packing_list_contractor_consignee',
            '{{%packing_list}}',
            'consignee_id',
            '{{%contractor}}',
            'id'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_packing_list_contractor_consignor', '{{%packing_list}}');
        $this->dropForeignKey('fk_packing_list_contractor_consignee', '{{%packing_list}}');
    }
}
