<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210719_142131_alter_employee_info_role_add_column extends Migration
{
    public function safeUp()
    {
        $table = '{{%employee_info_role}}';
        $tableSchema = Yii::$app->db->schema->getTableSchema($table);
        if (!isset($tableSchema->columns['sort'])) {
            $this->addColumn($table, 'sort', $this->tinyInteger()->unsigned()->defaultValue(0));
        }

        $this->execute('
            UPDATE {{%employee_info_role}}
            SET [[sort]] = (case
                when [[id]] = 1 then 10
                when [[id]] = 2 then 20
                when [[id]] = 3 then 40
                when [[id]] = 4 then 50
                when [[id]] = 5 then 60
                when [[id]] = 6 then 70
                when [[id]] = 7 then 80
                when [[id]] = 8 then 90
                when [[id]] = 9 then 30
            end)
        ');
    }

    public function safeDown()
    {
        //
    }
}
