<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170322_113832_alter_contractor_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%contractor}}', 'last_basis_document', $this->string(350));
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%contractor}}', 'last_basis_document');
    }
}
