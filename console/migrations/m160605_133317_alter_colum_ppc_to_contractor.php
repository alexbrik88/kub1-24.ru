<?php

use console\components\db\Migration;

class m160605_133317_alter_colum_ppc_to_contractor extends Migration
{
    public $tContractor = 'contractor';

    public function safeUp()
    {
        $this->alterColumn($this->tContractor, 'PPC', $this->char(9));
    }
    
    public function safeDown()
    {
        $this->alterColumn($this->tContractor, 'PPC', $this->char(9)->notNull());
    }
}


