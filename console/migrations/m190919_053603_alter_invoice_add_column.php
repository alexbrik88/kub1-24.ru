<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190919_053603_alter_invoice_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%invoice}}', 'print_id', $this->integer()->after('signature_id'));
        $this->addColumn('{{%invoice}}', 'chief_signature_id', $this->integer()->after('print_id'));
        $this->addColumn('{{%invoice}}', 'chief_accountant_signature_id', $this->integer()->after('chief_signature_id'));

        $this->addForeignKey(
            'FK_invoice_company_details_print',
            '{{%invoice}}',
            'print_id',
            '{{%company_details_file}}',
            'id'
        );

        $this->addForeignKey(
            'FK_invoice_company_details_chief_signature',
            '{{%invoice}}',
            'chief_signature_id',
            '{{%company_details_file}}',
            'id'
        );

        $this->addForeignKey(
            'FK_invoice_company_details_chief_accountant_signature',
            '{{%invoice}}',
            'chief_accountant_signature_id',
            '{{%company_details_file}}',
            'id'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_invoice_company_details_print', '{{%invoice}}');
        $this->dropForeignKey('FK_invoice_company_details_chief_signature', '{{%invoice}}');
        $this->dropForeignKey('FK_invoice_company_details_chief_accountant_signature', '{{%invoice}}' );

        $this->dropColumn('{{%invoice}}', 'print_id');
        $this->dropColumn('{{%invoice}}', 'chief_signature_id');
        $this->dropColumn('{{%invoice}}', 'chief_accountant_signature_id');
    }
}
