<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191227_111835_alter_user_config_add_column extends Migration
{
    public $table = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->table, 'table_view_document', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table, 'table_view_contractor_list', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table, 'table_view_contractor', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table, 'table_view_cash', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table, 'table_view_product', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'table_view_document');
        $this->dropColumn($this->table, 'table_view_contractor_list');
        $this->dropColumn($this->table, 'table_view_contractor');
        $this->dropColumn($this->table, 'table_view_cash');
        $this->dropColumn($this->table, 'table_view_product');
    }
}
