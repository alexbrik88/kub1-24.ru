<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200603_165407_alter_price_list_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%price_list}}', 'has_discount_from_sum', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn('{{%price_list}}', 'discount_from_sum', Schema::TYPE_BIGINT . ' UNSIGNED NOT NULL DEFAULT 0');
        $this->addColumn('{{%price_list}}', 'discount_from_sum_percent', $this->decimal(9, 6)->notNull()->defaultValue(0));

        $this->addColumn('{{%price_list}}', 'has_markup_from_sum', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn('{{%price_list}}', 'markup_from_sum', Schema::TYPE_BIGINT . ' UNSIGNED NOT NULL DEFAULT 0');
        $this->addColumn('{{%price_list}}', 'markup_from_sum_percent', $this->decimal(9, 6)->notNull()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%price_list}}', 'has_discount_from_sum');
        $this->dropColumn('{{%price_list}}', 'discount_from_sum');
        $this->dropColumn('{{%price_list}}', 'discount_from_sum_percent');

        $this->dropColumn('{{%price_list}}', 'has_markup_from_sum');
        $this->dropColumn('{{%price_list}}', 'markup_from_sum');
        $this->dropColumn('{{%price_list}}', 'markup_from_sum_percent');
    }
}
