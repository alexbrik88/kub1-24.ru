<?php
use console\components\db\Migration;

class m160810_102838_invoice_income_item extends Migration
{
    public function safeUp()
    {
        $this->createTable('invoice_income_item', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'sort' => $this->smallInteger()->defaultValue(100),
        ], $this->tableOptions);

        $this->batchInsert('invoice_income_item', ['name', 'sort'], [
            ['Оплата от покупателя',  1],
            ['Взнос наличными', 100],
            ['Возврат', 100],
            ['Займ', 100],
            ['Кредит', 100],
            ['От учредителя', 100],
            ['Прочие поступления', 100],
        ]);
    }
    
    public function safeDown()
    {
        $this->dropTable('invoice_income_item');
    }
}


