<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191120_094311_update_service_discount_by_quantity extends Migration
{
    public function safeUp()
    {
        // tariff 1
        $this->update('{{%service_discount_by_quantity}}', [
            'percent' => 16.6666, // from 25.0000
        ], [
            'tariff_id' => 1,
            'quantity' => 2,
        ]);
        $this->update('{{%service_discount_by_quantity}}', [
            'percent' => 25, // from 33.3333
        ], [
            'tariff_id' => 1,
            'quantity' => 3,
        ]);
        $this->update('{{%service_discount_by_quantity}}', [
            'percent' => 33.3333, // from 40.0000
        ], [
            'tariff_id' => 1,
            'quantity' => 4,
        ]);
        $this->update('{{%service_discount_by_quantity}}', [
            'percent' => 41.6666, // from 50.0000
        ], [
            'tariff_id' => 1,
            'quantity' => 5,
        ]);
        // tariff 2
        $this->update('{{%service_discount_by_quantity}}', [
            'percent' => 16.6666, // from 25.0000
        ], [
            'tariff_id' => 2,
            'quantity' => 2,
        ]);
        $this->update('{{%service_discount_by_quantity}}', [
            'percent' => 25, // from 33.3333
        ], [
            'tariff_id' => 2,
            'quantity' => 3,
        ]);
        $this->update('{{%service_discount_by_quantity}}', [
            'percent' => 33.3333, // from 40.0000
        ], [
            'tariff_id' => 2,
            'quantity' => 4,
        ]);
        $this->update('{{%service_discount_by_quantity}}', [
            'percent' => 41.6666, // from 50.0000
        ], [
            'tariff_id' => 2,
            'quantity' => 5,
        ]);
        // tariff 3
        $this->update('{{%service_discount_by_quantity}}', [
            'percent' => 16.6666, // from 25.0000
        ], [
            'tariff_id' => 3,
            'quantity' => 2,
        ]);
        $this->update('{{%service_discount_by_quantity}}', [
            'percent' => 25, // from 33.3333
        ], [
            'tariff_id' => 3,
            'quantity' => 3,
        ]);
        $this->update('{{%service_discount_by_quantity}}', [
            'percent' => 33.3333, // from 40.0000
        ], [
            'tariff_id' => 3,
            'quantity' => 4,
        ]);
        $this->update('{{%service_discount_by_quantity}}', [
            'percent' => 41.6666, // from 50.0000
        ], [
            'tariff_id' => 3,
            'quantity' => 5,
        ]);

        // tariff 11
        $this->update('{{%service_discount_by_quantity}}', [
            'percent' => 8.3333, // from 25.0000
        ], [
            'tariff_id' => 11,
            'quantity' => 2,
        ]);
        $this->update('{{%service_discount_by_quantity}}', [
            'percent' => 16.6666, // from 33.3333
        ], [
            'tariff_id' => 11,
            'quantity' => 3,
        ]);
        $this->delete('{{%service_discount_by_quantity}}', [
            'tariff_id' => 11,
            'quantity' => 4,
        ]);
        // tariff 12
        $this->update('{{%service_discount_by_quantity}}', [
            'percent' => 8.3333, // from 25.0000
        ], [
            'tariff_id' => 12,
            'quantity' => 2,
        ]);
        $this->update('{{%service_discount_by_quantity}}', [
            'percent' => 16.6666, // from 33.3333
        ], [
            'tariff_id' => 12,
            'quantity' => 3,
        ]);
    }

    public function safeDown()
    {
        // tariff 1
        $this->update('{{%service_discount_by_quantity}}', [
            'percent' => 25.0000, // 16.6666
        ], [
            'tariff_id' => 1,
            'quantity' => 2,
        ]);
        $this->update('{{%service_discount_by_quantity}}', [
            'percent' => 33.3333, // 25
        ], [
            'tariff_id' => 1,
            'quantity' => 3,
        ]);
        $this->update('{{%service_discount_by_quantity}}', [
            'percent' => 40.0000, // 33.3333
        ], [
            'tariff_id' => 1,
            'quantity' => 4,
        ]);
        $this->update('{{%service_discount_by_quantity}}', [
            'percent' => 50.0000, // 41.6666
        ], [
            'tariff_id' => 1,
            'quantity' => 5,
        ]);
        // tariff 2
        $this->update('{{%service_discount_by_quantity}}', [
            'percent' => 25.0000, // 16.6666
        ], [
            'tariff_id' => 2,
            'quantity' => 2,
        ]);
        $this->update('{{%service_discount_by_quantity}}', [
            'percent' => 33.3333, // 25
        ], [
            'tariff_id' => 2,
            'quantity' => 3,
        ]);
        $this->update('{{%service_discount_by_quantity}}', [
            'percent' => 40.0000, // 33.3333
        ], [
            'tariff_id' => 2,
            'quantity' => 4,
        ]);
        $this->update('{{%service_discount_by_quantity}}', [
            'percent' => 50.0000, // 41.6666
        ], [
            'tariff_id' => 2,
            'quantity' => 5,
        ]);
        // tariff 3
        $this->update('{{%service_discount_by_quantity}}', [
            'percent' => 25.0000, // 16.6666
        ], [
            'tariff_id' => 3,
            'quantity' => 2,
        ]);
        $this->update('{{%service_discount_by_quantity}}', [
            'percent' => 33.3333, // 25
        ], [
            'tariff_id' => 3,
            'quantity' => 3,
        ]);
        $this->update('{{%service_discount_by_quantity}}', [
            'percent' => 40.0000, // 33.3333
        ], [
            'tariff_id' => 3,
            'quantity' => 4,
        ]);
        $this->update('{{%service_discount_by_quantity}}', [
            'percent' => 50.0000, // 41.6666
        ], [
            'tariff_id' => 3,
            'quantity' => 5,
        ]);

        // tariff 11
        $this->update('{{%service_discount_by_quantity}}', [
            'percent' => 25.0000, // 8.3333
        ], [
            'tariff_id' => 11,
            'quantity' => 2,
        ]);
        $this->update('{{%service_discount_by_quantity}}', [
            'percent' => 33.3333, // 16.6666
        ], [
            'tariff_id' => 11,
            'quantity' => 3,
        ]);
        $this->insert('{{%service_discount_by_quantity}}', [
            'percent' => 34.0000,
            'tariff_id' => 11,
            'quantity' => 4,
        ]);
        // tariff 12
        $this->update('{{%service_discount_by_quantity}}', [
            'percent' => 25.0000, // 8.3333
        ], [
            'tariff_id' => 12,
            'quantity' => 2,
        ]);
        $this->update('{{%service_discount_by_quantity}}', [
            'percent' => 33.3333, // 16.6666
        ], [
            'tariff_id' => 12,
            'quantity' => 3,
        ]);
    }
}
