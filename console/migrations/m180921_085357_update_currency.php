<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180921_085357_update_currency extends Migration
{
    public function safeUp()
    {
        $this->update('currency', ['name' => 'BYR'], ['id' => '974']);
    }

    public function safeDown()
    {
        $this->update('currency', ['name' => 'BYN'], ['id' => '974']);
    }
}
