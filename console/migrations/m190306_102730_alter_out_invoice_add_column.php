<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190306_102730_alter_out_invoice_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('out_invoice', 'demo_id', $this->string(50)->defaultValue('')->after('id'));
        $this->createIndex('demo_id', 'out_invoice', 'demo_id');
    }

    public function safeDown()
    {
        $this->dropIndex('demo_id', 'out_invoice');
        $this->dropColumn('out_invoice', 'demo_id');
    }
}
