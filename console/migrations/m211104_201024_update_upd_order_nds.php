<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211104_201024_update_upd_order_nds extends Migration
{
    public function safeUp()
    {
        $this->execute('
            UPDATE `upd` 
            JOIN (
                SELECT ou.upd_id, SUM((o.purchase_price_with_vat - o.purchase_price_no_vat) * ou.quantity) AS summ
                FROM `order_upd` ou
                LEFT JOIN `order` `o` ON ou.order_id = o.id
                GROUP BY upd_id
            ) t ON t.upd_id = upd.id
            SET `upd`.`order_nds` = t.summ
            WHERE upd.type = 1
        ');

        $this->execute('
            UPDATE `upd` 
            JOIN (
                SELECT ou.upd_id, SUM((o.selling_price_with_vat - o.selling_price_no_vat) * ou.quantity) AS summ
                FROM `order_upd` ou
                LEFT JOIN `order` `o` ON ou.order_id = o.id
                GROUP BY upd_id
            ) t ON t.upd_id = upd.id
            SET `upd`.`order_nds` = t.summ
            WHERE upd.type = 2
        ');
    }

    public function safeDown()
    {
        // no down
    }
}
