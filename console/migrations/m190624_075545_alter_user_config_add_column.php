<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190624_075545_alter_user_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn(
            'user_config',
            'contractor_autionvoice',
            $this->boolean()->notNull()->defaultValue(false)->after('contractor_paidsum')
        );
    }

    public function safeDown()
    {
        $this->dropColumn('user_config', 'contractor_autionvoice');
    }
}
