<?php

use console\components\db\Migration;

class m200110_162954_insales_order_employee_id_drop extends Migration
{
    const TABLE = '{{%insales_order}}';

    public function safeUp()
    {
        $this->dropForeignKey('insales_order_employee_id', self::TABLE);
        $this->dropColumn(self::TABLE, 'employee_id');
    }

    public function safeDown()
    {
        $this->delete(self::TABLE);
        $this->addColumn(self::TABLE, 'employee_id', $this->string(11)->notNull()->after('id')->comment('ID клиента КУБ'));
        $this->addForeignKey(
            'insales_order_employee_id', self::TABLE, 'employee_id',
            '{{%employee}}', 'id',
            'CASCADE', 'CASCADE'
        );
    }
}
