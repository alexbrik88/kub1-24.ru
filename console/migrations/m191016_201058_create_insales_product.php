<?php

use console\components\db\Migration;

class m191016_201058_create_insales_product extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%insales_product}}', [
            'id' => $this->primaryKey()->unsigned()->notNull()->comment('Идентификатор товара в InSales'),
            'employee_id' => $this->integer()->comment('ID покупателя сайта'),
            'sale_price' => $this->float()->unsigned()->comment('Цена товара'),
            'sku' => $this->string(30)->comment('Артикул'),
            'title' => $this->string(100)->notNull()->comment('Название товара'),
            'vat' => $this->tinyInteger()->unsigned(),
            'barcode' => $this->string(40)->comment('Штрихкод'),
            'unit' => $this->string(20)->comment('Единица измерения'),
            'created_at' => $this->integer()->unsigned()->notNull(),
            'updated_at' => $this->integer()->unsigned()->notNull(),
        ], "COMMENT 'Интеграция InSales CMS: товары'");
        $this->addForeignKey(
            'insales_product_employee_id', '{{%insales_product}}', 'employee_id',
            '{{%employee}}', 'id', 'CASCADE', 'CASCADE'
        );
        $this->createIndex('insales_product_sku', '{{%insales_product}}', ['employee_id', 'sku'], true);
        $this->createIndex('insales_product_barcode', '{{%insales_product}}', ['employee_id', 'barcode'], true);
    }

    public function down()
    {
        $this->dropTable('{{insales_product}}');

    }
}
