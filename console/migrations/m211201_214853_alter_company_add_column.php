<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211201_214853_alter_company_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%company}}', 'show_cash_operations_onboarding', $this->boolean()->defaultValue(false));

        $this->execute('
            UPDATE {{%company}}
            SET {{%company}}.[[show_cash_operations_onboarding]] = 1
            WHERE {{%company}}.[[analytics_module_activated]] = 1
            AND (IFNULL({{%company}}.[[name_short]], "") = "" OR IFNULL({{%company}}.[[inn]], "") = "")
            AND {{%company}}.[[registration_page_type_id]] NOT IN (38, 40)
        ');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%company}}', 'show_cash_operations_onboarding');
    }
}
