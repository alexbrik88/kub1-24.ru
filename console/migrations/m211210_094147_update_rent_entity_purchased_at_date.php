<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211210_094147_update_rent_entity_purchased_at_date extends Migration
{
    public function safeUp()
    {
        $this->execute("
            UPDATE rent_entity 
            SET purchased_at = DATE_FORMAT(FROM_UNIXTIME(created_at), '%Y-%m-%d') 
            WHERE purchased_at IS NULL
        ");
    }

    public function safeDown()
    {
        // no down
    }
}
