<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200426_040026_alter_price_list_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%price_list}}', 'is_contractor_physical', $this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%price_list}}', 'is_contractor_physical');
    }
}
