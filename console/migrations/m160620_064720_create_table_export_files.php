<?php

use console\components\db\Migration;

class m160620_064720_create_table_export_files extends Migration
{
    private $_tableExportFiles = 'export_files';
    private $_tableEmployee = 'employee';
    private $_columnUserId = 'user_id';
    private $_columnId = 'id';
    private $_idxUserId = 'idx_user_id';
    private $_fkExportFilesUserIdUserId = 'fk_export_files_user_id_user_id';
    
    public function up()
    {
        $this->createTable($this->_tableExportFiles, [
            $this->_columnId        =>  $this->primaryKey(),
            'created_at'            =>  $this->integer(11)->notNull()->defaultValue(0),
            $this->_columnUserId    =>  $this->integer(11)->notNull()->defaultValue(0),
            'period_start_date'     =>  $this->date()->defaultValue(null),
            'period_end_date'       =>  $this->date()->defaultValue(null),
            'only_new'              =>  $this->boolean()->notNull()->defaultValue(0),
            'io_type_in'            =>  $this->boolean()->notNull()->defaultValue(0),
            'io_type_in_items'      =>  $this->string(255)->notNull()->defaultValue(''),
            'io_type_out'           =>  $this->boolean()->notNull()->defaultValue(0),
            'io_type_out_items'     =>  $this->string(255)->notNull()->defaultValue(''),
            'product_and_service'   =>  $this->boolean()->notNull()->defaultValue(0),
            'contractor'            =>  $this->boolean()->notNull()->defaultValue(0),
            'contractor_items'      =>  $this->string(255)->notNull()->defaultValue(''),
            'filename'              =>  $this->string(255)->notNull()->defaultValue(''),
            'total_objects'         =>  $this->integer(11)->notNull()->defaultValue(0),
            'objects_completed'     =>  $this->integer(11)->notNull()->defaultValue(0),
            'status'                =>  $this->integer(11)->notNull()->defaultValue(0),
        ], $this->tableOptions);
        $this->createIndex($this->_idxUserId, $this->_tableExportFiles, $this->_columnUserId);
        $this->addForeignKey($this->_fkExportFilesUserIdUserId, $this->_tableExportFiles,
            $this->_columnUserId,  $this->_tableEmployee, $this->_columnId, "CASCADE", "RESTRICT");
    }

    public function down()
    {
        $this->dropForeignKey($this->_fkExportFilesUserIdUserId, $this->_tableExportFiles);
        $this->dropIndex($this->_idxUserId, $this->_tableExportFiles);
        $this->dropTable($this->_tableExportFiles);
    }
}
