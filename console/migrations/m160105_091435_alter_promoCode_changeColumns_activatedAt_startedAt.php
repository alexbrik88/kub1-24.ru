<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160105_091435_alter_promoCode_changeColumns_activatedAt_startedAt extends Migration
{
    public $tPromoCode = 'service_payment_promo_code';

    public function safeUp()
    {
        $this->alterColumn($this->tPromoCode, 'started_at', $this->date()->notNull());
        $this->alterColumn($this->tPromoCode, 'expired_at', $this->date()->notNull());
    }
    
    public function safeDown()
    {
        $this->alterColumn($this->tPromoCode, 'started_at', $this->integer()->defaultValue(null));
        $this->alterColumn($this->tPromoCode, 'expired_at', $this->dateTime()->notNull());
    }
}


