<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181012_084247_update_agreement_type extends Migration
{
    public $tableName = 'agreement_type';

    public function safeUp()
    {
        $this->update($this->tableName, ['name' => 'Оферта'], ['id' => 8]);
        $this->update($this->tableName, ['name_dative' => 'оферте'], ['id' => 8]);
    }

    public function safeDown()
    {
        $this->update($this->tableName, ['name' => 'Офферта'], ['id' => 8]);
        $this->update($this->tableName, ['name_dative' => 'офферте'], ['id' => 8]);
    }
}


