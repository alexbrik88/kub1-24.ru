<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200716_141346_alter_user_config_append_project_fields extends Migration
{
    public $tableName = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'project_clients', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'project_end_date', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'project_before_date', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'project_result_sum', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'project_responsible', $this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'project_clients');
        $this->dropColumn($this->tableName, 'project_end_date');
        $this->dropColumn($this->tableName, 'project_before_date');
        $this->dropColumn($this->tableName, 'project_result_sum');
        $this->dropColumn($this->tableName, 'project_responsible');
    }
}
