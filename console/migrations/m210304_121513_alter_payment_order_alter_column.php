<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210304_121513_alter_payment_order_alter_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%payment_order}}', 'uin_code', $this->string());
    }

    public function safeDown()
    {
        $this->alterColumn('{{%payment_order}}', 'uin_code', $this->string(10));
    }
}
