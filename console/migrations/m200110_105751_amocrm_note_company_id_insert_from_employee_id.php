<?php

use common\models\amocrm\Note;
use common\models\employee\Employee;
use console\components\db\Migration;

class m200110_105751_amocrm_note_company_id_insert_from_employee_id extends Migration
{
    const TABLE = '{{%amocrm_note}}';

    public function safeUp()
    {
        foreach (Note::find()->all() as $note) {
            /** @var Note $note */
            /** @noinspection PhpUndefinedFieldInspection */
            $note->company_id = Employee::findOne(['id' => $note->employee_id])->company_id;
            $note->save();
        }
    }

    public function down()
    {
        $this->update(self::TABLE, ['company_id' => 0]);
    }
}
