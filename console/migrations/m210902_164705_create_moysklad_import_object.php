<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210902_164705_create_moysklad_import_object extends Migration
{
    public $objectType = 'moysklad_object_type';
    public $object = 'moysklad_object';
    public $objectValidation = 'moysklad_object_validation';

    public function safeUp()
    {
        // OBJECT TYPE
        $table = $this->objectType;
        $this->createTable($table, [
            'id' => $this->primaryKey(),
            'name' => $this->string()
        ]);

        $this->batchInsert($table, ['id', 'name'], [
            [1, 'Контрагент'],
            [2, 'ДоговорКонтрагента'],
            [3, 'Банк'],
            [4, 'БанковскийСчет'],
            [5, 'Номенклатура'],
            [6, 'НоменклатурнаяГруппа'],
            [7, 'Склад']
        ]);

        // OBJECT
        $table = $this->object;
        $this->createTable($table, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'object_type_id' => $this->integer()->notNull(),

            'object_name' => $this->char(255),
            'object_code' => $this->char(64),
            'object_date' => $this->date(),
            'object_number' => $this->char(64),
            'object_inn' => $this->char(16),
            'object_kpp' => $this->char(16),

            'agreement_id' => $this->integer(),
            'contractor_id' => $this->integer(),
            'product_id' => $this->integer(),
            'product_group_id' => $this->integer(),

            'is_validated' => $this->boolean(),
            'validated_at' => $this->integer(),
            'one_c_object' => $this->text(),
        ]);
        $this->addForeignKey("FK_{$table}_to_company", $table, 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("FK_{$table}_to_object_type", $table, 'object_type_id', $this->objectType, 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("FK_{$table}_to_agreement", $table, 'agreement_id', 'agreement', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("FK_{$table}_to_contractor", $table, 'contractor_id', 'contractor', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("FK_{$table}_to_product", $table, 'product_id', 'product', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("FK_{$table}_to_product_group", $table, 'product_group_id', 'product_group', 'id', 'CASCADE', 'CASCADE');

        $this->createIndex("IDX_{$table}_number", $table, ['company_id', 'object_type_id', 'object_number']);
	    $this->createIndex("IDX_{$table}_inn", $table, ['company_id', 'object_type_id', 'object_inn']);

        // OBJECT VALIDATION
        $table = $this->objectValidation;
        $this->createTable($table, [
            'import_id' => $this->integer(),
            'object_id' => $this->integer(),
            'errors' => $this->text()
        ]);

        $this->addPrimaryKey("PK_{$table}", $table, ['import_id', 'object_id']);
        $this->addForeignKey("FK_{$table}_to_import", $table, 'import_id', 'moysklad_import', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("FK_{$table}_to_object", $table, 'object_id', 'moysklad_object', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        // OBJECT VALIDATION
        $table = $this->objectValidation;
        $this->dropForeignKey("FK_{$table}_to_object", $table);
        $this->dropForeignKey("FK_{$table}_to_import", $table);
        $this->dropTable($table);

        // OBJECT
        $table = $this->object;
        $this->dropForeignKey("FK_{$table}_to_company", $table);
        $this->dropForeignKey("FK_{$table}_to_object_type", $table);
        $this->dropForeignKey("FK_{$table}_to_agreement", $table);
        $this->dropForeignKey("FK_{$table}_to_contractor", $table);
        $this->dropForeignKey("FK_{$table}_to_product", $table);
        $this->dropForeignKey("FK_{$table}_to_product_group", $table);
        $this->dropTable($table);

        // OBJECT TYPE
        $table = $this->objectType;
        $this->dropTable($table);


    }
}
