<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191213_230712_create_autoload_moneta extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%autoload_moneta}}', [
            'company_id' => $this->primaryKey(),
            'type' => $this->tinyInteger()->unsigned()->notNull(),
        ]);

        $this->addForeignKey('FK_autoload_moneta_to_company', 'autoload_moneta', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');

        $this->addColumn('{{%jobs}}', 'is_auto', $this->boolean()->defaultValue(false)->notNull());
    }

    public function safeDown()
    {
        $this->dropTable('{{%autoload_moneta}}');
        $this->dropColumn('{{%jobs}}', 'is_auto');
    }
}
