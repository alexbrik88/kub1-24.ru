<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201030_140844_alter_project_estimate_item_append_columns extends Migration
{
    public $tableName = 'project_estimate_item';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'id_project_estimate', $this->integer(11)->after('id'));
        $this->addColumn($this->tableName, 'created_at', $this->integer(11));
        $this->addColumn($this->tableName, 'updated_at', $this->integer(11));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'id_project_estimate');
        $this->dropColumn($this->tableName, 'created_at');
        $this->dropColumn($this->tableName, 'updated_at');
    }

}
