<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210801_163214_insert_log_event extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%log_event}}', [
            'id' => 6,
            'name' => 'направление изменено',
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%log_event}}', [
            'id' => 6,
        ]);
    }
}
