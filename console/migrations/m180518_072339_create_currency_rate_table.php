<?php

use yii\db\Migration;

/**
 * Handles the creation of table `currency_rate`.
 */
class m180518_072339_create_currency_rate_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('currency_rate', [
            'date' => $this->date()->notNull(),
            'name' => $this->char('3')->notNull(),
            'amount' => $this->integer()->notNull(),
            'value' => $this->decimal(13,4)->notNull(),
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', 'currency_rate', [
            'date',
            'name',
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('currency_rate');
    }
}
