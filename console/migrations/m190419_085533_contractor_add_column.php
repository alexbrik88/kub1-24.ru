<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190419_085533_contractor_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('contractor', 'discount', $this->decimal(5, 2)->notNull()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('contractor', 'discount');
    }
}
