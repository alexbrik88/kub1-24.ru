<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%bitrix24_widget_user}}`.
 */
class m220112_161030_create_bitrix24_widget_user_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%bitrix24_widget_user}}', [
            'client_id' => $this->integer()->notNull()->comment('ID клиента, из которого сделан запрос'),
            'user_id' => $this->integer()->notNull()->comment('ID пользователя, из под которого сделан запрос'),
            'token' => $this->char(50)->comment('Токен для доступа из виджета'),
            'employee_id' => $this->integer()->notNull()->comment('ID сотрудника в КУБ'),
            'created_at' => $this->integer()->notNull()->comment('Timestamp, когда подключен пользователь'),
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', '{{%bitrix24_widget_user}}', ['client_id', 'user_id']);

        $this->createIndex('user_id', '{{%bitrix24_widget_user}}', 'user_id');
        $this->createIndex('token', '{{%bitrix24_widget_user}}', 'token', true);

        $this->addForeignKey('fk_bitrix24_widget_user__client_id', '{{%bitrix24_widget_user}}', 'client_id', '{{%bitrix24_client}}', 'id');

        $this->addForeignKey('fk_bitrix24_widget_user__employee_id', '{{%bitrix24_widget_user}}', 'employee_id', '{{%employee}}', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%bitrix24_widget_user}}');
    }
}
