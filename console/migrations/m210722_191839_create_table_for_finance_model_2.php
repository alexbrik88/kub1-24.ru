<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210722_191839_create_table_for_finance_model_2 extends Migration
{
    const MODEL = 'finance_plan';
    const PRIME_COSTS = 'finance_plan_prime_costs';
    const PRIME_COST_ITEM = 'finance_plan_prime_cost_item';
    const PRIME_COST_ITEM_AUTOFILL = 'finance_plan_prime_cost_item_autofill';

    public function safeUp()
    {
        /*
         * Prime costs list
         */
        $this->createTable(self::PRIME_COSTS, [
            'id' => $this->primaryKey(),
            'name' => $this->string(),
            'sort' => $this->tinyInteger(),
        ]);

        $this->batchInsert(self::PRIME_COSTS, ['id', 'name', 'sort'], [
            [1, 'Себестоимость товара', 1],
            [2, 'Себестоимость услуг', 2],
            [3, 'Доставка', 3],
            [4, 'Списание товара', 4],
        ]);

        /*
         * Prime cost Item
         */
        $this->createTable(self::PRIME_COST_ITEM, [
            'id' => $this->primaryKey(),
            'model_id' => $this->integer()->notNull(),
            'prime_cost_item_id' => $this->integer()->notNull(),
            'data' => $this->text()
        ]);

        $this->addForeignKey('FK_'.self::PRIME_COST_ITEM.'_to_model', self::PRIME_COST_ITEM, 'model_id', self::MODEL, 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_'.self::PRIME_COST_ITEM.'_to_prime_costs', self::PRIME_COST_ITEM, 'prime_cost_item_id', self::PRIME_COSTS, 'id', 'CASCADE', 'CASCADE');

        /*
         * Autofill prime cost item
         */
        $this->createTable(self::PRIME_COST_ITEM_AUTOFILL, [
            'model_id' => $this->integer(11)->notNull(),
            'row_id' => $this->integer()->notNull(),
            'relation_type' => $this->tinyInteger()->unsigned()->notNull(),
            'action' => $this->tinyInteger()->unsigned()->notNull(),
        ]);

        $this->addPrimaryKey('PK_'.self::PRIME_COST_ITEM_AUTOFILL, self::PRIME_COST_ITEM_AUTOFILL, ['model_id', 'row_id']);
        $this->addForeignKey('FK_'.self::PRIME_COST_ITEM_AUTOFILL.'_to_model', self::PRIME_COST_ITEM_AUTOFILL, 'model_id', self::MODEL, 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_'.self::PRIME_COST_ITEM_AUTOFILL.'_to_item', self::PRIME_COST_ITEM_AUTOFILL, 'row_id', self::PRIME_COST_ITEM, 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_'.self::PRIME_COST_ITEM.'_to_prime_costs', self::PRIME_COST_ITEM);
        $this->dropForeignKey('FK_'.self::PRIME_COST_ITEM.'_to_model', self::PRIME_COST_ITEM);
        $this->dropForeignKey('FK_'.self::PRIME_COST_ITEM_AUTOFILL.'_to_model', self::PRIME_COST_ITEM_AUTOFILL);
        $this->dropForeignKey('FK_'.self::PRIME_COST_ITEM_AUTOFILL.'_to_item', self::PRIME_COST_ITEM_AUTOFILL);

        $this->dropTable(self::PRIME_COSTS);
        $this->dropTable(self::PRIME_COST_ITEM);
        $this->dropTable(self::PRIME_COST_ITEM_AUTOFILL);
    }
}
