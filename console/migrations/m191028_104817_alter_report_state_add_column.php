<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191028_104817_alter_report_state_add_column extends Migration
{
    public $table = 'report_state';

    public function safeUp()
    {
        $this->addColumn($this->table, 'invoice_tariff_id', $this->integer()->after('tariff_id'));
        $this->addForeignKey($this->table.'_invoice_tariff_FK', $this->table, 'invoice_tariff_id', 'service_invoice_tariff', 'id', 'CASCADE');

        $this->alterColumn($this->table, 'subscribe_id', $this->integer());
    }

    public function safeDown()
    {
        $this->execute("DELETE FROM `{$this->table}` WHERE `subscribe_id` IS NULL");
        $this->dropForeignKey($this->table.'_invoice_tariff_FK', $this->table);
        $this->dropColumn($this->table, 'invoice_tariff_id');
    }
}
