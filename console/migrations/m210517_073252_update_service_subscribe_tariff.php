<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210517_073252_update_service_subscribe_tariff extends Migration
{
    public function safeUp()
    {
        $this->update('{{%service_subscribe_tariff}}', [
            'tariff_limit' => 6,
        ], [
            'tariff_limit' => 3,
            'tariff_group_id' => range(11, 18),
        ]);
    }

    public function safeDown()
    {
        $this->update('{{%service_subscribe_tariff}}', [
            'tariff_limit' => 3,
        ], [
            'tariff_limit' => 6,
            'tariff_group_id' => range(11, 18),
        ]);
    }
}
