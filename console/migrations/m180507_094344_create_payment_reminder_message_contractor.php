<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180507_094344_create_payment_reminder_message_contractor extends Migration
{
    public $tableName = 'payment_reminder_message_contractor';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'contractor_id' => $this->integer()->notNull(),
            'message_1' => $this->boolean()->defaultValue(false),
            'message_2' => $this->boolean()->defaultValue(false),
            'message_3' => $this->boolean()->defaultValue(false),
            'message_4' => $this->boolean()->defaultValue(false),
            'message_5' => $this->boolean()->defaultValue(false),
            'message_6' => $this->boolean()->defaultValue(false),
            'message_7' => $this->boolean()->defaultValue(false),
            'message_8' => $this->boolean()->defaultValue(false),
            'message_9' => $this->boolean()->defaultValue(false),
            'message_10' => $this->boolean()->defaultValue(false),
        ]);

        $this->addForeignKey($this->tableName . '_company_id', $this->tableName, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tableName . '_contractor_id', $this->tableName, 'contractor_id', 'contractor', 'id', 'RESTRICT', 'CASCADE');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_company_id', $this->tableName);
        $this->dropForeignKey($this->tableName . '_contractor_id', $this->tableName);

        $this->dropTable($this->tableName);
    }
}


