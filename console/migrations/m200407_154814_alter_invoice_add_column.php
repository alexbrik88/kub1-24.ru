<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200407_154814_alter_invoice_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%invoice}}', 'updated_at', $this->integer());

        $this->execute('
            UPDATE {{%invoice}}
            SET {{%invoice}}.[[updated_at]] = {{%invoice}}.[[created_at]]
        ');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%invoice}}', 'updated_at');
    }
}
