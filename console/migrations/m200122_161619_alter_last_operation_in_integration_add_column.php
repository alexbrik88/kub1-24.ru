<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200122_161619_alter_last_operation_in_integration_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%last_operation_in_integration}}', 'date_from', $this->integer(11)->unsigned());
        $this->addColumn('{{%last_operation_in_integration}}', 'date_to', $this->integer(11)->unsigned());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%last_operation_in_integration}}', 'date_from');
        $this->dropColumn('{{%last_operation_in_integration}}', 'date_to');
    }
}
