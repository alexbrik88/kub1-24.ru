<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%ofd_receipt_upload}}`.
 */
class m200605_075210_create_ofd_receipt_upload_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%ofd_receipt_upload}}', [
            'id' => $this->primaryKey(),
            'kkt_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'period_from' => $this->dateTime(),
            'period_till' => $this->dateTime(),
            'receipt_total_sum' => $this->bigInteger(),
            'receipt_total_count' => $this->integer(),
            'receipt_insert_count' => $this->integer(),
            'last_receipt_number' => $this->string(),
            'last_receipt_time' => $this->dateTime(),
        ]);

        $this->addForeignKey('FK_ofd_receipt_upload_ofd_kkt', '{{%ofd_receipt_upload}}', 'kkt_id', '{{%ofd_kkt}}', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%ofd_receipt_upload}}');
    }
}
