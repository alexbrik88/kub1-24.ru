<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190703_150813_drop_contractor_agent_report extends Migration
{
    public function safeUp()
    {
        $this->dropForeignKey('FK_contractor_agent_report_to_document_author', '{{%contractor_agent_report}}');
        $this->dropForeignKey('FK_contractor_agent_report_to_agent', '{{%contractor_agent_report}}');
        $this->dropTable('{{%contractor_agent_report}}');
    }

    public function safeDown()
    {

    }
}
