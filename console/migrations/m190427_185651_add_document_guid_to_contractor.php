<?php

use console\components\db\Migration;
use yii\db\Schema;
use frontend\modules\export\models\one_c\OneCExport;

class m190427_185651_add_document_guid_to_contractor extends Migration
{
    public $tableName = 'contractor';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'document_guid', $this->string(255)->notNull()->defaultValue('')->after('current_account_guid'));

        foreach ((new \yii\db\Query())->select(['id'])->from($this->tableName)->orderBy('id')->batch(50) as $contractorIds) {
            foreach ($contractorIds as $contractorId) {
                Yii::$app->db->createCommand()->update($this->tableName, [
                    'document_guid' => OneCExport::generateGUID()
                ], 'id = :id', [':id' => $contractorId['id']])->execute();
            }
        };
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'document_guid');
    }
}
