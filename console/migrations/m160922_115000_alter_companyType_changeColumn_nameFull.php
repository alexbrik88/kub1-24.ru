<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160922_115000_alter_companyType_changeColumn_nameFull extends Migration
{
    public $tCompanyType = 'company_type';

    public function safeUp()
    {
        $this->alterColumn($this->tCompanyType, 'name_full', $this->string(250)->notNull());
    }
    
    public function safeDown()
    {
        $this->alterColumn($this->tCompanyType, 'name_full', $this->string(45)->notNull());
    }
}


