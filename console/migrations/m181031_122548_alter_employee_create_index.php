<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181031_122548_alter_employee_create_index extends Migration
{
    public function safeUp()
    {
        $this->createIndex('email', '{{%employee}}', 'email');
    }

    public function safeDown()
    {
        $this->dropIndex('email', '{{%employee}}');
    }
}
