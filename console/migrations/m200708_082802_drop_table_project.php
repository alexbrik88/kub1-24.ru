<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200708_082802_drop_table_project extends Migration
{
    public $table = 'project';

    public function safeUp()
    {
        try {
            $this->dropForeignKey('project_company_id', $this->table);
            $this->dropForeignKey('project_responsible_employee_id', $this->table);
        } catch (\Exception $e) {
            echo 'Drop foreign keys fails';
        }
        $this->dropTable($this->table);
    }

    public function safeDown()
    {
        $this->createTable($this->table, [
            'id' => 'pk',
            'company_id' => 'integer',
            'project_number' => 'string',
            'name' => 'string',
            'status' => 'integer',
            'description' => 'text',
            'start_date' => 'datetime',
            'end_date' => 'datetime',
            'responsible_employer_id' => 'integer',
            'additional_expense' => 'integer',
            'additional_expense_month' => 'integer',
            'created_at' => 'timestamp',
            'updated_at' => 'timestamp',

        ]);

    }
}
