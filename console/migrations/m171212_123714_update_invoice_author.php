<?php

use console\components\db\Migration;
use yii\db\Schema;
use common\models\document\Invoice;
use frontend\models\Documents;

class m171212_123714_update_invoice_author extends Migration
{
    public $tableName = 'invoice';
    public $tAct = 'act';

    public function safeUp()
    {
        /* @var $invoices Invoice[] */
        $invoices = Invoice::find()
            ->byCompany(5)
            ->byDeleted()
            ->byIOType(Documents::IO_TYPE_OUT)
            ->byDocumentDateRange('2017-01-01', '2017-12-31')
            ->andWhere(['in', 'document_number', [1, 2, 10, 14, 26, 29, 34, 42, 45, 51, 52, 56, 57, 58, 61, 68, 70, 76,
                79, 80, 85, 86, 94, 95, 99, 115, 120, 126, 128, 131, 132, 144]])
            ->all();

        foreach ($invoices as $invoice) {
            $this->update($this->tableName, ['document_author_id' => 6732], ['id' => $invoice->id]);
            if ($invoice->act) {
                $this->update($this->tAct, ['document_author_id' => 6732], ['id' => $invoice->act->id]);
            }
        }

        $invoices = Invoice::find()
            ->byCompany(5)
            ->byDeleted()
            ->byIOType(Documents::IO_TYPE_OUT)
            ->byDocumentDateRange('2017-01-01', '2017-12-31')
            ->andWhere(['in', 'document_number', [7, 8, 9, 13, 17, 19, 20, 21, 22, 23, 24, 25, 27, 30, 31, 33, 36, 37,
                38, 39, 40, 46, 48, 49, 50, 53, 54, 55, 59, 60, 63, 64, 65, 66, 67, 69, 72, 73, 74, 75, 78, 81, 82, 89,
                91, 92, 94, 96, 98, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 112, 116, 119, 122, 123, 127,
                129, 133, 136, 137, 138, 139, 141]])
            ->all();

        foreach ($invoices as $invoice) {
            $this->update($this->tableName, ['document_author_id' => 6721], ['id' => $invoice->id]);
            if ($invoice->act) {
                $this->update($this->tAct, ['document_author_id' => 6721], ['id' => $invoice->act->id]);
            }
        }

        $invoices = Invoice::find()
            ->byCompany(5)
            ->byDeleted()
            ->byIOType(Documents::IO_TYPE_OUT)
            ->byDocumentDateRange('2017-01-01', '2017-12-31')
            ->andWhere(['in', 'document_number', [15, 16, 28, 32, 35, 41, 43, 44, 47, 77, 83, 87, 97, 118]])
            ->all();

        foreach ($invoices as $invoice) {
            $this->update($this->tableName, ['document_author_id' => 6736], ['id' => $invoice->id]);
            if ($invoice->act) {
                $this->update($this->tAct, ['document_author_id' => 6736], ['id' => $invoice->act->id]);
            }
        }

        $invoices = Invoice::find()
            ->byCompany(5)
            ->byDeleted()
            ->byIOType(Documents::IO_TYPE_OUT)
            ->byDocumentDateRange('2017-01-01', '2017-12-31')
            ->andWhere(['in', 'document_number', [12, 18, 71, 84, 117, 135, 146]])
            ->all();

        foreach ($invoices as $invoice) {
            $this->update($this->tableName, ['document_author_id' => 6729], ['id' => $invoice->id]);
            if ($invoice->act) {
                $this->update($this->tAct, ['document_author_id' => 6729], ['id' => $invoice->act->id]);
            }
        }

        $invoices = Invoice::find()
            ->byCompany(5)
            ->byDeleted()
            ->byIOType(Documents::IO_TYPE_OUT)
            ->byDocumentDateRange('2017-01-01', '2017-12-31')
            ->andWhere(['in', 'document_number', [62, 88, 90, 111, 113, 114, 124, 125, 140, 142, 143, 147, 148, 149]])
            ->all();

        foreach ($invoices as $invoice) {
            $this->update($this->tableName, ['document_author_id' => 6725], ['id' => $invoice->id]);
            if ($invoice->act) {
                $this->update($this->tAct, ['document_author_id' => 6725], ['id' => $invoice->act->id]);
            }
        }

        $invoices = Invoice::find()
            ->byCompany(5)
            ->byDeleted()
            ->byIOType(Documents::IO_TYPE_OUT)
            ->byDocumentDateRange('2017-01-01', '2017-12-31')
            ->andWhere(['in', 'document_number', [121]])
            ->all();

        foreach ($invoices as $invoice) {
            $this->update($this->tableName, ['document_author_id' => 6734], ['id' => $invoice->id]);
            if ($invoice->act) {
                $this->update($this->tAct, ['document_author_id' => 6734], ['id' => $invoice->act->id]);
            }
        }

        $invoices = Invoice::find()
            ->byCompany(5)
            ->byDeleted()
            ->byIOType(Documents::IO_TYPE_OUT)
            ->byDocumentDateRange('2017-01-01', '2017-12-31')
            ->andWhere(['in', 'document_number', [130]])
            ->all();

        foreach ($invoices as $invoice) {
            $this->update($this->tableName, ['document_author_id' => 6726], ['id' => $invoice->id]);
            if ($invoice->act) {
                $this->update($this->tAct, ['document_author_id' => 6726], ['id' => $invoice->act->id]);
            }
        }

        $invoices = Invoice::find()
            ->byCompany(5)
            ->byDeleted()
            ->byIOType(Documents::IO_TYPE_OUT)
            ->byDocumentDateRange('2017-01-01', '2017-12-31')
            ->andWhere(['in', 'document_number', [134]])
            ->all();

        foreach ($invoices as $invoice) {
            $this->update($this->tableName, ['document_author_id' => 6727], ['id' => $invoice->id]);
            if ($invoice->act) {
                $this->update($this->tAct, ['document_author_id' => 6727], ['id' => $invoice->act->id]);
            }
        }

        $invoices = Invoice::find()
            ->byCompany(5)
            ->byDeleted()
            ->byIOType(Documents::IO_TYPE_OUT)
            ->byDocumentDateRange('2017-01-01', '2017-12-31')
            ->andWhere(['in', 'document_number', [145]])
            ->all();

        foreach ($invoices as $invoice) {
            $this->update($this->tableName, ['document_author_id' => 6731], ['id' => $invoice->id]);
            if ($invoice->act) {
                $this->update($this->tAct, ['document_author_id' => 6731], ['id' => $invoice->act->id]);
            }
        }
    }

    public function safeDown()
    {

    }
}


