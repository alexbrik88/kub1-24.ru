<?php

use common\models\Company;
use common\models\employee\Employee;
use console\components\db\Migration;

class m200109_153043_company_integration_insert_from_employee extends Migration
{
    public function safeUp()
    {
        $employee=Employee::find()->where('integration IS NOT NULL')->select(['company_id','integration'])->asArray()->all();
        foreach($employee as $item) {
            $company=Company::findOne(['id'=>$item['company_id']]);
            $company->integration=$item['integration'];
            $company->save();
        }
    }

    public function safeDown()
    {
        $company=Company::find()->where('integration IS NOT NULL')->select(['id','integration'])->asArray()->all();
        foreach($company as $item) {
            $employee=Employee::find()->where(['company_id'=>$item['id']])->all();
            foreach($employee as $empl) {
                $empl->integration=$item['integration'];
                $empl->save();
            }
        }
    }
}
