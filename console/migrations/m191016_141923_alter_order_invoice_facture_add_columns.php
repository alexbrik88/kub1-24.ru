<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191016_141923_alter_order_invoice_facture_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%order_invoice_facture}}', 'country_id', $this->integer()->notNull());
        $this->addColumn('{{%order_invoice_facture}}', 'custom_declaration_number', $this->string()->notNull());

        $this->execute('
            UPDATE {{%order_invoice_facture}} AS {{%order_invoice_facture}}
            LEFT JOIN {{%order}} ON {{%order_invoice_facture}}.[[order_id]] = {{%order}}.[[id]]
            LEFT JOIN {{%country}} ON {{%order}}.[[country_id]] = {{%country}}.[[id]]
            SET {{%order_invoice_facture}}.[[country_id]] = COALESCE({{%country}}.[[id]], 1),
                {{%order_invoice_facture}}.[[custom_declaration_number]] = {{%order}}.[[custom_declaration_number]]
        ');

        $this->addForeignKey('fk_orderInvoiceFacture_country', '{{%order_invoice_facture}}', 'country_id', '{{%country}}', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_orderInvoiceFacture_country', '{{%order_invoice_facture}}');
        $this->dropColumn('{{%order_invoice_facture}}', 'country_id');
        $this->dropColumn('{{%order_invoice_facture}}', 'custom_declaration_number');
    }
}
