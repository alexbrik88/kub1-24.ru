<?php

use console\components\db\Migration;
use yii\db\Schema;
use yii\db\Query;

class m210106_143747_alter_cashbox_add_column extends Migration
{
    private function getDbName()
    {
        if (preg_match('/dbname=([^;]*)/', Yii::$app->db->dsn, $match)) {
            return $match[1];
        } else {
            return '';
        }
    }

    public function safeUp()
    {
        $collate = (new Query)
            ->select('COLLATION_NAME')
            ->from('information_schema.COLUMNS')
            ->where([
                'TABLE_SCHEMA' => $this->getDbName(),
                'TABLE_NAME' => 'currency',
                'COLUMN_NAME' => 'id',
            ])->scalar() ?: 'utf8_unicode_ci';
        $append = "COLLATE $collate";

        $this->addColumn('{{%cashbox}}', 'currency_id', $this->char(3)->notNull()->after('company_id')->append($append));

        $this->update('{{%cashbox}}', ['currency_id' => '643']);

        $this->createIndex('currency_id', '{{%cashbox}}', 'currency_id');

        $this->addForeignKey('FK_cashbox__currency_id', '{{%cashbox}}', 'currency_id', '{{%currency}}', 'id');
    }

    public function safeDown()
    {
        $this->dropIndex('currency_id', '{{%cashbox}}');
        $this->dropForeignKey('FK_cashbox__currency_id', '{{%cashbox}}');

        $this->dropColumn('{{%cashbox}}', 'currency_id');
    }
}
