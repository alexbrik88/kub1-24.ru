<?php

use yii\db\Schema;
use yii\db\Migration;

class m151209_110550_alter_servicePayment_addColumn_byNovice extends Migration
{
    public $tPayment = 'service_payment';

    public function safeUp()
    {
        $this->addColumn($this->tPayment, 'by_novice', $this->boolean()->notNull()->defaultValue(0));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tPayment, 'by_novice');
    }
}
