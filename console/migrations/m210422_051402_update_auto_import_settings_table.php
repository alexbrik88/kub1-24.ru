<?php

use yii\db\Migration;

class m210422_051402_update_auto_import_settings_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%auto_import_settings}}';

    /**
     * @inheritDoc
     */
    public function safeUp()
    {
        $this->update(self::TABLE_NAME, ['command_type' => 2], ['command_type' => 0]);
    }

    /**
     * @inheritDoc
     */
    public function safeDown()
    {
        $this->update(self::TABLE_NAME, ['command_type' => 0], ['command_type' => 2]);
    }
}
