<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180424_133155_update_company_taxation_type extends Migration
{
    public $tableName = 'company_taxation_type';

    public function safeUp()
    {
        $this->update($this->tableName, ['osno' => 1], ['company_id' => 389]);
        $this->update($this->tableName, ['usn' => 0], ['company_id' => 389]);
        $this->update($this->tableName, ['usn_percent' => null], ['company_id' => 389]);
    }

    public function safeDown()
    {
        $this->update($this->tableName, ['osno' => 0], ['company_id' => 389]);
        $this->update($this->tableName, ['usn' => 1], ['company_id' => 389]);
        $this->update($this->tableName, ['usn_percent' => 15], ['company_id' => 389]);
    }
}


