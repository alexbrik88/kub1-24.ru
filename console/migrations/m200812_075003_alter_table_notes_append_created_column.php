<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200812_075003_alter_table_notes_append_created_column extends Migration
{
    public $tableName = 'notes';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'created_at', $this->integer(11));
        $this->addColumn($this->tableName, 'updated_at', $this->integer(11));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'created_at');
        $this->dropColumn($this->tableName, 'updated_at');
    }

}
