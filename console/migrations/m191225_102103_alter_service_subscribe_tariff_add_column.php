<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191225_102103_alter_service_subscribe_tariff_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%service_subscribe_tariff}}', 'is_pay_extra', $this->boolean()->defaultValue(false));
        $this->addColumn('{{%service_subscribe_tariff}}', 'pay_extra_for', $this->integer()->defaultValue(null));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%service_subscribe_tariff}}', 'is_pay_extra');
        $this->dropColumn('{{%service_subscribe_tariff}}', 'pay_extra_for');
    }
}
