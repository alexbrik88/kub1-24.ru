<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181010_220208_add_production_type_to_price_list extends Migration
{
    public $tableName = 'price_list';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'production_type', $this->boolean()->notNull());
        $this->addCommentOnColumn($this->tableName, 'production_type', '0 - услуга, 1 - товар');
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'production_type');
    }
}


