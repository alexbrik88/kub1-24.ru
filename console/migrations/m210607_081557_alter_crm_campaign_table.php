<?php

use yii\db\Migration;

class m210607_081557_alter_crm_campaign_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%crm_client_campaign}}';

    /**
     * @inheritDoc
     */
    public function up(): void
    {
        $this->renameColumn(self::TABLE_NAME, 'client_campaign_id', 'id');
        $this->renameTable(self::TABLE_NAME, '{{%contractor_campaign}}');
    }

    /**
     * @inheritDoc
     */
    public function down(): bool
    {
        return false;
    }
}
