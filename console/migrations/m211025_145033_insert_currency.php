<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211025_145033_insert_currency extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%currency}}', [
            'id' => '826',
            'name' => 'GBP',
            'label' => 'Фунт стерлингов',
            'old_value' => 0,
            'current_value' => 0,
            'sort' => 6,
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%currency}}', [
            'id' => '826',
        ]);
    }
}
