<?php

use console\components\db\Migration;
use frontend\modules\export\models\one_c\OneCExport;

class m160116_123346_create_export extends Migration
{
    public $export = 'export';
    public $exportGUID = 'export_guid';
    public $guidColumnName = 'object_guid';

    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        $this->createTable($this->export, [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer(11)->notNull()->defaultValue(0),
            'user_id' => $this->integer(11)->notNull()->defaultValue(0),
            'period_start_date' => $this->date()->defaultValue(null),
            'period_end_date' => $this->date()->defaultValue(null),
            'only_new' => $this->boolean()->notNull()->defaultValue(0),
            'io_type_in' => $this->boolean()->notNull()->defaultValue(0),
            'io_type_in_items' => $this->string(255)->notNull()->defaultValue(''),
            'io_type_out' => $this->boolean()->notNull()->defaultValue(0),
            'io_type_out_items' => $this->string(255)->notNull()->defaultValue(''),
            'product_and_service' => $this->boolean()->notNull()->defaultValue(0),
            'contractor' => $this->boolean()->notNull()->defaultValue(0),
            'contractor_items' => $this->string(255)->notNull()->defaultValue(''),
            'filename' => $this->string(255)->notNull()->defaultValue(''),
            'total_objects' => $this->integer(11)->notNull()->defaultValue(0),
            'objects_completed' => $this->integer(11)->notNull()->defaultValue(0),
            'status' => $this->integer(11)->notNull()->defaultValue(0),
        ], $this->tableOptions);

        $this->createTable($this->exportGUID, [
            'id' => $this->primaryKey(),
            'class_id' => $this->string(255)->notNull()->defaultValue(''),
            'object_id' => $this->integer(11)->notNull()->defaultValue(0),
            'guid' => $this->string(255)->notNull()->defaultValue(''),
        ], $this->tableOptions);

        /**  Add "GUID" column for objects */
        foreach ($this->getObjectTypesWithGUID() as $className => $tableName) {
            $this->addColumn($tableName, $this->guidColumnName, $this->string(36)->notNull()->defaultValue(''));

            foreach ((new \yii\db\Query())->select(['id'])->from($tableName)->orderBy('id')->batch(50) as $contractorIds) {
                foreach ($contractorIds as $contractorId) {
                    $GUIDstr = OneCExport::generateGUID();
                    \Yii::$app->db->createCommand()->update($tableName, [$this->guidColumnName => $GUIDstr], 'id = :id', [':id' => $contractorId['id']])->execute();
                    \Yii::$app->db->createCommand()->insert($this->exportGUID, [
                        'class_id' => $className,
                        'object_id' => $contractorId['id'],
                        'guid' => $GUIDstr,
                    ])->execute();
                }
            };
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        $this->dropTable($this->export);
        $this->dropTable($this->exportGUID);

       /** Remove "GUID" column */
        foreach ($this->getObjectTypesWithGUID() as $objectTableName) {
            $this->dropColumn($objectTableName, $this->guidColumnName);
        }
    }

    /**
     * @return array
     */
    protected function getObjectTypesWithGUID()
    {
        return [
            \common\models\Contractor::className() => 'contractor',
            \common\models\document\Act::className() => 'act',
            \common\models\document\InvoiceFacture::className() => 'invoice_facture',
            \common\models\product\Product::className() => 'product',
            \common\models\Company::className() => 'company',
            \common\models\document\Invoice::className() => 'invoice',
            \common\models\document\PackingList::className() => 'packing_list',
        ];
    }
}


