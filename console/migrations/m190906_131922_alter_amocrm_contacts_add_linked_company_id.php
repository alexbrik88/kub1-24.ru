<?php

use console\components\db\Migration;

class m190906_131922_alter_amocrm_contacts_add_linked_company_id extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{amocrm_contacts}}', 'linked_company_id', $this->integer()->unsigned());
        $this->addColumn('{{amocrm_contacts}}','link_changed',$this->boolean());

    }

    public function safeDown()
    {
        $this->dropColumn('{{amocrm_contacts}}', 'linked_company_id');
        $this->dropColumn('{{amocrm_contacts}}','link_changed');
    }
}
