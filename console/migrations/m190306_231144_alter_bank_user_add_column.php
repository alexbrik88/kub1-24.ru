<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190306_231144_alter_bank_user_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('bank_user', 'company_uid', $this->string()->notNull()->after('user_uid'));
        $this->createIndex('company_uid', 'bank_user', 'company_uid');
    }

    public function safeDown()
    {
        $this->dropIndex('company_uid', 'bank_user');
        $this->dropColumn('bank_user', 'company_uid');
    }
}
