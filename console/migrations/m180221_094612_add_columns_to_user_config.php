<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180221_094612_add_columns_to_user_config extends Migration
{
    public $tableName = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'order_document_product_article', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'order_document_product_reserve', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'order_document_product_quantity', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'order_document_product_weigh', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'order_document_product_volume', $this->boolean()->notNull()->defaultValue(false));


        $this->addCommentOnColumn($this->tableName, 'order_document_product_article', 'Показ колонки "Артикул" в списке товаров у заказа');
        $this->addCommentOnColumn($this->tableName, 'order_document_product_reserve', 'Показ колонки "Резерв" в списке товаров у заказа');
        $this->addCommentOnColumn($this->tableName, 'order_document_product_quantity', 'Показ колонки "Остаток" в списке товаров у заказа');
        $this->addCommentOnColumn($this->tableName, 'order_document_product_weigh', 'Показ колонки "Вес" в списке товаров у заказа');
        $this->addCommentOnColumn($this->tableName, 'order_document_product_volume', 'Показ колонки "Объем" в списке товаров у заказа');
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'order_document_product_article');
        $this->dropColumn($this->tableName, 'order_document_product_reserve');
        $this->dropColumn($this->tableName, 'order_document_product_quantity');
        $this->dropColumn($this->tableName, 'order_document_product_weigh');
        $this->dropColumn($this->tableName, 'order_document_product_volume');
    }
}


