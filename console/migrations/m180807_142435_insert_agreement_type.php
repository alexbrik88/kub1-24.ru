<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180807_142435_insert_agreement_type extends Migration
{
    public function safeUp()
    {
        $this->insert('agreement_type', [
            'id' => 7,
            'name' => 'Гос.Контракт',
            'name_dative' => 'гос.контракту',
            'sort' => 45,
        ]);
    }

    public function safeDown()
    {
        $this->delete('agreement_type', ['id' => 7]);
    }
}
