<?php

use console\components\db\Migration;
use yii\db\Schema;
use common\models\document\Act;

class m180912_122343_create_invoice_act extends Migration
{
    public $tableName = 'invoice_act';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'invoice_id' => $this->integer()->notNull(),
            'act_id' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey($this->tableName . '_invoice_id', $this->tableName, 'invoice_id', 'invoice', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tableName . '_act_id', $this->tableName, 'act_id', 'act', 'id', 'RESTRICT', 'CASCADE');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_invoice_id', $this->tableName);
        $this->dropForeignKey($this->tableName . '_act_id', $this->tableName);

        $this->dropTable($this->tableName);
    }
}


