<?php

use console\components\db\Migration;
use yii\db\Schema;

class m220119_040340_create_balance_initial extends Migration
{
    public function safeUp()
    {
        $this->createTable('balance_initial', [
            'company_id' => $this->primaryKey(),
            'date' => $this->date()->notNull(),
            'undistributed_profit' => $this->bigInteger()->notNull()->defaultValue(0)
        ]);

        $this->addForeignKey('FK_balance_initial_company', 'balance_initial', 'company_id', 'company', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable('balance_initial');
    }
}
