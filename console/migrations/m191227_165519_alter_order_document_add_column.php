<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191227_165519_alter_order_document_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%order_document}}', 'contractor_bank_city', $this->string()->after('contractor_bank_name'));

        $this->execute('
            UPDATE {{%order_document}}
            LEFT JOIN {{%bik_dictionary}} ON {{%bik_dictionary}}.[[bik]] = {{%order_document}}.[[contractor_bik]]
            SET {{%order_document}}.[[contractor_bank_city]] = {{%bik_dictionary}}.[[city]]
        ');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%order_document}}', 'contractor_bank_city');
    }
}
