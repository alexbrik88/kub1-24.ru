<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180104_091717_alter_user_config_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_config', 'contractor_contact', $this->boolean(1)->notNull()->defaultValue(false));
        $this->addColumn('user_config', 'contractor_phone', $this->boolean(1)->notNull()->defaultValue(false));
        $this->addColumn('user_config', 'contractor_agreement', $this->boolean(1)->notNull()->defaultValue(false));
        $this->addColumn('user_config', 'contractor_notpaidcount', $this->boolean(1)->notNull()->defaultValue(true));
        $this->addColumn('user_config', 'contractor_notpaidsum', $this->boolean(1)->notNull()->defaultValue(true));
        $this->addColumn('user_config', 'contractor_paidcount', $this->boolean(1)->notNull()->defaultValue(true));
        $this->addColumn('user_config', 'contractor_paidsum', $this->boolean(1)->notNull()->defaultValue(true));
        $this->addColumn('user_config', 'contractor_responsible', $this->boolean(1)->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('user_config', 'contractor_contact');
        $this->dropColumn('user_config', 'contractor_phone');
        $this->dropColumn('user_config', 'contractor_agreement');
        $this->dropColumn('user_config', 'contractor_notpaidcount');
        $this->dropColumn('user_config', 'contractor_notpaidsum');
        $this->dropColumn('user_config', 'contractor_paidcount');
        $this->dropColumn('user_config', 'contractor_paidsum');
        $this->dropColumn('user_config', 'contractor_responsible');
    }
}
