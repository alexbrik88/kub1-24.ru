<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210506_144918_alter_acquiring_operation_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('acquiring_operation', 'is_accounting', Schema::TYPE_BOOLEAN . ' NOT NULL DEFAULT 1 COMMENT \'Учёт в бухгалтерии: 0 - нет, 1 - да\'');
    }

    public function safeDown()
    {
        $this->dropColumn('acquiring_operation', 'is_accounting');
    }
}
