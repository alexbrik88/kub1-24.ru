<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160211_090809_create_table_page_type extends Migration
{
    public $tPageType = 'page_type';

    public function safeUp()
    {
        $this->createTable($this->tPageType, [
            'id' => $this->primaryKey(11),
            'name' => $this->text(),
        ]);
        $this->batchInsert($this->tPageType, ['id', 'name'], [
            [1, 'Покупатели'],
            [2, 'Поставщики'],
            [3, 'Исходящие документы/Счет'],
            [4, 'Входящие документы/Счет'],
            [5, 'Услуги'],
            [6, 'Товары'],
            [7, 'Сотрудники'],
        ]);
    }
    
    public function safeDown()
    {
        $this->dropTable($this->tPageType);
    }
}


