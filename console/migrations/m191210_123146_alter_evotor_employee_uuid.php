<?php

use console\components\db\Migration;
use common\models\evotor\Employee;
use common\models\evotor\EmployeeStore;
use common\models\evotor\Receipt;

class m191210_123146_alter_evotor_employee_uuid extends Migration
{
    public function safeUp()
    {
        Employee::deleteAll();
        EmployeeStore::deleteAll();
        Receipt::deleteAll();
        $this->dropForeignKey('evotor_employee_store_employee_id', '{{evotor_employee_store}}');
        $this->addColumn('{{evotor_employee}}', 'uuid', $this->char(36)->notNull()->unique()->comment('UUID сотрудника Эвотор'));
        $this->dropColumn('{{evotor_employee}}', 'id');
        $this->addColumn('{{evotor_employee}}', 'id', $this->primaryKey(11)->unsigned()->notNull()->first());
        $this->alterColumn('{{evotor_employee_store}}', 'employee_id', $this->integer(11)->unsigned()->notNull()->comment('ID сотрудника Эвотор (evotor_employee.id)'));
        $this->addForeignKey('evotor_employee_store_employee_id', '{{evotor_employee_store}}', 'employee_id',
            '{{evotor_employee}}', 'id', 'CASCADE', 'CASCADE');
        $this->alterColumn('{{evotor_receipt}}', 'evotor_employee_id', $this->integer(11)->unsigned()->after('store_id')->comment('ID сотрудника Эвотор (evotor_employee.id)'));
        $this->addForeignKey('evotor_receipt_evotor_employee_id', '{{evotor_receipt}}', 'evotor_employee_id',
            '{{evotor_employee}}', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        Employee::deleteAll();
        EmployeeStore::deleteAll();
        Receipt::deleteAll();
        $this->dropColumn('{{evotor_employee}}', 'uuid');
        $this->dropForeignKey('evotor_employee_store_employee_id', '{{evotor_employee_store}}');
        $this->dropForeignKey('evotor_receipt_evotor_employee_id', '{{evotor_receipt}}');
        $this->dropColumn('{{evotor_employee}}', 'id');
        $this->addColumn('{{evotor_employee}}', 'id', "BINARY(16) NOT NULL PRIMARY KEY COMMENT 'UUID Эвотора'");
        $this->alterColumn('{{evotor_employee_store}}', 'employee_id', "BINARY(16) NOT NULL COMMENT 'UUID сотрудника Эвотора'");
        $this->addForeignKey('evotor_employee_store_employee_id', '{{evotor_employee_store}}', 'employee_id',
            '{{evotor_employee}}', 'id', 'CASCADE', 'CASCADE');
        $this->alterColumn('{{evotor_receipt}}', 'evotor_employee_id', "BINARY(16) COMMENT 'UUID клиента Эвотор'");
    }
}
