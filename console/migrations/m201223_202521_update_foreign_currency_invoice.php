<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201223_202521_update_foreign_currency_invoice extends Migration
{
    public function safeUp()
    {
        $this->execute('
            UPDATE {{%foreign_currency_invoice}}
            LEFT JOIN {{%foreign_currency_account}}
                ON {{%foreign_currency_account}}.[[id]] = {{%foreign_currency_invoice}}.[[foreign_currency_account_id]]
            LEFT JOIN {{%checking_accountant}}
                ON {{%checking_accountant}}.[[company_id]] = {{%foreign_currency_account}}.[[company_id]]
                AND {{%checking_accountant}}.[[currency_id]] = {{%foreign_currency_account}}.[[currency_id]] COLLATE utf8_unicode_ci
                AND {{%checking_accountant}}.[[rs]] = {{%foreign_currency_account}}.[[rs]] COLLATE utf8_unicode_ci
            SET {{%foreign_currency_invoice}}.[[checking_accountant_id]] = {{%checking_accountant}}.[[id]]
        ');
    }

    public function safeDown()
    {
        $this->execute('
            UPDATE {{%foreign_currency_invoice}}
            LEFT JOIN {{%checking_accountant}}
                ON {{%checking_accountant}}.[[id]] = {{%foreign_currency_invoice}}.[[checking_accountant_id]]
            LEFT JOIN {{%foreign_currency_account}}
                ON {{%foreign_currency_account}}.[[company_id]] = {{%checking_accountant}}.[[company_id]]
                AND {{%foreign_currency_account}}.[[currency_id]] = {{%checking_accountant}}.[[currency_id]] COLLATE utf8_unicode_ci
                AND {{%foreign_currency_account}}.[[rs]] = {{%checking_accountant}}.[[rs]] COLLATE utf8_unicode_ci
            SET {{%foreign_currency_invoice}}.[[foreign_currency_account_id]] = {{%foreign_currency_account}}.[[id]]
        ');
    }
}
