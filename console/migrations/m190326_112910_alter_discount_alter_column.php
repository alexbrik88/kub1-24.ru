<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190326_112910_alter_discount_alter_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%discount}}', 'tariff_id', $this->string()->notNull());
    }

    public function safeDown()
    {
        $this->alterColumn('{{%discount}}', 'tariff_id', "SET('1','2','3') NOT NULL DEFAULT '1,2,3'");
    }
}
