<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181213_230512_add_goods_params_logistics_request extends Migration
{
    public $tableName = 'logistics_request';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'goods_name', $this->string()->defaultValue(null));
        $this->addColumn($this->tableName, 'goods_weight', $this->string()->defaultValue(null));
        $this->addColumn($this->tableName, 'goods_length', $this->integer()->defaultValue(null));
        $this->addColumn($this->tableName, 'goods_width', $this->integer()->defaultValue(null));
        $this->addColumn($this->tableName, 'goods_height', $this->integer()->defaultValue(null));
        $this->addColumn($this->tableName, 'goods_count', $this->integer()->defaultValue(1));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'goods_name');
        $this->dropColumn($this->tableName, 'goods_weight');
        $this->dropColumn($this->tableName, 'goods_length');
        $this->dropColumn($this->tableName, 'goods_width');
        $this->dropColumn($this->tableName, 'goods_height');
        $this->dropColumn($this->tableName, 'goods_count');
    }
}
