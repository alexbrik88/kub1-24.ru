<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201030_122602_add_table_project_estimate_item extends Migration
{
    public $tableName = 'project_estimate_item';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'number' => $this->integer(),
            'product_id' => $this->integer(11),
        ]);

        $this->createIndex($this->tableName . '_product_id_idx', $this->tableName, 'product_id');
    }

    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
