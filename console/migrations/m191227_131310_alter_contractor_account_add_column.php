<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191227_131310_alter_contractor_account_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%contractor_account}}', 'bank_city', $this->string()->after('bank_name'));

        $this->execute('
            UPDATE {{%contractor_account}}
            LEFT JOIN {{%bik_dictionary}} ON {{%bik_dictionary}}.[[bik]] = {{%contractor_account}}.[[bik]]
            SET {{%contractor_account}}.[[bank_city]] = {{%bik_dictionary}}.[[city]]
        ');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%contractor_account}}', 'bank_city');
    }
}
