<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%bitrix24_widget_invoice}}`.
 */
class m220113_142524_create_bitrix24_widget_invoice_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%bitrix24_widget_invoice}}', [
            'invoice_id' => $this->integer()->notNull(),
            'account_id' => $this->integer()->notNull(),
            'lead_id' => $this->integer()->notNull(),
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', '{{%bitrix24_widget_invoice}}', ['invoice_id', 'account_id']);
        $this->createIndex('amo_lead', '{{%bitrix24_widget_invoice}}', [
            'account_id',
            'lead_id',
        ]);
        $this->addForeignKey('fk_bitrix24_widget_invoice__account_id', '{{%bitrix24_widget_invoice}}', 'account_id', '{{%bitrix24_client}}', 'id');
        $this->addForeignKey('fk_bitrix24_widget_invoice__invoice_id', '{{%bitrix24_widget_invoice}}', 'invoice_id', '{{%invoice}}', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%bitrix24_widget_invoice}}');
    }
}
