<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181204_163628_add_is_own_to_logistics_request extends Migration
{
    public $tableName = 'logistics_request';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'is_own', $this->boolean()->defaultValue(0));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'is_own');
    }
}


