<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200901_094654_append_column_to_user_config extends Migration
{
    public $tableName = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'packing_list_order_sum_without_nds', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'packing_list_order_sum_nds', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'packing_list_contractor_inn', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'packing_list_payment_date', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'packing_list_responsible', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'packing_list_source', $this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'packing_list_order_sum_without_nds');
        $this->dropColumn($this->tableName, 'packing_list_order_sum_nds');
        $this->dropColumn($this->tableName, 'packing_list_contractor_inn');
        $this->dropColumn($this->tableName, 'packing_list_payment_date');
        $this->dropColumn($this->tableName, 'packing_list_responsible');
        $this->dropColumn($this->tableName, 'packing_list_source');
    }
}
