<?php

use console\components\db\Migration;

class m191029_172852_alter_evtor_employee_stores extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('{{evotor_employee}}','stores');
    }

    public function down()
    {
        $this->addColumn('{{evotor_employee}}', 'stores', $this->string(900)->comment('Список ID магазинов через запятую'));
    }
}
