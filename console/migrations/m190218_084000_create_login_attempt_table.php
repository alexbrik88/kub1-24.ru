<?php

use yii\db\Migration;

/**
 * Handles the creation of table `login_attempt`.
 */
class m190218_084000_create_login_attempt_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('login_attempt', [
            'id' => $this->primaryKey(),
            'key'=> $this->string()->notNull(),
            'ip'=> $this->string()->notNull(),
            'amount'=> $this->integer(2)->defaultValue(1),
            'reset_at'=> $this->integer(),
            'updated_at'=> $this->integer(),
            'created_at'=> $this->integer(),
        ]);
        $this->createIndex('login_attempt_key_index','login_attempt', 'key', false);
        $this->createIndex('login_attempt_ip_index','login_attempt', 'ip', false);
        $this->createIndex('login_attempt_amount_index','login_attempt', 'amount', false);
        $this->createIndex('login_attempt_reset_at_index','login_attempt', 'reset_at', false);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropIndex('login_attempt_key_index', 'login_attempt');
        $this->dropIndex('login_attempt_ip_index', 'login_attempt');
        $this->dropIndex('login_attempt_amount_index', 'login_attempt');
        $this->dropIndex('login_attempt_reset_at_index', 'login_attempt');
        $this->dropTable('login_attempt');
    }
}
