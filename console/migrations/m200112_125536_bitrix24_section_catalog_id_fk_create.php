<?php

use console\components\db\Migration;

class m200112_125536_bitrix24_section_catalog_id_fk_create extends Migration
{
    const TABLE = '{{%bitrix24_section}}';

    public function safeUp()
    {
        $this->addForeignKey(
            'bitrix24_section_catalog_id', self::TABLE, ['company_id', 'catalog_id'],
            '{{%bitrix24_catalog}}', ['company_id', 'id'],
            'CASCADE', 'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('bitrix24_section_catalog_id', self::TABLE);
    }
}
