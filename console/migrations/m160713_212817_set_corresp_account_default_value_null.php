<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160713_212817_set_corresp_account_default_value_null extends Migration
{
    public $tContractor = 'contractor';

    public function safeUp()
    {
        $this->alterColumn($this->tContractor, 'corresp_account', $this->string(20)->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->alterColumn($this->tContractor, 'corresp_account', $this->string(20)->notNull());
    }
}


