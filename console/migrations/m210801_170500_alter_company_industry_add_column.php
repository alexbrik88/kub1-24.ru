<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210801_170500_alter_company_industry_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('company_industry', 'status_id', $this->tinyInteger()->notNull()->defaultValue(1)->after('industry_type_id'));
    }

    public function safeDown()
    {
        $this->dropColumn('company_industry', 'status_id');
    }
}
