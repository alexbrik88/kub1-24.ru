<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210202_201641_create_news extends Migration
{
    public function safeUp()
    {
        $this->createTable('news', [
            'id' => $this->primaryKey(11)->unsigned(),
            'name' => $this->string()->notNull(),
            'text' => $this->text()->notNull(),
            'date' => $this->date()->notNull(),
            'type' => $this->tinyInteger(3)->unsigned()->notNull(),
            'status' => $this->tinyInteger(3)->unsigned()->notNull(),
            'serial_number' => $this->integer(11)->unsigned()->notNull(),
            'likes_count' => $this->integer(11)->unsigned(),
            'dislikes_count' => $this->integer(11)->unsigned(),
            'created_at' => $this->integer(11)->unsigned()->notNull(),
            'updated_at' => $this->integer(11)->unsigned()->notNull(),
        ]);

        $this->createTable('news_reactions', [
            'company_id' => $this->integer(11)->notNull(),
            'news_id' => $this->integer(11)->unsigned()->notNull(),
            'reaction' => $this->tinyInteger()->unsigned()->notNull(),
        ]);
        $this->addPrimaryKey('PRIMARY_KEY', '{{%news_reactions}}', ['company_id', 'news_id']);

        $this->addForeignKey('FK_news_reactions_to_company', 'news_reactions', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey('FK_news_reactions_to_news', 'news_reactions', 'news_id', 'news', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable('news_reactions');
        $this->dropTable('news');
    }
}
