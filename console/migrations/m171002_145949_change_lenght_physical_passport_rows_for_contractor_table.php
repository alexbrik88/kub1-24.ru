<?php

use console\components\db\Migration;
use yii\db\Schema;

class m171002_145949_change_lenght_physical_passport_rows_for_contractor_table extends Migration
{
    public $tableName = 'contractor';

    public function safeUp()
    {
        $this->alterColumn($this->tableName,'physical_passport_number','VARCHAR(25) NULL DEFAULT NULL');
        $this->alterColumn($this->tableName,'physical_passport_department','VARCHAR(255) NULL DEFAULT NULL');
        $this->alterColumn($this->tableName,'physical_passport_series','VARCHAR(25) NULL DEFAULT NULL');

    }
    
    public function safeDown()
    {
        $this->alterColumn($this->tableName,'physical_passport_number','VARCHAR(6) NULL DEFAULT NULL');
        $this->alterColumn($this->tableName,'physical_passport_department','VARCHAR(7) NULL DEFAULT NULL');
        $this->alterColumn($this->tableName,'physical_passport_series','VARCHAR(5) NULL DEFAULT NULL');

    }
}


