<?php

use yii\db\Migration;

class m210610_220045_drop_crm_client_event_table extends Migration
{
    /**
     * @inheritDoc
     */
    public function safeUp()
    {
        $this->dropTable('{{%crm_client_event}}');
    }

    /**
     * @inheritDoc
     */
    public function down(): bool
    {
        return false;
    }
}
