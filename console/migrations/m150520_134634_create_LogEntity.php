<?php

use frontend\models\log\LogEntityType;
use yii\db\Schema;
use yii\db\Migration;

class m150520_134634_create_LogEntity extends Migration
{
    public function safeUp()
    {
        $this->createTable('log_entity_type', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . '(45) NOT NULL',
        ]);

        $this->batchInsert('log_entity_type', ['id', 'name',],  [
            [1, 'счёт'],
            [2, 'акт'],
            [3, 'товарная накладная'],
            [4, 'счёт-фактура'],
            [5, 'платёжное поручение'],
            [6, 'банковская операция'],
            [7, 'перевод e-money'],
            [8, 'кассовый ордер'],
        ]);
    }
    
    public function safeDown()
    {
        $this->dropTable('log_entity_type');
    }
}
