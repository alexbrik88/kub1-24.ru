<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180712_142450_alter_out_invoice_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('out_invoice', 'invoice_comment', $this->string()->defaultValue('')->after('is_additional_number_before'));
    }

    public function safeDown()
    {
        $this->dropColumn('out_invoice', 'invoice_comment');
    }
}
