<?php

use yii\db\Schema;
use yii\db\Migration;

class m150617_083500_alter_Contractor extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('contractor', 'ITN', Schema::TYPE_STRING . '(12) NOT NULL');
        $this->alterColumn('contractor', 'BIN', 'CHAR(13) NOT NULL');
        $this->alterColumn('contractor', 'PPC', 'CHAR(9) NOT NULL');
        $this->alterColumn('contractor', 'BIC', 'CHAR(9) NOT NULL');
        $this->alterColumn('contractor', 'current_account', 'CHAR(20) NOT NULL');
        $this->alterColumn('contractor', 'corresp_account', 'CHAR(20) NOT NULL');
    }
    
    public function safeDown()
    {
        $this->alterColumn('contractor', 'ITN', Schema::TYPE_INTEGER . ' NOT NULL');
        $this->alterColumn('contractor', 'BIN', Schema::TYPE_INTEGER . ' NOT NULL');
        $this->alterColumn('contractor', 'PPC', Schema::TYPE_INTEGER . ' NOT NULL');
        $this->alterColumn('contractor', 'BIC', Schema::TYPE_INTEGER . ' NOT NULL');
        $this->alterColumn('contractor', 'current_account', Schema::TYPE_INTEGER . ' NOT NULL');
        $this->alterColumn('contractor', 'corresp_account', Schema::TYPE_INTEGER . ' NOT NULL');
    }
}
