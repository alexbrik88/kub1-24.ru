<?php

use console\components\db\Migration;
use yii\db\Schema;

class m161206_082402_alter_act_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('act', 'is_original', $this->boolean()->defaultValue(false));
    }
    
    public function safeDown()
    {
        $this->dropColumn('act', 'is_original');
    }
}
