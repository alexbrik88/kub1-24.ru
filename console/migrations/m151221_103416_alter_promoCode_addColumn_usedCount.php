<?php

use console\components\db\Migration;
use yii\db\Schema;

class m151221_103416_alter_promoCode_addColumn_usedCount extends Migration
{
    public $tPromo = 'service_payment_promo_code';

    public function safeUp()
    {
        $this->addColumn($this->tPromo, 'used_count', $this->integer()->defaultValue(0) . ' AFTER is_used');

        $this->update($this->tPromo, [
            'used_count' => 1,
        ], [
            'is_used' => 1,
        ]);
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tPromo, 'used_count');
    }
}


