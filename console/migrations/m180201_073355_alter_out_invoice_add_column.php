<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180201_073355_alter_out_invoice_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('out_invoice', 'is_autocomplete', $this->boolean()->defaultValue(false)->after('return_url'));
        $this->addColumn('out_invoice', 'additional_number', $this->string(45)->after('is_autocomplete'));
        $this->addColumn('out_invoice', 'is_additional_number_before', $this->boolean()->defaultValue(false)->after('additional_number'));
    }

    public function safeDown()
    {
        $this->dropColumn('out_invoice', 'is_autocomplete');
        $this->dropColumn('out_invoice', 'additional_number');
        $this->dropColumn('out_invoice', 'is_additional_number_before');
    }
}
