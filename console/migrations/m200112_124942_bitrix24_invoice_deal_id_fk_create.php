<?php

use console\components\db\Migration;

class m200112_124942_bitrix24_invoice_deal_id_fk_create extends Migration
{
    const TABLE = '{{%bitrix24_invoice}}';

    public function safeUp()
    {
        $this->addForeignKey(
            'bitrix24_invoice_deal_id', self::TABLE, ['company_id', 'deal_id'],
            '{{%bitrix24_deal}}', ['company_id', 'id'],
            'CASCADE', 'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('bitrix24_invoice_deal_id', self::TABLE);
    }
}
