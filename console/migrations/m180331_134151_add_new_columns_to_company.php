<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180331_134151_add_new_columns_to_company extends Migration
{
    public $tableName = 'company';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'store_where_empty_products_type', $this->integer()->defaultValue(0));
        $this->addColumn($this->tableName, 'store_where_empty_products_text_type', $this->integer()->defaultValue(null));
        $this->addColumn($this->tableName, 'store_show_novelty_button', $this->boolean()->defaultValue(false));
        $this->addColumn($this->tableName, 'store_novelty_product_by_days_count', $this->integer()->defaultValue(null));
        $this->addColumn($this->tableName, 'store_has_discount', $this->boolean()->defaultValue(false));
        $this->addColumn($this->tableName, 'store_discount_from_amount', $this->integer()->defaultValue(null));
        $this->addColumn($this->tableName, 'store_discount_type', $this->integer()->defaultValue(null));
        $this->addColumn($this->tableName, 'store_discount', $this->integer()->defaultValue(null));
        $this->addColumn($this->tableName, 'store_discount_another', $this->string()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'store_where_empty_products_type');
        $this->dropColumn($this->tableName, 'store_where_empty_products_text_type');
        $this->dropColumn($this->tableName, 'store_show_novelty_button');
        $this->dropColumn($this->tableName, 'store_novelty_product_by_days_count');
        $this->dropColumn($this->tableName, 'store_has_discount');
        $this->dropColumn($this->tableName, 'store_discount_from_amount');
        $this->dropColumn($this->tableName, 'store_discount_type');
        $this->dropColumn($this->tableName, 'store_discount');
        $this->dropColumn($this->tableName, 'store_discount_another');
    }
}


