<?php

use yii\db\Migration;

class m210527_152528_update_crm_client_table extends Migration
{
    /**
     * @inheritDoc
     */
    public function safeUp(): void
    {
        $this->db->createCommand("UPDATE crm_client SET client_id = contractor_id")->execute();
    }
}
