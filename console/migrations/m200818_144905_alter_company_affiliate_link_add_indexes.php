<?php

use common\models\Company;
use common\models\company\CompanyAffiliateLink;
use console\components\db\Migration;
use yii\db\Schema;

class m200818_144905_alter_company_affiliate_link_add_indexes extends Migration
{
    public function safeUp()
    {
        $this->createIndex('company_field_affiliate_link_idx', Company::tableName(), 'affiliate_link', true);
        $this->createIndex('company_affiliate_link_field_link_idx', CompanyAffiliateLink::tableName(), 'link', true);
        $this->createIndex('company_affiliate_link_field_custom_link_id_idx', CompanyAffiliateLink::tableName(), 'custom_link_id', true);
    }

    public function safeDown()
    {
        $this->dropIndex('company_field_affiliate_link_idx', Company::tableName());
        $this->dropIndex('company_affiliate_link_field_link_idx', CompanyAffiliateLink::tableName());
        $this->dropIndex('company_affiliate_link_field_custom_link_id_idx', CompanyAffiliateLink::tableName());
    }

}
