<?php

use console\components\db\Migration;

class m160805_111403_create_cash_bank_flow_to_invoice extends Migration
{
    public function safeUp()
    {
        $this->createTable('cash_bank_flow_to_invoice', [
            'id' => $this->primaryKey(),
            'flow_id' => $this->integer()->notNull(),
            'invoice_id' => $this->integer()->notNull(),
        ], $this->tableOptions);

        $this->addForeignKey('fk_cash_bank_flow_to_invoice_flow', 'cash_bank_flow_to_invoice', 'flow_id', 'cash_bank_flows', 'id', 'CASCADE');
        $this->addForeignKey('fk_cash_bank_flow_to_invoice_invoice', 'cash_bank_flow_to_invoice', 'invoice_id', 'invoice', 'id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable('cash_bank_flow_to_invoice');
    }
}
