<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210324_080605_add_registration_page_type extends Migration
{
    public $tableName = '{{%registration_page_type}}';

    public function safeUp()
    {
        $this->insert($this->tableName, [
            'id' => 39,
            'name' => 'Блог',
        ]);
    }

    public function safeDown()
    {
        $this->delete($this->tableName, ['id' => 39]);
    }
}
