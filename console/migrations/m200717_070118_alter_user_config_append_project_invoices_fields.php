<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200717_070118_alter_user_config_append_project_invoices_fields extends Migration
{
    public $tableName = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'project_invoices_scan', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'project_invoices_paylimit', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'project_invoices_paydate', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn($this->tableName, 'project_invoices_act', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'project_invoices_paclist', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn($this->tableName, 'project_invoices_invfacture', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'project_invoices_upd', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn($this->tableName, 'project_invoices_responsible', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn($this->tableName, 'project_invoices_comment', $this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'project_invoices_scan');
        $this->dropColumn($this->tableName, 'project_invoices_paylimit');
        $this->dropColumn($this->tableName, 'project_invoices_paydate');
        $this->dropColumn($this->tableName, 'project_invoices_act');
        $this->dropColumn($this->tableName, 'project_invoices_paclist');
        $this->dropColumn($this->tableName, 'project_invoices_invfacture');
        $this->dropColumn($this->tableName, 'project_invoices_upd');
        $this->dropColumn($this->tableName, 'project_invoices_responsible');
        $this->dropColumn($this->tableName, 'project_invoices_comment');
    }
}
