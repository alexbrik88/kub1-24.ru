<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210603_142535_alter_company_drop_column extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('{{%company}}', 'last_visit_at');
    }

    public function safeDown()
    {
        $this->addColumn('{{%company}}', 'last_visit_at', $this->integer()->after('company_type_id'));
    }
}
