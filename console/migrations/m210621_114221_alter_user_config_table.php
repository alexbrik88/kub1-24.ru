<?php

use yii\db\Migration;

class m210621_114221_alter_user_config_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%user_config}}';

    /**
     * @inheritDoc
     */
    public function up(): void
    {
        $this->renameColumn(self::TABLE_NAME, 'crm_client_deal', 'crm_deal_type');
        $this->renameColumn(self::TABLE_NAME, 'crm_client_deal_stage', 'crm_deal_stage');
        $this->addColumn(self::TABLE_NAME, 'crm_deal', $this->boolean()->notNull()->defaultValue(false));
    }

    /**
     * @inheritDoc
     */
    public function down(): bool
    {
        return false;
    }
}
