<?php

use yii\db\Schema;
use yii\db\Migration;

class m150618_072341_alter_PaymentOrder_changeTypeOfSumToInt extends Migration
{
    public function safeUp()
    {
        $this->update('payment_order', [
            'sum' => new \yii\db\Expression('sum * 100'),
        ]);
        $this->alterColumn('payment_order', 'sum', Schema::TYPE_INTEGER . ' NOT NULL');
    }
    
    public function safeDown()
    {
        $this->alterColumn('payment_order', 'sum', Schema::TYPE_FLOAT . ' NOT NULL');
        $this->update('payment_order', [
            'sum' => new \yii\db\Expression('sum / 100'),
        ]);
    }
}
