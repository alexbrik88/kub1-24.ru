<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210712_193857_alter_config_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_config', 'report_marketing_planning_round_numbers', $this->tinyInteger(1)->notNull()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('user_config', 'report_marketing_planning_round_numbers');
    }
}
