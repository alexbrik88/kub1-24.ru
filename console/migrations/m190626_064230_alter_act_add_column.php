<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190626_064230_alter_act_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('act', 'auto_created', $this->boolean()->defaultValue(false));
        $this->createIndex('auto_created', 'act', 'auto_created');
    }

    public function safeDown()
    {
        $this->dropIndex('auto_created', 'act');
        $this->dropColumn('act', 'auto_created');
    }
}
