<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201015_121111_drop_triggers_sale_invoice_olap_documents extends Migration
{
    public function safeUp()
    {
        $this->execute("DROP TRIGGER IF EXISTS `olap_documents_insert_order_sales_invoice`");

        $this->execute("DROP TRIGGER IF EXISTS `olap_documents_update_order_sales_invoice`");

        $this->execute("DROP TRIGGER IF EXISTS `olap_documents_delete_sales_invoice`");

        $this->execute("DROP TRIGGER IF EXISTS `olap_documents_update_sales_invoice`");

        $this->execute("DROP TRIGGER IF EXISTS `olap_invoices_insert_sales_invoice`");

        $this->execute("DROP TRIGGER IF EXISTS `olap_invoices_insert_sales_invoice`");
    }

    public function safeDown()
    {

    }
}
