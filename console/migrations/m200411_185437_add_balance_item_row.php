<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200411_185437_add_balance_item_row extends Migration
{
    public $tableName = '{{%balance_item}}';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'subname', $this->string()->defaultValue(''));
        $this->batchInsert($this->tableName, ['id', 'name', 'subname', 'sort'], [
            [25, 'Нераспределенная прибыль', 'Прошлые периоды', 3],
        ]);

        $this->execute('UPDATE `balance_item` SET `subname` = "Текущий год" WHERE `id` = 16 LIMIT 1');
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'subname');
        $this->execute('DELETE FROM `balance_item` WHERE `id` = 25 LIMIT 1');
    }
}
