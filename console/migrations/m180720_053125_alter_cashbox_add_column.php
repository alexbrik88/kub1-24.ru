<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180720_053125_alter_cashbox_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cashbox', 'accessible_to_all', $this->boolean()->defaultValue(false)->after('responsible_employee_id'));

        $this->createIndex('accessible_to_all', 'cashbox', 'accessible_to_all');
        $this->createIndex('is_accounting', 'cashbox', 'is_accounting');
        $this->createIndex('is_main', 'cashbox', 'is_main');
        $this->createIndex('is_closed', 'cashbox', 'is_closed');
    }

    public function safeDown()
    {
        $this->dropIndex('accessible_to_all', 'cashbox');
        $this->dropIndex('is_accounting', 'cashbox');
        $this->dropIndex('is_main', 'cashbox');
        $this->dropIndex('is_closed', 'cashbox');

        $this->dropColumn('cashbox', 'accessible_to_all');
    }
}
