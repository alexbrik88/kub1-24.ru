<?php

use console\components\db\Migration;
use yii\db\Schema;

class m171102_092247_alter_pay_sum_in_state_report extends Migration
{
    public $tableName = 'report_state';

    public function safeUp()
    {
        $this->alterColumn($this->tableName, 'pay_sum', $this->integer()->notNull());
    }
    
    public function safeDown()
    {
        $this->alterColumn($this->tableName, 'pay_sum', $this->string()->notNull());
    }
}


