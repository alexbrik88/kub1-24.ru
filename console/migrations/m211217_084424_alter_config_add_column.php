<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211217_084424_alter_config_add_column extends Migration
{
    public $tableName = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'project_income', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn($this->tableName, 'project_expense', $this->boolean()->notNull()->defaultValue(true));
        $this->addColumn($this->tableName, 'project_profit', $this->boolean()->notNull()->defaultValue(true));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'project_income');
        $this->dropColumn($this->tableName, 'project_expense');
        $this->dropColumn($this->tableName, 'project_profit');
    }
}
