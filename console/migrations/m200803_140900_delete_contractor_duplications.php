<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200803_140900_delete_contractor_duplications extends Migration
{
    public function safeUp()
    {
        $kubCompanyId = Yii::$app->params['service']['company_id'] ?? null;
        if (empty($kubCompanyId)) {
            return;
        }

        $ids = (new \yii\db\Query)->select('created_from_company_id')->from('contractor')->where([
            'company_id' => $kubCompanyId,
            'type' => 1,
        ])->andWhere([
            'not',
            ['created_from_company_id' => null],
        ])->distinct()->column();

        foreach ($ids as $fromCompanyId) {
            $contractorIds = (new \yii\db\Query)->select('contractor.id')->from('contractor')->leftJoin(
                '{{invoice}}',
                '{{invoice}}.[[contractor_id]] = {{contractor}}.[[id]]'
            )->where([
                'contractor.company_id' => $kubCompanyId,
                'contractor.created_from_company_id' => $fromCompanyId,
                'contractor.type' => 1,
                'invoice.id' => null,
            ])->column();

            foreach ($contractorIds as $id) {
                try {
                    $this->delete('contractor_account', ['contractor_id' => $id]);
                    $this->delete('contractor', ['id' => $id]);
                } catch (\Exception $e) {
                    // do nothing
                }
            }
        }
    }

    public function safeDown()
    {
        //
    }
}
