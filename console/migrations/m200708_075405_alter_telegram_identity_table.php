<?php

use console\components\db\Migration;

class m200708_075405_alter_telegram_identity_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%telegram_identity}}';

    /**
     * @inheritDoc`
     */
    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'notification_count', $this->bigInteger()->notNull()->defaultValue(0));
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'notification_count');
    }
}
