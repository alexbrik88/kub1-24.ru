<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200318_063451_alter_service_payment_promo_code_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%service_payment_promo_code}}', 'tariff_limit', $this->integer()->after('tariff_group_id'));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%service_payment_promo_code}}', 'tariff_limit');
    }
}
