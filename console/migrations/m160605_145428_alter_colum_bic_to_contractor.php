<?php

use console\components\db\Migration;

class m160605_145428_alter_colum_bic_to_contractor extends Migration
{
    public $tContractor = 'contractor';

    public function safeUp()
    {
        $this->alterColumn($this->tContractor, 'BIC', $this->char(9));
    }

    public function safeDown()
    {
        $this->alterColumn($this->tContractor, 'BIC', $this->char(9)->notNull());
    }
}


