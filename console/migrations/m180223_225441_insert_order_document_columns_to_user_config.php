
<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180223_225441_insert_order_document_columns_to_user_config extends Migration
{
    public $tableName = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'order_document_payment_sum', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'order_document_ship_up_to_date', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'order_document_stock_id', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'order_document_invoice', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'order_document_act', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'order_document_packing_list', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'order_document_invoice_facture', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'order_document_responsible_employee_id', $this->boolean()->notNull()->defaultValue(false));

        $this->addCommentOnColumn($this->tableName, 'order_document_payment_sum', 'Показ колонки "Оплачено" в списке заказов');
        $this->addCommentOnColumn($this->tableName, 'order_document_ship_up_to_date', 'Показ колонки "План. дата отгрузки" в списке заказов');
        $this->addCommentOnColumn($this->tableName, 'order_document_stock_id', 'Показ колонки "Со склада" в списке заказов');
        $this->addCommentOnColumn($this->tableName, 'order_document_invoice', 'Показ колонки "Счет" в списке заказов');
        $this->addCommentOnColumn($this->tableName, 'order_document_act', 'Показ колонки "Акт" в списке заказов');
        $this->addCommentOnColumn($this->tableName, 'order_document_packing_list', 'Показ колонки "ТН" в списке заказов');
        $this->addCommentOnColumn($this->tableName, 'order_document_invoice_facture', 'Показ колонки "СФ" в списке заказов');
        $this->addCommentOnColumn($this->tableName, 'order_document_responsible_employee_id', 'Показ колонки "Ответственный" в списке заказов');
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'order_document_payment_sum');
        $this->dropColumn($this->tableName, 'order_document_ship_up_to_date');
        $this->dropColumn($this->tableName, 'order_document_stock_id');
        $this->dropColumn($this->tableName, 'order_document_invoice');
        $this->dropColumn($this->tableName, 'order_document_act');
        $this->dropColumn($this->tableName, 'order_document_packing_list');
        $this->dropColumn($this->tableName, 'order_document_invoice_facture');
        $this->dropColumn($this->tableName, 'order_document_responsible_employee_id');
    }
}


