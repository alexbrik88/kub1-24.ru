<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190902_203142_add_cost_per_conversion_to_yandex_direct_report extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%yandex_direct_report}}', 'cost_per_conversion', $this->float(2)->defaultValue(null));
        $this->alterColumn('{{%yandex_direct_report}}', 'avg_cpc', $this->float(2)->defaultValue(null));
        $this->alterColumn('{{%yandex_direct_report}}', 'avg_page_views_count', $this->float(2)->defaultValue(null));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%yandex_direct_report}}', 'cost_per_conversion');
        $this->alterColumn('{{%yandex_direct_report}}', 'avg_cpc', $this->float(2)->notNull());
        $this->alterColumn('{{%yandex_direct_report}}', 'avg_page_views_count', $this->float(2)->notNull());
    }
}
