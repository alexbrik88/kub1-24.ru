<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211027_094016_update_articles_profit_loss_type extends Migration
{
    public function safeUp()
    {
        $operatingType = 30; // AnalyticsArticle::PAL_EXPENSES_OPERATING
        $fixedType = 20; //  AnalyticsArticle::PAL_EXPENSES_FIXED

        $this->execute(" UPDATE `analytics_article` SET `profit_and_loss_type` = {$fixedType} WHERE `profit_and_loss_type` = {$operatingType}");
    }

    public function safeDown()
    {
        // no revert
    }
}
