<?php

use common\models\product\Product;
use console\components\db\Migration;
use frontend\models\Documents;

class m170327_102727_new_order_act_transaction extends Migration
{
    public function safeUp()
    {
        /* @var $act \common\models\document\Act */
        foreach (\common\models\document\Act::find()->all() as $act) {
            if (!$act->getOrderActs()->exists()) {
                $act->order_sum = 0;
                $act->order_sum_without_nds = 0;
                $act->order_nds = 0;
                foreach (\common\models\document\Order::findAll(['invoice_id' => $act->invoice_id]) as $order) {
                    if ($order->product->production_type == Product::PRODUCTION_TYPE_SERVICE) {
                        $orderAct = new \common\models\document\OrderAct();
                        $orderAct->act_id = $act->id;
                        $orderAct->order_id = $order->id;
                        $orderAct->product_id = $order->product_id;
                        $orderAct->invoice_id = $order->invoice_id;
                        $orderAct->quantity = $order->quantity;
                        $orderAct->amount_purchase_no_vat = $order->amount_purchase_no_vat;
                        $orderAct->amount_sales_no_vat = $order->amount_sales_no_vat;
                        $orderAct->amount_purchase_with_vat = $order->amount_purchase_with_vat;
                        $orderAct->amount_sales_with_vat = $order->amount_sales_with_vat;
                        if ($order->invoice->type == Documents::IO_TYPE_IN) {
                            $orderAct->amount_vat = $orderAct->amount_purchase_with_vat - $orderAct->amount_purchase_no_vat;
                            $act->order_sum += $orderAct->amount_purchase_with_vat;
                            $act->order_sum_without_nds += $orderAct->amount_purchase_no_vat;
                            $act->order_nds += $orderAct->amount_vat;
                        } else {
                            $orderAct->amount_vat = $orderAct->amount_sales_with_vat - $orderAct->amount_sales_no_vat;
                            $act->order_sum += $orderAct->amount_sales_with_vat;
                            $act->order_sum_without_nds += $orderAct->amount_sales_no_vat;
                            $act->order_nds += $orderAct->amount_vat;
                        }
                        $orderAct->save();

                    }
                }
                $act->save(false);
            }
        }
    }

    public function safeDown()
    {

    }
}


