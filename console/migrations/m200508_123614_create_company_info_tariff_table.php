<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%company_info_tariff}}`.
 */
class m200508_123614_create_company_info_tariff_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%company_info_tariff}}', [
            'company_id' => $this->integer()->notNull(),
            'tariff_group_id' => $this->integer()->notNull(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%company_info_tariff}}');
    }
}
