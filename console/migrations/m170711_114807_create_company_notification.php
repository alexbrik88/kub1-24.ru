<?php

use console\components\db\Migration;
use yii\db\Schema;
use common\models\notification\Notification;
use common\models\Company;

class m170711_114807_create_company_notification extends Migration
{
    public $tableName = 'company_notification';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'notification_id' => $this->integer()->notNull(),
            'status' => $this->boolean()->defaultValue(false),
        ]);

        $this->addForeignKey($this->tableName . '_company_id', $this->tableName, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tableName . '_notification_id', $this->tableName, 'notification_id', 'notification', 'id', 'RESTRICT', 'CASCADE');

        $i = 1;
        /* @var $notification Notification */
        foreach (Notification::find()->byNotificationType(Notification::NOTIFICATION_TYPE_TAX)->all() as $notification) {
            /* @var $company Company */
            foreach (Company::find()->all() as $company) {
                $this->insert($this->tableName, [
                    'id' => $i,
                    'company_id' => $company->id,
                    'notification_id' => $notification->id,
                    'status' => Notification::STATUS_NOT_PASSED,
                ]);
                $i++;
            }
        }
    }

    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_company_id', $this->tableName);
        $this->dropForeignKey($this->tableName . '_notification_id', $this->tableName);

        $this->dropTable($this->tableName);
    }
}


