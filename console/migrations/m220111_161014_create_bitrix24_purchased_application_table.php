<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%bitrix24_purchased_application}}`.
 * Has foreign keys to the tables:
 *
 * - `{{%bitrix24_application}}`
 * - `{{%bitrix24_client}}`
 */
class m220111_161014_create_bitrix24_purchased_application_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%bitrix24_purchased_application}}', [
            'id' => $this->primaryKey(),
            'app_id' => $this->integer()->notNull(),
            'client_id' => $this->integer()->notNull(),
            'date' => $this->datetime(),
            'date_limit' => $this->datetime(),
            'installed' => $this->boolean(),
            'settings' => $this->string(),
            'user_limit' => $this->integer(),
        ]);

        // creates index for column `app_id`
        $this->createIndex(
            '{{%idx-bitrix24_purchased_application-app_id}}',
            '{{%bitrix24_purchased_application}}',
            'app_id'
        );

        // add foreign key for table `{{%bitrix24_application}}`
        $this->addForeignKey(
            '{{%fk-bitrix24_purchased_application-app_id}}',
            '{{%bitrix24_purchased_application}}',
            'app_id',
            '{{%bitrix24_application}}',
            'id',
            'CASCADE'
        );

        // creates index for column `client_id`
        $this->createIndex(
            '{{%idx-bitrix24_purchased_application-client_id}}',
            '{{%bitrix24_purchased_application}}',
            'client_id'
        );

        // add foreign key for table `{{%bitrix24_client}}`
        $this->addForeignKey(
            '{{%fk-bitrix24_purchased_application-client_id}}',
            '{{%bitrix24_purchased_application}}',
            'client_id',
            '{{%bitrix24_client}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%bitrix24_application}}`
        $this->dropForeignKey(
            '{{%fk-bitrix24_purchased_application-app_id}}',
            '{{%bitrix24_purchased_application}}'
        );

        // drops index for column `app_id`
        $this->dropIndex(
            '{{%idx-bitrix24_purchased_application-app_id}}',
            '{{%bitrix24_purchased_application}}'
        );

        // drops foreign key for table `{{%bitrix24_client}}`
        $this->dropForeignKey(
            '{{%fk-bitrix24_purchased_application-client_id}}',
            '{{%bitrix24_purchased_application}}'
        );

        // drops index for column `client_id`
        $this->dropIndex(
            '{{%idx-bitrix24_purchased_application-client_id}}',
            '{{%bitrix24_purchased_application}}'
        );

        $this->dropTable('{{%bitrix24_purchased_application}}');
    }
}
