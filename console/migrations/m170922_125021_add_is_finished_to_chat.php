<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170922_125021_add_is_finished_to_chat extends Migration
{
    public $tableName = 'chat';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'is_finished', $this->boolean()->defaultValue(true));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'is_finished');
    }
}


