<?php

use yii\db\Schema;
use yii\db\Migration;

class m150917_090258_alter_employee_updateEnlargeColumn_address extends Migration
{
    public $tEmployee = 'contractor';

    public function safeUp()
    {
        $this->alterColumn($this->tEmployee, 'legal_address', $this->string(160));
        $this->alterColumn($this->tEmployee, 'actual_address', $this->string(160));
    }
    
    public function safeDown()
    {
        $this->alterColumn($this->tEmployee, 'legal_address', $this->string(45));
        $this->alterColumn($this->tEmployee, 'actual_address', $this->string(45));
    }
}
