<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200225_215844_rework_google_ads_campaign_table extends Migration
{
    public function safeUp()
    {
        $this->renameColumn('{{%google_ads_campaign}}', 'is_trial_type', 'type');
        $this->alterColumn('{{%google_ads_campaign}}', 'type', $this->string()->defaultValue(null));
    }

    public function safeDown()
    {
        $this->renameColumn('{{%google_ads_campaign}}', 'type', 'is_trial_type');
        $this->alterColumn('{{%google_ads_campaign}}', 'is_trial_type', $this->boolean()->defaultValue(false)->notNull());
    }
}
