<?php

use yii\db\Migration;

/**
 * Handles the creation of table `scan_document`.
 */
class m180426_072828_create_scan_document_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('scan_document', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'employee_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'name' => $this->string(50),
            'description' => $this->string(250),
        ]);

        $this->addForeignKey ('FK_scanDocument_company', 'scan_document', 'company_id', 'company', 'id', 'CASCADE');
        $this->addForeignKey ('FK_scanDocument_employee', 'scan_document', 'employee_id', 'employee', 'id');
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('scan_document');
    }
}
