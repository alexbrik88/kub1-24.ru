<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191016_143110_alter_order_upd_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%order_upd}}', 'country_id', $this->integer()->notNull());
        $this->addColumn('{{%order_upd}}', 'custom_declaration_number', $this->string()->notNull());

        $this->execute('
            UPDATE {{%order_upd}} AS {{%order_upd}}
            LEFT JOIN {{%order}} ON {{%order_upd}}.[[order_id]] = {{%order}}.[[id]]
            LEFT JOIN {{%country}} ON {{%order}}.[[country_id]] = {{%country}}.[[id]]
            SET {{%order_upd}}.[[country_id]] = COALESCE({{%country}}.[[id]], 1),
                {{%order_upd}}.[[custom_declaration_number]] = {{%order}}.[[custom_declaration_number]]
        ');

        $this->addForeignKey('fk_orderUpd_country', '{{%order_upd}}', 'country_id', '{{%country}}', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_orderUpd_country', '{{%order_upd}}');
        $this->dropColumn('{{%order_upd}}', 'country_id');
        $this->dropColumn('{{%order_upd}}', 'custom_declaration_number');
    }
}
