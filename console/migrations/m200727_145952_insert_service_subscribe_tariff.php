<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200727_145952_insert_service_subscribe_tariff extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%service_subscribe_tariff}}', [
            'id' => 70,
            'tariff_group_id' => 17,
            'duration_month' => 0,
            'duration_day' => 14,
            'price' => 0,
            'is_active' => 0,
            'tariff_limit' => 3,
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%service_subscribe_tariff}}', [
            'id' => 70,
        ]);
    }
}
