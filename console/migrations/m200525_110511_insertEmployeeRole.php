<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200525_110511_insertEmployeeRole extends Migration
{
    public function safeUp()
    {
        $this->insert('employee_role', [
            'id' => 12,
            'name' => 'Маркетолог',
            'sort' => 16000
        ]);
    }

    public function safeDown()
    {
        $this->delete('employee_role', ['id' => 12]);
    }
}
