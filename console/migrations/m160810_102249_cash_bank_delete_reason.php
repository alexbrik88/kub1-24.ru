<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160810_102249_cash_bank_delete_reason extends Migration
{
    public function safeUp()
    {
        $this->dropForeignKey('fk_cash_bank_flows_reason_id', 'cash_bank_flows');
        $this->dropColumn('cash_bank_flows', 'reason_id');

        $this->dropTable('cash_bank_reason_type');
    }
    
    public function safeDown()
    {
        $this->createTable('cash_bank_reason_type', [
            'id' => $this->primaryKey(),
            'name' => $this->string(255)->notNull(),
            'flow_type' => $this->smallInteger()->defaultValue(null),
            'sort' => $this->smallInteger()->defaultValue(100),
        ], $this->tableOptions);

        $this->batchInsert('cash_bank_reason_type', ['name', 'flow_type', 'sort'], [
            ['Оплата поставщику', 0, 1],
            ['Аренда', 0, 100],
            ['Возврат', 0, 100],
            ['Выплата дивидендов', 0, 100],
            ['Выплата подотчетным лицам', 0, 100],
            ['Зарплата', 0, 100],
            ['Интернет', 0, 100],
            ['Комиссия банка', 0, 100],
            ['Мебель', 0, 100],
            ['Налоги', 0, 100],
            ['Налоги на ЗП', 0, 100],
            ['Оргтехника', 0, 100],
            ['Погашение займа', 0, 100],
            ['Прочие расходы', 0, 100],
            ['Реклама', 0, 100],
            ['Телефония', 0, 100],
            ['Товар', 0, 100],
            ['Услуги', 0, 100],
            ['Хоз. расходы', 0, 100],

            ['Оплата от покупателя', 1, 1],
            ['Взнос наличными', 1, 100],
            ['Возврат', 1, 100],
            ['Займ', 1, 100],
            ['Кредит', 1, 100],
            ['От учредителя', 1, 100],
            ['Прочие поступления', 1, 100],
        ]);

        $this->addColumn('cash_bank_flows', 'reason_id', $this->integer()->defaultValue(null));
        $this->addForeignKey('fk_cash_bank_flows_reason_id', 'cash_bank_flows', 'reason_id', 'cash_bank_reason_type', 'id');
    }
}


