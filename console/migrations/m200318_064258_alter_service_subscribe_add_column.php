<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200318_064258_alter_service_subscribe_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%service_subscribe}}', 'tariff_limit', $this->integer()->after('tariff_group_id'));

        $this->execute('
            UPDATE {{%service_subscribe}}
            LEFT JOIN {{%service_subscribe_tariff}} ON {{%service_subscribe_tariff}}.[[id]] = {{%service_subscribe}}.[[tariff_id]]
            SET {{%service_subscribe}}.[[tariff_limit]] = {{%service_subscribe_tariff}}.[[invoice_limit]]
        ');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%service_subscribe}}', 'tariff_limit');
    }
}
