<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%rent_agreement_item}}`.
 */
class m211119_125429_create_rent_agreement_item_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%rent_agreement_item}}', [
            'id' => $this->primaryKey()->unsigned(),
            'rent_agreement_id' => $this->integer()->unsigned()->notNull(),
            'rent_entity_id' => $this->integer()->unsigned()->notNull(),
            'number' => $this->integer()->notNull(),
            'price' => $this->bigInteger()->notNull(),
            'discount' => $this->decimal(22,6)->notNull(),
            'amount' => $this->bigInteger()->notNull(),
        ]);

        $this->addForeignKey('fk_rent_agreement_item__rent_entity_id', 'rent_agreement_item', 'rent_entity_id', 'rent_entity', 'id');
        $this->addForeignKey('fk_rent_agreement_item__rent_agreement_id', 'rent_agreement_item', 'rent_agreement_id', 'rent_agreement', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%rent_agreement_item}}');
    }
}
