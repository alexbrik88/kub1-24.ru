<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170118_143910_alter_company_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%company}}', 'free_tariff_start_at', $this->integer()->defaultValue(null));
        $this->addColumn('{{%company}}', 'is_free_tariff_notified', $this->boolean()->notNull()->defaultValue(false));
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%company}}', 'free_tariff_start_at');
        $this->dropColumn('{{%company}}', 'is_free_tariff_notified');
    }
}


