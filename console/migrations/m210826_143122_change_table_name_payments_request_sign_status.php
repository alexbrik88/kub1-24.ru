<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210826_143122_change_table_name_payments_request_sign_status extends Migration
{
    public function safeUp()
    {
        $this->renameTable('payments_request_sign_status', 'payments_request_sign_step');
        $this->addPrimaryKey("PK_payments_request_sign_step", 'payments_request_sign_step', ['request_id', 'employee_id']);
    }

    public function safeDown()
    {
        $this->dropPrimaryKey("PK_payments_request_sign_step", 'payments_request_sign_step');
        $this->renameTable('payments_request_sign_step', 'payments_request_sign_status');
    }
}
