<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201120_113040_alter_invoice_alter_column extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('{{%invoice}}', 'contractor_bik', $this->string(45));
    }

    public function safeDown()
    {
        $this->alterColumn('{{%invoice}}', 'contractor_bik', $this->string(9));
    }
}
