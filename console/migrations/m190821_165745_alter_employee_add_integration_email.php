<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190821_165745_alter_employee_add_integration_email extends Migration
{
    public function up()
    {
        $this->addColumn('{{%employee}}', 'integration_email', $this->string(2000)->comment('Настройки интеграции почты'));
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%employee}}', 'integration_email');
    }
}


