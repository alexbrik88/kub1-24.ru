<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170216_142758_create_cashOrderFlowToInvoice extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{%cash_order_flow_to_invoice}}', [
            'flow_id' => $this->integer()->notNull(),
            'invoice_id' => $this->integer()->notNull(),
            'amount' => $this->bigInteger(20)->notNull(),
        ], $this->tableOptions);

        $this->addPrimaryKey('PRIMARY_KEY', '{{%cash_order_flow_to_invoice}}', ['flow_id', 'invoice_id']);
        $this->addForeignKey('fk_cash_order_flow_to_invoice_flow', '{{%cash_order_flow_to_invoice}}', 'flow_id', '{{%cash_order_flows}}', 'id', 'CASCADE');
        $this->addForeignKey('fk_cash_order_flow_to_invoice_invoice', '{{%cash_order_flow_to_invoice}}', 'invoice_id', '{{%invoice}}', 'id', 'CASCADE');

        /*$this->execute("
            INSERT INTO {{%cash_order_flow_to_invoice}} (flow_id, invoice_id, amount)
            SELECT flow.id, inv.id, LEAST(inv.total_amount_with_nds, flow.amount)
            FROM {{%cash_order_flows}} flow INNER JOIN {{%invoice}} inv ON flow.invoice_id = inv.id
        ");
        $flowArray = */
    }

    public function safeDown()
    {
        $this->dropTable('{{%cash_order_flow_to_invoice}}');
    }
}
