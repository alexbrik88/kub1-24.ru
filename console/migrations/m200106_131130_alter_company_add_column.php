<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200106_131130_alter_company_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%company}}', 'login_redirect_times', $this->smallInteger()->defaultValue(0));
        $this->addColumn('{{%company}}', 'login_redirect_url', $this->text());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%company}}', 'login_redirect_url');
        $this->dropColumn('{{%company}}', 'login_redirect_times');
    }
}
