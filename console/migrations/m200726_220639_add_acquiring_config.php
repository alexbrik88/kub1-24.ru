<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200726_220639_add_acquiring_config extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%user_config}}', 'table_view_acquiring', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
    }

    public function safeDown()
    {
        $this->dropColumn('{{%user_config}}', 'table_view_acquiring');
    }
}
