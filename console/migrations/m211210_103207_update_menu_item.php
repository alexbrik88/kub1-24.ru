<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211210_103207_update_menu_item extends Migration
{
    public function safeUp()
    {
        $this->execute('
            UPDATE {{%menu_item}}
            LEFT JOIN {{%company}} ON {{%company}}.[[id]] = {{%menu_item}}.[[company_id]]
            LEFT JOIN {{%company_taxation_type}} ON {{%company_taxation_type}}.[[company_id]] = {{%company}}.[[id]]
            SET {{%menu_item}}.[[accountant_item]] = 1
            WHERE {{%menu_item}}.[[accountant_item]] = 0
              AND {{%company}}.[[company_type_id]] = 1
              AND {{%company_taxation_type}}.[[usn]] = 1
              AND {{%company_taxation_type}}.[[usn_type]] = 0
              AND {{%company_taxation_type}}.[[usn_percent]] = 6
        ');
    }

    public function safeDown()
    {
        // do nothing
    }
}
