<?php

use yii\db\Migration;

class m200827_194640_alter_crm_client_status_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%crm_client_status}}';

    /**
     * @inheritDoc
     */
    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'value', $this->tinyInteger(1)->null());
        $this->createIndex('ck_value', self::TABLE_NAME, ['value']);
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'value');
    }
}
