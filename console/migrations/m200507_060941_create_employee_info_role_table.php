<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%employee_info_role}}`.
 */
class m200507_060941_create_employee_info_role_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%employee_info_role}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);

        $this->batchInsert('{{%employee_info_role}}', [
            'id',
            'name',
        ], [
            [1, 'Руководитель компании'],
            [2, 'Собственник компании'],
            [3, 'Финансовый директор'],
            [4, 'Финансовый консультант (аутсорсинг)'],
            [5, 'Бухгалтер'],
            [6, 'Коммерческий директор'],
            [7, 'Маркетолог'],
            [8, 'Другое'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%employee_info_role}}');
    }
}
