<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%ofd_import_config}}`.
 */
class m211214_113325_create_ofd_import_config_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%ofd_import_config}}', [
            'company_id' => $this->integer()->notNull(),
            'import_config_id' => $this->integer()->notNull(),
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', '{{%ofd_import_config}}', 'company_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%ofd_import_config}}');
    }
}
