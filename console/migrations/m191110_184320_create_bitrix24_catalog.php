<?php

use console\components\db\Migration;

class m191110_184320_create_bitrix24_catalog extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{bitrix24_catalog}}', [
            'id' => $this->integer()->unsigned()->notNull()->comment('Идентификатор каталога Битрикс24'),
            'employee_id' => $this->integer()->notNull()->comment('ID клиента КУБ'),
            'name' => $this->string(120)->notNull()->comment('Название каталога'),
        ], "COMMENT 'Интеграция Битрикс24: товарные каталоги'");
        $this->addForeignKey(
            'bitrix24_catalog_employee_id', '{{bitrix24_catalog}}', 'employee_id',
            '{{employee}}', 'id', 'CASCADE', 'CASCADE'
        );
        $this->addPrimaryKey('bitrix24_catalog_id', '{{bitrix24_catalog}}', ['employee_id', 'id']);
    }

    public function down()
    {
        $this->dropTable('{{bitrix24_catalog}}');

    }
}
