<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%company_last_visit}}`.
 */
class m200729_142832_create_company_last_visit_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%company_last_visit}}', [
            'company_id' => $this->integer()->notNull(),
            'employee_id' => $this->integer()->notNull(),
            'ip' => $this->string(45)->notNull(),
            'time' => $this->integer()->notNull(),
            'PRIMARY KEY ([[company_id]], [[employee_id]], [[ip]])',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%company_last_visit}}');
    }
}
