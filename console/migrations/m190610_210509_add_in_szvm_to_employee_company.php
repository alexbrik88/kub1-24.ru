<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190610_210509_add_in_szvm_to_employee_company extends Migration
{
    public $tableName = 'employee_company';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'in_szvm', $this->boolean()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'in_szvm');
    }
}
