<?php

use console\components\db\Migration;
use yii\db\Schema;

class m171124_221647_insert_discount_type extends Migration
{
    public function safeUp()
    {
        $this->insert('discount_type', [
            'id' => 1,
            'description' => '"СКИДКА 20% при оплате КУБ сегодня". Увеличение на 10% при ответе на вопросы. ' .
                            'Для компаний на триале, которые выставили более одного счета в течение 10 дней ' .
                            'после регистрации и не оплатили подписку.',
        ]);
    }

    public function safeDown()
    {
        $this->delete('discount_type', ['id' => 1]);
    }
}
