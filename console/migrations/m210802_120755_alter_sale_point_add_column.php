<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210802_120755_alter_sale_point_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('sale_point', 'responsible_employee_id', $this->integer()->notNull()->after('company_id'));

        $this->execute("
            UPDATE `sale_point` s
            LEFT JOIN `company` c ON s.company_id = c.id
            SET s.responsible_employee_id = c.owner_employee_id
        ");

        $this->addForeignKey('FK_sale_point_to_employee', 'sale_point', 'responsible_employee_id', 'employee', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_sale_point_to_employee', 'sale_point');
        $this->dropColumn('sale_point', 'responsible_employee_id');
    }
}
