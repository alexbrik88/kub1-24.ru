<?php

use console\components\db\Migration;
use yii\db\Schema;
use common\models\service\Payment;
use common\models\service\SubscribeTariff;
use common\models\report\ReportState;
use common\models\document\Invoice;
use frontend\models\Documents;
use common\models\document\Act;
use common\models\document\PackingList;
use common\models\document\InvoiceFacture;
use common\models\document\Upd;
use common\models\product\Product;
use common\models\cash\CashBankStatementUpload;
use common\models\Company;
use frontend\modules\export\models\export\Export;
use common\models\employee\Employee;

class m170809_113307_update_report_state extends Migration
{
    public $tableName = 'report_state';

    public function safeUp()
    {
        $this->truncateTable($this->tableName);

        $firstPayments = Payment::find()->andWhere(['and',
            ['not', ['payment_date' => null]],
            ['is_confirmed' => 1],
            ['in', 'tariff_id', [SubscribeTariff::TARIFF_1, SubscribeTariff::TARIFF_2, SubscribeTariff::TARIFF_3]],
        ])
            ->orderBy(['payment_date' => SORT_DESC])
            ->groupBy('company_id')
            ->all();

        /* @var $firstPayment Payment */
        foreach ($firstPayments as $firstPayment) {
            $model = new ReportState();
            $model->company_id = $firstPayment->company_id;
            $model->subscribe_id = $firstPayment->subscribe->id;
            $model->pay_date = $firstPayment->payment_date;
            $model->pay_sum = $firstPayment->sum;
            $model->invoice_count = Invoice::find()
                ->byIOType(Documents::IO_TYPE_OUT)
                ->byDeleted()
                ->andWhere(['<=', Invoice::tableName() . '.created_at', $model->pay_date])
                ->byCompany($model->company_id)
                ->count();
            $model->act_count = Act::find()
                ->byIOType(Documents::IO_TYPE_OUT)
                ->andWhere(['<=', Act::tableName() . '.created_at', $model->pay_date])
                ->byCompany($model->company_id)
                ->count();
            $model->packing_list_count = PackingList::find()
                ->byIOType(Documents::IO_TYPE_OUT)
                ->andWhere(['<=', PackingList::tableName() . '.created_at', $model->pay_date])
                ->byCompany($model->company_id)
                ->count();
            $model->invoice_facture_count = InvoiceFacture::find()
                ->byIOType(Documents::IO_TYPE_OUT)
                ->andWhere(['<=', InvoiceFacture::tableName() . '.created_at', $model->pay_date])
                ->byCompany($model->company_id)
                ->count();
            $model->upd_count = Upd::find()
                ->byIOType(Documents::IO_TYPE_OUT)
                ->andWhere(['<=', Upd::tableName() . '.created_at', $model->pay_date])
                ->byCompany($model->company_id)
                ->count();
            $model->has_logo = (bool) $firstPayment->company->logo_link;
            $model->has_print = (bool) $firstPayment->company->print_link;
            $model->has_signature = (bool) $firstPayment->company->chief_signature_link;
            $model->product_count = $firstPayment->company->getProducts()
                ->andWhere(['production_type' => Product::PRODUCTION_TYPE_GOODS])
                ->andWhere(['<=', Product::tableName() . '.created_at', $model->pay_date])
                ->count();
            $model->service_count = $firstPayment->company->getProducts()
                ->andWhere(['production_type' => Product::PRODUCTION_TYPE_SERVICE])
                ->andWhere(['<=', Product::tableName() . '.created_at', $model->pay_date])
                ->count();
            $model->download_statement_count = CashBankStatementUpload::find()
                ->joinWith('company')
                ->andWhere(['<=', CashBankStatementUpload::tableName() . '.created_at', $model->pay_date])
                ->andWhere([
                    Company::tableName() . '.id' => $model->company_id,
                ])->count();
            $model->download_1c_count = Export::find()
                ->joinWith('employee')
                ->leftJoin('company', 'company.id = ' . Employee::tableName() . '.company_id')
                ->andWhere(['<=', Export::tableName() . '.created_at', $model->pay_date])
                ->andWhere([Company::tableName() . '.id' => $model->company_id])
                ->count();
            $model->save();
        }
    }

    public function safeDown()
    {
        $this->truncateTable($this->tableName);
    }
}


