<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211012_151852_alter_crm_task_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%crm_task}}', 'task_result_id', $this->bigInteger()->after('employee_id'));

        $this->createIndex('task_result_id', '{{%crm_task}}', 'task_result_id');
    }

    public function safeDown()
    {
        $this->dropIndex('task_result_id', '{{%crm_task}}');

        $this->dropColumn('{{%crm_task}}', 'task_result_id');
    }
}
