<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170818_121946_alter_company_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%company}}', 'subscribe_payment_key', $this->string());
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%company}}', 'subscribe_payment_key');
    }
}
