<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210621_192602_alter_invoice_drop_index extends Migration
{
    public $table = 'invoice';

    public function safeUp()
    {
        $this->dropIndex('from_demo_out_invoice', $this->table);
    }

    public function safeDown()
    {
        $this->createIndex('from_demo_out_invoice', $this->table, ['company_id', 'from_demo_out_invoice']);
    }
}
