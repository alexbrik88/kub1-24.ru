<?php

use console\components\db\Migration;
use yii\db\Schema;

class m151222_150739_alter_employee_update_created_at extends Migration
{
    public $tEmployee = 'employee';

    public function safeUp()
    {
        $this->addColumn($this->tEmployee, 'created', $this->integer(11)->defaultValue(null));
        $this->update($this->tEmployee, [
            'created' => new \yii\db\Expression('UNIX_TIMESTAMP (created_at)'),
        ]);
        $this->dropColumn($this->tEmployee, 'created_at');
        $this->renameColumn($this->tEmployee, 'created', 'created_at');
    }
    
    public function safeDown()
    {

    }
}


