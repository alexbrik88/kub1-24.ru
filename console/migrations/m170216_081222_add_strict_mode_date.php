<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170216_081222_add_strict_mode_date extends Migration
{
    public $tableName = 'company';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'strict_mode_date', $this->integer()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'strict_mode_date');
    }
}


