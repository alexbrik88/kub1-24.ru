<?php

use console\components\db\Migration;
use yii\db\Schema;
use frontend\modules\analytics\models\financeModel\helpers\FinanceModelCalcHelper as Calc;
use frontend\modules\analytics\models\financeModel\FinanceModel;

class m210601_103227_alter_finance_model_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('finance_model', 'total_margin', $this->bigInteger()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn('finance_model', 'total_margin');
    }
}
