<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200601_110603_alter_product_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('product', 'count_in_package', $this->string(45)->after('box_type')->comment('Количество в упаковке'));
        $this->execute('UPDATE product SET count_in_package = place_count');
    }

    public function safeDown()
    {
        $this->dropColumn('product', 'count_in_package');
    }
}
