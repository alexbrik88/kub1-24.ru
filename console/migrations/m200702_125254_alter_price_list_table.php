<?php

use console\components\db\Migration;

class m200702_125254_alter_price_list_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%price_list}}';

    /**
     * @inheritDoc
     */
    public function up()
    {
        $this->addColumn(self::TABLE_NAME, 'notify_to_telegram', $this->boolean()->notNull()->defaultValue(false));
        $this->createIndex('ck_notify_to_telegram', self::TABLE_NAME, ['notify_to_telegram']);
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        $this->dropColumn(self::TABLE_NAME, 'notify_to_telegram');
    }
}
