<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191006_115218_alter_product_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('product', 'archived_at', $this->integer()->after('created_at'));
    }

    public function safeDown()
    {
        $this->dropColumn('product', 'archived_at');
    }
}
