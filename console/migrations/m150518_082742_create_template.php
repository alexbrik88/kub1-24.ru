<?php

use yii\db\Schema;
use yii\db\Migration;

class m150518_082742_create_template extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('template', [
            'id' => Schema::TYPE_PK,
            'company_id' => Schema::TYPE_INTEGER,
            'sequence' => Schema::TYPE_INTEGER,
            'title' => Schema::TYPE_STRING,
        ], $tableOptions);

        $this->addForeignKey('template_to_company', 'template', 'company_id', 'company', 'id');

        $this->createTable('template_file', [
            'id' => Schema::TYPE_PK,
            'file_name' => Schema::TYPE_STRING,
            'template_id' => Schema::TYPE_INTEGER,
        ], $tableOptions);

        $this->addForeignKey('template_file_to_template', 'template_file', 'template_id', 'template', 'id');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('template_to_company', 'template');
        $this->dropTable('template');

        $this->dropForeignKey('template_file_to_template', 'template_file');
        $this->dropTable('template_file');
    }
}
