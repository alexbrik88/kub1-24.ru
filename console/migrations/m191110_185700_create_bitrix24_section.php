<?php

use console\components\db\Migration;

class m191110_185700_create_bitrix24_section extends Migration
{
    public function safeUp()
    {
        $this->createTable('{{bitrix24_section}}', [
            'id' => $this->integer()->unsigned()->notNull()->comment('Идентификатор группы Битрикс24'),
            'employee_id' => $this->integer()->notNull()->comment('ID клиента КУБ'),
            'catalog_id' => $this->integer()->unsigned()->notNull()->comment('Идентификатор товарного каталога'),
            'parent_id' => $this->integer()->unsigned()->comment('Идентификатор родительской группы'),
            'name' => $this->string(120)->notNull()->comment('Название группы'),
        ], "COMMENT 'Интеграция Битрикс24: группы (категории) товаров'");
        $this->addPrimaryKey('bitrix24_section_id', '{{bitrix24_section}}', ['employee_id', 'id']);
        $this->addForeignKey(
            'bitrix24_section_catalog_id', '{{bitrix24_section}}', ['employee_id', 'catalog_id'],
            '{{bitrix24_catalog}}', ['employee_id', 'id'], 'CASCADE', 'CASCADE'
        );
        $this->createIndex('bitrix24_section_parent_id', '{{bitrix24_section}}', ['employee_id', 'parent_id']);
    }

    public function down()
    {
        $this->dropTable('{{bitrix24_section}}');

    }
}
