<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180727_133806_update_invoice extends Migration
{
    public function safeUp()
    {
        $this->execute("
            UPDATE {{invoice}}
            SET
                {{invoice}}.[[can_add_act]] = (
                    SELECT IFNULL(SUM({{order}}.[[quantity]]), 0) FROM {{order}}
                    INNER JOIN {{product}} ON {{order}}.[[product_id]] = {{product}}.[[id]]
                    WHERE {{order}}.[[invoice_id]] = {{invoice}}.[[id]] AND {{product}}.[[production_type]] = 0
                ) > (
                    SELECT IFNULL(SUM({{order_act}}.[[quantity]]), 0) FROM {{order_act}}
                    INNER JOIN {{act}} ON {{order_act}}.[[act_id]] = {{act}}.[[id]]
                    WHERE {{act}}.[[invoice_id]] = {{invoice}}.[[id]]
                ),
                {{invoice}}.[[can_add_packing_list]] = (
                    SELECT IFNULL(SUM({{order}}.[[quantity]]), 0) FROM {{order}}
                    INNER JOIN {{product}} ON {{order}}.[[product_id]] = {{product}}.[[id]]
                    WHERE {{order}}.[[invoice_id]] = {{invoice}}.[[id]] AND {{product}}.[[production_type]] = 1
                ) > (
                    SELECT IFNULL(SUM({{order_packing_list}}.[[quantity]]), 0) FROM {{order_packing_list}}
                    INNER JOIN {{packing_list}} ON {{order_packing_list}}.[[packing_list_id]] = {{packing_list}}.[[id]]
                    WHERE {{packing_list}}.[[invoice_id]] = {{invoice}}.[[id]]
                ),
                {{invoice}}.[[can_add_invoice_facture]] = (
                    SELECT IFNULL(SUM({{order}}.[[quantity]]), 0) FROM {{order}}
                    WHERE {{order}}.[[invoice_id]] = {{invoice}}.[[id]]
                ) > (
                    SELECT IFNULL(SUM({{order_invoice_facture}}.[[quantity]]), 0) FROM {{order_invoice_facture}}
                    INNER JOIN {{invoice_facture}} ON {{order_invoice_facture}}.[[invoice_facture_id]] = {{invoice_facture}}.[[id]]
                    WHERE {{invoice_facture}}.[[invoice_id]] = {{invoice}}.[[id]]
                ),
                {{invoice}}.[[can_add_upd]] = (
                    SELECT IFNULL(SUM({{order}}.[[quantity]]), 0) FROM {{order}}
                    WHERE {{order}}.[[invoice_id]] = {{invoice}}.[[id]]
                ) > (
                    SELECT IFNULL(SUM({{order_upd}}.[[quantity]]), 0) FROM {{order_upd}}
                    INNER JOIN {{upd}} ON {{order_upd}}.[[upd_id]] = {{upd}}.[[id]]
                    WHERE {{upd}}.[[invoice_id]] = {{invoice}}.[[id]]
                );
        ");
    }

    public function safeDown()
    {
    }
}
