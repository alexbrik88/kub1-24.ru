<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190417_164245_create_profit_and_loss_company_item extends Migration
{
    public $tableName = 'profit_and_loss_company_item';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'company_id' => $this->integer()->notNull(),
            'item_id' => $this->integer()->notNull(),
            'expense_type' => $this->integer()->notNull()->comment('1 - Переменные, 2 - Постоянные, 3 - Операционные, 4 - Другие'),
            'can_update' => $this->boolean()->defaultValue(false),
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', $this->tableName, ['company_id', 'item_id']);
        $this->addForeignKey('profit_and_loss_company_item_company_id', $this->tableName, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('profit_and_loss_company_item_item_id', $this->tableName, 'item_id', 'invoice_expenditure_item', 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable($this->tableName);
    }
}
