<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180827_174338_add_act_id_to_email_file extends Migration
{
    public $tableName = 'email_file';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'act_id', $this->integer()->defaultValue(null));
        $this->addForeignKey($this->tableName . '_act_id', $this->tableName, 'act_id', 'act', 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_act_id', $this->tableName);
        $this->dropColumn($this->tableName, 'act_id');
    }
}


