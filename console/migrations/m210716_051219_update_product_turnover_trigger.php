<?php

use console\components\db\Migration;
use yii\db\Schema;

/**
 * Update "product_turnover.group_id" by trigger "product_turnover__product__after_update"
 */
class m210716_051219_update_product_turnover_trigger extends Migration
{
    public $dropSql = 'DROP TRIGGER IF EXISTS `product_turnover__product__after_update`;';

    public $revertSql = '
CREATE TRIGGER `product_turnover__product__after_update` AFTER UPDATE ON `product`
FOR EACH ROW
BEGIN
    IF
        IFNULL(NEW.`price_for_buy_with_nds`, 0) > 0
        AND
        IFNULL(OLD.`price_for_buy_with_nds`, 0) <> IFNULL(NEW.`price_for_buy_with_nds`, 0)
    THEN
        UPDATE `product_turnover`
        SET `purchase_price` = NEW.`price_for_buy_with_nds`,
            `purchase_amount` = (@purchase:=ROUND(`quantity` * NEW.`price_for_buy_with_nds`)),
            `margin` = `total_amount` - @purchase
        WHERE `product_id` = NEW.`id`
        AND `type` = 2
        AND `purchase_price` = 0;
    END IF;

    IF
        OLD.`not_for_sale` <> NEW.`not_for_sale`
    THEN
        UPDATE `product_turnover`
        SET `not_for_sale` = NEW.`not_for_sale`
        WHERE `product_id` = NEW.`id`;
    END IF;
END;
        ';

    public function safeUp()
    {
        $this->execute($this->dropSql);

        $this->execute('
CREATE TRIGGER `product_turnover__product__after_update` AFTER UPDATE ON `product`
FOR EACH ROW
BEGIN
    IF
        IFNULL(NEW.`price_for_buy_with_nds`, 0) > 0
        AND
        IFNULL(OLD.`price_for_buy_with_nds`, 0) <> IFNULL(NEW.`price_for_buy_with_nds`, 0)
    THEN
        UPDATE `product_turnover`
        SET `purchase_price` = NEW.`price_for_buy_with_nds`,
            `purchase_amount` = (@purchase:=ROUND(`quantity` * NEW.`price_for_buy_with_nds`)),
            `margin` = `total_amount` - @purchase
        WHERE `product_id` = NEW.`id`
        AND `type` = 2
        AND `purchase_price` = 0;
    END IF;

    IF
        OLD.`not_for_sale` <> NEW.`not_for_sale`
    THEN
        UPDATE `product_turnover`
        SET `not_for_sale` = NEW.`not_for_sale`
        WHERE `product_id` = NEW.`id`;
    END IF;
    
    IF
        OLD.`group_id` <> NEW.`group_id`
    THEN
        UPDATE `product_turnover`
        SET `product_group_id` = NEW.`group_id`
        WHERE `product_id` = NEW.`id`;
    END IF;
    
END;
        ');
    }

    public function safeDown()
    {
        $this->execute($this->dropSql);
        $this->execute($this->revertSql);
    }
}
