<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210612_140233_alter_act_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('act', 'contractor_id', $this->integer()->after('type'));
        $this->addColumn('act', 'company_id', $this->integer()->after('type'));

        $this->execute("
            UPDATE `act` doc
            LEFT JOIN `invoice_act` rel ON rel.act_id = doc.id
            LEFT JOIN `invoice` ON invoice.id = rel.invoice_id
            SET doc.company_id = invoice.company_id, doc.contractor_id = invoice.contractor_id
        ");

        $this->createIndex('IDX_act_to_company', 'act', 'company_id');
        $this->createIndex('IDX_act_to_contractor', 'act', 'contractor_id');
    }

    public function safeDown()
    {
        $this->dropColumn('act', 'contractor_id');
        $this->dropColumn('act', 'company_id');
    }
}
