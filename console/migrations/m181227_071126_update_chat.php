<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181227_071126_update_chat extends Migration
{
    public function safeUp()
    {
        $this->execute("
            UPDATE {{chat}}
            SET {{chat}}.[[message]] = SUBSTRING_INDEX(TRIM(TRAILING '</a>' FROM {{chat}}.[[message]]), '>', -1)
            WHERE {{chat}}.[[is_file]] = 1
        ");
    }

    public function safeDown()
    {
        $this->execute("
            UPDATE {{chat}}
            SET {{chat}}.[[message]] = CONCAT('<a href=\"/chat/download-file?id=', {{chat}}.[[id]], '\">', {{chat}}.[[message]], '</a>')
            WHERE {{chat}}.[[is_file]] = 1
        ");
    }
}
