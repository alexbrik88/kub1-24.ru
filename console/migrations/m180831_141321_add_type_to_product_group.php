<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180831_141321_add_type_to_product_group extends Migration
{
    public $tableName = 'product_group';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'production_type', $this->integer()->defaultValue(null));
        $this->update($this->tableName, ['production_type' => \common\models\product\Product::PRODUCTION_TYPE_GOODS], ['not', ['id' => 1]]);
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'production_type');
    }
}


