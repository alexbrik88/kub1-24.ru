<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180418_120828_alter_invoice_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('invoice', 'show_article', $this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('invoice', 'show_article');
    }
}
