<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170307_094226_alter_bank_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%bank}}', 'is_special_offer', $this->boolean()->notNull()->defaultValue(false));
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%bank}}', 'is_special_offer');
    }
}
