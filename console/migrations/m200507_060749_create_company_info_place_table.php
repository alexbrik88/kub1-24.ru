<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%company_info_place}}`.
 */
class m200507_060749_create_company_info_place_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%company_info_place}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);

        $this->batchInsert('{{%company_info_place}}', [
            'id',
            'name',
        ], [
            [1, 'Россия'],
            [2, 'Белоруссия'],
            [3, 'Казахстан'],
            [4, 'Deutschland'],
            [5, 'Italia'],
            [6, 'United Kingdom'],
            [7, 'USA'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%company_info_place}}');
    }
}
