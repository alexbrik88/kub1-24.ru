<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180507_064227_alter_scan_document_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('scan_document', 'owner_model', $this->string()->defaultValue(null)->after('employee_id'));
        $this->addColumn('scan_document', 'owner_id', $this->integer()->defaultValue(null)->after('owner_model'));
    }

    public function safeDown()
    {
        $this->dropColumn('scan_document', 'owner_model');
        $this->dropColumn('scan_document', 'owner_id');
    }
}
