<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201014_130746_alter_menu_item_append_business_analytic_item extends Migration
{
    public $tableName = 'menu_item';

    public function safeUp()
    {
        $this->addColumn($this->tableName,'business_analytics_item', $this->boolean()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'business_analytics_item');
    }
}
