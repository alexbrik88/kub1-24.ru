<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%jobs}}`.
 */
class m200722_113003_add_error_column_to_jobs_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('jobs', 'error', $this->text()->after('result'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('jobs', 'error');
    }
}
