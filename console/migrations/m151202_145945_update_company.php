<?php

use yii\db\Schema;
use yii\db\Migration;

class m151202_145945_update_company extends Migration
{
    public $tCompany = 'company';

    public function safeUp()
    {
        $this->addColumn($this->tCompany, 'last_visit_at', $this->integer(11)->notNull());
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tCompany, 'last_visit_at');
    }
}
