<?php

use console\components\db\Migration;
use yii\db\Schema;

class m220119_101516_alter_user_config_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_config', 'contractor_group_name', $this->boolean(1)->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('user_config', 'contractor_group_name');
    }
}
