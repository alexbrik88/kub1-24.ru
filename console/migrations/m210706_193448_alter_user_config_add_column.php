<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210706_193448_alter_user_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_config','report_operational_efficiency_chart',$this->boolean()->notNull()->defaultValue(false));
        $this->addColumn('user_config','report_operational_efficiency_help',$this->boolean()->notNull()->defaultValue(false));
        $this->addColumn('user_config', 'table_view_operational_efficiency', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn('user_config', 'report_operational_efficiency_row_profit_and_loss', $this->tinyInteger(1)->notNull()->defaultValue(1));
        $this->addColumn('user_config', 'report_operational_efficiency_row_msfo_profit', $this->tinyInteger(1)->notNull()->defaultValue(1));
        $this->addColumn('user_config', 'report_operational_efficiency_row_revenue_profitability', $this->tinyInteger(1)->notNull()->defaultValue(1));
        $this->addColumn('user_config', 'report_operational_efficiency_row_return_on_investment', $this->tinyInteger(1)->notNull()->defaultValue(1));

    }

    public function safeDown()
    {
        $this->dropColumn('user_config','report_operational_efficiency_chart');
        $this->dropColumn('user_config','report_operational_efficiency_help');
        $this->dropColumn('user_config','table_view_operational_efficiency');
        $this->dropColumn('user_config','report_operational_efficiency_row_profit_and_loss');
        $this->dropColumn('user_config','report_operational_efficiency_row_msfo_profit');
        $this->dropColumn('user_config','report_operational_efficiency_row_revenue_profitability');
        $this->dropColumn('user_config','report_operational_efficiency_row_return_on_investment');
    }
}
