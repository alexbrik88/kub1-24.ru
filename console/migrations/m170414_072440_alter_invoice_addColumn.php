<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170414_072440_alter_invoice_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%invoice}}', 'contractor_director_name', $this->string(255)->after('contractor_name_short'));

        \Yii::$app->db->createCommand("
            UPDATE {{%invoice}} LEFT JOIN {{%contractor}} ON {{%invoice}}.[[contractor_id]] = {{%contractor}}.[[id]]
            SET {{%invoice}}.[[contractor_director_name]] = {{%contractor}}.[[director_name]]
        ")->execute();
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%invoice}}', 'contractor_director_name');
    }
}
