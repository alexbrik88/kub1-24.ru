<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210119_135906_insert_sent_email_type extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%sent_email_type}}', [
            'id' => 2,
            'type' => 'FINDIRECTOR',
            'description' => 'Рассылка для пользователей модуля ФинДиректор',
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%sent_email_type}}', [
            'id' => 2,
        ]);
    }
}
