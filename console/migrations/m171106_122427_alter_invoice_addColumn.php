<?php

use console\components\db\Migration;
use frontend\models\Documents;
use yii\db\Schema;

class m171106_122427_alter_invoice_addColumn extends Migration
{
    public $tableName = 'invoice';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'has_nds', $this->boolean()->defaultValue(false)->after('company_checking_accountant_id'));

        $tCompany = 'company';
        $tTaxation = 'company_taxation_type';
        $type = Documents::IO_TYPE_OUT;
        $this->execute("
            UPDATE {{{$this->tableName}}}
			LEFT JOIN {{{$tCompany}}} ON {{{$tCompany}}}.[[id]] = {{{$this->tableName}}}.[[company_id]]
			LEFT JOIN {{{$tTaxation}}} ON {{{$tTaxation}}}.[[company_id]] = {{{$tCompany}}}.[[id]]
			SET {{{$this->tableName}}}.[[has_nds]] = IF({{{$tTaxation}}}.[[osno]] = 1, 1, 0)
			WHERE {{{$this->tableName}}}.[[type]] = {$type}
        ;");
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'has_nds');
    }
}
