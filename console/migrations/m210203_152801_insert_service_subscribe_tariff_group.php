<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210203_152801_insert_service_subscribe_tariff_group extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%service_subscribe_tariff_group}}', [
            'id' => 19,
            'name' => 'Выставление Инвойсов',
            'is_base' => 0,
            'has_base' => 0,
            'is_active' => 1,
            'description' => '',
            'hint' => null,
            'priority' => 3,
            'icon' => 'template',
        ]);
        $this->update('{{%service_subscribe_tariff_group}}', [
            'priority' => 6,
        ], [
            'id' => 7,
        ]);
        $this->update('{{%service_subscribe_tariff_group}}', [
            'priority' => 9,
        ], [
            'id' => 4,
        ]);
        $this->update('{{%service_subscribe_tariff_group}}', [
            'priority' => 12,
        ], [
            'id' => 10,
        ]);
        $this->update('{{%service_subscribe_tariff_group}}', [
            'priority' => 15,
        ], [
            'id' => 9,
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%service_subscribe_tariff_group}}', [
            'id' => 19,
        ]);
        $this->update('{{%service_subscribe_tariff_group}}', [
            'priority' => 3,
        ], [
            'id' => 7,
        ]);
        $this->update('{{%service_subscribe_tariff_group}}', [
            'priority' => 6,
        ], [
            'id' => 4,
        ]);
        $this->update('{{%service_subscribe_tariff_group}}', [
            'priority' => 9,
        ], [
            'id' => 10,
        ]);
        $this->update('{{%service_subscribe_tariff_group}}', [
            'priority' => 12,
        ], [
            'id' => 9,
        ]);

    }
}
