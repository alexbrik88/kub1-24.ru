<?php

use yii\db\Schema;
use yii\db\Migration;

class m150425_101713_alter_Invoice extends Migration
{
    public function safeUp()
    {
        $this->dropForeignKey('act_to_invoice', 'act');
        $this->dropForeignKey('invoice_facture_to_invoice', 'invoice_facture');
        $this->dropForeignKey('packing_list_to_invoice', 'packing_list');
        $this->dropForeignKey('order_to_invoice', 'order');

        $this->truncateTable('act');
        $this->truncateTable('packing_list');
        $this->truncateTable('invoice_facture');
        $this->truncateTable('order');
        $this->truncateTable('invoice');

        $this->dropTable('invoice');
        $this->createTable('invoice', [
            'id' => Schema::TYPE_PK,
            'type' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "1 - in, 2 - out"',
            'invoice_status_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'invoice_status_updated_at' => Schema::TYPE_DATETIME . ' NOT NULL',
            'invoice_status_author_id' => Schema::TYPE_INTEGER . ' NOT NULL',

            'created_at' => Schema::TYPE_DATETIME . ' NOT NULL',
            'document_author_id' => Schema::TYPE_INTEGER . ' NOT NULL',

            'document_date' => Schema::TYPE_DATE . ' NULL',
            'document_number' => Schema::TYPE_INTEGER . ' NOT NULL',
            'document_additional_number' => Schema::TYPE_STRING . '(45) NULL',
            'payment_limit_date' => Schema::TYPE_DATE . ' NOT NULL',

            'production_type' => Schema::TYPE_BOOLEAN . ' NOT NULL COMMENT "1 - good, 0 - service"',

            'invoice_expenditure_item_id' => Schema::TYPE_INTEGER . ' NULL COMMENT "статья расходов (in)"',

            'company_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'company_name_full' => Schema::TYPE_STRING . '(255) NOT NULL',
            'company_name_short' => Schema::TYPE_STRING . '(255) NOT NULL',
            'company_bank_name' => Schema::TYPE_STRING . '(255) NULL',
            'company_inn' => Schema::TYPE_STRING . '(45) NULL',
            'company_kpp' => Schema::TYPE_STRING . '(45) NULL',
            'company_egrip' => Schema::TYPE_STRING . '(45) NULL',
            'company_okpo' => Schema::TYPE_STRING . '(10) NULL',
            'company_bik' => 'CHAR(9) NULL',
            'company_ks' => 'CHAR(20) NULL',
            'company_rs' => 'CHAR(20) NULL',
            'company_phone' => Schema::TYPE_STRING . '(20) NULL',
            'company_address_legal_full' => Schema::TYPE_STRING . '(255) NOT NULL',
            'company_chief_post_name' => Schema::TYPE_STRING . '(45) NOT NULL',
            'company_chief_lastname' => Schema::TYPE_STRING . '(45) NOT NULL',
            'company_chief_firstname_initials' => Schema::TYPE_STRING . '(10) NOT NULL',
            'company_chief_patronymic_initials' => Schema::TYPE_STRING . '(10) NOT NULL',
            'company_print_filename' => Schema::TYPE_STRING . '(255) NULL',
            'company_chief_signature_filename' => Schema::TYPE_STRING . '(255) NULL',
            'company_chief_accountant_lastname' => Schema::TYPE_STRING . '(10) NOT NULL',
            'company_chief_accountant_firstname_initials' => Schema::TYPE_STRING . '(10) NOT NULL',
            'company_chief_accountant_patronymic_initials' => Schema::TYPE_STRING . '(10) NOT NULL',

            'contractor_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'contractor_name_full' => Schema::TYPE_STRING . '(45) NOT NULL',
            'contractor_name_short' => Schema::TYPE_STRING . '(45) NOT NULL',
            'contractor_bank_name' => Schema::TYPE_STRING . '(45) NULL',
            'contractor_address_legal_full' => Schema::TYPE_STRING . '(255) NOT NULL',
            'contractor_inn' => Schema::TYPE_STRING . '(45) NULL',
            'contractor_kpp' => Schema::TYPE_STRING . '(45) NULL',
            'contractor_bik' => 'CHAR(9) NULL',
            'contractor_ks' => 'CHAR(20) NULL',
            'contractor_rs' => 'CHAR(20) NULL',

            'total_place_count' => Schema::TYPE_STRING . ' NULL',
            'total_mass_gross' => Schema::TYPE_STRING . ' NULL',
            'total_amount_no_nds' => Schema::TYPE_INTEGER . ' NULL',

            'total_amount_with_nds' => Schema::TYPE_INTEGER . ' NOT NULL',
            'total_amount_has_nds' => Schema::TYPE_BOOLEAN . ' NOT NULL',
            'total_amount_nds' => Schema::TYPE_INTEGER . ' NOT NULL',
            'total_order_count' => Schema::TYPE_INTEGER . ' NOT NULL',

            'payment_form_id' => Schema::TYPE_INTEGER . ' NULL',
            'payment_partial_amount' => Schema::TYPE_INTEGER . ' NULL',

            'document_filename' => Schema::TYPE_STRING . '(255) NULL COMMENT "файл документа (in)"',
        ]);

        $this->addForeignKey('fk_ivoice_to_document_author', 'invoice', 'document_author_id', 'employee', 'id');
        $this->addForeignKey('fk_ivoice_to_status_author', 'invoice', 'invoice_status_author_id', 'employee', 'id');

        $this->addForeignKey('fk_ivoice_to_company', 'invoice', 'company_id', 'company', 'id');
        $this->addForeignKey('fk_ivoice_to_contractor', 'invoice', 'contractor_id', 'contractor', 'id');

        $this->addForeignKey('fk_act_to_invoice', 'act', 'invoice_id', 'invoice', 'id');
        $this->addForeignKey('fk_packing_list_to_invoice', 'packing_list', 'invoice_id', 'invoice', 'id');
        $this->addForeignKey('fk_invoice_facture_to_invoice', 'invoice_facture', 'invoice_id', 'invoice', 'id');
        $this->addForeignKey('fk_order_to_invoice', 'order', 'invoice_id', 'invoice', 'id');
        $this->addForeignKey('fk_invoice_to_invoice_expenditure_item', 'invoice', 'invoice_expenditure_item_id', 'invoice_expenditure_item', 'id');


        $this->dropTable('payment_variant');
        $this->createTable('payment_form', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . '(45) NOT NULL',
        ]);
        $this->batchInsert('payment_form', ['id', 'name'], [
            [1, 'Через банк'],
            [2, 'Через кассу'],
            [3, 'Через e-money'],
        ]);

        $this->addForeignKey('payment_type_to_invoice', 'invoice', 'payment_form_id', 'payment_form', 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('fk_act_to_invoice', 'act');
        $this->dropForeignKey('fk_invoice_facture_to_invoice', 'invoice_facture');
        $this->dropForeignKey('fk_packing_list_to_invoice', 'packing_list');
        $this->dropForeignKey('fk_order_to_invoice', 'order');

        $this->truncateTable('act');
        $this->truncateTable('packing_list');
        $this->truncateTable('invoice_facture');
        $this->truncateTable('order');
        $this->truncateTable('invoice');

        $this->dropTable('invoice');
        $this->execute("
        CREATE TABLE IF NOT EXISTS `invoice` (
          `id` int(11) NOT NULL AUTO_INCREMENT,
          `seller_id` int(11) NOT NULL COMMENT 'Продавец',
          `out_invoice_status_id` int(11) NOT NULL COMMENT 'Статус',
          `updated_status` int(11) NOT NULL COMMENT 'Дата возникновения текущего статуса',
          `author_status_id` int(11) NOT NULL COMMENT 'Автор текущего статуса',
          `created_at` int(11) NOT NULL COMMENT 'Дата создания',
          `author_id` int(11) NOT NULL COMMENT 'Автор',
          `date_invoice` date DEFAULT NULL COMMENT 'Дата счёта',
          `number` int(11) NOT NULL COMMENT 'Основной номер счета',
          `add_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Дополнительный номер счёта',
          `invoice_number` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Номер исх. счёта',
          `date_pay` datetime DEFAULT NULL COMMENT 'Оплатить до',
          `seller_bank` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Наименование банка продавца',
          `seller_inn` int(11) NOT NULL COMMENT 'ИНН продавца',
          `seller_kpp` int(11) NOT NULL COMMENT 'КПП продавца',
          `seller_egrip` int(11) NOT NULL COMMENT 'ЕГРИП продавца',
          `seller_bik` int(11) NOT NULL COMMENT 'БИК продавца',
          `seller_ks` int(11) NOT NULL COMMENT 'к/с продавца',
          `seller_ps` int(11) NOT NULL COMMENT 'р/с продавца',
          `seller_full_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Полное название продавца',
          `seller_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Краткое название продавца',
          `seller_phone` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Телефон продавца',
          `seller_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Юридический адрес продавца',
          `seller_boss_position` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Должность руководителя продавца',
          `seller_boss_lname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Фамилия руководителя продавца',
          `seller_boss_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Имя руководителя продавца (инициал)',
          `seller_boss_mname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Отчество руководителя продавца (инициал)',
          `seller_stamp` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Печать продавца',
          `seller_signature` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Подпись руководителя продавца',
          `seller_bookkeeper_lname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Фамилия главного бухгалтера продавца',
          `seller_bookkeeper_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Имя главного бухгалтера продавца (инициал)',
          `seller_bookkeeper_mname` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Отчество главного бухгалтера продавца (инициал)',
          `customer_id` int(11) NOT NULL COMMENT 'Покупатель',
          `customer_full_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Полное название покупателя',
          `customer_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Краткое название покупателя',
          `customer_inn` int(11) NOT NULL COMMENT 'ИНН покупателя',
          `customer_kpp` int(11) NOT NULL COMMENT 'КПП покупателя',
          `customer_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Юридический адрес покупателя',
          `customer_bank` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Наименование банка покупателя',
          `customer_bik` int(11) NOT NULL COMMENT 'БИК покупателя',
          `customer_ks` int(11) NOT NULL COMMENT 'к/с покупателя',
          `customer_ps` int(11) NOT NULL COMMENT 'р/с покупателя',
          `type` int(1) NOT NULL COMMENT 'Тип счёта',
          `count` int(11) NOT NULL COMMENT 'Итого количество мест, штук',
          `gross_mass` float NOT NULL COMMENT 'Итого масса брутто',
          `price_without_nds` float NOT NULL COMMENT 'Сумма без учёта НДС',
          `price_with_nds` float NOT NULL COMMENT 'Сумма НДС',
          `total_price` float NOT NULL COMMENT 'Итого',
          `avail_nds` int(1) NOT NULL COMMENT 'Наличие строки «В т.ч. НДС»',
          `including_nds` int(11) NOT NULL COMMENT 'В т.ч. НДС',
          `amount` float NOT NULL COMMENT 'Всего к оплате',
          `amount_string` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Сумма прописью',
          `pay_type_id` int(11) DEFAULT NULL COMMENT 'Форма оплаты',
          `amount_partial` float NOT NULL COMMENT 'Сумма частичной оплаты',
          `web_url` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL COMMENT 'Web-страница счёта',
          `production_type` int(1) NOT NULL COMMENT 'Тип счёта - 0 (услуга), 1 (товар)',
          `invoice_expenditure_item_id` int(11) DEFAULT NULL,
          PRIMARY KEY (`id`),
          KEY `invoice_to_invoice_expenditure_item` (`invoice_expenditure_item_id`)
        ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
        ");

        $this->addForeignKey('invoice_to_invoice_expenditure_item', 'invoice', 'invoice_expenditure_item_id', 'invoice_expenditure_item', 'id');

        $this->dropTable('payment_form');
        $this->createTable('payment_variant',[
            'id' => Schema::TYPE_PK,
            'payment_name' => Schema::TYPE_STRING . ' DEFAULT NULL COMMENT "Название формы оплаты"'
        ]);

        $this->addForeignKey('act_to_invoice', 'act', 'invoice_id', 'invoice', 'id');
        $this->addForeignKey('packing_list_to_invoice', 'packing_list', 'invoice_id', 'invoice', 'id');
        $this->addForeignKey('invoice_facture_to_invoice', 'invoice_facture', 'invoice_id', 'invoice', 'id');
        $this->addForeignKey('order_to_invoice', 'order', 'invoice_id', 'invoice', 'id');
    }
}
