<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210130_161018_alter_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_config','report_odds_active_tab',$this->tinyInteger()->notNull()->defaultValue(1));
    }

    public function safeDown()
    {
        $this->dropColumn('user_config','report_odds_active_tab');
    }
}
