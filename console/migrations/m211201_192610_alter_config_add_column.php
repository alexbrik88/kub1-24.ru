<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211201_192610_alter_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_config', 'detailing_index_chart_type', $this->tinyInteger()->defaultValue(1));
    }

    public function safeDown()
    {
        $this->dropColumn('user_config', 'detailing_index_chart_type');
    }
}