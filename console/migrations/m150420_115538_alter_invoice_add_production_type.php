<?php

use yii\db\Schema;
use yii\db\Migration;

class m150420_115538_alter_invoice_add_production_type extends Migration
{
    public function up()
    {
        $this->addColumn('invoice', 'production_type', Schema::TYPE_INTEGER . '(1) NOT NULL COMMENT "Тип счёта - 0 (услуга), 1 (товар)"');
    }

    public function down()
    {
        $this->dropColumn('invoice', 'production_type');
    }
}
