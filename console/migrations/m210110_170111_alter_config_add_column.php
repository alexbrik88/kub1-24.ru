<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210110_170111_alter_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_config', 'contractor_new_invoice', $this->boolean(1)->notNull()->defaultValue(true));
        $this->addColumn('user_config', 'contractor_prepaymentsum', $this->boolean(1)->notNull()->defaultValue(false));
        $this->addColumn('user_config', 'contractor_balancesum', $this->boolean(1)->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('user_config', 'contractor_new_invoice');
        $this->dropColumn('user_config', 'contractor_prepaymentsum');
        $this->dropColumn('user_config', 'contractor_balancesum');
    }
}