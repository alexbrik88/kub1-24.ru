<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210816_031840_alter_ofd_kkt_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('ofd_kkt', 'other_fiscal_numbers', $this->string()->after('fiscal_number')->comment('Номера предыдущих фискальных накопителей'));
    }

    public function safeDown()
    {
        $this->dropColumn('ofd_kkt', 'other_fiscal_numbers');
    }
}
