<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190619_130814_alter_invoice_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('invoice', 'has_volume', $this->boolean()->notNull()->defaultValue(false)->after('has_markup'));
        $this->addColumn('invoice', 'has_weight', $this->boolean()->notNull()->defaultValue(false)->after('has_markup'));
    }

    public function safeDown()
    {
        $this->dropColumn('invoice', 'has_volume');
        $this->dropColumn('invoice', 'has_weight');
    }
}
