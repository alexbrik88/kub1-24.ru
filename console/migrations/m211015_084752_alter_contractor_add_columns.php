<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211015_084752_alter_contractor_add_columns extends Migration
{
    public static $alterTable = 'contractor';
    public static $refIndustryTable = 'company_industry';
    public static $refSalePointTable = 'sale_point';
    
    public function safeUp()
    {
        $table = self::$alterTable;
        $this->addColumn($table, 'customer_industry_id', $this->integer());
        $this->addColumn($table, 'seller_industry_id', $this->integer());        
        $this->addColumn($table, 'customer_sale_point_id', $this->integer());
        $this->addColumn($table, 'seller_sale_point_id', $this->integer());
        $this->addForeignKey("FK_{$table}_to_customer_industry", $table, 'customer_industry_id', self::$refIndustryTable, 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey("FK_{$table}_to_seller_industry", $table, 'seller_industry_id', self::$refIndustryTable, 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey("FK_{$table}_to_customer_sale_point", $table, 'customer_sale_point_id', self::$refSalePointTable, 'id', 'SET NULL', 'CASCADE');
        $this->addForeignKey("FK_{$table}_to_seller_sale_point", $table, 'seller_sale_point_id', self::$refSalePointTable, 'id', 'SET NULL', 'CASCADE');
    }

    public function safeDown()
    {
        $table = self::$alterTable;
        $this->dropForeignKey("FK_{$table}_to_customer_industry", $table);
        $this->dropForeignKey("FK_{$table}_to_seller_industry", $table);
        $this->dropForeignKey("FK_{$table}_to_customer_sale_point", $table);
        $this->dropForeignKey("FK_{$table}_to_seller_sale_point", $table);
        $this->dropColumn($table, 'customer_industry_id');
        $this->dropColumn($table, 'seller_industry_id');
        $this->dropColumn($table, 'customer_sale_point_id');
        $this->dropColumn($table, 'seller_sale_point_id');
    }
}
