<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180205_095524_insert_discount_type extends Migration
{
    public function safeUp()
    {
        $this->batchInsert('discount_type', [
            'id',
            'description',
        ], [
            [2, 'Скидка на 13-14 день триала, если нет оплаченной подписки.'],
            [3, 'Скидка на 14 день после окончания триала, при оплате подписки на 4 месяца или 12 месяцев, если нет оплаченной подписки.'],
        ]);
    }

    public function safeDown()
    {
        $this->delete('discount_type', ['id' => [2, 3]]);
    }
}
