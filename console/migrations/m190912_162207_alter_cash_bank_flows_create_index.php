<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190912_162207_alter_cash_bank_flows_create_index extends Migration
{
    public function safeUp()
    {
        $this->createIndex('rs', '{{%cash_bank_flows}}', 'rs');
    }

    public function safeDown()
    {
        $this->dropIndex('rs', '{{%cash_bank_flows}}');
    }
}
