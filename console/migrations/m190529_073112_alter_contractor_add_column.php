<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190529_073112_alter_contractor_add_column extends Migration
{
    public $tContractor = 'contractor';
    public $tCreatedType = 'create_type';

    public function safeUp()
    {
        $this->addColumn($this->tContractor, 'create_type_id', $this->integer()->notNull()->defaultValue(1));
        $this->addForeignKey('FK_contractor_to_create_type', $this->tContractor, 'create_type_id', $this->tCreatedType, 'id');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_contractor_to_create_type', $this->tContractor);
        $this->dropColumn($this->tContractor, 'create_type_id');
    }
}
