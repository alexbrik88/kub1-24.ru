<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190306_202650_create_cash_flows_auto_plan extends Migration
{
    public $tableName = 'cash_flows_auto_plan';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'flow_type' => $this->boolean()->notNull()->comment('0 - Расход, 1 - Приход'),
            'payment_type' => $this->integer()->notNull()->comment('1 - Банк, 2 - Касса, 3 - E-money'),
            'item' => $this->integer()->notNull(),
            'contractor_id' => $this->string()->notNull(),
            'amount' => $this->decimal(38, 2)->defaultValue(null),
            'plan_date' => $this->date()->defaultValue(null),
            'end_plan_date' => $this->date()->defaultValue(null),
            'is_deleted' => $this->boolean()->defaultValue(false),
        ]);

        $this->addForeignKey($this->tableName . '_company_id', $this->tableName, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_company_id', $this->tableName);

        $this->dropTable($this->tableName);
    }
}
