<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200604_083500_alter_table_contractor_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('contractor', 'decoding_legal_form', $this->string(20));
        $this->addColumn('contractor', 'country_id', $this->integer());
        $this->addColumn('contractor', 'currency_id', $this->string(3));

        $this->addForeignKey(
            'contractor_country_id_fk',
            'contractor',
            'country_id',
            'country',
            'id',
            'RESTRICT'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey(
            'contractor_country_id_fk',
            'contractor'
        );

        $this->dropColumn('contractor', 'decoding_legal_form');
        $this->dropColumn('contractor', 'country_id');
        $this->dropColumn('contractor', 'currency_id');

    }
}
