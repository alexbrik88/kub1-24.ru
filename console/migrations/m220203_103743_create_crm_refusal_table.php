<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%crm_refusal}}`.
 */
class m220203_103743_create_crm_refusal_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%crm_refusal}}', [
            'refusal_id' => $this->bigPrimaryKey(),
            'company_id' => $this->integer()->notNull(),
            'employee_id' => $this->integer()->null(),
            'status' => $this->boolean()->notNull()->defaultValue(true),
            'created_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'updated_at' => $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP'),
            'name' => $this->text()->notNull(),
        ]);

        $this->createIndex('company_id__employee_id', '{{%crm_refusal}}', ['company_id', 'employee_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%crm_refusal}}');
    }
}
