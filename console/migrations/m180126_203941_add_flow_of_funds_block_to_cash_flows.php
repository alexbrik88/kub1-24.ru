<?php

use console\components\db\Migration;
use yii\db\Schema;
use common\models\Company;
use common\models\document\InvoiceIncomeItem;
use common\models\document\InvoiceExpenditureItem;

class m180126_203941_add_flow_of_funds_block_to_cash_flows extends Migration
{
    public $tIncomeItemFlowOfFunds = 'income_item_flow_of_funds';
    public $tExpenseItemFlowOfFunds = 'expense_item_flow_of_funds';

    public function safeUp()
    {
        $this->createTable($this->tIncomeItemFlowOfFunds, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'income_item_id' => $this->integer()->notNull(),
            'flow_of_funds_block' => $this->integer()->defaultValue(null),
        ]);

        $this->addForeignKey($this->tIncomeItemFlowOfFunds . '_company_id', $this->tIncomeItemFlowOfFunds, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tIncomeItemFlowOfFunds . '_income_item_id', $this->tIncomeItemFlowOfFunds, 'income_item_id', 'invoice_income_item', 'id', 'RESTRICT', 'CASCADE');


        $this->createTable($this->tExpenseItemFlowOfFunds, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'name' => $this->string()->notNull(),
            'expense_item_id' => $this->integer()->notNull(),
            'flow_of_funds_block' => $this->integer()->defaultValue(null),
        ]);

        $this->addForeignKey($this->tExpenseItemFlowOfFunds . '_company_id', $this->tExpenseItemFlowOfFunds, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tExpenseItemFlowOfFunds . '_expense_item_id', $this->tExpenseItemFlowOfFunds, 'expense_item_id', 'invoice_expenditure_item', 'id', 'RESTRICT', 'CASCADE');

        $incomeKey = 1;
        $expenseKey = 1;
        $query = (new \yii\db\Query())->select('id')->from('company');
        foreach ($query->all() as $company) {
            /* @var $incomeItem InvoiceIncomeItem */
            foreach (InvoiceIncomeItem::find()->all() as $incomeItem) {
                $this->insert($this->tIncomeItemFlowOfFunds, [
                    'id' => $incomeKey,
                    'company_id' => $company['id'],
                    'name' => $incomeItem->name,
                    'income_item_id' => $incomeItem->id,
                ]);
                $incomeKey++;
            }
            /* @var $expenseItem InvoiceExpenditureItem */
            foreach (InvoiceExpenditureItem::find()->all() as $expenseItem) {
                $this->insert($this->tExpenseItemFlowOfFunds, [
                    'id' => $expenseKey,
                    'company_id' => $company['id'],
                    'name' => $expenseItem->name,
                    'expense_item_id' => $expenseItem->id,
                ]);
                $expenseKey++;
            }
        }
    }

    public function safeDown()
    {
        $this->dropForeignKey($this->tIncomeItemFlowOfFunds . '_company_id', $this->tIncomeItemFlowOfFunds);
        $this->dropForeignKey($this->tIncomeItemFlowOfFunds . '_income_item_id', $this->tIncomeItemFlowOfFunds);

        $this->dropForeignKey($this->tExpenseItemFlowOfFunds . '_company_id', $this->tExpenseItemFlowOfFunds);
        $this->dropForeignKey($this->tExpenseItemFlowOfFunds . '_expense_item_id', $this->tExpenseItemFlowOfFunds);

        $this->dropTable($this->tIncomeItemFlowOfFunds);
        $this->dropTable($this->tExpenseItemFlowOfFunds);
    }
}


