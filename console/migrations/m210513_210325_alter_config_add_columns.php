<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210513_210325_alter_config_add_columns extends Migration
{
    public $table = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->table, 'table_retail_receipt_expense', $this->tinyInteger()->notNull()->defaultValue(1));
        $this->addColumn($this->table, 'table_retail_receipt_payment', $this->tinyInteger()->notNull()->defaultValue(1));
        $this->addColumn($this->table, 'table_retail_receipt_store', $this->tinyInteger()->notNull()->defaultValue(1));
        $this->addColumn($this->table, 'table_retail_receipt_kkt', $this->tinyInteger()->notNull()->defaultValue(1));
        $this->addColumn($this->table, 'table_retail_receipt_number', $this->tinyInteger()->notNull()->defaultValue(1));
        $this->addColumn($this->table, 'table_retail_receipt_document', $this->tinyInteger()->notNull()->defaultValue(1));
        $this->addColumn($this->table, 'table_retail_receipt_shift', $this->tinyInteger()->notNull()->defaultValue(1));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'table_retail_receipt_expense');
        $this->dropColumn($this->table, 'table_retail_receipt_payment');
        $this->dropColumn($this->table, 'table_retail_receipt_store');
        $this->dropColumn($this->table, 'table_retail_receipt_kkt');
        $this->dropColumn($this->table, 'table_retail_receipt_number');
        $this->dropColumn($this->table, 'table_retail_receipt_document');
        $this->dropColumn($this->table, 'table_retail_receipt_shift');
    }
}
