<?php

use yii\db\Migration;

/**
 * Handles the creation of table `service_discount_by_quantity`.
 */
class m190129_094749_create_service_discount_by_quantity_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('service_discount_by_quantity', [
            'id' => $this->primaryKey(),
            'tariff_id' => $this->integer()->notNull(),
            'quantity' => $this->integer()->notNull(),
            'percent' => $this->decimal(7,4)->notNull(),
        ]);

        $this->addForeignKey(
            'FK_service_discount_by_quantity_tariff',
            'service_discount_by_quantity', 'tariff_id',
            'service_subscribe_tariff', 'id'
        );

        $this->batchInsert('service_discount_by_quantity', [
            'tariff_id',
            'quantity',
            'percent',
        ], [
            [1, 1, 0],
            [1, 2, 25],
            [1, 3, 33.3333],
            [1, 4, 41.6666],
            [1, 5, 50],
            [2, 1, 0],
            [2, 2, 25],
            [2, 3, 33.3333],
            [2, 4, 41.6666],
            [2, 5, 50],
            [3, 1, 0],
            [3, 2, 25],
            [3, 3, 33.3333],
            [3, 4, 41.6666],
            [3, 5, 50],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('service_discount_by_quantity');
    }
}
