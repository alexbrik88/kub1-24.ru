<?php

use yii\db\Schema;
use yii\db\Migration;

class m150415_134625_create_productType extends Migration
{
    public function up()
    {
        $this->createTable('company_product_type', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . '(45) NOT NULL',
        ]);
        $this->batchInsert('company_product_type', ['id', 'name',], [
            [1, 'Товары'],
            [2, 'Услуги'],
        ]);

        $this->createTable('company_to_company_product_type', [
            'id' => Schema::TYPE_PK,
            'company_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            'company_product_type_id' => Schema::TYPE_INTEGER . ' NOT NULL',
        ]);
        $this->addForeignKey('company_to_product_type_company', 'company_to_company_product_type', 'company_id', 'company', 'id');
        $this->addForeignKey('company_to_product_type_company_product_type', 'company_to_company_product_type', 'company_product_type_id', 'company_product_type', 'id');
    }

    public function down()
    {
        $this->dropTable('company_to_company_product_type');
        $this->dropTable('company_product_type');
    }
}
