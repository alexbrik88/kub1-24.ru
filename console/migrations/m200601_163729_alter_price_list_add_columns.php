<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200601_163729_alter_price_list_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('price_list', 'include_count_in_package_column', $this->boolean()->defaultValue(false)->notNull()->after('include_price_column'));
        $this->addColumn('price_list', 'include_box_type_column', $this->boolean()->defaultValue(false)->notNull()->after('include_price_column'));
        $this->addColumn('price_list', 'include_custom_field_column', $this->boolean()->defaultValue(false)->notNull()->after('include_price_column'));
        $this->addColumn('price_list', 'include_brutto_column', $this->boolean()->defaultValue(false)->notNull()->after('include_price_column'));
        $this->addColumn('price_list', 'include_netto_column', $this->boolean()->defaultValue(false)->notNull()->after('include_price_column'));
        $this->addColumn('price_list', 'include_weight_column', $this->boolean()->defaultValue(false)->notNull()->after('include_price_column'));
    }

    public function safeDown()
    {
        $this->dropColumn('price_list', 'include_count_in_package_column');
        $this->dropColumn('price_list', 'include_box_type_column');
        $this->dropColumn('price_list', 'include_custom_field_column');
        $this->dropColumn('price_list', 'include_brutto_column');
        $this->dropColumn('price_list', 'include_netto_column');
        $this->dropColumn('price_list', 'include_weight_column');
    }
}