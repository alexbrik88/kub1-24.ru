<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%bitrix24_client}}`.
 */
class m220111_160914_create_bitrix24_client_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%bitrix24_client}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(50),
            'portal' => $this->string(100),
            'user_id' => $this->integer(),
            'data' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%bitrix24_client}}');
    }
}
