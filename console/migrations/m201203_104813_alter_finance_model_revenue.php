<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201203_104813_alter_finance_model_revenue extends Migration
{
    public static $table = 'finance_model_revenue';

    public function safeUp()
    {
        $this->alterColumn(self::$table, 'average_check', $this->bigInteger()->defaultValue(0));
        $this->alterColumn(self::$table, 'check_count', $this->bigInteger()->defaultValue(0));
        $this->alterColumn(self::$table, 'visit_count', $this->bigInteger()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->alterColumn(self::$table, 'average_check', $this->integer()->defaultValue(0));
        $this->alterColumn(self::$table, 'check_count', $this->integer()->defaultValue(0));
        $this->alterColumn(self::$table, 'visit_count', $this->integer()->defaultValue(0));
    }
}
