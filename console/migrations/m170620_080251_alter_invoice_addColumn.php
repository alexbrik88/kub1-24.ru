<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170620_080251_alter_invoice_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{invoice}}', 'has_discount', $this->smallInteger(1)->notNull()->defaultValue(false) . ' AFTER [[total_order_count]]');
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{invoice}}', 'has_discount');
    }
}
