<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211119_082856_alter_config_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('user_config', 'order_document_detailing', $this->boolean()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('user_config', 'order_document_detailing');
    }
}
