<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210902_161949_create_moysklad_import extends Migration
{
    public $table = 'moysklad_import';

    public function safeUp()
    {
        $table = $this->table;
        $this->createTable($table, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'employee_id' => $this->integer()->notNull(),
            'loading_step' => $this->tinyInteger(),
            'file_data' => $this->string(),
            'doubles_data' => $this->string(),
            'uploaded_data' => $this->string(),
            'runtime_error' => $this->string(),
            'created_at' => $this->integer()
        ]);
        $this->addForeignKey("FK_{$table}_to_company", $table, 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
        $this->addForeignKey("FK_{$table}_to_employee", $table, 'employee_id', 'employee', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $table = $this->table;
        $this->dropForeignKey("FK_{$table}_to_company", $table);
        $this->dropForeignKey("FK_{$table}_to_employee", $table);
        $this->dropTable($table);
    }
}
