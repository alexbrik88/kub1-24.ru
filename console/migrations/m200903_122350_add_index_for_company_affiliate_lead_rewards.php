<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200903_122350_add_index_for_company_affiliate_lead_rewards extends Migration
{
    public $tableName = 'company_affiliate_lead_rewards';

    public function safeUp()
    {
        $this->createIndex($this->tableName . '_company_id_idx', $this->tableName, 'company_id');
        $this->createIndex($this->tableName . '_is_current_idx', $this->tableName, 'is_current');
    }

    public function safeDown()
    {
        $this->dropIndex($this->tableName . '_company_id_idx', $this->tableName);
        $this->dropIndex($this->tableName . '_is_current_idx', $this->tableName);
    }
}
