<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191104_081732_alter_product_create_index extends Migration
{
    public function safeUp()
    {
        $this->createIndex(
            'article',
            '{{%product}}',
            'article'
        );
    }

    public function safeDown()
    {
        $this->dropIndex(
            'article',
            '{{%product}}'
        );
    }
}
