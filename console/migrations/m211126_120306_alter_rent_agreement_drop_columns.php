<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211126_120306_alter_rent_agreement_drop_columns extends Migration
{
    public function safeUp()
    {
        $this->dropForeignKey('rent_entity_rent_entity_id', '{{%rent_agreement}}');
        $this->dropForeignKey('rent_entity_rent_invoice_id', '{{%rent_agreement}}');

        $this->dropColumn('{{%rent_agreement}}', 'entity_id');
        $this->dropColumn('{{%rent_agreement}}', 'invoice_id');
        $this->dropColumn('{{%rent_agreement}}', 'price');
    }

    public function safeDown()
    {
        $this->addColumn('{{%rent_agreement}}', 'entity_id', $this->integer());
        $this->addColumn('{{%rent_agreement}}', 'invoice_id', $this->integer());
        $this->addColumn('{{%rent_agreement}}', 'price', $this->float());

        $this->addForeignKey('rent_entity_rent_entity_id', '{{%rent_agreement}}', 'entity_id', 'rent_entity', 'id');
        $this->addForeignKey('rent_entity_rent_invoice_id', '{{%rent_agreement}}', 'invoice_id', 'invoice', 'id');
    }
}
