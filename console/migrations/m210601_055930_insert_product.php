<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210601_055930_insert_product extends Migration
{
    public function safeUp()
    {
        $this->execute('
            INSERT INTO `product`(
                `creator_id`,
                `company_id`,
                `production_type`,
                `created_at`,
                `title`,
                `barcode`,
                `product_unit_id`,
                `price_for_sell_nds_id`,
                `price_for_sell_with_nds`,
                `country_origin_id`)
            SELECT
                co.`owner_employee_id`,
                ri.`company_id`,
                IF(ri.`product_type` IN (3,4), 0, 1),
                UNIX_TIMESTAMP(MIN(ri.`date_time`)),
                ri.`name`,
                ri.`barcode`,
                1,
                IF(IFNULL(tt.`osno`, 0)=1, 5, 4),
                ri.`price`,
                1
            FROM `ofd_receipt_item` ri
                LEFT JOIN `product` pr ON ri.`company_id` = pr.`company_id` AND ri.`name` = pr.`title` AND pr.`is_deleted` = 0
                LEFT JOIN `company` co ON ri.`company_id` = co.`id`
                LEFT JOIN `company_taxation_type` tt ON tt.`company_id` = co.`id`
            WHERE ri.`product_id` IS NULL AND pr.`id` IS NULL
            GROUP BY pr.`company_id`, ri.`name`
        ');
    }

    public function safeDown()
    {
        //
    }
}
