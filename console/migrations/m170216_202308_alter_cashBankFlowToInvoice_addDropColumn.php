<?php
use common\models\cash;
use common\models\document\Invoice;
use console\components\db\Migration;
use yii\db\Schema;

class m170216_202308_alter_cashBankFlowToInvoice_addDropColumn extends Migration
{
    public function safeUp()
    {
        $this->execute("
            DELETE t1 FROM {{%cash_bank_flow_to_invoice}} t1, {{%cash_bank_flow_to_invoice}} t2
            WHERE t1.id > t2.id AND t1.flow_id = t2.flow_id AND t1.invoice_id = t2.invoice_id
        ");

        $this->addColumn('{{%cash_bank_flow_to_invoice}}', 'amount', $this->bigInteger(20)->notNull());
        $this->dropColumn('{{%cash_bank_flow_to_invoice}}', 'id');
        $this->addPrimaryKey('PRIMARY_KEY', '{{%cash_bank_flow_to_invoice}}', ['flow_id', 'invoice_id']);

        /*$this->execute("
            UPDATE {{%cash_bank_flow_to_invoice}} t
            LEFT JOIN {{%cash_bank_flows}} b ON b.id = t.flow_id
            LEFT JOIN {{%invoice}} i ON i.id = t.invoice_id
            SET t.amount = LEAST(b.amount, i.total_amount_with_nds)
        ");*/
    }
    
    public function safeDown()
    {
        $this->dropPrimaryKey('PRIMARY_KEY', '{{%cash_bank_flow_to_invoice}}');
        $this->addColumn('{{%cash_bank_flow_to_invoice}}', 'id', $this->primaryKey()->first());
        $this->dropColumn('{{%cash_bank_flow_to_invoice}}', 'amount');
    }
}
