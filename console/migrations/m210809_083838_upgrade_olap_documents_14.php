<?php

use console\components\db\Migration;
use yii\db\Expression;
use yii\db\Query;
use yii\db\Schema;

/**
 * Table: olap_documents
 * Docs: act, upd, packing_list, sales_invoice
 * Changes:
 *   - add column "industry_id"
 * Fix:
 *   - add "project_id", "industry_id", "sale_point_id", "contractor_id" into trigger "olap_documents_update_invoice"
 *
 */
class m210809_083838_upgrade_olap_documents_14 extends Migration
{
    // Created table
    const TABLE_OLAP_DOCS = 'olap_documents';

    const TABLE_PACKING_LIST = 'packing_list';
    const TABLE_UPD = 'upd';
    const TABLE_ACT = 'act';
    const TABLE_SALES_INVOICE = 'sales_invoice';
    const TABLE_INVOICE = 'invoice';
    const REF_TABLE_INVOICE_UPD = 'invoice_upd';
    const REF_TABLE_INVOICE_ACT = 'invoice_act';

    const DOCUMENT_ACT = 1;
    const DOCUMENT_PACKING_LIST = 4;
    const DOCUMENT_UPD = 8;
    const DOCUMENT_SALES_INVOICE = 16;
    const DOCUMENT_INITIAL_BALANCE = 0;

    public function safeUp()
    {
        $table = self::TABLE_OLAP_DOCS;
        $idx = "idx_{$table}";

        $this->deleteTriggers();
        $this->deleteOlapTable();

        $this->createOlapTable();
        $this->createTriggers();

        $this->execute("CREATE INDEX {$idx}_date ON {$table} (`company_id`, `date`);");
        $this->execute("CREATE INDEX {$idx}_contractor ON {$table} (`contractor_id`);");
        $this->execute("ALTER TABLE {$table} ADD PRIMARY KEY (`id`, `by_document`);");
    }

    public function safeDown()
    {
        $this->deleteTriggers();
        $this->deleteOlapTable();
    }

    private function createOlapTable()
    {
        $table = self::TABLE_OLAP_DOCS;

        $this->execute("CREATE TABLE IF NOT EXISTS {$table} (
            `company_id` INT NOT NULL,
            `contractor_id` INT DEFAULT NULL,
            `sale_point_id` INT DEFAULT NULL,
            `industry_id` INT DEFAULT NULL,
            `project_id` INT DEFAULT NULL,
            `by_document` TINYINT NOT NULL,
            `id` INT NOT NULL,
            `type` TINYINT NOT NULL,
            `date` DATE NOT NULL,
            `amount` BIGINT DEFAULT 0,
            `is_accounting` TINYINT DEFAULT 1,
            `invoice_expenditure_item_id` INT,
            `invoice_income_item_id` INT            
        )");

        $this->execute("INSERT INTO {$table} ({$this->getOlapTableQuery()->createCommand()->rawSql});");
    }

    private function deleteOlapTable()
    {
        $table = self::TABLE_OLAP_DOCS;
        $this->execute("DROP TABLE IF EXISTS {$table}");
    }

    private function deleteTriggers()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_order_packing_list`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_delete_order_packing_list`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_order_upd`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_delete_order_upd`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_order_act`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_delete_order_act`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_insert_packing_list`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_packing_list`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_delete_packing_list`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_insert_upd`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_upd`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_delete_upd`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_insert_act`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_act`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_delete_act`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_invoice`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_contractor`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_order_sales_invoice`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_delete_order_sales_invoice`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_insert_sales_invoice`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_sales_invoice`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_delete_sales_invoice`;');
    }

    private function createTriggers()
    {
        $this->createTriggersOnSalesInvoice();
        $this->createTriggersOnPackingList();
        $this->createTriggersOnUpd();
        $this->createTriggersOnAct();
        $this->createTriggersOnInvoice();
        $this->createTriggersOnContractor();
    }

    private function getOlapTableQuery()
    {
        $select = [
            'invoice.company_id',
            'invoice.contractor_id',
            'invoice.sale_point_id',
            'invoice.industry_id',
            'invoice.project_id',
            'by_document',
            'document.id',
            'document.type',
            'document.document_date AS date',
            'SUM( IF(document.type = 2, `order_main`.selling_price_with_vat, `order_main`.purchase_price_with_vat) * `order`.quantity) AS amount',
            'IF (contractor.not_accounting, 0, 1) AS is_accounting',
            'invoice.invoice_expenditure_item_id',
            'invoice.invoice_income_item_id'
        ];

        // оборот по товарным накладным
        $query1 = (new Query)
            ->select($select)
            ->addSelect(['by_document' => new Expression('"' . self::DOCUMENT_PACKING_LIST . '"')])
            ->from(['order' => 'order_packing_list'])
            ->leftJoin(['document' => self::TABLE_PACKING_LIST], '{{document}}.[[id]] = {{order}}.[[packing_list_id]]')
            ->leftJoin(['invoice' => self::TABLE_INVOICE], '{{invoice}}.[[id]] = {{document}}.[[invoice_id]]')
            ->leftJoin(['order_main' => 'order'], '{{order_main}}.[[id]] = {{order}}.[[order_id]]')
            ->leftJoin(['contractor' => 'contractor'], 'contractor.id = invoice.contractor_id')
            ->andWhere([
                'invoice.is_deleted' => false,
                'invoice.invoice_status_id' => [1, 2, 3, 4, 6, 7, 8], // valid statuses
            ])
            ->andWhere([
                'or',
                ['document.status_out_id' => [1, 2, 3, 4]], // valid statuses
                ['document.status_out_id' => null],
            ])
            ->groupBy('invoice.id, document.id');

        // оборот по УПД
        $query2 = (new Query)
            ->select($select)
            ->addSelect(['by_document' => new Expression('"' . self::DOCUMENT_UPD . '"')])
            ->from(['order' => 'order_upd'])
            ->leftJoin(['document' => self::TABLE_UPD], '{{document}}.[[id]] = {{order}}.[[upd_id]]')
            ->leftJoin(['invoice_upd' => self::REF_TABLE_INVOICE_UPD], '{{invoice_upd}}.[[upd_id]] = {{document}}.[[id]]')
            ->leftJoin(['invoice' => self::TABLE_INVOICE], '{{invoice}}.[[id]] = {{invoice_upd}}.[[invoice_id]]')
            ->leftJoin(['order_main' => 'order'], '{{order_main}}.[[id]] = {{order}}.[[order_id]]')
            ->leftJoin(['contractor' => 'contractor'], 'contractor.id = invoice.contractor_id')
            ->andWhere([
                'invoice.is_deleted' => false,
                'invoice.invoice_status_id' => [1, 2, 3, 4, 6, 7, 8],
            ])
            ->andWhere([
                'or',
                ['document.status_out_id' => [1, 2, 3, 4]],
                ['document.status_out_id' => null],
            ])
            ->groupBy('invoice.id, document.id');

        // оборот по Актам
        $query3 = (new Query)
            ->select($select)
            ->addSelect(['by_document' => new Expression('"' . self::DOCUMENT_ACT . '"')])
            ->from(['order' => 'order_act'])
            ->leftJoin(['document' => self::TABLE_ACT], '{{document}}.[[id]] = {{order}}.[[act_id]]')
            ->leftJoin(['invoice_act' => self::REF_TABLE_INVOICE_ACT], '{{invoice_act}}.[[act_id]] = {{document}}.[[id]]')
            ->leftJoin(['invoice' => self::TABLE_INVOICE], '{{invoice}}.[[id]] = {{invoice_act}}.[[invoice_id]]')
            ->leftJoin(['order_main' => 'order'], '{{order_main}}.[[id]] = {{order}}.[[order_id]]')
            ->leftJoin(['contractor' => 'contractor'], 'contractor.id = invoice.contractor_id')
            ->andWhere([
                'invoice.is_deleted' => false,
                'invoice.invoice_status_id' => [1, 2, 3, 4, 6, 7, 8],
            ])
            ->andWhere([
                'or',
                ['document.status_out_id' => [1, 2, 3, 4]],
                ['document.status_out_id' => null],
            ])
            ->groupBy('invoice.id, document.id');

        // оборот по расходным накладным
        $query4 = (new Query)
            ->select($select)
            ->addSelect(['by_document' => new Expression('"' . self::DOCUMENT_SALES_INVOICE . '"')])
            ->from(['order' => 'order_sales_invoice'])
            ->leftJoin(['document' => self::TABLE_SALES_INVOICE], '{{document}}.[[id]] = {{order}}.[[sales_invoice_id]]')
            ->leftJoin(['invoice' => self::TABLE_INVOICE], '{{invoice}}.[[id]] = {{document}}.[[invoice_id]]')
            ->leftJoin(['order_main' => 'order'], '{{order_main}}.[[id]] = {{order}}.[[order_id]]')
            ->leftJoin(['contractor' => 'contractor'], 'contractor.id = invoice.contractor_id')
            ->andWhere([
                'invoice.is_deleted' => false,
                'invoice.invoice_status_id' => [1, 2, 3, 4, 6, 7, 8], // valid statuses
            ])
            ->andWhere([
                'or',
                ['document.status_out_id' => [1, 2, 3, 4]], // valid statuses
                ['document.status_out_id' => null],
            ])
            ->groupBy('invoice.id, document.id');


        return $query1->union($query2)->union($query3)->union($query4); // not "all" for prevent double primary (when two or more invoices)
    }

    private function createTriggersOnPackingList()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_insert_packing_list`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_packing_list`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_delete_packing_list`');

        $this->execute('
            CREATE TRIGGER `olap_documents_insert_packing_list`
            AFTER INSERT ON `packing_list`
            FOR EACH ROW
            BEGIN
            
              INSERT INTO `olap_documents` (`company_id`, `contractor_id`, `sale_point_id`, `industry_id`, `project_id`, `by_document`, `id`, `type`, `date`, `amount`, `is_accounting`, `invoice_expenditure_item_id`, `invoice_income_item_id`) 
              
              SELECT 
                `invoice`.`company_id`,
                `invoice`.`contractor_id`,
                `invoice`.`sale_point_id`,
                `invoice`.`industry_id`,
                `invoice`.`project_id`,
                "4" AS `by_document`, 
                `document`.`id`, 
                `document`.`type`, 
                `document`.`document_date` AS `date`, 
                IF(NEW.`status_out_id` = 5, 0, NEW.`orders_sum`) AS `amount`,
                IF (`contractor`.`not_accounting`, 0, 1) AS is_accounting,
                `invoice`.`invoice_expenditure_item_id`,
                `invoice`.`invoice_income_item_id`
              FROM `packing_list` `document`
              LEFT JOIN `invoice` ON `invoice`.`id` = `document`.`invoice_id`  
              LEFT JOIN `contractor` ON `contractor`.`id` = `invoice`.`contractor_id`
              WHERE 
                `document`.`id` = NEW.`id`;
              
            END        
        ');

        $this->execute('
            CREATE TRIGGER `olap_documents_update_packing_list`
            AFTER UPDATE ON `packing_list`
            FOR EACH ROW
            BEGIN
            
              IF IFNULL(NEW.`status_out_id`, 1) <> IFNULL(OLD.`status_out_id`, 1) OR NEW.`document_date` <> OLD.`document_date`
              THEN
                  UPDATE `olap_documents` 
                  SET 
                    `date` = NEW.`document_date`,
                    `amount` = IF(NEW.`status_out_id` = 5, 0, `amount`) 
                  WHERE `olap_documents`.`by_document` = "4" AND `olap_documents`.`id` = NEW.`id` LIMIT 1;           
              END IF;
              
            END
        ');

        $this->execute('
            CREATE TRIGGER `olap_documents_delete_packing_list`
            AFTER DELETE ON `packing_list`
            FOR EACH ROW
            BEGIN
            
              DELETE FROM `olap_documents` WHERE `by_document` = "4" AND `id` = OLD.`id` LIMIT 1;
              
            END  
        ');

        // ORDER_PACKING_LIST
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_order_packing_list`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_delete_order_packing_list`');

        $this->execute('
            CREATE TRIGGER `olap_documents_update_order_packing_list`
            AFTER UPDATE ON `order_packing_list`
            FOR EACH ROW
            BEGIN
             
              DECLARE _amount BIGINT;
              SET _amount = (
                SELECT 
                  SUM(IF(`document`.`status_out_id` = 5, 0, IF(`document`.`type` = 2, `order`.`selling_price_with_vat`, `order`.`purchase_price_with_vat`) * `order_doc`.`quantity`)) AS `amount`
                FROM `packing_list` `document`
                LEFT JOIN `order_packing_list` `order_doc` ON `document`.`id` = `order_doc`.`packing_list_id`
                LEFT JOIN `order` ON `order`.`id` = `order_doc`.`order_id`
                WHERE `document`.`id` = NEW.`packing_list_id` 
                GROUP BY `document`.`id`
              );
            
              UPDATE `olap_documents` 
              SET `amount` = _amount
              WHERE `olap_documents`.`by_document` = "4" AND `olap_documents`.`id` = NEW.`packing_list_id` LIMIT 1;           
              
            END   
        ');

        $this->execute('
            CREATE TRIGGER `olap_documents_delete_order_packing_list`
            AFTER DELETE ON `order_packing_list`
            FOR EACH ROW
            BEGIN
             
              DECLARE _amount BIGINT;
              SET _amount = (
                SELECT 
                  SUM(IF(`document`.`status_out_id` = 5, 0, IF(`document`.`type` = 2, `order`.`selling_price_with_vat`, `order`.`purchase_price_with_vat`) * `order_doc`.`quantity`)) AS `amount`
                FROM `packing_list` `document`
                LEFT JOIN `order_packing_list` `order_doc` ON `document`.`id` = `order_doc`.`packing_list_id`
                LEFT JOIN `order` ON `order`.`id` = `order_doc`.`order_id`
                WHERE `document`.`id` = OLD.`packing_list_id` 
                GROUP BY `document`.`id`
              );
            
              UPDATE `olap_documents` 
              SET `amount` = _amount
              WHERE `olap_documents`.`by_document` = "4" AND `olap_documents`.`id` = OLD.`packing_list_id` LIMIT 1;           
              
            END   
        ');
    }

    private function createTriggersOnUpd()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_insert_upd`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_upd`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_delete_upd`');

        $this->execute('
            CREATE TRIGGER `olap_documents_insert_upd`
            AFTER INSERT ON `upd`
            FOR EACH ROW
            BEGIN
            
              INSERT INTO `olap_documents` (`company_id`, `contractor_id`, `sale_point_id`, `industry_id`, `project_id`, `by_document`, `id`, `type`, `date`, `amount`, `is_accounting`, `invoice_expenditure_item_id`, `invoice_income_item_id`) 
              
              SELECT 
                `invoice`.`company_id`,
                `invoice`.`contractor_id`,
                `invoice`.`sale_point_id`,
                `invoice`.`industry_id`,
                `invoice`.`project_id`,
                "8" AS `by_document`, 
                `document`.`id`, 
                `document`.`type`, 
                `document`.`document_date` AS `date`, 
                IF(NEW.`status_out_id` = 5, 0, NEW.`orders_sum`) AS `amount`,
                IF (`contractor`.`not_accounting`, 0, 1) AS is_accounting,
                `invoice`.`invoice_expenditure_item_id`,
                `invoice`.`invoice_income_item_id`                
              FROM `upd` `document`
              LEFT JOIN `invoice` ON `invoice`.`id` = `document`.`invoice_id`  
              LEFT JOIN `contractor` ON `contractor`.`id` = `invoice`.`contractor_id`
              WHERE 
                `document`.`id` = NEW.`id`;
              
            END        
        ');

        $this->execute('
            CREATE TRIGGER `olap_documents_update_upd`
            AFTER UPDATE ON `upd`
            FOR EACH ROW
            BEGIN
            
              IF IFNULL(NEW.`status_out_id`, 1) <> IFNULL(OLD.`status_out_id`, 1) OR NEW.`document_date` <> OLD.`document_date`
              THEN            
                  UPDATE `olap_documents` 
                  SET 
                    `date` = NEW.`document_date`,
                    `amount` = IF(NEW.`status_out_id` = 5, 0, `amount`)
                  WHERE `olap_documents`.`by_document` = "8" AND `olap_documents`.`id` = NEW.`id` LIMIT 1;
              END IF;           
              
            END   
        ');

        $this->execute('
            CREATE TRIGGER `olap_documents_delete_upd`
            AFTER DELETE ON `upd`
            FOR EACH ROW
            BEGIN
            
              DELETE FROM `olap_documents` WHERE `by_document` = "8" AND `id` = OLD.`id` LIMIT 1;
              
            END        
        ');

        // ORDER_UPD
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_order_upd`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_delete_order_upd`');

        $this->execute('
            CREATE TRIGGER `olap_documents_update_order_upd`
            AFTER UPDATE ON `order_upd`
            FOR EACH ROW
            BEGIN
             
              DECLARE _amount BIGINT;
              SET _amount = (
                SELECT 
                  SUM(IF(`document`.`status_out_id` = 5, 0, IF(`document`.`type` = 2, `order`.`selling_price_with_vat`, `order`.`purchase_price_with_vat`) * `order_doc`.`quantity`)) AS `amount`
                FROM `upd` `document`
                LEFT JOIN `order_upd` `order_doc` ON `document`.`id` = `order_doc`.`upd_id`
                LEFT JOIN `order` ON `order`.`id` = `order_doc`.`order_id`
                WHERE `document`.`id` = NEW.`upd_id` 
                GROUP BY `document`.`id`
              );
            
              UPDATE `olap_documents` 
              SET `amount` = _amount
              WHERE `olap_documents`.`by_document` = "8" AND `olap_documents`.`id` = NEW.`upd_id` LIMIT 1;           
              
            END   
        ');

        $this->execute('
            CREATE TRIGGER `olap_documents_delete_order_upd`
            AFTER DELETE ON `order_upd`
            FOR EACH ROW
            BEGIN
             
              DECLARE _amount BIGINT;
              SET _amount = (
                SELECT 
                  SUM(IF(`document`.`status_out_id` = 5, 0, IF(`document`.`type` = 2, `order`.`selling_price_with_vat`, `order`.`purchase_price_with_vat`) * `order_doc`.`quantity`)) AS `amount`
                FROM `upd` `document`
                LEFT JOIN `order_upd` `order_doc` ON `document`.`id` = `order_doc`.`upd_id`
                LEFT JOIN `order` ON `order`.`id` = `order_doc`.`order_id`
                WHERE `document`.`id` = OLD.`upd_id` 
                GROUP BY `document`.`id`
              );
            
              UPDATE `olap_documents` 
              SET `amount` = _amount
              WHERE `olap_documents`.`by_document` = "8" AND `olap_documents`.`id` = OLD.`upd_id` LIMIT 1;           
              
            END   
        ');
    }

    private function createTriggersOnAct()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_insert_act`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_act`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_delete_act`');

        $this->execute('
            CREATE TRIGGER `olap_documents_insert_act`
            AFTER INSERT ON `act`
            FOR EACH ROW
            BEGIN
            
              INSERT INTO `olap_documents` (`company_id`, `contractor_id`, `sale_point_id`, `industry_id`, `project_id`, `by_document`, `id`, `type`, `date`, `amount`, `is_accounting`, `invoice_expenditure_item_id`, `invoice_income_item_id`) 
              
              SELECT 
                `invoice`.`company_id`,
                `invoice`.`contractor_id`,
                `invoice`.`sale_point_id`,
                `invoice`.`industry_id`,
                `invoice`.`project_id`,
                "1" AS `by_document`, 
                `document`.`id`, 
                `document`.`type`, 
                `document`.`document_date` AS `date`, 
                IF(NEW.`status_out_id` = 5, 0, NEW.`order_sum`) AS `amount`,               
                IF (`contractor`.`not_accounting`, 0, 1) AS is_accounting,
                `invoice`.`invoice_expenditure_item_id`,
                `invoice`.`invoice_income_item_id`
              FROM `act` `document`
              LEFT JOIN `invoice` ON `invoice`.`id` = `document`.`invoice_id`  
              LEFT JOIN `contractor` ON `contractor`.`id` = `invoice`.`contractor_id`
              WHERE 
                `document`.`id` = NEW.`id`;
              
            END        
        ');

        $this->execute('
            CREATE TRIGGER `olap_documents_update_act`
            AFTER UPDATE ON `act`
            FOR EACH ROW
            BEGIN
           
              IF IFNULL(NEW.`status_out_id`, 1) <> IFNULL(OLD.`status_out_id`, 1) OR NEW.`document_date` <> OLD.`document_date`
              THEN           
                  UPDATE `olap_documents` 
                  SET 
                    `date` = NEW.`document_date`,
                    `amount` = IF(NEW.`status_out_id` = 5, 0, `amount`)
                  WHERE `olap_documents`.`by_document` = "1" AND `olap_documents`.`id` = NEW.`id` LIMIT 1;
              END IF;           
              
            END   
        ');

        $this->execute('
            CREATE TRIGGER `olap_documents_delete_act`
            AFTER DELETE ON `act`
            FOR EACH ROW
            BEGIN
            
              DELETE FROM `olap_documents` WHERE `by_document` = "1" AND `id` = OLD.`id` LIMIT 1;
              
            END        
        ');

        // ORDER_ACT
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_order_act`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_delete_order_act`');

        $this->execute('
            CREATE TRIGGER `olap_documents_update_order_act`
            AFTER UPDATE ON `order_act`
            FOR EACH ROW
            BEGIN
             
              DECLARE _amount BIGINT;
              SET _amount = (
                SELECT 
                  SUM(IF(`document`.`status_out_id` = 5, 0, IF(`document`.`type` = 2, `order`.`selling_price_with_vat`, `order`.`purchase_price_with_vat`) * `order_doc`.`quantity`)) AS `amount`
                FROM `act` `document`
                LEFT JOIN `order_act` `order_doc` ON `document`.`id` = `order_doc`.`act_id`
                LEFT JOIN `order` ON `order`.`id` = `order_doc`.`order_id`
                WHERE `document`.`id` = NEW.`act_id` 
                GROUP BY `document`.`id`
              );
            
              UPDATE `olap_documents` 
              SET `amount` = _amount
              WHERE `olap_documents`.`by_document` = "1" AND `olap_documents`.`id` = NEW.`act_id` LIMIT 1;           
              
            END   
        ');

        $this->execute('
            CREATE TRIGGER `olap_documents_delete_order_act`
            AFTER DELETE ON `order_act`
            FOR EACH ROW
            BEGIN
             
              DECLARE _amount BIGINT;
              SET _amount = (
                SELECT 
                  SUM(IF(`document`.`status_out_id` = 5, 0, IF(`document`.`type` = 2, `order`.`selling_price_with_vat`, `order`.`purchase_price_with_vat`) * `order_doc`.`quantity`)) AS `amount`
                FROM `act` `document`
                LEFT JOIN `order_act` `order_doc` ON `document`.`id` = `order_doc`.`act_id`
                LEFT JOIN `order` ON `order`.`id` = `order_doc`.`order_id`
                WHERE `document`.`id` = OLD.`act_id` 
                GROUP BY `document`.`id`
              );
            
              UPDATE `olap_documents` 
              SET `amount` = _amount
              WHERE `olap_documents`.`by_document` = "1" AND `olap_documents`.`id` = OLD.`act_id` LIMIT 1;           
              
            END   
        ');
    }

    private function createTriggersOnInvoice()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_invoice`');

        $this->execute('
            CREATE TRIGGER `olap_documents_update_invoice`
            AFTER UPDATE ON `invoice`
            FOR EACH ROW
            BEGIN
            
              IF (
                IFNULL(NEW.`invoice_expenditure_item_id`, 0) <> IFNULL(OLD.`invoice_expenditure_item_id`, 0) OR
                IFNULL(NEW.`invoice_income_item_id`, 0) <> IFNULL(OLD.`invoice_income_item_id`, 0) OR
                IFNULL(NEW.`project_id`, 0) <> IFNULL(OLD.`project_id`, 0) OR
                IFNULL(NEW.`industry_id`, 0) <> IFNULL(OLD.`industry_id`, 0) OR
                IFNULL(NEW.`sale_point_id`, 0) <> IFNULL(OLD.`sale_point_id`, 0) OR
                IFNULL(NEW.`contractor_id`, 0) <> IFNULL(OLD.`contractor_id`, 0) 
              ) THEN
            
                /* Packing List */
                UPDATE `olap_documents` SET 
                  `invoice_expenditure_item_id` = NEW.`invoice_expenditure_item_id`,
                  `invoice_income_item_id` = NEW.`invoice_income_item_id`,
                  `project_id` = NEW.`project_id`,
                  `industry_id` = NEW.`industry_id`,
                  `sale_point_id` = NEW.`sale_point_id`,
                  `contractor_id` = NEW.`contractor_id`                  
                WHERE `olap_documents`.`by_document` = "4" AND `olap_documents`.`id` IN (    
                  
                  SELECT `id` FROM `packing_list` WHERE `invoice_id` = NEW.`id`    
                );
                
                /* Upd */
                UPDATE `olap_documents` SET 
                  `invoice_expenditure_item_id` = NEW.`invoice_expenditure_item_id`,
                  `invoice_income_item_id` = NEW.`invoice_income_item_id`,
                  `project_id` = NEW.`project_id`,
                  `industry_id` = NEW.`industry_id`,
                  `sale_point_id` = NEW.`sale_point_id`,
                  `contractor_id` = NEW.`contractor_id`
                WHERE `olap_documents`.`by_document` = "8" AND `olap_documents`.`id` IN (    
                  
                  SELECT `upd_id` FROM `invoice_upd` WHERE `invoice_id` = NEW.`id`
                );
                
                /* Act */
                UPDATE `olap_documents` SET 
                  `invoice_expenditure_item_id` = NEW.`invoice_expenditure_item_id`,
                  `invoice_income_item_id` = NEW.`invoice_income_item_id`,
                  `project_id` = NEW.`project_id`,
                  `industry_id` = NEW.`industry_id`,
                  `sale_point_id` = NEW.`sale_point_id`,
                  `contractor_id` = NEW.`contractor_id`
                WHERE `olap_documents`.`by_document` = "1" AND `olap_documents`.`id` IN (    
                  
                  SELECT `act_id` FROM `invoice_act` WHERE `invoice_id` = NEW.`id`
                );

              END IF;

            END        
        ');
    }

    private function createTriggersOnContractor()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_contractor`');

        $this->execute('
            CREATE TRIGGER `olap_documents_update_contractor`
            AFTER UPDATE ON `contractor`
            FOR EACH ROW
            BEGIN
            
              IF (NEW.`not_accounting` <> OLD.`not_accounting`) THEN
            
                UPDATE `olap_documents` 
                SET `is_accounting` = (IF (NEW.`not_accounting`, 0, 1))
                WHERE `olap_documents`.`contractor_id` = NEW.`id`;
                                
              END IF;	
              
            END
        
        ');
    }

    private function createTriggersOnSalesInvoice()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_insert_sales_invoice`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_sales_invoice`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_delete_sales_invoice`');

        $this->execute('
            CREATE TRIGGER `olap_documents_insert_sales_invoice`
            AFTER INSERT ON `sales_invoice`
            FOR EACH ROW
            BEGIN
            
              INSERT INTO `olap_documents` (`company_id`, `contractor_id`, `sale_point_id`, `industry_id`, `project_id`, `by_document`, `id`, `type`, `date`, `amount`, `is_accounting`, `invoice_expenditure_item_id`, `invoice_income_item_id`) 
              
              SELECT 
                `invoice`.`company_id`,
                `invoice`.`contractor_id`,
                `invoice`.`sale_point_id`,
                `invoice`.`industry_id`,
                `invoice`.`project_id`,
                "16" AS `by_document`, 
                `document`.`id`, 
                `document`.`type`, 
                `document`.`document_date` AS `date`, 
                IF(NEW.`status_out_id` = 5, 0, NEW.`orders_sum`) AS `amount`,
                IF (`contractor`.`not_accounting`, 0, 1) AS is_accounting,
                `invoice`.`invoice_expenditure_item_id`,
                `invoice`.`invoice_income_item_id`
              FROM `sales_invoice` `document`
              LEFT JOIN `invoice` ON `invoice`.`id` = `document`.`invoice_id`  
              LEFT JOIN `contractor` ON `contractor`.`id` = `invoice`.`contractor_id`
              WHERE 
                `document`.`id` = NEW.`id`;
              
            END        
        ');

        $this->execute('
            CREATE TRIGGER `olap_documents_update_sales_invoice`
            AFTER UPDATE ON `sales_invoice`
            FOR EACH ROW
            BEGIN
            
              IF IFNULL(NEW.`status_out_id`, 1) <> IFNULL(OLD.`status_out_id`, 1) OR NEW.`document_date` <> OLD.`document_date`
              THEN
                  UPDATE `olap_documents` 
                  SET 
                    `date` = NEW.`document_date`,
                    `amount` = IF(NEW.`status_out_id` = 5, 0, `amount`) 
                  WHERE `olap_documents`.`by_document` = "16" AND `olap_documents`.`id` = NEW.`id` LIMIT 1;
              END IF;      
                      
            END
        ');

        $this->execute('
            CREATE TRIGGER `olap_documents_delete_sales_invoice`
            AFTER DELETE ON `sales_invoice`
            FOR EACH ROW
            BEGIN
            
              DELETE FROM `olap_documents` WHERE `by_document` = "16" AND `id` = OLD.`id` LIMIT 1;
              
            END  
        ');

        // ORDER_SALES_INVOICE
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_order_sales_invoice`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_delete_order_sales_invoice`');

        $this->execute('
            CREATE TRIGGER `olap_documents_update_order_sales_invoice`
            AFTER UPDATE ON `order_sales_invoice`
            FOR EACH ROW
            BEGIN
             
              DECLARE _amount BIGINT;
              SET _amount = (
                SELECT 
                  SUM(IF(`document`.`status_out_id` = 5, 0, IF(`document`.`type` = 2, `order`.`selling_price_with_vat`, `order`.`purchase_price_with_vat`) * `order_doc`.`quantity`)) AS `amount`
                FROM `sales_invoice` `document`
                LEFT JOIN `order_sales_invoice` `order_doc` ON `document`.`id` = `order_doc`.`sales_invoice_id`
                LEFT JOIN `order` ON `order`.`id` = `order_doc`.`order_id`
                WHERE `document`.`id` = NEW.`sales_invoice_id` 
                GROUP BY `document`.`id`
              );
            
              UPDATE `olap_documents` 
              SET `amount` = _amount
              WHERE `olap_documents`.`by_document` = "16" AND `olap_documents`.`id` = NEW.`sales_invoice_id` LIMIT 1;           
              
            END   
        ');

        $this->execute('
            CREATE TRIGGER `olap_documents_delete_order_sales_invoice`
            AFTER DELETE ON `order_sales_invoice`
            FOR EACH ROW
            BEGIN
             
              DECLARE _amount BIGINT;
              SET _amount = (
                SELECT 
                  SUM(IF(`document`.`status_out_id` = 5, 0, IF(`document`.`type` = 2, `order`.`selling_price_with_vat`, `order`.`purchase_price_with_vat`) * `order_doc`.`quantity`)) AS `amount`
                FROM `sales_invoice` `document`
                LEFT JOIN `order_sales_invoice` `order_doc` ON `document`.`id` = `order_doc`.`sales_invoice_id`
                LEFT JOIN `order` ON `order`.`id` = `order_doc`.`order_id`
                WHERE `document`.`id` = OLD.`sales_invoice_id` 
                GROUP BY `document`.`id`
              );
            
              UPDATE `olap_documents` 
              SET `amount` = _amount
              WHERE `olap_documents`.`by_document` = "16" AND `olap_documents`.`id` = OLD.`sales_invoice_id` LIMIT 1;           
              
            END   
        ');
    }
}
