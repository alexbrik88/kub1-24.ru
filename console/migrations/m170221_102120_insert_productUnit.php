<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170221_102120_insert_productUnit extends Migration
{
    public $tProductUnit = 'product_unit';

    public function safeUp()
    {
        $this->insert($this->tProductUnit, [
            'id' => 13,
            'name' => 'м2',
            'code_okei' => '055',
            'object_guid' => \frontend\modules\export\models\one_c\OneCExport::generateGUID(),
        ]);
        $this->insert($this->tProductUnit, [
            'id' => 14,
            'name' => 'м3',
            'code_okei' => '113',
            'object_guid' => \frontend\modules\export\models\one_c\OneCExport::generateGUID(),
        ]);
    }

    public function safeDown()
    {
        $this->delete($this->tProductUnit, ['id' => [13, 14]]);
    }
}


