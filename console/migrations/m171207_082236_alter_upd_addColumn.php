<?php

use console\components\db\Migration;
use yii\db\Schema;

class m171207_082236_alter_upd_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('upd', 'show_service_units', $this->smallInteger(1)->defaultValue(false));

        $this->execute("
            UPDATE {{upd}}
            LEFT JOIN (
                SELECT {{order_upd}}.[[upd_id]],
                    SUM({{order_upd}}.[[empty_unit_code]]) [[unit_code]],
                    SUM({{order_upd}}.[[empty_unit_name]]) [[unit_name]],
                    SUM({{order_upd}}.[[empty_product_code]]) [[product_code]],
                    SUM({{order_upd}}.[[empty_box_type]]) [[box_type]]
                FROM {{order_upd}}
                INNER JOIN {{order}}
                    ON {{order_upd}}.[[order_id]] = {{order}}.[[id]]
                INNER JOIN {{product}} {{product}}
                    ON {{order}}.[[product_id]] = {{product}}.[[id]]
                WHERE {{product}}.[[production_type]] = 0
                GROUP BY {{order_upd}}.[[upd_id]]
            ) {{orders}} ON {{upd}}.[[id]] = {{orders}}.[[upd_id]]
            SET {{upd}}.[[show_service_units]] = 1
            WHERE {{upd}}.[[type]] = 2
                AND {{orders}}.[[upd_id]] IS NOT NULL
                AND {{orders}}.[[unit_code]] = 0
                AND {{orders}}.[[unit_name]] = 0
                AND {{orders}}.[[product_code]] = 0
                AND {{orders}}.[[box_type]] = 0
        ");
    }

    public function safeDown()
    {
        $this->dropColumn('{{%upd}}', 'show_service_units');
    }
}
