<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200731_074311_alter_table_company_append_column_invited_by_product_id extends Migration
{
    public $tableName = 'company';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'invited_by_product_id', $this->integer(11));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'invited_by_product_id');
    }
}
