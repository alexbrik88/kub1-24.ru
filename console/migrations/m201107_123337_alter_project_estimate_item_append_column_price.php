<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201107_123337_alter_project_estimate_item_append_column_price extends Migration
{
    public $tableName = 'project_estimate_item';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'price', $this->integer(11)->after('product_id'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'price');
    }

}