<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180410_211158_add_from_store_to_order extends Migration
{
    public $tableName = 'order_document';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'from_store', $this->boolean()->defaultValue(false));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'from_store');
    }
}


