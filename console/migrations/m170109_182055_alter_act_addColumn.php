<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170109_182055_alter_act_addColumn extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%act}}', 'is_original_updated_at', $this->integer()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%act}}', 'is_original_updated_at');
    }
}


