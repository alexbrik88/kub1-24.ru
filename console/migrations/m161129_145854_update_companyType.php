<?php

use console\components\db\Migration;
use yii\db\Schema;

class m161129_145854_update_companyType extends Migration
{
    public function safeUp()
    {
        $this->update('company_type', ['in_contractor' => 1], ['id' => 12]);
    }
    
    public function safeDown()
    {
        $this->update('company_type', ['in_contractor' => 0], ['id' => 12]);
    }
}


