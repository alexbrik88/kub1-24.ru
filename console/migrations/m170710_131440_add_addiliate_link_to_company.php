<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170710_131440_add_addiliate_link_to_company extends Migration
{
    public $tableName = 'company';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'affiliate_link', $this->string()->defaultValue(null));

        $this->addColumn($this->tableName, 'invited_by_company_id', $this->integer()->defaultValue(null));
        $this->addForeignKey($this->tableName . '_invited_by_company_id', $this->tableName, 'invited_by_company_id', $this->tableName, 'id', 'RESTRICT', 'CASCADE');

        $this->addColumn($this->tableName, 'affiliate_sum', $this->integer()->defaultValue(0));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'affiliate_link');

        $this->dropForeignKey($this->tableName . '_invited_by_company_id', $this->tableName);
        $this->dropColumn($this->tableName, 'invited_by_company_id');

        $this->dropColumn($this->tableName, 'affiliate_sum');
    }
}


