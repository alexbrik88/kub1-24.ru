<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210714_134106_alter_crm_task_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%crm_task}}', 'contact_id', $this->bigInteger());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%crm_task}}', 'contact_id');
    }
}
