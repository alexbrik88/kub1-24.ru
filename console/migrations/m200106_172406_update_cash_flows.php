<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200106_172406_update_cash_flows extends Migration
{
    public function safeUp()
    {
        Yii::$app->db->createCommand('
          UPDATE cash_bank_flows 
          SET is_taxable = 0
          WHERE contractor_id = "balance"')->execute();

        Yii::$app->db->createCommand('
          UPDATE cash_order_flows 
          SET is_taxable = 0
          WHERE contractor_id = "balance"')->execute();
    }

    public function safeDown()
    {
        //
    }
}
