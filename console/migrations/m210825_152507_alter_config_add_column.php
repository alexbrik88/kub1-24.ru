<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210825_152507_alter_config_add_column extends Migration
{
    public $table = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->table, 'payments_request_date', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'payments_request_date');
    }
}
