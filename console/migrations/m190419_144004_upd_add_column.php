<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190419_144004_upd_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('upd', 'add_stamp', $this->boolean()->notNull()->defaultValue(false));
    }

    public function safeDown()
    {
        $this->dropColumn('upd', 'add_stamp');
    }
}
