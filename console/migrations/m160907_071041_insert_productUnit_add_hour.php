<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160907_071041_insert_productUnit_add_hour extends Migration
{
    public $tProductUnit = 'product_unit';

    public function safeUp()
    {
        $this->insert($this->tProductUnit, [
            'id' => 11,
            'name' => 'час',
            'code_okei' => '356',
            'object_guid' => '2FD7F910-1E3D-9B1C-FD05-FDB652F5B80F',
        ]);
    }

    public function safeDown()
    {
        $this->delete($this->tProductUnit, 'id = 12');
    }
}


