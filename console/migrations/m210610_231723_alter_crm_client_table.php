<?php

use yii\db\Migration;

class m210610_231723_alter_crm_client_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%crm_client}}';

    /**
     * @inheritDoc
     */
    public function up(): void
    {
        $this->addColumn(self::TABLE_NAME, 'task_id', $this->bigInteger()->null()->after('client_status_id'));
        $this->addColumn(self::TABLE_NAME, 'event_type', $this->tinyInteger()->null()->after('task_id'));
        $this->createIndex('ck_task_id', self::TABLE_NAME, ['task_id']);
        $this->createIndex('ck_event_type', self::TABLE_NAME, ['event_type']);
        $this->addForeignKey(
            'fk_crm_client__crm_task',
            self::TABLE_NAME,
            ['task_id'],
            '{{%crm_task}}',
            ['task_id'],
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * @inheritDoc
     */
    public function down(): void
    {
        $this->dropForeignKey('fk_crm_client__crm_task', self::TABLE_NAME);
        $this->dropColumn(self::TABLE_NAME, 'task_id');
        $this->dropColumn(self::TABLE_NAME, 'event_type');
    }
}
