<?php

use yii\db\Migration;

/**
 * Handles the creation of table `pattern_step`.
 */
class m180301_103719_create_pattern_step_table extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('pattern_step', [
            'pattern_id' => $this->integer()->notNull(),
            'step' => $this->integer()->notNull(),
        ]);
        $this->addPrimaryKey('PRIMARY_KEY', 'pattern_step', ['pattern_id', 'step']);
        $this->addForeignKey(
            'FK_patternStep_pattern',
            'pattern_step',
            'pattern_id',
            'pattern',
            'id',
            'CASCADE'
        );
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('pattern_step');
    }
}
