<?php

use console\components\db\Migration;
use yii\db\Schema;

class m191203_162620_alter_cashbox_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cashbox', 'ofd_type_id', $this->integer());
        $this->addForeignKey("FK_cashbox_ofd_type", 'cashbox', 'ofd_type_id', 'ofd_type', 'id', 'SET NULL');
    }

    public function safeDown()
    {
        $this->dropForeignKey("FK_cashbox_ofd_type", 'cashbox');
        $this->dropColumn('cashbox', 'ofd_type_id');
    }
}