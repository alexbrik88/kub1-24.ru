<?php

use console\components\db\Migration;
use yii\db\Schema;
use common\models\document\Upd;

class m180906_133326_update_invoice_can_add_upd extends Migration
{
    public $tableName = 'invoice';

    public function safeUp()
    {
        foreach (Upd::find()->select('invoice_id')
                     ->groupBy('invoice_id')
                     ->column() as $invoiceID) {
            Yii::$app->db->createCommand('
              UPDATE {{invoice}}
              SET {{invoice}}.[[can_add_upd]] = (
                      SELECT IFNULL(SUM({{order}}.[[quantity]]), 0) FROM {{order}}
                      WHERE {{order}}.[[invoice_id]] = {{invoice}}.[[id]]
                  ) > (
                      SELECT IFNULL(SUM({{order_upd}}.[[quantity]]), 0) FROM {{order_upd}}
                      INNER JOIN {{upd}} ON {{order_upd}}.[[upd_id]] = {{upd}}.[[id]]
                      WHERE {{upd}}.[[invoice_id]] = {{invoice}}.[[id]]
                  ),
                  {{invoice}}.[[has_upd]] = EXISTS(
                      SELECT {{upd}}.[[id]] FROM {{upd}} WHERE {{upd}}.[[invoice_id]] = {{invoice}}.[[id]]
                  )
              WHERE {{invoice}}.[[id]] = :id
            ', [
                ':id' => $invoiceID,
            ])->execute();
        }
    }

    public function safeDown()
    {
        echo "This migration not rolled back";
    }
}


