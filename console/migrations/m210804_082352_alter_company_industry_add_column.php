<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210804_082352_alter_company_industry_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('company_industry', 'is_main', $this->tinyInteger()->defaultValue(0));
        $this->addColumn('company_industry', 'sort', $this->integer()->defaultValue(1));
        $this->execute(' UPDATE `company_industry` SET `is_main` = 1 WHERE `id` IN (SELECT MIN(`id`) FROM `company_industry` GROUP BY `company_id`)');
        $this->execute(' UPDATE `company_industry` SET `sort` = 2 WHERE `is_main` = 0');
    }

    public function safeDown()
    {
        $this->dropColumn('company_industry', 'is_main');
        $this->dropColumn('company_industry', 'sort');
    }
}