<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%bitrix24_widget_company}}`.
 */
class m220113_083048_create_bitrix24_widget_company_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%bitrix24_widget_company}}', [
            'client_id' => $this->integer()->notNull()->comment('ID клиента, из которого сделан запрос'),
            'company_id' => $this->integer()->notNull()->comment('ID компании в КУБ'),
            'created_at' => $this->integer()->notNull()->comment('Timestamp, когда подключена компания'),
        ]);

        $this->addPrimaryKey('PRIMARY_KEY', '{{%bitrix24_widget_company}}', ['client_id', 'company_id']);
        $this->createIndex('company_id', '{{%bitrix24_widget_company}}', 'company_id');
        $this->addForeignKey('fk_bitrix24_widget_company__client_id', '{{%bitrix24_widget_company}}', 'client_id', '{{%bitrix24_client}}', 'id');
        $this->addForeignKey('fk_bitrix24_widget_company__company_id', '{{%bitrix24_widget_company}}', 'company_id', '{{%company}}', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%bitrix24_widget_company}}');
    }
}
