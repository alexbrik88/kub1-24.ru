<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170713_134434_create_reward_request extends Migration
{
    public $tableName = 'reward_request';

    public function safeUp()
    {
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'created_at' => $this->integer()->notNull(),
            'company_id' => $this->integer()->notNull(),
            'sum' => $this->integer()->notNull(),
            'type' => $this->integer()->defaultValue(1),
            'status' => $this->integer()->defaultValue(1),
        ]);

        $this->addForeignKey($this->tableName . '_company_id', $this->tableName, 'company_id', 'company', 'id', 'RESTRICT', 'CASCADE');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey($this->tableName . '_company_id', $this->tableName);

        $this->dropTable($this->tableName);
    }
}


