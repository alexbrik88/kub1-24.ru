<?php

use console\components\db\Migration;
use yii\db\Schema;

class m181014_155247_create_table_agreement_new_status extends Migration
{
    /**
     * @inheritdoc
     */
    public function up()
    {
        $this->createTable('agreement_new_status', [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);

        $this->batchInsert('agreement_new_status', ['name'], [
            ['Создан'],
            ['Распечатан'],
            ['Передан'],
            ['Подписан'],
			['Отменён']
        ]);
    }

    /**
     * @inheritdoc
     */
    public function down()
    {
        $this->dropTable('agreement_new_status');
    }
}


