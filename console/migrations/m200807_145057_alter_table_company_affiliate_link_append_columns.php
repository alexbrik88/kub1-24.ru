<?php

use common\models\service\ServiceModule;
use console\components\db\Migration;
use yii\db\Schema;

class m200807_145057_alter_table_company_affiliate_link_append_columns extends Migration
{
    public $tableName = 'company_affiliate_link';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'module_id', $this->integer(11));

        $links = (new \yii\db\Query())
            ->from($this->tableName)
            ->all();

        $moduleIds = array_flip(ServiceModule::$serviceModuleArray);

        foreach ($links as $link) {
            $this->update($this->tableName,
                ['module_id' => $moduleIds[$link['service_module_id']]],
                ['id' => $link['id']]);
        }
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'module_id');
    }
}
