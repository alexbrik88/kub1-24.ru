<?php

use yii\db\Migration;

class m210601_005641_alter_user_config_table extends Migration
{
    /** @var string  */
    private const TABLE_NAME = '{{%user_config}}';

    /**
     * @inheritDoc
     */
    public function safeUp(): void
    {
        $this->addColumn(self::TABLE_NAME, 'crm_task_time', $this->boolean()->notNull()->defaultValue(true));
    }

    /**
     * @inheritDoc
     */
    public function safeDown(): void
    {
        $this->dropColumn(self::TABLE_NAME, 'crm_task_time');
    }
}
