<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190624_164400_alter_contractor_add_columns extends Migration
{
    public function up()
    {
        $this->addColumn('contractor', 'agent_start_date', $this->date());
        $this->addColumn('contractor', 'agent_payment_sum', $this->decimal(20, 4));
        $this->addColumn('contractor', 'agent_payment_type_id', $this->integer());

        $this->addForeignKey('FK_agent_to_payment_type', 'contractor', 'agent_payment_type_id', 'contractor_agent_payment_type', 'id', 'SET NULL');
    }

    public function down()
    {
        $this->dropForeignKey('FK_agent_to_payment_type', 'contractor');
        $this->dropColumn('contractor', 'agent_start_date');
        $this->dropColumn('contractor', 'agent_payment_sum');
        $this->dropColumn('contractor', 'agent_payment_type_id');
    }
}
