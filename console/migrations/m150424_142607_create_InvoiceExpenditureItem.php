<?php

use yii\db\Schema;
use yii\db\Migration;

class m150424_142607_create_InvoiceExpenditureItem extends Migration
{
    public function up()
    {
        $this->createTable('invoice_expenditure_item', [
            'id' => Schema::TYPE_PK,
            'name' => Schema::TYPE_STRING . '(45) NOT NULL',
        ]);
        $this->batchInsert('invoice_expenditure_item', ['id', 'name'], [
            [1, 'Товар'],
            [2, 'Аренда'],
            [3, 'Зарплата'],
            [4, 'Налоги на ЗП'],
            [5, 'Налоги'],
            [6, 'Интернет'],
            [7, 'Телефония'],
            [8, 'Хозяйственные нужды'],
            [9, 'Реклама'],
            [10, 'Мебель'],
            [11, 'Оргтехника'],
            [12, 'Прочее'],
        ]);

        $this->addColumn('invoice', 'invoice_expenditure_item_id', Schema::TYPE_INTEGER . ' NULL');
        $this->addForeignKey('invoice_to_invoice_expenditure_item', 'invoice', 'invoice_expenditure_item_id', 'invoice_expenditure_item', 'id');
    }

    public function down()
    {
        $this->dropForeignKey('invoice_to_invoice_expenditure_item', 'invoice');
        $this->dropColumn('invoice', 'invoice_expenditure_item_id');
        $this->dropTable('invoice_expenditure_item');
    }
}
