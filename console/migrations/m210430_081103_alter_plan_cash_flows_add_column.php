<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210430_081103_alter_plan_cash_flows_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('plan_cash_flows', 'credit_id', $this->bigInteger(20));
        $this->addColumn('plan_cash_foreign_currency_flows', 'credit_id', $this->bigInteger(20));

        $this->addForeignKey('FK_plan_cash_flows_to_credit', 'plan_cash_flows', 'credit_id', 'credit', 'credit_id', 'CASCADE');
        $this->addForeignKey('FK_plan_cash_foreign_currency_flows_to_credit', 'plan_cash_foreign_currency_flows', 'credit_id', 'credit', 'credit_id', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_plan_cash_flows_to_credit', 'plan_cash_flows');
        $this->dropForeignKey('FK_plan_cash_foreign_currency_flows_to_credit', 'plan_cash_foreign_currency_flows');

        $this->dropColumn('plan_cash_flows', 'credit_id');
        $this->dropColumn('plan_cash_foreign_currency_flows', 'credit_id');
    }
}
