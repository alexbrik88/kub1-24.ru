<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180123_152327_add_push_nitification_to_employee extends Migration
{
    public $tableName = 'employee';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'push_notification_new_message', $this->boolean()->defaultValue(false));
        $this->addColumn($this->tableName, 'push_notification_create_closest_document', $this->boolean()->defaultValue(false));
        $this->addColumn($this->tableName, 'push_notification_overdue_invoice', $this->boolean()->defaultValue(false));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'push_notification_new_message');
        $this->dropColumn($this->tableName, 'push_notification_create_closest_document');
        $this->dropColumn($this->tableName, 'push_notification_overdue_invoice');
    }
}


