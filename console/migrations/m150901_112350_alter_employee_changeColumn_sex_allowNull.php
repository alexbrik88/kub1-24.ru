<?php

use yii\db\Schema;
use yii\db\Migration;

class m150901_112350_alter_employee_changeColumn_sex_allowNull extends Migration
{
    public $tableEmployee = 'employee';

    public function safeUp()
    {
        $this->alterColumn($this->tableEmployee, 'sex', $this->boolean());
    }
    
    public function safeDown()
    {
        $this->alterColumn($this->tableEmployee, 'sex', $this->boolean()->notNull());
    }
}
