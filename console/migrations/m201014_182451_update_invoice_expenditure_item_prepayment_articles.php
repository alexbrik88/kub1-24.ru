<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201014_182451_update_invoice_expenditure_item_prepayment_articles extends Migration
{
    public function safeUp()
    {
        $this->execute("
            UPDATE `expense_item_flow_of_funds` 
            SET `is_prepayment` = 0 
            WHERE `expense_item_id` = 52
        ");

        $this->execute("
            UPDATE `invoice_expenditure_item` 
            SET `is_prepayment` = 0 
            WHERE `id` = 52; 
        ");
    }

    public function safeDown()
    {
        $this->execute("
            UPDATE `expense_item_flow_of_funds` 
            SET `is_prepayment` = 1 
            WHERE `expense_item_id` = 52
        ");

        $this->execute("
            UPDATE `invoice_expenditure_item` 
            SET `is_prepayment` = 1 
            WHERE `id` = 52; 
        ");
    }
}
