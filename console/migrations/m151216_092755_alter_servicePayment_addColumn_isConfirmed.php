<?php

use console\components\db\Migration;
use yii\db\Schema;

class m151216_092755_alter_servicePayment_addColumn_isConfirmed extends Migration
{
    public $tPayment = 'service_payment';

    public function safeUp()
    {
        $this->addColumn($this->tPayment, 'is_confirmed', $this->boolean()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tPayment, 'is_confirmed');
    }
}
