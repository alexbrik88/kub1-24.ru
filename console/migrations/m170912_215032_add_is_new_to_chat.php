<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170912_215032_add_is_new_to_chat extends Migration
{
    public $tableName = 'chat';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'is_new', $this->boolean()->defaultValue(false));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'is_new');
    }
}


