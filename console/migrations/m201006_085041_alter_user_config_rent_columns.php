<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201006_085041_alter_user_config_rent_columns extends Migration
{
    public $tableName = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'rent_categories', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'rent_statuses', $this->boolean()->notNull()->defaultValue(false));
        $this->addColumn($this->tableName, 'rent_responsible', $this->boolean()->notNull()->defaultValue(false));

        $this->addColumn($this->tableName, 'table_rent_list', $this->boolean()->notNull()->defaultValue(true));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'rent_categories');
        $this->dropColumn($this->tableName, 'rent_statuses');
        $this->dropColumn($this->tableName, 'rent_responsible');

        $this->dropColumn($this->tableName, 'table_rent_list');
    }
}
