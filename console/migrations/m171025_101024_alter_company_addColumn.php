<?php

use console\components\db\Migration;
use yii\db\Schema;

class m171025_101024_alter_company_addColumn extends Migration
{
    public $tableName = 'company';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'created_by', $this->integer()->defaultValue(null)->after('created_at'));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'created_by');
    }
}
