<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211014_183506_urotdel_visits extends Migration
{
    public function safeUp()
    {
        $this->createTable('urotdel_visits', [
            'id' => $this->primaryKey(11)->unsigned(),
            'company_id' => $this->db->getTableSchema('company')->getColumn('id')->dbType . ' NOT NULL',
            'duration' => $this->integer(11)->unsigned()->notNull(),
            'created_at' => $this->integer(11)->unsigned()->notNull(),
        ]);

        $this->addForeignKey('FK_urotdel_visits_to_company', 'urotdel_visits', 'company_id', 'company', 'id', 'CASCADE', 'CASCADE');
    }

    public function safeDown()
    {
        $this->dropTable('urotdel_visits');
    }
}
