<?php

use yii\db\Migration;

class m210325_023100_alter_auto_import_settings_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%auto_import_settings}}';

    /** @var string[] */
    private const UK_COLUMNS = ['company_id', 'command_type', 'identifier'];

    /**
     * @inheritDoc
     */
    public function up()
    {
        $this->dropForeignKey('FK_autoload_moneta_to_company', self::TABLE_NAME);
        $this->alterColumn(self::TABLE_NAME, 'company_id', $this->integer()->notNull());
        $this->dropPrimaryKey('PRIMARY', self::TABLE_NAME);

        $timestampType = $this->timestamp()->notNull()->defaultExpression('CURRENT_TIMESTAMP');

        $this->addColumn(self::TABLE_NAME, 'id', $this->bigPrimaryKey()->notNull()->first());
        $this->addColumn(self::TABLE_NAME, 'created_at', $timestampType);
        $this->addColumn(self::TABLE_NAME, 'updated_at', $timestampType);

        $this->createIndex('ck_company_id', self::TABLE_NAME, ['company_id']);
        $this->createIndex('uk_auto_import_settings', self::TABLE_NAME, self::UK_COLUMNS, true);
        $this->addForeignKey(
            'auto_import_settings_company_id__company_id',
            self::TABLE_NAME,
            ['company_id'],
            '{{%company}}',
            ['id'],
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritDoc
     */
    public function down()
    {
        return false;
    }
}
