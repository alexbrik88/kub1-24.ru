<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200219_104823_alter_user_config_add_column extends Migration
{
    public $table = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->table, 'table_view_product_turnover', $this->integer()->notNull()->defaultValue(0));
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'table_view_product_turnover');
    }
}
