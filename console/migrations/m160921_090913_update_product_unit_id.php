<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160921_090913_update_product_unit_id extends Migration
{
    public $tProductUnit = 'product_unit';
    public $tProduct = 'product';
    public $tOrder = 'order';

    public function safeUp()
    {
    //    $this->dropForeignKey($this->tProduct . '_to_product_unit', $this->tProduct);
    //    $this->update($this->tProduct, ['product_unit_id' => 10], ['product_unit_id' => 11]);
    //    $this->update($this->tProduct, ['product_unit_id' => 11], ['product_unit_id' => 12]);
//
    //    $this->dropForeignKey($this->tOrder . '_to_product_unit', $this->tOrder);
    //    $this->update($this->tOrder, ['unit_id' => 10], ['unit_id' => 11]);
    //    $this->update($this->tOrder, ['unit_id' => 11], ['unit_id' => 12]);
//
    //    $this->update($this->tProductUnit, ['id' => 10], ['id' => 11]);
    //    $this->update($this->tProductUnit, ['id' => 11], ['id' => 12]);
//
    //    $this->addForeignKey($this->tProduct . '_to_product_unit', $this->tProduct, 'product_unit_id', $this->tProductUnit, 'id', 'RESTRICT', 'CASCADE');
    //    $this->addForeignKey($this->tOrder . '_to_product_unit', $this->tOrder, 'unit_id', $this->tProductUnit, 'id', 'RESTRICT', 'CASCADE');
    }

    public function safeDown()
    {
        echo 'This migration not rolled back';
    }
}


