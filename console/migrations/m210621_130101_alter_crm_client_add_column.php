<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210621_130101_alter_crm_client_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%crm_client}}', 'site_url', $this->text());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%crm_client}}', 'site_url');
    }
}
