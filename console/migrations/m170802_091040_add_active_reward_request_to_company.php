<?php

use console\components\db\Migration;
use yii\db\Schema;
use common\models\service\RewardRequest;

class m170802_091040_add_active_reward_request_to_company extends Migration
{
    public $tableName = 'company';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'active_reward_requests', $this->integer()->defaultValue(0));
        /* @var $rewardRequest RewardRequest */
        foreach (RewardRequest::find()->all() as $rewardRequest) {
            if ($rewardRequest->status == RewardRequest::STATUS_DEFAULT) {
                $company = $rewardRequest->company;
                $company->active_reward_requests += 1;
                $company->save(true, ['active_reward_requests']);
            }
        }
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'active_reward_requests');
    }
}


