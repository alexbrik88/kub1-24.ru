<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180402_082713_add_columns_to_store_user extends Migration
{
    public $tStoreUser = 'store_user';
    public $tCompany = 'company';

    public function safeUp()
    {
        $this->addColumn($this->tStoreUser, 'company_id', $this->integer()->defaultValue(null));
        $this->addColumn($this->tStoreUser, 'login_password', $this->string()->defaultValue(null));
        $this->addColumn($this->tStoreUser, 'login_key', $this->string()->defaultValue(null));

        $this->addForeignKey($this->tStoreUser . '_company_id', $this->tStoreUser, 'company_id', $this->tCompany, 'id', 'RESTRICT', 'CASCADE');
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tStoreUser, 'company_id');
        $this->dropColumn($this->tStoreUser, 'login_password');
        $this->dropColumn($this->tStoreUser, 'login_key');
    }
}


