<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200911_093749_trigger_product_turnover__order__after_update extends Migration
{
    /**
     * @var string
     */
    public $dropSql = '
        DROP TRIGGER IF EXISTS `product_turnover__order__after_update`;
    ';

    public function safeUp()
    {
        /**
         * Delet if exists
         */
        $this->execute($this->dropSql);

        /**
         * `product` triggers
         */
        $this->execute('
CREATE TRIGGER `product_turnover__order__after_update` AFTER UPDATE ON `order`
FOR EACH ROW
BEGIN
    IF
        IFNULL(NEW.`purchase_price_with_vat`, 0) <> IFNULL(OLD.`purchase_price_with_vat`, 0)
        OR
        IFNULL(NEW.`selling_price_with_vat`, 0) <> IFNULL(OLD.`selling_price_with_vat`, 0)
    THEN
        UPDATE `product_turnover`
        LEFT JOIN `invoice` ON `invoice`.`id` = NEW.`invoice_id`
        SET `product_turnover`.`price_one` = (@price:=IFNULL(IF(`invoice`.`type` = 1, NEW.`purchase_price_with_vat`, NEW.`selling_price_with_vat`), 0)),
            `product_turnover`.`total_amount` = (@total:=ROUND(@price * `product_turnover`.`quantity`, IF(`invoice`.`price_precision` = 4, 2, 0))),
            `product_turnover`.`margin` = @total-`product_turnover`.`purchase_amount`
        WHERE `product_turnover`.`order_id` = NEW.`id`;
    END IF;
END;
        ');
    }

    public function safeDown()
    {
        /**
         * Delet if exists
         */
        $this->execute($this->dropSql);

        /**
         * `product` triggers
         */
        $this->execute('
CREATE TRIGGER `product_turnover__order__after_update` AFTER UPDATE ON `order`
FOR EACH ROW
BEGIN
    IF
        IFNULL(NEW.`purchase_price_with_vat`, 0) <> IFNULL(OLD.`purchase_price_with_vat`, 0)
        OR
        IFNULL(NEW.`selling_price_with_vat`, 0) <> IFNULL(OLD.`selling_price_with_vat`, 0)
    THEN
        UPDATE `product_turnover`
        LEFT JOIN `invoice` ON `invoice`.`id` = NEW.`invoice_id`
        SET `product_turnover`.`price` = (@price:=IFNULL(IF(`invoice`.`type` = 1, NEW.`purchase_price_with_vat`, NEW.`selling_price_with_vat`), 0)),
            `product_turnover`.`total_amount` = (@total:=ROUND(@price * `product_turnover`.`quantity`, IF(`invoice`.`price_precision` = 4, 2, 0))),
            `product_turnover`.`margin` = @total-`product_turnover`.`purchase_amount`
        WHERE `product_turnover`.`order_id` = NEW.`id`;
    END IF;
END;
        ');
    }
}
