<?php

use yii\db\Schema;
use yii\db\Migration;

class m150928_094244_insert_productUnit_add_ton extends Migration
{
    public $tProductUnit = 'product_unit';

    public function safeUp()
    {
        $this->insert($this->tProductUnit, [
            'id' => 9,
            'name' => 'Тонна',
            'code_okei' => '168',
        ]);
    }

    public function safeDown()
    {
        $this->delete($this->tProductUnit, 'id = 9');
    }
}
