<?php

use console\components\db\Migration;
use yii\db\Query;
use yii\db\Schema;

class m190919_060935_update_document_to_details_file_relations extends Migration
{
    private static $tableList = [
        'invoice',
        'act',
        'packing_list',
        'invoice_facture',
        'upd',
        'agreement',
        'proxy',
        'agent_report',
    ];
    private static $hasToMany = [
        'act',
        'invoice_facture',
        'upd',
    ];
    private static $hasCompanyId = [
        'invoice',
        'agreement',
        'agent_report',
    ];
    private static $typeList = [
        2 => 'print_id',
        3 => 'chief_signature_id',
        4 => 'chief_accountant_signature_id',
    ];

    public function safeUp()
    {
        foreach (self::$typeList as $typeId => $fieldName) {
            $offset = 0;
            $limit = 1000;
            do {
                $fileRows = (new Query)->select([
                    'id',
                    'company_id',
                ])->from('{{%company_details_file}}')->where([
                    'type_id' => $typeId,
                    'status_id' => 1,
                ])->orderBy('id')->offset($offset)->limit($limit)->all();
                $offset += $limit;

                foreach ($fileRows as $key => $row) {
                    foreach (self::$tableList as $tableName) {
                        if (in_array($tableName, self::$hasCompanyId)) {
                            $this->update("{{%{$tableName}}}", [
                                $fieldName => $row['id'],
                            ], [
                                'company_id' => $row['company_id'],
                            ]);
                        } elseif (in_array($tableName, self::$hasToMany)) {
                            $this->execute("
                                UPDATE {{%{$tableName}}} AS {{document}}
                                LEFT JOIN {{%invoice_{$tableName}}} AS {{link}}
                                     ON {{link}}.[[{$tableName}_id]] = {{document}}.[[id]]
                                LEFT JOIN {{%invoice}} ON {{link}}.[[invoice_id]] = {{%invoice}}.[[id]]
                                SET {{document}}.[[{$fieldName}]] = :id
                                WHERE {{%invoice}}.[[company_id]] = :company_id
                            ", [
                                ':id' => $row['id'],
                                ':company_id' => $row['company_id'],
                            ]);
                        } else {
                            $this->execute("
                                UPDATE {{%{$tableName}}} AS {{document}}
                                LEFT JOIN {{%invoice}} ON {{document}}.[[invoice_id]] = {{%invoice}}.[[id]]
                                SET {{document}}.[[{$fieldName}]] = :id
                                WHERE {{%invoice}}.[[company_id]] = :company_id
                            ", [
                                ':id' => $row['id'],
                                ':company_id' => $row['company_id'],
                            ]);
                        }
                    }
                }
            } while ($fileRows);
        }
    }

    public function safeDown()
    {
        foreach (self::$tableList as $tableName) {
            $this->update("{{%{$tableName}}}", [
                'print_id' => null,
                'chief_signature_id' => null,
                'chief_accountant_signature_id' => null,
            ]);
        }
    }
}
