<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201112_093652_create_log_document_email extends Migration
{
    public function safeUp()
    {
        $this->createTable('log_document_email', [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'email' => $this->string(255)->notNull(),
            'model_id' => $this->integer()->notNull(),
            'model_name' => $this->string(255)->notNull(),
            'created_at' => $this->integer()->notNull(),
        ]);

        $this->createIndex('model_idx', 'log_document_email', [
            'model_name', 'model_id',
        ]);

        $this->addForeignKey('FK_log_document_email_to_company', 'log_document_email', 'company_id', 'company', 'id');
    }

    public function safeDown()
    {
        $this->dropTable('log_document_email');
    }
}
