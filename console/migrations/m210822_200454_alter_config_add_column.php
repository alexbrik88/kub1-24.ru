<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210822_200454_alter_config_add_column extends Migration
{
    public $table = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->table, 'table_view_payments_request', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table, 'payments_request_payments_count', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 1');
        $this->addColumn($this->table, 'payments_request_payments_status', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 1');
        $this->addColumn($this->table, 'payments_request_author', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 1');

        $this->addColumn($this->table, 'payments_request_order_invoice', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table, 'payments_request_order_project', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table, 'payments_request_order_sale_point', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table, 'payments_request_order_industry', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table, 'payments_request_order_priority', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'table_view_payments_request');
        $this->dropColumn($this->table, 'payments_request_payments_count');
        $this->dropColumn($this->table, 'payments_request_payments_status');
        $this->dropColumn($this->table, 'payments_request_author');

        $this->dropColumn($this->table, 'payments_request_order_invoice');
        $this->dropColumn($this->table, 'payments_request_order_project');
        $this->dropColumn($this->table, 'payments_request_order_sale_point');
        $this->dropColumn($this->table, 'payments_request_order_industry');
        $this->dropColumn($this->table, 'payments_request_order_priority');
    }
}
