<?php
/* 20-272 */
use common\models\product\Product;
use console\components\db\Migration;
use yii\db\Expression;
use yii\db\Query;
use yii\db\Schema;

class m200817_044354_create_olap_documents extends Migration
{
    // Created table
    const TABLE_OLAP_DOCS = 'olap_documents';

    const TABLE_PACKING_LIST = 'packing_list';
    const TABLE_UPD = 'upd';
    const TABLE_ACT = 'act';
    const TABLE_INVOICE = 'invoice';
    const TABLE_PRODUCT = 'product';
    const TABLE_PRODUCT_INITIAL_BALANCE = 'product_initial_balance';
    const REF_TABLE_INVOICE_UPD = 'invoice_upd';
    const REF_TABLE_INVOICE_ACT = 'invoice_act';

    const DOCUMENT_ACT = 1;
    const DOCUMENT_PACKING_LIST = 4;
    const DOCUMENT_UPD = 8;
    const DOCUMENT_INITIAL_BALANCE = 0;

    public function safeUp()
    {
        $table = self::TABLE_OLAP_DOCS;
        $idx = "idx_{$table}";

        $query = $this->getOlapTableQuery();

        $this->execute("DROP TABLE IF EXISTS {$table}");
        $this->execute("CREATE TABLE IF NOT EXISTS {$table} (
            `company_id` INT NOT NULL,
            `contractor_id` INT DEFAULT NULL,
            `by_document` TINYINT NOT NULL,
            `id` INT NOT NULL,
            `type` TINYINT NOT NULL,
            `date` DATE NOT NULL,
            `amount` BIGINT DEFAULT 0,
            `amount_in_price_for_buy` BIGINT DEFAULT 0,
            `product_amount_in_price_for_buy` BIGINT DEFAULT 0,
            `is_accounting` TINYINT DEFAULT 1,
            `invoice_expenditure_item_id` INT
        )");

        $this->execute("INSERT INTO {$table} SELECT * FROM({$query->createCommand()->rawSql}) {{s}};");
        $this->execute("CREATE INDEX {$idx}_date ON {$table} (`company_id`, `date`);");
        $this->execute("CREATE INDEX {$idx}_contractor ON {$table} (`contractor_id`);");
        $this->execute("ALTER TABLE {$table} ADD PRIMARY KEY (`id`, `by_document`);");

        $this->createTriggersOnProductInitialBalance();
        $this->createTriggersOnProductStore();
        $this->createTriggersOnOrderPackingList();
        $this->createTriggersOnPackingList();
        $this->createTriggersOnOrderUpd();
        $this->createTriggersOnUpd();
        $this->createTriggersOnOrderAct();
        $this->createTriggersOnAct();        
        $this->createTriggersOnProduct();
        $this->createTriggersOnInvoice();
        $this->createTriggersOnContractor();
    }

    public function safeDown()
    {
        $table = self::TABLE_OLAP_DOCS;
        $this->execute("DROP TABLE IF EXISTS {$table}");
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_product_initial_balance`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_insert_product_initial_balance`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_product_store`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_insert_product_store`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_order_packing_list`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_insert_order_packing_list`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_order_upd`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_insert_order_upd`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_packing_list`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_delete_packing_list`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_upd`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_delete_upd`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_act`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_delete_act`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_product`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_invoice`;');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_contractor`;');
    }

    private function getOlapTableQuery()
    {
        $select = [
            'invoice.company_id',
            'invoice.contractor_id',
            'by_document',
            'document.id',
            'document.type',
            'document.document_date AS date',
            'SUM( IF(document.type = 2, `order_main`.selling_price_with_vat, `order_main`.purchase_price_with_vat) * `order`.quantity) AS amount',
            'SUM(`order`.`quantity` * `product`.`price_for_buy_with_nds`) AS amount_in_price_for_buy',
            'SUM( IF(product.production_type = 1, `order`.`quantity` * `product`.`price_for_buy_with_nds`, 0)) AS product_amount_in_price_for_buy',
            'IF (contractor.not_accounting, 0, 1) AS is_accounting',
            'invoice.invoice_expenditure_item_id',
        ];

        $selectInitialBalance = new Expression('
            product.company_id,
            NULL AS contractor_id,
            "0" AS by_document,
            product.id AS id,
            1 AS type,
            IFNULL(pib.date, "'.date('Y-m-d').'") AS date,
            0 AS amount,
            SUM(store.initial_quantity * product.price_for_buy_with_nds) AS amount_in_price_for_buy,
            SUM(store.initial_quantity * product.price_for_buy_with_nds) AS product_amount_in_price_for_buy,
            1 AS is_accounting,
            NULL AS invoice_expenditure_item_id
        ');

        // оборот по товарным накладным
        $query1 = (new Query)
            ->select($select)
            ->addSelect(['by_document' => new Expression('"' . self::DOCUMENT_PACKING_LIST . '"')])
            ->from(['order' => 'order_packing_list'])
            ->leftJoin(['document' => self::TABLE_PACKING_LIST], '{{document}}.[[id]] = {{order}}.[[packing_list_id]]')
            ->leftJoin(['invoice' => self::TABLE_INVOICE], '{{invoice}}.[[id]] = {{document}}.[[invoice_id]]')
            ->leftJoin(['product' => self::TABLE_PRODUCT], '{{product}}.[[id]] = {{order}}.[[product_id]]')
            ->leftJoin(['order_main' => 'order'], '{{order_main}}.[[id]] = {{order}}.[[order_id]]')
            ->leftJoin(['contractor' => 'contractor'], 'contractor.id = invoice.contractor_id')            
            ->andWhere([
                //'invoice.company_id' => $this->company->id,
                'invoice.is_deleted' => false,
                'invoice.invoice_status_id' => [1,2,3,4,6,7,8], // valid statuses
            ])
            ->andWhere([
                'or',
                ['document.status_out_id' => [1,2,3,4]], // valid statuses
                ['document.status_out_id' => null],
            ])
            ->groupBy('document.id');

        // оборот по УПД
        $query2 = (new Query)
            ->select($select)
            ->addSelect(['by_document' => new Expression('"' . self::DOCUMENT_UPD . '"')])
            ->from(['order' => 'order_upd'])
            ->leftJoin(['document' => self::TABLE_UPD], '{{document}}.[[id]] = {{order}}.[[upd_id]]')
            ->leftJoin(['invoice_upd' => self::REF_TABLE_INVOICE_UPD], '{{invoice_upd}}.[[upd_id]] = {{document}}.[[id]]')
            ->leftJoin(['invoice' => self::TABLE_INVOICE], '{{invoice}}.[[id]] = {{invoice_upd}}.[[invoice_id]]')
            ->leftJoin(['product' => self::TABLE_PRODUCT], '{{product}}.[[id]] = {{order}}.[[product_id]]')
            ->leftJoin(['order_main' => 'order'], '{{order_main}}.[[id]] = {{order}}.[[order_id]]')
            ->leftJoin(['contractor' => 'contractor'], 'contractor.id = invoice.contractor_id')
            ->andWhere([
                //'invoice.company_id' => $this->company->id,
                'invoice.is_deleted' => false,
                'invoice.invoice_status_id' => [1,2,3,4,6,7,8],
            ])
            ->andWhere([
                'or',
                ['document.status_out_id' => [1,2,3,4]],
                ['document.status_out_id' => null],
            ])
            ->groupBy('invoice.id, document.id');

        // оборот по Актам
        $query3 = (new Query)
            ->select($select)
            ->addSelect(['by_document' => new Expression('"' . self::DOCUMENT_ACT . '"')])
            ->from(['order' => 'order_act'])
            ->leftJoin(['document' => self::TABLE_ACT], '{{document}}.[[id]] = {{order}}.[[act_id]]')
            ->leftJoin(['invoice_act' => self::REF_TABLE_INVOICE_ACT], '{{invoice_act}}.[[act_id]] = {{document}}.[[id]]')
            ->leftJoin(['invoice' => self::TABLE_INVOICE], '{{invoice}}.[[id]] = {{invoice_act}}.[[invoice_id]]')
            ->leftJoin(['product' => self::TABLE_PRODUCT], '{{product}}.[[id]] = {{order}}.[[product_id]]')
            ->leftJoin(['order_main' => 'order'], '{{order_main}}.[[id]] = {{order}}.[[order_id]]')
            ->leftJoin(['contractor' => 'contractor'], 'contractor.id = invoice.contractor_id')
            ->andWhere([
                //'invoice.company_id' => $this->company->id,
                'invoice.is_deleted' => false,
                'invoice.invoice_status_id' => [1,2,3,4,6,7,8],
            ])
            ->andWhere([
                'or',
                ['document.status_out_id' => [1,2,3,4]],
                ['document.status_out_id' => null],
            ])
            ->groupBy('invoice.id, document.id');

        // Начальные остатки
        $query4 = (new Query)
            ->select($selectInitialBalance)
            ->from('product')
            ->leftJoin(['store' => 'product_store'], 'product.id = store.product_id')
            ->leftJoin(['pib' => self::TABLE_PRODUCT_INITIAL_BALANCE], 'product.id = pib.product_id')
            ->where([
                //'product.company_id' => $this->company->id,
                'product.is_deleted' => false,
            ])
            ->andWhere(['not', ['product.status' => 0]])
            ->andHaving('amount_in_price_for_buy > 0')
            ->groupBy('product.id');

        return $query1->union($query2)->union($query3)->union($query4, true); // not "all" for prevent double primary
    }

    private function createTriggersOnProductInitialBalance()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_product_initial_balance`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_insert_product_initial_balance`');
        $this->execute('
            CREATE TRIGGER `olap_documents_update_product_initial_balance`
            AFTER UPDATE ON `product_initial_balance`
            FOR EACH ROW
            BEGIN
            
              DECLARE _company_id INT;
              DECLARE _amount_in_price_for_buy BIGINT;
              
              SET _company_id = (
                SELECT `company_id` FROM `product` WHERE `product`.`id` = NEW.`product_id` LIMIT 1
              );  
              
              SET _amount_in_price_for_buy = (
                SELECT IFNULL(SUM(`store`.`initial_quantity` * `product`.`price_for_buy_with_nds`), 0)
                FROM `product` 
                LEFT JOIN `product_store` `store` ON `product`.`id` = `store`.`product_id`
                WHERE `product`.`id` = NEW.`product_id`
              );
                
              INSERT INTO `olap_documents` 
                (`company_id`, `by_document`, `id`, `type`, `date`, `amount_in_price_for_buy`, `product_amount_in_price_for_buy`) VALUES
                (_company_id, "0", NEW.`product_id`, 1, NEW.`date`, IFNULL(_amount_in_price_for_buy, 0), IFNULL(_amount_in_price_for_buy, 0))
              ON DUPLICATE KEY UPDATE `date` = VALUES(`date`);
              
            END     
        ');
        $this->execute('
            CREATE TRIGGER `olap_documents_insert_product_initial_balance`
            AFTER INSERT ON `product_initial_balance`
            FOR EACH ROW
            BEGIN
            
              DECLARE _company_id INT;
              DECLARE _amount_in_price_for_buy BIGINT;
              
              SET _company_id = (
                SELECT `company_id` FROM `product` WHERE `product`.`id` = NEW.`product_id` LIMIT 1
              );  
              
              SET _amount_in_price_for_buy = (
                SELECT IFNULL(SUM(`store`.`initial_quantity` * `product`.`price_for_buy_with_nds`), 0)
                FROM `product` 
                LEFT JOIN `product_store` `store` ON `product`.`id` = `store`.`product_id`
                WHERE `product`.`id` = NEW.`product_id`
              );
                
              INSERT INTO `olap_documents` 
                (`company_id`, `by_document`, `id`, `type`, `date`, `amount_in_price_for_buy`, `product_amount_in_price_for_buy`) VALUES
                (_company_id, "0", NEW.`product_id`, 1, NEW.`date`, IFNULL(_amount_in_price_for_buy, 0), IFNULL(_amount_in_price_for_buy, 0))
              ON DUPLICATE KEY UPDATE `date` = VALUES(`date`);
              
            END
        ');
    }

    private function createTriggersOnProductStore()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_product_store`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_insert_product_store`');
        $this->execute('
            CREATE TRIGGER `olap_documents_update_product_store`
            AFTER UPDATE ON `product_store`
            FOR EACH ROW
            BEGIN
            
              DECLARE _company_id INT;
              DECLARE _amount_in_price_for_buy BIGINT;
              
              SET _company_id = (
                SELECT `company_id` FROM `store` WHERE `store`.`id` = NEW.`store_id` LIMIT 1
              );  
              
              SET _amount_in_price_for_buy = (
                SELECT IFNULL(SUM(`store`.`initial_quantity` * `product`.`price_for_buy_with_nds`), 0)
                FROM `product` 
                LEFT JOIN `product_store` `store` ON `product`.`id` = `store`.`product_id`
                WHERE `product`.`id` = NEW.`product_id`
                    AND `product`.`production_type` = 1
                    AND `product`.`is_deleted` = FALSE
                    AND NOT (`product`.`status` = 0)
              );
                
              INSERT INTO `olap_documents` 
                (`company_id`, `by_document`, `id`, `type`, `date`, `amount_in_price_for_buy`, `product_amount_in_price_for_buy`) VALUES
                (_company_id, "0", NEW.`product_id`, 1, NOW(), IFNULL(_amount_in_price_for_buy, 0), IFNULL(_amount_in_price_for_buy, 0))
              ON DUPLICATE KEY UPDATE 
                `amount_in_price_for_buy` = VALUES(`amount_in_price_for_buy`), 
                `product_amount_in_price_for_buy` = VALUES(`product_amount_in_price_for_buy`);
              
            END        
        ');
        $this->execute('
            CREATE TRIGGER `olap_documents_insert_product_store`
            AFTER INSERT ON `product_store`
            FOR EACH ROW
            BEGIN
            
              DECLARE _company_id INT;
              DECLARE _amount_in_price_for_buy BIGINT;
              
              SET _company_id = (
                SELECT `company_id` FROM `store` WHERE `store`.`id` = NEW.`store_id` LIMIT 1
              );  
              
              SET _amount_in_price_for_buy = (
                SELECT IFNULL(SUM(`store`.`initial_quantity` * `product`.`price_for_buy_with_nds`), 0)
                FROM `product` 
                LEFT JOIN `product_store` `store` ON `product`.`id` = `store`.`product_id`
                WHERE `product`.`id` = NEW.`product_id`
                    AND `product`.`production_type` = 1
                    AND `product`.`is_deleted` = FALSE
                    AND NOT (`product`.`status` = 0)
              );
                
              INSERT INTO `olap_documents` 
                (`company_id`, `by_document`, `id`, `type`, `date`, `amount_in_price_for_buy`, `product_amount_in_price_for_buy`) VALUES
                (_company_id, "0", NEW.`product_id`, 1, NOW(), IFNULL(_amount_in_price_for_buy, 0), IFNULL(_amount_in_price_for_buy, 0))
              ON DUPLICATE KEY UPDATE 
                `amount_in_price_for_buy` = VALUES(`amount_in_price_for_buy`), 
                `product_amount_in_price_for_buy` = VALUES(`product_amount_in_price_for_buy`);
                  
            END
        ');
    }

    private function createTriggersOnOrderPackingList()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_order_packing_list`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_insert_order_packing_list`');
        $this->execute('
            CREATE TRIGGER `olap_documents_update_order_packing_list`
            AFTER UPDATE ON `order_packing_list`
            FOR EACH ROW
            BEGIN
            
              INSERT INTO `olap_documents` (`company_id`, `contractor_id`, `by_document`, `id`, `type`, `date`, `amount`, `amount_in_price_for_buy`, `product_amount_in_price_for_buy`, `is_accounting`, `invoice_expenditure_item_id`) 
              
              SELECT 
                `invoice`.`company_id`,
                `invoice`.`contractor_id`,
                "4" AS `by_document`, 
                `document`.`id`, 
                `document`.`type`, 
                `document`.`document_date` AS `date`, 
                SUM(IF(`document`.`status_out_id` = 5, 0, IF(`document`.`type` = 2, `order_main`.`selling_price_with_vat`, `order_main`.`purchase_price_with_vat`) * `order_packing_list`.`quantity`)) AS `amount`,
                SUM(IF(`document`.`status_out_id` = 5, 0, `order_packing_list`.`quantity` * `product`.`price_for_buy_with_nds`)) AS `amount_in_price_for_buy`,
                SUM(IF(`document`.`status_out_id` = 5, 0, IF(`product`.`production_type` = 1, `order_packing_list`.`quantity` * `product`.`price_for_buy_with_nds`, 0))) AS `product_amount_in_price_for_buy`,
                IF (`contractor`.`not_accounting`, 0, 1) AS is_accounting,
                `invoice`.`invoice_expenditure_item_id`
              FROM `order_packing_list`
              LEFT JOIN `packing_list` `document` ON `document`.`id` = `order_packing_list`.`packing_list_id` 
              LEFT JOIN `invoice` ON `invoice`.`id` = `document`.`invoice_id` 
              LEFT JOIN `product` ON `product`.`id` = `order_packing_list`.`product_id`
              LEFT JOIN `order` `order_main` ON `order_main`.`id` = `order_packing_list`.`order_id` 
              LEFT JOIN `contractor` ON `contractor`.`id` = `invoice`.`contractor_id`
              WHERE 
                `order_packing_list`.`packing_list_id` = NEW.`packing_list_id` 
              GROUP BY `document`.`id`
              
              ON DUPLICATE KEY UPDATE
                `amount` = VALUES(`amount`),
                `amount_in_price_for_buy` = VALUES(`amount_in_price_for_buy`),
                `product_amount_in_price_for_buy` = VALUES(`product_amount_in_price_for_buy`);
              
            END
        ');
        $this->execute('
            CREATE TRIGGER `olap_documents_insert_order_packing_list`
            AFTER INSERT ON `order_packing_list`
            FOR EACH ROW
            BEGIN
            
              INSERT INTO `olap_documents` (`company_id`, `contractor_id`, `by_document`, `id`, `type`, `date`, `amount`, `amount_in_price_for_buy`, `product_amount_in_price_for_buy`, `is_accounting`, `invoice_expenditure_item_id`) 
              
              SELECT 
                `invoice`.`company_id`,
                `invoice`.`contractor_id`,
                "4" AS `by_document`, 
                `document`.`id`, 
                `document`.`type`, 
                `document`.`document_date` AS `date`, 
                SUM(IF(`document`.`status_out_id` = 5, 0, IF(`document`.`type` = 2, `order_main`.`selling_price_with_vat`, `order_main`.`purchase_price_with_vat`) * `order_packing_list`.`quantity`)) AS `amount`,
                SUM(IF(`document`.`status_out_id` = 5, 0, `order_packing_list`.`quantity` * `product`.`price_for_buy_with_nds`)) AS `amount_in_price_for_buy`,
                SUM(IF(`document`.`status_out_id` = 5, 0, IF(`product`.`production_type` = 1, `order_packing_list`.`quantity` * `product`.`price_for_buy_with_nds`, 0))) AS `product_amount_in_price_for_buy`,
                IF (`contractor`.`not_accounting`, 0, 1) AS is_accounting,
                `invoice`.`invoice_expenditure_item_id`
              FROM `order_packing_list`
              LEFT JOIN `packing_list` `document` ON `document`.`id` = `order_packing_list`.`packing_list_id` 
              LEFT JOIN `invoice` ON `invoice`.`id` = `document`.`invoice_id` 
              LEFT JOIN `product` ON `product`.`id` = `order_packing_list`.`product_id`
              LEFT JOIN `order` `order_main` ON `order_main`.`id` = `order_packing_list`.`order_id` 
              LEFT JOIN `contractor` ON `contractor`.`id` = `invoice`.`contractor_id`
              WHERE 
                `order_packing_list`.`packing_list_id` = NEW.`packing_list_id` 
              GROUP BY `document`.`id`
              
              ON DUPLICATE KEY UPDATE
                `amount` = VALUES(`amount`),
                `amount_in_price_for_buy` = VALUES(`amount_in_price_for_buy`),
                `product_amount_in_price_for_buy` = VALUES(`product_amount_in_price_for_buy`);
              
            END        
        ');
    }
    
    private function createTriggersOnPackingList()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_packing_list`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_delete_packing_list`');
        $this->execute('
            CREATE TRIGGER `olap_documents_update_packing_list`
            AFTER UPDATE ON `packing_list`
            FOR EACH ROW
            BEGIN
            
              INSERT INTO `olap_documents` (`company_id`, `contractor_id`, `by_document`, `id`, `type`, `date`, `amount`, `amount_in_price_for_buy`, `product_amount_in_price_for_buy`, `is_accounting`, `invoice_expenditure_item_id`) 
              
              SELECT 
                `invoice`.`company_id`,
                `invoice`.`contractor_id`,
                "4" AS `by_document`, 
                `document`.`id`, 
                `document`.`type`, 
                `document`.`document_date` AS `date`, 
                SUM(IF(`document`.`status_out_id` = 5, 0, IF(`document`.`type` = 2, `order_main`.`selling_price_with_vat`, `order_main`.`purchase_price_with_vat`) * `order_packing_list`.`quantity`)) AS `amount`,
                SUM(IF(`document`.`status_out_id` = 5, 0, `order_packing_list`.`quantity` * `product`.`price_for_buy_with_nds`)) AS `amount_in_price_for_buy`,
                SUM(IF(`document`.`status_out_id` = 5, 0, IF(`product`.`production_type` = 1, `order_packing_list`.`quantity` * `product`.`price_for_buy_with_nds`, 0))) AS `product_amount_in_price_for_buy`,
                IF (`contractor`.`not_accounting`, 0, 1) AS is_accounting,
                `invoice`.`invoice_expenditure_item_id`
              FROM `order_packing_list`
              LEFT JOIN `packing_list` `document` ON `document`.`id` = `order_packing_list`.`packing_list_id` 
              LEFT JOIN `invoice` ON `invoice`.`id` = `document`.`invoice_id` 
              LEFT JOIN `product` ON `product`.`id` = `order_packing_list`.`product_id`
              LEFT JOIN `order` `order_main` ON `order_main`.`id` = `order_packing_list`.`order_id` 
              LEFT JOIN `contractor` ON `contractor`.`id` = `invoice`.`contractor_id`
              WHERE 
                `order_packing_list`.`packing_list_id` = NEW.`id` 
              GROUP BY `document`.`id`
              
              ON DUPLICATE KEY UPDATE
                `date` = VALUES(`date`);
              
            END   
        ');
        $this->execute('
            CREATE TRIGGER `olap_documents_delete_packing_list`
            AFTER DELETE ON `packing_list`
            FOR EACH ROW
            BEGIN
            
              DELETE FROM `olap_documents` WHERE `by_document` = "4" AND `id` = OLD.`id` LIMIT 1;
              
            END  
        ');
    }

    private function createTriggersOnOrderUpd()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_order_upd`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_insert_order_upd`');
        $this->execute('
            CREATE TRIGGER `olap_documents_update_order_upd`
            AFTER UPDATE ON `order_upd`
            FOR EACH ROW
            BEGIN
            
              INSERT INTO `olap_documents` (`company_id`, `contractor_id`, `by_document`, `id`, `type`, `date`, `amount`, `amount_in_price_for_buy`, `product_amount_in_price_for_buy`, `is_accounting`, `invoice_expenditure_item_id`) 
              
              SELECT 
                `invoice`.`company_id`,
                `invoice`.`contractor_id`,
                "8" AS `by_document`, 
                `document`.`id`, 
                `document`.`type`, 
                `document`.`document_date` AS `date`, 
                SUM(IF(`document`.`status_out_id` = 5, 0, IF(`document`.`type` = 2, `order_main`.`selling_price_with_vat`, `order_main`.`purchase_price_with_vat`) * `order_upd`.`quantity`)) AS `amount`,
                SUM(IF(`document`.`status_out_id` = 5, 0, `order_upd`.`quantity` * `product`.`price_for_buy_with_nds`)) AS `amount_in_price_for_buy`,
                SUM(IF(`document`.`status_out_id` = 5, 0, IF(`product`.`production_type` = 1, `order_upd`.`quantity` * `product`.`price_for_buy_with_nds`, 0))) AS `product_amount_in_price_for_buy`,
                IF (`contractor`.`not_accounting`, 0, 1) AS is_accounting,
                `invoice`.`invoice_expenditure_item_id`
              FROM `order_upd`
              LEFT JOIN `upd` `document` ON `document`.`id` = `order_upd`.`upd_id` 
              LEFT JOIN `invoice_upd` ON `invoice_upd`.`upd_id` = `document`.`id`
              LEFT JOIN `invoice` ON `invoice_upd`.`invoice_id` = `invoice`.`id` 
              LEFT JOIN `product` ON `product`.`id` = `order_upd`.`product_id`
              LEFT JOIN `order` `order_main` ON `order_main`.`id` = `order_upd`.`order_id` 
              LEFT JOIN `contractor` ON `contractor`.`id` = `invoice`.`contractor_id`
              WHERE 
                `order_upd`.`upd_id` = NEW.`upd_id` 
              GROUP BY `invoice`.`id`, `document`.`id`
              
              ON DUPLICATE KEY UPDATE
                `amount` = VALUES(`amount`),
                `amount_in_price_for_buy` = VALUES(`amount_in_price_for_buy`),
                `product_amount_in_price_for_buy` = VALUES(`product_amount_in_price_for_buy`);
              
            END    
        ');
        $this->execute('
            CREATE TRIGGER `olap_documents_insert_order_upd`
            AFTER INSERT ON `order_upd`
            FOR EACH ROW
            BEGIN
            
              INSERT INTO `olap_documents` (`company_id`, `contractor_id`, `by_document`, `id`, `type`, `date`, `amount`, `amount_in_price_for_buy`, `product_amount_in_price_for_buy`, `is_accounting`, `invoice_expenditure_item_id`) 
              
              SELECT 
                `invoice`.`company_id`,
                `invoice`.`contractor_id`,
                "8" AS `by_document`, 
                `document`.`id`, 
                `document`.`type`, 
                `document`.`document_date` AS `date`, 
                SUM(IF(`document`.`status_out_id` = 5, 0, IF(`document`.`type` = 2, `order_main`.`selling_price_with_vat`, `order_main`.`purchase_price_with_vat`) * `order_upd`.`quantity`)) AS `amount`,
                SUM(IF(`document`.`status_out_id` = 5, 0, `order_upd`.`quantity` * `product`.`price_for_buy_with_nds`)) AS `amount_in_price_for_buy`,
                SUM(IF(`document`.`status_out_id` = 5, 0, IF(`product`.`production_type` = 1, `order_upd`.`quantity` * `product`.`price_for_buy_with_nds`, 0))) AS `product_amount_in_price_for_buy`,
                IF (`contractor`.`not_accounting`, 0, 1) AS is_accounting,
                `invoice`.`invoice_expenditure_item_id`
              FROM `order_upd`
              LEFT JOIN `upd` `document` ON `document`.`id` = `order_upd`.`upd_id` 
              LEFT JOIN `invoice_upd` ON `invoice_upd`.`upd_id` = `document`.`id`
              LEFT JOIN `invoice` ON `invoice_upd`.`invoice_id` = `invoice`.`id`
              LEFT JOIN `product` ON `product`.`id` = `order_upd`.`product_id`
              LEFT JOIN `order` `order_main` ON `order_main`.`id` = `order_upd`.`order_id` 
              LEFT JOIN `contractor` ON `contractor`.`id` = `invoice`.`contractor_id`
              WHERE 
                `order_upd`.`upd_id` = NEW.`upd_id` 
              GROUP BY `invoice`.`id`, `document`.`id`
              
              ON DUPLICATE KEY UPDATE
                `amount` = VALUES(`amount`),
                `amount_in_price_for_buy` = VALUES(`amount_in_price_for_buy`),
                `product_amount_in_price_for_buy` = VALUES(`product_amount_in_price_for_buy`);
              
            END    
        ');
    }
    
    private function createTriggersOnUpd()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_upd`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_delete_upd`');

        $this->execute('
            CREATE TRIGGER `olap_documents_update_upd`
            AFTER UPDATE ON `upd`
            FOR EACH ROW
            BEGIN
            
              INSERT INTO `olap_documents` (`company_id`, `contractor_id`, `by_document`, `id`, `type`, `date`, `amount`, `amount_in_price_for_buy`, `product_amount_in_price_for_buy`, `is_accounting`, `invoice_expenditure_item_id`) 
              
              SELECT 
                `invoice`.`company_id`,
                `invoice`.`contractor_id`,
                "8" AS `by_document`, 
                `document`.`id`, 
                `document`.`type`, 
                `document`.`document_date` AS `date`, 
                SUM(IF(`document`.`status_out_id` = 5, 0, IF(`document`.`type` = 2, `order_main`.`selling_price_with_vat`, `order_main`.`purchase_price_with_vat`) * `order_upd`.`quantity`)) AS `amount`,
                SUM(IF(`document`.`status_out_id` = 5, 0, `order_upd`.`quantity` * `product`.`price_for_buy_with_nds`)) AS `amount_in_price_for_buy`,
                SUM(IF(`document`.`status_out_id` = 5, 0, IF(`product`.`production_type` = 1, `order_upd`.`quantity` * `product`.`price_for_buy_with_nds`, 0))) AS `product_amount_in_price_for_buy`,
                IF (`contractor`.`not_accounting`, 0, 1) AS is_accounting,
                `invoice`.`invoice_expenditure_item_id`
              FROM `order_upd`
              LEFT JOIN `upd` `document` ON `document`.`id` = `order_upd`.`upd_id` 
              LEFT JOIN `invoice_upd` ON `invoice_upd`.`upd_id` = `document`.`id`
              LEFT JOIN `invoice` ON `invoice_upd`.`invoice_id` = `invoice`.`id`
              LEFT JOIN `product` ON `product`.`id` = `order_upd`.`product_id`
              LEFT JOIN `order` `order_main` ON `order_main`.`id` = `order_upd`.`order_id` 
              LEFT JOIN `contractor` ON `contractor`.`id` = `invoice`.`contractor_id`
              WHERE 
                `order_upd`.`upd_id` = NEW.`id` 
              GROUP BY `invoice`.`id`, `document`.`id`
              
              ON DUPLICATE KEY UPDATE
                `date` = VALUES(`date`);
              
            END
        ');

        $this->execute('
            CREATE TRIGGER `olap_documents_delete_upd`
            AFTER DELETE ON `upd`
            FOR EACH ROW
            BEGIN
            
              DELETE FROM `olap_documents` WHERE `by_document` = "8" AND `id` = OLD.`id` LIMIT 1;
              
            END        
        ');
    }
    
    private function createTriggersOnOrderAct()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_order_act`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_insert_order_act`');
        $this->execute('
            CREATE TRIGGER `olap_documents_update_order_act`
            AFTER UPDATE ON `order_act`
            FOR EACH ROW
            BEGIN
            
              INSERT INTO `olap_documents` (`company_id`, `contractor_id`, `by_document`, `id`, `type`, `date`, `amount`, `amount_in_price_for_buy`, `product_amount_in_price_for_buy`, `is_accounting`, `invoice_expenditure_item_id`) 
              
              SELECT 
                `invoice`.`company_id`,
                `invoice`.`contractor_id`,
                "1" AS `by_document`, 
                `document`.`id`, 
                `document`.`type`, 
                `document`.`document_date` AS `date`, 
                SUM(IF(`document`.`status_out_id` = 5, 0, IF(`document`.`type` = 2, `order_main`.`selling_price_with_vat`, `order_main`.`purchase_price_with_vat`) * `order_act`.`quantity`)) AS `amount`,
                SUM(IF(`document`.`status_out_id` = 5, 0, `order_act`.`quantity` * `product`.`price_for_buy_with_nds`)) AS `amount_in_price_for_buy`,
                SUM(IF(`document`.`status_out_id` = 5, 0, IF(`product`.`production_type` = 1, `order_act`.`quantity` * `product`.`price_for_buy_with_nds`, 0))) AS `product_amount_in_price_for_buy`,
                IF (`contractor`.`not_accounting`, 0, 1) AS is_accounting,
                `invoice`.`invoice_expenditure_item_id`
              FROM `order_act`
              LEFT JOIN `act` `document` ON `document`.`id` = `order_act`.`act_id` 
              LEFT JOIN `invoice_act` ON `invoice_act`.`act_id` = `document`.`id`
              LEFT JOIN `invoice` ON `invoice_act`.`invoice_id` = `invoice`.`id` 
              LEFT JOIN `product` ON `product`.`id` = `order_act`.`product_id`
              LEFT JOIN `order` `order_main` ON `order_main`.`id` = `order_act`.`order_id` 
              LEFT JOIN `contractor` ON `contractor`.`id` = `invoice`.`contractor_id`
              WHERE 
                `order_act`.`act_id` = NEW.`act_id` 
              GROUP BY `invoice`.`id`, `document`.`id`
              
              ON DUPLICATE KEY UPDATE
                `amount` = VALUES(`amount`),
                `amount_in_price_for_buy` = VALUES(`amount_in_price_for_buy`),
                `product_amount_in_price_for_buy` = VALUES(`product_amount_in_price_for_buy`);
              
            END    
        ');
        $this->execute('
            CREATE TRIGGER `olap_documents_insert_order_act`
            AFTER INSERT ON `order_act`
            FOR EACH ROW
            BEGIN
            
              INSERT INTO `olap_documents` (`company_id`, `contractor_id`, `by_document`, `id`, `type`, `date`, `amount`, `amount_in_price_for_buy`, `product_amount_in_price_for_buy`, `is_accounting`, `invoice_expenditure_item_id`) 
              
              SELECT 
                `invoice`.`company_id`,
                `invoice`.`contractor_id`,
                "1" AS `by_document`, 
                `document`.`id`, 
                `document`.`type`, 
                `document`.`document_date` AS `date`, 
                SUM(IF(`document`.`status_out_id` = 5, 0, IF(`document`.`type` = 2, `order_main`.`selling_price_with_vat`, `order_main`.`purchase_price_with_vat`) * `order_act`.`quantity`)) AS `amount`,
                SUM(IF(`document`.`status_out_id` = 5, 0, `order_act`.`quantity` * `product`.`price_for_buy_with_nds`)) AS `amount_in_price_for_buy`,
                SUM(IF(`document`.`status_out_id` = 5, 0, IF(`product`.`production_type` = 1, `order_act`.`quantity` * `product`.`price_for_buy_with_nds`, 0))) AS `product_amount_in_price_for_buy`,
                IF (`contractor`.`not_accounting`, 0, 1) AS is_accounting,
                `invoice`.`invoice_expenditure_item_id`
              FROM `order_act`
              LEFT JOIN `act` `document` ON `document`.`id` = `order_act`.`act_id` 
              LEFT JOIN `invoice_act` ON `invoice_act`.`act_id` = `document`.`id`
              LEFT JOIN `invoice` ON `invoice_act`.`invoice_id` = `invoice`.`id`
              LEFT JOIN `product` ON `product`.`id` = `order_act`.`product_id`
              LEFT JOIN `order` `order_main` ON `order_main`.`id` = `order_act`.`order_id` 
              LEFT JOIN `contractor` ON `contractor`.`id` = `invoice`.`contractor_id`
              WHERE 
                `order_act`.`act_id` = NEW.`act_id` 
              GROUP BY `invoice`.`id`, `document`.`id`
              
              ON DUPLICATE KEY UPDATE
                `amount` = VALUES(`amount`),
                `amount_in_price_for_buy` = VALUES(`amount_in_price_for_buy`),
                `product_amount_in_price_for_buy` = VALUES(`product_amount_in_price_for_buy`);
              
            END    
        ');
    }
    
    private function createTriggersOnAct()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_act`');
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_delete_act`');

        $this->execute('
            CREATE TRIGGER `olap_documents_update_act`
            AFTER UPDATE ON `act`
            FOR EACH ROW
            BEGIN
            
              INSERT INTO `olap_documents` (`company_id`, `contractor_id`, `by_document`, `id`, `type`, `date`, `amount`, `amount_in_price_for_buy`, `product_amount_in_price_for_buy`, `is_accounting`, `invoice_expenditure_item_id`) 
              
              SELECT 
                `invoice`.`company_id`,
                `invoice`.`contractor_id`,
                "1" AS `by_document`, 
                `document`.`id`, 
                `document`.`type`, 
                `document`.`document_date` AS `date`, 
                SUM(IF(`document`.`status_out_id` = 5, 0, IF(`document`.`type` = 2, `order_main`.`selling_price_with_vat`, `order_main`.`purchase_price_with_vat`) * `order_act`.`quantity`)) AS `amount`,
                SUM(IF(`document`.`status_out_id` = 5, 0, `order_act`.`quantity` * `product`.`price_for_buy_with_nds`)) AS `amount_in_price_for_buy`,
                SUM(IF(`document`.`status_out_id` = 5, 0, IF(`product`.`production_type` = 1, `order_act`.`quantity` * `product`.`price_for_buy_with_nds`, 0))) AS `product_amount_in_price_for_buy`,
                IF (`contractor`.`not_accounting`, 0, 1) AS is_accounting,
                `invoice`.`invoice_expenditure_item_id`
              FROM `order_act`
              LEFT JOIN `act` `document` ON `document`.`id` = `order_act`.`act_id` 
              LEFT JOIN `invoice_act` ON `invoice_act`.`act_id` = `document`.`id`
              LEFT JOIN `invoice` ON `invoice_act`.`invoice_id` = `invoice`.`id`
              LEFT JOIN `product` ON `product`.`id` = `order_act`.`product_id`
              LEFT JOIN `order` `order_main` ON `order_main`.`id` = `order_act`.`order_id` 
              LEFT JOIN `contractor` ON `contractor`.`id` = `invoice`.`contractor_id`
              WHERE 
                `order_act`.`act_id` = NEW.`id` 
              GROUP BY `invoice`.`id`, `document`.`id`
              
              ON DUPLICATE KEY UPDATE
                `date` = VALUES(`date`);
              
            END
        ');

        $this->execute('
            CREATE TRIGGER `olap_documents_delete_act`
            AFTER DELETE ON `act`
            FOR EACH ROW
            BEGIN
            
              DELETE FROM `olap_documents` WHERE `by_document` = "1" AND `id` = OLD.`id` LIMIT 1;
              
            END        
        ');
    }    

    private function createTriggersOnProduct()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_product`');

        $this->execute('
            CREATE TRIGGER `olap_documents_update_product`
            AFTER UPDATE ON `product`
            FOR EACH ROW
            BEGIN
            
              IF (NEW.`price_for_buy_with_nds` <> OLD.`price_for_buy_with_nds`) THEN
            
                /* Act */
                UPDATE `olap_documents` SET `amount_in_price_for_buy` = (
                
                  SELECT SUM(IF(`document`.`status_out_id` = 5, 0, `order_act`.`quantity` * `product`.`price_for_buy_with_nds`)) AS `amount_in_price_for_buy` 
                    FROM `order_act`
                    LEFT JOIN `act` `document` ON `document`.`id` = `order_act`.`act_id` 
                    LEFT JOIN `product` ON `product`.`id` = `order_act`.`product_id` 
                    WHERE 
                    `order_act`.`act_id` = `olap_documents`.`id` AND `by_document` = "1"
                    GROUP BY `document`.`id`)    
                
                WHERE `olap_documents`.`by_document` = "1" AND `olap_documents`.`id` IN (    
                  
                  SELECT `act_id` FROM `order_act` WHERE `product_id` = NEW.`id`    
                );            
            
                /* Packing List */
                UPDATE `olap_documents` SET `amount_in_price_for_buy` = (
                
                  SELECT SUM(IF(`document`.`status_out_id` = 5, 0, `order_packing_list`.`quantity` * `product`.`price_for_buy_with_nds`)) AS `amount_in_price_for_buy` 
                    FROM `order_packing_list`
                    LEFT JOIN `packing_list` `document` ON `document`.`id` = `order_packing_list`.`packing_list_id` 
                    LEFT JOIN `product` ON `product`.`id` = `order_packing_list`.`product_id` 
                    WHERE 
                    `order_packing_list`.`packing_list_id` = `olap_documents`.`id` AND `by_document` = "4"
                    GROUP BY `document`.`id`),
                    
                  `product_amount_in_price_for_buy` = `amount_in_price_for_buy`     
                
                WHERE `olap_documents`.`by_document` = "4" AND `olap_documents`.`id` IN (    
                  
                  SELECT `packing_list_id` FROM `order_packing_list` WHERE `product_id` = NEW.`id`    
                );
                
                
                /* UPD */
                UPDATE `olap_documents` SET `amount_in_price_for_buy` = (
                
                  SELECT SUM(IF(`document`.`status_out_id` = 5, 0, `order_upd`.`quantity` * `product`.`price_for_buy_with_nds`)) AS `amount_in_price_for_buy` 
                    FROM `order_upd`
                    LEFT JOIN `upd` `document` ON `document`.`id` = `order_upd`.`upd_id` 
                    LEFT JOIN `product` ON `product`.`id` = `order_upd`.`product_id` 
                    WHERE 
                    `order_upd`.`upd_id` = `olap_documents`.`id` AND `by_document` = "8"
                    GROUP BY `document`.`id`)    
                
                WHERE `olap_documents`.`by_document` = "8" AND `olap_documents`.`id` IN (    
                  
                  SELECT `upd_id` FROM `order_upd` WHERE `product_id` = NEW.`id`    
                );
                
                UPDATE `olap_documents` SET `product_amount_in_price_for_buy` = (
                
                  SELECT IFNULL(SUM(IF(`document`.`status_out_id` = 5, 0, `order_upd`.`quantity` * `product`.`price_for_buy_with_nds`)), 0) AS `product_amount_in_price_for_buy` 
                    FROM `order_upd`
                    LEFT JOIN `upd` `document` ON `document`.`id` = `order_upd`.`upd_id` 
                    LEFT JOIN `product` ON `product`.`id` = `order_upd`.`product_id` 
                    WHERE 
                    `order_upd`.`upd_id` = `olap_documents`.`id` AND `by_document` = "8" AND `product`.`production_type` = 1
                    GROUP BY `document`.`id`)
                
                WHERE `olap_documents`.`by_document` = "8" AND `olap_documents`.`id` IN (    
                  
                  SELECT `upd_id` FROM `order_upd` WHERE `product_id` = NEW.`id`    
                );                
                
              END IF;	
              
            END
        
        ');
    }

    private function createTriggersOnInvoice()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_invoice`');

        $this->execute('
            CREATE TRIGGER `olap_documents_update_invoice`
            AFTER UPDATE ON `invoice`
            FOR EACH ROW
            BEGIN
            
              IF (NEW.`invoice_expenditure_item_id` <> OLD.`invoice_expenditure_item_id`) THEN
            
                /* Packing List */
                UPDATE `olap_documents` SET `invoice_expenditure_item_id` = NEW.`invoice_expenditure_item_id`
                WHERE `olap_documents`.`by_document` = "4" AND `olap_documents`.`id` IN (    
                  
                  SELECT `id` FROM `packing_list` WHERE `invoice_id` = NEW.`id`    
                );
                
                /* Upd */
                UPDATE `olap_documents` SET `invoice_expenditure_item_id` = NEW.`invoice_expenditure_item_id`
                WHERE `olap_documents`.`by_document` = "8" AND `olap_documents`.`id` IN (    
                  
                  SELECT `upd_id` FROM `invoice_upd` WHERE `invoice_id` = NEW.`id`
                );
                
                /* Act */
                UPDATE `olap_documents` SET `invoice_expenditure_item_id` = NEW.`invoice_expenditure_item_id`
                WHERE `olap_documents`.`by_document` = "1" AND `olap_documents`.`id` IN (    
                  
                  SELECT `act_id` FROM `invoice_act` WHERE `invoice_id` = NEW.`id`
                );
                                
              END IF;	
              
            END
        
        ');
    }

    private function createTriggersOnContractor()
    {
        $this->execute('DROP TRIGGER IF EXISTS `olap_documents_update_contractor`');

        $this->execute('
            CREATE TRIGGER `olap_documents_update_contractor`
            AFTER UPDATE ON `contractor`
            FOR EACH ROW
            BEGIN
            
              IF (NEW.`not_accounting` <> OLD.`not_accounting`) THEN
            
                UPDATE `olap_documents` 
                SET `is_accounting` = (IF (NEW.`not_accounting`, 0, 1))
                WHERE `olap_documents`.`contractor_id` = NEW.`id`;
                                
              END IF;	
              
            END
        
        ');
    }
}
