<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180515_071432_alter_currency_add_columns extends Migration
{
    public function safeUp()
    {
        $this->alterColumn('currency', 'id', $this->char(3));
        $this->alterColumn('currency', 'name', $this->char(3)->notNull() . " COLLATE 'utf8_general_ci'");
        $this->alterColumn('currency', 'old_value', $this->decimal(13, 4)->notNull()->unsigned());
        $this->alterColumn('currency', 'current_value', $this->decimal(13, 4)->notNull()->unsigned());

        $this->addColumn('currency', 'label', $this->string()->notNull()->after('name'));
        $this->addColumn('currency', 'is_default', $this->boolean()->notNull()->unsigned()->defaultValue(false)->after('label'));
        $this->addColumn('currency', 'amount', $this->integer()->notNull()->unsigned()->defaultValue(1)->after('is_default'));
        $this->addColumn('currency', 'sort', $this->integer()->notNull()->unsigned()->defaultValue(1));

        $this->createIndex('KEY_name', 'currency', 'name', true);

        $this->execute("
            UPDATE {{currency}}
            SET [[id]] = CASE
                    WHEN [[name]] = 'USD' THEN '840'
                    WHEN [[name]] = 'EUR' THEN '978'
                    WHEN [[name]] = 'BYN' THEN '974'
                    WHEN [[name]] = 'KZT' THEN '398'
                    WHEN [[name]] = 'UAH' THEN '980'
                END,
                [[label]] = CASE
                    WHEN [[name]] = 'USD' THEN 'Доллар США'
                    WHEN [[name]] = 'EUR' THEN 'Евро'
                    WHEN [[name]] = 'BYN' THEN 'Белорусский рубль'
                    WHEN [[name]] = 'KZT' THEN 'Казахстанский тенге'
                    WHEN [[name]] = 'UAH' THEN 'Украинская гривна'
                END,
                [[amount]] = CASE
                    WHEN [[name]] = 'USD' THEN 1
                    WHEN [[name]] = 'EUR' THEN 1
                    WHEN [[name]] = 'BYN' THEN 1
                    WHEN [[name]] = 'KZT' THEN 100
                    WHEN [[name]] = 'UAH' THEN 10
                END,
                [[sort]] = CASE
                    WHEN [[name]] = 'USD' THEN 1
                    WHEN [[name]] = 'EUR' THEN 2
                    WHEN [[name]] = 'BYN' THEN 3
                    WHEN [[name]] = 'KZT' THEN 4
                    WHEN [[name]] = 'UAH' THEN 5
                END
        ");
    }

    public function safeDown()
    {
        $this->execute("
            UPDATE {{currency}}
            SET [[id]] = CASE
                    WHEN [[name]] = 'USD' THEN 1
                    WHEN [[name]] = 'EUR' THEN 2
                    WHEN [[name]] = 'BYN' THEN 3
                    WHEN [[name]] = 'KZT' THEN 4
                    WHEN [[name]] = 'UAH' THEN 5
                END
        ");

        $this->dropIndex('KEY_name', 'currency');

        $this->dropColumn('currency', 'label');
        $this->dropColumn('currency', 'is_default');
        $this->dropColumn('currency', 'amount');
        $this->dropColumn('currency', 'sort');

        $this->alterColumn('currency', 'id', $this->integer()->notNull() . ' AUTO_INCREMENT');
        $this->alterColumn('currency', 'name', $this->string() . " COLLATE 'utf8_unicode_ci'");
        $this->alterColumn('currency', 'old_value', $this->string());
        $this->alterColumn('currency', 'current_value', $this->string());
    }
}
