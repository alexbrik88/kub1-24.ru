<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170721_180004_alter_servicePayment_dropColumns extends Migration
{
    public function safeUp()
    {
        $this->dropForeignKey('fk_service_payment_to_subscribe', '{{%service_payment}}');
        
        $this->dropColumn('{{%service_payment}}', 'subscribe_id');
        $this->dropColumn('{{%service_payment}}', 'price');
        $this->dropColumn('{{%service_payment}}', 'discount');
    }
    
    public function safeDown()
    {
        $this->addColumn('{{%service_payment}}', 'subscribe_id', $this->integer() . ' AFTER [[invoice_number]]');
        $this->addColumn('{{%service_payment}}', 'price', $this->string(45) . ' AFTER [[subscribe_id]]');
        $this->addColumn('{{%service_payment}}', 'discount', $this->integer()->defaultValue(0) . ' AFTER [[price]]');
        
        $this->addForeignKey('fk_service_payment_to_subscribe', '{{%service_payment}}', 'subscribe_id', '{{%service_subscribe}}', 'id', 'SET NULL');

        $this->execute("
            UPDATE
                {{%service_payment}}
            LEFT JOIN
                {{%service_subscribe}} ON {{%service_subscribe}}.[[payment_id]] = {{%service_payment}}.[[id]]
            LEFT JOIN
                {{%service_payment_order}} ON {{%service_payment_order}}.[[payment_id]] = {{%service_payment}}.[[id]]
            SET
                {{%service_payment}}.[[subscribe_id]] = {{%service_subscribe}}.[[id]],
                {{%service_payment}}.[[price]] = IFNULL({{%service_payment_order}}.[[price]], {{%service_payment}}.[[sum]]),
                {{%service_payment}}.[[discount]] = IFNULL({{%service_payment_order}}.[[discount]], 0)
        ");
    }
}
