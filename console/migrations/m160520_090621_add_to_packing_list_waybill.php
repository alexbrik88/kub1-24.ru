<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160520_090621_add_to_packing_list_waybill extends Migration
{
    public $tPackingList = 'packing_list';

    public function safeUp()
    {
        $this->addColumn($this->tPackingList, 'waybill_number', $this->string(255)->defaultValue(null));
        $this->addColumn($this->tPackingList, 'waybill_date', $this->date()->defaultValue(null));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tPackingList, 'waybill_number');
        $this->dropColumn($this->tPackingList, 'waybill_date');
    }
}


