<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210726_100540_alter_config_add_columns extends Migration
{
    public $table = 'user_config';

    public function safeUp()
    {
        $this->addColumn($this->table, 'table_view_finance_plan', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table, 'finance_plan_revenue', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table, 'finance_plan_currency', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table, 'finance_plan_net_profit', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table, 'finance_plan_margin', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 0');
        $this->addColumn($this->table, 'finance_plan_comment', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 1');
        $this->addColumn($this->table, 'finance_plan_employee', Schema::TYPE_TINYINT . ' NOT NULL DEFAULT 1');
    }

    public function safeDown()
    {
        $this->dropColumn($this->table, 'table_view_finance_plan');
        $this->dropColumn($this->table, 'finance_plan_revenue');
        $this->dropColumn($this->table, 'finance_plan_currency');
        $this->dropColumn($this->table, 'finance_plan_net_profit');
        $this->dropColumn($this->table, 'finance_plan_margin');
        $this->dropColumn($this->table, 'finance_plan_comment');
        $this->dropColumn($this->table, 'finance_plan_employee');
    }
}
