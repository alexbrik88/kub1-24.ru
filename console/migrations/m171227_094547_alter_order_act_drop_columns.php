<?php

use console\components\db\Migration;
use yii\db\Schema;

class m171227_094547_alter_order_act_drop_columns extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('order_act', 'amount_sales_with_vat');
        $this->dropColumn('order_act', 'amount_sales_no_vat');
        $this->dropColumn('order_act', 'amount_purchase_with_vat');
        $this->dropColumn('order_act', 'amount_purchase_no_vat');
        $this->dropColumn('order_act', 'amount_vat');
    }

    public function safeDown()
    {
        $this->addColumn('order_act', 'amount_vat', $this->integer()->notNull());
        $this->addColumn('order_act', 'amount_purchase_no_vat', $this->integer()->notNull());
        $this->addColumn('order_act', 'amount_purchase_with_vat', $this->integer()->notNull());
        $this->addColumn('order_act', 'amount_sales_no_vat', $this->integer()->notNull());
        $this->addColumn('order_act', 'amount_sales_with_vat', $this->integer()->notNull());
    }
}
