<?php

use yii\db\Migration;

class m210421_000328_alter_card_operation_table extends Migration
{
    /** @var string */
    private const TABLE_NAME = '{{%card_operation}}';

    /**
     * @inheritDoc
     */
    public function up(): void
    {
        $this->addColumn(self::TABLE_NAME, 'bill_id', $this->bigInteger()->notNull()->after('account_id'));
        $this->createIndex('ck_bill_id', self::TABLE_NAME, ['bill_id']);
        $this->addForeignKey(
            'fk_card_operation__card_bill',
            self::TABLE_NAME,
            ['bill_id'],
            '{{%card_bill}}',
            ['id'],
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * @inheritDoc
     */
    public function down(): void
    {
        $this->dropForeignKey('fk_card_operation__card_bill', self::TABLE_NAME);
        $this->dropColumn(self::TABLE_NAME, 'bill_id');
    }
}
