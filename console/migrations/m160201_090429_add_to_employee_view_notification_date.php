<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160201_090429_add_to_employee_view_notification_date extends Migration
{
    public $tEmployee = 'employee';

    public function safeUp()
    {
        $this->addColumn($this->tEmployee, 'view_notification_date', $this->dateTime());
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tEmployee, 'view_notification_date');
    }
}


