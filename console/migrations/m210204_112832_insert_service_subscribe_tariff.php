<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210204_112832_insert_service_subscribe_tariff extends Migration
{
    public function safeUp()
    {
        $this->batchInsert('{{%service_subscribe_tariff}}', [
            'id',
            'tariff_group_id',
            'duration_month',
            'duration_day',
            'price',
            'is_active',
            'proposition',
        ], [
            [72, 19, 1, 0, 1000, 1, 'Получи скидку 10% при оплате за 4 месяца'],
            [73, 19, 4, 0, 3600, 1, 'Получи скидку 20% при оплате за год'],
            [74, 19, 12, 0, 9600, 1, 'Лучшая цена!'],
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%service_subscribe_tariff}}', [
            'id' => [72, 73, 74]
        ]);
    }
}
