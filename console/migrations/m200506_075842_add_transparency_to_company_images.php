<?php

use console\components\db\Migration;
use yii\db\Schema;
use common\models\Company;

class m200506_075842_add_transparency_to_company_images extends Migration
{

    public function safeUp()
    {
        Yii::$app->db->createCommand('SET session wait_timeout=288000')->execute();
        Yii::$app->db->createCommand('SET session interactive_timeout=288000')->execute();

        $cntCompanies = 0;
        $cntImages = 0;
        $companiesIds = Company::find()->select('id')->asArray()->all();
        foreach ($companiesIds as $id) {

            $cntCompanies++;

            $company = Company::findOne($id);
            if ($company) {
                foreach (['printImage', 'chiefSignatureImage', 'chiefAccountantSignatureImage'] as $imageSlug) {
                    $imgPath = $company->getImage($imageSlug);
                    if ($imgPath) {
                        $im = new \Imagick($imgPath);
                        $im->transparentPaintImage($im->getImageBackgroundColor(), 0, 2500, false);
                        $im->setImageFormat('png');
                        $im->writeImage($imgPath);
                        $im->destroy();

                        $cntImages++;
                    }
                }
            }
        }

        echo 'Companies: ' . $cntCompanies . ', images: ' . $cntImages ."\n";
    }

    public function safeDown()
    {
        // no down;
    }
}
