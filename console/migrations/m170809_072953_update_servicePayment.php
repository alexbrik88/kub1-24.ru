<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170809_072953_update_servicePayment extends Migration
{
    public function safeUp()
    {
        $this->execute("
			UPDATE {{%service_payment}} {{payment}}
			INNER JOIN {{%service_subscribe}} {{subscribe}}
				ON {{subscribe}}.[[payment_id]] = {{payment}}.[[id]]
			SET {{payment}}.[[is_confirmed]] = 1
			WHERE {{payment}}.[[is_confirmed]] = 0 AND {{subscribe}}.[[status_id]] > 1
        ");
    }
    
    public function safeDown()
    {
    	echo "This migration not rolled back\n";
    }
}
