<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190422_052229_employeecompany_add_column extends Migration
{
    public $tableName = 'employee_company';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'has_no_patronymic', $this->boolean()->defaultValue(false)->after('patronymic'));

        $this->execute('
            UPDATE {{employee_company}}
            SET {{employee_company}}.[[has_no_patronymic]] = 1
            WHERE {{employee_company}}.[[patronymic]] = ""
        ');
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'has_no_patronymic');
    }
}
