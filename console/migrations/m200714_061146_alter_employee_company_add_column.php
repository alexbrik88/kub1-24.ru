<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200714_061146_alter_employee_company_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%employee_company}}', 'can_business_analytics', $this->boolean()->defaultValue(false));
        $this->addColumn('{{%employee_company}}', 'can_business_analytics_time', $this->integer());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%employee_company}}', 'can_business_analytics');
        $this->dropColumn('{{%employee_company}}', 'can_business_analytics_time');
    }
}
