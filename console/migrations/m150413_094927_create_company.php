<?php

use yii\db\Schema;
use yii\db\Migration;

class m150413_094927_create_company extends Migration
{
    public function up()
    {
        $this->createTable('company', [
            'id' => Schema::TYPE_PK,
            'company_form' => Schema::TYPE_BOOLEAN . ' NOT NULL COMMENT "0 - ИП, 1 - ООО"',
            'taxation_type_id' => Schema::TYPE_INTEGER . ' NOT NULL',
            // name
            'name_full' => Schema::TYPE_STRING . '(255) NOT NULL',
            'name_short' => Schema::TYPE_STRING . '(255) NOT NULL',
            // legal address
            'address_legal_city' => Schema::TYPE_STRING . '(100) NULL',
            'address_legal_street' => Schema::TYPE_STRING . '(100) NULL',
            'address_legal_house_type_id' => 'INT NOT NULL',
            'address_legal_house' => Schema::TYPE_STRING . '(45) NULL',
            'address_legal_housing_type_id' => 'INT NOT NULL',
            'address_legal_housing' => Schema::TYPE_STRING . '(45) NULL',
            'address_legal_apartment_type_id' => 'INT NOT NULL',
            'address_legal_apartment' => Schema::TYPE_STRING . '(45) NULL',
            'address_legal_postcode' => 'INT NULL',
            'address_legal_full' => Schema::TYPE_STRING . '(1024) NULL',
            'address_legal_is_fact' => Schema::TYPE_BOOLEAN . ' NULL COMMENT "Совпадение юридического и фактического адресов: 0 - не совпадают, 1 - совпадают"',
            // actual address
            'address_actual_city' => Schema::TYPE_STRING . '(100) NULL',
            'address_actual_street' => Schema::TYPE_STRING . '(100) NULL',
            'address_actual_house_type_id' => 'INT NOT NULL',
            'address_actual_house' => Schema::TYPE_STRING . '(45) NULL',
            'address_actual_housing_type_id' => 'INT NOT NULL',
            'address_actual_housing' => Schema::TYPE_STRING . '(45) NULL',
            'address_actual_apartment_type_id' => 'INT NOT NULL',
            'address_actual_apartment' => Schema::TYPE_STRING . '(45) NULL',
            'address_actual_postcode' => Schema::TYPE_STRING . '(45) NULL',
            'address_actual_full' => Schema::TYPE_STRING . '(1024) NULL',

            'visitcard_url' => Schema::TYPE_STRING . '(255) NULL',
            'phone' => Schema::TYPE_STRING . '(45) NULL',
            'email' => Schema::TYPE_STRING . '(45) NULL',
            // chief
            'chief_post_name' => Schema::TYPE_STRING . '(45) NULL',
            'chief_lastname' => Schema::TYPE_STRING . '(45) NULL',
            'chief_firstname' => Schema::TYPE_STRING . '(45) NULL',
            'chief_firstname_initials' => Schema::TYPE_STRING . '(10) NULL',
            'chief_patronymic' => Schema::TYPE_STRING . '(45) NULL',
            'chief_patronymic_initials' => Schema::TYPE_STRING . '(10) NULL',
            'chief_is_chief_accountant' => Schema::TYPE_STRING . '(45) NULL',
            /// chief accountant
            'chief_accountant_lastname' => Schema::TYPE_STRING . '(45) NULL',
            'chief_accountant_firstname' => Schema::TYPE_STRING . '(45) NULL',
            'chief_accountant_firstname_initials' => Schema::TYPE_STRING . '(10) NULL',
            'chief_accountant_patronymic' => Schema::TYPE_STRING . '(45) NULL',
            'chief_accountant_patronymic_initials' => Schema::TYPE_STRING . '(45) NULL',

            'orgn' => Schema::TYPE_STRING . '(45) NULL COMMENT "для ООО"',
            'egrip' => Schema::TYPE_STRING . '(45) NULL COMMENT "для ИП"',
            'inn' => Schema::TYPE_STRING . '(45) NULL',
            'kpp' => Schema::TYPE_STRING . '(45) NULL COMMENT "для ООО"',
            // individual
            'ie_lastname' => Schema::TYPE_STRING . '(45) NULL',
            'ie_firstname' => Schema::TYPE_STRING . '(45) NULL',
            'ie_firstname_initials' => Schema::TYPE_STRING . '(45) NULL',
            'ie_patronymic' => Schema::TYPE_STRING . '(45) NULL',
            'ie_patronymic_initials' => Schema::TYPE_STRING . '(45) NULL',

            'rs' => 'CHAR(20) NULL',
            'bank_name' => Schema::TYPE_STRING . '(45) NULL',
            'ks' => 'CHAR(20) NULL',
            'bik' => 'CHAR(9) NULL',
            'okpo' => Schema::TYPE_STRING . '(10) NULL',
            'okogu' => Schema::TYPE_STRING . '(45) NULL COMMENT "для ООО"',
            'okato' => Schema::TYPE_STRING . '(45) NULL COMMENT "для ООО"',
            'okded' => Schema::TYPE_STRING . '(45) NULL',
            'okfs' => Schema::TYPE_STRING . '(45) NULL COMMENT "для ООО"',
            'okopf' => Schema::TYPE_STRING . '(45) NULL COMMENT "для ООО"',
            'oktmo' => Schema::TYPE_STRING . '(45) NULL COMMENT "для ИП"',
            'pfr' => Schema::TYPE_STRING . '(45) NULL COMMENT "для ООО"',
            'fss' => Schema::TYPE_STRING . '(45) NULL',

            'pfr_ie' => Schema::TYPE_STRING . '(45) NULL COMMENT "для ИП"',
            'pfr_employer' => Schema::TYPE_STRING . '(45) NULL COMMENT "для ИП"',

            'tax_authority_registration_date' => 'DATE NULL',

            'logo_link' => Schema::TYPE_STRING . '(255) NULL',
            'print_link' => Schema::TYPE_STRING . '(255) NULL',
            'chief_signature_link' => Schema::TYPE_STRING . '(255) NULL',
        ]);

        $this->addForeignKey('taxation_type_rel', 'company', 'taxation_type_id', 'taxation_type', 'id');

        $this->addForeignKey('address_legal_house_type_rel', 'company', 'address_legal_house_type_id', 'address_house_type', 'id');
        $this->addForeignKey('address_legal_housing_type_rel', 'company', 'address_legal_housing_type_id', 'address_housing_type', 'id');
        $this->addForeignKey('address_legal_apartment_type_rel', 'company', 'address_legal_apartment_type_id', 'address_apartment_type', 'id');
        $this->addForeignKey('address_actual_house_type_rel', 'company', 'address_actual_house_type_id', 'address_house_type', 'id');
        $this->addForeignKey('address_actual_housing_type_rel', 'company', 'address_actual_housing_type_id', 'address_housing_type', 'id');
        $this->addForeignKey('address_actual_apartment_type_rel', 'company', 'address_actual_apartment_type_id', 'address_apartment_type', 'id');
    }

    public function down()
    {
        $this->dropTable('company');
    }
}


