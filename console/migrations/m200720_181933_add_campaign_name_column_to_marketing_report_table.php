<?php

use yii\db\Migration;

/**
 * Handles adding columns to table `{{%marketing_report}}`.
 */
class m200720_181933_add_campaign_name_column_to_marketing_report_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('marketing_report', 'campaign_name', $this->string()->after('date'));
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('marketing_report', 'campaign_name');
    }
}
