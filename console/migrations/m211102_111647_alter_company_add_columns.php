<?php

use console\components\db\Migration;
use yii\db\Schema;

class m211102_111647_alter_company_add_columns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%company}}', 'seller_payment_delay', $this->integer()->notNull()->defaultValue(10));
        $this->addColumn('{{%company}}', 'customer_payment_delay', $this->integer()->notNull()->defaultValue(10));
    }

    public function safeDown()
    {
        $this->dropColumn('{{%company}}', 'seller_payment_delay');
        $this->dropColumn('{{%company}}', 'customer_payment_delay');
    }
}
