<?php

use console\components\db\Migration;
use yii\db\Schema;
use common\models\employee\Employee;

class m171016_111228_update_chat_photo extends Migration
{
    public $tEmployee = 'employee';

    public function safeUp()
    {
        /* @var $employee Employee */
        foreach (Employee::find()->andWhere(['sex' => null])->all() as $employee) {
            $this->update($this->tEmployee,
                ['chat_photo' => Employee::NO_CHAT_PHOTO],
                ['id' => $employee->id]);
        }
    }

    public function safeDown()
    {

    }
}


