<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201229_212207_alter_config_drop_column extends Migration
{
    public $tableName = 'user_config';

    public function safeUp()
    {
        $this->dropColumn($this->tableName, 'project_estimate_number');
    }

    public function safeDown()
    {
        $this->addColumn($this->tableName, 'project_estimate_number', $this->boolean()->notNull()->defaultValue(false));
    }
}
