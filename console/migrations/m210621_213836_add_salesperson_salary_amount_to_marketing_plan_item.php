<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210621_213836_add_salesperson_salary_amount_to_marketing_plan_item extends Migration
{
    public function safeUp()
    {
        $this->addColumn('marketing_plan_item', 'salesperson_salary_amount', $this->bigInteger(20)->unsigned());
    }

    public function safeDown()
    {
        $this->dropColumn('marketing_plan_item', 'salesperson_salary_amount');
    }
}
