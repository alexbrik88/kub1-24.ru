<?php

use console\components\db\Migration;
use yii\db\Schema;

class m200305_080501_insert_service_subscribe_tariff_group extends Migration
{
    public function safeUp()
    {
        $this->insert('{{%service_subscribe_tariff_group}}', [
            'id' => 10,
            'name' => 'Прайс-лист',
            'is_base' => 0,
            'has_base' => 0,
            'is_active' => 0,
            'description' => '',
            'hint' => '',
            'priority' => 10,
            'icon' => 'price-list',
        ]);
    }

    public function safeDown()
    {
        $this->delete('{{%service_subscribe_tariff_group}}', [
            'id' => 10,
        ]);
    }
}
