<?php

use yii\db\Schema;
use yii\db\Migration;

class m151116_083940_create_subscribe extends Migration
{
    public $tSubscribe = 'service_subscribe';
    public $tSubscribeStatus = 'service_subscribe_status';
    public $tSubscribeTariff = 'service_subscribe_tariff';
    public $tCompany = 'company';

    public function safeUp()
    {
        $this->createTable($this->tSubscribeStatus, [
            'id' => $this->primaryKey(),
            'name' => $this->string()->notNull(),
        ]);

        $this->batchInsert($this->tSubscribeStatus, ['id', 'name'], [
            [1, 'Новая'],
            [2, 'Оплачена'],
            [3, 'Активирована'],
        ]);

        $this->createTable($this->tSubscribe, [
            'id' => $this->primaryKey(),
            'company_id' => $this->integer()->notNull(),
            'tariff_id' => $this->integer()->notNull(),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
            'activated_at' => $this->integer(),
            'expired_at' => $this->integer(),
            'status_id' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey('fk_service_subscribe_to_company', $this->tSubscribe, 'company_id', $this->tCompany, 'id');
        $this->addForeignKey('fk_service_subscribe_to_tariff', $this->tSubscribe, 'tariff_id', $this->tSubscribeTariff, 'id');
        $this->addForeignKey('fk_service_subscribe_to_status', $this->tSubscribe, 'status_id', $this->tSubscribeStatus, 'id');
    }
    
    public function safeDown()
    {
        $this->dropTable($this->tSubscribe);
        $this->dropTable($this->tSubscribeStatus);
    }
}
