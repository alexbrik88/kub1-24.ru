<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190212_105320_add_contract_essence_template_to_invoice extends Migration
{
    public $tableName = '{{%invoice}}';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'contract_essence_template', $this->integer()->defaultValue(2)->after('contract_essence')->comment('0 - Пустой бланк, 1 - Оказание услуг, 2 - Поставка товаров'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'contract_essence_template');
    }
}
