<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180823_143640_alter_payment_order_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('payment_order', 'company_bik', $this->string()->after('invoice_id'));
        $this->addColumn('payment_order', 'company_bank_name', $this->string()->after('company_bik'));
        $this->addColumn('payment_order', 'company_ks', $this->string()->after('company_bank_name'));
        $this->addColumn('payment_order', 'company_rs', $this->string()->after('company_ks'));

        $this->execute('
            UPDATE {{payment_order}}
            LEFT JOIN {{checking_accountant}}
            ON {{checking_accountant}}.[[company_id]] = {{payment_order}}.[[company_id]] AND {{checking_accountant}}.[[type]] = 0
            SET {{payment_order}}.[[company_bik]] = {{checking_accountant}}.[[bik]],
                 {{payment_order}}.[[company_bank_name]] = {{checking_accountant}}.[[bank_name]],
                 {{payment_order}}.[[company_ks]] = {{checking_accountant}}.[[ks]],
                 {{payment_order}}.[[company_rs]] = {{checking_accountant}}.[[rs]];
        ');
    }

    public function safeDown()
    {
        $this->dropColumn('payment_order', 'company_bik');
        $this->dropColumn('payment_order', 'company_bank_name');
        $this->dropColumn('payment_order', 'company_ks');
        $this->dropColumn('payment_order', 'company_rs');
    }
}
