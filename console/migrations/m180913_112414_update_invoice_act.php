<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180913_112414_update_invoice_act extends Migration
{
    public $tableName = '{{%invoice_act}}';

    public function safeUp()
    {
        $this->dropTable($this->tableName);
        $this->createTable($this->tableName, [
            'invoice_id' => $this->integer()->notNull(),
            'act_id' => $this->integer()->notNull(),
        ]);
        $this->addPrimaryKey('PRIMARY_KEY', $this->tableName, ['invoice_id', 'act_id']);
        $this->addForeignKey('invoice_act_invoice_id', $this->tableName, 'invoice_id', 'invoice', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey('invoice_act_act_id', $this->tableName, 'act_id', 'act', 'id', 'CASCADE', 'CASCADE');

        $this->execute("
            INSERT INTO {{%invoice_act}} (invoice_id, act_id)
            SELECT {{%act}}.[[invoice_id]], {{%act}}.[[id]]
            FROM {{%act}}
            LEFT JOIN {{%invoice}}
                ON {{%act}}.[[invoice_id]] = {{%invoice}}.[[id]]
			WHERE {{%invoice}}.[[id]] IS NOT NULL
			AND {{%act}}.[[invoice_id]] IS NOT NULL
            AND {{%act}}.[[invoice_id]] IS NOT NULL;
        ");
    }
    
    public function safeDown()
    {
        $this->dropTable($this->tableName);
        $this->createTable($this->tableName, [
            'id' => $this->primaryKey(),
            'invoice_id' => $this->integer()->notNull(),
            'act_id' => $this->integer()->notNull(),
        ]);

        $this->addForeignKey($this->tableName . '_invoice_id', $this->tableName, 'invoice_id', 'invoice', 'id', 'RESTRICT', 'CASCADE');
        $this->addForeignKey($this->tableName . '_act_id', $this->tableName, 'act_id', 'act', 'id', 'RESTRICT', 'CASCADE');
    }
}