<?php

use console\components\db\Migration;
use yii\db\Schema;

class m201109_091123_alter_project_estimate_append_customer_id extends Migration
{
    public $tableName = 'project_estimate';

    public function safeUp()
    {
        $this->addColumn($this->tableName, 'customer_id', $this->integer(11)->after('project_id'));
    }

    public function safeDown()
    {
        $this->dropColumn($this->tableName, 'customer_id');
    }
}
