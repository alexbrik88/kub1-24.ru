<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210916_102400_alter_moysklad_document_add_index extends Migration
{
    public $table = 'moysklad_document';

    public function safeUp()
    {
        $table = $this->table;
        $this->createIndex("IDX_{$table}_type", $table, ['company_id', 'document_type_id']);
        $this->createIndex("IDX_{$table}_document_number", $table, ['company_id', 'document_number']);
        $this->createIndex("IDX_{$table}_contractor_inn", $table, ['company_id', 'contractor_inn']);
        $this->createIndex("IDX_{$table}_contractor_name", $table, ['company_id', 'contractor_name']);
    }

    public function safeDown()
    {
        $table = $this->table;
        $this->dropIndex("IDX_{$table}_type", $table);
        $this->dropIndex("IDX_{$table}_document_number", $table);
        $this->dropIndex("IDX_{$table}_contractor_inn", $table);
        $this->dropIndex("IDX_{$table}_contractor_name", $table);
    }
}
