<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210527_183638_update_order_waybill extends Migration
{
    public function safeUp()
    {
        $this->dropColumn('waybill', 'consignment_short_description');
        $this->dropColumn('waybill', 'with_packing_list');
        $this->dropColumn('waybill', 'with_invoice');
        $this->dropColumn('waybill', 'with_invoice_facture');
        $this->dropColumn('waybill', 'consignment_packaging_type');
        $this->dropColumn('waybill', 'consignment_places_count');
        $this->dropColumn('waybill', 'weight_determining_method');
        $this->dropColumn('waybill', 'consignment_code');
        $this->dropColumn('waybill', 'consignment_container_number');
        $this->dropColumn('waybill', 'consignment_classification');
        $this->dropColumn('waybill', 'consignment_gross_weight');

        $this->addColumn('order_waybill', 'with_packing_list', $this->boolean()->unsigned());
        $this->addColumn('order_waybill', 'with_invoice', $this->boolean()->unsigned());
        $this->addColumn('order_waybill', 'with_invoice_facture', $this->boolean()->unsigned());
        $this->addColumn('order_waybill', 'consignment_packaging_type', $this->string());
        $this->addColumn('order_waybill', 'consignment_places_count', $this->string());
        $this->addColumn('order_waybill', 'weight_determining_method', $this->tinyInteger()->unsigned());
        $this->addColumn('order_waybill', 'consignment_code', $this->string());
        $this->addColumn('order_waybill', 'consignment_container_number', $this->string());
        $this->addColumn('order_waybill', 'consignment_classification', $this->string());
        $this->addColumn('order_waybill', 'consignment_gross_weight', $this->float(2));
    }

    public function safeDown()
    {
        $this->dropColumn('order_waybill', 'with_packing_list');
        $this->dropColumn('order_waybill', 'with_invoice');
        $this->dropColumn('order_waybill', 'with_invoice_facture');
        $this->dropColumn('order_waybill', 'consignment_packaging_type');
        $this->dropColumn('order_waybill', 'consignment_places_count');
        $this->dropColumn('order_waybill', 'weight_determining_method');
        $this->dropColumn('order_waybill', 'consignment_code');
        $this->dropColumn('order_waybill', 'consignment_container_number');
        $this->dropColumn('order_waybill', 'consignment_classification');
        $this->dropColumn('order_waybill', 'consignment_gross_weight');

        $this->addColumn('waybill', 'consignment_short_description', $this->string());
        $this->addColumn('waybill', 'with_packing_list', $this->boolean()->unsigned());
        $this->addColumn('waybill', 'with_invoice', $this->boolean()->unsigned());
        $this->addColumn('waybill', 'with_invoice_facture', $this->boolean()->unsigned());
        $this->addColumn('waybill', 'consignment_packaging_type', $this->string());
        $this->addColumn('waybill', 'consignment_places_count', $this->string());
        $this->addColumn('waybill', 'weight_determining_method', $this->tinyInteger()->unsigned());
        $this->addColumn('waybill', 'consignment_code', $this->string());
        $this->addColumn('waybill', 'consignment_container_number', $this->string());
        $this->addColumn('waybill', 'consignment_classification', $this->string());
        $this->addColumn('waybill', 'consignment_gross_weight', $this->float(2));
    }
}
