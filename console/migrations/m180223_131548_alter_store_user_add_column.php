<?php

use console\components\db\Migration;
use yii\db\Schema;

class m180223_131548_alter_store_user_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('store_user', 'store_company_id', $this->integer()->after('status'));

        $this->addForeignKey(
            'FK_storeUser_storeCompany',
            'store_user',
            'store_company_id',
            'store_company',
            'id',
            'SET NULL'
        );

        $this->execute("
            UPDATE {{store_user}} AS {{user}}
            LEFT JOIN (
                SELECT [[store_company_id]], [[store_user_id]]
                FROM {{store_user_company}}
                WHERE [[role_id]] = 1 AND [[status]] = 10
                GROUP BY [[store_user_id]]
            ) AS {{link}} ON {{link}}.[[store_user_id]] = {{user}}.[[id]]
            SET {{user}}.[[store_company_id]] = {{link}}.[[store_company_id]]
        ");
    }

    public function safeDown()
    {
        $this->dropForeignKey('FK_storeUser_storeCompany', 'store_user');

        $this->dropColumn('store_user', 'store_company_id');
    }
}
