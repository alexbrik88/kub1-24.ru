<?php

use console\components\db\Migration;
use yii\db\Schema;

class m190118_165318_insert_registration_page_type extends Migration
{
    public $tableName = 'registration_page_type';

    public function safeUp()
    {
        $this->batchInsert($this->tableName, ['id', 'name'], [
            [12, 'Платежка'],
            [13, 'Прайс'],
            [14, 'ТТН'],
            [15, 'Лэндинг'],
        ]);
    }

    public function safeDown()
    {
        $this->delete($this->tableName, ['in', 'id', [12, 13, 14, 15]]);
    }
}
