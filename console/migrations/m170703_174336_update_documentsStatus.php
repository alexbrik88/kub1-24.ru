<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170703_174336_update_documentsStatus extends Migration
{
    public function safeUp()
    {
        $this->execute("
            UPDATE {{invoice_facture}} AS t1
            LEFT JOIN {{invoice}} ON {{invoice}}.[[id]] = {{t1}}.[[invoice_id]]
            SET {{t1}}.[[status_out_id]] = 4,
                {{t1}}.[[status_out_updated_at]] = {{invoice}}.[[invoice_status_updated_at]],
                {{t1}}.[[status_out_author_id]] = {{invoice}}.[[invoice_status_author_id]]
            WHERE {{invoice}}.[[invoice_status_id]] = 5;
        ");
        $this->execute("
            UPDATE {{act}} AS t1
            LEFT JOIN {{invoice}} ON {{invoice}}.[[id]] = {{t1}}.[[invoice_id]]
            SET {{t1}}.[[status_out_id]] = 5,
                {{t1}}.[[status_out_updated_at]] = {{invoice}}.[[invoice_status_updated_at]],
                {{t1}}.[[status_out_author_id]] = {{invoice}}.[[invoice_status_author_id]]
            WHERE {{invoice}}.[[invoice_status_id]] = 5;
        ");
        $this->execute("
            UPDATE {{packing_list}} AS t1
            LEFT JOIN {{invoice}} ON {{invoice}}.[[id]] = {{t1}}.[[invoice_id]]
            SET {{t1}}.[[status_out_id]] = 5,
                {{t1}}.[[status_out_updated_at]] = {{invoice}}.[[invoice_status_updated_at]],
                {{t1}}.[[status_out_author_id]] = {{invoice}}.[[invoice_status_author_id]]
            WHERE {{invoice}}.[[invoice_status_id]] = 5;
        ");
        $this->execute("
            UPDATE {{upd}} AS t1
            LEFT JOIN {{invoice}} ON {{invoice}}.[[id]] = {{t1}}.[[invoice_id]]
            SET {{t1}}.[[status_out_id]] = 5,
                {{t1}}.[[status_out_updated_at]] = {{invoice}}.[[invoice_status_updated_at]],
                {{t1}}.[[status_out_author_id]] = {{invoice}}.[[invoice_status_author_id]]
            WHERE {{invoice}}.[[invoice_status_id]] = 5;
        ");
    }
    
    public function safeDown()
    {
        $this->execute("
            UPDATE {{invoice_facture}} AS t1
            SET {{t1}}.[[status_out_id]] = 1;
            WHERE {{t1}}.[[status_out_id]] = 4;
        ");
        $this->execute("
            UPDATE {{act}} AS t1
            SET {{t1}}.[[status_out_id]] = 1;
            WHERE {{t1}}.[[status_out_id]] = 5;
        ");
        $this->execute("
            UPDATE {{packing_list}} AS t1
            SET {{t1}}.[[status_out_id]] = 1;
            WHERE {{t1}}.[[status_out_id]] = 5;
        ");
        $this->execute("
            UPDATE {{upd}} AS t1
            SET {{t1}}.[[status_out_id]] = 1;
            WHERE {{t1}}.[[status_out_id]] = 5;
        ");
    }
}


