<?php

use console\components\db\Migration;
use yii\db\Schema;

class m210917_130159_alter_cash_bank_flows_add_column extends Migration
{
    public function safeUp()
    {
        $this->addColumn('cash_bank_flows', 'has_tin_children', $this->tinyInteger(1)->notNull()->defaultValue(0)->after('account_id'));

        $this->execute("
            UPDATE `cash_bank_flows`
            SET `has_tin_children` = 1
            WHERE `id` IN (SELECT DISTINCT `parent_id` FROM `cash_bank_flows` WHERE `parent_id` IS NOT NULL)
        ");
    }

    public function safeDown()
    {
        $this->dropColumn('cash_bank_flows', 'has_children');
    }
}
