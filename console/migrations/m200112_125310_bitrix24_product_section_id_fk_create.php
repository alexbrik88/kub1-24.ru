<?php

use console\components\db\Migration;

class m200112_125310_bitrix24_product_section_id_fk_create extends Migration
{
    const TABLE = '{{%bitrix24_product}}';

    public function safeUp()
    {
        $this->addForeignKey(
            'bitrix24_product_section_id', self::TABLE, ['company_id', 'section_id'],
            '{{%bitrix24_section}}', ['company_id', 'id'],
            'CASCADE', 'CASCADE'
        );
    }

    public function safeDown()
    {
        $this->dropForeignKey('bitrix24_product_section_id', self::TABLE);
    }
}
