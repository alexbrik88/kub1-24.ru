<?php

use console\components\db\Migration;
use yii\db\Schema;

class m170710_071229_alter_productUnit_addColumns extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%product_unit}}', 'goods', $this->smallInteger(1)->defaultValue(false) . ' AFTER [[code_okei]]');
        $this->addColumn('{{%product_unit}}', 'services', $this->smallInteger(1)->defaultValue(false) . ' AFTER [[goods]]');

        $this->createIndex('name', '{{%product_unit}}', 'name', true);
        $this->createIndex('code_okei', '{{%product_unit}}', 'code_okei', true);
        $this->createIndex('object_guid', '{{%product_unit}}', 'object_guid', true);

        $this->update('{{%product_unit}}', [
            'goods' => 1,
        ], ['id' => [1, 2, 3, 4, 5, 6, 7, 9, 10, 13, 14, 15]]); // product units

        $this->update('{{%product_unit}}', [
            'services' => 1,
        ], ['id' => [1, 8, 9, 10, 11, 12, 13, 14, 15]]); // service units
    }
    
    public function safeDown()
    {
        $this->dropColumn('{{%product_unit}}', 'goods');
        $this->dropColumn('{{%product_unit}}', 'services');
    }
}
