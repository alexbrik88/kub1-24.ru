<?php

use yii\db\Schema;
use yii\db\Migration;

class m151126_074618_insert_trialSubscribeForAll extends Migration
{
    public $tSubscribe = 'service_subscribe';

    public function safeUp()
    {
        $this->execute("
            INSERT
                INTO {$this->tSubscribe} (company_id, tariff_id, created_at, updated_at, status_id)
                SELECT company.id, 4 /*trial*/, UNIX_TIMESTAMP(), UNIX_TIMESTAMP(), 2 /*payed*/
            FROM company
            ");
    }

    public function safeDown()
    {
        $this->delete($this->tSubscribe, 'tariff_id = 4');
    }
}