<?php

use yii\db\Schema;
use yii\db\Migration;

class m150511_073018_create_payment_order extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('payment_order', [
            'id' => Schema::TYPE_PK,
            'company_id' => Schema::TYPE_INTEGER . ' COMMENT "Присутствует, если ПП не связано со счётом (=0)"',
            'created_at' => Schema::TYPE_DATETIME . ' NOT NULL',
            'document_author_id' => Schema::TYPE_INTEGER . ' NOT NULL COMMENT "Автор"',
            'document_number' => Schema::TYPE_STRING . ' COMMENT "Номер платёжного поручения"',
            'document_date' => Schema::TYPE_DATETIME . ' COMMENT "Дата платёжного поручения"',
            'relation_with_in_invoice' => Schema::TYPE_BOOLEAN . ' COMMENT "Связано ли ПП с вх. счётом (1 - да, 0 - нет)"',
            'invoice_id' => Schema::TYPE_INTEGER . ' COMMENT "Присутствует, если ПП связана со счётом"',
            'contractor_name' => Schema::TYPE_STRING . ' COMMENT "Присутствует, если ПП не связано со счётом"',
            'contractor_inn' => Schema::TYPE_INTEGER . ' COMMENT "Присутствует, если ПП не связано со счётом"',
            'contractor_bank_name' => Schema::TYPE_STRING . ' COMMENT "Присутствует, если ПП не связано со счётом"',
            'contractor_current_account' => Schema::TYPE_INTEGER . ' COMMENT "Присутствует, если ПП не связано со счётом"',
            'contractor_corresponding_account' => Schema::TYPE_INTEGER . ' COMMENT "Присутствует, если ПП не связано со счётом"',
            'contractor_bik' => Schema::TYPE_INTEGER . ' COMMENT "Присутствует, если ПП не связано со счётом"',
            'contractor_kpp' => Schema::TYPE_INTEGER . ' COMMENT "Присутствует, если ПП не связано со счётом"',
            'sum' => Schema::TYPE_FLOAT. ' COMMENT "Присутствует, если ПП не связано со счётом"',
            'sum_in_words' => Schema::TYPE_STRING . ' COMMENT "Присутствует, если ПП не связано со счётом"',
            'ranking_of_payment' => Schema::TYPE_INTEGER . ' COMMENT "Очерёдность платежа"',
            'operation_type_id' => Schema::TYPE_INTEGER,
            'purpose_of_payment_for' => Schema::TYPE_STRING . ' COMMENT "Присутствует, если ПП связано со счётом"',
            'purpose_of_payment' => Schema::TYPE_STRING . ' COMMENT "Присутствует, если ПП не связано со счётом"',
            'presence_status_budget_payment' => Schema::TYPE_BOOLEAN . ' COMMENT "Значения переменной: 1 – есть; 0 – нет. Статус может быть только ПП не связано со счётом"',
            'taxpayers_status_id' => Schema::TYPE_INTEGER . ' COMMENT "Присутствует, если ПП имеет статус «Бюджетный платёж»"',
            'kbk' => Schema::TYPE_INTEGER . ' COMMENT "Присутствует, если ПП имеет статус «Бюджетный платёж»"',
            'oktmo_code' => Schema::TYPE_INTEGER . ' COMMENT "Присутствует, если ПП имеет статус «Бюджетный платёж»"',
            'payment_details_id' => Schema::TYPE_INTEGER . ' COMMENT "Присутствует, если ПП имеет статус «Бюджетный платёж»"',
            'tax_period_code' => Schema::TYPE_STRING . ' COMMENT "Присутствует, если ПП имеет статус «Бюджетный платёж»"',
            'document_number_budget_payment' => Schema::TYPE_STRING . ' COMMENT "Присутствует, если ПП имеет статус «Бюджетный платёж»"',
            'document_date_budget_payment' => Schema::TYPE_DATETIME . ' COMMENT "Присутствует, если ПП имеет статус «Бюджетный платёж»"',
            'payment_type_id' => Schema::TYPE_INTEGER . ' COMMENT "Присутствует, если ПП имеет статус «Бюджетный платёж»"',
            'uin_code' => Schema::TYPE_INTEGER . ' COMMENT "Присутствует, если ПП имеет статус «Бюджетный платёж»"',
        ], $tableOptions);

        $this->addForeignKey('payment_order_to_payment_type', 'payment_order', 'payment_type_id', 'payment_type', 'id');
        $this->addForeignKey('payment_order_to_payment_details', 'payment_order', 'payment_details_id', 'payment_details', 'id');
        $this->addForeignKey('payment_order_to_taxpayers_status', 'payment_order', 'taxpayers_status_id', 'taxpayers_status', 'id');
        $this->addForeignKey('payment_order_to_operation_type', 'payment_order', 'operation_type_id', 'operation_type', 'id');
        $this->addForeignKey('payment_order_to_invoice', 'payment_order', 'invoice_id', 'invoice', 'id');
        $this->addForeignKey('payment_order_to_employee', 'payment_order', 'document_author_id', 'employee', 'id');
        $this->addForeignKey('payment_order_to_company', 'payment_order', 'company_id', 'company', 'id');
    }
    
    public function safeDown()
    {
        $this->dropForeignKey('payment_order_to_payment_type', 'payment_order');
        $this->dropForeignKey('payment_order_to_payment_details', 'payment_order');
        $this->dropForeignKey('payment_order_to_taxpayers_status', 'payment_order');
        $this->dropForeignKey('payment_order_to_operation_type', 'payment_order');
        $this->dropForeignKey('payment_order_to_invoice', 'payment_order');
        $this->dropForeignKey('payment_order_to_employee', 'payment_order');
        $this->dropForeignKey('payment_order_to_company', 'payment_order');

        $this->dropTable('payment_order');
    }
}
