<?php

use console\components\db\Migration;
use yii\db\Schema;

class m160104_081026_alter_promoCode_addColumn_name extends Migration
{
    public $tPromoCode = 'service_payment_promo_code';

    public function safeUp()
    {
        $this->addColumn($this->tPromoCode, 'name', $this->string(45)->defaultValue(null) . ' AFTER `code`');
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tPromoCode, 'name');
    }
}


