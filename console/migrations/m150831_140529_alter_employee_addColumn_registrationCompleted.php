<?php

use yii\db\Schema;
use yii\db\Migration;

class m150831_140529_alter_employee_addColumn_registrationCompleted extends Migration
{
    public $tableEmployee = 'employee';

    public function safeUp()
    {
        $this->addColumn($this->tableEmployee, 'is_registration_completed', $this->boolean()->defaultValue(1));
    }
    
    public function safeDown()
    {
        $this->dropColumn($this->tableEmployee, 'is_registration_completed');
    }
}
