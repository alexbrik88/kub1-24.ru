<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 08.09.2017
 * Time: 4:29
 */

namespace console\components\chat;

use common\models\employee\Employee;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;
use Yii;
use yii\db\Exception;

/**
 * Class SocketServer
 * @package console\components\chat
 */
class SocketServer implements MessageComponentInterface
{
    /**
     * @var \SplObjectStorage
     */
    protected $clients;

    /**
     *
     */
    public function __construct()
    {
        $this->clients = new \SplObjectStorage;
    }

    /**
     * @param ConnectionInterface $conn
     */
    public function onOpen(ConnectionInterface $conn)
    {
        $this->clients->attach($conn);
        Yii::$app->db->close();
        echo "New connection! ({$conn->resourceId})\n";
    }

    /**
     * @param ConnectionInterface $from
     * @param string $msg
     * @throws Exception
     */
    public function onMessage(ConnectionInterface $from, $msg)
    {
        if (Yii::$app->db->isActive) {
            Yii::$app->db->close();
        }

        Yii::$app->db->open();
        $data = json_decode($msg, true);
        if (isset($data[2], $data[0]) && $data[0] === 2 && $data[2] === 'send') {
            $data = $data[3];
        }

        $message = $data['message'] ?? null;
        if ($message !== null) {
            switch ($message['type']) {
                case 'finishedJob':
                    $employee = Employee::findOne($message['employeeId']);
                    foreach ($this->clients as $client) {
                        if ($client->resourceId === $employee->socket_id) {
                            $client->send(json_encode($data));
                        }
                    }
                    break;
                case 'connect':
                    $employee = Employee::findOne($message['employeeId']);
                    if ($employee !== null) {
                        $employee->socket_id = $from->resourceId;
                        $employee->save(true, ['socket_id']);
                    }
                    break;
            }
        }

        Yii::$app->db->close();
    }

    /**
     * @param ConnectionInterface $conn
     */
    public function onClose(ConnectionInterface $conn)
    {
        $this->clients->detach($conn);
        echo "Connection {$conn->resourceId} has disconnected\n";
    }

    /**
     * @param ConnectionInterface $conn
     * @param \Exception $e
     */
    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        echo "An error has occurred: {$e->getMessage()}\n";
        $conn->close();
    }
}