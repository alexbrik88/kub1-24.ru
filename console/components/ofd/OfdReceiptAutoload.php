<?php
namespace console\components\ofd;

use Yii;
use common\models\Company;
use common\models\ofd\Ofd;
use common\models\ofd\OfdStore;
use common\models\ofd\OfdUser;
use yii\base\Component;
use yii\db\Query;

class OfdReceiptAutoload extends Component
{
    public function process($company_id = null)
    {
        set_time_limit(7200);
        Yii::$app->db->createCommand('SET session wait_timeout=28800')->execute();
        Yii::$app->db->createCommand('SET session interactive_timeout=28800')->execute();
        $queryLimit = 1000;
        $queryOffset = 0;

        $query = OfdStore::find()
        ->alias('ofd_store')
        ->distinct()
        ->select([
            'ofd_store.id',
            'ofd_store.ofd_id',
        ])
        ->leftJoin([
            'ofd' => Ofd::tableName(),
        ], '{{ofd}}.[[id]] = {{ofd_store}}.[[ofd_id]]')
        ->leftJoin([
            'ofd_user' => OfdUser::tableName(),
        ], '{{ofd_user}}.[[company_id]] = {{ofd_store}}.[[company_id]] AND {{ofd_user}}.[[ofd_id]] = {{ofd_store}}.[[ofd_id]]')
        ->andWhere([
            'ofd.is_active' => true,
            'ofd_store.autoload_mode' => Ofd::AUTOLOAD_MODE_DAILY,
        ])
        ->andWhere([
            'not',
            ['ofd_user.auth_key' => null],
        ])
        ->andFilterWhere([
            'ofd_store.company_id' => $company_id,
        ])
        ->orderBy([
            'ofd_store.id' => SORT_ASC,
        ]);

        do {
            $queryResult = (new Query)->select([
                'id',
                'ofd_id',
            ])->from([
                't' => $query,
            ])->limit($queryLimit)->offset($queryOffset)->all();

            foreach ($queryResult as $row) {
                $this->createJob($row['id'], $row['ofd_id']);
            }

            $queryOffset += $queryLimit;
        } while (count($queryResult) == $queryLimit);
    }

    protected function createJob($storeId, $ofdId) : void
    {
        $jobClass = Ofd::$receiptAutoloadJobClass[$ofdId] ?? null;
        if ($jobClass && class_exists($jobClass)) {
            Yii::$app->queue->push(new $jobClass([
                'storeId' => $storeId,
            ]));
        }
    }
}
