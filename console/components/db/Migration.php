<?php
/**
 * Created by PhpStorm.
 * User: jack_savich
 * Date: 17.08.15
 * Time: 15:14
 */
namespace console\components\db;

/**
 * Class Migration
 * @package console\compoents\db
 *
 * @property string $tableOptions
 */
class Migration extends \yii\db\Migration
{
    protected $tableName;
    private $_tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';

    public function getTableOptions()
    {
        return $this->db->driverName === 'mysql' ? $this->_tableOptions : null;
    }
}