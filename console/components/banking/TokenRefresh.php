<?php
namespace console\components\banking;

use frontend\modules\cash\modules\banking\components\Banking;
use Yii;
use yii\base\Component;

class TokenRefresh extends Component
{
    public function process($company_id = null)
    {
        set_time_limit(7200);
        Yii::$app->db->createCommand('SET session wait_timeout=28800')->execute();
        Yii::$app->db->createCommand('SET session interactive_timeout=28800')->execute();

        foreach (Banking::$modelClassArray as $class) {
            if (method_exists($class, 'bankingTokenRefresh')) {
                $class::bankingTokenRefresh($company_id);
            }
        }
    }
}
