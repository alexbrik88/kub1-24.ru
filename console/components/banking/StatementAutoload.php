<?php
namespace console\components\banking;

use common\models\bank\StatementAutoloadMode;
use common\models\cash\CashBankStatementUpload;
use common\models\Company;
use common\models\company\CheckingAccountant;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\models\AbstractBankModel;
use frontend\modules\cash\modules\banking\modules\sberbank\models\BankModel as SberbankModel;
use DateTime;
use Yii;
use yii\base\Component;

class StatementAutoload extends Component
{
    public function process($company_id = null)
    {
        set_time_limit(7200);
        Yii::$app->db->createCommand('SET session wait_timeout=28800')->execute();
        Yii::$app->db->createCommand('SET session interactive_timeout=28800')->execute();

        $modeArray = StatementAutoloadMode::find()->where(['like', 'week_days', date('N')])->indexBy('id')->all();
        $query = CheckingAccountant::find()->alias('account')->select('account.id')->leftJoin([
            'loaded' => CashBankStatementUpload::tableName(),
        ], '{{account}}.[[id]] = {{loaded}}.[[account_id]] AND DATE(FROM_UNIXTIME({{loaded}}.[[created_at]])) = :date', [
            ':date' => date('Y-m-d'),
        ])->where([
            'account.autoload_mode_id' => array_keys($modeArray),
            'loaded.id' => null,
        ]);

        if ($company_id !== null) {
            $query->andWhere(['company_id' => $company_id]);
        }
        $idArray = $query->column();

        if ($idArray) {
            $bankingArray = [];

            foreach ($idArray as $id) {
                $account = CheckingAccountant::findOne($id);
                $moduleAlias = Banking::aliasByBik($account->bik);
                if ($moduleAlias == SberbankModel::ALIAS && !$account->company->getHasPaidActualSubscription()) {
                    $account->updateAttributes(['autoload_mode_id' => null]);

                    continue;
                }
                $modelClass = "frontend\\modules\\cash\\modules\\banking\\modules\\{$moduleAlias}\\models\\BankModel";
                if ($account->company && class_exists($modelClass)) {
                    $bankingArray[$id] = new $modelClass($account->company, [
                        'scenario' => AbstractBankModel::SCENARIO_AUTOLOAD,
                        'account_id' => $account->id,
                    ]);
                    try {
                        if ($bankingArray[$id]->getHasAutoload()) {
                            $bankingArray[$id]->start_date = $modeArray[$account->autoload_mode_id]->previousDate->format('d.m.Y');

                            if ($bankingArray[$id]->needPreRequest) { // Если нужен предварительный запрос
                                if (!$bankingArray[$id]->autoloadPreRequest()) {
                                    unset($bankingArray[$id]);
                                }
                            } else {
                                $bankingArray[$id]->autoloadStatement();
                                unset($bankingArray[$id]);
                            }
                        } else {
                            unset($bankingArray[$id]);
                        }
                    } catch (\Exception $e) {
                        unset($bankingArray[$id]);
                    }
                }
            }

            if ($bankingArray) {
                sleep(60);
                foreach ($bankingArray as $banking) {
                    $banking->autoloadStatement();
                }
            }
        }
    }
}
