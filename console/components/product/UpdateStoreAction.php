<?php

namespace console\components\product;

use common\models\Company;
use Yii;
use yii\base\Model;
use yii\helpers\Url;
use yii\web\ServerErrorHttpException;

/**
 * UpdateStoreAction
 *
 * Пересчет остатков товара компании
 */
class UpdateStoreAction extends \yii\base\Action
{
    /**
     * Runs the action.
     * @param  integer|string $id Company id|'all'
     */
    public function run($id)
    {
        $id == 'all' ? $this->updateAll() : $this->updateCompany($id);

        $this->controller->stdout("Done\n");
    }

    /**
     * @param  integer $id
     * @return int number of rows affected by the execution.
     * @throws Exception execution failed
     */
    public function updateCompany($id)
    {
        $command = Yii::$app->db->createCommand("
            UPDATE {{product_store}}
            INNER JOIN {{store}} ON {{store}}.[[id]] = {{product_store}}.[[store_id]] AND {{store}}.[[is_main]] = true
            LEFT JOIN (
                SELECT {{product_store}}.[[product_id]], SUM({{product_store}}.[[initial_quantity]]) AS [[total]]
                FROM {{product_store}}
                LEFT JOIN {{store}} ON {{product_store}}.[[store_id]] = {{store}}.[[id]]
                WHERE {{store}}.[[company_id]] = :cid
                GROUP BY [[product_id]]
            ) {{init}} ON {{init}}.[[product_id]] = {{product_store}}.[[product_id]]
            LEFT JOIN (
                SELECT {{product_store}}.[[product_id]], SUM({{product_store}}.[[quantity]]) AS [[total]]
                FROM {{product_store}}
                LEFT JOIN {{store}} ON {{product_store}}.[[store_id]] = {{store}}.[[id]]
                WHERE {{store}}.[[company_id]] = :cid
                AND {{store}}.[[is_main]] = false
                GROUP BY {{product_store}}.[[product_id]]
            ) {{other}} ON {{other}}.[[product_id]] = {{product_store}}.[[product_id]]
            LEFT JOIN (
                SELECT {{order_packing_list}}.[[product_id]], SUM({{order_packing_list}}.[[quantity]]) AS [[total]]
                FROM {{order_packing_list}}
                LEFT JOIN {{packing_list}} ON {{order_packing_list}}.[[packing_list_id]] = {{packing_list}}.[[id]]
                LEFT JOIN {{invoice}} ON {{invoice}}.[[id]] = {{packing_list}}.[[invoice_id]]
                WHERE {{invoice}}.[[company_id]] = :cid
                AND {{invoice}}.[[type]] = 1
                AND {{invoice}}.[[is_deleted]] = false
                AND {{invoice}}.[[invoice_status_id]] NOT IN (5, 9)
                GROUP BY {{order_packing_list}}.[[product_id]]
            ) {{buy1}} ON {{buy1}}.[[product_id]] = {{product_store}}.[[product_id]]
            LEFT JOIN (
                SELECT {{order_upd}}.[[product_id]], SUM({{order_upd}}.[[quantity]]) AS [[total]]
                FROM {{order_upd}}
                LEFT JOIN {{upd}} ON {{order_upd}}.[[upd_id]] = {{upd}}.[[id]]
                LEFT JOIN {{invoice}} ON {{invoice}}.[[id]] = {{upd}}.[[invoice_id]]
                WHERE {{invoice}}.[[company_id]] = :cid
                AND {{invoice}}.[[type]] = 1
                AND {{invoice}}.[[is_deleted]] = false
                AND {{invoice}}.[[invoice_status_id]] NOT IN (5, 9)
                GROUP BY {{order_upd}}.[[product_id]]
            ) {{buy2}} ON {{buy2}}.[[product_id]] = {{product_store}}.[[product_id]]
            LEFT JOIN (
                SELECT {{order_packing_list}}.[[product_id]], SUM({{order_packing_list}}.[[quantity]]) AS [[total]]
                FROM {{order_packing_list}}
                LEFT JOIN {{packing_list}} ON {{order_packing_list}}.[[packing_list_id]] = {{packing_list}}.[[id]]
                LEFT JOIN {{invoice}} ON {{invoice}}.[[id]] = {{packing_list}}.[[invoice_id]]
                WHERE {{invoice}}.[[company_id]] = :cid
                AND {{invoice}}.[[type]] = 2
                AND {{invoice}}.[[is_deleted]] = false
                AND {{invoice}}.[[invoice_status_id]] NOT IN (5, 9)
                AND {{packing_list}}.[[status_out_id]] <> 5
                GROUP BY {{order_packing_list}}.[[product_id]]
            ) {{sell1}} ON {{sell1}}.[[product_id]] = {{product_store}}.[[product_id]]
            LEFT JOIN (
                SELECT {{order_upd}}.[[product_id]], SUM({{order_upd}}.[[quantity]]) AS [[total]]
                FROM {{order_upd}}
                LEFT JOIN {{upd}} ON {{order_upd}}.[[upd_id]] = {{upd}}.[[id]]
                LEFT JOIN {{invoice}} ON {{invoice}}.[[id]] = {{upd}}.[[invoice_id]]
                WHERE {{invoice}}.[[company_id]] = :cid
                AND {{invoice}}.[[type]] = 2
                AND {{invoice}}.[[is_deleted]] = false
                AND {{invoice}}.[[invoice_status_id]] NOT IN (5, 9)
                AND {{upd}}.[[status_out_id]] <> 5
                GROUP BY {{order_upd}}.[[product_id]]
            ) {{sell2}} ON {{sell2}}.[[product_id]] = {{product_store}}.[[product_id]]
            SET {{product_store}}.[[quantity]] = {{init}}.[[total]]
                                    + IFNULL({{buy1}}.[[total]], 0)
                                    + IFNULL({{buy2}}.[[total]], 0)
                                    - IFNULL({{sell1}}.[[total]], 0)
                                    - IFNULL({{sell2}}.[[total]], 0)
                                    - IFNULL({{other}}.[[total]], 0)
            WHERE {{store}}.[[company_id]] = :cid
        ", [':cid' => $id]);

        return $command->execute();
    }

    /**
     * @return int number of rows affected by the execution.
     * @throws Exception execution failed
     */
    public function updateAll()
    {
        $command = Yii::$app->db->createCommand("
            UPDATE {{product_store}}
            INNER JOIN {{store}} ON {{store}}.[[id]] = {{product_store}}.[[store_id]] AND {{store}}.[[is_main]] = true
            LEFT JOIN (
                SELECT {{product_store}}.[[product_id]], SUM({{product_store}}.[[initial_quantity]]) AS [[total]]
                FROM {{product_store}}
                GROUP BY [[product_id]]
            ) {{init}} ON {{init}}.[[product_id]] = {{product_store}}.[[product_id]]
            LEFT JOIN (
                SELECT {{product_store}}.[[product_id]], SUM({{product_store}}.[[quantity]]) AS [[total]]
                FROM {{product_store}}
                LEFT JOIN {{store}} ON {{product_store}}.[[store_id]] = {{store}}.[[id]]
                WHERE {{store}}.[[is_main]] = false
                GROUP BY {{product_store}}.[[product_id]]
            ) {{other}} ON {{other}}.[[product_id]] = {{product_store}}.[[product_id]]
            LEFT JOIN (
                SELECT {{order_packing_list}}.[[product_id]], SUM({{order_packing_list}}.[[quantity]]) AS [[total]]
                FROM {{order_packing_list}}
                LEFT JOIN {{packing_list}} ON {{order_packing_list}}.[[packing_list_id]] = {{packing_list}}.[[id]]
                LEFT JOIN {{invoice}} ON {{invoice}}.[[id]] = {{packing_list}}.[[invoice_id]]
                WHERE {{invoice}}.[[type]] = 1
                AND {{invoice}}.[[is_deleted]] = false
                AND {{invoice}}.[[invoice_status_id]] NOT IN (5, 9)
                GROUP BY {{order_packing_list}}.[[product_id]]
            ) {{buy1}} ON {{buy1}}.[[product_id]] = {{product_store}}.[[product_id]]
            LEFT JOIN (
                SELECT {{order_upd}}.[[product_id]], SUM({{order_upd}}.[[quantity]]) AS [[total]]
                FROM {{order_upd}}
                LEFT JOIN {{upd}} ON {{order_upd}}.[[upd_id]] = {{upd}}.[[id]]
                LEFT JOIN {{invoice}} ON {{invoice}}.[[id]] = {{upd}}.[[invoice_id]]
                WHERE {{invoice}}.[[type]] = 1
                AND {{invoice}}.[[is_deleted]] = false
                AND {{invoice}}.[[invoice_status_id]] NOT IN (5, 9)
                GROUP BY {{order_upd}}.[[product_id]]
            ) {{buy2}} ON {{buy2}}.[[product_id]] = {{product_store}}.[[product_id]]
            LEFT JOIN (
                SELECT {{order_packing_list}}.[[product_id]], SUM({{order_packing_list}}.[[quantity]]) AS [[total]]
                FROM {{order_packing_list}}
                LEFT JOIN {{packing_list}} ON {{order_packing_list}}.[[packing_list_id]] = {{packing_list}}.[[id]]
                LEFT JOIN {{invoice}} ON {{invoice}}.[[id]] = {{packing_list}}.[[invoice_id]]
                WHERE {{invoice}}.[[type]] = 2
                AND {{invoice}}.[[is_deleted]] = false
                AND {{invoice}}.[[invoice_status_id]] NOT IN (5, 9)
                AND {{packing_list}}.[[status_out_id]] <> 5
                GROUP BY {{order_packing_list}}.[[product_id]]
            ) {{sell1}} ON {{sell1}}.[[product_id]] = {{product_store}}.[[product_id]]
            LEFT JOIN (
                SELECT {{order_upd}}.[[product_id]], SUM({{order_upd}}.[[quantity]]) AS [[total]]
                FROM {{order_upd}}
                LEFT JOIN {{upd}} ON {{order_upd}}.[[upd_id]] = {{upd}}.[[id]]
                LEFT JOIN {{invoice}} ON {{invoice}}.[[id]] = {{upd}}.[[invoice_id]]
                WHERE {{invoice}}.[[type]] = 2
                AND {{invoice}}.[[is_deleted]] = false
                AND {{invoice}}.[[invoice_status_id]] NOT IN (5, 9)
                AND {{upd}}.[[status_out_id]] <> 5
                GROUP BY {{order_upd}}.[[product_id]]
            ) {{sell2}} ON {{sell2}}.[[product_id]] = {{product_store}}.[[product_id]]
            SET {{product_store}}.[[quantity]] = {{init}}.[[total]]
                                    + IFNULL({{buy1}}.[[total]], 0)
                                    + IFNULL({{buy2}}.[[total]], 0)
                                    - IFNULL({{sell1}}.[[total]], 0)
                                    - IFNULL({{sell2}}.[[total]], 0)
                                    - IFNULL({{other}}.[[total]], 0)
        ");

        return $command->execute();
    }
}
