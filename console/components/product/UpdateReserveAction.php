<?php

namespace console\components\product;

use common\models\document\status;
use common\models\product\Product;
use frontend\models\Documents;
use Yii;

/**
 * UpdateReserveAction
 *
 * Пересчет резервов товара компании
 */
class UpdateReserveAction extends \yii\base\Action
{
    /**
     * Runs the action.
     * @param  integer|string $id Company id|'all'
     */
    public function run($id)
    {
        $id == 'all' ? $this->updateAll() : $this->updateCompany($id);

        $this->controller->stdout("Done\n");
    }

    /**
     * @param  integer $id
     * @return int number of rows affected by the execution.
     * @throws Exception execution failed
     */
    public function updateCompany($id)
    {
        $command = Yii::$app->db->createCommand("
            UPDATE {{order}}
            LEFT JOIN {{invoice}} ON {{invoice}}.[[id]] = {{order}}.[[invoice_id]]
            LEFT JOIN {{product}} ON {{product}}.[[id]] = {{order}}.[[product_id]]
            LEFT JOIN (
                SELECT {{sold_order}}.[[order_id]], SUM({{sold_order}}.[[sold_quantity]]) AS [[quantity]]
                FROM (
                    SELECT {{order_packing_list}}.[[order_id]], {{order_packing_list}}.[[quantity]] AS [[sold_quantity]]
                        FROM {{order_packing_list}}
                        LEFT JOIN {{packing_list}} ON {{packing_list}}.[[id]] = {{order_packing_list}}.[[packing_list_id]]
                        WHERE {{packing_list}}.[[status_out_id]] <> :rejectedPL
                    UNION ALL
                    SELECT {{order_upd}}.[[order_id]], {{order_upd}}.[[quantity]] AS [[sold_quantity]]
                        FROM {{order_upd}}
                        LEFT JOIN {{upd}} ON {{upd}}.[[id]] = {{order_upd}}.[[upd_id]]
                        WHERE {{upd}}.[[status_out_id]] <> :rejectedUPD
                ) {{sold_order}}
                GROUP BY [[order_id]]
            ) {{sold}} ON {{sold}}.[[order_id]] = {{order}}.[[id]]
            SET {{order}}.[[reserve]] = IF(
                {{product}}.[[production_type]] = :goods
                AND {{invoice}}.[[type]] = :type
                AND {{invoice}}.[[invoice_status_id]] <> :auto
                AND {{invoice}}.[[is_deleted]] = 0,

                GREATEST(
                    0,
                    {{order}}.[[quantity]] - COALESCE({{sold}}.[[quantity]], 0)
                ),

                0
            )
            WHERE {{invoice}}.[[company_id]] = :id
        ", [
            ':id' => $id,
            ':goods' => Product::PRODUCTION_TYPE_GOODS,
            ':type' => Documents::IO_TYPE_OUT,
            ':auto' => status\InvoiceStatus::STATUS_AUTOINVOICE,
            ':rejectedPL' => status\PackingListStatus::STATUS_REJECTED,
            ':rejectedUPD' => status\UpdStatus::STATUS_REJECTED,

        ]);
        echo $command->rawSql."\n";

        return $command->execute();
    }

    /**
     * @return int number of rows affected by the execution.
     * @throws Exception execution failed
     */
    public function updateAll()
    {
        $command = Yii::$app->db->createCommand("
            UPDATE {{order}}
            LEFT JOIN {{invoice}} ON {{invoice}}.[[id]] = {{order}}.[[invoice_id]]
            LEFT JOIN {{product}} ON {{product}}.[[id]] = {{order}}.[[product_id]]
            LEFT JOIN (
                SELECT {{sold_order}}.[[order_id]], SUM({{sold_order}}.[[sold_quantity]]) AS [[quantity]]
                FROM (
                    SELECT {{order_packing_list}}.[[order_id]], {{order_packing_list}}.[[quantity]] AS [[sold_quantity]]
                        FROM {{order_packing_list}}
                        LEFT JOIN {{packing_list}} ON {{packing_list}}.[[id]] = {{order_packing_list}}.[[packing_list_id]]
                        WHERE {{packing_list}}.[[status_out_id]] <> :rejectedPL
                    UNION ALL
                    SELECT {{order_upd}}.[[order_id]], {{order_upd}}.[[quantity]] AS [[sold_quantity]]
                        FROM {{order_upd}}
                        LEFT JOIN {{upd}} ON {{upd}}.[[id]] = {{order_upd}}.[[upd_id]]
                        WHERE {{upd}}.[[status_out_id]] <> :rejectedUPD
                ) {{sold_order}}
                GROUP BY [[order_id]]
            ) {{sold}} ON {{sold}}.[[order_id]] = {{order}}.[[id]]
            SET {{order}}.[[reserve]] = IF(
                {{product}}.[[production_type]] = :goods
                AND {{invoice}}.[[type]] = :type
                AND {{invoice}}.[[invoice_status_id]] <> :auto
                AND {{invoice}}.[[is_deleted]] = 0,

                GREATEST(
                    0,
                    {{order}}.[[quantity]] - COALESCE({{sold}}.[[quantity]], 0)
                ),

                0
            )
        ", [
            ':goods' => Product::PRODUCTION_TYPE_GOODS,
            ':type' => Documents::IO_TYPE_OUT,
            ':auto' => status\InvoiceStatus::STATUS_AUTOINVOICE,
            ':rejectedPL' => status\PackingListStatus::STATUS_REJECTED,
            ':rejectedUPD' => status\UpdStatus::STATUS_REJECTED,

        ]);

        return $command->execute();
    }
}
