<?php
namespace console\components\service;

use common\components\pdf\PdfRenderer;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;
use DateTime;
use frontend\models\Documents;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use Yii;
use yii\base\Component;

class RemindContractor extends Component
{
    public static function process()
    {
        Yii::$app->urlManager->baseUrl = '/';
        Yii::$app->urlManager->hostInfo = Yii::$app->params['serviceSiteLk'];
        Yii::setAlias('@webroot', '@frontend/web');
        Yii::setAlias('@web', '@frontend/web');

        // invoice payment limit date
        $date = new DateTime('+1 day');

        $invoiceIdArray = Invoice::find()
            ->select(['id'])
            ->where([
                'type' => Documents::IO_TYPE_OUT,
                'is_deleted' => false,
                'remind_contractor' => true,
                'payment_limit_date' => $date->format('Y-m-d'),
                'invoice_status_id' => [
                    InvoiceStatus::STATUS_CREATED,
                    InvoiceStatus::STATUS_SEND,
                    InvoiceStatus::STATUS_PAYED_PARTIAL,
                ],
            ])
            ->column();

        $count = 0;
        if ($invoiceIdArray) {
            foreach ($invoiceIdArray as $invoiceId) {
                if ($model = Invoice::findOne($invoiceId)) {
                    $sender = $model->company->employeeChief;
                    if ($model->contractor->director_email) {
                        $sendTo = $model->contractor->director_email;
                    } elseif ($model->contractor->chief_accountant_email) {
                        $sendTo = $model->contractor->chief_accountant_email;
                    } elseif ($model->contractor->contact_email) {
                        $sendTo = $model->contractor->contact_email;
                    }
                    if ($sender && !empty($sendTo)) {
                        $mail = \Yii::$app->mailer
                            ->compose([
                                'html' => 'system/documents/invoice-out/html',
                                'text' => 'system/documents/invoice-out/text',
                            ], [
                                'model' => $model,
                                'employee' => $sender,
                                'toEmail' => $sendTo,
                                'subject' => $model->emailSubject,
                            ])
                            ->setFrom([\Yii::$app->params['emailList']['docs'] => $sender->getFio()])
                            ->setReplyTo([$sender->email => $sender->getFio(true)])
                            ->setSubject($model->emailSubject)
                            ->attachContent(Invoice::getRenderer(null, $model, PdfRenderer::DESTINATION_STRING)->output(false), [
                                'fileName' => $model->pdfFileName,
                                'contentType' => 'application/pdf',
                            ])
                            ->setTo($sendTo);

                        if ($model->company->autoact->send_together && $model->acts) {
                            foreach ($model->acts as $act) {
                                $mail->attachContent(Act::getRenderer(null, $act, PdfRenderer::DESTINATION_STRING)->output(false), [
                                    'fileName' => $act->pdfFileName,
                                    'contentType' => 'application/pdf',
                                ]);
                            }
                        }

                        if ($mail->send() && ++$count &&
                            in_array($model->invoice_status_id, [InvoiceStatus::STATUS_CREATED, InvoiceStatus::STATUS_SEND])
                        ) {
                            LogHelper::save(
                                $model,
                                LogEntityType::TYPE_DOCUMENT,
                                LogEvent::LOG_EVENT_UPDATE_STATUS,
                                function (Invoice $model) use ($sender) {
                                    if ($model->company->first_send_invoice_date == null) {
                                        $model->company->updateAttributes(['first_send_invoice_date' => time()]);
                                    }
                                    $model->invoice_status_id = InvoiceStatus::STATUS_SEND;
                                    $model->invoice_status_updated_at = time();
                                    $model->invoice_status_author_id = $sender->id;
                                    $model->email_messages += 1;
                                    return $model->save(true, [
                                        'invoice_status_id',
                                        'invoice_status_updated_at',
                                        'invoice_status_author_id',
                                        'email_messages',
                                    ]);
                                }
                            );
                        }
                    }
                }
            }
        }

        echo "Sent emails: {$count}\n";

        return;
    }
}
