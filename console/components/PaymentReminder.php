<?php

namespace console\components;

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;
use common\models\EmployeeCompany;
use common\models\paymentReminder\PaymentReminderMessage;
use common\models\paymentReminder\PaymentReminderMessageContractor;
use common\models\paymentReminder\Report;
use common\models\paymentReminder\Settings;
use frontend\models\Documents;
use Yii;

/**
 * Class PaymentReminder
 * @package console\components
 */
class PaymentReminder
{
    public static $defaultTime = '09:45';

    public static $queryLimit = 100;

    public static $overdueMessages = [
        PaymentReminderMessage::MESSAGE_1,
        PaymentReminderMessage::MESSAGE_2,
        PaymentReminderMessage::MESSAGE_3,
        PaymentReminderMessage::MESSAGE_4,
        PaymentReminderMessage::MESSAGE_5,
    ];

    public static $limitMessages = [
        PaymentReminderMessage::MESSAGE_6,
        PaymentReminderMessage::MESSAGE_7,
        PaymentReminderMessage::MESSAGE_8,
        PaymentReminderMessage::MESSAGE_9,
    ];

    protected $dateTime;

    /**
     * run every minute by cron
     */
    public function send()
    {
        $this->dateTime = date_create();

        $offset = 0;

        do {
            $settingsArray = Settings::find()->with('company')->andWhere([
                'send_mode' => Settings::$activeModes,
            ])->orderBy([
                'id' => SORT_ASC,
            ])->offset($offset)->limit(self::$queryLimit)->all();
            $offset += self::$queryLimit;

            foreach ($settingsArray as $settings) {
                $this->processData($settings);
            }
        } while (count($settingsArray) == self::$queryLimit);
    }

    public function processData($settings)
    {
        $query = $settings->company->getActualPaymentReminderMessages()->andWhere([
            'not', ['number' => PaymentReminderMessage::MESSAGE_10],
        ]);

        $time = $this->dateTime->format('H:i');

        if ($time == self::$defaultTime) {
            $query->andWhere([
                'or',
                ['time' => null],
                ['TIME_FORMAT([[time]], "%H:%i")' => $time],
            ]);
        } else {
            $query->andWhere([
                'TIME_FORMAT([[time]], "%H:%i")' => $time,
            ]);
        }

        $actualMessages = $query->all();

        foreach ($actualMessages as $message) {
            $message->populateRelation('company', $settings->company);
            $this->processMessage($message, $settings);
        }
    }

    public function processMessage($message, $settings)
    {
        if (in_array($message->number, self::$overdueMessages)) {
            $this->processOverdueInvoice($message, $settings);
        } elseif (in_array($message->number, self::$limitMessages)) {
            $this->processExceededLimit($message, $settings);
        }
    }

    public function selectedContractorIds($message)
    {
        return PaymentReminderMessageContractor::find()->select('contractor_id')->andWhere([
            'company_id' => $message->company_id,
            'message_'.$message->number => 1,
        ])->column();
    }

    public function overdueInvoiceQuery($date, $companyId)
    {
        return Invoice::find()->with('contractor')
            ->byIOType(Documents::IO_TYPE_OUT)
            ->byCompany($companyId)
            ->byStatus(InvoiceStatus::$payAllowed)
            ->byDeleted()
            ->andWhere([
                'invoice.payment_limit_date' => $date->format('Y-m-d'),
            ]);
    }

    public function exceededLimitQuery($date, $companyId)
    {
        return Invoice::find()
            ->byIOType(Documents::IO_TYPE_OUT)
            ->byCompany($companyId)
            ->byStatus(InvoiceStatus::$payAllowed)
            ->byDeleted()
            ->andWhere([
                '<=', 'invoice.payment_limit_date', $date->format('Y-m-d'),
            ]);
    }

    public function processOverdueInvoice($message, $settings)
    {
        if ($date = date_create(($message->number == PaymentReminderMessage::MESSAGE_1 ? '+' : '-') . $message->days . ' days')) {
            if ($settings->send_mode == Settings::SEND_BY_SUITABLE && $settings->overdue_invoice) {
                $query = $this->overdueInvoiceQuery($date, $settings->company_id);
                $excludeIds = array_filter($settings->excludeIds);
                if (!empty($excludeIds)) {
                    $query->andWhere(['not', ['invoice.contractor_id' => $excludeIds]]);
                }
            } elseif ($settings->send_mode == Settings::SEND_TO_SELECTED && ($contractorIds = $this->selectedContractorIds($message))) {
                $query = $this->overdueInvoiceQuery($date, $settings->company_id)->andWhere([
                    'invoice.contractor_id' => $contractorIds,
                ]);
            } else {
                return;
            }

            if (isset($query)) {
                $offset = 0;

                do {
                    $invoiceArray = $query->offset($offset)->limit(self::$queryLimit)->all();
                    $offset += self::$queryLimit;
                    if (!empty($invoiceArray)) {
                        foreach ($invoiceArray as $invoice) {
                            self::sendForOverdueInvoice($invoice, $message, $settings);
                        }
                    }
                } while (count($invoiceArray) == self::$queryLimit);
            }
        }
    }

    public function processExceededLimit($message, $settings)
    {
        if ($date = date_create('-' . $message->days . ' days')) {
            if ($settings->send_mode == Settings::SEND_BY_SUITABLE && $settings->exceeded_limit) {
                $query = $this->exceededLimitQuery($date, $settings->company_id);
                $excludeIds = array_filter($settings->excludeIds);
                if (!empty($excludeIds)) {
                    $query->andWhere(['not', ['invoice.contractor_id' => $excludeIds]]);
                }
            } elseif ($settings->send_mode == Settings::SEND_TO_SELECTED && ($contractorIds = $this->selectedContractorIds($message))) {
                $query = $this->exceededLimitQuery($date, $settings->company_id)->andWhere([
                    'invoice.contractor_id' => $contractorIds,
                ]);
            } else {
                return;
            }

            if (isset($query)) {
                $offset = 0;
                $query->select([
                    'invoice.contractor_id',
                    'invoiceIds' => 'GROUP_CONCAT(DISTINCT {{invoice}}.[[id]] SEPARATOR ",")',
                    'overdueAmount' => 'SUM({{invoice}}.[[remaining_amount]])',
                    'lastDate' => 'MAX({{invoice}}.[[payment_limit_date]])',
                ])->groupBy('{{invoice}}.[[contractor_id]]')->andHaving([
                    '>=', 'overdueAmount', $message->debt_sum * 100
                ])->andHaving([
                    'lastDate' => $date->format('Y-m-d')
                ])->orderBy('contractor_id')->asArray();

                do {
                    $dataArray = $query->offset($offset)->limit(self::$queryLimit)->all();
                    $offset += self::$queryLimit;
                    if (!empty($dataArray)) {
                        foreach ($dataArray as $data) {
                            self::sendForExceededLimit($data, $message, $settings);
                        }
                    }
                } while (count($dataArray) == self::$queryLimit);
            }
        }
    }

    /**
     * @param PaymentReminderMessage $message
     * @param Contractor $contractor
     * @return array
     */
    public static function getEmails($message, $contractor)
    {
        $emails = [];
        if (in_array($message->number, [PaymentReminderMessage::MESSAGE_5], PaymentReminderMessage::MESSAGE_9)) {
            foreach (explode(',', $message->for_email_list) as $oneEmail) {
                if ($oneEmail && filter_var($oneEmail, FILTER_VALIDATE_EMAIL)) {
                    $emails[] = $oneEmail;
                }
            }
        } else {
            $emails = self::getContractorEmails($message, $contractor);
        }

        return $emails;
    }

    /**
     * @param $message
     * @param $contractor
     * @return array
     */
    public static function getContractorEmails($message, $contractor)
    {
        $email = [];

        if (($message->send_for_chief || $message->send_for_all) && $contractor->director_email) {
            $email[] = $contractor->director_email;
        }
        if (($message->send_for_contact || $message->send_for_all) && $contractor->contact_email) {
            $email[] = $contractor->contact_email;
        }
        if ($message->send_for_all && $contractor->chief_accountant_email) {
            $email[] = $contractor->chief_accountant_email;
        }

        return $email;
    }

    /**
     * @param $invoice
     * @param $message
     * @param $contractor
     * @return array
     */
    public static function getContentData($invoices, $message, $contractor)
    {
        if ($invoices instanceof Invoice) {
            $invoices = [$invoices];
        }
        $invoiceNumber = [];
        $invoiceSum = [];
        $debtAmount = [];
        $daysOverdue = [];
        foreach ($invoices as $invoice) {
            $invoiceNumber[] = $invoice->getFullNumber();
            $invoiceSum[] = (int) $invoice->total_amount_with_nds;
            $debtAmount[] = (int) $invoice->remaining_amount;
            $daysOverdue[] = (int) date_diff(date_create($invoice->payment_limit_date), date_create())->days;
        }

        $invoiceSum = array_sum($invoiceSum);
        $debtAmount = array_sum($debtAmount);
        $contractorFio = $contractor->getDirectorFio();
        $contractorName = $contractor->getTitle(true);
        $invoiceNumber = implode(', ', $invoiceNumber);
        $invoiceDate = DateHelper::format($invoice->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
        $agreement = $invoice->basis_document_number . ' ' .
            (DateHelper::format($invoice->basis_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE));
        $paymentPeriod = date_diff(new \DateTime($invoice->document_date), new \DateTime($invoice->payment_limit_date))->days;
        $overdueDays = max($daysOverdue);

        $content = strip_tags($message->message_template_body, '<br>');
        $content = str_replace('[Имя покупателя]', $contractorFio, $content);
        $content = str_replace('[Название компании покупателя]', $contractorName, $content);
        $content = str_replace('[№ и дата договора]', empty($agreement) ? '' : $agreement, $content);
        $content = str_replace('[Период оплаты]', $paymentPeriod, $content);
        $content = str_replace('[№ счета]', $invoiceNumber, $content);
        $content = str_replace('[Дата счета]', $invoiceDate, $content);
        $content = str_replace('[Сумма по счету]', TextHelper::invoiceMoneyFormat($invoiceSum, 2), $content);
        $content = str_replace('[Сумма задолженности]', TextHelper::invoiceMoneyFormat($debtAmount, 2), $content);
        $content = str_replace('[Просрочено дней]', $overdueDays, $content);

        return [
            'days_overdue' => $overdueDays,
            'debt_amount' => $debtAmount,
            'invoice' => $invoiceNumber,
            'content' => $content
        ];
    }

    /**
     * @param PaymentReminderMessage $messageTemplate
     * @param $content
     * @param $email
     * @param $author
     * @throws \yii\base\Exception
     */
    public static function sendMessage($messageTemplate, $content, $emails, $author)
    {
        Yii::$app->mailer->htmlLayout = 'layouts/document-html';

        $isSent = false;

        foreach ($emails as $email) {
            $message = Yii::$app->mailer->compose([
                'html' => "system/payment-reminder/html",
                'text' => "system/payment-reminder/text",
            ], [
                'supportEmail' => Yii::$app->params['emailList']['support'],
                'subject' => $messageTemplate->message_template_subject,
                'content' => $content,
                'author' => $author,
            ])->setFrom([
                Yii::$app->params['emailList']['docs'] => $author->getFio(true),
            ])->setReplyTo([
                $author->employee->email => $author->getFio(true),
            ])->setSubject($messageTemplate->message_template_subject)->setTo($email);

            if ($message->send() && !$isSent) {
                $isSent = true;
            }
        }

        return $isSent;
    }

    /**
     * @param $message
     * @param $contractor
     * @param $invoices
     * @param $isSent
     * @param $emails
     * @throws \yii\base\Exception
     */
    public static function createReport($message, $contractor, $invoices, $isSent, $emails, $contentData)
    {
        if ($invoices instanceof Invoice) {
            $invoices = [$invoices];
        }

        $model = new Report([
            'company_id' => $message->company_id,
            'contractor_id' => $contractor->id,
            'message_id' => $message->id,
            'category_id' => $message->category_id,
            'date' => date('Y-m-d'),
            'days_overdue' => $contentData['days_overdue'] ?? 0,
            'debt_amount' => $contentData['debt_amount'] ?? 0,
            'invoice' => $contentData['invoice'] ?? '',
            'is_sent' => $isSent,
            'subject' => $message->message_template_subject,
            'event_header' => $message->event_header,
            'event_body' => $message->event_body,
            'email' => implode(',', (array) $emails),
        ]);

        if ($model->save(false)) {
            foreach ($invoices as $invoice) {
                $model->link('invoices', $invoice);
            }
        } else {
            \common\components\helpers\ModelHelper::logErrors($model, __METHOD__);
        }
    }

    public static function sendForOverdueInvoice($invoice, $message, $settings)
    {
        $isSent = false;
        $contractor = $invoice->contractor;
        $emails = self::getEmails($message, $contractor);
        $contentData = self::getContentData($invoice, $message, $contractor);
        if (!empty($emails)) {
            $isSent = self::sendMessage($message, $contentData['content'], $emails, self::getSender($message, $contractor));
        }
        self::createReport($message, $contractor, $invoice, $isSent, $emails, $contentData);
    }

    public static function sendForExceededLimit($dataArray, $message, $settings)
    {
        foreach ($dataArray as $data) {
            $contractor = \common\models\Contractor::findOne($data['contractor_id']);
            $invoiceArray = Invoice::find()->where(['id' => explode(',', $data['invoiceIds'])])->all();
            $isSent = false;
            $emails = self::getEmails($message, $contractor);
            $contentData = self::getContentData($invoiceArray, $message, $contractor);
            if (!empty($emails)) {
                $isSent = self::sendMessage($message, $contentData['content'], $emails, self::getSender($message, $contractor));
            }
            self::createReport($message, $contractor, $invoice, $isSent, $emails, $contentData);
        }
    }

    public static function sendForPaidInvoice($invoice, $message)
    {
        $query = Report::find()->joinWith('reportInvoices')->andWhere([
            'payment_reminder_report.company_id' => $invoice->company_id,
            'payment_reminder_report.contractor_id' => $invoice->contractor_id,
            'payment_reminder_report.message_id' => $message->id,
            'payment_reminder_report_invoice.invoice_id' => $invoice->id,
        ]);

        if (!$query->exists()) {
            $isSent = false;
            $contractor = $invoice->contractor;
            $emails = self::getContractorEmails($message, $contractor);
            $contentData = self::getContentData($invoice, $message, $contractor);
            if (!empty($emails)) {
                $isSent = self::sendMessage($message, $contentData['content'], $emails, self::getSender($message, $contractor));
            }
            self::createReport($message, $contractor, $invoice, $isSent, $emails, $contentData);
        }
    }

    public static function getSender($message, $contractor)
    {
        return EmployeeCompany::findOne([
            'employee_id' => $contractor->responsible_employee_id,
            'company_id' => $message->company_id,
        ]) ? : $message->company->chiefEmployeeCompany;
    }
}
