<?php

namespace console\components\KubCrmClient;

use Yii;
use common\models\Company;
use common\models\Contractor;
use common\models\EmployeeCompany;
use common\models\company\RegistrationPageType;
use common\models\employee\Employee;
use console\components\sender\Findirector;
use frontend\modules\crm\models\Task;
use frontend\modules\crm\models\TaskEventList;
use frontend\modules\crm\services\Client\KubClientFromCompanyService;
use yii\helpers\ArrayHelper;

class FromInactiveFindirector
{
    const INACTIVE_TIME = 60*60;

    private Company $kubCompany;

    public function __construct(Company $kubCompany)
    {
        $this->kubCompany = $kubCompany;
    }

    public function run()
    {
        $lastId = 0;
        $query = $this->searchQuery($lastId);
        while ($companyArray = $query->all()) {
            foreach ($companyArray as $company) {
                $lastId = $company->id;
                $employeeCompany = $company->getChiefEmployeeCompany();
                if ($this->createKubCrmClient($company, $employeeCompany)) {
                    try {
                        $this->sendEmail($company, $employeeCompany);
                    } catch (\Exception $e) {

                    }
                }
            }
            $query = $this->searchQuery($lastId);
        }
    }

    public function searchQuery($lastId)
    {
        return Company::find()->alias('company')->leftJoin([
            'contractor' => Contractor::tableName()
        ], '{{company}}.[[id]]={{contractor}}.[[created_from_company_id]] AND {{contractor}}.[[company_id]] = :companyId', [
            ':companyId' => $this->kubCompany->id,
        ])->andWhere([
            'company.registration_page_type_id' => RegistrationPageType::$analyticsPages,
            'company.analytics_module_activated' => false,
            'contractor.id' => null,
        ])->andWhere([
            '<', 'company.created_at', time() - self::INACTIVE_TIME,
        ])->andWhere([
            '>', 'company.id', $lastId,
        ])->orderBy([
            'company.id' => SORT_ASC,
        ])->limit(100);
    }

    private function createKubCrmClient(Company $company, EmployeeCompany $employeeCompany)
    {
        $taskDescription = 'Не заполнил стартовую форму финдиректора и не активировал его';
        $clientService = new KubClientFromCompanyService($company);

        if (($contractor = $clientService->create()) !== null) {
            $comment = implode("\n", [
                sprintf('Страна местонахождения бизнеса: %s', $company->infoPlace ? $company->infoPlace->name : '---'),
                sprintf('Роль в компании: %s', $employeeCompany->infoRole ? $employeeCompany->infoRole->name : '---'),
                sprintf('Тип бизнеса: %s', $company->infoIndustry ? $company->infoIndustry->name : '---'),
                $taskDescription,
            ]);
            $contractor->updateAttributes([
                'comment' => implode("\n", array_filter([$contractor->comment, $comment])),
            ]);

            if ($contractor->responsible_employee_id &&
                !$contractor->getTasks()->andWhere([
                    'description' => $taskDescription,
                ])->exists()
            ) {
                $task = new Task();
                $task->task_number = Task::getNextNumber(Yii::$app->kubCompany);
                $task->company_id = Yii::$app->kubCompany->id;
                $task->contractor_id = $contractor->id;
                $task->employee_id = $contractor->responsible_employee_id;
                $task->contact_id = $contractor->defaultContact ? $contractor->defaultContact->contact_id : null;
                $task->description = $taskDescription;
                $task->event_type = TaskEventList::EVENT_TYPE_CALL;
                $task->status = Task::STATUS_IN_PROGRESS;
                $task->datetime = date('Y-m-d', strtotime('+1 Weekday')).' 11:00:00';
                $task->save();
            }

            return true;
        }

        return false;
    }

    private function sendEmail(Company $company, EmployeeCompany $employeeCompany)
    {
        /** @var $employee \common\models\employee\Employee */
        $employee = $employeeCompany->employee;
        $subject = 'Регистрация в КУБ24.ФинДиректор без активации';
        \Yii::$app->mailer->compose([
            'html' => 'system/new-analytics-registration/html',
            'text' => 'system/new-analytics-registration/text',
        ], [
            'name' => $employee->firstname,
            'email' => $employee->email,
            'phone' => $employee->phone,
            'regPage' => ArrayHelper::getValue($company, ['registrationPageType', 'name']),
            'time' => time(),
            'source' => in_array($company->registration_page_type_id, RegistrationPageType::$analyticsPages) ? 'Лендинг' : 'КУБ',
            'subject' => $subject,
            'place' => $company->infoPlace ? $company->infoPlace->name : '---',
            'role' => $employeeCompany->infoRole ? $employeeCompany->infoRole->name : '---',
            'business' => $company->infoIndustry ? $company->infoIndustry->name : '---',
        ])
        ->setFrom([Yii::$app->params['emailList']['info'] => Findirector::$from])
        ->setTo(Yii::$app->params['emailList']['support'])
        ->setSubject($subject)
        ->send();
    }
}
