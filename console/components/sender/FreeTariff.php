<?php
namespace console\components\sender;

use common\components\sender\unisender\UniSender;
use common\models\Company;
use common\models\EmployeeCompany;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\service\Subscribe;
use common\models\service\SubscribeStatus;
use common\models\service\SubscribeTariff;
use DateTime;
use Yii;
use yii\base\Component;
use yii\console\Controller;
use yii\db\Query;

class FreeTariff extends Component
{
    /**
     *  Daily email send
     * @throws yii\base\Exception
     */
    public function send($email = null)
    {
        $date = new DateTime('+ 1 day');

        $emailArray = [];
        if ($email) {
            if (($employee = Employee::findOne(['email' => $email, 'is_deleted' => false])) && $employee->company) {
                $emailArray = [['email' => $email]];
            }
        } else {
            $subQuery = Subscribe::find()->select(['id', 'company_id'])
                ->where([
                    'or',
                    [
                        'tariff_id' => SubscribeTariff::paidStandartIds(),
                        'status_id' => SubscribeStatus::STATUS_PAYED,
                    ],
                    [
                        'and',
                        ['status_id' => SubscribeStatus::STATUS_ACTIVATED],
                        ['>', 'expired_at', $date->setTime(0, 0)->getTimestamp()],
                    ],
                ])
                ->groupBy('company_id');

            $query = (new Query)
                ->select(['employee.email'])
                ->distinct()
                ->from(['link' => EmployeeCompany::tableName()])
                ->leftJoin(['employee' => Employee::tableName()], '{{link}}.[[employee_id]] = {{employee}}.[[id]]')
                ->leftJoin(['company' => Company::tableName()], '{{link}}.[[company_id]] = {{company}}.[[id]]')
                ->leftJoin(['subscribe' => $subQuery], '{{subscribe}}.[[company_id]] = {{company}}.[[id]]')
                ->where([
                    'employee.notify_new_features' => true,
                    'employee.is_active' => Employee::ACTIVE,
                    'employee.is_deleted' => Employee::NOT_DELETED,
                    'link.employee_role_id' => EmployeeRole::ROLE_CHIEF,
                    'link.is_working' => true,
                    'company.blocked' => Company::UNBLOCKED,
                    'subscribe.id' => null,
                ]);

            $emailArray = $query->all();
        }

        if ($emailArray) {
            echo '"Тариф БЕСПЛАТНО!" sending... ', "\n";
            $uniSender = new UniSender();
            $uniSender->setEmailTemplateName('mail_11')
                ->setFromEmail(\Yii::$app->params['emailList']['info'])
                ->setFromName(\Yii::$app->params['emailFromName'])
                ->setParams([
                    'supportEmail' => \Yii::$app->params['emailList']['support'],
                    'callUs' => \Yii::$app->params['uniSender']['phone'],
                    'subject' => 'Тариф БЕСПЛАТНО!',
                    'day' => 16,
                ])
                ->setContacts($emailArray)
                ->setSubject('Тариф БЕСПЛАТНО!')
                ->send(false);

            echo 'completed', "\n";
        } else {
            echo '"Тариф БЕСПЛАТНО!" no contacts', "\n";
        }
    }
}
