<?php
namespace console\components\sender;

use common\components\sender\unisender\UniSender;
use common\models\Company;
use common\models\EmployeeCompany;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\service\Subscribe;
use common\models\service\SubscribeStatus;
use common\models\service\SubscribeTariff;
use console\models\SentEmail;
use console\models\SentEmailType;
use Yii;
use yii\base\Component;

/**
 * InactiveCompany class
 */
class InactiveCompany extends Component
{
    public static $sendingParams = [
        1 => [
            'subject' => 'НЕ нужно дожидаться бухгалтера!',
            'template' => 'inactive/mail_1',
            'fromName' => 'Алексей Кущенко',
        ],
        2 => [
            'subject' => 'Зачем Вам выставлять АКТы? Если хочешь мира, готовься к войне!',
            'template' => 'inactive/mail_2',
            'fromName' => 'Алексей Кущенко',
        ],
        3 => [
            'subject' => 'Как при продаже товара не остаться и без товара и без денег?',
            'template' => 'inactive/mail_3',
            'fromName' => 'Алексей Кущенко',
        ],
        4 => [
            'subject' => 'Зачем нужна счет-фактура?',
            'template' => 'inactive/mail_4',
            'fromName' => 'Арслан Хакимов',
        ],
        5 => [
            'subject' => 'Разошлем ваши счета по расписанию! Доставку гарантируем.',
            'template' => 'inactive/mail_5',
            'fromName' => 'Алексей Кущенко',
        ],
        6 => [
            'subject' => 'Теперь для сверки с клиентом вам НЕ НУЖЕН бухгалтер!',
            'template' => 'inactive/mail_6',
            'fromName' => 'Арслан Хакимов',
        ],
        7 => [
            'subject' => 'Акты, Акты, Акты….МНОГО АКТОВ',
            'template' => 'inactive/mail_7',
            'fromName' => 'Алексей Кущенко',
        ],
    ];

    /**
     * InactiveCompany email send
     * @throws yii\base\Exception
     */
    public function send($email = null, $number = null)
    {
        $fields = [
            'company_id',
            'employee_id',
            'email_type_id',
            'email_type_number',
            'created_at',
            'email',
        ];

        foreach (static::$sendingParams as $key => $params) {
            if ($key != 1) {
                sleep(60);
            }
            $emails = [];
            $rows = [];

            if ($email === null) {
                $dataArray = $this->getDataArray($key);
                if ($dataArray) {
                    foreach ($dataArray as $data) {

                        $emails[] = $data['email'];

                        $rows[] = [
                            $data['cid'],
                            $data['uid'],
                            SentEmailType::INACTIVE_COMPANY,
                            $key,
                            time(),
                            $data['email'],
                        ];
                    }
                }
            } else {
                $emails = (!$number || $number == $key) ? [$email] : [];
            }

            if ($emails) {
                echo '"' . $params['subject'] . '" sending... ', "\n";

                if ($this->sendEmail($emails, $params['template'], $params['subject'], $params['fromName']) && $rows) {
                    Yii::$app->db->createCommand()->batchInsert(SentEmail::tableName(), $fields, $rows)->execute();
                }

                echo 'completed', "\n";
            } else {
                echo '"' . $params['subject'] . '" no contacts', "\n";
            }
        }
    }

    protected function getDataArray($number)
    {
        $query = Company::find()->alias('company')->distinct()
            ->select(['{{company}}.[[id]] AS [[cid]]', '{{employee}}.[[id]] AS [[uid]]', 'employee.email'])
            ->leftJoin(
                ['subscribe' => Subscribe::tableName()],
                '{{subscribe}}.[[company_id]] = {{company}}.[[id]] AND {{subscribe}}.[[tariff_id]] = :tariff'
            )
            ->leftJoin(
                ['link' => EmployeeCompany::tableName()],
                '{{link}}.[[company_id]] = {{company}}.[[id]]' .
                ' AND {{link}}.[[employee_id]] = (
                    SELECT {{l}}.[[employee_id]]
                    FROM {{' . EmployeeCompany::tableName() . '}} {{l}}
                    WHERE {{l}}.[[company_id]] = {{company}}.[[id]]
                    AND {{l}}.[[employee_role_id]] = :role
                    AND {{l}}.[[is_working]] = true
                    ORDER BY {{l}}.[[created_at]] ASC
                    LIMIT 1
                )'
            )
            ->leftJoin(['employee' => Employee::tableName()], '{{link}}.[[employee_id]] = {{employee}}.[[id]]')
            ->leftJoin([
                'visit' => 'company_last_visit',
            ], '{{link}}.[[employee_id]] = {{visit}}.[[employee_id]] AND {{link}}.[[company_id]] = {{visit}}.[[company_id]]')
            ->andWhere(['<', 'IFNULL({{visit}}.[[time]], 0)', date_create('today -3 days')->getTimeStamp()])
            ->andWhere(['<', 'subscribe.expired_at', date_create('today')->getTimeStamp()])
            ->andWhere(['employee.notify_new_features' => true])
            ->andWhere(['not', ['employee.id' => null]])
            ->andWhere(['not', ['employee.email' => '']])
            ->andWhere(['company.blocked' => Company::UNBLOCKED])
            ->params([
                ':type' => SentEmailType::INACTIVE_COMPANY,
                ':number' => $number,
                ':role' => EmployeeRole::ROLE_CHIEF,
                ':tariff' => SubscribeTariff::TARIFF_TRIAL,
            ]);

        if ($number > 1) {
            $query
                ->leftJoin(
                    ['sent1' => SentEmail::tableName()],
                    '{{sent1}}.[[company_id]] = {{company}}.[[id]]' .
                    ' AND {{sent1}}.[[email_type_id]] = :type' .
                    ' AND {{sent1}}.[[email_type_number]] = (:number -1)'
                )
                ->leftJoin(
                    ['sent2' => SentEmail::tableName()],
                    '{{sent2}}.[[company_id]] = {{company}}.[[id]]' .
                    ' AND {{sent2}}.[[email_type_id]] = :type' .
                    ' AND {{sent2}}.[[email_type_number]] = :number'
                )
                ->andWhere(['not', ['sent1.id' => null]])
                ->andWhere(['sent2.id' => null])
                ->andWhere(['<', 'sent1.created_at', date_create('today -3 days')->getTimeStamp()]);

        } else {
            $query
                ->leftJoin(
                    ['sent' => SentEmail::tableName()],
                    '{{sent}}.[[company_id]] = {{company}}.[[id]]' .
                    ' AND {{sent}}.[[email_type_id]] = :type' .
                    ' AND {{sent}}.[[email_type_number]] = :number'
                )
                ->andWhere(['sent.id' => null]);
        }

        return $query->groupBy('company.id')->asArray()->all();
    }

    protected function sendEmail($emailArray = [], $template, $subject, $fromName)
    {
        $uniSender = new UniSender();
        $uniSender->setEmailTemplateName($template)
            ->setFromEmail(\Yii::$app->params['emailList']['info'])
            ->setFromName($fromName)
            ->setParams([
                'supportEmail' => \Yii::$app->params['emailList']['support'],
                'callUs' => \Yii::$app->params['uniSender']['phone'],
                'subject' => $subject,
            ])
            ->setContacts($emailArray)
            ->setSubject($subject);

        return $uniSender->send(false);
    }
}
