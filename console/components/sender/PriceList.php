<?php
namespace console\components\sender;

use common\components\sender\unisender\UniSender;
use common\models\Company;
use common\models\company\RegistrationPageType;
use common\models\EmployeeCompany;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use Yii;
use yii\base\Component;

/**
 * Рассылка "Работа с должниками"
 *
 * Class PriceList
 */
class PriceList extends Component
{
    protected $controller;

    public function getMailingParams()
    {
        return [
            /*1 => [
                'condition' => [
                    'registration_page_type_id' => RegistrationPageType::PAGE_TYPE_PRICE_LIST,
                ],
                'subject' => 'Мы исправились! Теперь создать Прайс-Лист очень просто',
                'tamplate' => 'price_list/mail_1',
                'fromName' => 'Алексей Кущенко',
            ],*/
            /*2 => [
                'condition' => [
                    'not',
                    ['registration_page_type_id' => RegistrationPageType::PAGE_TYPE_PRICE_LIST],
                ],
                'subject' => 'Поможем продавать больше!',
                'tamplate' => 'price_list/mail_2',
                'fromName' => 'Алексей Кущенко',
            ],*/
        ];
    }

    /**
     *  Daily email send
     * @throws yii\base\Exception
     */
    public function send($email = null, $mailNum = null)
    {
        foreach ($this->getMailingParams() as $key => $params) {
            if ($mailNum !== null && $mailNum != $key) {
                continue;
            }

            if ($email) {
                $emailArray = [$email];
            } else {
                $companyIds = Company::find()->select([
                    'id',
                ])->andWhere([
                    'blocked' => false,
                    'test' => false,
                ])->andWhere($params['condition'])->column();

                $emailArray = $companyIds ? EmployeeCompany::find()->select([
                    'employee.email',
                ])->leftJoin(Employee::tableName(), '{{employee}}.[[id]] = {{employee_company}}.[[employee_id]]')->andWhere([
                    'employee_company.company_id' => $companyIds,
                    'employee_company.is_working' => true,
                    'employee_company.employee_role_id' => EmployeeRole::ROLE_CHIEF,
                ])->column() : [];
            }

            if ($emailArray) {
                file_put_contents(
                    Yii::getAlias('@runtime/logs/PriceList-contacts.log'),
                    date('Y-m-d H:i')." {$params['tamplate']} ".var_export($emailArray, true)."\n\n",
                    FILE_APPEND
                );
                echo '"', $params['subject'], '" sending... ';

                $offset = 0;
                $limit = 500;
                $contactArray = [];
                do {
                    $contactArray = array_slice($emailArray, $offset * $limit, $limit);
                    $offset++;

                    if ($contactArray) {
                        $this->sendToContacts($contactArray, $params);
                    }
                    sleep(5);
                } while ($contactArray);

                echo 'completed', "\n";
            } else {
                echo '"', $params['subject'], '" no contacts', "\n";
            }
        }
    }

    public function sendToContacts($emailArray, $params)
    {
        $uniSender = new UniSender();
        $uniSender->setEmailTemplateName($params['tamplate'])
            ->setFromEmail(\Yii::$app->params['emailList']['info'])
            ->setFromName($params['fromName'])
            ->setParams([
                'supportEmail' => \Yii::$app->params['emailList']['support'],
                'callUs' => \Yii::$app->params['uniSender']['phone'],
                'subject' => $params['subject'],
            ])
            ->setContacts($emailArray)
            ->setSubject($params['subject']);

        if ($uniSender->send(false)) {
            echo 'Ok', "\n";
            if ($w = $uniSender->getLastWarnings()) {
                file_put_contents(
                    Yii::getAlias('@runtime/logs/PriceList.log'),
                    date('Y-m-d H:i').' lastWarnings: '.var_export($w, true)."\n\n",
                    FILE_APPEND
                );
                echo 'has warnings', "\n";
            }
        } else {
            file_put_contents(
                Yii::getAlias('@runtime/logs/PriceList.log'),
                date('Y-m-d H:i').' lastError: '.var_export($uniSender->getLastError(), true)."\n\n",
                FILE_APPEND
            );
            echo 'fail', "\n";
        }

        file_put_contents(
            Yii::getAlias('@runtime/logs/PriceList.log'),
            date('Y-m-d H:i').' lastResult: '.var_export($uniSender->getLastResult(), true)."\n\n",
            FILE_APPEND
        );
    }
}
