<?php
namespace console\components\sender;

use common\components\sender\unisender\UniSender;
use common\models\employee\Employee;
use console\models\AutovoronkaSendLog;
use Yii;
use yii\base\Component;
use yii\console\Controller;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

class Autovoronka extends Component
{
    public static $sender_1 = 'Алексей Кущенко';

    public static $subject_1_1 = 'Как ПРАВИЛЬНО заполнить декларацию ИП?';
    public static $subject_1_2 = 'Можно ли ИП не платить налоги?';
    public static $subject_1_3 = 'Для особых ИП - выгодные условия!';

    public static $subject_2_1 = '10 причин, блокировки счета вашего ИП';
    public static $subject_2_2 = 'Налоговая закроет ваше ИП! Почему?';
    public static $subject_2_3 = 'Осторожно: ЗЛАЯ налоговая!';
    public static $subject_2_4 = 'Как ИП не платить за бухгалтерию?';
    public static $subject_2_5 = 'Можно ли ИП не платить налоги?';
    public static $subject_2_6 = 'Для особых ИП - выгодные условия!';

    public static $subject_3_1 = 'Налоговая закроет ваше ИП! Почему?';
    public static $subject_3_2 = 'Осторожно: ЗЛАЯ налоговая!';
    public static $subject_3_3 = 'Как ИП не платить за бухгалтерию?';
    public static $subject_3_4 = 'Можно ли ИП не платить налоги?';
    public static $subject_3_5 = 'Для особых ИП - выгодные условия!';

    public static $subject_4_1 = 'Налоговый календарь для ИП!';
    public static $subject_4_2 = 'Налоговая закроет ваше ИП! Почему?';
    public static $subject_4_3 = 'Осторожно: ЗЛАЯ налоговая!';
    public static $subject_4_4 = 'Как ИП не платить за бухгалтерию?';
    public static $subject_4_5 = 'Можно ли ИП не платить налоги?';
    public static $subject_4_6 = 'Для особых ИП - выгодные условия!';

    public static $subject_repeat_1 = 'Когда ИП может не сдавать декларацию?';
    public static $subject_repeat_2 = 'Как ИП не платить за бухгалтерию?';
    public static $subject_repeat_3 = 'Подготовка декларации за 10 минут';
    public static $subject_repeat_4 = 'Для особых ИП - выгодные условия!';

    protected static $table = '{{%autovoronka}}';
    protected static $templatePath = '@common/mail/unisender/autovoronka';

    protected static $checkRead = [
        'case_2' => [
            'mail_1',
            'mail_5',
        ],
        'case_3' => [
            'mail_4',
        ],
        'case_4' => [
            'mail_1',
            'mail_5',
        ],
    ];

    public static function mailingParams()
    {
        return [
            'case_1' => [
                'mail_1' => [0, self::$subject_1_1],
                'mail_2' => [1, self::$subject_1_2],
                'mail_3' => [2, self::$subject_1_3],
            ],
            'case_2' => [
                'mail_1' => [0, self::$subject_2_1],
                'mail_2' => [2, self::$subject_2_2],
                'mail_3' => [4, self::$subject_2_3],
                'mail_4' => [6, self::$subject_2_4],
                'mail_5' => [10, self::$subject_2_5],
                'mail_6' => [14, self::$subject_2_6],
            ],
            'case_3' => [
                'mail_1' => [0, self::$subject_3_1],
                'mail_2' => [2, self::$subject_3_2],
                'mail_3' => [4, self::$subject_3_3],
                'mail_4' => [6, self::$subject_3_4],
                'mail_5' => [10, self::$subject_3_5],
            ],
            'case_4' => [
                'mail_1' => [0, self::$subject_4_1],
                'mail_2' => [2, self::$subject_4_2],
                'mail_3' => [4, self::$subject_4_3],
                'mail_4' => [6, self::$subject_4_4],
                'mail_5' => [10, self::$subject_4_5],
                'mail_6' => [14, self::$subject_4_6],
            ],
            'repeat' => [
                'mail_1' => [0, self::$subject_repeat_1],
                'mail_2' => [2, self::$subject_repeat_2],
                'mail_3' => [6, self::$subject_repeat_3],
                'mail_4' => [10, self::$subject_repeat_4],
            ]
        ];
    }

    public static function mailingAttachments()
    {
        return [
            'case_1' => [
            ],
            'case_2' => [
                'mail_1' => [
                    '10 причин блокировки счета ИП и 1 совет.doc' => '10_reasons_to_block_an_account.doc',
                ],
                'mail_3' => [
                    'Налоговый календарь для ИП на УСН 6% БЕЗ сотрудников.doc' => 'tax_calendar_for_IP_USN_6_without_employees.doc',
                ],
                'mail_4' => [
                    'Руководство по заполнению декларации ИП.doc' => 'guide_for_completing_the_IP_declaration.doc',
                ],
            ],
            'case_3' => [
                'mail_2' => [
                    'Налоговый календарь для ИП на УСН 6% БЕЗ сотрудников.doc' => 'tax_calendar_for_IP_USN_6_without_employees.doc',
                ],
                'mail_3' => [
                    'Руководство по заполнению декларации ИП.doc' => 'guide_for_completing_the_IP_declaration.doc',
                ],
            ],
            'case_4' => [
                'mail_3' => [
                    'Налоговый календарь для ИП на УСН 6% БЕЗ сотрудников.doc' => 'tax_calendar_for_IP_USN_6_without_employees.doc',
                ],
                'mail_4' => [
                    'Руководство по заполнению декларации ИП.doc' => 'guide_for_completing_the_IP_declaration.doc',
                ],
            ],
            'repeat' => [
                'mail_2' => [
                    'Руководство по заполнению декларации ИП.doc' => 'guide_for_completing_the_IP_declaration.doc',
                ],
            ]
        ];
    }

    /**
     * @throws yii\base\Exception
     */
    public function send($email = null, $case = null, $mail = null)
    {
        if ($email === null) {
            $this->exportContacts();
            $this->runMailing();
            $this->unreadMailing();
        } elseif (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $mailingParams = self::mailingParams();
            if (!isset($case, $mail)) {
                if ($list_id = ArrayHelper::getValue(Yii::$app, 'params.uniSender.listId')) {
                    foreach ($mailingParams as $case => $data) {
                        foreach ($data as $mail => $params) {
                            $this->sendMailing([
                                'list_id' => ArrayHelper::getValue(Yii::$app, 'params.uniSender.listId'),
                                'contacts' => [$email],
                                'case' => $case,
                                'mail' => $mail,
                                'subject' => $params[1],
                                'date' => date('Y-m-d'),
                                'for_unread' => false,
                            ]);
                            sleep(1);
                        }
                    }
                } else {
                    echo '`list_id` not found'. PHP_EOL;
                }
            } else {
                if (isset($mailingParams[$case][$mail][1])) {
                    if ($list_id = ArrayHelper::getValue(Yii::$app, 'params.uniSender.listId')) {
                        $this->sendMailing([
                            'list_id' => ArrayHelper::getValue(Yii::$app, 'params.uniSender.listId'),
                            'contacts' => [$email],
                            'case' => $case,
                            'mail' => $mail,
                            'subject' => $mailingParams[$case][$mail][1],
                            'date' => date('Y-m-d'),
                            'for_unread' => false,
                        ]);
                    } else {
                        echo '`list_id` not found'. PHP_EOL;
                    }
                } else {
                    echo 'template not found'. PHP_EOL;
                }
            }
        } else {
            echo 'invalid email'. PHP_EOL;
        }
        echo 'completed'. PHP_EOL;
    }

    /**
     * @inheritdoc
     */
    public function runMailing()
    {
        echo 'begin mailing'. PHP_EOL;
        $currentDate = date('Y-m-d');
        $repeatDate = date_create("+14 days")->format('Y-m-d');
        $hour = intval(date('G'));
        foreach (self::mailingParams() as $case => $mailingData) {
            if ($case == 'repeat') {
                if ($hour > 9) {
                    $this->repeatMailing($mailingData);
                }
            } else {
                $mailList = array_keys($mailingData);
                $firstMail = reset($mailList);
                $lastMail = end($mailList);

                if ($listId = self::listIdByCase($case)) {
                    foreach ($mailingData as $mail => $data) {
                        if ($firstMail == $mail || $hour > 9) {
                            $days = $data[0];
                            $subject = $data[1];
                            $tamplate = "{$case}/{$mail}";
                            $columns = [
                                'last_mail' => $currentDate,
                            ];
                            $fromDate = date_create("-{$days} days")->format('Y-m-d');
                            $emailArray = (new Query)->select('t.email')->distinct()->from([
                                't' => self::$table,
                            ])->leftJoin([
                                'e' => Employee::tableName(),
                            ], '{{e}}.[[email]] = {{t}}.[[email]]')->where([
                                'and',
                                ['t.case' => $case],
                                ['t.list_id' => $listId],
                                ['t.created' => $fromDate],
                                ['not', ['t.last_mail' => $currentDate]],
                                [
                                    'or',
                                    ['e.id' => null],
                                    ['e.is_deleted' => 1],
                                    [
                                        '<',
                                        new \yii\db\Expression('FROM_UNIXTIME({{e}}.[[created_at]])'),
                                        new \yii\db\Expression('{{t}}.[[created]]'),
                                    ],
                                ],
                            ])->column();

                            if (!empty($emailArray)) {
                                echo 'mailing '.$tamplate.', count:'.count($emailArray).PHP_EOL;
                                if ($mail == $lastMail) {
                                    $columns['repeat'] = $repeatDate;
                                }
                                Yii::$app->db->createCommand()->update(self::$table, $columns, [
                                    'email' => $emailArray,
                                ])->execute();

                                $this->sendMailing([
                                    'list_id' => $listId,
                                    'contacts' => $emailArray,
                                    'case' => $case,
                                    'mail' => $mail,
                                    'subject' => $subject,
                                    'date' => $currentDate,
                                    'for_unread' => false,
                                ]);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function repeatMailing($mailingData)
    {
        echo 'begin repeatMailing'. PHP_EOL;
        $currentDate = date('Y-m-d');
        $columns = [
            'last_mail' => $currentDate,
        ];
        foreach ($mailingData as $mail => $data) {
            $days = $data[0];
            $subject = $data[1];
            $mailList = array_keys($mailingData);
            $lastMail = end($mailList);
            $tamplate = "repeat/{$mail}";
            $fromDate = date_create("-{$days} days")->format('Y-m-d');

            $contactDataArray = (new Query)->select([
                't.email',
                't.list_id',
            ])->from([
                't' => self::$table,
            ])->leftJoin([
                'e' => Employee::tableName(),
            ], '{{e}}.[[email]] = {{t}}.[[email]]')->where([
                'and',
                ['t.repeat' => $fromDate],
                ['not', ['t.last_mail' => $currentDate]],
                ['e.email' => null],
            ])->column();

            $emailDataArray = [];
            foreach ($contactDataArray as $contactData) {
                $emailDataArray[$contactData['list_id']][] = $contactData['email'];
            }

            foreach ($emailDataArray as $listId => $emailArray) {
                echo 'mailing '.$tamplate.', listId: '.$listId.', count:'.count($emailArray).PHP_EOL;
                Yii::$app->db->createCommand()->update(self::$table, $columns, [
                    'email' => $emailArray,
                ])->execute();

                $this->sendMailing([
                    'list_id' => $listId,
                    'contacts' => $emailArray,
                    'case' => 'repeat',
                    'mail' => $mail,
                    'subject' => $subject,
                    'date' => $currentDate,
                    'for_unread' => false,
                ]);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function unreadMailing()
    {
        echo 'begin unreadMailing'. PHP_EOL;
        $currentDate = date('Y-m-d');
        $date = date_create('-1 days')->format('Y-m-d');

        foreach (self::$checkRead as $case => $mailArray) {
            foreach ($mailArray as $mail) {
                $logArray = AutovoronkaSendLog::find()->where([
                    'for_unread' => false,
                    'date' => $date,
                    'case' => $case,
                    'mail' => $mail,
                ])->all();

                foreach ($logArray as $log) {
                    if ($campaign_id = ArrayHelper::getValue($log, 'result.result.campaign_id')) {
                        $stat = $this->getStat($campaign_id);
                        $fields = (array) ArrayHelper::getValue($stat, 'fields');
                        $data = (array) ArrayHelper::getValue($stat, 'data');
                        $resultKey = array_search('send_result', $fields);
                        $emailKey = array_search('email', $fields);
                        if (isset($fields, $data, $resultKey, $emailKey)) {
                            $contacts = [];
                            foreach ($data as $value) {
                                if ($value[$resultKey] == 'ok_delivered') {
                                    $contacts[] = $value[$emailKey];
                                }
                            }
                            $subject = ArrayHelper::getValue(self::mailingParams(), [$case, $mail, 1]);
                            if (!empty($contacts) && $subject) {
                                $this->sendMailing([
                                    'list_id' => $log->list_id,
                                    'contacts' => $contacts,
                                    'case' => $case,
                                    'mail' => $mail,
                                    'subject' => $subject,
                                    'date' => $currentDate,
                                    'for_unread' => true,
                                ]);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * @param $params
     * `list_id`
     * `contacts`
     * `case`
     * `mail`
     * `subject`
     * `date`
     * `for_unread`
     */
    public function sendMailing($params)
    {
        $tamplate = "{$params['case']}/{$params['mail']}";
        $attachments = [];
        $attachData = (array) ArrayHelper::getValue(self::mailingAttachments(), "{$params['case']}.{$params['mail']}");
        foreach ($attachData as $key => $value) {
            $file = Yii::getAlias('@console/components/sender/attachments/'.$value);
            if (is_file($file)) {
                $attachments[$key] = file_get_contents($file);
            }
        }

        $uniSender = new UniSender();

        $uniSender->setListId($params['list_id'])
            ->setEmailTemplatePath(self::$templatePath)
            ->setEmailTemplateName($tamplate)
            ->setFromEmail(\Yii::$app->params['emailList']['info'])
            ->setFromName(self::$sender_1)
            ->setParams([
                'supportEmail' => \Yii::$app->params['emailList']['support'],
                'callUs' => \Yii::$app->params['uniSender']['phone'],
                'subject' => $params['subject'],
            ])
            ->setContacts($params['contacts'])
            ->setSubject($params['subject'])
            ->setAttachments($attachments)
            ->send(false);

        $result = $uniSender->getLastResult();
        $log = new AutovoronkaSendLog($params);
        $log->setResult($result);
        $log->save();
    }

    /**
     * Get list Id
     *
     * @return array
     */
    public static function listIdAll()
    {
        return (array) ArrayHelper::getValue(Yii::$app->params, ['uniSender', 'autovoronka.listIds']);
    }

    /**
     * Get list Id
     *
     * @return array
     */
    public static function listIdByCase($case)
    {
        return ArrayHelper::getValue(self::listIdAll(), $case);
    }

    /**
     * Export contacts from Unisender
     * @throws yii\base\Exception
     */
    public function exportContacts()
    {
        foreach (self::listIdAll() as $case => $ids) {
            foreach ((array) $ids as $listId) {
                echo 'begin export case "'.$case.'" list "'.$listId.'"'. PHP_EOL;
                $uniSender = new UniSender();
                $offset = 0;
                $continue = true;
                $date = date('Y-m-d');

                do {
                    $response = $uniSender->exportContacts([
                        'field_names' => [
                            'email',
                            'email_status',
                        ],
                        'list_id' => $listId,
                        'offset' => $offset++ * UniSender::EXPORT_ROWS_LIMIT,
                        'limit' => UniSender::EXPORT_ROWS_LIMIT,
                    ]);
                    $result = json_decode($response, true);
                    if (empty($result['result']['data'])) {
                        $continue = false;
                    } else {
                        $rows = [];
                        foreach ($result['result']['data'] as $item) {
                            if (!empty($item[0]) && in_array($item[1], ['active', 'new'])) {
                                $rows[] = [
                                    $item[0],
                                    $case,
                                    $listId,
                                    $date,
                                    '0000-00-00',
                                ];
                            }
                        }
                        if ($rows) {
                            $this->insertContacts($rows);
                        }
                    }
                    echo 'export: ' . $offset . PHP_EOL;
                } while ($continue);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function insertContacts($rows)
    {
        $db = Yii::$app->db;
        $sql = $db->createCommand()->batchInsert('{{%autovoronka}}', [
            'email',
            'case',
            'list_id',
            'created',
            'last_mail',
        ], $rows)->getRawSql();
        $sql = strtr($sql, ['INSERT INTO' => 'INSERT IGNORE INTO']);
        $db->createCommand($sql)->execute();
    }

    /**
     * @inheritdoc
     */
    public function getStat($id)
    {
        $uniSender = new UniSender();
        $result = Json::decode($uniSender->getCampaignDeliveryStats(['campaign_id' => $id]));

        return (array) ArrayHelper::getValue($result, 'result');
    }
}
