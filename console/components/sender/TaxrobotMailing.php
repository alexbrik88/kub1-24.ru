<?php
namespace console\components\sender;

use common\components\sender\unisender\UniSender;
use common\models\Company;
use common\models\company\CompanyFirstEvent;
use common\models\company\RegistrationPageType;
use common\models\EmployeeCompany;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\service\Subscribe;
use common\models\service\SubscribeStatus;
use common\models\service\SubscribeTariff;
use common\models\service\SubscribeTariffGroup;
use DateTime;
use Yii;
use yii\base\Component;
use yii\console\Controller;
use yii\db\Query;

class TaxrobotMailing extends Component
{
    protected $controller;

    public function getDailyParams()
    {
        return [
            [
                'day' => 2,
                'mail' => [
                    'subject' => 'Сроки сдачи отчетности на ' . date('Y') . ' год для ИП',
                    'tamplate' => 'tax-robot/mail_2',
                    'fromName' => 'Арслан Хакимов',
                    'partial' => false,
                ],
                'params' => [
                    'activation_type' => [],
                ],
                'fromLandingFilter' => false,
                'standartPaidFilter' => false,
                'taxrobotPaidFilter' => true,
            ],
            [
                'day' => 4,
                'mail' => [
                    'subject' => 'Вы знаете, как уменьшить налог для ИП?',
                    'tamplate' => 'tax-robot/mail_3',
                    'fromName' => 'Алексей Кущенко',
                    'partial' => false,
                ],
                'params' => [
                    'activation_type' => [],
                ],
                'fromLandingFilter' => false,
                'standartPaidFilter' => false,
                'taxrobotPaidFilter' => true,
            ],
            [
                'day' => 6,
                'mail' => [
                    'subject' => 'Что такое КУДиР и зачем он нужен',
                    'tamplate' => 'tax-robot/mail_4',
                    'fromName' => 'Арслан Хакимов',
                    'partial' => false,
                ],
                'params' => [
                    'activation_type' => [],
                ],
                'fromLandingFilter' => false,
                'standartPaidFilter' => false,
                'taxrobotPaidFilter' => true,
            ],
            [
                'day' => 8,
                'mail' => [
                    'subject' => 'Почему 12 выгоднее, чем 4!',
                    'tamplate' => 'tax-robot/mail_5',
                    'fromName' => 'Алексей Кущенко',
                    'partial' => false,
                ],
                'params' => [
                    'activation_type' => [],
                ],
                'fromLandingFilter' => false,
                'standartPaidFilter' => false,
                'taxrobotPaidFilter' => true,
            ],
            [
                'day' => 10,
                'mail' => [
                    'subject' => 'Счета без ошибок',
                    'tamplate' => 'mail_2',
                    'fromName' => 'Арслан Хакимов',
                    'partial' => false,
                ],
                'params' => [
                    'activation_type' => [
                        Company::ACTIVATION_EMPTY_PROFILE,
                        Company::ACTIVATION_NO_INVOICE,
                    ],
                ],
                'fromLandingFilter' => true,
                'standartPaidFilter' => true,
                'taxrobotPaidFilter' => true,
            ],
            [
                'day' => 12,
                'mail' => [
                    'subject' => 'В среднем наши клиенты получают деньги на 7 дней быстрее!',
                    'tamplate' => 'mail_3',
                    'fromName' => 'Арслан Хакимов',
                    'partial' => false,
                ],
                'params' => [
                    'activation_type' => [
                        Company::ACTIVATION_EMPTY_PROFILE,
                        Company::ACTIVATION_NO_INVOICE,
                        Company::ACTIVATION_NOT_SEND_INVOICES,
                        Company::ACTIVATION_ALL_COMPLETE,
                    ],
                ],
                'fromLandingFilter' => true,
                'standartPaidFilter' => true,
                'taxrobotPaidFilter' => true,
            ],
            [
                'day' => 14,
                'mail' => [
                    'subject' => 'Удивите своих клиентов!',
                    'tamplate' => 'mail_4',
                    'fromName' => 'Арслан Хакимов',
                    'partial' => false,
                ],
                'params' => [
                    'activation_type' => [
                        Company::ACTIVATION_EMPTY_PROFILE,
                        Company::ACTIVATION_NO_INVOICE,
                        Company::ACTIVATION_NOT_SEND_INVOICES,
                        Company::ACTIVATION_ALL_COMPLETE,
                    ],
                ],
                'fromLandingFilter' => true,
                'standartPaidFilter' => true,
                'taxrobotPaidFilter' => true,
            ],
            [
                'day' => 16,
                'mail' => [
                    'subject' => 'Ваш счет оплачен!',
                    'tamplate' => 'mail_5',
                    'fromName' => 'Алексей Кущенко',
                    'partial' => false,
                ],
                'params' => [
                    'activation_type' => [
                        Company::ACTIVATION_EMPTY_PROFILE,
                        Company::ACTIVATION_NO_INVOICE,
                        Company::ACTIVATION_NOT_SEND_INVOICES,
                        Company::ACTIVATION_ALL_COMPLETE,
                    ],
                ],
                'fromLandingFilter' => true,
                'standartPaidFilter' => true,
                'taxrobotPaidFilter' => true,
            ],
            [
                'day' => 18,
                'mail' => [
                    'subject' => 'Денег нет, но вы там держитесь!',
                    'tamplate' => 'mail_6',
                    'fromName' => 'Алексей Кущенко',
                    'partial' => false,
                ],
                'params' => [
                    'activation_type' => [
                        Company::ACTIVATION_EMPTY_PROFILE,
                        Company::ACTIVATION_NO_INVOICE,
                        Company::ACTIVATION_NOT_SEND_INVOICES,
                        Company::ACTIVATION_ALL_COMPLETE,
                    ],
                ],
                'fromLandingFilter' => true,
                'standartPaidFilter' => true,
                'taxrobotPaidFilter' => true,
            ],
            [
                'day' => 20,
                'mail' => [
                    'subject' => 'Посмотрите, ваш бизнес растет!',
                    'tamplate' => 'mail_7',
                    'fromName' => 'Алексей Кущенко',
                    'partial' => false,
                ],
                'params' => [
                    'activation_type' => [
                        Company::ACTIVATION_EMPTY_PROFILE,
                        Company::ACTIVATION_NO_INVOICE,
                        Company::ACTIVATION_NOT_SEND_INVOICES,
                        Company::ACTIVATION_ALL_COMPLETE,
                    ],
                ],
                'fromLandingFilter' => true,
                'standartPaidFilter' => true,
                'taxrobotPaidFilter' => true,
            ],
            /*[
                'day' => 21,
                'mail' => [
                    'subject' => 'Забирайте, свой подарок!',
                    'tamplate' => 'mail_9',
                    'fromName' => 'Арслан Хакимов',
                    'partial' => 'mail_9',
                ],
                'params' => [
                    'activation_type' => [
                        Company::ACTIVATION_ALL_COMPLETE,
                    ],
                ],
                'fromLandingFilter' => true,
                'standartPaidFilter' => true,
                'taxrobotPaidFilter' => true,
            ],*/
            [
                'day' => 22,
                'mail' => [
                    'subject' => 'Хорошие новости!',
                    'tamplate' => 'mail_8',
                    'fromName' => 'Арслан Хакимов',
                    'partial' => false,
                ],
                'params' => [
                    'activation_type' => [
                        Company::ACTIVATION_EMPTY_PROFILE,
                        Company::ACTIVATION_NO_INVOICE,
                        Company::ACTIVATION_NOT_SEND_INVOICES,
                    ],
                ],
                'fromLandingFilter' => true,
                'standartPaidFilter' => true,
                'taxrobotPaidFilter' => true,
            ],
            /*[
                'day' => 22,
                'mail' => [
                    'subject' => 'Используйте Ваши бонусы!',
                    'tamplate' => 'mail_10',
                    'fromName' => 'Арслан Хакимов',
                    'partial' => 'mail_10',
                ],
                'params' => [
                    'activation_type' => [
                        Company::ACTIVATION_ALL_COMPLETE,
                    ],
                ],
                'fromLandingFilter' => true,
                'standartPaidFilter' => true,
                'taxrobotPaidFilter' => true,
            ],*/
        ];
    }

    public function __construct(Controller $controller, $config = [])
    {
        $this->controller = $controller;

        parent::__construct($config);
    }

    protected function taxrobotPaidFilter($query)
    {
        $ids = SubscribeTariff::find()->select('id')->andWhere([
            'tariff_group_id' => [
                SubscribeTariffGroup::TAX_DECLAR_IP_USN_6,
                SubscribeTariffGroup::TAX_IP_USN_6,
            ],
        ])->andWhere(['>', 'price', 0])->column();

        $subQuery = Subscribe::find()->select(['id', 'company_id'])
            ->where(['tariff_id' => $ids])
            ->andWhere(['not', ['status_id' => SubscribeStatus::STATUS_NEW]])
            ->groupBy('company_id');

        $query
            ->leftJoin(['taxrobot_subscribe' => $subQuery], '{{taxrobot_subscribe}}.[[company_id]] = {{company}}.[[id]]')
            ->andWhere(['taxrobot_subscribe.id' => null]);

        return $query;
    }

    protected function standartPaidFilter($query)
    {
        $subQuery = Subscribe::find()->select(['id', 'company_id'])
            ->where(['tariff_id' => SubscribeTariff::paidStandartIds()])
            ->andWhere(['not', ['status_id' => SubscribeStatus::STATUS_NEW]])
            ->groupBy('company_id');

        $query
            ->leftJoin(['standart_subscribe' => $subQuery], '{{standart_subscribe}}.[[company_id]] = {{company}}.[[id]]')
            ->andWhere(['standart_subscribe.id' => null]);

        return $query;
    }

    /**
     *  Daily email send
     * @throws yii\base\Exception
     */
    public function send($email = null, $template = null)
    {
        foreach ($this->dailyParams as $key => $params) {
            if ($template !== null && $template != $params['mail']['tamplate']) {
                continue;
            }
            $daysOffset = 1 - $params['day'];
            $date = new DateTime("$daysOffset days");

            $dataArray = [];
            if ($email) {
                if (($employee = Employee::findOne(['email' => $email, 'is_deleted' => false])) && $employee->company) {
                    $dataArray = [['email' => $email, 'id' => $employee->company->id]];
                }
            } else {
                $query = (new Query)
                    ->select(['employee.email', 'company.id'])
                    ->from(['link' => EmployeeCompany::tableName()])
                    ->leftJoin(['employee' => Employee::tableName()], '{{link}}.[[employee_id]] = {{employee}}.[[id]]')
                    ->leftJoin(['company' => Company::tableName()], '{{link}}.[[company_id]] = {{company}}.[[id]]')
                    ->leftJoin(['event' => CompanyFirstEvent::tableName()], '{{event}}.[[company_id]] = {{company}}.[[id]]')
                    ->where([
                        'employee.is_active' => Employee::ACTIVE,
                        'employee.is_deleted' => Employee::NOT_DELETED,
                        'employee.is_mailing_active' => true,
                        'link.employee_role_id' => EmployeeRole::ROLE_CHIEF,
                        'link.is_working' => true,
                        'company.blocked' => Company::UNBLOCKED,
                        'DATE(FROM_UNIXTIME({{event}}.[[created_at]]))' => $date->format('Y-m-d'),
                    ]);

                if ($params['fromLandingFilter']) {
                    $query->andWhere([
                        'company.registration_page_type_id' => RegistrationPageType::PAGE_TYPE_BANK_ROBOT,
                    ]);
                }
                if ($params['standartPaidFilter']) {
                    $query = $this->standartPaidFilter($query);
                }
                if ($params['taxrobotPaidFilter']) {
                    $query = $this->taxrobotPaidFilter($query);
                }

                foreach ($params['params'] as $key => $value) {
                    $query->andWhere(["company.$key" => $value]);
                }

                $dataArray = $query->all();
            }

            if ($dataArray) {
                $emailArray = [];
                if ($params['mail']['partial']) {
                    $uniSender = new UniSender();
                    $offset = 0;
                    do {
                        $contactArray = array_slice($dataArray, $offset++ * UniSender::EMPLOYEE_SELECT_LIMIT, UniSender::EMPLOYEE_SELECT_LIMIT);

                        if ($contactArray) {
                            $emails = [];
                            $data = [];
                            foreach ($contactArray as $contact) {
                                $emails[] = $contact['email'];
                                $company = Company::findOne($contact['id']);
                                if (!$company->subscribe_payment_key) {
                                    $company->subscribe_payment_key = uniqid();
                                    $company->save(false, ['subscribe_payment_key']);
                                }
                                $data[] = [
                                    $contact['email'],
                                    Yii::$app->params['uniSender']['listId'],
                                    $this->controller->renderPartial("daily/{$params['mail']['partial']}", [
                                        'company' => $company,
                                        'subject' => $params['mail']['subject'],
                                    ]),
                                ];
                            }

                            $importData = [
                                'field_names' => [
                                    'email',
                                    'email_list_ids',
                                    'html_content',
                                ],
                                'data' => $data,
                            ];

                            $result = json_decode($uniSender->importContacts($importData));

                            if ($result && empty($result->error)) {
                                echo "- import Ok\n";
                                $emailArray = array_merge($emailArray, $emails);
                            } elseif (!empty($result->error)) {
                                echo "- import error:\n", var_export($result->error, true), "\n";
                            } else {
                                echo "- no import result\n";
                            }
                        }
                    } while ($contactArray);
                } else {
                    $emailArray = $dataArray;
                }

                if ($emailArray) {
                    echo '"', $params['mail']['subject'], '" sending... ';
                    $uniSender = new UniSender();
                    $uniSender->setEmailTemplateName($params['mail']['tamplate'])
                        ->setFromEmail(\Yii::$app->params['emailList']['info'])
                        ->setFromName($params['mail']['fromName'])
                        ->setParams([
                            'supportEmail' => \Yii::$app->params['emailList']['support'],
                            'callUs' => \Yii::$app->params['uniSender']['phone'],
                            'subject' => $params['mail']['subject'],
                            'day' => $params['day'],
                        ])
                        ->setContacts($emailArray)
                        ->setSubject($params['mail']['subject'])
                        ->send(false);

                    sleep(300);
                    $uniSender->clearHtmlContent($emailArray, Yii::$app->params['uniSender']['listId']);

                    echo 'completed', "\n";
                } else {
                    echo '"', $params['mail']['subject'], '" no contacts', "\n";
                }
            }
        }
    }
}
