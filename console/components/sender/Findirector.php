<?php
namespace console\components\sender;

use common\components\sender\unisender\UniSender;
use common\models\Company;
use common\models\Discount;
use common\models\EmployeeCompany;
use common\models\SentEmailStart;
use common\models\SentEmailType;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\service\Subscribe;
use common\models\service\SubscribeStatus;
use common\models\service\SubscribeTariff;
use DateTime;
use Yii;
use yii\base\Component;
use yii\base\Controller;
use yii\helpers\ArrayHelper;

class Findirector extends Component
{
    protected $controller;
    protected static $templatePath = '@common/mail/unisender/findirector';

    public static $from = 'КУБ24.ФинДиректор';

    public static $sendingParams = [
        // This email is a system
        //2 => [
        //    'subject' => 'Порядок в финансах - с чего начать?',
        //    'mail' => 'mail_2',
        //    'days' => 1,
        //],
        //3 => [
        //    'subject' => 'Учет финансов в проектном бизнесе',
        //    'mail' => 'mail_3',
        //    'days' => 3,
        //],
        //4 => [
        //    'subject' => 'Прибыль ЕСТЬ, а денег НЕТ?',
        //    'mail' => 'mail_4',
        //    'days' => 5,
        //],
        5 => [
            'subject' => 'Ассистент вам в помощь!',
            'mail' => 'mail_5',
            'days' => 6,
        ],
        //6 => [
        //    'subject' => '7 причин кассового разрыва!',
        //    'mail' => 'mail_6',
        //    'days' => 8,
        //],
        //7 => [
        //    'subject' => 'Забирать дивиденды РЕДКО — это ПЛОХО!',
        //    'mail' => 'mail_7',
        //    'days' => 10,
        //],
        8 => [
            'subject' => 'Муж (ассистент) на час',
            'mail' => 'mail_8',
            'days' => 12,
        ],
        /*9 => [
            'subject' => 'Ваш пробный период заканчивается',
            'mail' => 'trial_expires',
            'days' => 13,
        ],*/
        9 => [
            'subject' => 'Хиты возможностей для Вас!',
            'mail' => 'mail_9',
            'days' => 15,
        ],
        10 => [
            'subject' => 'Зачем вашему бизнесу Финансовая модель?',
            'mail' => 'mail_10',
            'days' => 18,
        ],
        11 => [
            'subject' => 'Покупка товара – это не расходы!',
            'mail' => 'mail_11',
            'days' => 22,
        ],
        //12 => [
        //    'subject' => 'Платежный календарь или рецепт от стресса',
        //    'mail' => 'mail_12',
        //    'days' => 26,
        //],
        13 => [
            'subject' => 'Дивиденды – это награда, а не зарплата',
            'mail' => 'mail_13',
            'days' => 30,
        ],
        14 => [
            'subject' => 'Как не попасть в кассовый разрыв',
            'mail' => 'mail_14',
            'days' => 34,
        ],
        15 => [
            'subject' => 'ФАКТ & ПЛАН. Управления бизнесом на основе цифр',
            'mail' => 'mail_15',
            'days' => 39,
        ],
        16 => [
            'subject' => 'Как запланировать покупку Mercedes',
            'mail' => 'mail_16',
            'days' => 44,
        ],
        17 => [
            'subject' => 'Как НЕ превратится в банкира!',
            'mail' => 'mail_17',
            'days' => 50,
        ],
    ];

    public function send($email, $letterNumder)
    {
        $this->sendTrialExpires($email, $letterNumder);

        $sleep = false;
        foreach (self::$sendingParams as $key => $params) {
            if ($letterNumder && $letterNumder != $key) {
                continue;
            }
            if ($email) {
                $contacts = [$email];
            } else {
                $contacts = $this->getContacts($params);
            }

            if ($contacts) {
                $this->importAndSend($contacts, $params);
            }
        }
    }

    public function sendTrialExpires($email, $letterNumder)
    {
        if ($email) {
            $contacts = ($letterNumder === null || $letterNumder == 'trial_expires') ? [$email] : [];
        } else {
            $contacts = $this->getTrialExpiresContacts();
        }

        if ($contacts) {
            $params = [
                'subject' => 'Ваш пробный период заканчивается',
                'mail' => 'trial_expires',
                'days' => 13,
            ];
            $this->sendData($contacts, $params);
        }
    }

    public function getTrialExpiresContacts()
    {
        $from = time();
        $till = (new DateTime("tomorrow"))->getTimestamp();
        $query = Company::find()
            ->leftJoin('employee_company', '{{employee_company}}.[[company_id]]={{company}}.[[id]]')
            ->leftJoin('service_subscribe', '{{service_subscribe}}.[[company_id]]={{company}}.[[id]]')
            ->leftJoin('employee', '{{employee}}.[[id]]={{employee_company}}.[[employee_id]]')
            ->select('employee.email')
            ->andWhere([
                'employee_company.is_working' => true,
                'employee_company.employee_role_id' => EmployeeRole::ROLE_CHIEF,
                'company.blocked' => false,
                'company.test' => false,
                'employee.is_deleted' => false,
                'employee.is_active' => true,
                'employee.notify_new_features' => true,
                'employee.is_mailing_active' => true,
                'service_subscribe.tariff_id' => SubscribeTariff::TARIFF_ANALYTICS_TRIAL,
            ])->andWhere([
                'between',
                'service_subscribe.expired_at',
                $from,
                $till,
            ])->groupBy('employee.email');

        return $query->column();
    }

    public function getContacts($params)
    {
        $days = $params['days'] - 1;
        $date = new DateTime("-{$days} days");

        $filterByCompany = '
            (
                SELECT COUNT(c.id) 
                FROM employee_company ec 
                LEFT JOIN company c ON ec.company_id = c.id AND c.blocked = 0 AND ec.is_working = 1 
                WHERE ec.employee_id = employee.id
            ) unblocked_companies_count ';

        $query = SentEmailStart::find()
            ->joinWith('employee')
            ->select('employee.email')
            ->addSelect($filterByCompany)
            ->andWhere([
                'sent_email_start.date' => $date->format('Y-m-d'),
                'sent_email_start.sent_email_type_id' => SentEmailType::FINDIRECTOR,
            ])
            ->having(['>', 'unblocked_companies_count', 0]);

        return $query->column();
    }

    public function importAndSend($contacts, $params)
    {
        //echo var_export($contacts, true), "\n"; return;
        $this->importContacts($contacts);
        $this->sendData($contacts, $params);
    }

    public function importContacts($contacts)
    {
        $uniSender = new UniSender();
        $listId = $this->getUnisenderListId();
        $uniSender->setListId($listId);
        $offset = 0;
        do {
            $emailArray = array_slice($contacts, $offset++ * UniSender::EMPLOYEE_SELECT_LIMIT, UniSender::EMPLOYEE_SELECT_LIMIT);

            if ($emailArray) {
                $data = [];
                foreach ($emailArray as $email) {
                    $data[] = [
                        $email,
                        $listId,
                    ];
                }

                $importData = [
                    'field_names' => [
                        'email',
                        'email_list_ids',
                    ],
                    'data' => $data,
                ];

                $result = json_decode($uniSender->importContacts($importData));

                if (!empty($result->error)) {
                    echo "- import error:\n", var_export($result->error, true), "\n";
                }

            }
        } while ($emailArray);
    }

    public function sendData($emailArray, $params)
    {
        $uniSender = new UniSender();
        $listId = $this->getUnisenderListId();
        $uniSender->setListId($listId);
        if ($emailArray) {
            echo "- sending mailing...\n";
            $uniSender
                ->setEmailTemplatePath(self::$templatePath)
                ->setEmailTemplateName($params['mail'])
                ->setFromEmail(\Yii::$app->params['emailList']['info'])
                ->setFromName(self::$from)
                ->setParams([
                    'supportEmail' => \Yii::$app->params['emailList']['support'],
                    'subject' => $params['subject'],
                ])
                ->setSubject($params['subject'])
                ->setContacts($emailArray)
                ->send(false);
        }
    }

    public function getUnisenderListId()
    {
        return Yii::$app->params['uniSender']['listId'];
    }

    public static function query()
    {
        $query = EmployeeCompany::find()->joinWith([
            'employee',
            'company',
        ], false)->leftJoin(SentEmailStart::tableName(), '{{employee}}.[[id]]={{sent_email_start}}.[[employee_id]]')->andWhere([
            'employee_company.is_working' => true,
            'employee_company.employee_role_id' => EmployeeRole::ROLE_CHIEF,
            'company.blocked' => false,
            'company.test' => false,
            'company.analytics_module_activated' => true,
            'employee.is_deleted' => false,
            'employee.is_active' => true,
            'employee.notify_new_features' => true,
            'employee.is_mailing_active' => true,
            'sent_email_start.employee_id' => null,
        ])->groupBy('employee.email');

        return $query;
    }

    public static function start(int $days)
    {
        $daysOffset = 1 - $days;
        $date = new DateTime("$daysOffset days");

        $query = self::query()->select('employee.id')->andWhere([
            '<',
            'company.created_at',
            $date->getTimestamp(),
        ]);

        $result = $query->column();

        SentEmailStart::start(SentEmailType::FINDIRECTOR, $result);
    }

    /**
     * @return Company
     */
    public static function sendMail2($email)
    {
        $denyList = \common\models\employee\EmailDeny::list();
        if (isset($denyList[strtolower($email)])) {
            return;
        }

        $subject = 'Порядок в финансах - с чего начать?';
        $mailer = clone \Yii::$app->mailer;
        $mailer->htmlLayout = false;
        $mailer->compose([
            'html' => 'unisender/findirector/mail_2/html',
        ], [
            'supportEmail' => \Yii::$app->params['emailList']['support'],
            'subject' => $subject,
        ])
        ->setFrom([Yii::$app->params['emailList']['info'] => Findirector::$from])
        ->setTo($email)
        ->setSubject($subject)
        ->send();
    }
}
