<?php
namespace console\components\sender;

use common\components\sender\unisender\UniSender;
use common\models\Company;
use common\models\company\CompanyType;
use common\models\EmployeeCompany;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\notification\ForWhom;
use common\models\notification\Notification;
use DateTime;
use Yii;
use yii\db\Query;
use yii\base\Component;
use yii\base\Controller;

class TaxCalendar extends Component
{
    protected $controller;

    public function __construct(Controller $controller, $config = [])
    {
        $this->controller = $controller;

        parent::__construct($config);
    }

    public function send($email = null)
    {
        $date = new DateTime;
        $month = Yii::$app->formatter->asDate($date, 'LLLL');
        $year = $date->format('Y');
        $subject = "Налоговый календарь на {$month} {$year}";
        $dateFrom = $date->modify('first day of this month')->format('Y-m-d');
        $dateTill = $date->modify('last day of this month')->format('Y-m-d');

        $notifications = Notification::find()
            ->andWhere(['notification_type' => 1])
            ->andWhere(['between', 'event_date', $dateFrom, $dateTill])
            ->orderBy(['event_date' => SORT_ASC])
            ->all();

        if ($notifications) {
            foreach ($this->getData($email) as $key => $dataArray) {
                if ($key != 1) {
                    sleep(300);
                }
                echo "sending part #{$key}\n";

                $this->sendData($dataArray, $notifications, $subject);
            }
            echo "sending complete\n";
        } else {
            echo "notifications not found\n";
        }
    }

    public function getData($email = null)
    {
        $query = new Query;
        $query
            ->select([
                'employee.email',
                'company_id' => 'company.id',
                'company_name' => 'company.name_short',
                'company_type' => 'company_type.name_short',
                'company_type_id',
                'self_employed' => 'company.self_employed',
                'company_taxation_type.osno',
                'company_taxation_type.usn',
                'company_taxation_type.envd',
            ])
            ->from('employee_company')
            ->leftJoin('employee', '{{employee}}.[[id]] = {{employee_company}}.[[employee_id]]')
            ->leftJoin('company', '{{company}}.[[id]] = {{employee_company}}.[[company_id]]')
            ->leftJoin('company_type', '{{company_type}}.[[id]] = {{company}}.[[company_type_id]]')
            ->leftJoin('company_taxation_type', '{{company_taxation_type}}.[[company_id]] = {{company}}.[[id]]')
            ->where($email ? ['employee.email' => $email] : [
                'employee_company.employee_role_id' => EmployeeRole::ROLE_CHIEF,
                'employee_company.is_working' => true,
                'employee.is_active' => Employee::ACTIVE,
                'employee.is_deleted' => Employee::NOT_DELETED,
                'employee.is_mailing_active' => true,
                'company.blocked' => Company::UNBLOCKED,
                // 'company.strict_mode' => Company::OFF_STRICT_MODE,
            ])
            ->andWhere(['not', ['company.name_short' => null]]);

        $queryResult = $query->all();

        $resultData = [];
        foreach ($queryResult as $queryRow) {
            $email = array_shift($queryRow);
            $i = 1;
            while (isset($resultData[$i][$email])) {
                $i++;
            }
            $resultData[$i][$email] = $queryRow;
        }

        return $resultData;
    }

    public function sendData($dataArray, $allNotifications, $subject)
    {
        $uniSender = new UniSender();
        $emailArray = [];
        $offset = 0;
        do {
            $contacts = array_slice($dataArray, $offset++ * UniSender::EMPLOYEE_SELECT_LIMIT, UniSender::EMPLOYEE_SELECT_LIMIT);

            if ($contacts) {
                $emails = [];
                $data = [];
                foreach ($contacts as $email => $companyData) {
                    $companyNotifications = $this->getCompanyNotifications($allNotifications, $companyData);

                    if ($companyNotifications) {
                        $emails[] = $email;
                    }

                    $data[] = [
                        $email,
                        Yii::$app->params['uniSender']['listId'],
                        $companyData['company_type'] . ' ' . $companyData['company_name'],
                        $this->controller->renderPartial('tax-calendar', [
                            'subject' => $subject,
                            'companyData' => $companyData,
                            'companyNotifications' => $companyNotifications,
                        ]),
                    ];
                }

                $importData = [
                    'field_names' => [
                        'email',
                        'email_list_ids',
                        'company',
                        'html_content',
                    ],
                    'data' => $data,
                ];

                $result = json_decode($uniSender->importContacts($importData));

                if ($result && empty($result->error)) {
                    $emailArray = array_merge($emailArray, $emails);
                }

            }
        } while ($contacts);

        if ($emailArray) {
            $uniSender->setEmailTemplateName('tax-calendar')
                ->setFromEmail(\Yii::$app->params['emailList']['info'])
                ->setFromName('Алексей Кущенко')
                ->setParams([
                    'supportEmail' => \Yii::$app->params['emailList']['support'],
                    'subject' => $subject,
                ])
                ->setSubject($subject . '{{company? для }}{{company}}')
                ->setContacts($emailArray)
                ->send(false);

            sleep(300);
            $uniSender->clearHtmlContent($emailArray, Yii::$app->params['uniSender']['listId']);
        }
    }

    public function getCompanyNotifications($allNotifications, $companyData)
    {
        $companyNotifications = [];

        foreach ($allNotifications as $notification) {
            switch ($notification->for_whom) {
                case ForWhom::NOTIFICATION_FOR_ALL:
                    $companyNotifications[] = $notification;
                    break;

                case ForWhom::NOTIFICATION_FOR_OSNO:
                    if ($companyData['osno']) {
                        $companyNotifications[] = $notification;
                    }
                    break;

                case ForWhom::NOTIFICATION_FOR_USN_ENVD:
                    if ($companyData['usn'] || $companyData['envd']) {
                        $companyNotifications[] = $notification;
                    }
                    break;

                case ForWhom::NOTIFICATION_FOR_IP:
                    if ($companyData['company_type_id'] == CompanyType::TYPE_IP) {
                        $companyNotifications[] = $notification;
                    }
                    break;

                case ForWhom::NOTIFICATION_FOR_SELF_EMPLOYED:
                    if ($companyData['self_employed']) {
                        $companyNotifications[] = $notification;
                    }
                    break;
            }
        }

        return $companyNotifications;
    }
}