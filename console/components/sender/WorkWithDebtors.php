<?php
namespace console\components\sender;

use common\components\sender\unisender\UniSender;
use common\models\Company;
use common\models\EmployeeCompany;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\service\Subscribe;
use common\models\service\SubscribeTariffGroup;
use DateTime;
use Yii;
use yii\base\Component;

/**
 * Рассылка "Работа с должниками"
 *
 * Class WorkWithDebtors
 */
class WorkWithDebtors extends Component
{
    protected $controller;

    public function getMailingParams()
    {
        return [
            1 => [
                'day' => 1,
                'mail' => [
                    'subject' => 'Заплатит или не заплатит?',
                    'tamplate' => 'work_with_debtors/mail_1',
                    'fromName' => 'Алексей Кущенко',
                ],
            ],
            2 => [
                'day' => 2,
                'mail' => [
                    'subject' => 'Прививка от возникновения долгов',
                    'tamplate' => 'work_with_debtors/mail_2',
                    'fromName' => 'Алексей Кущенко',
                ],
            ],
        ];
    }

    /**
     *  Daily email send
     * @throws yii\base\Exception
     */
    public function send($email = null, $mailNum = null)
    {
        return;

        foreach ($this->getMailingParams() as $key => $params) {
            if ($mailNum !== null && $mailNum != $key) {
                continue;
            }
            $daysOffset = 1 - $params['day'];
            $date = new DateTime("$daysOffset days");

            $beginOfDay = clone $date;
            $beginOfDay->modify('today');

            $endOfDay = clone $beginOfDay;
            $endOfDay->modify('tomorrow');

            if ($email) {
                $emailArray = [$email];
            } else {
                $companyIds = Subscribe::find()->select([
                    'company_id',
                    'activated' => 'MIN([[activated_at]])',
                ])->andWhere([
                    'tariff_group_id' => SubscribeTariffGroup::CHECK_CONTRACTOR,
                ])->andHaving([
                    'between',
                    'activated',
                    $beginOfDay->getTimestamp(),
                    $endOfDay->getTimestamp()-1,
                ])->groupBy('company_id')->column();

                $emailArray = EmployeeCompany::find()->select(['employee.email'])
                    ->leftJoin(Employee::tableName(), '{{employee}}.[[id]] = {{employee_company}}.[[employee_id]]')
                    ->leftJoin(Company::tableName(), '{{company}}.[[id]] = {{employee_company}}.[[company_id]]')
                    ->andWhere([
                    'employee_company.company_id' => $companyIds,
                    'employee_company.is_working' => true,
                    'employee_company.employee_role_id' => EmployeeRole::ROLE_CHIEF,
                    'company.blocked' => Company::UNBLOCKED
                ])->column();
            }

            if ($emailArray) {
                echo '"', $params['mail']['subject'], '" sending... ';
                $uniSender = new UniSender();
                $uniSender->setEmailTemplateName($params['mail']['tamplate'])
                    ->setFromEmail(\Yii::$app->params['emailList']['info'])
                    ->setFromName($params['mail']['fromName'])
                    ->setParams([
                        'supportEmail' => \Yii::$app->params['emailList']['support'],
                        'callUs' => \Yii::$app->params['uniSender']['phone'],
                        'subject' => $params['mail']['subject'],
                        'day' => $params['day'],
                    ])
                    ->setContacts($emailArray)
                    ->setSubject($params['mail']['subject'])
                    ->send(false);

                echo 'completed', "\n";
            } else {
                echo '"', $params['mail']['subject'], '" no contacts', "\n";
            }
        }
    }
}
