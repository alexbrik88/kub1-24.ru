<?php
namespace console\components\sender;

use common\components\sender\unisender\UniSender;
use common\models\Company;
use common\models\Discount;
use common\models\company\RegistrationPageType;
use common\models\employee\Employee;
use common\models\service\Subscribe;
use common\models\service\SubscribeStatus;
use common\models\service\SubscribeTariff;
use DateTime;
use Yii;
use yii\base\Component;
use yii\base\Controller;
use yii\helpers\ArrayHelper;

class SubscriptionExpires extends Component
{
    protected $controller;

    public static $sendingParams = [
        1 => [
            'subject' => 'Ваша подписка скоро заканчивается',
            'days' => 5,
            'operator' => '+',
            'text' => 'text1',
            'discount' => false,
        ],
        2 => [
            'subject' => 'Завтра заканчивается ваша подписка',
            'days' => 1,
            'operator' => '+',
            'text' => 'text2',
            'discount' => false,
        ],
        3 => [
            'subject' => 'Упс… Ваш КУБ ограничен',
            'days' => 3,
            'operator' => '-',
            'text' => 'text3',
            'discount' => false,
        ],
        /*4 => [
            'subject' => 'Важно! Только для Вас!',
            'days' => 14,
            'operator' => '-',
            'text' => 'text4',
            'discount' => true,
        ],*/
    ];

    public function __construct(Controller $controller, $config = [])
    {
        $this->controller = $controller;

        parent::__construct($config);
    }

    public function send($email, $letterNumder)
    {
        $sleep = false;
        foreach (self::$sendingParams as $key => $params) {
            if ($letterNumder && $letterNumder != $key) {
                continue;
            }
            if ($email) {
                $employee = Employee::findOne(['email' => $email, 'is_deleted' => false]);
                $companyArray = ($employee && $employee->company) ? [$employee->company] : [];
            } else {
                $companyArray = $this->getByExpires($params['days'], $params['operator']);
            }

            if ($companyArray) {
                if ($sleep) {
                    sleep(300);
                } else {
                    $sleep = true;
                }
                /*if ($key == 4) {
                    $dateFrom = new DateTime('today');
                    $dateTill = new DateTime('+5 days');
                    $dateTill->setTime(23, 59, 59);
                    foreach ($companyArray as $company) {
                        Discount::createType3($company, $dateFrom, $dateTill);
                    }
                }*/

                $this->prepareAndSend($companyArray, $params, $email);
            }
        }
    }

    public function getByExpires($days, $operator = '+')
    {
        $date = new DateTime("$operator $days days");
        $from = $date->setTime(0, 0, 0)->getTimestamp();
        $till = $date->setTime(23, 59, 59)->getTimestamp();
        $query = Company::find()
            ->alias('company')
            ->addSelect(['company.*', 'MAX({{activated}}.[[expired_at]]) [[expired]]'])
            ->innerJoin(
                Subscribe::tableName() . ' activated',
                '{{company}}.[[active_subscribe_id]] = {{activated}}.[[id]] AND {{activated}}.[[status_id]] = :active',
                [':active' => SubscribeStatus::STATUS_ACTIVATED,]
            )
            ->leftJoin(
                Subscribe::tableName() . ' payed',
                '{{company}}.[[id]] = {{payed}}.[[company_id]] AND {{payed}}.[[status_id]] = :paid',
                [':paid' => SubscribeStatus::STATUS_PAYED,]
            )
            ->andWhere(['payed.id' => null])
            ->andWhere([
                'not',
                ['company.registration_page_type_id' => RegistrationPageType::PAGE_TYPE_ANALYTICS]
            ])
            ->andWhere(['company.blocked' => Company::UNBLOCKED])
            ->groupBy('{{company}}.[[id]]')
            ->having(['between', 'expired', $from, $till]);

        return $query->all();
    }

    public function prepareAndSend($companyArray, $params, $testEmail)
    {
        if ($companyArray) {
            $resultData = [];
            foreach ($companyArray as $company) {
                $email = $testEmail ? : ($company->employeeChief ? $company->employeeChief->email : null);
                if ($email === null) {
                    continue;
                }
                $i = 1;
                while (isset($resultData[$i][$email])) {
                    $i++;
                }
                $resultData[$i][$email] = $company;
            }
            foreach ($resultData as $key => $dataArray) {
                set_time_limit(1000);
                if ($key != 1) {
                    sleep(300);
                }
                echo "sending \"{$params['subject']}\" part #{$key}\n";

                $this->sendData($dataArray, $params);
            }
        }
    }

    public function sendData($dataArray, $params)
    {
        $uniSender = new UniSender();
        $emailArray = [];
        $offset = 0;
        do {
            $contacts = array_slice($dataArray, $offset++ * UniSender::EMPLOYEE_SELECT_LIMIT, UniSender::EMPLOYEE_SELECT_LIMIT);

            if ($contacts) {
                $emails = [];
                $data = [];
                foreach ($contacts as $email => $company) {
                    $employee = Employee::findOne([
                        'email' => $email,
                        'is_deleted' => false,
                        'notify_new_features' => true,
                        'is_mailing_active' => true,
                    ]);
                    if ($employee !== null) {
                        $emails[] = $email;
                        if (!$company->subscribe_payment_key) {
                            $company->subscribe_payment_key = uniqid();
                            $company->save(false, ['subscribe_payment_key']);
                        }
                        $data[] = [
                            $email,
                            Yii::$app->params['uniSender']['listId'],
                            $this->controller->renderPartial('subscription-expires', [
                                'company' => $company,
                                'employee' => $employee,
                                'params' => $params,
                            ]),
                        ];
                    }
                }

                $importData = [
                    'field_names' => [
                        'email',
                        'email_list_ids',
                        'html_content',
                    ],
                    'data' => $data,
                ];

                $result = json_decode($uniSender->importContacts($importData));

                if ($result && empty($result->error)) {
                    echo "- import Ok\n";
                    $emailArray = array_merge($emailArray, $emails);
                } elseif (!empty($result->error)) {
                    echo "- import error:\n", var_export($result->error, true), "\n";
                } else {
                    echo "- no import result\n";
                }

            }
        } while ($contacts);

        if ($emailArray) {
            echo "- sending mailing...\n";
            $uniSender->setEmailTemplateName('subscription-expires')
                ->setFromEmail(\Yii::$app->params['emailList']['info'])
                ->setFromName('Арслан Хакимов')
                ->setParams([
                    'supportEmail' => \Yii::$app->params['emailList']['support'],
                    'subject' => $params['subject'],
                ])
                ->setSubject($params['subject'])
                ->setContacts($emailArray)
                ->send(false);

            sleep(300);
            $uniSender->clearHtmlContent($emailArray, Yii::$app->params['uniSender']['listId']);
        }
    }
}
