<?php
namespace console\components\sender;

use common\components\sender\unisender\UniSender;
use common\models\Company;
use common\models\EmployeeCompany;
use common\models\company\RegistrationPageType;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\service\Subscribe;
use common\models\service\SubscribeStatus;
use common\models\service\SubscribeTariff;
use DateTime;
use Yii;
use yii\base\Component;
use yii\console\Controller;
use yii\db\Query;

class DailyMailing extends Component
{
    protected $controller;

    public function getDailyParams()
    {
        return [
            2 => [
                'day' => 2,
                'mail' => [
                    'subject' => 'Счета без ошибок',
                    'tamplate' => 'mail_2',
                    'fromName' => 'Арслан Хакимов',
                    'partial' => false,
                ],
                'params' => [
                    'activation_type' => [
                        Company::ACTIVATION_EMPTY_PROFILE,
                        Company::ACTIVATION_NO_INVOICE,
                    ],
                ],
                'paidFilter' => false,
            ],
            3 => [
                'day' => 4,
                'mail' => [
                    'subject' => 'В среднем наши клиенты получают деньги на 7 дней быстрее!',
                    'tamplate' => 'mail_3',
                    'fromName' => 'Арслан Хакимов',
                    'partial' => false,
                ],
                'params' => [
                    'activation_type' => [
                        Company::ACTIVATION_EMPTY_PROFILE,
                        Company::ACTIVATION_NO_INVOICE,
                        Company::ACTIVATION_NOT_SEND_INVOICES,
                        Company::ACTIVATION_ALL_COMPLETE,
                    ],
                ],
                'paidFilter' => false,
            ],
            4 => [
                'day' => 6,
                'mail' => [
                    'subject' => 'Удивите своих клиентов!',
                    'tamplate' => 'mail_4',
                    'fromName' => 'Арслан Хакимов',
                    'partial' => false,
                ],
                'params' => [
                    'activation_type' => [
                        Company::ACTIVATION_EMPTY_PROFILE,
                        Company::ACTIVATION_NO_INVOICE,
                        Company::ACTIVATION_NOT_SEND_INVOICES,
                        Company::ACTIVATION_ALL_COMPLETE,
                    ],
                ],
                'paidFilter' => false,
            ],
            5 => [
                'day' => 8,
                'mail' => [
                    'subject' => 'Ваш счет оплачен!',
                    'tamplate' => 'mail_5',
                    'fromName' => 'Алексей Кущенко',
                    'partial' => false,
                ],
                'params' => [
                    'activation_type' => [
                        Company::ACTIVATION_EMPTY_PROFILE,
                        Company::ACTIVATION_NO_INVOICE,
                        Company::ACTIVATION_NOT_SEND_INVOICES,
                        Company::ACTIVATION_ALL_COMPLETE,
                    ],
                ],
                'paidFilter' => false,
            ],
            6 => [
                'day' => 10,
                'mail' => [
                    'subject' => 'Денег нет, но вы там держитесь!',
                    'tamplate' => 'mail_6',
                    'fromName' => 'Алексей Кущенко',
                    'partial' => false,
                ],
                'params' => [
                    'activation_type' => [
                        Company::ACTIVATION_EMPTY_PROFILE,
                        Company::ACTIVATION_NO_INVOICE,
                        Company::ACTIVATION_NOT_SEND_INVOICES,
                        Company::ACTIVATION_ALL_COMPLETE,
                    ],
                ],
                'paidFilter' => false,
            ],
            7 => [
                'day' => 12,
                'mail' => [
                    'subject' => 'Посмотрите, ваш бизнес растет!',
                    'tamplate' => 'mail_7',
                    'fromName' => 'Алексей Кущенко',
                    'partial' => false,
                ],
                'params' => [
                    'activation_type' => [
                        Company::ACTIVATION_EMPTY_PROFILE,
                        Company::ACTIVATION_NO_INVOICE,
                        Company::ACTIVATION_NOT_SEND_INVOICES,
                        Company::ACTIVATION_ALL_COMPLETE,
                    ],
                ],
                'paidFilter' => false,
            ],
            /*9 => [
                'day' => 13,
                'mail' => [
                    'subject' => 'Забирайте, свой подарок!',
                    'tamplate' => 'mail_9',
                    'fromName' => 'Арслан Хакимов',
                    'partial' => 'mail_9',
                ],
                'params' => [
                    'activation_type' => [
                        Company::ACTIVATION_ALL_COMPLETE,
                    ],
                ],
                'paidFilter' => true,
            ],*/
            8 => [
                'day' => 14,
                'mail' => [
                    'subject' => 'Хорошие новости!',
                    'tamplate' => 'mail_8',
                    'fromName' => 'Арслан Хакимов',
                    'partial' => false,
                ],
                'params' => [
                    'activation_type' => [
                        Company::ACTIVATION_EMPTY_PROFILE,
                        Company::ACTIVATION_NO_INVOICE,
                        Company::ACTIVATION_NOT_SEND_INVOICES,
                    ],
                ],
                'paidFilter' => true,
            ],
            /*10 => [
                'day' => 14,
                'mail' => [
                    'subject' => 'Используйте Ваши бонусы!',
                    'tamplate' => 'mail_10',
                    'fromName' => 'Арслан Хакимов',
                    'partial' => 'mail_10',
                ],
                'params' => [
                    'activation_type' => [
                        Company::ACTIVATION_ALL_COMPLETE,
                    ],
                ],
                'paidFilter' => true,
            ],*/
            11 => [
                'day' => 16,
                'mail' => [
                    'subject' => 'Тариф БЕСПЛАТНО!',
                    'tamplate' => 'mail_11',
                    'fromName' => 'Алексей Кущенко',
                    'partial' => false,
                ],
                'params' => [
                    'activation_type' => [
                        Company::ACTIVATION_ALL_COMPLETE,
                    ],
                ],
                'paidFilter' => true,
            ],
            12 => [
                'day' => 20,
                'mail' => [
                    'subject' => 'Оцените сервис.',
                    'tamplate' => 'mail_12',
                    'fromName' => 'Арслан Хакимов',
                    'partial' => false,
                ],
                'params' => [
                    'activation_type' => [
                        Company::ACTIVATION_EMPTY_PROFILE,
                        Company::ACTIVATION_NO_INVOICE,
                        Company::ACTIVATION_NOT_SEND_INVOICES,
                        Company::ACTIVATION_ALL_COMPLETE,
                    ],
                ],
                'paidFilter' => false,
            ],
            13 => [
                'day' => 27,
                'mail' => [
                    'subject' => 'Пришлите ваш прайс-лист!',
                    'tamplate' => 'mail_13',
                    'fromName' => 'Алексей Кущенко',
                    'partial' => false,
                ],
                'params' => [
                    'activation_type' => [
                        Company::ACTIVATION_EMPTY_PROFILE,
                        Company::ACTIVATION_NO_INVOICE,
                        Company::ACTIVATION_NOT_SEND_INVOICES,
                        Company::ACTIVATION_ALL_COMPLETE,
                    ],
                ],
                'paidFilter' => false,
            ],
            14 => [
                'day' => 35,
                'mail' => [
                    'subject' => 'Как бороться с должниками?',
                    'tamplate' => 'mail_14',
                    'fromName' => 'Алексей Кущенко',
                    'partial' => false,
                ],
                'params' => [
                    'activation_type' => [
                        Company::ACTIVATION_EMPTY_PROFILE,
                        Company::ACTIVATION_NO_INVOICE,
                        Company::ACTIVATION_NOT_SEND_INVOICES,
                        Company::ACTIVATION_ALL_COMPLETE,
                    ],
                ],
                'paidFilter' => false,
            ],
        ];
    }

    public function __construct(Controller $controller, $config = [])
    {
        $this->controller = $controller;

        parent::__construct($config);
    }

    protected function paidFilter($query)
    {
        $subQuery = Subscribe::find()->select(['id', 'company_id'])
            ->where(['tariff_id' => SubscribeTariff::paidStandartIds()])
            ->andWhere(['not', ['status_id' => SubscribeStatus::STATUS_NEW]])
            ->groupBy('company_id');

        $query
            ->leftJoin(['subscribe' => $subQuery], '{{subscribe}}.[[company_id]] = {{company}}.[[id]]')
            ->andWhere(['subscribe.id' => null]);

        return $query;
    }

    /**
     *  Daily email send
     * @throws yii\base\Exception
     */
    public function send($email = null, $mailNum = null)
    {
        foreach ($this->dailyParams as $key => $params) {
            if ($mailNum !== null && $mailNum != $key) {
                continue;
            }
            $daysOffset = 1 - $params['day'];
            $date = new DateTime("$daysOffset days");

            $dataArray = [];
            if ($email) {
                if (($employee = Employee::findOne(['email' => $email, 'is_deleted' => false])) && $employee->company) {
                    $dataArray = [['email' => $email, 'id' => $employee->company->id]];
                }
            } else {
                $query = (new Query)
                    ->select(['employee.email', 'company.id'])
                    ->from(['link' => EmployeeCompany::tableName()])
                    ->leftJoin(['employee' => Employee::tableName()], '{{link}}.[[employee_id]] = {{employee}}.[[id]]')
                    ->leftJoin(['company' => Company::tableName()], '{{link}}.[[company_id]] = {{company}}.[[id]]')
                    ->where([
                        'employee.notify_new_features' => true,
                        'employee.is_active' => Employee::ACTIVE,
                        'employee.is_deleted' => Employee::NOT_DELETED,
                        'employee.is_mailing_active' => true,
                        'link.employee_role_id' => EmployeeRole::ROLE_CHIEF,
                        'link.is_working' => true,
                        'company.blocked' => Company::UNBLOCKED,
                        'DATE(FROM_UNIXTIME({{company}}.[[created_at]]))' => $date->format('Y-m-d'),
                    ])->andWhere([
                        'not',
                        ['company.registration_page_type_id' => RegistrationPageType::PAGE_TYPE_ANALYTICS]
                    ]);

                if ($params['paidFilter']) {
                    $query = $this->paidFilter($query);
                }

                foreach ($params['params'] as $key => $value) {
                    $query->andWhere(["company.$key" => $value]);
                }

                $dataArray = $query->all();
            }

            if ($dataArray) {
                $emailArray = [];
                if ($params['mail']['partial']) {
                    $uniSender = new UniSender();
                    $offset = 0;
                    do {
                        $contactArray = array_slice($dataArray, $offset++ * UniSender::EMPLOYEE_SELECT_LIMIT, UniSender::EMPLOYEE_SELECT_LIMIT);

                        if ($contactArray) {
                            $emails = [];
                            $data = [];
                            foreach ($contactArray as $contact) {
                                $emails[] = $contact['email'];
                                $company = Company::findOne($contact['id']);
                                if (!$company->subscribe_payment_key) {
                                    $company->subscribe_payment_key = uniqid();
                                    $company->save(false, ['subscribe_payment_key']);
                                }
                                $data[] = [
                                    $contact['email'],
                                    Yii::$app->params['uniSender']['listId'],
                                    $this->controller->renderPartial("daily/{$params['mail']['partial']}", [
                                        'company' => $company,
                                        'subject' => $params['mail']['subject'],
                                    ]),
                                ];
                            }

                            $importData = [
                                'field_names' => [
                                    'email',
                                    'email_list_ids',
                                    'html_content',
                                ],
                                'data' => $data,
                            ];

                            $result = json_decode($uniSender->importContacts($importData));

                            if ($result && empty($result->error)) {
                                echo "- import Ok\n";
                                $emailArray = array_merge($emailArray, $emails);
                            } elseif (!empty($result->error)) {
                                echo "- import error:\n", var_export($result->error, true), "\n";
                            } else {
                                echo "- no import result\n";
                            }
                        }
                    } while ($contactArray);
                } else {
                    $emailArray = $dataArray;
                }

                if ($emailArray) {
                    echo '"', $params['mail']['subject'], '" sending... ';
                    $uniSender = new UniSender();
                    $uniSender->setEmailTemplateName($params['mail']['tamplate'])
                        ->setFromEmail(\Yii::$app->params['emailList']['info'])
                        ->setFromName($params['mail']['fromName'])
                        ->setParams([
                            'supportEmail' => \Yii::$app->params['emailList']['support'],
                            'callUs' => \Yii::$app->params['uniSender']['phone'],
                            'subject' => $params['mail']['subject'],
                            'day' => $params['day'],
                        ])
                        ->setContacts($emailArray)
                        ->setSubject($params['mail']['subject'])
                        ->send(false);

                    sleep(300);
                    $uniSender->clearHtmlContent($emailArray, Yii::$app->params['uniSender']['listId']);

                    echo 'completed', "\n";
                } else {
                    echo '"', $params['mail']['subject'], '" no contacts', "\n";
                }
            }
        }
    }
}
