<?php
return [
    'components' => [
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => '',
        ],
        'urlManager' => [
            'hostInfo' => 'http://hostname',
        ],
        'user' => [
            'loginUrl' => 'https://kub-24.ru/login/',
        ],
        'session' => [
            'name' => 'PHPFRONTSESSID_prod',
        ],
    ],
];
