<?php
return [
    // Domains must match the domain that is specified in "components > user > loginUrl"
    'corsFilter' => [
        'https://www.kub-24.ru',
        'https://kub-24.ru',
    ],
];
