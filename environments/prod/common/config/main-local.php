<?php
return [
    'name' => 'service.prod',

    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=lkdevirtru_lkdevirt',
            'username' => 'lkdevirtru_lkd',
            'password' => '',
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@common/mail',
        ],

        'assetManager' => [
            'forceCopy' => false,
            'linkAssets' => true,
        ],
    ],
];
