<?php
return [
    'emailList' => [
        'admin' => 'admin@domain',
        'info' => 'info@domain',
        'support' => 'support@domain',
    ],

    'service' => [
        'company_id' => 0,
        'contractor_id' => 0,
        'remote_employee_id' => null,
        'demo_company_id' => null,
        'demo_employee_id' => null,
    ],
    'serviceSite' => 'http://kub-24.ru',

    'robokassa' => [
        'merchantFormAction' => 'https://merchant.roboxchange.com/Index.aspx',
        'merchantLogin' => '',
        'password1' => '',
        'password2' => '',
        'isTest' => 0,
        'culture' => 'ru',
        'requestMethod' => 'post', // 'post' or 'get' in lower case. Call Yii::$app->request->{method}()

        'errorLogSubject' => 'Online payment ERROR',
        'successLogSubject' => 'Online payment SUCCESSFUL',
    ],
    'uniSender' => [
        'apiKey'          => '',
        'fromEmail'       => '',
        'fromName'        => '',
        'templatesFolder' => '',
        'baseUrl'         => '',
    ],

    // bank integration module params
    'banking' => [
        // tinkoff bank
        '044525974' => [
            'ssl_key' => 'tinkoff.key',
            'ssl_cert' => 'tinkoff.cer',
            'ssl_key_pass' => '',
            'auth_basic' => '',
            'client_id' => '',
            'securityKey' => '',
            'partnerId' => '',
        ],
    ],
];
