<?php
return [
    'adminEmail' => 'admin@domain',
    'supportEmail' => 'support@domain',
    'user.passwordResetTokenExpire' => 3600,
];
