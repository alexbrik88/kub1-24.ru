<?php
return [
    'emailList' => [
        'admin' => 'admin@domain',
        'info' => 'info@domain',
        'support' => 'support@domain',
    ],
    'service' => [
        'company_id' => 0,
        'contractor_id' => 0,
        'remote_employee_id' => null,
        'demo_company_id' => null,
        'demo_employee_id' => null,
    ],
    'serviceSite' => 'http://kub-24.ru',

    'facebook' => [
        'api' => [
            'access_token' => null,
        ],
        'pixel_id' => null,
        'tracker_log' => false,
    ],

    'robokassa' => [
        'merchantFormAction' => 'https://merchant.roboxchange.com/Index.aspx',
        'merchantLogin' => '',
        'password1' => '',
        'password2' => '',
        'isTest' => 1,
        'culture' => 'ru',
        'requestMethod' => 'get', // 'post' or 'get' in lower case. Call Yii::$app->request->{method}()

        'errorLogSubject' => 'Online payment ERROR',
        'successLogSubject' => 'Online payment SUCCESSFUL',
    ],
    'uniSender' => [
        'apiKey' => '5ui9tajabarz87mochxqdswaidf5m3im63jnntpa',
        'fromEmail' => 'camarro88@gmail.com',
        'fromName' => 'support@kub-24.ru',
        'templatesFolder' => '@frontend/email/unisender',
        'baseUrl' => 'http://smartplus',
        'lkBaseUrl' => 'http://kub-24.ru',
        'domainName' => 'kub-24.ru',
        'imgPath' => '@frontend/web/img/unisender',
        'listId' => 6451613,
        'phone' => '+7 (977) 813-42-50',
        'feedback' => 'https://kub-24.ru/pomoshh/',
        'imgUrl' => 'http://dev.kub-24.ru/img/unisender',
        'utm' => [
            'logo' => '/?utm_source=mailing-list&utm_medium=email&utm_campaign=logo',
            'entrance' => '/login/?utm_source=mailing-list&utm_medium=email&utm_campaign=login-top',
            'entranceKub' => '/login/?utm_source=mailing-list&utm_medium=email&utm_campaign=login-bottom',
            'leaveMessage' => '/pomoshh/?utm_source=mailing-list&utm_medium=email&utm_campaign=help',
            'billInvoice' => '/documents/invoice/create?type=2?utm_source=mailing-list&utm_medium=email&utm_campaign=statistics-bill',
            'addCosts' => '/documents/invoice/create?type=1?utm_source=mailing-list&utm_medium=email&utm_campaign=statistics-costs',
        ],
    ],

    // bank integration module params
    'banking' => [
        // tinkoff bank
        '044525974' => [
            'ssl_key' => 'tinkoff.key',
            'ssl_cert' => 'tinkoff.cer',
            'ssl_key_pass' => '',
            'auth_basic' => '',
            'client_id' => '',
            'securityKey' => '',
            'partnerId' => '',
        ],
    ],

    //Analytics module free access
    'analytics-free-access' => false,
];
