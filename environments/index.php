<?php
/**
 * The manifest of files that are local to specific environment.
 * This file returns a list of environments that the application
 * may be installed under. The returned data must be in the following
 * format:
 *
 * ```php
 * return [
 *     'environment name' => [
 *         'path' => 'directory storing the local files',
 *         'setWritable' => [
 *             // list of directories that should be set writable
 *         ],
 *         'setExecutable' => [
 *             // list of files that should be set executable
 *         ],
 *         'setCookieValidationKey' => [
 *             // list of config files that need to be inserted with automatically generated cookie validation keys
 *         ],
 *         'createSymlink' => [
 *             // list of symlinks to be created. Keys are symlinks, and values are the targets.
 *         ],
 *     ],
 * ];
 */

$setWritable = [
    // custom
    'common/uploads/company',
    'common/uploads/documents/act',
    'common/uploads/documents/invoice',
    'common/uploads/documents/invoice_facture',
    'common/uploads/documents/packing-list',
    'common/uploads/report_file',
    'common/uploads/template_file',
    'frontend/runtime/export',

    // mpdf
    'vendor/mpdf/mpdf/tmp',
    'vendor/mpdf/mpdf/ttfontdata',
    'vendor/mpdf/mpdf/graph_cache',
];

return [
    'Development' => [
        'path' => 'dev',
        'setWritable' => array_merge([
            'backend/runtime',
            'backend/web/assets',
            'frontend/runtime',
            'frontend/web/assets',
            'retail/runtime',
            'retail/web/assets',
            'store/runtime',
            'store/web/assets',
        ], $setWritable),
        'setExecutable' => [
            'yii',
        ],
        'setCookieValidationKey' => [
            'backend/config/main-local.php',
            'frontend/config/main-local.php',
            'retail/config/main-local.php',
            'store/config/main-local.php',
        ],
    ],
    'Production' => [
        'path' => 'prod',
        'setWritable' => array_merge([
            'backend/runtime',
            'backend/web/assets',
            'frontend/runtime',
            'frontend/web/assets',
            'retail/runtime',
            'retail/web/assets',
            'store/runtime',
            'store/web/assets',
        ], $setWritable),
        'setExecutable' => [
            'yii',
        ],
        'setCookieValidationKey' => [
            'backend/config/main-local.php',
            'frontend/config/main-local.php',
            'retail/config/main-local.php',
            'store/config/main-local.php',
        ],
    ],
];
