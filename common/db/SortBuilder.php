<?php

namespace common\db;

use Yii;
use yii\data\Sort;

final class SortBuilder
{
    /**
     * @var array
     */
    private $params = [];

    /**
     * @param string $attribute
     * @param string $table
     * @param string|null $column
     * @param int $order
     * @return void
     */
    public function addOrder(string $attribute, string $table, ?string $column = null, int $order = SORT_ASC): void
    {
        $this->setAsc($attribute, $table, $column, $order);
        $this->setDesc($attribute, $table, $column, ($order == SORT_ASC) ? SORT_DESC : SORT_ASC);
    }

    /**
     * @param array $attributes
     * @param string $table
     * @param string $column
     * @param int $order
     * @return void
     */
    public function addOrders(array $attributes, string $table, string $column, int $order = SORT_ASC): void
    {
        foreach ($attributes as $attribute) {
            $this->addOrder($attribute, $table, $column, $order);
        }
    }

    /**
     * @param string $attribute
     * @param string $table
     * @param string|null $column
     * @param int $order
     * @return void
     */
    public function setAsc(string $attribute, string $table, ?string $column = null, int $order = SORT_ASC): void
    {
        $this->setOrder($attribute, 'asc', $table, $column, $order);
    }

    /**
     * @param string $attribute
     * @param string $table
     * @param string|null $column
     * @param int $order
     * @return void
     */
    public function setDesc(string $attribute, string $table, ?string $column = null, int $order = SORT_DESC): void
    {
        $this->setOrder($attribute, 'desc', $table, $column, $order);
    }

    /**
     * @param string $attribute
     * @param int $order
     * @return void
     */
    public function addDefaultOrder(string $attribute, int $order = SORT_ASC): void
    {
        $this->params['defaultOrder'][$attribute] = $order;
    }

    /**
     * @return Sort
     */
    public function buildSort(): Sort
    {
        return new Sort($this->params);
    }

    /**
     * @param string $table
     * @param string $column
     * @return string
     */
    private function getFullName(string $table, string $column): string
    {
        return sprintf('%s.%s', Yii::$app->db->schema->getRawTableName($table), $column);
    }

    /**
     * @param string $attribute
     * @param string $table
     * @param string $direction
     * @param string|null $column
     * @param int $order
     * @return void
     */
    private function setOrder(
        string $attribute,
        string $direction,
        string $table,
        ?string $column,
        int $order
    ): void {
        if (!isset($this->params['attributes'][$attribute][$direction])) {
            $this->params['attributes'][$attribute][$direction] = [];
        }

        if ($column === null) {
            $column = $attribute;
        }

        $this->params['attributes'][$attribute][$direction][$this->getFullName($table, $column)] = $order;
    }
}
