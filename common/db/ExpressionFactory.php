<?php

namespace common\db;

use yii\db\Expression;

class ExpressionFactory
{
    /**
     * @param int $expiresIn
     * @return Expression
     */
    public function createExpiredAtExpression(int $expiresIn): Expression
    {
        return new Expression("DATE_ADD(NOW(), INTERVAL {$expiresIn} SECOND)");
    }

    /**
     * @return Expression
     */
    public function createNowExpression(): Expression
    {
        return new Expression('NOW()');
    }
}
