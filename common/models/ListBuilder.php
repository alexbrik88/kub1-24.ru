<?php

namespace common\models;

use yii\data\DataProviderInterface;
use yii\data\Sort;
use RuntimeException;

class ListBuilder
{
    /** @var int */
    private const DEFAULT_LIMIT = 100;

    /**
     * @var callable
     */
    private $formatKeyCallback;

    /**
     * @var callable
     */
    private $formatValueCallback;

    /**
     * @var int
     */
    private $limit = self::DEFAULT_LIMIT;

    /**
     * @var string|null
     */
    private $indexBy;

    /**
     * @var Sort|null
     */
    private $sort;

    /**
     *
     */
    public function __construct()
    {
        $this->formatKeyCallback = [$this, 'formatKey'];
        $this->formatValueCallback = [$this, 'formatValue'];
    }

    /**
     * @param string $indexBy
     * @return void
     */
    public function setIndexBy(string $indexBy): void
    {
        $this->indexBy = $indexBy;
    }

    /**
     * @param Sort $sort
     * @return void
     */
    public function setSort(Sort $sort): void
    {
        $this->sort = $sort;
    }

    /**
     * @param int $limit
     */
    public function setLimit(int $limit): void
    {
        $this->limit = $limit;
    }

    /**
     * @param callable $formatValueCallback
     * @return void
     */
    public function setFormatValueCallback(callable $formatValueCallback): void
    {
        $this->formatValueCallback = $formatValueCallback;
    }

    /**
     * @param callable $formatKeyCallback
     * @return void
     */
    public function setFormatKeyCallback(callable $formatKeyCallback): void
    {
        $this->formatKeyCallback = $formatKeyCallback;
    }

    /**
     * @param DataProviderInterface $dataProvider
     * @param string $valueKey
     * @return string[]
     * @throws RuntimeException
     */
    public function buildItems(DataProviderInterface $dataProvider, string $valueKey): array
    {
        $this->updatePagination($dataProvider);
        $this->updateSort($dataProvider);
        $dataProvider->prepare(true);
        $models = $dataProvider->getModels();
        $items = [];

        array_walk($models, function ($model, $keyValue) use (&$items, $valueKey) {
            $key = call_user_func($this->formatKeyCallback, $keyValue, $model);
            $value = call_user_func($this->formatValueCallback, $valueKey, $model);
            $items[$key] = (string) $value;
        });

        return $items;
    }

    /**
     * @param DataProviderInterface $dataProvider
     * @return void
     */
    private function updateSort(DataProviderInterface $dataProvider): void
    {
        if (false && $this->sort) { // TODO: need fix it
            $properties = get_object_vars($this->sort);
            $sort = $dataProvider->getSort();

            array_walk($properties, function ($value, string $name) use ($sort): void  {
                $sort->$name = $value;
            });

            $sort->init();
        }
    }

    /**
     * @param DataProviderInterface $dataProvider
     * @return void
     */
    private function updatePagination(DataProviderInterface $dataProvider): void
    {
        if ($dataProvider->getPagination() !== false) {
            $dataProvider->getPagination()->setPageSize($this->limit, false);
        }
    }

    /**
     * @param string|int $keyValue Значение ключа
     * @param object|array $model
     * @return string|int
     * @throws RuntimeException
     */
    protected function formatKey($keyValue, $model)
    {
        if ($this->indexBy === null) {
            return $keyValue;
        }

        if (is_array($model)) {
            return $model[$this->indexBy];
        }

        if (is_object($model)) {
            return $model->{$this->indexBy};
        }

        throw new RuntimeException(sprintf('Unsupported data type: %s', gettype($model)));
    }

    /**
     * @param string $valueKey Ключ значения
     * @param object|array $model
     * @return string
     * @throws RuntimeException
     */
    protected function formatValue(string $valueKey, $model): string
    {
        if (is_array($model)) {
            return (string) $model[$valueKey];
        }

        if (is_object($model)) {
            return (string) $model->$valueKey;
        }

        throw new RuntimeException(sprintf('Unsupported data type: %s', gettype($model)));
    }
}
