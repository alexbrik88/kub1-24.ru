<?php

namespace common\models\integration;

use Yii;
use common\models\Company;

/**
 * This is the model class for table "last_operation_in_integration".
 *
 * @property int $company_id
 * @property int $type
 * @property int $date
 *
 * @property Company $company
 */
class LastOperationInIntegration extends \yii\db\ActiveRecord
{
    const YANDEX_DIRECT_TYPE = 0;
    const MONETA_TYPE = 1;
    const GOOGLE_ADS_TYPE = 2;
    const VK_ADS_TYPE = 3;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'last_operation_in_integration';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'type', 'date'], 'required'],
            [['company_id', 'type', 'date'], 'integer'],
            [['type'], 'in', 'range' => [
                self::YANDEX_DIRECT_TYPE,
                self::MONETA_TYPE,
                self::GOOGLE_ADS_TYPE,
                self::VK_ADS_TYPE,
            ]],
            [['company_id', 'type'], 'unique', 'targetAttribute' => ['company_id', 'type']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'company_id' => 'Company ID',
            'type' => 'Type',
            'date' => 'Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    public static function updateIntegrationDate(Company $company, $type, $date)
    {
        /** @var self $model */
        $model = self::find()
            ->andWhere(['company_id' => $company->id])
            ->andWhere(['type' => $type])
            ->one();
        if ($model === null) {
            $model = new self();
            $model->company_id = $company->id;
            $model->type = $type;
        }

        $model->date = $date;

        return $model->save();
    }
}
