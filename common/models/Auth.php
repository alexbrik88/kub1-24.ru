<?php

namespace common\models;

use common\models\employee\Employee;
use Yii;
use yii\helpers\Json;

/**
 * This is the model class for table "auth".
 *
 * @property int $id
 * @property int $employee_id
 * @property string $auth_id
 * @property string $auth_user_id
 * @property string $user_data
 * @property int $created_at
 *
 * @property Employee $employee
 */
class Auth extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'createdAtBehavior' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'updatedAtAttribute' => false,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'auth';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'employee_id' => 'Employee ID',
            'auth_id' => 'Auth ID',
            'auth_user_id' => 'Auth User ID',
            'user_data' => 'User Data',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @param $data array
     */
    public function setUserData(array $data)
    {
        $this->user_data = Json::encode($data);
    }

    /**
     * @return array
     */
    public function getUserData()
    {
        return (array) Json::decode($this->user_data);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }
}
