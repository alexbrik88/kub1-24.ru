<?php

namespace common\models\cash;

use Yii;
use common\models\Company;
use common\models\Contractor;
use common\models\cash\Cashbox;
use common\models\cash\CashFlowsBase;
use common\models\company\CheckingAccountant;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;

/**
 * This is the model class for table "cash_flow_linkage".
 *
 * @property int $id
 * @property int $company_id
 * @property int $number
 * @property int $status
 * @property int $is_deleted
 * @property int $if_flow_type
 * @property int $if_wallet_id
 * @property int $if_contractor_id
 * @property int $then_flow_type
 * @property int $then_wallet_type
 * @property int $then_wallet_id
 * @property int $then_contractor_id
 * @property int $then_date_diff
 * @property int $then_amount_action
 * @property float $then_amount_action_value
 * @property string $then_description
 * @property int|null $created_at
 * @property int|null $updated_at
 * @property int|null $created_by
 * @property int|null $updated_by
 *
 * @property Company $company
 */
class CashFlowLinkage extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;

    const ACTION_MINUS_PERCENT = 1;
    const ACTION_TAKE_PERCENT = 2;

    private static $created = false;

    public static $flowTypeItems = [
        CashFlowsBase::FLOW_TYPE_INCOME => 'Приход',
        CashFlowsBase::FLOW_TYPE_EXPENSE => 'Расход',
    ];

    public static $thenActionItems = [
        CashFlowLinkage::ACTION_MINUS_PERCENT => 'За минусом',
        CashFlowLinkage::ACTION_TAKE_PERCENT => 'Только процент',
    ];

    public static $thenWallets = [
        CashFlowsBase::WALLET_BANK,
        CashFlowsBase::WALLET_CASHBOX,
    ];

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'yii\behaviors\TimestampBehavior',
            'yii\behaviors\BlameableBehavior',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cash_flow_linkage';
    }

    /**
     * @return yii\db\ActiveQuery
     */
    public static function find()
    {
        return new query\CashFlowLinkageQuery(get_called_class());
    }

    public function setThenWalletId($value)
    {
        $value = strval($value);

        $this->then_wallet_type = substr($value, 0, 1) ?: null;
        $this->then_wallet_id = substr($value, 2) ?: null;
    }

    public function getThenWalletId()
    {
        return printf('%s_%s', $this->then_wallet_type, $this->then_wallet_id);
    }

    /**
     * @return array
     */
    public static function dateDiffItems()
    {
        $items = [];
        foreach (range(0, 10) as $value) {
            $items[$value] = $value == 0 ? 'В тот же день' : '+'.$value;
        }

        return $items;
    }

    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->is_deleted = 0;
                $this->number = 1 + (int) (new \yii\db\Query)->from(self::tableName())->where([
                    'company_id' => $this->company_id,
                ])->max('number');
            }

            return true;
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function afterFind()
    {
        parent::afterFind();

        $this->then_amount_action_value *=1;
    }

    /**
     * {@inheritdoc}
     */
    public function delete()
    {
        return $this->updateAttributes(['is_deleted' => true]) ? true : false;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'if_flow_type',
                    'if_wallet_id',
                    'if_contractor_id',
                    'then_flow_type',
                    'then_wallet_type',
                    'then_wallet_id',
                    'then_contractor_id',
                    'then_date_diff',
                    'then_amount_action',
                    'then_amount_action_value',
                    'then_description',
                ], 
                'required',
            ],
            [
                [
                    'if_flow_type',
                    'if_wallet_id',
                    'then_flow_type',
                    'then_wallet_type',
                    'then_wallet_id',
                    'then_amount_action',
                ],
                'integer',
            ],
            [
                [
                    'if_flow_type',
                    'then_flow_type',
                ],
                'in',
                'range' => [
                    CashFlowsBase::FLOW_TYPE_INCOME,
                    CashFlowsBase::FLOW_TYPE_EXPENSE,
                ],
            ],
            [
                [
                    'if_wallet_id',
                ],
                'in',
                'range' => [
                    CashFlowsBase::WALLET_BANK,
                ],
            ],
            [
                [
                    'then_amount_action',
                ],
                'in',
                'range' => [
                    CashFlowLinkage::ACTION_MINUS_PERCENT,
                    CashFlowLinkage::ACTION_TAKE_PERCENT,
                ],
            ],
            [['then_date_diff'], 'integer', 'min' => 0, 'max' => 10],
            [['then_amount_action_value'], 'number', 'min' => 0, 'max' => 99.99],
            [
                [
                    'if_wallet_id',
                ], 
                'in',
                'range' => [
                    CashFlowsBase::WALLET_BANK,
                ],
            ],
            [
                [
                    'then_wallet_type',
                ],
                'in',
                'range' => CashFlowLinkage::$thenWallets,
            ],
            [
                [
                    'then_wallet_id',
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => CheckingAccountant::class,
                'targetAttribute' => 'id',
                'filter' => ['company_id' => $this->company_id],
                'when' => function ($model) {
                    return $model->then_wallet_type == CashFlowsBase::WALLET_BANK;
                }
            ],
            [
                [
                    'then_wallet_id',
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => Cashbox::class,
                'targetAttribute' => 'id',
                'filter' => ['company_id' => $this->company_id],
                'when' => function ($model) {
                    return $model->then_wallet_type == CashFlowsBase::WALLET_CASHBOX;
                }
            ],
            [
                [
                    'if_contractor_id',
                    'then_contractor_id',
                ],
                'exist',
                'skipOnError' => true,
                'targetClass' => Contractor::class,
                'targetAttribute' => 'id',
                'filter' => ['company_id' => $this->company_id],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'number' => '№№',
            'status' => 'Статус',
            'is_deleted' => 'Is Deleted',
            'if_flow_type' => 'For Flow Type',
            'if_wallet_id' => 'For Wallet ID',
            'if_contractor_id' => 'For Contractor ID',
            'then_flow_type' => 'Create Flow Type',
            'then_wallet_id' => 'Create Wallet ID',
            'then_wallet_table' => 'Create Wallet Table',
            'then_contractor_id' => 'Create Contractor ID',
            'then_date_diff' => 'Date Diff',
            'then_amount_action_value' => 'Amount Diff',
            'then_description' => 'Amount Diff',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'created_by' => 'Created By',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * Gets query for [[Contractor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIfContractor()
    {
        return $this->hasOne(Contractor::className(), ['id' => 'if_contractor_id']);
    }

    /**
     * Gets query for [[Contractor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getThenCashbox()
    {
        return $this->hasOne(Cashbox::className(), ['id' => 'then_wallet_id']);
    }

    /**
     * Gets query for [[Contractor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getThenCheckingAccountant()
    {
        return $this->hasOne(CheckingAccountant::className(), ['id' => 'then_wallet_id']);
    }

    /**
     * @return \yii\db\ActiveRecord
     */
    public function getThenWallet()
    {
        switch ($this->then_wallet_type) {
            case CashFlowsBase::WALLET_BANK:
                return $this->thenCheckingAccountant;
                break;

            case CashFlowsBase::WALLET_CASHBOX:
                return $this->thenCashbox;
                break;

            default:
                return null;
                break;
        }
    }

    /**
     * Gets query for [[Contractor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getThenContractor()
    {
        return $this->hasOne(Contractor::className(), ['id' => 'then_contractor_id']);
    }

    /**
     * @return string
     */
    public function getIfFlowType()
    {
        return self::$flowTypeItems[$this->if_flow_type] ?? '';
    }

    /**
     * @return string
     */
    public function getThenFlowType()
    {
        return self::$flowTypeItems[$this->then_flow_type] ?? '';
    }

    /**
     * @return string
     */
    public function getThenAmountAction()
    {
        return self::$thenActionItems[$this->then_amount_action] ?? '';
    }

    /**
     * @return string
     */
    public function getThenDateDiff()
    {
        return self::dateDiffItems()[$this->then_date_diff] ?? '';
    }

    /**
     * @return int
     */
    public function getNewAmount(CashFlowsBase $flow) : int
    {
        $amount = $flow->amount;
        $value = $this->then_amount_action_value;
        switch ($this->then_amount_action) {
            case CashFlowLinkage::ACTION_MINUS_PERCENT:
                $newAmount = round($amount - ($amount / 100 * $this->then_amount_action_value));
                break;

            case CashFlowLinkage::ACTION_TAKE_PERCENT:
                $newAmount = round($amount / 100 * $this->then_amount_action_value);
                break;

            default:
                $newAmount = $amount;
                break;
        }

        return $newAmount;
    }

    /**
     * @return int
     */
    public function getNewModel(CashFlowsBase $flow) : CashFlowsBase
    {
        $date = date_create($flow->date);
        if ($this->then_date_diff) {
            $date->modify("+{$this->then_date_diff} days");
        }
        $amount = $this->getNewAmount($flow);
        $wallet = $this->getThenWallet();

        $model = $wallet->getCashFlowNewModel();

        if ($model->hasAttribute('number') && method_exists($wallet, 'getNextNumber')) {
            $model->number = $wallet->getNextNumber($this->then_flow_type);
        }

        if ($model->hasAttribute('author_id')) {
            $model->author_id = $this->created_by;
        }

        $model->flow_type = $this->then_flow_type;
        $model->date = $date->format('d.m.Y');
        $model->description = $this->then_description ?: $flow->description;
        $model->amount = $amount/100;

        return $model;
    }

    /**
     * @return bool
     */
    public function createFlow(CashFlowsBase $flow) : bool
    {
        if ($flow->linkage_id) return false;

        $contractor = $this->thenContractor;

        if ($contractor !== null) {

            $model = $this->getNewModel($flow);
            $model->contractor_id = $contractor->id;
            $model->linkage_id = $this->id;

            if ($this->then_flow_type == CashFlowsBase::FLOW_TYPE_INCOME) {
                $model->income_item_id = $contractor->invoice_income_item_id ?: InvoiceIncomeItem::ITEM_PAYMENT_FROM_BUYER;
            } elseif ($this->then_flow_type == CashFlowsBase::FLOW_TYPE_EXPENSE) {
                $model->expenditure_item_id = $contractor->invoice_expenditure_item_id ?: InvoiceExpenditureItem::ITEM_CONTRACTOR_PAYMENT;
            }

            if ($model->save()) {
                return true;
            }

            \common\components\helpers\ModelHelper::logErrors($model, __METHOD__);
        }

        return false;
    }
}
