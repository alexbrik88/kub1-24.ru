<?php

namespace common\models\cash;

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\components\cash\InternalTransferFlowInterface;
use common\components\cash\InternalTransferHelper;
use common\components\pdf\Printable;
use common\models\Company;
use common\models\Contractor;
use common\models\cash\query\CashFlowBase;
use common\models\currency\Currency;
use common\models\document\ForeignCurrencyInvoice;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\employee\Employee;
use frontend\models\log\Log;
use frontend\modules\analytics\behaviors\credits\CreditFlowBehavior;
use frontend\modules\cash\models\CashContractorType;
use Yii;
use yii\db\ActiveQuery;
use yii\helpers\Html;

/**
 * This is the model class for table "cash_order_foreign_currency_flows".
 *
 * @property int $id
 * @property int $company_id
 * @property int $cashbox_id
 * @property int $author_id
 * @property string $currency_id
 * @property string $currency_name
 * @property string $date
 * @property string|null $recognition_date
 * @property int $is_internal_transfer
 * @property string|null $internal_transfer_account_table
 * @property int|null $internal_transfer_account_id
 * @property string|null $internal_transfer_flow_table
 * @property int|null $internal_transfer_flow_id
 * @property int $flow_type 0 - расход, 1 - приход
 * @property string|null $number
 * @property string $contractor_id
 * @property int $amount
 * @property string|null $description
 * @property string|null $other
 * @property string|null $application
 * @property int|null $expenditure_item_id
 * @property int|null $income_item_id
 * @property int|null $reason_id
 * @property int|null $cash_funding_type_id
 * @property int|null $project_id
 * @property int $created_at
 * @property int $is_accounting Учёт в бухгалтерии: 0 - нет, 1 - да
 * @property int $has_invoice Связь со счётом: 0 - нет, 1 - да
 * @property int $is_taxable
 * @property int $is_prepaid_expense
 * @property int $is_funding_flow
 *
 * @property string $contractorInput
 *
 * @property Employee $author
 * @property CashFundingType $cashFundingType
 * @property CashOrderForeignCurrencyFlowsToInvoice[] $cashOrderForeignCurrencyFlowsToInvoices
 * @property Cashbox $cashbox
 * @property Company $company
 * @property Currency $currency
 * @property InvoiceExpenditureItem $expenditureItem
 * @property InvoiceIncomeItem $incomeItem
 * @property ForeignCurrencyInvoice[] $invoices
 * @property CashOrdersReasonsTypes $reason
 *
 */
class CashOrderForeignCurrencyFlows extends CashFlowsBase implements Printable, InternalTransferFlowInterface
{
    public function getIsForeign()
    {
        return true;
    }

    /**
     * Returns next flow number.
     * If passed existed model with same type - return it's number.
     * @param int $flowType
     * @param CashOrderFlows $model
     * @return int
     */
    public static function getNextNumber($companyId, $flowType, $date = null, $number = null)
    {
        $year = ($date && ($d = date_create($date))) ? $d->format('Y') : date('Y');
        $query = self::find()->where([
            'and',
            ['company_id' => $companyId],
            ['flow_type' => $flowType],
            ['between', 'date', "$year-01-01", "$year-12-31"],
        ]);

        $nextNumber = ($number ? : (int) $query->max('(number * 1)')) + 1;

        if ($query->andWhere(['number' => $nextNumber])->exists()) {
            return static::getNextNumber($companyId, $flowType, $date, $nextNumber);
        } else {
            return $nextNumber;
        }
    }

    /**
     * Set next flow number.
     */
    public function setNextNumber($number = null) : void
    {
        $this->number = self::getNextNumber($this->company_id, $this->flow_type, $this->date, $number);
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cash_order_foreign_currency_flows';
    }

    public function getCashContractorTypeText() : string
    {
        return \frontend\modules\cash\models\CashContractorType::ORDER_CURRENCY_TEXT;
    }

    public function getInternalTransferAccountClass() : ?string
    {
        return InternalTransferHelper::accountClassByTable($this->internal_transfer_account_table);
    }

    public function getInternalTransferFlowClass() : ?string
    {
        return InternalTransferHelper::flowClassByTable($this->internal_transfer_flow_table);
    }

    public function getSelfAccount() : ActiveQuery
    {
        return $this->getCashbox();
    }

    public function getInternalTransferAccount() : ?ActiveQuery
    {
        if ($class = $this->getInternalTransferAccountClass()) {
            return $this->hasOne($class, ['id' => 'internal_transfer_account_id']);
        }

        return null;
    }

    public function getInternalTransferFlow() : ?ActiveQuery
    {
        if ($class = $this->getInternalTransferFlowClass()) {
            return $this->hasOne($class, ['id' => 'internal_transfer_flow_id']);
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function getPdfFileName()
    {
        return $this->getPrintTitle() . '.pdf';
    }

    /**
     * @inheritdoc
     */
    public function getPrintTitle()
    {
        return str_replace([' ', '"'], ['_'],
            $this->name . '_' .
            date_format(date_create($this->date), 'd.m.Y') . '_' .
            $this->company->getTitle(true)
        );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return ($this->flow_type == self::FLOW_TYPE_INCOME ? 'ПКО' : 'РКО') . ' №' . $this->number;
    }

    /**
     * Get formatted log message from Log model
     * @param Log $log
     * @return string
     */
    public function getLogMessage(Log $log)
    {
        return $this->getLogMessageBase($log, 'по кассе');
    }

    /**
     * @param string $value
     */
    public function setAmount_input($value)
    {
        $value = strtr($value, [',' => '.']);
        $this->amount = is_numeric($value) ? $value * 100 : $value;
    }

    /**
     * @return string
     */
    public function getAmount_input()
    {
        return is_numeric($this->amount) ? $this->amount / 100 : $this->amount;
    }

    /**
     * @param string $value
     */
    public function setDate_input($value)
    {
        $this->date = ($d = date_create_from_format('d.m.Y', $value)) ? $d->format('Y-m-d') : $value;
    }

    /**
     * @return string
     */
    public function getDate_input()
    {
        return ($d = date_create_from_format('Y-m-d', $this->date)) ? $d->format('d.m.Y') : $this->date;
    }

    /**
     * @param string $value
     */
    public function setRecognition_date_input($value)
    {
        $this->recognition_date = ($d = date_create_from_format('d.m.Y', $value)) ? $d->format('Y-m-d') : $value;
    }

    /**
     * @return string
     */
    public function getRecognition_date_input()
    {
        return ($d = date_create_from_format('Y-m-d', $this->recognition_date)) ? $d->format('d.m.Y') : $this->recognition_date;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'cashbox_id',
                    'author_id',
                    'date',
                    'contractor_id',
                    'amount_input',
                ],
                'required',
            ],
            [
                [
                    'cashbox_id',
                    'amount',
                    'expenditure_item_id',
                    'income_item_id',
                    'reason_id',
                    'cash_funding_type_id',
                    'project_id',
                    'is_accounting',
                    'is_taxable',
                    'is_prepaid_expense',
                    'is_funding_flow',
                ],
                'integer',
            ],
            [
                [
                    'is_accounting',
                    'is_taxable',
                    'is_prepaid_expense',
                    'is_funding_flow',
                ],
                'boolean',
            ],
            [
                ['contractor_id', 'contractorInput'],
                'validateContractor',
                'skipOnEmpty' => false,
                'when' => function ($model) {
                    return !$model->is_internal_transfer;
                },
            ],
            [
                [
                    'amount_input'
                ], 'number',
                'skipOnError' => true,
                'numberPattern' => '/^[0-9]{1,18}([.,][0-9]{0,2})?$/',
                'min' => 0.01,
                'max' => Yii::$app->params['maxCashSum'],
                'tooBig' => 'Значение «{attribute}» не должно превышать ' . TextHelper::moneyFormat(Yii::$app->params['maxCashSum'], 2) . '.',
            ],
            [
                [
                    'number',
                    'description',
                ], 'required',
                'message' => 'Необходимо заполнить.',
                'when' => function ($model) {
                    return !$model->is_internal_transfer;
                },
            ],
            [
                ['number'], 'unique', 'filter' => function (CashFlowBase $query) {
                    $year = ($d = date_create($this->date)) ? $d->format('Y') : date('Y');

                    return $query->andWhere([
                        'and',
                        ['company_id' => $this->company_id],
                        ['flow_type' => $this->flow_type],
                        ['between', 'date', "$year-01-01", "$year-12-31"],
                    ]);
                },
                'when' => function ($model) {
                    return !$model->is_internal_transfer;
                },
            ],
            [
                ['expenditure_item_id'], 'required',
                'when' => function ($model) {
                    return $model->flow_type == CashFlowsBase::FLOW_TYPE_EXPENSE;
                },
                'message' => 'Необходимо заполнить.',
            ],
            [
                ['income_item_id'], 'required',
                'when' => function ($model) {
                    return !$model->hasErrors('flow_type') && $model->flow_type == CashFlowsBase::FLOW_TYPE_INCOME;
                },
                'message' => 'Необходимо заполнить.',
            ],
            [
                ['other'], 'required',
                'when' => function ($model) {
                    return $model->reason_id == CashOrdersReasonsTypes::VALUE_OTHER;
                },
                'message' => 'Необходимо заполнить.',
            ],
            [['reason_id'], 'validateReason'],
            [
                [
                    'date_input',
                    'recognition_date_input',
                ],
                'date',
                'format' => 'php:d.m.Y',
            ],
            [['number', 'contractor_id', 'description', 'other', 'application'], 'string', 'max' => 255],
            [['cash_funding_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => CashFundingType::className(), 'targetAttribute' => ['cash_funding_type_id' => 'id']],
            [
                ['cashbox_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Cashbox::className(),
                'targetAttribute' => ['cashbox_id' => 'id'],
                'filter' => [
                    'and',
                    ['company_id' => $this->company_id],
                    ['not', ['currency_id' => Currency::DEFAULT_ID]],
                ],
            ],
            [['expenditure_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => InvoiceExpenditureItem::className(), 'targetAttribute' => ['expenditure_item_id' => 'id']],
            [['income_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => InvoiceIncomeItem::className(), 'targetAttribute' => ['income_item_id' => 'id']],
            [['reason_id'], 'exist', 'skipOnError' => true, 'targetClass' => CashOrdersReasonsTypes::className(), 'targetAttribute' => ['reason_id' => 'id']],
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateReason($attribute, $params)
    {
        $reason = CashOrdersReasonsTypes::find()
            ->andWhere([
                'id' => $this->$attribute,
            ])
            ->andWhere(['OR',
                ['flow_type' => $this->flow_type],
                ['flow_type' => null],
            ])
            ->one();

        if ($reason === null) {
            $this->addError($attribute, '{attribute} не найдено.');
        }
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateContractor($attribute, $params)
    {
        if ($this->$attribute) {
            $contractor = Contractor::find()->andWhere([
                'id' => $this->contractor_id,
            ])->andWhere([
                'or',
                ['company_id' => null],
                ['company_id' => $this->company_id],
            ])->one();

            if ($contractor === null) {
                $type = CashContractorType::findOne(['name' => $this->contractor_id]);
                if ($type == null) {
                    $this->addError($attribute, 'Контрагент не найден..');
                } elseif ($type->name == CashContractorType::ORDER_TEXT) {
                    $query = $this->company->getCashboxes()
                        ->andWhere(['id' => $this->other_cashbox_id])
                        ->andWhere(['not', ['id' => $this->cashbox_id]]);
                    if (!$query->exists()) {
                        $this->addError($attribute, 'Контрагент не найден...');
                    }
                }
            } else {
                if (!$this->isNewRecord && $this->isAttributeChanged($attribute) && $contractor->type == Contractor::TYPE_FOUNDER) {
                    $this->addError($attribute, 'Редактирование котрагента-учредителя запрещено.');
                }
            }
        } else {
            $this->addError($attribute, 'Необходимо заполнить.');
        }
    }

    /**
     * {@inheritdoc}
     */
    public function afterValidate()
    {
        $cashbox = $this->cashbox ?? null;
        $currency = $cashbox->currency ?? null;
        $this->currency_id = $currency->id ?? null;
        $this->currency_name = $currency->name ?? null;
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'author_id' => 'Автор',
            'number' => 'Номер',
            //'date' => 'Дата КО', // Переименовываем согласно ТЗ
            'flow_type' => 'Тип ордера', // Переименовываем согласно ТЗ
            'is_accounting' => 'Учитывать в бухгалтерии',
            'reason_id' => 'Основание',
            'application' => 'Приложение',
            'other' => 'Значение "Прочее"',
            'income_item_id' => 'Статья прихода',
            'cashbox_id' => 'Касса',
            'other_cashbox_id' => 'Касса',
            'other_rs_id' => 'Счет',
            'other_emoney_id' => 'E-money',
            'is_prepaid_expense' => 'Авансовый платеж',
            'project_id' => 'ID проекта',
            'credit_id' => 'Кредитный договор',
            'credit_amount' => 'На сумму',
        ]);
    }

    /**
     * Gets query for [[Author]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Employee::className(), ['id' => 'author_id']);
    }

    /**
     * Gets query for [[CashFundingType]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCashFundingType()
    {
        return $this->hasOne(CashFundingType::className(), ['id' => 'cash_funding_type_id']);
    }

    /**
     * Gets query for [[CashOrderForeignCurrencyFlowsToInvoices]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCashOrderForeignCurrencyFlowsToInvoices()
    {
        return $this->hasMany(CashOrderForeignCurrencyFlowsToInvoice::className(), ['flow_id' => 'id']);
    }

    /**
     * Gets query for [[Cashbox]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCashbox()
    {
        return $this->hasOne(Cashbox::className(), ['id' => 'cashbox_id']);
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * Gets query for [[Currency]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * Gets query for [[ExpenditureItem]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getExpenditureItem()
    {
        return $this->hasOne(InvoiceExpenditureItem::className(), ['id' => 'expenditure_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFlowInvoices()
    {
        return $this->hasMany(CashOrderForeignCurrencyFlowsToInvoice::className(), ['flow_id' => 'id']);
    }

    /**
     * Gets query for [[IncomeItem]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIncomeItem()
    {
        return $this->hasOne(InvoiceIncomeItem::className(), ['id' => 'income_item_id']);
    }

    /**
     * Gets query for [[Invoices]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInvoices()
    {
        return $this->hasMany(ForeignCurrencyInvoice::className(), ['id' => 'invoice_id'])->viaTable('cash_order_foreign_currency_flows_to_invoice', ['flow_id' => 'id']);
    }

    /**
     * Gets query for [[Reason]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getReason()
    {
        return $this->hasOne(CashOrdersReasonsTypes::className(), ['id' => 'reason_id']);
    }

    /**
     * @return  string
     */
    public function getContractorText()
    {
        return $this->contractor ? $this->contractor->nameWithType : ($this->cashContractor->text ?? '');
    }

    /**
     * @return string
     */
    public function getBillInvoices()
    {
        $invoices = [];
        foreach ($this->invoices as $invoice) {
            $invoices[] = Html::a('№' . $invoice->fullNumber, [
                '/documents/invoice/view',
                'id' => $invoice->id,
                'type' => $invoice->type,
            ]);
        }

        return !empty($invoices) ? implode(', ', $invoices) : '';
    }

    /**
     * @return string
     */
    public function getBillPaying($showClarifyLink = true)
    {
        if ($this->is_internal_transfer) {
            return '';
        }
        $invoicesList = $this->getBillInvoices();
        if ($showClarifyLink && $this->getAvailableAmount() > 0) {
            return ($invoicesList ? ($invoicesList . '<br />') : '') .
                Html::a('<span class="cash-bank-need-clarify">Уточнить</span>', [
                    '/cash/order/update',
                    'id' => $this->id,
                ], [
                    'class' => 'red-link',
                    'title' => 'Уточнить',
                    'data' => [
                        'title' => ($this->flow_type == self::FLOW_TYPE_INCOME) ? 'Приход' : 'Расход',
                        'pjax' => '0',
                        'toggle' => 'modal',
                        'target' => '#update-movement',
                    ],
                ]);
        }

        return $invoicesList;
    }

    /**
     * @return CashOrderFlows
     */
    public function cloneSelf()
    {
        $model = clone $this;
        $model->isNewRecord = true;
        unset($model->id);
        $model->number = static::getNextNumber($this->company_id, $this->flow_type);
        $model->date = date('Y-m-d');
        $model->recognition_date = date('Y-m-d');
        $model->internal_transfer_flow_id = null;
        $model->has_invoice = 0;

        return $model;
    }

    /**
     * @param string $val
     */
    public function setContractorInput($value)
    {
        $this->contractor_id = $value;
    }

    /**
     * @return string
     */
    public function getContractorInput()
    {
        return $this->contractor_id;
    }

    public function isPlanDate()
    {
        //if (!$this->date)
        //    return false;
        //
        //return (DateHelper::format($this->date, 'Y-m-d', 'd.m.Y') > date('Y-m-d'));

        return false; // todo: uncomment if foreign plan flows enabled
    }
}
