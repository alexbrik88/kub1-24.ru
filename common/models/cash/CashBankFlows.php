<?php

namespace common\models\cash;

use common\components\TaxRobotHelper;
use common\components\TextHelper;
use common\components\cash\InternalTransferFlowInterface;
use common\components\cash\InternalTransferHelper;
use common\models\company\CheckingAccountant;
use common\models\company\CompanyIndustry;
use common\models\company\CompanyType;
use common\models\companyStructure\SalePoint;
use common\models\currency\Currency;
use common\models\document\Invoice;
use common\models\document\InvoiceExpenditureGroup;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\document\OperationType;
use common\models\document\PaymentOrder;
use common\models\document\status\PaymentOrderStatus;
use common\models\document\TaxpayersStatus;
use common\models\project\Project;
use frontend\components\StatisticPeriod;
use frontend\models\log\Log;
use frontend\modules\analytics\behaviors\credits\CreditFlowBehavior;
use frontend\modules\analytics\models\credits\Credit;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\helpers\Html;

/**
 * @author ErickSkrauch <erickskrauch@yandex.ru>
 *
 * @property integer $parent_id
 * @property integer $tin_child_amount
 * @property integer $has_tin_children
 *
 * @property string $date
 * @property string $recognition_date
 * @property string $rs
 * @property string $bank_name
 * @property string $payment_order_number
 * @property integer $income_item_id
 * @property integer $available_amount
 * @property integer $taxpayers_status_id
 * @property string $kbk
 * @property string $oktmo_code
 * @property integer $payment_details_id
 * @property string $tax_period_code
 * @property string $document_number_budget_payment
 * @property string $document_date_budget_payment
 * @property integer $payment_type_id
 * @property integer $is_prepaid_expense
 * @property integer $account_id
 * @property integer $project_id
 * @property integer $sale_point_id
 * @property integer $industry_id
 *
 * @property string $recognitionDateInput
 *
 * @property CashBankFlowToInvoice[] $flowInvoices
 * @property Invoice[] $invoices
 * @property InvoiceIncomeItem $incomeReason
 * @property string $billPaying
 * @property InvoiceExpenditureItem $expenditureItem
 * @property-read string[] $creditList
 * @property null|CashBankFlows[] $children
 * @property null|CashBankFlows $parent
 */
class CashBankFlows extends CashFlowsBase implements InternalTransferFlowInterface
{
    /** @var array */
    protected $_added_invoices = [];

    public $available_amount;
    public $bik;

    /**
     * @var int|null
     */
    public $credit_id;

    /**
     * @var float
     */
    public $credit_amount = 0;

    /** @inheritdoc */
    public static function tableName()
    {
        return 'cash_bank_flows';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getChildren()
    {
        return $this->hasMany(self::class, ['parent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParent()
    {
        return $this->hasOne(self::class, ['id' => 'parent_id']);
    }

    /**
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function deleteChildren()
    {
        $ret = true;
        foreach ($this->children as $child) {
            if (!$child->delete())
                $ret = false;
        }
        
        return $ret;
    }    

    public function getCashContractorTypeText() : string
    {
        return \frontend\modules\cash\models\CashContractorType::BANK_TEXT;
    }

    public function getInternalTransferAccountClass() : ?string
    {
        return InternalTransferHelper::accountClassByTable($this->internal_transfer_account_table);
    }

    public function getInternalTransferFlowClass() : ?string
    {
        return InternalTransferHelper::flowClassByTable($this->internal_transfer_flow_table);
    }

    public function getSelfAccount() : ActiveQuery
    {
        return $this->getCheckingAccountant();
    }

    public function getInternalTransferAccount() : ?ActiveQuery
    {
        if ($class = $this->getInternalTransferAccountClass()) {
            return $this->hasOne($class, ['id' => 'internal_transfer_account_id']);
        }

        return null;
    }

    public function getInternalTransferFlow() : ?ActiveQuery
    {
        if ($class = $this->getInternalTransferFlowClass()) {
            return $this->hasOne($class, ['id' => 'internal_transfer_flow_id']);
        }

        return null;
    }

    /**
     * @return array the list of expandable field names or field definitions. Please refer
     * to [[fields()]] on the format of the return value.
     * @see toArray()
     * @see fields()
     */
    public function extraFields()
    {
        return [
            'contractor',
            'invoices',
        ];
    }

    /** @inheritdoc */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'flow_type' => 'Приход/Расход',
            'rs' => 'Расчетный счет',
            'bank_name' => 'Название банка',
            'payment_order_number' => 'Номер ПП',
            'income_item_id' => 'Статья прихода',
            'billPaying' => 'Опл. счета',
            'taxpayers_status_id' => 'Статус плательщика',
            'kbk' => 'КБК',
            'oktmo_code' => 'Код ОКТМО',
            'payment_details_id' => 'Основание платежа',
            'tax_period_code' => 'Налоговый период',
            'document_number_budget_payment' => 'Номер бюджетного документа',
            'document_date_budget_payment' => 'Дата бюджетного документа',
            'dateBudgetPayment' => 'Дата бюджетного платежа',
            'payment_type_id' => 'Тип платежа',
            'uin_code' => 'Код УИН',
            'is_prepaid_expense' => 'Авансовый платеж',
            'credit_id' => 'Кредитный договор',
            'credit_amount' => 'На сумму',
        ]);
    }

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['creditFlow'] = [
            'class' => CreditFlowBehavior::class,
            'attribute' => 'cash_bank_flow_id',
            'wallet_id' => CashFlowsBase::WALLET_BANK,
        ];

        return $behaviors;
    }

    /** @inheritdoc */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['date', 'has_invoice'], 'required', 'message' => 'Необходимо заполнить.',],
            [['is_prepaid_expense'], 'safe'],
            [['expenditure_item_id', 'income_item_id', 'is_prepaid_expense'], 'integer'],
            [['expenditure_item_id'], 'required', 'when' => function (CashBankFlows $model) {
                return !$model->hasErrors('flow_type') && $model->flow_type == CashFlowsBase::FLOW_TYPE_EXPENSE;
            }, 'whenClient' => '
                function(attribute, value) {
                    return $("[name=\'' . Html::getInputName($this, 'flow_type') . '\']:checked").val() == ' . CashFlowsBase::FLOW_TYPE_EXPENSE . ';
                }
            ', 'message' => 'Необходимо заполнить.',],
            [['income_item_id'], 'required', 'when' => function (CashBankFlows $model) {
                return !$model->hasErrors('flow_type') && $model->flow_type == CashFlowsBase::FLOW_TYPE_INCOME;
            }, 'whenClient' => '
                function(attribute, value) {
                    return $("[name=\'' . Html::getInputName($this, 'flow_type') . '\']:checked").val() == ' . CashFlowsBase::FLOW_TYPE_INCOME . ';
                }
            ', 'message' => 'Необходимо заполнить.',],
            [['rs', 'payment_order_number'], 'string', 'max' => 255],
            [['rs'], 'required', 'message' => 'Необходимо заполнить.', 'on' => ['create', 'update']],
            [
                ['rs'], 'exist',
                'targetClass' => CheckingAccountant::className(),
                'targetAttribute' => 'rs',
                'filter' => [
                    'company_id' => $this->company_id,
                    'type' => CheckingAccountant::$typeActive,
                    'currency_id' => Currency::DEFAULT_ID,
                ]
            ],
            [['expenditure_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => InvoiceExpenditureItem::className(), 'targetAttribute' => ['expenditure_item_id' => 'id']],
            [['income_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => InvoiceIncomeItem::className(), 'targetAttribute' => ['income_item_id' => 'id']],

            [['recognitionDateInput'], 'string'],
            [['recognitionDateInput'], 'date', 'format' => 'php:d.m.Y'],
            [['kbk', 'tax_period_code', 'oktmo_code', 'document_number_budget_payment', 'uin_code'], 'trim'],
            [['oktmo_code', 'document_number_budget_payment', 'uin_code', 'dateBudgetPayment'], 'string'],
            [['kbk', 'tax_period_code'], 'required', 'when' => function ($model) {
                return $model->flow_type == CashFlowsBase::FLOW_TYPE_EXPENSE &&
                    in_array($model->expenditure_item_id, InvoiceExpenditureItem::taxIds());
            },],
            [['kbk'], 'string', 'length' => 20],
            ['tax_period_code', 'filter', 'filter' => function ($value) {
                return empty($value) ? '' : $value;
            }],
            [['tax_period_code'], 'string', 'length' => 10, 'max' => 10],
            [
                ['tax_period_code'], 'match',
                'pattern' => '#^(МС|КВ|ПЛ|ГД|\d{2})\.\d{2}\.\d{4}$#',
                'message' => 'Значение «{attribute}» имеет недопустимый формат.',
            ],
            [['dateBudgetPayment'], 'date', 'format' => 'php:d.m.Y'],
            [['taxpayers_status_id', 'payment_details_id', 'payment_type_id'], 'integer'],
            [
                ['oktmo_code'], 'match', 'pattern' => '#^(\d{8}|\d{11}|0)$#',
                'message' => 'Значение «{attribute}» должно состоять из 8 или 11 цифр или должно быть равно 0.',
                'when' => function ($model) {
                    return $this->flow_type == CashFlowsBase::FLOW_TYPE_EXPENSE;
                },
            ],
            'contractor_required' => [
                'contractor_id', 'required',
                'message' => 'Необходимо заполнить.',
                'when' => function ($model) {
                    return !($model->flow_type == CashFlowsBase::FLOW_TYPE_INCOME && $model->is_taxrobot_manual_entry);
                },
            ],
            [['credit_id'], 'filter', 'filter' => function ($value) { return (int)$value ?: null; }],
            [['credit_id'], 'exist', 'skipOnError' => true, 'skipOnEmpty' => true, 'targetClass' => Credit::class, 'targetAttribute' => ['credit_id' => 'credit_id', 'company_id' => 'company_id']],
            [['project_id', 'sale_point_id', 'industry_id'], 'filter', 'filter' => function ($value) {
                return intval($value) ?: null;
            }],
            [['project_id'], 'exist', 'targetClass' => Project::class, 'targetAttribute' => 'id', 'filter' => ['company_id' => $this->company_id]],
            [['sale_point_id'], 'exist', 'targetClass' => SalePoint::class, 'targetAttribute' => 'id', 'filter' => ['company_id' => $this->company_id]],
            [['industry_id'], 'exist', 'targetClass' => CompanyIndustry::class, 'targetAttribute' => 'id'],
            [['account_id'], 'integer'],
            [['parent_id'], 'exist', 'targetClass' => self::class, 'targetAttribute' => 'id', 'filter' => ['company_id' => $this->company_id]],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function getIsTaxPayment()
    {
        return $this->flow_type == CashFlowsBase::FLOW_TYPE_EXPENSE && InvoiceExpenditureItem::find()->where([
            'id' => $this->expenditure_item_id,
            'group_id' => InvoiceExpenditureGroup::TAXES,
        ])->exists();
    }

    /**
     * @param string $value
     */
    public function setDateBudgetPayment($value)
    {
        $this->document_date_budget_payment = ($date = date_create_from_format('d.m.Y', $value)) ? $date->format('Y-m-d') : null;
    }

    /**
     * @return string
     */
    public function getDateBudgetPayment()
    {
        if ($this->document_date_budget_payment) {
            $date = date_create($this->document_date_budget_payment)->format('d.m.Y');
        } else {
            $date = null;
        }

        return $date;
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();

        $invoices_amount = $this->getInvoices()->select("SUM(invoice.total_amount_with_nds)")->scalar();
        $this->available_amount = ($this->amount > $invoices_amount) ? ($this->amount - $invoices_amount) : 0;
        if ($this->is_prepaid_expense)
            $this->recognition_date = null;
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();

        if ($model = $this->getConformingPaymentOrder()) {
            $model->updateAttributes([
                'payment_order_status_id' => PaymentOrderStatus::STATUS_CREATED,
                'payment_order_status_updated_at' => time(),
            ]);
        }
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if (empty($this->kbk)) {
            $this->kbk = '';
        }

        return parent::beforeValidate();
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            return $this->deleteChildren();
        } else {
            return false;
        }
    }
    

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isAttributeChanged('rs') || ($insert && $this->account_id === null)) {
                $rs = $this->rs ? $this->getCheckingAccountant()->one() : null;
                $this->bank_name = $rs !== null ? $rs->bank_name : null;
                $this->account_id = $rs !== null ? $rs->id : null;
            }

            if ($insert && !isset($this->source)) {
                $this->source = CashBankStatementUpload::SOURCE_MANUAL;
            }
            if ($insert && $this->is_taxable === null) {
                if (in_array($this->income_item_id, TaxRobotHelper::$excludeItems) || $this->contractor_id === 'balance') {
                    $this->is_taxable = 0;
                } else {
                    $this->is_taxable = 1;
                }
            }

            // Подоперации
            if ($this->parent_id) {
                $this->tin_child_amount = $this->amount;
                $this->amount = 0;
            } else {
                $this->tin_child_amount = null;
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * Get formatted log message from Log model
     * @param Log $log
     * @return string
     */
    public function getLogMessage(Log $log)
    {
        return $this->getLogMessageBase($log, 'по банку');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFlowInvoices()
    {
        return $this->hasMany(CashBankFlowToInvoice::className(), ['flow_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIncomeReason()
    {
        return $this->hasOne(InvoiceIncomeItem::className(), ['id' => 'income_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCheckingAccountants()
    {
        return $this->hasMany(CheckingAccountant::className(), ['company_id' => 'company_id', 'rs' => 'rs']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCheckingAccountant()
    {
        return $this->hasOne(CheckingAccountant::className(), ['company_id' => 'company_id', 'rs' => 'rs'])
            ->orderBy(['type' => SORT_ASC]);
    }

    /**
     * @return string
     */
    public function getBillPaying($showClarifyLink = true)
    {
        if ($this->is_internal_transfer) {
            return '';
        }
        $invoices = [];
        foreach ($this->invoices as $invoice) {
            $invoices[] = Html::a('№' . $invoice->fullNumber, [
                '/documents/invoice/view',
                'id' => $invoice->id,
                'type' => $invoice->type,
            ]);
        }

        $invoicesList = !empty($invoices) ? implode(', ', $invoices) : '';

        $options = [
            'title' => 'Прикрепите счета, которые были оплачены',
            'class' => 'red-link',
            'data' => [
                'toggle' => 'modal',
                'target' => '#update-movement',
            ],
        ];
        if ($showClarifyLink && $this->getAvailableAmount() > 0) {
            return ($invoicesList ? ($invoicesList . '<br />') : '') .
                Html::a('<span class="cash-bank-need-clarify">Уточнить</span>', '/cash/bank/update?id=' . $this->id, $options);
        } else {
            return $invoicesList;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpenditureItem()
    {
        return $this->hasOne(InvoiceExpenditureItem::className(), ['id' => 'expenditure_item_id']);
    }

    /**
     * @param string $value
     */
    public function setRecognitionDateInput($value)
    {
        $this->recognition_date = is_string($value) ? implode('-', array_reverse(explode('.', $value))) : null;
    }

    /**
     * @return string
     */
    public function getRecognitionDateInput()
    {
        return $this->recognition_date ? implode('.', array_reverse(explode('-', $this->recognition_date))) : null;
    }

    /**
     * @return PaymentOrder|null
     */
    public function getConformingPaymentOrder()
    {
        return $this->contractor ? PaymentOrder::find()->andWhere([
            'company_id' => $this->company_id,
        ])->andWhere([
            'or',
            ['cash_bank_flows_id' => $this->id],
            [
                'and',
                ['cash_bank_flows_id' => null],
                ['sum' => $this->amount],
                ['contractor_inn' => $this->contractor->ITN],
                ['contractor_kpp' => $this->contractor->PPC],
                ['contractor_bik' => $this->contractor->BIC],
                ['contractor_current_account' => $this->contractor->current_account],
                ['purpose_of_payment' => $this->description],
                ['kbk' => $this->kbk],
                ['tax_period_code' => $this->tax_period_code],
            ],
        ])->one() : null;
    }

    /**
     * @return string
     */
    public function findPaymentOrder()
    {
        if ($this->flow_type == CashFlowsBase::FLOW_TYPE_EXPENSE && $this->contractor) {
            $model = $this->getConformingPaymentOrder();

            if ($model === null) {
                $account = $this->checkingAccountant ? : $this->company->mainCheckingAccountant;
                $model = new PaymentOrder([
                    'document_author_id' => $this->company->chiefEmployeeCompany->employee_id,
                    'document_number' => $this->payment_order_number,
                    'document_date' => date_create($this->date)->format('Y-m-d'),
                    'sum' => $this->amount,
                    'sum_in_words' => TextHelper::mb_ucfirst(TextHelper::amountToWords($this->amount / 100)),
                    'company_id' => $this->company_id,
                    'company_name' => $this->company->getTitle(true, true),
                    'company_inn' => $this->company->inn,
                    'company_kpp' => strlen($this->company->kpp) == 9 ? $this->company->kpp : 0,
                    'company_bik' => $account->bik,
                    'company_bank_name' => $account->bank_name,
                    'company_ks' => $account->ks,
                    'company_rs' => $account->rs,
                    'contractor_id' => $this->contractor_id,
                    'contractor_name' => $this->contractor->name,
                    'contractor_inn' => $this->contractor->ITN,
                    'contractor_kpp' => $this->contractor->PPC,
                    'contractor_bik' => $this->contractor->BIC,
                    'contractor_bank_name' => $this->contractor->bank_name,
                    'contractor_corresponding_account' => $this->contractor->corresp_account,
                    'contractor_current_account' => $this->contractor->current_account,
                    'ranking_of_payment' => 5,
                    'operation_type_id' => OperationType::TYPE_PAYMENT_ORDER,
                    'purpose_of_payment' => $this->description,
                    'taxpayers_status_id' => $this->taxpayers_status_id ? : (
                        $this->company->getIsLikeIP() ?
                        TaxpayersStatus::DEFAULT_IP :
                        TaxpayersStatus::DEFAULT_OOO
                    ),
                    'kbk' => $this->kbk,
                    'oktmo_code' => $this->oktmo_code,
                    'payment_details_id' => $this->payment_details_id,
                    'tax_period_code' => $this->tax_period_code,
                    'document_number_budget_payment' => $this->document_number_budget_payment,
                    'document_date_budget_payment' => $this->document_date_budget_payment,
                    'payment_type_id' => $this->payment_type_id,
                    'uin_code' => $this->uin_code,
                    'payment_order_status_id' => PaymentOrderStatus::STATUS_PAID,
                    'payment_order_status_updated_at' => strtotime($this->date),
                    'presence_status_budget_payment' => (!empty($this->kbk) || !empty($this->tax_period_code)) ? 1 : 0,
                    'cash_bank_flows_id' => $this->id,
                ]);
                if (!$model->save(false)) {
                    $model = null;
                }
            } else {
                $updateAttributes = [];
                if (!$model->cash_bank_flows_id != $this->id) {
                    $updateAttributes['cash_bank_flows_id'] = $this->id;
                }
                if (!$model->presence_status_budget_payment && ($this->kbk || $this->tax_period_code)) {
                    $updateAttributes['presence_status_budget_payment'] = 1;
                }
                if ($model->payment_order_status_id != PaymentOrderStatus::STATUS_PAID) {
                    $updateAttributes['payment_order_status_id'] = PaymentOrderStatus::STATUS_PAID;
                    $updateAttributes['payment_order_status_updated_at'] = strtotime($this->date);
                }
                if ($updateAttributes) {
                    $model->updateAttributes($updateAttributes);
                }
            }

            return $model;
        }

        return null;
    }

    /**
     * @return CashOrderFlows
     */
    public function cloneSelf()
    {
        $model = clone $this;
        $model->isNewRecord = true;
        unset($model->id);
        $model->payment_order_number = null;
        $model->date = date('Y-m-d');
        $model->recognition_date = date('Y-m-d');
        $model->invoice_id = null;
        $model->internal_transfer_flow_id = null;
        $model->has_invoice = 0;

        return $model;
    }

    /**
     * @param $flowID
     * @param bool $inverted
     * @return query\CashFlowBase
     */
    public static function getTinChildrenQuery($flowID, $inverted = false)
    {
        return self::find()
            ->where(['parent_id' => $flowID])
            ->orderBy(['tin_child_amount' => ($inverted) ? SORT_ASC : SORT_DESC]);
    }

    /**
     *
     */
    public function checkTinChildren()
    {
        if ($this->id) {
            if (self::find()->where(['parent_id' => $this->id])->exists()) {
                if ($this->has_tin_children == 0)
                    $this->updateAttributes(['has_tin_children' => 1]);
            } else {
                if ($this->has_tin_children == 1)
                    $this->updateAttributes(['has_tin_children' => 0]);
            }
        }
    }

    public function getNumber()
    {
        return $this->payment_order_number;
    }
}
