<?php

namespace common\models\cash;

use Yii;
use common\components\TextHelper;
use common\components\cash\InternalTransferFlowInterface;
use common\components\cash\InternalTransferHelper;
use common\components\pdf\Printable;
use common\models\Company;
use common\models\Contractor;
use common\models\cash\query\CashFlowBase;
use common\models\currency\Currency;
use common\models\document\ForeignCurrencyInvoice;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\employee\Employee;
use frontend\models\log\Log;
use frontend\modules\analytics\behaviors\credits\CreditFlowBehavior;
use frontend\modules\cash\models\CashContractorType;
use yii\db\ActiveQuery;
use yii\helpers\Html;

/**
 * This is the model class for table "cash_emoney_foreign_currency_flows".
 *
 * @property int $id
 * @property int $company_id
 * @property int $emoney_id
 * @property string $currency_id
 * @property string $currency_name
 * @property string $date
 * @property string $date_time
 * @property string|null $recognition_date
 * @property int $is_internal_transfer
 * @property string|null $internal_transfer_account_table
 * @property int|null $internal_transfer_account_id
 * @property string|null $internal_transfer_flow_table
 * @property int|null $internal_transfer_flow_id
 * @property int $flow_type 0 - расход, 1 - приход
 * @property string|null $number
 * @property string $contractor_id
 * @property int $amount
 * @property string|null $description
 * @property int|null $expenditure_item_id
 * @property int|null $income_item_id
 * @property int|null $cash_funding_type_id
 * @property int|null $project_id
 * @property int|null $moneta_id
 * @property int $created_at
 * @property int $is_accounting Учёт в бухгалтерии: 0 - нет, 1 - да
 * @property int $has_invoice Связь со счётом: 0 - нет, 1 - да
 * @property int $is_prepaid_expense
 * @property int $is_funding_flow
 *
 * @property CashEmoneyForeignCurrencyFlowsToInvoice[] $cashEmoneyForeignCurrencyFlowsToInvoices
 * @property CashFundingType $cashFundingType
 * @property Company $company
 * @property Currency $currency
 * @property Emoney $emoney
 * @property InvoiceExpenditureItem $expenditureItem
 * @property InvoiceIncomeItem $incomeItem
 * @property ForeignCurrencyInvoice[] $invoices
 */
class CashEmoneyForeignCurrencyFlows extends CashFlowsBase implements InternalTransferFlowInterface
{
    public  $time;

    public function getIsForeign()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cash_emoney_foreign_currency_flows';
    }

    public function getCashContractorTypeText() : string
    {
        return \frontend\modules\cash\models\CashContractorType::EMONEY_CURRENCY_TEXT;
    }

    public function getInternalTransferAccountClass() : ?string
    {
        return InternalTransferHelper::accountClassByTable($this->internal_transfer_account_table);
    }

    public function getInternalTransferFlowClass() : ?string
    {
        return InternalTransferHelper::flowClassByTable($this->internal_transfer_flow_table);
    }

    public function getSelfAccount() : ActiveQuery
    {
        return $this->getEmoney();
    }

    public function getInternalTransferAccount() : ?ActiveQuery
    {
        if ($class = $this->getInternalTransferAccountClass()) {
            return $this->hasOne($class, ['id' => 'internal_transfer_account_id']);
        }

        return null;
    }

    public function getInternalTransferFlow() : ?ActiveQuery
    {
        if ($class = $this->getInternalTransferFlowClass()) {
            return $this->hasOne($class, ['id' => 'internal_transfer_flow_id']);
        }

        return null;
    }

    /**
     * Get formatted log message from Log model
     * @param Log $log
     * @return string
     */
    public function getLogMessage(Log $log)
    {
        return $this->getLogMessageBase($log, 'по кассе');
    }

    /**
     * @param string $value
     */
    public function setAmount_input($value)
    {
        $value = strtr($value, [',' => '.']);
        $this->amount = is_numeric($value) ? $value * 100 : $value;
    }

    /**
     * @return string
     */
    public function getAmount_input()
    {
        return is_numeric($this->amount) ? $this->amount / 100 : $this->amount;
    }

    /**
     * @param string $value
     */
    public function setDate_input($value)
    {
        $this->date = ($d = date_create_from_format('d.m.Y', $value)) ? $d->format('Y-m-d') : $value;
    }

    /**
     * @return string
     */
    public function getDate_input()
    {
        return ($d = date_create_from_format('Y-m-d', $this->date)) ? $d->format('d.m.Y') : $this->date;
    }

    /**
     * @param string $value
     */
    public function setRecognition_date_input($value)
    {
        $this->recognition_date = ($d = date_create_from_format('d.m.Y', $value)) ? $d->format('Y-m-d') : $value;
    }

    /**
     * @return string
     */
    public function getRecognition_date_input()
    {
        return ($d = date_create_from_format('Y-m-d', $this->recognition_date)) ? $d->format('d.m.Y') : $this->recognition_date;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'emoney_id',
                    'date',
                    'contractor_id',
                    'amount_input',
                ],
                'required',
            ],
            [
                [
                    'emoney_id',
                    'internal_transfer_account_id',
                    'internal_transfer_flow_id',
                    'expenditure_item_id',
                    'income_item_id',
                    'cash_funding_type_id',
                    'project_id',
                    'moneta_id',
                    'created_at',
                ],
                'integer',
            ],
            [
                [
                    'is_internal_transfer',
                    'is_accounting',
                    'has_invoice',
                    'is_prepaid_expense',
                    'is_funding_flow',
                ],
                'boolean',
            ],
            [
                [
                    'amount_input'
                ], 'number',
                'skipOnError' => true,
                'numberPattern' => '/^[0-9]{1,18}([.,][0-9]{0,2})?$/',
                'min' => 0.01,
                'max' => Yii::$app->params['maxCashSum'],
                'tooBig' => 'Значение «{attribute}» не должно превышать ' . TextHelper::moneyFormat(Yii::$app->params['maxCashSum'], 2) . '.',
            ],
            [['date_input', 'recognition_date_input'], 'date', 'format' => 'php:d.m.Y'],
            [['time'], 'date', 'format' => 'php:H:i:s'],
            [
                [
                    'number',
                    'contractor_id',
                    'description',
                ],
                'string',
                'max' => 255,
            ],
            [
                ['cash_funding_type_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => CashFundingType::className(),
                'targetAttribute' => ['cash_funding_type_id' => 'id'],
            ],
            [
                ['emoney_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => Emoney::className(),
                'targetAttribute' => ['emoney_id' => 'id'],
                'filter' => [
                    'and',
                    ['company_id' => $this->company_id],
                    ['not', ['currency_id' => Currency::DEFAULT_ID]],
                ],
            ],
            [
                ['expenditure_item_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => InvoiceExpenditureItem::className(),
                'targetAttribute' => ['expenditure_item_id' => 'id'],
            ],
            [
                ['income_item_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => InvoiceIncomeItem::className(),
                'targetAttribute' => ['income_item_id' => 'id'],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function afterFind()
    {
        $this->time = date_create($this->date_time)->format('H:i:s');
    }

    /**
     * {@inheritdoc}
     */
    public function afterValidate()
    {
        $emoney = $this->emoney ?? null;
        $currency = $emoney->currency ?? null;
        $this->currency_id = $currency->id ?? null;
        $this->currency_name = $currency->name ?? null;
        if (!$this->hasErrors('date')) {
            $this->date_time = date_create($this->date)->format('Y-m-d ').($this->hasErrors('time') ? '00:00:00' : $this->time);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'emoney_id' => 'E-money',
            'date' => 'Дата',
            'date_time' => 'Время',
            'is_accounting' => 'Учитывать в бухгалтерии',
            'income_item_id' => 'Статья прихода',
            'number' => 'Номер',
            'recognition_date' => 'Дата признания',
            'is_prepaid_expense' => 'Авансовый платеж',
            'other_cashbox_id' => 'Касса',
            'other_rs_id' => 'Счет',
            'other_emoney_id' => 'E-money',
            'credit_id' => 'Кредитный договор',
            'credit_amount' => 'На сумму',
        ]);
    }

    /**
     * Gets query for [[CashEmoneyForeignCurrencyFlowsToInvoices]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCashEmoneyForeignCurrencyFlowsToInvoices()
    {
        return $this->hasMany(CashEmoneyForeignCurrencyFlowsToInvoice::className(), ['flow_id' => 'id']);
    }

    /**
     * Gets query for [[CashFundingType]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCashFundingType()
    {
        return $this->hasOne(CashFundingType::className(), ['id' => 'cash_funding_type_id']);
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * Gets query for [[Currency]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * Gets query for [[Emoney]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmoney()
    {
        return $this->hasOne(Emoney::className(), ['id' => 'emoney_id']);
    }

    /**
     * Gets query for [[ExpenditureItem]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getExpenditureItem()
    {
        return $this->hasOne(InvoiceExpenditureItem::className(), ['id' => 'expenditure_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFlowInvoices()
    {
        return $this->hasMany(CashEmoneyForeignCurrencyFlowsToInvoice::className(), ['flow_id' => 'id']);
    }

    /**
     * Gets query for [[IncomeItem]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIncomeItem()
    {
        return $this->hasOne(InvoiceIncomeItem::className(), ['id' => 'income_item_id']);
    }

    /**
     * Gets query for [[Invoices]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInvoices()
    {
        return $this->hasMany(ForeignCurrencyInvoice::className(), [
            'id' => 'invoice_id',
        ])->viaTable('cash_emoney_foreign_currency_flows_to_invoice', ['flow_id' => 'id']);
    }

    /**
     * @return CashOrderFlows
     */
    public function cloneSelf()
    {
        $model = clone $this;
        $model->isNewRecord = true;
        unset($model->id);
        $model->number = static::getNextNumber($this->flow_type , null, $this->company_id);
        $model->date = date('Y-m-d');
        $model->recognition_date = date('Y-m-d');
        $model->invoice_id = null;
        $model->internal_transfer_flow_id = null;
        $model->has_invoice = 0;

        return $model;
    }
}
