<?php

namespace common\models\cash;

use common\components\TextHelper;
use common\components\cash\InternalTransferAccountInterface;
use common\components\cash\InternalTransferFlowInterface;
use common\models\Company;
use common\models\companyStructure\OfdType;
use common\models\currency\Currency;
use common\models\document\InvoiceIncomeItem;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use frontend\modules\cash\models\CashContractorType;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "cashbox".
 *
 * @property integer $id
 * @property integer $company_id
 * @property integer $currency_id
 * @property integer $responsible_employee_id
 * @property integer $is_accounting
 * @property integer $is_main
 * @property integer $is_closed
 * @property string $name
 * @property string $reg_number
 * @property integer ofd_type_id
 *
 * @property integer|string $accessible
 *
 * @property CashOrderFlows[] $cashOrderFlows
 * @property Company $company
 * @property Employee $responsibleEmployee
 */
class Cashbox extends ActiveRecord implements WalletInterface, InternalTransferAccountInterface
{
    public $saved = false;
    public $createStartBalance = false;
    public $startBalanceAmount;
    public $startBalanceDate;

    /**
     * roles that all Cashboxes can see
     * @var array
     */
    public static $rolesViewAll = [
        EmployeeRole::ROLE_CHIEF,
        EmployeeRole::ROLE_FOUNDER,
        EmployeeRole::ROLE_FINANCE_DIRECTOR,
        EmployeeRole::ROLE_FINANCE_ADVISER,
    ];

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'cashbox';
    }

    public function getCashContractorTypeText() : string
    {
        return CashContractorType::ORDER_TEXT;
    }

    public function getInternalTransferId() : string
    {
        return self::tableName() . $this->id;
    }

    public function getInternalTransferName() : ?string
    {
        return $this->name;
    }

    public function getCurrencyId() : ?string
    {
        return $this->currency_id ?? null;
    }

    public function getCurrencyName() : ?string
    {
        return Currency::name($this->currency_id);
    }

    public function getCashFlowClass() : string
    {
        return $this->currency_id == Currency::DEFAULT_ID ? CashOrderFlows::class : CashOrderForeignCurrencyFlows::class;
    }

    public function getCashFlowTable() : string
    {
        $class = $this->getCashFlowClass();

        return $class::tableName();
    }

    public function getIsForeign() : string
    {
        return $this->currency_id != Currency::DEFAULT_ID;
    }

    public function getWalletId() : int
    {
        return CashFlowsBase::WALLET_CASHBOX;
    }

    public function getBalance() : int
    {
        if ($this->id) {
            $class = $this->getIsForeign() ? CashOrderForeignCurrencyFlows::class : CashOrderFlows::class;
            return (new \yii\db\Query())
                ->from($class::tableName())
                ->where(['company_id' => $this->company_id])
                ->andWhere(['cashbox_id' => $this->id])
                ->sum(new \yii\db\Expression('CASE WHEN flow_type = 1 THEN amount ELSE -amount END')) ?: 0;
        }

        return 0;
    }

    public function getCashFlowNewModel() : InternalTransferFlowInterface
    {
        $class = $this->getCashFlowClass();
        $model = new $class;
        $model->company_id = $this->company_id;
        $model->cashbox_id = $this->id;
        $model->is_accounting = $this->is_accounting;
        $model->has_invoice = 0;

        if ($this->currency_id != Currency::DEFAULT_ID) {
            $model->currency_id = $this->currency_id;
            $model->currency_name = Currency::name($this->currency_id);
        }

        $model->populateRelation('selfAccount', $this);

        return $model;
    }

    public function getNextNumber($flow_type)
    {
        $class = $this->getCashFlowClass();

        return $class::getNextNumber($this->company_id, $flow_type);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['currency_id', 'default', 'value' => '643'], // rub
            [['name'], 'trim'],
            [['currency_id', 'name'], 'required'],
            [['currency_id'], 'exist', 'targetClass' => Currency::class, 'targetAttribute' => ['currency_id' => 'id']],
            [['accessible',], 'safe'],
            [['startBalanceDate'], 'date', 'format' => 'd.M.yyyy'],
            [
                [
                    'startBalanceAmount',
                    'startBalanceDate',
                ],
                'required',
                'when' => function (Cashbox $model) {
                    return $model->createStartBalance;
                },
                'message' => 'Необходимо заполнить',
            ],
            [['accessible_to_all'], 'boolean'],
            [['is_accounting', 'is_closed'], 'filter', 'filter' => function($value) {
                return (int) $value;
            }],
            [['responsible_employee_id', 'ofd_type_id'], 'filter', 'filter' => function($value) {
                return (int) $value ? : null;
            }],
            [['is_closed'], function ($attribute, $params) {
                if ($this->$attribute && $this->is_main) {
                    $this->addError($attribute, 'Основная касса не может быть закрыта.');
                }
            }],
            [['startBalanceAmount'],
                'number',
                'numberPattern' => Yii::$app->params['numberPattern'],
                'max' => Yii::$app->params['maxCashSum'] * 100, // with kopeck
                'tooBig' => 'Значение «{attribute}» не должно превышать ' . TextHelper::moneyFormat(Yii::$app->params['maxCashSum'], 2) . '.',
                'whenClient' => 'function(){}', // because of `123,00` - not valid float number (with comma) and there doesn't exist any methods to parse it on client.
            ],
            [['responsible_employee_id', 'is_accounting', 'is_closed', 'ofd_type_id'], 'integer'],
            [['createStartBalance'], 'safe'],
            [['name'], 'string', 'max' => 100],
            [
                ['responsible_employee_id'], 'exist', 'skipOnError' => true,
                'targetClass' => Employee::className(),
                'targetAttribute' => ['responsible_employee_id' => 'id'],
                'filter' => ['company_id' => $this->company_id],
            ],
            [
                ['ofd_type_id'], 'exist',
                'targetClass' => OfdType::class,
                'targetAttribute' => 'id',
                'filter' => [
                    'or',
                    ['company_id' => $this->company_id],
                    ['company_id' => null],
                ]
            ],
            [['is_main'], 'boolean']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'currency_id' => 'Валюта',
            'responsible_employee_id' => 'Ответственный',
            'responsibleEmployee.fio' => 'Ответственный',
            'is_accounting' => 'Для учета в бухгалтерии',
            'is_main' => 'Основная',
            'is_closed' => 'Касса закрыта',
            'name' => 'Касса (название)',
            'accessible' => 'Доступна',
            'createStartBalance' => 'Указать начальный остаток',
            'startBalanceAmount' => 'Сумма начального остатка',
            'startBalanceDate' => 'на дату',
            'ofd_type_id' => 'ОФД'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashOrderFlows()
    {
        return $this->hasMany(CashOrderFlows::className(), ['cashbox_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResponsibleEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'responsible_employee_id']);
    }

    /**
     * @return boolean
     */
    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }

        return $this->canDelete();
    }

    /**
     * @param boolean $insert
     * @return boolean
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if ($this->is_main) {
            $this->responsible_employee_id = null;
            $this->is_closed = false;
            $this->accessible_to_all = 0;
        } elseif ($this->responsible_employee_id) {
            $this->accessible_to_all = 0;
        }

        if (!$this->is_accounting) {
            $this->ofd_type_id = null;
        }

        if (!$this->currency_id) {
            $this->currency_id = Currency::DEFAULT_ID;
        }

        $condition = [
            'and',
            ['company_id' => $this->company_id],
            ['currency_id' => $this->currency_id],
            ['is_main' => 1],
            ['not', ['id' => $this->id]],
        ];
        if (!$this->is_closed && !$this->is_main && !self::find()->where($condition)->exists()) {
            $this->is_main = 1;
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $this->saved = true;

        if ($this->createStartBalance) {
            if ($this->currency_id == Currency::DEFAULT_ID) {
                $model = new CashOrderFlows();
                $model->date = $this->startBalanceDate;
                $model->amount = $this->startBalanceAmount;
                $model->number = (string) CashOrderFlows::getNextNumber(
                    $this->company_id,
                    CashFlowsBase::FLOW_TYPE_INCOME,
                    $this->startBalanceDate
                );
            } else {
                $model = new CashOrderForeignCurrencyFlows();
                $model->currency_id = $this->currency_id;
                $model->date_input = $this->startBalanceDate;
                $model->amount_input = $this->startBalanceAmount;
                $model->number = (string) CashOrderForeignCurrencyFlows::getNextNumber(
                    $this->company_id,
                    CashFlowsBase::FLOW_TYPE_INCOME,
                    $this->startBalanceDate
                );
            }
            $model->flow_type = CashFlowsBase::FLOW_TYPE_INCOME;
            $model->company_id = $this->company_id;
            $model->cashbox_id = $this->id;
            $model->is_accounting = $this->is_accounting;
            $model->author_id = Yii::$app->id == 'app-frontend' ? Yii::$app->user->identity->id : null;
            $model->contractor_id = CashContractorType::BALANCE_TEXT;
            $model->income_item_id = InvoiceIncomeItem::ITEM_STARTING_BALANCE;
            $model->description = 'Баланс начальный';
            if (!$model->save()) {
                \common\components\helpers\ModelHelper::logErrors($model, __METHOD__);
            }
        }

        if ($this->is_main) {
            $condition = [
                'and',
                ['company_id' => $this->company_id],
                ['currency_id' => $this->currency_id],
                ['is_main' => 1],
                ['not', ['id' => $this->id]],
            ];
            if (self::find()->where($condition)->exists()) {
                self::updateAll(['is_main' => 0], $condition);
            }
        }
    }

    /**
     * @return boolean
     */
    public function canDelete()
    {
        return !($this->is_main || $this->getCashOrderFlows()->exists());
    }

    /**
     * @inheritdoc
     */
    public function setAccessible($value)
    {
        if ($value === 'all') {
            $this->responsible_employee_id = null;
            $this->accessible_to_all = 1;
        } else {
            $this->responsible_employee_id = $value;
            $this->accessible_to_all = 0;
        }
    }

    /**
     * @inheritdoc
     */
    public function getAccessible()
    {
        return $this->accessible_to_all ? 'all' : $this->responsible_employee_id;
    }

    /**
     * @inheritDoc
     */
    public function getWalletName(): string
    {
        return $this->name;
    }

    public function getCurrencySymbol() : string
    {
        return ArrayHelper::getValue(Currency::$currencySymbols, $this->currency->name);
    }
}
