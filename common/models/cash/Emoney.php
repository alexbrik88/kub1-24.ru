<?php

namespace common\models\cash;

use Yii;
use common\components\TextHelper;
use common\components\cash\InternalTransferAccountInterface;
use common\components\cash\InternalTransferFlowInterface;
use common\models\Company;
use common\models\currency\Currency;
use common\models\document\InvoiceIncomeItem;
use common\models\employee\EmployeeRole;
use frontend\modules\cash\models\CashContractorType;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "emoney".
 *
 * @property integer $id
 * @property integer $company_id
 * @property integer $currency_id
 * @property string $name
 * @property integer $is_main
 * @property integer $is_accounting
 * @property integer $is_closed
 *
 * @property Company $company
 */
class Emoney extends ActiveRecord implements WalletInterface, InternalTransferAccountInterface
{
    public $createStartBalance = false;
    public $startBalanceAmount;
    public $startBalanceDate;

    /**
     * roles that all Emoneys can see
     * @var array
     */
    public static $rolesViewAll = [
        EmployeeRole::ROLE_CHIEF,
        EmployeeRole::ROLE_FOUNDER,
        EmployeeRole::ROLE_FINANCE_DIRECTOR,
        EmployeeRole::ROLE_FINANCE_ADVISER,
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'emoney';
    }

    public function getCashContractorTypeText() : string
    {
        return CashContractorType::EMONEY_TEXT;
    }

    public function getInternalTransferId() : string
    {
        return self::tableName() . $this->id;
    }

    public function getInternalTransferName() : ?string
    {
        return $this->name;
    }

    public function getCurrencyId() : ?string
    {
        return $this->currency_id ?? null;
    }

    public function getCurrencyName() : ?string
    {
        return Currency::name($this->currency_id);
    }

    public function getCashFlowClass() : string
    {
        return $this->currency_id == Currency::DEFAULT_ID ? CashEmoneyFlows::class : CashEmoneyForeignCurrencyFlows::class;
    }

    public function getCashFlowTable() : string
    {
        $class = $this->getCashFlowClass();

        return $class::tableName();
    }

    public function getIsForeign() : string
    {
        return $this->currency_id != Currency::DEFAULT_ID;
    }

    public function getWalletId() : int
    {
        return CashFlowsBase::WALLET_EMONEY;
    }

    public function getBalance() : int
    {
        if ($this->id) {
            $class = $this->getIsForeign() ? CashEmoneyForeignCurrencyFlows::class : CashEmoneyFlows::class;
            return (new \yii\db\Query())
                ->from($class::tableName())
                ->where(['company_id' => $this->company_id])
                ->andWhere(['emoney_id' => $this->id])
                ->sum(new \yii\db\Expression('CASE WHEN flow_type = 1 THEN amount ELSE -amount END')) ?: 0;
        }

        return 0;
    }

    public function getCashFlowNewModel() : InternalTransferFlowInterface
    {
        $class = $this->getCashFlowClass();
        $model = new $class;
        $model->company_id = $this->company_id;
        $model->emoney_id = $this->id;
        $model->is_accounting = $this->is_accounting;
        $model->has_invoice = 0;

        if ($this->currency_id != Currency::DEFAULT_ID) {
            $model->currency_id = $this->currency_id;
            $model->currency_name = Currency::name($this->currency_id);
        }

        $model->populateRelation('selfAccount', $this);

        return $model;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['currency_id', 'default', 'value' => '643'], // rub
            [['name'], 'trim'],
            [['currency_id', 'name'], 'required'],
            [['currency_id'], 'exist', 'targetClass' => Currency::class, 'targetAttribute' => ['currency_id' => 'id']],
            [['is_accounting', 'is_closed'], 'filter', 'filter' => function($value) {
                return (int) $value;
            }],
            [['is_main', 'is_accounting', 'is_closed'], 'boolean'],
            [['name'], 'string', 'max' => 50],
            [
                ['name'], 'unique',
                'targetAttribute' => ['company_id', 'name'],
                'message' => 'Такое название уже существует.',
            ],
            [['is_closed'], function ($attribute, $params) {
                if ($this->$attribute && $this->is_main) {
                    $this->addError($attribute, 'Основной счет E-money не может быть закрыт.');
                }
            }],
            [['createStartBalance'], 'safe'],
            [['startBalanceDate'], 'date', 'format' => 'd.M.yyyy'],
            [
                [
                    'startBalanceAmount',
                    'startBalanceDate',
                ],
                'required',
                'when' => function (Emoney $model) {
                    return $model->createStartBalance;
                },
                'message' => 'Необходимо заполнить',
            ],
            [['startBalanceAmount'],
                'number',
                'numberPattern' => Yii::$app->params['numberPattern'],
                'max' => Yii::$app->params['maxCashSum'] * 100, // with kopeck
                'tooBig' => 'Значение «{attribute}» не должно превышать ' . TextHelper::moneyFormat(Yii::$app->params['maxCashSum'], 2) . '.',
                'whenClient' => 'function(){}', // because of `123,00` - not valid float number (with comma) and there doesn't exist any methods to parse it on client.
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'currency_id' => 'Валюта',
            'name' => 'Название',
            'is_main' => 'Основной',
            'is_accounting' => 'Для учета в бухгалтерии',
            'is_closed' => 'Закрыт',
            'createStartBalance' => 'Указать начальный остаток',
            'startBalanceAmount' => 'Сумма начального остатка',
            'startBalanceDate' => 'на дату',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashEmoneyFlows()
    {
        return $this->hasMany(CashEmoneyFlows::className(), ['emoney_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * @param boolean $insert
     * @return boolean
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if (!$this->currency_id) {
            $this->currency_id = Currency::DEFAULT_ID;
        }

        if ($this->is_main) {
            $this->is_closed = false;
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $condition = [
            'and',
            ['company_id' => $this->company_id],
            ['is_main' => 1],
            ['not', ['id' => $this->id]],
        ];

        if ($this->is_main && self::find()->where($condition)->exists()) {
            self::updateAll(['is_main' => 0], $condition);
        }

        if ($this->createStartBalance) {
            if ($this->currency_id == Currency::DEFAULT_ID) {
                $model = new CashEmoneyFlows();
                $model->date = $this->startBalanceDate;
                $model->amount = $this->startBalanceAmount;
            } else {
                $model = new CashEmoneyForeignCurrencyFlows();
                $model->currency_id = $this->currency_id;
                $model->date_input = $this->startBalanceDate;
                $model->amount_input = $this->startBalanceAmount;
            }
            $model->flow_type = CashFlowsBase::FLOW_TYPE_INCOME;
            $model->company_id = $this->company_id;
            $model->emoney_id = $this->id;
            $model->is_accounting = $this->is_accounting;
            $model->contractor_id = CashContractorType::BALANCE_TEXT;
            $model->income_item_id = InvoiceIncomeItem::ITEM_STARTING_BALANCE;
            $model->description = 'Баланс начальный';
            $model->save();
        }
    }

    /**
     * @return boolean
     */
    public function canDelete()
    {
        return !($this->is_main || $this->getCashEmoneyFlows()->exists());
    }

    /**
     * @return string
     */
    public function getTypeLabel()
    {
        return $this->is_closed ? 'Закрыт' : ($this->is_main ? 'Основной' : 'Дополнительный');
    }

    /**
     * @inheritDoc
     */
    public function getWalletName(): string
    {
        return $this->name;
    }
}
