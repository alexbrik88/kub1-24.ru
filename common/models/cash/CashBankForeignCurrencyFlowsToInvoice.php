<?php

namespace common\models\cash;

use Yii;

/**
 * This is the model class for table "cash_bank_foreign_currency_flows_to_invoice".
 *
 * @property int $flow_id
 * @property int $invoice_id
 * @property int $amount
 *
 * @property CashBankForeignCurrencyFlows $flow
 * @property ForeignCurrencyInvoice $invoice
 */
class CashBankForeignCurrencyFlowsToInvoice extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cash_bank_foreign_currency_flows_to_invoice';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['flow_id', 'invoice_id', 'amount'], 'required'],
            [['flow_id', 'invoice_id', 'amount'], 'integer'],
            [['flow_id', 'invoice_id'], 'unique', 'targetAttribute' => ['flow_id', 'invoice_id']],
            [['flow_id'], 'exist', 'skipOnError' => true, 'targetClass' => CashBankForeignCurrencyFlows::className(), 'targetAttribute' => ['flow_id' => 'id']],
            [['invoice_id'], 'exist', 'skipOnError' => true, 'targetClass' => ForeignCurrencyInvoice::className(), 'targetAttribute' => ['invoice_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'flow_id' => 'Flow ID',
            'invoice_id' => 'Invoice ID',
            'amount' => 'Amount',
        ];
    }

    /**
     * Gets query for [[Flow]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFlow()
    {
        return $this->hasOne(CashBankForeignCurrencyFlows::className(), ['id' => 'flow_id']);
    }

    /**
     * Gets query for [[Invoice]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(ForeignCurrencyInvoice::className(), ['id' => 'invoice_id']);
    }
}
