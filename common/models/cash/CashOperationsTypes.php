<?php

namespace common\models\cash;

use Yii;

/**
 * @property integer $id
 * @property integer $code
 * @property string $name
 */
class CashOperationsTypes extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'cash_operations_types';
    }

    public function rules()
    {
        return [
            [['code'], 'integer'],
            [['code', 'name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Код',
            'name' => 'Название',
        ];
    }
}
