<?php

namespace common\models\cash;

use common\components\cash\InternalTransferFlowInterface;
use common\components\cash\behaviors\CalculationBehavior;
use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\cash\form\CashBankFlowsForm;
use common\models\Company;
use common\models\Contractor;
use common\models\currency\Currency;
use common\models\currency\CurrencyRate;
use common\models\document\Invoice;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\document\PayableInvoiceInterface;
use common\models\document\status\InvoiceStatus;
use common\models\project\Project;
use common\models\ExpenseItemFlowOfFunds;
use common\models\IncomeItemFlowOfFunds;
use DateTime;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\models\log\ILogMessage;
use frontend\models\log\Log;
use frontend\models\log\LogEvent;
use frontend\modules\analytics\models\credits\Credit;
use frontend\modules\cash\models\CashContractorType;
use frontend\modules\documents\components\InvoiceHelper;
use Yii;
use yii\base\Exception;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * @author ErickSkrauch <erickskrauch@yandex.ru>
 *
 * Поля и отношения, которые присутствуют во всех унаследованных моделях
 * Поля:
 * @property integer $id
 * @property integer $company_id
 * @property string $date
 * @property integer $flow_type
 * @property integer $has_invoice
 * @property integer $invoice_id
 * @property integer $amount
 * @property string $description
 * @property integer $income_item_id
 * @property integer $expenditure_item_id
 * @property integer $contractor_id
 * @property integer $created_at
 * @property integer $is_funding_flow
 * @property integer $cash_funding_type_id
 * @property integer $cash_id
 *
 * Геттеры:
 * @property string $formattedDescription Назначение с набором условий согласно ТЗ
 * @property string $amountIncome Возвращает amount только если операция прихода
 * @property string $amountExpense Возвращает amount только если операция расхода
 *
 * Поведения:
 * @property CalculationBehavior $Calculation
 *
 * Миксины поведений:
 * @mixin CalculationBehavior
 *
 * Отношения:
 * @property Contractor $contractor
 * @property Company $company
 * @property Invoice $invoice
 * @property InvoiceExpenditureItem $expenditureReason
 * @Property InvoiceIncomeItem $incomeReason
 * @property CashContractorType $cashContractor
 * @property int $walletType
 * @property string $creditPaying
 */
abstract class CashFlowsBase extends ActiveRecord implements ILogMessage
{
    const WALLET_BANK = 1;
    const WALLET_CASHBOX = 2;
    const WALLET_EMONEY = 3;
    const WALLET_ACQUIRING = 4;
    const WALLET_CARD = 5;

    const WALLET_FOREIGN_BANK = 11;
    const WALLET_FOREIGN_CASHBOX = 12;
    const WALLET_FOREIGN_EMONEY = 13;

    const OPERATION_TYPE_FACT = 1;
    const OPERATION_TYPE_PLAN = 2;

    /**
     *
     */
    const CONTRACTOR_ALL = 0;

    /**
     *
     */
    const FLOW_TYPE_EXPENSE = 0;
    /**
     *
     */
    const FLOW_TYPE_INCOME = 1;
    /**
     *
     */
    const INTERNAL_TRANSFER = 2;

    /**
     *
     */
    const HAS_INVOICE = 1;
    /**
     *
     */
    const NON_INVOICE = 0;

    /**
     *
     */
    const NON_ACCOUNTING = 0;
    /**
     *
     */
    const ACCOUNTING = 1;

    public $invoices_list = [];
    public $invoice_ids;
    public $paid_sum;

    public $employeeCompany;

    public $incomeItemIdManyItem;
    public $expenditureItemIdManyItem;

    protected $_statisticQuery;

    /**
     * @var bool
     */
    public static $formNameModify = false;

    /**
     * @inheritdoc
     */
    public function formName()
    {
        return static::$formNameModify ? parent::formName() . ($this->isNewRecord? '': $this->id) : parent::formName();
    }

    public function getIsForeign()
    {
        return false;
    }

    /**
     * @inheritdoc
     */
    public function getStatisticQuery()
    {
        return $this->_statisticQuery ? clone $this->_statisticQuery : null;
    }

    /**
     * @inheritdoc
     */
    public function setInternalTransferAttributes(CashFlowsBase $flow) : void
    {
        if ($flow instanceof InternalTransferFlowInterface) {
            $this->is_internal_transfer = 1;
            $this->internal_transfer_flow_table = $flow::tableName();
            $this->internal_transfer_flow_id = $flow->id;
            $this->contractor_id = $flow->getCashContractorTypeText();

            if ($this->hasAttribute('cash_id')) {
                $this->cash_id = $flow->id;
            }

            if ($account = $flow->selfAccount) {
                $this->internal_transfer_account_table = $account::tableName();
                $this->internal_transfer_account_id = $account->id;
                if ($flow instanceof CashBankFlows && $this->hasAttribute('other_rs_id')) {
                    $this->other_rs_id = $account->id;
                }
                if ($flow instanceof CashOrderFlows && $this->hasAttribute('other_cashbox_id')) {
                    $this->other_cashbox_id = $account->id;
                }
                if ($flow instanceof CashEmoneyFlows && $this->hasAttribute('other_emoney_id')) {
                    $this->other_emoney_id = $account->id;
                }
            } else {
                $this->internal_transfer_account_table = null;
                $this->internal_transfer_account_id = null;
            }
        }
    }

    /**
     * @param int $companyId
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getFundingArray($companyId)
    {
        $datePeriod = StatisticPeriod::getSessionPeriod();

        $fundingArray = static::find()
            ->addSelect('cash_funding_type_id')
            ->selectSum('sum')
            ->byCompany($companyId)
            ->byFundingFlow()
            ->andWhere([
                'between', 'date', $datePeriod['from'], $datePeriod['to'],
            ])
            ->groupBy('cash_funding_type_id')
            ->asArray()
            ->all();
        $fundingArray = \yii\helpers\ArrayHelper::map($fundingArray, 'cash_funding_type_id', 'sum');

        return $fundingArray;
    }

    /**
     * @return query\CashFlowBase
     */
    public static function find()
    {
        return new query\CashFlowBase(get_called_class());
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Компания',
            'date' => 'Дата оплаты',
            'date_input' => 'Дата оплаты',
            'flow_type' => 'Тип движения',
            'has_invoice' => 'Связь со счётом',
            'invoice_id' => 'Счёт',
            'amount' => 'Сумма',
            'amount_input' => 'Сумма',
            'description' => 'Назначение',
            'income_item_id' => 'Статья дохода',
            'expenditure_item_id' => 'Статья расхода',
            'contractor_id' => $this->getContractorLabel(),
            'created_at' => 'Время создания',
            'is_funding_flow' => 'Финансирование',
            'cash_funding_type_id' => 'Тип финансирования',
            'invoices_list' => 'Оплаченные счета',
            'project_id' => 'Проект',
            'recognitionDateInput' => 'Дата признания ' . (
                $this->flow_type == self::FLOW_TYPE_INCOME ? 'дохода' : 'расхода'
            ),
            'recognition_date_input' => 'Дата признания ' . (
                $this->flow_type == self::FLOW_TYPE_INCOME ? 'дохода' : 'расхода'
            ),
        ];
    }

    /**
     * @return string
     */
    public function getContractorLabel()
    {
        return ArrayHelper::getValue([
            self::FLOW_TYPE_INCOME => 'Покупатель',
            self::FLOW_TYPE_EXPENSE => 'Поставщик',
        ], $this->flow_type, 'Контрагент');
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
            ],
            'Calculation' => [
                'class' => CalculationBehavior::className(),
                'beginAt' => DateTime::createFromFormat(DateHelper::FORMAT_DATE, StatisticPeriod::getSessionPeriod()['from']),
                'endAt' => DateTime::createFromFormat(DateHelper::FORMAT_DATE, StatisticPeriod::getSessionPeriod()['to']),
            ],
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['expenditure_item_id', 'income_item_id'], 'filter', 'filter' => function ($value) {
                return intval($value) ? : null;
            }],
            [['amount'], 'required', 'message' => 'Необходимо заполнить.',],
            [['amount'],
                'number',
                'numberPattern' => Yii::$app->params['numberPattern'],
                'max' => Yii::$app->params['maxCashSum'] * 100, // with kopeck
                'tooBig' => 'Значение «{attribute}» не должно превышать ' . TextHelper::moneyFormat(Yii::$app->params['maxCashSum'], 2) . '.',
                'whenClient' => 'function(){}', // because of `123,00` - not valid float number (with comma) and there doesn't exist any methods to parse it on client.
            ],
            [['amount'], 'compare', 'operator' => '>', 'compareValue' => 0,
                'whenClient' => 'function(){}', // because of `123,00` - not valid float number (with comma) and there doesn't exist any methods to parse it on client.
            ],
            [['date'], 'date', 'format' => 'd.M.yyyy'],
            [['has_invoice'], 'default', 'value' => self::NON_INVOICE],
            [['company_id', 'flow_type',], 'required', 'message' => 'Необходимо заполнить.',],
            [
                ['description',],
                'required',
                'message' => 'Необходимо заполнить.',
                'when' => function ($model) {
                    return !$model->is_internal_transfer;
                },
            ],
            [['company_id', 'has_invoice', 'expenditure_item_id', 'cash_id'], 'integer'],
            [['flow_type'], 'in', 'range' => array_keys(self::getFlowTypes())],
            [['has_invoice', 'is_funding_flow'], 'boolean'],
            [['description'], 'string', 'max' => 255],
            [['cash_funding_type_id'], 'in', 'range' => CashFundingType::find()->select('id')->column(),
                'message' => 'Тип финансирования не найден.',
            ],
            'contractor_required' => ['contractor_id', 'required', 'message' => 'Необходимо заполнить.'],
            'contractor' => [['contractor_id'], 'validateContractor'],
            [['invoices_list'], 'validateInvoicesList'],
            [
                ['expenditure_item_id'],
                'in',
                'range' => InvoiceExpenditureItem::$internalItems,
                'when' => function ($model) {
                    return $model->is_internal_transfer;
                }
            ],
            [
                ['income_item_id'],
                'in',
                'range' => InvoiceIncomeItem::$internalItems,
                'when' => function ($model) {
                    return $model->is_internal_transfer;
                }
            ],

        ];
    }

    /**
     * @inheritdoc
     */
    public function fields()
    {
        $fields = parent::fields();
        $fields['invoices_list'] = 'invoices_list';

        return $fields;
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateContractor($attribute, $params)
    {
        $isExist = Contractor::find()->andWhere([
            'id' => $this->$attribute,
        ])->andWhere([
            'or',
            ['company_id' => null],
            ['company_id' => $this->company_id],
        ])->exists() || CashContractorType::find()->andWhere([
            'name' => $this->$attribute,
        ])->exists();

        if (!$isExist) {
            $this->addError($attribute, 'Контрагент не найден.');
        }
    }

    public function validateInvoicesList($attribute, $params)
    {
        if (!is_array($this->$attribute)) {
            $this->$attribute = (array) $this->$attribute;
        }

        if ($this->$attribute &&
            $this->company &&
            $this->contractor &&
            !$this->hasErrors('flow_type')
        ) {
            $availableInvoices = form\CashBankFlowsForm::getAvailableInvoices(
                $this->company,
                $this->contractor,
                ($this->flow_type != static::FLOW_TYPE_INCOME),
                $this
            );

            $availableInvoicesIds = [];

            foreach ($availableInvoices as $invoice) {
                $availableInvoicesIds[] = $invoice['id'];
            }

            foreach ($this->$attribute as $invoiceId) {
                if (!is_numeric($invoiceId) || !in_array($invoiceId, $availableInvoicesIds)) {
                    $this->addError($attribute, 'Выбран некорректный счет');
                }
            }
        }
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if (!$this->getIsForeign()) {
            $this->amount = TextHelper::parseMoneyInput($this->amount);

            if ($this->isAttributeChanged('amount', false)) {
                $this->amount = round($this->amount * 100);
            }
        }

        return parent::beforeValidate();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContractor()
    {
        return $this->hasOne(Contractor::className(), ['id' => 'contractor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return common\models\employee\Employee
     */
    public function getAuthor()
    {
        if (Yii::$app->id == 'app-frontend' && $this->company->getEmployees()->andWhere(['id' => Yii::$app->user->id])->exists()) {
            return Yii::$app->user->getIdentity();
        } else {
            return $this->company->getEmployeeChief();
        }
    }

    /**
     * @return common\models\document\Invoice|null
     */
    public function getInvoice()
    {
        return $this->getInvoices()->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpenditureReason()
    {
        return $this->hasOne(InvoiceExpenditureItem::className(), ['id' => 'expenditure_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIncomeReason()
    {
        return $this->hasOne(InvoiceIncomeItem::className(), ['id' => 'income_item_id']);
    }

    /**
     * @return [common\models\document\Invoice]
     */
    public function getInvoices()
    {
        return $this->hasMany(Invoice::className(), ['id' => 'invoice_id'])
            ->via('flowInvoices');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    /**
     * @return bool
     */
    public function hasInvoice()
    {
        return $this->getInvoices()->exists();
    }

    /**
     * @param Invoice $invoice
     * @return bool
     */
    public function linkInvoice(PayableInvoiceInterface $invoice, $amount = null)
    {
        if ($this->contractor_id == $invoice->contractor_id && !$this->getFlowInvoices()->andWhere(['invoice_id' => $invoice->id])->exists()) {
            if (!$this->isForeign) {
                $date = \DateTime::createFromFormat('Y-m-d|', $this->date);
                if ($date && $invoice->recalculateAmountOnDate($date)) {
                    InvoiceHelper::save($invoice);
                }
            }
            if ($amount) {
                $amount = min((int)$amount, (int)$this->getAvailableAmount(), (int)$invoice->getAvailablePaymentAmount());
            } else {
                $amount = min((int)$this->getAvailableAmount(), (int)$invoice->getAvailablePaymentAmount());
            }
            $amount = max(0, $amount);
            if ($amount) {
                $this->link('invoices', $invoice, ['amount' => $amount]);
                if ($this->has_invoice == self::NON_INVOICE) {
                    $this->updateAttributes([
                        'has_invoice' => self::HAS_INVOICE,
                    ]);
                }
            }
            $invoice->checkPaymentStatus($this);

            if (!$this->isForeign) {
                if ($invoice->invoice_status_id == InvoiceStatus::STATUS_PAYED)
                    \frontend\modules\analytics\models\PlanCashContractor::deleteAllFlowsByInvoice($invoice);
                elseif ($invoice->invoice_status_id == InvoiceStatus::STATUS_PAYED_PARTIAL)
                    \frontend\modules\analytics\models\PlanCashContractor::updateFlowByInvoice($invoice);
            }

            return $this->getFlowInvoices()->andWhere(['invoice_id' => $invoice->id])->exists();
        }

        return false;
    }

    /**
     * @param Invoice $invoice
     * @return bool
     */
    public function unlinkInvoice(PayableInvoiceInterface $invoice, $checkHasInvoice = true)
    {
        if ($this->getFlowInvoices()->andWhere(['invoice_id' => $invoice->id])->exists()) {
            $this->unlink('invoices', $invoice, true);

            if ($checkHasInvoice && !$this->hasInvoice()) {
                $this->updateAttributes([
                    'has_invoice' => self::NON_INVOICE,
                ]);
            }
        }
        $invoice->checkPaymentStatus($this);

        if (!$this->isForeign) {
            \frontend\modules\analytics\models\PlanCashContractor::createFlowByInvoice($invoice);
        }

        return true;
    }

    /**
     * @return array
     */
    public static function getFlowTypes()
    {
        return [
            self::FLOW_TYPE_INCOME => 'Приход',
            self::FLOW_TYPE_EXPENSE => 'Расход',
        ];
    }

    /**
     * @return array
     */
    public function getFlowTypeLabel()
    {
        return self::getFlowTypes()[$this->flow_type] ?? '';
    }


    /**
     * @return string
     */
    public function getFormattedDescription()
    {
        if (!$this->hasInvoice()) {
            return $this->description;
        }

        if ($this->flow_type == self::FLOW_TYPE_EXPENSE) {
            $string = 'вх. счёт №';
        } else {
            $string = 'исх. счет №';
        }

        return $string;
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if ($this->flow_type == self::FLOW_TYPE_INCOME && $this->expenditure_item_id) {
            $this->expenditure_item_id = null;
        }
        if ($this->flow_type == self::FLOW_TYPE_EXPENSE && $this->income_item_id) {
            $this->income_item_id = null;
        }
        if ($this->has_invoice === null) {
            $this->has_invoice = false;
        }

        if (!$insert && $this->isAttributeChanged('flow_type', false)) {
            $this->addError('flow_type', 'Нельзя изменять тип операции');

            return false;
        }

        if (!$this instanceof CashBankForeignCurrencyFlows) {
            // Конвертируем человеко-понятный формат в формат для базы
            $this->date = DateHelper::format($this->date, DateHelper::FORMAT_DATE, DateHelper::FORMAT_USER_DATE);
        }

        if ($this->hasAttribute('recognition_date') && empty($this->recognition_date)) {
            $this->recognition_date = $this->date;
        }

        if ($this->getIsForeign() &&
            $this->hasAttribute('amount_rub') &&
            ($this->isAttributeChanged('amount', false) || $this->isAttributeChanged('date') || $this->isAttributeChanged('currency_name'))
        ) {
            if (($date = date_create($this->date)) && ($rate = CurrencyRate::getRateOnDate($date)[$this->currency_name] ?? null)) {
                $this->amount_rub = round($this->amount/$rate['amount']*$rate['value']);
            } else {
                $this->amount_rub = 0;
            }
        }

        return true;
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            foreach ($this->getInvoices()->all() as $invoice) {
                $this->unlinkInvoice($invoice, false);
            }

            return !$this->getInvoices()->exists();
        } else {
            return false;
        }
    }

    /** @inheritdoc */
    public function afterFind()
    {
        parent::afterFind();

        if (!$this instanceof CashBankForeignCurrencyFlows && strlen($this->date) > 0) {
            $this->date = DateHelper::format($this->date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
        }

        foreach ($this->invoices as $invoice) {
            $this->invoices_list[] = $invoice->id;
        }
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert && ($linkage = $this->getLinkage())) {
            $linkage->createFlow($this);
        }

        if ($contractor = $this->contractor) {
            $contractorAttributes = [];
            if ($this->flow_type == self::FLOW_TYPE_EXPENSE &&
                $contractor->invoice_expenditure_item_id != $this->expenditure_item_id
            ) {
                $contractorAttributes['invoice_expenditure_item_id'] = $this->expenditure_item_id;
            }
            if ($this->flow_type == self::FLOW_TYPE_INCOME &&
                $contractor->invoice_income_item_id != $this->income_item_id
            ) {
                $contractorAttributes['invoice_income_item_id'] = $this->income_item_id;
            }
            if ($contractorAttributes) {
                if ($contractor->isKub()) {
                    $kubContractorAttributes = [];
                    if (isset($contractorAttributes['invoice_income_item_id']))
                        $kubContractorAttributes['invoice_income_item_id'] = $contractorAttributes['invoice_income_item_id'];
                    if (isset($contractorAttributes['invoice_expenditure_item_id']))
                        $kubContractorAttributes['invoice_expenditure_item_id'] = $contractorAttributes['invoice_expenditure_item_id'];
                    if ($kubContractorAttributes) {
                        $this->contractor->getKubItem($this->company_id)->updateAttributes($kubContractorAttributes);
                    }
                } else {
                    $this->contractor->updateAttributes($contractorAttributes);
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();

        if ($this->is_internal_transfer && ($model = $this->internalTransferFlow) !== null) {
            if (!$model->delete()) {
                throw new \yii\web\ServerErrorHttpException("Failed to delete the associated model");
            }
        }
        /*
        if (in_array($this->contractor_id, [CashContractorType::BANK_TEXT, CashContractorType::EMONEY_TEXT, CashContractorType::ORDER_TEXT])) {
            $className = $this->getClassNameByCashContractorType($this->contractor_id);
            if (($this->cash_id ?? null) !== null) {
                $model = $className::findOne($this->cash_id);
            } else {
                $model = $className::findOne(['cash_id' => $this->id]);
            }
            if ($model !== null && !$model->delete()) {
                throw new \yii\web\ServerErrorHttpException("Failed to delete the associated model");
            }
        }
        */
    }

    /**
     * @inheritdoc
     */
    public function getLinkage() : ?CashFlowLinkage
    {
        $model = null;

        if (is_numeric($this->contractor_id) && $this instanceof CashBankFlows) {
            $model = CashFlowLinkage::findOne([
                'company_id' => $this->company_id,
                'if_flow_type' => $this->flow_type,
                'if_contractor_id' => $this->contractor_id,
                'is_deleted' => false,
                'status' => CashFlowLinkage::STATUS_ACTIVE,
            ]);
            if ($model !== null) {
                $model->populateRelation('company', $this->company);
            }
        }

        return $model;
    }

    /** @inheritdoc */
    public function linkSelectedInvoices()
    {
        $invoiceIdArray = is_array($this->invoices_list) ? $this->invoices_list : [];

        /** Пересчитываем привязки к счетам */
        $invioceArray = $this->getInvoices()->all();
        foreach ($invioceArray as $invoice) {
            if (in_array($invoice->id, $invoiceIdArray)) {
                $invoiceIdArray = array_diff($invoiceIdArray, [$invoice->id]);
            } else {
                $this->unlinkInvoice($invoice);
            }
        }

        if ($invoiceIdArray && ($newInvoiceArray = Invoice::find()->andWhere(['id' => $invoiceIdArray])->all())) {
            foreach ($newInvoiceArray as $invoice) {
                $this->linkInvoice($invoice);
            }
        }
    }

    /** @inheritdoc */
    public function linkSelectedTinChildInvoices()
    {
        if (empty($this->parent_id))
            return;

        $invoiceIdArray = is_array($this->invoices_list) ? $this->invoices_list : [];

        /** Пересчитываем привязки к счетам */
        $invioceArray = $this->getInvoices()->all();
        foreach ($invioceArray as $invoice) {
            if (in_array($invoice->id, $invoiceIdArray)) {
                $invoiceIdArray = array_diff($invoiceIdArray, [$invoice->id]);
            } else {
                $this->unlink('invoices', $invoice, true);
            }
        }

        if ($invoiceIdArray && ($newInvoiceArray = Invoice::find()->andWhere(['id' => $invoiceIdArray])->all())) {
            foreach ($newInvoiceArray as $invoice) {
                $parentPayment = CashBankFlowToInvoice::find()
                    ->where(['flow_id' => $this->parent_id, 'invoice_id' => $invoice->id])
                    ->sum('amount');
                $tinChildPayment = min((int)$this->tin_child_amount, (int)$invoice->getAvailablePaymentAmount() + (int)$parentPayment);
                $this->link('invoices', $invoice, ['amount' => 0, 'tin_child_amount' => $tinChildPayment]);
            }
        }
    }

    /** @inheritdoc */
    public function linkSelectedTinParentInvoices(array $pieces)
    {
        $invoiceIdArray = is_array($this->invoices_list) ? $this->invoices_list : [];

        /** Пересчитываем привязки к счетам */
        $invioceArray = $this->getInvoices()->all();
        foreach ($invioceArray as $invoice) {
            if (in_array($invoice->id, $invoiceIdArray)) {
                $invoiceIdArray = array_diff($invoiceIdArray, [$invoice->id]);
            } else {
                $this->unlinkInvoice($invoice);
            }
        }

        foreach ($pieces as $piece) {
            $invoiceIdArray = $piece->invoices_list;
            $tinChildAmountPart = $piece->tin_child_amount;
            if ($invoiceIdArray && ($newInvoiceArray = Invoice::find()->andWhere(['id' => $invoiceIdArray])->all())) {
                foreach ($newInvoiceArray as $invoice) {
                    $this->linkInvoice($invoice, $tinChildAmountPart);
                }
            }
        }
    }


    /**
     * @return int|string
     */
    public function getAmountIncome()
    {
        return $this->flow_type == self::FLOW_TYPE_INCOME ? $this->amount : '-';
    }

    /**
     * @return int|string
     */
    public function getAmountExpense()
    {
        return $this->flow_type == self::FLOW_TYPE_EXPENSE ? $this->amount : '-';
    }

    /**
     * @param bool|true $addAll
     * @param bool|false $includeDeleted
     * @return array
     */
    public function getContractorList($addAll = true, $includeDeleted = false)
    {
        $contractorArray = [];

        if ($addAll) {
            $contractorArray[null] = 'Все контрагенты';
        }

        $query = self::find()
            ->distinct()
            ->select(['contractor.id', 'contractor.name'])
            ->innerJoin(Contractor::tableName(), 'contractor.id = contractor_id')
            ->andWhere([
                'contractor.company_id' => Yii::$app->user->identity->company->id,
                'contractor.is_deleted' => (bool)$includeDeleted,
                'contractor.status' => Contractor::ACTIVE,
            ])
            ->orderBy('contractor.name ASC');

        $dateRange = StatisticPeriod::getSessionPeriod();
        $query->andWhere(['between', 'date', $dateRange['from'], $dateRange['to']]);

        $contractorArray += ArrayHelper::map($query->asArray()->all(), 'id', 'name');


        return $contractorArray;
    }

    /**
     * @param Log $log
     * @param $flowName
     * @return string
     */
    protected function getLogMessageBase(Log $log, $flowName)
    {
        $contractor = Contractor::findOne($log->getModelAttributeNew('contractor_id'));
        $contractorName = $contractor ? ', ' . $contractor->getTitle(true) . ', ' : '';
        $currencyName = $this instanceof CashBankForeignCurrencyFlows ? $log->getModelAttributeNew('currency_name') : 'руб.';

        switch ($log->log_event_id) {
            case LogEvent::LOG_EVENT_CREATE:
                return 'Платеж ' . $flowName .
                ' на сумму ' . TextHelper::invoiceMoneyFormat($log->getModelAttributeNew('amount'), 2) . " $currencyName" .
                $contractorName .
                ' <b>проведен</b>';
            case LogEvent::LOG_EVENT_UPDATE:
                return 'Платеж ' . $flowName .
                ' на сумму ' . TextHelper::invoiceMoneyFormat($log->getModelAttributeNew('amount'), 2) . " $currencyName" .
                $contractorName .
                ' <b>изменен</b>';
            case LogEvent::LOG_EVENT_DELETE:
                return 'Платеж ' . $flowName .
                ' на сумму ' . TextHelper::invoiceMoneyFormat($log->getModelAttributeNew('amount'), 2) . " $currencyName" .
                $contractorName .
                ' <b>удалён</b>';
            default:
                return $log->message;
        }
    }

    /**
     * @inheritdoc
     */
    public function getLogIcon(Log $log)
    {
        $icon = ['', 'fa fa-rub'];

        if ($log->getModelAttributeNew('flow_type') == self::FLOW_TYPE_EXPENSE) {
            $icon[0] = 'label-danger';
        } else {
            $icon[0] = 'label-success';
        }

        return $icon;
    }

    /**
     * @param Invoice $invoice
     * @return int
     */
    public function getInvoiceAmount(Invoice $invoice)
    {
        return (int)$this->getFlowInvoices()->andWhere(['invoice_id' => $invoice->id])->select('amount')->scalar();
    }

    /**
     * @return int
     */
    public function getInvoicesTotalAmount()
    {
        return (int)$this->getFlowInvoices()->sum('amount');
    }

    /**
     * @return int
     */
    public function getAvailableAmount()
    {
        return $this->amount - min([$this->amount, $this->getInvoicesTotalAmount()]);
    }

    /**
     * @return array|null|ActiveRecord
     */
    public function getCashContractor()
    {
        return CashContractorType::find()->andWhere(['name' => $this->contractor_id])->one();
    }

    /**
     * @param bool|false $copy
     * @return bool
     */
    public function checkContractor($copy = false)
    {
        return true;
    }

    /**
     * @param $type
     * @param bool|false $copy
     * @return $this
     * @throws Exception
     */
    public function setNewCash($type, $copy = false)
    {
        $flowType = !$this->flow_type;
        $model = $this->getNewModelByCashContractorType($type);
        $model->setAttributes([
            'author_id' => Yii::$app->user->identity->id,
            'number' => (string) CashOrderFlows::getNextNumber($this->company_id, $flowType, $this->date),
            'company_id' => $this->company_id,
            'date' => $this->date,
            'flow_type' => $flowType,
            'has_invoice' => $this->has_invoice ? $this->has_invoice : false,
            'invoice_id' => $this->invoice_id,
            'amount' => $copy ? $this->amount : $this->amount * 100,
            'description' => $this->description,
            'expenditure_item_id' => $flowType == self::FLOW_TYPE_EXPENSE ? InvoiceExpenditureItem::ITEM_OWN_FOUNDS : null,
            'income_item_id' => $flowType == self::FLOW_TYPE_INCOME ? InvoiceIncomeItem::ITEM_OWN_FOUNDS : null,
            'contractor_id' => $this->getNewCashContractorId(),
            'created_at' => $this->created_at,
            'reason_id' => CashOrdersReasonsTypes::VALUE_OTHER,
            'other' => 'Прочее',
            'is_funding_flow' => $this->is_funding_flow,
            'cash_funding_type_id' => $this->cash_funding_type_id,
            'cash_id' => $this->id,
            'is_accounting' => $this->is_accounting
        ]);

        return $model;
    }

    /**
     * @param $type
     * @return CashBankFlows|CashEmoneyFlows|CashOrderFlows
     * @throws Exception
     */
    public function getNewModelByCashContractorType($type)
    {
        switch ($type) {
            case CashContractorType::BANK_TEXT:
            case CashContractorType::COMPANY_TEXT:
                $model = new CashBankFlows();
                break;
            case CashContractorType::ORDER_TEXT:
                $model = new CashOrderFlows();
                break;
            case CashContractorType::EMONEY_TEXT:
                $model = new CashEmoneyFlows();
                break;
            case CashContractorType::BANK_CURRENCY_TEXT:
                $model = new CashBankForeignCurrencyFlows();
                break;
            default:
                throw new Exception('Cash with type = "' . $type . '" not found.');
        }

        return $model;
    }

    /**
     * @param $type
     * @return string
     * @throws Exception
     */
    public function getClassNameByCashContractorType($type)
    {
        switch ($type) {
            case CashContractorType::BANK_TEXT:
                $className = CashBankFlows::className();
                break;
            case CashContractorType::ORDER_TEXT:
                $className = CashOrderFlows::className();
                break;
            case CashContractorType::EMONEY_TEXT:
                $className = CashEmoneyFlows::className();
                break;
            default:
                throw new Exception('Cash with type = "' . $type . '" not found.');
        }

        return $className;
    }

    /**
     * @return int
     */
    public function getNewCashContractorId()
    {
        if ($this instanceof CashBankFlows) {
            $cashContractorId = CashContractorType::BANK_TEXT;
        } elseif ($this instanceof CashOrderFlows) {
            $cashContractorId = CashContractorType::ORDER_TEXT;
        } else {
            $cashContractorId = CashContractorType::EMONEY_TEXT;
        }

        return $cashContractorId;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getAllFlows($select, $where, $unionAll = true, $join = null)
    {
        $way1 = [CashFactory::TYPE_BANK . ' AS [[flow_way]]'];
        $way2 = [CashFactory::TYPE_ORDER . ' AS [[flow_way]]'];
        $way3 = [CashFactory::TYPE_EMONEY . ' AS [[flow_way]]'];

        $query1 = CashBankFlows::find()->alias('flow')->select(array_merge($select, $way1))->andWhere($where);
        $query2 = CashOrderFlows::find()->alias('flow')->select(array_merge($select, $way2))->andWhere($where);
        $query3 = CashEmoneyFlows::find()->alias('flow')->select(array_merge($select, $way3))->andWhere($where);

        if ($join !== null) {
            $query1->{$join}(['link' => 'cash_bank_flow_to_invoice'], '{{flow}}.[[id]] = {{link}}.[[flow_id]]');
            $query2->{$join}(['link' => 'cash_order_flow_to_invoice'], '{{flow}}.[[id]] = {{link}}.[[flow_id]]');
            $query3->{$join}(['link' => 'cash_emoney_flow_to_invoice'], '{{flow}}.[[id]] = {{link}}.[[flow_id]]');
        }

        return $query1->union($query2, $unionAll)->union($query3, $unionAll);
    }

    public function getCurrencySymbol()
    {
        return ArrayHelper::getValue(Currency::$currencySymbols, $this->currency_name ?? Currency::DEFAULT_NAME);
    }

    public function getWalletType()
    {
        switch (get_class($this)) {
            case CashBankFlows::class:
            case CashBankFlowsForm::class:
                return self::WALLET_BANK;
            case CashOrderFlows::class:
                return self::WALLET_CASHBOX;
            case CashEmoneyFlows::class:
                return self::WALLET_EMONEY;
            case CashBankForeignCurrencyFlows::class:
                return self::WALLET_FOREIGN_BANK;
            case CashOrderForeignCurrencyFlows::class:
                return self::WALLET_FOREIGN_CASHBOX;
            case CashEmoneyForeignCurrencyFlows::class:
                return self::WALLET_FOREIGN_EMONEY;
        }

        return null;
    }

    public function canChangeProject()
    {
        if (!$this->project_id)
            return true;

        foreach ((array)$this->invoices_list as $invoiceId) {
            if ($relInvoice = Invoice::findOne($invoiceId)) {
                if ($this->project_id == $relInvoice->project_id) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * @return Credit|null
     */
    public function getCredit()
    {
        return Credit::findOne([
            'company_id' => $this->company_id,
            'credit_id' => $this->credit_id
        ]);
    }

    /**
     * @return string
     */
    public function getCreditPaying($showLink = true)
    {
        if (!property_exists($this, 'credit_id') || !$this->credit_id) {
            return '';
        }

        return ($credit = $this->getCredit()) ? (
            $showLink ? \yii\helpers\Html::a('Договор<br>№' . $credit->agreement_number, [
                '/analytics/credit/registry',
                'credit_id' => $credit->credit_id,
            ]) : 'Договор №' . $credit->agreement_number
        ) : '';
    }
}
