<?php

namespace common\models\cash;

use common\models\Company;
use common\models\employee\Employee;
use frontend\models\Documents;
use frontend\models\log\Log;
use frontend\models\log\ILogMessage;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use Yii;

/**
 * This is the model class for table "cash_order_statement_upload".
 *
 * @property integer $id
 * @property integer $company_id
 * @property integer $cashbox_id
 * @property string $service
 * @property integer $source
 * @property string $period_from
 * @property string $period_till
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $saved_count
 *
 * @property Cashbox $cashbox
 * @property Company $company
 * @property Employee $createdBy
 */
class CashOrderStatementUpload extends \yii\db\ActiveRecord implements ILogMessage
{
    const SOURCE_FILE = 1;
    const SOURCE_OFD = 2;
    const SOURCE_OFD_AUTO = 3;

    public static $sources = [
        self::SOURCE_FILE,
        self::SOURCE_OFD,
        self::SOURCE_OFD_AUTO,
    ];

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
            ],
            [
                'class' => BlameableBehavior::className(),
                'updatedByAttribute' => false,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cash_order_statement_upload';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'cashbox_id' => 'Cashbox ID',
            'period_from' => 'Period From',
            'period_till' => 'Period Till',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'saved_count' => 'Saved Count',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashbox()
    {
        return $this->hasOne(Cashbox::className(), ['id' => 'cashbox_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->getEmployee();
    }

    /**
     * Get formatted log message from Log model
     * @param Log $log
     * @return string
     */
    public function getLogMessage(Log $log)
    {
        return "Выписка по {$this->cashbox->name} загружена";
    }

    /**
     * Returns array for set color and icon. Used on displaying log messages.
     * @example: ['label-danger', 'fa fa-file-text-o']
     * @param Log $log
     * @return array
     */
    public function getLogIcon(Log $log)
    {
        return Documents::getLogIcon($log);
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if (is_a(Yii::$app,'yii\console\Application')) {
            $this->created_by = $this->company->employeeChief->id;
        }

        return true;
    }
}
