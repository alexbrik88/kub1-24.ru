<?php

namespace common\models\cash;

use common\components\cash\InternalTransferFlowInterface;
use common\components\cash\InternalTransferHelper;
use common\components\date\DateHelper;
use common\components\pdf\Printable;
use common\components\TaxRobotHelper;
use common\models\cash\query\CashFlowBase;
use common\models\company\CheckingAccountant;
use common\models\company\CompanyIndustry;
use common\models\companyStructure\SalePoint;
use common\models\Contractor;
use common\models\document\Invoice;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\employee\Employee;
use common\models\EmployeeCompany;
use common\models\ofd\OfdReceipt;
use frontend\models\log\Log;
use frontend\modules\analytics\behaviors\credits\CreditFlowBehavior;
use frontend\modules\analytics\models\credits\Credit;
use frontend\modules\cash\models\CashContractorType;
use php_rutils\RUtils;
use Yii;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * @author ErickSkrauch <erickskrauch@yandex.ru>
 *
 * Поля:
 * @property string $date
 * @property string $recognition_date
 * @property integer $author_id
 * @property integer $company_id
 * @property integer $cashbox_id
 * @property integer $is_accounting
 * @property string $number
 * @property integer $reason_id
 * @property string $application
 * @property string $other
 * @property integer $income_item_id
 * @property integer $is_prepaid_expense
 *
 * @property integer $project_id
 * @property integer $sale_point_id
 * @property integer $industry_id
 * @property string $ofd_receipt_uid
 *
 * @property string $contractorInput
 *
 * Геттеры:
 * @property string $amountRubString Сумма прописью целая часть
 * @property string $amountKopeckString Сумма прописью дробная часть
 * @property string $name Название КО
 * @property string $including В том числе
 *
 * Отношения:
 * @property CashOrdersReasonsTypes $reason
 * @property Employee $author
 * @property CashOrderFlowToInvoice[] $flowInvoices
 * @property InvoiceExpenditureItem $expenditureItem
 * @property EmployeeCompany $employeeCompany
 * @property-read string[] $creditList
 * @property string $billPaying
 */
class CashOrderFlows extends CashFlowsBase implements Printable, InternalTransferFlowInterface
{
    /**
     * @var int|null
     */
    public $credit_id;

    /**
     * @var float
     */
    public $credit_amount = 0;

    /**
     * @var array
     */
    public $contractorType = [
        CashFlowsBase::FLOW_TYPE_EXPENSE => Contractor::TYPE_CUSTOMER,
        CashFlowsBase::FLOW_TYPE_INCOME => Contractor::TYPE_SELLER,
    ];

    /**
     * @var bool
     */
    private $_needBankContractorFlow;
    private $_needCashboxContractorFlow;
    private $_needEmoneyContractorFlow;

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['creditFlow'] = [
            'class' => CreditFlowBehavior::class,
            'attribute' => 'cash_order_flow_id',
            'wallet_id' => CashFlowsBase::WALLET_CASHBOX,
        ];

        return $behaviors;
    }

    /**
     * Returns next flow number.
     * If passed existed model with same type - return it's number.
     * @param int $flowType
     * @param CashOrderFlows $model
     * @return int
     */
    public static function getNextNumber($companyId, $flowType, $date = null, $number = null)
    {
        $year = ($date && ($d = date_create($date))) ? $d->format('Y') : date('Y');
        $query = self::find()->where([
            'and',
            ['company_id' => $companyId],
            ['flow_type' => $flowType],
            ['between', 'date', "$year-01-01", "$year-12-31"],
        ]);

        $nextNumber = ($number ? : (int) $query->max('(number * 1)')) + 1;

        if ($query->andWhere(['number' => $nextNumber])->exists()) {
            return static::getNextNumber($companyId, $flowType, $date, $nextNumber);
        } else {
            return $nextNumber;
        }
    }

    /**
     * @return query\CashOrderFlow
     */
    public static function find()
    {
        return new query\CashOrderFlow(get_called_class());
    }

    public function afterFind()
    {
        parent::afterFind();

        if ($this->is_prepaid_expense)
            $this->recognition_date = null;
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'cash_order_flows';
    }

    public function getCashContractorTypeText() : string
    {
        return \frontend\modules\cash\models\CashContractorType::ORDER_TEXT;
    }

    public function getInternalTransferAccountClass() : ?string
    {
        return InternalTransferHelper::accountClassByTable($this->internal_transfer_account_table);
    }

    public function getInternalTransferFlowClass() : ?string
    {
        return InternalTransferHelper::flowClassByTable($this->internal_transfer_flow_table);
    }

    public function getSelfAccount() : ActiveQuery
    {
        return $this->getCashbox();
    }

    public function getInternalTransferAccount() : ?ActiveQuery
    {
        if ($class = $this->getInternalTransferAccountClass()) {
            return $this->hasOne($class, ['id' => 'internal_transfer_account_id']);
        }

        return null;
    }

    public function getInternalTransferFlow() : ?ActiveQuery
    {
        if ($class = $this->getInternalTransferFlowClass()) {
            return $this->hasOne($class, ['id' => 'internal_transfer_flow_id']);
        }

        return null;
    }

    /**
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();
        unset($rules['contractor']);

        return array_merge($rules, [
            [
                ['contractorInput'],
                'validateContractor',
                'skipOnEmpty' => false,
                'when' => function ($model) {
                    return !$model->is_internal_transfer;
                },
            ],
            [['number'], 'filter', 'filter' => function ($value) {
                return (string)$value;
            }],
            [['date', 'cashbox_id', 'author_id'], 'required', 'message' => 'Необходимо заполнить.',],
            [
                [
                    'number',
                    'description',
                ], 'required',
                'message' => 'Необходимо заполнить.',
                'when' => function ($model) {
                    return !$model->is_internal_transfer;
                },
            ],
            [['is_accounting', 'is_prepaid_expense', 'project_id'], 'safe'],
            [['author_id', 'is_accounting', 'is_prepaid_expense', 'project_id'], 'integer'],
            [['number', 'application', 'other'], 'string', 'max' => 255],
            [['expenditure_item_id'], 'required', 'when' => function (CashOrderFlows $model) {
                return !$model->hasErrors('flow_type') && $model->flow_type == CashFlowsBase::FLOW_TYPE_EXPENSE;
            }, 'whenClient' => '
                function(attribute, value) {
                    return $("[name=\'' . Html::getInputName($this, 'flow_type') . '\']").val() == ' . CashFlowsBase::FLOW_TYPE_EXPENSE . ';
                }
            ', 'message' => 'Необходимо заполнить.',],
            [['income_item_id'], 'required', 'when' => function (CashOrderFlows $model) {
                return !$model->hasErrors('flow_type') && $model->flow_type == CashFlowsBase::FLOW_TYPE_INCOME;
            }, 'whenClient' => '
                function(attribute, value) {
                    return $("[name=\'' . Html::getInputName($this, 'flow_type') . '\']:checked").val() == ' . CashFlowsBase::FLOW_TYPE_INCOME . ';
                }
            ', 'message' => 'Необходимо заполнить.',],
            [['other'], 'required', 'when' => function (CashOrderFlows $model) {
                return $model->reason_id == CashOrdersReasonsTypes::VALUE_OTHER;
            }, 'whenClient' => '
                function(attribute, value) {
                    return $("#cashorderflows-reason_id").val() == ' . CashOrdersReasonsTypes::VALUE_OTHER . ';
                }
            ', 'message' => 'Необходимо заполнить.',],
            [['reason_id'], 'validateReason'],
            [
                ['number'], 'unique', 'filter' => function (CashFlowBase $query) {
                    $year = ($d = date_create($this->date)) ? $d->format('Y') : date('Y');

                    return $query->andWhere([
                        'and',
                        ['company_id' => $this->company_id],
                        ['flow_type' => $this->flow_type],
                        ['between', 'date', "$year-01-01", "$year-12-31"],
                    ]);
                },
                'when' => function ($model) {
                    return !$model->is_internal_transfer;
                },
            ],
            [['cashbox_id'], 'exist',
                'targetClass' => Cashbox::className(),
                'targetAttribute' => 'id',
                'filter' => [
                    'company_id' => $this->company_id,
                    'currency_id' => \common\models\currency\Currency::DEFAULT_ID,
                ],
            ],
            [['income_item_id'], 'exist', 'skipOnError' => true,
                'targetClass' => InvoiceIncomeItem::className(),
                'targetAttribute' => ['income_item_id' => 'id'],
            ],
            [
                ['other_rs_id'], 'exist',
                'targetClass' => CheckingAccountant::className(),
                'targetAttribute' => ['other_rs_id' => 'id'],
                'filter' => ['company_id' => $this->company_id],
                'when' => function (CashOrderFlows $model) {
                    return !$model->is_internal_transfer && $model->contractor_id == CashContractorType::BANK_TEXT;
                },
            ],
            [
                ['other_rs_id'], 'required',
                'when' => function (CashOrderFlows $model) {
                    return !$model->is_internal_transfer && $model->contractor_id == CashContractorType::BANK_TEXT;
                },
                'message' => 'Необходимо заполнить.',
            ],
            [
                ['other_emoney_id'], 'exist',
                'targetClass' => Emoney::className(),
                'targetAttribute' => ['other_emoney_id' => 'id'],
                'filter' => ['company_id' => $this->company_id],
                'when' => function (CashOrderFlows $model) {
                    return !$model->is_internal_transfer && $model->contractor_id == CashContractorType::EMONEY_TEXT;
                },
            ],
            [
                ['other_emoney_id'], 'required',
                'when' => function (CashOrderFlows $model) {
                    return !$model->is_internal_transfer && $model->contractor_id == CashContractorType::EMONEY_TEXT;
                },
                'message' => 'Необходимо заполнить.',
            ],

            [['recognitionDateInput'], 'string'],
            [['recognitionDateInput'], 'date', 'format' => 'php:d.m.Y'],
            [['needBankContractorFlow', 'needEmoneyContractorFlow', 'needCashboxContractorFlow'], 'safe'],
            [['credit_id'], 'filter', 'filter' => function ($value) { return (int)$value ?: null; }],
            [['credit_id'], 'exist', 'skipOnError' => true, 'skipOnEmpty' => true, 'targetClass' => Credit::class, 'targetAttribute' => ['credit_id' => 'credit_id', 'company_id' => 'company_id']],
            [['project_id', 'sale_point_id', 'industry_id'], 'filter', 'filter' => function ($value) {
                return intval($value) ?: null;
            }],
            [['sale_point_id'], 'exist', 'targetClass' => SalePoint::class, 'targetAttribute' => 'id', 'filter' => ['company_id' => $this->company_id]],
            [['industry_id'], 'exist', 'targetClass' => CompanyIndustry::class, 'targetAttribute' => 'id'],
        ]);
    }


    /**
     * @param $attribute
     * @param $params
     */
    public function uniqueCompany($attribute, $params)
    {
        $paymentOrder = self::find()
            ->byCompany(Yii::$app->user->identity->company_id)
            ->byFlowType($this->flow_type)
            ->andWhere(['!=', self::tableName() . '.id', $this->id])
            ->andWhere([self::tableName() . '.number' => $this->$attribute])
            ->one();
        if ($paymentOrder !== null) {
            $this->addError($attribute, 'Такой номер уже существует, одинаковые номера не допустимы');
        }
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateReason($attribute, $params)
    {
        $reason = CashOrdersReasonsTypes::find()
            ->andWhere([
                'id' => $this->$attribute,
            ])
            ->andWhere(['OR',
                ['flow_type' => $this->flow_type],
                ['flow_type' => null],
            ])
            ->one();

        if ($reason === null) {
            $this->addError($attribute, '{attribute} не найдено.');
        }
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateContractor($attribute, $params)
    {
        if ($this->$attribute) {
            $contractor = Contractor::find()->andWhere([
                'id' => $this->contractor_id,
            ])->andWhere([
                'or',
                ['company_id' => null],
                ['company_id' => $this->company_id],
            ])->one();

            if ($contractor === null) {
                $type = CashContractorType::findOne(['name' => $this->contractor_id]);
                if ($type == null) {
                    $this->addError($attribute, 'Контрагент не найден..');
                } elseif ($type->name == CashContractorType::ORDER_TEXT) {
                    $query = $this->company->getCashboxes()
                        ->andWhere(['id' => $this->other_cashbox_id])
                        ->andWhere(['not', ['id' => $this->cashbox_id]]);
                    if (!$query->exists()) {
                        $this->addError($attribute, 'Контрагент не найден...');
                    }
                }
            } else {
                if (!$this->isNewRecord && $this->isAttributeChanged($attribute) && $contractor->type == Contractor::TYPE_FOUNDER) {
                    $this->addError($attribute, 'Редактирование котрагента-учредителя запрещено.');
                }
            }
        } else {
            $this->addError($attribute, 'Необходимо заполнить.');
        }
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'author_id' => 'Автор',
            'number' => 'Номер',
            //'date' => 'Дата КО', // Переименовываем согласно ТЗ
            'flow_type' => 'Тип ордера', // Переименовываем согласно ТЗ
            'is_accounting' => 'Учитывать в бухгалтерии',
            'reason_id' => 'Основание',
            'application' => 'Приложение',
            'other' => 'Значение "Прочее"',
            'income_item_id' => 'Статья прихода',
            'cashbox_id' => 'Касса',
            'other_cashbox_id' => 'Касса',
            'other_rs_id' => 'Счет',
            'other_emoney_id' => 'E-money',
            'is_prepaid_expense' => 'Авансовый платеж',
            'project_id' => 'ID проекта',
            'credit_id' => 'Кредитный договор',
            'credit_amount' => 'На сумму',
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReason()
    {
        return $this->hasOne(CashOrdersReasonsTypes::className(), ['id' => 'reason_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Employee::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashbox()
    {
        return $this->hasOne(Cashbox::className(), ['id' => 'cashbox_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfdReceipt()
    {
        return $this->hasOne(OfdReceipt::className(), ['uid' => 'ofd_receipt_uid']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOtherCashbox()
    {
        return $this->hasOne(Cashbox::className(), ['id' => 'other_cashbox_id']);
    }

    /**
     * @return string
     */
    public function getAmountRubString()
    {
        return RUtils::numeral()->getInWordsInt($this->amount / 100);
    }

    /**
     * @return string
     */
    public function getAmountKopeckString()
    {
        return RUtils::numeral()->getInWords(substr($this->amount, -2));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return ($this->flow_type == self::FLOW_TYPE_INCOME ? 'ПКО' : 'РКО') . ' №' . $this->number;
    }

    /**
     * @return null|string
     */
    public function getIncluding()
    {
        if ($this->hasInvoice()) {
            return null;
        }

        if ($this->invoice->total_amount_has_nds) {
            return 'НДС ' . $this->invoice->total_amount_nds . 'р';
        } else {
            return 'Без НДС';
        }
    }

    /**
     * @return array
     */
    public static function getContractors()
    {
        $contractorArray = [
            self::CONTRACTOR_ALL => 'Все',
        ];

        return array_merge($contractorArray, ArrayHelper::map(Yii::$app->user->identity->company->contractors, 'id', 'name'));
    }

    /**
     * Get formatted log message from Log model
     * @param Log $log
     * @return string
     */
    public function getLogMessage(Log $log)
    {
        return $this->getLogMessageBase($log, 'по кассе');
    }

    /**
     * @return CashOrderFlows
     */
    public function cloneSelf()
    {
        $model = clone $this;
        $model->isNewRecord = true;
        unset($model->id);
        $model->number = static::getNextNumber($this->company_id, $this->flow_type);
        $model->date = date('Y-m-d');
        $model->recognition_date = date('Y-m-d');
        $model->invoice_id = null;
        $model->internal_transfer_flow_id = null;
        $model->has_invoice = 0;

        return $model;
    }

    /**
     * @inheritdoc
     */
    public function getPdfFileName()
    {
        return $this->getPrintTitle() . '.pdf';
    }

    /**
     * @inheritdoc
     */
    public function getPrintTitle()
    {
        return str_replace([' ', '"'], ['_'],
            $this->name . '_' .
            date_format(date_create($this->date), 'd.m.Y') . '_' .
            $this->company->getTitle(true)
        );
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFlowInvoices()
    {
        return $this->hasMany(CashOrderFlowToInvoice::className(), ['flow_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpenditureItem()
    {
        return $this->hasOne(InvoiceExpenditureItem::className(), ['id' => 'expenditure_item_id']);
    }

    /**
     * @return $this
     */
    public function getEmployeeCompany()
    {
        return $this->hasOne(EmployeeCompany::className(), ['employee_id' => 'author_id'])
            ->onCondition(['company_id' => $this->company_id]);
    }

    /**
     * @param boolean $insert
     * @return boolean
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if (!$this->cashbox_id) {
            $this->cashbox_id = $this->company->getCashboxes()->andWhere(['is_main' => true])->select('id')->scalar();
        }

        if ($insert && $this->is_taxable === null) {
            if (in_array($this->income_item_id, TaxRobotHelper::$excludeItems) || $this->contractor_id === 'balance') {
                $this->is_taxable = 0;
            } else {
                $this->is_taxable = 1;
            }
        }

        return $this->cashbox_id ? true : false;
    }

    /**
     * @return string
     */
    public function getBillInvoices()
    {
        $invoices = [];
        foreach ($this->invoices as $invoice) {
            $invoices[] = Html::a('№' . $invoice->fullNumber, [
                '/documents/invoice/view',
                'id' => $invoice->id,
                'type' => $invoice->type,
            ]);
        }

        return !empty($invoices) ? implode(', ', $invoices) : '';
    }

    /**
     * @return string
     */
    public function getBillPaying($showClarifyLink = true)
    {
        if ($this->is_internal_transfer) {
            return '';
        }
        $invoicesList = $this->getBillInvoices();
        if ($showClarifyLink && $this->getAvailableAmount() > 0) {
            return ($invoicesList ? ($invoicesList . '<br />') : '') .
                Html::a('<span class="cash-bank-need-clarify">Уточнить</span>', [
                    '/cash/order/update',
                    'id' => $this->id,
                ], [
                    'class' => 'red-link',
                    'title' => 'Уточнить',
                    'data' => [
                        'toggle' => 'modal',
                        'target' => '#update-movement',
                        'title' => ($this->flow_type == self::FLOW_TYPE_INCOME) ? 'Приход' : 'Расход',
                        'pjax' => '0',
                    ],
                ]);
        }

        return $invoicesList;
    }

    /**
     * @param string $val
     */
    public function setContractorInput($value)
    {
        $val = explode('.', $value);
        $this->contractor_id = reset($val);
        if ($this->contractor_id == CashContractorType::ORDER_TEXT) {
            $this->other_cashbox_id = isset($val[1]) ? $val[1] : null;
        }
    }

    /**
     * @return string
     */
    public function getContractorInput()
    {
        return $this->contractor_id . ($this->other_cashbox_id ? '.' . $this->other_cashbox_id : null);
    }

    /**
     * @return  string
     */
    public function getContractorText()
    {
        return $this->contractor ? $this->contractor->nameWithType :
            ($this->otherCashbox ? $this->otherCashbox->name : $this->cashContractor->text);
    }

    /**
     * @param integer $type
     * @param boolean $copy
     * @return CashOrderFlows
     * @throws Exception
     */
    public function setNewCash($type, $copy = false)
    {
        $model = parent::setNewCash($type, $copy);

        if ($this->contractor_id == CashContractorType::ORDER_TEXT) {
            $model->cashbox_id = $this->other_cashbox_id;
            $model->other_cashbox_id = $this->cashbox_id;
            $model->is_accounting = $this->is_accounting;
        }

        if ($this->contractor_id == CashContractorType::EMONEY_TEXT) {
            $model->emoney_id = $this->other_emoney_id;
            $model->other_cashbox_id = $this->cashbox_id;
            $model->is_accounting = $this->is_accounting;
        }

        return $model;
    }

    /**
     * @param string $value
     */
    public function setRecognitionDateInput($value)
    {
        $this->recognition_date = is_string($value) ? implode('-', array_reverse(explode('.', $value))) : null;
    }

    /**
     * @return string
     */
    public function getRecognitionDateInput()
    {
        return $this->recognition_date ? implode('.', array_reverse(explode('-', $this->recognition_date))) : null;
    }

    /**
     * @param bool $copy
     * @return bool
     */
    public function checkContractor($copy = false)
    {
        return true;
    }

    /**
     * @param $value
     */
    public function setNeedBankContractorFlow($value) {
        $this->_needBankContractorFlow = !!$value;
    }
    public function getNeedBankContractorFlow() {
        return (bool)($this->cash_id ?: $this->_needBankContractorFlow);
    }

    /**
     * @param $value
     */
    public function setNeedEmoneyContractorFlow($value) {
        $this->_needEmoneyContractorFlow = !!$value;
    }
    public function getNeedEmoneyContractorFlow() {
        return (bool)($this->cash_id ?: $this->_needEmoneyContractorFlow);
    }

    /**
     * @param $value
     */
    public function setNeedCashboxContractorFlow($value) {
        $this->_needCashboxContractorFlow = !!$value;
    }
    public function getNeedCashboxContractorFlow() {
        return (bool)($this->cash_id ?: $this->_needCashboxContractorFlow);
    }

    public function isPlanDate()
    {
        if (!$this->date)
            return false;

        return (DateHelper::format($this->date, 'Y-m-d', 'd.m.Y') > date('Y-m-d'));
    }
}
