<?php

namespace common\models\cash;

use Yii;
use common\models\document\Invoice;

/**
 * This is the model class for table "cash_emoney_flow_to_invoice".
 *
 * @property integer $flow_id
 * @property integer $invoice_id
 * @property integer $amount
 *
 * @property Invoice $invoice
 * @property CashEmoneyFlows $flow
 */
class CashEmoneyFlowToInvoice extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cash_emoney_flow_to_invoice';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // [['flow_id', 'invoice_id', 'amount'], 'required'],
            // [['flow_id', 'invoice_id', 'amount'], 'integer'],
            // [['invoice_id'], 'exist', 'skipOnError' => true, 'targetClass' => Invoice::className(), 'targetAttribute' => ['invoice_id' => 'id']],
            // [['flow_id'], 'exist', 'skipOnError' => true, 'targetClass' => CashEmoneyFlows::className(), 'targetAttribute' => ['flow_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'flow_id' => 'Flow ID',
            'invoice_id' => 'Invoice ID',
            'amount' => 'Amount',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(Invoice::className(), ['id' => 'invoice_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFlow()
    {
        return $this->hasOne(CashEmoneyFlows::className(), ['id' => 'flow_id']);
    }
}
