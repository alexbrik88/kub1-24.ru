<?php

namespace common\models\cash;

interface WalletInterface
{
    /**
     * Получить название кошелька
     *
     * @return string
     */
    public function getWalletName(): string;
}
