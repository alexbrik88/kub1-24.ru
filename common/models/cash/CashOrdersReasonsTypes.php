<?php

namespace common\models\cash;

use Yii;

/**
 * @property integer $id
 * @property string $name
 * @property string $flow_type
 */
class CashOrdersReasonsTypes extends \yii\db\ActiveRecord
{
    /**
     * Выручка
     */
    const VALUE_PROCEEDS = 9;
    /**
     * Прочее
     */
    const VALUE_OTHER = 16;

    /**
     * @param $flowType
     * @return array|\yii\db\ActiveRecord[]
     * @example
     * [
     *      ...
     *      [`id` => '', `name` => ''],
     *      ...
     * ]
     */
    public static function getArray($flowType)
    {
        return CashOrdersReasonsTypes::find()
            ->distinct()
            ->select(['id', 'name'])
            ->andWhere(['OR',
                ['flow_type' => $flowType,],
                ['flow_type' => null]
            ])
            ->orderBy(['(IF([[flow_type]] IS NULL,1,0))' => SORT_ASC])
            ->asArray()
            ->all();
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'cash_orders_reasons_types';
    }
}
