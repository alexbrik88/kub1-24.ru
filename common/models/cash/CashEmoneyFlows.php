<?php

namespace common\models\cash;

use common\components\cash\InternalTransferFlowInterface;
use common\components\cash\InternalTransferHelper;
use common\components\helpers\ModelHelper;
use common\models\company\CompanyIndustry;
use common\models\companyStructure\SalePoint;
use common\models\Contractor;
use common\models\cash\query\CashFlowBase;
use common\models\company\CheckingAccountant;
use common\models\currency\Currency;
use common\models\document\Invoice;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use frontend\components\StatisticPeriod;
use frontend\models\log\Log;
use frontend\modules\analytics\behaviors\credits\CreditFlowBehavior;
use frontend\modules\analytics\models\credits\Credit;
use frontend\modules\cash\models\CashContractorType;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\helpers\Html;

/**
 * @author ErickSkrauch <erickskrauch@yandex.ru>
 *
 * @property integer $emoney_id
 * @property integer $is_accounting
 * @property integer $income_item_id
 * @property CashEmoneyFlowToInvoice[] $flowInvoices
 * @property InvoiceExpenditureItem $expenditureItem
 * @property InvoiceIncomeItem $incomeItem
 * @property string $contractorInput
 * @property string $recognition_date
 * @property string $number
 * @property integer $is_prepaid_expense
 * @property string $date_time
 *
 * @property int $project_id
 * @property int $sale_point_id
 * @property integer $industry_id
 *
 * @property integer $moneta_id
 * @property-read string[] $creditList
 * @property string $billPaying
 */
class CashEmoneyFlows extends CashFlowsBase implements InternalTransferFlowInterface
{
    /**
     * @var int|null
     */
    public $credit_id;

    /**
     * @var float
     */
    public $credit_amount = 0;

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'cash_emoney_flows';
    }

    public function getCashContractorTypeText() : string
    {
        return \frontend\modules\cash\models\CashContractorType::EMONEY_TEXT;
    }

    public function getInternalTransferAccountClass() : ?string
    {
        return InternalTransferHelper::accountClassByTable($this->internal_transfer_account_table);
    }

    public function getInternalTransferFlowClass() : ?string
    {
        return InternalTransferHelper::flowClassByTable($this->internal_transfer_flow_table);
    }

    public function getSelfAccount() : ActiveQuery
    {
        return $this->getEmoney();
    }

    public function getInternalTransferAccount() : ?ActiveQuery
    {
        if ($class = $this->getInternalTransferAccountClass()) {
            return $this->hasOne($class, ['id' => 'internal_transfer_account_id']);
        }

        return null;
    }

    public function getInternalTransferFlow() : ?ActiveQuery
    {
        if ($class = $this->getInternalTransferFlowClass()) {
            return $this->hasOne($class, ['id' => 'internal_transfer_flow_id']);
        }

        return null;
    }

    /**
     * @var bool
     */
    private $_needBankContractorFlow;
    private $_needCashboxContractorFlow;
    private $_needEmoneyContractorFlow;

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['creditFlow'] = [
            'class' => CreditFlowBehavior::class,
            'attribute' => 'cash_emoney_flow_id',
            'wallet_id' => CashFlowsBase::WALLET_EMONEY,
        ];

        return $behaviors;
    }

    /**
     * @return array
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [
                ['contractor_id'], 'validateContractor', 'skipOnEmpty' => false,
                'when' => function ($model) {
                    return !$model->is_internal_transfer;
                },
            ],
            [['emoney_id', 'date'], 'required'],
            [
                ['description'], 'required',
                'when' => function ($model) {
                    return !$model->is_internal_transfer;
                },
            ],
            [['emoney_id', 'is_accounting', 'has_invoice', 'moneta_id'], 'integer'],
            [['is_accounting'], 'safe'],
            [['expenditure_item_id'], 'required', 'when' => function (CashEmoneyFlows $model) {
                return !$model->hasErrors('flow_type') && $model->flow_type == CashFlowsBase::FLOW_TYPE_EXPENSE;
            }, 'whenClient' => '
                function(attribute, value) {
                    return $("[name=\'' . Html::getInputName($this, 'flow_type') . '\']:checked").val() == ' . CashFlowsBase::FLOW_TYPE_EXPENSE . ';
                }
            '],
            [['income_item_id'], 'required', 'when' => function (CashEmoneyFlows $model) {
                return !$model->hasErrors('flow_type') && $model->flow_type == CashFlowsBase::FLOW_TYPE_INCOME;
            }, 'whenClient' => '
                function(attribute, value) {
                    return $("[name=\'' . Html::getInputName($this, 'flow_type') . '\']:checked").val() == ' . CashFlowsBase::FLOW_TYPE_INCOME . ';
                }
            '],
            [
                ['emoney_id'], 'exist',
                'skipOnError' => true,
                'targetClass' => Emoney::className(),
                'targetAttribute' => 'id',
                'filter' => [
                    'company_id' => $this->company_id,
                    'currency_id' => Currency::DEFAULT_ID,
                ],
            ],
            [['income_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => InvoiceIncomeItem::className(), 'targetAttribute' => ['income_item_id' => 'id']],
            [['recognitionDateInput'], 'string'],
            [['recognitionDateInput'], 'date', 'format' => 'php:d.m.Y'],
            [['number'], 'filter', 'filter' => function ($value) {
                return (string)$value;
            }],
            [['number'], 'string', 'max' => 255],
            [
                ['number'], 'unique', 'filter' => function (CashFlowBase $query) {
                    return $query->byCompany($this->company_id)
                        ->byFlowType($this->flow_type);
                },
                'when' => function ($model) {
                    return !$model->is_internal_transfer;
                },
            ],
            [['is_prepaid_expense'], 'safe'],
            [['is_prepaid_expense'], 'integer'],
            [
                ['date_time'], 'date', 'format' => 'php:H:i:s',
                'when' => function ($model) {
                    return !$model->is_internal_transfer && strlen($model->date_time) < 10;
                },
            ],
            [
                ['date_time'], 'date', 'format' => 'php:Y-m-d H:i:s',
                'when' => function ($model) {
                    return !$model->is_internal_transfer && strlen($model->date_time) >= 10;
                },
            ],
            [
                ['other_rs_id'], 'exist',
                'targetClass' => CheckingAccountant::className(),
                'targetAttribute' => ['other_rs_id' => 'id'],
                'filter' => ['company_id' => $this->company_id],
                'when' => function (CashEmoneyFlows $model) {
                    return !$model->is_internal_transfer && $model->contractor_id == CashContractorType::BANK_TEXT;
                },
            ],
            [
                ['other_rs_id'], 'required',
                'when' => function (CashEmoneyFlows $model) {
                    return !$model->is_internal_transfer && $model->contractor_id == CashContractorType::BANK_TEXT;
                },
                'message' => 'Необходимо заполнить.',
            ],
            [
                ['other_cashbox_id'], 'exist',
                'targetClass' => Cashbox::className(),
                'targetAttribute' => ['other_cashbox_id' => 'id'],
                'filter' => ['company_id' => $this->company_id],
                'when' => function (CashEmoneyFlows $model) {
                    return !$model->is_internal_transfer && $model->contractor_id == CashContractorType::ORDER_TEXT;
                },
            ],
            [
                ['other_cashbox_id'], 'required',
                'when' => function (CashEmoneyFlows $model) {
                    return !$model->is_internal_transfer && $model->contractor_id == CashContractorType::ORDER_TEXT;
                },
                'message' => 'Необходимо заполнить.',
            ],
            [['needBankContractorFlow', 'needEmoneyContractorFlow', 'needCashboxContractorFlow'], 'safe'],
            [['credit_id'], 'filter', 'filter' => function ($value) { return (int)$value ?: null; }],
            [['credit_id'], 'exist', 'skipOnError' => true, 'skipOnEmpty' => true, 'targetClass' => Credit::class, 'targetAttribute' => ['credit_id' => 'credit_id', 'company_id' => 'company_id']],
            [['project_id', 'sale_point_id', 'industry_id'], 'filter', 'filter' => function ($value) {
                return intval($value) ?: null;
            }],
            [['sale_point_id'], 'exist', 'targetClass' => SalePoint::class, 'targetAttribute' => 'id', 'filter' => ['company_id' => $this->company_id]],
            [['industry_id'], 'exist', 'targetClass' => CompanyIndustry::class, 'targetAttribute' => 'id'],
        ]);
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'emoney_id' => 'E-money',
            'date' => 'Дата',
            'date_time' => 'Время',
            'is_accounting' => 'Учитывать в бухгалтерии',
            'income_item_id' => 'Статья прихода',
            'number' => 'Номер',
            'recognition_date' => 'Дата признания',
            'is_prepaid_expense' => 'Авансовый платеж',
            'other_cashbox_id' => 'Касса',
            'other_rs_id' => 'Счет',
            'other_emoney_id' => 'E-money',
            'credit_id' => 'Кредитный договор',
            'credit_amount' => 'На сумму',
        ]);
    }

    public function afterFind()
    {
        parent::afterFind();

        if ($this->is_prepaid_expense)
            $this->recognition_date = null;
    }

    /**
     * @param boolean $insert
     * @return boolean
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        ModelHelper::HtmlEntitiesModelAttributes($this);

        if (!$this->emoney_id) {
            $this->emoney_id = $this->company->emoney->id;
        }
        $date = date_create($this->date);
        $date_time = date_create($this->date_time);
        $date_time->setDate(intval($date->format('Y')), intval($date->format('m')), intval($date->format('d')));

        $this->date_time = $date_time->format('Y-m-d H:i:s');

        return true;
    }

    /**
     * Get formatted log message from Log model
     * @param Log $log
     * @return string
     */
    public function getLogMessage(Log $log)
    {
        return $this->getLogMessageBase($log, 'по e-money');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFlowInvoices()
    {
        return $this->hasMany(CashEmoneyFlowToInvoice::className(), ['flow_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmoney()
    {
        return $this->hasOne(Emoney::className(), ['id' => 'emoney_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpenditureItem()
    {
        return $this->hasOne(InvoiceExpenditureItem::className(), ['id' => 'expenditure_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIncomeItem()
    {
        return $this->hasOne(InvoiceIncomeItem::className(), ['id' => 'expenditure_item_id']);
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateContractor($attribute, $params)
    {
        if ($this->$attribute) {
            $contractor = Contractor::find()->andWhere([
                'id' => $this->$attribute,
            ])->andWhere([
                'or',
                ['company_id' => null],
                ['company_id' => $this->company_id],
            ])->one();

            if ($contractor === null) {
                $type = CashContractorType::findOne(['name' => $this->contractor_id]);
                if ($type == null) {
                    $this->addError($attribute, 'Контрагент не найден..');
                }
            } else {
                if (!$this->isNewRecord && $this->isAttributeChanged($attribute) && $contractor->type == Contractor::TYPE_FOUNDER) {
                    $this->addError($attribute, 'Редактирование котрагента-учредителя запрещено.');
                }
            }
        } else {
            $this->addError($attribute, 'Необходимо заполнить.');
        }
    }

    /**
     * @param string $value
     */
    public function setRecognitionDateInput($value)
    {
        $this->recognition_date = is_string($value) ? implode('-', array_reverse(explode('.', $value))) : null;
    }

    /**
     * @return string
     */
    public function getRecognitionDateInput()
    {
        return $this->recognition_date ? implode('.', array_reverse(explode('-', $this->recognition_date))) : null;
    }

    /**
     * Returns next flow number.
     * If passed existed model with same type - return it's number.
     * @param int $flowType
     * @param CashOrderFlows $model
     * @return int
     */
    public static function getNextNumber($flowType, CashOrderFlows $model = null, $company_id = null)
    {
        if ($model !== null && !$model->isNewRecord && $model->flow_type == $flowType) {
            return $model->number;
        }

        return 1 + self::find()
                ->byCompany($company_id ? $company_id : Yii::$app->user->identity->company->id)
                ->byFlowType($flowType)
                ->max('number * 1');
    }


    /**
     * @param bool $copy
     * @return bool
     * @throws \yii\base\Exception
     */
    public function checkContractor($copy = false)
    {
        return true;
    }

    /**
     * @param $value
     */
    public function setNeedBankContractorFlow($value) {
        $this->_needBankContractorFlow = !!$value;
    }
    public function getNeedBankContractorFlow() {
        return (bool)($this->cash_id ?: $this->_needBankContractorFlow);
    }

    /**
     * @param $value
     */
    public function setNeedEmoneyContractorFlow($value) {
        $this->_needEmoneyContractorFlow = !!$value;
    }
    public function getNeedEmoneyContractorFlow() {
        return (bool)($this->cash_id ?: $this->_needEmoneyContractorFlow);
    }

    /**
     * @param $value
     */
    public function setNeedCashboxContractorFlow($value) {
        $this->_needCashboxContractorFlow = !!$value;
    }
    public function getNeedCashboxContractorFlow() {
        return (bool)($this->cash_id ?: $this->_needCashboxContractorFlow);
    }

    /**
     * @return string
     */
    public function getBillPaying($showClarifyLink = true)
    {
        if ($this->is_internal_transfer) {
            return '';
        }
        $invoices = [];
        foreach ($this->invoices as $invoice) {
            $invoices[] = Html::a('№' . $invoice->fullNumber, [
                '/documents/invoice/view',
                'id' => $invoice->id,
                'type' => $invoice->type,
            ]);
        }

        $invoicesList = !empty($invoices) ? implode(', ', $invoices) : '';

        $options = ['title' => 'Уточнить', 'data' => ['toggle' => 'modal', 'target' => '#update-movement'], 'class' => 'red-link'];
        if ($showClarifyLink && $this->getAvailableAmount() > 0) {
            return ($invoicesList ? ($invoicesList . '<br />') : '') .
                Html::a('<span class="cash-bank-need-clarify">Уточнить</span>', '/cash/e-money/update?id=' . $this->id, $options);
        } else {
            return $invoicesList;
        }
    }

    /**
     * @return CashOrderFlows
     */
    public function cloneSelf()
    {
        $model = clone $this;
        $model->isNewRecord = true;
        unset($model->id);
        $model->number = static::getNextNumber($this->flow_type , null, $this->company_id);
        $model->date = date('Y-m-d');
        $model->recognition_date = date('Y-m-d');
        $model->invoice_id = null;
        $model->internal_transfer_flow_id = null;
        $model->has_invoice = 0;

        return $model;
    }
}
