<?php namespace common\models\cash\excel;

use common\components\helpers\ArrayHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashBankForeignCurrencyFlows;
use common\models\cash\Cashbox;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashEmoneyForeignCurrencyFlows;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\cash\CashOrderForeignCurrencyFlows;
use common\models\cash\Emoney;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\Contractor;
use common\components\excel\Excel;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\employee\Employee;
use common\models\project\Project;
use common\components\date\DateHelper;
use common\models\company\CompanyIndustry;
use common\models\companyStructure\SalePoint;
use common\modules\acquiring\models\Acquiring;
use common\modules\acquiring\models\AcquiringOperation;
use common\modules\cards\models\CardAccount;
use common\modules\cards\models\CardOperation;
use frontend\modules\analytics\models\PlanCashFlows;
use frontend\modules\analytics\models\PlanCashForeignCurrencyFlows;
use frontend\modules\cash\models\CashContractorType;
use yii\db\Expression;

class CashOperationsXlsHelper extends CashXlsHelper {

    CONST TITLE = 'Операции';

    protected static array $_walletName = [
        CashFlowsBase::WALLET_BANK => [],
        CashFlowsBase::WALLET_CASHBOX => [],
        CashFlowsBase::WALLET_EMONEY => [],
        CashFlowsBase::WALLET_CARD => [],
        CashFlowsBase::WALLET_ACQUIRING => [],
        CashFlowsBase::WALLET_FOREIGN_BANK => [],
        CashFlowsBase::WALLET_FOREIGN_CASHBOX => [],
        CashFlowsBase::WALLET_FOREIGN_EMONEY => [],
    ];

    protected static array $_id2Rs = [];

    public static function init(Employee $employee): void
    {
        self::_prepareWalletName($employee);
        self::_prepareHiddenColumns($employee);
    }

    public static function generateXls($period, $flows)
    {
        $title = self::TITLE;
        $fileName = self::_getFileName($title, $period);

        $headers = [
            'date' => 'Дата',
            'recognition_date' => 'Дата признания',
            'amount' => 'Сумма',
            'wallet_name' => 'Счета',
            'contractor_id' => 'Контрагент',
            'project_id' => 'Проект',
            'sale_point_id' => 'Точка продаж',
            'industry_id' => 'Направление',
            'description' => 'Назначение',
            'billPaying' => 'Опл. счета',
            'reason_id' => 'Статья',
        ];

        $columns = [
            [
                'attribute' => 'date',
                'value' => function ($flow) {
                    return ($flow['date'])
                        ? DateHelper::format($flow['date'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE)
                        : '';
                },
            ],
            [
                'attribute' => 'recognition_date',
                'value' => function ($flow) {
                    return ($flow['recognition_date'])
                        ? DateHelper::format($flow['recognition_date'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE)
                        : '';
                },
            ],
            [
                'attribute' => 'amount',
                'styleArray' => ['numberformat' => ['code' => '# ##0.00']],
                'dataType' => 'n',
                'value' => function ($flow) {
                    return bcdiv(($flow['flow_type'] ? 1:-1) * $flow['amount'], 100, 2);
                },
            ],
            [
                'attribute' => 'wallet_name',
                'dataType' => 's',
                'value' => function ($flow) {

                    switch ($flow['tb']) {
                        // RUB
                        case CashBankFlows::tableName():
                            $model = CashBankFlows::findOne($flow['id']);
                            $name = ArrayHelper::getValue(self::$_walletName[CashFlowsBase::WALLET_BANK], $flow['rs'])
                                . ($flow['rs'] ? " ({$flow['rs']})" : '');
                            break;
                        case CashOrderFlows::tableName():
                            $model = CashOrderFlows::findOne($flow['id']);
                            $name = ArrayHelper::getValue(self::$_walletName[CashFlowsBase::WALLET_CASHBOX], $flow['cashbox_id']);
                            break;
                        case CashEmoneyFlows::tableName():
                            $model = CashEmoneyFlows::findOne($flow['id']);
                            $name = ArrayHelper::getValue(self::$_walletName[CashFlowsBase::WALLET_EMONEY], $flow['emoney_id']);
                            break;
                        case PlanCashFlows::tableName():
                            $model = PlanCashFlows::findOne($flow['id']);
                            switch ($model->payment_type) {
                                case CashFlowsBase::WALLET_BANK:
                                    $rs = self::$_id2Rs[$flow['rs']] ?? null;
                                    $name = ArrayHelper::getValue(self::$_walletName[CashFlowsBase::WALLET_BANK], $rs)
                                        . ($rs ? " ({$rs})" : '');
                                    break;
                                case CashFlowsBase::WALLET_CASHBOX:
                                    $name = ArrayHelper::getValue(self::$_walletName[CashFlowsBase::WALLET_CASHBOX], $flow['cashbox_id']);
                                    break;
                                case CashFlowsBase::WALLET_EMONEY:
                                    $name = ArrayHelper::getValue(self::$_walletName[CashFlowsBase::WALLET_EMONEY], $flow['emoney_id']);
                                    break;
                                default:
                                    $name = '';
                            }
                            break;
                        // RUB ACQUIRING
                        case AcquiringOperation::tableName():
                            $model = AcquiringOperation::findOne($flow['id']);
                            $name = ArrayHelper::getValue(self::$_walletName[CashFlowsBase::WALLET_ACQUIRING], $flow['acquiring_id']);
                            break;
                        // RUB CARD
                        case CardOperation::tableName():
                            $model = CardOperation::findOne($flow['id']);
                            $name = ArrayHelper::getValue(self::$_walletName[CashFlowsBase::WALLET_CARD], $flow['card_id']);
                            break;

                        // FOREIGN
                        case CashBankForeignCurrencyFlows::tableName():
                            $model = CashBankForeignCurrencyFlows::findOne($flow['id']);
                            $name = $model->currency->name .' '. ArrayHelper::getValue(self::$_walletName[CashFlowsBase::WALLET_FOREIGN_BANK], $flow['rs'])
                                . ($flow['rs'] ? " ({$flow['rs']})" : '');
                            break;
                        case CashOrderForeignCurrencyFlows::tableName():
                            $model = CashOrderForeignCurrencyFlows::findOne($flow['id']);
                            $name = $model->currency->name .' '. ArrayHelper::getValue(self::$_walletName[CashFlowsBase::WALLET_FOREIGN_CASHBOX], $flow['cashbox_id']);
                            break;
                        case CashEmoneyForeignCurrencyFlows::tableName():
                            $model = CashEmoneyForeignCurrencyFlows::findOne($flow['id']);
                            $name = $model->currency->name .' '. ArrayHelper::getValue(self::$_walletName[CashFlowsBase::WALLET_FOREIGN_EMONEY], $flow['emoney_id']);
                            break;
                        case PlanCashForeignCurrencyFlows::tableName():
                            $model = PlanCashForeignCurrencyFlows::findOne($flow['id']);
                            switch ($model->payment_type) {
                                case CashFlowsBase::WALLET_FOREIGN_BANK:
                                    $rs = self::$_id2Rs[$flow['rs']] ?? null;
                                    $name = $model->currency->name .' '. ArrayHelper::getValue(self::$_walletName[CashFlowsBase::WALLET_FOREIGN_BANK], $rs)
                                        . ($rs ? " ({$rs})" : '');
                                    break;
                                case CashFlowsBase::WALLET_FOREIGN_CASHBOX:
                                    $name = $model->currency->name .' '. ArrayHelper::getValue(self::$_walletName[CashFlowsBase::WALLET_FOREIGN_CASHBOX], $flow['cashbox_id']);
                                    break;
                                case CashFlowsBase::WALLET_FOREIGN_EMONEY:
                                    $name = $model->currency->name .' '. ArrayHelper::getValue(self::$_walletName[CashFlowsBase::WALLET_FOREIGN_EMONEY], $flow['emoney_id']);
                                    break;
                                default:
                                    $name = '';
                            }
                            break;

                        default:
                            return '';
                    }

                    return $name;
                },
            ],
            [
                'attribute' => 'contractor_id',
                'value' => function ($flow) {
                    if ($flow['is_internal_transfer'])
                        return '';
                    if ($flow['contractor_id'] > 0 && ($model = Contractor::findOne($flow['contractor_id'])) !== null) {
                        return $model->nameWithType;
                    }
                    return CashContractorType::getNameById($flow['contractor_id']);
                },
            ],
            [
                'attribute' => 'project_id',
                'dataType' => 's',
                'value' => function ($flow) {
                    return ($flow['project_id'] && ($model = Project::findOne(['id' => $flow['project_id']])))
                        ? $model->name
                        : '';
                }
            ],
            [
                'attribute' => 'sale_point_id',
                'dataType' => 's',
                'value' => function ($flow) {
                    return ($flow['sale_point_id'] && ($model = SalePoint::findOne(['id' => $flow['sale_point_id']])))
                        ? $model->name
                        : '';
                }
            ],
            [
                'attribute' => 'industry_id',
                'dataType' => 's',
                'value' => function ($flow) {
                    return ($flow['industry_id'] && ($model = CompanyIndustry::findOne(['id' => $flow['industry_id']])))
                        ? $model->name
                        : '';
                }
            ],
            [
                'attribute' => 'description',
                'dataType' => 's',
                'value' => function ($flow) {
                    return $flow['description'];
                }
            ],
            [
                'attribute' => 'billPaying',
                'dataType' => 's',
                'value' => function ($flow) {
                    switch ($flow['tb']) {
                        case PlanCashFlows::tableName():
                            if ($model = PlanCashFlows::findOne($flow['id'])) {
                                $currDate = date('Y-m-d');
                                if ($model->first_date < $currDate && $model->date != $model->first_date)
                                    return 'Перенос';
                                elseif ($model->date >= $currDate)
                                    return 'План';
                                else
                                    return 'Просрочен';
                            }
                            break;
                        case CashBankFlows::tableName():
                            $model = CashBankFlows::findOne($flow['id']);
                            break;
                        case CashOrderFlows::tableName():
                            $model = CashOrderFlows::findOne($flow['id']);
                            break;
                        case CashEmoneyFlows::tableName():
                            $model = CashEmoneyFlows::findOne($flow['id']);
                            break;
                        case CashBankForeignCurrencyFlows::tableName():
                            // no invoices
                            break;
                    }

                    if (!empty($model)) {
                        $link = ($model->credit_id) ? $model->creditPaying : $model->billPaying;
                        $text = strip_tags($link);
                        return ($text !== 'Уточнить') ? $text : '';
                    }

                    return '';
                }
            ],
            [
                'attribute' => 'reason_id',
                'dataType' => 's',
                'value' => function ($flow) {
                    if ($flow['flow_type'] == CashFlowsBase::FLOW_TYPE_INCOME) {
                        $reason = (($item = InvoiceIncomeItem::findOne($flow['income_item_id']))
                            ? $item->fullName
                            : "id={$flow['income_item_id']}");
                    } else {
                        $reason = (($item = InvoiceExpenditureItem::findOne($flow['expenditure_item_id']))
                            ? $item->fullName
                            : "id={$flow['expenditure_item_id']}");
                    }

                    return $reason ?: '-';
                }
            ],
        ];

        $excel = new Excel();
        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $flows,
            'title' => str_replace($title, '_', ' '),
            'headers' => self::_hideSomeHeaders($headers),
            'columns' => self::_hideSomeColumns($columns),
            'format' => 'Xlsx',
            'fileName' => $fileName,
        ]);
    }

    private static function _prepareWalletName(Employee $employee): void
    {
        $company = $employee->currentEmployeeCompany->company;
        self::$_walletName[CashFlowsBase::WALLET_BANK] = $company->getRubleAccounts()->select('name')->indexBy('rs')->column();
        self::$_walletName[CashFlowsBase::WALLET_CASHBOX] = $company->getRubleCashboxes()->select('name')->indexBy('id')->column();
        self::$_walletName[CashFlowsBase::WALLET_EMONEY] = $company->getRubleEmoneys()->select('name')->indexBy('id')->column();;
        self::$_walletName[CashFlowsBase::WALLET_ACQUIRING] = Acquiring::find()->where(['company_id' => $company->id])
            ->select(new Expression('CONCAT(CASE WHEN type=0 THEN "Монета" WHEN type=1 THEN "ЮKassa" END, " (",identifier ,")")'))->indexBy('identifier')->column();
        self::$_walletName[CashFlowsBase::WALLET_CARD] = CardAccount::find()->where(['company_id' => $company->id])
            ->select(new Expression('CONCAT("Дзен мани", " (", identifier, ")")'))->indexBy('id')->column();
        self::$_walletName[CashFlowsBase::WALLET_FOREIGN_BANK] = $company->getForeignCurrencyAccounts()->select('name')->indexBy('rs')->column();
        self::$_walletName[CashFlowsBase::WALLET_FOREIGN_CASHBOX] = $company->getForeignCurrencyCashboxes()->select('name')->indexBy('id')->column();
        self::$_walletName[CashFlowsBase::WALLET_FOREIGN_EMONEY] = $company->getForeignCurrencyEmoneys()->select('name')->indexBy('id')->column();;
        self::$_id2Rs = $company->getCheckingAccountants()->select('rs')->indexBy('id')->column();
    }

    private static function _prepareHiddenColumns(Employee $employee): void
    {
        $config2column = [
            'cash_column_recognition_date' => 'recognition_date',
            'cash_column_paying' => 'billPaying',
            'cash_column_project' => 'project_id',
            'cash_column_sale_point' => 'sale_point_id',
            'cash_column_industry' => 'industry_id',
        ];

        foreach ($config2column as $config => $column)
            if (empty($employee->config->{$config}))
                self::$_hiddenColumn[$column] = true;
    }
}