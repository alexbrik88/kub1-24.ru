<?php namespace common\models\cash\excel;

use common\components\date\DateHelper;
use common\components\excel\Excel;
use yii\base\Exception;

class CashXlsHelper {

    const BANK = 'bank';
    const ORDER = 'order';
    const OPERATIONS = 'operations';

    protected static array $_walletName = [];
    protected static array $_hiddenColumn = [];

    protected static function _getFileName(string $title, array $period): string
    {
        return str_replace(' ', '_', $title)
            . '_' . DateHelper::format($period['from'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE)
            . '-' . DateHelper::format($period['to'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
    }

    protected static function _hideSomeHeaders(array $headers): array
    {
        foreach ($headers as $attr => $name)
            if (isset(self::$_hiddenColumn[$attr]))
                unset($headers[$attr]);

        return $headers;
    }

    protected static function _hideSomeColumns(array $columns): array
    {
        foreach ($columns as $key => $col) {
            $attr = $col['attribute'];
            if (isset(self::$_hiddenColumn[$attr]))
                unset($columns[$key]);
        }

        return $columns;
    }
}