<?php namespace common\models\cash\excel;

use common\models\cash\CashOrderFlows;
use common\models\cash\CashOrderForeignCurrencyFlows;
use common\models\cash\CashFlowsBase;
use common\models\Company;
use common\models\Contractor;
use common\components\excel\Excel;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\project\Project;
use common\components\date\DateHelper;
use common\models\company\CompanyIndustry;
use common\models\companyStructure\SalePoint;
use frontend\modules\analytics\models\PlanCashFlows;
use frontend\modules\cash\models\CashContractorType;
use common\models\employee\Employee;

class CashOrderXlsHelper extends CashXlsHelper {

    CONST TITLE = 'Операции по банку';
    
    public static function init(Employee $employee): void
    {
        self::_prepareWalletNames($employee);
        self::_prepareHiddenColumns($employee);
    }

    public static function generateXls($period, $flows)
    {
        $title = self::TITLE;
        $fileName = self::_getFileName($title, $period);

        $headers = [
            'date' => 'Дата',
            'amount' => 'Сумма',
            'cashbox_name' => 'Касса',
            'contractor_id' => 'Контрагент',
            'project_id' => 'Проект',
            'sale_point_id' => 'Точка продаж',
            'industry_id' => 'Направление',
            'description' => 'Назначение',
            'billPaying' => 'Опл. счета',
            'reason_id' => 'Статья',
            'number' => 'Ордер'
        ];

        $columns = [
            [
                'attribute' => 'date',
                'value' => function ($flow) {
                    return ($flow['date'])
                        ? DateHelper::format($flow['date'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE)
                        : '';
                },
            ],
            [
                'attribute' => 'amount',
                'styleArray' => ['numberformat' => ['code' => '# ##0.00']],
                'dataType' => 'n',
                'value' => function ($flow) {
                    return bcdiv(($flow['flow_type'] ? 1:-1) * $flow['amount'], 100, 2);
                },
            ],
            [
                'attribute' => 'cashbox_name',
                'dataType' => 's',
                'value' => function ($flow) {
                    return self::$_walletName[$flow['cashbox_id']] ?? '---';
                },
            ],
            [
                'attribute' => 'contractor_id',
                'value' => function ($flow) {
                    if ($flow['is_internal_transfer'])
                        return '';
                    if ($flow['contractor_id'] > 0 && ($model = Contractor::findOne($flow['contractor_id'])) !== null) {
                        return $model->nameWithType;
                    }
                    return CashContractorType::getNameById($flow['contractor_id']);
                },
            ],
            [
                'attribute' => 'project_id',
                'dataType' => 's',
                'value' => function ($flow) {
                    return ($flow['project_id'] && ($model = Project::findOne(['id' => $flow['project_id']])))
                        ? $model->name
                        : '';
                }
            ],
            [
                'attribute' => 'sale_point_id',
                'dataType' => 's',
                'value' => function ($flow) {
                    return ($flow['sale_point_id'] && ($model = SalePoint::findOne(['id' => $flow['sale_point_id']])))
                        ? $model->name
                        : '';
                }
            ],
            [
                'attribute' => 'industry_id',
                'dataType' => 's',
                'value' => function ($flow) {
                    return ($flow['industry_id'] && ($model = CompanyIndustry::findOne(['id' => $flow['industry_id']])))
                        ? $model->name
                        : '';
                }
            ],
            [
                'attribute' => 'description',
                'dataType' => 's',
                'value' => function ($flow) {
                    return $flow['description'];
                }
            ],
            [
                'attribute' => 'billPaying',
                'dataType' => 's',
                'value' => function ($flow) {
                    switch ($flow['tb']) {
                        case PlanCashFlows::tableName():
                            if ($model = PlanCashFlows::findOne($flow['id'])) {
                                $currDate = date('Y-m-d');
                                if ($model->first_date < $currDate && $model->date != $model->first_date)
                                    return 'Перенос';
                                elseif ($model->date >= $currDate)
                                    return 'План';
                                else
                                    return 'Просрочен';
                            }
                            break;
                        case CashOrderFlows::tableName():
                            if ($model = CashOrderFlows::findOne($flow['id'])) {
                                $link = ($model->credit_id) ? $model->creditPaying : $model->billPaying;
                                $text = strip_tags($link);
                                return ($text !== 'Уточнить') ? $text : '';
                            }
                            break;
                        case CashOrderForeignCurrencyFlows::tableName():
                            // no invoices
                            break;
                    }

                    return '';
                }
            ],
            [
                'attribute' => 'reason_id',
                'dataType' => 's',
                'value' => function ($flow) {
                    if ($flow['flow_type'] == CashFlowsBase::FLOW_TYPE_INCOME) {
                        $reason = (($item = InvoiceIncomeItem::findOne($flow['income_item_id']))
                            ? $item->fullName
                            : "id={$flow['income_item_id']}");
                    } else {
                        $reason = (($item = InvoiceExpenditureItem::findOne($flow['expenditure_item_id']))
                            ? $item->fullName
                            : "id={$flow['expenditure_item_id']}");
                    }

                    return $reason ?: '-';
                }
            ],
            [
                'attribute' => 'number',
                'dataType' => 's',
                'value' => function ($flow) {
                    if ($flow['tb'] == PlanCashFlows::tableName())
                        return '';

                    return ($flow['flow_type'] == CashFlowsBase::FLOW_TYPE_INCOME ? 'ПКО' : 'РКО') . ' №' . $flow['number'];
                },
            ]
        ];

        $excel = new Excel();
        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $flows,
            'title' => str_replace($title, '_', ' '),
            'headers' => self::_hideSomeHeaders($headers),
            'columns' => self::_hideSomeColumns($columns),
            'format' => 'Xlsx',
            'fileName' => $fileName,
        ]);
    }

    private static function _prepareWalletNames(Employee $employee): void
    {
        self::$_walletName = $employee->currentEmployeeCompany->company
            ->getCashboxes()
            ->select('name')
            ->indexBy('id')
            ->column();
    }

    private static function _prepareHiddenColumns(Employee $employee): void
    {
        $config2column = [
            'order_column_recognition_date' => 'recognition_date',
            'order_column_paying' => 'billPaying',
            'order_column_project' => 'project_id',
            'order_column_sale_point' => 'sale_point_id',
            'order_column_industry' => 'industry_id',
        ];

        foreach ($config2column as $config => $column)
            if (empty($employee->config->{$config}))
                self::$_hiddenColumn[$column] = true;
    }
}