<?php

namespace common\models\cash;


use common\models\service\PaymentType;
use frontend\models\Documents;
use yii\base\Exception;
use yii\web\NotFoundHttpException;

/**
 * Class CashFactory
 * @package common\models\cash
 */
class CashFactory
{
    /**
     * Банк
     */
    const TYPE_BANK = 1;
    /**
     * Кассовый ордер
     */
    const TYPE_ORDER = 2;
    /**
     * E-money
     */
    const TYPE_EMONEY = 3;

    const TYPE_FOREIGN_BANK = 11;
    const TYPE_FOREIGN_ORDER = 12;
    const TYPE_FOREIGN_EMONEY = 13;

    public static $documentToFlowType = [
        Documents::IO_TYPE_OUT => CashFlowsBase::FLOW_TYPE_INCOME,
        Documents::IO_TYPE_IN => CashFlowsBase::FLOW_TYPE_EXPENSE,
    ];

    public static $paymentToFlowType = [
        PaymentType::TYPE_INVOICE => self::TYPE_BANK,
        PaymentType::TYPE_RECEIPT => self::TYPE_ORDER,
        PaymentType::TYPE_ONLINE => self::TYPE_EMONEY,
    ];

    /**
     * Array of cash flow types
     */
    public static $flowTypeArray = [
        self::TYPE_BANK,
        self::TYPE_ORDER,
        self::TYPE_EMONEY,
    ];

    /**
     * @param $type
     * @return string
     * @throws NotFoundHttpException
     */
    public static function getCashClass($type)
    {
        switch ($type) {
            case self::TYPE_BANK:
                $class = CashBankFlows::className();
                break;
            case self::TYPE_ORDER:
                $class = CashOrderFlows::className();
                break;
            case self::TYPE_EMONEY:
                $class = CashEmoneyFlows::className();
                break;
            case self::TYPE_FOREIGN_BANK:
                $class = CashBankForeignCurrencyFlows::className();
                break;
            case self::TYPE_FOREIGN_ORDER:
                $class = CashOrderForeignCurrencyFlows::className();
                break;
            case self::TYPE_FOREIGN_EMONEY:
                $class = CashEmoneyForeignCurrencyFlows::className();
                break;
            default:
            throw new NotFoundHttpException();
        }

        return $class;
    }

    /**
     * @return array
     */
    public static function getCashClasses()
    {
        try {
            return [
                self::getCashClass(self::TYPE_BANK),
                self::getCashClass(self::TYPE_ORDER),
                self::getCashClass(self::TYPE_EMONEY),
                self::getCashClass(self::TYPE_FOREIGN_BANK),
                self::getCashClass(self::TYPE_FOREIGN_ORDER),
                self::getCashClass(self::TYPE_FOREIGN_EMONEY),
            ];
        } catch (\Throwable $e) {

            return [];
        }
    }
}
