<?php

namespace common\models\cash;

use Yii;

/**
 * This is the model class for table "cash_order_foreign_currency_flows_to_invoice".
 *
 * @property int $flow_id
 * @property int $invoice_id
 * @property int $amount
 *
 * @property CashOrderForeignCurrencyFlows $flow
 * @property ForeignCurrencyInvoice $invoice
 */
class CashOrderForeignCurrencyFlowsToInvoice extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cash_order_foreign_currency_flows_to_invoice';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'flow_id' => 'Flow ID',
            'invoice_id' => 'Invoice ID',
            'amount' => 'Amount',
        ];
    }

    /**
     * Gets query for [[Flow]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFlow()
    {
        return $this->hasOne(CashOrderForeignCurrencyFlows::className(), ['id' => 'flow_id']);
    }

    /**
     * Gets query for [[Invoice]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(ForeignCurrencyInvoice::className(), ['id' => 'invoice_id']);
    }
}
