<?php

namespace common\models\cash;

use Yii;
use common\components\TextHelper;
use common\components\cash\InternalTransferFlowInterface;
use common\components\cash\InternalTransferHelper;
use common\components\date\DateHelper;
use common\models\Company;
use common\models\Contractor;
use common\models\company\CheckingAccountant;
use common\models\currency\Currency;
use common\models\document\ForeignCurrencyInvoice;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\document\PayableInvoiceInterface;
use frontend\models\log\Log;
use frontend\modules\cash\models\CashContractorType;
use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "cash_bank_foreign_currency_flows".
 *
 * @property int $id
 * @property int $company_id
 * @property int $account_id
 * @property string $currency_id
 * @property int|null $income_item_id
 * @property int|null $expenditure_item_id
 * @property string|null $contractor_id
 * @property int $flow_type 0 - расход, 1 - приход
 * @property string $date
 * @property string|null $recognition_date
 * @property int $amount
 * @property string|null $description
 * @property string|null $rs
 * @property string|null $bank_name
 * @property string|null $bank_address
 * @property int|null $is_taxable
 * @property int|null $has_invoice Связь со счётом: 0 - нет, 1 - да
 * @property int|null $is_funding_flow
 * @property int|null $cash_funding_type_id
 * @property int|null $created_at
 *
 * @property CashBankForeignCurrencyFlowsToInvoice[] $cashBankForeignCurrencyFlowsToInvoices
 * @property CashFundingType $cashFundingType
 * @property Company $company
 * @property Currency $currency
 * @property InvoiceExpenditureItem $expenditureItem
 * @property InvoiceIncomeItem $incomeItem
 * @property ForeignCurrencyInvoice[] $invoices
 */
class CashBankForeignCurrencyFlows extends CashFlowsBase implements InternalTransferFlowInterface
{
    public function getIsForeign()
    {
        return true;
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cash_bank_foreign_currency_flows';
    }

    public function getCashContractorTypeText() : string
    {
        return \frontend\modules\cash\models\CashContractorType::BANK_CURRENCY_TEXT;
    }

    public function getInternalTransferAccountClass() : ?string
    {
        return InternalTransferHelper::accountClassByTable($this->internal_transfer_account_table);
    }

    public function getInternalTransferFlowClass() : ?string
    {
        return InternalTransferHelper::flowClassByTable($this->internal_transfer_flow_table);
    }

    public function getSelfAccount() : ActiveQuery
    {
        return $this->getCheckingAccountant();
    }

    public function getInternalTransferAccount() : ?ActiveQuery
    {
        if ($class = $this->getInternalTransferAccountClass()) {
            return $this->hasOne($class, ['id' => 'internal_transfer_account_id']);
        }

        return null;
    }

    public function getInternalTransferFlow() : ?ActiveQuery
    {
        if ($class = $this->getInternalTransferFlowClass()) {
            return $this->hasOne($class, ['id' => 'internal_transfer_flow_id']);
        }

        return null;
    }

    /**
     * @return query\CashBankForeignCurrencyQuery
     */
    public static function find()
    {
        return new query\CashBankForeignCurrencyQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [
                [
                    'rs',
                    'contractor_id',
                    'amount_input',
                    'date_input',
                    'recognition_date_input',
                    'description',
                    'payment_order_number',
                ], 'trim'
            ],
            [
                [
                    'rs',
                    'flow_type',
                    'date_input',
                    'amount_input',
                ], 'required'
            ],
            [
                [
                    'contractor_id',
                ], 'required',
                'when' => function ($model) {
                    return !$model->is_internal_transfer;
                },
            ],
            [
                [
                    'is_prepaid_expense',
                ], 'boolean'
            ],
            [
                [
                    'income_item_id',
                    'expenditure_item_id',
                    'flow_type',
                    'amount',
                    'is_taxable',
                    'is_funding_flow',
                    'cash_funding_type_id',
                    'created_at',
                    'payment_order_number',
                ], 'integer'
            ],
            [
                [
                    'amount_input'
                ], 'number',
                'skipOnError' => true,
                'numberPattern' => '/^[0-9]{1,18}([.,][0-9]{0,2})?$/',
                'min' => 0.01,
                'max' => Yii::$app->params['maxCashSum'],
                'tooBig' => 'Значение «{attribute}» не должно превышать ' . TextHelper::moneyFormat(Yii::$app->params['maxCashSum'], 2) . '.',
            ],
            [
                [
                    'date_input',
                    'recognition_date_input'
                ], 'date',
                'format' => 'php:d.m.Y',
            ],
            [
                [
                    'contractor_id',
                    'description',
                    'rs',
                ], 'string',
                'max' => 255,
            ],
            [['expenditure_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => InvoiceExpenditureItem::className(), 'targetAttribute' => ['expenditure_item_id' => 'id']],
            [['income_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => InvoiceIncomeItem::className(), 'targetAttribute' => ['income_item_id' => 'id']],
            [
                ['rs'], 'exist',
                'skipOnError' => true,
                'targetClass' => CheckingAccountant::className(),
                'filter' => [
                    'and',
                    ['company_id' => $this->company_id],
                    ['not', ['currency_id' => Currency::DEFAULT_ID]],
                ],
            ],
            [['contractor_id'], 'validateContractor'],
            [['invoices_list'], 'validateInvoicesList'],
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateContractor($attribute, $params)
    {
        if ($this->hasErrors($attribute)) {
            return;
        }

        $isExist = Contractor::find()->andWhere([
            'id' => $this->$attribute,
        ])->andWhere([
            'or',
            ['company_id' => null],
            ['company_id' => $this->company_id],
        ])->exists() || CashContractorType::find()->andWhere([
            'name' => $this->$attribute,
        ])->exists();

        if (!$isExist) {
            $this->addError($attribute, 'Контрагент не найден.');
        }
    }

    public function validateInvoicesList($attribute, $params)
    {
        if (!is_array($this->$attribute)) {
            $this->$attribute = (array) $this->$attribute;
        }

        if ($this->$attribute &&
            $this->company &&
            $this->contractor &&
            !$this->hasErrors('flow_type')
        ) {
            $availableInvoices = form\CashBankFlowsForm::getAvailableInvoices(
                $this->company,
                $this->contractor,
                ($this->flow_type != static::FLOW_TYPE_INCOME),
                $this
            );

            $availableInvoicesIds = [];

            foreach ($availableInvoices as $invoice) {
                $availableInvoicesIds[] = $invoice['id'];
            }

            foreach ($this->$attribute as $invoiceId) {
                if (!is_numeric($invoiceId) || !in_array($invoiceId, $availableInvoicesIds)) {
                    $this->addError($attribute, 'Выбран некорректный счет');
                }
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function afterValidate()
    {
        $account = $this->account ?? null;
        $currency = $account->currency ?? null;
        $this->swift = $account->swift ?? null;
        $this->bank_name = $account->bank_name ?? null;
        $this->currency_id = $currency->id ?? null;
        $this->currency_name = $currency->name ?? null;
    }

    /** @inheritdoc */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'flow_type' => 'Приход/Расход',
            'rs' => 'Расчетный счет',
            'currency_id' => 'Валюта счета',
            'bank_name' => 'Название банка',
            'bank_address' => 'Адрес банка',
            'payment_order_number' => 'Номер ПП',
            'income_item_id' => 'Статья прихода',
            'expenditure_item_id' => 'Статья расход',
            'billPaying' => 'Опл. счета',
            'taxpayers_status_id' => 'Статус плательщика',
            'kbk' => 'КБК',
            'oktmo_code' => 'Код ОКТМО',
            'payment_details_id' => 'Основание платежа',
            'tax_period_code' => 'Налоговый период',
            'document_number_budget_payment' => 'Номер бюджетного документа',
            'document_date_budget_payment' => 'Дата бюджетного документа',
            'dateBudgetPayment' => 'Дата бюджетного платежа',
            'payment_type_id' => 'Тип платежа',
            'uin_code' => 'Код УИН',
            'is_prepaid_expense' => 'Авансовый платеж',
            'credit_id' => 'Кредитный договор',
            'credit_amount' => 'На сумму',
        ]);
    }

    /**
     * @param string $value
     */
    public function setAmount_input($value)
    {
        $value = strtr($value, [',' => '.']);
        $this->amount = is_numeric($value) ? $value * 100 : $value;
    }

    /**
     * @return string
     */
    public function getAmount_input()
    {
        return is_numeric($this->amount) ? $this->amount / 100 : $this->amount;
    }

    /**
     * @param string $value
     */
    public function setDate_input($value)
    {
        $this->date = ($d = date_create_from_format('d.m.Y', $value)) ? $d->format('Y-m-d') : $value;
    }

    /**
     * @return string
     */
    public function getDate_input()
    {
        return ($d = date_create_from_format('Y-m-d', $this->date)) ? $d->format('d.m.Y') : $this->date;
    }

    /**
     * @param string $value
     */
    public function setRecognition_date_input($value)
    {
        $this->recognition_date = ($d = date_create_from_format('d.m.Y', $value)) ? $d->format('Y-m-d') : $value;
    }

    /**
     * @return string
     */
    public function getRecognition_date_input()
    {
        return ($d = date_create_from_format('Y-m-d', $this->recognition_date)) ? $d->format('d.m.Y') : $this->recognition_date;
    }

    /**
     * Gets query for [[CashBankForeignCurrencyFlowsToInvoices]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCashBankForeignCurrencyFlowsToInvoices()
    {
        return $this->hasMany(CashBankForeignCurrencyFlowsToInvoice::className(), ['flow_id' => 'id']);
    }

    /**
     * Gets query for [[CashFundingType]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCashFundingType()
    {
        return $this->hasOne(CashFundingType::className(), ['id' => 'cash_funding_type_id']);
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContractor()
    {
        return $this->hasOne(Contractor::className(), ['id' => 'contractor_id']);
    }

    /**
     * Gets query for [[Currency]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * Gets query for [[ExpenditureItem]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getExpenditureItem()
    {
        return $this->hasOne(InvoiceExpenditureItem::className(), ['id' => 'expenditure_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFlowInvoices()
    {
        return $this->hasMany(CashBankForeignCurrencyFlowsToInvoice::className(), ['flow_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCheckingAccountants()
    {
        return $this->hasMany(CheckingAccountant::className(), ['company_id' => 'company_id', 'rs' => 'rs']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForeignCurrencyAccounts()
    {
        return $this->getCheckingAccountants();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCheckingAccountant()
    {
        $query = $this->getCheckingAccountants();
        $query->multiple = false;

        return $query->orderBy(['type' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForeignCurrencyAccount()
    {
        return $this->getCheckingAccountant();
    }

    /**
     * Gets query for [[IncomeItem]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIncomeItem()
    {
        return $this->hasOne(InvoiceIncomeItem::className(), ['id' => 'income_item_id']);
    }

    /**
     * Gets query for [[Invoices]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInvoices()
    {
        return $this->hasMany(ForeignCurrencyInvoice::className(), ['id' => 'invoice_id'])->viaTable('cash_bank_foreign_currency_flows_to_invoice', ['flow_id' => 'id']);
    }

    /**
     * Gets query for [[Invoices]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        $query = $this->getForeignCurrencyAccounts()->orderBy([
            'type' => SORT_ASC,
        ]);
        $query->multiple = false;

        return $query;
    }

    /**
     * Get formatted log message from Log model
     * @param Log $log
     * @return string
     */
    public function getLogMessage(Log $log)
    {
        return $this->getLogMessageBase($log, 'по банку');
    }

    /**
     * @return string
     */
    public function getBillPaying($showClarifyLink = true)
    {
        if ($this->is_internal_transfer) {
            return '';
        }
        $invoices = [];
        foreach ($this->invoices as $invoice) {
            $invoices[] = Html::a('№' . $invoice->fullNumber, [
                '/documents/foreign-currency-invoice/view',
                'id' => $invoice->id,
                'type' => $invoice->type,
            ]);
        }

        $invoicesList = !empty($invoices) ? implode(', ', $invoices) : '-';

        $options = ['title' => 'Уточнить', 'data' => ['toggle' => 'modal', 'target' => '#update-movement'], 'class' => 'red-link'];
        if ($showClarifyLink && $this->getAvailableAmount() > 0) {
            return $invoicesList . '<br />' . Html::a('<span class="cash-bank-need-clarify">Уточнить</span>', [
                '/cash/bank/update',
                'id' => $this->id,
                'foreign' => 1,
            ], $options);
        } else {
            return $invoicesList;
        }
    }

    /**
     * @return int
     */
    public function getInvoicesTotalAmount()
    {
        return (int)$this->getFlowInvoices()->sum('amount');
    }

    /**
     * @param Invoice $invoice
     * @return bool
     */
    public function linkInvoice(PayableInvoiceInterface $invoice, $amount = null)
    {
        if ($this->contractor_id == $invoice->contractor_id && !$this->getFlowInvoices()->andWhere(['invoice_id' => $invoice->id])->exists()) {
            if ($amount) {
                $amount = min((int)$amount, (int)$this->getAvailableAmount(), (int)$invoice->getAvailablePaymentAmount());
            } else {
                $amount = min((int)$this->getAvailableAmount(), (int)$invoice->getAvailablePaymentAmount());
            }
            $amount = max(0, $amount);
            if ($amount) {
                $this->link('invoices', $invoice, ['amount' => $amount]);
                if ($this->has_invoice == self::NON_INVOICE) {
                    $this->updateAttributes([
                        'has_invoice' => self::HAS_INVOICE,
                    ]);
                }
            }
            $invoice->checkPaymentStatus($this);

            return $this->getFlowInvoices()->andWhere(['invoice_id' => $invoice->id])->exists();
        }

        return false;
    }

    /**
     * @param Invoice $invoice
     * @return bool
     */
    public function unlinkInvoice(PayableInvoiceInterface $invoice, $checkHasInvoice = true)
    {
        if ($this->getFlowInvoices()->andWhere(['invoice_id' => $invoice->id])->exists()) {
            $flowInvoice = $this->getFlowInvoices()->andWhere(['invoice_id' => $invoice->id])->one();
            $this->unlink('invoices', $invoice, true);

            if ($checkHasInvoice && !$this->hasInvoice()) {
                $this->updateAttributes([
                    'has_invoice' => self::NON_INVOICE,
                ]);
            }
        }
        $invoice->checkPaymentStatus($this);

        return true;
    }

    /**
     * @return CashOrderFlows
     */
    public function cloneSelf()
    {
        $model = clone $this;
        $model->isNewRecord = true;
        unset($model->id);
        $model->payment_order_number = null;
        $model->date = date('Y-m-d');
        $model->recognition_date = date('Y-m-d');
        $model->invoice_id = null;
        $model->internal_transfer_flow_id = null;
        $model->has_invoice = 0;

        return $model;
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isAttributeChanged('rs')) {
                $rs = $this->rs ? $this->getCheckingAccountant()->one() : null;
                $this->bank_name = $rs !== null ? $rs->bank_name : null;
                $this->account_id = $rs !== null ? $rs->id : null;
            }

            if ($insert && !isset($this->source)) {
                $this->source = CashBankStatementUpload::SOURCE_MANUAL;
            }

            return true;
        } else {
            return false;
        }
    }

    public function getNumber()
    {
        return $this->payment_order_number;
    }
}
