<?php

namespace common\models\cash\query;

use common\models\cash\CashFlowLinkage;
use yii\db\ActiveQuery;

/**
 * Class CashFlowLinkageQuery
 * @package common\models\cash\query
 */
class CashFlowLinkageQuery extends ActiveQuery
{
    /**
     * Constructor.
     * @param string|ActiveRecord $modelClass the model class associated with this query
     * @param array $config configurations to be applied to the newly created query object
     */
    public function __construct($modelClass, $config = [])
    {
        parent::__construct($modelClass, $config);

        $this->andWhere([
            $this->getTableAlias() . '.is_deleted' => false,
        ]);
    }

    /**
     * @return CashFlowLinkageQuery
     */
    public function active()
    {
        return $this->andWhere([
            $this->getTableAlias().'.status' => CashFlowLinkage::STATUS_ACTIVE,
        ]);
    }

    /**
     * @return CashFlowLinkageQuery
     */
    public function inactive()
    {
        return $this->andWhere([
            $this->getTableAlias().'.status' => CashFlowLinkage::STATUS_INACTIVE,
        ]);
    }

    public function getTableAlias()
    {
        return $this->getTableNameAndAlias()[1];
    }
}
