<?php
/**
 * Created by konstantin.
 * Date: 29.6.15
 * Time: 17.37
 */

namespace common\models\cash\query;


use common\models\cash\CashOrderFlows;

/**
 * Class CashOrderFlowQuery
 * @package common\models\cash
 */
class CashOrderFlow extends CashFlowBase
{

    /**
     * @param int $invoiceId
     * @return CashOrderFlow
     */
    public function byInvoice($invoiceId)
    {
        return $this->andWhere([
            CashOrderFlows::tableName() . '.invoice_id' => $invoiceId,
        ]);
    }

}