<?php
/**
 * Created by konstantin.
 * Date: 2.7.15
 * Time: 10.38
 */

namespace common\models\cash\query;

use common\models\cash\CashFlowsBase;
use common\models\company\CompanyType;
use yii\db\ActiveQuery;

/**
 * Class CashFlowBase
 * @package common\models\cash\query
 */
class CashFlowBase extends ActiveQuery
{
    use \common\components\TableAliasTrait;

    /**
     * @param $flowType
     * @return CashFlowBase
     */
    public function byFlowType($flowType)
    {
        return $this->andWhere([
            'flow_type' => $flowType,
        ]);
    }

    /**
     * @param $companyId
     * @return CashFlowBase
     */
    public function byCompany($companyId)
    {
        return $this->andWhere([
            'company_id' => $companyId,
        ]);
    }

    public function byFundingFlow($isFundingFlow = true)
    {
        return $this->andWhere([
            'is_funding_flow' => $isFundingFlow,
        ]);
    }

    public function selectSum($as = 'sum')
    {
        return $this->addSelect('SUM(IF(flow_type = ' . CashFlowsBase::FLOW_TYPE_INCOME . ', `amount`, -`amount`))' . ($as !== null ? ' AS ' . $as : ''));
    }

    public function getTableAlias()
    {
        return $this->getTableNameAndAlias()[1];
    }

    public function byContractorName($name)
    {
        $nameParts = explode(' ', preg_replace('/\s{2,}/', ' ', trim($name)));

        if (count($nameParts) > 1) {

            foreach ($nameParts as $key => $namePart) {

                $companyTypeId = CompanyType::find()
                    ->select('id')
                    ->andWhere(['name_short' => $namePart])
                    ->scalar();

                if ($companyTypeId) {
                    $this->andWhere(['contractor.company_type_id' => $companyTypeId]);
                    unset($nameParts[$key]);
                    break;
                }
            }
        }

        return $this->andWhere(['or',
            ['like', 'contractor.ITN', $name],
            ['like', 'contractor.name', $name],
            ['like', 'contractor.name', implode(' ', $nameParts)],
        ]);
    }
}
