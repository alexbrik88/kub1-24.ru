<?php

namespace common\models\cash\query;

use common\models\cash\CashFlowsBase;
use yii\db\ActiveQuery;

/**
 * Class CashBankForeignCurrencyQuery
 * @package common\models\cash\query
 */
class CashBankForeignCurrencyQuery extends ActiveQuery
{

    /**
     * @param $flowType
     * @return CashBankForeignCurrencyQuery
     */
    public function byFlowType($flowType)
    {
        return $this->andWhere([
            $this->getTableAlias().'.flow_type' => $flowType,
        ]);
    }

    /**
     * @param $companyId
     * @return CashBankForeignCurrencyQuery
     */
    public function byCompany($companyId)
    {
        return $this->andWhere([
            $this->getTableAlias().'.company_id' => $companyId,
        ]);
    }

    public function byFundingFlow($isFundingFlow = true)
    {
        return $this->andWhere([
            $this->getTableAlias().'.is_funding_flow' => $isFundingFlow,
        ]);
    }

    public function selectSum($as = 'sum')
    {
        return $this->addSelect('SUM(IF(flow_type = ' . CashFlowsBase::FLOW_TYPE_INCOME . ', `amount`, -`amount`))' . ($as !== null ? ' AS ' . $as : ''));
    }

    public function getTableAlias()
    {
        return $this->getTableNameAndAlias()[1];
    }
}
