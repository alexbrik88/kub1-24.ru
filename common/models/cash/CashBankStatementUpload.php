<?php

namespace common\models\cash;

use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\employee\Employee;
use frontend\models\Documents;
use frontend\models\log\Log;
use frontend\models\log\ILogMessage;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use Yii;

/**
 * This is the model class for table "cash_bank_statement_upload".
 *
 * @property integer $id
 * @property integer $created_by
 * @property integer $checking_accountant_id
 * @property integer $created_at
 * @property integer $source
 * @property integer $saved_count
 *
 * @property CheckingAccountant $checkingAccountant
 * @property Employee $employee
 * @property Company $company
 */
class CashBankStatementUpload extends \yii\db\ActiveRecord implements ILogMessage
{
    const SOURCE_MANUAL = 0;
    const SOURCE_FILE = 1;
    const SOURCE_BANK = 2;
    const SOURCE_BANK_AUTO = 3;

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
            ],
            [
                'class' => BlameableBehavior::className(),
                'updatedByAttribute' => false,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cash_bank_statement_upload';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bik', 'rs'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Cjmpany ID',
            'bik' => 'BIK',
            'rs' => 'RS',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'saved_count' => 'Загружено операций',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCheckingAccountant()
    {
        return $this->hasOne(CheckingAccountant::className(), [
            //'id' => 'account_id',
            'company_id' => 'company_id',
            'bik' => 'bik',
            'rs' => 'rs',
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'created_by']);
    }

    /**
     * Get formatted log message from Log model
     * @param Log $log
     * @return string
     */
    public function getLogMessage(Log $log)
    {
        return "Выписка по р/с {$log->getModelAttributeNew('rs')} загружена";
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->getEmployee();
    }

    /**
     * Returns array for set color and icon. Used on displaying log messages.
     * @example: ['label-danger', 'fa fa-file-text-o']
     * @param Log $log
     * @return array
     */
    public function getLogIcon(Log $log)
    {
        return Documents::getLogIcon($log);
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if (is_a(Yii::$app,'yii\console\Application')) {
            $this->created_by = $this->company->employeeChief->id;
        }

        \common\models\company\CompanyFirstEvent::checkEvent($this->company, 47);

        return true;
    }
}
