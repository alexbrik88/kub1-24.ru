<?php

namespace common\models\cash;

use common\models\document\Invoice;
use Yii;

/**
 * @property integer $id
 * @property string $flow_id
 * @property string $invoice_id
 * @property integer $amount
 * @property integer $tin_child_amount
 *
 * @property CashBankFlows $flow
 * @property Invoice $invoice
 */
class CashBankFlowToInvoice extends \yii\db\ActiveRecord
{
    /** @inheritdoc */
    public static function tableName()
    {
        return 'cash_bank_flow_to_invoice';
    }

    /** @inheritdoc */
    public function rules()
    {
        return [
            // [['flow_id', 'invoice_id'], 'required'],
            // [['flow_id', 'invoice_id'], 'integer'],
            // [['flow_id'], 'exist', 'targetClass' => CashBankFlows::className(), 'targetAttribute' => 'id',],
            // [['invoice_id'], 'exist', 'targetClass' => Invoice::className(), 'targetAttribute' => 'id',],
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFlow()
    {
        return $this->hasOne(CashBankFlows::className(), ['id' => 'flow_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(Invoice::className(), ['id' => 'invoice_id']);
    }
}
