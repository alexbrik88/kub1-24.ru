<?php

namespace common\models\cash;

use Yii;

/**
 * This is the model class for table "cash_funding_type".
 *
 * @property integer $id
 * @property string $name
 */
class CashFundingType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'cash_funding_type';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
        ];
    }
}
