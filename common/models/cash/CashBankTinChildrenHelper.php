<?php

namespace common\models\cash;

use common\components\date\DateHelper;
use common\components\helpers\ArrayHelper;
use common\components\TextHelper;
use common\models\company\CompanyIndustry;
use common\models\companyStructure\SalePoint;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\project\Project;
use yii\db\ActiveRecord;
use yii\db\Exception;
use yii\helpers\Html;

class CashBankTinChildrenHelper {

    private static $_limitPerOneParent = 25;
    private static $_cache = [];

    /**
     * @param $flowData
     * @param $piecesKey
     * @return string
     */
    public static function getPieces($flowData, $piecesKey)
    {
        if (empty($flowData['has_tin_children']))
            return '';

        $tinColumn = self::getColumn($flowData['id'], $piecesKey);
        //$ret = '<span class="tin-children-amount">';
        $ret='';
        foreach ($tinColumn as $row => $tinValue) {

            //if ($row > 0)
                $ret .= '<br/>';

            switch ($piecesKey) {
                case 'tin_child_amount':
                    $ret .= Html::tag('span', TextHelper::invoiceMoneyFormat($tinValue, 2), ['class' => 'tin-children']);
                    break;
                case 'recognition_date':
                    $ret .= Html::tag('span', DateHelper::format($tinValue, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE), ['class' => 'tin-children']);
                    break;
                case 'project_id':
                case 'sale_point_id':
                case 'industry_id':
                    if ($tinValue) {
                        $class = self::_getClassByKey($piecesKey);
                        $model = $class::findOne(['id' => $tinValue]);
                        $ret .= Html::tag('span', Html::encode($model->name ?? '-'), ['title' => $model->name ?? '-', 'class' => 'tin-children']);
                    } else {
                        $ret .= '-';
                    }
                    break;
                case 'income_item_id':
                case 'expenditure_item_id':
                    if ($tinValue) {
                        $class = self::_getClassByKey($piecesKey);
                        $model = $class::findOne(['id' => $tinValue]);
                        $ret .= Html::tag('span', Html::encode($model->name ?? '-'), ['class' => 'tin-children']);
                    } else {
                        $ret .= '-';
                    }
                    break;
            }
        }

        //$ret .= '</span>';

        return $ret;
    }

    /**
     * @param int $parentFlowID
     * @return array
     */
    public static function getAll($parentFlowID)
    {
        if (!isset(self::$_cache[$parentFlowID])) {
            self::_preload($parentFlowID);
        }

        return self::$_cache[$parentFlowID] ?? [];
    }

    /**
     * @param int $parentFlowID
     * @param $column
     * @return array
     */
    public static function getColumn($parentFlowID, $column)
    {
        if (!isset(self::$_cache[$parentFlowID])) {
            self::_preload($parentFlowID);
        }

        return ArrayHelper::getColumn(self::$_cache[$parentFlowID], $column);
    }

    /**
     * @param int $parentFlowID
     */
    private static function _preload($parentFlowID)
    {
        if (!$parentFlowID || !is_numeric($parentFlowID)) {
            self::$_cache[$parentFlowID] = [];
            return;
        }

        self::$_cache[$parentFlowID] = CashBankFlows::find()
            ->where(['parent_id' => $parentFlowID])
            ->orderBy(['tin_child_amount' => SORT_DESC])
            ->asArray()
            ->limit(self::$_limitPerOneParent)
            ->all();
    }

    private static function _getClassByKey($piecesKey)
    {
        switch ($piecesKey) {
            case 'project_id':
                $class = Project::class;
                break;
            case 'sale_point_id':
                $class = SalePoint::class;
                break;
            case 'industry_id':
                $class = CompanyIndustry::class;
                break;
            case 'income_item_id':
                $class = InvoiceIncomeItem::class;
                break;
            case 'expenditure_item_id':
                $class = InvoiceExpenditureItem::class;
                break;
            default:
                throw new Exception('Unknown pieces class');
        }

        return $class;
    }
}