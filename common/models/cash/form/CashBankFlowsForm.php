<?php

namespace common\models\cash\form;

use common\components\date\DateHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashBankFlowToInvoice;
use common\models\Company;
use common\models\Contractor;
use common\models\document\Invoice;
use common\models\employee\Employee;

/**
 * Class CashBankFlowsForm
 */
class CashBankFlowsForm extends CashBankFlows
{
    public $invoices_list = [];

    /** @inheritdoc */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'invoices_list' => 'Оплаченные счета',
        ]);
    }

    /** @inheritdoc */
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['invoices_list'], 'validateInvoicesList'],
        ]);
    }

    /** @inheritdoc */
    public function scenarios()
    {
        return [
            'create' => [
                'flow_type', 'rs', 'bank_name', 'contractor_id', 'expenditure_item_id',
                'project_id', 'sale_point_id', 'industry_id',
                'income_item_id', 'amount', 'date', 'payment_order_number', 'description',
                'invoices_list', 'recognitionDateInput',
                'taxpayers_status',
                'kbk',
                'oktmo_code',
                'payment_details',
                'tax_period_code',
                'document_number_budget_payment',
                'dateBudgetPayment',
                'payment_type',
                'uin_code',
                'is_prepaid_expense',
                'credit_id',
            ],
            'update' => [
                'rs', 'bank_name', 'contractor_id', 'expenditure_item_id', 'income_item_id',
                'project_id', 'sale_point_id', 'industry_id',
                'amount', 'date', 'payment_order_number', 'description',
                'invoices_list', 'recognitionDateInput',
                'kbk', 'tax_period_code',
                'taxpayers_status',
                'kbk',
                'oktmo_code',
                'payment_details',
                'tax_period_code',
                'document_number_budget_payment',
                'dateBudgetPayment',
                'payment_type',
                'uin_code',
                'is_prepaid_expense',
                'credit_id',
            ],
        ];
    }

    public function validateInvoicesList($attribute, $params)
    {
        if (!is_array($this->$attribute)) {
            $this->$attribute = [];
        }

        if (!empty($this->$attribute) && !$this->hasErrors('flow_type') && !$this->hasErrors('contractor_id')) {
            if ($this->contractor) {
                $availableInvoices =  CashBankFlowsForm::getAvailableInvoices(
                    $this->company,
                    $this->contractor,
                    ($this->flow_type != static::FLOW_TYPE_INCOME),
                    $this
                );
            } else {
                $availableInvoices = [];
            }

            $availableInvoicesIds = [];

            foreach ($availableInvoices as $invoice) {
                $availableInvoicesIds[] = $invoice['id'];
            }

            foreach ($this->$attribute as $invoiceId) {
                if (!is_numeric($invoiceId) || !in_array($invoiceId, $availableInvoicesIds)) {

                    // skip by children
                    if ($this->parent && in_array($invoiceId, $this->parent->invoices_list))
                        continue;

                    $this->addError($attribute, 'Выбран некорректный счет');
                    return;
                }
            }
        }
    }

    /** @inheritdoc */
    public function beforeSave($insert)
    {
        if ($this->flow_type == static::FLOW_TYPE_EXPENSE) {
            $this->income_item_id = null;
        } elseif ($this->flow_type == static::FLOW_TYPE_INCOME) {
            $this->expenditure_item_id = null;
        }

        return parent::beforeSave($insert);
    }

    /**
     * @param Company $company
     * @param Contractor $contractor
     * @param bool $isExpense
     * @param CashBankFlows|null $flowModel
     * @return array
     */
    public static function getAvailableInvoices(Company $company, Contractor $contractor, $isExpense, $flowModel = null, $flowDate = null)
    {
        $foundedIds = [];

        $invoices = $contractor->getUnPayedInvoicesJs($company, $isExpense, $flowDate);

        foreach ($invoices as $invoice) {
            $foundedIds[] = $invoice['id'];
        }

        if (!empty($flowModel) && !$flowModel->isNewRecord) {
            $modelInvoices = $flowModel->invoices;

            if (!empty($modelInvoices)) {
                foreach ($modelInvoices as $invoice) {
                    if (!in_array($invoice->id, $foundedIds)) {
                        $invoices[] = [
                            'id' => $invoice->id,
                            'number' => $invoice->fullNumber,
                            'date' => ($d = date_create_from_format('Y-m-d', $invoice->document_date)) ? $d->format('d.m.Y') : '',
                            'amount' => $invoice->total_amount_with_nds / 100,
                            'amountPrint' => \common\components\TextHelper::invoiceMoneyFormat($invoice->total_amount_with_nds, 2),
                            'amountRemaining' => $invoice->remaining_amount / 100,
                            'projectId' => $invoice->project_id
                        ];
                    }
                }
            }
        }

        usort($invoices, function($first, $second) {
            return (intval($second['number']) - intval($first['number']));
        });

        return $invoices;
    }

    public static function ajaxValidateForm($model): array
    {
        $ajaxErrors = [];
        $model->validate(null, false);
        foreach ($model->getErrors() as $attribute => $errors) {
            $ajaxErrors[\yii\helpers\Html::getInputId($model, $attribute)] = $errors;
        }
        return $ajaxErrors;
    }

    public function getInvoicesListSum(): float
    {
        $companyId = $this->company_id;
        $invoicesList = array_filter((array)$this->invoices_list);

        if (!$companyId || !$invoicesList)
            return (float)"0";

        $invoiceRemainingSum = (float)Invoice::find()
            ->where(['id' => $invoicesList, 'company_id' => $companyId])
            ->sum('remaining_amount');

        if ($this->parent_id) {
            $parentFlowInvoiceSum = (float)CashBankFlowToInvoice::find()
                ->where(['flow_id' => $this->parent_id, 'invoice_id' => $invoicesList])
                ->sum('amount');

        } else {
            $parentFlowInvoiceSum = 0;
        }

        return ($parentFlowInvoiceSum)
            ? ($parentFlowInvoiceSum + $invoiceRemainingSum) / 100
            : ($invoiceRemainingSum) / 100;
    }

    /** @inheritdoc */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $invoiceIdArray = is_array($this->invoices_list) ? $this->invoices_list : [];

        /** Пересчитываем привязки к счетам */
        $invioceArray = $this->getInvoices()->all();
        foreach ($invioceArray as $invoice) {
            if (in_array($invoice->id, $invoiceIdArray)) {
                $invoiceIdArray = array_diff($invoiceIdArray, [$invoice->id]);
            } else {
                $this->unlinkInvoice($invoice);
            }
        }

        if ($invoiceIdArray && ($newInvoiceArray = Invoice::find()->andWhere(['id' => $invoiceIdArray])->all())) {
            foreach ($newInvoiceArray as $invoice) {
                $this->linkInvoice($invoice);
            }
        }
    }
}
