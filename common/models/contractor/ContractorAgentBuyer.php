<?php

namespace common\models\contractor;

use Yii;
use common\models\Contractor;

/**
 * This is the model class for table "contractor_agent_buyer".
 *
 * @property integer $agent_id
 * @property integer $buyer_id
 * @property string $start_date
 * @property integer $created_at
 * @property integer $ord
 *
 * @property Contractor $agent
 * @property Contractor $buyer
 */
class ContractorAgentBuyer extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contractor_agent_buyer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['agent_id', 'buyer_id'], 'required'],
            [['agent_id', 'buyer_id', 'created_at', 'ord'], 'integer'],
            [['start_date'], 'safe'],
            [['agent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contractor::className(), 'targetAttribute' => ['agent_id' => 'id']],
            [['buyer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contractor::className(), 'targetAttribute' => ['buyer_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'agent_id' => 'Агент',
            'buyer_id' => 'Покупатель',
            'start_date' => 'Дата',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgent()
    {
        return $this->hasOne(Contractor::className(), ['id' => 'agent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBuyer()
    {
        return $this->hasOne(Contractor::className(), ['id' => 'buyer_id']);
    }
}
