<?php

namespace common\models\contractor;

use common\models\Company;
use common\models\ListBuilder;
use common\models\ListInterface;

class ContractorList implements ListInterface
{
    /**
     * @var string[]|null
     */
    private ?array $items = null;

    /**
     * @var ContractorRepository
     */
    private ContractorRepository $repository;

    /**
     * @param Company $company
     */
    public function __construct(Company $company)
    {
        $this->repository = new ContractorRepository(['company' => $company]);
    }

    /**
     * @inheritDoc
     */
    public function getItems(): array
    {
        if ($this->items === null) {
            $builder = new ListBuilder();
            $builder->setIndexBy('id');
            $builder->setSort($this->repository->getSort());
            $this->items = $builder->buildItems($this->repository->getDataProvider(), 'name');
        }

        return $this->items;
    }
}
