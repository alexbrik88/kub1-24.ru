<?php namespace common\models\contractor;
use common\models\Contractor;
use frontend\modules\cash\models\CashContractorType;

class ContractorHelper {

    public static function getShortNameById($id)
    {
        switch ($id) {
            case CashContractorType::CUSTOMERS_TEXT:
                return 'Физ. лица';
            case CashContractorType::COMPANY_TEXT:
                return 'Компания';
            case CashContractorType::BALANCE_TEXT:
                return 'Баланс начальный';
            case CashContractorType::BANK_TEXT:
            case CashContractorType::EMONEY_TEXT:
            case CashContractorType::ORDER_TEXT:
                return 'Перевод м.с.';
            case "":
            case null:
                return "";
        }

        $c = Contractor::findOne($id);

        return ($c) ? $c->getShortTitle() : $id;
    }
}