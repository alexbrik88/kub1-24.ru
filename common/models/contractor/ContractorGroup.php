<?php

namespace common\models\contractor;

use common\models\Contractor;
use common\models\employee\Employee;
use common\models\retail\RetailHotKey;
use common\models\store\StoreUser;
use Yii;

/**
 * This is the model class for table "Contractor_group".
 *
 * @property integer $id
 * @property string $title
 * @property integer $company_id
 * @property integer type
 *
 * Relations:
 * @property RetailHotKey[] $retailHotKeys
 */
class ContractorGroup extends \yii\db\ActiveRecord
{
    const TYPE_WITHOUT = 0;
    const ID_WITHOUT = 1;
    /**
     * @inheritdoc
     */
    public static $field = [Contractor::TYPE_SELLER=>"seller_group_id",Contractor::TYPE_CUSTOMER=>"customer_group_id"];

    public static function tableName()
    {
        return 'contractor_group';
    }

    public function rules()
    {
        return [
            [['title'], 'trim'],
            [['title'], 'filter', 'filter' => function ($value) {
                return mb_strtoupper(mb_substr($value, 0, 1)) . mb_substr($value, 1);
            }],
            [['title'], 'required'],
            [['title'], 'string', 'max' => 60],
            [
                ['title'], 'unique', 'filter' => [
                'and',
                [
                    'or',
                    ['company_id' => null],
                    ['company_id' => $this->company_id],
                ],
                [
                    'or',
                    ['type' => null],
                    ['type' => $this->type],
                ],
            ],
                'message' => 'Такая группа уже существует',
            ],
            [['type'],'in', 'range' => [Contractor::TYPE_SELLER, Contractor::TYPE_CUSTOMER]]
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Наименование',
            'company_id' => 'Компания',
            'type' => 'Тип Пользователя',
        ];
    }

    public static function getListGroupExistContactor($type,$company){

        $subQuery = Contractor::find()
            ->select('COUNT(*)')
            ->where(self::$field[$type].'='.self::tableName().".id")
            ->createCommand()->getRawSql();
        $groupArray = ContractorGroup::find()
            ->select(['*','('.$subQuery.') as exist_contractor'])
            ->andWhere(['or',
                [
                    'and',
                    ['type' => $type],
                    ['company_id' => $company],
                ],
                [
                    'and',
                    ['type' => ContractorGroup::TYPE_WITHOUT],
                    ['id' => ContractorGroup::ID_WITHOUT],
                ],
            ])
            ->orderBy([
                'title' => SORT_ASC,
            ])->asArray()->all();

        return  json_decode(json_encode($groupArray));
    }

    public function getContractors($type = "")
    {
        if(empty($type)){
            return false;
        }
        return $this->hasMany(Contractor::className(), [self::$field[$type] => 'id' ])->asArray()->all();
    }

    /*public function getRetailHotKeys()
    {
        return $this->hasMany(RetailHotKey::className(), ['group_id' => 'id']);
    }*/

    /**
     * @param null $product_unit_id
     * @param int $productionType
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getGroups($type = ContractorGroup::WITHOUT)
    {
        $company = null;
        if (Yii::$app->user->isGuest) {
            $company = null;
        } elseif (Yii::$app->user->identity instanceof Employee) {
            $company = Yii::$app->user->identity->company->id;
        } elseif (Yii::$app->user->identity instanceof StoreUser) {
            $company = Yii::$app->user->identity->company ?
                Yii::$app->user->identity->company->id :
                Yii::$app->user->identity->currentKubContractor->company_id;
        }
        $group = ContractorGroup::find()
            ->andWhere(['or',
                [
                    'and',
                    ['type' => $type],
                    ['company_id' => $company],
                ],
                [
                    'and',
                    ['type' => ContractorGroup::TYPE_CUSTOMER],
                    ['id' => ContractorGroup::ID_WITHOUT],
                ],
            ]);
        $group->orderBy([
            'IF([[id]] = 1, 0, 1)' => SORT_ASC,
            'title' => SORT_ASC,
        ]);
        return $group->all();
    }
    public static function model($className=__CLASS__){
        return parent::model($className);
    }
}
