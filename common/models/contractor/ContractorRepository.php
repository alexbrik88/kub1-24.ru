<?php

namespace common\models\contractor;

use common\models\FilterInterface;
use common\models\Company;
use common\models\Contractor;
use common\models\RepositoryCompanyTrait;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\DataProviderInterface;
use yii\data\Sort;
use yii\db\ActiveQueryInterface;
use yii\db\ActiveRecordInterface;

class ContractorRepository extends Model implements FilterInterface
{
    use RepositoryCompanyTrait;

    /**
     * @var string|null
     */
    public $name;

    /**
     * @param string $name
     * @param int $type
     * @param int $faceType
     * @param Company|null $company
     * @return Contractor
     */
    public static function createContractor(string $name, int $type, int $faceType, ?Company $company): Contractor
    {
        return new Contractor([
            'company_id' => $company ? $company->id : null,
            'type' => $type,
            'face_type' => $faceType,
            'name' => $name,
            'contact_is_director' => false,
        ]);
    }

    /**
     * @param string $name
     * @param Company|null $company
     * @return ActiveRecordInterface|Contractor|null
     */
    public static function findByName(string $name, ?Company $company): ?Contractor
    {
        $repository = new static([
            'company' => $company,
            'name' => $name,
        ]);

        return $repository->getQuery()->one();
    }

    /**
     * @inheritDoc
     */
    public function getDataProvider(): DataProviderInterface
    {
        return new ActiveDataProvider([
            'pagination' => false,
            'query' => $this->getQuery(),
            'sort' => $this->getSort(),
        ]);
    }

    /**
     * @return ActiveQueryInterface
     */
    public function getQuery(): ActiveQueryInterface
    {
        return Contractor::find()->andFilterWhere([
            'company_id' => $this->company->id,
            'name' => $this->name,
        ]);
    }

    /**
     * @inheritDoc
     */
    public function getSort(): Sort
    {
        return new Sort(['defaultOrder' => ['id' => SORT_ASC]]);
    }
}
