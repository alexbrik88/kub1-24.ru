<?php

namespace common\models\vehicle;

use common\components\date\DatePickerFormatBehavior;
use common\components\helpers\Html;
use common\components\ImageHelper;
use common\components\validators\CarStateNumberValidator;
use common\models\file\File;
use Yii;
use common\models\driver\Driver;
use common\models\employee\Employee;
use common\models\Company;
use common\models\Contractor;
use yii\behaviors\TimestampBehavior;
use yii\db\Connection;

/**
 * This is the model class for table "vehicle".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $company_id
 * @property integer $author_id
 * @property string $number
 * @property string $date
 * @property integer $is_own
 * @property integer $vehicle_type_id
 * @property integer $fuel_type_id
 * @property string $fuel_consumption
 * @property string $initial_mileage_date
 * @property string $initial_mileage
 * @property string $current_mileage
 * @property integer $contractor_id
 * @property integer $driver_id
 * @property string $model
 * @property string $state_number
 * @property integer $is_state_number_russian
 * @property string $color
 * @property integer $semitrailer_type_id
 * @property integer $trailer_type_id
 * @property integer $body_type_id
 * @property string $tonnage
 * @property string $weight
 * @property string $length
 * @property string $width
 * @property string $height
 * @property string $volume
 * @property string $comment
 *
 * @property Driver[] $drivers
 * @property Employee $author
 * @property BodyType $bodyType
 * @property Company $company
 * @property Contractor $contractor
 * @property Driver $driver
 * @property FuelType $fuelType
 * @property Vehicle $semitrailerType
 * @property Vehicle $trailerType
 * @property Vehicle[] $ownSemitrailerVehicles
 * @property Vehicle[] $ownTrailerVehicles
 * @property VehicleType $vehicleType
 * @property File[] $files
 */
class Vehicle extends \yii\db\ActiveRecord
{
    /**
     * @var array
     */
    public $scanDocuments = [];

    /**
     * @var string
     */
    public static $uploadDirectory = 'vehicle';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'vehicle';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            [
                'class' => DatePickerFormatBehavior::class,
                'attributes' => [
                    'date' => [
                        'message' => 'Дата указана неверно.',
                    ],
                    'initial_mileage_date' => [
                        'message' => 'Дата начального пробега указана неверно.',
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'author_id', 'number', 'date', 'vehicle_type_id', 'model', 'state_number'], 'required'],
            [['created_at', 'updated_at', 'company_id', 'author_id', 'is_own', 'vehicle_type_id', 'fuel_type_id',
                'contractor_id', 'driver_id', 'semitrailer_type_id', 'trailer_type_id', 'body_type_id',
                'is_state_number_russian'], 'integer'],
            [['date', 'initial_mileage_date'], 'safe'],
            [['comment'], 'string'],
            [['state_number'], CarStateNumberValidator::class, 'when' => function (Vehicle $model) {
                return $model->is_state_number_russian == true;
            },],
            [['number', 'fuel_consumption', 'initial_mileage', 'current_mileage', 'model', 'state_number', 'color',
                'tonnage', 'weight', 'length', 'width', 'height', 'volume'], 'string', 'max' => 255],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::class, 'targetAttribute' => ['author_id' => 'id']],
            [['body_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => BodyType::class, 'targetAttribute' => ['body_type_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['company_id' => 'id']],
            [['contractor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contractor::class, 'targetAttribute' => ['contractor_id' => 'id']],
            [['driver_id'], 'exist', 'skipOnError' => true, 'targetClass' => Driver::class, 'targetAttribute' => ['driver_id' => 'id']],
            [['fuel_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => FuelType::class, 'targetAttribute' => ['fuel_type_id' => 'id']],
            [['semitrailer_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => Vehicle::class, 'targetAttribute' => ['semitrailer_type_id' => 'id']],
            [['trailer_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => Vehicle::class, 'targetAttribute' => ['trailer_type_id' => 'id']],
            [['vehicle_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => VehicleType::class, 'targetAttribute' => ['vehicle_type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
            'company_id' => 'Компания',
            'author_id' => 'Автор',
            'number' => 'Порядковый номер',
            'date' => 'Дата',
            'is_own' => 'Собственное ТС',
            'vehicle_type_id' => 'Тип ТС',
            'fuel_type_id' => 'Вид топлива',
            'fuel_consumption' => 'Норма расхода (л/100км)',
            'initial_mileage_date' => 'Дата начального пробега',
            'initial_mileage' => 'Начальный пробег',
            'current_mileage' => 'Текущий пробег',
            'contractor_id' => 'Перевозчик',
            'driver_id' => 'Водитель',
            'model' => 'Марка',
            'state_number' => 'Номер ТС',
            'is_state_number_russian' => 'ГосНомер РФ',
            'color' => 'Цвет ТС',
            'semitrailer_type_id' => 'Полуприцеп',
            'trailer_type_id' => 'Прицеп',
            'body_type_id' => 'Тип кузова',
            'tonnage' => 'Тоннаж (тонн)',
            'weight' => 'Вес ПП (тонн)',
            'length' => 'Длинна',
            'width' => 'Ширина',
            'height' => 'Высота',
            'volume' => 'Объём (м3)',
            'comment' => 'Комментарий',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDrivers()
    {
        return $this->hasMany(Driver::class, ['vehicle_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Employee::class, ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBodyType()
    {
        return $this->hasOne(BodyType::class, ['id' => 'body_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContractor()
    {
        return $this->hasOne(Contractor::class, ['id' => 'contractor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDriver()
    {
        return $this->hasOne(Driver::class, ['id' => 'driver_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFuelType()
    {
        return $this->hasOne(FuelType::class, ['id' => 'fuel_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSemitrailerType()
    {
        return $this->hasOne(Vehicle::class, ['id' => 'semitrailer_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTrailerType()
    {
        return $this->hasOne(Vehicle::class, ['id' => 'trailer_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwnSemitrailerVehicles()
    {
        return $this->hasMany(Vehicle::class, ['semitrailer_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwnTrailerVehicles()
    {
        return $this->hasMany(Vehicle::class, ['trailer_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehicleType()
    {
        return $this->hasOne(VehicleType::class, ['id' => 'vehicle_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasMany(File::class, ['owner_id' => 'id'])
            ->onCondition([File::tableName() . '.owner_model' => Vehicle::class]);
    }

    /**
     * @param array $css
     * @return string|null
     */
    public function getVehicleImg($css = [])
    {
        $img = null;
        $webroot = Yii::getAlias('@webroot');
        switch ($this->vehicle_type_id) {
            case VehicleType::TYPE_TRACTOR:
                if ($this->semitrailerType) {
                    $img = ImageHelper::getThumb("{$webroot}/img/logistics/tractor_semitrailer.png", [320, 145], $css);
                } else {
                    $img = ImageHelper::getThumb("{$webroot}/img/logistics/tractor.png", [320, 145], $css);
                }
                break;
            case VehicleType::TYPE_SEMITRAILER:
                $img = ImageHelper::getThumb("{$webroot}/img/logistics/semitrailer.png", [320, 145], $css);
                break;
            case VehicleType::TYPE_WAGON:
                if ($this->trailerType) {

                } else {
                    $img = ImageHelper::getThumb("{$webroot}/img/logistics/wagon.png", [320, 145], $css);
                }
                break;
            case VehicleType::TYPE_TRAILER:

                break;
        }

        return $img;
    }

    /**
     * @param Company $company
     * @param null $number
     * @return int|null
     */
    public static function getNextDocumentNumber(Company $company, $number = null)
    {
        $numQuery = static::find()
            ->andWhere(['company_id' => $company->id]);
        $lastNumber = $number ? $number : (int)$numQuery->max('(number * 1)');
        $nextNumber = 1 + $lastNumber;
        $existNumQuery = static::find()
            ->select('number')
            ->andWhere(['company_id' => $company->id]);
        $numArray = $existNumQuery->column();
        if (in_array($nextNumber, $numArray)) {
            return static::getNextDocumentNumber($company, $nextNumber);
        } else {
            return $nextNumber;
        }
    }

    /**
     * @param bool $html
     * @return string
     */
    public function getVehicleTypeText($html = false)
    {
        $type = $this->vehicleType->name;
        if ($this->vehicle_type_id == VehicleType::TYPE_TRACTOR && $this->semitrailerType) {
            return $type . ' + ' .
                ($html ? '<br>' : null) .
                $this->semitrailerType->vehicleType->name;
        } elseif ($this->vehicle_type_id == VehicleType::TYPE_WAGON && $this->trailerType) {
            return $type . ' + ' .
                ($html ? '<br>' : null) .
                $this->trailerType->vehicleType->name;
        } else {
            return $type;
        }
    }

    /**
     * @param bool $html
     * @return string
     */
    public function getStateNumberFull($html = false)
    {
        $stateNumber = $this->state_number;
        if ($this->vehicle_type_id == VehicleType::TYPE_TRACTOR && $this->semitrailerType) {
            return $stateNumber . ' + ' .
                ($html ? '<br>' : null) .
                $this->semitrailerType->state_number;
        } elseif ($this->vehicle_type_id == VehicleType::TYPE_WAGON && $this->trailerType) {
            return $stateNumber . ' + ' .
                ($html ? '<br>' : null) .
                $this->trailerType->state_number;
        } else {
            return $stateNumber;
        }
    }

    /**
     * @return string
     */
    public function getTonnageFull()
    {
        $stateNumber = $this->tonnage;
        if ($this->vehicle_type_id == VehicleType::TYPE_TRACTOR && $this->semitrailerType) {
            $stateNumber += $this->semitrailerType->tonnage;
        } elseif ($this->vehicle_type_id == VehicleType::TYPE_WAGON && $this->trailerType) {
            $stateNumber += $this->trailerType->tonnage;
        }

        return $stateNumber;
    }

    /**
     * @return string
     */
    public function getVolumeFull()
    {
        $volume = $this->volume;
        if ($this->vehicle_type_id == VehicleType::TYPE_TRACTOR && $this->semitrailerType) {
            $volume += $this->semitrailerType->volume;
        } elseif ($this->vehicle_type_id == VehicleType::TYPE_WAGON && $this->trailerType) {
            $volume += $this->trailerType->volume;
        }

        return $volume;
    }

    /**
     * @return string
     */
    public function getVolumeFullWithParams()
    {
        $l = $this->length;
        $w = $this->width;
        $h = $this->height;
        if ($this->vehicle_type_id == VehicleType::TYPE_TRACTOR && $this->semitrailerType) {
            $l += $this->semitrailerType->length;
            $w += $this->semitrailerType->width;
            $h += $this->semitrailerType->height;
        } elseif ($this->vehicle_type_id == VehicleType::TYPE_WAGON && $this->trailerType) {
            $l += $this->trailerType->length;
            $w += $this->trailerType->width;
            $h += $this->trailerType->height;
        }

        return "{$l}x{$w}x{$h}={$this->getVolumeFull()} м3";
    }

    /**
     * @param bool $html
     * @param bool $vehicleTypeWidth
     * @return string
     */
    public function getVehicleTitle($html = true, $vehicleTypeWidth = false)
    {
        $title = null;
        if ($vehicleTypeWidth) {
            $title .= Html::beginTag('span', [
                'style' => 'width: 28%;display: inline-block;',
            ]);
        }
        $title .= $this->vehicleType->name;
        if ($vehicleTypeWidth) {
            $title .= Html::endTag('span');
        }

        return $title . ' ' . $this->getMarkAndNumber($html, $vehicleTypeWidth);
    }

    public function getMarkAndNumber($html = true, $vehicleTypeWidth = false) {
        $title = "{$this->state_number} ";
        if ($this->model) {
            $title .= "{$this->model} ";
        }

        if ($this->vehicle_type_id == VehicleType::TYPE_TRACTOR && $this->semitrailerType) {
            $title .= $html ? '<br>' : ' ';
            if ($vehicleTypeWidth) {
                $title .= Html::beginTag('span', [
                    'style' => 'width: 28%;display: inline-block;',
                ]);
            }
            $title .= $this->semitrailerType->vehicleType->name;
            if ($vehicleTypeWidth) {
                $title .= Html::endTag('span');
            }
            if ($this->semitrailerType->model) {
                $title .= (' ' . $this->semitrailerType->model);
            }
            $title .= (' ' . $this->semitrailerType->state_number);
        } elseif ($this->vehicle_type_id == VehicleType::TYPE_WAGON && $this->trailerType) {
            $title .= $html ? '<br>' : ' ';
            if ($vehicleTypeWidth) {
                $title .= Html::beginTag('span', [
                    'style' => 'width: 28%;display: inline-block;',
                ]);
            }
            $title .= $this->trailerType->vehicleType->name;
            if ($vehicleTypeWidth) {
                $title .= Html::endTag('span');
            }
            if ($this->trailerType->model) {
                $title .= (' ' . $this->trailerType->model);
            }
            $title .= (' ' . $this->trailerType->state_number);
        }

        return $title;
    }

    /**
     * @return string
     */
    public function getEmailSubject()
    {
        return 'Транспортное средство';
    }

    /**
     * @return string
     */
    public function getEmailText()
    {
        $driverFio = $this->driver ? $this->driver->getFio() : null;
        $bodyType = $this->bodyType ? $this->bodyType->name : null;
        $trailerText = null;
        if ($this->vehicle_type_id == VehicleType::TYPE_TRACTOR && $this->semitrailerType) {
            $trailerText = 'Полуприцеп: ' . ($this->semitrailerType->model ? ($this->semitrailerType->model . ' ') : null) . ' ' . $this->semitrailerType->state_number;
        } elseif ($this->vehicle_type_id == VehicleType::TYPE_WAGON && $this->trailerType) {
            $trailerText = 'Прицеп: ' . ($this->trailerType->model ? ($this->trailerType->model . ' ') : null) . ' ' . $this->trailerType->state_number;
        }

        $text = <<<EMAIL_TEXT
Здравствуйте!

Водитель: {$driverFio}

Марка: {$this->model}
Номер ТС: {$this->state_number}
Тип кузова: {$bodyType}
Цвет ТС: {$this->color}
{$trailerText}

EMAIL_TEXT;

        return $text;
    }

    /**
     * @return bool
     */
    public function _save()
    {
        if ($this->save()) {
            /* @var $driver Driver */
            if ($this->driver_id) {
                $driver = Driver::find()
                    ->andWhere(['id' => $this->driver_id])
                    ->andWhere(['company_id' => $this->company_id])
                    ->one();
                if ($driver && $driver->vehicle_id !== $this->id) {
                    $driver->vehicle_id = $this->id;

                    return $driver->save(true, ['vehicle_id']);
                }
            } elseif (($driver = Driver::find()
                    ->andWhere(['vehicle_id' => $this->id])
                    ->andWhere(['company_id' => $this->company_id])
                    ->one()) !== null) {
                $driver->vehicle_id = null;

                return $driver->save(true, ['vehicle_id']);
            }

            return true;
        }
        return false;
    }

    /**
     * @return mixed
     * @throws \Throwable
     */
    public function _delete()
    {
        $model = $this;
        return Yii::$app->db->transaction(function (Connection $db) use ($model) {
            if ($model->driver) {
                $driver = $this->driver;
                $driver->vehicle_id = null;
                if (!$driver->save(true, ['vehicle_id'])) {
                    $db->transaction->rollBack();

                    return false;
                }
            }

            if ($model->ownSemitrailerVehicles) {
                foreach ($model->ownSemitrailerVehicles as $ownSemitrailerVehicle) {
                    $ownSemitrailerVehicle->semitrailer_type_id = null;
                    if (!$ownSemitrailerVehicle->save('true', ['semitrailer_type_id'])) {
                        $db->transaction->rollBack();

                        return false;
                    }
                }
            }
            if ($model->ownTrailerVehicles) {
                foreach ($model->ownTrailerVehicles as $ownTrailerVehicle) {
                    $ownTrailerVehicle->trailer_type_id = null;
                    if (!$ownTrailerVehicle->save('true', ['trailer_type_id'])) {
                        $db->transaction->rollBack();

                        return false;
                    }
                }
            }

            if (!$model->delete()) {
                $db->transaction->rollBack();

                return false;
            }

            return true;
        });
    }
}
