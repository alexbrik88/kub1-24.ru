<?php

namespace common\models\out;

use common\components\helpers\ArrayHelper;
use common\models\Company;
use common\models\product\Product;
use common\models\employee\Employee;
use common\models\service\SubscribeTariffGroup;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Url;

/**
 * This is the model class for table "out_invoice".
 *
 * @property string $id
 * @property string $demo_id
 * @property integer $company_id
 * @property integer $status
 * @property integer $is_outer_shopcart
 * @property string $note
 * @property string $return_url
 * @property boolean $show_article
 * @property boolean $strict_data
 * @property string $data_url
 * @property boolean $send_result
 * @property string $result_url
 * @property integer $send_with_stamp
 * @property integer $is_autocomplete
 * @property integer $is_bik_autocomplete
 * @property string $additional_number
 * @property integer $is_additional_number_before
 * @property string $invoice_comment
 * @property string $add_comment
 * @property integer $notify_to_email
 * @property integer $notify_to_telegram
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Company $company
 * @property Employee $createdBy
 * @property Employee $updatedBy
 * @property OutInvoiceProduct[] $outInvoiceProducts
 * @property Product[] $products
 */
class OutInvoice extends ActiveRecord
{
    const ACTIVE = 10;
    const INACTIVE = 1;
    const DELETED = 0;

    public static $statusAll = [
        self::ACTIVE => 'Активна',
        self::INACTIVE => 'Не активна',
        self::DELETED => 'Удалена',
    ];

    public static $statusAvailable = [
        self::ACTIVE => 'Активна',
        self::INACTIVE => 'Не активна',
    ];

    public $productId = [];

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\behaviors\TimestampBehavior',
            ],
            [
                'class' => 'yii\behaviors\BlameableBehavior',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'out_invoice';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'return_url'], 'required'],
            [['productId'], 'safe'],
            [['status'], 'in', 'range' => [self::ACTIVE, self::INACTIVE, self::DELETED]],
            [['return_url', 'data_url', 'result_url'], 'url', 'defaultScheme' => 'http'],
            [['is_outer_shopcart', 'show_article', 'strict_data', 'send_result'], 'boolean'],
            [['data_url'], 'required', 'when' => function ($model) {
                return (boolean) $model->strict_data;
            }],
            [['result_url'], 'required', 'when' => function ($model) {
                return (boolean) $model->send_result;
            }],
            [['note'], 'string'],
            [['additional_number'], 'string', 'max' => 45],
            [['invoice_comment', 'add_comment'], 'string', 'max' => 255],
            [
                [
                    'is_autocomplete',
                    'is_bik_autocomplete',
                    'is_additional_number_before',
                    'send_with_stamp',
                    'notify_to_email',
                    'notify_to_telegram',
                ],
                'boolean',
            ],
            [
                ['productId'], 'required',
                'when' => function ($model) {
                    return !$model->is_outer_shopcart;
                },
            ],
            [
                ['productId'], 'each', 'rule' => [
                    'exist',
                    'targetClass' => Product::class,
                    'targetAttribute' => 'id',
                    'filter' => [
                        'company_id' => $this->company->id,
                        'is_deleted' => false,
                        'not_for_sale' => false,
                    ]
                ],
                'when' => function ($model) {
                    return !$model->is_outer_shopcart;
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'demo_id' => 'Demo ID',
            'company_id' => 'Сомпания',
            'status' => 'Статус',
            'is_outer_shopcart' => 'Формировать корзину на сайте магазина',
            'note' => 'Примечание',
            'outUrl' => 'Адрес ссылки',
            'demoOutUrl' => 'Адрес ссылки (ДЕМО)',
            'return_url' => 'URL возврата',
            'show_article' => 'Выводить Артикул',
            'strict_data' => 'Запрашивать данные',
            'data_url' => 'URL данных',
            'send_result' => 'Отправлять результат',
            'result_url' => 'URL результата',
            'created_at' => 'Создан',
            'updated_at' => 'Изменен',
            'created_by' => 'Создал',
            'updated_by' => 'Изменил',
            'productId' => 'Доступные товары/услуги',
            'is_autocomplete' => 'Автозаполнение по ИНН',
            'is_bik_autocomplete' => 'Автозаполнение по БИК',
            'additional_number' => 'Доп номер',
            'is_additional_number_before' => 'К номеру счета Доп номер',
            'send_with_stamp' => 'Добавить в счет печать и подпись',
            'invoice_comment' => 'Добавить комментарий в счете',
            'add_comment' => 'Добавить комментарий к письму со счетом',
            'notify_to_email' => 'Получить уведомление на почту о новом счете',
            'notify_to_telegram' => 'Получить уведомление в Telegram о новом счете',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(Employee::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(Employee::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutInvoiceProducts()
    {
        return $this->hasMany(OutInvoiceProduct::className(), ['out_invoice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['id' => 'product_id'])
            ->viaTable('out_invoice_product', ['out_invoice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function setCreatedBy(Employee $user)
    {
        $this->created_by = $user->id;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function setUpdatedBy(Employee $user)
    {
        $this->updated_by = $user->id;
    }

    /**
     * @return Product[]
     */
    public function getSelectedProducts()
    {
        return $this->company->getProducts()->alias('product')->andWhere([
            'product.id' => $this->productId,
            'product.is_deleted' => false,
            'product.not_for_sale' => false,
        ])->all();
    }

    public function getOutUrl()
    {
        return Url::to(['/out/invoice/create', 'id' => $this->id], true);
    }

    public function getDemoOutUrl()
    {
        return ($this->demo_id) ? Url::to(['/out/invoice/create', 'id' => $this->demo_id], true) : null;
    }

    /**
     * @return Product[]
     */
    public function getStatusValue()
    {
        return isset(self::$statusAll[$this->status]) ? self::$statusAll[$this->status] : '---';
    }

    /**
     * @param  boolean $insert
     * @return boolean
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if ($insert) {

            do {
                $this->id = uniqid();
            } while (self::find()->where(['id' => $this->id])->exists() || self::find()->where(['demo_id' => $this->id])->exists());

            do {
                $this->demo_id = uniqid();
            } while (self::find()->where(['id' => $this->demo_id])->exists() || self::find()->where(['demo_id' => $this->demo_id])->exists());

            $this->company_id = $this->company->id;
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();

        $this->productId = $this->getProducts()->select('id')->column();
    }

    /**
     * @param  boolean $runValidation
     * @param  array   $attributeNames
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        if (parent::save($runValidation, $attributeNames)) {
            $this->unlinkAll('products', true);
            foreach ($this->getSelectedProducts() as $product) {
                $this->link('products', $product);
            }

            return true;
        }

        return false;
    }

    public function getIsPaid()
    {
        if (!$this->company)
            return false;

        if (ArrayHelper::getValue(Yii::$app->params, 'b2b-paid', false)) {
            return true;
        }

        return $this->company->getHasActualSubscription(SubscribeTariffGroup::B2B_PAYMENT);
    }
}
