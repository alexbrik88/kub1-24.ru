<?php

namespace common\models\out;

use common\models\product\Product;
use Yii;

/**
 * This is the model class for table "out_invoice_product".
 *
 * @property string $out_invoice_id
 * @property integer $product_id
 *
 * @property OutInvoice $outInvoice
 * @property Product $product
 */
class OutInvoiceProduct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'out_invoice_product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['out_invoice_id', 'product_id'], 'required'],
            [['product_id'], 'integer'],
            [['out_invoice_id'], 'string', 'max' => 50],
            [['out_invoice_id'], 'exist', 'skipOnError' => true, 'targetClass' => OutInvoice::className(), 'targetAttribute' => ['out_invoice_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'out_invoice_id' => 'Out Invoice ID',
            'product_id' => 'Product ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutInvoice()
    {
        return $this->hasOne(OutInvoice::className(), ['id' => 'out_invoice_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
