<?php

namespace common\models\rent;

use Yii;
use common\models\product\ProductUnit;

/**
 * This is the model class for table "rent_agreement_item".
 *
 * @property int $id
 * @property int $rent_agreement_id
 * @property int $rent_entity_id
 * @property int $number
 * @property int $price
 * @property float $discount
 * @property int $amount
 *
 * @property Entity $entity
 * @property RentAgreement $rentAgreement
 * @property ProductUnit $unit
 */
class RentAgreementItem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'rent_agreement_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['rent_entity_id', 'price'], 'required'],
            [['rent_entity_id'], 'integer'],
            [['rent_entity_id'], 'exist', 'skipOnError' => true, 'targetClass' => Entity::className(), 'targetAttribute' => ['rent_entity_id' => 'id']],
            [['discount', 'price'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'rent_agreement_id' => 'Rent Entity Rent ID',
            'rent_entity_id' => 'Rent Entity ID',
            'unit_id' => 'Unit ID',
            'price' => 'Price',
            'discount' => 'Discount',
            'amount' => 'Amount',
        ];
    }

    /**
     * Gets query for [[Entity]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEntity()
    {
        return $this->hasOne(Entity::className(), ['id' => 'rent_entity_id']);
    }

    /**
     * Gets query for [[RentAgreement]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getRentAgreement()
    {
        return $this->hasOne(RentAgreement::className(), ['id' => 'rent_agreement_id']);
    }

    /**
     * Gets query for [[Unit]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUnit()
    {
        return $this->hasOne(ProductUnit::className(), ['id' => 'unit_id']);
    }

    public function __clone()
    {
        $this->id = null;
        $this->rent_agreement_id = null;
        $this->setIsNewRecord(true);
        $this->populateRelation('rentAgreement', null);
        foreach ($this->behaviors as $behavior) {
            $behavior->owner = $this;
        }
    }
}
