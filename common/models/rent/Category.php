<?php

namespace common\models\rent;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * Учёт аренды техники: категория (тип) объекта
 * @property int    $id
 * @property string $title
 */
class Category extends ActiveRecord
{

    const ID_REALTY = 1; //ID категории "недвижимость"
    const ID_TECH = 2; //ID категории "спец. техника"
    const ID_DEVICE = 3; //ID категории "оборудование"
    const ID_WAGON_HOUSE = 4; //ID категории "Вагон-дом"

    public static function tableName()
    {
        return '{{%rent_category}}';
    }

    /**
     * Возвращает список категорий в формате ID=>Название
     * @return string[]
     */
    public static function flatList(): array
    {
        $categoryList = ArrayHelper::map(self::find()->asArray()->all(), 'id', 'title');
        foreach ($categoryList as $i => $item) {
            $categoryList[$i] = mb_strtoupper(mb_substr($item, 0, 1)) . mb_substr($item, 1);
        }
        return $categoryList;
    }

    public function rules()
    {
        return [
            ['title', 'required'],
            ['title', 'string', 'max' => 50]
        ];
    }

    public function attributeLabels()
    {
        return [
            'title' => 'Тип'
        ];
    }
}