<?php

namespace common\models\rent;

use common\components\date\DateHelper;
use common\models\Company;
use common\models\Contractor;
use common\models\employee\Employee;
use common\models\product\ProductGroup;
use common\models\product\ProductUnit;
use common\models\TaxRate;
use ErrorException;
use Throwable;
use yii\base\InvalidCallException;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Exception;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use common\models\product\Product;
use Yii;

/**
 * Учёт аренды техники: арендуемые объекты
 * @property int              $id
 * @property int              $company_id           ID компании
 * @property int              $owner_id             ID собственника
 * @property int              $category_id          ID категории объекта
 * @property int|null         $product_id           ID товара (услуги), закреплённого для счетов
 * @property string           $title                Название проекта
 * @property string           $ownership            Тип собственности
 * @property bool             $archive              Признак, что объек находится в архиве
 * @property bool             $deleted              Объект удален
 * @property string           $description          Описание
 * @property string           $comment              Комментарий
 * @property integer          $count                Колличество
 * @property integer          $useful_life_in_month Срок полезного использования
 * @property string           $purchased_at         Дата приобретения
 * @property integer          $purchased_price      Стоимость приобретения
 * @property integer          $created_at           Дата и время создания
 * @property integer          $created_by           Ответственный
 * @property integer          $retired              Выбывший
 *
 * @property AttributeValue[] $attributeValues      Значения атрибутов
 * @property Category         $category
 * @property Company          $company
 * @property Contractor       $owner
 * @property RentAgreement[]     $rents
 * @property RentAgreement[]     $currentRents         Список аренд, действующих сегодня
 * @property Product|null     $product
 * @property Employee         $employee             Ответственный
 * @property string           $status               Статус аренды
 * @property string           $statusLabel          Статус аренды
 * @property string           $ownershipLabel       Тип собственности
 */
class Entity extends ActiveRecord
{

    //Тип собственности
    const OWNERSHIP_TYPE = [self::OWNERSHIP_TYPE_SELF, self::OWNERSHIP_TYPE_RENT, self::OWNERSHIP_TYPE_AGENT];
    const OWNERSHIP_TYPE_SELF = 'self'; //в собственности
    const OWNERSHIP_TYPE_RENT = 'rent'; //аренда
    const OWNERSHIP_TYPE_AGENT = 'agent'; //агент
    const OWNERSHIP_TYPE_LABELS = [
        self::OWNERSHIP_TYPE_SELF => 'В собственности',
        self::OWNERSHIP_TYPE_RENT => 'В аренде',
        self::OWNERSHIP_TYPE_AGENT => 'Агент',
    ];

    const DEFAULT_CATEGORY_ID = 1; //категория по умолчанию для новых объектов

    //Соответствие ID категории - ID атрибута "цена" (минимальная)
    const ATTRIBUTE_BASE_PRICE = [
        Category::ID_REALTY => 'price',
        Category::ID_TECH => 'price',
        Category::ID_DEVICE => 'price',
        Category::ID_WAGON_HOUSE => 'price',
    ];
    const ADDITIONAL_STATUS_LIST = [
        Category::ID_REALTY => [
        ],
        Category::ID_TECH => [
            0 => '',
            1 => 'Дествующее ТО',
            2 => 'Необходимо ТО',
        ],
        Category::ID_DEVICE => [
        ],
        Category::ID_WAGON_HOUSE => [
            0 => '',
            1 => 'Дествующее ТО',
            2 => 'Необходимо ТО',
        ],
    ];

    // статус объекта
    const STATUS_FREE = 'free'; // свободен (сейчас и в будущем)
    const STATUS_RESERVED = 'reserved'; // в резерве
    const STATUS_RENT = 'rent'; // в аренде
    const STATUS_RETIRED = 'retired'; // выбыл
    const STATUS = [self::STATUS_FREE, self::STATUS_RENT, self::STATUS_RESERVED, self::STATUS_RETIRED];
    const STATUS_LABELS = [
        self::STATUS_FREE => 'Свободен',
        self::STATUS_RESERVED => 'В резерве',
        self::STATUS_RENT => 'В аренде',
        self::STATUS_RETIRED => 'Выбыл',
    ];

    protected $attrValues;

    public static function tableName()
    {
        return '{{%rent_entity}}';
    }

    /**
     * Возвращает экземпляр класса, инициированный обязательными значениями по умолчанию
     * @param int $companyId
     * @param int $createdBy
     * @return static
     */
    public static function instanceEntity(int $companyId, int $createdBy): self
    {
        $self = new static();
        $self->company_id = $companyId;
        $self->category_id = self::DEFAULT_CATEGORY_ID;
        $self->created_by = $createdBy;
        $self->purchased_at = date('Y-m-d');
        return $self;
    }

    /**
     * Скрывает (отправляет в архив) объекты аренды с проверкой на принадлежность указанной компании
     * @param int   $companyId ID компании
     * @param array $ids       Массив ID удаляемых объектов
     * @throws Throwable
     */
    public static function archiveMany(int $companyId, array $ids)
    {
        return (bool)static::updateAll(['archive' => true], ['company_id' => $companyId, 'id' => $ids]);
    }

    /**
     * Удаляет объекты аренды с проверкой на принадлежность указанной компании
     * Также удаляет изображения
     * @param int   $companyId ID компании
     * @param array $ids       Массив ID удаляемых объектов
     * @return bool
     * @throws Throwable
     */
    public static function deleteMany(int $companyId, array $ids) : bool
    {
        return (bool)static::updateAll(['deleted' => true], ['company_id' => $companyId, 'id' => $ids]);
    }

    /**
     * Делает объекты аренды выбывшими с проверкой на принадлежность указанной компании
     * Также удаляет изображения
     * @param int   $companyId ID компании
     * @param array $ids       Массив ID удаляемых объектов
     * @return bool
     * @throws Throwable
     */
    public static function retiredMany(int $companyId, array $ids)
    {
        $transaction = Yii::$app->getDb()->beginTransaction();
        try {
            foreach ($ids as $id) {
                $entity = static::findOne(['id' => $id]);

                $rents = $entity->getRents()
                    ->andWhere(['and',
                        ['entity_id' => $id],
                        ['<=', 'date_begin', new Expression('CURRENT_DATE()')],
                    ])
                    ->all();
                foreach ($rents as $rent) {
                    $rent->updateAttributes([
                        'deleted' => 1,
                        'date_end' => date(DateHelper::FORMAT_DATE),
                    ]);
                }

                $entity->updateAttributes([
                    'retired' => 1,
                ]);
            }
            $transaction->commit();
        } catch (\Exception $e) {
            $transaction->rollBack();
            Yii::$app->session->setFlash('errors', 'Некоторые договора аренды не были прекращены');
            return false;
        }

        return true;
    }

    /**
     * @param int $companyId
     * @return ActiveQuery
     */
    public static function findActual(int $companyId) {
        return self::find()->where([
            self::tableName() . '.company_id' => $companyId,
            self::tableName() . '.archive' => false,
            self::tableName() . '.deleted' => false
        ]);
    }

    public static function flatList(int $companyId)
    {
        return ArrayHelper::map(self::findActual($companyId)->orderBy('title')->asArray()->all(), 'id', 'title');
    }

    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => null
            ]
        ];
    }

    /**
     * @throws ErrorException
     */
    public function afterDelete()
    {
        if ($this->id) {
            FileHelper::removeDirectory($this->uploadPath());
        }
        parent::afterDelete();
    }

    public function rules()
    {
        return [
            [['archive', 'retired'], 'default', 'value' => false],
            [['company_id', 'owner_id', 'category_id', 'title', 'created_by', 'purchased_at'], 'required'],
            [['company_id', 'owner_id', 'category_id', 'product_id', 'count', 'purchased_price', 'useful_life_in_month', 'created_by'], 'integer'],
            ['title', 'string', 'min' => 3, 'max' => 150],
            ['ownership', 'in', 'range' => self::OWNERSHIP_TYPE],
            [['archive', 'retired', 'deleted'], 'boolean'],
            [['description', 'comment'], 'string', 'max' => 1200],
            [['purchased_at'], 'date', 'format' => 'php:d.m.Y'],
            [
                ['purchased_at'],
                'filter',
                'filter' => function ($value) {
                    if ($value) {
                        $value = explode('.', $value);
                        return $value[2] . '-' . $value[1] . '-' . $value[0];
                    }

                    return null;
                }
            ],

            [['created_at'], 'safe']
        ];
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Название объекта',
            'owner_id' => 'Собственник',
            'category_id' => 'Категория',
            'description' => 'Описание',
            'comment' => 'Комментарий',
            'typeLabel' => 'Категория',
            'count' => 'Колличество',
            'useful_life_in_month' => 'Срок полезного использования, мес.',
            'purchased_at' => 'Дата приобретения',
            'purchased_price' => 'Стоимость приобретения',
            'created_at' => 'Дата добавления',
            'created_by' => 'Ответственный',
            'status' => 'Статус',
            'ownership' => 'Собственность',
        ];
    }

    /**
     * Сохраняет дополнительные атрибуты в таблицу rent_attribute_value
     * @inheritDoc
     * @throws Exception
     */
    public function save($runValidation = true, $attributeNames = null)
    {
        $transaction = $this->getDb()->beginTransaction();
        if ($this->product_id) {
            $oldPrice = $this->price();
        } else {
            $oldPrice = null;
        }
        if (parent::save($runValidation, $attributeNames) === false) {
            return false;
        }
        //Если минимальная цена изменилась, то обновить цену связанного товара
        $price = $this->price();
        if ($this->product_id && $oldPrice != $price) {
            $this->product->price_for_sell_with_nds = $price;
            $this->product->save();
        }
        $transaction->commit();
        return true;
    }

    /**
     * Возвращает цену аренды объекта
     * @return float|null
     */
    public function price()
    {
        $attribute = Attribute::find()->where([
            'category_id' => $this->category_id,
            'alias' => self::ATTRIBUTE_BASE_PRICE[$this->category_id]
        ])->select('id')->scalar();

        if (!$this->id || !$attribute) {
            return null;
        }
        $attribute = AttributeValue::findOne(['entity_id' => $this->id, 'attribute_id' => $attribute]);
        if ($attribute === null) {
            return null;
        }
        return (float)$attribute->value;
    }

    /**
     * Возвращает массив всех дополнительных атрибутов
     * @return string[]
     */
    public function additionalAttributes(): array
    {
        if ($this->attrValues === null) {
            //Сырой запрос для экономии
            $this->attrValues = [];
            $this->attrValues = (new Query())->select(['value', 'alias', 'title'])
                ->from(['v' => AttributeValue::tableName()])
                ->innerJoin(['a' => Attribute::tableName()], 'a.id=v.attribute_id')
                ->where(['v.entity_id' => $this->id])->indexBy('alias')->all();
            foreach ($this->attrValues as $key => $item) {
                unset($this->attrValues[$key]['alias']);
            }
        }
        return $this->attrValues;
    }

    /**
     * Возвращает значение дополнительного атрибута
     * @param string $alias     Псевдоним атрибута
     * @param bool   $valueOnly Вернуть только значение атрибута
     * @return string|string[]|null
     */
    public function attributeValue(string $alias, bool $valueOnly = true)
    {
        $value = $this->additionalAttributes()[$alias] ?? null;
        if ($value !== null && $valueOnly === true) {
            $value = $value['value'];
        }
        return $value;
    }

    /**
     * Возвращает значение дополнительного атрибута
     * @param string $alias Псевдоним атрибута
     * @return string|null
     */
    public function getAttributeValue(string $alias)
    {
        return $this->additionalAttributes()[$alias] ?? null;
    }

    public function getAttributeValues(): ActiveQuery
    {
        return $this->hasMany(AttributeValue::class, ['entity_id' => 'id']);
    }

    public function getCategory(): ActiveQuery
    {
        return $this->hasOne(Category::class, ['id' => 'category_id']);
    }

    public function getCompany(): ActiveQuery
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    public function getOwner(): ActiveQuery
    {
        return $this->hasOne(Contractor::class, ['id' => 'owner_id']);
    }

    public function getRentAgreements(): ActiveQuery
    {
        return $this->hasMany(RentAgreement::class, ['id' => 'rent_agreement_id'])->viaTable('{{%rent_agreement_item}}', ['rent_entity_id' => 'id']);
    }

    public function getRents(): ActiveQuery
    {
        return $this->getRentAgreements();
    }

    public function getCurrentRents(): ActiveQuery
    {
        $date = date('Y-m-d');
        return $this->getRentAgreements()
            ->andOnCondition(['<=', 'date_begin', $date])
            ->andOnCondition(['>=', 'date_end', $date])
            ->andOnCondition(['deleted' => false]);
    }

    public function getEmployee()
    {
        return $this->hasOne(Employee::class, ['id' => 'created_by']);
    }

    /**
     * @return ActiveQuery
     * @throws Exception
     * TODO: Товар может быть "мягко" удалён, на это нужно реагировать
     */
    public function getProduct(): ActiveQuery
    {
        if ($this->product_id === null) {
            $this->_createProduct();
            $this->save(false, ['product_id']);
        }
        return $this->hasOne(Product::class, ['id' => 'product_id']);
    }

    /**
     * Возвращает путь к папке, содержащей изображения объекта
     * @param int  $id   ID модели
     * @param bool $temp Путь ко временному директорию
     * @return string
     */
    protected function uploadPath(int $id = null, bool $temp = false): string
    {
        if ($id === null) {
            $id = $this->id;
        }
        if ($id === null) {
            throw new InvalidCallException('You have to save model before');
        }
        $path = Company::fileUploadPath($this->company_id) . DIRECTORY_SEPARATOR . 'rent' . DIRECTORY_SEPARATOR . $id;
        if ($temp === true) {
            $path .= DIRECTORY_SEPARATOR . 'temp';
        }
        return $path;
    }

    private function _createProduct()
    {
        $price = $this->price();
        $product = new Product();
        $product->scenario = Product::SCENARIO_PROD_USLUG;
        $product->creator_id = Yii::$app->user->id;
        $product->company_id = $this->company_id;
        $product->production_type = Product::PRODUCTION_TYPE_SERVICE;
        $product->title = 'Аренда ' . $this->title;
        $product->excise = 'Без акциза';
        $product->product_unit_id = ProductUnit::UNIT_COUNT;
        $product->price_for_buy_nds_id = TaxRate::RATE_WITHOUT;
        $product->price_for_buy_with_nds = 0;
        $product->price_for_sell_nds_id = TaxRate::RATE_20;
        $product->price_for_sell_with_nds = $price;
        $product->country_origin_id = 1;
        $product->group_id = ProductGroup::WITHOUT;
        $product->save();
        $this->populateRelation('product', $product);
        $this->product_id = $product->id;
    }

    /**
     * Возвращает текущий статус объекта
     * @return string
     */
    public function getStatus()
    {
        if (!empty($this->getCurrentRents()->exists())) {
            return self::STATUS_RENT;
        }

        if ($this->getRents()->where(['>', 'date_begin', date('Y-m-d')])->exists()) {
            return self::STATUS_RESERVED;
        }

        if ($this->retired) {
            return self::STATUS_RETIRED;
        }

        return self::STATUS_FREE;
    }

    public function getStatusLabel()
    {
        return self::STATUS_LABELS[$this->status];
    }

    public function getOwnershipLabel()
    {
        return self::OWNERSHIP_TYPE_LABELS[$this->ownership];
    }

    /**
     * @param $condition
     */
    public function setOwnershipRent($condition)
    {
        if ($condition) {
            $this->ownership = self::OWNERSHIP_TYPE_RENT;
        }
    }

    /**
     * @return int|null
     */
    public function getAdditionalStatusList()
    {
        return self::ADDITIONAL_STATUS_LIST[$this->category_id];
    }

    /**
     * @return int|null
     */
    public function getAdditionalStatusLabel()
    {
        return $this->getAdditionalStatusList()[$this->getAdditionalStatus()] ?? '';
    }

    /**
     * @return int|null
     */
    public function getAdditionalStatus()
    {
        switch ($this->category_id) {
            case Category::ID_TECH:
                return $this->getWagonHouseAdditionalStatus();
                break;
            case Category::ID_WAGON_HOUSE:
                return $this->getWagonHouseAdditionalStatus();
                break;

            default:
                return null;
                break;
        }
    }

    /**
     * @return int|null
     */
    public function getHasAdditionalStatus()
    {
        switch ($this->category_id) {
            case Category::ID_TECH:
            case Category::ID_WAGON_HOUSE:
                return true;
                break;

            default:
                return false;
                break;
        }
    }

    /**
     * @return int|null
     */
    public function getAdditionalStatusStyle()
    {
        switch ($this->category_id) {
            case Category::ID_TECH:
            case Category::ID_WAGON_HOUSE:
                if ($this->getAdditionalStatus() == 2) {
                    return [
                        'background-color' => '#e30611',
                        'border-color' => '#e30611',
                        'color' => '#ffffff',
                    ];
                } elseif ($this->getAdditionalStatus() == 1) {
                    return [
                        'background-color' => '#26cd58',
                        'border-color' => '#26cd58',
                        'color' => '#ffffff',
                    ];
                }
                return [];
                break;

            default:
                return [];
                break;
        }
    }

    /**
     * @return int
     */
    public function getTechAdditionalStatus()
    {
        $validityPeriod = $this->attributeValue('validityPeriod');
        if ($validityPeriod && ($validityDate = date_create_from_format('d.m.Y', $validityPeriod))) {
            $currentDate = date_create();
            if ($currentDate > $validityDate) {
                return 2;
            } else {
                return 1;
            }
        }

        return 0;
    }

    /**
     * @return int
     */
    public function getWagonHouseAdditionalStatus()
    {
        return $this->getTechAdditionalStatus();
    }

    /**
     * Обнуление аттрибутов после клонирования
     */
    public function __clone()
    {
        /** @var AttributeValue $attributeValues */
        $attributeValues = $this->getAttributeValues()->indexBy('attribute_id')->all();
        foreach ($attributeValues as $value) {
            $value->entity_id = null;
        }

        $this->id = null;
        $this->created_at = null;
        $this->created_by = null;
        $this->setIsNewRecord(true);
        $this->populateRelation('attributeValues', $attributeValues);
        foreach ($this->behaviors as $behavior) {
            $behavior->owner = $this;
        }
    }
}