<?php

namespace common\models\rent;

use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * Учёт аренды техники: дополнительный атрибут объекта аренды
 * @property int         $id
 * @property int         $category_id  ID категории
 * @property string      $alias        Псевдоним
 * @property string      $title        Название атрибута
 * @property string      $type         Тип данных атрибута
 * @property bool        $required     Обязательный параметр
 * @property string|null $default      Значение по умолчанию
 * @property int         $sort         Индекс сортировки
 * @property string|null $containerCSS CSS-класс контейнера поля
 * @property array|null  $data         Дополнительные параметры (JSON)
 * @property string      $tab          Таб для отображения
 * @property bool        $show         Показывать/Не показывать
 */
class Attribute extends ActiveRecord
{
    //Тип данных атрибута
    const TYPE = [self::TYPE_INTEGER, self::TYPE_FLOAT, self::TYPE_STRING, self::TYPE_ENUM, self::TYPE_SET, self::TYPE_DATE];
    const TYPE_INTEGER = 'integer'; //целое число
    const TYPE_FLOAT = 'float'; //дробное число
    const TYPE_STRING = 'string'; //строка
    const TYPE_ENUM = 'enum'; //один из вариантов в списке
    const TYPE_SET = 'set'; //несколько вариантов из списка
    const TYPE_DATE = 'date'; //дата 'дд.мм.гггг'

    public static function tableName()
    {
        return '{{%rent_attribute}}';
    }

    /**
     * @param int $categoryId
     * @param string|null $tab
     * @return array
     */
    public static function getArrayByCategory(int $categoryId, string $tab = null)
    {
        $query = static::find()->where(['category_id' => $categoryId, 'show' => 1])->orderBy('sort');

        if ($tab) {
            return ArrayHelper::map(
                $query->andWhere(['tab' => $tab])->all(),
                'id',
                function($model) { return $model; },
            );
        }

        return ArrayHelper::map(
            $query->all(),
            'id',
            function($model) { return $model; },
            'tab'
        );
    }

    public function rules()
    {
        return [
            [['category_id', 'title', 'type', 'tab'], 'required'],
            ['title', 'string', 'max' => 30],
            ['type', 'in', 'range' => self::TYPE],
            ['default', 'string'],
        ];
    }

    public function afterFind()
    {
        if ($this->data !== null) {
            /** @noinspection PhpParamsInspection */
            $this->data = json_decode($this->data, true);
        }
        $this->required = (bool)$this->required;
    }
}