<?php


namespace common\models\rent;

use common\components\date\DatePickerFormatBehavior;
use common\components\helpers\ArrayHelper;
use common\models\Agreement;
use common\models\Company;
use common\models\Contractor;
use common\models\EmployeeCompany;
use common\models\document\AbstractDocument;
use common\models\document\DocumentType;
use common\models\document\Invoice;
use common\models\document\Order;
use common\models\document\OrderHelper;
use common\models\document\ScanDocument;
use common\models\document\status\AgentReportStatus;
use common\models\document\status\AgreementStatus;
use common\models\employee\Employee;
use common\models\file\File;
use common\models\product\Product;
use common\models\product\ProductUnit;
use frontend\models\Documents;
use frontend\models\log\Log;
use frontend\models\log\LogEvent;
use frontend\modules\documents\components\InvoiceHelper;
use Throwable;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\Exception;
use yii\db\StaleObjectException;
use yii\helpers\Html;

/**
 * Учёт арнеды техники: аренды
 * @property int         $id
 * @property int         $contractor_id              ID контрагента (арендатора)
 * @property string      $date_begin                 Дата начала аренды
 * @property string      $date_end                   Дата окончания аренды
 * @property float       $amount                     Сумма
 * @property int         $agreement_id               ID договора
 * @property int|null    $created_at                 Дата добавления
 * @property int|null    $document_author_id         Ответственный
 * @property bool        $deleted                    Удален
 * @property string      $document_number            Номер документа
 * @property string|null $document_additional_number Доп. номер документа
 * @property string      $document_date
 * @property integer     $type (Только исходящие)
 * @property integer     $payment_period             Период оплаты
 * @property integer     $has_discount               Указать скидку
 * @property integer     $comment                    Комментарий
 *
 * @property Agreement  $agreement
 * @property Entity     $entity
 * @property Invoice    $invoice
 * @property Contractor $contractor
 * @property Employee   $employee
 * @property int        $daysLeft
 * @property bool       $isActive
 * @property string     $statusLabel
 */
class RentAgreement extends \yii\db\ActiveRecord // AbstractDocument
{
    const STATUS_PROCEED = '1';
    const STATUS_COMPLETED = '2';

    const STATUS = [self::STATUS_PROCEED, self::STATUS_COMPLETED];

    public static $statuses = [
        self::STATUS_PROCEED => 'Действует',
        self::STATUS_COMPLETED => 'Завершен',
    ];

    public $status;

    const PAYMENT_PERIOD_DAY = 1;
    const PAYMENT_PERIOD_MONTH = 2;
    const PAYMENT_PERIOD_YEAR = 3;
    const PAYMENT_PERIOD_ALL = 4;
    const PAYMENT_PERIOD_DEFAULT = self::PAYMENT_PERIOD_MONTH;
    static $paymenPeriodList = [
        self::PAYMENT_PERIOD_DAY => 'День',
        self::PAYMENT_PERIOD_MONTH => 'Месяц',
        self::PAYMENT_PERIOD_YEAR => 'Год',
        self::PAYMENT_PERIOD_ALL => 'За весь период',
    ];

    protected $printablePrefix = 'Договор аренды';

    public static $uploadDirectory = 'rent';

    /**
     * @var string
     */
    public $urlPart = 'rent-agreement';

    /**
     * @return ActQuery
     */
    public static function find()
    {
        return new RentAgreementQuery(get_called_class());
    }

    public static function tableName()
    {
        return '{{%rent_agreement}}';
    }

    /**
     * @inheritdoc
     */
    public function getFullNumber()
    {
        return $this->document_number . $this->document_additional_number;
    }

    /**
     * @inheritdoc
     * @throws NotSupportedException
     */
    public function getLogMessage(Log $log)
    {
        $title = 'Агентский отчет №';

        $ioType = $log->getModelAttributeNew('type');
        if ($ioType == Documents::IO_TYPE_IN) {
            $title .= $log->getModelAttributeNew('document_number');
        } else {
            $title .= $log->getModelAttributeNew('document_number');
        }

        $link = Html::a($title, [
            '/documents/agent-report/view',
            'type' => $log->getModelAttributeNew('type'),
            'id' => $log->getModelAttributeNew('id'),
        ]);

        switch ($log->log_event_id) {
            case LogEvent::LOG_EVENT_CREATE:
                return $link . ' был создан.';
            case LogEvent::LOG_EVENT_DELETE:
                return $title . ' был удалён.';
            case LogEvent::LOG_EVENT_UPDATE:
                return $link . ' был изменен.';
            case LogEvent::LOG_EVENT_UPDATE_STATUS:
                return $link . ' статус "' . AgentReportStatus::findOne($log->getModelAttributeNew('status_out_id'))->name . '"';
            default:
                return $log->message;
        }
    }
    const SCENARIO_CREATE = 'create';
    const SCENARIO_UPDATE = 'update';

    const STATUS_ACTIVE = 'active';
    const STATUS_FINISHED = 'finished';
    const STATUS_LABEL = [
        self::STATUS_ACTIVE => 'Действует',
        self::STATUS_FINISHED => 'Окончен'
    ];

    /**
     * Возвращает список контрагентов
     * @param int $companyId ID компании
     * @return string[]
     */
    public static function contractorList(int $companyId): array
    {
        $data = [];
        foreach (Contractor::find()->select(['id', 'name', 'company_type_id'])->where([
            'company_id' => $companyId,
            'type' => Contractor::TYPE_CUSTOMER,
            'status' => 1
        ])->all() as $item) {
            $data[$item->id] = $item->nameWithType;
        }
        return $data;
    }

    /**
     * Возвращает список действующих договоров
     * @param int|null $contractorId ID контрагента
     * @param int      $companyId    ID компании
     * @return string[]
     */
    public static function agreementList($contractorId, int $companyId): array
    {
        if ($contractorId === null) {
            return [];
        }
        return array_map(function (Agreement $item) {
            return $item->getListItemName();
        }, Agreement::find()
            ->where(['company_id' => $companyId])
            ->andWhere(['contractor_id' => $contractorId])
            ->andWhere(['NOT IN', 'status_id', [AgreementStatus::STATUS_REJECTED, AgreementStatus::STATUS_FINISHED]])
            ->indexBy('id')->all());
    }

    public static function findByFilter(int $companyId, int $entityId = null, int $contractorId = null, int $employeeId = null, bool $deleted = false): ActiveQuery
    {
        $query = static::find()->joinWith([
            'entity',
            'contractor',
            'agreement',
        ], true)
        ->where([self::tableName() . '.deleted' => (int)$deleted]);

        if ($contractorId !== null) {
            $query->andWhere([self::tableName() . '.contractor_id' => $contractorId]);
        }
        if ($employeeId !== null) {
            $query->andWhere([self::tableName() . '.document_author_id' => $employeeId]);
        }
        return $query;
    }

    /**
     * @param int $companyId ID компании
     * @param array $id ID аренды
     * @return bool
     */
    public static function deleteMany(int $companyId, array $id): bool
    {
        $rents = self::find()->joinWith('invoices invoice')->andWhere([
            self::tableName() . '.id' => $id,
            self::tableName() . '.company_id' => $companyId,
            'invoice.id' => null,
        ])->all();

        if (empty($rents)) {
            return false;
        }

        return boolval(self::updateAll(['deleted' => true], ['id' => ArrayHelper::getColumn($rents, 'id')]));
    }

    public function behaviors()
    {
        return [
            'documentDateBehavior' => [
                'class' => DatePickerFormatBehavior::className(),
                'attributes' => [
                    'document_date' => ['message' => 'Дата документа указана неверно.'],
                    'date_begin' => ['message' => 'Дата начала аренды указана неверно.'],
                    'date_end' => ['message' => 'Дата окончания аренды указана неверно.'],
                ],
            ],
            'documentCreatedAtBehavior' => [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
            ],
        ];
    }

    public function rules()
    {
        return [
            [['type'], 'default', 'value' => Documents::IO_TYPE_OUT, 'on' => 'insert'],
            [['contractor_id', 'document_author_id'], 'required'],
            [['amount'], 'integer', 'min' => 0],
            [['date_begin', 'date_end'], 'safe'],
            [['date_end'], 'validatePeriod'],
            [
                ['document_number'],
                'unique',
                'filter' => function ($query) {
                    $query->andWhere(['company_id' => $this->company_id]);
                    if ($d = date_create($this->document_date)) {
                        $query->andWhere(['between', 'document_date', $d->format('Y-01-01'), $d->format('Y-12-31')]);
                    }
                }
            ],
            [
                ['contractor_id'],
                'exist',
                'targetClass' => Contractor::class,
                'targetAttribute' => 'id',
                'filter' => function ($query) {
                    $query->andWhere(['company_id' => $this->company_id]);
                },
            ],
            [
                ['agreement_id'],
                'exist',
                'targetClass' => Agreement::class,
                'targetAttribute' => 'id',
                'filter' => function ($query) {
                    $query->andWhere(['company_id' => $this->company_id]);
                },
            ],
            [
                ['document_author_id'],
                'exist',
                'targetClass' => EmployeeCompany::class,
                'targetAttribute' => 'employee_id',
                'filter' => function ($query) {
                    $query->andWhere(['company_id' => $this->company_id]);
                },
            ],
            [['payment_period'], 'in', 'range' => array_keys(RentAgreement::$paymenPeriodList)],
        ];
    }

    public function validatePeriod($attribute, $params)
    {
        $start = date_create($this->date_begin);
        $end = date_create($this->date_end);
        if ($start > $end) {
            $this->addError($attribute, 'Дата начала аренды не может быть болшьше даты её окончания.');

            return;
        }
    }

    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'contractor_id' => 'Арендатор',
            'date_begin' => 'Дата начала',
            'date_end' => 'Дата окончания',
            'amount' => 'Сумма',
            'agreement_id' => 'Договор',
            'created_at' => 'Дата добавления',
            'days_left' => 'Осталось дней',
            'document_number' => 'Номер документа',
            'document_additional_number' => 'Доп. номер документа',
            'payment_period' => 'Период оплаты',
            'has_discount' => 'Указать скидку',
            'comment' => 'Комментарий',
        ];
    }

    /**
     * @inheritdoc
     */
    public function getFile()
    {
        return $this->hasOne(File::className(), ['owner_id' => 'id'])
            ->onCondition(['owner_model' => static::class,]);
    }

    /**
     * @inheritdoc
     */
    public function getFiles()
    {
        return $this->hasMany(File::className(), ['owner_id' => 'id'])
            ->onCondition([File::tableName() . '.owner_model' => static::class,]);
    }

    /**
     * @inheritdoc
     */
    public function getScanDocuments()
    {
        return $this->hasMany(ScanDocument::className(), ['owner_id' => 'id'])
            ->onCondition([ScanDocument::tableName() . '.owner_model' => static::class,]);
    }

    /**
     * @return boolean
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            if (!$this->getInvoices()->exists()) {
                return true;
            }
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function delete()
    {
        if (!$this->beforeDelete()) {
            return false;
        }

        $this->updateAttributes([
            'deleted' => 1,
        ]);

        $this->afterDelete();

        return 1;
    }

    /**
     * @return bool|null
     * @throws Exception
     */
    public function createInvoice()
    {
        $company = $this->company;
        $contractor = $this->contractor;
        $delay = isset($contractor->customer_payment_delay) ? $contractor->customer_payment_delay : $company->$contractor->customer_payment_delay;
        $invoice = new Invoice();
        $invoice->company_id = $this->company_id;
        $invoice->populateRelation('company', $this->company);
        $invoice->type = $this->type ?: Documents::IO_TYPE_OUT;
        $invoice->contractor_id = $this->contractor_id;
        $invoice->document_number = (string)Invoice::getNextDocumentNumber($this->company, 2);
        $invoice->document_date = date('Y-m-d');
        $invoice->payment_limit_date = date_create(sprintf('+%s days', $delay))->format('Y-m-d');
        $invoice->has_discount = $this->has_discount;
        $invoice->discount_type = $this->discount_type;
        $invoice->nds_view_type_id = $company->nds_view_type_id;

        $orders = [];
        foreach ($this->items as $item) {
            $product = Product::findOne(['id' => $item->entity->product_id]);
            if (!$product) {
                return false;
            }

            $order = OrderHelper::createOrderByProduct($product, $invoice, 1, $item->price, $item->discount);
            $order->unit_id = ProductUnit::UNIT_COUNT;
            $orders[] = $order;
        }
        if (count($orders) == 0) {
            return false;
        }

        $invoice->populateRelation('orders', $orders);

        if (InvoiceHelper::save($invoice)) {
            $this->link('invoices', $invoice);
            $this->populateRelation('invoice', $invoice);
            $this->populateRelation('invoices', [$invoice]);

            return true;
        }

        return true;
    }

    public function getAgreement(): ActiveQuery
    {
        return $this->hasOne(Agreement::class, ['id' => 'agreement_id']);
    }

    public function getEntity(): ActiveQuery
    {
        return $this->hasOne(Entity::class, ['id' => 'rent_entity_id'])->viaTable('{{%rent_agreement_item}}', ['rent_agreement_id' => 'id']);
    }

    public function getEntities(): ActiveQuery
    {
        return $this->hasMany(Entity::class, ['id' => 'rent_entity_id'])->viaTable('{{%rent_agreement_item}}', ['rent_agreement_id' => 'id']);
    }

    public function getInvoices(): ActiveQuery
    {
        return $this->hasOne(Invoice::class, [
            'id' => 'invoice_id',
        ])->viaTable('{{%rent_agreement_invoice}}', [
            'rent_agreement_id' => 'id',
        ]);
    }

    public function getInvoice(): ActiveQuery
    {
        return $this->hasOne(Invoice::class, [
            'id' => 'invoice_id',
        ])->viaTable('{{%rent_agreement_invoice}}', [
            'rent_agreement_id' => 'id',
        ])->orderBy([
            'document_date' => SORT_DESC,
        ]);
    }

    public function getContractor(): ActiveQuery
    {
        return $this->hasOne(Contractor::class, ['id' => 'contractor_id']);
    }

    public function getEmployee()
    {
        return $this->hasOne(Employee::class, ['id' => 'document_author_id']);
    }

    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    public function getItems(): ActiveQuery
    {
        return $this->hasMany(RentAgreementItem::class, ['rent_agreement_id' => 'id']);
    }

    /**
     * Кол-во дней до завершения
     * @return int
     * @throws \Exception
     */
    public function getDaysLeft()
    {
        $diff = date_diff(new \DateTime($this->date_end), new \DateTime());

        return $diff->invert ? $diff->days : -$diff->days;
    }

    /**
     * @return bool
     */
    public function getIsActive()
    {
        return $this->date_end >= date('Y-m-d');
    }

    /**
     * @return string
     */
    public function getPaymentPeriod()
    {
        return self::$paymenPeriodList[$this->payment_period] ?? '';
    }

    /**
     * @return string
     */
    public function getStatusLabel()
    {
        return self::STATUS_LABEL[$this->isActive ? self::STATUS_ACTIVE : self::STATUS_FINISHED];
    }

    public static function getNextNumber(Company $company) {
        return (int) self::find()->alias('t')->joinWith('entity e')->andWhere([
            'e.company_id' => $company->id,
        ])->andWhere([
            'between', 't.document_date', date('Y-01-01'), date('Y-12-31')
        ])->max('document_number') + 1;
    }

    public function __clone()
    {
        $oldItems = $this->items;
        $items = [];
        $this->id = null;
        $this->created_at = null;
        $this->document_number = null;
        $this->document_author_id = null;
        $this->setIsNewRecord(true);
        $this->populateRelation('invoices', null);
        $this->populateRelation('invoice', null);
        foreach ($this->behaviors as $behavior) {
            $behavior->owner = $this;
        }
        foreach ($oldItems as $oldItems) {
            $item = clone $oldItems;
            $item->populateRelation('rentAgreement', $this);
            $items[] = $item;
        }
        $this->populateRelation('items', $items);
    }
}
