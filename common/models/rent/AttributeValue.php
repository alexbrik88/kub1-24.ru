<?php

namespace common\models\rent;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Учёт аренды техники: значения дополнительных атрибутов
 * @property int       $entity_id    ID арендуемого объекта
 * @property int       $attribute_id ID атрибута
 * @property string    $value        Значение
 *
 * @property Attribute $attributeClass
 */
class AttributeValue extends ActiveRecord
{
    public static function tableName()
    {
        return '{{%rent_attribute_value}}';
    }

    public function rules()
    {
        return [
            [['entity_id', 'attribute_id'], 'required'],
            [['entity_id', 'attribute_id'], 'integer'],
            ['attribute_id', 'unique', 'targetAttribute' => ['entity_id', 'attribute_id']],
            [
                'value',
                'filter',
                'filter' => function ($value) {
                    if (is_array($value) === true && $this->attributeClass->type === Attribute::TYPE_SET) {
                        $value = json_encode($value, JSON_UNESCAPED_UNICODE);
                    }
                    return $value;
                }
            ],
            ['value', 'string'],
            ['value', 'date', 'format' => 'php:d.m.Y', 'when' => function ($model) {
                return $model->attributeClass->type === Attribute::TYPE_DATE;
            }],
        ];
    }

    public function afterFind()
    {
        parent::afterFind();
        if ($this->value && $this->value[0] === '[') {
            $this->value = json_decode($this->value);
        }
    }

    public function getAttributeClass(): ActiveQuery
    {
        return $this->hasOne(Attribute::class, ['id' => 'attribute_id']);
    }

    /**
     * @param $execute
     * @param $attribute
     * @param $value
     * @return bool
     */
    public function hook($execute, $value = ''):bool
    {
        if (!$execute || $this->value) {
            return true;
        }

        try {
            $this->updateAttributes(['value' => $value]);
        } catch (\Exception $e) {
            return false;
        }

        return true;
    }
}