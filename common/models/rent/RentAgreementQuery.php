<?php

namespace common\models\rent;

use common\models\ICompanyStrictQuery;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Class RentAgreementQuery
 *
 * @package common\models\document
 */
class RentAgreementQuery extends ActiveQuery implements ICompanyStrictQuery
{
    use \common\components\TableAliasTrait;

    /**
     * @param $companyId
     *
     * @return $this
     */
    public function byCompany($companyId)
    {
        return $this->andWhere([$this->getTableAlias().'.company_id' => $companyId]);
    }
}
