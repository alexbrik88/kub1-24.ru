<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 12/9/15
 * Time: 4:02 PM
 * Email: t.kanstantsin@gmail.com
 */

namespace common\models;

use common\components\DadataClient;
use common\components\TextHelper;
use common\models\company\CompanyType;
use common\models\company\CompanyImageUploadLog;
use common\models\company\DetailsFile;
use common\models\company\DetailsFileStatus;
use common\models\company\DetailsFileType;
use common\models\employee\Employee;
use common\models\User;
use frontend\models\Documents;
use Yii;
use yii\web\UploadedFile;
use Intervention\Image\ImageManagerStatic as Image;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;

/**
 * Class CompanyHelper
 * @package common\models
 */
class CompanyHelper
{

    public static $profileFullnessAttributes = [
        CompanyType::TYPE_IP => [
            'name_short', 'name_full', 'company_type_id', 'taxation_type_id', 'phone', 'email',
            'chief_post_name', 'chief_lastname', 'chief_firstname', 'chief_firstname_initials',
            'egrip', 'inn', 'rs', 'bik', 'name', 'ks', 'okpo', 'okved', 'oktmo', 'fss', 'pfr_ip', 'pfr_employer',
            'logo_link', 'print_link', 'chief_signature_link',
        ],
        CompanyType::TYPE_OOO => [
            'name_short', 'name_full', 'company_type_id', 'taxation_type_id', 'phone', 'email',
            'chief_post_name', 'chief_lastname', 'chief_firstname', 'chief_firstname_initials',
            'ogrn', 'inn', 'kpp', 'rs', 'bik', 'bank_name', 'ks', 'okpo', 'okogu', 'okato', 'okved', 'okfs', 'okopf', 'pfr', 'fss', 'tax_authority_registration_date',
            'logo_link', 'print_link', 'chief_signature_link',
        ],
    ];

    /**
     * @param Company $company
     * @return string
     */
    public static function getProfileFullness(Company $company)
    {
        $attributes = $company->company_type_id == CompanyType::TYPE_IP
            ? static::$profileFullnessAttributes[CompanyType::TYPE_IP]
            : static::$profileFullnessAttributes[CompanyType::TYPE_IP];

        $emptyAttributes = array_filter($attributes, function ($attribute) use ($company) {
            return $company->hasAttribute($attribute) && !empty($company->$attribute);
        });

        return bcmul(bcdiv(count($emptyAttributes), count($attributes), 2), 100, 0);
    }

    /**
     * @param Company $model
     * @param bool|false $inInvoice
     * @return bool
     */
    public static function load(Company $model, $inInvoice = false)
    {
        if (!$inInvoice) {
            if ($model->load(Yii::$app->request->post())) {
                $model->logoImage = UploadedFile::getInstance($model, 'logoImage');
                $model->printImage = UploadedFile::getInstance($model, 'printImage');
                $model->chiefSignatureImage = UploadedFile::getInstance($model, 'chiefSignatureImage');

                return true;
            }
        } else {
        //    if ($model->load(Yii::$app->request->post())) {
                if (!empty($_FILES)) {
                    $model->logoImage = isset($_FILES['logoImage']) ? $_FILES['logoImage'] : null;
                    $model->printImage = isset($_FILES['printImage']) ? $_FILES['printImage'] : null;
                    $model->chiefSignatureImage = isset($_FILES['chiefSignatureImage']) ? $_FILES['chiefSignatureImage'] : null;
                }

                return true;
          //  }
        }

        return false;
    }

    /**
     * @param Company $model
     * @param bool|false $inInvoice
     * @return bool
     */
    public static function validate(Company $model, $inInvoice = false)
    {
        if ($inInvoice) {
            $formats = ['gif', 'jpg', 'png', 'jpeg', 'bmp', 'pdf',];
            foreach ([
                         'logoImage' => $model->logoImage,
                         'printImage' => $model->printImage,
                         'chiefSignatureImage' => $model->chiefSignatureImage,] as $attribute => $image) {
                if ($image !== null && !in_array(pathinfo($image['name'], PATHINFO_EXTENSION), $formats)) {
                    $model->addError($attribute, 'Разрешена загрузка файлов только со следующими расширениями: gif, jpg, png, jpeg, bmp, pdf.');
                    $model->logoImage = null;
                    $model->printImage = null;
                    $model->chiefSignatureImage = null;

                    return false;
                }
            }

            return true;
        }

        return $model->validate();
    }

    /**
     * @param Company $model
     * @param bool|false $inInvoice
     * @return bool
     * @throws \yii\base\Exception
     */
    public static function save(Company $model, $inInvoice = false)
    {
        // logoImage
        if ($model->logoImage) {
            self::saveImage($model, 'logoImage', $this->logoImage);
            $model->logoImage = null;
        } elseif ($model->deleteLogoImage) {
            self::deleteImage($this, 'logoImage');
        }

        // printImage

        if ($model->printImage) {
            self::saveImage($model, 'printImage', $this->printImage);
            $model->printImage = null;
        } elseif ($model->deletePrintImage) {
            self::deleteImage($this, 'printImage');
        }

        // chiefSignatureImage

        if ($model->chiefSignatureImage) {
            self::saveImage($model, 'chiefSignatureImage', $this->chiefSignatureImage);
            $model->chiefSignatureImage = null;
        } elseif ($model->deleteChiefSignatureImage) {
            self::deleteImage($this, 'chiefSignatureImage');
        }
    }

    public static function deleteImage(Company $company, $attribute, $tmp = false)
    {
        if ($typeId = DetailsFileType::findTypeId($attribute)) {
            if ($tmp) {
                $company->deleteTemporaryImages($typeId);
            } else {
                $prop = 'delete'.ucfirst($attribute).'All';
                if (property_exists($company, $prop) && $company->$prop) {
                    if ($imageModel = $company->getImageModel($typeId)) {
                        $imageModel->deleteFromAll();
                    }
                    //DetailsFile::deleteFromAllDocuments($company->id, $typeId);
                }
                DetailsFile::updateAll(['status_id' => null], [
                    'company_id' => $company->id,
                    'type_id' => $typeId,
                    'status_id' => DetailsFileStatus::STATUS_ACTIVE,
                ]);
            }
        }
    }

    public static function saveImage(Company $company, $attribute, UploadedFile $file, $tmp = false)
    {
        $imageName = $company->getImageName($attribute);
        if ($imageName && !$file->hasError) {
            if ($typeId = DetailsFileType::findTypeId($attribute)) {
                $model = new DetailsFile([
                    'company_id' => $company->id,
                    'type_id' => $typeId,
                    'status_id' => $tmp ? DetailsFileStatus::STATUS_TEMPORARY : DetailsFileStatus::STATUS_ACTIVE,
                    'file' => $file,
                ]);
                if ($model->save()) {
                    if ($attribute !== 'logoImage') {
                        $path = $model->getUploadPath();
                        $origFile = $path . DIRECTORY_SEPARATOR . $model->name;
                        $newName = uniqid() . '.png';
                        $newFile = $path . DIRECTORY_SEPARATOR . $newName;

                        if (extension_loaded('imagick')) {
                            $im = new \Imagick($origFile);
                            $im->transparentPaintImage($im->getImageBackgroundColor(), 0, 2500, false);
                            $im->setImageFormat('png');
                            $im->writeImage($newFile);
                            $im->destroy();
                        } else {
                            exec("convert \"{$origFile}\" -fill none -fuzz 20% -draw \"matte 0,0 floodfill\" -flop  -draw \"matte 0,0 floodfill\" -flop \"{$newFile}\"");
                            exec("convert \"{$newFile}\" -fuzz 4% -transparent white \"{$newFile}\"");
                        }

                        if ($model->name != $newName && file_exists($origFile)) {
                            $model->updateAttributes([
                                'name' => $newName,
                                'ext' => 'png'
                            ]);
                            unlink($origFile);
                        }
                    }
                    if (!$tmp) {
                        static::checkEvent($company, $attribute);
                        static::log($company, $imageName);
                    }

                    return true;
                }
            }
        }

        return false;
    }

    public static function moveFromTmp(Company $company, $attribute)
    {
        $imageName = $company->getImageName($attribute);
        $model = $company->getDetailsFiles()->andWhere([
            'type_id' => DetailsFileType::findTypeId($attribute),
            'status_id' => DetailsFileStatus::STATUS_TEMPORARY,
        ])->one();

        if ($imageName && $model !== null) {
            $model->status_id = DetailsFileStatus::STATUS_ACTIVE;
            if ($model->save()) {
                static::checkEvent($company, $attribute);
                static::log($company, $imageName);
            }
        }
    }

    public static function log(Company $model, $imageName)
    {
        $log = new CompanyImageUploadLog([
            'company_id' => $model->id,
            'type' => $imageName,
        ]);
        if (Yii::$app->user->identity instanceof Employee) {
            $log->created_by = Yii::$app->user->identity->id;
        } else {
            $log->created_by = ($employeeChief = $model->employeeChief) ? $employeeChief->id : null;
        }
        $log->save(false);
    }

    public static function checkEvent(Company $model, $attribute)
    {
        switch ($attribute) {
            case 'logoImage':
                \common\models\company\CompanyFirstEvent::checkEvent($model, 8);
                break;
            case 'printImage':
                \common\models\company\CompanyFirstEvent::checkEvent($model, 9);
                break;
            case 'chiefSignatureImage':
                \common\models\company\CompanyFirstEvent::checkEvent($model, 10);
                break;
        }
    }

    /**
     * Company data by INN
     * @param  string $inn
     * @param  string $kpp
     * @return array
     */
    public static function getCompanyData($inn, $kpp = null)
    {
        return DadataClient::getCompanyAttributes($inn, $kpp);
    }
}
