<?php

namespace common\models\vkAds;

use common\models\Company;
use Yii;

/**
 * This is the model class for table "vk_ads_campaign".
 *
 * @property int $id
 * @property int $company_id
 * @property string $type
 * @property string $name
 * @property int $status
 * @property int|null $day_limit
 * @property int|null $all_limit
 * @property int|null $start_time
 * @property int|null $stop_time
 *
 * @property VkAdsAd[] $vkAdsAds
 * @property Company $company
 * @property VkAdsReport[] $vkAdsReports
 */
class VkAdsCampaign extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vk_ads_campaign';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'company_id', 'type', 'name', 'status'], 'required'],
            [['id', 'company_id', 'status', 'day_limit', 'all_limit', 'start_time', 'stop_time'], 'integer'],
            [['type', 'name'], 'string', 'max' => 255],
            [['id', 'company_id'], 'unique', 'targetAttribute' => ['id', 'company_id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'type' => 'Тип',
            'name' => 'Имя',
            'status' => 'Статус',
            'day_limit' => 'Дневной лимит',
            'all_limit' => 'Общий лимит',
            'start_time' => 'Дата запуска',
            'stop_time' => 'Дата остановки',
        ];
    }

    /**
     * Gets query for [[VkAdsAds]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVkAdsAds()
    {
        return $this->hasMany(VkAdsAd::class, ['campaign_id' => 'id', 'company_id' => 'company_id']);
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * Gets query for [[VkAdsReports]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVkAdsReports()
    {
        return $this->hasMany(VkAdsReport::class, ['campaign_id' => 'id']);
    }
}
