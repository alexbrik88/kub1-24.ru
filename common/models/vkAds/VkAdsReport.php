<?php

namespace common\models\vkAds;

use common\models\Company;
use Yii;

/**
 * This is the model class for table "vk_ads_report".
 *
 * @property int $id
 * @property int $company_id
 * @property int $ad_id
 * @property int $campaign_id
 * @property string $date
 * @property float $spent_amount
 * @property int $impressions_count
 * @property int $clicks_count
 * @property int $reach
 *
 * @property Company $company
 * @property VkAdsAd $ad
 * @property VkAdsCampaign $campaign
 */
class VkAdsReport extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vk_ads_report';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'ad_id', 'campaign_id', 'date', 'spent_amount', 'impressions_count', 'clicks_count', 'reach'], 'required'],
            [['company_id', 'ad_id', 'campaign_id', 'impressions_count', 'clicks_count', 'reach'], 'integer'],
            [['date'], 'safe'],
            [['spent_amount'], 'number'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['company_id' => 'id']],
            [['ad_id'], 'exist', 'skipOnError' => true, 'targetClass' => VkAdsAd::class, 'targetAttribute' => ['ad_id' => 'id']],
            [['campaign_id'], 'exist', 'skipOnError' => true, 'targetClass' => VkAdsCampaign::class, 'targetAttribute' => ['campaign_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'ad_id' => 'Ad ID',
            'campaign_id' => 'Campaign ID',
            'date' => 'date',
            'spent_amount' => 'Потрачено',
            'impressions_count' => 'Просмотры',
            'clicks_count' => 'Клики',
            'reach' => 'Охват',
        ];
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * Gets query for [[Ad]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAd()
    {
        return $this->hasOne(VkAdsAd::class, ['id' => 'ad_id']);
    }

    /**
     * Gets query for [[Campaign]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCampaign()
    {
        return $this->hasOne(VkAdsCampaign::class, ['id' => 'campaign_id']);
    }
}
