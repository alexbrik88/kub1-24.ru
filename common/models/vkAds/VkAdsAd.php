<?php

namespace common\models\vkAds;

use common\models\Company;
use Yii;

/**
 * This is the model class for table "vk_ads_ad".
 *
 * @property int $id
 * @property int $campaign_id
 * @property int $company_id
 * @property int $format
 * @property int $cost_type
 * @property int|null $cpc
 * @property int|null $cpm
 * @property int|null $ocpm
 * @property int $goal_type
 * @property int|null $impressions_limit
 * @property bool|null $is_impressions_limited
 * @property string|null $ad_platform
 * @property bool|null $is_ad_platform_no_wall
 * @property bool|null $is_ad_platform_no_ad_network
 * @property int|null $all_limit
 * @property int|null $day_limit
 * @property int|null $autobidding_max_cost
 * @property int $status
 * @property string $name
 * @property int $approved
 * @property bool|null $is_video
 *
 * @property Company $company
 * @property VkAdsCampaign $campaign
 * @property VkAdsReport[] $vkAdsReports
 */
class VkAdsAd extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'vk_ads_ad';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'campaign_id', 'company_id', 'format', 'cost_type', 'goal_type', 'status', 'name', 'approved'], 'required'],
            [['id', 'campaign_id', 'company_id', 'format', 'cost_type', 'cpc', 'cpm', 'ocpm', 'goal_type',
                'impressions_limit', 'is_impressions_limited', 'is_ad_platform_no_wall', 'is_ad_platform_no_ad_network',
                'all_limit', 'day_limit', 'autobidding_max_cost', 'status', 'approved', 'is_video'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['ad_platform'], 'safe'],
            [['id', 'campaign_id', 'company_id'], 'unique', 'targetAttribute' => ['id', 'campaign_id', 'company_id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['company_id' => 'id']],
            [['campaign_id', 'company_id'], 'exist', 'skipOnError' => true, 'targetClass' => VkAdsCampaign::class, 'targetAttribute' => ['campaign_id' => 'id', 'company_id' => 'company_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'campaign_id' => 'Кампания',
            'company_id' => 'Company ID',
            'format' => 'Формат',
            'cost_type' => 'Тип оплаты',
            'cpc' => 'Цена за переход',
            'cpm' => 'Цена за 1000 показов',
            'ocpm' => 'Цена за действие для oCPM',
            'goal_type' => 'Тип цели',
            'impressions_limit' => 'Ограничение показов на одного пользователя',
            'is_impressions_limited' => 'Is Impressions Limited',
            'ad_platform' => 'Рекламные площадки',
            'is_ad_platform_no_wall' => 'Не показывать на стенах сообществ?',
            'is_ad_platform_no_ad_network' => 'Не показывать в рекламной сети?',
            'all_limit' => 'Общий лимит',
            'day_limit' => 'Дневной лимит',
            'autobidding_max_cost' => 'Максимальное ограничение автоматической ставки',
            'status' => 'Статус',
            'name' => 'Имя',
            'approved' => 'Статус модерации',
            'is_video' => 'Является видеорекламой?',
        ];
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * Gets query for [[Campaign]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCampaign()
    {
        return $this->hasOne(VkAdsCampaign::class, ['id' => 'campaign_id', 'company_id' => 'company_id']);
    }

    /**
     * Gets query for [[VkAdsReports]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getVkAdsReports()
    {
        return $this->hasMany(VkAdsReport::class, ['ad_id' => 'id']);
    }
}
