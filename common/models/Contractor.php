<?php

namespace common\models;

use common\components\contractor\behaviors\CalculationBehavior;
use common\components\date\DateHelper;
use common\components\date\DatePickerFormatBehavior;
use common\components\helpers\ModelHelper;
use common\components\helpers\Month;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashBankFlows;
use common\models\cash\CashBankFlowToInvoice;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashEmoneyFlowToInvoice;
use common\models\cash\CashOrderFlows;
use common\models\cash\CashOrderFlowToInvoice;
use common\models\company\CompanyIndustry;
use common\models\company\CompanyType;
use common\models\companyStructure\SalePoint;
use common\models\contractor\ContractorAgentBuyer;
use common\models\contractor\ContractorGroup;
use common\models\currency\Currency;
use common\models\dictionary\bik\BikDictionary;
use common\models\document\Act;
use common\models\document\Autoinvoice;
use common\models\document\Invoice;
use common\models\document\InvoiceAuto;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\document\Order;
use common\models\document\OrderAct;
use common\models\document\OrderDocument;
use common\models\document\OrderPackingList;
use common\models\document\OrderUpd;
use common\models\document\PackingList;
use common\models\document\status\InvoiceStatus;
use common\models\document\Upd;
use common\models\document\UserEmail;
use common\models\EmployeeCompany;
use common\models\employee\Employee;
use common\models\paymentReminder\PaymentReminderMessageContractor;
use common\models\product\Product;
use common\models\product\ProductTurnover;
use common\models\service\ServiceContractorItem;
use common\models\store\StoreCompany;
use common\models\store\StoreCompanyContractor;
use common\models\store\StoreUser;
use common\models\store\StoreUserRole;
use common\tasks\CreatePaymentReminderMessagesContractor;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\modules\crm\models\Client;
use frontend\modules\crm\models\Contact;
use frontend\modules\crm\models\ContractorActivity;
use frontend\modules\crm\models\ContractorCampaign;
use frontend\modules\crm\models\Task;
use frontend\modules\reports\models\AnalysisSearch;
use frontend\modules\reports\models\DisciplineSearch;
use frontend\modules\reports\models\FlowOfFundsReportSearch;
use Yii;
use yii\base\BaseObject;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\JsExpression;

/**
 * This is the model class for table "contractor".
 *
 * @property integer                          $id
 * @property integer                          $company_id
 * @property integer                          $is_seller
 * @property integer                          $is_customer
 * @property integer                          $is_founder
 * @property integer                          $type
 * @property integer                          $oppositeType
 * @property integer                          $status
 * @property integer                          $company_type_id
 * @property string                           $name
 * @property string                           $director_name
 * @property string                           $director_email
 * @property string                           $director_phone
 * @property integer                          $chief_accountant_is_director
 * @property string                           $chief_accountant_email
 * @property string                           $chief_accountant_phone
 * @property string                           $legal_address
 * @property string                           $actual_address
 * @property string                           $postal_address
 * @property integer                          $BIN
 * @property integer                          $ITN
 * @property integer                          $PPC
 * @property integer                          $current_account
 * @property string                           $bank_name
 * @property integer                          $corresp_account
 * @property integer                          $BIC
 * @property integer                          $contact_is_director
 * @property string                           $contact_name
 * @property string                           $contact_phone
 * @property string                           $contact_email
 * @property mixed                            created_at
 * @property integer                          $employee_id
 * @property integer                          $taxation_system
 * @property string                           $chief_accountant_name
 * @property string                           $is_deleted
 * @property string                           $current_account_guid
 * @property string                           $document_guid
 * @property integer                          $face_type
 * @property integer                          $physical_passport_isRf
 * @property integer                          $physical_passport_country,
 * @property string                           $physical_firstname
 * @property string                           $physical_lastname
 * @property string                           $physical_patronymic
 * @property string                           $physical_no_patronymic
 * @property string                           $physical_address
 * @property string                           $physical_passport_number
 * @property string                           $physical_passport_series
 * @property string                           $physical_passport_date_output
 * @property string                           $physical_passport_issued_by
 * @property string                           $physical_passport_department
 * @property integer                          $invoice_expenditure_item_id
 * @property integer                          $invoice_income_item_id
 * @property integer                          $nds_view_type_id
 * @property integer                          $responsible_employee_id
 * @property string                           $comment
 * @property integer                          $order_currency
 * @property integer                          $payment_delay
 * @property integer                          $seller_payment_delay
 * @property integer                          $customer_payment_delay
 * @property integer                          $guaranty_delay
 * @property integer                          $seller_guaranty_delay
 * @property integer                          $customer_guaranty_delay
 * @property integer                          $okpo
 * @property boolean                          $not_accounting
 * @property number                           $discount
 * @property number                           $seller_discount
 * @property number                           $customer_discount
 * @property integer                          $create_type_id
 * @property integer                          $create_api_id
 * @property integer                          $payment_priority
 *
 * @property integer                          $is_agent
 * @property string                           $agent_start_date
 * @property string                           $agent_payment_percent
 * @property integer                          $agent_payment_type_id
 * @property integer                          $agent_agreement_id
 *
 * @property array                            $ndsViewList
 * @property string                           $someEmail
 * @property integer                          $docsIoType
 * @property integer                          $flowsType
 * @property boolean                          $isOutInvoiceHasNds
 *
 * @property boolean                          $director_in_act
 * @property boolean                          $chief_accountant_in_act
 * @property boolean                          $contact_in_act
 * @property bool|int                         $verified                 Признак, что клиент "проверил" контрагента (хотя бы раз открывал его досье)
 * @property string                           $decodingLegalForm
 * @property integer                          $country_id
 * @property int $activity_id
 * @property int $campaign_id
 * @property int $customer_industry_id
 * @property int $seller_industry_id
 * @property int $customer_sale_point_id
 * @property int $seller_sale_point_id
 *
 * Relations:
 * @property CashBankFlows[]                  $cashBankFlows
 * @property CashEmoneyFlows[]                $cashEmoneyFlows
 * @property CashOrderFlows[]                 $cashOrderFlows
 * @property Company                          $company
 * @property ContractorAccount                $contractorAccount
 * @property ContractorAccount[]              $contractorAccounts
 * @property Employee                         $employee
 * @property Invoice[]                        $invoices
 * @property CompanyType                      $companyType
 * @property string                           $nameWithType
 * @property InvoiceAuto                      $invoicesAuto
 * @property OrderDocument[]                  $orderDocuments
 * @property PaymentReminderMessageContractor $paymentReminderMessageContractor
 * @property UserEmail[]                      $userEmails
 * @property Agreement                        $agentAgreement
 * @property ResponsibleEmployeeCompany       $responsibleEmployeeCompany
 * @property InvoiceIncomeItem                $income
 * @property InvoiceExpenditureItem           $expenditure
 * @property ServiceContractorItem            $kubItem
 *
 * Behaviors:
 * @mixin CalculationBehavior
 *
 * @property CalculationBehavior              $calculation
 * @property-read Employee $responsibleEmployee
 * @property-read Client|null $client
 * @property-read Contact[] $contacts
 * @property-read ContractorActivity|null $activity
 * @property-read ContractorCampaign|null $campaign
 */
class Contractor extends \yii\db\ActiveRecord
{

    /**
     * NOTE: this constants are the same as Documents::IO_TYPE_IN and Documents::IO_TYPE_OUT
     * @see frontend\models\Documents
     */
    const TYPE_SELLER = 1;
    /**
     *
     */
    const TYPE_CUSTOMER = 2;
    /**
     *
     */
    const TYPE_FOUNDER = 3;
    /**
     * NOTE: used in import when type has not yet been defined
     */
    const TYPE_UNSET = 0;

    /** @var int Используется в CRM */
    const TYPE_POTENTIAL_CLIENT = 4;

    /**
     *
     */
    const TYPE_LEGAL_PERSON = 0;
    /**
     *
     */
    const TYPE_PHYSICAL_PERSON = 1;
    /**
     *
     */
    const TYPE_FOREIGN_LEGAL_PERSON = 2;
    /**
     *
     */
    const SCENARIO_LEGAL_FACE = 'legalFace';
    /**
     *
     */
    const SCENARIO_FIZ_FACE = 'fizFace';
    /**
     *
     */
    const SCENARIO_FOREIGN_LEGAL_FACE = 'foreignLegalFace';
    /**
     *
     */
    const WITH_NDS = 1;
    /**
     *
     */
    const WITHOUT_NDS = 0;
    /**
     *
     */
    const NOT_SET = '---';
    /**
     *
     */
    const ACTIVE = 1;
    /**
     *
     */
    const INACTIVE = 0;
    /**
     *
     */
    const ALL_ACTIVITY = -1;
    /**
     *
     */
    const DELETED = 1;
    /**
     *
     */
    const NOT_DELETED = 0;

    const TAB_BENEFIT = 1;
    const TAB_SETTINGS = 2;
    const TAB_CABINETS = 3;
    const TAB_PAYMENT = 4;

    const TAB_INVOICE_BENEFIT = 5;
    const TAB_INVOICE_LINKS = 6;
    const TAB_INVOICE_SETTINGS = 7;
    const TAB_INVOICE_PAYMENT = 8;

    const TAB_INVOICE = 1;
    const TAB_ANALYTICS = 2;
    const TAB_SELLING = 3;
    const TAB_CONTRACTS = 4;
    const TAB_CONTRACTOR_INFO = 5;

    const DEFAULT_LEGAL_ADDRESS = 'Юридический адрес';
    const DEFAULT_DIRECTOR_NAME = 'ФИО Руководителя';

    const PAYMENT_PRIORITY_HIGH = 1;
    const PAYMENT_PRIORITY_MEDIUM = 2;
    const PAYMENT_PRIORITY_LOW = 3;

   public $group_name;
    /**
     * @var null
     */
    public $current_dept = null;
    /**
     * @var null
     */
    public $debt_0_10 = null;
    /**
     * @var null
     */
    public $debt_11_30 = null;
    /**
     * @var null
     */
    public $debt_31_60 = null;
    /**
     * @var null
     */
    public $debt_61_90 = null;
    /**
     * @var null
     */
    public $debt_more_90 = null;

    /**
     * @var null
     */
    public $debt_january = null;
    /**
     * @var null
     */
    public $debt_february = null;
    /**
     * @var null
     */
    public $debt_march = null;
    /**
     * @var null
     */
    public $debt_april = null;
    /**
     * @var null
     */
    public $debt_may = null;
    /**
     * @var null
     */
    public $debt_june = null;
    /**
     * @var null
     */
    public $debt_july = null;
    /**
     * @var null
     */
    public $debt_august = null;
    /**
     * @var null
     */
    public $debt_september = null;
    /**
     * @var null
     */
    public $debt_october = null;
    /**
     * @var null
     */
    public $debt_november = null;
    /**
     * @var null
     */
    public $debt_december = null;
    /**
     * @var null
     */
    public $allDebt = null;
    /**
     * @var null
     */
    public $opposite;
    /**
     * @var bool
     */
    public $isMonetaCustomer = false;
    /**
     * @var null
     */
    protected $_has_opposite;
    protected $_contractorAccount;
    protected $_hasAgentBuyers;

    public static $CURRENCY_CAPTION = [
        0 => 'руб.',
        1 => 'долларов США',
        2 => 'евро',
        3 => 'белорусских рублей',
        4 => 'казахстанских тенге',
        5 => 'украинских гривен'
    ];

    public static $passportAttributes = [
        'physical_passport_series',
        'physical_passport_number',
        'physical_passport_issued_by',
        'physical_passport_date_output',
        'physical_passport_department',
    ];

    /**
     * @var array
     */
    public static $contractorFaceType = [
        self::TYPE_LEGAL_PERSON => 'Юр. лицо',
        self::TYPE_PHYSICAL_PERSON => 'Физ. лицо',
        self::TYPE_FOREIGN_LEGAL_PERSON => 'Иностранное юр.лицо',
    ];

    /**
     * @var array
     */
    public static $contractorTitleSingle = [
        self::TYPE_CUSTOMER => 'Покупатель',
        self::TYPE_SELLER => 'Поставщик',
    ];

    /**
     * @var array
     */
    public static $contractorTitle = [
        self::TYPE_SELLER => 'Поставщики',
        self::TYPE_CUSTOMER => 'Покупатели',
    ];

    /**
     * @var array
     */
    protected $cache = [];

    /**
     * @param Company $company
     * @param Employee $employee
     * @return Contractor
     */
    public static function newModel(Company $company, Employee $employee, $params = []) : Contractor
    {
        $model = new Contractor();
        $model->company_id = $company->id;
        $model->face_type = Contractor::TYPE_LEGAL_PERSON;
        $model->seller_payment_delay = $company->seller_payment_delay;
        $model->customer_payment_delay = $company->customer_payment_delay;
        $model->employee_id = $employee->id;
        $model->responsible_employee_id = $employee->id;
        $model->status = Contractor::ACTIVE;
        $model->taxation_system = Contractor::WITHOUT_NDS;
        $model->chief_accountant_is_director = 1;
        $model->contact_is_director = 1;
        $model->order_currency = Currency::DEFAULT_ID;
        $model->invoice_income_item_id = $company->default_contractor_income_item_id;
        $model->invoice_expenditure_item_id = $company->default_contractor_expenditure_item_id;
        $model->object_guid = \frontend\modules\export\models\one_c\OneCExport::generateGUID();

        $model->setScenario('insert');
        $model->loadDefaultValues();
        $model->populateRelation('company', $company);

        if (!empty($params)) {
            $model->load($params, '');
        }

        return $model;
    }

    /**
     * @param Company $company
     * @return mixed
     */
    public static function createFounder(Company $company)
    {
        return Yii::$app->db->createCommand()->insert(Contractor::tableName(), [
            'company_id' => $company->id,
            'type' => Contractor::TYPE_FOUNDER,
            'status' => Contractor::ACTIVE,
            'company_type_id' => $company->company_type_id,
            'name' => 'Учредитель',
            'created_at' => time(),
            'taxation_system' => '0',
            'is_deleted' => '0',
            'employee_id' => null,
            'director_name' => '',
            'chief_accountant_is_director' => false,
            'contact_is_director' => false,
        ])->execute();
    }

    /**
     * @param $companyId
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function getCompanysFounder($companyId)
    {
        return static::find()
            ->byCompany($companyId)
            ->byDeleted(false)
            ->byStatus(self::ACTIVE)
            ->byContractor(self::TYPE_FOUNDER)
            ->one();
    }

    /**
     * @param int $companyId
     * @param int|null $contractorType
     * @return array
     */
    public static function flatList(int $companyId, int $contractorType = null)
    {
        $query = static::find()
            ->byCompany($companyId)
            ->byDeleted(false)
            ->byStatus(self::ACTIVE)
            ->indexBy('id');

        if ($contractorType) {
            $query->byContractor($contractorType);
        }

        $contractors = [];

        /** @var Contractor $contractor */
        foreach ($query->all() as $id => $contractor) {
            $contractors[$id] = $contractor->getNameWithType();
        }

        return $contractors;
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
            ],
            'calculation' => [
                'class' => CalculationBehavior::className(),
                'beginAt' => StatisticPeriod::getSessionPeriod()['from'],
                'endAt' => StatisticPeriod::getSessionPeriod()['to'],
            ],
            [
                'class' => DatePickerFormatBehavior::className(),
                'attributes' => [
                    'physical_passport_date_output',
                    'agent_start_date'
                ],
            ],
        ];
    }

    /**
     * @return integer|null
     */
    public function getDocsIoType()
    {
        switch ($this->type) {
            case self::TYPE_SELLER:
                return Documents::IO_TYPE_IN;
                break;

            case self::TYPE_CUSTOMER:
                return Documents::IO_TYPE_OUT;
                break;

            default:
                return null;
                break;
        }
    }

    /**
     * @return integer|null
     */
    public function getFlowsType()
    {
        switch ($this->type) {
            case self::TYPE_SELLER:
                return CashFlowsBase::FLOW_TYPE_EXPENSE;
                break;

            case self::TYPE_CUSTOMER:
                return CashFlowsBase::FLOW_TYPE_INCOME;
                break;

            default:
                return null;
                break;
        }
    }

    /**
     * @return array
     */
    public function fields()
    {
        $parent = parent::fields();
        $fields = [
            'BIC',
            'bank_name',
            'bank_city',
            'corresp_account',
            'current_account',
        ];

        return array_merge($parent, array_combine($fields, $fields));
    }

    /**
     * @return array the list of expandable field names or field definitions. Please refer
     * to [[fields()]] on the format of the return value.
     * @see toArray()
     * @see fields()
     */
    public function extraFields()
    {
        return [
            'companyType',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contractor';
    }

    /**
     * @return ContractorQuery
     */
    public static function find()
    {
        return new ContractorQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();

        $this->opposite = (int) $this->getIsSellerCustomer();
    }

    /**
     * @inheritdoc
     */
    public function afterValidate()
    {
        parent::afterValidate();

        if ($this->isNewRecord && ($this->getContractorAccount()->bik || $this->getContractorAccount()->rs)) {
            $this->getContractorAccount()->validate();
            if ($errors = $this->getContractorAccount()->getFirstErrors()) {
                if (isset($errors['bik'])) {
                    $this->addError('BIC', $errors['bik']);
                }
                if (isset($errors['rs'])) {
                    $this->addError('current_account', $errors['rs']);
                }
            }
        }
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->company_id === null) {
                $this->addError('id', "Контрагент {$this->shortTitle} не может быть изменен.");

                return false;
            }

            if ($insert && !empty($this->type)) {
                $this->is_seller = ($this->type == self::TYPE_SELLER) ? 1 : 0;
                $this->is_customer = ($this->type == self::TYPE_CUSTOMER) ? 1 : 0;
                $this->is_founder = ($this->type == self::TYPE_FOUNDER) ? 1 : 0;
            }

            if ($this->opposite) {
                $this->is_seller = $this->is_customer = 1;
            }

            if ($insert) {
                if (!isset($this->status)) {
                    $this->status = self::ACTIVE;
                }
                if ($this->type == self::TYPE_CUSTOMER && !$this->invoice_income_item_id && $this->company->default_contractor_income_item_id !== null) {
                    $this->invoice_income_item_id = $this->company->default_contractor_income_item_id;
                } else if ($this->type == self::TYPE_SELLER && !$this->invoice_expenditure_item_id && $this->company->default_contractor_expenditure_item_id !== null) {
                    $this->invoice_expenditure_item_id = $this->company->default_contractor_expenditure_item_id;
                }
                if (!isset($this->seller_payment_delay)) {
                    $this->seller_payment_delay = $this->company->seller_payment_delay;
                }
                if (!isset($this->customer_payment_delay)) {
                    $this->customer_payment_delay = $this->company->customer_payment_delay;
                }
                if (!isset($this->consignee_kpp_same)) {
                    $this->consignee_kpp_same = 1;
                }
                if (!isset($this->consignee_address_same)) {
                    $this->consignee_address_same = 1;
                }
            }

            if (!$this->director_post_name) {
                if (in_array($this->company_type_id, CompanyType::likeIpId())) {
                    $this->director_post_name = 'Предприниматель';
                }
                if ($this->face_type != Contractor::TYPE_PHYSICAL_PERSON) {
                    $this->director_post_name = 'Генеральный директор';
                } else {
                    $this->director_post_name = '';
                }
            }

            if ($this->face_type == Contractor::TYPE_PHYSICAL_PERSON) {
                $this->company_type_id = null;
                $this->name = $this->physical_fio;
                if ($this->isMonetaCustomer) {
                    $this->director_name = $this->physical_lastname;
                } else {
                    $this->director_name = $this->physical_fio;
                }

                if ($this->physical_no_patronymic) {
                    $this->physical_patronymic = '';
                }
            }

            $this->chief_accountant_is_director = (bool) $this->chief_accountant_is_director;
            if ($this->chief_accountant_is_director) {
                $this->chief_accountant_name = $this->chief_accountant_phone = $this->chief_accountant_email = null;
            }

            if ($this->contact_is_director) {
                $this->contact_name = $this->contact_phone = $this->contact_email = null;
            }

            if ($this->isAttributeChanged('status') && $this->status == self::INACTIVE && count($this->invoicesAuto) > 0) {
                foreach ($this->invoicesAuto as $invoiceAuto) {
                    $invoiceAuto->auto->status = Autoinvoice::CANCELED;
                    if ($invoiceAuto->auto->isAttributeChanged('status')) {
                        $invoiceAuto->auto->save(false, ['status']);
                        $invoiceAuto->invoice_status_updated_at = time();
                        $invoiceAuto->save(false, ['invoice_status_updated_at']);
                    }
                }
            }

            $this->name = str_replace(['&quot;', '&laquo;', '&raquo;', '«', '»', '<', '>'], '"', $this->name);
            if ($insert && $this->company_type_id && !in_array($this->company_type_id, CompanyType::likeIpId())) {
                if (!preg_match('/"/', $this->name)) {
                    $this->name = '"' . $this->name . '"';
                }
            }

            $this->create_type_id = ArrayHelper::getValue(Yii::$app->params, 'create_type_id', 1);
            $this->create_api_id = ArrayHelper::getValue(Yii::$app->params, 'create_api_id', null);

            if ($this->BIN === null) {
                $this->BIN = '';
            }
            if ($this->director_name === null) {
                $this->director_name = '';
            }

            return true;
        } else {
            return false;
        }
    }

    public function getIsSellerCustomer() : bool
    {
        if (!$this->hasAttribute('is_seller')) { // Fix stupid migration: m201013_151357_alter_contractor_add_column
           return false;
        }

        return $this->is_seller && $this->is_customer;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert) {
            switch ($this->type) {
                case self::TYPE_SELLER:
                    \common\models\company\CompanyFirstEvent::checkEvent($this->company, 4);
                    break;
                case self::TYPE_CUSTOMER:
                    \common\models\company\CompanyFirstEvent::checkEvent($this->company, 3);
                    break;
            }

            Yii::$app->queue->push(CreatePaymentReminderMessagesContractor::create($this->id));
        }

        if (array_key_exists('responsible_employee_id', $changedAttributes)) {
            if ($this->responsible_employee_id && $this->company_id) {
                Invoice::updateAll([
                    'responsible_employee_id' => $this->responsible_employee_id,
                ], [
                    'type' => $this->type,
                    'company_id' => $this->company_id,
                    'contractor_id' => $this->id,
                    'is_deleted' => false,
                    'invoice_status_id' => InvoiceStatus::$payAllowed,
                ]);
                Agreement::updateAll([
                    'created_by' => $this->responsible_employee_id,
                    'document_author_id' => $this->responsible_employee_id
                ], [
                    'type' => $this->type,
                    'company_id' => $this->company_id,
                    'contractor_id' => $this->id
                ]);
            }
        }

        if (array_key_exists('director_email', $changedAttributes) && $this->director_email) {
            $storeCompany = StoreCompany::find()->joinWith('storeCompanyContractors')
                ->andWhere([
                    'store_company.status' => StoreCompany::STATUS_ACTIVE,
                    'store_company_contractor.status' => StoreCompanyContractor::STATUS_ACTIVE,
                    'store_company_contractor.contractor_id' => $this->id,
                ])
                ->one();

            if ($storeCompany && !$storeCompany->getStoreUsers()->andWhere(['email' => $this->director_email])->exists()) {
                foreach ($storeCompany->storeUsers as $storeUser) {
                    $storeCompany->unlink('storeUsers', $storeUser, true);
                    $storeUser->updateAttributes([
                        'status' => StoreUser::STATUS_DELETED,
                    ]);
                }
                $this->createStoreUserAccount($storeCompany);
            }
        }

        if ($insert &&
            $this->getContractorAccount()->isNewRecord &&
            $this->getContractorAccount()->bik &&
            $this->getContractorAccount()->rs
        ) {
            $this->getContractorAccount()->contractor_id = $this->id;
            $this->getContractorAccount()->save();
        }
    }

    /**
     * @return boolean
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            foreach ($this->agreements as $agreement) {
                if (!$agreement->delete()) {
                    return false;
                }
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @return boolean|integer
     */
    public function delete()
    {
        if ($this->company_id === null) {
            $this->addError('id', "Контрагент {$this->shortTitle} не может быть удален.");

            return false;
        }

        if ($this->getInvoices()->exists() ||
            $this->getInvoicesAuto()->exists() ||
            $this->getCashBankFlows()->exists() ||
            $this->getCashOrderFlows()->exists() ||
            $this->getCashEmoneyFlows()->exists() ||
            $this->getStoreCompanies()->exists()
        ) {
            return $this->updateAttributes(['is_deleted' => true]);
        } else {
            return parent::delete();
        }
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        return array_merge(parent::scenarios(), [
            self::SCENARIO_LEGAL_FACE => [
                'contractorType',
                'face_type',
                'ITN',
                'name',
                'company_type_id',
                'companyTypeId',
                'PPC',
                'director_email',
                'director_name',
                'director_post_name',
                'director_phone',
                'legal_address',
                'responsible_employee_id',
                'seller_discount',
                'customer_discount',
                'seller_payment_delay',
                'customer_payment_delay',
                'seller_guaranty_delay',
                'customer_guaranty_delay',
                'current_account',
                'bank_name',
                'BIC',
                'corresp_account',
                'payment_priority',
                'one_c_imported',
                'one_c_exported',
                'activity_id',
                'campaign_id',
                'customer_industry_id',
                'seller_industry_id',
                'customer_sale_point_id',
                'seller_sale_point_id',
            ],
            self::SCENARIO_FOREIGN_LEGAL_FACE => [
                'contractorType',
                'face_type',
                'ITN',
                'name',
                'company_type_id',
                'companyTypeId',
                'PPC',
                'director_email',
                'director_name',
                'director_post_name',
                'legal_address',
                'current_account',
                'bank_name',
                'BIC',
                'corresp_account',
                'responsible_employee_id',
                'seller_discount',
                'customer_discount',
                'seller_payment_delay',
                'customer_payment_delay',
                'seller_guaranty_delay',
                'customer_guaranty_delay',
                'decoding_legal_form',
                'country_id',
                'payment_priority',
                'one_c_imported',
                'one_c_exported',
                'activity_id',
                'campaign_id',
                'customer_industry_id',
                'seller_industry_id',
                'customer_sale_point_id',
                'seller_sale_point_id',
            ],
            self::SCENARIO_FIZ_FACE => [
                'contractorType',
                'face_type',
                'physical_fio',
                'physical_lastname',
                'physical_firstname',
                'physical_patronymic',
                'physical_no_patronymic',
                'physical_address',
                'director_email',
                'director_phone',
                'contact_email',
                'physical_passport_isRf',
                'physical_passport_country',
                'physical_passport_series',
                'physical_passport_number',
                'physical_passport_date_output',
                'physical_passport_issued_by',
                'physical_passport_department',
                'responsible_employee_id',
                'seller_discount',
                'customer_discount',
                'seller_payment_delay',
                'customer_payment_delay',
                'seller_guaranty_delay',
                'customer_guaranty_delay',
                'current_account',
                'bank_name',
                'BIC',
                'corresp_account',
                'payment_priority',
                'one_c_imported',
                'one_c_exported',
                'activity_id',
                'campaign_id',
                'customer_industry_id',
                'seller_industry_id',
                'customer_sale_point_id',
                'seller_sale_point_id',
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['director_email', 'chief_accountant_email', 'contact_email'], 'trim'],
            [
                [
                    'status',
                    'chief_accountant_is_director',
                    'contact_is_director',
                    'taxation_system',
                    'physical_no_patronymic',
                    'seller_payment_delay',
                    'customer_payment_delay',
                    'seller_guaranty_delay',
                    'customer_guaranty_delay',
                    'order_currency',
                    'physical_passport_isRf',
                    'responsible_employee_id',
                ],
                'filter',
                'filter' => function ($value) {
                    return is_numeric($value) ? $value * 1 : $value;
                }
            ],
            ['type', 'required', 'on' => 'insert'],
            ['order_currency', 'integer'],
            [
                ['type'],
                'in',
                'range' => [
                    self::TYPE_CUSTOMER,
                    self::TYPE_SELLER,
                    self::TYPE_POTENTIAL_CLIENT,
                    self::TYPE_UNSET
                ],
                'message' => 'Тип контрагента указан неверно.',
                'on' => 'insert',
            ],
            // INSERT
            [['taxation_system'], 'integer', 'on' => 'insert',],
            // OTHER
            [
                [
                    'face_type',
                ],
                'required',
                'message' => 'Необходимо выбрать.',
            ],
            [
                [
                    //'face_type',
                    'ITN',
                    'name',
                ],
                'required',
                'when' => function (Contractor $model) {
                    return $model->face_type == Contractor::TYPE_LEGAL_PERSON;
                },
                'whenClient' => 'function () {
                    return contractorIsLegal();
                }',
                'on' => self::SCENARIO_LEGAL_FACE,
                'message' => 'Необходимо заполнить',
            ],
            [
                [
                    'ITN',
                    //'face_type',
                    'name',
                    'foreign_legal_form',
                ],
                'required',
                'when' => function (Contractor $model) {
                    return $model->face_type == Contractor::TYPE_FOREIGN_LEGAL_PERSON;
                },
                'whenClient' => 'function () {
                    return contractorIsForeignLegal();
                }',
            ],
            [
                [
                    'PPC',
                ],
                'required',
                'when' => function (Contractor $model) {
                    return $model->face_type == Contractor::TYPE_LEGAL_PERSON &&
                        !in_array($model->company_type_id, CompanyType::likeIpId());
                },
                'whenClient' => 'function () {
                    return false;
                }',
                'on' => self::SCENARIO_LEGAL_FACE,
                'message' => 'Необходимо заполнить',
            ],
            [
                ['physical_no_patronymic'],
                'boolean',
                'when' => function (Contractor $model) {
                    return $model->face_type == Contractor::TYPE_PHYSICAL_PERSON;
                },
                'whenClient' => 'function () {
                    return contractorIsPhysical();
                }',
            ],
            [
                ['physical_patronymic'],
                'required',
                'when' => function (Contractor $model) {
                    return ($model->face_type == Contractor::TYPE_PHYSICAL_PERSON && !$model->physical_no_patronymic);
                },
                'whenClient' => 'function () {
                    return contractorIsPhysical() && !$("#contractor-physical_no_patronymic").is(":checked");
                }',
            ],
            [
                [
                    //'face_type',
                    'physical_fio',
                    'physical_lastname',
                    'physical_firstname',
                ],
                'required',
                'when' => function (Contractor $model) {
                    return $model->face_type == Contractor::TYPE_PHYSICAL_PERSON;
                },
                'whenClient' => 'function () {
                    $("#contractor-face_type").find(":checked").val() == ' . Contractor::TYPE_PHYSICAL_PERSON . ';
                }',
                'on' => self::SCENARIO_FIZ_FACE,
                'message' => 'Необходимо заполнить',
            ],
            [
                [
                    'name',
                    'director_name',
                    'chief_accountant_is_director',
                    'legal_address',
                    'ITN',
                    'contact_is_director',
                    //'face_type',
                ],
                'required',
                'when' => function (Contractor $model) {
                    return $model->face_type == Contractor::TYPE_LEGAL_PERSON;
                },
                'whenClient' => 'function () {
                    return contractorIsLegal();
                }',
                'on' => 'insert',
            ],
            [
                [
                    'physical_fio',
                    'physical_firstname',
                    'physical_lastname',
                    //'face_type',
                ],
                'required',
                'when' => function (Contractor $model) {
                    return $model->face_type == Contractor::TYPE_PHYSICAL_PERSON;
                },
                'whenClient' => 'function () {
                    return contractorIsPhysical();
                }',
                'on' => 'insert',
            ],
            [
                [
                    'seller_payment_delay',
                    'customer_payment_delay',
                ],
                'default',
                'value' => function($model, $attribute) {
                    return ArrayHelper::getValue($model->company, $attribute, 10);
                },
            ],
            [
                [
                    'seller_guaranty_delay',
                    'customer_guaranty_delay',
                ],
                'default',
                'value' => 20,
            ],
            ['payment_priority', 'default', 'value' => self::PAYMENT_PRIORITY_LOW],
            [
                [
                    'status',
                    'BIN',
                    'ITN',
                    'PPC',
                    'physical_passport_number',
                    'invoice_expenditure_item_id',
                    'invoice_income_item_id',
                    'seller_payment_delay',
                    'customer_payment_delay',
                    'seller_guaranty_delay',
                    'customer_guaranty_delay',
                    'payment_priority'
                ],
                'integer',
                'min' => 0,
                'when' => function (Contractor $model) {
                    return $model->face_type != Contractor::TYPE_FOREIGN_LEGAL_PERSON;
                },
                'whenClient' => 'function () {
                    return !contractorIsForeignLegal();
                }',
            ],

            [
                [
                    'current_account',
                ],
                'string',
                'length' => 20,
                'max' => 20,
                'when' => function (Contractor $model) {
                    return $model->face_type != Contractor::TYPE_FOREIGN_LEGAL_PERSON;
                },
                'whenClient' => 'function () {
                    return !contractorIsForeignLegal();
                }',
            ],
            [
                [
                    'country_id',
                ],
                'integer',
                'when' => function (Contractor $model) {
                    return $model->face_type == Contractor::TYPE_FOREIGN_LEGAL_PERSON;
                },
                'whenClient' => 'function () {
                    return contractorIsForeignLegal();
                }',
            ],
            [
                [
                    'foreign_legal_form',
                    'decoding_legal_form',
                ],
                'string',
                'max' => 255,
                'when' => function (Contractor $model) {
                    return $model->face_type == Contractor::TYPE_FOREIGN_LEGAL_PERSON;
                },
                'whenClient' => 'function () {
                    return contractorIsForeignLegal();
                }',
            ],
            [
                ['company_type_id'],
                'exist',
                'skipOnError' => true,
                'skipOnEmpty' => true,
                'targetClass' => CompanyType::className(),
                'targetAttribute' => ['company_type_id' => 'id'],
                //'filter' => ['in_contractor' => true],
                'message' => 'Тип компании указан неверно.',
                'when' => function (Contractor $model) {
                    return $model->face_type == Contractor::TYPE_LEGAL_PERSON;
                },
                'whenClient' => 'function () {
                    return !contractorIsPhysical();
                }',
            ],
            [
                ['companyTypeId'],
                'exist',
                'targetClass' => CompanyType::class,
                'targetAttribute' => 'id',
                'filter' => ['in_contractor' => true],
                'message' => 'Тип компании указан неверно.',
                'when' => function (Contractor $model) {
                    return $model->face_type == Contractor::TYPE_LEGAL_PERSON && $model->companyTypeId != -1;
                },
                'whenClient' => 'function () {
                    return !contractorIsPhysical() && $("#contractor-companytypeid").val() != "-1";
                }',
            ],
            [
                ['face_type'],
                'in',
                'range' => [self::TYPE_LEGAL_PERSON, self::TYPE_PHYSICAL_PERSON, self::TYPE_FOREIGN_LEGAL_PERSON]
            ],
            [ // inn
                ['ITN'],
                'string',
                'length' => 10,
                'when' => function (Contractor $model) {
                    return $model->face_type == Contractor::TYPE_LEGAL_PERSON &&
                        !in_array($model->company_type_id, CompanyType::likeIpId());
                },
                'whenClient' => 'function () {
                    return false;
                }',
            ],
            [ // inn
                ['ITN'],
                'string',
                'length' => 12,
                'when' => function (Contractor $model) {
                    return $model->face_type == Contractor::TYPE_LEGAL_PERSON &&
                        in_array($model->company_type_id, CompanyType::likeIpId());
                },
                'whenClient' => 'function () {
                    return contractorIsIp() && contractorIsLegal();
                }',
            ],
            [ // inn
                ['ITN'],
                'string',
                'max' => 40,
                'when' => function (Contractor $model) {
                    return $model->face_type == Contractor::TYPE_FOREIGN_LEGAL_PERSON;
                },
                'whenClient' => 'function () {
                    return contractorIsForeignLegal();
                }',
            ],
            [
                ['BIN'],
                'string',
                'length' => 15,
                'when' => function (Contractor $model) {
                    return $model->face_type == Contractor::TYPE_LEGAL_PERSON &&
                        in_array($model->company_type_id, CompanyType::likeIpId());
                },
                'notEqual' => 'Значение «ОГРНИП» должно содержать 15 символов.',
                'whenClient' => 'function () {
                    return false;
                }',
            ],
            [
                ['BIN'],
                'string',
                'length' => 13,
                'when' => function (Contractor $model) {
                    return $model->face_type == Contractor::TYPE_LEGAL_PERSON &&
                        !in_array($model->company_type_id, CompanyType::likeIpId());
                },
                'whenClient' => 'function () {
                    return false;
                }',
                'notEqual' => 'Значение «ОГРН» должно содержать 13 символов.',
            ],
            [
                ['PPC'],
                'string',
                'max' => 9,
                'when' => function (Contractor $model) {
                    return $model->face_type == Contractor::TYPE_LEGAL_PERSON &&
                        !in_array($model->company_type_id, CompanyType::likeIpId());
                },
                'whenClient' => 'function () {
                    return false;
                }',
            ],
            [
                ['director_name', 'director_phone',],
                'string',
                'max' => 45,
                'when' => function (Contractor $model) {
                    return $model->face_type == Contractor::TYPE_LEGAL_PERSON ||
                        $model->face_type == Contractor::TYPE_PHYSICAL_PERSON;
                },
                'whenClient' => 'function () {
                    return !contractorIsForeignLegal();
                }',
            ],
            [
                ['director_post_name',],
                'required',
                'when' => function (Contractor $model) {
                    return $model->face_type == Contractor::TYPE_LEGAL_PERSON &&
                        !in_array($model->company_type_id, CompanyType::likeIpId());
                },
                'whenClient' => 'function () {
                    return false;
                }',
                'on' => 'insert',
            ],
            [
                ['director_post_name',],
                'string',
                'max' => 255,
                'when' => function (Contractor $model) {
                    return $model->face_type == Contractor::TYPE_LEGAL_PERSON &&
                        !in_array($model->company_type_id, CompanyType::likeIpId());
                },
                'whenClient' => 'function () {
                    return false;
                }',
            ],
            [
                ['name'],
                'string',
                'max' => 255,
                'when' => function (Contractor $model) {
                    return $model->face_type == Contractor::TYPE_LEGAL_PERSON || $model->face_type == Contractor::TYPE_PHYSICAL_PERSON;
                },
                'whenClient' => 'function () {
                    return !contractorIsForeignLegal();
                }',
            ],
            [
                ['legal_address', 'actual_address',],
                'string',
                'max' => 255,
                'when' => function (Contractor $model) {
                    return $model->face_type == Contractor::TYPE_LEGAL_PERSON || $model->face_type == Contractor::TYPE_PHYSICAL_PERSON;
                },
                'whenClient' => 'function () {
                    return !contractorIsForeignLegal();
                }',
            ],
            [['postal_address',], 'string', 'max' => 255],
            [
                [
                    'physical_firstname',
                    'physical_lastname',
                    'physical_patronymic',
                ], 'string', 'max' => 45,
            ],
            [
                [
                    'is_deleted',
                    'contact_is_director',
                    'chief_accountant_is_director',
                    'not_accounting',
                    'director_in_act',
                    'chief_accountant_in_act',
                    'contact_in_act'
                ],
                'boolean',
            ],
            [['director_email', 'chief_accountant_email', 'contact_email'], 'email'],
            [ // chief accountant
                ['chief_accountant_name'],
                'required',
                'when' => function (Contractor $model) {
                    return $model->face_type == Contractor::TYPE_LEGAL_PERSON && !$this->chief_accountant_is_director;
                },
                'whenClient' => 'chiefAccountantIsNotDirector',
                'on' => 'insert',
            ],
            [
                ['chief_accountant_name', 'chief_accountant_phone'],
                'string',
                'max' => 45,
                'when' => function (Contractor $model) {
                    return !$this->chief_accountant_is_director;
                },
                'whenClient' => 'chiefAccountantIsNotDirector',
            ],
            [ // contact
                ['contact_name'],
                'required',
                'when' => function (Contractor $model) {
                    return (!$this->contact_is_director) && ($model->face_type == Contractor::TYPE_LEGAL_PERSON);
                },
                'whenClient' => 'function () {
                    return (contactIsNotDirector()) && (!contractorIsPhysical());
                }',
                'on' => 'insert',
            ],
            [
                ['contact_name', 'contact_phone'],
                'string',
                'max' => 45,
                'when' => function (Contractor $model) {
                    return !$this->contact_is_director;
                },
                'whenClient' => 'contactIsNotDirector',
                'on' => 'insert',
            ],
            [['current_account_guid', 'document_guid'], 'string', 'max' => 255],
            ['physical_passport_issued_by', 'string', 'max' => 160],
            [
                'physical_passport_department',
                'string',
                'max' => 255,
                'when' => function (Contractor $model, $attr) {
                    $pattern = true;
                    if ($model->physical_passport_isRf == 1) {
                        $pattern = preg_match('/^([0-9]{3})-([0-9]{3})/', $model->$attr);
                    } else {
                        $pattern = preg_match('/^[a-zа-яё\d\s]/i', $model->$attr);
                    }

                    $attributeLabels = $model->attributeLabels();

                    $message = isset($attributeLabels[$attr]) ?
                        '"' . $attributeLabels[$attr] . '"' :
                        '';
                    if (!$pattern) {
                        $this->addError($attr, "Значение $message неверно.");
                    }

                    return $model->face_type == Contractor::TYPE_LEGAL_PERSON || $model->face_type == Contractor::TYPE_PHYSICAL_PERSON;
                },
                'whenClient' => 'function () {
                    return !contractorIsForeignLegal();
                }',
            ],
            [
                'physical_passport_series',
                'string',
                'max' => 25,
                'when' => function (Contractor $model, $attr) {
                    $pattern = true;
                    if ($model->physical_passport_isRf == 1) {
                        $pattern = preg_match('/^([0-9]{2}) ([0-9]{2})/', $model->$attr);
                    } else {
                        $pattern = preg_match('/^[a-zа-яё\d\s]/i', $model->$attr);
                    }

                    $attributeLabels = $model->attributeLabels();
                    $message = isset($attributeLabels[$attr]) ?
                        '"' . $attributeLabels[$attr] . '"' :
                        '';

                    if (!$pattern) {
                        $this->addError($attr, "Значение $message неверно.");
                    }

                    return $model->face_type == Contractor::TYPE_LEGAL_PERSON || $model->face_type == Contractor::TYPE_PHYSICAL_PERSON;
                },
                'whenClient' => 'function () {
                    return !contractorIsForeignLegal();
                }',
            ],
            [
                'physical_passport_number',
                'string',
                'max' => 25,
                'when' => function (Contractor $model, $attr) {
                    $pattern = true;
                    if ($model->physical_passport_isRf == 1) {
                        $pattern = strlen($model->$attr) == 6 ? true : false;
                    } else {
                        $pattern = preg_match('/^[a-zа-яё\d\s]/i', $model->$attr);
                    }

                    $attributeLabels = $model->attributeLabels();
                    $message = isset($attributeLabels[$attr]) ?
                        '"' . $attributeLabels[$attr] . '"' :
                        '';

                    if (!$pattern) {
                        $this->addError($attr, "Значение $message неверно.");
                    }

                    return $model->face_type == Contractor::TYPE_LEGAL_PERSON || $model->face_type == Contractor::TYPE_PHYSICAL_PERSON;
                },
                'whenClient' => 'function () {
                    return !contractorIsForeignLegal();
                }',
            ],
            [
                'physical_address',
                'string',
                'max' => 255,
                'when' => function (Contractor $model) {
                    return $model->face_type == Contractor::TYPE_LEGAL_PERSON || $model->face_type == Contractor::TYPE_PHYSICAL_PERSON;
                },
                'whenClient' => 'function () {
                    return !contractorIsForeignLegal();
                }',
            ],
            [
                'responsible_employee_id',
                'exist',
                'targetClass' => EmployeeCompany::className(),
                'targetAttribute' => ['responsible_employee_id' => 'employee_id', 'company_id' => 'company_id'],
            ],
            ['comment', 'string'],

            [
                'physical_passport_isRf',
                'integer',
                'when' => function (Contractor $model) {
                    if ($this->physical_passport_isRf == 1) {
                        $this->physical_passport_country = '';
                    }
                    return true;
                },
            ],

            ['physical_passport_country', 'string', 'max' => 25],
            ['okpo', 'integer'],
            ['okpo', 'string', 'length' => 8, 'max' => 8],

            [
                ['opposite'],
                'boolean',
                'on' => 'insert',
            ],
            [
                [
                    'current_account',
                    'bank_name',
                    'BIC',
                    'corresp_account',
                ],
                'string',
            ],
            [
                [
                    'seller_discount',
                    'customer_discount',
                ],
                'number',
                'numberPattern' => '/^\d+(\.\d*)?$/',
                'min' => 0,
                'max' => 99.999999,
            ],
            [
                [
                    'seller_discount',
                    'customer_discount',
                ],
                'filter',
                'filter' => function ($value) {
                    return round($value, 6);
                }
            ],
            [['is_agent', 'verified'], 'boolean'],
            [['agent_payment_percent'], 'number', 'numberPattern' => '/^\d+(\.\d*)?$/', 'min' => 0, 'max' => 99.99],
            [
                ['agent_payment_type_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => ContractorAgentPaymentType::className(),
                'targetAttribute' => ['agent_payment_type_id' => 'id'],
                'message' => 'Тип платежа агента указан неверно.',
                'when' => function (Contractor $model) {
                    return $model->is_agent;
                },
                'whenClient' => 'function () {
                    return contractorIsAgent();
                }',
            ],
            ['hasAgentBuyers', 'each', 'rule' => ['validateBuyer', 'skipOnEmpty' => false]],
            [
                ['agent_start_date', 'agent_payment_percent', 'agent_payment_type_id', 'hasAgentBuyers'],
                'required',
                'when' => function (Contractor $model) {
                    return $model->is_agent;
                },
                'whenClient' => 'function () {
                    return contractorIsAgent();
                }',
            ],
            [['agent_agreement_id'], 'safe'],
            [
                [
                    'corr_bank_name',
                    'corr_bank_address',
                    'corr_bank_swift',
                ],
                'string',
                'max' => 255,
                'when' => function (Contractor $model) {
                    return $model->face_type == self::TYPE_FOREIGN_LEGAL_PERSON;
                },
            ],
            [['one_c_imported', 'one_c_exported'], 'safe'],

            [['activity_id'], 'integer'],
            [
                ['activity_id'],
                'exist',
                'targetClass' => ContractorActivity::class,
                'targetAttribute' => 'id',
                'filter' => function (ActiveQuery $query) {
                    $query->andWhere(['company_id' => $this->company_id]);
                },
            ],

            [['customer_group_id'],'integer'],
            [['seller_group_id'],'integer'],



            [['campaign_id'], 'integer'],
            [
                ['campaign_id'],
                'exist',
                'targetClass' => ContractorCampaign::class,
                'targetAttribute' => 'id',
                'filter' => function (ActiveQuery $query) {
                    $query->andWhere(['company_id' => $this->company_id]);
                },
            ],
            [['customer_industry_id', 'seller_industry_id', 'customer_sale_point_id', 'seller_sale_point_id'], 'filter', 'filter' => function($value) {
                return intval($value) ?: null;
            }],
            [['customer_industry_id', 'seller_industry_id', 'customer_sale_point_id', 'seller_sale_point_id'], 'integer'],
            [
                ['customer_industry_id', 'seller_industry_id'], 'exist',
                'targetClass' => CompanyIndustry::class,
                'targetAttribute' => 'id',
                //'filter' => ['company_id' => $this->company_id],
            ],
            [
                ['customer_sale_point_id', 'seller_sale_point_id'], 'exist',
                'targetClass' => SalePoint::class,
                'targetAttribute' => 'id',
                //'filter' => ['company_id' => $this->company_id],
            ],
            [
                [
                    'consignee_kpp_same',
                    'consignee_address_same',
                ],
                'default',
                'value' => '1',
            ],
            [
                [
                    'consignee_kpp_same',
                    'consignee_address_same',
                ],
                'boolean',
            ],
            [
                [
                    'consignee_kpp',
                    'consignee_address',
                ],
                'trim',
            ],
            [
                [
                    'consignee_kpp',
                ],
                'required',
                'when' => function ($model) {
                    return !boolval($model->consignee_kpp_same);
                },
                'message' => 'Укажите КПП или поставьте галочку справа',
            ],
            [
                [
                    'consignee_address',
                ],
                'required',
                'when' => function ($model) {
                    return !boolval($model->consignee_address_same);
                },
                'message' => 'Укажите адрес или поставьте галочку справа',
            ],
            [
                [
                    'consignee_kpp',
                ],
                'string',
                'max' => 50,
            ],
            [
                [
                    'consignee_address',
                ],
                'string',
                'max' => 500,
            ],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Компания',
            'contractorType' => 'Тип контрагента',
            'is_seller' => 'Поставщик',
            'is_customer' => 'Покупатель',
            'is_founder' => 'Учредитель',
            'type' => 'Тип контрагента',
            'status' => 'Статус',
            'company_type_id' => 'Форма',
            'companyTypeId' => 'Форма',
            'name' => 'Название контрагента',
            'director_name' => 'ФИО',
            'director_post_name' => 'Должность',
            'director_email' => 'E-mail',
            'director_phone' => 'Телефон',
            'chief_accountant_is_director' => 'Совпадает с руководителем',
            'chief_accountant_name' => 'ФИО',
            'chief_accountant_email' => 'Email',
            'chief_accountant_phone' => 'Телефон',
            'legal_address' => 'Юридический адрес',
            'actual_address' => 'Фактический адрес',
            'postal_address' => 'Адрес для почты',
            'BIN' => in_array($this->company_type_id, CompanyType::likeIpId()) ? 'ОГРНИП' : 'ОГРН',
            'ITN' => 'ИНН',
            'PPC' => 'КПП',
            'okpo' => 'ОКПО',
            'current_account' => 'Р/с',
            'bank_name' => 'Наименование банка',
            'bank_address' => 'Адрес банка',
            'bank_city' => 'Город',
            'corresp_account' => 'К/с',
            'BIC' => 'БИК',
            'contact_is_director' => 'Совпадает с руководителем',
            'contact_name' => 'ФИО',
            'contact_phone' => 'Телефон',
            'contact_email' => 'E-mail',
            'created_at' => 'Дата создания',
            'employee_id' => 'Employee ID',
            'taxation_system' => 'Налогообложение',
            'is_deleted' => 'Удалён?',
            'face_type' => 'Юр/Физ лицо/Иностранное юр.лицо',
            'physical_fio' => 'ФИО',
            'physical_passport_isRf' => 'Паспорт',
            'physical_passport_country' => 'Страна',
            'physical_firstname' => 'Имя физ. лица',
            'physical_lastname' => 'Фамилия физ. лица',
            'physical_patronymic' => 'Отчество физ. лица',
            'physical_no_patronymic' => 'Нет отчества',
            'physical_address' => 'Адрес регистрации физ. лица',
            'physical_passport_number' => 'Номер:',
            'physical_passport_series' => 'Серия:',
            'physical_passport_date_output' => 'Дата выдачи паспотрта физ. лица',
            'physical_passport_issued_by' => 'Кем выдан паспорт физ. лица',
            'physical_passport_department' => 'Код подразделения:',
            'invoice_expenditure_item_id' => 'Статья расходов',
            'invoice_income_item_id' => 'Статья приходов',
            'nds_view_type_id' => 'НДС во входящем счете',
            'responsible_employee_id' => 'Ответственный сотрудник',
            'comment' => 'Комментарии',
            'payment_delay' => 'Отсрочка оплаты в днях',
            'seller_payment_delay' => 'Отсрочка оплаты в днях',
            'customer_payment_delay' => 'Отсрочка оплаты в днях',
            'guaranty_delay' => 'Отсрочка выполнения обязательств в днях',
            'seller_guaranty_delay' => 'Отсрочка выполнения обязательств в днях',
            'customer_guaranty_delay' => 'Отсрочка выполнения обязательств в днях',
            'not_accounting' => 'Не для бухгалтерии',
            'opposite' => 'Покупатель и Поставщик',
            'discount' => 'Фиксированная скидка',
            'seller_discount' => 'Фиксированная скидка',
            'customer_discount' => 'Фиксированная скидка',
            'is_agent' => 'Агент',
            'agent_start_date' => 'Дата начала расчетов',
            'agent_payment_percent' => 'Комиссионное вознаграждение',
            'agent_payment_type_id' => 'Тип платежа',
            'hasAgentBuyers' => 'Покупатели агента',
            'agent_agreement_id' => 'Агентский договор',
            'director_in_act' => 'ФИО директора в акте',
            'chief_accountant_in_act' => 'ФИО главного бухгалтера в акте',
            'contact_in_act' => 'ФИО контакта в акте',
            'country_id' => 'Страна',
            'decoding_legal_form' => 'Расшифровка формы',
            'corr_bank_name' => 'Correspondent bank Name',
            'corr_bank_address' => 'Correspondent bank adress',
            'corr_bank_swift' => 'Correspondent bank SWIFT',
            'payment_priority' => 'Приоритет в оплате',
            'activity_id' => 'Вид деятельности',
            'campaign_id' => 'Источник',
            'seller_group_id' => 'Группа',
            'customer_group_id' => 'Группа',
            'group_name' =>'Группа',
            'consignee_kpp_same' =>'Совпадает с основным КПП',
            'consignee_kpp' =>'КПП в строке Грузополучатель',
            'consignee_address_same' =>'Совпадает с юр.адресом',
            'consignee_address' =>'Адрес в строке Грузополучатель',
        ];
    }

    /**
     * Returns the attribute hints.
     *
     * Attribute hints are mainly used for display purpose. For example, given an attribute
     * `isPublic`, we can declare a hint `Whether the post should be visible for not logged in users`,
     * which provides user-friendly description of the attribute meaning and can be displayed to end users.
     *
     * Unlike label hint will not be generated, if its explicit declaration is omitted.
     *
     * Note, in order to inherit hints defined in the parent class, a child class needs to
     * merge the parent hints with child hints using functions such as `array_merge()`.
     *
     * @return array attribute hints (name => hint)
     * @since 2.0.4
     */
    public function attributeHints()
    {
        return [
            'name' => 'Впишите название компании без организационно-правовой формы',
        ];
    }

    /**
     * @return string
     */
    public function getTypeLabel()
    {
        return isset(self::$contractorTitleSingle[$this->type]) ? self::$contractorTitleSingle[$this->type] : 'Контрагент';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreements()
    {
        return $this->hasMany(Agreement::className(), ['contractor_id' => 'id'])->onCondition(['is_created' => true]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRandomAgreement()
    {
        return $this->hasOne(Agreement::className(), ['contractor_id' => 'id'])->onCondition(['is_created' => true]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgentAgreement()
    {
        return $this->hasOne(Agreement::className(),
            ['id' => 'agent_agreement_id'])->onCondition(['is_created' => true]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyType()
    {
        return $this->hasOne(CompanyType::className(), ['id' => 'company_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashBankFlows()
    {
        return $this->hasMany(CashBankFlows::className(), ['contractor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashEmoneyFlows()
    {
        return $this->hasMany(CashEmoneyFlows::className(), ['contractor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashOrderFlows()
    {
        return $this->hasMany(CashOrderFlows::className(), ['contractor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    public function getGroup()
    {
        return $this->hasOne(ContractorGroup::className(), ['id' => 'group_id']);
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContractorAccounts()
    {
        return $this->hasMany(ContractorAccount::className(), ['contractor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrmClient()
    {
        return $this->hasOne(\frontend\modules\crm\models\Client::className(), ['contractor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrmTasks()
    {
        return $this->hasMany(\frontend\modules\crm\models\Task::className(), ['contractor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResponsibleEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'responsible_employee_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResponsibleEmployeeCompany()
    {
        return $this->hasOne(EmployeeCompany::className(), [
            'employee_id' => 'responsible_employee_id',
            'company_id' => 'company_id',
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserEmails()
    {
        return $this->hasMany(UserEmail::className(), ['contractor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoices()
    {
        return $this->hasMany(Invoice::className(), ['contractor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderDocuments()
    {
        return $this->hasMany(OrderDocument::className(), ['contractor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoicesAuto()
    {
        return $this->hasMany(InvoiceAuto::className(), ['contractor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpenditure()
    {
        return $this->hasOne(InvoiceExpenditureItem::className(), ['id' => 'invoice_expenditure_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIncome()
    {
        return $this->hasOne(InvoiceIncomeItem::className(), ['id' => 'invoice_income_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBank()
    {
        return $this->hasOne(BikDictionary::className(), ['bik' => 'bik'])->via('contractorAccounts',
            function ($query) {
                return $query->andWhere(['is_main' => 1]);
            });
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoreCompanyContractors()
    {
        return $this->hasMany(StoreCompanyContractor::className(), ['contractor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoreCompanies()
    {
        return $this->hasMany(StoreCompany::className(), ['id' => 'store_company_id'])
            ->viaTable(StoreCompanyContractor::tableName(), ['contractor_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTasks()
    {
        return $this->hasMany(Task::className(), ['contractor_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgentBuyers()
    {
        return $this->hasMany(Contractor::className(), ['id' => 'buyer_id'])
            ->viaTable(ContractorAgentBuyer::tableName(), ['agent_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgentBuyersInfo()
    {
        return $this->hasMany(ContractorAgentBuyer::className(), ['agent_id' => 'id'])->orderBy(['ord' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgentPaymentType()
    {
        return $this->hasOne(ContractorAgentPaymentType::className(), ['id' => 'agent_payment_type_id']);
    }

    /**
     * @return ContractorAccount
     */
    public function getContractorAccount()
    {
        if ($this->_contractorAccount === null) {
            $this->_contractorAccount = $this->getContractorAccounts()->orderBy([
                'is_main' => SORT_DESC,
            ])->one() ?: new ContractorAccount([
                'contractor_id' => $this->id,
            ]);
            $this->_contractorAccount->populateRelation('contractor', $this);
        }

        return $this->_contractorAccount;
    }

    /**
     * @param integer $bik
     * @param integer $rs
     * @return boolean
     */
    public function hasAccount($bik, $rs)
    {
        return $this->getContractorAccounts()->andWhere(['bik' => $bik, 'rs' => $rs])->exists();
    }

    /**
     * @param integer $bik
     * @param integer $rs
     * @return boolean
     */
    public function addAccount($bik, $rs)
    {
        $model = new ContractorAccount([
            'contractor_id' => $this->id,
            'bik' => $bik,
            'rs' => $rs,
        ]);

        return $model->save();
    }

    /**
     * @return string
     */
    public function setCurrent_account($value)
    {
        return $this->getContractorAccount()->rs = $value;
    }

    /**
     * @return string
     */
    public function setBank_name($value)
    {
        return $this->getContractorAccount()->bank_name = $value;
    }

    /**
     * @return string
     */
    public function setBank_city($value)
    {
        return $this->getContractorAccount()->bank_city = $value;
    }

    /**
     * @return string
     */
    public function setBank_address($value)
    {
        return $this->getContractorAccount()->bank_address = $value;
    }

    /**
     * @return string
     */
    public function setSwift($value)
    {
        return $this->getContractorAccount()->swift = $value;
    }

    /**
     * @return string
     */
    public function setBIC($value)
    {
        return $this->getContractorAccount()->bik = $value;
    }

    /**
     * @return string
     */
    public function setCorresp_account($value)
    {
        return $this->getContractorAccount()->ks = $value;
    }

    /**
     * @return string
     */
    public function setCorr_bank_name($value)
    {
        return $this->getContractorAccount()->corr_bank_name = $value;
    }

    /**
     * @return string
     */
    public function setCorr_bank_address($value)
    {
        return $this->getContractorAccount()->corr_bank_address = $value;
    }

    /**
     * @return string
     */
    public function setCorr_bank_swift($value)
    {
        return $this->getContractorAccount()->corr_bank_swift = $value;
    }

    /**
     * @return string
     */
    public function getCurrent_account()
    {
        return $this->getContractorAccount()->rs;
    }

    /**
     * @return string
     */
    public function getBank_name()
    {
        return $this->getContractorAccount()->bank_name;
    }

    /**
     * @return string
     */
    public function getBank_city()
    {
        return $this->getContractorAccount()->bank_city;
    }

    /**
     * @return string
     */
    public function getBank_address()
    {
        return $this->getContractorAccount()->bank_address;
    }

    /**
     * @return string
     */
    public function getSwift()
    {
        return $this->getContractorAccount()->swift;
    }

    /**
     * @return string
     */
    public function getBIC()
    {
        return $this->getContractorAccount()->bik;
    }

    /**
     * @return string
     */
    public function getCorresp_account()
    {
        return $this->getContractorAccount()->ks;
    }

    /**
     * @return string
     */
    public function getCorr_bank_name()
    {
        return $this->getContractorAccount()->corr_bank_name;
    }

    /**
     * @return string
     */
    public function getCorr_bank_address()
    {
        return $this->getContractorAccount()->corr_bank_address;
    }

    /**
     * @return string
     */
    public function getCorr_bank_swift()
    {
        return $this->getContractorAccount()->corr_bank_swift;
    }

    /**
     * @param $val
     */
    public function setCompanyTypeId($val)
    {
        $val = (int) $val;

        return $this->company_type_id = $val == -1 ? null : $val;
    }

    /**
     * @return integer
     */
    public function getCompanyTypeId()
    {
        return $this->company_type_id ?: -1;
    }

    /**
     * @return mixed
     */
    public function getFirstPayedInvoiceDate()
    {
        return Invoice::find()->byContractorId($this->id)->byStatus(InvoiceStatus::STATUS_PAYED)->min('invoice_status_updated_at');
    }

    /**
     * @return $this
     */
    public function getPaymentReminderMessageContractor()
    {
        return $this->hasOne(PaymentReminderMessageContractor::className(), ['contractor_id' => 'id']);
    }

    /**
     * sorting Contractors by company type and name
     *
     * @return ContractorQuery
     */
    public static function getSorted()
    {
        $tContractor = self::tableName();
        $tType = CompanyType::tableName();

        return Contractor::find()
            ->joinWith('companyType')
            ->orderBy([
                new \yii\db\Expression("ISNULL({{{$tType}}}.[[name_short]])"),
                "$tType.name_short" => SORT_ASC,
                "$tContractor.name" => SORT_ASC,
            ]);
    }

    /**
     * @return CompanyType
     */
    public function companyType()
    {
        return CompanyType::findOne($this->company_type_id);
    }

    /**
     * @return string
     */
    public function getShortName($shortFio = false)
    {
        if ($this->face_type == Contractor::TYPE_FOREIGN_LEGAL_PERSON) {
            return $this->foreign_legal_form.' '.$this->name;
        }

        $name = '';
        if ($this->face_type == Contractor::TYPE_LEGAL_PERSON && ($companyType = $this->companyType)) {
            $name .= $companyType->name_short . ' ';
        }

        if (in_array($this->company_type_id, CompanyType::likeIpId()) && $shortFio) {
            $name .= $this->directorFio;
        } else {
            $name .= $this->name;
        }

        return $name;
    }

    /**
     * @return int
     */
    public function getNotPaidInvoiceCount()
    {
        return 0;
    }

    /**
     * @return string
     */
    public function getRealContactName()
    {
        return $this->contact_is_director ? $this->director_name : $this->contact_name;
    }

    /**
     * @return string
     */
    public function getRealContactEmail()
    {
        return $this->contact_is_director ? $this->director_email : $this->contact_email;
    }

    /**
     * @return string
     */
    public function getRealContactPhone()
    {
        return $this->contact_is_director ? $this->director_phone : $this->contact_phone;
    }

    /**
     * @return bool
     */
    public function getDeleteItemCondition()
    {
        $activeInvoices = $this->getInvoices()->andFilterWhere([
            'is_deleted' => Invoice::NOT_IS_DELETED,
        ])->exists();
        $activeAutoInvoices = $this->getInvoicesAuto()->andFilterWhere([
            'is_deleted' => Invoice::NOT_IS_DELETED,
        ])->exists();

        return !($activeInvoices || $activeAutoInvoices);
    }

    /**
     * @return bool
     */
    public function hasCashFlows()
    {
        return $this->getCashOrderFlows()->exists() || $this->getCashBankFlows()->exists() || $this->getCashEmoneyFlows()->exists();
    }

    /**
     * @param bool $short
     * @return string
     */
    public function getCompanyTypeName($short = true)
    {
        if ($this->face_type == self::TYPE_FOREIGN_LEGAL_PERSON) {
            return (string) $this->foreign_legal_form;
        } else {
            return (string) CompanyType::getValue($this->company_type_id, $short ? 'name_short' : 'name_full', '');
        }
    }

    /**
     * @param bool|false $short
     * @return string
     */
    public function getTitle($short = false)
    {
        $pieces = [
            $this->getCompanyTypeName($short),
            $this->name,
        ];
        $glue = ' ';
        if ($this->face_type == self::TYPE_FOREIGN_LEGAL_PERSON) {
            $pieces = array_reverse($pieces);
            $glue = ', ';
        }
        return implode($glue, $pieces);
    }

    /**
     * @return string
     */
    public function getShortTitle()
    {
        return $this->getTitle(true);
    }

    /**
     * @return string
     */
    public function getNameWithType()
    {
        return $this->getTitle(true);
    }

    /**
     * @return array
     */
    public function getTypeArray()
    {
        $defaultTypes = ArrayHelper::map(CompanyType::find()->inContractor()->andWhere(['<', 'id', 5])->all(), 'id',
            'name_short');
        $sortingTypes = ArrayHelper::map(CompanyType::find()->inContractor()->andWhere([
            '>',
            'id',
            4
        ])->orderBy('name_short')->all(), 'id', 'name_short');

        return [-1 => ''] + ArrayHelper::merge($defaultTypes, $sortingTypes);
    }

    /**
     * @param           $type
     * @param bool|true $addNew
     * @param array     $exceptIds
     * @return array
     */
    public static function getAllContractorList($type, $addNew = true, $exceptIds = null)
    {
        $addNewName = $type == Contractor::TYPE_CUSTOMER ? 'покупателя' : 'поставщика';
        $filterArray = [];
        if ($addNew) {
            $filterArray["add-modal-contractor"] = '[ + Добавить ' . $addNewName . ' ]';
        }
        $contractorArray = Contractor::getSorted()
            ->select([
                'contractor_name' => 'CONCAT_WS(" ", {{company_type}}.[[name_short]], {{contractor}}.[[name]])',
                'contractor.id',
            ])
            ->byCompany(Yii::$app->user->identity->company->id)
            ->byContractor($type)
            ->byIsDeleted(Contractor::NOT_DELETED)
            ->byStatus(Contractor::ACTIVE)
            ->byExceptedIds($exceptIds)
            ->indexBy('id')
            ->column();

        return ($filterArray + $contractorArray);
    }

    /**
     * @param           $type
     * @param bool|true $addNew
     * @return array
     */
    public static function getAllContractorDuplicate($type)
    {
        $kppArray = [];
        $contractors = Contractor::find()
            ->select(['id', 'ITN', 'PPC'])
            ->byCompany(Yii::$app->user->identity->company->id)
            ->byContractor($type)
            ->byIsDeleted(Contractor::NOT_DELETED)
            ->byStatus(Contractor::ACTIVE)
            ->orderBy(['ITN' => SORT_ASC, 'PPC' => SORT_ASC, 'id' => SORT_DESC])
            ->asArray()
            ->all();

        if (count($contractors) > 1) {
            for ($n = 1; $n < count($contractors); $n++) {
                if ($contractors[$n - 1]['ITN'] == $contractors[$n]['ITN']) {
                    $kppArray[$contractors[$n]['id']] = ['data-kpp' => 'КПП ' . $contractors[$n]['PPC']];
                    if ($contractors[$n - 1]['PPC'] == $contractors[$n]['PPC']) {
                        $kppArray[$contractors[$n - 1]['id']] = ['data-kpp' => 'ДУБЛЬ'];
                    } else {
                        $kppArray[$contractors[$n - 1]['id']] = ['data-kpp' => 'КПП ' . $contractors[$n - 1]['PPC']];
                    }
                }
            }
        }

        return $kppArray;
    }

    /**
     * @param mixed        $type
     * @param null|Company $company
     * @return array
     */
    public static function getAllContractorSelect2Options($type, $company = null)
    {
        if (!$company) {
            $company = Yii::$app->user->identity->company;
        }

        $optionsArray = [];
        $contractors = Contractor::find()
            ->select(['id', 'ITN', 'PPC', 'verified', 'face_type'])
            ->byCompany($company->id)
            ->byContractor($type)
            ->byIsDeleted(Contractor::NOT_DELETED)
            ->byStatus(Contractor::ACTIVE)
            ->orderBy(['ITN' => SORT_ASC, 'PPC' => SORT_ASC, 'id' => SORT_DESC])
            ->asArray()
            ->all();

        foreach ($contractors as $key => $contractor) {
            $optionsArray[$contractor['id']] = ['data-inn' => $contractor['ITN']];
        }

        if (count($contractors) > 1) {
            for ($n = 1; $n < count($contractors); $n++) {
                if ($contractors[$n - 1]['ITN'] == $contractors[$n]['ITN']) {
                    $optionsArray[$contractors[$n]['id']]['data-kpp'] = 'КПП ' . $contractors[$n]['PPC'];
                    if ($contractors[$n - 1]['PPC'] == $contractors[$n]['PPC']) {
                        $optionsArray[$contractors[$n - 1]['id']]['data-kpp'] = 'ДУБЛЬ';
                    } else {
                        $optionsArray[$contractors[$n - 1]['id']]['data-kpp'] = 'КПП ' . $contractors[$n - 1]['PPC'];
                    }
                }
                $optionsArray[$contractors[$n - 1]['id']]['data-verified'] = $contractors[$n - 1]['verified'];
                $optionsArray[$contractors[$n - 1]['id']]['data-face_type'] = $contractors[$n - 1]['face_type'];
            }
        }

        return $optionsArray;
    }

    /**
     * @param $type
     * @return array
     */
    public static function getAllContractorListArray($type)
    {
        $modelArray = self::getSorted()
            ->byCompany(Yii::$app->user->identity->company->id)
            ->byContractor($type)
            ->byIsDeleted(self::NOT_DELETED)
            ->byStatus(self::ACTIVE)
            ->all();

        return ArrayHelper::map($modelArray, 'id', 'nameWithType');
    }

    /**
     * @param $type
     * @return array
     */
    public static function getAllContractorListForJs($type)
    {
        return array_map(
            [
                Contractor::className(),
                'getContractorName'
            ],
            Contractor::getSorted()
                ->byCompany(Yii::$app->user->identity->company->id)
                ->byContractor($type)
                ->byIsDeleted(Contractor::NOT_DELETED)
                ->byStatus(Contractor::ACTIVE)
                ->all()
        );
    }

    /**
     * @param Contractor $contractor
     * @return array
     */
    public static function getContractorName(Contractor $contractor)
    {
        if ($contractor->company_type_id !== null) {
            if (in_array($contractor->company_type_id, CompanyType::likeIpId())) {
                $result[][$contractor->id] = $contractor->companyType->name_short . ' ' . $contractor->name;
            } else {
                $result[][$contractor->id] = $contractor->companyType->name_short . ' "' . $contractor->name . '"';
            }
        } else {
            $result[][$contractor->id] = $contractor->name;
        }

        return $result;
    }

    /**
     * @param Company $company
     * @param bool    $isIn
     * @return Invoice[]
     */
    public function getUnPayedInvoices(Company $company, $isIn, $orderBy = ['document_number' => SORT_ASC], $maxDate = null)
    {
        $ioType = $isIn ? Documents::IO_TYPE_IN : Documents::IO_TYPE_OUT;

        // Все статусы, кроме оплаченного
        $statuses = array_diff(ArrayHelper::getValue(InvoiceStatus::$statusByType, $ioType), [
            InvoiceStatus::STATUS_PAYED,
            InvoiceStatus::STATUS_REJECTED,
        ]);

        $query = Invoice::find()
            ->byIOType($ioType)
            ->byCompany($company->id)
            ->byContractorId($this->id)
            ->byStatus($statuses)
            ->byDeleted(false)
            ->orderBy($orderBy);

        if ($maxDate !== null) {
            if (is_string($maxDate)) {
                $maxDate = date_create($maxDate);
            }
        }

        if ($maxDate instanceof \DateTimeInterface) {
            $query->andWhere([
                '<=',
                'document_date',
                $maxDate->format('Y-m-d'),
            ]);
        }

        return $query->all();
    }

    /**
     * @param Company $company
     * @param bool    $isIn
     * @return array
     */
    public function getUnPayedInvoicesJs(Company $company, $isIn, $flowDate = null)
    {
        $invoices = $this->getUnPayedInvoices($company, $isIn);
        $date = $flowDate ? \DateTime::createFromFormat('d.m.Y|', $flowDate) : false;

        return array_map(function (Invoice $value) use ($date) {
            if ($date) {
                $value->recalculateAmountOnDate($date);
            }

            return [
                'id' => $value->id,
                'number' => $value->fullNumber,
                'date' => ($d = date_create_from_format('Y-m-d', $value->document_date)) ? $d->format('d.m.Y') : '',
                'amount' => $value->total_amount_with_nds / 100,
                'amountPrint' => \common\components\TextHelper::invoiceMoneyFormat($value->total_amount_with_nds, 2),
                'amountRemaining' => $value->remaining_amount / 100,
                'projectId' => $value->project_id
            ];
        }, $invoices);
    }

    /**
     * @return int
     */
    public function getNdsViewTypeId()
    {
        if (\Yii::$app->params['service']['contractor_id'] !== $this->id && $this->nds_view_type_id !== null) {
            return $this->nds_view_type_id;
        } elseif ($this->taxation_system == self::WITHOUT_NDS) {
            return Invoice::NDS_VIEW_WITHOUT;
        } else {
            return Invoice::NDS_VIEW_IN;
        }
    }

    /**
     * @return boolean
     */
    public function getIsOutInvoiceHasNds()
    {
        $nds_view_type_id = $this->getInvoices()
            ->select('nds_view_type_id')
            ->andWhere([
                'is_deleted' => false,
                'type' => Documents::IO_TYPE_OUT,
            ])
            ->orderBy(['id' => SORT_DESC])
            ->limit(1)
            ->scalar();

        return $nds_view_type_id !== null && $nds_view_type_id !== false && $nds_view_type_id != Invoice::NDS_VIEW_WITHOUT;
    }

    /**
     * @return string
     */
    public function getCurrentDebt()
    {
        return TextHelper::invoiceMoneyFormat($this->getInvoices()
            ->andWhere([Invoice::tableName() . '.type' => Documents::IO_TYPE_OUT,])
            ->andWhere(['>', Invoice::tableName() . '.payment_limit_date', date(DateHelper::FORMAT_DATE)])
            ->andWhere([Invoice::tableName() . '.is_deleted' => false])
            ->andWhere(['not', [Invoice::tableName() . '.invoice_status_id' => [InvoiceStatus::STATUS_OVERDUE, InvoiceStatus::STATUS_PAYED]]])
            ->sum('total_amount_with_nds'), 2);
    }

    /**
     * @return Contractor
     */
    public static function getBasicContractorModel()
    {
        if (isset(Yii::$app->request->post('Contractor')['face_type'])) {
            $scenario = Yii::$app->request->post('Contractor')['face_type'] == 1 ? Contractor::SCENARIO_FIZ_FACE : Contractor::SCENARIO_LEGAL_FACE;
        } else {
            $scenario = Contractor::SCENARIO_LEGAL_FACE;
        }
        $contractor = new Contractor([
            'type' => Contractor::TYPE_CUSTOMER,
            'status' => Contractor::ACTIVE,
            'scenario' => $scenario,
        ]);

        return $contractor;
    }

    /**
     * @return string
     */
    public function getPassportText()
    {
        $string = '';

        if ($this->physical_passport_number && $this->physical_passport_series
            && $this->physical_passport_date_output && $this->physical_passport_issued_by
        ) {
            $string .= 'Пасспорт гражданина РФ, серия ' . $this->physical_passport_series;
            $string .= ', № ' . $this->physical_passport_number;
            $string .= ', выдан ' . date('d.m.Y', strtotime($this->physical_passport_date_output)) . ' года';
            $string .= ', ' . $this->physical_passport_issued_by;
        }

        return $string;
    }

    /**
     * @return common\models\Agreement|null
     */
    public function getLastAgreement()
    {
        return $this->getAgreements()->orderBy(['document_date' => SORT_DESC])->one();
    }

    /**
     * @return string|null
     */
    public function getSomeEmail()
    {
        return $this->director_email ?
            $this->director_email :
            (
            $this->chief_accountant_email ?
                $this->chief_accountant_email :
                (
                $this->contact_email ?
                    $this->contact_email :
                    null
                )
            );
    }

    /**
     * @return string
     */
    public function getDirectorFio()
    {
        $name = '';
        $nameIremsArray = explode(' ', $this->director_name);
        foreach ($nameIremsArray as $key => $value) {
            $name .= $key ? mb_substr($value, 0, 1) . '.' : $value . ' ';
        }

        return $name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotLinkedBankFlows(Company $company)
    {
        return $this->getCashBankFlows()->alias('flow')
            ->leftJoin([
                'paid' => CashBankFlowToInvoice::find()
                    ->select(['flow_id', 'invoices_amount' => new Expression("SUM([[amount]])")])
                    ->groupBy('flow_id')
            ], '{{flow}}.[[id]] = {{paid}}.[[flow_id]]')
            ->andWhere(['flow.company_id' => $company->id])
            ->andWhere(['>', 'flow.amount', new Expression("IFNULL({{paid}}.[[invoices_amount]], 0)")]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotLinkedOrderFlows(Company $company)
    {
        return $this->getCashOrderFlows()->alias('flow')
            ->leftJoin([
                'paid' => CashOrderFlowToInvoice::find()
                    ->select(['flow_id', 'invoices_amount' => new Expression("SUM([[amount]])")])
                    ->groupBy('flow_id')
            ], '{{flow}}.[[id]] = {{paid}}.[[flow_id]]')
            ->andWhere(['flow.company_id' => $company->id])
            ->andWhere(['>', 'flow.amount', new Expression("IFNULL({{paid}}.[[invoices_amount]], 0)")]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotLinkedEmoneyFlows(Company $company)
    {
        return $this->getCashEmoneyFlows()->alias('flow')
            ->leftJoin([
                'paid' => CashEmoneyFlowToInvoice::find()
                    ->select(['flow_id', 'invoices_amount' => new Expression("SUM([[amount]])")])
                    ->groupBy('flow_id')
            ], '{{flow}}.[[id]] = {{paid}}.[[flow_id]]')
            ->andWhere(['flow.company_id' => $company->id])
            ->andWhere(['>', 'flow.amount', new Expression("IFNULL({{paid}}.[[invoices_amount]], 0)")]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastAgreementStr($nbsp = true)
    {
        $space = $nbsp ? '&nbsp;' : '';
        $content = '';
        $agreement = $this->lastAgreement;
        if ($agreement) {
            $content .= $agreement->agreementType->name;
            $content .= " №{$space}" . Html::encode($agreement->document_number);
            $content .= " от{$space}" . (new \DateTime($agreement->document_date))->format('d.m.Y');
        }

        return $content;
    }

    /**
     * @param $group
     * @return string
     */
    public function getDisciplineGroupColor($group)
    {
        $searchModel = new DisciplineSearch([
            'company' => Yii::$app->user->identity->company,
            'period' => DisciplineSearch::DEFAULT_PERIOD,
        ]);

        return $searchModel->getGroupColor($group);
    }

    /**
     * @param      $group
     * @param bool $full
     * @return string
     */
    public function getGroupText($group, $full = false)
    {
        switch ($group) {
            case AnalysisSearch::GROUP_A:
                return $full ? 'Ключевые' : 'A';
                break;
            case AnalysisSearch::GROUP_B:
                return $full ? 'Второстепенные' : 'B';
                break;
            case AnalysisSearch::GROUP_C:
                return $full ? 'Прочие' : 'C';
                break;
            default:
                return $full ? 'Прочие' : 'C';
        }
    }

    /**
     * @param $group
     * @return string
     */
    public function getGroupColor($group)
    {
        switch ($group) {
            case AnalysisSearch::GROUP_A:
                return '#7dde81';
                break;
            case AnalysisSearch::GROUP_B:
                return '#dfba49';
                break;
            case AnalysisSearch::GROUP_C:
                return '#f3555d';
                break;
            default:
                return '#f3555d';
        }
    }

    /**
     * @param $group
     * @return string
     */
    public function getCustomerTurnoverData()
    {
        $query = ProductTurnover::find()->select([
            'proceeds' => 'SUM([[total_amount]])',
            'cost_price' => 'SUM([[purchase_amount]])',
            'profit' => 'SUM([[total_amount]]-[[purchase_amount]])',
        ])->andWhere([
            'company_id' => $this->company_id,
            'contractor_id' => $this->id,
            'is_invoice_actual' => 1,
            'is_document_actual' => 1,
        ]);

        return $query->asArray()->one();
    }

    /**
     * @return float|int
     */
    public function getTotalRevenue()
    {
        $actSum = Act::find()
            ->joinWith('invoice')
            ->joinWith('orderActs')
            ->joinWith('orderActs.order')
            ->andWhere([Invoice::tableName() . '.contractor_id' => $this->id])
            ->andWhere([Invoice::tableName() . '.type' => Documents::IO_TYPE_OUT])
            ->andWhere([Invoice::tableName() . '.is_deleted' => Invoice::NOT_IS_DELETED])
            ->andWhere(['not', ['invoice_status_id' => InvoiceStatus::STATUS_REJECTED]])
            ->sum(Order::tableName() . '.amount_sales_with_vat * ' . OrderAct::tableName() . '.quantity / ' . Order::tableName() . '.quantity');
        $packingListSum = PackingList::find()
            ->joinWith('invoice')
            ->joinWith('orderPackingLists')
            ->joinWith('orderPackingLists.order')
            ->andWhere([Invoice::tableName() . '.contractor_id' => $this->id])
            ->andWhere([Invoice::tableName() . '.type' => Documents::IO_TYPE_OUT])
            ->andWhere([Invoice::tableName() . '.is_deleted' => Invoice::NOT_IS_DELETED])
            ->andWhere(['not', ['invoice_status_id' => InvoiceStatus::STATUS_REJECTED]])
            ->sum(Order::tableName() . '.amount_sales_with_vat * ' . OrderPackingList::tableName() . '.quantity / ' . Order::tableName() . '.quantity');
        $updSum = Upd::find()
            ->joinWith('invoice')
            ->joinWith('orders')
            ->andWhere([Invoice::tableName() . '.contractor_id' => $this->id])
            ->andWhere([Invoice::tableName() . '.type' => Documents::IO_TYPE_OUT])
            ->andWhere([Invoice::tableName() . '.is_deleted' => Invoice::NOT_IS_DELETED])
            ->andWhere(['not', ['invoice_status_id' => InvoiceStatus::STATUS_REJECTED]])
            ->sum(Order::tableName() . '.amount_sales_with_vat * ' . OrderUpd::tableName() . '.quantity / ' . Order::tableName() . '.quantity');

        return ($actSum + $packingListSum + $updSum) * 1;
    }

    /**
     * @return float|int
     */
    public function getTotalCostPrice()
    {
        $actSum = Act::find()
            ->joinWith('invoice')
            ->joinWith('orders')
            ->joinWith('orders.product')
            ->andWhere([Invoice::tableName() . '.contractor_id' => $this->id])
            ->andWhere([Invoice::tableName() . '.type' => Documents::IO_TYPE_OUT])
            ->andWhere([Invoice::tableName() . '.is_deleted' => Invoice::NOT_IS_DELETED])
            ->andWhere(['not', ['invoice_status_id' => InvoiceStatus::STATUS_REJECTED]])
            ->sum(Product::tableName() . '.price_for_sell_with_nds * ' . OrderAct::tableName() . '.quantity / ' . Order::tableName() . '.quantity');

        $packingListSum = PackingList::find()
            ->joinWith('invoice')
            ->joinWith('orderPackingLists')
            ->joinWith('orderPackingLists.order')
            ->joinWith('orderPackingLists.order.product')
            ->andWhere([Invoice::tableName() . '.contractor_id' => $this->id])
            ->andWhere([Invoice::tableName() . '.type' => Documents::IO_TYPE_OUT])
            ->andWhere([Invoice::tableName() . '.is_deleted' => Invoice::NOT_IS_DELETED])
            ->andWhere(['not', ['invoice_status_id' => InvoiceStatus::STATUS_REJECTED]])
            ->sum(Product::tableName() . '.price_for_sell_with_nds * ' . OrderPackingList::tableName() . '.quantity / ' . Order::tableName() . '.quantity');

        $updSum = Upd::find()
            ->joinWith('invoice')
            ->joinWith('orders')
            ->joinWith('orders.product')
            ->andWhere([Invoice::tableName() . '.contractor_id' => $this->id])
            ->andWhere([Invoice::tableName() . '.type' => Documents::IO_TYPE_OUT])
            ->andWhere([Invoice::tableName() . '.is_deleted' => Invoice::NOT_IS_DELETED])
            ->andWhere(['not', ['invoice_status_id' => InvoiceStatus::STATUS_REJECTED]])
            ->sum(Product::tableName() . '.price_for_sell_with_nds * ' . OrderUpd::tableName() . '.quantity / ' . Order::tableName() . '.quantity');

        return ($actSum + $packingListSum + $updSum) * 1;
    }

    /**
     * @return mixed
     */
    public function getAllBilledAmount()
    {
        return $this->getInvoices()
            ->andWhere(['type' => Documents::IO_TYPE_OUT])
            ->andWhere(['is_deleted' => Invoice::NOT_IS_DELETED])
            ->andWhere(['not', ['invoice_status_id' => InvoiceStatus::STATUS_REJECTED]])
            ->sum('total_amount_with_nds');
    }

    /**
     * @return mixed|null
     */
    public function getAllPayedInvoicesAmount()
    {
        $amount = null;
        /* @var $cashFlowClassName CashBankFlows|CashOrderFlows|CashEmoneyFlows */
        foreach ([
                     CashBankFlows::className(),
                     CashOrderFlows::className(),
                     CashEmoneyFlows::className(),
                 ] as $cashFlowClassName) {
            $amount += $cashFlowClassName::find()
                ->andWhere(['flow_type' => CashFlowsBase::FLOW_TYPE_INCOME])
                ->andWhere(['contractor_id' => $this->id])
                ->sum('amount');
        }
        return $amount;
    }

    /**
     * @param $ioType
     * @return mixed
     */
    public function getAnalyticsDateItems($ioType)
    {
        if (!isset($this->cache['analyticsDateItems'])) {
            $this->cache['analyticsDateItems'] = [
                Documents::IO_TYPE_IN => [],
                Documents::IO_TYPE_OUT => [],
            ];
        }

        if (!$this->cache['analyticsDateItems'][$ioType]) {
            $startDate = $this->getInvoices()
                ->andWhere([Invoice::tableName() . '.type' => $ioType])
                ->andWhere([Invoice::tableName() . '.is_deleted' => 0,])
                ->min('document_date');
            $startDate = $startDate ? DateHelper::format($startDate, 'Y-m', DateHelper::FORMAT_DATE) : date('Y-m');
            $currentDate = date('Y-m');

            do {
                $items[$startDate] = FlowOfFundsReportSearch::$month[DateHelper::format($startDate, 'm', 'Y-m')];
                $startDate = date('Y-m', strtotime('+1 month', strtotime($startDate)));
            } while ($startDate <= $currentDate);
            krsort($items);
            $this->cache['analyticsDateItems'][$ioType] = $items;
        }

        return $this->cache['analyticsDateItems'][$ioType];
    }

    /**
     * @return mixed
     */
    public function getActivationDate()
    {
        $date[] = $this->created_at;
        $minInvoiceDate = $this->getInvoices()
            ->andWhere(['is_deleted' => false])
            ->min('document_date');
        if ($minInvoiceDate) {
            $date[] = strtotime($minInvoiceDate);
        }
        /* @var $cashFlowClassName CashBankFlows|CashOrderFlows|CashEmoneyFlows */
        foreach ([
                     CashBankFlows::className(),
                     CashOrderFlows::className(),
                     CashEmoneyFlows::className(),
                 ] as $cashFlowClassName) {
            $minCashFlowDate = $cashFlowClassName::find()
                ->andWhere(['contractor_id' => $this->id])
                ->min('date');
            if ($minCashFlowDate) {
                $date[] = strtotime($minCashFlowDate);
            }
        }

        return min($date);
    }

    /**
     * @param $monthNumber
     * @param $yearNumber
     * @return mixed
     */
    public function getAnalyticsChartDate($monthNumber, $yearNumber)
    {
        if (!isset($this->cache['analyticsChartDate'])) {
            $i = 1;
            do {
                $month[] = [
                    'monthNumber' => $monthNumber,
                    'year' => $yearNumber,
                    'monthText' => Month::$monthShortByFull[$monthNumber],
                ];
                $i++;
                $newDate = strtotime('-1 month', strtotime("01.{$monthNumber}.{$yearNumber}"));
                $monthNumber = date('m', $newDate);
                $yearNumber = date('Y', $newDate);
            } while ($i <= 12);
            krsort($month);
            $this->cache['analyticsChartDate'] = $month;
        }

        return $this->cache['analyticsChartDate'];
    }

    /**
     * @param $monthNumber
     * @param $yearNumber
     * @return mixed
     */
    public function getSellingChartDate($monthNumber, $yearNumber)
    {
        if (!isset($this->cache['sellingChartDate'])) {
            $i = 1;
            do {
                $month[] = [
                    'monthNumber' => $monthNumber,
                    'year' => $yearNumber,
                    'monthText' => Month::$monthShortByFull[$monthNumber],
                ];
                $i++;
                $newDate = strtotime('-1 month', strtotime("01.{$monthNumber}.{$yearNumber}"));
                $monthNumber = date('m', $newDate);
                $yearNumber = date('Y', $newDate);
            } while ($i <= 12);
            krsort($month);
            $this->cache['sellingChartDate'] = $month;
        }

        return $this->cache['sellingChartDate'];
    }

    /**
     * @param $monthNumber
     * @param $yearNumber
     * @return array
     */
    public function getAnalyticsInvoiceAmountHighChartsOptions($monthNumber, $yearNumber)
    {
        $categories = [];
        $data = [];
        $month = $this->getAnalyticsChartDate($monthNumber, $yearNumber);
        foreach ($month as $key => $dataMonth) {
            $categories[] = $dataMonth['monthText'] . ' ' . $dataMonth['year'];
            $data[] = (int)$this->getInvoices()
                    ->andWhere(['type' => Documents::IO_TYPE_OUT])
                    ->andWhere(['is_deleted' => false])
                    ->andWhere([
                        'between',
                        Invoice::tableName() . '.document_date',
                        $dataMonth['year'] . '-' . $dataMonth['monthNumber'] . '-01',
                        $dataMonth['year'] . '-' . $dataMonth['monthNumber'] . '-' . cal_days_in_month(CAL_GREGORIAN,
                            $dataMonth['monthNumber'], $dataMonth['year'])
                    ])->sum('total_amount_with_nds') / 100;
        }
        $series = [
            0 => [
                'name' => 'Сумма',
                'color' => '#45b6af',
                'data' => $data,
            ],
        ];

        return [
            'chart' => [
                'type' => 'column',
            ],
            'title' => [
                'text' => '',
            ],
            'lang' => [
                'printChart' => 'На печать',
                'downloadPNG' => 'Скачать PNG',
                'downloadJPEG' => 'Скачать JPEG',
                'downloadPDF' => 'Скачать PDF',
                'downloadSVG' => 'Скачать SVG',
                'contextButtonTitle' => 'Меню',
            ],
            'xAxis' => [
                'categories' => $categories,
            ],
            'yAxis' => [
                'min' => 0,
                'title' => [
                    'text' => 'Счета <i class="fa fa-rub"></i>',
                    'useHTML' => true,
                ],
                'labels' => [
                    'enabled' => false,
                ],
            ],
            'series' => array_values($series),
            'legend' => [
                'enabled' => false,
            ],
            'plotOptions' => [
                'column' => [
                    'dataLabels' => [
                        'enabled' => false,
                    ],
                    'tooltip' => [
                        'valueDecimals' => 2,
                    ],
                    'pointWidth' => 30,
                ],
            ],
        ];
    }

    /**
     * @param $monthNumber
     * @param $yearNumber
     * @return array
     */
    public function getAnalyticsInvoiceCountHighChartsOptions($monthNumber, $yearNumber)
    {
        $categories = [];
        $data = [];
        $month = $this->getAnalyticsChartDate($monthNumber, $yearNumber);
        foreach ($month as $key => $dataMonth) {
            $categories[] = $dataMonth['monthText'] . ' ' . $dataMonth['year'];
            $data[] = (int)$this->getInvoices()
                ->andWhere(['type' => Documents::IO_TYPE_OUT])
                ->andWhere(['is_deleted' => false])
                ->andWhere([
                    'between',
                    Invoice::tableName() . '.document_date',
                    $dataMonth['year'] . '-' . $dataMonth['monthNumber'] . '-01',
                    $dataMonth['year'] . '-' . $dataMonth['monthNumber'] . '-' . cal_days_in_month(CAL_GREGORIAN,
                        $dataMonth['monthNumber'], $dataMonth['year'])
                ])->count();
        }
        $series = [
            0 => [
                'name' => 'Количество',
                'color' => '#45b6af',
                'data' => $data,
            ],
        ];

        return [
            'chart' => [
                'type' => 'column',
            ],
            'title' => [
                'text' => '',
            ],
            'lang' => [
                'printChart' => 'На печать',
                'downloadPNG' => 'Скачать PNG',
                'downloadJPEG' => 'Скачать JPEG',
                'downloadPDF' => 'Скачать PDF',
                'downloadSVG' => 'Скачать SVG',
                'contextButtonTitle' => 'Меню',
            ],
            'xAxis' => [
                'categories' => $categories,
            ],
            'yAxis' => [
                'min' => 0,
                'title' => [
                    'text' => 'Счета (шт)',
                    'useHTML' => true,
                ],
                'labels' => [
                    'enabled' => false,
                ],
            ],
            'series' => array_values($series),
            'legend' => [
                'enabled' => false,
            ],
            'plotOptions' => [
                'column' => [
                    'dataLabels' => [
                        'enabled' => false,
                    ],
                    'pointWidth' => 30,
                ],
            ],
        ];
    }

    /**
     * @param $monthNumber
     * @param $yearNumber
     * @return array
     */
    public function getAnalyticsInvoicePayedAmountHighChartsOptions($monthNumber, $yearNumber)
    {
        $categories = [];
        $data = [];
        $month = $this->getAnalyticsChartDate($monthNumber, $yearNumber);
        foreach ($month as $key => $dataMonth) {
            $categories[] = $dataMonth['monthText'] . ' ' . $dataMonth['year'];
            $data[] = (int)$this->getInvoices()
                    ->andWhere(['type' => Documents::IO_TYPE_OUT])
                    ->andWhere(['is_deleted' => false])
                    ->andWhere([
                        'in',
                        'invoice_status_id',
                        [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_PAYED_PARTIAL]
                    ])
                    ->andWhere([
                        'between',
                        Invoice::tableName() . '.document_date',
                        $dataMonth['year'] . '-' . $dataMonth['monthNumber'] . '-01',
                        $dataMonth['year'] . '-' . $dataMonth['monthNumber'] . '-' . cal_days_in_month(CAL_GREGORIAN,
                            $dataMonth['monthNumber'], $dataMonth['year'])
                    ])->sum('total_amount_with_nds - remaining_amount') / 100;
        }
        $series = [
            0 => [
                'name' => 'Сумма',
                'color' => '#45b6af',
                'data' => $data,
            ],
        ];

        return [
            'chart' => [
                'type' => 'column',
            ],
            'title' => [
                'text' => '',
            ],
            'lang' => [
                'printChart' => 'На печать',
                'downloadPNG' => 'Скачать PNG',
                'downloadJPEG' => 'Скачать JPEG',
                'downloadPDF' => 'Скачать PDF',
                'downloadSVG' => 'Скачать SVG',
                'contextButtonTitle' => 'Меню',
            ],
            'xAxis' => [
                'categories' => $categories,
            ],
            'yAxis' => [
                'min' => 0,
                'title' => [
                    'text' => 'Оплата <i class="fa fa-rub"></i>',
                    'useHTML' => true,
                ],
                'labels' => [
                    'enabled' => false,
                ],
            ],
            'series' => array_values($series),
            'legend' => [
                'enabled' => false,
            ],
            'plotOptions' => [
                'column' => [
                    'dataLabels' => [
                        'enabled' => false,
                    ],
                    'tooltip' => [
                        'valueDecimals' => 2,
                    ],
                    'pointWidth' => 30,
                ],
            ],
        ];
    }

    /**
     * @param $monthNumber
     * @param $yearNumber
     * @return array
     */
    public function getAnalyticsInvoiceDebtAmountHighChartsOptions($monthNumber, $yearNumber)
    {
        $categories = [];
        $data = [];
        $month = $this->getAnalyticsChartDate($monthNumber, $yearNumber);
        foreach ($month as $key => $dataMonth) {
            $categories[] = $dataMonth['monthText'] . ' ' . $dataMonth['year'];
            $data[] = (int)$this->getInvoices()
                    ->andWhere(['type' => Documents::IO_TYPE_OUT])
                    ->andWhere(['is_deleted' => false])
                    ->andWhere(['not', ['invoice_status_id' => InvoiceStatus::STATUS_PAYED]])
                    ->andWhere([
                        'between',
                        Invoice::tableName() . '.document_date',
                        $dataMonth['year'] . '-' . $dataMonth['monthNumber'] . '-01',
                        $dataMonth['year'] . '-' . $dataMonth['monthNumber'] . '-' . cal_days_in_month(CAL_GREGORIAN,
                            $dataMonth['monthNumber'], $dataMonth['year'])
                    ])->sum('remaining_amount') / 100;
        }
        $series = [
            0 => [
                'name' => 'Сумма',
                'color' => '#f3565d',
                'data' => $data,
            ],
        ];

        return [
            'chart' => [
                'type' => 'column',
            ],
            'title' => [
                'text' => '',
            ],
            'lang' => [
                'printChart' => 'На печать',
                'downloadPNG' => 'Скачать PNG',
                'downloadJPEG' => 'Скачать JPEG',
                'downloadPDF' => 'Скачать PDF',
                'downloadSVG' => 'Скачать SVG',
                'contextButtonTitle' => 'Меню',
            ],
            'xAxis' => [
                'categories' => $categories,
            ],
            'yAxis' => [
                'min' => 0,
                'title' => [
                    'text' => 'Долг <i class="fa fa-rub"></i>',
                    'useHTML' => true,
                ],
                'labels' => [
                    'enabled' => false,
                ],
            ],
            'series' => array_values($series),
            'legend' => [
                'enabled' => false,
            ],
            'plotOptions' => [
                'column' => [
                    'dataLabels' => [
                        'enabled' => false,
                    ],
                    'tooltip' => [
                        'valueDecimals' => 2,
                    ],
                    'pointWidth' => 30,
                ],
            ],
        ];
    }

    /**
     * @param @ioType
     * @param $monthNumber
     * @param $yearNumber
     * @return array
     */
    public function getProductHighChartsOptions($ioType, $monthNumber, $yearNumber, $productDataArray)
    {
        $result = [];
        $month = $this->getSellingChartDate($monthNumber, $yearNumber);
        $firstMonth = $month[11];
        $lastMonth = $month[0];

        $ioAmountColumn = Order::tableName() . '.' . ($ioType == Documents::IO_TYPE_OUT ? 'amount_sales_with_vat' : 'amount_purchase_with_vat');

        foreach ($productDataArray as $productData) {
            $categories = [];
            $data = [];
            $lines = [];
            foreach ($month as $key => $dataMonth) {
                $categories[] = $dataMonth['monthText'] . ' ' . $dataMonth['year'];
                $data[] = (int)$this->getInvoices()
                        ->select([
                            'SUM(' . $ioAmountColumn . ') as totalAmount',
                        ])
                        ->joinWith('orders')
                        ->andWhere([Order::tableName() . '.product_id' => $productData['product_id']])
                        ->andWhere(['type' => $ioType])
                        ->andWhere(['is_deleted' => false])
                        ->andWhere([
                            'between',
                            Invoice::tableName() . '.document_date',
                            $dataMonth['year'] . '-' . $dataMonth['monthNumber'] . '-01',
                            $dataMonth['year'] . '-' . $dataMonth['monthNumber'] . '-' . cal_days_in_month(CAL_GREGORIAN,
                                $dataMonth['monthNumber'], $dataMonth['year'])
                        ])->column()[0] / 100;
                $lines[] = (float)$this->getInvoices()
                    ->select([
                        'SUM(' . Order::tableName() . '.quantity) as totalQuantity',
                    ])
                    ->joinWith('orders')
                    ->andWhere([Order::tableName() . '.product_id' => $productData['product_id']])
                    ->andWhere(['type' => $ioType])
                    ->andWhere(['is_deleted' => false])
                    ->andWhere([
                        'between',
                        Invoice::tableName() . '.document_date',
                        $dataMonth['year'] . '-' . $dataMonth['monthNumber'] . '-01',
                        $dataMonth['year'] . '-' . $dataMonth['monthNumber'] . '-' . cal_days_in_month(CAL_GREGORIAN,
                            $dataMonth['monthNumber'], $dataMonth['year'])
                    ])->column()[0];
            }
            $series = [
                [
                    'type' => 'column',
                    'name' => 'Сумма',
                    'color' => '#45b6af',
                    'data' => $data,
                    'yAxis' => 1,
                ],
                [
                    'type' => 'spline',
                    'name' => 'Количество',
                    'color' => '#db4437',
                    'data' => $lines,
                ],
            ];
            $result[$productData['product_id']] = [
                'productTitle' => $productData['product_title'],
                'productUnitTitle' => $productData['productUnitTitle'],
                'chartInfo' => [
                    'chart' => [
                        'type' => 'column',
                        'events' => [
                            'load' => new JsExpression('function (event) {
                                    $this = $($(this)[0].container);
                                    $productUnitBlock = $this.closest(".high-chart").siblings(".product-unit-label-chart").clone();

                                    $productUnitBlock.show();
                                    $this.append($productUnitBlock);
                                }
                            '),
                        ],
                    ],
                    'title' => [
                        'text' => '',
                    ],
                    'lang' => [
                        'printChart' => 'На печать',
                        'downloadPNG' => 'Скачать PNG',
                        'downloadJPEG' => 'Скачать JPEG',
                        'downloadPDF' => 'Скачать PDF',
                        'downloadSVG' => 'Скачать SVG',
                        'contextButtonTitle' => 'Меню',
                    ],
                    'xAxis' => [
                        'categories' => $categories,
                    ],
                    'yAxis' => [
                        [
                            'min' => 0,
                            'title' => [
                                'text' => 'Сумма <i class="fa fa-rub"></i>',
                                'useHTML' => true,
                            ],
                            'labels' => [
                                'enabled' => false,
                            ],
                        ],
                        [
                            'min' => 0,
                            'title' => [
                                'text' => null
                            ],
                            'labels' => [
                                'enabled' => false,
                            ],
                            'opposite' => true,
                        ],
                    ],
                    'series' => array_values($series),
                    'legend' => [
                        'enabled' => false,
                    ],
                    'plotOptions' => [
                        'column' => [
                            'dataLabels' => [
                                'enabled' => false,
                            ],
                            'tooltip' => [
                                'valueDecimals' => 2,
                            ],
                            'pointWidth' => 30,
                        ],
                    ],
                ],
            ];
        }

        return $result;
    }

    /**
     * @param $ioType
     * @param $month
     * @param $year
     * @param $productID
     * @return mixed
     */
    public function getProductSumSellingData($ioType, $month, $year, $productID)
    {
        $ioAmountColumn = Order::tableName() . '.' . ($ioType == Documents::IO_TYPE_OUT ? 'amount_sales_with_vat' : 'amount_purchase_with_vat');

        return $this->getInvoices()
            ->select([
                'SUM(' . $ioAmountColumn . ') as totalAmount',
            ])
            ->joinWith('orders')
            ->andWhere([Order::tableName() . '.product_id' => $productID])
            ->andWhere(['type' => $ioType])
            ->andWhere(['is_deleted' => false])
            ->andWhere([
                'between',
                Invoice::tableName() . '.document_date',
                $year . '-' . $month . '-01',
                $year . '-' . $month . '-' . cal_days_in_month(CAL_GREGORIAN, $month, $year)
            ])->column()[0];
    }

    /**
     * @param $ioType
     * @param $month
     * @param $year
     * @return mixed
     */
    public function getSumSellingData($ioType, $month, $year)
    {
        $ioAmountColumn = Order::tableName() . '.' . ($ioType == Documents::IO_TYPE_OUT ? 'amount_sales_with_vat' : 'amount_purchase_with_vat');

        return $this->getInvoices()
            ->select([
                'SUM(' . $ioAmountColumn . ') as totalAmount',
            ])
            ->joinWith('orders')
            ->andWhere(['type' => $ioType])
            ->andWhere(['is_deleted' => false])
            ->andWhere([
                'between',
                Invoice::tableName() . '.document_date',
                $year . '-' . $month . '-01',
                $year . '-' . $month . '-' . cal_days_in_month(CAL_GREGORIAN, $month, $year)
            ])->column()[0];
    }

    /**
     * @param $month
     * @param $year
     * @return float|int
     */
    public function getInvoiceSumAnalyticsData($month, $year)
    {
        return $this->getInvoices()
            ->andWhere(['type' => Documents::IO_TYPE_OUT])
            ->andWhere(['is_deleted' => false])
            ->andWhere([
                'between',
                Invoice::tableName() . '.document_date',
                $year . '-' . $month . '-01',
                $year . '-' . $month . '-' . cal_days_in_month(CAL_GREGORIAN, $month, $year)
            ])->sum('total_amount_with_nds');
    }

    /**
     * @param $month
     * @param $year
     * @return float|int
     */
    public function getInvoiceCountAnalyticsData($month, $year)
    {
        return $this->getInvoices()
            ->andWhere(['type' => Documents::IO_TYPE_OUT])
            ->andWhere(['is_deleted' => false])
            ->andWhere([
                'between',
                Invoice::tableName() . '.document_date',
                $year . '-' . $month . '-01',
                $year . '-' . $month . '-' . cal_days_in_month(CAL_GREGORIAN, $month, $year)
            ])->count();
    }

    /**
     * @param $month
     * @param $year
     * @return mixed
     */
    public function getInvoicePayedSumAnalyticsData($month, $year)
    {
        return $this->getInvoices()
            ->andWhere(['type' => Documents::IO_TYPE_OUT])
            ->andWhere(['is_deleted' => false])
            ->andWhere(['in', 'invoice_status_id', [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_PAYED_PARTIAL]])
            ->andWhere([
                'between',
                Invoice::tableName() . '.document_date',
                $year . '-' . $month . '-01',
                $year . '-' . $month . '-' . cal_days_in_month(CAL_GREGORIAN, $month, $year)
            ])->sum('total_amount_with_nds - remaining_amount');
    }

    /**
     * @param $month
     * @param $year
     * @return mixed
     */
    public function getInvoiceDebtSumAnalyticsData($month, $year)
    {
        return $this->getInvoices()
            ->andWhere(['type' => Documents::IO_TYPE_OUT])
            ->andWhere(['is_deleted' => false])
            ->andWhere(['not', ['invoice_status_id' => InvoiceStatus::STATUS_PAYED]])
            ->andWhere([
                'between',
                Invoice::tableName() . '.document_date',
                $year . '-' . $month . '-01',
                $year . '-' . $month . '-' . cal_days_in_month(CAL_GREGORIAN, $month, $year)
            ])->sum('remaining_amount');
    }

    /**
     * @return bool|mixed
     * @throws \Throwable
     */
    public function createStoreAccount()
    {
        if ($storeCompany = $this->searchStoreCompany()) {
            return $this->addToStoreCompany($storeCompany);
        }

        $error = [];
        if (empty($this->director_email)) {
            $error[] = 'Необходимо заполнить Email руководителя';
        }
        if ($error) {
            Yii::$app->session->setFlash('error', implode('<br>', $error));

            return false;
        }

        $storeCompany = new StoreCompany;
        $storeCompany->company_type_id = $this->company_type_id;
        $storeCompany->name = $this->name;
        $storeCompany->inn = $this->ITN;
        $storeCompany->kpp = $this->PPC;
        $storeCompany->address = $this->legal_address;
        $storeCompany->status = StoreCompany::STATUS_ACTIVE;

        $name = explode(' ', $this->director_name);
        $password = substr(str_shuffle('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, 6);
        $storeUser = new StoreUser;
        $storeUser->lastname = isset($name[0]) ? $name[0] : '';
        $storeUser->firstname = isset($name[1]) ? $name[1] : '';
        $storeUser->patronymic = isset($name[2]) ? $name[2] : '';
        $storeUser->email = $this->director_email;
        $storeUser->phone = $this->director_phone;
        $storeUser->status = StoreUser::STATUS_ACTIVE;
        $storeUser->time_zone_id = TimeZone::DEFAULT_TIME_ZONE;
        $storeUser->setPassword($password);
        $storeUser->generateAuthKey();

        return Yii::$app->db->transaction(function ($db) use ($storeCompany, $storeUser, $password) {
            if ($storeCompany->save()) {
                $storeUser->store_company_id = $storeCompany->id;
                if ($storeUser->save()) {
                    try {
                        $storeCompany->link('contractors', $this, [
                            'status' => StoreCompanyContractor::STATUS_ACTIVE,
                            'created_at' => time(),
                        ]);
                        $storeCompany->link('storeUsers', $storeUser, [
                            'role_id' => StoreUserRole::CHIEF,
                            'status' => StoreUser::STATUS_ACTIVE,
                            'created_at' => time(),
                        ]);

                        if ($this->sendCreateStoreAccountEmail($storeUser, $password)) {
                            return true;
                        }
                    } catch (\Exception $e) {
                        $error = 'Во время создания кабинета произошла ошибка.';
                    }
                } else {
                    $error = $storeUser->firstErrors;
                    $error = reset($error);
                }
            } else {
                $error = $storeCompany->firstErrors;
                $error = reset($error);
            }

            if (Yii::$app->id == 'app-frontend' && !empty($error)) {
                Yii::$app->session->setFlash('error', $error);
            }
            var_dump($error);
            exit;
            $db->transaction->rollBack();

            return false;
        });
    }

    /**
     * @return StoreCompany|null
     */
    public function createStoreUserAccount(StoreCompany $storeCompany)
    {
        $name = explode(' ', $this->director_name);
        $password = substr(str_shuffle('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, 6);
        $storeUser = StoreUser::findOne(['email' => $this->director_email]) ?: new StoreUser;

        $storeUser->store_company_id = $storeCompany->id;
        $storeUser->contractor_id = $this->id;
        $storeUser->lastname = isset($name[0]) ? $name[0] : '';
        $storeUser->firstname = isset($name[1]) ? $name[1] : '';
        $storeUser->patronymic = isset($name[2]) ? $name[2] : '';
        $storeUser->email = $this->director_email;
        $storeUser->phone = $this->director_phone;
        $storeUser->status = StoreUser::STATUS_ACTIVE;
        $storeUser->time_zone_id = TimeZone::DEFAULT_TIME_ZONE;
        $storeUser->setPassword($password);
        $storeUser->generateAuthKey();

        return Yii::$app->db->transaction(function ($db) use ($storeCompany, $storeUser, $password) {
            if ($storeUser->save()) {
                try {
                    $storeCompany->link('storeUsers', $storeUser, [
                        'role_id' => StoreUserRole::CHIEF,
                        'status' => StoreUser::STATUS_ACTIVE,
                        'created_at' => time(),
                    ]);

                    if ($this->sendCreateStoreAccountEmail($storeUser, $password)) {
                        return true;
                    }
                } catch (\Exception $e) {
                    $error = 'Во время создания кабинета произошла ошибка.';
                }
            } else {
                $error = $storeUser->firstErrors;
                $error = reset($error);
            }

            if (Yii::$app->id == 'app-frontend' && !empty($error)) {
                Yii::$app->session->setFlash('error', $error);
            }

            $db->transaction->rollBack();

            return false;
        });
    }

    /**
     * @return StoreCompany|null
     */
    public function searchStoreCompany()
    {
        return StoreCompany::findOne([
            'inn' => $this->ITN,
            'kpp' => $this->PPC ?: '',
        ]);
    }

    /**
     * @return boolean
     */
    public function addToStoreCompany(StoreCompany $storeCompany)
    {
        try {
            $storeCompany->link('contractors', $this, [
                'status' => StoreCompanyContractor::STATUS_ACTIVE,
                'created_at' => time(),
            ]);

            return true;
        } catch (Exception $e) {
            $error = 'Во время добавления к кабинету покупателя произошла ошибка.';
        }
        if (Yii::$app->id == 'app-frontend' && !empty($error)) {
            Yii::$app->session->setFlash('error', $error);
        }

        return false;
    }

    /**
     * @return StoreCompany|null
     */
    public function sendCreateStoreAccountEmail(StoreUser $storeUser, $password)
    {
        $email = Yii::$app
            ->mailer
            ->compose([
                'html' => 'system/create-store-account/html',
                'text' => 'system/create-store-account/text',
            ], [
                'user' => $storeUser,
                'password' => $password,
                'subject' => 'Создание кабинета покупателя',
                'supportEmail' => Yii::$app->params['emailList']['support'],
            ])
            ->setFrom([Yii::$app->params['emailList']['support'] => Yii::$app->params['emailFromName']])
            ->setTo($this->director_email)
            ->setSubject('Создание кабинета покупателя');

        if ($email->send()) {
            Yii::$app->session->setFlash(
                'success',
                'Кабинет покупателя создан. <br>' .
                'Данные для входа отправлены на email руководителя.'
            );

            return true;
        } else {
            Yii::$app->session->setFlash('error', 'Не удалось отправить письмо.');
        }

        return false;
    }

    /**
     * @return StoreCompany|null
     */
    public function sendAddToStoreAccountEmail(StoreCompany $storeCompany)
    {
        $userLink = $storeCompany->getChiefUser();

        if ($userLink) {
            $email = Yii::$app->mailer
                ->compose([
                    'html' => 'system/add-to-store-account/html',
                    'text' => 'system/add-to-store-account/text',
                ], [
                    'user' => $userLink->storeUser,
                    'company' => $this->company,
                    'subject' => 'В ваш аккаунт на store.kub-24.ru добавлен продавец',
                    'supportEmail' => Yii::$app->params['emailList']['support'],
                ])
                ->setFrom([Yii::$app->params['emailList']['support'] => Yii::$app->params['emailFromName']])
                ->setTo($this->director_email)
                ->setSubject('В ваш аккаунт на store.kub-24.ru добавлен продавец');

            if ($email->send()) {
                if (Yii::$app->id == 'app-frontend') {
                    Yii::$app->session->setFlash('success', 'Письмо о добавлении к кабинету покупателя отправлено.');
                }

                return true;
            } else {
                Yii::$app->session->setFlash('error', 'Не удалось отправить письмо владелецу кабинета покупателя.');
            }
        } else {
            Yii::$app->session->setFlash('error', 'Владелец кабинета покупателя не найден.');
        }

        return false;
    }

    /**
     * @param null $address
     * @param bool $bankRequisites
     * @return string
     */
    public function getRequisitesFull($address = null, $bankRequisites = true)
    {
        if ($address) {
            $address = $address ? ", {$address}" : '';
        } else {
            $address = $this->legal_address ? ", {$this->legal_address}" : '';
        }
        $str = $this->getTitle(true);
        $str .= $this->ITN ? ", ИНН {$this->ITN}" : '';
        $str .= $this->PPC ? ", КПП {$this->PPC}" : '';
        $str .= $address;
        if ($bankRequisites) {
            $str .= $this->current_account ? ", р/с {$this->current_account}" : '';
            $str .= $this->bank_name ? ", в банке {$this->bank_name}" : '';
            $str .= $this->BIC ? ", БИК {$this->BIC}" : '';
            $str .= $this->corresp_account ? ", к/с {$this->corresp_account}" : '';
        }

        return $str;
    }

    /**
     * @param $value
     */
    public function setPhysical_fio($value)
    {
        $value = array_values(array_filter(explode(' ', trim($value))));

        $this->physical_lastname = ArrayHelper::getValue($value, 0);
        $this->physical_firstname = ArrayHelper::getValue($value, 1);
        $this->physical_patronymic = ArrayHelper::getValue($value, 2);
        $this->physical_no_patronymic = $value && !$this->physical_patronymic;
    }

    /**
     * @return string
     */
    public function getPhysical_fio()
    {
        $value = [
            $this->physical_lastname,
            $this->physical_firstname,
            $this->physical_patronymic,
        ];

        return implode(' ', array_filter($value));
    }

    /**
     * @return boolean
     */
    public function getOppositeType()
    {
        switch ($this->type) {
            case self::TYPE_CUSTOMER:
                return self::TYPE_SELLER;
            case self::TYPE_SELLER:
                return self::TYPE_CUSTOMER;
        }

        return null;
    }

    /**
     * @return boolean
     */
    public function getCanHasOpposite()
    {
        return $this->company_id && in_array($this->type, [self::TYPE_CUSTOMER, self::TYPE_SELLER]);
    }

    /**
     * @return ContractorQuery
     */
    public function getOppositeQuery()
    {
        return $this->getDuplicateQuery();
    }

    /**
     * @return ContractorQuery
     */
    public function getDuplicateQuery()
    {
        $query = Contractor::find()->andWhere([
            'company_id' => $this->company_id,
            //'company_type_id' => $this->company_type_id,
            'face_type' => $this->face_type,
            'is_deleted' => false,
        ])->andFilterWhere([
            'not',
            ['id' => $this->id],
        ]);

        if ($this->face_type == Contractor::TYPE_LEGAL_PERSON) {
            if (!empty($this->ITN)) {
                $query->andWhere([
                    'ITN' => $this->ITN,
                ]);
            } else {
                $query->andWhere([
                    'name' => $this->name,
                ]);
            }
            /*if (!empty($this->PPC)) {
                $query->andWhere([
                    'PPC' => $this->PPC,
                ]);
            } else {
                $query->andWhere([
                    'or',
                    ['PPC' => ''],
                    ['PPC' => '0'],
                    ['PPC' => null],
                ]);
            }*/
        } else {
            $query->andWhere([
                'name' => $this->name,
            ]);
        }

        return $query;
    }

    /**
     * @return boolean
     */
    public function getHasOpposite()
    {
        return $this->getIsSellerCustomer();
    }

    /**
     * @return boolean
     */
    public function getIsForeign()
    {
        return $this->face_type == Contractor::TYPE_FOREIGN_LEGAL_PERSON;
    }

    /**
     * @return Contractor|null|\yii\db\ActiveRecord
     */
    public function getFirstDuplicate()
    {
        $contractor = Contractor::find()
            ->byCompany($this->company_id)
            ->byIsDeleted(false)
            ->byContractor($this->type)
            ->andHaving('created_at = MIN(created_at)');

        if ($this->face_type == self::TYPE_PHYSICAL_PERSON) {
            $contractor->andWhere([
                'and',
                ['physical_firstname' => $this->physical_firstname],
                ['physical_lastname' => $this->physical_lastname],
                ['physical_patronymic' => $this->physical_patronymic],
            ])->andWhere(['face_type' => self::TYPE_PHYSICAL_PERSON]);
        } else {
            $contractor->andWhere([
                'and',
                ['ITN' => $this->ITN],
            ]);
        }

        return $contractor->one();
    }

    /**
     * @return string
     */
    public function getDuplicateDifference()
    {
        $contractor = $this->getFirstDuplicate();
        if (!$contractor || $contractor->id == $this->id) {
            return '';
        }
        if ($this->face_type == self::TYPE_PHYSICAL_PERSON) {
            $attributes = [
                'physical_lastname' => 'ФИО',
                'physical_firstname' => 'ФИО',
                'physical_patronymic' => 'ФИО',
            ];
        } else {
            $attributes = [
                'PPC' => 'КПП',
                'legal_address' => 'Юр. адресу',
                'current_account' => 'Р/с',
                'BIC' => 'БИК',
                'director_post_name' => 'Должность',
                'director_name' => 'ФИО',
            ];
        }

        $difference = [];
        $message = 'Нет отличий';
        foreach ($attributes as $attribute => $label) {
            $firstContractorValue = mb_strtolower($contractor->$attribute);
            $thisContractorValue = mb_strtolower($this->$attribute);
            if ($this->face_type == self::TYPE_PHYSICAL_PERSON) {
                $firstContractorValue = $contractor->$attribute;
                $thisContractorValue = $this->$attribute;
            }
            if ($firstContractorValue != $thisContractorValue) {
                $difference[] = $label;
            }
        }
        if (!empty($difference)) {
            $difference = array_unique($difference);
            $message = 'Отличается по ' . implode(', ', $difference);
        }

        return $message;
    }

    /** AGENT */

    public function loadAgent($post)
    {
        if (!$this->is_agent || !isset($post['agentBuyer']) || !is_array($post['agentBuyer'])) {
            return true;
        }

        foreach ($post['agentBuyer'] as $value) {
            if ($value['contractor_id']) {
                $this->_hasAgentBuyers[] = [
                    'contractor_id' => ArrayHelper::getValue($value, 'contractor_id'),
                    'start_date' => ArrayHelper::getValue($value, 'start_date')
                ];
            }
        }

        return true;
    }

    public function validateAgent()
    {
        if (!$this->is_agent) {
            return true;
        }

        foreach ((array)$this->_hasAgentBuyers as $key => $value) {

            if (!($buyer = Contractor::find()->where([
                'company_id' => $this->company_id,
                'id' => $value['contractor_id']
            ])->one())) {
                unset($this->_hasAgentBuyers[$key]);
                continue;
            }
            $date_parts = explode('.', $value['start_date']);

            if (!$value['start_date'] || count($date_parts) != 3 || !checkdate((int)$date_parts[1], (int)$date_parts[0],
                    (int)$date_parts[2])) {
                $this->addError('hasAgentBuyers',
                    'Дата заполнена некорретно для покупателя: ' . $buyer->getShortName());
                return false;
            }
        }

        if (!count($this->_hasAgentBuyers)) {
            $this->addError('hasAgentBuyers', 'Необходимо заполнить покупателей агента.');
            return false;
        }

        return true;
    }

    public function saveAgent()
    {
        if ($this->updateAgentBuyersLinks()) {

            if (!is_array($this->_hasAgentBuyers)) {
                return true;
            }

            $i = 0;
            foreach ($this->_hasAgentBuyers as $value) {
                if ($agentBuyer = ContractorAgentBuyer::findOne([
                    'agent_id' => $this->id,
                    'buyer_id' => $value['contractor_id']
                ])) {
                    $agentBuyer->start_date = DateHelper::format($value['start_date'], DateHelper::FORMAT_DATE,
                        DateHelper::FORMAT_USER_DATE);
                    $agentBuyer->created_at = time();
                    $agentBuyer->ord = ++$i;
                    $agentBuyer->updateAttributes(['start_date', 'created_at', 'ord']);
                }
            }

            return true;
        }

        return false;
    }

    public function getHasAgentBuyers()
    {
        return $this->_hasAgentBuyers;
    }

    public function setHasAgentBuyers($value)
    {
        $this->_hasAgentBuyers = $value;
    }

    public function updateAgentBuyersLinks()
    {
        $this->unlinkAll('agentBuyers', true);

        if (!is_array($this->_hasAgentBuyers)) {
            return true;
        }

        foreach ($this->_hasAgentBuyers as $contractor_id) {
            if ($contractor = Contractor::findOne(['company_id' => $this->company_id, 'id' => $contractor_id])) {
                $this->link('agentBuyers', $contractor);
            }
        }

        return true;
    }

    public function validateBuyer($attribute, $params, $validator)
    {
        if (!$this->$attribute || !is_array($this->$attribute)) {
            $this->addError($attribute, 'Некорректно заполнен "Покупатель агента"');
        }
    }

    public function getEmailSubject()
    {
        return '';
    }

    /**
     * Устанавливает и сохраняет признак "проверен"
     * @param bool $value
     */
    public function saveVerified(bool $value = true)
    {
        if ($value == $this->verified) {
            return;
        }
        $companyId = $this->company_id;
        $this->company_id = 1;
        $this->verified=$value;
        $this->save(false, ['verified']);
        $this->company_id = $companyId;
    }

    /**
     * @return bool
     */
    public function getIsForeignCurrency()
    {
        return $this->face_type == self::TYPE_FOREIGN_LEGAL_PERSON && $this->order_currency != Currency::DEFAULT_ID;
    }

    /**
     * Слияние документов и оборотов контрагента 2 с документами и оборотами контрагента 1, контрагент 2 при этом помечается, как удаленный
     *
     * @param  Contractor $contractor1
     * @param  Contractor $contractor2
     * @param  boolean    $bothTypes
     * @return [type]
     */
    public static function merge(Contractor $contractor1, Contractor $contractor2, $bothTypes = false)
    {
        $bothTypes = (bool) $bothTypes;
        if ($contractor2->id == $contractor1->id ||
            (empty($contractor1->company_id) && empty($contractor2->company_id)) ||
            (!empty($contractor1->company_id) && !empty($contractor2->company_id) && $contractor2->company_id != $contractor1->company_id)
        ) {

            return false;
        }

        return Yii::$app->db->transaction(function ($db) use ($contractor1, $contractor2, $bothTypes) {
            $updateAttributes = [];
            if (!$contractor1->is_seller && ($bothTypes || $contractor2->is_seller)) {
                $updateAttributes['is_seller'] = 1;
            }
            if (!$contractor1->is_customer && ($bothTypes || $contractor2->is_customer)) {
                $updateAttributes['is_customer'] = 1;
            }
            if ($contractor2->is_founder && !$contractor1->is_founder) {
                $updateAttributes['is_founder'] = 1;
            }

            $tableArray = [
                '{{%agreement}}',
                '{{%autoinvoice}}',
                '{{%invoice}}',
                '{{%act}}',
                '{{%packing_list}}',
                '{{%upd}}',
                '{{%foreign_currency_invoice}}',
                '{{%order_document}}',
                '{{%olap_documents}}',
                '{{%olap_flows}}',
                '{{%olap_invoices}}',
                '{{%product_turnover}}',
                '{{%cash_bank_flows}}',
                '{{%cash_bank_foreign_currency_flows}}',
                '{{%cash_emoney_flows}}',
                '{{%cash_emoney_foreign_currency_flows}}',
                '{{%cash_order_flows}}',
                '{{%cash_order_foreign_currency_flows}}',
            ];

            try {
                foreach ($tableArray as $table) {
                    $db->createCommand()->update($table, [
                        'contractor_id' => $contractor1->id,
                    ], [
                        'company_id' => $contractor2->company_id,
                        'contractor_id' => (string) $contractor2->id,
                    ])->execute();
                }
                if ($updateAttributes) {
                    $contractor1->updateAttributes($updateAttributes);
                }
                $contractor2->updateAttributes(['is_deleted' => 1]);

                return true;
            } catch (\Exception $e) {
                $error = $e->__toString();
            } catch (\Throwable $e) {
                $error = $e->__toString();
            }

            if ($db->transaction->isActive) {
                $db->transaction->rollBack();
            }
            $text = date('Y-m-d H:i:s')." ".$error."\n\n";
            $file = Yii::getAlias('@runtime/logs/contractor_merge_error.log');
            file_put_contents($file, $text, FILE_APPEND);

            return false;
        });
    }

    /**
     * @param null $contractorType
     * @param null $dateRange
     * @return false|int|string|null
     */
    public function getPrepaidAmount($contractorType = null)
    {
        if ($contractorType === null)
            $contractorType = $this->type;
        if ($contractorType == self::TYPE_SELLER) {
            $flowType = CashFlowsBase::FLOW_TYPE_EXPENSE;
        }
        elseif ($contractorType == self::TYPE_CUSTOMER) {
            $flowType = CashFlowsBase::FLOW_TYPE_INCOME;
        } else {
            return 0;
        }

        $query1 = (new \yii\db\Query)
            ->select([
                'a.id',
                'a.contractor_id',
                'a.amount AS flowsAmount',
                'IFNULL(SUM(b.amount), 0) AS invoicesAmount',
            ])
            ->from(['a' => CashBankFlows::tableName()])
            ->leftJoin(['b' => CashBankFlowToInvoice::tableName()], 'a.id = b.flow_id')
            ->where(['a.company_id' => $this->company_id, 'a.contractor_id' => $this->id, 'a.flow_type' => $flowType])
            ->groupBy('a.id');

        $query2 = (new \yii\db\Query)
            ->select([
                'a.id',
                'a.contractor_id',
                'a.amount AS flowsAmount',
                'IFNULL(SUM(b.amount), 0) AS invoicesAmount',
            ])
            ->from(['a' => CashOrderFlows::tableName()])
            ->leftJoin(['b' => CashOrderFlowToInvoice::tableName()], 'a.id = b.flow_id')
            ->where(['a.company_id' => $this->company_id, 'a.contractor_id' => $this->id, 'a.flow_type' => $flowType])
            ->groupBy('a.id');

        $query3 = (new \yii\db\Query)
            ->select([
                'a.id',
                'a.contractor_id',
                'a.amount AS flowsAmount',
                'IFNULL(SUM(b.amount), 0) AS invoicesAmount',
            ])
            ->from(['a' => CashEmoneyFlows::tableName()])
            ->leftJoin(['b' => CashEmoneyFlowToInvoice::tableName()], 'a.id = b.flow_id')
            ->where(['a.company_id' => $this->company_id, 'a.contractor_id' => $this->id, 'a.flow_type' => $flowType])
            ->groupBy('a.id');

        return (new \yii\db\Query)
            ->from(['t' => $query1->union($query2, true)->union($query3, true)])
            ->select(new \yii\db\Expression('SUM(flowsAmount) - SUM(invoicesAmount)'))
            ->groupBy('contractor_id')
            ->scalar();
    }

    /**
     * @return ActiveQuery
     */
    public function getClient(): ActiveQuery
    {
        return $this->hasOne(Client::class, ['contractor_id' => 'id']);
    }

    /**
     * @param Company $company
     * @param $id
     * @return static|null
     */
    public static function findById(Company $company, $id): ?ActiveRecord
    {
        return self::find()
            ->andWhere(['company_id' => $company->id, 'id' => $id])
            ->one();
    }

    /**
     * @return ActiveQuery
     */
    public function getContacts(): ActiveQuery
    {
        return $this->hasMany(Contact::class, ['contractor_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getDefaultContacts(): ActiveQuery
    {
        return $this->hasMany(Contact::class, ['contractor_id' => 'id'])->andOnCondition(['contact_default' => true]);
    }

    /**
     * @return ActiveQuery
     */
    public function getDefaultContact(): ActiveQuery
    {
        return $this->hasOne(Contact::class, ['contractor_id' => 'id'])->andOnCondition(['contact_default' => true]);
    }

    /**
     * @return Contact
     */
    public function getActualContact(): Contact
    {
        foreach ($this->contacts as $contact) {
            if ($contact->contact_default) {
                return $contact;
            }
        }

        return Contact::createDefaultContact($this);
    }

    private $allContactsData;
    public function getAllContactsData(): array
    {
        if (!isset($this->allContactsData)) {
            $this->allContactsData = [];
            if ($this->director_name) {
                $this->allContactsData[-1] = [
                    'id' => -1,
                    'name' => $this->director_name,
                    'phone' => $this->director_phone,
                    'email' => $this->director_email,
                ];
            }
            if (!$this->chief_accountant_is_director && $this->chief_accountant_name) {
                $this->allContactsData[-2] = [
                    'id' => -2,
                    'name' => $this->chief_accountant_name,
                    'phone' => $this->chief_accountant_phone,
                    'email' => $this->chief_accountant_email,
                ];
            }
            if (!$this->contact_is_director && $this->contact_name) {
                $this->allContactsData[-3] = [
                    'id' => -3,
                    'name' => $this->contact_name,
                    'phone' => $this->contact_phone,
                    'email' => $this->contact_email,
                ];
            }
            foreach ($this->contacts as $contact) {
                $this->allContactsData[$contact->contact_id] = [
                    'id' => $contact->contact_id,
                    'name' => $contact->contact_name,
                    'phone' => $contact->primary_phone_number ?: $contact->secondary_phone_number,
                    'email' => $contact->email_address,
                ];
            }
        }

        return $this->allContactsData;
    }

    /**
     * @return ActiveQuery
     */
    public function getActivity(): ActiveQuery
    {
        return $this->hasOne(ContractorActivity::class, ['id' => 'activity_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCampaign(): ActiveQuery
    {
        return $this->hasOne(ContractorCampaign::class, ['id' => 'campaign_id']);
    }

    /**
     * @return bool
     */
    public function isKub()
    {
        return \Yii::$app->params['service']['contractor_id'] === $this->id;
    }

    /**
     * @param int $companyId
     * @return ServiceContractorItem
     */
    public function getKubItem($companyId = null)
    {
        if (!$companyId)
            $companyId = Yii::$app->user->identity->company_id;

        $kubItem = ServiceContractorItem::findOne(['company_id' => $companyId]);

        if (!$kubItem) {
            $kubItem = new ServiceContractorItem(['company_id' => $companyId]);
            $kubItem->save();
        }

        return $kubItem;
    }

    /**
     * @param null $type
     * @return ActiveQuery
     */
    public function getIndustry($type = null)
    {
        $type = $type ?? $this->type;
        if ($type == self::TYPE_CUSTOMER)
            return $this->hasOne(CompanyIndustry::class, ['id' => 'customer_industry_id']);
        else
            return $this->hasOne(CompanyIndustry::class, ['id' => 'seller_industry_id']);
    }

    /**
     * @param null $type
     * @return ActiveQuery
     */
    public function getSalePoint($type = null)
    {
        $type = $type ?? $this->type;
        if ($type == self::TYPE_CUSTOMER)
            return $this->hasOne(SalePoint::class, ['id' => 'customer_sale_point_id']);
        else
            return $this->hasOne(SalePoint::class, ['id' => 'seller_sale_point_id']);
    }

    public static function title($data, $short = true)
    {
        $faceType = ArrayHelper::getValue($data, 'face_type');

        $pieces = [
            $faceType == self::TYPE_FOREIGN_LEGAL_PERSON ?
                ArrayHelper::getValue($data, 'foreign_legal_form') :
                CompanyType::getValue(ArrayHelper::getValue($data, 'company_type_id'), $short ? 'name_short' : 'name_full', ''),
            ArrayHelper::getValue($data, 'name'),
        ];
        $glue = ' ';
        if ($faceType == self::TYPE_FOREIGN_LEGAL_PERSON) {
            $pieces = array_reverse($pieces);
            $glue = ', ';
        }

        return implode($glue, array_filter($pieces));
    }


    public function getGroupName($type = null){
            switch ($type ?? $this->type){
                case Contractor::TYPE_CUSTOMER:
                    return $this->group_name =   ContractorGroup::find()
                            ->select("title")
                            ->where(["id"=>$this->customer_group_id])
                            ->asArray()->one()['title'];
                    break;

                case Contractor::TYPE_SELLER:
                    return $this->group_name = ContractorGroup::find()
                        ->select("title")
                        ->where(["id"=>$this->seller_group_id])
                        ->asArray()->one()['title'];
                    break;

                default:
                    return null;
                    break;
            }
    }

}
