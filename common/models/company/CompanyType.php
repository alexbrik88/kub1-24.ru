<?php

namespace common\models\company;

use common\components\helpers\ArrayHelper;
use common\models\Company;
use common\models\Contractor;
use Yii;

/**
 * This is the model class for table "company_type".
 *
 * @property integer $id
 * @property string $name_short
 * @property string $name_full
 * @property string $like_ip
 *
 * @property string $name emulates `name` field like in most dictionary-like models.
 */
class CompanyType extends \yii\db\ActiveRecord
{
    const TYPE_EMPTY = 0;
    /**
     * ИП
     */
    const TYPE_IP = 1;
    /**
     * ООО
     */
    const TYPE_OOO = 2;
    /**
     * ЗАО
     */
    const TYPE_ZAO = 3;
    /**
     * ПАО
     */
    const TYPE_PAO = 4;
    /**
     * АО
     */
    const TYPE_AO = 5;
    /**
     * АНО
     */
    const TYPE_ANO = 6;
    /**
     * ФГУП
     */
    const TYPE_FGUP = 7;
    /**
     * ОАО
     */
    const TYPE_OAO = 8;
    /**
     * ННО
     */
    const TYPE_NNO = 9;
    /**
     * ФГКУ
     */
    const TYPE_FGKY = 10;
    /**
     * НО
     */
    const TYPE_NO = 11;
    /**
     * УФК
     */
    const TYPE_UFK = 12;

    protected static $_ipIds = false;
    protected static $_items;

    /**
     * @return CompanyTypeQuery
     * @return mixed
     */
    public static function find()
    {
        return new CompanyTypeQuery(get_called_class());
    }

    /**
     * @return mixed
     */
    public static function getValue($id, $paramName, $defaultValue = null)
    {
        if (!isset(self::$_items)) {
            self::$_items = self::find()->asArray()->indexBy('id')->all();
        }

        return ArrayHelper::getValue(self::$_items, [$id, $paramName], $defaultValue);
    }

    /**
     * Returns allowed company types for COMPANY (not for contractor).
     * @param $companyType
     * @return array
     */
    public static function getCompanyTypeVariants($companyType)
    {
        $typeArray = [];
        if ($companyType == self::TYPE_IP) {
            $typeArray[] = self::TYPE_IP;
        } else {
            $typeArray = CompanyType::find()->andWhere(['in_company' => true])->andWhere(['not', ['id' => CompanyType::TYPE_IP]])->column();
            //$typeArray = [
            //    self::TYPE_OOO,
            //    self::TYPE_ZAO,
            //    self::TYPE_PAO,
            //    self::TYPE_OAO,
            //];
        }

        return array_intersect_key(ArrayHelper::getAssoc(static::find()->all(), 'id'), array_fill_keys($typeArray, null));
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_type';
    }

    /**
     * @return array
     */
    public static function likeIpId()
    {
        if (self::$_ipIds === false) {
            self::$_ipIds = self::find()->select('id')->where(['like_ip' => true])->column();
        }

        return self::$_ipIds;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name_short', 'name_full'], 'required'],
            [['name_full'], 'unique'],
            [['name_short'], 'string', 'max' => 45],
            [['name_full'], 'string', 'max' => 250],
            [['in_company', 'in_contractor', 'like_ip'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => '№',
            'name_short' => 'Аббревиатура',
            'name_full' => 'Название',
            'in_company' => 'Компании',
            'in_contractor' => 'Контрагенты',
            'like_ip' => 'Как ИП',
        ];
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name_short;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Company::className(), ['company_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContractors()
    {
        return $this->hasMany(Contractor::className(), ['company_type_id' => 'id']);
    }
}
