<?php

namespace common\models\company;

use common\models\Company;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "inn_deny".
 *
 * @property int $id
 * @property string $inn
 * @property int $created_at
 */
class InnDeny extends \yii\db\ActiveRecord
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'inn_deny';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['inn'], 'trim'],
            [['inn'], 'required'],
            [['inn'], 'string', 'max' => 255],
            [['inn'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'inn' => 'ИНН',
            'created_at' => 'Добавлен',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        Yii::$app->db->createCommand()->update(Company::tableName(), [
            'blocked' => 1,
            'blocked_by_inn' => 1,
        ], [
            'blocked' => 0,
            'inn' => $this->inn,
        ])->execute();
    }

    /**
     * {@inheritdoc}
     */
    public function afterDelete()
    {
        parent::afterFind();

        Yii::$app->db->createCommand()->update(Company::tableName(), [
            'blocked' => 0,
            'blocked_by_inn' => 0,
        ], [
            'blocked' => 1,
            'blocked_by_inn' => 1,
            'inn' => $this->inn,
        ])->execute();
    }
}
