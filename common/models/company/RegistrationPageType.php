<?php

namespace common\models\company;

use Yii;

/**
 * This is the model class for table "registration_page_type".
 *
 * @property integer $id
 * @property string $name
 */
class RegistrationPageType extends \yii\db\ActiveRecord
{
    const PAGE_TYPE_INVOICE = 1;
    const PAGE_TYPE_INVOICE_FACTURE = 2;
    const PAGE_TYPE_ACT = 3;
    const PAGE_TYPE_TTN = 4;
    const PAGE_TYPE_PACKING_LIST = 5;
    const PAGE_TYPE_UPD = 6;
    const PAGE_TYPE_INVOICE_CONTRACT = 7;
    const PAGE_TYPE_PRICE_LIST = 8;
    const PAGE_TYPE_BANK_OTKRITIE = 9;
    const PAGE_TYPE_BANK_ROBOT = 10;
    const PAGE_TYPE_B2B_MODULE = 11;
    const PAGE_TYPE_ANDROID = 12;
    const PAGE_TYPE_LANDING = 13;
    const PAGE_TYPE_PAYMENT = 14;
    const PAGE_TYPE_DECLARATION_TEMPLATE_IP = 15;
    const PAGE_TYPE_AGREEMENT_SERVICE = 16;
    const PAGE_TYPE_NULL_DECLARATION_TEMPLATE_IP = 17;
    const PAGE_TYPE_TAX_COM_OFD = 18;
    const PAGE_TYPE_BANK_OTKRITIE_TAXROBOT = 19;
    const PAGE_TYPE_FINANCE = 20;
    const PAGE_TYPE_NULL_PATTERNS = 21;
    const PAGE_TYPE_NULL_LANDING = 22;
    const PAGE_TYPE_MTS = 23;
    const PAGE_TYPE_MTS_INVOICE = 23;
    const PAGE_TYPE_MTS_BUH_IP6 = 24;
    const PAGE_TYPE_MTS_B2B = 25;
    const PAGE_TYPE_BANK_SBERBANK = 26;
    const PAGE_TYPE_SBERBANK_TAXROBOT = 27;
    const PAGE_TYPE_PAYMENT_REMINDER = 28;
    const PAGE_TYPE_EVOTOR = 29;
    const PAGE_TYPE_ANALYTICS = 30;
    const PAGE_TYPE_PRICE_LIST_LANDING = 31;
    const PAGE_TYPE_FOREIGN_INVOICE_LANDING = 32;
    const PAGE_TYPE_ANALYTICS_PLAN_FACT = 33;
    const PAGE_TYPE_ANALYTICS_PAYMENT_CALENDAR = 34;
    const PAGE_TYPE_ANALYTICS_PROFIT_AND_LOSS = 35;
    const PAGE_TYPE_ANALYTICS_BALANCE = 36;
    const PAGE_TYPE_ANALYTICS_FINANCE_MODEL = 37;
    const PAGE_TYPE_ANALYTICS_BREAKEVEN = 38;
    const PAGE_TYPE_BLOG = 39;
    const PAGE_TYPE_LANDING_PRODUCTS = 40;
    // 41 - AmoCRM!
    const PAGE_TYPE_CRM = 42;
    const PAGE_TYPE_LANDING_FINANCE_MODEL = 43;
    const PAGE_TYPE_ANALYTICS_FINANCE_ANALYSIS = 44;
    const PAGE_TYPE_ANALYTICS_INCOME_EXPENSE_ANALYSIS = 45;
    const PAGE_TYPE_ANALYTICS_MANAGEMENT_AND_FINANCES = 46;
    const PAGE_TYPE_ANALYTICS_FINANCE_CONTROL = 47;
    const PAGE_TYPE_PAYMENT_APPROVAL = 48;

    const PAGE_TYPE_LANDING_SBER_UNITY = 49;
    const PAGE_TYPE_ANALYTICS_SBER_UNITY = 50;
    const PAGE_TYPE_BANK_OTKRITIE_ANALYTICS = 51;

    const PAGE_TYPE_ALFABANK = 52;
    const PAGE_TYPE_ANALYTICS_DASHBOARD = 53;

    public static $analyticsPages = [
        self::PAGE_TYPE_ANALYTICS,
        self::PAGE_TYPE_ANALYTICS_PLAN_FACT,
        self::PAGE_TYPE_ANALYTICS_PAYMENT_CALENDAR,
        self::PAGE_TYPE_ANALYTICS_PROFIT_AND_LOSS,
        self::PAGE_TYPE_ANALYTICS_BALANCE,
        self::PAGE_TYPE_ANALYTICS_FINANCE_MODEL,
        self::PAGE_TYPE_ANALYTICS_BREAKEVEN,
        self::PAGE_TYPE_ANALYTICS_DASHBOARD,
        self::PAGE_TYPE_LANDING_PRODUCTS,
        self::PAGE_TYPE_LANDING_FINANCE_MODEL,
        self::PAGE_TYPE_BANK_OTKRITIE_ANALYTICS,
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'registration_page_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
}
