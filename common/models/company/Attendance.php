<?php

namespace common\models\company;

use Yii;
use common\models\Company;

/**
 * This is the model class for table "attendance".
 *
 * @property integer $id
 * @property integer $company_id
 * @property string $date
 * @property integer $activity_status
 * @property boolean $is_first_visit
 *
 * @property Company $company
 */
class Attendance extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'attendance';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'date'], 'required'],
            [['company_id', 'activity_status'], 'integer'],
            [['date'], 'safe'],
            [['is_first_visit'], 'boolean'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Пользователь',
            'date' => 'Дата',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
}
