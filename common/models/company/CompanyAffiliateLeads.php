<?php

namespace common\models\company;

use common\components\helpers\ArrayHelper;
use common\components\helpers\ModelHelper;
use common\models\Company;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Class CompanyAffiliateLeads
 * @package common\models\company
 *
 * @property integer $company_id
 * @property string $name
 * @property string $username
 * @property string $email
 * @property string $phone
 * @property string $comment
 * @property integer $date
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Company $company
 */

class CompanyAffiliateLeads extends ActiveRecord
{
    public $date;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{company_affiliate_leads}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'username', 'email'], 'required'],
            [['name', 'username', 'email', 'phone', 'comment'], 'trim'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => 'Название',
            'company_id' => 'Компания',
            'product_id' => 'Продукт',
            'username' => 'ФИО',
            'email' => 'E-mail',
            'phone' => 'Телефон',
            'comment' => 'Комментарий',
            'date' => 'Дата',
            'created_at' => 'Дата создания',
            'updated_at' => 'Последнее изменение',
        ];
    }

    /**
     * @inheritDoc
     */
    public function beforeSave($insert)
    {
        ModelHelper::HtmlEntitiesModelAttributes($this);

        return parent::beforeSave($insert);
    }

    /**
     * @return ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

}