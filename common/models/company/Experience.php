<?php

namespace common\models\company;

use Yii;

/**
 * This is the model class for table "company_experience".
 *
 * @property integer $id
 * @property string $name
 */
class Experience extends \yii\db\ActiveRecord
{
    const EXP_NOVICE = 1;
    const EXP_CURRENT = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_experience';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
        ];
    }
}
