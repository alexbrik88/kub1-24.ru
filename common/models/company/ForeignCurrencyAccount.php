<?php

namespace common\models\company;

use Yii;
use common\components\TextHelper;
use common\components\cash\InternalTransferAccountInterface;
use common\components\cash\InternalTransferFlowInterface;
use common\models\Company;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashBankForeignCurrencyFlows;
use common\models\currency\Currency;
use common\models\document\Invoice;
use common\models\document\InvoiceIncomeItem;
use frontend\modules\cash\models\CashContractorType;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "foreign_currency_account".
 *
 * @property integer $id
 * @property integer $company_id
 * @property string $currency_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $type
 * @property string $rs
 * @property string $iban
 * @property string $swift
 * @property string $bank_name
 * @property string $bank_address
 * @property string $corr_rs
 * @property string $corr_swift
 * @property string $corr_bank_name
 * @property string $corr_bank_address
 * @property string $is_foreign_bank
 */
class ForeignCurrencyAccount extends ActiveRecord implements InternalTransferAccountInterface
{
    /**
     *  Основной
     */
    const TYPE_MAIN = 0;

    /**
     *  Вспомогательный
     */
    const TYPE_ADDITIONAL = 1;

    /**
     *  Закрытый
     */
    const TYPE_CLOSED = 2;

    /**
     * @var array
     */
    public $typeAccount = [
        self::TYPE_MAIN => 'Основной',
        self::TYPE_ADDITIONAL => 'Дополнительный',
        self::TYPE_CLOSED => 'Закрытый',
    ];

    public $isMain;
    public $isClosed;
    public $createStartBalance = false;
    public $startBalanceAmount;
    public $startBalanceDate;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'foreign_currency_account';
    }

    public function getCashContractorTypeText() : string
    {
        return CashContractorType::BANK_CURRENCY_TEXT;
    }

    public function getInternalTransferId() : string
    {
        return self::tableName() . $this->id;
    }

    public function getInternalTransferName() : ?string
    {
        return $this->bank_name;
    }

    public function getCurrencyId() : ?string
    {
        return $this->currency_id ?? null;
    }

    public function getCurrencyName() : ?string
    {
        return Currency::name($this->currency_id);
    }

    public function getCashFlowClass() : string
    {
        return CashBankForeignCurrencyFlows::class;
    }

    public function getCashFlowTable() : string
    {
        return CashBankForeignCurrencyFlows::tableName();
    }

    public function getBalance() : int
    {
        if ($this->rs) {
            return (new \yii\db\Query())
                ->from(CashBankForeignCurrencyFlows::tableName())
                ->where(['company_id' => $this->company_id])
                ->andWhere(['rs' => $this->rs])
                ->sum(new \yii\db\Expression('CASE WHEN flow_type = 1 THEN amount ELSE -amount END')) ?: 0;
        }

        return 0;
    }

    public function getCashFlowNewModel() : InternalTransferFlowInterface
    {
        $class = $this->getCashFlowClass();
        $model = new $class;
        $model->company_id = $this->company_id;
        $model->currency_id = $this->currency_id;
        $model->currency_name = Currency::name($this->currency_id);
        $model->rs = $this->rs;
        $model->swift = $this->swift;
        $model->bank_name = $this->bank_name;
        $model->bank_address = $this->bank_address;
        $model->has_invoice = 0;

        $model->populateRelation('selfAccount', $this);

        return $model;
    }

    /**
     * @return CheckingAccountantQuery
     */
    public static function find()
    {
        return new CheckingAccountantQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'rs',
                    'iban',
                    'swift',
                    'bank_name',
                    'bank_address',
                    'corr_rs',
                    'corr_swift',
                    'corr_bank_name',
                    'corr_bank_address',
                ],
                'trim',
            ],
            [
                [
                    'swift',
                    'corr_swift',
                    'rs',
                    'corr_rs',
                ],
                'filter',
                'filter' => 'strtoupper',
            ],
            [
                [
                    'rs',
                    'corr_rs',
                ],
                'match',
                'pattern' => '/[^0-9A-Z]/',
                'not' => true,
                'message' => 'Только числа и латиница',
                'when' => function ($model) {
                    return $model->is_foreign_bank;
                }
            ],
            [
                [
                    'rs',
                    'corr_rs',
                ],
                'match',
                'pattern' => '/[^0-9]/',
                'not' => true,
                'message' => 'Только числа',
                'when' => function ($model) {
                    return !$model->is_foreign_bank;
                }
            ],
            [
                [
                    'swift',
                    'corr_swift',
                ],
                'match',
                'pattern' => '/[^a-zA-Z]/',
                'not' => true,
                'message' => 'Только латинские символы',
            ],
            [
                [
                    'bank_name',
                    'swift',
                ],
                'required',
                'message' => 'Необходимо заполнить',
            ],
            [
                [
                    'rs',
                ],
                'string',
                'length' => 20,
                'when' => function ($model) {
                    return !$model->is_foreign_bank;
                }
            ],
            [
                [
                    'rs',
                ],
                'string',
                'min' => 15,
                'max' => 34,
                'when' => function ($model) {
                    return $model->is_foreign_bank;
                }
            ],
            [
                [
                    'iban',
                ],
                'string',
                'min' => 15,
                'max' => 34,
            ],
            [
                [
                    'rs',
                ],
                'required',
                'message' => 'Необходимо заполнить',
                'when' => function ($model) {
                    return empty($model->iban);
                },
            ],
            [
                [
                    'iban',
                ],
                'required',
                'message' => 'Необходимо заполнить',
                'when' => function ($model) {
                    return empty($model->rs);
                },
            ],
            [
                [
                    'swift',
                    'bank_name',
                    'corr_swift',
                    'corr_bank_name',
                ],
                'string',
                'max' => 255,
            ],
            [
                ['currency_id'],
                'exist',
                'targetClass' => Currency::class,
                'targetAttribute' => ['currency_id' => 'id'],
                'filter' => ['not', ['id' => Currency::DEFAULT_ID]],
            ],
            [
                [
                    'bank_address',
                    'corr_bank_address',
                ],
                'string',
            ],
            [
                [
                    'isMain',
                    'isClosed',
                    'is_foreign_bank',
                ],
                'boolean',
            ],
            [['createStartBalance'], 'safe'],
            [['startBalanceDate'], 'date', 'format' => 'd.M.yyyy'],
            [
                [
                    'startBalanceAmount',
                    'startBalanceDate',
                ],
                'required',
                'when' => function ($model) {
                    return $model->createStartBalance;
                },
                'message' => 'Необходимо заполнить',
            ],
            [['startBalanceAmount'],
                'number',
                'numberPattern' => Yii::$app->params['numberPattern'],
                'max' => Yii::$app->params['maxCashSum'] * 100, // with kopeck
                'tooBig' => 'Значение «{attribute}» не должно превышать ' . TextHelper::moneyFormat(Yii::$app->params['maxCashSum'], 2) . '.',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'company_id' => 'Company ID',
            'rs' => 'Валютный счет/ Account №',
            'iban' => 'IBAN (международный номер банковского счета)',
            'currency_id' => 'Валюта',
            'swift' => 'SWIFT',
            'bank_name' => 'Банк (на английском для инвойсов)',
            'bank_address' => 'Адрес банка (на английском для инвойсов)',
            'corr_rs' => 'Счет в банке-корреспонденте / Account №',
            'corr_swift' => 'SWIFT банка-корреспондента / Intermediary Bank',
            'corr_bank_name' => 'Банк-корреспондент / Intermediary Bank',
            'corr_bank_address' => 'Адрес банка-корреспондента',
            'type' => 'Тип',
            'isMain' => 'Основной',
            'isClosed' => 'Закрыть счет',
            'createStartBalance' => 'Указать начальный остаток',
            'startBalanceAmount' => 'Сумма начального остатка',
            'startBalanceDate' => 'на дату',
            'is_foreign_bank' => 'Иностранный банк',
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->type === null) {
                $this->type = self::TYPE_ADDITIONAL;
            }
            $condition = [
                'and',
                ['company_id' => $this->company_id],
                ['currency_id' => $this->currency_id],
                ['type' => self::TYPE_MAIN],
                ['not', ['id' => $this->id]],
            ];
            if ($this->type !== self::TYPE_MAIN && !self::find()->where($condition)->exists()) {
                $this->type = self::TYPE_MAIN;
            }

            return true;
        }

        return false;
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes) : void
    {
        parent::afterSave($insert, $changedAttributes);

        $condition = [
            'and',
            ['company_id' => $this->company_id],
            ['currency_id' => $this->currency_id],
            ['type' => self::TYPE_MAIN],
            ['not', ['id' => $this->id]],
        ];

        if ($this->type == self::TYPE_MAIN && self::find()->where($condition)->exists()) {
            Yii::$app->db->createCommand()->update(self::tableName(), [
                'type' => self::TYPE_ADDITIONAL,
            ], $condition)->execute();
        }

        if ($this->createStartBalance) {
            $model = new CashBankForeignCurrencyFlows([
                'flow_type' => CashFlowsBase::FLOW_TYPE_INCOME,
                'company_id' => $this->company_id,
                'currency_id' => $this->currency_id,
            ]);
            $model->date_input = $this->startBalanceDate;
            $model->amount_input = $this->startBalanceAmount;
            $model->contractor_id = CashContractorType::BALANCE_TEXT;
            $model->income_item_id = InvoiceIncomeItem::ITEM_STARTING_BALANCE;
            $model->description = 'Баланс начальный';
            $model->rs = $this->rs;
            if (!$model->save()) {
                \common\components\helpers\ModelHelper::logErrors($model, __METHOD__);
            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashBankForeignCurrencyFlows()
    {
        return $this->hasMany(CashBankForeignCurrencyFlows::className(), [
            'company_id' => 'company_id',
            'rs' => 'rs',
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * @return bool
     */
    public function hasMovement()
    {
        $movement = Invoice::find()->andWhere(['foreign_currency_account_id' => $this->id])->one();
        return (bool)$movement;
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();

        $this->isMain = $this->type == self::TYPE_MAIN ? '1' : '0';
        $this->isClosed = $this->type == self::TYPE_CLOSED ? '1' : '0';
    }

    /**
     * @return int
     */
    public function getAccountantType()
    {
        if (isset(Yii::$app->request->post('CheckingAccountant')['mainType'])) {
            $mainType = Yii::$app->request->post('CheckingAccountant')['mainType'];
            $closedType = !empty(Yii::$app->request->post('CheckingAccountant')['closedType']) ?
                Yii::$app->request->post('CheckingAccountant')['closedType'] : null;
            if ($closedType) {
                return self::TYPE_CLOSED;
            } elseif ($mainType) {
                return self::TYPE_MAIN;
            } else {
                return self::TYPE_ADDITIONAL;
            }
        }

        return self::TYPE_MAIN;
    }

    public function afterValidate()
    {
        if ($this->isClosed == '1') {
            $this->type = self::TYPE_CLOSED;
        } elseif ($this->isMain == '1') {
            $this->type = self::TYPE_MAIN;
        } elseif ($this->type === null || $this->isMain == '0') {
            $this->type = self::TYPE_ADDITIONAL;
        }

        parent::afterValidate();
    }

    public function getIsDeleteAllowed() : bool
    {
        return !$this->getCashBankForeignCurrencyFlows()->exists();
    }
}
