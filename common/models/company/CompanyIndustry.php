<?php

namespace common\models\company;

use frontend\models\log\ILogMessage;
use frontend\models\log\Log;
use frontend\models\log\LogEvent;
use Yii;
use common\models\Company;
use common\models\employee\Employee;
use common\models\company\CompanyInfoIndustry;
use common\modules\acquiring\models\AcquiringOperation;
use common\modules\cards\models\CardOperation;
use common\models\cash\CashBankFlows;
use common\models\cash\CashBankForeignCurrencyFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashEmoneyForeignCurrencyFlows;
use common\models\cash\CashOrderFlows;
use common\models\cash\CashOrderForeignCurrencyFlows;
use common\models\document\GoodsCancellation;
use common\models\document\Invoice;

/**
 * This is the model class for table "company_industry".
 *
 * @property int $id
 * @property int $company_id
 * @property int $employee_id
 * @property int $industry_type_id
 * @property int $status_id
 * @property string $name
 * @property string|null $created_at
 * @property string|null $comment
 * @property integer $is_main
 * @property integer $sort
 *
 * @property AcquiringOperation[] $acquiringOperations
 * @property CardOperation[] $cardOperations
 * @property CashBankFlows[] $cashBankFlows
 * @property CashBankForeignCurrencyFlows[] $cashBankForeignCurrencyFlows
 * @property CashEmoneyFlows[] $cashEmoneyFlows
 * @property CashEmoneyForeignCurrencyFlows[] $cashEmoneyForeignCurrencyFlows
 * @property CashOrderFlows[] $cashOrderFlows
 * @property CashOrderForeignCurrencyFlows[] $cashOrderForeignCurrencyFlows
 * @property Company $company
 * @property Employee $employee
 * @property CompanyInfoIndustry $industryType
 * @property GoodsCancellation[] $goodsCancellations
 * @property Invoice[] $invoices
 */
class CompanyIndustry extends \yii\db\ActiveRecord implements ILogMessage
{
    const STATUS_ACTIVE = 1;
    const STATUS_CLOSED = 2;

    const STRING_ACTIVE = 'В работе';
    const STRING_CLOSED = 'Закрыто';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company_industry';
    }

    /**
     * @return ActiveQuery
     */
    public static function find()
    {
        return new CompanyIndustryQuery(get_called_class());
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'employee_id', 'industry_type_id', 'name'], 'required'],
            [['company_id', 'employee_id', 'industry_type_id', 'status_id'], 'integer'],
            [['status_id'], 'default', 'value' => self::STATUS_ACTIVE],
            [['status_id'], 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_CLOSED]],
            [['created_at'], 'safe'],
            [['comment'], 'string'],
            [['name'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['company_id' => 'id']],
            [['employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::class, 'targetAttribute' => ['employee_id' => 'id']],
            [['industry_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => CompanyInfoIndustry::class, 'targetAttribute' => ['industry_type_id' => 'id']],
            [['is_main'], 'boolean']
        ];
    }

    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if ($this->isNewRecord)
            $this->created_at = date('Y-m-d H:i:s');

        $isMainExists = self::find()->where([
            'and',
            ['company_id' => $this->company_id],
            ['is_main' => 1],
            ['not', ['id' => $this->id]]
        ])->exists();

        if (!$this->is_main && !$isMainExists) {
            $this->is_main = 1;
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($this->is_main) {
            self::updateAll(['is_main' => 0], [
                'and',
                ['company_id' => $this->company_id],
                ['is_main' => 1],
                ['not', ['id' => $this->id]]
            ]);
        }
    }

        /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Компания',
            'employee_id' => 'Ответственный сотрудник',
            'industry_type_id' => 'Тип направления',
            'name' => 'Название',
            'created_at' => 'Дата создания',
            'comment' => 'Комментарий',
            'is_main' => 'Основное',
        ];
    }

    /**
     * @return string
     */
    public function getClassName()
    {
        return __CLASS__;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcquiringOperations()
    {
        return $this->hasMany(AcquiringOperation::class, ['industry_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCardOperations()
    {
        return $this->hasMany(CardOperation::class, ['industry_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashBankFlows()
    {
        return $this->hasMany(CashBankFlows::class, ['industry_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashBankForeignCurrencyFlows()
    {
        return $this->hasMany(CashBankForeignCurrencyFlows::class, ['industry_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashEmoneyFlows()
    {
        return $this->hasMany(CashEmoneyFlows::class, ['industry_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashEmoneyForeignCurrencyFlows()
    {
        return $this->hasMany(CashEmoneyForeignCurrencyFlows::class, ['industry_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashOrderFlows()
    {
        return $this->hasMany(CashOrderFlows::class, ['industry_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashOrderForeignCurrencyFlows()
    {
        return $this->hasMany(CashOrderForeignCurrencyFlows::class, ['industry_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::class, ['id' => 'employee_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Employee::class, ['id' => 'employee_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndustryType()
    {
        return $this->hasOne(CompanyInfoIndustry::class, ['id' => 'industry_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGoodsCancellations()
    {
        return $this->hasMany(GoodsCancellation::class, ['industry_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoices()
    {
        return $this->hasMany(Invoice::class, ['industry_id' => 'id']);
    }

    /**
     * @return bool
     */
    public function hasMovement()
    {
        return
            $this->getAcquiringOperations()->exists() ||
            $this->getCardOperations()->exists() ||
            $this->getCashBankFlows()->exists() ||
            $this->getCashBankForeignCurrencyFlows()->exists() ||
            $this->getCashEmoneyFlows()->exists() ||
            $this->getCashEmoneyForeignCurrencyFlows()->exists() ||
            $this->getCashOrderFlows()->exists() ||
            $this->getCashOrderForeignCurrencyFlows()->exists() ||
            $this->getGoodsCancellations()->exists() ||
            $this->getInvoices()->exists();
    }

    /**
     * @return array
     */
    public static function getSelect2Data()
    {
        $employee = Yii::$app->user->identity;

        return [0 => 'Без направления'] + CompanyIndustry::find()
                ->where(['company_id' => $employee->company->id])
                ->select(['name'])
                ->orderBy(['name' => SORT_ASC])
                ->indexBy('id')
                ->column();
    }

    // LOG

    /**
     * Get formatted log message from Log model
     * @param Log $log
     * @return string
     */
    public function getLogMessage(Log $log)
    {
        $text = 'Направление ' . $this->name;

        $link = \common\components\helpers\Html::a($text, [
            '/analytics/detailing/view-industry',
            'id' => $this->id,
        ]);

        switch ($log->log_event_id) {
            case LogEvent::LOG_EVENT_CREATE:
                return $link . '<span class="pl-1">было создано.</span>';
            case LogEvent::LOG_EVENT_DELETE:
                return $text . '<span class="pl-1">было удалено.</span>';
            case LogEvent::LOG_EVENT_UPDATE:
                return $link . '<span class="pl-1">было изменено.</span>';
            case LogEvent::LOG_EVENT_UPDATE_STATUS:
                $status = $log->getModelAttributeNew('status');
                $status_name = self::STATUS_ACTIVE == $status ? 'В работе' : 'Закрыто';

                return $link . ' статус "' . $status_name . '"';
            default:
                return $log->message;
        }
    }

    public function getLogIcon(Log $log)
    {
        return ['label-info', 'fa fa-file-text-o'];
    }
}
