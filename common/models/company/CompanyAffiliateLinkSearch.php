<?php


namespace common\models\company;


use common\models\Company;
use frontend\components\StatisticPeriod;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * Class CompanyAffiliateLInkSearch
 * @package backend\modules\company\models
 */
class CompanyAffiliateLinkSearch extends CompanyAffiliateLink
{
    /** @var $search */
    public $search;

    /** @var $company_id */
    public $company_id;

    public $creationDate;

    public $countAffiliateCompanies;

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['company_id', 'integer']],
            [['search'], 'safe'],
            [['creationDate', 'countAffiliateCompanies'], 'safe'],
        ];
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);

        $this->company_id = isset($params['company_id']) ? $params['company_id'] : 0;

        $query = $this->getBaseQuery();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'creationDate',
                    'product',
                    'countAffiliateCompanies',
                ],
                'defaultOrder' => [
                    'creationDate' => SORT_DESC,
                    'product' => SORT_ASC,
                ],
            ],
        ]);

        return $dataProvider;
    }

    /**
     * @return ActiveQuery
     */
    public function getBaseQuery()
    {
        $dateRange = StatisticPeriod::getSessionPeriod();

        $subQuery = CompanyAffiliateLink::find()
            ->addSelect([
                CompanyAffiliateLink::tableName() . '.`service_module_id` as `module`',
                CompanyAffiliateLink::tableName() . '.`id` as `affiliate_link_id`',
                CompanyAffiliateLink::tableName() . '.`custom_link_id` as `custom_link`',
                'COUNT('.Company::tableName() . '.`id`) as count',
            ])
            ->leftJoin(Company::tableName(), Company::tableName().'.invited_by_referral_code = ' . CompanyAffiliateLink::tableName() . '.link')
            ->andWhere([
                'invited_by_company_id' => $this->company_id,
            ])
            ->andWhere(['between', 'DATE(FROM_UNIXTIME(' . Company::tableName() . '.created_at))', $dateRange['from'], $dateRange['to']])
            ->groupBy(['`module`', '`affiliate_link_id`', '`custom_link`', '`company_affiliate_link`.`company_id`']);

        $query = CompanyAffiliateLink::find()
            ->distinct()
            ->addSelect([
                CompanyAffiliateLink::tableName() . '.*',
                CompanyAffiliateLink::tableName() . '.created_at as creationDate',
                'invitedCompanies.count as countAffiliateCompanies',
            ])
            ->joinWith('company')
            ->leftJoin(
                ['invitedCompanies' => $subQuery],
                CompanyAffiliateLink::tableName() . '.service_module_id = invitedCompanies.module'
                    . ' AND `company_affiliate_link`.`id` = `invitedCompanies`.`affiliate_link_id`');

        if ($this->company_id) {
            $query->andWhere([Company::tableName() . '.id' => $this->company_id]);
        }

        return $query;
    }

}