<?php

namespace common\models\company;

use Yii;
use yii\helpers\ArrayHelper;
use common\models\Company;

/**
 * This is the model class for table "company_taxation_type".
 *
 * @property integer $id
 * @property integer $company_id
 * @property integer $osno
 * @property integer $usn
 * @property integer $envd
 * @property integer $psn
 * @property integer $usn_type
 * @property integer $usn_percent
 *
 * @property Company $company
 * @property string $name
 */
class CompanyTaxationType extends \yii\db\ActiveRecord
{
    /**
     *
     */
    const INCOME = 0;
    /**
     *
     */
    const INCOME_EXPENSES = 1;

    const SELF_EMPLOYED_PERCENT = 4;

    /**
     * @var array
     */
    public static $usnType = [
        self::INCOME => 'Доход',
        self::INCOME_EXPENSES => 'Доход-Расход',
    ];

    /**
     * @var array
     */
    public static $usnDefaultPercent = [
        self::INCOME => 6,
        self::INCOME_EXPENSES => 15,
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_taxation_type';
    }

    /**
     * @return array
     */
    public function fields()
    {
        return [
            "osno",
            "usn",
            "psn",
            "usn_type",
            "usn_percent",
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->osno) {
                $this->usn = false;
            }
            if ($this->usn) {
                $this->osno = false;
            }
            /*if (!$this->usn && !$this->osno && !$this->envd) {
                $this->usn = true;
            }*/
            if (!in_array($this->company->company_type_id, [CompanyType::TYPE_OAO, CompanyType::TYPE_IP])) {
                $this->psn = false;
            }
            return true;
        }

        return false;
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     * @return bool
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ((array_key_exists('usn', $changedAttributes) && $changedAttributes['usn'] != $this->usn) ||
            (array_key_exists('usn_type', $changedAttributes) && $changedAttributes['usn_type'] != $this->usn_type) ||
            (array_key_exists('usn_percent', $changedAttributes) && $changedAttributes['usn_percent'] != $this->usn_percent)
        ) {
            if ($this->usn == 1 && $this->usn_type == self::INCOME && $this->usn_percent == 6) {
                MenuItem::updateAll([
                    'accountant_item' => 1,
                ], [
                    'accountant_item' => 0,
                    'company_id' => $this->company_id,
                ]);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['usn_percent'], 'integer'],
            [['osno', 'usn', 'psn', 'usn_type'], 'boolean'],
            [
                'osno',
                function ($attribute, $options) {
                    if (!$this->osno && !$this->usn && !$this->psn) {
                        $this->addError($attribute, 'Необходимо выбрать систему налогообложения');
                    }
                },
                'skipOnEmpty' => false,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'osno' => 'ОСНО',
            'usn' => 'УСН',
            'envd' => 'ЕНВД',
            'psn' => 'Патент',
            'usn_type' => 'Usn Type',
            'usn_percent' => 'Usn Percent',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return string
     */
    public function getName()
    {
        $result = null;
        if ($this->osno) {
            $result = 'ОСНО';
        } elseif ($this->usn) {
            $percent = $this->usn_percent ? ' (' . $this->usn_percent . '%)' : '';
            $result = 'УСН' . $percent;
        }
        if ($this->envd) {
            if ($result) {
                $result .= ' и ';
            }
            $result .= 'ЕНВД';
        }
        if ($this->psn) {
            if ($result) {
                $result .= ' и ';
            }
            $result .= 'Патент';
        }

        return $result;
    }

    /**
     * @return integer
     */
    public function defaultUsnPercent($type, $defaut = null)
    {
        return ArrayHelper::getValue(self::$usnDefaultPercent, $type, $defaut);
    }

    /**
     * @return string
     */
    public function getUsnDeclarationTaxRateIndicator()
    {
        return $this->usn_percent > 6 ? "2" : "1";
    }

}
