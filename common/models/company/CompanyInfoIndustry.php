<?php

namespace common\models\company;

use Yii;
use common\models\Company;

/**
 * This is the model class for table "company_info_industry".
 *
 * @property int $id
 * @property string $name
 *
 * @property Company[] $companies
 */
class CompanyInfoIndustry extends \yii\db\ActiveRecord
{
    const OTHER = 8;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company_info_industry';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Company::className(), ['info_industry_id' => 'id']);
    }
}
