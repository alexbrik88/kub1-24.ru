<?php


namespace common\models\company;


use common\models\Company;
use frontend\components\StatisticPeriod;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * Class CompanyAffiliateLeadsSearch
 * @package common\models\company
 */
class CompanyAffiliateLeadsSearch extends CompanyAffiliateLeads
{
    /** @var $search */
    public $search;

    /** @var $company_id */
    public $company_id;

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['search'], 'safe'],
        ];
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);

        $query = $this->getBaseQuery();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'name',
                    'username',
                    'email',
                    'phone',
                    'date',
                ],
                'defaultOrder' => [
                    'name' => SORT_ASC,
                    'username' => SORT_ASC,
                ],
            ],
        ]);

        return $dataProvider;
    }

    /**
     * @return ActiveQuery
     */
    public function getBaseQuery()
    {
        $query = CompanyAffiliateLeads::find()
            ->distinct()
            ->select([
                CompanyAffiliateLeads::tableName() . '.*',
                'created_at as date',
            ])
            ->andFilterWhere([
                'company_id' => $this->company_id
            ]);

        return $query;
    }

}