<?php

namespace common\models\company;

use Yii;
use common\models\Company;
use common\models\employee\Employee;

/**
 * This is the model class for table "menu_item".
 *
 * @property integer $company_id
 * @property integer $employee_id
 * @property integer $invoice_item
 * @property integer $b2b_item
 * @property integer $analytics_item
 * @property integer $logistics_item
 * @property integer $accountant_item
 * @property integer $project_item
 * @property integer $rent_item
 * @property integer $product_item
 * @property integer $crm_item
 *
 * @property Company $company
 * @property Employee $employee
 */
class MenuItem extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'menu_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['invoice_item', 'b2b_item', 'analytics_item', 'logistics_item', 'accountant_item', 'project_item', 'product_item', 'rent_item', 'crm_item'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'company_id' => 'Company ID',
            'employee_id' => 'Employee ID',
            'invoice_item' => 'Invoice Item',
            'b2b_item' => 'B2b Item',
            'analytics_item' => 'Analytics Item',
            'logistics_item' => 'Logistics Item',
            'accountant_item' => 'Accountant Item',
            'project_item' => 'Project Item',
            'product_item' => 'Product Item',
            'rent_item' => 'Rent Item',
            'crm_item' => 'CRM Item',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }
}
