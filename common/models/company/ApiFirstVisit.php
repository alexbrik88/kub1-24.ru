<?php

namespace common\models\company;

use Yii;
use common\models\Company;
use common\models\api\ApiPartners;

/**
 * This is the model class for table "company_api_first_visit".
 *
 * @property int $company_id
 * @property int $api_partners_id
 * @property string $visit_at
 */
class ApiFirstVisit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company_api_first_visit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'api_partners_id'], 'required'],
            [['company_id', 'api_partners_id'], 'integer'],
            [['visit_at'], 'safe'],
            [['company_id', 'api_partners_id'], 'unique', 'targetAttribute' => ['company_id', 'api_partners_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'company_id' => 'Company ID',
            'api_partners_id' => 'Api Partners ID',
            'visit_at' => 'Visit At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApiPartners()
    {
        return $this->hasOne(ApiPartners::className(), ['id' => 'api_partners_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
}
