<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 01.08.2016
 * Time: 4:26
 */

namespace common\models\company;

use yii\data\ActiveDataProvider;

/**
 * Class CheckingAccountantSearch
 * @package common\models\company
 */
class CheckingAccountantSearch extends CheckingAccountant
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @param $params
     * @param null $companyId
     * @return ActiveDataProvider
     */
    public function search($params, $active = true)
    {
        $query = self::find()
            ->byCompany($this->company_id);

        if ($active) {
            $query->byType([CheckingAccountant::TYPE_MAIN, CheckingAccountant::TYPE_ADDITIONAL]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'type' => SORT_ASC,
                    'bank_name' => SORT_ASC,
                ]
            ],
        ]);

        $this->load($params);

        return $dataProvider;
    }
}
