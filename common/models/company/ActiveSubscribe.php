<?php

namespace common\models\company;

use common\models\Company;
use common\models\service\Subscribe;
use common\models\service\SubscribeTariffGroup;
use Yii;

/**
 * This is the model class for table "company_active_subscribe".
 *
 * @property integer $company_id
 * @property integer $tariff_group_id
 * @property integer $subscribe_id
 *
 * @property Company $company
 * @property Subscribe $subscribe
 * @property SubscribeTariffGroup $tariffGroup
 * @property Subscribe $serviceSubscribe
 * @property SubscribeTariffGroup $subscribeTariffGroup
 */
class ActiveSubscribe extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_active_subscribe';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'company_id' => 'Company ID',
            'tariff_group_id' => 'Tariff Group ID',
            'subscribe_id' => 'Subscribe ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscribe()
    {
        return $this->hasOne(Subscribe::className(), ['id' => 'subscribe_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariffGroup()
    {
        return $this->hasOne(SubscribeTariffGroup::className(), ['id' => 'tariff_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServiceSubscribe()
    {
        return $this->hasOne(Subscribe::className(), ['id' => 'subscribe_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscribeTariffGroup()
    {
        return $this->hasOne(SubscribeTariffGroup::className(), ['id' => 'tariff_group_id']);
    }
}
