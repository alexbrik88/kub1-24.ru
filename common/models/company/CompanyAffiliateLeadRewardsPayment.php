<?php

namespace common\models\company;

use common\components\helpers\ArrayHelper;
use common\models\Company;
use common\models\document\Invoice;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * Class CompanyAffiliateLeadRewardsPayment
 * @package common\models\company
 *
 * @property integer $company_id
 * @property integer $lead_company_id
 * @property integer $invoice_id
 * @property integer $rewards_percent
 * @property integer $rewards_amount
 *
 * @property Company $company
 * @property Company $leadCompany
 * @property Invoice $invoice
 */
class CompanyAffiliateLeadRewardsPayment extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ]);
    }

    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id', 'company_id']);
    }

    public function getLeadCompany()
    {
        return $this->hasOne(Company::class, ['id', 'lead_company_id']);
    }

    public function getInvoice()
    {
        return $this->hasOne(Invoice::class, ['id', 'invoice_id']);
    }

    public static function setRewardsPayment($companyId, $leadCompanyId, $invoiceId, $amount)
    {
        $rewardPercent = CompanyAffiliateLeadRewards::getCurrentRewardsPercent($companyId);

        $reward = new static([
            'company_id' => $companyId,
            'lead_company_id' => $leadCompanyId,
            'invoice_id' => $invoiceId,
            'rewards_percent' => $rewardPercent,
            'rewards_amount' => ($amount / 100) * $rewardPercent,
        ]);

        return $reward->save();
    }

}