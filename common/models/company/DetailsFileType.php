<?php

namespace common\models\company;

use common\models\Company;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "company_details_file_type".
 *
 * @property int $id
 * @property string $name
 *
 * @property CompanyDetailsFile[] $companyDetailsFiles
 */
class DetailsFileType extends \yii\db\ActiveRecord
{
    const TYPE_LOGO = 1;
    const TYPE_PRINT = 2;
    const TYPE_CHIEF_SIGNATURE = 3;
    const TYPE_CHIEF_ACCOUNTANT_SIGNATURE = 4;

    const DIR_LOGO = 'logo';
    const DIR_PRINT = 'print';
    const DIR_CHIEF_SIGNATURE = 'chief_signature';
    const DIR_CHIEF_ACCOUNTANT_SIGNATURE = 'chief_accountant_signature';

    public static $dirNameList = [
        self::TYPE_LOGO => self::DIR_LOGO,
        self::TYPE_PRINT => self::DIR_PRINT,
        self::TYPE_CHIEF_SIGNATURE => self::DIR_CHIEF_SIGNATURE,
        self::TYPE_CHIEF_ACCOUNTANT_SIGNATURE => self::DIR_CHIEF_ACCOUNTANT_SIGNATURE,
    ];

    public static $fieldNameList = [
        self::TYPE_LOGO => self::DIR_LOGO.'_id',
        self::TYPE_PRINT => self::DIR_PRINT.'_id',
        self::TYPE_CHIEF_SIGNATURE => self::DIR_CHIEF_SIGNATURE.'_id',
        self::TYPE_CHIEF_ACCOUNTANT_SIGNATURE => self::DIR_CHIEF_ACCOUNTANT_SIGNATURE.'_id',
    ];

    public static $imgNameList = [
        self::TYPE_LOGO => Company::IMAGE_LOGO_NAME,
        self::TYPE_PRINT => Company::IMAGE_PRINT_NAME,
        self::TYPE_CHIEF_SIGNATURE => Company::IMAGE_CHIEF_SIGNATURE,
        self::TYPE_CHIEF_ACCOUNTANT_SIGNATURE => Company::IMAGE_CHIEF_ACCOUNTANT_SIGNATURE,
    ];

    public static $imgAttrList = [
        self::TYPE_LOGO => Company::IMAGE_LOGO_NAME.'Image',
        self::TYPE_PRINT => Company::IMAGE_PRINT_NAME.'Image',
        self::TYPE_CHIEF_SIGNATURE => Company::IMAGE_CHIEF_SIGNATURE.'Image',
        self::TYPE_CHIEF_ACCOUNTANT_SIGNATURE => Company::IMAGE_CHIEF_ACCOUNTANT_SIGNATURE.'Image',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company_details_file_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyDetailsFiles()
    {
        return $this->hasMany(CompanyDetailsFile::className(), ['type_id' => 'id']);
    }

    /**
     * @param  string $type
     * @return DetailsFile
     */
    public static function findTypeId($type)
    {
        if (isset(self::$dirNameList[$type])) {
            return $type;
        }
        if (($typeId = array_search($type, self::$imgNameList)) ||
            ($typeId = array_search($type, self::$imgAttrList)) ||
            ($typeId = array_search($type, self::$dirNameList)) ||
            ($typeId = array_search($type, self::$fieldNameList))
        ) {
            return $typeId;
        }

        return null;
    }
}
