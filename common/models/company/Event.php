<?php

namespace common\models\company;

use Yii;

/**
 * This is the model class for table "event".
 *
 * @property integer $id
 * @property string $name
 *
 * @property CompanyFirstEvent[] $companyFirstEvents
 * @property Company[] $companies
 */
class Event extends \yii\db\ActiveRecord
{
    const FINANCE_DETAILING_INDUSTRY = 126;
    const FINANCE_DETAILING_SALE_POINT = 127;
    const FINANCE_DETAILING_PROJECT = 128;


    protected $_steps = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'event';
    }

    /**
     * Returns the list of all attribute names of the model.
     * The default implementation will return all column names of the table associated with this AR class.
     * @return array list of attribute names.
     */
    public function attributes()
    {
        if ($this->_steps === []) {
            foreach (range(1, 100) as $num) {
                $this->_steps[] = 'step' . $num;
            }
        }
        return array_merge(array_keys(static::getTableSchema()->columns), $this->_steps);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyFirstEvents()
    {
        return $this->hasMany(CompanyFirstEvent::className(), ['event_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Company::className(), ['id' => 'company_id'])->viaTable('company_first_event', ['event_id' => 'id']);
    }
}
