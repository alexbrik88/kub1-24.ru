<?php

namespace common\models\company;

use common\models\Company;
use Yii;

/**
 * This is the model class for table "employee_count".
 *
 * @property int $id
 * @property string $name
 *
 * @property Company[] $companies
 */
class EmployeeCount extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'employee_count';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * Gets query for [[Companies]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Company::className(), ['employee_count_id' => 'id']);
    }
}
