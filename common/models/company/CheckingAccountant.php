<?php

namespace common\models\company;

use backend\models\Bank;
use common\components\TextHelper;
use common\components\cash\InternalTransferAccountInterface;
use common\components\cash\InternalTransferFlowInterface;
use common\components\validators\BikValidator;
use common\models\cash\CashBankStatementUpload;
use common\models\cash\CashBankFlows;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashBankForeignCurrencyFlows;
use common\models\cash\WalletInterface;
use common\models\Company;
use common\models\Contractor;
use common\models\currency\Currency;
use common\models\dictionary\bik\BikDictionary;
use common\models\document\ForeignCurrencyInvoice;
use common\models\document\Invoice;
use common\models\document\InvoiceIncomeItem;
use frontend\modules\cash\models\CashContractorType;
use frontend\modules\cash\modules\banking\models\BankingEmail;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "checking_accountant".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $company_id
 * @property integer $currency_id
 * @property string $bank_name
 * @property string $bank_city
 * @property string $bank_address
 * @property integer $bik
 * @property integer $ks
 * @property integer $rs
 * @property integer $type
 * @property integer $isMain
 * @property integer $isClosed
 * @property string $iban
 * @property string $swift
 * @property string $bank_name_en
 * @property string $bank_address_en
 * @property string $corr_rs
 * @property string $corr_swift
 * @property string $corr_bank_name
 * @property string $corr_bank_address
 * @property string $is_foreign_bank
 *
 *
 * @property BikDictionary $bank
 * @property Bank $sysBank
 * @property CashBankStatementUpload $statementUploads
 * @property BankingEmail $bankingEmail
 */
class CheckingAccountant extends ActiveRecord implements WalletInterface, InternalTransferAccountInterface
{
    /**
     *  Основной
     */
    const TYPE_MAIN = 0;
    /**
     *  Вспомогательный
     */
    const TYPE_ADDITIONAL = 1;
    /**
     *  Закрытый
     */
    const TYPE_CLOSED = 2;
    /**
     *
     */
    const SCENARIO_LANDING_CREATION = 'landing-creation';

    public $isMain;
    public $isClosed;
    public $createStartBalance = false;
    public $startBalanceAmount;
    public $startBalanceDate;

    /**
     * @var array
     */
    public $typeText = [
        self::TYPE_MAIN => 'Основной',
        self::TYPE_ADDITIONAL => 'Дополнительный',
        self::TYPE_CLOSED => 'Закрытый',
    ];

    /**
     * @var bool
     */
    public static $formNameModify = true;

    /**
     * @var array
     */
    public static $typeActive = [
        self::TYPE_MAIN,
        self::TYPE_ADDITIONAL,
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'checking_accountant';
    }

    public function getCashContractorTypeText() : string
    {
        return CashContractorType::BANK_TEXT;
    }

    public function getInternalTransferId() : string
    {
        return self::tableName() . $this->id;
    }

    public function getInternalTransferName() : ?string
    {
        return $this->name;
    }

    public function getCurrencyId() : ?string
    {
        return $this->currency_id ?? null;
    }

    public function getCurrencyName() : ?string
    {
        return Currency::name($this->currency_id);
    }

    public function getCashFlowClass() : string
    {
        return $this->currency_id == Currency::DEFAULT_ID ? CashBankFlows::class : CashBankForeignCurrencyFlows::class;
    }

    public function getCashFlowTable() : string
    {
        $class = $this->getCashFlowClass();

        return $class::tableName();
    }

    public function getIsForeign() : string
    {
        return $this->currency_id != Currency::DEFAULT_ID;
    }

    public function getWalletId() : int
    {
        return CashFlowsBase::WALLET_BANK;
    }

    public function getBalance() : int
    {
        if ($this->rs) {
            $class = $this->getIsForeign() ? CashBankForeignCurrencyFlows::class : CashBankFlows::class;
            return (new \yii\db\Query())
                ->from($class::tableName())
                ->where(['company_id' => $this->company_id])
                ->andWhere(['rs' => $this->rs])
                ->sum(new \yii\db\Expression('CASE WHEN flow_type = 1 THEN amount ELSE -amount END')) ?: 0;
        }

        return 0;
    }

    public function getCashFlowNewModel() : InternalTransferFlowInterface
    {
        $class = $this->getCashFlowClass();
        $model = new $class;
        $model->company_id = $this->company_id;
        $model->rs = $this->rs;
        $model->bank_name = $this->bank_name;
        $model->has_invoice = 0;

        if ($this->currency_id == Currency::DEFAULT_ID) {
            $model->scenario = 'create';
        } else {
            $model->currency_id = $this->currency_id;
            $model->currency_name = Currency::name($this->currency_id);
            $model->swift = $this->swift;
            $model->bank_address = $this->bank_address;
        }

        $model->populateRelation('selfAccount', $this);

        return $model;
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();

        $this->isMain = $this->type == self::TYPE_MAIN ? '1' : '0';
        $this->isClosed = $this->type == self::TYPE_CLOSED ? '1' : '0';
    }

    /**
     * @inheritdoc
     */
    public function formName()
    {
        return static::$formNameModify ? parent::formName() . ($this->isNewRecord? '': $this->id) : parent::formName();
    }

    /**
     * @return CheckingAccountantQuery
     */
    public static function find()
    {
        return new CheckingAccountantQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }
    /**
     * @return array
     */
    public function scenarios()
    {
        return array_merge(parent::scenarios(), [
            self::SCENARIO_LANDING_CREATION => [
                'company_id',
                'bik',
                'rs',
                'created_at',
                'updated_at',
                'ks',
                'type',
                'bank_name',
            ],
        ]);
    }
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['currency_id', 'default', 'value' => '643'], // rub
            [
                [
                    'name',
                    'bik',
                    'rs',
                    'iban',
                    'swift',
                    'bank_name',
                    'bank_city',
                    'bank_address',
                    'bank_name_en',
                    'bank_address_en',
                    'corr_rs',
                    'corr_swift',
                    'corr_bank_name',
                    'corr_bank_address',
                ],
                'trim',
            ],
            [
                [
                    'iban',
                    'swift',
                    'corr_swift',
                    'rs',
                    'corr_rs',
                ],
                'filter',
                'filter' => 'strtoupper',
            ],
            [
                [
                    'rs',
                ],
                'required',
                'message' => 'Необходимо заполнить',
            ],
            [['currency_id'], 'exist', 'targetClass' => Currency::class, 'targetAttribute' => ['currency_id' => 'id']],
            [
                [
                    'bik',
                ],
                'required',
                'message' => 'Необходимо заполнить',
                'when' => function ($model) {
                    return !$model->is_foreign_bank;
                }
            ],
            [
                [
                    'swift',
                ],
                'required',
                'message' => 'Необходимо заполнить',
                'when' => function ($model) {
                    return $model->is_foreign_bank || $model->getIsForeign();
                }
            ],
            [
                [
                    'bank_name',
                ],
                'required',
                'message' => 'Необходимо заполнить',
                'when' => function ($model) {
                    return $model->is_foreign_bank;
                }
            ],
            [
                ['inn',
                    'company_id',
                    'bik',
                    'rs',
                    'ks',
                    'bank_name',
                ],
                'required',
                'on' => self::SCENARIO_LANDING_CREATION,
                'message' => 'Необходимо заполнить',
            ],
            [['created_at', 'updated_at', 'ks', 'type'], 'integer'],
            [
                ['bik'], 'string',
                'length' => 9,
                'max' => 9,
                'notEqual' => 'Должно быть 9 символов',
                'when' => function ($model) {
                    return !$model->is_foreign_bank;
                }
            ],
            [
                ['bik'], 'exist',
                'targetClass' => BikDictionary::class,
                'message' => 'Значение "БИК" неверно. Возможно ваш банк изменил БИК. Проверьте на сайте банка.'
            ],
            [
                'bik', BikValidator::className(),
                'related' => [
                    'bank_name' => 'name',
                    'ks' => 'ks',
                ],
            ],
            [
                ['rs'], 'string',
                'length' => 20,
                'max' => 20,
                'notEqual' => 'Должно быть 20 символов',
                'when' => function ($model) {
                    return !$model->is_foreign_bank;
                }
            ],
            [
                ['rs'], 'string',
                'max' => 34,
                'when' => function ($model) {
                    return $model->is_foreign_bank;
                }
            ],
            [
                [
                    'swift',
                    'bank_name',
                    'bank_city',
                    'bank_address',
                    'bank_name_en',
                    'bank_address_en',
                    'corr_rs',
                    'corr_swift',
                    'corr_bank_name',
                    'corr_bank_address',
                ], 'string',
                'max' => 255,
            ],
            [
                [
                    'iban',
                ],
                'string',
                'min' => 15,
                'max' => 34,
            ],
            [
                [
                    'name',
                ],
                'string',
                'max' => 30,
                'when' => function ($model) {
                    return $model->name != ($model->bank_name ?: $model->bank_name_en);
                },
            ],
            [
                'rs', 'unique',
                'targetAttribute' => ['company_id', 'rs'],
                'message' => 'Такой расчетный счет уже существует',
            ],
            [
                [
                    'isMain',
                    'isClosed',
                    'is_foreign_bank',
                ], 'boolean'
            ],
            [['createStartBalance'], 'safe'],
            [['startBalanceDate'], 'date', 'format' => 'd.M.yyyy'],
            [
                [
                    'startBalanceAmount',
                    'startBalanceDate',
                ],
                'required',
                'when' => function (CheckingAccountant $model) {
                    return $model->createStartBalance;
                },
                'whenClient' => 'function (attribute, value) { return $("#checkingaccountant-createstartbalance").is(":checked"); }',
                'message' => 'Необходимо заполнить',
            ],
            [['startBalanceAmount'],
                'number',
                'numberPattern' => Yii::$app->params['numberPattern'],
                'max' => Yii::$app->params['maxCashSum'] * 100, // with kopeck
                'tooBig' => 'Значение «{attribute}» не должно превышать ' . TextHelper::moneyFormat(Yii::$app->params['maxCashSum'], 2) . '.',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'company_id' => 'Company ID',
            'name' => 'Название, которое будет отображаться',
            'bank_name' => 'Банк',
            'bank_address' => 'Адрес банка',
            'bank_city' => 'Город',
            'bank_name_en' => 'Банк (на английском)',
            'bank_address_en' => 'Адрес банка (на английском)',
            'bik' => 'БИК',
            'ks' => 'Корреспондентский счет',
            'rs' => 'Счет №',
            'type' => 'Тип',
            'isMain' => 'Основной',
            'isClosed' => 'Счет закрыт',
            'createStartBalance' => 'Указать начальный остаток',
            'startBalanceAmount' => 'Сумма начального остатка',
            'startBalanceDate' => 'на дату',
            'corr_rs' => 'Счет в банке-корреспонденте / Account №',
            'corr_swift' => 'SWIFT банка-корреспондента / Intermediary Bank',
            'corr_bank_name' => 'Банк-корреспондент / Intermediary Bank',
            'corr_bank_address' => 'Адрес банка-корреспондента',
            'iban' => 'IBAN (международный номер банковского счета)',
            'currency_id' => 'Валюта',
            'swift' => 'SWIFT',
            'is_foreign_bank' => 'Иностранный банк',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBankingEmail()
    {
        return $this->hasOne(BankingEmail::className(), ['checking_accountant_id' => 'id']);
    }

    /**
     * @return int
     */
    public function getAccountantType()
    {
        if (isset(Yii::$app->request->post('CheckingAccountant')['mainType'])) {
            $mainType = Yii::$app->request->post('CheckingAccountant')['mainType'];
            $closedType = !empty(Yii::$app->request->post('CheckingAccountant')['closedType']) ?
                            Yii::$app->request->post('CheckingAccountant')['closedType'] : null;
            if ($closedType) {
                return self::TYPE_CLOSED;
            } elseif ($mainType) {
                return self::TYPE_MAIN;
            } else {
                return self::TYPE_ADDITIONAL;
            }
        }

        return self::TYPE_MAIN;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBank()
    {
        return $this->hasOne(BikDictionary::className(), ['bik' => 'bik']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSysBank()
    {
        return $this->hasOne(Bank::className(), ['bik' => 'bik']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatementUploads()
    {
        return $this->hasMany(CashBankStatementUpload::className(), [
            'company_id' => 'company_id',
            'bik' => 'bik',
            'rs' => 'rs',
        ]);
    }

    /**
     * @return string
     */
    public function getTypeText()
    {
        return ArrayHelper::getValue(self::$typeText, $this->type, '');
    }

    /**
     * @return array
     */
    public static function getList()
    {
        /* @var CheckingAccountant $mainAccountant */
        $array = array_map([self::className(), 'getRsWithBankName'],
            self::find()->byCompany(Yii::$app->user->identity->company->id)
                ->andWhere(['!=', 'type', self::TYPE_CLOSED])
                ->orderBy('type, bank_name')
                ->all());
        $result = [];
        foreach ($array as $key => $accountant) {
            unset($array[$key]);
            foreach ($accountant as $id => $rs) {
                $result[$id] = $rs;
            }
        }

        return $result;
    }

    /**
     * @param CheckingAccountant $checkingAccountant
     * @return mixed
     */
    public static function getRsWithBankName(CheckingAccountant $checkingAccountant)
    {
        $result[$checkingAccountant->id] = $checkingAccountant->rs . ' ' . $checkingAccountant->bank_name;

        return $result;
    }

    /**
     * @return bool
     */
    public function hasMovement()
    {
        $query1 = Invoice::find()->andWhere(['company_checking_accountant_id' => $this->id, 'is_deleted' => 0]);
        $query2 = ForeignCurrencyInvoice::find()->andWhere(['checking_accountant_id' => $this->id, 'is_deleted' => 0]);
        $query3 = CashBankFlows::find()->andWhere(['rs' => $this->rs, 'company_id' => $this->company_id]);
        $query4 = CashBankForeignCurrencyFlows::find()->andWhere(['rs' => $this->rs, 'company_id' => $this->company_id]);

        if ($query1->exists() || $query2->exists() || $query3->exists() || $query4->exists()) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * @return bool
     */
    public function afterValidate()
    {
        if ($this->isAttributeChanged('bik') && !$this->is_foreign_bank) {
            $bank = BikDictionary::find()->byBik($this->bik)->byActive()->one();
            $this->bank_name = $bank ? $bank->name : null;
            $this->bank_city = $bank ? $bank->city : null;
            $this->ks = $bank ? $bank->ks : null;
        }

        if ($this->isClosed == '1') {
            $this->type = self::TYPE_CLOSED;
        } elseif ($this->isMain == '1') {
            $this->type = self::TYPE_MAIN;
        } elseif ($this->type === null || $this->isMain == '0') {
            $this->type = self::TYPE_ADDITIONAL;
        }

        parent::afterValidate();
    }

    /**
     * @param Company|null $company
     * @return bool
     */
    public function _save(Company $company = null)
    {
        $this->company_id = ($company == null) ? Yii::$app->user->identity->currentEmployeeCompany->company->id : $company->id;
        return $this->save();
    }

    /**
     * @param Company|null $company
     * @return bool
     */
    public function _saveForNewCompany(Company $company = null)
    {
        if (!$company)
            return false;

        $this->company_id = $company->id;
        if (count($company->checkingAccountants) !== 1 || $this->isNewRecord) {
            if (!$this->isNewRecord) {
                $this->type = $this->getAccountantType();
            } else {
                $this->type = !$this->type;
            }
            $this->type = (int)$this->type;
            /* @var CheckingAccountant $mainCheckingAccountant */
            $mainCheckingAccountant = $company->mainCheckingAccountant;
            if ($this->type == self::TYPE_MAIN) {
                if ($mainCheckingAccountant !== null && $mainCheckingAccountant->id !== $this->id) {
                    $mainCheckingAccountant->type = self::TYPE_ADDITIONAL;
                    if (!$mainCheckingAccountant->save(true, ['type'])) {
                        return false;
                    }
                }
            } elseif ($mainCheckingAccountant == null) {
                $this->type = CheckingAccountant::TYPE_MAIN;
            }
        }

        return $this->save();
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->type === null) {
                $this->type = self::TYPE_ADDITIONAL;
            }

            if (!$this->currency_id) {
                $this->currency_id = Currency::DEFAULT_ID;
            }

            $condition = [
                'and',
                ['company_id' => $this->company_id],
                ['currency_id' => $this->currency_id],
                ['type' => self::TYPE_MAIN],
                ['not', ['id' => $this->id]],
            ];
            if ($this->type !== self::TYPE_MAIN && !self::find()->where($condition)->exists()) {
                $this->type = self::TYPE_MAIN;
            }

            if (empty($this->name)) {
                $this->name = $this->bank_name ?: $this->bank_name_en;
            }

            return true;
        }

        return false;
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $condition = [
            'and',
            ['company_id' => $this->company_id],
            ['currency_id' => $this->currency_id],
            ['type' => self::TYPE_MAIN],
            ['not', ['id' => $this->id]],
        ];
        if ($this->type == self::TYPE_MAIN && self::find()->where($condition)->exists()) {
            self::updateAll(['type' => self::TYPE_ADDITIONAL], $condition);
        }

        if ($insert) {
            \common\models\company\CompanyFirstEvent::checkEvent($this->company, 2);
        }

        if ($this->createStartBalance) {
            if ($this->currency_id == Currency::DEFAULT_ID) {
                $model = new CashBankFlows([
                    'flow_type' => CashFlowsBase::FLOW_TYPE_INCOME,
                    'company_id' => $this->company_id,
                ]);
                $model->date = $this->startBalanceDate;
                $model->amount = $this->startBalanceAmount;
            } else {
                $model = new CashBankForeignCurrencyFlows([
                    'flow_type' => CashFlowsBase::FLOW_TYPE_INCOME,
                    'company_id' => $this->company_id,
                    'currency_id' => $this->currency_id,
                ]);
                $model->date_input = $this->startBalanceDate;
                $model->amount_input = $this->startBalanceAmount;
            }
            $model->contractor_id = CashContractorType::BALANCE_TEXT;
            $model->income_item_id = InvoiceIncomeItem::ITEM_STARTING_BALANCE;
            $model->description = 'Баланс начальный';
            $model->rs = $this->rs;

            if (!$model->save()) {
                \common\components\helpers\ModelHelper::logErrors($model, __METHOD__);
            }
        }
    }

    /**
     * @return  string
     */
    public function getLastUploadDate()
    {
        return $this->getStatementUploads()->max('period_till');
    }

    /**
     * @return  string
     */
    public function getBankCity()
    {
        return !empty($this->bank_city) ? 'г. '.$this->bank_city : null;
    }

    /**
     * @inheritDoc
     */
    public function getWalletName(): string
    {
        return sprintf('%s, %s', $this->rs, $this->bank_name);
    }

    /**
     * @inheritDoc
     */
    public function getIsClosed(): bool
    {
        return $this->type == self::TYPE_CLOSED;
    }
}
