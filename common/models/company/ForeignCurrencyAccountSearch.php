<?php

namespace common\models\company;

use yii\data\ActiveDataProvider;

/**
 * Class ForeignCurrencyAccountSearch
 * @package common\models\company
 */
class ForeignCurrencyAccountSearch extends ForeignCurrencyAccount
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @param $params
     * @param bool $active
     * @return ActiveDataProvider
     */
    public function search($params, $active = true)
    {
        $query = self::find()
            ->byCompany($this->company_id);

        if ($active) {
            $query->byType([ForeignCurrencyAccount::TYPE_MAIN, ForeignCurrencyAccount::TYPE_ADDITIONAL]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'type' => SORT_ASC,
                    'bank_name' => SORT_ASC,
                ]
            ],
        ]);

        $this->load($params);

        return $dataProvider;
    }
}
