<?php

namespace common\models\company;

use Yii;
use common\models\Company;
use common\models\employee\Employee;

/**
 * This is the model class for table "company_last_visit".
 *
 * @property int $company_id
 * @property int $employee_id
 * @property string $ip
 * @property int $time
 */
class CompanyLastVisit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company_last_visit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'employee_id', 'ip', 'time'], 'required'],
            [['company_id', 'employee_id', 'time'], 'integer'],
            [['ip'], 'string', 'max' => 45],
            [['company_id', 'employee_id', 'ip'], 'unique', 'targetAttribute' => ['company_id', 'employee_id', 'ip']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'company_id' => 'Company ID',
            'employee_id' => 'Employee ID',
            'ip' => 'Ip',
            'time' => 'Time',
        ];
    }

    public static function create(Company $company, Employee $employee, string $ip = '') : int
    {
        return Yii::$app->db->createCommand()->upsert(self::tableName(), [
            'company_id' => $company->id,
            'employee_id' => $employee->id,
            'ip' => $ip,
            'time' => $time = time(),
        ], [
            'ip' => $ip,
            'time' => $time,
        ])->execute();
    }
}
