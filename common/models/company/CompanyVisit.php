<?php

namespace common\models\company;

use common\models\Company;
use Yii;

/**
 * This is the model class for table "company_visit".
 *
 * @property integer $company_id
 * @property integer $number
 * @property integer $created_at
 * @property integer $updated_at
 */
class CompanyVisit extends \yii\db\ActiveRecord
{
    public $duration;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_visit';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'number', 'created_at', 'updated_at'], 'required'],
            [['company_id', 'number', 'created_at', 'updated_at'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'number' => 'Номер посещения',
            'duration' => 'Время нахождения в сервисе',
            'created_at' => 'Дата',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function checkVisit(Company $company)
    {
        $visit = self::find()
            ->andWhere(['company_id' => $company->id])
            ->andWhere(['>', 'updated_at', date_create('today')->getTimestamp()])
            ->orderBy(['updated_at' => SORT_DESC])
            ->one();

        if ($visit === null) {

            Yii::$app->db->createCommand('INSERT IGNORE INTO `company_visit` (`company_id`, `number`, `created_at`, `updated_at`) VALUES (:company_id, :number, :created_at, :updated_at)', [
                'company_id' => $company->id,
                'number' => self::find()->andWhere(['company_id' => $company->id])->count() + 1,
                'created_at' => time(),
                'updated_at' => time(),
            ])->execute();

            //$visit = new CompanyVisit([
            //    'company_id' => $company->id,
            //    'number' => self::find()->andWhere(['company_id' => $company->id])->count() + 1,
            //    'created_at' => time(),
            //    'updated_at' => time(),
            //]);
            //$visit->save(false);
        } else {
            $visit->updateAttributes([
                'updated_at' => time(),
            ]);
        }
    }
}
