<?php

namespace common\models\company;

use Yii;

/**
 * This is the model class for table "company_details_file_status".
 *
 * @property int $id
 * @property string $name
 *
 * @property CompanyDetailsFile[] $companyDetailsFiles
 */
class DetailsFileStatus extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_TEMPORARY = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company_details_file_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetailsFiles()
    {
        return $this->hasMany(DetailsFile::className(), ['status_id' => 'id']);
    }
}
