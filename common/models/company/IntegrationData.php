<?php

namespace common\models\company;

use common\models\Company;
use yii\base\ArrayableTrait;
use yii\base\BaseObject;
use yii\base\InvalidConfigException;

class IntegrationData extends BaseObject
{
    use ArrayableTrait;

    /**
     * @var Company
     */
    public $company;

    /**
     * @var string
     */
    public $module;

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (!($this->company instanceof Company) || empty($this->module)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @inheritDoc
     */
    public function fields()
    {
        return ['accessToken', 'accountId', 'balance'];
    }

    /**
     * @return string
     */
    public function getAccessToken(): string
    {
        return (string) $this->getConfig('access_token', '');
    }

    /**
     * @return string
     */
    public function getAccountId(): string
    {
        return (string) $this->getConfig('account_id', '');
    }

    /**
     * @return float
     */
    public function getBalance(): float
    {
        return (float) $this->getConfig('balance_amount', 0.0);
    }

    /**
     * @return bool
     */
    public function hasAccessToken(): bool
    {
        $token = $this->getAccessToken();

        return (strlen($token) > 0);
    }

    /**
     * @return bool
     */
    public function hasAccountId(): bool
    {
        $account = $this->getAccountId();

        return (strlen($account) > 0);
    }

    /**
     * @param string $accessToken
     * @return void
     */
    public function setAccessToken(string $accessToken): void
    {
        $this->setConfig('access_token', $accessToken);
    }

    /**
     * @param string $accountId
     * @return void
     */
    public function setAccountId(string $accountId): void
    {
        $this->setConfig('account_id', $accountId);
    }

    /**
     * @param float $balance
     * @return void
     */
    public function setBalance(float $balance): void
    {
        $this->setConfig('balance_amount', $balance);
    }

    /**
     * @return void
     */
    public function cleanConfig(): void
    {
        $this->company->setIntegration($this->module, null);
    }

    /**
     * @return bool
     */
    public function saveConfig(): bool
    {
        return $this->company->save(false, ['integration']);
    }

    /**
     * @param string $key
     * @param mixed $defaultValue
     * @return mixed
     */
    private function getConfig(string $key, $defaultValue = null)
    {
        $config = $this->company->integration($this->module) ?? [];

        if (!array_key_exists($key, $config) || $config[$key] === null) {
            return $defaultValue;
        }

        return $config[$key];
    }

    /**
     * @param string $key
     * @param mixed $value
     * @return void
     */
    private function setConfig(string $key, $value): void
    {
        $config = $this->company->integration($this->module) ?? [];
        $config[$key] = $value;
        $this->company->setIntegration($this->module, $config);
    }
}
