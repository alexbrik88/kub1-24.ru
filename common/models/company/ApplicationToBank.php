<?php

namespace common\models\company;

use backend\models\Bank;
use common\components\DadataClient;
use common\components\validators\PhoneValidator;
use common\models\Company;
use frontend\modules\cash\modules\banking\components\Banking;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "application_to_bank".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $company_id
 * @property integer $bank_id
 * @property string $company_name
 * @property string $inn
 * @property string $kpp
 * @property string $legal_address
 * @property string $fio
 * @property string $contact_phone
 *
 * @property Company $company
 * @property Bank $bank
 */
class ApplicationToBank extends \yii\db\ActiveRecord
{
    public $subject = 'Заявка на открытие расчетного счета';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'application_to_bank';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_name', 'inn', 'kpp', 'legal_address', 'fio', 'contact_email'], 'trim'],
            [['company_id', 'company_name', 'inn', 'legal_address', 'fio', 'contact_phone', 'contact_email', 'bank_id'], 'required'],
            [['created_at', 'company_id', 'bank_id'], 'integer'],
            [['company_name', 'legal_address', 'fio'], 'string', 'max' => 255],
            [['inn'], 'string', 'max' => 12,],
            [['kpp'], 'string', 'length' => 9,],
            [['inn'], 'string', 'length' => 10,
                'when' => function () {
                    return Yii::$app->user->identity->company->company_type_id !== CompanyType::TYPE_IP;
                },
                'whenClient' => 'function(){}',
            ],
            [['inn'], 'string', 'length' => 12,
                'when' => function () {
                    return Yii::$app->user->identity->company->company_type_id == CompanyType::TYPE_IP;
                },
                'whenClient' => 'function(){}',
            ],
            [
                'inn', 'validateByDadata',
            ],
            [['contact_phone'], PhoneValidator::className()],
            [['contact_email'], 'email'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['bank_id'], 'exist', 'skipOnError' => true, 'targetClass' => Bank::className(), 'targetAttribute' => ['bank_id' => 'id']],
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateByDadata($attribute, $params)
    {
        if (DadataClient::getCompanyData($this->$attribute) === null) {
            $this->addError($attribute, 'Уточните ваш ИНН');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'company_id' => 'Company ID',
            'bank_id' => 'Bank Id',
            'company_name' => 'Название компании',
            'inn' => 'ИНН',
            'kpp' => 'КПП',
            'legal_address' => 'Юр. адрес',
            'fio' => 'Ваше ФИО',
            'contact_phone' => 'Контактный телефон',
            'contact_email' => 'Email',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBank()
    {
        return $this->hasOne(Bank::className(), ['id' => 'bank_id']);
    }

    /**
     * @return bool
     */
    public function send()
    {
        if (in_array($this->bank->bik, Banking::$bikListApplicationApi)) {
            $isSent = $this->sendByApi();
        } else {
            $isSent = $this->sendByEmail();
        }

        if ($isSent) {
            \Yii::$app->mailer->compose([
                'html' => 'system/checking-accountant/html',
                'text' => 'system/checking-accountant/text',
            ], [
                'application' => $this,
                'subject' => $this->subject,
            ])
                ->setFrom([\Yii::$app->params['emailList']['info'] => \Yii::$app->params['emailFromName']])
                ->setTo(\Yii::$app->params['emailList']['support'])
                ->setSubject($this->subject)
                ->send();

            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function sendByApi()
    {
        $apiModelClass = Banking::classByBik($this->bank->bik);

        if (class_exists($apiModelClass) && method_exists($apiModelClass, 'applicationRequest')) {
            return $apiModelClass::applicationRequest($this);
        }

        return false;
    }

    /**
     * @return bool
     */
    public function sendByEmail()
    {
        $mail = \Yii::$app->mailer->compose([
            'html' => 'system/checking-accountant/html',
            'text' => 'system/checking-accountant/text',
        ], [
            'application' => $this,
            'subject' => $this->subject,
        ])
            ->setFrom([\Yii::$app->params['emailList']['info'] => \Yii::$app->params['emailFromName']])
            ->setTo($this->bank->email)
            ->setSubject($this->subject);

        return $mail->send();
    }
}
