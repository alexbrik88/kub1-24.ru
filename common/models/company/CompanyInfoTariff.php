<?php

namespace common\models\company;

use common\models\Company;
use common\models\service\SubscribeTariffGroup;
use Yii;

/**
 * This is the model class for table "company_info_tariff".
 *
 * @property int $company_id
 * @property int $tariff_group_id
 */
class CompanyInfoTariff extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company_info_tariff';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'tariff_group_id'], 'required'],
            [
                ['company_id'], 'exist',
                'skipOnError' => true,
                'targetClass' => Company::class,
                'targetAttribute' => ['company_id' => 'id'],
            ],
            [
                ['tariff_group_id'], 'exist',
                'skipOnError' => true,
                'targetClass' => SubscribeTariffGroup::class,
                'targetAttribute' => ['tariff_group_id' => 'id'],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'company_id' => 'Company ID',
            'tariff_group_id' => 'Tariff Group ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariffGroup()
    {
        return $this->hasOne(Company::className(), ['id' => 'tariff_group_id']);
    }
}
