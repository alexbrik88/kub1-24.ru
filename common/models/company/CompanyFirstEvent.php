<?php

namespace common\models\company;

use common\models\Company;
use Yii;
use yii\db\Exception;

/**
 * This is the model class for table "company_first_event".
 *
 * @property integer $company_id
 * @property integer $event_id
 * @property integer $step
 * @property integer $visit
 * @property integer $created_at
 *
 * @property Company $company
 * @property Event $event
 */
class CompanyFirstEvent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_first_event';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'event_id', 'step', 'visit', 'created_at'], 'required'],
            [['company_id', 'event_id', 'step', 'visit', 'created_at'], 'integer'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['event_id'], 'exist', 'skipOnError' => true, 'targetClass' => Event::className(), 'targetAttribute' => ['event_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'company_id' => 'Company ID',
            'event_id' => 'Действия',
            'event.name' => 'Действия',
            'name' => 'Действия',
            'step' => 'Шаги',
            'visit' => 'Посещения',
            'created_at' => 'Время',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Event::className(), ['id' => 'event_id']);
    }

    /**
     * @inheritdoc
     */
    public static function checkEvent(Company $company, $event_id, $byPaidSubscribes = false)
    {
        $visit = CompanyVisit::find()->where(['company_id' => $company->id])->max('number');
        if ($visit !== null && ($byPaidSubscribes || !$company->getHasPaidSubscribes())) {
            if (!self::find()->where(['company_id' => $company->id, 'event_id' => $event_id])->exists()) {
                $event = new CompanyFirstEvent([
                    'company_id' => $company->id,
                    'event_id' => $event_id,
                    'step' => self::find()->where(['company_id' => $company->id])->max('step') + 1,
                    'visit' => $visit,
                    'created_at' => time(),
                ]);
                try {
                    $event->save(false);
                } catch(Exception $e) {}
            }
        }
    }
}
