<?php

namespace common\models\company;

use common\models\Company;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "company_site".
 *
 * @property integer $id
 * @property integer $company_id
 * @property string $url
 * @property integer $created_at
 *
 * @property Company $company
 */
class CompanySite extends \yii\db\ActiveRecord
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_site';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['url'], 'required'],
            [['url'], 'string', 'max' => 255],
            [['url'], 'url', 'defaultScheme' => 'http'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'url' => 'Адрес сайта',
            'created_at' => 'Добавлен',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
}
