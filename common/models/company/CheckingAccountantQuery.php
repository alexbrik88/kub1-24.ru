<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 01.08.2016
 * Time: 4:29
 */

namespace common\models\company;


use yii\db\ActiveQuery;

/**
 * Class CheckingAccountantQuery
 * @package common\models\company
 */
class CheckingAccountantQuery extends ActiveQuery
{
    /**
     * @param $companyId
     * @return $this
     */
    public function byCompany($companyId)
    {
        return $this->andWhere([
            'company_id' => $companyId,
        ]);
    }

    /**
     * @param int $type
     * @return $this
     */
    public function byType($type = CheckingAccountant::TYPE_MAIN)
    {
        return $this->andWhere([
            'type' => $type,
        ]);
    }

    /**
     * @param int $type
     * @return $this
     */
    public function active($active = true)
    {
        return $this->andWhere([
            'type' => $active ? [
                CheckingAccountant::TYPE_MAIN,
                CheckingAccountant::TYPE_ADDITIONAL
            ] : CheckingAccountant::TYPE_CLOSED,
        ]);
    }
}
