<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 2.10.15
 * Time: 14.55
 * Email: t.kanstantsin@gmail.com
 */

namespace common\models\company;


use yii\db\ActiveQuery;

/**
 * Class CompanyTypeQuery
 * @package common\models\company
 */
class CompanyTypeQuery extends ActiveQuery
{

    /**
     * @param bool|true $inCompany
     * @return $this
     */
    public function inCompany($inCompany = true)
    {
        return $this->andWhere([
            'in_company' => $inCompany,
        ]);
    }

    /**
     * @param bool|true $inContractor
     * @return $this
     */
    public function inContractor($inContractor = true)
    {
        return $this->andWhere([
            'in_contractor' => $inContractor,
        ]);
    }

    /**
     * @param bool|true $inContractor
     * @return $this
     */
    public function likeIp($likeIp = true)
    {
        return $this->andWhere([
            'like_ip' => $likeIp,
        ]);
    }
}