<?php

namespace common\models\company;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Class CompanyIndustryQuery
 *
 * @package common\models\document
 */
class CompanyIndustryQuery extends ActiveQuery
{
    use \common\components\TableAliasTrait;

    /**
     * @param $companyId
     *
     * @return $this
     */
    public function active()
    {
        return $this->andWhere([
            $this->getTableAlias().'.status_id' => CompanyIndustry::STATUS_ACTIVE,
        ]);
    }
}
