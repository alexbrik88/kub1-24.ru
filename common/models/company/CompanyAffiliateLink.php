<?php

namespace common\models\company;


use common\components\helpers\ArrayHelper;
use common\models\Company;
use common\models\service\ServiceModule;
use Yii;
use yii\base\Exception;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * Class CompanyAffiliateLink
 * @package backend\modules\company\models
 *
 * @property integer $company_id
 * @property string $name
 * @property string $product
 * @property string $link
 * @property integer $custom_link_id
 * @property integer $service_module_id
 * @property integer $module_id
 * @property integer $percent_reward
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Company $company
 * @property  ServiceModule $serviceModule
 */

class CompanyAffiliateLink extends ActiveRecord
{
    public $countAffiliateCompanies;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{company_affiliate_link}}';
    }

    public function rules()
    {
        return [
            ['name', 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'timestamp' => [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'name' => ' Название',
            'product' => 'Продукт',
            'link' => 'Ссылка',
            'service_module_id' => 'Продукт',
            'created_at' => 'Дата создания',
            'updated_at' => 'Последнее изменение',
            'module_id' => 'ID модуля',
            'countAffiliateCompanies' => 'Количество регистраций',

        ];
    }

    /**
     * @param $company_id
     * @return bool
     * @throws Exception
     */
    public static function generateAffiliateLinks($company_id)
    {
        $companyAffiliateLinks = self::find()->where(['company_id' => $company_id])->exists();
        if ($companyAffiliateLinks) {
            return false;
        }

        $company = Company::find()
            ->andWhere(['id' => $company_id])
            ->andWhere(['is_partner' => true])
            ->one();

        if ($company) {
            while (!$company->affiliate_link) {
                $affiliateLink = Yii::$app->security->generateRandomString(15);
                if (!Company::find()->andWhere(['affiliate_link' => $affiliateLink])->exists()) {
                    $company->updateAttributes([
                        'affiliate_link' => $affiliateLink,
                        'affiliate_link_created_at' => time()
                    ]);
                }
            }

            $modules = ServiceModule::$serviceModuleLabel;

            foreach ($modules as $key => $value) {
                $moduleIndex = array_search($key, ServiceModule::$serviceModuleArray);
                $affiliateLink = $company->affiliate_link . '.' . (false !== $moduleIndex ? $moduleIndex : "");

                $link = new static;
                $link->percent_reward = 20;
                $link->company_id = $company->id;
                $link->product = $value;
                $link->name = "";
                $link->service_module_id = $key;
                $link->module_id = (false !== $moduleIndex ? $moduleIndex : null);
                $link->link = $affiliateLink;
                if (CompanyAffiliateLink::find()->andWhere(['link' => $affiliateLink])->exists()
                    || !$link->save(true))
                {
                    Yii::$app->session->setFlash('error', 'Создание партнерских ссылок завершилось неудачей.');
                };
            }

            return true;
        }

        return false;
    }

    /**
     * @param $company_id
     * @return bool
     * @throws Exception
     */
    public static function generateCustomAffiliateLink($company_id, $params)
    {
        $company = Company::find()
            ->andWhere(['id' => $company_id])
            ->andWhere(['is_partner' => true])
            ->one();

        if ($company) {
            $moduleIndex = array_search($params['service_module_id'], ServiceModule::$serviceModuleArray);
            do {
                $customLink = Yii::$app->security->generateRandomString(5);
            } while (CompanyAffiliateLink::find()->andWhere(['custom_link_id' => $customLink])->exists());

            $link = new static;
            $link->percent_reward = 20;
            $link->company_id = $company->id;
            $link->product = ServiceModule::$serviceModuleLabel[$params['service_module_id']];
            $link->name = $params['name'];
            $link->service_module_id = $params['service_module_id'];
            $link->module_id = (false !== $moduleIndex ? $moduleIndex : null);
            $link->link = $company->affiliate_link . '.' . $customLink;
            $link->custom_link_id = $customLink;
            if (!$link->save(true)) {
                Yii::warning($link->errors);
            };

            return true;
        }

        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServiceModule()
    {
        return $this->hasOne(ServiceModule::className(), ['id' => 'service_module_id']);
    }

}