<?php

namespace common\models\company;

use yii\db\ActiveQuery;

/**
 * Class ForeignCurrencyAccountQuery
 * @package common\models\company
 */
class ForeignCurrencyAccountQuery extends ActiveQuery
{
    /**
     * @param $companyId
     * @return $this
     */
    public function byCompany($companyId)
    {
        return $this->andWhere([
            'company_id' => $companyId,
        ]);
    }

    /**
     * @param int $type
     * @return $this
     */
    public function byType($type = ForeignCurrencyAccount::TYPE_MAIN)
    {
        return $this->andWhere([
            'type' => $type,
        ]);
    }

    /**
     * @param bool $active
     * @return ForeignCurrencyAccountQuery
     */
    public function active($active = true)
    {
        return $this->andWhere([
            'type' => $active ? [
                ForeignCurrencyAccount::TYPE_MAIN,
                ForeignCurrencyAccount::TYPE_ADDITIONAL
            ] : ForeignCurrencyAccount::TYPE_CLOSED,
        ]);
    }
}
