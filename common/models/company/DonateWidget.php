<?php

namespace common\models\company;

use common\models\Company;
use common\models\document\Order;
use common\models\employee\Employee;
use common\models\product\Product;
use Yii;
use yii\helpers\Url;

/**
 * This is the model class for table "donate_widget".
 *
 * @property string $id
 * @property integer $status
 * @property integer $company_id
 * @property integer $product_id
 * @property string $note
 * @property string $return_url
 * @property integer $is_autocomplete
 * @property integer $is_additional_number_before
 * @property string $additional_number
 * @property integer $send_with_stamp
 * @property integer $send_result
 * @property string $result_url
 * @property integer $is_bik_autocomplete
 * @property string $email_comment
 * @property string $invoice_comment
 * @property integer $notify_to_email
 * @property integer $need_contract
 * @property integer $need_contract_sum
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $created_by
 * @property integer $updated_by
 *
 * @property Company $company
 * @property Employee $createdBy
 * @property Product $product
 * @property Employee $updatedBy
 */
class DonateWidget extends \yii\db\ActiveRecord
{
    const ACTIVE = 10;
    const INACTIVE = 1;
    const DELETED = 0;

    public $productId = [];

    public static $statusAll = [
        self::ACTIVE => 'Активна',
        self::INACTIVE => 'Не активна',
        self::DELETED => 'Удалена',
    ];

    public static $statusAvailable = [
        self::ACTIVE => 'Активна',
        self::INACTIVE => 'Не активна',
    ];

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\behaviors\TimestampBehavior',
            ],
            [
                'class' => 'yii\behaviors\BlameableBehavior',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'donate_widget';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['status', 'productId', 'return_url'], 'required'],
            [['status', 'is_autocomplete', 'is_additional_number_before', 'send_with_stamp', 'send_result', 'is_bik_autocomplete', 'notify_to_email', 'need_contract', 'need_contract_sum'], 'integer'],
            [['is_autocomplete', 'is_additional_number_before', 'send_with_stamp', 'send_result', 'is_bik_autocomplete', 'notify_to_email', 'need_contract'], 'boolean'],
            [['note', 'return_url', 'result_url'], 'string'],
            [['additional_number'], 'string', 'max' => 45],
            [['email_comment', 'invoice_comment'], 'string', 'max' => 255],
            [['result_url'], 'required', 'when' => function ($model) {
                return (boolean) $model->send_result;
            }],
            [['need_contract_sum'], 'required', 'when' => function ($model) {
                return (boolean) $model->need_contract;
            }],
            [
                ['productId'], 'each', 'rule' => [
                    'exist',
                    'targetClass' => Product::class,
                    'targetAttribute' => 'id',
                    'filter' => [
                        'company_id' => $this->company_id,
                        'is_deleted' => false,
                        'not_for_sale' => false,
                    ]
                ],
            ],
            ['productId', function ($attribute, $params) {
                if (is_array($this->$attribute) && count($this->$attribute) > 1) {
                    $this->addError($attribute, 'Допускается только одна позиция.');
                }
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Компания',
            'status' => 'Статус',
            'note' => 'Примечание',
            'outUrl' => 'Адрес ссылки',
            'return_url' => 'URL возврата',
            'send_result' => 'Отправлять результат',
            'result_url' => 'URL результата',
            'created_at' => 'Создан',
            'updated_at' => 'Изменен',
            'created_by' => 'Создал',
            'updated_by' => 'Изменил',
            'productId' => 'Товар/услуга',
            'is_autocomplete' => 'Автозаполнение по ИНН',
            'is_bik_autocomplete' => 'Автозаполнение по БИК',
            'additional_number' => 'Доп номер',
            'is_additional_number_before' => 'К номеру счета Доп номер',
            'send_with_stamp' => 'Добавить в счет печать и подпись',
            'invoice_comment' => 'Добавить комментарий в счете',
            'email_comment' => 'Добавить комментарий к письму со счетом',
            'notify_to_email' => 'Получить уведомление на почту о новом счете',
            'need_contract' => 'Нужен договор',
            'need_contract_sum' => 'если сумма больше',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(Employee::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id'])->onCondition([
            'is_deleted' => false,
            'not_for_sale' => false,
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(Employee::className(), ['id' => 'updated_by']);
    }

    /**
     * @return Product[]
     */
    public function getSelectedProducts()
    {
        return Product::find()->andWhere([
            'id' => $this->productId,
            'company_id' => $this->company_id,
            'is_deleted' => false,
            'not_for_sale' => false,
        ])->all();
    }

    /**
     * @param  boolean $insert
     * @return boolean
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                do {
                    $this->id = uniqid();
                } while (self::find()->where(['id' => $this->id])->exists());
            }

            $this->product_id = reset($this->productId);

            return true;
        }

        return false;
    }

    /**
     * @return Product[]
     */
    public function afterFind()
    {
        $this->productId = [$this->product_id];
        parent::afterFind();
    }

    /**
     * @return Product[]
     */
    public function getOutUrl()
    {
        return Url::to(['/donate/default/index', 'id' => $this->id], true);
    }

    /**
     * @return Product[]
     */
    public function getStatusValue()
    {
        return isset(self::$statusAll[$this->status]) ? self::$statusAll[$this->status] : '---';
    }
}
