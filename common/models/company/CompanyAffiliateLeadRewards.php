<?php


namespace common\models\company;

use common\components\helpers\ArrayHelper;
use common\models\Company;
use common\models\document\Invoice;
use common\models\service\Payment;
use Exception;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * Class CompanyAffiliateLeadRewards
 * @package common\models\company
 *
 * @property integer $bottom_line;
 * @property integer $top_line;
 * @property integer $rewards;
 * @property integer $is_current;
 *
 * @property Company $company
 */
class CompanyAffiliateLeadRewards extends ActiveRecord
{
    public static $rangeRevardsArray = [
        [
            'bottom_line' => null,
            'top_line' => 650000,
            'rewards' => 20,
        ],
        [
            'bottom_line' => 650001,
            'top_line' => 2500000,
            'rewards' => 25,
        ],
        [
            'bottom_line' => 2500001,
            'top_line' => null,
            'rewards' => 30,
        ]
    ];

    public function rules()
    {
        return [
            [['top_line', 'bottom_line', 'rewards'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'company_id' => 'Компания',
            'bottom_line' => 'Нижняя граница',
            'top_line' => 'Верхняя граница',
            'range_rewards' => 'Общая сумма оплат (от начала сотрудничества)',
            'rewards' => 'Размер партнерского вознаграждения',
            'is_current' => 'Текущий уровень вознаграждения',
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ]);
    }

    public function beforeSave($insert)
    {
        $this->top_line = (bool)$this->top_line ? (int)$this->top_line : null;
        $this->bottom_line = (bool)$this->bottom_line ? (int)$this->bottom_line : null;

        return parent::beforeSave($insert);
    }

    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    public static function generateAffiliateRewards($company_id)
    {
        if (CompanyAffiliateLeadRewards::findOne(['company_id' => $company_id])) {
            return false;
        }

        $totalAffiliatePayment = Invoice::find()
                ->joinWith('payment')
                ->andWhere([
                    Invoice::tableName() . '.company_id' => $company_id,
                    Payment::tableName().'.is_confirmed' => 1,
                    Payment::tableName().'.payment_for' => Payment::FOR_SUBSCRIBE,
                ])
                ->sum('total_amount_with_nds') / 100;

        foreach (self::$rangeRevardsArray as $rewards) {
            $leadRewards = new CompanyAffiliateLeadRewards([
                'company_id' => $company_id,
                'bottom_line' => $rewards['bottom_line'],
                'top_line' => $rewards['top_line'],
                'rewards' => $rewards['rewards'],
            ]);
            $leadRewards->is_current = $leadRewards->bottom_line < $totalAffiliatePayment && ($leadRewards->top_line && $leadRewards->top_line > $totalAffiliatePayment);
            $leadRewards->save();
        }

    }

    public static function getCurrentRewardsPercent($company_id)
    {
        self::setIsCurrentRewardsLevel($company_id);
        /** @var CompanyAffiliateLeadRewards $leadReward */
        $leadReward = static::find()
            ->andWhere(['and',
                ['company_id' => $company_id],
                ['is_current' => 1],
            ])
            ->one();

        if (!$leadReward) {
            return 0;
        }

        return $leadReward->rewards;
    }

    /**
     * @param $company_id
     * @return bool
     */
    public static function setIsCurrentRewardsLevel($company_id)
    {
        $totalAffiliatePayment = Invoice::find()
                ->joinWith('payment')
                ->andWhere([
                    Invoice::tableName() . '.company_id' => $company_id,
                    Payment::tableName().'.is_confirmed' => 1,
                    Payment::tableName().'.payment_for' => Payment::FOR_SUBSCRIBE,
                ])
                ->sum('total_amount_with_nds') / 100;

        $leadRewards = static::findAll(['company_id' => $company_id]);

        if (!$totalAffiliatePayment || !$leadRewards) {
            return false;
        }

        try {
            foreach ($leadRewards as $leadReward) {
                $leadReward->is_current = $leadReward->bottom_line <= $totalAffiliatePayment && $totalAffiliatePayment <= $leadReward->top_line
                    || (empty($leadReward->top_line) && $leadReward->bottom_line <= $totalAffiliatePayment)
                    || (empty($leadReward->bottom_line) && $leadReward->top_line >= $totalAffiliatePayment);
                $leadReward->save();
            }
        } catch (Exception $e) {
            return false;
        }

        return true;
    }

}