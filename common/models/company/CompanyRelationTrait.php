<?php

namespace common\models\company;

use common\models\Company;
use yii\db\ActiveQuery;

/**
 * @property-read Company $company
 */
trait CompanyRelationTrait
{
    /**
     * @var string[]
     */
    protected array $companyLink = ['id' => 'company_id'];

    /**
     * @return Company
     */
    public function getCompany(): Company
    {
        return $this->__get('companyRelation');
    }

    /**
     * @return ActiveQuery
     */
    public function getCompanyRelation(): ActiveQuery
    {
        return $this->hasOne(Company::class, $this->companyLink);
    }

    /**
     * @return bool
     */
    public function hasCompany(): bool
    {
        if ($this->__get('companyRelation')) {
            return true;
        }

        return false;
    }
}
