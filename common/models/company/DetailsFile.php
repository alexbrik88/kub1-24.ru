<?php

namespace common\models\company;

use common\models\Company;
use common\models\employee\Employee;
use frontend\models\Documents;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;

/**
 * This is the model class for table "company_details_file".
 *
 * @property int $id
 * @property int $company_id
 * @property int $type_id
 * @property int $status_id
 * @property string $name
 * @property string $ext
 * @property int $size
 * @property int $created_by
 * @property int $created_at
 *
 * @property Company $company
 * @property CompanyDetailsFileStatus $status
 * @property CompanyDetailsFileType $type
 * @property Employee $createdBy
 */
class DetailsFile extends \yii\db\ActiveRecord
{
    /**
     * @property
     */
    public $file;

    /**
     * @var integer
     */
    public static $maxFileSize = 3 * 1024 * 1024;

    /**
     * @var array
     */
    public static $applyTypeList = [
        DetailsFileType::TYPE_PRINT,
        DetailsFileType::TYPE_CHIEF_SIGNATURE,
        DetailsFileType::TYPE_CHIEF_ACCOUNTANT_SIGNATURE,
    ];

    /**
     * @var array
     */
    public static $applyDocList = [
        Documents::DOCUMENT_ACT,
        Documents::DOCUMENT_INVOICE,
        Documents::DOCUMENT_INVOICE_FACTURE,
        Documents::DOCUMENT_PACKING_LIST,
        Documents::DOCUMENT_UPD,
        Documents::DOCUMENT_AGREEMENT,
        Documents::DOCUMENT_PROXY,
        Documents::DOCUMENT_AGENT_REPORT,
    ];
    public static $hasToMany = [
        Documents::DOCUMENT_ACT,
        Documents::DOCUMENT_INVOICE_FACTURE,
        Documents::DOCUMENT_UPD,
    ];
    public static $hasCompanyId = [
        Documents::DOCUMENT_INVOICE,
        Documents::DOCUMENT_AGREEMENT,
        Documents::DOCUMENT_AGENT_REPORT,
    ];

    public function behaviors()
    {
        return [
            'created' => [
                'class' => 'yii\behaviors\TimestampBehavior',
                'updatedAtAttribute' => false,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company_details_file';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['file'], 'required', 'on' => 'insert'],
            [['file'], 'file', 'maxSize' => self::$maxFileSize],
            [['file'], 'image'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'type_id' => 'Type ID',
            'status_id' => 'Status ID',
            'name' => 'Name',
            'ext' => 'Ext',
            'size' => 'Size',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(DetailsFileStatus::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(DetailsFileType::className(), ['id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(Employee::className(), ['id' => 'created_by']);
    }

    /**
     * @return string
     */
    public function getUploadPath()
    {
        if (isset($this->company_id, $this->type_id, $this->id)) {
            return Company::fileUploadPath($this->company_id) .
                DIRECTORY_SEPARATOR . DetailsFileType::$dirNameList[$this->type_id] .
                DIRECTORY_SEPARATOR . $this->id;
        }

        throw new Exception('Model attributes error');
    }

    /**
     * @return string
     */
    public function getFilePath()
    {
        return $this->getUploadPath() . DIRECTORY_SEPARATOR . $this->name;
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                if (empty($this->created_by) &&
                    ($user = ArrayHelper::getValue(Yii::$app, 'user.identity')) instanceof Employee
                ) {
                    $this->created_by = $user->id;
                }
                $ext = $this->file->extension;
                $this->name = uniqid() . ($ext ? '.' . $ext : '');
                $this->ext = $ext;
                $this->size = $this->file->size;

            }
            if ($this->status_id == DetailsFileStatus::STATUS_ACTIVE) {
                self::updateAll(['status_id' => null], [
                    'company_id' => $this->company_id,
                    'type_id' => $this->type_id,
                    'status_id' => DetailsFileStatus::STATUS_ACTIVE
                ]);
            } elseif ($this->status_id == DetailsFileStatus::STATUS_TEMPORARY) {
                $modelArray = self::find()->where([
                    'company_id' => $this->company_id,
                    'type_id' => $this->type_id,
                    'status_id' => DetailsFileStatus::STATUS_TEMPORARY
                ])->all();
                foreach ($modelArray as $model) {
                    $model->delete();
                }
            }

            return true;
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            $uploadPath = $this->getUploadPath();

            if (!file_exists($uploadPath)) {
                FileHelper::createDirectory($uploadPath);
            }

            $filePath = $uploadPath . DIRECTORY_SEPARATOR . $this->name;

            if (!$this->file->saveAs($filePath)) {
                throw new Exception('File saving error');
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();
        $file = $this->getFilePath();
        if (is_file($file)) {
            unlink($file);
        }
    }

    /**
     * @inheritdoc
     */
    public function applyForAll()
    {
        if (in_array($this->type_id, self::$applyTypeList)) {
            $fieldName = DetailsFileType::$fieldNameList[$this->type_id];

            foreach (self::$applyDocList as $docTypeId) {
                $className = Documents::getBaseModel($docTypeId);
                $tableName = $className::tableName();
                if (in_array($docTypeId, self::$hasCompanyId)) {
                    Yii::$app->db->createCommand()->update("{{%{$tableName}}}", [
                        $fieldName => $this->id,
                    ], [
                        'company_id' => $this->company_id,
                    ])->execute();
                } elseif (in_array($docTypeId, self::$hasToMany)) {
                    Yii::$app->db->createCommand("
                        UPDATE {{%{$tableName}}} AS {{document}}
                        RIGHT JOIN {{%invoice_{$tableName}}} AS {{link}}
                             ON {{link}}.[[{$tableName}_id]] = {{document}}.[[id]]
                        RIGHT JOIN {{%invoice}} ON {{link}}.[[invoice_id]] = {{%invoice}}.[[id]]
                        SET {{document}}.[[{$fieldName}]] = :id
                        WHERE {{%invoice}}.[[company_id]] = :company_id
                    ", [
                        ':id' => $this->id,
                        ':company_id' => $this->company_id,
                    ])->execute();
                } else {
                    Yii::$app->db->createCommand("
                        UPDATE {{%{$tableName}}} AS {{document}}
                        RIGHT JOIN {{%invoice}} ON {{document}}.[[invoice_id]] = {{%invoice}}.[[id]]
                        SET {{document}}.[[{$fieldName}]] = :id
                        WHERE {{%invoice}}.[[company_id]] = :company_id
                    ", [
                        ':id' => $this->id,
                        ':company_id' => $this->company_id,
                    ])->execute();
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function deleteFromAll()
    {
        if (in_array($this->type_id, self::$applyTypeList)) {
            $fieldName = DetailsFileType::$fieldNameList[$this->type_id];

            foreach (self::$applyDocList as $docTypeId) {
                $className = Documents::getBaseModel($docTypeId);
                $tableName = $className::tableName();
                if (in_array($docTypeId, self::$hasCompanyId)) {
                    Yii::$app->db->createCommand()->update("{{%{$tableName}}}", [
                        $fieldName => null,
                    ], [
                        $fieldName => $this->id,
                        'company_id' => $this->company_id,
                    ])->execute();
                } elseif (in_array($docTypeId, self::$hasToMany)) {
                    Yii::$app->db->createCommand("
                        UPDATE {{%{$tableName}}} AS {{document}}
                        LEFT JOIN {{%invoice_{$tableName}}} AS {{link}}
                             ON {{link}}.[[{$tableName}_id]] = {{document}}.[[id]]
                        LEFT JOIN {{%invoice}} ON {{link}}.[[invoice_id]] = {{%invoice}}.[[id]]
                        SET {{document}}.[[{$fieldName}]] = NULL
                        WHERE {{%invoice}}.[[company_id]] = :company_id
                        AND {{document}}.[[{$fieldName}]] = :id
                    ", [
                        ':id' => $this->id,
                        ':company_id' => $this->company_id,
                    ])->execute();
                } else {
                    Yii::$app->db->createCommand("
                        UPDATE {{%{$tableName}}} AS {{document}}
                        LEFT JOIN {{%invoice}} ON {{document}}.[[invoice_id]] = {{%invoice}}.[[id]]
                        SET {{document}}.[[{$fieldName}]] = NULL
                        WHERE {{%invoice}}.[[company_id]] = :company_id
                        AND {{document}}.[[{$fieldName}]] = :id
                    ", [
                        ':id' => $this->id,
                        ':company_id' => $this->company_id,
                    ])->execute();
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public static function deleteFromAllDocuments($companyId, $typeId)
    {
        if (in_array($typeId, self::$applyTypeList)) {
            $fieldName = DetailsFileType::$fieldNameList[$typeId];
            foreach (self::$applyDocList as $docTypeId) {
                $className = Documents::getBaseModel($docTypeId);
                $tableName = $className::tableName();
                if (in_array($docTypeId, self::$hasCompanyId)) {
                    Yii::$app->db->createCommand("
                        UPDATE {{%{$tableName}}}
                        SET {{%{$tableName}}}.[[{$fieldName}]] = NULL
                        WHERE {{%{$tableName}}}.[[company_id]] = :company_id
                    ", [
                        ':company_id' => $companyId,
                    ])->execute();
                } elseif (in_array($docTypeId, self::$hasToMany)) {
                    Yii::$app->db->createCommand("
                        UPDATE {{%{$tableName}}} AS {{document}}
                        LEFT JOIN {{%invoice_{$tableName}}} AS {{link}}
                             ON {{link}}.[[{$tableName}_id]] = {{document}}.[[id]]
                        LEFT JOIN {{%invoice}} ON {{link}}.[[invoice_id]] = {{%invoice}}.[[id]]
                        SET {{document}}.[[{$fieldName}]] = NULL
                        WHERE {{%invoice}}.[[company_id]] = :company_id
                    ", [
                        ':company_id' => $companyId,
                    ])->execute();
                } else {
                    Yii::$app->db->createCommand("
                        UPDATE {{%{$tableName}}} AS {{document}}
                        LEFT JOIN {{%invoice}} ON {{document}}.[[invoice_id]] = {{%invoice}}.[[id]]
                        SET {{document}}.[[{$fieldName}]] = NULL
                        WHERE {{%invoice}}.[[company_id]] = :company_id
                    ", [
                        ':company_id' => $companyId,
                    ])->execute();
                }
            }
        }
    }

    /**
     * @param  integer $company_id
     * @param  string $type
     * @param  boolean $tmp
     * @return DetailsFile
     */
    public static function getInstanceFor($companyId, $type, $tmp = false)
    {
        if ($typeId = DetailsFileType::findTypeId($type)) {
            return static::findOne([
                'company_id' => $companyId,
                'type_id' => $typeId,
                'status_id' => $tmp ? DetailsFileStatus::STATUS_TEMPORARY : DetailsFileStatus::STATUS_ACTIVE,
            ]);
        }

        return null;
    }
}
