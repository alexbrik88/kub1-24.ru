<?php

namespace common\models\company;

use Yii;
use common\models\Company;

/**
 * This is the model class for table "company_prepayment_wallets".
 *
 * @property int $id
 * @property int $company_id
 * @property int $bank
 * @property int $order
 * @property int $emoney
 *
 * @property Company $company
 */
class CompanyPrepaymentWallets extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company_prepayment_wallets';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id'], 'required'],
            [['company_id', 'bank', 'order', 'emoney'], 'integer'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'bank' => 'Bank',
            'order' => 'Order',
            'emoney' => 'Emoney',
        ];
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
}
