<?php

namespace common\models\company;

use Yii;

/**
 * This is the model class for table "company_info_site".
 *
 * @property int $id
 * @property string $name
 *
 * @property Company[] $companies
 */
class CompanyInfoSite extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company_info_site';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Company::className(), ['info_site_id' => 'id']);
    }
}
