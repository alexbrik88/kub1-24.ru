<?php


namespace common\models\company;


use common\models\Company;
use frontend\components\StatisticPeriod;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use function Matrix\identity;

/**
 * Class CompanyAffiliateLeadRewardsSearch
 * @package common\models\company
 */
class CompanyAffiliateLeadRewardsSearch extends CompanyAffiliateLeadRewards
{
    /** @var $search */
    public $search;

    /** @var $company_id */
    public $company_id;

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['search'], 'safe'],
        ];
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);

        $query = $this->getBaseQuery();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'id',
                ],
                'defaultOrder' => [
                    'id' => SORT_ASC,
                ],
            ],
        ]);

        return $dataProvider;
    }

    /**
     * @return ActiveQuery
     */
    public function getBaseQuery()
    {
        $query = CompanyAffiliateLeadRewards::find()
            ->andWhere(['company_id' => $this->company_id]);

        return $query;
    }

}