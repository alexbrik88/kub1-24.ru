<?php
namespace common\models\company;

use common\components\helpers\ArrayHelper;
use common\models\Company;
use common\models\document\Invoice;
use common\models\service\Payment;
use common\models\service\ServiceModule;
use common\models\service\SubscribeTariff;
use frontend\components\StatisticPeriod;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\Expression;

/**
 * Class CompanyPartnerSearch
 * @package backend\modules\company\models
 */
class CompanyAffiliateSearch extends Company
{
    /** @var $search */
    public $search;

    public $company_id;

    public $status;
    public $activeTariff;
    public $registrationPageType;
    public $affiliateLink;

    public $paidInvoicesCount;
    public $paidInvoicesSum;

    const SUBSCRIBE_AFFILIATE = 1;
    const SUBSCRIBE_AFFILIATE_GET_INVOICE = 2;
    const SUBSCRIBE_AFFILIATE_INVOICE_PAYED = 3;
    const SUBSCRIBE_AFFILIATE_PAYED = 4;

    const COMPANY_STATUS_ACTIVE = 'Активна';
    const COMPANY_STATUS_BLOCKED = 'Заблокирована';

    /** @var array $payedStatusArray */
    public static $payedStatusArray = [
        self::SUBSCRIBE_AFFILIATE => 'Количество регистраций',
        self::SUBSCRIBE_AFFILIATE_GET_INVOICE => 'Количество выставивших счет',
        self::SUBSCRIBE_AFFILIATE_INVOICE_PAYED => 'Количество оплативших',
        self::SUBSCRIBE_AFFILIATE_PAYED => 'Всего оплат',
    ];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['search', 'company_id'], 'safe'],
            [['status', 'activeTariff', 'registrationPageType'], 'safe'],
            [['product'], 'safe'],
            [['paidInvoicesCount', 'paidInvoicesCount'], 'safe'],
        ];
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);

        $this->company_id = isset($params['company_id']) ? $params['company_id'] : 0;

        $query = $this->getBaseQuery();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'attributes' => [
                    'registration_date' => [
                        'asc' => ['created_at' => SORT_ASC],
                        'desc' => ['created_at' => SORT_DESC],
                        'default' => SORT_DESC
                    ],
                    'name_short',
                    'paidCount',
                    'paidSum',
                    'paidInvoicesCount',
                    'paidInvoicesSum',
                ],
                'defaultOrder' => [
                    'registration_date' => SORT_DESC,
                ],
            ],
        ]);

        return $dataProvider;
    }

    /**
     * @return ActiveQuery
     */
    public function getBaseQuery()
    {
        $dateRange = StatisticPeriod::getSessionPeriod();

        $payedInvoices = Company::find()
            ->select([
                Company::tableName() . '.id as company_id',
                'COUNT('.Payment::tableName() . '.sum) as count',
                'SUM('.Payment::tableName() . '.sum) as sum',
            ])
            ->joinWith('payments')
            ->andWhere(['is_confirmed' => true])
            ->andWhere(['between', 'DATE(FROM_UNIXTIME(' . Company::tableName() . '.created_at))', $dateRange['from'], $dateRange['to']])
            ->groupBy('`company_id`');

        $invoices = Company::find()
            ->select([
                Company::tableName() . '.id as company_id',
                'COUNT('.Payment::tableName() . '.sum) as count',
                'SUM('.Payment::tableName() . '.sum) as sum',
            ])
            ->joinWith('payments')
            ->andWhere(['between', 'DATE(FROM_UNIXTIME(' . Company::tableName() . '.created_at))', $dateRange['from'], $dateRange['to']])
            ->groupBy('`company_id`');

        $query = Company::find()
            ->addSelect([
                Company::tableName() . '.*',
                Payment::tableName() . '.sum',
                Company::tableName() . '.invited_by_product_id as product',
                'invoices.count paidCount',
                'invoices.sum as paidSum',
                'paidInvoices.count as paidInvoicesCount',
                'paidInvoices.sum as paidInvoicesSum',
            ])
            ->joinWith('payments')
            ->leftJoin(['paidInvoices' => $payedInvoices], Company::tableName() . '.id = paidInvoices.company_id')
            ->leftJoin(['invoices' => $invoices], Company::tableName() . '.id = invoices.company_id')
            ->andWhere([Company::tableName() . '.invited_by_company_id' => $this->company_id])
            ->andWhere(['or',
                [Payment::tableName() . '.payment_for' => 'subscribe'],
                ['IS', Payment::tableName() . '.payment_for', new Expression('NULL')],
            ])
            ->andWhere(['between', 'DATE(FROM_UNIXTIME(' . Company::tableName() . '.created_at))', $dateRange['from'], $dateRange['to']])
            ->groupBy('`company`.`id`');

        if ($this->registrationPageType) {
            $query->andFilterWhere([Company::tableName() . '.registration_page_type_id' => $this->registrationPageType]);
        }

        if ($this->status) {
            $query->andFilterWhere([Company::tableName() . '.blocked' => $this->status]);
        }

        if ($this->activeTariff) {
            $query
                ->andFilterWhere([Payment::tableName() . '.tariff_id' => $this->activeTariff]);
        }

        if ($this->product) {
            $query->andFilterWhere([Company::tableName() . '.invited_by_product_id' => $this->product]);
        }

        $query->andFilterWhere(['OR',
            ['like', Company::tableName() . '.inn', $this->search],
            ['like', Company::tableName() . '.id', $this->search],
            ['like', Company::tableName() . '.name_short', $this->search],
            ['like', Company::tableName() . '.name_full', $this->search],
            ['like', Company::tableName() . '.email', $this->search],
        ]);

        return $query;
    }

    /**
     * @return array
     */
    public function getTariffFilter()
    {
        $result[null] = 'Все';

        $tariff = $this->getBaseQuery()
            ->select(Payment::tableName() . '.tariff_id as tariff')
            ->groupBy(Payment::tableName() . '.tariff_id');

        foreach ($tariff->column() as $tariffID) {
            $tariff = SubscribeTariff::findOne($tariffID);
            if ($tariff) {
                $result[$tariffID] = $tariff->tariffGroup->name .' '. $tariff->getTariffName();
            }
        }

        return $result;
    }

    public function getAffiliateProductFilter()
    {
        $result[null] = 'Все';

        foreach (ServiceModule::$serviceModuleArray as $key => $value) {
            $result[$key] = ServiceModule::$serviceModuleLabel[$value];
        }

        return $result;

    }

}