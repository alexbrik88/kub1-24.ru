<?php

namespace common\models;

use common\models\document\InvoiceExpenditureItem;
use Yii;

/**
 * This is the model class for table "tax_kbk".
 *
 * @property string $id
 * @property integer $expenditure_item_id
 *
 * @property InvoiceExpenditureItem $expenditureItem
 */
class TaxKbk extends \yii\db\ActiveRecord
{
    const DEFAULT_ITEM = InvoiceExpenditureItem::ITEM_TAX;

    protected static $_itemToKbk;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tax_kbk';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'expenditure_item_id'], 'required'],
            [['expenditure_item_id'], 'integer'],
            [['id'], 'string', 'max' => 20],
            [['expenditure_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => InvoiceExpenditureItem::className(), 'targetAttribute' => ['expenditure_item_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'expenditure_item_id' => 'Expenditure Item ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpenditureItem()
    {
        return $this->hasOne(InvoiceExpenditureItem::className(), ['id' => 'expenditure_item_id']);
    }

    /**
     * InvoiceExpenditureItem::id by TaxKbk::id
     * @param  string $kbkId
     * @return integer
     */
    public static function itemId($kbkId)
    {
        return ($kbk = self::findOne($kbkId)) ? $kbk->expenditure_item_id : self::DEFAULT_ITEM;
    }

    /**
     * @return array
     */
    public static function itemToKbk()
    {
        if (!isset(self::$_itemToKbk)) {
            self::$_itemToKbk = self::find()->select([
                'id',
                'expenditure_item_id',
            ])->indexBy('expenditure_item_id')->column();
        }

        return self::$_itemToKbk;
    }
}
