<?php

namespace common\models\amocrm;

use Yii;

/**
 * This is the model class for table "amocrm_widget_invoice".
 *
 * @property int $invoice_id
 * @property int $account_id
 * @property int $lead_id
 *
 * @property AmocrmWidget $account
 * @property Invoice $invoice
 */
class AmocrmWidgetInvoice extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'amocrm_widget_invoice';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['invoice_id', 'account_id', 'lead_id'], 'required'],
            [['invoice_id', 'account_id', 'lead_id'], 'integer'],
            [['invoice_id', 'account_id'], 'unique', 'targetAttribute' => ['invoice_id', 'account_id']],
            [['account_id'], 'exist', 'skipOnError' => true, 'targetClass' => AmocrmWidget::className(), 'targetAttribute' => ['account_id' => 'account_id']],
            [['invoice_id'], 'exist', 'skipOnError' => true, 'targetClass' => Invoice::className(), 'targetAttribute' => ['invoice_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'invoice_id' => 'Invoice ID',
            'account_id' => 'Account ID',
            'lead_id' => 'Lead ID',
        ];
    }

    /**
     * Gets query for [[Account]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(AmocrmWidget::className(), ['account_id' => 'account_id']);
    }

    /**
     * Gets query for [[Invoice]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(Invoice::className(), ['id' => 'invoice_id']);
    }
}
