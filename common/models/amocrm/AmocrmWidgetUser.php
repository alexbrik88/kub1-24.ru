<?php

namespace common\models\amocrm;

use Yii;
use common\models\EmployeeCompany;

/**
 * This is the model class for table "amocrm_widget_user".
 *
 * @property int $account_id ID аккаунта, из которого сделан запрос
 * @property int $user_id ID пользователя, из под которого сделан запрос
 * @property string $token Токен для доступа из виджета
 * @property int $employee_id ID сотрудника в КУБ
 * @property int $company_id ID компании в КУБ
 * @property string $client_uuid UUID интеграции, которая сделала запрос
 * @property string $jti UUID токена
 * @property string $iss Адрес аккаунта в amoCRM
 * @property string $aud Базовый адрес, который сформирован исходя из значения redirect_uri в интеграции
 * @property int $created_at Timestamp, когда подключен пользователь
 *
 * @property EmployeeCompany $employeeCompany
 */
class AmocrmWidgetUser extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'updatedAtAttribute' => false,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'amocrm_widget_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'account_id' => 'Account ID',
            'user_id' => 'Amo User ID',
            'token' => 'Access Token',
            'employee_id' => 'Employee ID',
            'company_id' => 'Company ID',
            'client_uuid' => 'Client Uuid',
            'jti' => 'Jti',
            'iss' => 'Iss',
            'aud' => 'Aud',
            'created_at' => 'Created At',
        ];
    }

    /**
     * Gets query for [[AmocrmWidget]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAmocrmWidget()
    {
        return $this->hasOne(AmocrmWidget::className(), ['account_id' => 'account_id']);
    }

    /**
     * Gets query for [[EmployeeCompany]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmployeeCompany()
    {
        return $this->hasOne(EmployeeCompany::className(), ['employee_id' => 'employee_id', 'company_id' => 'company_id']);
    }
}
