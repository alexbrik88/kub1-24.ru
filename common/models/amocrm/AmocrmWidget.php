<?php

namespace common\models\amocrm;

use Yii;
use common\models\EmployeeCompany;
use common\models\document\Invoice;

/**
 * This is the model class for table "amocrm_widget".
 *
 * @property int $account_id ID аккаунта, из которого сделан запрос
 * @property int $created_at Timestamp, когда подключен виджет
 * @property int $employee_id ID сотрудника в КУБ, из под которого подключен виджет
 * @property int $company_id ID компании в КУБ, для которой подключен виджет
 * @property int $user_id ID пользователя, из под которого сделан запрос
 * @property string $client_uuid UUID интеграции, которая сделала запрос
 * @property string $iss Адрес аккаунта в amoCRM
 * @property string $aud Базовый адрес, который сформирован исходя из значения redirect_uri в интеграции
 *
 * @property EmployeeCompany $employeeCompany
 */
class AmocrmWidget extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            [
                'class' => \yii\behaviors\TimestampBehavior::className(),
                'updatedAtAttribute' => false,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'amocrm_widget';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'account_id' => 'Account ID',
            'created_at' => 'Created At',
            'employee_id' => 'Employee ID',
            'company_id' => 'Company ID',
            'user_id' => 'Amo User ID',
            'client_uuid' => 'Client Uuid',
            'iss' => 'Iss',
            'aud' => 'Aud',
        ];
    }

    /**
     * Gets query for [[AmocrmWidgetInvoices]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAmocrmWidgetInvoices()
    {
        return $this->hasMany(AmocrmWidgetInvoice::className(), ['account_id' => 'account_id']);
    }

    /**
     * Gets query for [[AmocrmWidgetUsers]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAmocrmWidgetUsers()
    {
        return $this->hasMany(AmocrmWidgetUser::className(), ['account_id' => 'account_id']);
    }

    /**
     * Gets query for [[EmployeeCompany]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmployeeCompany()
    {
        return $this->hasOne(EmployeeCompany::className(), ['employee_id' => 'employee_id', 'company_id' => 'company_id']);
    }

    /**
     * Gets query for [[Invoices]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInvoices()
    {
        return $this->hasMany(Invoice::className(), ['id' => 'invoice_id'])->viaTable('amocrm_widget_invoice', ['account_id' => 'account_id']);
    }

    /**
     * @param $userId int
     * @return ?AmocrmWidgetUser
     */
    public function getWidgetUserByUserId($userId)
    {
        return $userId ? $this->getAmocrmWidgetUsers()->andWhere([
            'user_id' => $userId,
        ])->one() : null;
    }
}
