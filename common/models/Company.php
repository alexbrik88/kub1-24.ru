<?php

namespace common\models;

use backend\modules\company\models\CompanyStatistic;
use common\components\DadataClient;
use common\components\date\DateHelper;
use common\components\date\DatePickerFormatBehavior;
use common\components\getResponse\GetResponseApi;
use common\components\getResponse\GetResponseContact;
use common\components\helpers\ArrayHelper;
use common\components\helpers\ModelHelper;
use common\components\pdf\Printable;
use common\components\validators\PhoneValidator;
use common\components\TextHelper;
use common\models\bank\BankUser;
use common\models\cash\CashBankFlows;
use common\models\cash\CashBankForeignCurrencyFlows;
use common\models\cash\CashBankStatementUpload;
use common\models\cash\Cashbox;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashEmoneyForeignCurrencyFlows;
use common\models\cash\CashFlowLinkage;
use common\models\cash\CashOrderFlows;
use common\models\cash\CashOrderForeignCurrencyFlows;
use common\models\cash\Emoney;
use common\models\company\ActiveSubscribe;
use common\models\company\Activities;
use common\models\company\ApiFirstVisit;
use common\models\company\ApplicationToBank;
use common\models\company\Attendance;
use common\models\company\CompanyActivity;
use common\models\company\CompanyAffiliateLink;
use common\models\company\CompanyImageUploadLog;
use common\models\company\CompanyIndustry;
use common\models\company\CompanyInfoIndustry;
use common\models\company\CompanyInfoPlace;
use common\models\company\CompanyInfoSite;
use common\models\company\CompanyLastVisit;
use common\models\company\CompanyNotification;
use common\models\company\CompanyTaxationType;
use common\models\company\IntegrationData;
use common\models\company\MenuItem;
use common\models\company\DetailsFile;
use common\models\company\DetailsFileStatus;
use common\models\company\DetailsFileType;
use common\models\company\EmployeeCount;
use common\models\company\RegistrationPageType;
use common\models\address\AddressApartmentType;
use common\models\address\AddressHouseType;
use common\models\address\AddressHousingType;
use common\models\companyStructure\AccountingSystemType;
use common\models\companyStructure\CompanyToAcquiring;
use common\models\companyStructure\OfdType;
use common\models\companyStructure\OnlinePaymentType;
use common\models\currency\Currency;
use common\models\document\Act;
use common\models\document\ActEssence;
use common\models\document\Autoact;
use common\models\document\Autoinvoice;
use common\models\document\Autoinvoicefacture;
use common\models\document\EmailSignature;
use common\models\document\EmailTemplate;
use common\models\document\ForeignCurrencyInvoice;
use common\models\document\Invoice;
use common\models\document\InvoiceContractEssence;
use common\models\document\InvoiceContractorSignature;
use common\models\document\InvoiceEmailText;
use common\models\document\InvoiceEssence;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceFacture;
use common\models\document\InvoiceIncomeItem;
use common\models\document\NdsViewType;
use common\models\document\OrderDocument;
use common\models\document\PackingList;
use common\models\document\PaymentOrder;
use common\models\document\ScanDocument;
use common\models\document\status\AgreementStatus;
use common\models\document\status\InvoiceStatus;
use common\models\document\Upd;
use common\models\document\UserEmail;
use common\models\driver\Driver;
use common\models\EmployeeCompany;
use common\models\company\CheckingAccountant;
use common\models\company\CompanyType;
use common\models\dictionary\bik\BikDictionary;
use common\models\document\UploadedDocuments;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\file\File;
use common\models\Ifns;
use common\models\logisticsRequest\LogisticsRequest;
use common\models\logisticsRequest\LogisticsRequestAdditionalExpenses;
use common\models\logisticsRequest\LogisticsRequestContractEssence;
use common\models\ofd\OfdImportConfig;
use common\models\ofd\OfdImportGeneralCashbox;
use common\models\ofd\OfdKkt;
use common\models\ofd\OfdStore;
use common\models\ofd\OfdUser;
use common\models\out\OutInvoice;
use common\models\paymentReminder\PaymentReminderMessage;
use common\models\paymentReminder\Settings;
use common\models\product\PriceList;
use common\models\product\Product;
use common\models\product\Store;
use common\models\project\Project;
use common\models\rent\Entity;
use common\models\rent\RentAgreement;
use common\models\report\Report;
use common\models\selling\SellingSubscribe;
use common\models\service\Payment;
use common\models\service\PaymentType;
use common\models\service\PromoCode;
use common\models\service\RewardRequest;
use common\models\service\ServiceInvoiceTariff;
use common\models\service\ServiceModule;
use common\models\service\StoreOutInvoiceTariff;
use common\models\service\StoreTariff;
use common\models\service\Subscribe;
use common\models\service\SubscribeHelper;
use common\models\service\SubscribeStatus;
use common\models\service\SubscribeTariff;
use common\models\service\SubscribeTariffGroup;
use common\models\store\StoreCompanyContractor;
use common\models\store\StoreUser;
use common\tasks\CreateCompanyNotificationsFromCompany;
use common\tasks\CreatePaymentReminderMessages;
use common\models\vehicle\Vehicle;
use common\models\vehicle\VehicleType;
use common\modules\cards\models\CardAccount;
use common\modules\cards\models\CardBill;
use dateTime;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\models\log\Log;
use frontend\models\RegistrationForm;
use common\modules\marketing\models\GoogleAnalyticsProfile; // TODO: неиспользовать тут
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\models\BankingEmail;
use frontend\modules\documents\models\SpecificDocument;
use frontend\modules\export\models\export\Export;
use frontend\modules\export\models\export\ExportFiles;
use Yii;
use yii\base\Exception;
use yii\base\InvalidArgumentException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\Connection;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use yii\web\NotFoundHttpException;
use yii\web\UploadedFile;
use common\models\cash\CashFlowsBase;
use frontend\modules\analytics\models\PlanCashRule;

/**
 * This is the model class for table "company".
 *
 * @property integer $id
 * @property integer $self_employed
 * @property integer $strict_mode
 * @property integer $company_type_id
 * @property integer $blocked
 * @property integer $taxation_type_id
 * @property string $name_full
 * @property string $name_short
 * @property string $address_legal
 * @property string $address_legal_city
 * @property string $address_legal_street
 * @property integer $address_legal_house_type_id
 * @property string $address_legal_house
 * @property integer $address_legal_housing_type_id
 * @property string $address_legal_housing
 * @property integer $address_legal_apartment_type_id
 * @property string $address_legal_apartment
 * @property integer $address_legal_postcode
 * @property integer $address_legal_is_actual
 * @property string $address_actual
 * @property string $address_actual_city
 * @property string $address_actual_street
 * @property integer $address_actual_house_type_id
 * @property string $address_actual_house
 * @property integer $address_actual_housing_type_id
 * @property string $address_actual_housing
 * @property integer $address_actual_apartment_type_id
 * @property string $address_actual_apartment
 * @property string $address_actual_postcode
 * @property string $visitcard_url
 * @property string $phone
 * @property string $email
 * @property string $chief_post_name
 * @property string $chief_lastname
 * @property string $chief_firstname
 * @property string $chief_firstname_initials
 * @property string $chief_patronymic
 * @property integer $has_chief_patronymic
 * @property string $chief_patronymic_initials
 * @property string $chief_is_chief_accountant
 * @property string $chief_accountant_lastname
 * @property string $chief_accountant_firstname
 * @property string $chief_accountant_firstname_initials
 * @property string $chief_accountant_patronymic
 * @property integer $has_chief_accountant_patronymic
 * @property string $chief_accountant_patronymic_initials
 * @property string $ogrn
 * @property string $egrip
 * @property string $inn
 * @property string $kpp
 * @property string $ip_lastname
 * @property string $ip_firstname
 * @property string $ip_firstname_initials
 * @property string $ip_patronymic
 * @property string $ip_patronymic_initials
 * @property string $ip_certificate_number
 * @property string $ip_certificate_date
 * @property string $ip_certificate_issued_by
 * @property string $ip_patent_city
 * @property string $ip_patent_date
 * @property string $ip_patent_date_end
 * @property string $certificateDate
 * @property string $okpo
 * @property string $okogu
 * @property string $okato
 * @property string $okved
 * @property string $okfs
 * @property string $okopf
 * @property string $oktmo
 * @property string $okud
 * @property string $pfr
 * @property string $fss
 * @property string $pfr_ip
 * @property string $pfr_employer
 * @property string $tax_authority_registration_date
 * @property string $logo_link
 * @property string $print_link
 * @property string $chief_signature_link
 * @property string $created_at
 * @property string $updated_at
 * @property string $active_subscribe_id
 * @property integer $experience_id
 * @property string $time_zone_id
 * @property integer $email_messages_send_visit_card;
 * @property integer $nds
 * @property integer $documents_type
 * @property string $documentsType
 * @property boolean $isDocsTypeUpd
 * @property integer $main_id
 * @property boolean $test
 * @property string comment
 * @property integer $activities_id
 * @property integer $import_xls_product_count
 * @property integer $import_xls_service_count
 * @property string $form_source
 * @property string $form_keyword
 * @property string $form_region
 * @property string $form_date
 * @property string $form_hour
 * @property string $form_sign_in_page
 * @property string $form_entrance_page
 * @property integer $form_unique_events_count
 * @property string $form_google_analytics_id
 * @property string $form_utm
 * @property integer $new_template_order
 * @property boolean $show_reminder_7
 * @property boolean $show_reminder_5
 * @property boolean $show_reminder_3
 * @property boolean $show_reminder_1
 * @property boolean $is_additional_number_before
 * @property integer $after_registration_block_type
 * @property integer $after_registration_expose_invoice_count
 * @property integer $after_registration_expose_other_documents_count
 * @property integer $after_registration_business_analyse_count
 * @property integer $after_registration_employee_plan_count
 * @property integer $after_registration_stock_control_count
 * @property integer $after_registration_bill_count
 * @property integer $store_where_empty_products_type
 * @property integer $store_where_empty_products_text_type
 * @property boolean $store_show_novelty_button
 * @property integer $store_novelty_product_by_days_count
 * @property boolean $store_has_discount
 * @property integer $store_discount_from_amount
 * @property integer $store_discount_type
 * @property integer $store_discount
 * @property string $store_discount_another
 * @property boolean $show_popup_expose_other_documents
 * @property boolean $show_popup_business_analyse
 * @property boolean $show_popup_product
 * @property boolean $show_popup_sale_increase
 * @property boolean $show_popup_kub_do_it
 * @property boolean $show_popup_import_products
 * @property integer $show_invoice_payment_popup_date
 * @property boolean $shared_kub_vk
 * @property string $qiwi_public_key
 * @property integer $registration_page_type_id
 * @property string $capital
 * @property integer $info_industry_id
 * @property integer $info_employers_count
 * @property integer $info_sellers_count
 * @property integer $info_site_id
 * @property integer $info_has_shop
 * @property integer $info_has_storage
 * @property integer $info_shops_count
 * @property array|null $integration                   Настройки интеграции, JSON
 * @property integer $company_activity_id
 * @property integer $employee_count_id
 * @property integer $requisites_updated_at
 * @property integer $default_contractor_expenditure_item_id
 * @property integer $default_contractor_income_item_id
 * @property string $name_short_en
 * @property string $name_full_en
 * @property string $form_legal_en
 * @property string $address_legal_en
 * @property string $address_actual_en
 * @property string $chief_post_name_en
 * @property string $lastname_en
 * @property string $firstname_en
 * @property string $patronymic_en
 * @property integer $not_has_patronymic_en
 * @property boolean $show_waybill_button_in_invoice
 * @property int $owner_employee_id
 * @property int $seller_payment_delay
 * @property int $customer_payment_delay
 * @property CompanyType $companyType
 * @property address\AddressApartmentType $addressActualApartmentType
 * @property address\AddressHouseType $addressActualHouseType
 * @property address\AddressHousingType $addressActualHousingType
 * @property address\AddressApartmentType $addressLegalApartmentType
 * @property address\AddressHouseType $addressLegalHouseType
 * @property address\AddressHousingType $addressLegalHousingType
 * @property TaxationType $taxationType
 * @property Contractor[] $contractors
 * @property Employee[] $employees
 * @property Subscribe $activeSubscribe
 * @property Subscribe[] $activeSubscribes
 * @property CheckingAccountant $mainCheckingAccountant
 * @property CheckingAccountant[] $checkingAccountants
 * @property Subscribe[] $subscribes
 * @property BikDictionary $bank
 * @property CompanyTaxationType $companyTaxationType
 * @property boolean $isFreeTariff
 * @property boolean $freeTariffNotified
 * @property integer $strict_mode_date
 * @property integer $first_send_invoice_date
 * @property integer $activation_type
 * @property integer $activationStatus
 * @property File[] $files
 * @property Export[] $export
 * @property Agreement[] $agreements
 * @property ApplicationToBank[] $applicationsToBank
 * @property Attendance[] $attendances
 * @property Autoinvoice[] $autoinvoices
 * @property CashBankFlows[] $cashBankFlows
 * @property CashBankStatementUpload[] $cashBankStatementUploads
 * @property CashEmoneyFlows[] $cashEmoneyFlows
 * @property CashOrderFlows[] $cashOrderFlows
 * @property ExportFiles[] $exportFiles
 * @property Log[] $logs
 * @property Report[] $reports
 * @property PromoCode[] $servicePaymentPromoCodes
 * @property SpecificDocument[] $specificDocuments
 * @property Invoice[] $invoices
 * @property Product[] $products
 * @property EmployeeCompany[] $employeeCompanies
 * @property Activities $activities
 * @property UploadedDocuments[] $uploadedDocuments
 * @property Company[] $invitedCompanies
 * @property CompanyNotification[] $companyNotifications
 * @property RewardRequest[] $rewardRequests
 * @property Company $invitedByCompany
 * @property Payment[] $payments
 * @property SellingSubscribe[] $sellingSubscribes
 * @property ServiceMoreStock $serviceMoreStock
 * @property integer $notCreatedInvoice
 * @property integer $notCreatedAct
 * @property integer $notCreatedPL
 * @property integer $notCreatedIF
 * @property string $affiliate_link
 * @property string $invited_by_company_id
 * @property string $invited_by_product_id
 * @property string $invited_by_referral_code
 * @property integer $affiliate_sum
 * @property integer $affiliate_link_created_at
 * @property integer $affiliate_link_updated_at
 * @property integer $affiliate_link_deleted_at
 * @property integer $total_affiliate_sum
 * @property integer $payed_reward_for_kub
 * @property integer $payed_reward_for_rs
 * @property integer $payed_reward_for_card
 * @property integer $invited_companies_count
 * @property integer $active_reward_requests
 * @property boolean $show_service_more_stock_modal_first_rule
 * @property boolean $show_service_more_stock_modal_second_rule
 * @property boolean $hasPaidSubscribes
 * @property boolean $pdf_signed
 * @property boolean $pdf_act_signed
 * @property Store[] $stores
 * @property Store $mainStore
 * @property StoreUser $storeUser
 * @property PaymentReminderMessage[] $paymentReminderMessages
 * @property InvoiceContractEssence $invoiceContractEssence
 * @property InvoiceContractorSignature $invoiceContractorSignature
 * @property InvoiceEmailText $invoiceEmailText
 * @property InvoiceEssence $invoiceEssence
 * @property OrderDocument[] $orderDocuments
 * @property Payment $firstPayment
 * @property Payment $lastPayment
 * @property EmailTemplate[] $emailTemplates
 * @property EmailTemplate $activeEmailTemplate
 * @property UserEmail[] $userEmails
 * @property PriceList[] $priceLists
 * @property RegistrationPageType $registrationPageType
 * @property ActEssence $actEssence
 * @property CompanyAffiliateLink[] $companyAffiliateLinks
 * @property CompanyInfoIndustry $infoIndustry
 * @property CompanyInfoSite $infoSite
 * @property AutoloadMoneta $autoloadMoneta
 * @property OfdType $ofdTypes[]
 * @property CompanyToAcquiring[] $acquiringAccounts
 * @property OnlinePaymentType[] $onlinePaymentTypes
 * @property AccountingSystemType[] $accountingSystemTypes
 * @property GoogleAnalyticsProfile $googleAnalyticsProfile
 * @property Driver[] $drivers
 * @property Vehicle[] $vehicles
 * @property LogisticsRequestContractEssence $logisticsRequestContractEssenceCustomerAll
 * @property LogisticsRequestContractEssence $logisticsRequestContractEssenceCustomerForContractor
 * @property LogisticsRequestContractEssence $logisticsRequestContractEssenceCarrierAll
 * @property LogisticsRequestContractEssence $logisticsRequestContractEssenceCarrierForContractor
 * @property CompanyInfoPlace $infoPlace
 * @property string $utm_source
 * @property int $invoice_facture_print_template
 * @property CheckingAccountant $mainForeignCurrencyAccount
 * @property boolean $hasBankingEmail
 *
 * @mixin TimestampBehavior
 * @mixin DatePickerFormatBehavior
 */
class Company extends \yii\db\ActiveRecord implements Printable
{

    const INTEGRATION_MODULE = [
        Employee::INTEGRATION_AMOCRM,
        Employee::INTEGRATION_EMAIL,
        Employee::INTEGRATION_EVOTOR,
        Employee::INTEGRATION_FACEBOOK,
        Employee::INTEGRATION_VK,
        Employee::INTEGRATION_UNISENDER,
        Employee::INTEGRATION_INSALES,
        Employee::INTEGRATION_BITRIX24,
        Employee::INTEGRATION_GOOGLE_ADS,
        Employee::INTEGRATION_YANDEX_DIRECT,
    ];

    /**
     * @var
     */
    public $rs;

    /**
     * @var
     */
    public $requiredEnAttributes = false;

    /**
     * scenarios
     */
    const SCENARIO_ADMIN_INSERT = 'admin_insert';
    const SCENARIO_REGISTRATION = 'admin_insert';
    const SCENARIO_ADMIN_UPDATE = 'admin_update';
    const SCENARIO_USER_UPDATE = 'user_update';
    const SCENARIO_IP_UPDATE = 'ipUpdate';
    /**
     *
     */
    const SCENARIO_IP_UPDATE_LANDING = 'ipUpdateLanding';
    /**
     *
     */
    const SCENARIO_OOO_UPDATE = 'oooUpdate';
    /**
     *
     */
    const SCENARIO_OOO_UPDATE_LANDING = 'oooUpdateLanding';
    /**
     *
     */
    const SCENARIO_CREATE_COMPANY = 'create_company';
    const SCENARIO_TAX_ROBOT = 'tax_robot';
    const SCENARIO_TAX_IFNS = 'tax_ifns';
    const SCENARIO_TAX_PSN = 'tax_psn';
    const SCENARIO_B2B_IP = 'b2bIp';
    const SCENARIO_B2B_OOO = 'b2bOoo';
    const SCENARIO_DECLARATION_OOO = 'declarationOoo';
    const SCENARIO_ANALYTICS_IP = 'analyticsIp';
    const SCENARIO_ANALYTICS_OOO = 'analyticsOoo';
    /**
     *
     */
    const BLOCKED = 1;
    /**
     *
     */
    const UNBLOCKED = 0;
    /**
     *
     */
    const TEST_COMPANY = 1;
    /**
     *
     */
    const KUB_ID = 1;
    /**
     *
     */
    const ACTIVATION_EMPTY_PROFILE = 1;
    /**
     *
     */
    const ACTIVATION_NO_INVOICE = 2;
    /**
     *
     */
    const ACTIVATION_NOT_SEND_INVOICES = 3;
    /**
     *
     */
    const ACTIVATION_ALL_COMPLETE = 4;
    /**
     * documents type
     */
    const DOCS_TYPE_PACK = 1; // Акт, Товарная накладная, Счет-фактура
    /**
     *
     */
    const DOCS_TYPE_UPD = 2; // УПД

    const AFTER_REGISTRATION_MAIN_MODAL = 0;
    const AFTER_REGISTRATION_EXPOSE_INVOICE = 1;
    const AFTER_REGISTRATION_EXPOSE_OTHER_DOCUMENTS = 2;
    const AFTER_REGISTRATION_BUSINESS_ANALYSE = 3;
    const AFTER_REGISTRATION_EMPLOYEE_PLAN = 4;
    const AFTER_REGISTRATION_STOCK_CONTROL = 5;
    const AFTER_REGISTRATION_BILL = 6;

    const WHERE_EMPTY_PRODUCTS_TYPE_NOT_SHOW = 0;
    const WHERE_EMPTY_PRODUCTS_TYPE_SHOW = 1;
    const WHERE_EMPTY_PRODUCTS_TYPE_TEXT = 2;

    const WHERE_EMPTY_PRODUCTS_TEXT_TYPE_FOR_ORDER = 0;
    const WHERE_EMPTY_PRODUCTS_TEXT_TYPE_BY_AGREEMENT = 1;
    const WHERE_EMPTY_PRODUCTS_TEXT_TYPE_OUT_OF_STOCK = 2;
    const WHERE_EMPTY_PRODUCTS_TEXT_TYPE_CALL = 3;
    const WHERE_EMPTY_PRODUCTS_TEXT_TYPE_REFINE = 4;

    const DISCOUNT_TYPE_PERCENT = 0;
    const DISCOUNT_TYPE_ANOTHER = 1;

    /**
     * @var array
     */
    public static $notShowModalAttributes = [
        self::AFTER_REGISTRATION_EXPOSE_OTHER_DOCUMENTS => 'show_popup_expose_other_documents',
        self::AFTER_REGISTRATION_BUSINESS_ANALYSE => 'show_popup_business_analyse',
        self::AFTER_REGISTRATION_STOCK_CONTROL => 'show_popup_product',
        self::AFTER_REGISTRATION_BILL => 'show_popup_sale_increase',
    ];

    /**
     * @var array
     */
    public static $whereEmptyProductsTextType = [
        Company::WHERE_EMPTY_PRODUCTS_TEXT_TYPE_FOR_ORDER => 'Под заказ',
        Company::WHERE_EMPTY_PRODUCTS_TEXT_TYPE_BY_AGREEMENT => 'По согласованию',
        Company::WHERE_EMPTY_PRODUCTS_TEXT_TYPE_OUT_OF_STOCK => 'На дальнем складе',
        Company::WHERE_EMPTY_PRODUCTS_TEXT_TYPE_CALL => 'Звоните',
        Company::WHERE_EMPTY_PRODUCTS_TEXT_TYPE_REFINE => 'Уточняйте',
    ];

    /**
     * @var array
     */
    public static $afterRegistrationBlock = [
        null => '',
        self::AFTER_REGISTRATION_MAIN_MODAL => 'Не выбрал ничего',
        self::AFTER_REGISTRATION_EXPOSE_INVOICE => 'Выставить счет',
        self::AFTER_REGISTRATION_EXPOSE_OTHER_DOCUMENTS => 'Подготовить акт, тн, сф',
        self::AFTER_REGISTRATION_BUSINESS_ANALYSE => 'Анализ бизнеса',
        self::AFTER_REGISTRATION_EMPLOYEE_PLAN => 'Контроль работы сотрудников',
        self::AFTER_REGISTRATION_STOCK_CONTROL => 'Учет товаров',
        self::AFTER_REGISTRATION_BILL => 'Повышение продаж',
    ];

    public static $afterRegistrationBlockAttributes = [
        self::AFTER_REGISTRATION_EXPOSE_INVOICE => 'after_registration_expose_invoice_count',
        self::AFTER_REGISTRATION_EXPOSE_OTHER_DOCUMENTS => 'after_registration_expose_other_documents_count',
        self::AFTER_REGISTRATION_BUSINESS_ANALYSE => 'after_registration_business_analyse_count',
        self::AFTER_REGISTRATION_EMPLOYEE_PLAN => 'after_registration_employee_plan_count',
        self::AFTER_REGISTRATION_STOCK_CONTROL => 'after_registration_stock_control_count',
        self::AFTER_REGISTRATION_BILL => 'after_registration_bill_count',
    ];

    /**
     * @var array
     */
    public static $docsType = [
        self::DOCS_TYPE_UPD => 'УПД',
        self::DOCS_TYPE_PACK => 'Акт, Товарную накладную, Счет-фактуру',
    ];

    /**
     * @var array
     */
    public static $activationType = [
        null => 'Не указан',
        self::ACTIVATION_EMPTY_PROFILE => 'НЕ заполнен профиль',
        self::ACTIVATION_NO_INVOICE => 'Заполнен профиль, НЕ выставил счет',
        self::ACTIVATION_NOT_SEND_INVOICES => 'Заполнен профиль, выставлен счет, НЕ отправил его по e-mail',
        self::ACTIVATION_ALL_COMPLETE => 'Заполнен профиль, выставлен счет, отправлен счет по e-mail',
    ];

    /**
     * @var array
     */
    public static $companyFormView = [
        CompanyType::TYPE_IP => '_type_ip',
        CompanyType::TYPE_OOO => '_type_ooo',
    ];

    /**
     * @var array
     */
    public static $requisites = [
        'inn',
        'kpp',
        'address_legal',
        'name_short',
        'name_full',
    ];

    /**
     * @var array
     */
    public static $invoiceRequisites = [
        'company_inn',
        'company_kpp',
        'company_address_legal_full',
        'company_name_short',
        'company_name_full',
        'company_egrip',
        'company_okpo',
        'company_phone',
        'company_chief_post_name',
        'company_chief_lastname',
        'company_chief_firstname_initials',
        'company_chief_patronymic_initials',
        'company_chief_accountant_lastname',
        'company_chief_accountant_firstname_initials',
        'company_chief_accountant_patronymic_initials',
        'company_print_filename',
        'company_chief_signature_filename',
    ];

    /**
     * @var array
     */
    public static $imageAttrArray = [
        'logoImage' => 'logo_link',
        'printImage' => 'print_link',
        'chiefSignatureImage' => 'chief_signature_link',
        'chiefAccountantSignatureImage' => 'chief_accountant_signature_link',
    ];

    /**
     * @var array
     */
    public static $imageDataArray = [
        'logoImage' => [
            'file_name' => self::IMAGE_LOGO_NAME,
            'width' => 545,
            'height' => 185,
            'label' => 'Логотип',
            'crop_text' => 'Выбранная область с логотипом будет выводиться в счете и акте',
            'preview_text' => 'Так будет выглядеть логотип на ваших документах',
            'dummy_text' => 'Рекомендуемый размер для загрузки: 545х185',
        ],
        'printImage' => [
            'file_name' => self::IMAGE_PRINT_NAME,
            'width' => 200,
            'height' => 200,
            'label' => 'Печать',
            'crop_text' => 'Выбранная область с печатью будет выводиться в счете и акте',
            'preview_text' => 'Так будет выглядеть печать на ваших документах',
            'dummy_text' => 'Рекомендуемый размер для загрузки: 200х200',
        ],
        'chiefSignatureImage' => [
            'file_name' => self::IMAGE_CHIEF_SIGNATURE,
            'width' => 165,
            'height' => 50,
            'label' => 'Подпись',
            'crop_text' => 'Выбранная область с подписью будет выводиться в счете и акте',
            'preview_text' => 'Так будет выглядеть подпись на ваших документах',
            'dummy_text' => 'Рекомендуемый размер для загрузки: 165х50',
        ],
        'chiefAccountantSignatureImage' => [
            'file_name' => self::IMAGE_CHIEF_ACCOUNTANT_SIGNATURE,
            'width' => 165,
            'height' => 50,
            'label' => 'Подпись',
            'crop_text' => 'Выбранная область с подписью будет выводиться в счете и акте',
            'preview_text' => 'Так будет выглядеть подпись на ваших документах',
            'dummy_text' => 'Рекомендуемый размер для загрузки: 165х50',
        ],
    ];

    public static $addNumPositions = [
        0 => 'После номера счета: <span style="font-style: italic;">№ 123-В</span>',
        1 => 'Перед номером счета: <span style="font-style: italic;">№ В-123</span>',
    ];

    public static $addTemplateSFandUPD = [
        0 => 'Нет',
        1 => 'Вариант 1',
        2 => 'Вариант 2',
    ];


    public static $applyNewImage = [
        1 => 'Применить ко всем документам',
        0 => 'Применить только к новым документам',
    ];

    /**
     *
     */
    const IMAGE_LOGO_NAME = 'logo';
    /**
     *
     */
    const IMAGE_PRINT_NAME = 'print';
    /**
     *
     */
    const IMAGE_CHIEF_SIGNATURE = 'chiefSignature';
    /**
     *
     */
    const IMAGE_CHIEF_ACCOUNTANT_SIGNATURE = 'chiefAccountantSignature';
    /**
     *
     */
    const ON_STRICT_MODE = 1;
    /**
     *
     */
    const OFF_STRICT_MODE = 0;

    /**
     * @var UploadedFile
     */
    public $logoImage;

    /**
     * @var UploadedFile
     */
    public $printImage;

    /**
     * @var UploadedFile
     */
    public $chiefSignatureImage;

    /**
     * @var UploadedFile
     */
    public $chiefAccountantSignatureImage;

    /**
     * New Image apply mode
     * @var boolean
     */
    public $logoImageApply = 0;
    public $printImageApply = 0;
    public $chiefSignatureImageApply = 0;
    public $chiefAccountantSignatureImageApply = 0;

    /**
     * @var boolean
     */
    public $deleteLogoImage;
    /**
     * @var
     */
    public $deletePrintImage;
    /**
     * @var
     */
    public $deleteChiefSignatureImage;
    /**
     * @var
     */
    public $deleteChiefAccountantSignatureImage;
    /**
     * @var
     */
    public $deletePrintImageAll;
    /**
     * @var
     */
    public $deleteChiefSignatureImageAll;
    /**
     * @var
     */
    public $deleteChiefAccountantSignatureImageAll;

    /**
     * Current actual subscription
     * @var Subscribe
     */
    public $subscription;

    /**
     * @var
     */
    public $typeUsn;

    public $invoice_count;
    public $act_count;
    public $packing_list_count;
    public $invoice_facture_count;
    public $product_count;
    public $employee_count;
    public $store_count;
    public $out_invoice_count;

    /**
     * @var
     */
    public $subscribe_count;
    /**
     * @var
     */
    public $tmpId;
    /**
     * @var int
     */
    public $capitalInput = 0;
    /**
     * @var
     */
    protected $_period = null;
    /**
     * @var
     */
    protected $_hasAccountants = null;
    protected $_hasPaidSubscribes = null;
    protected $_expireSubscribeTimestamp = null;
    protected $_createInvoiceAllowed = [];

    /** @var Subscribe[] */
    protected $_actualSubscriptions = [];
    protected $_maxDiscount = [];
    protected $_data = [];

    public $product;

    //Partner property
    public $registrationDate;
    public $partnerRegistrationDate;
    public $partnerName;
    public $invoiceCount;
    public $priceListsCount;
    public $b2bCount;
    public $bookkeepingCount;
    public $analysisCount;
    public $paidCount;
    public $paidSum;
    public $paidInvoicesCount;
    public $paidInvoicesSum;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company';
    }

    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new CompanyQuery(get_called_class());
    }

    /**
     * @return array the list of expandable field names or field definitions. Please refer
     * to [[fields()]] on the format of the return value.
     * @see toArray()
     * @see fields()
     */
    public function extraFields()
    {
        return [
            'mainAccountant',
            'mainCheckingAccountant',
            'checkingAccountants',
            'companyTaxationType',
            'companyType',
            'timeZone',
        ];
    }

    /**
     * Распаковывает JSON-строку после загрузки данных из БД
     */
    public function afterFind()
    {
        parent::afterFind();
        if ($this->integration !== null) {
            $this->integration = json_decode($this->integration, true);
        }
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            [
                'class' => DatePickerFormatBehavior::className(),
                'attributes' => [
                    'tax_authority_registration_date',
                    'ip_patent_date',
                    'ip_patent_date_end',
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        return array_merge(parent::scenarios(), [
            self::SCENARIO_OOO_UPDATE_LANDING => [
                'inn',
                'name_short',
                'company_type_id',
                'address_legal_postcode',
                'address_legal_city',
                'address_legal_street',
                'address_legal_house',
                'address_legal_housing',
                'address_legal_apartment',
                'ogrn',
                'kpp',
                'chief_lastname',
                'chief_firstname',
                'chief_patronymic',
                'chief_firstname_initials',
                'chief_patronymic_initials',
                'chief_accountant_lastname',
                'chief_accountant_firstname',
                'chief_accountant_patronymic',
                'chief_accountant_firstname_initials',
                'chief_accountant_patronymic_initials',
                'pdf_signed',
            ],
            self::SCENARIO_OOO_UPDATE => [
                'inn',
                'name_short',
                'company_type_id',
                'address_legal',
                'address_actual',
                'ogrn',
                'kpp',
                'chief_fio',
                'chief_lastname',
                'chief_firstname',
                'chief_patronymic',
                'has_chief_patronymic',
                'pdf_signed',
                'pdf_act_signed',
                'pdf_act_send_signed',
                'phone',
            ],
            self::SCENARIO_IP_UPDATE_LANDING => [
                'inn',
                'ip_lastname',
                'ip_firstname',
                'ip_patronymic',
                'company_type_id',
                'ip_firstname_initials',
                'ip_patronymic_initials',
                'address_legal_postcode',
                'address_legal_city',
                'address_legal_street',
                'address_legal_house',
                'address_legal_housing',
                'address_legal_apartment',
                'egrip',
                'pdf_signed',
                'phone',
                'tmpId',
                'name_short_en',
                'name_full_en',
                'form_legal_en',
                'address_legal_en',
                'address_actual_en',
                'chief_post_name_en',
                'lastname_en',
                'firstname_en',
                'patronymic_en',
                'seller_payment_delay',
                'customer_payment_delay',
            ],
            self::SCENARIO_IP_UPDATE => [
                'inn',
                'chief_fio',
                'ip_lastname',
                'ip_firstname',
                'ip_patronymic',
                'company_type_id',
                'has_chief_patronymic',
                'address_legal',
                'address_actual',
                'egrip',
                'pdf_signed',
                'pdf_act_signed',
                'pdf_act_send_signed',
                'phone',
                'tmpId',
                'name_short_en',
                'name_full_en',
                'form_legal_en',
                'address_legal_en',
                'address_actual_en',
                'chief_post_name_en',
                'lastname_en',
                'firstname_en',
                'patronymic_en',
                'seller_payment_delay',
                'customer_payment_delay',
            ],
            self::SCENARIO_B2B_IP => [
                'inn',
                'chief_fio',
                'ip_lastname',
                'ip_firstname',
                'ip_patronymic',
                'has_chief_patronymic',
                'address_legal',
                'address_actual',
                'egrip',
                'pdf_signed',
                'pdf_act_signed',
                'pdf_act_send_signed',
                'tmpId',
                'nds',
                'phone',
                'taxRegistrationDate',
                'seller_payment_delay',
                'customer_payment_delay',
            ],
            self::SCENARIO_B2B_OOO => [
                'inn',
                'name_short',
                'name_full',
                'company_type_id',
                'address_legal',
                'ogrn',
                'kpp',
                'chief_fio',
                'chief_lastname',
                'chief_firstname',
                'chief_patronymic',
                'has_chief_patronymic',
                'pdf_signed',
                'pdf_act_signed',
                'pdf_act_send_signed',
                //'phone',
                'tmpId',
                'nds',
                'taxRegistrationDate',
                'seller_payment_delay',
                'customer_payment_delay',
            ],
            self::SCENARIO_DECLARATION_OOO => [
                'inn',
                'name_short',
                'name_full',
                'company_type_id',
                'address_legal',
                'ogrn',
                'kpp',
                'chief_fio',
                'chief_lastname',
                'chief_firstname',
                'chief_patronymic',
                'has_chief_patronymic',
                'pdf_signed',
                'pdf_act_signed',
                'pdf_act_send_signed',
                'tmpId',
                'nds',
                'taxRegistrationDate',
                'okato',
                'oktmo',
                'ifns_ga',
                'okved',
                'seller_payment_delay',
                'customer_payment_delay',
            ],
            self::SCENARIO_TAX_ROBOT => [
                'inn',
                'egrip',
                'okato',
                'oktmo',
                'ifns_ga',
                'ip_lastname',
                'ip_firstname',
                'ip_patronymic',
                'address_legal',
                'address_legal_postcode',
                'taxRegistrationDate',
                'has_chief_patronymic',
                'address_legal_city',
                'okved',
                'seller_payment_delay',
                'customer_payment_delay',
            ],
            self::SCENARIO_TAX_IFNS => [
                'okato',
                'ifns_ga',
                'seller_payment_delay',
                'customer_payment_delay',
            ],
            self::SCENARIO_TAX_PSN => [
                'ip_patent_city',
                'patentDate',
                'patentDateEnd',
                'seller_payment_delay',
                'customer_payment_delay',
            ],
            self::SCENARIO_CREATE_COMPANY => [
                'company_type_id',
                'seller_payment_delay',
                'customer_payment_delay',
            ],
            self::SCENARIO_REGISTRATION => [
                'company_type_id',
                'email',
                'phone',
                'seller_payment_delay',
                'customer_payment_delay',
            ],
            self::SCENARIO_ANALYTICS_IP => [
                'inn',
                'chief_fio',
                'ip_lastname',
                'ip_firstname',
                'ip_patronymic',
                'has_chief_patronymic',
                'address_legal',
                'address_actual',
                'egrip',
                'pdf_signed',
                'pdf_act_signed',
                'pdf_act_send_signed',
                'tmpId',
                'nds',
                'phone',
                'taxRegistrationDate',
                'info_industry_id',
                'info_employers_count',
                'info_sellers_count',
                'info_site_id',
                'info_has_shop',
                'info_has_storage',
                'info_shops_count',
                'seller_payment_delay',
                'customer_payment_delay',
            ],
            self::SCENARIO_ANALYTICS_OOO => [
                'inn',
                'name_short',
                'name_full',
                'company_type_id',
                'address_legal',
                'ogrn',
                'kpp',
                'chief_fio',
                'chief_lastname',
                'chief_firstname',
                'chief_patronymic',
                'has_chief_patronymic',
                'pdf_signed',
                'pdf_act_signed',
                'pdf_act_send_signed',
                //'phone',
                'tmpId',
                'nds',
                'taxRegistrationDate',
                'info_industry_id',
                'info_employers_count',
                'info_sellers_count',
                'info_site_id',
                'info_has_shop',
                'info_has_storage',
                'info_shops_count',
                'seller_payment_delay',
                'customer_payment_delay',
            ],
        ]);
    }

    /**
     * @inheritdoc
     *
     * The default implementation returns the names of the columns whose values have been populated into this record.
     */
    public function fields()
    {
        $fields = [
            "id",
            "strict_mode",
            "self_employed",
            "inn",
            "company_type_id",
            "name_full",
            "name_short",
            "time_zone_id",
            "address_legal",
            "address_actual",
            "address_legal_is_actual",
            "phone",
            "email",
            "chief_post_name",
            "chief_lastname",
            "chief_firstname",
            "chief_firstname_initials",
            "chief_patronymic",
            "chief_patronymic_initials",
            "chief_is_chief_accountant",
            "chief_accountant_lastname",
            "chief_accountant_firstname",
            "chief_accountant_firstname_initials",
            "chief_accountant_patronymic",
            "chief_accountant_patronymic_initials",
            "ogrn",
            "egrip",
            "kpp",
            "ip_lastname",
            "ip_firstname",
            "ip_firstname_initials",
            "ip_patronymic",
            "ip_patronymic_initials",
            "ip_certificate_number",
            "ip_certificate_date",
            "ip_certificate_issued_by",
            "ip_patent_city",
            "ip_patent_date",
            "ip_patent_date_end",
            "okpo",
            "okogu",
            "okato",
            "okved",
            "okfs",
            "okopf",
            "oktmo",
            "okud",
            "ifns_ga",
            "tax_authority_registration_date",
            "logo_link",
            "print_link",
            "chief_signature_link",
            "chief_accountant_signature_link",
            "created_at",
            "created_by",
            "updated_at",
            "last_visit_at",
            "active_subscribe_id",
            "experience_id",
            "nds",
            "nds_view_type_id",
            "documents_type",
            "object_guid",
            "current_account_guid",
            "main_id",
            "has_chief_patronymic",
            "has_chief_accountant_patronymic",
            "comment",
            "free_tariff_start_at",
            "is_free_tariff_notified",
            "qiwi_public_key",
            "expireSubscribeDate",
            "name_short_en",
            "name_full_en",
            "form_legal_en",
            "address_legal_en",
            "address_actual_en",
            "chief_post_name_en",
            "lastname_en",
            "firstname_en",
            "patronymic_en",
            'seller_payment_delay',
            'customer_payment_delay',
        ];

        return array_combine($fields, $fields);
    }

    /**
     * @inheritdoc
     */
    public function getIsLikeOOO()
    {
        return !$this->self_employed && $this->companyType && !$this->companyType->like_ip;
    }

    /**
     * @inheritdoc
     */
    public function getIsLikeIP()
    {
        return $this->self_employed || ($this->companyType && $this->companyType->like_ip);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        /** Scenarios:
         * admin_insert
         * admin_update
         * user_update
         */
        $oooClosure = function (Company $model) {
            return $model->getIsLikeOOO();
        };
        $oooClosureJs = 'function () {
            return !companyIsSelfEmployed() && companyIsOoo();
        }';

        $ipClosure = function (Company $model) {
            return $model->company_type_id == CompanyType::TYPE_IP;
        };
        $ipClosureJs = 'function () {
            return companyIsIp();
        }';

        $likeIpClosure = function (Company $model) {
            return $model->self_employed || $model->company_type_id == CompanyType::TYPE_IP;
        };
        $likeIpClosureJs = 'function () {
            return companyIsSelfEmployed() || companyIsIp();
        }';

        $selfEmployedClosure = function (Company $model) {
            return $model->self_employed;
        };
        $selfEmployedClosureJs = 'function () {
            return companyIsSelfEmployed();
        }';

        return [
            [['paidCount', 'paidSum'], 'safe'],
            [['self_employed'], 'boolean', 'on' => [
                self::SCENARIO_ADMIN_UPDATE,
                self::SCENARIO_USER_UPDATE,
            ]],
            [['chief_fio'], 'string'],
            [['email',], 'trim'],
            [
                [
                    'inn',
                    'ip_lastname',
                    'ip_firstname',
                    'ip_patronymic',
                ],
                'trim',
                'on' => [self::SCENARIO_IP_UPDATE, self::SCENARIO_B2B_IP, self::SCENARIO_ANALYTICS_IP],
            ],
            [
                [
                    'inn',
                    'name_short',
                    'chief_lastname',
                    'chief_firstname',
                    'chief_patronymic',
                ],
                'trim',
                'on' => [self::SCENARIO_OOO_UPDATE, self::SCENARIO_B2B_OOO, self::SCENARIO_ANALYTICS_OOO],
            ],
            [
                [
                    'inn',
                    'chief_fio',
                    'ip_lastname',
                    'ip_firstname',
                ],
                'required',
                'on' => [self::SCENARIO_IP_UPDATE, self::SCENARIO_B2B_IP, self::SCENARIO_ANALYTICS_IP],
                'message' => 'Необходимо заполнить',
            ],
            [
                ['ip_patronymic'], 'required',
                'on' => [self::SCENARIO_IP_UPDATE, self::SCENARIO_B2B_IP, self::SCENARIO_ANALYTICS_IP],
                'message' => 'Необходимо заполнить',
                'when' => function ($model) {
                    return !$model->has_chief_patronymic && $model->getIsLikeIP();
                },
                'whenClient' => 'function () {
                    return !$("#company-has_chief_patronymic").prop("checked") && (companyIsSelfEmployed() || companyIsIp());
                }',
            ],
            [
                [
                    'ip_patronymic',
                ],
                'required',
                'on' => self::SCENARIO_TAX_ROBOT,
                'when' => function ($model) {
                    return !$model->has_chief_patronymic;
                },
                'message' => 'Необходимо заполнить',
            ],
            [
                [
                    'inn',
                    'ip_lastname',
                    'ip_firstname',
                    'address_legal',
                    'egrip',
                    'oktmo',
                    'ifns_ga',
                    'taxRegistrationDate'
                ],
                'required',
                'on' => self::SCENARIO_TAX_ROBOT,
                'message' => 'Необходимо заполнить',
            ],
            [
                [
                    'okato',
                    'oktmo',
                    'address_legal',
                ],
                'string',
                'on' => self::SCENARIO_TAX_ROBOT,
            ],
            [
                [
                    'ifns_ga',
                    'address_legal_postcode',
                ],
                'integer',
                'on' => self::SCENARIO_TAX_ROBOT,
            ],
            [
                [
                    'seller_payment_delay',
                    'customer_payment_delay',
                ],
                'default',
                'value' => 10,
            ],
            [
                [
                    'seller_payment_delay',
                    'customer_payment_delay',
                ],
                'integer',
                'min' => 0,
            ],
            [
                [
                    'inn',
                    'address_legal',
                    'egrip',
                    'oktmo',
                    'ifns_ga',
                    'taxRegistrationDate'
                ],
                'required',
                'on' => self::SCENARIO_DECLARATION_OOO,
                'message' => 'Необходимо заполнить',
            ],
            [
                [
                    'okato',
                    'oktmo',
                    'address_legal',
                ],
                'string',
                'on' => self::SCENARIO_DECLARATION_OOO,
            ],
            [
                [
                    'ifns_ga',
                    'address_legal_postcode',
                ],
                'integer',
                'on' => self::SCENARIO_DECLARATION_OOO,
            ],
            [
                [
                    'okato',
                    'ifns_ga',
                ],
                'required',
                'on' => self::SCENARIO_TAX_IFNS,
                'message' => 'Необходимо заполнить',
            ],
            [
                [
                    'inn',
                    'name_short',
                    'company_type_id',
                    'address_legal',
                    'kpp',
                    'chief_fio',
                    'chief_lastname',
                    'chief_firstname',
                ],
                'required',
                'on' => [self::SCENARIO_OOO_UPDATE, self::SCENARIO_B2B_OOO, self::SCENARIO_ANALYTICS_OOO],
                'when' => function (Company $model) {
                    return !$model->self_employed;
                },
                'message' => 'Необходимо заполнить',
            ],
            [
                ['chief_patronymic'], 'required',
                'on' => [self::SCENARIO_OOO_UPDATE, self::SCENARIO_B2B_OOO, self::SCENARIO_ANALYTICS_OOO],
                'message' => 'Необходимо заполнить',
                'when' => function ($model) {
                    return !$model->has_chief_patronymic && $this->getIsLikeOOO();
                },
                'whenClient' => 'function () {
                    return companyIsOoo() && !$("#company-has_chief_patronymic").prop("checked");
                }',
            ],
            [
                [
                    'ip_lastname',
                    'ip_firstname',
                    'ip_patronymic',
                    'ip_firstname_initials',
                    'ip_patronymic_initials',
                    'address_legal_postcode',
                    'address_legal_city',
                    'address_legal_street',
                    'address_legal_house',
                    'address_legal_housing',
                    'address_legal_apartment',
                ],
                'trim',
                'on' => self::SCENARIO_IP_UPDATE_LANDING,
            ],
            [
                [
                    'name_short',
                    'address_legal_city',
                    'address_legal_street',
                    'address_legal_house',
                    'address_legal_housing',
                    'address_legal_apartment',
                    'chief_lastname',
                    'chief_firstname',
                    'chief_patronymic',
                    'chief_firstname_initials',
                    'chief_patronymic_initials',
                ],
                'trim',
                'on' => self::SCENARIO_OOO_UPDATE_LANDING,
            ],
            [
                ['inn',
                    'ip_lastname',
                    'ip_firstname',
                    'ip_patronymic',
                ],
                'required',
                'on' => self::SCENARIO_IP_UPDATE_LANDING,
                'message' => 'Необходимо заполнить',
            ],
            [
                ['inn',
                    'name_short',
                    'company_type_id',
                    'kpp',
                    'chief_lastname',
                    'chief_firstname',
                    'chief_patronymic',
                    'chief_accountant_lastname',
                    'chief_accountant_firstname',
                    'chief_accountant_patronymic',
                ],
                'required',
                'on' => self::SCENARIO_OOO_UPDATE_LANDING,
                'message' => 'Необходимо заполнить',
            ],
            // admin required
            [
                ['company_type_id',], 'required',
                'when' => function (Company $model) {
                    return !$model->self_employed;
                },
                'on' => [self::SCENARIO_ADMIN_INSERT, self::SCENARIO_REGISTRATION, self::SCENARIO_ADMIN_UPDATE, self::SCENARIO_CREATE_COMPANY, self::SCENARIO_USER_UPDATE],
            ],
            [['main_id'], 'integer', 'on' => [self::SCENARIO_ADMIN_INSERT, self::SCENARIO_REGISTRATION, self::SCENARIO_ADMIN_UPDATE, self::SCENARIO_CREATE_COMPANY]],
            [['nds'], 'required', 'on' => [self::SCENARIO_USER_UPDATE, self::SCENARIO_ADMIN_UPDATE, self::SCENARIO_B2B_OOO, self::SCENARIO_B2B_IP, self::SCENARIO_ANALYTICS_IP, self::SCENARIO_ANALYTICS_OOO],
                'when' => function (Company $model) {
                    return $model->companyTaxationType->osno;
                },
                'enableClientValidation' => false,
            ],
            [['strict_mode_date', 'first_send_invoice_date', 'activation_type', 'after_registration_block_type',
                'after_registration_expose_invoice_count', 'after_registration_expose_other_documents_count',
                'after_registration_business_analyse_count', 'after_registration_employee_plan_count',
                'after_registration_stock_control_count', 'after_registration_bill_count',
                'store_where_empty_products_type', 'store_where_empty_products_text_type',
                'store_novelty_product_by_days_count', 'store_discount_from_amount', 'store_discount_type', 'store_discount'], 'integer'],
            // admin and update required
            //    [
            //        ['taxation_type_id'],
            //        'required',
            //        'on' => [self::SCENARIO_ADMIN_INSERT, self::SCENARIO_REGISTRATION, self::SCENARIO_ADMIN_UPDATE, self::SCENARIO_USER_UPDATE, self::SCENARIO_CREATE_COMPANY],
            //    ],
            [
                ['name_full'],
                'required',
                'when' => $oooClosure,
                'whenClient' => $oooClosureJs,
                'on' => [self::SCENARIO_ADMIN_UPDATE, self::SCENARIO_USER_UPDATE, self::SCENARIO_B2B_OOO, self::SCENARIO_ANALYTICS_OOO],
            ],
            [
                ['name_short'],
                'required',
                'when' => $oooClosure,
                'whenClient' => $oooClosureJs,
                'on' => [self::SCENARIO_ADMIN_UPDATE, self::SCENARIO_USER_UPDATE, self::SCENARIO_B2B_OOO, self::SCENARIO_ANALYTICS_OOO],
            ],
            [
                ['phone'],
                'required',
                'on' => [self::SCENARIO_ADMIN_UPDATE, self::SCENARIO_USER_UPDATE, self::SCENARIO_OOO_UPDATE, self::SCENARIO_B2B_IP, self::SCENARIO_ANALYTICS_IP],
            ],
            //    [
            //        ['taxation_type_id'],
            //        'required',
            //        'when' => $oooClosure,
            //        'whenClient' => $oooClosureJs,
            //        'on' => [self::SCENARIO_ADMIN_INSERT, self::SCENARIO_REGISTRATION, self::SCENARIO_ADMIN_UPDATE, self::SCENARIO_USER_UPDATE],
            //    ],
            [
                ['ip_lastname', 'ip_firstname',],
                'required',
                'when' => $likeIpClosure,
                'whenClient' => $likeIpClosureJs,
                'on' => [self::SCENARIO_ADMIN_UPDATE, self::SCENARIO_USER_UPDATE/*, self::SCENARIO_CREATE_COMPANY*/],
            ],
            [
                ['name_short', 'name_full'],
                'string',
                'max' => 255,
                'on' => [self::SCENARIO_ADMIN_INSERT, self::SCENARIO_ADMIN_UPDATE, self::SCENARIO_USER_UPDATE],
            ],
            //[['taxation_type_id'], 'validateTaxationType'],
            // user_update
            [ // common through company type
                [
                    'inn',
                ],
                'required',
                'on' => [self::SCENARIO_USER_UPDATE],
            ],
            [ // chief of ooo
                ['chief_post_name', 'chief_lastname', 'chief_firstname'],
                'required',
                'when' => $oooClosure,
                'whenClient' => $oooClosureJs,
                'on' => [self::SCENARIO_USER_UPDATE],
            ],
            [
                ['chief_patronymic'],
                'required',
                'when' => function (Company $model) {
                    return (bool)!$this->has_chief_patronymic && $model->getIsLikeOOO();
                },
                'on' => [self::SCENARIO_USER_UPDATE],
            ],
            [ // ip fio
                ['ip_lastname', 'ip_firstname'],
                'required',
                'when' => $likeIpClosure,
                'whenClient' => $likeIpClosureJs,
                'on' => [self::SCENARIO_USER_UPDATE],
            ],
            [
                ['ip_patronymic'],
                'required',
                'when' => function (Company $model) {
                    return (!$model->has_chief_patronymic && $model->getIsLikeIP());
                },
                'on' => [self::SCENARIO_USER_UPDATE],
            ],
            [ // chief accountant
                [
                    'chief_accountant_lastname', 'chief_accountant_firstname',
                ],
                'required',
                'when' => function (Company $model) {
                    return !$model->getIsLikeIP() && (bool)!$model->chief_is_chief_accountant;
                },
                'whenClient' => 'chiefIsChiefAccountant',
                'on' => [self::SCENARIO_USER_UPDATE],
            ],
            [
                ['chief_accountant_patronymic'],
                'required',
                'when' => function (Company $model) {
                    return !$model->getIsLikeIP()
                        && $this->chief_is_chief_accountant == false
                        && $this->has_chief_accountant_patronymic == false;
                },
                'on' => [self::SCENARIO_USER_UPDATE],
            ],
            [ // other
                ['kpp'], 'required',
                'when' => $oooClosure,
                'whenClient' => $oooClosureJs,
                'on' => [self::SCENARIO_USER_UPDATE],
            ],
//            [['egrip'], 'required',
//                'when' => $ipClosure,
//                'whenClient' => $ipClosureJs,
//                'on' => [self::SCENARIO_USER_UPDATE],
//            ],
            // OTHER RULES
            // company type
            [
                ['company_type_id'], 'exist',
                'targetClass' => CompanyType::class,
                'targetAttribute' => 'id',
                'filter' => ['in_company' => true],
                'when' => function (Company $model) {
                    return !$model->self_employed;
                },
                'message' => 'Тип компании указан неверно.',
            ],
            [['capital'],
                'number',
                'numberPattern' => Yii::$app->params['numberPattern'],
                'max' => Yii::$app->params['maxCashSum'] * 100, // with kopeck
                'tooBig' => 'Значение «{attribute}» не должно превышать ' . TextHelper::moneyFormat(Yii::$app->params['maxCashSum'], 2) . '.',
                'whenClient' => 'function(){}', // because of `123,00` - not valid float number (with comma) and there doesn't exist any methods to parse it on client.
            ],
            // other
            [['email',], 'string', 'max' => 45],
            [['phone'], PhoneValidator::className()],
            [['email',], 'email'],
            [['has_chief_patronymic', 'store_show_novelty_button', 'store_has_discount', 'show_popup_kub_do_it',
                'shared_kub_vk'], 'boolean'],
            [['has_chief_accountant_patronymic', 'show_popup_expose_other_documents', 'show_popup_business_analyse',
                'show_popup_product', 'show_popup_sale_increase', 'show_popup_import_products', 'show_waybill_button_in_invoice'], 'boolean'],
            // chief
            [['chief_post_name'], 'string', 'max' => 255],
            [['chief_is_chief_accountant'], 'boolean'],
            [ // chief [accountant] FIO/IP FIO
                [
                    'chief_lastname',
                    'chief_firstname',
                    'chief_patronymic',
                    'chief_accountant_lastname',
                    'chief_accountant_firstname',
                    'chief_accountant_patronymic',
                    'ip_lastname',
                    'ip_firstname',
                    'ip_patronymic',
                    'form_legal_en',
                    'lastname_en',
                    'firstname_en',
                    'patronymic_en',
                ],
                'string',
                'max' => 45,
            ],
            [ // house/housing/apartment types
                ['typeUsn', 'activities_id',
                    'import_xls_product_count', 'import_xls_service_count',
                ], 'integer',
            ],
            // bills and codes
            [
                ['okfs', 'okopf', 'fss', 'okved',],
                'string', 'max' => 45,
            ],
            [['oktmo'], 'match', 'pattern' => '|^\d{8}(\d{3})?$|', 'message' => 'Значение «{attribute}» должно иметь 8 или 11 цифр.'],
            [['okud'], 'string', 'length' => 8, 'max' => 8,],
            [['ogrn', 'egrip', 'inn', 'kpp', 'okpo', 'okogu', 'show_invoice_payment_popup_date'], 'integer'],
            [['ogrn'], 'string', 'length' => 13, 'max' => 13,],
            [['egrip'], 'string', 'length' => 15, 'max' => 15,],
            [['kpp'], 'string', 'length' => 9, 'max' => 9,],
            [['okogu'], 'string', 'length' => 7, 'max' => 7,],
            [['pfr', 'pfr_ip', 'pfr_employer'], 'string', 'length' => 12, 'max' => 12,],
            [['inn'], 'string', 'max' => 12,],
            [['inn'], 'string', 'length' => 10,
                'when' => $oooClosure,
                'whenClient' => $oooClosureJs,
            ],
            [['inn'], 'string', 'length' => 12,
                'when' => $likeIpClosure,
                'whenClient' => $likeIpClosureJs,
            ],
            [['okpo'], 'string', 'length' => 8,
                'when' => $oooClosure,
                'whenClient' => $oooClosureJs,
            ],
            [['okpo'], 'string', 'length' => 10,
                'when' => $ipClosure,
                'whenClient' => $ipClosureJs,
            ],
            [['okato'], 'string', 'length' => 11],
            // tax authority
            [['tax_authority_registration_date'], 'safe'],
            // patent
            [['ip_patent_city', 'patentDate', 'patentDateEnd'], 'safe'],
            // address_legal_city
            [['address_legal_city'], 'safe'],
            // files
            [['visitcard_url', 'logo_link', 'print_link', 'chief_signature_link', 'store_discount_another'], 'string', 'max' => 255],
            [['logoImage', 'printImage', 'chiefSignatureImage'], 'file', 'extensions' => 'gif, jpg, png, jpeg, bmp, pdf'],
            [
                [
                    'test',
                    'deleteLogoImage', 'deletePrintImage', 'deleteChiefSignatureImage', 'deleteChiefAccountantSignatureImage',
                    'deletePrintImageAll', 'deleteChiefSignatureImageAll', 'deleteChiefAccountantSignatureImageAll',
                    'printImageApply', 'chiefSignatureImageApply', 'chiefAccountantSignatureImageApply',
                    'show_reminder_7', 'show_reminder_5', 'show_reminder_3', 'show_reminder_1',
                    'show_service_more_stock_modal_first_rule', 'show_service_more_stock_modal_second_rule'
                ], 'boolean',
            ],
            ['time_zone_id', 'required'],
            [[
                'pdf_signed',
                'pdf_act_signed',
                'pdf_act_send_signed',
            ], 'boolean'],
            [['tmpId', 'form_source', 'form_keyword', 'form_region', 'form_date', 'form_entrance_page', 'form_sign_in_page', 'utm_source'], 'string'],
            [['form_hour'], 'safe'],
            [['form_unique_events_count'], 'integer'],
            [['documents_type'], 'in', 'range' => array_keys(self::$docsType)],
            [['affiliate_link'], 'string', 'max' => 255],
            [['affiliate_link'], 'unique',],
            [['invited_by_company_id'], 'integer'],
            [['invited_by_company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['invited_by_company_id' => 'id']],
            [['invited_by_product_id'], 'integer'],
            [['registration_page_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => RegistrationPageType::className(), 'targetAttribute' => ['registration_page_type_id' => 'id']],
            [
                [
                    'affiliate_sum',
                    'affiliate_link_created_at',
                    'affiliate_link_updated_at',
                    'affiliate_link_deleted_at',
                    'total_affiliate_sum',
                    'payed_reward_for_kub',
                    'payed_reward_for_rs',
                    'payed_reward_for_card',
                    'invited_companies_count',
                    'active_reward_requests',
                ],
                'integer',
            ],
            [['new_template_order'], 'integer'],
            [['chiefAccountantSignatureImage'], 'file', 'extensions' => 'png, bmp, jpg, jpeg'],
            [['new_template_order'], 'integer'],
            [['ifns_ga'], 'filter', 'filter' => function ($value) {
                return ltrim($value, '0');
            }],
            [['ifns_ga'], 'exist', 'targetClass' => Ifns::className(), 'targetAttribute' => 'ga'],
            [
                ['hasAccountants'],
                function ($attribute) {
                    if (!$this->getHasAccountants()) {
                        $this->addError($attribute, 'Необходимо указать хотя бы один расчетный счет.');
                    }
                },
                'on' => [self::SCENARIO_USER_UPDATE],
            ],
            [['ifns_ga'], 'exist', 'targetClass' => Ifns::className(), 'targetAttribute' => 'ga'],
            [
                ['is_additional_number_before'],
                'boolean',
                'on' => [self::SCENARIO_USER_UPDATE],
            ],
            // address
            [
                [
                    'address_legal',
                    'address_actual',
                    'name_short_en',
                    'name_full_en',
                    'form_legal_en',
                    'address_legal_en',
                    'address_actual_en',
                    'chief_post_name_en',
                    'lastname_en',
                    'firstname_en',
                    'patronymic_en',
                ],
                'trim',
            ],
            [
                [
                    'address_legal',
                ],
                'required',
                'message' => 'Необходимо заполнить почтовый индекс и адрес',
            ],
            [
                [
                    'address_legal',
                    'address_actual',
                    'name_short_en',
                    'name_full_en',
                    'address_legal_en',
                    'address_actual_en',
                    'chief_post_name_en',
                ],
                'string', 'max' => 255,
            ],
            [
                [
                    'address_legal',
                    'address_actual',
                ],
                'validateAddress',
            ],
            [
                ['ip_certificate_number', 'ip_certificate_issued_by'],
                'string',
                'max' => 255,
                'when' => $ipClosure,
                'whenClient' => $ipClosureJs,
                'on' => [self::SCENARIO_USER_UPDATE],
            ],
            [
                ['certificateDate',],
                'date',
                'when' => $ipClosure,
                'whenClient' => $ipClosureJs,
                'on' => [self::SCENARIO_USER_UPDATE],
            ],
            [['qiwi_public_key'], 'string', 'max' => 255],
            [['hide_widget_footer'], 'boolean'],
            /**
             * Check for black list
             */
            [
                'inn', 'unique',
                'targetClass' => 'common\models\company\InnDeny',
                'message' => 'Недопустимый ИНН',
            ],
            [
                ['info_industry_id',
                    'info_employers_count',
                    'info_sellers_count',
                    'info_site_id',
                    'info_has_shop',
                    'info_has_storage',
                    'info_shops_count',
                ],
                'integer',
            ],
            [
                'inn', 'validateByDadata',
                'on' => self::SCENARIO_TAX_ROBOT,
            ],
            [['is_partner', 'invoice_facture_print_template'], 'safe'],
            [
                [
                    'name_short_en',
                    'name_full_en',
                    'form_legal_en',
                    'address_legal_en',
                    'chief_post_name_en',
                    'lastname_en',
                    'firstname_en',
                    'patronymic_en',
                ],
                'required',
                'message' => 'Необходимо заполнить',
                'when' => function (Company $model) {
                    return (bool) $model->requiredEnAttributes;
                }
            ],
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateAddress($attribute, $params)
    {
        preg_match("/^(\d+)(.*)/u", $this->$attribute, $matches);

        if (empty($matches[1])) {
            $this->addError($attribute, 'Адрес должен начинаться с почтового индекса');
        } elseif (strlen($matches[1]) != 6) {
            $this->addError($attribute, 'Почтовый индекс заполнен не правильно');
        }

        $address = isset($matches[2]) ? mb_ereg_replace("[^A-zА-я]", "", $matches[2]) : '';
        if (strlen($address) < 3) {
            $this->addError($attribute, 'Адрес заполнен не правильно');
        }
    }

    /**
     * @return string
     */
    public function getLegalPostcode()
    {
        preg_match("/^(\d+)(.*)/u", $this->address_legal, $matches);

        if (isset($matches[1])) {
            return $matches[1];
        }

        return '';
    }

    /**
     * @return string
     */
    public function getLegalAddress()
    {
        return trim(mb_substr($this->address_legal, strlen($this->getLegalPostcode())), ", \t\n\r\0\x0B");
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateTaxationType($attribute, $params)
    {
        if (!$this->isNewRecord) {
            if ($this->hasNds($this->$attribute) !== $this->hasNds($this->getOldAttribute($attribute))) {
                $this->addError($attribute, 'Нельзя менять тип налогообложения');
            }
        }
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateByDadata($attribute, $params)
    {
        if (DadataClient::getCompanyData($this->$attribute, $this->kpp, true) === null) {
            $this->addError($attribute, 'Уточните ваш ИНН');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'strict_mode' => 'Режим ограниченной функциональности',
            'company_type_id' => 'Форма компании',
            'taxation_type_id' => 'Система налогообложения',
            'companyTaxationType' => 'Система налогообложения',
            'name_full' => 'Полное наименование организации',
            'name_short' => 'Название организации',
            'address_legal' => $this->self_employed ? 'Фактический адрес' : 'Юридический адрес',
            'address_actual' => 'Фактический адрес',
            'companyProductTypes' => 'Реализуемая продукция',
            'address_legal_city' => 'Город (населённый пункт) (ЮА)',
            'address_legal_street' => 'Улица (ЮА)',
            'address_legal_house_type_id' => 'Тип дома (ЮА)',
            'address_legal_house' => 'Дом (ЮА)',
            'address_legal_housing_type_id' => 'Тип корпуса (ЮА)',
            'address_legal_housing' => 'Корпус (ЮА)',
            'address_legal_apartment_type_id' => 'Тип квартиры (ЮА)',
            'address_legal_apartment' => 'Квартира (ЮА)',
            'address_legal_postcode' => 'Почтовый индекс (ЮА)',
            'address_legal_is_actual' => 'ЮА совпадает с ФА',
            'address_actual_city' => 'Город (населённый пункт) (ФА)',
            'address_actual_street' => 'Улица (ФА)',
            'address_actual_house_type_id' => 'Тип дома (ФА)',
            'address_actual_house' => 'Дом (ФА)',
            'address_actual_housing_type_id' => 'Тип корпуса (ФА)',
            'address_actual_housing' => 'Корпус (ФА)',
            'address_actual_apartment_type_id' => 'Тип квартиры (ФА)',
            'address_actual_apartment' => 'Квартира (ФА)',
            'address_actual_postcode' => 'Почтовый индекс (ФА)',
            'visitcard_url' => 'Визитка компании',
            'phone' => 'Телефон',
            'email' => 'Электронная почта',
            'chief_post_name' => 'Должность руководителя',
            'chief_lastname' => 'Фамилия руководителя',
            'chief_firstname' => 'Имя руководителя',
            'chief_firstname_initials' => 'Имя руководителя (инициал)',
            'chief_patronymic' => 'Отчество руководителя',
            'chief_patronymic_initials' => 'Отчество руководителя (инициал)',
            'chief_is_chief_accountant' => 'Руководитель является главным бухгалтером',
            'chief_accountant_lastname' => 'Фамилия главного бухгалтера',
            'chief_accountant_firstname' => 'Имя главного бухгалтера',
            'chief_accountant_firstname_initials' => 'Имя главного бухгалтера (инициал)',
            'chief_accountant_patronymic' => 'Отчество главного бухгалтера',
            'chief_accountant_patronymic_initials' => 'Отчество главного бухгалтера (инициал)',
            'ogrn' => 'ОГРН',
            'egrip' => 'ОГРНИП',
            'inn' => 'ИНН',
            'kpp' => 'КПП',
            'ip_lastname' => $this->self_employed ? 'Фамилия' : 'Фамилия ИП',
            'ip_firstname' => $this->self_employed ? 'Имя' : 'Имя ИП',
            'ip_firstname_initials' => 'Имя ИП (инициал)',
            'ip_patronymic' => $this->self_employed ? 'Отчество' : 'Отчество ИП',
            'ip_patronymic_initials' => 'Отчество ИП (инициал)',
            'okpo' => 'ОКПО',
            'okogu' => 'ОКОГУ',
            'okato' => 'ОКАТО',
            'okved' => 'ОКВЭД',
            'okfs' => 'ОКФС',
            'okopf' => 'ОКОПФ',
            'oktmo' => 'ОКТМО',
            'okud' => 'ОКУД',
            'ifns_ga' => 'код ИФНС',
            'pfr' => 'ПФР',
            'fss' => 'ФСС',
            'pfr_ip' => 'ПФР (как ИП)',
            'pfr_employer' => 'ПФР (как работодателя)',
            'tax_authority_registration_date' => 'Дата постановки на учёт в налоговом органе',
            'logo_link' => 'Логоформа компании',
            'print_link' => 'Печать компании',
            'chief_signature_link' => 'Подпись руководителя',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата редактирования',
            'last_visit_at' => 'Последнее посещение',
            'blocked' => 'Статус',
            'active_subscribe_id' => 'Активная подписка',
            'time_zone_id' => 'Часовой пояс',
            'email_messages_send_visit_card' => 'Количество отправленных визиток на e-mail',
            'nds' => 'НДС для ОСНО',
            'pdf_signed' => 'Для счета',
            'pdf_send_signed' => 'Счет',
            'pdf_act_signed' => 'Для акта',
            'pdf_act_send_signed' => 'Акт',
            'test' => 'Тестовый аккаунт',
            'documents_type' => 'К счету формировать',
            'documentsType' => 'К счету формировать',
            'deleteChiefAccountantSignatureImage' => 'Удалить',
            'new_template_order' => 'Отображения страницы счета',
            'has_chief_patronymic' => 'Нет отчества',
            'is_additional_number_before' => 'К номеру счета Доп номер',
            'chief_fio' => 'ФИО руководителя',
            'ip_certificate_number' => 'Серия и номер свидетельства',
            'ip_certificate_issued_by' => 'Кем выдано',
            'ip_certificate_date' => 'Дата',
            'ip_patent_city' => 'Наименование субъекта РФ, в котором получен патент',
            'patentDate' => 'Дата начала действия патента',
            'patentDateEnd' => 'Дата окончания действия патента',
            'certificateDate' => 'Дата',
            'qiwi_public_key' => 'Ключ идентификации мерчанта в QIWI',
            'hide_widget_footer' => 'Скрыть футер виджетов',
            'capital' => 'Уставный капитал',
            'shortTitle' => 'Наименование',
            'info_industry_id' => 'Отрасль',
            'info_site_id' => 'Наличие сайта',
            'info_has_shop' => 'Наличие торговой точки (розничного магазина)',
            'info_has_storage' => 'Наличие склада',
            'info_shops_count' => 'Кол-во торговых точек',
            'info_sellers_count' => 'Кол-во продавцов в компании',
            'info_employers_count' => 'Кол-во сотрудников в компании',
            'deletePrintImageAll' => 'Удалить из всех документов',
            'deleteChiefSignatureImageAll' => 'Удалить из всех документов',
            'deleteChiefAccountantSignatureImageAll' => 'Удалить из всех документов',
            'ofdTypes' => 'ОФД',
            'acquiringAccounts' => 'Эквайринг',
            'onlinePaymentTypes' => 'Прием платежей на сайте',
            'accountingSystemTypes' => 'Учетная система',
            'name_short_en' => 'Название организации',
            'name_full_en' => 'Полное наименование',
            'form_legal_en' => 'Форма',
            'address_legal_en' => 'Юридический адрес',
            'address_actual_en' => 'Фактический адрес',
            'chief_post_name_en' => 'Должность',
            'lastname_en' => 'Фамилия',
            'firstname_en' => 'Имя',
            'patronymic_en' => 'Отчество',
            'not_has_patronymic_en' => 'Нет отчества',
        ];
    }

    public function getGoogleAnalyticsProfile(): ActiveQuery
    {
        return $this->hasOne(GoogleAnalyticsProfile::class, ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApiFirstVisits()
    {
        return $this->hasMany(ApiFirstVisit::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutoact()
    {
        return $this->hasOne(Autoact::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutoinvoicefacture()
    {
        return $this->hasOne(Autoinvoicefacture::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyIndustries()
    {
        return $this->hasMany(CompanyIndustry::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyType()
    {
        return $this->hasOne(CompanyType::className(), ['id' => 'company_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyActivity()
    {
        return $this->hasOne(CompanyActivity::className(), ['id' => 'company_activity_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoreUser()
    {
        return $this->hasOne(StoreUser::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyTaxationType()
    {
        return $this->hasOne(CompanyTaxationType::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyProductTypes()
    {
        return $this->hasMany(CompanyProductType::className(), ['id' => 'company_product_type_id'])
            ->viaTable('company_to_company_product_type', ['company_id' => 'id']);
    }

    public function getIncomeFlowOfFundsItems()
    {
        return $this->hasMany(IncomeItemFlowOfFunds::class, ['company_id' => 'id']);
    }

    public function getExpenditureFlowOfFundsItems()
    {
        return $this->hasMany(ExpenseItemFlowOfFunds::class, ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddressActualApartmentType()
    {
        return $this->hasOne(address\AddressApartmentType::className(), ['id' => 'address_actual_apartment_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddressActualHouseType()
    {
        return $this->hasOne(address\AddressHouseType::className(), ['id' => 'address_actual_house_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddressActualHousingType()
    {
        return $this->hasOne(address\AddressHousingType::className(), ['id' => 'address_actual_housing_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivities()
    {
        return $this->hasOne(Activities::className(), ['id' => 'activities_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddressLegalApartmentType()
    {
        return $this->hasOne(address\AddressApartmentType::className(), ['id' => 'address_legal_apartment_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddressLegalHouseType()
    {
        return $this->hasOne(address\AddressHouseType::className(), ['id' => 'address_legal_house_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddressLegalHousingType()
    {
        return $this->hasOne(address\AddressHousingType::className(), ['id' => 'address_legal_housing_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCardAccounts()
    {
        return $this->hasMany(CardAccount::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCardBills()
    {
        return $this->hasMany(CardBill::className(), ['account_id' => 'id'])->via('cardAccounts');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxationType()
    {
        return $this->hasOne(TaxationType::className(), ['id' => 'taxation_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyLastVisits()
    {
        return $this->hasMany(CompanyLastVisit::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContractors()
    {
        return $this->hasMany(Contractor::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDossierLogs()
    {
        return $this->hasMany(DossierLog::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployees()
    {
        return $this->hasMany(Employee::className(), ['id' => 'employee_id'])->viaTable('employee_company', ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployeeCompanies()
    {
        return $this->hasMany(EmployeeCompany::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployees0()
    {
        return $this->hasMany(Employee::className(), ['id' => 'employee_id'])->viaTable('employee_company', ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntities()
    {
        return $this->hasMany(Entity::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRentAgreements()
    {
        return $this->hasMany(RentAgreement::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEntityRents()
    {
        return $this->getRentAgreements();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwnerEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'owner_employee_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployeeCount()
    {
        return $this->hasOne(EmployeeCount::className(), ['id' => 'employee_count_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActiveSubscribe($groupId = SubscribeTariffGroup::STANDART)
    {
        return $this->hasOne(Subscribe::className(), ['id' => 'subscribe_id'])
            ->via('companyActiveSubscribes', function ($query) use ($groupId) {
                return $query->andOnCondition(['company_active_subscribe.tariff_group_id' => $groupId]);
            });
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyActiveSubscribes()
    {
        return $this->hasMany(ActiveSubscribe::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariffGroups()
    {
        return $this->hasMany(SubscribeTariffGroup::className(), ['id' => 'tariff_group_id'])->via('companyActiveSubscribes');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActiveSubscribes()
    {
        return $this->hasMany(Subscribe::className(), ['id' => 'subscribe_id'])->via('companyActiveSubscribes');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscribes()
    {
        return $this->hasMany(Subscribe::className(), ['company_id' => 'id']);
    }

    /**
     * @param null $productionType
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedPriceLists()
    {
        return $this->hasMany(PriceList::className(), ['company_id' => 'id']);
    }

    /**
     * @param null $productionType
     * @return \yii\db\ActiveQuery
     */
    public function getPriceLists($productionType = null)
    {
        $query = $this->getCreatedPriceLists()->andOnCondition(['is_finished' => 1]);
        if ($productionType !== null) {
            $query->andOnCondition(['production_type' => $productionType]);
        }

        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimeZone()
    {
        return $this->hasOne(TimeZone::className(), ['id' => 'time_zone_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNdsOsno()
    {
        return $this->hasOne(NdsOsno::className(), ['id' => 'nds']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasMany(File::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDetailsFiles()
    {
        return $this->hasMany(DetailsFile::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashFlowLinkages()
    {
        return $this->hasMany(CashFlowLinkage::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyAffiliateLinks()
    {
        return $this->hasMany(CompanyAffiliateLink::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBank()
    {
        return BikDictionary::findOne(['bik' => (int)$this->mainCheckingAccountant->bik]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForeignCurrencyCardBills()
    {
        return $this->getCardBills()->andWhere([
            'not', ['currency_id' => Currency::DEFAULT_ID]
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForeignCurrencyInvoices()
    {
        return $this->hasMany(ForeignCurrencyInvoice::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoices()
    {
        return $this->hasMany(Invoice::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderDocuments()
    {
        return $this->hasMany(OrderDocument::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductGroups()
    {
        return $this->hasMany(ProductGroup::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjects()
    {
        return $this->hasMany(Project::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInquirers()
    {
        return $this->hasMany(Inquirer::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvitedCompanies()
    {
        return $this->hasMany(Company::className(), ['invited_by_company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIfns()
    {
        return $this->hasOne(Ifns::className(), ['ga' => 'ifns_ga']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayments()
    {
        return $this->hasMany(Payment::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscounts()
    {
        $query = Discount::find()->andWhere([
            'or',
            ['company_id' => $this->id],
            ['company_id' => null],
        ]);
        $query->multiple = true;

        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashboxes()
    {
        return $this->hasMany(Cashbox::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRubleCashboxes()
    {
        return $this->getCashboxes()->andOnCondition([
            'currency_id' => Currency::DEFAULT_ID,
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForeignCurrencyCashboxes()
    {
        return $this->getCashboxes()->andOnCondition([
            'not', ['currency_id' => Currency::DEFAULT_ID]
        ])->andOnCondition([
            'not', ['currency_id' => null]
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmoneys()
    {
        return $this->hasMany(Emoney::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRubleEmoneys()
    {
        return $this->getEmoneys()->andOnCondition([
            'currency_id' => Currency::DEFAULT_ID,
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForeignCurrencyEmoneys()
    {
        return $this->getEmoneys()->andOnCondition([
            'not', ['currency_id' => Currency::DEFAULT_ID]
        ])->andOnCondition([
            'not', ['currency_id' => null]
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmoney()
    {
        return $this->hasOne(Emoney::className(), ['company_id' => 'id'])->orderBy([
            'is_closed' => SORT_ASC,
            'is_main' => SORT_DESC,
            'id' => SORT_ASC,
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfdImportConfig()
    {
        return $this->hasOne(OfdImportConfig::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfdImportGeneralCashbox()
    {
        return $this->hasOne(OfdImportGeneralCashbox::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfdTypes()
    {
        return $this->hasMany(OfdType::className(), ['id' => 'ofd_id'])
            ->viaTable('company_to_ofd_type', ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfdUsers()
    {
        return $this->hasMany(OfdUser::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfdUsersByOfd($ofd)
    {
        return $this->getOfdUsers()->andWhere([
            'ofd_id' => $ofd->id,
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcquiringAccounts()
    {
        return $this->hasMany(CheckingAccountant::className(), ['id' => 'account_id'])
            ->viaTable('company_to_acquiring', ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOnlinePaymentTypes()
    {
        return $this->hasMany(OnlinePaymentType::className(), ['id' => 'payment_type_id'])
            ->viaTable('company_to_online_payment_type', ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccountingSystemTypes()
    {
        return $this->hasMany(AccountingSystemType::className(), ['id' => 'accounting_system_id'])
            ->viaTable('company_to_accounting_system_type', ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScanDocuments()
    {
        return $this->hasMany(ScanDocument::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStores()
    {
        return $this->hasMany(Store::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentOrders()
    {
        return $this->hasMany(PaymentOrder::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfdStores()
    {
        return $this->hasMany(OfdStore::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfdKkts()
    {
        return $this->hasMany(OfdKkt::className(), ['company_id' => 'id']);
    }

    /**
     * @return $this
     */
    public function getMainStore()
    {
        return $this->hasOne(Store::className(), ['company_id' => 'id'])
            ->onCondition(['is_main' => true]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStore($id = null)
    {
        $t = Store::tableName();
        $id = $id ?: -1;

        return $this->hasOne(Store::className(), ['company_id' => 'id'])->orderBy([
            "IF({{{$t}}}.[[id]] = {$id}, 1, 0)" => SORT_DESC,
            "{{{$t}}}.[[is_main]]" => SORT_DESC,
        ]);
    }

    /**
     * @param $employeeID
     * @return array|null|EmailSignature
     */
    public function getEmailSignature($employeeID)
    {
        return $this->hasOne(EmailSignature::className(), ['company_id' => 'id'])
            ->onCondition(['employee_id' => $employeeID])->one();
    }

    /**
     * @param $employeeID
     * @return array|null|EmailTemplate[]
     */
    public function getEmailTemplates($employeeID)
    {
        return $this->hasMany(EmailTemplate::className(), ['company_id' => 'id'])
            ->onCondition(['employee_id' => $employeeID])->all();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserEmails()
    {
        return $this->hasMany(UserEmail::className(), ['company_id' => 'id']);
    }

    /**
     * @param $employeeID
     * @return array|null|EmailTemplate
     */
    public function getActiveEmailTemplate($employeeID)
    {
        return $this->hasOne(EmailTemplate::className(), ['company_id' => 'id'])
            ->onCondition(['status' => EmailTemplate::STATUS_ACTIVE])
            ->onCondition(['employee_id' => $employeeID])->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFirstPayment()
    {
        return $this->hasOne(Payment::className(), ['company_id' => 'id'])->onCondition(['and',
            ['in', 'tariff_id', SubscribeTariff::paidStandartIds()],
            ['promo_code_id' => null],
            ['is_confirmed' => true],
        ])->orderBy(['payment_date' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLastPayment()
    {
        return $this->hasOne(Payment::className(), ['company_id' => 'id'])->onCondition(['and',
            ['in', 'tariff_id', SubscribeTariff::paidStandartIds()],
            ['promo_code_id' => null],
            ['is_confirmed' => true],
        ])->orderBy(['payment_date' => SORT_DESC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyImageUploadLogs()
    {
        return $this->hasMany(CompanyImageUploadLog::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBankUsers()
    {
        return $this->hasMany(BankUser::className(), ['company_id' => 'id']);
    }

    /**
     * @return int
     */
    public function getPayedInvitedCompaniesCount()
    {
        $count = 0;
        foreach ($this->invitedCompanies as $invitedCompany) {
            if (Subscribe::find()
                ->byCompany($invitedCompany->id)
                ->andWhere(['in', 'tariff_id', SubscribeTariff::paidStandartIds()])
                ->andWhere(['in', 'status_id', [SubscribeStatus::STATUS_PAYED,
                    SubscribeStatus::STATUS_ACTIVATED],
                ])->exists()
            ) {
                $count++;
            }
        }

        return $count;
    }

    /**
     * @param $companyID
     * @return bool
     */
    public function checkInvitedCompanyOnPayed($companyID)
    {
        return Subscribe::find()
            ->byCompany($companyID)
            ->andWhere(['in', 'tariff_id', SubscribeTariff::paidStandartIds()])
            ->andWhere(['in', 'status_id', [SubscribeStatus::STATUS_PAYED,
                SubscribeStatus::STATUS_ACTIVATED],
            ])->exists();
    }

    /**
     * sorting by company type and name query
     *
     * @return \yii\db\ActiveQuery
     */
    public static function getSorted()
    {
        $selfTable = self::tableName();
        $typeTable = CompanyType::tableName();

        return self::find()
            ->leftJoin($typeTable, "{{{$selfTable}}}.[[company_type_id]] = {{{$typeTable}}}.[[id]]")
            ->orderBy([
                new \yii\db\Expression("ISNULL({{{$typeTable}}}.[[name_short]])"),
                "$typeTable.name_short" => SORT_ASC,
                "$selfTable.name_short" => SORT_ASC,
            ]);
    }

    /**
     * @param bool $short
     * @return string
     * @throws NotFoundHttpException
     */
    public function getTitle($short = false, $ipNameFull = false)
    {
        if ($this->self_employed) {
            $type = 'Самозанятый';
        } elseif ($this->companyType !== null) {
            $type = $short ? $this->companyType->name_short : $this->companyType->name_full;
        }

        if ($this->getIsLikeIP()) {
            $name = $this->getIpFio($ipNameFull ? false : $short);
        } else {
            $name = $short ? $this->name_short : $this->name_full;
        }

        return isset($type) ? $type . ' ' . $name : $name;
    }

    /**
     * @return string
     */
    public function getShortTitle()
    {
        return $this->getTitle(true);
    }

    /**
     * @return string
     */
    public function getTitleEn()
    {
        return $this->name_full_en;
    }

    /**
     * @return string
     */
    public function getShortTitleEn()
    {
        return $this->form_legal_en.' '. $this->name_short_en;
    }

    /**
     * @return Employee
     */
    public function getEmployeeChief($all = false)
    {
        $query = Employee::find()
            ->leftJoin('employee_company', '{{employee}}.[[id]] = {{employee_company}}.[[employee_id]]')
            ->leftJoin('company', '{{employee_company}}.[[company_id]] = {{company}}.[[id]]')
            ->where([
                'employee.is_active' => Employee::ACTIVE,
                'employee.is_deleted' => Employee::NOT_DELETED,
                'employee_company.is_working' => true,
                'employee_company.employee_role_id' => EmployeeRole::ROLE_CHIEF,
                'company.id' => $this->id,
            ])
            ->orderBy([
                '({{company}}.[[owner_employee_id]] = {{employee}}.[[id]])' => SORT_DESC,
                'employee_company.created_at' => SORT_ASC,
            ]);

        return $all ? $query->all() : $query->one();
    }

    /**
     * @param bool $all
     * @return EmployeeCompany[]|EmployeeCompany
     */
    public function getChiefEmployeeCompany($all = false)
    {
        $query = $this->getEmployeeCompanies()->joinWith(['employee', 'company'], false)->andWhere([
            'employee.is_active' => Employee::ACTIVE,
            'employee.is_deleted' => Employee::NOT_DELETED,
            'employee_company.is_working' => true,
            'employee_company.employee_role_id' => EmployeeRole::ROLE_CHIEF,
        ])->orderBy([
            '({{company}}.[[owner_employee_id]] = {{employee}}.[[id]])' => SORT_DESC,
            'employee_company.created_at' => SORT_ASC
        ]);

        return $all ? $query->all() : $query->one();
    }

    /**
     * @param bool $short
     *
     * @return string
     */
    public function getChiefFio($short = false)
    {
        return $this->getFio('chief', $short);
    }

    /**
     * @param bool $short
     *
     * @return string
     */
    public function getChiefAccountantFio($short = false)
    {
        return $this->chief_is_chief_accountant ? $this->getChiefFio($short) : $this->getFio('chief_accountant', $short);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSellingSubscribes()
    {
        return $this->hasMany(SellingSubscribe::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServiceMoreStock()
    {
        return $this->hasOne(ServiceMoreStock::className(), ['company_id' => 'id']);
    }

    /**
     * @param bool $short
     *
     * @return string
     */
    public function getIpFio($short = false)
    {
        return $this->getFio('ip', $short);
    }

    /**
     * @return int|string
     */
    public function getOutInvoiceCount()
    {
        return Invoice::find()->byIOType(Documents::IO_TYPE_OUT)->byDeleted()->byCompany($this->id)->count();
    }

    /**
     * @return int|string
     */
    public function getOutActCount()
    {
        return Act::find()->byIOType(Documents::IO_TYPE_OUT)->byCompany($this->id)->count();
    }

    /**
     * @return int|string
     */
    public function getOutPackingListCount()
    {
        return PackingList::find()->byIOType(Documents::IO_TYPE_OUT)->byCompany($this->id)->count();
    }

    /**
     * @return int|string
     */
    public function getOutInvoiceFactureCount()
    {
        return InvoiceFacture::find()->byIOType(Documents::IO_TYPE_OUT)->byCompany($this->id)->count();
    }

    /**
     * @return int|string
     */
    public function getOutUpdCount()
    {
        return Upd::find()->byIOType(Documents::IO_TYPE_OUT)->byCompany($this->id)->andWhere([
            Invoice::tableName() . '.is_deleted' => false,
        ])->count();
    }

    /**
     * @return int|string
     */
    public function getInInvoiceCount()
    {
        return Invoice::find()->byIOType(Documents::IO_TYPE_IN)->byDeleted()->byCompany($this->id)->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExport()
    {
        return $this->hasMany(Export::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRegistrationPageType()
    {
        return $this->hasOne(RegistrationPageType::className(), ['id' => 'registration_page_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUploadedDocuments()
    {
        return $this->hasMany(UploadedDocuments::className(), ['company_id' => 'id'])->orderBy(['created_at' => SORT_DESC]);
    }

    /**
     * @return int|string
     */
    public function getStateFromBankCount()
    {
        return CashBankStatementUpload::find()
            ->joinWith('company')
            ->andWhere([
                Company::tableName() . '.id' => $this->id,
            ])->count();
    }

    /**
     * @return string
     */
    public function getStatementFromBank()
    {
        $manual = $this->getCashBankStatementUploads()
            ->andWhere([CashBankStatementUpload::tableName() . '.source' => CashBankStatementUpload::SOURCE_BANK])
            ->count();
        $auto = $this->getCashBankStatementUploads()
            ->andWhere([CashBankStatementUpload::tableName() . '.source' => CashBankStatementUpload::SOURCE_BANK_AUTO])
            ->count();

        return "{$manual} ({$auto})";
    }

    /**
     * @return string
     */
    public function getBankIntegration()
    {
        return "";
    }

    /**
     * @return int|string
     */
    public function getOneCExportCount()
    {
        return Export::find()
            ->joinWith('employee')
            ->leftJoin('company', 'company.id = ' . Employee::tableName() . '.company_id')
            ->andWhere([Company::tableName() . '.id' => $this->id])
            ->count();
    }

    /**
     * @return int|string
     */
    public function getOutInvoiceSendEmailCount()
    {
        return Invoice::find()
            ->byIOType(Documents::IO_TYPE_OUT)
            ->byCompany($this->id)
            ->byDeleted()
            ->andWhere(['and',
                ['>', 'email_messages', 0],
                ['is not', 'email_messages', null],
            ])->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvitedByCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'invited_by_company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDrivers()
    {
        return $this->hasMany(Driver::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehicles()
    {
        return $this->hasMany(Vehicle::className(), ['company_id' => 'id']);
    }

    /**
     * @return int|mixed
     */
    public function getExpireSubscribeTimestamp()
    {
        if ($this->_expireSubscribeTimestamp === null) {
            $subscribeArray = SubscribeHelper::getPayedSubscriptions($this->id);
            $this->_expireSubscribeTimestamp = SubscribeHelper::getExpireDate($subscribeArray);
        }

        return $this->_expireSubscribeTimestamp;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInfoIndustry()
    {
        return $this->hasOne(CompanyInfoIndustry::class, ['id' => 'info_industry_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInfoPlace()
    {
        return $this->hasOne(CompanyInfoPlace::class, ['id' => 'info_place_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInfoSite()
    {
        return $this->hasOne(CompanyInfoSite::class, ['id' => 'info_site_id']);
    }

    /**
     * @return int|mixed
     */
    public function getExpireSubscribeDate()
    {
        return date(DateHelper::FORMAT_USER_DATE, $this->expireSubscribeTimestamp);
    }

    /**
     * @return int|mixed
     */
    public function getActiveSubscribeDaysCount()
    {
        return SubscribeHelper::getExpireLeftDays($this->expireSubscribeTimestamp);
    }

    /**
     * @return int
     */
    public function getNotPaidInvoiceCount()
    {
        return (int)Subscribe::find()
            ->byCompany($this->id)
            ->byStatus([SubscribeStatus::STATUS_NEW])
            ->count();
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if (Yii::$app->request instanceof \yii\web\Request) {
            $path = parse_url(Yii::$app->request->referrer, PHP_URL_PATH);
            $tmpPath = [
                '/company/update',
                '/company/continue-create',
            ];
            if (in_array($path, $tmpPath)) {
                foreach (array_keys(self::$imageAttrArray) as $attribute) {
                    CompanyHelper::moveFromTmp($this, $attribute);
                }
            }

            foreach (array_keys(self::$imageAttrArray) as $attribute) {
                if ($this->{'delete' . ucfirst($attribute)}) {
                    CompanyHelper::deleteImage($this, $attribute, true);
                    CompanyHelper::deleteImage($this, $attribute);
                    $this->{self::$imageAttrArray[$attribute]} = null;
                }
                if ($this->$attribute instanceof UploadedFile) {
                    CompanyHelper::saveImage($this, $attribute, $this->$attribute);
                }
                if ($this->{$attribute . 'Apply'} == 1) {
                    $image = $this->getImageModel($attribute);
                    $image->applyForAll();
                }
            }
        }

        if (!$this->getAutoact()->exists()) {
            (new Autoact([
                'company_id' => $this->id,
                'rule' => Autoact::MANUAL,
            ]))->loadDefaultValues()->save();
        }
        if (!$this->getAutoinvoicefacture()->exists()) {
            (new Autoinvoicefacture([
                'company_id' => $this->id,
                'rule' => Autoinvoicefacture::MANUAL,
            ]))->loadDefaultValues()->save();
        }

        if ($insert) {
            /* @var $incomeItem InvoiceIncomeItem */
            foreach (InvoiceIncomeItem::find()
                         ->andWhere(['or',
                             ['company_id' => null],
                             ['company_id' => $this->id],
                         ])
                         ->all() as $incomeItem) {
                $incomeItemFlowOfFunds = new IncomeItemFlowOfFunds();
                $incomeItemFlowOfFunds->company_id = $this->id;
                $incomeItemFlowOfFunds->income_item_id = $incomeItem->id;
                $incomeItemFlowOfFunds->is_visible = $incomeItem->is_visible;
                $incomeItemFlowOfFunds->is_prepayment = $incomeItem->is_prepayment;
                $incomeItemFlowOfFunds->save();
            }
            /* @var $expenseItem InvoiceExpenditureItem */
            foreach (InvoiceExpenditureItem::find()
                         ->andWhere(['or',
                             ['company_id' => null],
                             ['company_id' => $this->id],
                         ])
                         ->all() as $expenseItem) {
                $expenseItemFlowOfFunds = new ExpenseItemFlowOfFunds();
                $expenseItemFlowOfFunds->company_id = $this->id;
                $expenseItemFlowOfFunds->expense_item_id = $expenseItem->id;
                $expenseItemFlowOfFunds->is_visible = $expenseItem->is_visible;
                $expenseItemFlowOfFunds->is_prepayment = $expenseItem->is_prepayment;
                $expenseItemFlowOfFunds->save();
            }

            $planCashRule = new PlanCashRule([
                'flow_type' => CashFlowsBase::FLOW_TYPE_INCOME,
                'company_id' => $this->id,
                'income_item_id' => PlanCashRule::ALL_INCOME_ITEMS,
                'expenditure_item_id' => null,
                'created_at' => time(),
                'updated_at' => time()
            ]);
            $planCashRule->save();

            $planCashRule = new PlanCashRule([
                'flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE,
                'company_id' => $this->id,
                'income_item_id' => null,
                'expenditure_item_id' => PlanCashRule::ALL_EXPENDITURE_ITEMS,
                'created_at' => time(),
                'updated_at' => time()
            ]);
            $planCashRule->save();

            /* @var $cashbox Cashbox */
            if (!$this->getCashboxes()->andWhere(['is_main' => true])->exists()) {
                $cashbox = new Cashbox;
                $cashbox->company_id = $this->id;
                $cashbox->is_accounting = true;
                $cashbox->is_main = true;
                $cashbox->is_closed = false;
                $cashbox->name = 'Касса';
                $cashbox->save(false);
            }
            /* @var $store Store */
            if (!$this->getStores()->andWhere(['is_main' => true])->exists()) {
                $store = new Store;
                $store->company_id = $this->id;
                $store->is_main = true;
                $store->is_closed = false;
                $store->name = 'Склад';
                $store->save(false);
            }
            /* @var $store Store */
            if (!$this->getEmoneys()->andWhere(['is_main' => true])->exists()) {
                $emoney = new Emoney;
                $emoney->company_id = $this->id;
                $emoney->name = 'E-money';
                $emoney->is_main = true;
                $emoney->is_accounting = false;
                $emoney->is_closed = false;
                $emoney->save(false);
            }

            Yii::$app->queue->push(CreateCompanyNotificationsFromCompany::create($this->id));
            Yii::$app->queue->push(CreatePaymentReminderMessages::create($this->id));

            $prepaymentWallets = new \common\models\company\CompanyPrepaymentWallets;
            $prepaymentWallets->company_id = $this->id;
            $prepaymentWallets->bank = true;
            $prepaymentWallets->order = true;
            $prepaymentWallets->emoney = false;
            $prepaymentWallets->save(false);
        }

        if (isset($changedAttributes['strict_mode']) && $this->strict_mode == 0) {
            \common\models\company\CompanyFirstEvent::checkEvent($this, 1);
            /**
             * If the owner of this company has paid MTS subscriptions, activate them
             */
            if ($mtsSubscribes = ArrayHelper::getValue($this, 'ownerEmployee.mtsUser.mtsSubscribes')) {
                $ids = $this->ownerEmployee->getCompaniesByChief()->select('id')->column();
                $count = count($ids);
                foreach ($mtsSubscribes as $item) {
                    if ($item->company_number == $count && $item->purchase_inn == $this->inn) {
                        $item->createPayment($this, $ids);
                    }
                }
            }

            // Update contact in GetResponse service
            $gr = new GetResponseApi();
            $gr->updateGetResponseContact($this->ownerEmployee, [
                'status' => GetResponseApi::STATUS_FILLED_PROFILE
            ]);
        }
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            if ($this->scenario == self::SCENARIO_CREATE_COMPANY) {
                $this->self_employed = $this->company_type_id == CompanyType::TYPE_EMPTY;
            }

            if ($this->getIsLikeIP()) {
                //    TODO разобраться с именем компании
                $this->name_full = $this->getIpFio(false);
                $this->name_short = $this->getIpFio(true);

                if (empty($this->chief_post_name)) {
                    $this->chief_post_name = $this->self_employed ? 'Самозанятый' : 'Предприниматель';
                }
                $this->chief_lastname = $this->ip_lastname;
                $this->chief_firstname = $this->ip_firstname;
                $this->chief_patronymic = $this->ip_patronymic;
            } elseif (in_array($this->company_type_id, [CompanyType::TYPE_OOO, CompanyType::TYPE_ZAO, CompanyType::TYPE_PAO, CompanyType::TYPE_OAO,])) {

            }

            $this->chiefAccountantSignatureImage = UploadedFile::getInstance($this, 'chiefAccountantSignatureImage');

            $this->capital = TextHelper::parseMoneyInput($this->capital);

            if ($this->isAttributeChanged('capital') && $this->capital == $this->capitalInput) {
                $this->capital = round($this->capital * 100);
            }

            return true;
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function afterValidate()
    {
        parent::afterValidate();

        if (!$this->getIsNewRecord() && $this->inn != $this->getOldAttribute('inn') && $this->getAnyPaidSubscriptions()->exists()) {
            $this->addError('inn', 'Нельзя изменить ИНН компании, у которой был оплачен сервис.');
        }
    }

    /**
     * @return array
     */
    public function getTaxationTypeNewCompany()
    {
        $taxation = ArrayHelper::map(TaxationType::find()->andWhere(['!=', 'id', TaxationType::TYPE_USN_15])->all(), 'id', 'name');
        $taxation[TaxationType::TYPE_USN_6] = 'УСН';

        return $taxation;
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->blocked = self::UNBLOCKED;
                if ($this->default_contractor_income_item_id == null) {
                    $this->default_contractor_income_item_id = InvoiceIncomeItem::ITEM_PAYMENT_FROM_BUYER;
                }

                if ($this->default_contractor_expenditure_item_id == null) {
                    $this->default_contractor_expenditure_item_id = InvoiceExpenditureItem::ITEM_CONTRACTOR_PAYMENT;
                }

                if ($this->form_utm) {
                    parse_str($this->form_utm, $utmArr);
                    $this->utm_source = $utmArr['utm_source'] ?? null;
                }
            }

            if ($this->address_legal_is_actual) {
                $this->address_actual_city = $this->address_actual_street = $this->address_actual_house_type_id =
                $this->address_actual_house = $this->address_actual_housing_type_id = $this->address_actual_housing =
                $this->address_actual_apartment_type_id = $this->address_actual_apartment = $this->address_actual_postcode = null;
            }

            //Initials
            $this->ip_firstname_initials = $this->ip_firstname ?
                mb_substr($this->ip_firstname, 0, 1) : null;
            $this->ip_patronymic_initials = $this->ip_patronymic ?
                mb_substr($this->ip_patronymic, 0, 1) : null;
            $this->chief_firstname_initials = $this->chief_firstname ?
                mb_substr($this->chief_firstname, 0, 1) : null;
            $this->chief_patronymic_initials = $this->chief_patronymic ?
                mb_substr($this->chief_patronymic, 0, 1) : null;
            $this->chief_accountant_firstname_initials = $this->chief_accountant_firstname ?
                mb_substr($this->chief_accountant_firstname, 0, 1) : null;
            $this->chief_accountant_patronymic_initials = $this->chief_accountant_patronymic ?
                mb_substr($this->chief_accountant_patronymic, 0, 1) : null;

            if ($this->company_type_id != CompanyType::TYPE_IP) {
                if ($this->chief_is_chief_accountant) {
                    $this->chief_accountant_lastname = $this->chief_lastname;
                    $this->chief_accountant_firstname = $this->chief_firstname;
                    $this->chief_accountant_firstname_initials = $this->chief_firstname_initials;
                    $this->has_chief_accountant_patronymic = $this->has_chief_patronymic;
                    $this->chief_accountant_patronymic = $this->chief_patronymic;
                    $this->chief_accountant_patronymic_initials = $this->chief_patronymic_initials;

                }
            }

            if (empty($this->name_full)) {
                $this->name_full = $this->name_short;
            }
            if (empty($this->address_actual)) {
                $this->address_actual = $this->address_legal;
            }

            if (!$this->getIsLikeIP()) {
                if ($this->isAttributeChanged('name_full') &&
                    empty($this->getOldAttribute('name_full')) &&
                    strpos($this->name_full, '"') === false
                ) {
                    $this->name_full = '"' . $this->name_full . '"';
                }
                if ($this->isAttributeChanged('name_short') &&
                    empty($this->getOldAttribute('name_short')) &&
                    strpos($this->name_short, '"') === false
                ) {
                    $this->name_short = '"' . $this->name_short . '"';
                }
            }

            if ($insert || $this->isAttributeChanged('nds')) {
                if ($this->nds === null && $this->companyTaxationType && $this->companyTaxationType->osno) {
                    $this->nds = NdsOsno::WITH_NDS;
                }
                if ($this->nds == NdsOsno::WITH_NDS) {
                    $this->nds_view_type_id = NdsViewType::NDS_VIEW_IN;
                } elseif ($this->nds == NdsOsno::WITHOUT_NDS) {
                    $this->nds_view_type_id = NdsViewType::NDS_VIEW_OUT;
                } else {
                    $this->nds_view_type_id = NdsViewType::NDS_VIEW_WITHOUT;
                }
            }

            if ($this->isAttributeChanged('inn')) {
                $this->checkTaxRegDate();
            } else {
                if ($this->isAttributeChanged('tax_authority_registration_date')) {
                    $oldValue = $this->getOldAttribute('tax_authority_registration_date');
                    if (!empty($oldValue)) {
                        $this->tax_authority_registration_date = $oldValue;
                    }
                }
            }

            $this->strict_mode = $this->isStrictMode() ? self::ON_STRICT_MODE : self::OFF_STRICT_MODE;
            $this->strict_mode_date = $this->isStrictMode() ? $this->strict_mode_date : time();
            if (is_array($this->integration) === true) {
                $this->integration = json_encode($this->integration, JSON_UNESCAPED_UNICODE);
            }

            foreach (self::$requisites as $name) {
                if ($this->isAttributeChanged($name)) {
                    $this->requisites_updated_at = time();
                    break;
                }
            }

            return true;
        }

        return false;
    }

    /**
     * Returns array of values of strict mode
     * @return array
     */
    protected function getStrictModeAttributes()
    {
        return [
            //$this->name_full,
            $this->name_short,
            $this->companyType,
            $this->address_legal,
            //$this->chief_post_name,
            //$this->chief_lastname, $this->chief_firstname, $this->chief_patronymic,
            //$this->chief_firstname_initials, $this->chief_patronymic_initials,
            $this->inn,
            $this->mainAccountant,
        ];
    }

    /**
     * @return bool
     */
    public function isStrictMode()
    {
        return $this->checkEmpty($this->getStrictModeAttributes());
    }

    /**
     * @return array
     */
    protected function getRequiredForeignAttributes()
    {
        return [
            $this->name_short,
            $this->companyType,
            $this->address_legal,
            $this->inn,
            $this->name_short_en,
            $this->form_legal_en,
            $this->address_legal_en,
            $this->chief_post_name_en,
            $this->lastname_en,
            $this->firstname_en,
        ];
    }

    /**
     * @return bool
     */
    public function getIsForeignAttributesRequired()
    {
        return $this->checkEmpty($this->getRequiredForeignAttributes());
    }

    /**
     * Checks if any value in array is empty. Returns false if all are non-empty.
     * @param $array
     * @return bool
     */
    protected function checkEmpty($array)
    {
        foreach ($array as $key => $elem) {
            if (empty($elem)) {
                return true;
            }
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function checkTaxRegDate($set = true)
    {
        $taxRegDate = null;
        if ($data = DadataClient::getCompanyData($this->inn)) {
            if ($regDate = round(ArrayHelper::getValue($data, 'state.registration_date') / 1000)) {
                $taxRegDate = date('Y-m-d', $regDate);
            }
        }
        if ($set) {
            $this->tax_authority_registration_date = $taxRegDate;
        }

        return $taxRegDate;
    }

    /**
     * @return string
     */
    public function getAddressLegalFull()
    {
        return $this->address_legal;

        if ($this->address_legal_house == null) {
            $this->address_legal_house_type_id = '';
        }
        if ($this->address_legal_housing == null) {
            $this->address_legal_housing_type_id = '';
        }
        if ($this->address_legal_apartment == null) {
            $this->address_legal_apartment_type_id = '';
        }

        return join(', ', array_filter([
            $this->address_legal_postcode,
            $this->address_legal_city,
            $this->address_legal_street,
            $this->address_legal_house_type_id ? mb_strtolower($this->addressLegalHouseType->name) . ' ' . $this->address_legal_house : '',
            $this->address_legal_housing_type_id ? $this->addressLegalHousingType->name_short . ' ' . $this->address_legal_housing : '',
            $this->address_legal_apartment_type_id ? $this->addressLegalApartmentType->name_short . ' ' . $this->address_legal_apartment : '',
        ]));
    }

    /**
     * @return string
     */
    public function getAddressActualFull()
    {
        return $this->address_actual;

        if ($this->address_actual_house == null) {
            $this->address_actual_house_type_id = '';
        }
        if ($this->address_actual_housing == null) {
            $this->address_actual_housing_type_id = '';
        }
        if ($this->address_actual_apartment == null) {
            $this->address_actual_apartment_type_id = '';
        }
        if ($this->address_legal_is_actual) {
            return $this->getAddressLegalFull();
        } else {
            return join(', ', array_filter([
                $this->address_actual_postcode,
                $this->address_actual_city,
                $this->address_actual_street,
                $this->address_actual_house_type_id ? $this->addressActualHouseType->name . ' ' . $this->address_actual_house : '',
                $this->address_actual_housing_type_id ? $this->addressActualHousingType->name_short . ' ' . $this->address_actual_housing : '',
                $this->address_actual_apartment_type_id ? $this->addressActualApartmentType->name_short . ' ' . $this->address_actual_apartment : '',
            ]));
        }
    }

    /**
     * @param $prefix
     * @param bool|false $short
     * @return string
     */
    private function getFio($prefix, $short = false)
    {
        $name = join(' ', array_filter([
            $this->{$prefix . '_lastname'},
            $this->{$prefix . '_firstname'},
            $this->{$prefix . '_patronymic'},
        ]));

        return $short ? TextHelper::nameShort($name) : $name;
    }

    // file uploads

    /**
     *
     * @return string
     */
    public static function fileUploadDir($company_id)
    {
        return 'company_files' .
            DIRECTORY_SEPARATOR . intval($company_id / 10000) .
            DIRECTORY_SEPARATOR . intval($company_id / 100) .
            DIRECTORY_SEPARATOR . $company_id;
    }

    /**
     *
     * @return string
     */
    public static function fileUploadPath($company_id)
    {
        return Yii::getAlias('@common/uploads') .
            DIRECTORY_SEPARATOR . self::fileUploadDir($company_id);
    }

    /**
     * @param bool|true $absolutePath
     * @return string
     * @throws Exception
     */
    public function getUploadPath($absolutePath = true, $tmp = false)
    {
        if ($absolutePath) {
            $path = self::fileUploadPath($this->id) . DIRECTORY_SEPARATOR . 'profile';
        } else {
            $path = self::fileUploadDir($this->id) . DIRECTORY_SEPARATOR . 'profile';
        }
        if ($tmp) {
            $path .= DIRECTORY_SEPARATOR . 'tmp';
        }

        return $path;
    }

    /**
     * @param $fileName
     * @param bool|true $absolutePath
     * @return string
     * @throws Exception
     */
    public function getImagePath($fileName, $absolutePath = true, $tmp = false)
    {
        $fileName = trim($fileName, "\\/");

        return $this->getUploadPath($absolutePath, $tmp) . DIRECTORY_SEPARATOR . $fileName;
    }

    /**
     * @param string $attribute
     * @return string|null
     */
    public function getImageName($attribute)
    {
        if (isset(self::$imageDataArray[$attribute])) {
            return self::$imageDataArray[$attribute]['file_name'];
        }

        return null;
    }

    /**
     * @return int|string
     */
    public function getCustomersCount()
    {
        return $this->getContractors()
            ->andWhere(['and',
                ['is_deleted' => 0],
                ['is_customer' => 1],
            ])->count();
    }

    /**
     * @return int|string
     */
    public function getSellersCount()
    {
        return $this->getContractors()
            ->andWhere(['and',
                ['is_deleted' => 0],
                ['is_seller' => 1],
            ])->count();
    }

    /**
     * @return int|string
     */
    public function getAutoInvoicesCount()
    {
        return Autoinvoice::find()->andWhere(['company_id' => $this->id])->count();
    }

    /**
     * @return int|string
     */
    public function getPaymentOrderCount()
    {
        return PaymentOrder::find()->andWhere(['company_id' => $this->id])->count();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentReminderMessages()
    {
        return $this->hasMany(PaymentReminderMessage::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActualPaymentReminderMessages()
    {
        $query = $this->getPaymentReminderMessages()->andOnCondition([
            'status' => PaymentReminderMessage::STATUS_ACTIVE,
        ]);

        if (!$this->getHasPaidActualSubscription()) {
            $query->andOnCondition([
                'number' => PaymentReminderMessage::MESSAGE_1,
            ]);
        }

        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentReminderSettings()
    {
        return $this->hasOne(Settings::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceContractEssence()
    {
        return $this->hasOne(InvoiceContractEssence::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogisticsRequestContractEssenceCustomerAll()
    {
        return $this->hasOne(LogisticsRequestContractEssence::className(), ['company_id' => 'id'])
            ->andOnCondition(['for_contractor' => false])
            ->andOnCondition(['type' => LogisticsRequestContractEssence::TYPE_CUSTOMER]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogisticsRequestContractEssenceCustomerForContractor()
    {
        return $this->hasOne(LogisticsRequestContractEssence::className(), ['company_id' => 'id'])
            ->andOnCondition(['for_contractor' => true])
            ->andOnCondition(['type' => LogisticsRequestContractEssence::TYPE_CUSTOMER]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogisticsRequestContractEssenceCarrierAll()
    {
        return $this->hasOne(LogisticsRequestContractEssence::className(), ['company_id' => 'id'])
            ->andOnCondition(['for_contractor' => false])
            ->andOnCondition(['type' => LogisticsRequestContractEssence::TYPE_CARRIER]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogisticsRequestContractEssenceCarrierForContractor()
    {
        return $this->hasOne(LogisticsRequestContractEssence::className(), ['company_id' => 'id'])
            ->andOnCondition(['for_contractor' => true])
            ->andOnCondition(['type' => LogisticsRequestContractEssence::TYPE_CARRIER]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceEssence()
    {
        return $this->hasOne(InvoiceEssence::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActEssence()
    {
        return $this->hasOne(ActEssence::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceEmailText()
    {
        return $this->hasOne(InvoiceEmailText::className(), ['company_id' => 'id']);
    }

    /**
     * @return int|string
     */
    public function getGoodsCount()
    {
        return $this->getProducts()->andWhere(['and',
            ['is_deleted' => 0],
            ['production_type' => Product::PRODUCTION_TYPE_GOODS],
        ])->count();
    }

    /**
     * @return int|string
     */
    public function getServicesCount()
    {
        return $this->getProducts()->andWhere(['and',
            ['is_deleted' => 0],
            ['production_type' => Product::PRODUCTION_TYPE_SERVICE],
        ])->count();
    }

    /**
     * @param string $attribute
     * @param bool $tmp
     * @return DetailsFile|null
     */
    public function getImageModel($attribute, $tmp = false)
    {
        return DetailsFile::getInstanceFor($this->id, $attribute, $tmp);
    }

    /**
     * @param string $attribute
     * @param bool $tmp
     * @return string|null
     */
    public function getImage($attribute, $tmp = false)
    {
        return ArrayHelper::getValue($this->getImageModel($attribute, $tmp), 'filePath');
    }

    /**
     * {@inheritdoc}
     */
    public function deleteTemporaryImages($typeId = null)
    {
        $modelArray = $this->getDetailsFiles()->andWhere([
            'status_id' => DetailsFileStatus::STATUS_TEMPORARY,
        ])->andFilterWhere([
            'type_id' => $typeId,
        ])->all();

        foreach ($modelArray as $model) {
            $model->delete();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyNotifications()
    {
        return $this->hasMany(CompanyNotification::className(), ['company_id' => 'id']);
    }

    /**
     * Checks whether company's taxation type has NDS. Companies without NDS have simplified interface.
     * @param null $value
     * @return bool
     */
    public function hasNds($value = null)
    {
        if ($value === null) {
            $value = $this->companyTaxationType->osno;
        }

        return $value;
    }

    /**
     * @return bool
     */
    public function hasViewNds()
    {
        //return NdsOsno::hasNds($this->nds);
        return $this->nds == \common\models\NdsOsno::WITH_NDS;
    }

    /**
     * @return bool
     */
    public function getIsNdsInclude()
    {
        return $this->nds == \common\models\NdsOsno::WITH_NDS;
    }

    /**
     * @return bool
     */
    public function getIsNdsExclude()
    {
        return $this->nds == \common\models\NdsOsno::WITHOUT_NDS;
    }

    /**
     * @inheritdoc
     */
    public function getPdfFileName()
    {
        return $this->getPrintTitle() . '.pdf';
    }

    /**
     * @inheritdoc
     */
    public function getPrintTitle()
    {
        return $this->name_short;
    }

    /**
     * @param $name
     * @throws Exception
     */
    public function convertPdfToJpg($name)
    {
        $newPath = $this->getUploadPath(true) . '\\' . $name . '.jpg';
        if ($name == Company::IMAGE_LOGO_NAME) {
            $path = $this->getUploadPath(true) . $this->logo_link;
            exec("convert -density 300 \"{$path}\" \"{$newPath}\"");
            $this->logo_link = '\\' . $name . '.jpg';
            unlink($path);
        } elseif ($name == Company::IMAGE_PRINT_NAME) {
            $path = $this->getUploadPath(true) . $this->print_link;
            exec("convert -density 300 \"{$path}\" \"{$newPath}\"");
            $this->print_link = '\\' . $name . '.jpg';
            unlink($path);
        } elseif ($name == Company::IMAGE_CHIEF_SIGNATURE) {
            $path = $this->getUploadPath(true) . $this->chief_signature_link;
            exec("convert -density 300 \"{$path}\" \"{$newPath}\"");
            $this->chief_signature_link = '\\' . $name . '.jpg';
            unlink($path);
        }
    }

    /**
     *
     */
    public function checkFirstUpdate()
    {
        if (!$this->address_legal_is_actual) {
            if ($this->address_actual_city == null) {
                $this->address_legal_is_actual = true;
            //    $this->chief_is_chief_accountant = true;
            }
        }
    }

    /**
     * @return string
     */
    public function getShortName()
    {
        $typeName = CompanyType::getValue($this->company_type_id, 'name_short');

        if ($typeName) {
            return $typeName . ' ' . $this->name_short;
        } else {
            return $this->name_short;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRewardRequests()
    {
        return $this->hasMany(RewardRequest::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCheckingAccountant()
    {
        return $this->hasOne(CheckingAccountant::className(), ['company_id' => 'id'])
            ->onCondition(['<>', 'type', CheckingAccountant::TYPE_CLOSED])
            ->orderBy(['type' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForeignCurrencyAccounts()
    {
        return $this->hasMany(CheckingAccountant::className(), [
            'company_id' => 'id',
        ])->andOnCondition([
            'not', ['currency_id' => Currency::DEFAULT_ID]
        ])->andOnCondition([
            'not', ['currency_id' => null]
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRubleAccounts()
    {
        return $this->hasMany(CheckingAccountant::className(), [
            'company_id' => 'id',
        ])->andOnCondition([
            'currency_id' => Currency::DEFAULT_ID,
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCheckingAccountants()
    {
        return $this->hasMany(CheckingAccountant::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMainCheckingAccountant()
    {
        return $this->hasOne(CheckingAccountant::className(), ['company_id' => 'id'])
            ->onCondition([
                'type' => CheckingAccountant::TYPE_MAIN,
                'currency_id' => Currency::DEFAULT_ID,
            ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMainForeignCurrencyAccount()
    {
        return $this->hasOne(CheckingAccountant::className(), ['company_id' => 'id'])
            ->andOnCondition(['type' => CheckingAccountant::TYPE_MAIN])
            ->andOnCondition(['not', ['currency_id' => Currency::DEFAULT_ID]]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMainAccountant()
    {
        return $this->getMainCheckingAccountant();
    }

    /**
     * @return boolean
     */
    public function getHasAccountants()
    {
        return $this->isRelationPopulated('mainAccountant') || $this->checkingAccountant !== null;
    }

    /**
     * @return boolean
     */
    public function setHasAccountants($value)
    {
    }

    /**
     * Счета в банках, имеющих API получения выписки
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBankingAccountants()
    {
        return $this->getCheckingAccountants()
            ->andWhere(['bik' => Banking::bikList()])
            ->orderBy(['type' => SORT_ASC, 'rs' => SORT_ASC]);
    }

    /**
     * Счета в банках, имеющих API отправки платежного поручения
     *
     * @return \yii\db\ActiveQuery
     */
    public function getBankingPaymentAccountants()
    {
        return $this->getCheckingAccountants()
            ->andWhere(['bik' => Banking::bikListPaymentApi()])
            ->orderBy(['type' => SORT_ASC, 'rs' => SORT_ASC]);
    }

    /**
     * @param $rs string
     * @return CheckingAccountant|null
     */
    public function getRs($rs)
    {
        return $this->getCheckingAccountants()->andWhere([
            'rs' => $rs,
        ])->orderBy([
            'type' => SORT_ASC,
        ])->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function accountByRs($rs) : ?CheckingAccountant
    {
        if (empty($rs)) return null;

        if (!isset($this->_data['accountByRs']) || !array_key_exists($rs, $this->_data['accountByRs'])) {
            $this->_data['accountByRs'][$rs] = $this->getRs($rs);
        }

        return $this->_data['accountByRs'][$rs];
    }

    /**
     * @param $type
     * @return string
     */
    public function getFirstTenProducts($type)
    {
        $result = '';
        $products = $this->getProducts()
            ->select(['title'])
            ->andWhere(['production_type' => $type])
            ->orderBy(['created_at' => SORT_DESC])
            ->limit(10)
            ->column();
        foreach ($products as $key => $productName) {
            if (empty($result)) {
                $result .= $productName;
            } else {
                $result .= ', ' . $productName;
            }
        }

        return $result;
    }

    /**
     * @param $id
     * @return array|null|\yii\db\ActiveRecord
     */
    public static function getModel($id)
    {
        return Company::find()
            ->andWhere(['id' => $id])
            ->one();
    }

    /**
     *
     * @return boolean
     */
    public static function hasRequiredParams()
    {
        $id = Yii::$app->user->identity->company->id;
        $model = self::getModel($id);
        $scenario = Company::getScenarioForModal();

        $model->setScenario($scenario);

        return $model->validate();
    }

    /**
     *
     * @return string
     */
    public static function getScenarioForModal()
    {
        $id = Yii::$app->user->identity->company->id;
        $model = Company::getModel($id);

        if ($model->getIsLikeIP()) {
            $scenario = self::SCENARIO_IP_UPDATE;
        } else {
            $scenario = self::SCENARIO_OOO_UPDATE;
        }

        return $scenario;
    }

    /**
     * @return bool
     */
    public function setDirectorInitials()
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $user->setScenario(Employee::SCENARIO_CONTINUE_REGISTRATION);
        if ($this->company_type_id !== CompanyType::TYPE_IP) {
            $prefix = 'chief_';
            $chiefPostName = 'Генеральный директор';
        } else {
            $prefix = 'ip_';
            $chiefPostName = 'Предприниматель';
        }
        $firstNameInitials = mb_substr($this->getAttribute($prefix . 'firstname'), 0, 1);
        $patronymicInitials = mb_substr($this->getAttribute($prefix . 'patronymic'), 0, 1);
        $this->chief_post_name = $chiefPostName;
        $this->setAttributes([
            $prefix . 'firstname_initials' => $firstNameInitials,
            $prefix . 'patronymic_initials' => $patronymicInitials,
        ]);
        $user->setAttributes([
            'position' => $chiefPostName,
            'lastname' => $this->getAttribute($prefix . 'lastname'),
            'firstname' => $this->getAttribute($prefix . 'firstname'),
            'patronymic' => $this->getAttribute($prefix . 'patronymic'),
            'firstname_initial' => $firstNameInitials,
            'patronymic_initial' => $patronymicInitials,
        ]);

        $employeeCompany = EmployeeCompany::findOne(['employee_id' => $user->id, 'company_id' => $this->id]);

        if ($employeeCompany) {
            $employeeCompany->setEmployee($user);
        } else {
            return false;
        }

        return $employeeCompany->save(false) && $user->save(false, [
                'position',
                'lastname',
                'firstname',
                'patronymic',
                'firstname_initial',
                'patronymic_initial',
                'my_companies',
            ]);
    }

    /**
     * creating a trial subscribe for the Company
     * @return bool
     */
    public function createTrialSubscribe()
    {
        $tariff = SubscribeTariff::findOne(SubscribeTariff::TARIFF_TRIAL);

        $subscribe = new Subscribe([
            'company_id' => $this->id,
            'tariff_group_id' => $tariff->tariff_group_id,
            'tariff_limit' => $tariff->tariff_limit,
            'tariff_id' => $tariff->id,
            'duration_month' => $tariff->duration_month,
            'duration_day' => $tariff->duration_day,
            'status_id' => SubscribeStatus::STATUS_PAYED,
        ]);

        if ($subscribe->save()) {
            $sellingSubscribe = new SellingSubscribe();
            $sellingSubscribe->company_id = $this->id;
            $sellingSubscribe->subscribe_id = $subscribe->id;
            $sellingSubscribe->type = SellingSubscribe::TYPE_TRIAL;
            $sellingSubscribe->main_type = SellingSubscribe::TYPE_MAIN_FREE;
            $sellingSubscribe->invoice_number = null;
            $sellingSubscribe->creation_date = $subscribe->created_at;
            $sellingSubscribe->activation_date = time();
            $sellingSubscribe->end_date = $subscribe->expired_at;

            if ($sellingSubscribe->save()) {
                return true;
            } else {
                \common\components\helpers\ModelHelper::logErrors($sellingSubscribe, __METHOD__);
            }
        } else {
            \common\components\helpers\ModelHelper::logErrors($subscribe, __METHOD__);
        }

        return false;
    }

    /**
     * @return bool
     */
    public function isActualActiveSubscribe($groupId = SubscribeTariffGroup::STANDART)
    {
        $query = $this->getActiveSubscribe($groupId)->andWhere([
            'and',
            ['status_id' => SubscribeStatus::STATUS_ACTIVATED],
            ['>', 'expired_at', time()]
        ]);

        return $query->exists();
    }

    /**
     * @param Subscribe $subscribe
     */
    public function setActiveSubscribe(Subscribe $subscribe)
    {
        if ($subscribe->tariff_group_id == SubscribeTariffGroup::STANDART) {
            $this->active_subscribe_id = $subscribe->id;
            $this->free_tariff_start_at = null;
            $this->is_free_tariff_notified = false;
            $this->update(false, [
                'active_subscribe_id',
                'free_tariff_start_at',
                'is_free_tariff_notified',
            ]);
            $this->populateRelation('activeSubscribe', $subscribe);
            /* @var $sellingSubscribe SellingSubscribe */
            $sellingSubscribe = SellingSubscribe::find()->andWhere(['and',
                ['company_id' => $this->id],
                ['end_date' => null],
            ])->one();
            if ($sellingSubscribe) {
                $sellingSubscribe->end_date = time();
                $sellingSubscribe->save(true, ['end_date']);
            }
        }
        if ($subscribe->tariff_group_id) {
            $this->_actualSubscriptions[$subscribe->tariff_group_id] = $subscribe;
            $this->unlinkActiveSubscribe($subscribe->tariff_group_id);
            $this->link('activeSubscribes', $subscribe, ['tariff_group_id' => $subscribe->tariff_group_id]);
        }
    }

    /**
     * @param integer $groupId
     */
    public function unlinkActiveSubscribe($groupId)
    {
        Yii::$app->db->createCommand()->delete(ActiveSubscribe::tableName(), [
            'company_id' => $this->id,
            'tariff_group_id' => $groupId,
        ])->execute();
    }

    /**
     * @return Company
     */
    public static function getBasicCompanyModel()
    {
        if (isset(Yii::$app->request->post('Company')['company_type_id'])) {
            $scenario = Yii::$app->request->post('Company')['company_type_id'] == CompanyType::TYPE_IP ? Company::SCENARIO_IP_UPDATE_LANDING : Company::SCENARIO_OOO_UPDATE_LANDING;
        } else {
            $scenario = Company::SCENARIO_OOO_UPDATE_LANDING;
        }

        $company = new Company([
            'time_zone_id' => TimeZone::DEFAULT_TIME_ZONE,
            'chief_is_chief_accountant' => false,
            'address_legal_is_actual' => true,
            'scenario' => $scenario,
            'test' => Company::TEST_COMPANY,
            'address_legal_house_type_id' => AddressHouseType::TYPE_HOUSE,
            'address_legal_housing_type_id' => AddressHousingType::TYPE_BUILDING,
            'address_legal_apartment_type_id' => AddressApartmentType::TYPE_OFFICE,
        ]);

        return $company;
    }

    /**
     * @param CheckingAccountant $checkingAccountant
     * @param Invoice $invoice
     * @param Contractor $contractor
     * @param $ndsViewTypeId
     * @return array|bool
     */
    public function saveCompanyByLanding(CheckingAccountant $checkingAccountant, Invoice $invoice, Contractor $contractor, $ndsViewTypeId)
    {
        $this->chief_firstname_initials = mb_substr($this->chief_firstname, 0, 1);
        $this->chief_patronymic_initials = mb_substr($this->chief_patronymic, 0, 1);
        $this->chief_accountant_firstname_initials = mb_substr($this->chief_accountant_firstname, 0, 1);
        $this->chief_accountant_patronymic_initials = mb_substr($this->chief_accountant_patronymic, 0, 1);

        if ($this->company_type_id == CompanyType::TYPE_IP) {
            $this->ip_firstname_initials = mb_substr($this->ip_firstname, 0, 1);
            $this->ip_patronymic_initials = mb_substr($this->ip_patronymic, 0, 1);
        }
        $this->chief_post_name = 'Генеральный директор';
        if (Yii::$app->request->post('Company')['taxation_type_id'] == 1) {
            $this->nds = $ndsViewTypeId == Invoice::NDS_VIEW_OUT ? NdsOsno::WITHOUT_NDS : NdsOsno::WITH_NDS;
        }

        if ($this->address_legal_house == null) {
            $this->address_legal_house_type_id = '';
        }
        if ($this->address_legal_housing == null) {
            $this->address_legal_housing_type_id = '';
        }
        if ($this->address_legal_apartment == null) {
            $this->address_legal_apartment_type_id = '';
        }

        $this->address_legal = join(', ', array_filter([
            $this->address_legal_postcode,
            $this->address_legal_city,
            $this->address_legal_street,
            $this->address_legal_house_type_id ? mb_strtolower($this->addressLegalHouseType->name) . ' ' . $this->address_legal_house : '',
            $this->address_legal_housing_type_id ? $this->addressLegalHousingType->name_short . ' ' . $this->address_legal_housing : '',
            $this->address_legal_apartment_type_id ? $this->addressLegalApartmentType->name_short . ' ' . $this->address_legal_apartment : '',
        ]));
        if ($this->save() && $this->createTrialSubscribe() && SubscribeHelper::checkSubscription($this)) {
            $checkingAccountant->scenario = CheckingAccountant::SCENARIO_LANDING_CREATION;
            $checkingAccountant->company_id = $this->id;
            $invoice->company_id = $this->id;
            $this->main_id = $this->id;
            $contractor->company_id = $this->id;
            if ($this->save(true, ['main_id']) && $checkingAccountant->load(Yii::$app->request->post()) && $checkingAccountant->save()) {
                $this->populateRelation('mainCheckingAccountant', $checkingAccountant);
                $this->populateRelation('mainAccountant', $checkingAccountant);
                $this->save();

                return true;
            }
        }

        return array_merge($this->getErrors(), $checkingAccountant->getErrors());
    }

    /**
     * @return bool|false|int
     * @throws \Exception
     */
    public function deleteCompanyFromLanding()
    {
        /* @var Invoice $invoice
         * @var Product[] $products
         * @var Contractor $contractor
         * @var Subscribe subscribe
         */
        $invoice = Invoice::findOne(['company_id' => $this->id]);
        $products = Product::findAll(['company_id' => $this->id]);
        $contractor = Contractor::findOne(['company_id' => $this->id]);
        $subscribe = Subscribe::findOne(['company_id' => $this->id]);
        foreach ($invoice->orders as $order) {
            $order->delete();
        }
        foreach ($products as $product) {
            $product->delete();
        }
        $invoice->delete();
        $contractor->delete();
        $this->active_subscribe_id = null;
        $this->companyTaxationType->delete();
        if ($this->save(false, ['active_subscribe_id'])) {
            $subscribe->delete();

            return $this->delete();
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function applyFreeTariff()
    {
        $this->unlinkActiveSubscribe(SubscribeTariffGroup::STANDART);
        $this->active_subscribe_id = null;
        $this->free_tariff_start_at = time();
        $this->is_free_tariff_notified = false;
        $this->update(false, [
            'active_subscribe_id',
            'free_tariff_start_at',
            'is_free_tariff_notified',
        ]);
        $this->populateRelation('activeSubscribe', null);

        $sellingSubscribe = new SellingSubscribe();
        $sellingSubscribe->company_id = $this->id;
        $sellingSubscribe->type = SellingSubscribe::TYPE_TARIFF_FREE;
        $sellingSubscribe->main_type = SellingSubscribe::TYPE_MAIN_FREE;
        $sellingSubscribe->invoice_number = null;
        $sellingSubscribe->creation_date = $this->free_tariff_start_at;
        $sellingSubscribe->activation_date = $this->free_tariff_start_at;
        $sellingSubscribe->end_date = null;
        $sellingSubscribe->save();
    }

    /**
     * @return bool
     */
    public function getFreeTariffNotified()
    {
        return (bool)$this->is_free_tariff_notified;
    }

    /**
     * @inheritdoc
     */
    public function setFreeTariffNotified($value)
    {
        $this->is_free_tariff_notified = (bool)$value;
        $this->update(false, [
            'is_free_tariff_notified',
        ]);
    }

    /**
     * @return bool
     */
    public function getIsFreeTariff()
    {
        return (bool)$this->free_tariff_start_at;
    }

    /**
     * @inheritdoc
     */
    public static function searchForFreeTariff()
    {
        return self::find()
            ->joinWith('activeSubscribe')
            ->andWhere(['company.blocked' => false])
            ->andWhere(['company.free_tariff_start_at' => null])
            ->andWhere([
                'or',
                ['<=', 'service_subscribe.expired_at', time()],
                ['service_subscribe.id' => null],
            ])
            ->all();
    }

    /**
     * @return bool
     */
    public function getFreeTariffInvoicesCount($ioType = Documents::IO_TYPE_OUT)
    {
        if ($this->getIsFreeTariff() && $ioType == Documents::IO_TYPE_OUT) {
            $date = new \DateTime();
            $date->modify('first day of this month');
            $date->modify("today");

            return $this->getInvoices()
                ->where([
                    'and',
                    ['type' => $ioType],
                    // ['is_deleted' => false], // удаленные считаем тоже
                    ['>=', 'created_at', $date->getTimestamp()]
                ])
                ->andWhere(['from_demo_out_invoice' => 0])
                ->count();
        } else {
            return 0;
        }
    }

    /**
     * @param int $ioType
     * @return int|string
     */
    public function getTrialTariffInvoicesCount($ioType = Documents::IO_TYPE_OUT)
    {
        if ($this->getIsTrialNotPaid() && $ioType == Documents::IO_TYPE_OUT) {
            return $this->getInvoices()
                ->where([
                    'and',
                    ['type' => $ioType],
                    // ['is_deleted' => false], // удаленные считаем тоже
                    ['>=', 'created_at', $this->activeSubscribe->activated_at],
                ])
                ->andWhere(['from_demo_out_invoice' => 0])
                ->count();
        }

        return 0;
    }

    /**
     * @return mixed
     */
    public function getInvoiceLimitForFreeTariff()
    {
        return $this->getInvoicePaidCount() + Yii::$app->params['free_tariff']['invoice_limit'] + (int)$this->shared_kub_vk;
    }

    /**
     * @return mixed
     */
    public function getInvoiceLimitForTrialTariff()
    {
        return $this->getInvoicePaidCount() + Yii::$app->params['trial_tariff']['invoice_limit'] + (int)$this->shared_kub_vk;
    }

    /**
     * @return array
     */
    public function getInvoiceLimitDateForFreeTariff()
    {
        return [
            'from' => date('01.m.Y'),
            'to' => cal_days_in_month(CAL_GREGORIAN, date('m'), date('Y')) . date('.m.Y'),
        ];
    }

    /**
     * @return mixed
     */
    public function getPayedInvoiceCount()
    {
        $currentMonth = date('m');
        $currentYear = date('Y');

        return $this->getPayments()
            ->joinWith('invoiceTariff')
            ->andWhere(['payment_for' => Payment::FOR_ADD_INVOICE])
            ->andWhere(['is_confirmed' => true])
            ->andWhere(['between', 'date(from_unixtime(payment_date))', $currentYear . '.' . $currentMonth . '.01', $currentYear . '.' . $currentMonth . '.' . cal_days_in_month(CAL_GREGORIAN, $currentMonth, $currentYear)])
            ->sum(ServiceInvoiceTariff::tableName() . '.invoice_count');
    }

    /**
     * @return mixed
     */
    public function getPaidInvoicesCount()
    {
        $dateRange = StatisticPeriod::getSessionPeriod();

        return $this->getPayments()
            ->andWhere(['is_confirmed' => true])
            ->andWhere(['between', 'DATE(FROM_UNIXTIME(' . Company::tableName() . '.created_at))', $dateRange['from'], $dateRange['to']])
            ->count(Payment::tableName() . '.sum');
    }

    /**
     * @return mixed
     */
    public function getPaidInvoicesSum()
    {
        $dateRange = StatisticPeriod::getSessionPeriod();

        return $this->getPayments()
            ->andWhere(['is_confirmed' => true])
            ->andWhere(['between', 'DATE(FROM_UNIXTIME(' . Company::tableName() . '.created_at))', $dateRange['from'], $dateRange['to']])
            ->sum(Payment::tableName() . '.sum');
    }

    /**
     * @return integer
     */
    public function getInvoiceLimitFromTime()
    {
        if ($activeSubscribe = $this->getActualSubscription(SubscribeTariffGroup::STANDART)) {
            return $activeSubscribe->getLimitFromTime();
        } else {
            $lasrExpired = $this->getSubscribes()->andWhere([
                'tariff_group_id' => SubscribeTariffGroup::STANDART,
                'status_id' => SubscribeStatus::STATUS_ACTIVATED
            ])->max('expired_at') ?: 0;

            return max($lasrExpired, strtotime(date('Y-m-01 00:00:00')));
        }
    }

    /**
     * @return mixed
     */
    public function getInvoicePaidCount($refresh = false)
    {
        if ($refresh || !isset($this->_data['getInvoicePaidCount'])) {
            $this->_data['getInvoicePaidCount'] = (int) $this->getPayments()->joinWith('invoiceTariff')->andWhere([
                'and',
                ['payment_for' => Payment::FOR_ADD_INVOICE],
                ['is_confirmed' => true],
                ['>=', 'payment_date', $this->getInvoiceLimitFromTime()],
            ])->sum(ServiceInvoiceTariff::tableName() . '.invoice_count');
        }

        return $this->_data['getInvoicePaidCount'];
    }

    /**
     * @return bool
     */
    public function getInvoiceCreatedCount($ioType = Documents::IO_TYPE_OUT, $refresh = false)
    {
        $fromTime = $this->getInvoiceLimitFromTime();

        return $ioType = Documents::IO_TYPE_OUT ?
            $this->getOutInvoiceCreatedCount($fromTime) :
            intval($this->getInvoices()->where([
                'and',
                ['type' => $ioType],
                // ['is_deleted' => false], // удаленные считаем тоже
                ['>=', 'created_at', $fromTime],
                ['from_demo_out_invoice' => 0],
            ])->count()) + intval($this->getForeignCurrencyInvoices()->andWhere([
                'and',
                ['type' => $ioType],
                ['>=', 'created_at', $fromTime],
            ])->count());
    }

    /**
     * @param  $fromTime Unixtimestamp
     * @return int
     */
    public function getOutInvoiceCreatedCount($fromTime)
    {
        $key = intval($fromTime);
        if (!isset($this->_data['getOutInvoiceCreatedCount'][$key])) {
            $this->_data['getOutInvoiceCreatedCount'][$key] = intval($this->getInvoices()->andWhere([
                'and',
                ['type' => Documents::IO_TYPE_OUT],
                ['>=', 'created_at', $fromTime],
                ['from_demo_out_invoice' => 0],
            ])->count());
        }

        return $this->_data['getOutInvoiceCreatedCount'][$key];
    }

    /**
     * @return integer
     */
    public function getInvoiceLimitByTariff($ioType = Documents::IO_TYPE_OUT, $refresh = false)
    {
        if ($subscribe = $this->getActualSubscription(SubscribeTariffGroup::STANDART)) {
            return $subscribe->getInvoiceLimit();
        } else {
            return Yii::$app->params['free_tariff']['invoice_limit'];
        }
    }

    /**
     * @return integer
     */
    public function getInvoiceLimit($ioType = Documents::IO_TYPE_OUT, $refresh = false)
    {
        return $this->getInvoiceLimitByTariff($ioType, $refresh) +
            $this->getInvoicePaidCount() +
            $this->shared_kub_vk;
    }

    /**
     * @return integer
     */
    public function getInvoiceLeft($ioType = Documents::IO_TYPE_OUT, $refresh = false)
    {
        return max(0, $this->getInvoiceLimit($ioType, $refresh) - $this->getInvoiceCreatedCount($ioType, $refresh));
    }

    /**
     * @return integer
     */
    public function getPricelistLeft()
    {
        if ($subscribe = $this->getActualSubscription(SubscribeTariffGroup::PRICE_LIST)) {
            return $subscribe->getLimitLeft();
        }

        return 0;
    }

    /**
     * @return bool
     */
    public function createInvoiceAllowed($ioType, $refresh = false, $showError = false)
    {
        if ($ioType == Documents::IO_TYPE_IN || $this->getInvoiceLeft($ioType, $refresh) > 0) {
            return true;
        }

        if ($showError) {
            $limit = $this->getInvoiceLimit($ioType, $refresh);
            if ($this->isFreeTariff) {
                $link = Html::a('Перейти на платный тариф', ['/subscribe']);
                \Yii::$app->session->setFlash(
                    'error',
                    "Для текущего тарифа БЕСПЛАТНО вы не можете создавать более $limit счетов. $link"
                );
            } elseif ($this->getIsTrialNotPaid()) {
                $link = Html::a('Перейти на платный тариф', ['/subscribe']);
                \Yii::$app->session->setFlash(
                    'error',
                    "Для текущего тарифа ПРОБНЫЙ ПЕРИОД вы не можете создавать более $limit счетов. $link"
                );
            } else {
                $link = Html::a('Оплатить сервис', ['/subscribe']);
                \Yii::$app->session->setFlash(
                    'error',
                    "Для текущего тарифа вы не можете создавать более $limit счетов. $link"
                );
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function uploadedTotalSize()
    {
        $companyStatistic = new CompanyStatistic([
            'company' => $this,
        ]);

        return $companyStatistic->getAllFileUploadSize();
    }

    /**
     * @return bool
     */
    public function uploadAllowed()
    {
        if ($this->isFreeTariff) {
            $size = $this->uploadedTotalSize();

            return $size < \Yii::$app->params['free_tariff']['upload_limit'];
        } else {
            return true;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutoinvoices()
    {
        return $this->hasMany(Contractor::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreements()
    {
        return $this->hasMany(Agreement::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplicationsToBank()
    {
        return $this->hasMany(ApplicationToBank::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAttendances()
    {
        return $this->hasMany(Attendance::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashBankFlows()
    {
        return $this->hasMany(CashBankFlows::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashBankForeignCurrencyFlows()
    {
        return $this->hasMany(CashBankForeignCurrencyFlows::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashBankStatementUploads()
    {
        return $this->hasMany(CashBankStatementUpload::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashEmoneyFlows()
    {
        return $this->hasMany(CashEmoneyFlows::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashEmoneyForeignCurrencyFlows()
    {
        return $this->hasMany(CashEmoneyForeignCurrencyFlows::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashOrderFlows()
    {
        return $this->hasMany(CashOrderFlows::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashOrderForeignCurrencyFlows()
    {
        return $this->hasMany(CashOrderForeignCurrencyFlows::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAllFlows($select, $where, $unionAll = true)
    {
        $bank = ['flow' => new \yii\db\Expression('"bank"')];
        $order = ['flow' => new \yii\db\Expression('"order"')];
        $emoney = ['flow' => new \yii\db\Expression('"emoney"')];

        return $this->getCashBankFlows()->select(array_merge($select, $bank))->andWhere($where)
            ->union($this->getCashOrderFlows()->select(array_merge($select, $order))->andWhere($where), $unionAll)
            ->union($this->getCashEmoneyFlows()->select(array_merge($select, $emoney))->andWhere($where), $unionAll);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAllForeignCurrencyFlows($select, $where, $unionAll = true)
    {
        $bank = ['flow' => new \yii\db\Expression('"bank_foreign"')];
        $order = ['flow' => new \yii\db\Expression('"order_foreign"')];
        $emoney = ['flow' => new \yii\db\Expression('"emoney_foreign"')];

        return $this->getCashBankForeignCurrencyFlows()->select(array_merge($select, $bank))->andWhere($where)
            ->union($this->getCashOrderForeignCurrencyFlows()->select(array_merge($select, $order))->andWhere($where), $unionAll)
            ->union($this->getCashEmoneyForeignCurrencyFlows()->select(array_merge($select, $emoney))->andWhere($where), $unionAll);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAllAnyFlows($select, $where, $unionAll = true)
    {
        return $this->getAllFlows($select, $where, $unionAll)
            ->union($this->getAllForeignCurrencyFlows($select, $where, $unionAll), $unionAll);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExportFiles()
    {
        return $this->hasMany(ExportFiles::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogs()
    {
        return $this->hasMany(Log::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReports()
    {
        return $this->hasMany(Report::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServicePaymentPromoCodes()
    {
        return $this->hasMany(PromoCode::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSpecificDocuments()
    {
        return $this->hasMany(SpecificDocument::className(), ['company_id' => 'id']);
    }

    /**
     * @return 0|1
     */
    public function getIsFileLogo()
    {
        return $this->logo_link ? 1 : 0;
    }

    /**
     * @return 0|1
     */
    public function getIsFilePrint()
    {
        return $this->print_link ? 1 : 0;
    }

    /**
     * @return 0|1
     */
    public function getIsFileSignature()
    {
        return $this->chief_signature_link ? 1 : 0;
    }

    /**
     * @return integer
     */
    public function getFilesCount()
    {
        return $this->isFileLogo + $this->isFilePrint + $this->isFileSignature;
    }

    /**
     * @return string
     */
    public function getActivationStatus()
    {
        return isset(self::$activationType[$this->activation_type]) ?
            self::$activationType[$this->activation_type] : '';
    }

    /**
     * @return string
     */
    public function getDocumentsType()
    {
        $types = $this->getDocsTypeArray();

        return isset($types[$this->documents_type]) ? $types[$this->documents_type] : '';
    }

    /**
     * @return string
     */
    public function getIsDocsTypeUpd()
    {
        return $this->documents_type == self::DOCS_TYPE_UPD;
    }

    /**
     * @return array
     */
    public function getDocsTypeArray()
    {
        return [
            self::DOCS_TYPE_PACK => 'Акт, Товарную накладную' . ($this->hasNds() ? ', Счет-фактуру' : ''),
            self::DOCS_TYPE_UPD => 'УПД',
        ];
    }

    /**
     * @return array
     */
    public function setPeriod($period)
    {
        if (!empty($period['from']) && !empty($period['to'])) {
            $this->_period = $period;
        }
    }

    /**
     * @return array
     */
    public function getPeriod()
    {
        if ($this->_period == null) {
            $this->_period = StatisticPeriod::getSessionPeriod();
        }

        return $this->_period;
    }

    /**
     * @return integer
     */
    public function getNotCreatedInvoice()
    {
        return $this->getInvoices()->joinWith(['company', 'act', 'packingList', 'invoiceFacture', 'upd'])
            ->leftJoin('company_taxation_type', '{{company_taxation_type}}.[[company_id]] = {{company}}.[[id]]')
            ->andWhere([
                'invoice.is_deleted' => false,
                'upd.id' => null,
            ])
            ->andWhere(['not', ['invoice.invoice_status_id' => InvoiceStatus::STATUS_REJECTED]])
            ->andWhere(['between', 'invoice.document_date', $this->period['from'], $this->period['to']])
            ->andWhere([
                'or',
                [
                    'and',
                    ['act.id' => null],
                    ['like', 'invoice.production_type', (string)Product::PRODUCTION_TYPE_SERVICE],
                ],
                [
                    'and',
                    ['packing_list.id' => null],
                    ['like', 'invoice.production_type', (string)Product::PRODUCTION_TYPE_GOODS],
                ],
                [
                    'and',
                    ['company_taxation_type.osno' => 1],
                    ['invoice_facture.id' => null],
                ],
            ])
            ->groupBy(['invoice.id'])
            ->count();
    }

    /**
     * @return integer
     */
    public function getNotCreatedAct()
    {
        return $this->getInvoices()->joinWith(['act', 'upd'])
            ->andWhere([
                'invoice.is_deleted' => false,
                'act.id' => null,
                'upd.id' => null,
            ])
            ->andWhere(['not', ['invoice.invoice_status_id' => InvoiceStatus::STATUS_REJECTED]])
            ->andWhere(['between', 'invoice.document_date', $this->period['from'], $this->period['to']])
            ->andWhere(['like', 'invoice.production_type', (string)Product::PRODUCTION_TYPE_SERVICE])
            ->count();
    }

    /**
     * @return integer
     */
    public function getNotCreatedPL()
    {
        return $this->getInvoices()->joinWith(['packingList', 'upd'])
            ->andWhere([
                'invoice.is_deleted' => false,
                'packing_list.id' => null,
                'upd.id' => null,
            ])
            ->andWhere(['not', ['invoice.invoice_status_id' => InvoiceStatus::STATUS_REJECTED]])
            ->andWhere(['between', 'invoice.document_date', $this->period['from'], $this->period['to']])
            ->andWhere(['like', 'invoice.production_type', (string)Product::PRODUCTION_TYPE_GOODS])
            ->count();
    }

    /**
     * @return integer
     */
    public function getNotCreatedIF()
    {
        return !$this->hasNds() ? 0 : $this->getInvoices()->joinWith(['invoiceFacture', 'upd'])
            ->andWhere([
                'invoice.is_deleted' => false,
                'invoice_facture.id' => null,
                'upd.id' => null,
            ])
            ->andWhere(['not', ['invoice.invoice_status_id' => InvoiceStatus::STATUS_REJECTED]])
            ->andWhere(['between', 'invoice.document_date', $this->period['from'], $this->period['to']])
            ->count();
    }

    /**
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     */
    public function _delete()
    {
        $model = $this;

        return Yii::$app->db->transaction(function (Connection $db) use ($model) {
            foreach ($this->agreements as $agreement) {
                if (!$agreement->delete()) {
                    $db->transaction->rollBack();

                    return false;
                }
            }
            foreach ($this->applicationsToBank as $applicationToBank) {
                if (!$applicationToBank->delete()) {
                    $db->transaction->rollBack();

                    return false;
                }
            }
            foreach ($this->attendances as $attendance) {
                if (!$attendance->delete()) {
                    $db->transaction->rollBack();

                    return false;
                }
            }
            foreach ($this->autoinvoices as $autoInvoice) {
                if (!$autoInvoice->delete()) {
                    $db->transaction->rollBack();

                    return false;
                }
            }
            foreach ($this->cashBankFlows as $cashBankFlow) {
                foreach ($cashBankFlow->flowInvoices as $flowInvoice) {
                    if (!$flowInvoice->delete()) {
                        $db->transaction->rollBack();

                        return false;
                    }
                }
                if (!$cashBankFlow->delete()) {
                    $db->transaction->rollBack();

                    return false;
                }
            }
            foreach ($this->cashBankStatementUploads as $cashBankStatementUpload) {
                if (!$cashBankStatementUpload->delete()) {
                    $db->transaction->rollBack();

                    return false;
                }
            }
            foreach ($this->cashEmoneyFlows as $cashEmoneyFlow) {
                foreach ($cashEmoneyFlow->flowInvoices as $flowInvoice) {
                    if (!$flowInvoice->delete()) {
                        $db->transaction->rollBack();

                        return false;
                    }
                }
                if (!$cashEmoneyFlow->delete()) {
                    $db->transaction->rollBack();

                    return false;
                }
            }
            foreach ($this->cashOrderFlows as $cashOrderFlow) {
                foreach ($cashOrderFlow->flowInvoices as $flowInvoice) {
                    if (!$flowInvoice->delete()) {
                        $db->transaction->rollBack();

                        return false;
                    }
                }
                if (!$cashOrderFlow->delete()) {
                    $db->transaction->rollBack();

                    return false;
                }
            }
            if (!$this->companyTaxationType->delete()) {
                $db->transaction->rollBack();

                return false;
            }
            foreach ($this->export as $export) {
                if (!$export->delete()) {
                    $db->transaction->rollBack();

                    return false;
                }
            }
            foreach ($this->exportFiles as $exportFile) {
                if (!$exportFile->delete()) {
                    $db->transaction->rollBack();

                    return false;
                }
            }
            foreach ($this->files as $file) {
                if (!$file->delete()) {
                    $db->transaction->rollBack();

                    return false;
                }
            }
            foreach ($this->reports as $report) {
                foreach ($report->reportFiles as $reportFile) {
                    if (!$reportFile->delete()) {
                        $db->transaction->rollBack();

                        return false;
                    }
                }
                if (!$report->delete()) {
                    $db->transaction->rollBack();

                    return false;
                }
            }
            $this->active_subscribe_id = null;
            if (!$this->save(true, ['active_subscribe_id'])) {
                $db->transaction->rollBack();

                return false;
            }
            foreach ($this->subscribes as $subscribe) {
                if ($subscribe->payment) {
                    if ($subscribe->payment->promoCode && $subscribe->payment->promoCode->company_id == $this->id) {
                        $promoCode = $subscribe->payment->promoCode;
                    }
                    if (!$subscribe->payment->delete()) {
                        $db->transaction->rollBack();

                        return false;
                    }
                }
                if (isset($promoCode)) {
                    if (!$promoCode->delete()) {
                        $db->transaction->rollBack();

                        return false;
                    }
                }
                if (!$subscribe->delete()) {
                    $db->transaction->rollBack();

                    return false;
                }
            }
            foreach ($this->specificDocuments as $specificDocument) {
                if (!$specificDocument->delete()) {
                    $db->transaction->rollBack();

                    return false;
                }
            }
            foreach ($this->invoices as $invoice) {
                foreach ($invoice->acts as $act) {
                    foreach ($act->orderActs as $orderAct) {
                        if (!$orderAct->delete()) {
                            $db->transaction->rollBack();

                            return false;
                        }
                    }
                    if (!$act->delete()) {
                        $db->transaction->rollBack();

                        return false;
                    }
                }
                foreach ($invoice->packingLists as $packingList) {
                    foreach ($packingList->orderPackingLists as $orderPackingList) {
                        if (!$orderPackingList->delete()) {
                            $db->transaction->rollBack();

                            return false;
                        }
                    }
                    if (!$packingList->delete()) {
                        $db->transaction->rollBack();

                        return false;
                    }
                }
                if ($invoice->invoiceFacture) {
                    if (!$invoice->invoiceFacture->delete()) {
                        $db->transaction->rollBack();

                        return false;
                    }
                }
                if ($invoice->paymentOrder) {
                    if (!$invoice->paymentOrder->delete()) {
                        $db->transaction->rollBack();

                        return false;
                    }
                }
                foreach ($invoice->orders as $order) {
                    if (!$order->delete()) {
                        $db->transaction->rollBack();

                        return false;
                    }
                }
                if (!$invoice->delete()) {
                    $db->transaction->rollBack();

                    return false;
                }
            }
            foreach ($this->checkingAccountants as $checkingAccountant) {
                if (!$checkingAccountant->delete()) {
                    $db->transaction->rollBack();

                    return false;
                }
            }
            foreach ($this->products as $product) {
                if (!$product->delete()) {
                    $db->transaction->rollBack();

                    return false;
                }
            }
            foreach ($this->logs as $log) {
                if (!$log->delete()) {
                    $db->transaction->rollBack();

                    return false;
                }
            }

            if ($this->invitedByCompany) {
                $invitedByCompany = $this->invitedByCompany;
                $invitedByCompany->invited_companies_count -= 1;
                if (!$invitedByCompany->save(true, ['invited_companies_count'])) {
                    $db->transaction->rollBack();

                    return false;
                }
            }
            foreach ($this->employeeCompanies as $employeeCompany) {
                $employee = $employeeCompany->employee;
                if (!$employeeCompany->delete()) {
                    $db->transaction->rollBack();

                    return false;
                }
                if (EmployeeCompany::find()->andWhere(['and',
                    ['not', ['company_id' => $employeeCompany->company_id]],
                    ['employee_id' => $employeeCompany->employee_id],
                ])->exists()
                ) {
                    if ($employee->company_id == $this->id) {
                        /* @var $newEmployeeCompany EmployeeCompany */
                        $newEmployeeCompany = EmployeeCompany::find()->andWhere(['and',
                            ['not', ['company_id' => $employee->company_id]],
                            ['employee_id' => $employee->id],
                        ])->one();
                        $employee->company_id = $newEmployeeCompany->company_id;
                        if ($employee->main_company_id == $this->id) {
                            $employee->main_company_id = $newEmployeeCompany->company_id;
                        }
                        if (!$employee->save(true, ['company_id', 'main_company_id'])) {
                            $db->transaction->rollBack();

                            return false;
                        }
                    }
                } else {
                    if (!$employee->delete()) {
                        $db->transaction->rollBack();

                        return false;
                    }
                }
            }
            foreach ($this->contractors as $contractor) {
                if (!$contractor->delete()) {
                    $db->transaction->rollBack();

                    return false;
                }
            }
            foreach ($this->companyNotifications as $companyNotification) {
                $companyNotification->delete();
            }

            if ($this->id == $this->main_id) {
                $mainCompanyID = $this->main_id;
            }
            if (!$this->delete()) {
                $db->transaction->rollBack();

                return false;
            }
            if (isset($mainCompanyID)) {
                /* @var $employees Employee[] */
                $employees = Employee::find()->andWhere(['company_id' => $mainCompanyID])->all();
                /* @var $randomCompany Company */
                $randomCompany = self::find()->andWhere(['main_id' => $mainCompanyID])->one();
                /* @var $companies Company[] */
                $companies = self::find()->andWhere(['main_id' => $mainCompanyID])->all();
                foreach ($companies as $company) {
                    $company->main_id = $randomCompany->id;
                    if (!$company->save(true, ['main_id'])) {
                        $db->transaction->rollBack();

                        return false;
                    }
                }
                foreach ($employees as $employee) {
                    $employee->main_company_id = $randomCompany->id;
                    if (!$employee->save(true, ['main_company_id'])) {
                        $db->transaction->rollBack();

                        return false;
                    }
                }
            }

            return true;
        });
    }

    /**
     * @return boolean
     */
    public function getHasActualSubscription($groupId = SubscribeTariffGroup::STANDART, $refresh = false)
    {
        return $this->getActualSubscription($groupId, $refresh) !== null;
    }

    /**
     * @return boolean
     */

    /**
     * @param int $groupId
     * @param bool $refresh
     * @return Subscribe|null
     */
    public function getActualSubscription($groupId = SubscribeTariffGroup::STANDART, $refresh = false): ?Subscribe
    {
        if (!array_key_exists($groupId, $this->_actualSubscriptions) || $refresh) {
            $this->_actualSubscriptions[$groupId] = SubscribeHelper::findActualSubscription($this, $groupId);
        }

        return $this->_actualSubscriptions[$groupId];
    }

    /**
     * @return boolean
     */
    public function getAllActualSubscriptions()
    {
        $result = [];
        foreach (SubscribeTariffGroup::allIds() as $groupId) {
            if ($subscribe = $this->getActualSubscription($groupId)) {
                $result[] = $subscribe;
            }
        }

        return $result;
    }

    /**
     * @return boolean
     */
    public function getHasPaidActualSubscription()
    {
        foreach ($this->getAllActualSubscriptions() as $subscribe) {
            if (!$subscribe->getIsTrial()) {
                return true;
            }
        }

        return false;
    }

    /**
     * Последняя активная подписка
     * @return Subscribe
     */
    public function getLastActiveSubscribe()
    {
        return $this->getSubscribes()->andWhere([
            'status_id' => SubscribeStatus::STATUS_ACTIVATED
        ])->orderBy(['expired_at' => SORT_DESC])->one();
    }

    /**
     * Оплаченные подписки
     * @return \yii\db\ActiveQuery
     */
    public function getPaidSubscriptions()
    {
        return $this->hasMany(Subscribe::className(), ['company_id' => 'id'])
            ->andOnCondition([
                'tariff_id' => SubscribeTariff::paidStandartIds()
            ])
            ->andOnCondition(['not', ['status_id' => SubscribeStatus::STATUS_NEW]]);
    }

    /**
     * Оплаченные подписки
     * @return \yii\db\ActiveQuery
     */
    public function getAnyPaidSubscriptions()
    {
        return $this->hasMany(Subscribe::className(), ['company_id' => 'id'])
            ->andOnCondition([
                'tariff_id' => SubscribeTariff::find()->select('id')->paid()->column(),
            ])
            ->andOnCondition(['not', ['status_id' => SubscribeStatus::STATUS_NEW]]);
    }

    /**
     * Есть ли хоть одна оплаченная подписка
     * @return boolean
     */
    public function getHasPaidSubscribes($update = false)
    {
        if ($update || $this->_hasPaidSubscribes === null) {
            $this->_hasPaidSubscribes = $this->getPaidSubscriptions()->exists() ||
                $this->hasMany(Subscribe::className(), ['company_id' => 'id'])
                    ->joinWith('payment')
                    ->andOnCondition([Payment::tableName() . '.type_id' => PaymentType::TYPE_PROMO_CODE])
                    ->andOnCondition([Payment::tableName() . '.payment_for' => Payment::FOR_SUBSCRIBE])
                    ->andOnCondition(['not', ['status_id' => SubscribeStatus::STATUS_NEW]])
                    ->exists();
        }

        return $this->_hasPaidSubscribes;
    }

    /**
     * true, если компания на триале и не имеет оплаченных подписок
     * @return boolean
     */
    public function getIsTrialNotPaid()
    {
        if ($this->hasActualSubscription) {
            if ($this->activeSubscribe &&
                $this->activeSubscribe->tariff_id == SubscribeTariff::TARIFF_TRIAL &&
                !$this->hasPaidSubscribes
            ) {
                return true;
            }
        }

        return false;
    }

    /**
     * Актуальные на данный момент скидки
     * @return \yii\db\ActiveQuery
     */
    public function getActualDiscounts()
    {
        $time = time();

        return $this->getDiscounts()
            ->andWhere(['is_active' => true])
            ->andWhere(['<', 'active_from', $time])
            ->andWhere(['>', 'active_to', $time]);
    }

    /**
     * @return Payment[]
     */
    public function getActiveStorePayments()
    {
        return Payment::find()
            ->select([
                Payment::tableName() . '.*',
                'DATE_FORMAT(DATE_ADD(FROM_UNIXTIME(`payment_date`), INTERVAL 1 YEAR), "%Y-%m-%d") as end_date',
            ])
            ->byCompany($this->id)
            ->andWhere(['payment_for' => Payment::FOR_STORE_CABINET])
            ->andWhere(['is_confirmed' => true])
            ->andHaving(['>', 'end_date', date(DateHelper::FORMAT_DATE, time())])
            ->all();
    }

    /**
     * @return Payment[]
     */
    public function getActiveOutInvoicePayments()
    {
        return Payment::find()
            ->select([
                Payment::tableName() . '.*',
                'DATE_FORMAT(DATE_ADD(FROM_UNIXTIME(`payment_date`), INTERVAL 1 YEAR), "%Y-%m-%d") as end_date',
            ])
            ->byCompany($this->id)
            ->andWhere(['payment_for' => Payment::FOR_OUT_INVOICE])
            ->andWhere(['is_confirmed' => true])
            ->andHaving(['>', 'end_date', date(DateHelper::FORMAT_DATE, time())])
            ->all();
    }

    /**
     * @return bool
     */
    public function canAddStoreCabinet()
    {
        return $this->getAvailableCabinetsCount() > 0;
    }

    /**
     * @return mixed
     */
    public function getAvailableCabinetsCount()
    {
        if ($this->id == 11270) {
            return 1000;
        }

        return Yii::$app->params['free_store_cabinets_count'] + Payment::find()
                ->select([
                    Payment::tableName() . '.*',
                    PromoCode::tableName() . '.cabinets_count as cabinets_count',
                    'DATE_FORMAT(DATE_ADD(DATE_ADD(FROM_UNIXTIME(`payment_date`), INTERVAL ' . PromoCode::tableName() . '.duration_month MONTH), INTERVAL ' . PromoCode::tableName() . '.duration_day DAY), "%Y-%m-%d") as end_date',
                ])
                ->joinWith('promoCode')
                ->byCompany($this->id)
                ->andWhere(['payment_for' => Payment::FOR_STORE_CABINET])
                ->andWhere(['is_confirmed' => true])
                ->andHaving(['>', 'end_date', date(DateHelper::FORMAT_DATE, time())])
                ->sum('cabinets_count') +
            Payment::find()
                ->select([
                    Payment::tableName() . '.*',
                    StoreTariff::tableName() . '.cabinets_count as cabinets_count',
                    'DATE_FORMAT(DATE_ADD(FROM_UNIXTIME(`payment_date`), INTERVAL 1 YEAR), "%Y-%m-%d") as end_date',
                ])
                ->joinWith('storeTariff')
                ->byCompany($this->id)
                ->andWhere(['payment_for' => Payment::FOR_STORE_CABINET])
                ->andWhere(['is_confirmed' => true])
                ->andWhere(Payment::tableName() . '.sum = ' . StoreTariff::tableName() . '.total_amount')
                ->andHaving(['>', 'end_date', date(DateHelper::FORMAT_DATE, time())])
                ->sum('cabinets_count') +
            Payment::find()
                ->select([
                    Payment::tableName() . '.*',
                    StoreTariff::tableName() . '.cabinets_count as cabinets_count',
                    'DATE_FORMAT(DATE_ADD(FROM_UNIXTIME(`payment_date`), INTERVAL 1 MONTH), "%Y-%m-%d") as end_date',
                ])
                ->joinWith('storeTariff')
                ->byCompany($this->id)
                ->andWhere(['payment_for' => Payment::FOR_STORE_CABINET])
                ->andWhere(['is_confirmed' => true])
                ->andWhere(Payment::tableName() . '.sum != ' . StoreTariff::tableName() . '.total_amount')
                ->andHaving(['>', 'end_date', date(DateHelper::FORMAT_DATE, time())])
                ->sum('cabinets_count') -
            StoreCompanyContractor::find()
                ->joinWith('contractor')
                ->andWhere([StoreCompanyContractor::tableName() . '.status' => StoreCompanyContractor::STATUS_ACTIVE])
                ->andWhere([Contractor::tableName() . '.is_deleted' => false])
                ->andWhere([Contractor::tableName() . '.company_id' => $this->id])
                ->andWhere([Contractor::tableName() . '.status' => Contractor::ACTIVE])
                ->groupBy(StoreCompanyContractor::tableName() . '.contractor_id')
                ->count();
    }

    /**
     * @return bool
     */
    public function canDonateWidget()
    {
        $ids = (array)ArrayHelper::getValue(Yii::$app->params, 'canDonateWidget');

        return in_array($this->id, $ids);
    }

    /**
     * @return bool
     */
    public function canAddOutInvoice()
    {
        if (Payment::find()->joinWith('outInvoiceTariff')
            ->select([
                Payment::tableName() . '.*',
                'DATE_FORMAT(DATE_ADD(FROM_UNIXTIME(`payment_date`), INTERVAL 1 YEAR), "%Y-%m-%d") as end_date',
            ])
            ->byCompany($this->id)
            ->andWhere(['payment_for' => Payment::FOR_OUT_INVOICE])
            ->andWhere(['is_confirmed' => true])
            ->andWhere(['out_invoice_tariff_id' => StoreOutInvoiceTariff::UNLIM_TARIFF])
            ->andHaving(['>', 'end_date', date(DateHelper::FORMAT_DATE, time())])
            ->exists()) {
            return true;
        }

        return $this->getAvailableLinksCount() > 0;
    }

    /**
     * @return mixed
     */
    public function getAvailableLinksCount()
    {
        if ($this->id == 17334) {
            return 1000;
        }

        return Payment::find()
                ->select([
                    Payment::tableName() . '.*',
                    PromoCode::tableName() . '.out_invoice_count as links_count',
                    'DATE_FORMAT(DATE_ADD(DATE_ADD(FROM_UNIXTIME(`payment_date`), INTERVAL ' . PromoCode::tableName() . '.duration_month MONTH), INTERVAL ' . PromoCode::tableName() . '.duration_day DAY), "%Y-%m-%d") as end_date',
                ])
                ->joinWith('promoCode')
                ->byCompany($this->id)
                ->andWhere(['payment_for' => Payment::FOR_OUT_INVOICE])
                ->andWhere(['is_confirmed' => true])
                ->andHaving(['or',
                    ['>', 'end_date', date(DateHelper::FORMAT_DATE, time())],
                    ['out_invoice_tariff_id' => StoreOutInvoiceTariff::UNLIM_TIME_AND_LINKS_TARIFF],
                ])
                ->sum('links_count') +
            Payment::find()
                ->select([
                    Payment::tableName() . '.*',
                    StoreOutInvoiceTariff::tableName() . '.links_count as links_count',
                    'DATE_FORMAT(DATE_ADD(FROM_UNIXTIME(`payment_date`), INTERVAL 1 YEAR), "%Y-%m-%d") as end_date',
                ])
                ->joinWith('outInvoiceTariff')
                ->byCompany($this->id)
                ->andWhere(['payment_for' => Payment::FOR_OUT_INVOICE])
                ->andWhere(['is_confirmed' => true])
                ->andHaving(['or',
                    ['>', 'end_date', date(DateHelper::FORMAT_DATE, time())],
                    ['out_invoice_tariff_id' => StoreOutInvoiceTariff::UNLIM_TIME_AND_LINKS_TARIFF],
                ])
                ->sum('links_count') -
            OutInvoice::find()
                ->andWhere(['company_id' => $this->id])
                ->andWhere(['status' => OutInvoice::ACTIVE])
                ->count();
    }

    /**
     * @return Discount|null
     */
    public function getMaxDiscount($forTariff)
    {
        if (empty($forTariff) || !is_numeric($forTariff)) {
            return null;
        }
        $forTariff = intval($forTariff);
        if (!array_key_exists($forTariff, $this->_maxDiscount)) {
            $discountArray = [];
            foreach ($this->actualDiscounts as $discount) {
                if (in_array($forTariff, $discount->tariffId)) {
                    $discountArray[] = $discount;
                }
            }
            if ($discountArray) {
                usort($discountArray, function ($a, $b) {
                    if ($a->value == $b->value) {
                        if ($a->active_to == $b->active_to) {
                            return 0;
                        }

                        return ($a->active_to < $b->active_to) ? -1 : 1;
                    }

                    return ($a->value < $b->value) ? -1 : 1;
                });
                $this->_maxDiscount[$forTariff] = end($discountArray);
            } else {
                $this->_maxDiscount[$forTariff] = null;
            }
        }

        return $this->_maxDiscount[$forTariff];
    }

    /**
     * @return integer
     */
    public function getDiscount($forTariff = null)
    {
        $discount = $this->getMaxDiscount($forTariff);

        return $discount ? $discount->value : 0;
    }

    /**
     * Whether the user is an employee of the company
     *
     * @return boolean
     */
    public function isEmployee(Employee $user)
    {
        return $this->getEmployeeCompanies()->where(['employee_id' => $user->id])->exists();
    }

    /**
     * @return bool
     */
    public function resetShowReminder()
    {
        $this->show_reminder_1 = $this->show_reminder_3 = $this->show_reminder_5 = $this->show_reminder_7 = true;

        return $this->save(true, ['show_reminder_1', 'show_reminder_3', 'show_reminder_5', 'show_reminder_7']);
    }

    /**
     * @return bool
     */
    public function getCanTaxModule()
    {
        $taxationType = $this->companyTaxationType;
        if (!$this->self_employed &&
            $this->company_type_id == CompanyType::TYPE_IP &&
            $taxationType->usn &&
            $taxationType->usn_type == CompanyTaxationType::INCOME
        ) {
            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function getCanOOOTaxModule()
    {
        $taxationType = $this->companyTaxationType;

        return !$this->self_employed &&
            $this->company_type_id == CompanyType::TYPE_OOO &&
            $taxationType->osno;
    }

    /**
     * @return EmployeeCompany[]
     */
    public function getEmployeeCompaniesChat()
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;

        return EmployeeCompany::find()
            ->leftJoin('chat', 'chat.to_employee_id = employee_company.employee_id OR chat.from_employee_id = employee_company.employee_id')
            ->andWhere(['or',
                ['and',
                    ['company_id' => Company::KUB_ID],
                    ['employee_id' => Employee::SUPPORT_KUB_EMPLOYEE_ID],
                ],
                ['and',
                    ['company_id' => $this->id],
                    ['is_working' => 1],
                    ['not', ['employee_id' => $user->id]],
                ],
            ])
            ->orderBy(['chat.created_at' => SORT_DESC])
            ->all();
    }

    /**
     * @return boolean
     */
    public function getHasInUpd(Contractor $contractor = null)
    {
        $query = Upd::find()->byIOType(Documents::IO_TYPE_IN)->byCompany($this->id)->andWhere([
            Invoice::tableName() . '.is_deleted' => false,
        ]);
        if ($contractor !== null) {
            $query->andWhere([
                Invoice::tableName() . '.contractor_id' => $contractor->id,
            ]);
        }

        return $query->exists();
    }

    /**
     * @return boolean
     */
    public function getHasUpd(Contractor $contractor = null)
    {
        $query = Upd::find()->byCompany($this->id)->andWhere([
            Invoice::tableName() . '.is_deleted' => false,
        ]);
        if ($contractor !== null) {
            $query->andWhere([
                Invoice::tableName() . '.contractor_id' => $contractor->id,
            ]);
        }

        return $query->exists();
    }

    /**
     * @return boolean
     */
    public function hasReverseInvoice(Invoice $invoice)
    {
        $query = $this->getInvoices()->andWhere([
            'type' => $invoice->type == Documents::IO_TYPE_OUT ? Documents::IO_TYPE_IN : Documents::IO_TYPE_OUT,
            'is_deleted' => false,
            'document_date' => $invoice->document_date,
            'document_number' => $invoice->document_number,
            'document_additional_number' => $invoice->document_additional_number,
            'total_amount_with_nds' => $invoice->total_amount_with_nds,
            'company_inn' => $invoice->contractor_inn,
            'company_kpp' => $invoice->contractor_kpp,
            'contractor_inn' => $invoice->company_inn,
            'contractor_kpp' => $invoice->company_kpp,
        ]);

        return $query->exists();
    }

    /**
     * @return boolean
     */
    public function hasReverseForeignCurrencyInvoice(ForeignCurrencyInvoice $invoice)
    {
        $query = $this->getForeignCurrencyInvoices()->joinWith('foreignCurrencyInvoiceData data')->andWhere([
            'type' => $invoice->type == Documents::IO_TYPE_OUT ? Documents::IO_TYPE_IN : Documents::IO_TYPE_OUT,
            'is_deleted' => false,
            'document_date' => $invoice->document_date,
            'document_number' => $invoice->document_number,
            'document_additional_number' => $invoice->document_additional_number,
            'total_amount_with_nds' => $invoice->total_amount_with_nds,
            'data.company_inn' => $invoice->contractor_inn,
            'data.company_kpp' => $invoice->contractor_kpp,
            'data.contractor_inn' => $invoice->company_inn,
            'data.contractor_kpp' => $invoice->company_kpp,
        ]);

        return $query->exists();
    }

    /**
     * @return boolean
     */
    public function hasOrderDocument(OrderDocument $orderDocument)
    {
        $query = $this->getOrderDocuments()->andWhere([
            'is_deleted' => false,
            'id' => $orderDocument->id,
            'document_date' => $orderDocument->document_date,
            'document_number' => $orderDocument->document_number,
            'document_additional_number' => $orderDocument->document_additional_number,
        ]);

        return $query->exists();
    }

    /**
     * @param $type
     * @return mixed
     */
    public static function getAfterRegistrationCountByType($type)
    {
        $dateRange = StatisticPeriod::getSessionPeriod();
        $attribute = self::$afterRegistrationBlockAttributes[$type];

        return self::find()
            ->isBlocked(self::UNBLOCKED)
            ->isTest(!Company::TEST_COMPANY)
            ->andWhere(['between', 'date(from_unixtime(' . Company::tableName() . '.created_at))', $dateRange['from'], $dateRange['to']])
            ->sum($attribute);
    }

    /**
     * @return array
     */
    public static function getAfterRegistrationData()
    {
        $data = [];
        $data['all'] = 0;
        foreach (self::$afterRegistrationBlock as $type => $text) {
            if ($type) {
                $count = self::getAfterRegistrationCountByType($type);
                $data[$type] = $count;
                $data['all'] += $count;
            }
        }

        return $data;
    }

    /**
     * create StoreUser
     */
    public function createStoreUser()
    {
        $storeUser = $this->storeUser;
        if (!$storeUser) {
            $chief = $this->getEmployeeChief();
            $email = $this->email ? $this->email : $chief->email;
            $phone = $this->phone ? $this->phone : $chief->phone;
            $password = substr(str_shuffle('0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'), 0, 6);
            $storeUser = new StoreUser();
            $storeUser->company_id = $this->id;
            $storeUser->login_password = $password;
            $storeUser->login_key = Yii::$app->security->generateRandomString();
            $storeUser->email = 'company' . $this->id . $email;
            $storeUser->phone = $phone;
            $storeUser->status = StoreUser::STATUS_ACTIVE;
            $storeUser->time_zone_id = TimeZone::DEFAULT_TIME_ZONE;
            $storeUser->setPassword($password);
            $storeUser->generateAuthKey();
            $storeUser->save();
            $this->populateRelation('storeUser', $storeUser);
        }
    }

    /**
     * @param $deactivateCabinetsCount
     */
    public function deactivateCabinets($deactivateCabinetsCount)
    {
        /* @var $deactivateCabinets StoreCompanyContractor[] */
        $deactivateCabinets = StoreCompanyContractor::find()
            ->joinWith('contractor')
            ->andWhere([StoreCompanyContractor::tableName() . '.status' => StoreCompanyContractor::STATUS_ACTIVE])
            ->andWhere([Contractor::tableName() . '.is_deleted' => false])
            ->andWhere([Contractor::tableName() . '.company_id' => $this->id])
            ->andWhere([Contractor::tableName() . '.status' => Contractor::ACTIVE])
            ->orderBy(['created_at' => SORT_ASC])
            ->groupBy(StoreCompanyContractor::tableName() . '.contractor_id')
            ->limit($deactivateCabinetsCount)
            ->all();
        foreach ($deactivateCabinets as $storeCabinet) {
            $storeCabinet->status = StoreCompanyContractor::STATUS_BLOCKED;
            $storeCabinet->save(true, ['status']);
        }
    }

    /**
     * @param $deactivateLinksCount
     */
    public function deactivateLinks($deactivateLinksCount)
    {
        /* @var $deactivateLinks OutInvoice[] */
        $deactivateLinks = OutInvoice::find()
            ->andWhere(['company_id' => $this->id])
            ->andWhere(['status' => OutInvoice::ACTIVE])
            ->orderBy(['created_at' => SORT_ASC])
            ->limit($deactivateLinksCount)
            ->all();
        foreach ($deactivateLinks as $outLink) {
            $outLink->status = OutInvoice::INACTIVE;
            $outLink->save(true, ['status']);
        }
    }

    /**
     * @param $value
     */
    public function setChief_fio($value)
    {
        $value = array_values(array_filter(explode(' ', trim($value))));
        if ($this->getIsLikeIP()) {
            $this->ip_lastname = ArrayHelper::getValue($value, 0);
            $this->ip_firstname = ArrayHelper::getValue($value, 1);
            $this->ip_patronymic = ArrayHelper::getValue($value, 2);
        } else {
            $this->chief_lastname = ArrayHelper::getValue($value, 0);
            $this->chief_firstname = ArrayHelper::getValue($value, 1);
            $this->chief_patronymic = ArrayHelper::getValue($value, 2);
        }
    }

    /**
     * @return string
     */
    public function getChief_fio()
    {
        if ($this->getIsLikeIP()) {
            $value = [
                $this->ip_lastname,
                $this->ip_firstname,
                $this->ip_patronymic,
            ];
        } else {
            $value = [
                $this->chief_lastname,
                $this->chief_firstname,
                $this->chief_patronymic,
            ];
        }

        return implode(' ', array_filter($value));
    }

    /**
     * @return bool|int
     */
    public function createAgreementTemplateService()
    {
        $agreementTemplate = new AgreementTemplate();
        $agreementTemplate->company_id = $this->id;
        $agreementTemplate->type = AgreementTemplate::TYPE_WITH_BUYER;
        $agreementTemplate->document_date = date(DateHelper::FORMAT_DATE);
        $agreementTemplate->document_number = $agreementTemplate->getNextDocumentNumber($this, $agreementTemplate->document_date);
        $agreementTemplate->document_name = 'Договор об оказании услуг';
        $agreementTemplate->document_type_id = AgreementType::TYPE_AGREEMENT;
        $agreementTemplate->document_header = '<p>{Название_Компании_Контрагента}, именуемое в дальнейшем Заказчик, в лице {Должность_Руководителя_Контрагента} {ФИО_Руководителя_Контрагента}, действующего на основании&nbsp;{Основание} с одной стороны, и</p>
<p>{Название_Компании}, именуемое в дальнейшем Исполнитель, в лице {Должность руководителя} {ФИО_Руководителя}, действующего на основании {Основание} с&nbsp;другой стороны, вместе именуемые стороны, а индивидуально Сторона, заключили настоящий договор возмездного оказания услуг (далее по тексту - Договор) о нижеследующем:</p>';

        $agreementTemplate->document_body = '<p style="padding-left: 40px;"><strong>1.Предмет и общие условия договора</strong></p>
<p>&nbsp;</p>
<p>1.1. В соответствии с настоящим договором Исполнитель обязуется по заданию Заказчика оказать ему или указанному им лицу следующие услуги:_________________________________</p>
<p>_______________________________________________________________________________,</p>
<p>а Заказчик обязуется оплатить эти услуги. Оказание услуг производится в порядке и в сроки,</p>
<p>установленные Графиком оказания услуг, подписываемым обеими сторонами и являющимся</p>
<p>неотъемлемой частью настоящего договора.</p>
<p>1.2. Исполнитель обязуется оказать предусмотренные настоящим договором услуги лично.</p>
<p>1.3. Срок действия настоящего договора:</p>
<p>Начало:___________________________________________________________________________;</p>
<p>Окончание:________________________________________________________________________;</p>
<p>1.4. В случае, невозможности исполнения, возникшей по вине Заказчика, услуги подлежат оплате в полном объеме. В случае, когда невозможность исполнения возникла по обстоятельствам, за которые ни одна из сторон не отвечает, Заказчик возмещает Исполнителю фактические понесённые последним расходы.</p>
<p>1.5. Заказчик вправе отказаться от исполнения настоящего договора при условии оплаты фактически понесенных им расходов.</p>
<p>1.6. Исполнитель вправе отказаться от исполнения настоящего договора при условии пол&shy;ного возмещения Заказчику убытков.</p>
<p>&nbsp;</p>
<p style="padding-left: 40px;"><strong>2.Права и обязанности сторон</strong></p>
<p>2.1. Исполнитель обязуется:</p>
<p>2.1.1. Оказывать услуги в полном объеме в соответствии с условиями настоящего договора.</p>
<p>2.1.2. Информировать Заказчика о ходе оказания услуг по настоящему договору.</p>
<p>2.1.3. По завершении оказания услуг предоставлять Заказчику Отчет в письменной форме о результатах оказания услуг.</p>
<p>2.1.4. Сохранять конфиденциальность о деятельности Заказчика и информации, полученной в ходе оказания услуг по настоящему договору.</p>
<p>2.1.5. Информировать Заказчика о предполагаемых изменениях и последствиях, которые могут возникнуть у Заказчика в ходе или в результате оказания услуг, если таковые изменения и последствия предвидятся Исполнителем.</p>
<p>2.1.6. В процессе оказания услуг по настоящему договору руководствоваться интересами Заказчика.</p>
<p>2.2. Заказчик обязуется:</p>
<p>2.2.1. Предоставить Исполнителю всю необходимую для оказания услуг информацию и документы.</p>
<p>2.2.2. Организовать необходимые условия для эффективной работы Исполнителя (время, место, нужное оборудование).</p>
<p>2.2.3. Принять и оплатить оказанные услуги в соответствии с условиями настоящего договора.</p>
<p>&nbsp;</p>
<p style="padding-left: 40px;"><strong>3.Сдача-приемка услуг</strong></p>
<p>&nbsp;</p>
<p>3.1. Отчет о результатах оказания услуг является основанием для подписания сторонами Акта сдачи-приемки услуг, который составляется Исполнителем и подписывается сторонами в течение трех дней с момента сдачи Заказчику упомянутого отчета оказания услуг.</p>
<p>3.2. Претензии Заказчика по качеству и своевременности оказанных услуг направляются Исполнителю в письменном виде в течение 5 (пяти) календарных дней с момента окончания оказания услуг или их отдельных этапов, обусловленных договором. В противном случае услуги считаются принятыми без претензий.</p>
<p>&nbsp;</p>
<p style="padding-left: 40px;"><strong>4. Стоимость и порядок расчетов</strong></p>
<p>4.1. Стоимость оказываемых Исполнителем услуг составляет______________(____________</p>
<p>_____________) руб., в том числе НДС по ставке ____% на сумму ___________________руб.</p>
<p>4.2. Оплата услуг производится в следующем порядке:________________________________.</p>
<p>&nbsp;</p>
<p style="padding-left: 40px;"><strong>5. Ответственность сторон</strong></p>
<p>5.1. За неисполнение или ненадлежащее исполнение своих обязательств по настоящему договору стороны несут ответственность в соответствии с действующим законодательством РФ.</p>
<p>5.2. При полной или частичной просрочке оплаты оказанных услуг Заказчик уплачивает Исполнителю пеню в размере ____ % от неоплаченной суммы за каждый день просрочки.</p>
<p>&nbsp;</p>
<p style="padding-left: 40px;"><strong>6. Прочие условия договора</strong></p>
<p>6.1. Настоящий договор вступает в силу с момента подписания обеими сторонами и действует до момента полного исполнения сторонами своих обязательств.</p>
<p>6.2. Настоящий договор заключен в двух экземплярах, имеющих равную юридическую силу, по одному для каждой из сторон.</p>
<p>6.3. Все вопросы, не урегулированные настоящим договором, решаются в соответствии с действующим законодательством РФ.</p>
<p>6.4. Все споры, возникающие в связи с исполнением настоящего договора, разрешаются в судебном порядке в соответствии с действующим законодательством РФ.</p>
<p>6.5. Все изменения и дополнения к настоящему договору вступают в силу с момента подписания его обеими сторонами.</p>';

        $agreementTemplate->document_requisites_customer = '<p>{Название_Компании}</p>
<p>&nbsp;</p>
<p>Юридический адрес: {Адрес}</p>
<p>ОГРН {ОГРН}</p>
<p>ОКПО {ОКПО}</p>
<p>ИНН {ИНН}</p>
<p>КПП {КПП}</p>
<p>р/с {РС}</p>
<p>в {Название_Банка}</p>
<p>к/с {КС}</p>
<p>БИК {БИК_Банка}</p>
<p>&nbsp;</p>
<p>{ФИО_Руководителя_сокращенно}</p>
<p>{Должность_Руководителя}</p>
<p>&nbsp;</p>
<p>_________________________________</p>
<p>(подпись)</p>
<p>М.П.</p>';

        $agreementTemplate->document_requisites_executer = '<p>{Название_Компании_Контрагента}</p>
<p>&nbsp;</p>
<p>Юридический адрес: {Адрес_Контрагента}</p>
<p>ОГРН {ОГРН_Контрагента}</p>
<p>ОКПО {ОКПО_Контрагента}</p>
<p>ИНН {ИНН_Контрагента}</p>
<p>КПП {КПП_Контрагента}</p>
<p>р/с {РС_Контрагента}</p>
<p>в {Название_Банка_Контрагента}</p>
<p>к/с {КС_Контрагента}</p>
<p>БИК {БИК_Банка_Контрагента}</p>
<p>&nbsp;</p>
<p>{ФИО_Руководителя_Контрагента_сокращенно}</p>
<p>{Должность_Руководителя_Контрагента}</p>
<p>&nbsp;</p>
<p>_________________________________</p>
<p>(подпись)</p>
<p>М.П.</p>';

        $agreementTemplate->payment_delay = 10;
        $agreementTemplate->created_by = Yii::$app->user->identity->id;
        $agreementTemplate->status = AgreementStatus::STATUS_CREATED;
        if ($agreementTemplate->save()) {
            return $agreementTemplate->id;
        }

        return false;
    }

    /**
     * @param $value
     */
    public function setCertificateDate($value)
    {
        $this->ip_certificate_date = $value && ($date = date_create_from_format('d.m.Y', $value)) ? $date->format('Y-m-d') : null;
    }

    /**
     * @return string
     */
    public function getCertificateDate()
    {
        return $this->ip_certificate_date ? date_create_from_format('Y-m-d', $this->ip_certificate_date)->format('d.m.Y') : '';
    }

    /**
     * @param bool $withHeader
     * @return string
     */
    public function getCertificate($withHeader = true)
    {
        $certificate = "";
        if ($this->ip_certificate_number && $this->ip_certificate_date) {
            if ($withHeader) {
                $certificate = "Сертификат ";
            }
            $certificate .= "{$this->ip_certificate_number} от {$this->certificateDate}";
            if ($this->ip_certificate_issued_by) {
                $certificate .= ", Выдано {$this->ip_certificate_issued_by}";
            }
            return $certificate;
        } else {
            return $certificate;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceContractorSignature()
    {
        return $this->hasOne(InvoiceContractorSignature::className(), ['company_id' => 'id']);
    }

    /**
     * @var $contractor Contractor
     */
    public function setParamsByContractor(Contractor $contractor)
    {
        $this->company_type_id = $contractor->company_type_id;
        $this->inn = $contractor->ITN;
        $this->name_short = $contractor->name;
        $this->address_legal = $contractor->legal_address;
        $this->kpp = $contractor->PPC ?: null;
        $this->ifns_ga = $contractor->PPC ? substr($contractor->PPC, 0, 4) : null;
        $this->ogrn = strlen($contractor->BIN) == 13 ? $contractor->BIN : null;
        $this->egrip = strlen($contractor->BIN) == 15 ? $contractor->BIN : null;
        $this->chief_post_name = $contractor->director_post_name;
        $this->chief_fio = $contractor->director_name != 'ФИО Руководителя' ? $contractor->director_name : '';
        $this->email = $contractor->director_email;
    }

    /**
     * @param integer $type
     * @return array
     */
    public function sortedContractorList($type = null)
    {
        $tContractor = Contractor::tableName();
        $tType = CompanyType::tableName();

        return Contractor::getSorted()->select([
            'contractor_name' => "IF(
                {{{$tType}}}.[[id]] IS NULl,
                {{{$tContractor}}}.[[name]],
                CONCAT({{{$tType}}}.[[name_short]], ' ', {{{$tContractor}}}.[[name]])
            )",
            "{$tContractor}.id",
        ])->andWhere([
            "{$tContractor}.is_deleted" => false,
            "{$tContractor}.status" => Contractor::ACTIVE,
        ])->andWhere([
            'or',
            ["{$tContractor}.company_id" => null],
            ["{$tContractor}.company_id" => $this->id],
        ])->byType($type)->indexBy('id')->column();
    }

    /**
     * @return array
     */
    public function getVehicleList()
    {
        return [null => ''] + ArrayHelper::map(Vehicle::find()
                ->andWhere(['company_id' => $this->id])
                ->andWhere(['in', 'vehicle_type_id', [VehicleType::TYPE_TRACTOR, VehicleType::TYPE_WAGON]])
                ->all(), 'id', function (Vehicle $model) {
                $name = $model->vehicleType->name . ' |' . $model->state_number . '|';
                if ($model->vehicle_type_id == VehicleType::TYPE_TRACTOR && $model->semitrailerType) {
                    $name .= (' + полуприцеп |' . $model->semitrailerType->state_number . '|');
                } elseif ($model->vehicle_type_id == VehicleType::TYPE_WAGON && $model->trailerType) {
                    $name .= (' + прицеп |' . $model->trailerType->state_number . '|');
                }
                return $name;
            });
    }

    /**
     * @param $value
     */
    public function setPatentDate($value)
    {
        $this->ip_patent_date = $value && ($date = date_create_from_format('d.m.Y', $value)) ? $date->format('Y-m-d') : null;
    }

    /**
     * @return string
     */
    public function getPatentDate()
    {
        return $this->ip_patent_date ? date_create_from_format('Y-m-d', $this->ip_patent_date)->format('d.m.Y') : '';
    }

    /**
     * @param $value
     */
    public function setPatentDateEnd($value)
    {
        $this->ip_patent_date_end = $value && ($date = date_create_from_format('d.m.Y', $value)) ? $date->format('Y-m-d') : null;
    }

    /**
     * @return string
     */
    public function getPatentDateEnd()
    {
        return $this->ip_patent_date_end ? date_create_from_format('Y-m-d', $this->ip_patent_date_end)->format('d.m.Y') : '';
    }

    /**
     * @param $value
     */
    public function settaxRegistrationDate($value)
    {
        $this->tax_authority_registration_date = $value && ($date = date_create_from_format('d.m.Y', $value)) ? $date->format('Y-m-d') : null;
    }

    /**
     * @return string
     */
    public function gettaxRegistrationDate()
    {
        return $this->tax_authority_registration_date ? date_create_from_format('Y-m-d', $this->tax_authority_registration_date)->format('d.m.Y') : '';
    }

    /**
     * @return boolean
     */
    public function needDossierConfirm(Contractor $contractor = null)
    {
        if ($subscribe = $this->getActualSubscription(SubscribeTariffGroup::CHECK_CONTRACTOR)) {
            $dossierQuery = $this->getDossierLogs()->andWhere([
                'and',
                ['contractor_id' => $contractor->id ?? null],
                ['inn' => $contractor->ITN ?? $this->inn],
                ['>=', 'created_at', $subscribe->activated_at],
            ]);
            if ($dossierQuery->exists()) {
                return false;
            }
        }

        return true;
    }

    /**
     * Возвращает настройки интеграции указанного модуля
     *
     * @param string $module
     * @return mixed|null Массив настроек или null, если модуль интеграции не подключён
     */
    public function integration(string $module)
    {
        if (in_array($module, self::INTEGRATION_MODULE) === false) {
            throw new InvalidArgumentException('Unknown module ' . $module);
        }

        if (is_array($this->integration) === false) {
            return null;
        }

        $integration = $this->integration[$module] ?? null;
        if ($integration !== null) {
            switch ($module) {
                case Employee::INTEGRATION_UNISENDER:
                    $integration['apiKey'] = Yii::$app->security->decryptByPassword(
                        base64_decode($integration['apiKey']),
                        Yii::$app->params['moneta']['encrypt_password']
                    );
                    return $integration;
            }
        }

        return $integration;
    }

    /**
     * @param string $module
     * @return IntegrationData
     */
    public function getIntegrationData(string $module): IntegrationData
    {
        return new IntegrationData(['company' => $this, 'module' => $module]);
    }

    /**
     * Устанавливает параметры интеграции для модуля
     *
     * @param string $module
     * @param mixed|null $config Если NULL, то данные для соответствующего модуля будут удалены
     * @throws InvalidArgumentException
     */
    public function setIntegration(string $module, $config)
    {
        if (in_array($module, self::INTEGRATION_MODULE) === false) {
            throw new InvalidArgumentException('Unknown module ' . $module);
        }

        $integration = $this->integration;
        if ($config === null) {
            unset($integration[$module]);
        } else {
            $integration[$module] = $config;
        }

        $this->integration = $integration;
    }

    /**
     * Сохраняет параметры интеграции в базе данных
     *
     * @param string $module
     * @param mixed|null $config Если NULL, то данные для соответствующего модуля будут удалены
     * @return bool
     * @throws InvalidArgumentException
     */
    public function saveIntegration(string $module = null, $config = null): bool
    {
        if ($module !== null) {
            $this->setIntegration($module, $config);
        }
        return $this->save(false, ['integration']);
    }

    public function saveInvite($req)
    {
        if (empty($req)) {
            return false;
        }
        $reqItems = explode('.', $req, 2);
        $invitedLink = $reqItems[0] ?? null;
        $invitedProduct = $reqItems[1] ?? ServiceModule::STANDART;

        if (!empty($invitedLink) && ($refCompany = Company::findOne(['affiliate_link' => $invitedLink])) !== null) {
            if (5 == mb_strlen($invitedProduct)) {
                $invitedProduct = CompanyAffiliateLink::findOne([
                    'company_id' => $refCompany->id,
                    'link' => $req,
                ])->module_id ?? null;
            }

            $this->invited_by_referral_code = $req;
            $this->invited_by_company_id = $refCompany->id;
            $this->invited_by_product_id = ServiceModule::findOne($invitedProduct)->id ?? null;
        }
    }

    public function getLast_visit_at()
    {
        if (!isset($this->_data['getLast_visit_at']) || !array_key_exists('getLast_visit_at', $this->_data)) {
            $this->_data['getLast_visit_at'] = $this->getCompanyLastVisits()->max('time');
        }

        return $this->_data['getLast_visit_at'];
    }

    public function getHasForeignCurrencyAccounts()
    {
        if (!isset($this->_data['getHasForeignCurrencyAccounts'])) {
            $this->_data['getHasForeignCurrencyAccounts'] = $this->getForeignCurrencyAccounts()->exists()
                || $this->getForeignCurrencyCashboxes()->exists()
                || $this->getForeignCurrencyEmoneys()->exists()
                || $this->getForeignCurrencyCardBills()->exists();
        }

        return $this->_data['getHasForeignCurrencyAccounts'];
    }

    public function getHasBankingEmail()
    {
        return BankingEmail::find()->where(['company_id' => $this->id])->exists();
    }

    public function getCanCreateCashFlowLinkage() : bool
    {
        $this->checkCashFlowLinkage();

        return !$this->_data['checkCashFlowLinkage']['hasLimit'] || !$this->_data['checkCashFlowLinkage']['usedLimit'];
    }

    public function checkCashFlowLinkage() : void
    {
        if (!isset($this->_data['checkCashFlowLinkage'])) {
            $hasLimit = true;
            foreach (SubscribeTariffGroup::$analyticsItems as $groupId) {
                if (($subscribe = $this->getActualSubscription($groupId)) && !$subscribe->isTrial) {
                    $hasLimit = false;
                    break;
                }
            }

            $this->_data['checkCashFlowLinkage']['hasLimit'] = $hasLimit;
            $this->_data['checkCashFlowLinkage']['usedLimit'] = false;
            if ($hasLimit) {
                $active = $this->getCashFlowLinkages()->orderBy([
                    'number' => SORT_ASC,
                ])->one();
                if ($active !== null) {
                    $this->_data['checkCashFlowLinkage']['usedLimit'] = true;
                    if ($active->status == CashFlowLinkage::STATUS_INACTIVE) {
                        $active->updateAttributes(['status' => CashFlowLinkage::STATUS_ACTIVE]);
                    }
                    $condition = [
                        'and',
                        ['company_id' => $this->id],
                        ['is_deleted' => false],
                        ['status' => CashFlowLinkage::STATUS_ACTIVE],
                        ['not', ['id' => $active->id]]
                    ];
                    if (CashFlowLinkage::find()->where($condition)->exists()) {
                        CashFlowLinkage::updateAll([
                            'status' => CashFlowLinkage::STATUS_INACTIVE,
                        ], $condition);
                    }
                }
            } else {
                if ($this->getCashFlowLinkages()->inactive()->exists()) {
                    CashFlowLinkage::updateAll([
                        'status' => CashFlowLinkage::STATUS_ACTIVE,
                    ], [
                        'and',
                        ['company_id' => $this->id],
                        ['is_deleted' => false],
                        ['status' => CashFlowLinkage::STATUS_INACTIVE],
                    ]);
                }
            }
        }
    }

    public function getCompanyActiveSubscribesIndexedByGroup() : array
    {
        if (!isset($this->_data['getCompanyActiveSubscribesIndexedByGroup'])) {
            $this->_data['getCompanyActiveSubscribesIndexedByGroup'] = $this->getCompanyActiveSubscribes()->with([
                'serviceSubscribe',
                'subscribeTariffGroup',
            ])->indexBy('tariff_group_id')->all();
        }

        return $this->_data['getCompanyActiveSubscribesIndexedByGroup'];
    }

    public function setCompanyActiveSubscribesIndexedByGroup(array $value)
    {
        $this->_data['getCompanyActiveSubscribesIndexedByGroup'] = $value;
    }
}
