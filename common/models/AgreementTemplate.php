<?php

namespace common\models;

use common\components\ImageHelper;
use Yii;
use common\components\date\DateHelper;
use common\components\date\DatePickerFormatBehavior;
use common\models\employee\Employee;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use frontend\models\log\Log;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\models\log\ILogMessage;
use common\components\helpers\Html;
use frontend\models\Documents;
use common\components\image\EasyThumbnailImage;

/**
 * This is the model class for table "agreement_template".
 *
 * @property integer $id
 * @property integer $company_id
 * @property integer $type
 * @property string $document_date
 * @property string $document_number
 * @property string $document_name
 * @property string $document_body
 * @property integer $documents_created
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $status
 * @property string $document_date_input
 * @property string comment_internal
 * @property string document_additional_number
 * @property integer payment_delay
 * @property string document_header
 * @property string document_requisites_customer
 * @property string document_requisites_executer
 * @property string other_patterns
 * @property integer $document_type_id
 * @property integer $document_author_id

 *
 * @property Employee $createdBy
 * @property Company $company
 */
class AgreementTemplate extends \yii\db\ActiveRecord implements ILogMessage
{
    /**
     * type
     */
    const TYPE_WITH_SUPPLIER = 1;
    const TYPE_WITH_BUYER = 2;

    public static $TYPE = [
        2 => 'С покупателем',
        1 => 'С поставщиком'
    ];

    const STATUS_DELETED  = 0;
    const STATUS_ACTIVE   = 1;
    const STATUS_ARCHIVED = 2;

    public static $STATUS = [
        0 => 'Удален',
        1 => 'В работе',
        2 => 'В архиве',
    ];

    public static $DOCUMENT_PATTERNS_AGREEMENT = [
        '{Номер_Договора}',
        '{Доп_Номер_Договора}',
        '{Дата_Договора}'
    ];

    public static $DOCUMENT_PATTERNS = [
        '{Номер_Документа}',
        '{Доп_Номер_Документа}',
        '{Дата_Документа}'
    ];

    public static $CUSTOMER_PATTERNS = [
        '{Название_Компании}',
        '{Должность_Руководителя}',
        '{ФИО_Руководителя}',
        '{ФИО_Руководителя_сокращенно}',
        '{Основание}',
        '{Адрес}',
        '{Емейл}',
        '{Телефон}',
        '{ИНН}',
        '{КПП}',
        '{ОГРН}',
        '{ОКПО}',
        '{РС}',
        '{КС}',
        '{Название_Банка}',
        '{БИК_Банка}',
        '{Подпись}',
        '{Печать}',
        '{Логотип}'
    ];

    public static $CONTRACTOR_PATTERNS = [
        '{Название_Компании_Контрагента}',
        '{Должность_Руководителя_Контрагента}',
        '{ФИО_Руководителя_Контрагента}',
        '{ФИО_Руководителя_Контрагента_сокращенно}',
        '{Основание_Контрагента}',
        '{Адрес_Контрагента}',
        '{Емейл_Контрагента}',
        '{Телефон_Контрагента}',
        '{ИНН_Контрагента}',
        '{КПП_Контрагента}',
        '{ОГРН_Контрагента}',
        '{ОКПО_Контрагента}',
        '{РС_Контрагента}',
        '{КС_Контрагента}',
        '{Название_Банка_Контрагента}',
        '{БИК_Банка_Контрагента}',
        '{Реквизиты_Контрагента_Физ_Лицо}'

    ];

    public static $OTHER_PATTERNS = [];

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'documentCreatedAtBehavior' => [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
            ],
            'blameableBehavior' => [
                'class' => BlameableBehavior::className(),
                'updatedByAttribute' => false,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'agreement_template';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['document_date_input', 'document_number', 'document_date', 'type', 'document_header', 'document_body', 'document_type_id'], 'required'],
            [['payment_delay', 'document_name', 'document_requisites_customer', 'document_requisites_executer'], 'required', 'when' => function ($model) {
                return $model->document_type_id == AgreementType::TYPE_AGREEMENT;
            }],
            [['document_number', 'payment_delay'], 'integer'],
            [['type'], 'integer', 'min' => 1, 'max' => 2],
            [['document_name', 'document_additional_number'], 'string', 'max' => 255],
            [['document_date_input'], 'date', 'format' => 'php:d.m.Y', 'message' => 'Дата документа указана неверно.'],
            [['document_date'], 'date', 'format' => 'php:Y-m-d', 'message' => 'Дата документа указана неверно.'],
            [
                ['document_type_id'], 'exist',
                'targetClass' => AgreementType::class,
                'targetAttribute' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'ID Компании',
            'document_date' => 'Дата',
            'document_date_input' => 'Дата',
            'document_number' => 'Номер договора',
            'document_name' => 'Название договора',
            'document_body' => ($this->document_type_id == AgreementType::TYPE_AGREEMENT) ? 'Предмет договора' : 'Текст документа',
            'documents_created' => 'Кол-во созданных',
            'type' => 'Тип',
            'created_at' => 'Created At',
            'created_by' => 'Ответственный',
            'status' => 'Статус',
            'document_additional_number' => 'Доп. номер договора',
            'payment_delay' => 'Отсрочка платежа',
            'document_header' => ($this->document_type_id == AgreementType::TYPE_AGREEMENT) ? 'Шапка договора' : 'Шапка документа',
            'document_requisites_customer' => 'Реквизиты заказчика',
            'document_requisites_executer' => 'Реквизиты исполнителя',
            'other_patterns' => 'Добавить свой вариант',
            'comment_internal' => 'Комментарий',
            'document_type_id' => 'Тип документа'
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        return parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return boolean
     */
    public function beforeDelete()
    {
        return parent::beforeDelete();
    }

    /**
     * @inheritdoc
     */
    public function setDocument_date_input($value)
    {
        $this->document_date = is_string($value) ? implode('-', array_reverse(explode('.', $value))) : null;
    }

    /**
     * @inheritdoc
     */
    public function getDocument_date_input()
    {
        return $this->document_date ? implode('.', array_reverse(explode('-', $this->document_date))) : null;
    }

    public function getIs_deleted() {
        return ($this->status === 0) ? true : false;
    }

    public function setOtherPatterns() {
        if (!empty($this->other_patterns)) {
            $patterns = json_decode($this->other_patterns, true);
            if (is_array($patterns)) {
                foreach ($patterns as $k => $v) {
                    $pattern_data = json_decode($v, true);
                    self::$OTHER_PATTERNS[$pattern_data['key']] = $pattern_data['value'];
                }
            }
        } else {
            self::$OTHER_PATTERNS = $this::getAllOtherPatterns();
        }
    }

    public static function getAllOtherPatterns() {
        $templates = AgreementTemplate::find()
            ->where(['company_id' => Yii::$app->user->identity->company->id])
            ->all();

        $return_patterns = [];
        foreach($templates as $t) {
            $patterns = json_decode($t->other_patterns, true);
            if (is_array($patterns)) {
                foreach ($patterns as $k => $v) {
                    $pattern_data = json_decode($v, true);
                    if (is_array($pattern_data))
                        $return_patterns[$pattern_data['key']] = $pattern_data['value'];
                }
            }
        }

        return $return_patterns;
    }

    /**
     * @param Company|int $company
     * @return string
     */
    public function getNextDocumentNumber($company, $date = null)
    {
        $numQuery = self::find()->andWhere([
            'company_id' => is_int($company) ? $company : $company->id,
        ]);
        if ($date) {
            $numQuery->andWhere('YEAR(`document_date`) = :year', [':year' => date('Y', strtotime($date))]);
        }
        $maxNumber = $numQuery->max('document_number * 1');

        return (string)(1 + (int)$maxNumber);
    }

    /**
     * Get formatted log message from Log model
     * @param Log $log
     * @return string
     */
    public function getLogMessage(Log $log)
    {
        $text = ('Шаблон документа №' . join(' ', array_filter([
                $log->getModelAttributeNew('document_number'),
                $log->getModelAttributeNew('document_additional_number'),
            ])));

        $link = Html::a($text, [
            '/documents/agreement-template/view',
            'id' => $log->getModelAttributeNew('id'),
        ]);

        $innerText = null;

        switch ($log->log_event_id) {
            case LogEvent::LOG_EVENT_CREATE:
                return $link . $innerText . ' был создан.';
            case LogEvent::LOG_EVENT_DELETE:
                return $text . $innerText . ' был удалён.';
            case LogEvent::LOG_EVENT_UPDATE:
                return $link . $innerText . ' был изменен.';
            case LogEvent::LOG_EVENT_UPDATE_STATUS:
                $status_id = $log->getModelAttributeNew('status');
                $status = ($status_id == self::STATUS_ACTIVE) ?
                    'В работе' : (($status_id == self::STATUS_ARCHIVED) ? 'В архиве' : 'Удалён');
                return $link . $innerText . ' статус "' . $status . '"';
            default:
                return $log->message;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Employee::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentType()
    {
        return $this->hasOne(AgreementType::className(), ['id' => 'document_type_id']);
    }

    /**
     * @inheritdoc
     */
    public function getLogIcon(Log $log)
    {
        return Documents::getLogIcon($log);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentStatus()
    {
        return new \yii\db\ActiveQuery(get_class());
    }

    /**
     * @param $text
     * @param Agreement $agreement
     * @param Company $company
     * @param Contractor $contractor
     * @param array $companyRs
     * @return mixed
     */
    public static function replacePatterns($text, Agreement $agreement, Company $company, Contractor $contractor, array $companyRs)
    {
        $images = ['{Логотип}', '{Печать}', '{Подпись}'];
        $requisites = ['{Реквизиты_Контрагента_Физ_Лицо}'];

        //$logo = EasyThumbnailImage::thumbnailSrc(
        //    $company->getImage('logoImage'),
        //    150,
        //    90,
        //    EasyThumbnailImage::THUMBNAIL_INSET_BOX
        //);

        $print = EasyThumbnailImage::thumbnailSrc(
            $company->getImage('printImage'),
            150,
            150,
            EasyThumbnailImage::THUMBNAIL_INSET_BOX
        );

        $defaultSignatureHeight = 50;
        $signatureHeight = ImageHelper::getSignatureHeight($company->getImage('chiefSignatureImage'), 50, 100);

        $signature = EasyThumbnailImage::thumbnailSrc(
            $company->getImage('chiefSignatureImage'),
            165,
            $signatureHeight,
            EasyThumbnailImage::THUMBNAIL_INSET_BOX
        );

        $replacePatterns = [
            // Document patterns
            '{Номер_Документа}' => $agreement->document_number,
            '{Доп_Номер_Документа}' => $agreement->document_additional_number,
            '{Дата_Документа}' => DateHelper::format(
                $agreement->document_date,
                DateHelper::FORMAT_USER_DATE,
                DateHelper::FORMAT_DATE
            ),
            '{Номер_Договора}' => $agreement->document_number,
            '{Доп_Номер_Договора}' => $agreement->document_additional_number,
            '{Дата_Договора}' => DateHelper::format(
                $agreement->document_date,
                DateHelper::FORMAT_USER_DATE,
                DateHelper::FORMAT_DATE
            ),
            // Customer patterns
            '{Название_Компании}' => $company->getShortName(),
            '{Должность_Руководителя}' => $company->chief_post_name,
            '{ФИО_Руководителя}' => $company->chief_lastname . ' ' . $company->chief_firstname . ($company->has_chief_patronymic ? (' ' . $company->chief_patronymic) : ''),
            '{ФИО_Руководителя_сокращенно}' => $company->getChiefFio(true),
            '{Основание}' => 'Устав',
            '{Адрес}' => $company->address_legal,
            //'{Название_Города}' => $company->address_legal_city,
            '{Емейл}' => $company->email,
            '{Телефон}' => $company->phone,
            '{ИНН}' => $company->inn,
            '{КПП}' => $company->kpp,
            '{ОГРН}' => $company->ogrn,
            '{ОКПО}' => $company->okpo,
            '{РС}' => $companyRs['rs'],
            '{КС}' => $companyRs['ks'],
            '{Название_Банка}' => $companyRs['bank_name'],
            '{БИК_Банка}' => $companyRs['bik'],
            // Contractor patterns
            '{Название_Компании_Контрагента}' => $contractor->getShortName(true),
            '{Должность_Руководителя_Контрагента}' => $contractor->director_post_name,
            '{ФИО_Руководителя_Контрагента}' => $contractor->director_name,
            '{ФИО_Руководителя_Контрагента_сокращенно}' => $contractor->getDirectorFio(),
            '{Основание_Контрагента}' => 'Устав',
            '{Адрес_Контрагента}' => $contractor->legal_address,
            '{Емейл_Контрагента}' => $contractor->director_email,
            '{Телефон_Контрагента}' => $contractor->director_phone,
            '{ИНН_Контрагента}' => $contractor->ITN,
            '{КПП_Контрагента}' => $contractor->PPC,
            '{ОГРН_Контрагента}' => $contractor->BIN,
            '{ОКПО_Контрагента}' => $contractor->okpo,
            '{РС_Контрагента}' => $contractor->current_account,
            '{КС_Контрагента}' => $contractor->corresp_account,
            '{Название_Банка_Контрагента}' => $contractor->bank_name,
            '{БИК_Банка_Контрагента}' => $contractor->BIC,
            '{Реквизиты_Контрагента_Физ_Лицо}' => self::getRequisitesPhysical($contractor),
            // Images
            '{Логотип}' => '', // $logo ? Html::img($logo, ['class' => 'agreement-image', 'alt' => '']) : '',
            '{Печать}' => $print ? Html::img($print, ['class' => 'agreement-image', 'alt' => '']) : '',
            '{Подпись}' => $signature ? Html::img($signature, ['class' => 'agreement-image', 'alt' => '', 'style' => 'margin-bottom:'.($defaultSignatureHeight - $signatureHeight).'px']) : '',
        ];

        // Extra patterns
        $replacePatterns = array_merge($replacePatterns, AgreementTemplate::getAllOtherPatterns());

        $pattern = [];
        $replace = [];
        foreach ($replacePatterns as $key => $value) {
            $pattern[] = $key;
            $replace[] = (in_array($key, $images) || in_array($key, $requisites)) ? $value : Html::encode($value);
        }

        return str_replace($pattern, $replace, $text);
    }

    public static function getRequisitesPhysical(Contractor $c): string
    {
        $requisites = [
            'Паспорт серии ' . $c->physical_passport_series . ' № ' . $c->physical_passport_number,
            'выдан ' . $c->physical_passport_issued_by,
            'Адрес места регистрации: ' . $c->physical_address
        ];

        return '<p>' . join('</p><p>', $requisites) . '</p>';
    }

    public function getFullNumber() {

        if ($this->document_additional_number)
            return $this->document_additional_number .'-'. $this->document_number;

        return $this->document_number;
    }

    public static function getAllTemplatesList($company_id, $type, $document_type_id = null) {
        $allTemplates = [];
        $templates = AgreementTemplate::find()
            ->where([
                'company_id' => $company_id,
                'type' => $type,
                'status' => AgreementTemplate::STATUS_ACTIVE
            ])
            ->andFilterWhere(['document_type_id' => $document_type_id])
            ->asArray()
            ->all();
        foreach ($templates as $t) {
            $allTemplates[$t['id']] = 'Шаблон №' . $t['document_number'] . ' от ' .
                DateHelper::format($t['document_date'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
        }

        return $allTemplates;
    }

    public function hasLogo() {

        return strpos($this->document_header, '{Логотип}') !== false;
    }

    public function getLogoPosition() {

        // Check position by tag <p style="text-align: right;">...</p>
        $header_rows = explode("\n", $this->document_header);
        foreach ($header_rows as $row) {
            if (strpos($row, '{Логотип}') !== false) {
                if (strpos($row, 'right') !== false)

                    return 'RIGHT';
            }
        }

        return 'LEFT';
    }

    public function highlightPatterns($field, $images = null) {

        $logoLink = isset($images['logo']) ? $images['logo'] : null;
        $printLink = isset($images['print']) ? $images['print'] : null;
        $signatureLink = isset($images['signature']) ? $images['signature'] : null;

        $text = $this->{$field};

        foreach (AgreementTemplate::$CUSTOMER_PATTERNS as $pattern) {

            // IMAGES
            if ($pattern == '{Подпись}') {
                $text = $signatureLink ?
                    str_replace($pattern, "<img style='position:absolute; float: left; margin: -0.25cm 0 0 0' src='{$signatureLink}'/>", $text) :
                    str_replace($pattern, "", $text);
            }
            if ($pattern == '{Печать}') {
                $text = $printLink ?
                    str_replace($pattern, "<img style='position:absolute; float: left; margin: -2cm 0 0 0' src='{$printLink}'/>", $text) :
                    str_replace($pattern, "", $text);
            }
            if ($pattern == '{Логотип}') {
                $text = str_replace($pattern, "", $text); // Logo added in head of document
            }

            $text = str_replace($pattern, "<span style='color:#619eff'>{$pattern}</span>", $text);
        }

        foreach (AgreementTemplate::$CONTRACTOR_PATTERNS as $pattern) {
            $text = str_replace($pattern, "<span style='color:#969902'>{$pattern}</span>", $text);
        }

        $this->setOtherPatterns();
        foreach (AgreementTemplate::$OTHER_PATTERNS as $pattern=>$value) {
            $text = str_replace($pattern, "<span style='color:#833a16'>{$pattern}</span>", $text);
        }

        foreach (AgreementTemplate::$DOCUMENT_PATTERNS as $pattern) {
            $text = str_replace($pattern, "<span style='color:#833a16'>{$pattern}</span>", $text);
        }
        foreach (AgreementTemplate::$DOCUMENT_PATTERNS_AGREEMENT as $pattern) {
            $text = str_replace($pattern, "<span style='color:#833a16'>{$pattern}</span>", $text);
        }

        return $text;
    }

    public function getDocument_author_id()
    {
        return $this->created_by;
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        $date = DateHelper::format($this->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

        return sprintf('№ %d от %s', $this->document_number, $date);
    }
}
