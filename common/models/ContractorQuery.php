<?php

namespace common\models;


use common\models\company\CompanyType;
use common\models\ICompanyStrictQuery;
use yii\db\ActiveQuery;
use yii\db\Query;

/**
 * Class ContractorQuery
 *
 * @package common\models\document
 */
class ContractorQuery extends ActiveQuery implements ICompanyStrictQuery
{
    use \common\components\TableAliasTrait;

    /**
     * @param $companyId
     *
     * @return ContractorQuery
     */
    public function byCompany($companyId)
    {
        return $this->andWhere(['OR',
            ['=', $this->getTableAlias() . '.company_id', $companyId],
            ['is', $this->getTableAlias() . '.company_id', null], // global contractors (e.g. "Куб")
        ]);
    }

    /**
     * @param $isDeleted
     *
     * @return ContractorQuery
     */
    public function byIsDeleted($isDeleted)
    {
        return $this->andWhere([
            $this->getTableAlias() . '.is_deleted' => $isDeleted,
        ]);
    }

    /**
     * @param $type
     *
     * @return ContractorQuery
     */
    public function byType($type)
    {
        if ($type !== null && $type !== '' && !(is_array($type) && empty($type))) {
            $conditionItems = [];
            foreach ((array) $type as $value) {
                if ($item = $this->getTypeCondition($value)) {
                    $conditionItems[] = $item;
                }
            }
            if (count($conditionItems) > 1) {
                $condition = ['or'];
                foreach ($conditionItems as $item) {
                    $condition[] = (array) $item;
                }
            } elseif (count($conditionItems) == 1) {
                $condition = reset($conditionItems);
            } else {
                $condition = '1=0';
            }
            $this->andWhere($condition);
        }

        return $this;
    }

    /**
     * @param $type
     *
     * @return array|null
     */
    protected function getTypeCondition($type)
    {
        switch ($type) {
            case Contractor::TYPE_SELLER:
                return [$this->getTableAlias() . '.is_seller' => 1];

            case Contractor::TYPE_CUSTOMER:
                return [$this->getTableAlias() . '.is_customer' => 1];

            case Contractor::TYPE_FOUNDER:
                return [$this->getTableAlias() . '.is_founder' => 1];

            case Contractor::TYPE_POTENTIAL_CLIENT:
                return [
                    $this->getTableAlias() . '.is_seller' => 0,
                    $this->getTableAlias() . '.is_customer' => 0,
                    $this->getTableAlias() . '.is_founder' => 0,
                ];

            default:
                return null;
        }
    }

    /**
     * @param $contractorType
     *
     * @return ContractorQuery
     */
    public function byContractor($contractorType)
    {
        return $this->byType($contractorType);

        return $this->andWhere([
            $this->getTableAlias() . '.type' => $contractorType,
        ]);
    }

    /**
     * @param bool $isDeleted
     * @return ContractorQuery
     */
    public function byDeleted($isDeleted = false)
    {
        return $this->andWhere([
            $this->getTableAlias() . '.is_deleted' => $isDeleted,
        ]);
    }

    /**
     * @param $status
     *
     * @return static
     */
    public function byStatus($status)
    {
        return $this->andWhere([
            $this->getTableAlias() . '.status' => $status,
        ]);
    }

    /**
     * @param $status
     *
     * @return static
     */
    public function foreign()
    {
        return $this->andWhere([
            $this->getTableAlias() . '.face_type' => Contractor::TYPE_FOREIGN_LEGAL_PERSON,
        ]);
    }

    /**
     * @param $status
     *
     * @return static
     */
    public function byExceptedIds($ids)
    {
        if ($ids) {
            return $this->andWhere([
                'not in', $this->getTableAlias() . '.id', $ids,
            ]);
        }

        return $this;
    }

    public function byName($name)
    {
        if (!$name)
            return $this;

        $nameParts = explode(' ', preg_replace('/\s{2,}/', ' ', trim($name)));

        if (count($nameParts) > 1) {

            foreach ($nameParts as $key => $namePart) {

                $companyTypeId = CompanyType::find()
                    ->select('id')
                    ->andWhere(['name_short' => $namePart])
                    ->scalar();

                if ($companyTypeId) {
                    $this->andWhere([$this->getTableAlias().'.company_type_id' => $companyTypeId]);
                    unset($nameParts[$key]);
                    break;
                }
            }
        }

        return $this->andWhere(['or',
            ['like', $this->getTableAlias().'.ITN', $name],
            ['like', $this->getTableAlias().'.name', $name],
            ['like', $this->getTableAlias().'.name', implode(' ', $nameParts)],
        ]);
    }

    /**
     * @return static
     */
    public function sorted()
    {
        $c = $this->getTableAlias();
        $t = CompanyType::tableName();

        return $this
            ->joinWith('companyType', false)
            ->orderBy([
                "IF({{{$t}}}.[[name_short]] IS NULL, 1, 0)" => SORT_ASC,
                "$t.name_short" => SORT_ASC,
                "$c.name" => SORT_ASC,
            ]);
    }
}
