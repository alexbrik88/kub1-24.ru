<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "contractor_agent_payment_type".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Contractor[] $contractors
 */
class ContractorAgentPaymentType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contractor_agent_payment_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContractors()
    {
        return $this->hasMany(Contractor::className(), ['agent_payment_type_id' => 'id']);
    }
}
