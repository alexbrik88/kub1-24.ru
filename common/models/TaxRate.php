<?php

namespace common\models;

use common\components\helpers\ArrayHelper;
use Yii;

/**
 * This is the model class for table "tax_rate".
 *
 * @property integer $id
 * @property string $name
 * @property string $rate
 */
class TaxRate extends \yii\db\ActiveRecord
{
    const RATE_20 = 5;
    const RATE_18 = 1;
    const RATE_10 = 2;
    const RATE_0 = 3;
    const RATE_WITHOUT = 4;

    /**
     * @var TaxRate[]
     */
    private static $_items;

    /**
     * @param null $id
     * @return TaxRate|TaxRate[]
     */
    public static function items()
    {
        if (self::$_items === null) {
            self::$_items = self::find()->indexBy('id')->all();
        }

        return self::$_items;
    }

    /**
     * @param null $id
     * @return TaxRate|TaxRate[]
     */
    public static function get($id = null)
    {
        if (self::$_items === null) {
            self::$_items = ArrayHelper::getAssoc(self::find()->all(), 'id');
        }

        if ($id === null) {
            return self::items();
        }

        return self::items()[$id] ?? null;
    }

    /**
     * @param int $id
     */
    public static function rateById($id)
    {
        if (!empty($id)) {
            $item = self::items()[$id] ?? null;
            if ($item !== null) {
                return $item->rate;
            }
        }

        return '';
    }

    /**
     * @return TaxRate[]
     */
    public static function sortedArray()
    {
        return TaxRate::find()->orderBy(['rate' => SORT_DESC])->all();
    }

    /**
     * @return TaxRate[]
     */
    public static function sortedActualArray()
    {
        return TaxRate::find()->where(['<>', 'id', self::RATE_18])->orderBy(['rate' => SORT_DESC])->all();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tax_rate';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'rate' => 'Коэффициент',
        ];
    }

    /**
     * @return array
     */
    public static function taxRatesIdArray()
    {
        return array_keys(self::items());
    }
}
