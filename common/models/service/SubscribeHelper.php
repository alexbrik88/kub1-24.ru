<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 11/23/15
 * Time: 12:07 PM
 * Email: t.kanstantsin@gmail.com
 */

namespace common\models\service;

use common\components\date\DateHelper;
use common\components\helpers\ArrayHelper;
use common\services\Contractor\KubContractorFromCompany;
use common\models\address\Country;
use common\models\Company;
use common\models\Contractor;
use common\models\company\CompanyType;
use common\models\document\Invoice;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\OrderHelper;
use common\models\product\Product;
use common\models\product\ProductUnit;
use common\models\TaxationType;
use common\models\TaxRate;
use frontend\models\Documents;
use frontend\modules\documents\components\InvoiceHelper;
use frontend\modules\export\models\one_c\OneCExport;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use php_rutils\RUtils;
use Yii;
use yii\base\BaseObject;

/**
 * Class SubscribeHelper
 * @package common\models\service
 */
class SubscribeHelper
{
    const PRODUCT_TITLE = 'Подписка на сервис www.kub-24.ru';
    const PAYMENT_PERIOD_LIMIT = 10;

    /**
     *
     * @param Company $company
     * @return bool
     */
    public static function hasActualSubscription(Company $company, $groupId = SubscribeTariffGroup::STANDART)
    {
        return $company->getHasActualSubscription($groupId);
    }

    /**
     * @param Company $company
     * @param int $groupId
     * @return Subscribe|null
     */
    public static function findActualSubscription(Company $company, int $groupId = SubscribeTariffGroup::STANDART): ?Subscribe
    {
        /** @var Subscribe $actualSubscribe */
        $companyActiveSubscribesIndexedByGroup = $company->getCompanyActiveSubscribesIndexedByGroup();
        $actualSubscribe = ArrayHelper::getValue($companyActiveSubscribesIndexedByGroup, [$groupId, 'serviceSubscribe']);
        if ($actualSubscribe !== null) {
            $actualSubscribe->populateRelation('company', $company);
            if (!$actualSubscribe->getIsActual()) {
                $company->unlinkActiveSubscribe($groupId);
                unset($companyActiveSubscribesIndexedByGroup[$groupId]);
                $company->setCompanyActiveSubscribesIndexedByGroup($companyActiveSubscribesIndexedByGroup);
                $actualSubscribe = null;
            }
        }
        $actualSubscribeId = ArrayHelper::getValue($actualSubscribe, 'id');
        $lastExpired = static::getLastExpiredSubscription($company, $groupId);

        /** @var Subscribe[] $subscribeArray */
        $subscribeArray = array_merge(
            static::getNotExpiredSubscriptions($company, $groupId, $actualSubscribeId),
            static::getOnlyPayedSubscriptions($company, $groupId)
        );

        if (!empty($subscribeArray)) {
            foreach ($subscribeArray as $subscribe) {
                if ($actualSubscribe !== null) {
                    //if the company has more than one actual active subscription
                    if ($subscribe->status_id == SubscribeStatus::STATUS_ACTIVATED) {
                        $subscribe->updateAttributes([
                            'status_id' => SubscribeStatus::STATUS_PAYED,
                            'activated_at' => null,
                            'expired_at' => null,
                        ]);
                    }
                    if ($actualSubscribe->getIsLimitReached() || $actualSubscribe->getIsTrial()) {
                        $time = time();
                        if (in_array($groupId, SubscribeTariffGroup::$limitPerMonth) &&
                            ($monthsLeft = $actualSubscribe->getMonthsLeft()) > 0
                        ) {
                            $actualSubscribe->updateAttributes([
                                'duration_month' => $monthsLeft,
                                'duration_day' => 0,
                                'status_id' => SubscribeStatus::STATUS_PAYED,
                                'activated_at' => null,
                                'expired_at' => null,
                            ]);
                        } else {
                            $actualSubscribe->updateAttributes(['expired_at' => $time - 1]);
                        }
                        $subscribe->activate($time);
                        $company->setActiveSubscribe($subscribe);
                    }
                } else {
                    if ($lastExpired !== null) {
                        $subscribe->activate($lastExpired->expired_at + 1);
                    } else {
                        $subscribe->activate();
                    }

                    if ($subscribe->expired_at > time()) {
                        $company->setActiveSubscribe($subscribe);
                        $actualSubscribe = $subscribe;
                    } else {
                        $lastExpired = $subscribe;
                    }
                }
            }
        }

        return $actualSubscribe;
    }

    /**
     * @param Company $company
     * @param int $groupId
     * @param int $actualSubscribeId
     * @return Subscribe[]
     */
    public static function getNotExpiredSubscriptions(Company $company, $groupId = SubscribeTariffGroup::STANDART, $actualSubscribeId = null)
    {
        $query = Subscribe::find()->andWhere([
            'and',
            ['company_id' => $company->id],
            ['status_id' => SubscribeStatus::STATUS_ACTIVATED],
            ['tariff_group_id' => $groupId],
            ['>', 'expired_at', time()],
        ]);

        if (isset($actualSubscribeId)) {
            $query->andFilterWhere(['!=', 'id', $actualSubscribeId]);
        }

        $query->orderBy(['created_at' => SORT_ASC]);

        $modelArray = $query->all();

        foreach ($modelArray as $model) {
            $model->populateRelation('company', $company);
        }

        return $modelArray;
    }

    /**
     * @param Company $company
     * @return Subscribe[]
     */
    public static function getOnlyPayedSubscriptions(Company $company, $groupId = SubscribeTariffGroup::STANDART)
    {
        $modelArray = Subscribe::find()
            ->andWhere([
                'status_id' => SubscribeStatus::STATUS_PAYED,
                'company_id' => $company->id,
                'tariff_group_id' => $groupId,
            ])
            ->orderBy(['created_at' => SORT_ASC])
            ->all();

        foreach ($modelArray as $model) {
            $model->populateRelation('company', $company);
        }

        return $modelArray;
    }

    /**
     * @param Company $company
     * @return Subscribe|null
     */
    public static function getLastExpiredSubscription(Company $company, $groupId = SubscribeTariffGroup::STANDART)
    {
        $model = Subscribe::find()
            ->byStatus([SubscribeStatus::STATUS_ACTIVATED,])
            ->byCompany($company->id)
            ->andWHere(['tariff_group_id' => $groupId])
            ->andWHere(['<=', 'expired_at', time()])
            ->orderBy(['expired_at' => SORT_DESC])
            ->one();

        if ($model !== null) {
            $model->populateRelation('company', $company);
        }

        return $model;
    }

    /**
     * Finds all payed ar actual subscribes
     * @param $companyId
     * @return Subscribe[]
     */
    public static function getPayedSubscriptions($companyId, $groupId = SubscribeTariffGroup::STANDART)
    {
        return Subscribe::find()
            ->byStatus([SubscribeStatus::STATUS_PAYED, SubscribeStatus::STATUS_ACTIVATED,])
            ->byCompany($companyId)
            ->andWHere(['tariff_group_id' => $groupId])
            ->notExpired()
            ->orderBy(['created_at' => SORT_ASC])
            ->all();
    }

    /**
     * @param Subscribe[] $subscribeArray
     * @return int|false - int - unix timestamp, false - if no subscription persists
     */
    public static function getExpireDate(array $subscribeArray)
    {
        if (empty($subscribeArray)) {
            return false;
        }

        $payedMonths = 0;
        $payedDays = 0;

        if (count(array_unique(ArrayHelper::getColumn($subscribeArray, 'tariff_group_id'))) > 1) {
            throw new \yii\base\Exception('Multiple groups not allowed');
        }

        foreach ($subscribeArray as $subscribe) {
            if ($subscribe->status_id == SubscribeStatus::STATUS_PAYED) {
                $payedMonths += $subscribe->duration_month;
                $payedDays += $subscribe->duration_day;
            } else {
                $from = new \DateTime(date(DateHelper::FORMAT_DATE, time()));
                $to = new \DateTime(date(DateHelper::FORMAT_DATE, $subscribe->expired_at));

                $payedDays = $from->diff($to)->days;
            }
        }

        return strtotime("+{$payedMonths} month +{$payedDays} day");
    }

    /**
     * @param int $expiredAtDate
     * @return int|mixed
     */
    public static function getExpireLeftDays($expiredAtDate)
    {
        if ($expiredAtDate === false) {
            return 0;
        }

        $from = new \DateTime(date(DateHelper::FORMAT_DATE, time()));
        $to = new \DateTime(date(DateHelper::FORMAT_DATE, $expiredAtDate));

        return $from->diff($to)->days;
    }

    /**
     * Creates product by tariff
     * @param SubscribeTariff $tariff
     * @param bool $saveProduct
     * @return Product
     */
    public static function getSubscribeProduct(SubscribeTariff $tariff, $saveProduct = false)
    {
        $product = Product::findOne([
            'creator_id' => null,
            'company_id' => null,
            'production_type' => Product::PRODUCTION_TYPE_SERVICE,
            'title' => self::PRODUCT_TITLE,
        ]);
        if ($product === null) {
            $product = new Product([
                'creator_id' => null,
                'company_id' => null,
                'production_type' => Product::PRODUCTION_TYPE_SERVICE,
                'title' => self::PRODUCT_TITLE . " \"{$tariff->tariffGroup->name}\" на {$tariff->getTariffName()}",
                'code' => Product::DEFAULT_VALUE,
                'product_unit_id' => ProductUnit::UNIT_COUNT,
                'box_type' => Product::DEFAULT_VALUE,
                'count_in_place' => Product::DEFAULT_VALUE,
                'place_count' => Product::DEFAULT_VALUE,
                'mass_gross' => Product::DEFAULT_VALUE,
                'has_excise' => 0,
                'price_for_buy_nds_id' => TaxRate::RATE_WITHOUT,
                'price_for_buy_with_nds' => 0, // stored in kopecks
                'price_for_sell_nds_id' => TaxRate::RATE_WITHOUT,
                'price_for_sell_with_nds' => 0, // stored in kopecks
                'country_origin_id' => Country::COUNTRY_WITHOUT,
                'customs_declaration_number' => Product::DEFAULT_VALUE,
                'object_guid' => OneCExport::generateGUID(),
            ]);

            if ($saveProduct) {
                $product->save(false);
            }
        }

        return $product;
    }

    /**
     * @param Company $company
     * @param bool $save
     * @return Contractor|null
     */
    public static function getContractorByCompany(Company $company, $save = true)
    {
        $createContractorService = new KubContractorFromCompany($company);

        return $createContractorService->create($save);
/*
        $kubCompanyId = Yii::$app->kubCompany->id;
        $contractor = Contractor::findOne([
            //'type' => Contractor::TYPE_CUSTOMER,
            'company_id' => $kubCompanyId,
            'created_from_company_id' => $company->id,
        ]);
        $account = $company->mainCheckingAccountant;

        if ($contractor === null) {
            $contractor = new Contractor([
                'company_id' => $kubCompanyId,
                'type' => Contractor::TYPE_CUSTOMER,
                'status' => Contractor::ACTIVE,
                'company_type_id' => $company->company_type_id,
                'name' => !empty($company->name_full) ? $company->name_full : $company->name_short,
                'director_name' => $company->getChiefFio(),
                'director_email' => $company->email,
                'chief_accountant_is_director' => true,
                'contact_is_director' => true,
                'legal_address' => $company->getAddressLegalFull(),
                'actual_address' => $company->getAddressActualFull(),
                'BIN' => ($company->company_type_id == CompanyType::TYPE_IP) ? ($company->egrip ? $company->egrip : '') : $company->ogrn,
                'ITN' => $company->inn,
                'PPC' => $company->kpp,
                'current_account' => $account ? $account->rs : null,
                'bank_name' => $account ? $account->bank_name : null,
                'corresp_account' => $account ? $account->ks : null,
                'BIC' => $account ? $account->bik : null,
                'taxation_system' => $company->companyTaxationType->osno
                    ? Contractor::WITH_NDS : Contractor::WITHOUT_NDS,
                'is_deleted' => false,
                'object_guid' => OneCExport::generateGUID(),
                'created_from_company_id' => $company->id,
            ]);
        } else { //update the contractor for changies
            $contractor->is_customer = 1;
            $contractor->company_type_id = $company->company_type_id;
            $contractor->name = !empty($company->name_full) ? $company->name_full : $company->name_short;
            $contractor->director_name = $company->getChiefFio();
            $contractor->director_email = $company->email;
            $contractor->legal_address = $company->getAddressLegalFull();
            $contractor->actual_address = $company->getAddressActualFull();
            $contractor->BIN = ($company->company_type_id == CompanyType::TYPE_IP) ? ($company->egrip ? $company->egrip : '') : $company->ogrn;
            $contractor->ITN = $company->inn;
            $contractor->PPC = $company->kpp;
            $contractor->current_account = $account ? $account->rs : null;
            $contractor->bank_name = $account ? $account->bank_name : null;
            $contractor->corresp_account = $account ? $account->ks : null;
            $contractor->BIC = $account ? $account->bik : null;
            $contractor->taxation_system = $company->companyTaxationType->osno
                ? Contractor::WITH_NDS : Contractor::WITHOUT_NDS;
            $contractor->is_deleted = false;
        }

        if ($save && !$contractor->save(false)) { // without saving cauz company may not be properly filled.
            \common\components\helpers\ModelHelper::logErrors($contractor, __METHOD__);

            return null;
        }

        return $contractor;
*/
    }

    /**
     * Creates invoice by subscribe and its tariff
     * @param Payment $payment
     * @param SubscribeTariff $tariff
     * @return Invoice|false - false if save failed
     */
    public static function getInvoice(Payment $payment, $tariffArray, Company $contractorCompany)
    {
        /* @var Company $company KUB-service company model */
        $company = Yii::$app->kubCompany;
        $contractor = static::getContractorByCompany($contractorCompany);
        $orderCount = count($payment->orders);

        $invoice = new Invoice([
            'type' => Documents::IO_TYPE_OUT,
            'company_id' => $company->id,
            'contractor_id' => $contractor->id,
            'production_type' => Product::PRODUCTION_TYPE_SERVICE,
            'document_number' => (string) Invoice::getNextDocumentNumber($company, Documents::IO_TYPE_OUT, null, date(DateHelper::FORMAT_DATE)),
            'document_date' => date(DateHelper::FORMAT_DATE),
            'payment_limit_date' => date(DateHelper::FORMAT_DATE, strtotime('+' . self::PAYMENT_PERIOD_LIMIT . ' day')),
            'invoice_expenditure_item_id' => InvoiceExpenditureItem::ITEM_OTHER,
            'object_guid' => OneCExport::generateGUID(),
            'is_subscribe_invoice' => true,
            'service_payment_id' => $payment->id,
            'price_precision' => 2,
            'has_discount' => false,
            'nds_view_type_id' => $company->companyTaxationType->osno ?
                                  ($company->isNdsExclude ? Invoice::NDS_VIEW_OUT : Invoice::NDS_VIEW_IN) :
                                  Invoice::NDS_VIEW_WITHOUT,
        ]);

        $invoice->populateRelation('company', $company);
        $invoice->populateRelation('contractor', $contractor);

        $orderArray = [];
        $paymentOrders = [];
        foreach ($payment->orders as $paymentOrder) {
            if ($paymentOrder->discount) {
                $invoice->has_discount = true;
            }
            $paymentOrders[$paymentOrder->tariff_id][(string) $paymentOrder->discount][] = $paymentOrder;
        }
        foreach ($paymentOrders as $byDiscount) {
            foreach ($byDiscount as $discount => $paymentOrderArray) {
                $paymentOrder = reset($paymentOrderArray);
                $price = $paymentOrder->price;
                $tariff = $paymentOrder->tariff;
                $product = static::getSubscribeProduct($tariff, true);
                $order = OrderHelper::createOrderByProduct($product, $invoice, count($paymentOrderArray), $price * 100, $discount);
                $order->title = "{$order->title} \"{$tariff->tariffGroup->name}\" на {$tariff->getTariffName()}";
                $orderArray[] = $order;
            }
        }

        $invoice->populateRelation('orders', $orderArray);
        $invoice->total_amount_has_nds = $invoice->total_amount_nds > 0;

        $invoice->total_mass_gross = (string) array_sum(ArrayHelper::getColumn($invoice->orders, 'mass_gross'));
        $invoice->total_place_count = (string) array_sum(ArrayHelper::getColumn($invoice->orders, 'place_count'));

        $save = LogHelper::save($invoice, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE, function (Invoice $model) {
            if ($model->save()) {
                foreach ($model->orders as $order) {
                    $order->invoice_id = $model->id;
                    if (!$order->save()) {
                        \common\components\helpers\ModelHelper::logErrors($order, __METHOD__);

                        return false;
                    }
                }

                return true;
            }
            \common\components\helpers\ModelHelper::logErrors($model, __METHOD__);

            return false;
        });

        if ($save) {
            InvoiceHelper::afterSave($invoice->id);
        }

        return $save ? $invoice : null;
    }

    /**
     * @todo: create all-around method to clone invoice with all dependencies.
     * @param Invoice $outInvoice out invoice in kub-company
     * @param Company $company current company
     * @return Invoice|false
     */
    public static function getInInvoiceByOut(Invoice $outInvoice, Company $company)
    {
        /* @var Contractor $kubContractor */
        $kubContractor = Contractor::findOne(\Yii::$app->params['service']['contractor_id']);
        $kubItem = $kubContractor->getKubItem($company->id);

        $inInvoice = new Invoice([
            'type' => Documents::IO_TYPE_IN,
            'company_id' => $company->id,
            'contractor_id' => $kubContractor->id,
            'production_type' => $outInvoice->production_type,
            'document_number' => (string) $outInvoice->document_number,
            'document_date' => $outInvoice->document_date,
            'payment_limit_date' => $outInvoice->payment_limit_date,
            //'invoice_expenditure_item_id' => $outInvoice->invoice_expenditure_item_id,
            'object_guid' => OneCExport::generateGUID(),
            'nds_view_type_id' => $kubContractor->ndsViewTypeId,
            'is_subscribe_invoice' => $outInvoice->is_subscribe_invoice,
            'service_payment_id' => $outInvoice->service_payment_id,
            'price_precision' => 2,
            'has_discount' => $outInvoice->has_discount,
            'nds_view_type_id' => $outInvoice->nds_view_type_id,
            'invoice_expenditure_item_id' => ($kubItem->expenditure)
                ? $kubItem->expenditure->id
                : $outInvoice->invoice_expenditure_item_id
        ]);
        $inInvoice->populateRelation('company', $company);
        $inInvoice->populateRelation('contractor', $kubContractor);

        $product = new Product($outInvoice->orders[0]->product->getAttributes());
        $product->id = null;
        $product->creator_id = (\Yii::$app->params['paymentUser'] !== null) ? \Yii::$app->params['paymentUser']->id : Yii::$app->user->id;
        $product->company_id = $company->id;
        $product->price_for_buy_with_nds /= 100;
        $product->price_for_sell_with_nds /= 100;
        $product->title = $outInvoice->orders[0]->product_title;
        $product->save();

        $inOrderArray = [];
        foreach ($outInvoice->orders as $outOrder) {
            $inOrderArray[] = OrderHelper::createOrderByProduct(
                $product,
                $inInvoice,
                $outOrder->quantity,
                $outOrder->price * 100,
                $outOrder->discount
            );
        }

        $inInvoice->populateRelation('orders', $inOrderArray);
        $inInvoice->total_amount_has_nds = $outInvoice->total_amount_has_nds;

        $inInvoice->total_mass_gross = $outInvoice->total_mass_gross;
        $inInvoice->total_place_count = $outInvoice->total_place_count;

        $save = LogHelper::save($inInvoice, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE, function (Invoice $model) {
            if ($model->save()) {
                foreach ($model->orders as $order) {
                    $order->invoice_id = $model->id;
                    if (!$order->save()) {
                        \common\components\helpers\ModelHelper::logErrors($order, __METHOD__);

                        return false;
                    }
                }
                InvoiceHelper::afterSave($model->id);

                try {
                    \frontend\modules\analytics\models\PlanCashContractor::createFlowByInvoice($model);
                } catch (\Throwable $e) {
                    //
                }

                return true;
            }
            \common\components\helpers\ModelHelper::logErrors($model, __METHOD__);

            return false;
        });

        return $save ? $inInvoice : null;
    }


    /**
     * Returns human readable string of subscribe duration
     *
     * @param Subscribe|SubscribeTariff|PromoCode $model
     * @return string
     */
    public static function getReadableDuration($model)
    {
        $duration = [];

        if ($model->duration_month > 0) {
            $duration[] = RUtils::numeral()->getPlural($model->duration_month, ['месяц', 'месяца', 'месяцев']);
        }

        if ($model->duration_day > 0) {
            $duration[] = RUtils::numeral()->getPlural($model->duration_day, ['день', 'дня', 'дней']);
        }

        return join(' ', $duration);
    }
}
