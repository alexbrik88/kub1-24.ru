<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 8.12.15
 * Time: 15.07
 * Email: t.kanstantsin@gmail.com
 */

namespace common\models\service;
use yii\db\ActiveQuery;

/**
 * Class StatisticQuery
 * @package common\models\service
 */
class StatisticQuery extends ActiveQuery
{

    /**
     * @param int $year
     * @param int $month
     * @return $this
     */
    public function byDate($year, $month)
    {
        return $this->andWhere([
            'year' => $year,
            'month' => $month,
        ]);
    }

}
