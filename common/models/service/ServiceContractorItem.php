<?php

namespace common\models\service;

use Yii;
use common\models\Company;
use common\models\document\InvoiceIncomeItem;
use common\models\document\InvoiceExpenditureItem;

/**
 * This is the model class for table "service_contractor_item".
 *
 * @property int $company_id
 * @property int|null $invoice_income_item_id
 * @property int|null $invoice_expenditure_item_id
 *
 * @property Company $company
 * @property InvoiceExpenditureItem $expenditure
 * @property InvoiceIncomeItem $income
 */
class ServiceContractorItem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'service_contractor_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id'], 'required'],
            [['company_id', 'invoice_income_item_id', 'invoice_expenditure_item_id'], 'integer'],
            ['invoice_income_item_id', 'default', 'value' => InvoiceIncomeItem::ITEM_PAYMENT_FROM_BUYER],
            ['invoice_expenditure_item_id', 'default', 'value' => InvoiceExpenditureItem::ITEM_OTHER],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['company_id' => 'id']],
            [['invoice_expenditure_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => InvoiceExpenditureItem::class, 'targetAttribute' => ['invoice_expenditure_item_id' => 'id']],
            [['invoice_income_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => InvoiceIncomeItem::class, 'targetAttribute' => ['invoice_income_item_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'company_id' => 'Company ID',
            'invoice_income_item_id' => 'Income Item ID',
            'invoice_expenditure_item_id' => 'Expenditure Item ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpenditure()
    {
        return $this->hasOne(InvoiceExpenditureItem::class, ['id' => 'invoice_expenditure_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIncome()
    {
        return $this->hasOne(InvoiceIncomeItem::class, ['id' => 'invoice_income_item_id']);
    }
}
