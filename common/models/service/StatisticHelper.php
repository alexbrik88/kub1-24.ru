<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 8.12.15
 * Time: 14.54
 * Email: t.kanstantsin@gmail.com
 */

namespace common\models\service;

use yii\db\ActiveQuery;
use yii\helpers\ArrayHelper;

/**
 * Class StatisticHelper
 * @package common\models\service
 */
class StatisticHelper
{

    /**
     * @return int
     */
    public static function getServiceStatisticLastUpdate()
    {
        return (int) Statistic::find()->max('updated_at');
    }

    /**
     * @return int
     */
    public static function getSubscribeLastUpdate()
    {
        return (int) Subscribe::find()->max('updated_at');
    }

    /**
     * @return bool
     */
    public static function isSubscribeChanged()
    {
        return static::getSubscribeLastUpdate() > static::getServiceStatisticLastUpdate();
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getUpdatedPeriods()
    {
        return Subscribe::find()
            ->select([
                'YEAR(FROM_UNIXTIME(updated_at)) AS year',
                'MONTH(FROM_UNIXTIME(updated_at)) AS month',
            ])
            ->andWhere([
                '>', 'updated_at', static::getServiceStatisticLastUpdate(),
            ])
            ->groupBy([
                'YEAR(FROM_UNIXTIME(updated_at))',
                'MONTH(FROM_UNIXTIME(updated_at))',
            ])
            ->asArray()
            ->all();
    }

    /**
     * @note: potential error: set `payment info`, add new subscribe, set tariff -> not actual.
     * @todo: set condition subscribe.updated_at <= getSubscribeLastUpdate().
     * @param array $updatedPeriods
     */
    public static function updateStatistics(array $updatedPeriods)
    {
        var_dump($updatedPeriods);
        foreach ($updatedPeriods as $period) {
            $statistic = Statistic::find()->byDate($period['year'], $period['month'])->one();
            if ($statistic === null) {
                $statistic = new Statistic([
                    'year' => $period['year'],
                    'month' => $period['month'],
                ]);
            }
            var_dump($period);
            static::setStatistic($statistic);

            $statistic->save();
        }
    }

    /**
     * @param Statistic $statistic
     */
    public function setStatistic(Statistic $statistic)
    {
        $statistic->invoice_count = (int) Subscribe::find()
            ->byYearMonth($statistic->year, $statistic->month)
            ->count();

        $payedInfo = static::getPayedPaymentInfo($statistic, [SubscribeStatus::STATUS_PAYED, SubscribeStatus::STATUS_ACTIVATED]);
        list($statistic->payed_invoice_count, $statistic->payed_sum) = [(int) $payedInfo['count'], (int) $payedInfo['sum']];

        $notPayedInfo = static::getNotPayedPaymentInfo($statistic, [SubscribeStatus::STATUS_NEW]);
        list($statistic->not_payed_invoice_count, $statistic->not_payed_sum) = [(int) $notPayedInfo['count'], (int) $notPayedInfo['sum']];

        $byNovicePayedInfo = static::getPaymentInfoByNovice($statistic);
        list($statistic->payed_invoice_count_by_novice, $statistic->payed_sum_by_novice) = [(int) $byNovicePayedInfo['count'], (int) $byNovicePayedInfo['sum']];

        $tariffInfo = static::getTariffInfo($statistic);
        list($statistic->tariff_1, $statistic->tariff_2, $statistic->tariff_3) = [
            (int) ArrayHelper::getValue($tariffInfo, SubscribeTariff::TARIFF_1),
            (int) ArrayHelper::getValue($tariffInfo, SubscribeTariff::TARIFF_2),
            (int) ArrayHelper::getValue($tariffInfo, SubscribeTariff::TARIFF_3),
        ];

        $paymentTypeInfo = static::getPaymentTypeInfo($statistic);
        list($statistic->payment_type_1, $statistic->payment_type_2, $statistic->payment_type_3) = [
            (int) ArrayHelper::getValue($paymentTypeInfo, PaymentType::TYPE_ONLINE),
            (int) ArrayHelper::getValue($paymentTypeInfo, PaymentType::TYPE_RECEIPT),
            (int) ArrayHelper::getValue($paymentTypeInfo, PaymentType::TYPE_INVOICE),
        ];
    }

    /**
     * @param Statistic $statistic
     * @param array $statuses
     * @return array|null|\yii\db\ActiveRecord
     */
    protected static function getPayedPaymentInfo(Statistic $statistic, array $statuses)
    {
        return static::getInfoQuery($statistic)
            ->select(['COUNT(*) AS count', 'SUM(payment.sum) AS sum',])
            ->byStatus($statuses)
            ->asArray()
            ->one();
    }

    /**
     * @param Statistic $statistic
     * @param array $statuses
     * @return array|null|\yii\db\ActiveRecord
     */
    protected static function getNotPayedPaymentInfo(Statistic $statistic, array $statuses)
    {
        return static::getInfoQuery($statistic, false)
            ->select(['COUNT(*) AS count', 'SUM(tariff.price) AS sum',])
            ->joinWith(['tariff' => function (ActiveQuery $query) {
                return $query->from([
                    'tariff' => SubscribeTariff::tableName(),
                ]);
            }], false)
            ->byStatus($statuses)
            ->asArray()
            ->one();
    }

    /**
     * @param Statistic $statistic
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getPaymentInfoByNovice(Statistic $statistic)
    {
        return static::getInfoQuery($statistic)
            ->select(['COUNT(*) AS count', 'SUM(payment.sum) AS sum',])
            ->andWhere([
                'payment.by_novice' => true,
            ])
            ->asArray()
            ->one();
    }

    /**
     * @param Statistic $statistic
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getTariffInfo(Statistic $statistic)
    {
        $tariffInfo = static::getInfoQuery($statistic, false)
            ->select(['subscribe.tariff_id AS tariff_id', 'COUNT(*) AS count'])
            ->groupBy('tariff_id')
            ->asArray()
            ->all();
        $tariffInfo = ArrayHelper::map($tariffInfo, 'tariff_id', 'count');

        return $tariffInfo;
    }

    /**
     * @param Statistic $statistic
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getPaymentTypeInfo(Statistic $statistic)
    {
        $paymentTypeInfo = static::getInfoQuery($statistic)
            ->select(['payment.type_id AS type_id', 'COUNT(*) AS count'])
            ->groupBy('payment.type_id')
            ->asArray()
            ->all();
        $paymentTypeInfo = ArrayHelper::map($paymentTypeInfo, 'type_id', 'count');

        return $paymentTypeInfo;
    }

    /**
     * @param Statistic $statistic
     * @param bool $setPaymentRelation
     * @return SubscribeQuery|ActiveQuery
     */
    protected static function getInfoQuery(Statistic $statistic, $setPaymentRelation = true)
    {
        $query = Subscribe::find()
            ->from(['subscribe' => Subscribe::tableName(),])
            ->andWhere(static::getNotTrialCondition())
            ->andWhere(static::getSubscribeByMonthCondition($statistic, 'subscribe'));

        if ($setPaymentRelation) {
            $query
                ->joinWith(['payment' => function (ActiveQuery $query) {
                    return $query->from([
                        'payment' => Payment::tableName(),
                    ]);
                }], false)
                ->andWhere(static::getNotPromoCondition());
        }

        return $query;
    }

    /**
     * @return array
     */
    protected static function getNotPromoCondition()
    {
        return [
            '<>', 'payment.type_id', PaymentType::TYPE_PROMO_CODE,
        ];
    }

    /**
     * @return array
     */
    protected static function getNotTrialCondition()
    {
        return [
            '<>', 'subscribe.tariff_id', SubscribeTariff::TARIFF_TRIAL,
        ];
    }

    /**
     * @param Statistic $statistic
     * @param null $prefix
     * @return array
     */
    protected static function getSubscribeByMonthCondition(Statistic $statistic, $prefix = null)
    {
        $columnName = 'updated_at';
        $columnName = $prefix ? $prefix . '.' . $columnName : $columnName;

        return ['AND',
            ['=', "YEAR(FROM_UNIXTIME($columnName))", $statistic->year],
            ['=', "MONTH(FROM_UNIXTIME($columnName))", $statistic->month],
        ];
    }
}
