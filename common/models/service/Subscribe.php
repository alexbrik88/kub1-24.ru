<?php

namespace common\models\service;

use common\components\date\DateHelper;
use common\models\Company;
use common\models\document\Invoice;
use common\models\employee\Employee;
use common\models\product\Product;
use common\models\report\ReportState;
use common\models\selling\SellingSubscribe;
use frontend\models\Documents;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use common\models\service\PaymentType;

/**
 * This is the model class for table "service_subscribe".
 *
 * @property integer $id
 * @property integer $company_id
 * @property integer $tariff_id
 * @property integer $tariff_group_id
 * @property integer $tariff_limit
 * @property integer $duration_month
 * @property integer $duration_day
 * @property integer $activated_at
 * @property integer $expired_at
 * @property integer $status_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $reward
 * @property integer $payment_type_id
 *
 * @property Company $company
 * @property Company $companyInActive
 * @property SubscribeStatus $status
 * @property SubscribeTariff $tariff
 * @property Payment $payment
 * @property Invoice $invoice
 * @property ReportState $reportState
 * @property SellingSubscribe $sellingSubscribe
 * @property PaymentType $paymentType
 * @property-read bool $isTrial
 *
 * @mixin TimestampBehavior
 */
class Subscribe extends ActiveRecord
{
    const REWARD_PERCENT = 20;

    private $_limitUsed;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service_subscribe';
    }

    /**
     * @return SubscribeQuery
     */
    public static function find()
    {
        return new SubscribeQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'status_id'], 'required'],
            [
                ['company_id', 'tariff_id', 'activated_at', 'expired_at', 'status_id', 'reward'],
                'integer',
            ],
            [['discount'], 'number', 'numberPattern' => '/^\d+(\.\d*)?$/', 'min' => 0, 'max' => 99.9999],
            [['discount'], 'filter', 'filter' => function ($value) {
                return round($value, 4);
            },],

            // relations
            [['tariff_id'], 'exist', 'targetClass' => SubscribeTariff::className(), 'targetAttribute' => 'id',],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Компания',
            'tariff_id' => 'Тариф',
            'duration_month' => 'Длительность (месяцев)',
            'duration_day' => 'Длительность (дней)',
            'activated_at' => 'Дата активации',
            'expired_at' => 'Дата окончания',
            'status_id' => 'Статус',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
            'reward' => 'Вознаграждение',
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if ($this->isAttributeChanged('status_id')) {
            if ($this->status_id == SubscribeStatus::STATUS_PAYED &&
                $this->company->invited_by_company_id &&
                $this->tariff_id !== SubscribeTariff::TARIFF_TRIAL
            ) {
                $invitedByCompany = $this->company->invitedByCompany;
                if ($this->payment && !$invitedByCompany->checkInvitedCompanyOnPayed($this->company->id)) {
                    $reward = round($this->payment->sum * self::REWARD_PERCENT / 100);
                    $this->reward = $reward;
                    $invitedByCompany->affiliate_sum += $reward;
                    $invitedByCompany->total_affiliate_sum += $reward;

                    $invitedByCompany->save(true, ['affiliate_sum', 'total_affiliate_sum']);
                }
            }
        }

        return true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyInActive()
    {
        return $this->hasOne(Company::className(), ['active_subscribe_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(SubscribeStatus::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(SubscribeTariff::className(), ['id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayment()
    {
        return $this->hasOne(Payment::className(), ['id' => 'payment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentOrder()
    {
        return $this->hasOne(PaymentOrder::className(), ['id' => 'payment_order_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariffGroup()
    {
        return $this->hasOne(SubscribeTariffGroup::className(), ['id' => 'tariff_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyActiveSubscribes()
    {
        return $this->hasMany(ActiveSubscribe::className(), ['subscribe_id' => 'id']);
    }

    /**
     * @return Invoice|null
     */
    public function getInvoice()
    {
        return $this->getOutInvoice();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoices()
    {
        return $this->hasMany(Invoice::className(), ['subscribe_id' => 'id']);
    }

    /**
     * @return Invoice|null
     */
    public function getOutInvoice()
    {
        return $this->getInvoices()->andWhere(['type' => Documents::IO_TYPE_OUT, 'is_deleted' => 0])->one();
    }

    /**
     * @return boolean
     */
    public function getIsActual()
    {
        return $this->status_id == SubscribeStatus::STATUS_ACTIVATED && $this->expired_at > time();
    }

    /**
     * @param bool|false $withPromoText
     * @return string
     */
    public function getTariffName($withPromoText = false)
    {
        if ($tariff = $this->tariff) {
            $result = $this->tariff->getTariffName();
        } else {
            $result = $withPromoText ? 'Промокод (' . SubscribeHelper::getReadableDuration($this) . ')' : SubscribeHelper::getReadableDuration($this);
        }


        return $result;
    }

    /**
     * @param int $time unix timestamp
     * @return string
     */
    public function activate($time = null)
    {
        $payment_date = ArrayHelper::getValue($this, 'payment.payment_date') ? : time();
        $this->activated_at = max($payment_date, intval($time));

        $date = new \DateTime();
        $date->setTimestamp($this->activated_at);
        $date->modify("-1 day");
        $date->setTime(23, 59, 59);

        if ($this->duration_month) {
            $date->modify("+{$this->duration_month} month");
        }
        if ($this->duration_day) {
            $date->modify("+{$this->duration_day} day");
        }
        $expired_at = $date->getTimestamp();

        $this->expired_at = max($this->activated_at, $expired_at);
        $this->status_id = SubscribeStatus::STATUS_ACTIVATED;

        $this->save(false, ['activated_at', 'expired_at', 'status_id']);

        if ($this->sellingSubscribe) {
            $sellingSubscribe = $this->sellingSubscribe;
            $sellingSubscribe->activation_date = $this->activated_at;
            $sellingSubscribe->end_date = $this->expired_at;
            $sellingSubscribe->save(true, ['activation_date', 'end_date']);
        }
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            if ($this->company->active_subscribe_id == $this->id) {
                $this->company->updateAttributes(['active_subscribe_id' => null]);
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     *
     */
    public function createReportState()
    {
        $tariffsByGroup = SubscribeTariff::getTariffsByTariff($this->tariff_id);
        if ($tariffsByGroup && !ReportState::find()->andWhere(['company_id' => $this->company_id])->andWhere(['tariff_id' => $tariffsByGroup])->exists()) {
            if (($this->status_id == SubscribeStatus::STATUS_PAYED || $this->status_id == SubscribeStatus::STATUS_ACTIVATED) &&
                !in_array($this->tariff_id, [SubscribeTariff::TARIFF_TRIAL, null])
            ) {
                $reportState = new ReportState();
                $reportState->company_id = $this->company_id;
                $reportState->subscribe_id = $this->id;
                $reportState->tariff_id = $this->tariff_id;
                $reportState->pay_date = $this->payment->payment_date;
                $reportState->days_without_payment = intval(ceil(($reportState->pay_date - $reportState->company->created_at) / (60 * 60 * 24)));
                $reportState->pay_sum = $this->payment->sum;
                $reportState->invoice_count = $this->company->getOutInvoiceCount();
                $reportState->act_count = $this->company->getOutActCount();
                $reportState->packing_list_count = $this->company->getOutPackingListCount();
                $reportState->invoice_facture_count = $this->company->getOutInvoiceFactureCount();
                $reportState->upd_count = $this->company->getOutUpdCount();
                $reportState->has_logo = (bool) $this->company->logo_link;
                $reportState->has_print = (bool) $this->company->print_link;
                $reportState->has_signature = (bool) $this->company->chief_signature_link;
                $reportState->sum_company_images = (int) ($reportState->has_logo + $reportState->has_print + $reportState->has_signature);
                $reportState->employees_count = Employee::find()
                    ->byCompany($reportState->company_id)
                    ->byIsActive(Employee::ACTIVE)
                    ->byIsDeleted(Employee::NOT_DELETED)
                    ->byIsWorking(Employee::STATUS_IS_WORKING)
                    ->andWhere(['not', ['email' => 'support@kub-24.ru']])
                    ->andWhere(['<=', Employee::tableName() . '.created_at', $reportState->pay_date])
                    ->count();
                $reportState->company_type_id = $reportState->company->company_type_id;
                $reportState->company_taxation_type_name = $reportState->company->companyTaxationType->name;
                $reportState->product_count = $this->company->getProducts()->andWhere(['production_type' => Product::PRODUCTION_TYPE_GOODS])->count();
                $reportState->service_count = $this->company->getProducts()->andWhere(['production_type' => Product::PRODUCTION_TYPE_SERVICE])->count();
                $reportState->download_statement_count = $this->company->getStateFromBankCount();
                $reportState->download_1c_count = $this->company->getOneCExportCount();
                $reportState->import_xls_product_count = $this->company->import_xls_product_count;
                $reportState->import_xls_service_count = $this->company->import_xls_service_count;

                $reportState->save();
            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReportState()
    {
        return $this->hasOne(ReportState::className(), ['subscribe_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSellingSubscribe()
    {
        return $this->hasOne(SellingSubscribe::className(), ['subscribe_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentType()
    {
        return $this->hasOne(PaymentType::className(), ['id' => 'payment_type_id']);
    }

    /**
     * @return integer
     */
    public function getLimit()
    {
        return $this->tariff_limit ?? SubscribeTariff::UNLIMIT_COUNT;
    }

    /**
     * @return integer
     */
    public function getInvoiceLimit()
    {
        if ($this->tariff_group_id == SubscribeTariffGroup::STANDART) {
            return $this->tariff_limit ?? SubscribeTariff::UNLIMIT_COUNT;
        } else {
            return Yii::$app->params['free_tariff']['invoice_limit'];
        }
    }

    /**
     * @return integer
     */
    public function getLimitFromTime()
    {
        $from = $this->activated_at;
        if (in_array($this->tariff_group_id, SubscribeTariffGroup::$payPerQuantity)) {
            return $from;
        }
        $currentDate = date_create();
        $date = date_create('today');
        $date->setTimestamp($from);
        $date->setTime(0, 0);
        $diff = intval(date_diff($date, $currentDate)->format('%m'));
        if ($diff > 0) {
            return $date->modify("+{$diff} month")->getTimestamp();
        } else {
            return $from;
        }
    }

    public function getLimitNextTime()
    {
        $date = date_create();
        $date->setTimestamp($this->getLimitFromTime());
        return $date->modify('+1 month')->getTimestamp();
    }

    /**
     * @return integer
     */
    public function getLimitUsed()
    {
        if (!isset($this->_limitUsed)) {
            switch ($this->tariff_group_id) {
                case SubscribeTariffGroup::STANDART:
                    $this->_limitUsed = $this->company->getOutInvoiceCreatedCount($this->getLimitFromTime());
                    break;

                case SubscribeTariffGroup::PRICE_LIST:
                    $this->_limitUsed = $this->company->getCreatedPriceLists()->andWhere([
                        'or',
                        ['>=', 'created_at', $this->getLimitFromTime()],
                        [
                            'and',
                            ['not', ['status_id' => \common\models\product\PriceListStatus::STATUS_ARCHIVE]],
                            ['is_deleted' => false],
                        ],
                    ])->count();
                    break;

                case SubscribeTariffGroup::CHECK_CONTRACTOR:
                    $this->_limitUsed = $this->company->getDossierLogs()->andWhere([
                        '>=', 'created_at', $this->getLimitFromTime(),
                    ])->count();
                    break;

                default:
                    $this->_limitUsed = 0;
                    break;
            }
        }

        return $this->_limitUsed;
    }

    /**
     * @return integer
     */
    public function getLimitLeft()
    {
        $limit = $this->tariff_limit ?? SubscribeTariff::UNLIMIT_COUNT;

        return max(0, $limit - $this->getLimitUsed());
    }

    /**
     * @return boolean
     */
    public function getIsLimitReached()
    {
        return $this->getLimitLeft() == 0;
    }

    /**
     * @return boolean
     */
    public function getIsTrial()
    {
        return $this->tariff && $this->tariff->price == 0;
    }

    /**
     * @return integer
     */
    public function getMonthsLeft()
    {
        $currentDate = date_create();
        $currentDate->setTime(23, 59, 59);
        $date = date_create();
        $date->setTimestamp($this->expired_at);
        $date->setTime(23, 59, 59);

        return intval(date_diff($currentDate, $date)->format('%m'));
    }
}
