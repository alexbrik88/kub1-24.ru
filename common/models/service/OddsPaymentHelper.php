<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 13.06.2018
 * Time: 9:39
 */

namespace common\models\service;

use common\components\date\DateHelper;
use common\components\helpers\ArrayHelper;
use common\models\address\Country;
use common\models\Company;
use common\models\Contractor;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\NdsViewType;
use common\models\document\OrderHelper;
use common\models\product\Product;
use common\models\product\ProductUnit;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use common\models\document\Invoice;
use common\models\TaxRate;
use frontend\models\Documents;
use Yii;
use common\models\company\CompanyType;
use frontend\modules\export\models\one_c\OneCExport;

/**
 * Class OddsPaymentHelper
 * @package common\models\service
 */
class OddsPaymentHelper
{
    /**
     * Период оплаты
     */
    const PAYMENT_PERIOD_LIMIT = 10;

    /**
     * @param Company $company
     * @param bool $save
     * @return Contractor|false
     */
    public static function getContractorByCompany(Company $company, $save = true)
    {
        $contractor = Contractor::findOne([
            //'type' => Contractor::TYPE_CUSTOMER,
            'company_id' => Yii::$app->params['service']['company_id'],
            'created_from_company_id' => $company->id,
        ]);

        if ($contractor === null) {
            $contractor = new Contractor([
                'company_id' => Yii::$app->params['service']['company_id'],
                'type' => Contractor::TYPE_CUSTOMER,
                'status' => Contractor::ACTIVE,
                'company_type_id' => $company->company_type_id,
                'name' => !empty($company->name_full) ? $company->name_full : $company->name_short,
                'director_name' => $company->getChiefFio(),
                'director_email' => $company->email,
                'chief_accountant_is_director' => true,
                'contact_is_director' => true,
                'legal_address' => $company->getAddressLegalFull(),
                'actual_address' => $company->getAddressActualFull(),
                'BIN' => ($company->company_type_id == CompanyType::TYPE_IP) ? ($company->egrip ? $company->egrip : '') : $company->ogrn,
                'ITN' => $company->inn,
                'PPC' => $company->kpp,
                'current_account' => $company->mainCheckingAccountant->rs,
                'bank_name' => $company->mainCheckingAccountant->bank_name,
                'corresp_account' => $company->mainCheckingAccountant->ks,
                'BIC' => $company->mainCheckingAccountant->bik,
                'taxation_system' => $company->companyTaxationType->osno
                    ? Contractor::WITH_NDS : Contractor::WITHOUT_NDS,
                'is_deleted' => false,
                'object_guid' => OneCExport::generateGUID(),
                'created_from_company_id' => $company->id,
            ]);
        } else { //update the contractor for changies
            $contractor->is_customer = 1;
            $contractor->company_type_id = $company->company_type_id;
            $contractor->name = !empty($company->name_full) ? $company->name_full : $company->name_short;
            $contractor->director_name = $company->getChiefFio();
            $contractor->director_email = $company->email;
            $contractor->legal_address = $company->getAddressLegalFull();
            $contractor->actual_address = $company->getAddressActualFull();
            $contractor->BIN = ($company->company_type_id == CompanyType::TYPE_IP) ? ($company->egrip ? $company->egrip : '') : $company->ogrn;
            $contractor->ITN = $company->inn;
            $contractor->PPC = $company->kpp;
            $contractor->current_account = $company->mainCheckingAccountant->rs;
            $contractor->bank_name = $company->mainCheckingAccountant->bank_name;
            $contractor->corresp_account = $company->mainCheckingAccountant->ks;
            $contractor->BIC = $company->mainCheckingAccountant->bik;
            $contractor->taxation_system = $company->companyTaxationType->osno
                ? Contractor::WITH_NDS : Contractor::WITHOUT_NDS;
            $contractor->is_deleted = false;
        }

        if ($save && !$contractor->save(false)) { // without saving cauz company may not be properly filled.
            return false;
        }

        return $contractor;
    }

    /**
     * Creates product by tariff
     * @param bool $saveProduct
     * @return Product
     */
    public static function getStoreProduct($saveProduct = false)
    {
        $product = new Product([
            'creator_id' => null,
            'company_id' => null,
            'production_type' => Product::PRODUCTION_TYPE_SERVICE,
            'title' => 'Настройка финансовых отчетов',
            'code' => Product::DEFAULT_VALUE,
            'product_unit_id' => ProductUnit::UNIT_COUNT,
            'box_type' => Product::DEFAULT_VALUE,
            'count_in_place' => Product::DEFAULT_VALUE,
            'place_count' => Product::DEFAULT_VALUE,
            'mass_gross' => Product::DEFAULT_VALUE,
            'has_excise' => 0,
            'price_for_buy_nds_id' => TaxRate::RATE_WITHOUT,
            'price_for_buy_with_nds' => 750000, // stored in kopecks
            'price_for_sell_nds_id' => TaxRate::RATE_WITHOUT,
            'price_for_sell_with_nds' => 750000, // stored in kopecks
            'country_origin_id' => Country::COUNTRY_WITHOUT,
            'customs_declaration_number' => Product::DEFAULT_VALUE,
            'object_guid' => OneCExport::generateGUID(),
        ]);

        if ($saveProduct) {
            $product->save(false);
        }

        return $product;
    }

    /**
     * @param Payment $payment
     * @param Company $contractorCompany
     * @return Invoice|null
     */
    public static function getInvoice(Payment $payment, Company $contractorCompany)
    {
        /* @var Company $company KUB-service company model */
        $company = Yii::$app->kubCompany;
        $contractor = static::getContractorByCompany($contractorCompany);

        $invoice = new Invoice([
            'type' => Documents::IO_TYPE_OUT,
            'company_id' => $company->id,
            'contractor_id' => $contractor->id,
            'production_type' => Product::PRODUCTION_TYPE_SERVICE,
            'document_number' => (string)Invoice::getNextDocumentNumber($company, Documents::IO_TYPE_OUT, null, date(DateHelper::FORMAT_DATE)),
            'document_date' => date(DateHelper::FORMAT_DATE),
            'payment_limit_date' => date(DateHelper::FORMAT_DATE, strtotime('+' . self::PAYMENT_PERIOD_LIMIT . ' day')),
            'invoice_expenditure_item_id' => InvoiceExpenditureItem::ITEM_OTHER,
            'object_guid' => OneCExport::generateGUID(),
            'is_subscribe_invoice' => true,
            'service_payment_id' => $payment->id,
            'has_discount' => false,
            'price_precision' => 2,
            'nds_view_type_id' => NdsViewType::NDS_VIEW_WITHOUT,
        ]);
        $invoice->populateRelation('company', $company);
        $invoice->populateRelation('contractor', $contractor);

        $product = static::getStoreProduct(true);

        $paymentOrdersByDiscount = [];
        foreach ($payment->orders as $paymentOrder) {
            if ($paymentOrder->discount) {
                $invoice->has_discount = true;
            }
            $paymentOrdersByDiscount[(string)$paymentOrder->discount][] = $paymentOrder;
        }
        $orderArray = [];
        foreach ($paymentOrdersByDiscount as $discount => $paymentOrderArray) {
            $orderArray[] = OrderHelper::createOrderByProduct($product, $invoice, count($paymentOrderArray), $paymentOrderArray[0]->sum * 100, $discount * 1);
        }

        $invoice->populateRelation('orders', $orderArray);
        $invoice->total_amount_has_nds = $invoice->total_amount_nds > 0;

        $invoice->total_mass_gross = (string)array_sum(ArrayHelper::getColumn($invoice->orders, 'mass_gross'));
        $invoice->total_place_count = (string)array_sum(ArrayHelper::getColumn($invoice->orders, 'place_count'));

        $save = LogHelper::save($invoice, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE, function (Invoice $model) {
            if ($model->save()) {
                foreach ($model->orders as $order) {
                    $order->invoice_id = $model->id;
                    if (!$order->save()) {
                        return false;
                    }
                }

                return true;
            }

            return false;
        });

        return $save ? $invoice : null;
    }

    /**
     * @todo: create all-around method to clone invoice with all dependencies.
     * @param Invoice $outInvoice out invoice in kub-company
     * @param Company $company current company
     * @return Invoice|false
     */
    public static function getInInvoiceByOut(Invoice $outInvoice, Company $company)
    {
        /* @var Contractor $kubContractor */
        $kubContractor = Contractor::findOne(\Yii::$app->params['service']['contractor_id']);
        $kubItem = $kubContractor->getKubItem($company->id);

        $inInvoice = new Invoice([
            'type' => Documents::IO_TYPE_IN,
            'company_id' => $company->id,
            'contractor_id' => $kubContractor->id,
            'production_type' => $outInvoice->production_type,
            'document_number' => (string)$outInvoice->document_number,
            'document_date' => $outInvoice->document_date,
            'payment_limit_date' => $outInvoice->payment_limit_date,
            //'invoice_expenditure_item_id' => $outInvoice->invoice_expenditure_item_id,
            'object_guid' => OneCExport::generateGUID(),
            'nds_view_type_id' => $kubContractor->ndsViewTypeId,
            'is_subscribe_invoice' => $outInvoice->is_subscribe_invoice,
            'service_payment_id' => $outInvoice->service_payment_id,
            'has_discount' => $outInvoice->has_discount,
            'price_precision' => 2,
            'invoice_expenditure_item_id' => ($kubItem->expenditure)
                ? $kubItem->expenditure->id
                : $outInvoice->invoice_expenditure_item_id
        ]);
        $inInvoice->populateRelation('company', $company);
        $inInvoice->populateRelation('contractor', $kubContractor);

        $product = new Product($outInvoice->orders[0]->product->getAttributes());
        $product->id = null;
        $product->creator_id = (\Yii::$app->params['paymentUser'] !== null) ? \Yii::$app->params['paymentUser']->id : Yii::$app->user->id;
        $product->company_id = $company->id;
        $product->price_for_buy_with_nds /= 100;
        $product->price_for_sell_with_nds /= 100;
        $product->save();

        $inOrderArray = [];
        foreach ($outInvoice->orders as $outOrder) {
            $inOrderArray[] = OrderHelper::createOrderByProduct(
                $product,
                $inInvoice,
                $outOrder->quantity,
                $outOrder->selling_price_with_vat,
                $outOrder->discount
            );
        }

        $inInvoice->populateRelation('orders', $inOrderArray);
        $inInvoice->total_amount_has_nds = $outInvoice->total_amount_has_nds;

        $inInvoice->total_mass_gross = $outInvoice->total_mass_gross;
        $inInvoice->total_place_count = $outInvoice->total_place_count;

        $save = LogHelper::save($inInvoice, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE, function (Invoice $model) {
            if ($model->save()) {
                foreach ($model->orders as $order) {
                    $order->invoice_id = $model->id;
                    if (!$order->save()) {
                        return false;
                    }
                }

                return true;
            }

            return false;
        });

        return $save ? $inInvoice : null;
    }
}