<?php

namespace common\models\service;

use common\components\date\DateHelper;
use common\components\date\DatePickerFormatBehavior;
use common\components\TextHelper;
use common\models\Company;
use common\models\company\Experience;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\StringHelper;

/**
 * This is the model class for table "service_payment_promo_code".
 *
 * @property integer $id
 * @property string $code
 * @property string $name - "акция" - not separated entity because no any management planned
 * @property integer $duration_month
 * @property integer $duration_day
 * @property integer $is_used
 * @property integer $used_count
 * @property integer $created_at
 * @property integer $activated_at - when first user applies promo code
 * @property integer $started_at - when promo code will be available for activating
 * @property integer $expired_at - when promo code ends
 * @property integer $days_from_registration
 * @property boolean $email_send
 * @property integer $type
 * @property integer $cabinets_count
 * @property integer $out_invoice_count
 * @property integer $group_id
 * @property integer $for_company_experience_id
 * @property integer $company_id
 * @property integer $tariff_group_id
 * @property integer $tariff_limit
 *
 * @property Payment[] $payments
 * @property PromoCodeGroup $group
 * @property Experience $forCompanyExperience
 * @property Company $company
 *
 * @mixin TimestampBehavior
 * @mixin DatePickerFormatBehavior
 */
class PromoCode extends ActiveRecord
{

    const CODE_MAX_LENGTH = 10;
    const CODE_MIN_LENGTH = 5;

    const NOT_USED = 'Нет';
    const USED = 'Да';

    const TYPE_SUBSCRIBE = 1;
    const TYPE_CABINET = 2;
    const TYPE_OUT_INVOICE = 3;

    /**
     * @var array
     */
    public static $typeArray = [
        self::TYPE_SUBSCRIBE => 'Куб',
        self::TYPE_CABINET => 'Кабинет',
        self::TYPE_OUT_INVOICE => 'Виджет',
    ];


    /**
     * @return PromoCodeQuery
     */
    public static function find()
    {
        return new PromoCodeQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service_payment_promo_code';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
            ],
            'documentDateBehavior' => [
                'class' => DatePickerFormatBehavior::className(),
                'formatTo' => DateHelper::FORMAT_DATE,
                'attributes' => [
                    'started_at', 'expired_at',
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // filters
            [['duration_month', 'duration_day'], 'filter', 'filter' => 'intval',],
            [['name', 'code'], 'filter', 'filter' => 'trim',],
            [['code'], 'filter', 'filter' => function ($value) {
                if (empty($value)) {
                    // TODO: random validator.
                    for ($i = 0; $i < 100; $i++) {
                        $code = TextHelper::randString(PromoCode::CODE_MAX_LENGTH);
                        $isUsed = PromoCode::find()->byCode($code)->count();

                        if ($isUsed === false) { // error on query
                            break;
                        }
                        if ((int)$isUsed === 0) { // code is free
                            return $code;
                        }
                    }
                }

                return $value;
            },],

            // rules
            [['group_id', 'type'], 'required'],

            // code duration
            [['duration_month', 'duration_day', 'tariff_limit', 'type', 'cabinets_count', 'out_invoice_count',], 'integer'],
            [['duration_month'], 'required', 'when' => function ($model, $attribute) {
                return empty($model->duration_day);
            }, 'enableClientValidation' => false,],
            [['duration_day'], 'required', 'when' => function ($model, $attribute) {
                return empty($model->duration_month);
            }, 'enableClientValidation' => false,],
            [['cabinets_count'], 'required', 'when' => function ($model, $attribute) {
                return $model->type == self::TYPE_CABINET;
            }, 'enableClientValidation' => false,],
            [['out_invoice_count'], 'required', 'when' => function ($model, $attribute) {
                return $model->type == self::TYPE_OUT_INVOICE;
            }, 'enableClientValidation' => false,],
            // code
            [['code'], 'string', 'min' => static::CODE_MIN_LENGTH, 'max' => static::CODE_MAX_LENGTH],
            [['code'], 'unique'],

            [['name'], 'string', 'max' => 45],

            [['started_at', 'expired_at',], 'required'],

            // relations check
            [['group_id'], 'exist', 'targetClass' => PromoCodeGroup::className(), 'targetAttribute' => 'id',],
            [['for_company_experience_id'], 'exist', 'targetClass' => Experience::className(), 'targetAttribute' => 'id',],
            [['company_id'], 'exist', 'targetClass' => Company::className(), 'targetAttribute' => 'id',],
            [['tariff_group_id'], 'exist', 'targetClass' => SubscribeTariffGroup::className(), 'targetAttribute' => 'id',],
            [['tariff_limit'], 'required', 'when' => function ($model, $attribute) {
                return in_array($model->tariff_group_id, SubscribeTariffGroup::$hasLimit);
            }, 'enableClientValidation' => false,],
            ['days_from_registration', 'integer', 'min' => 1, 'max' => 99],
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeValidate()
    {
        if ($this->group_id == PromoCodeGroup::GROUP_GROUPED) {
            $this->company_id = null;
        }

        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->tariff_limit === '') {
                $this->tariff_limit = null;
            }

            return true;
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Промокод',
            'name' => 'Акция',
            'duration_month' => 'Длительность (месяцев)',
            'duration_day' => 'Длительность (дней)',
            'is_used' => 'Использован',
            'used_count' => 'Использовано раз',
            'created_at' => 'Дата создания',
            'activated_at' => 'Дата активации',
            'started_at' => 'Дата начала',
            'expired_at' => 'Дата окончания',
            'group_id' => 'Тип',
            'for_company_experience_id' => 'Для кого',
            'company_id' => 'Пользователь',
            'type' => 'Тип',
            'cabinets_count' => 'Количество кабинетов',
            'out_invoice_count' => 'Количество ссылок',
            'tariff_group_id' => 'Сервис',
            'tariff_limit' => 'Лимит',
            'days_from_registration' => 'Активен после регистрации дней',
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeHints()
    {
        return [
            'code' => 'Оставьте пустым, чтобы сгенерировать автоматически',
            'name' => 'Название промокода',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayments()
    {
        return $this->hasOne(Payment::className(), ['promo_code_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(PromoCodeGroup::className(), ['id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariffGroup()
    {
        return $this->hasOne(SubscribeTariffGroup::className(), ['id' => 'tariff_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForCompanyExperience()
    {
        return $this->hasOne(Experience::className(), ['id' => 'for_company_experience_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
}
