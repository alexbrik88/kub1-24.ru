<?php

namespace common\models\service;

use Yii;

/**
 * This is the model class for table "service_invoice_tariff".
 *
 * @property integer $id
 * @property integer $invoice_count
 * @property integer $total_amount
 *
 * @property Payment[] $payments
 */
class ServiceInvoiceTariff extends \yii\db\ActiveRecord
{
    const ONE_INVOICE = 4;
    const THREE_INVOICES = 5;
    const HUNDRED_INVOICES = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service_invoice_tariff';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['invoice_count', 'total_amount'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'invoice_count' => 'Invoice Count',
            'total_amount' => 'Total Amount',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayments()
    {
        return $this->hasMany(Payment::className(), ['invoice_tariff_id' => 'id']);
    }
}
