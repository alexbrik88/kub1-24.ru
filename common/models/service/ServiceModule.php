<?php

namespace common\models\service;

use common\models\company\CompanyType;
use Yii;

/**
 * This is the model class for table "service_module".
 *
 * @property int $id
 * @property string $name
 *
 * @property ServiceModuleVisit[] $serviceModuleVisits
 */
class ServiceModule extends \yii\db\ActiveRecord
{
    const STANDART = 1;
    const LOGISTICS = 2;
    const B2B_PAYMENT = 3;
    const IP_USN_6 = 4;
    const ANALYTICS = 5;

    const SERVICE_INVOICES = 'invoices';
    const SERVICE_PRICE_LIST = 'price-lists';
    const SERVICE_B2B_PAYMENT = 'b2b';
    const SERVICE_IP_USN_6 = 'bookkeeping';
    const SERVICE_ANALYTICS = 'analysis';


    public static $productArray = [
        self::STANDART,
        self::IP_USN_6,
        self::B2B_PAYMENT,
        self::ANALYTICS,
    ];

    public static $serviceModuleArray = [
        1 => self::SERVICE_INVOICES,
        2 => self::SERVICE_PRICE_LIST,
        3 => self::SERVICE_B2B_PAYMENT,
        4 => self::SERVICE_IP_USN_6,
        5 => self::SERVICE_ANALYTICS,
    ];

    public static $productLabel = [
        self::STANDART => 'Выставление счетов',
        self::IP_USN_6 => 'Бухгалтерия ИП',
        self::B2B_PAYMENT => 'Бизнес платежи',
        self::ANALYTICS => 'ФинДиректор',
    ];

    public static $serviceModuleLabel = [
        self::SERVICE_INVOICES => 'Выставление счетов',
        self::SERVICE_PRICE_LIST => 'Прайс-листы',
        self::SERVICE_B2B_PAYMENT => 'Бизнес платежи',
        self::SERVICE_IP_USN_6 => 'Бухгалтерия ИП',
        self::SERVICE_ANALYTICS => 'ФинДиректор',
    ];

    public static $productRouteMap = [
        self::STANDART => ['/documents/invoice/index', 'type' => 2],
        self::IP_USN_6 => ['/tax/robot/index'],
        self::B2B_PAYMENT => ['/b2b/module'],
        self::ANALYTICS => ['/analytics/options/start'],
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'service_module';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getServiceModuleVisits()
    {
        return $this->hasMany(ServiceModuleVisit::className(), ['module_id' => 'id']);
    }

    /**
     * @param $company
     * @return bool|int
     */
    public static function getCurrentModuleId($company)
    {
        if (!$company || Yii::$app->request->isAjax || Yii::$app->request->isPjax || Yii::$app->request->isPost)
            return false;

        $isIp = $company->company_type_id == CompanyType::TYPE_IP;
        $module = Yii::$app->controller->module->id;
        $controller = Yii::$app->controller->id;
        $current = false;

        switch ($module) {
            case "analytics":
                $current = self::ANALYTICS;
                break;
            case "app-frontend":
                if ($controller == "b2b")
                    $current = self::B2B_PAYMENT;
                break;
            case "tax":
            case "notification":
                if ($isIp && in_array($controller, ["robot", "declaration"]))
                    $current = self::IP_USN_6;
                break;
            case "documents":
            case "contractor":
            case "cash":
            case "product":
            case "reports":
            case "export":
            default:
                $current = self::STANDART;
                break;
        }

        return $current;
    }

    /**
     * @param $company
     * @return bool|int
     */
    public static function indexRoute($moduleId)
    {
        switch ($moduleId) {
            case self::ANALYTICS:
                $route = \frontend\components\BusinessAnalyticsAccess::indexRoute();
                break;

            case self::IP_USN_6:
                $route = ['/tax/robot/index'];
                break;

            case self::B2B_PAYMENT:
                $route = ['/b2b/module'];
                break;

            case self::STANDART:
            default:
                $route = ['/documents/invoice/index', 'type' => 2];
                break;
        }

        return $route;
    }
}
