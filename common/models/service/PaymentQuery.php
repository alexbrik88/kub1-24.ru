<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 1/5/16
 * Time: 4:02 PM
 * Email: t.kanstantsin@gmail.com
 */

namespace common\models\service;

use common\models\ICompanyStrictQuery;
use yii\db\ActiveQuery;

/**
 * Class PaymentQuery
 * @package common\models\service
 */
class PaymentQuery extends ActiveQuery implements ICompanyStrictQuery
{

    /**
     * @param int $companyId
     *
     * @return $this
     */
    public function byCompany($companyId)
    {
        return $this
            ->joinWith('subscribe')
            ->andWhere([
                Payment::tableName() . '.company_id' => $companyId,
            ]);
    }

    /**
     * @param $promoCodeId
     * @return $this
     */
    public function byPromoCode($promoCodeId)
    {
        return $this->andWhere([
            Payment::tableName() . '.type_id' => PaymentType::TYPE_PROMO_CODE,
            Payment::tableName() . '.promo_code_id' => $promoCodeId,
        ]);
    }

}
