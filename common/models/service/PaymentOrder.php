<?php

namespace common\models\service;

use common\models\Company;
use common\models\Discount;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "service_payment_order".
 *
 * @property integer $id
 * @property integer $payment_id
 * @property integer $company_id
 * @property integer $tariff_id
 * @property integer $discount_id
 * @property integer $price
 * @property integer $discount
 * @property integer $sum
 * @property integer $store_tariff_id
 * @property integer $out_invoice_tariff_id
 * @property integer $invoice_tariff_id
 *
 * @property ServiceSubscribeTariff $tariff
 * @property StoreTariff $storeTariff
 * @proeprty StoreOutInvoiceTariff $outInvoiceTariff
 * @property Company $company
 * @property Discount $discountModel
 * @property ServicePayment $payment
 * @property ServiceSubscribe[] $serviceSubscribes
 * @property ServiceInvoiceTariff $invoiceTariff
 */
class PaymentOrder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service_payment_order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['price', 'sum'], 'required'],
            [['tariff_id', 'price', 'sum', 'store_tariff_id'], 'integer'],
            [['tariff_id'], 'exist', 'skipOnError' => true, 'targetClass' => SubscribeTariff::className(), 'targetAttribute' => ['tariff_id' => 'id']],
            [['store_tariff_id'], 'exist', 'skipOnError' => true, 'targetClass' => StoreTariff::className(), 'targetAttribute' => ['store_tariff_id' => 'id']],
            [['discount'], 'number', 'numberPattern' => '/^\d+(\.\d*)?$/', 'min' => 0, 'max' => 99.999999],
            [['discount'], 'filter', 'filter' => function ($value) {
                return round($value, 6);
            },],
            //[['payment_id', 'company_id'], 'unique', 'targetAttribute' => ['payment_id', 'company_id'], 'message' => 'The combination of Payment ID and Company ID has already been taken.'],
            //[['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            //[['payment_id'], 'exist', 'skipOnError' => true, 'targetClass' => Payment::className(), 'targetAttribute' => ['payment_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'payment_id' => 'Payment ID',
            'company_id' => 'Company ID',
            'tariff_id' => 'Tariff ID',
            'price' => 'Price',
            'discount' => 'Discount',
            'sum' => 'Sum',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(SubscribeTariff::className(), ['id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoreTariff()
    {
        return $this->hasOne(StoreTariff::className(), ['id' => 'store_tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutInvoiceTariff()
    {
        return $this->hasOne(StoreOutInvoiceTariff::className(), ['id' => 'out_invoice_tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceTariff()
    {
        return $this->hasOne(ServiceInvoiceTariff::className(), ['id' => 'invoice_tariff']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscountModel()
    {
        return $this->hasOne(Discount::className(), ['id' => 'discount_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayment()
    {
        return $this->hasOne(Payment::className(), ['id' => 'payment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscribe()
    {
        return $this->hasOne(Subscribe::className(), ['payment_order_id' => 'id']);
    }

    /**
     *
     */
    public function createSubscribe()
    {
        $tariffGroup = $this->tariff->tariffGroup;
        $subscribe = $this->subscribe ? : new Subscribe([
            'company_id' => $this->company_id,
            'payment_id' => $this->payment->id,
            'payment_order_id' => $this->id,
        ]);
        $subscribe->tariff_group_id = $tariffGroup->id;
        $subscribe->tariff_limit = $this->tariff->tariff_limit;
        $subscribe->tariff_id = $this->tariff_id;
        $subscribe->discount = $this->discount;
        $subscribe->duration_month = $this->tariff->duration_month;
        $subscribe->duration_day = $this->tariff->duration_day;
        $subscribe->payment_type_id = $this->payment->type_id;
        $subscribe->status_id = $this->payment->is_confirmed ? SubscribeStatus::STATUS_PAYED : SubscribeStatus::STATUS_NEW;

        $subscribeArray = [$subscribe];
        if ($this->tariff->containsTariffs) {
            foreach ($this->tariff->containsTariffs as $containsTariff) {
                $subscribeArray[] = new Subscribe([
                    'company_id' => $this->company_id,
                    'payment_id' => $this->payment->id,
                    'payment_order_id' => $this->id,
                    'tariff_group_id' => $containsTariff->tariff_group_id,
                    'tariff_limit' => $containsTariff->tariff_limit,
                    'tariff_id' => $containsTariff->id,
                    'duration_month' => $containsTariff->duration_month,
                    'duration_day' => $containsTariff->duration_day,
                    'payment_type_id' => $this->payment->type_id,
                    'status_id' => $this->payment->is_confirmed ?
                                   SubscribeStatus::STATUS_PAYED :
                                   SubscribeStatus::STATUS_NEW,
                ]);
            }
        }

        foreach ($subscribeArray as $item) {
            if (!$item->save()) {
                \common\components\helpers\ModelHelper::logErrors($item, __METHOD__);

                return false;
            }
        }

        if ($forTariff = $this->tariff->payExtraForTariff) {
            $forSubscribe = Subscribe::find()->andWhere([
                'tariff_id' => $forTariff->id,
            ])->active()->byCompany($this->company_id)->one();

            if ($forSubscribe !== null) {
                $forSubscribe->updateAttributes(['expired_at' => time()-1]);
            }
        }
        $subscribe->createReportState();
        $this->populateRelation('subscribe', $subscribe);

        foreach ($subscribeArray as $item) {
            $this->company->getHasActualSubscription($item->tariff_group_id, true);
        }

        return true;
    }

    public function associateWithCompany(Company $company, $checkActive = true)
    {
        if ($this->company_id === null &&
            (!$checkActive || $this->tariff->tariffGroup->activeByCompany($company)) &&
            $this->updateAttributes(['company_id' => $company->id])
        ) {
            $this->populateRelation('company', $company);
            if ($this->payment->is_confirmed) {
                $this->createSubscribe();
            }

            return true;
        }

        return false;
    }
}
