<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 11/23/15
 * Time: 11:51 AM
 * Email: t.kanstantsin@gmail.com
 */

namespace common\models\service;


use common\models\ICompanyStrictQuery;
use yii\db\ActiveQuery;
use yii\db\Expression;

/**
 * Class SubscribeQuery
 * @package common\models\service
 */
class SubscribeQuery extends ActiveQuery implements ICompanyStrictQuery
{

    /**
     * @param int $companyId
     *
     * @return $this
     */
    public function byCompany($companyId)
    {
        return $this->andWhere([
            'company_id' => $companyId,
        ]);
    }

    /**
     * @param array $statusArray
     * @return $this
     */
    public function byStatus(array $statusArray)
    {
        return $this->andWhere([
            'status_id' => $statusArray,
        ]);
    }

    /**
     * @param int|array $value
     * @return $this
     */
    public function byGroup($value)
    {
        return $this->andWhere([
            'tariff_group_id' => $value,
        ]);
    }

    /**
     * @param bool $notExpired
     * @return $this
     */
    public function notExpired($notExpired = true)
    {
        return $this->andWhere(['OR',
            [($notExpired ? '>' : '<'), 'expired_at', new Expression('UNIX_TIMESTAMP()'),],
            ['IS', 'expired_at', null],
        ]);
    }

    public function createdToday()
    {
        return $this->andWhere([
            '=',
            new Expression('FROM_UNIXTIME(created_at, \'%Y-%m-%d\')'),
            new Expression('FROM_UNIXTIME(UNIX_TIMESTAMP(), \'%Y-%m-%d\')'),
        ]);
    }

    /**
     * @param int $year
     * @param int $month
     * @return $this
     */
    public function byYearMonth($year, $month)
    {
        return $this->andWhere(['AND',
            ['=', 'YEAR(FROM_UNIXTIME(updated_at))', $year],
            ['=', 'MONTH(FROM_UNIXTIME(updated_at))', $month],
        ]);
    }

    /**
     * @return $this
     */
    public function active()
    {
        return $this->andWhere([
            'and',
            ['status_id' => SubscribeStatus::STATUS_ACTIVATED],
            ['>', 'expired_at', new Expression('UNIX_TIMESTAMP()')],
        ]);
    }
}
