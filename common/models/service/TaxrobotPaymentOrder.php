<?php

namespace common\models\service;

use common\models\Company;
use Yii;

/**
 * This is the model class for table "taxrobot_payment_order".
 *
 * @property integer $id
 * @property integer $payment_id
 * @property integer $company_id
 * @property integer $year
 * @property integer $quarter
 * @property integer $price
 * @property number $discount
 * @property integer $sum
 *
 * @property Company $company
 * @property ServicePayment $payment
 */
class TaxrobotPaymentOrder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'taxrobot_payment_order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['payment_id', 'company_id', 'year', 'quarter'], 'required'],
            //[['payment_id', 'company_id', 'year', 'quarter'], 'integer'],
            //[['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            //[['payment_id'], 'exist', 'skipOnError' => true, 'targetClass' => Payment::className(), 'targetAttribute' => ['payment_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'payment_id' => 'Payment ID',
            'company_id' => 'Company ID',
            'year' => 'Year',
            'quarter' => 'Quarter',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayment()
    {
        return $this->hasOne(Payment::className(), ['id' => 'payment_id']);
    }

    /**
     * @return string
     */
    public function getPeriodUrl()
    {
        return "{$this->year}_{$this->quarter}";
    }
}
