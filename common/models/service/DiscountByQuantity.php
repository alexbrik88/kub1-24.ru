<?php

namespace common\models\service;

use Yii;

/**
 * This is the model class for table "service_discount_by_quantity".
 *
 * @property integer $id
 * @property integer $tariff_id
 * @property integer $quantity
 * @property string $percent
 *
 * @property ServiceSubscribeTariff $tariff
 */
class DiscountByQuantity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service_discount_by_quantity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['tariff_id', 'quantity', 'percent'], 'required'],
            //[['tariff_id', 'quantity'], 'integer'],
            //[['percent'], 'number'],
            //[['tariff_id'], 'exist', 'skipOnError' => true, 'targetClass' => ServiceSubscribeTariff::className(), 'targetAttribute' => ['tariff_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tariff_id' => 'Tariff ID',
            'quantity' => 'Quantity',
            'percent' => 'Percent',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(SubscribeTariff::className(), ['id' => 'tariff_id']);
    }
}
