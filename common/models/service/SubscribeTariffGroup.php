<?php

namespace common\models\service;

use common\models\Company;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "service_subscribe_tariff_group".
 *
 * @property integer $id
 * @property string $name
 * @property integer $is_base
 * @property integer $has_base
 * @property integer $is_active
 * @property string $description
 * @property integer $priority
 * @property string $icon
 *
 * @property SubscribeTariff[] $allTariffs
 * @property SubscribeTariff[] $paidTariffs
 * @property SubscribeTariff[] $actualTariffs
 */
class SubscribeTariffGroup extends \yii\db\ActiveRecord
{
    const STANDART = 1;
    const LOGISTICS = 2;
    const B2B_PAYMENT = 3;
    const IP_USN_6 = 4;
    const TAX_DECLAR_IP_USN_6 = 4;
    const TAX_IP_USN_6 = 7;
    const ANALYTICS = 5;
    const OOO_OSNO_NULL_REPORTING = 6;
    const CHECK_CONTRACTOR = 8;
    const DOCUMENT_RECOGNITION = 9;
    const PRICE_LIST = 10;
    const BI_FINANCE = 11;
    const BI_FINANCE_PLUS = 12;
    const BI_MARKETING = 13;
    const BI_MARKETING_PLUS = 14;
    const BI_SALES = 15;
    const BI_PRODUCTS = 16;
    const BI_ALL_INCLUSIVE = 17;
    const BI_FULL_CONSTRUCTION = 18;
    const FOREIGN_CURRENCY_INVOICE = 19;

    /**
     * @inheritdoc
     */
    public static $hasLimit = [
        self::STANDART,
        self::CHECK_CONTRACTOR,
        self::DOCUMENT_RECOGNITION,
        self::PRICE_LIST,
        self::FOREIGN_CURRENCY_INVOICE,
    ];

    /**
     * @inheritdoc
     */
    public static $analyticsItems = [
        self::BI_FINANCE,
        self::BI_FINANCE_PLUS,
        self::BI_MARKETING,
        self::BI_MARKETING_PLUS,
        self::BI_SALES,
        self::BI_PRODUCTS,
        self::BI_ALL_INCLUSIVE,
        self::BI_FULL_CONSTRUCTION,
    ];

    /**
     * @inheritdoc
     */
    public static $limitItemLabel = [
        self::STANDART => [
            'one' => 'счет',
            'of' => 'счетов',
            'many' => 'счета',
        ],
        self::FOREIGN_CURRENCY_INVOICE => [
            'one' => 'инвойс',
            'of' => 'инвойсов',
            'many' => 'инвойсы',
        ],
        self::CHECK_CONTRACTOR => [
            'one' => 'проверка',
            'of' => 'проверок',
            'many' => 'проверки',
        ],
        self::DOCUMENT_RECOGNITION => [
            'one' => 'страница',
            'of' => 'страниц',
            'many' => 'страницы',
        ],
        self::PRICE_LIST => [
            'one' => 'прайс-лист',
            'of' => 'прайс-листов',
            'many' => 'прайс-листы',
        ],
    ];

    /**
     * @inheritdoc
     */
    public static $limitDescription = [
        self::STANDART => 'Кол-во выставленных счетов',
        self::FOREIGN_CURRENCY_INVOICE => 'Кол-во выставленных инвойсов',
        self::CHECK_CONTRACTOR => 'Кол-во проверок',
        self::DOCUMENT_RECOGNITION => 'Кол-во страниц',
        self::PRICE_LIST => 'Кол-во прайс-листов',
        self::BI_FINANCE => 'Кол-во сотрудников, имеющих доступ',
        self::BI_FINANCE_PLUS => 'Кол-во сотрудников, имеющих доступ',
        self::BI_MARKETING => 'Кол-во сотрудников, имеющих доступ',
        self::BI_MARKETING_PLUS => 'Кол-во сотрудников, имеющих доступ',
        self::BI_SALES => 'Кол-во сотрудников, имеющих доступ',
        self::BI_PRODUCTS => 'Кол-во сотрудников, имеющих доступ',
        self::BI_ALL_INCLUSIVE => 'Кол-во сотрудников, имеющих доступ',
        self::BI_FULL_CONSTRUCTION => 'Кол-во сотрудников, имеющих доступ',
    ];

    /**
     * @inheritdoc
     */
    public static $limitPerMonth = [
        self::STANDART,
        self::FOREIGN_CURRENCY_INVOICE,
        self::PRICE_LIST,
    ];

    /**
     * @inheritdoc
     */
    public static $payPerQuantity = [
        self::CHECK_CONTRACTOR,
        self::DOCUMENT_RECOGNITION,
    ];

    private static $_allIds;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service_subscribe_tariff_group';
    }

    /**
     * @return SubscribeTariffGroupQuery
     */
    public static function find()
    {
        return new SubscribeTariffGroupQuery(get_called_class());
    }

    /**
     * @return SubscribeTariffGroupQuery
     */
    public static function allIds()
    {
        if (!isset(self::$_allIds)) {
            self::$_allIds = self::find()->select('id')->column();
        }

        return self::$_allIds;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_active'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'is_base' => 'Is Base',
            'has_base' => 'Has Base',
            'is_active' => 'Is Active',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAllTariffs()
    {
        return $this->hasMany(SubscribeTariff::className(), ['tariff_group_id' => 'id'])->onCondition(['is_active' => true]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaidTariffs()
    {
        return $this->getAllTariffs()->andOnCondition(['>', 'price', 0]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActualTariffs()
    {
        return $this->getPaidTariffs()->andOnCondition(['is_pay_extra' => false]);
    }

    /**
     * @return boolean
     */
    public function activeByCompany(Company $company)
    {
        if (!$this->is_active) {
            return false;
        }

        switch ($this->id) {
            case self::TAX_DECLAR_IP_USN_6:
            case self::TAX_IP_USN_6:
                return $company->getIsLikeIP() &&
                       $company->companyTaxationType->usn;
                break;

            case self::OOO_OSNO_NULL_REPORTING:
                return $company->company_type_id == \common\models\company\CompanyType::TYPE_OOO &&
                       $company->companyTaxationType->osno;
                break;
        }

        return true;
    }

    /**
     * @return array
     */
    public static function getTariffsByGroups()
    {
        return [
            SubscribeTariffGroup::STANDART => SubscribeTariff::find()->where(['tariff_group_id' => SubscribeTariffGroup::STANDART])->andWhere(['!=', 'id', SubscribeTariff::TARIFF_TRIAL])->select('id')->column(),
            SubscribeTariffGroup::IP_USN_6 => SubscribeTariff::find()->where(['tariff_group_id' => SubscribeTariffGroup::IP_USN_6])->select('id')->column(),
            SubscribeTariffGroup::B2B_PAYMENT => SubscribeTariff::find()->where(['tariff_group_id' => SubscribeTariffGroup::B2B_PAYMENT])->select('id')->column(),
            SubscribeTariffGroup::ANALYTICS => SubscribeTariff::find()->where(['tariff_group_id' => SubscribeTariffGroup::ANALYTICS])->select('id')->column(),
        ];
    }

    /**
     * @param integer $groupId
     * @param string $limit
     * @return string
     */
    public static function getlimitLabel($groupId, $limit, $short = false)
    {
        if ($item = ArrayHelper::getValue(self::$limitItemLabel, [$groupId, 'of'])) {
            if ($groupId == self::STANDART) {
                return $limit ? "До {$limit} {$item}".($short ? '' : ' в месяц') : 'Безлимит';
            } else {
                return $limit ? "{$limit} {$item}" : 'Безлимит';
            }
        }

        return '';
    }

    /**
     * @param string $limit
     * @return string
     */
    public function limitLabel($limit, $short = false)
    {
        return self::getlimitLabel($this->id, $limit, $short);
    }

    /**
     * @return string
     */
    public function getIsPayPerQuantity()
    {
        return in_array($this->id, self::$payPerQuantity);
    }

    /**
     * @return string
     */
    public function perOneLabel()
    {
        if ($this->getIsPayPerQuantity()) {
            return self::$limitItemLabel[$this->id]['one'];
        }

        return 'месяц';
    }
}
