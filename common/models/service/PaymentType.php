<?php

namespace common\models\service;

use Yii;

/**
 * This is the model class for table "service_payment_type".
 *
 * @property integer $id
 * @property string $name
 *
 * @property ServicePayment[] $servicePayments
 */
class PaymentType extends \yii\db\ActiveRecord
{
    const TYPE_ONLINE = 1;
    const TYPE_RECEIPT = 2;
    const TYPE_INVOICE = 3;
    const TYPE_PROMO_CODE = 4;

    const TYPE_REWARD = 5;

    protected static $_items;

    /**
     * @return mixed
     */
    public static function getValue($id, $paramName)
    {
        if (!isset(self::$_items)) {
            self::$_items = self::find()->asArray()->indexBy('id')->all();
        }

        return self::$_items[$id][$paramName] ?? null;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service_payment_type';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
        ];
    }
}
