<?php

namespace common\models\service;

use Yii;

/**
 * This is the model class for table "service_subscribe_contains_tariff".
 *
 * @property int $id
 * @property int $tariff_id
 * @property int $contains_tariff_id
 *
 * @property ServiceSubscribeTariff $tariff
 * @property ServiceSubscribeTariff $containsTariff
 */
class SubscribeContainsTariff extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'service_subscribe_contains_tariff';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tariff_id' => 'Tariff ID',
            'contains_tariff_id' => 'Contains Tariff ID',
        ];
    }

    /**
     * Gets query for [[Tariff]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(ServiceSubscribeTariff::className(), ['id' => 'tariff_id']);
    }

    /**
     * Gets query for [[ContainsTariff]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getContainsTariff()
    {
        return $this->hasOne(ServiceSubscribeTariff::className(), ['id' => 'contains_tariff_id']);
    }
}
