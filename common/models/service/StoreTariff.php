<?php

namespace common\models\service;

use Yii;

/**
 * This is the model class for table "service_store_tariff".
 *
 * @property integer $id
 * @property integer $cabinets_count
 * @property integer $cost_one_month_for_one_cabinet
 * @property integer $total_amount
 */
class StoreTariff extends \yii\db\ActiveRecord
{
    const TARIFF_2_CABINETS = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service_store_tariff';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cabinets_count', 'cost_one_month_for_one_cabinet', 'total_amount'], 'required'],
            [['cabinets_count', 'cost_one_month_for_one_cabinet', 'total_amount'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cabinets_count' => 'Кол-во кабинетов',
            'cost_one_month_for_one_cabinet' => 'Стоимость за 1 кабинет в месяц',
            'total_amount' => 'Оплата за 12 месяцев',
        ];
    }
}
