<?php

namespace common\models\service;

use common\components\date\DateHelper;
use common\models\cash\CashFactory;
use common\models\Company;
use common\models\company\Experience;
use common\models\Contractor;
use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;
use common\models\employee\Employee;
use common\models\out\OutInvoice;
use common\models\product\Product;
use common\models\report\ReportState;
use common\models\selling\SellingSubscribe;
use common\models\service\Subscribe;
use common\models\service\SubscribeStatus;
use common\models\store\StoreCompanyContractor;
use frontend\models\Documents;
use frontend\modules\documents\forms\InvoiceFlowForm;
use Yii;
use yii\base\Exception;
use yii\behaviors\TimestampBehavior;
use yii\db\Connection;

/**
 * This is the model class for table "service_payment".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $payment_date
 * @ptoperty integer $company_id
 * @property string $invoice_number
 * @property string $sum
 * @property integer $type_id
 * @property integer $promo_code_id
 * @property bool $by_novice
 * @property bool $by_novice_in_module
 * @property bool $is_confirmed
 * @property integer $serial_number
 * @property integer $store_tariff_id
 * @property integer $out_invoice_tariff_id
 * @property integer $invoice_tariff_id
 *
 * @property PromoCode $promoCode
 * @property PaymentType $type
 * @property Subscribe $subscribe
 * @property Company[] $orderCompanies
 * @property Company $company
 * @property Invoice $inInvoice
 * @property Invoice $outInvoice
 * @property StoreTariff $storeTariff
 * @property StoreOutInvoiceTariff $outInvoiceTariff
 * @property ServiceInvoiceTariff $invoiceTariff
 */
class Payment extends \yii\db\ActiveRecord
{
    const FOR_SUBSCRIBE = 'subscribe';
    const FOR_TAXROBOT = 'taxrobot';
    const FOR_STORE_CABINET = 'store_cabinet';
    const FOR_OUT_INVOICE = 'out_invoice';
    const FOR_ODDS = 'odds';
    const FOR_ADD_INVOICE = 'add_invoice';

    /**
     *
     */
    const ONLINE_PAYMENT = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service_payment';
    }

    /**
     * @return PaymentQuery
     */
    public static function find()
    {
        return new PaymentQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // rules
            [['type_id'], 'required'],
            [['serial_number', 'sum'], 'integer'],
            [['invoice_number'], 'string', 'max' => 45],
            [['is_confirmed'], 'boolean', 'when' => function (Payment $model) {
                return $model->type_id == PaymentType::TYPE_ONLINE;
            },],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
            'payment_date' => 'Дата оплаты',
            'invoice_number' => 'Номер счёта',
            'sum' => 'Сумма',
            'type_id' => 'Тип платежа',
            'promo_code_id' => 'Промо код',
            'by_novice' => 'Оплачено новым пользователем',
            'by_novice_in_module' => 'Модуль оплачен первый раз',
            'is_confirmed' => 'Подтверждён',
        ];
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if ($insert) {
            $this->by_novice = (int)($this->company->experience_id == Experience::EXP_NOVICE);
            $this->by_novice_in_module = (int)($this->isByNoviceInModule());
            $this->is_confirmed = false;
            if ($this->type_id == PaymentType::TYPE_PROMO_CODE) {
                $this->is_confirmed = true;
                $this->company->resetShowReminder();
            } else {
                $this->is_confirmed = false;
            }
        }
        if ($this->isAttributeChanged('is_confirmed') && $this->is_confirmed == true) {
            $this->company->resetShowReminder();
        }

        return true;
    }

    /**
     * @return bool
     */
    public function isByNoviceInModule()
    {
        $tariffs = SubscribeTariff::getTariffsByTariff($this->tariff_id);

        if ($tariffs) {
            $hasPayments = Payment::find()
                ->where(['company_id' => $this->company_id])
                ->andWhere(['tariff_id' => $tariffs])
                ->andWhere(['not', ['type_id' => PaymentType::TYPE_PROMO_CODE]])
                ->andWhere(['is_confirmed' => 1])
                ->exists();

            return !($hasPayments);
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($this->is_confirmed && $this->company->experience_id == Experience::EXP_NOVICE && $this->type_id != PaymentType::TYPE_PROMO_CODE) {
            $this->company->updateAttributes([
                'experience_id' => Experience::EXP_CURRENT,
            ]);
        }

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPromoCode()
    {
        return $this->hasOne(PromoCode::className(), ['id' => 'promo_code_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(SubscribeTariff::className(), ['id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(PaymentType::className(), ['id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscribe()
    {
        return $this->hasOne(Subscribe::className(), ['payment_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscribes()
    {
        return $this->hasMany(Subscribe::className(), ['payment_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(PaymentOrder::className(), ['payment_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(PaymentOrder::className(), ['payment_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxrobotOrder()
    {
        return $this->hasOne(TaxrobotPaymentOrder::className(), ['payment_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxrobotOrders()
    {
        return $this->hasMany(TaxrobotPaymentOrder::className(), ['payment_id' => 'id']);
    }

    /**
     * @return [\common\models\document\Invoice]
     */
    public function getOrderCompanies()
    {
        return $this->hasMany(Company::className(), ['id' => 'company_id'])->via('orders');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInInvoice()
    {
        return $this->hasOne(Invoice::className(), ['service_payment_id' => 'id'])
            ->andOnCondition(['type' => Documents::IO_TYPE_IN, 'is_subscribe_invoice' => true, 'is_deleted' => false]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoreTariff()
    {
        return $this->hasOne(StoreTariff::className(), ['id' => 'store_tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutInvoiceTariff()
    {
        return $this->hasOne(StoreOutInvoiceTariff::className(), ['id' => 'out_invoice_tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceTariff()
    {
        return $this->hasOne(ServiceInvoiceTariff::className(), ['id' => 'invoice_tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutInvoice()
    {
        return $this->hasOne(Invoice::className(), ['service_payment_id' => 'id'])
            ->andOnCondition(['type' => Documents::IO_TYPE_OUT, 'is_subscribe_invoice' => true, 'is_deleted' => false]);
    }

    public function getInvoiceDocumentAuthor()
    {
        if ($this->outInvoice) {
            return $this->outInvoice->documentAuthor;
        }

        return null;
    }

    /**
     * @return string
     */
    public function getPaymentName()
    {
        $name = PaymentType::getValue($this->type_id, 'name');

        return $name . (
            $this->type_id == PaymentType::TYPE_PROMO_CODE && $this->promoCode
            ? ' (' . $this->promoCode->code . ')'
            : ''
        );
    }

    /**
     * @return string
     */
    public function getAllFlows()
    {
        return ($this->outInvoice && $this->outInvoice->allFlows) ? $this->outInvoice->allFlows : [];
    }

    /**
     * @param null $paymentDate
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     */
    public function paid($paymentDate = null)
    {
        $this->is_confirmed = 1;
        $this->payment_date = $paymentDate ? $paymentDate : time();

        if ($this->payment_for == self::FOR_TAXROBOT || $this->payment_for == self::FOR_STORE_CABINET ||
            $this->payment_for == self::FOR_OUT_INVOICE || $this->payment_for == self::FOR_ODDS ||
            $this->payment_for == self::FOR_ADD_INVOICE
        ) {
            $flowForm = ($this->outInvoice && ($amount = $this->outInvoice->AvailablePaymentAmount)) ? new InvoiceFlowForm([
                'invoice' => $this->outInvoice,
                'amount' => (string)min($this->sum, $amount / 100),
                'date_pay' => (string)date(DateHelper::FORMAT_USER_DATE),
                'flowType' => (string)CashFactory::$paymentToFlowType[$this->type_id],
                'isAccounting' => '1',
                'createNew' => '1',
            ]) : null;

            if ($this->type_id !== PaymentType::TYPE_PROMO_CODE) {
                $this->serial_number = Payment::find()->byCompany($this->company_id)->max('serial_number') + 1;
            }
            if ((!$flowForm || $flowForm->save())) {
                if ($this->inInvoice && !$this->inInvoice->act) {
                    $this->inInvoice->createAct();
                }
                if ($this->outInvoice && !$this->outInvoice->act) {
                    $this->outInvoice->createAct();
                }

                return $this->save(false, ['is_confirmed', 'payment_date', 'serial_number']);
            }

            return false;
        }

        $flowForm = ($this->outInvoice && ($amount = $this->outInvoice->AvailablePaymentAmount)) ? new InvoiceFlowForm([
            'invoice' => $this->outInvoice,
            'amount' => (string)min($this->sum, $amount / 100),
            'date_pay' => (string)date(DateHelper::FORMAT_USER_DATE),
            'flowType' => (string)CashFactory::$paymentToFlowType[$this->type_id],
            'isAccounting' => '1',
            'createNew' => '1',
        ]) : null;

        $payment = $this;

        return \Yii::$app->db->transaction(function (Connection $db) use ($payment, $flowForm) {
            $company = $this->company;
            if ($this->save() && (!$flowForm || $flowForm->save())) {
                if (in_array($this->tariff_id, SubscribeTariff::paidStandartIds())) {
                    $this->updateAttributes([
                        'serial_number' => Payment::find()->byCompany($company->id)->max('serial_number') + 1,
                    ]);
                    \common\models\company\CompanyFirstEvent::checkEvent($company, 83);
                }
                foreach ($this->orders as $paymentOrder) {
                    if (!$paymentOrder->company_id) {
                        continue;
                    }
                    if (!$paymentOrder->createSubscribe()) {
                        $db->transaction->rollBack();

                        return false;
                    }
                    $subscribe = $paymentOrder->subscribe;
                    $sellingSubscribe = new SellingSubscribe();
                    $sellingSubscribe->company_id = $company->id;
                    $sellingSubscribe->subscribe_id = $subscribe->id;
                    switch ($subscribe->tariff_id) {
                        case SubscribeTariff::TARIFF_1:
                            $sellingSubscribe->type = SellingSubscribe::TYPE_TARIFF_1;
                            $sellingSubscribe->main_type = SellingSubscribe::TYPE_MAIN_PAY;
                            break;
                        case SubscribeTariff::TARIFF_2:
                            $sellingSubscribe->type = SellingSubscribe::TYPE_TARIFF_2;
                            $sellingSubscribe->main_type = SellingSubscribe::TYPE_MAIN_PAY;
                            break;
                        case SubscribeTariff::TARIFF_3:
                            $sellingSubscribe->type = SellingSubscribe::TYPE_TARIFF_3;
                            $sellingSubscribe->main_type = SellingSubscribe::TYPE_MAIN_PAY;
                            break;
                        case SubscribeTariff::TARIFF_TRIAL:
                            $sellingSubscribe->type = SellingSubscribe::TYPE_TRIAL;
                            $sellingSubscribe->main_type = SellingSubscribe::TYPE_MAIN_FREE;
                            break;
                        case null:
                            if ($this->promoCode) {
                                if ($this->promoCode->code == '77777') {
                                    $sellingSubscribe->type = SellingSubscribe::TYPE_PROMO_CODE_77777;
                                } else {
                                    if ($this->promoCode->group_id == PromoCodeGroup::GROUP_INDIVIDUAL) {
                                        $sellingSubscribe->type = SellingSubscribe::TYPE_PROMO_CODE_INDIVIDUAL;
                                    } else {
                                        $sellingSubscribe->type = SellingSubscribe::TYPE_PROMO_CODE_GROUPED;
                                    }
                                }
                                $sellingSubscribe->main_type = SellingSubscribe::TYPE_MAIN_FREE;
                            }
                    }
                    $sellingSubscribe->invoice_number = $this->outInvoice ? $this->outInvoice->getFullNumber() : '';
                    $sellingSubscribe->creation_date = $subscribe->created_at;
                    $sellingSubscribe->activation_date = $subscribe->activated_at;
                    $sellingSubscribe->end_date = $subscribe->expired_at;
                    $sellingSubscribe->save();

                    if (in_array($subscribe->tariff_group_id, [
                        SubscribeTariffGroup::TAX_IP_USN_6,
                        SubscribeTariffGroup::TAX_DECLAR_IP_USN_6,
                    ])) {
                        \common\models\company\CompanyFirstEvent::checkEvent($company, 88); // Робот оплачен
                    }
                    if ($paymentOrder->discountModel !== null && $paymentOrder->discountModel->is_one_time) {
                        $paymentOrder->discountModel->updateAttributes(['is_active' => false]);
                    }
                }

                if ($this->outInvoice && !$this->outInvoice->act) {
                    $this->outInvoice->createAct();
                }

                if ($this->inInvoice && !$this->inInvoice->act) {
                    $this->inInvoice->createAct();
                }

                return true;
            }
            $db->transaction->rollBack();

            return false;
        });
    }

    /**
     * @return boolean
     */
    public function unpaid()
    {
        $this->is_confirmed = 0;
        $this->payment_date = null;
        $this->serial_number = null;

        if ($this->payment_for == self::FOR_TAXROBOT || $this->payment_for == self::FOR_STORE_CABINET ||
            $this->payment_for == self::FOR_OUT_INVOICE || $this->payment_for == self::FOR_ODDS
        ) {
            if ($this->payment_for == self::FOR_STORE_CABINET) {
                $deactivateCabinetsCount = $this->storeTariff->cabinets_count;
                $this->company->deactivateCabinets($deactivateCabinetsCount);
            } elseif ($this->payment_for == self::FOR_OUT_INVOICE) {
                $deactivateLinksCount = $this->outInvoiceTariff->links_count;
                $this->company->deactivateLinks($deactivateLinksCount);
            }
            return $this->save(false, ['is_confirmed', 'payment_date', 'serial_number']);
        }

        $payment = $this;

        return \Yii::$app->db->transaction(function (Connection $db) use ($payment) {
            foreach ($payment->allFlows as $flow) {
                if (!$flow->delete()) {
                    $db->transaction->rollBack();

                    return false;
                }
            }
            foreach ($payment->subscribes as $subscribe) {
                if ($subscribe->reportState) {
                    $subscribe->reportState->delete();
                }
                if (!$subscribe->delete()) {
                    $db->transaction->rollBack();

                    return false;
                }
            }
            if ($payment->type_id == PaymentType::TYPE_PROMO_CODE && $payment->delete()) {
                return true;
            } elseif ($payment->save(false, ['is_confirmed', 'payment_date', 'serial_number'])) {
                return true;
            }
            $db->transaction->rollBack();

            return false;
        });
    }

    /**
     * @return boolean
     */
    public function deleteCompletely()
    {
        return \Yii::$app->db->transaction(function (Connection $db) {
            $delete = true;
            if ($this->is_confirmed) {
                $delete = $this->unpaid();
            }
            if ($delete && $this->outInvoice) {
                $delete = $this->outInvoice->deletingAll();
            }
            if ($delete && $this->orders) {
                foreach ($this->orders as $order) {
                    if ($delete = $order->delete()) {
                        continue;
                    } else {
                        break;
                    }
                }
            }

            if ($delete && $this->delete()) {
                return true;
            } else {
                $db->transaction->rollBack();

                return false;
            }
        });
    }

    /**
     *
     */
    public function createSubscribes()
    {
        return \Yii::$app->db->transaction(function (Connection $db) {
            $subscribeArray = [];
            foreach ($this->orders as $order) {
                if ($order->subscribe) {
                    $subscribeArray[] = $order->subscribe;
                } else {
                    $subscribe = new Subscribe([
                        'company_id' => $order->company_id,
                        'tariff_group_id' => $order->tariff->tariff_group_id,
                        'tariff_limit' => $order->tariff->tariff_limit,
                        'payment_id' => $this->id,
                        'payment_order_id' => $order->id,
                        'tariff_id' => $order->tariff_id,
                        'discount' => $order->discount,
                        'duration_month' => $order->tariff->duration_month,
                        'duration_day' => $order->tariff->duration_day,
                        'status_id' => $this->is_confirmed ? SubscribeStatus::STATUS_PAYED : SubscribeStatus::STATUS_NEW,
                        'payment_type_id' => $this->type_id,
                    ]);
                    if ($subscribe->save()) {
                        $subscribeArray[] = $subscribe;
                    } else {
                        $db->transaction->rollBack();

                        return false;
                    }
                }
            }
            $this->populateRelation('subscribes', $subscribeArray);

            return true;
        });
    }

    /**
     * @return int
     */
    public function getCabinetsCount()
    {
        if ($this->payment_for == self::FOR_STORE_CABINET) {
            if ($this->promoCode) {
                return $this->promoCode->cabinets_count;
            } else {
                return $this->storeTariff->cabinets_count;
            }
        }

        return 0;
    }

    /**
     * @return bool|null|string
     */
    public function getExpiredAtCabinet()
    {
        if ($this->payment_for == self::FOR_STORE_CABINET) {
            if ($this->promoCode) {
                return date(DateHelper::FORMAT_USER_DATE,
                    strtotime('+' . $this->promoCode->duration_month . ' month +' . $this->promoCode->duration_day . ' days', $this->payment_date));
            } else {
                $period = (int)$this->sum == (int)$this->storeTariff->total_amount ?
                    'year' : 'month';
                return date(DateHelper::FORMAT_USER_DATE, strtotime("+1 {$period}", $this->payment_date));
            }
        }

        return null;
    }

    /**
     *
     */
    public function createReportStateForAddInvoice()
    {
        if (!ReportState::find()->andWhere(['company_id' => $this->company_id])->andWhere(['>', 'invoice_tariff_id', 0])->exists()) {

            $reportState = new ReportState();
            $reportState->company_id = $this->company_id;
            $reportState->subscribe_id = NULL;
            $reportState->tariff_id = NULL;
            $reportState->invoice_tariff_id = $this->invoice_tariff_id;
            $reportState->pay_date = $this->payment_date;
            $reportState->days_without_payment = intval(ceil(($reportState->pay_date - $reportState->company->created_at) / (60 * 60 * 24)));
            $reportState->pay_sum = $this->sum;
            $reportState->invoice_count = $this->company->getOutInvoiceCount();
            $reportState->act_count = $this->company->getOutActCount();
            $reportState->packing_list_count = $this->company->getOutPackingListCount();
            $reportState->invoice_facture_count = $this->company->getOutInvoiceFactureCount();
            $reportState->upd_count = $this->company->getOutUpdCount();
            $reportState->has_logo = (bool) $this->company->logo_link;
            $reportState->has_print = (bool) $this->company->print_link;
            $reportState->has_signature = (bool) $this->company->chief_signature_link;
            $reportState->sum_company_images = (int) ($reportState->has_logo + $reportState->has_print + $reportState->has_signature);
            $reportState->employees_count = Employee::find()
                ->byCompany($reportState->company_id)
                ->byIsActive(Employee::ACTIVE)
                ->byIsDeleted(Employee::NOT_DELETED)
                ->byIsWorking(Employee::STATUS_IS_WORKING)
                ->andWhere(['not', ['email' => 'support@kub-24.ru']])
                ->andWhere(['<=', Employee::tableName() . '.created_at', $reportState->pay_date])
                ->count();
            $reportState->company_type_id = $reportState->company->company_type_id;
            $reportState->company_taxation_type_name = $reportState->company->companyTaxationType->name;
            $reportState->product_count = $this->company->getProducts()->andWhere(['production_type' => Product::PRODUCTION_TYPE_GOODS])->count();
            $reportState->service_count = $this->company->getProducts()->andWhere(['production_type' => Product::PRODUCTION_TYPE_SERVICE])->count();
            $reportState->download_statement_count = $this->company->getStateFromBankCount();
            $reportState->download_1c_count = $this->company->getOneCExportCount();
            $reportState->import_xls_product_count = $this->company->import_xls_product_count;
            $reportState->import_xls_service_count = $this->company->import_xls_service_count;

            $reportState->save();
        }
    }

}
