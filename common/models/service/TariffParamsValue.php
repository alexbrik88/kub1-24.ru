<?php

namespace common\models\service;

use Yii;

/**
 * This is the model class for table "service_tariff_params_value".
 *
 * @property int $id
 * @property int $tariff_id
 * @property int $params_id
 * @property string $value
 * @property string $text
 *
 * @property ServiceTariffParams $params
 * @property ServiceSubscribeTariff $tariff
 */
class TariffParamsValue extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'service_tariff_params_value';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'tariff_id' => 'Tariff ID',
            'params_id' => 'Params ID',
            'value' => 'Value',
            'text' => 'Text',
        ];
    }

    /**
     * Gets query for [[Params]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getParams()
    {
        return $this->hasOne(ServiceTariffParams::className(), ['id' => 'params_id']);
    }

    /**
     * Gets query for [[Tariff]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(ServiceSubscribeTariff::className(), ['id' => 'tariff_id']);
    }
}
