<?php

namespace common\models\service;

use Yii;

/**
 * This is the model class for table "service_subscribe_status".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Subscribe[] $serviceSubscribes
 */
class SubscribeStatus extends \yii\db\ActiveRecord
{
    const STATUS_NEW = 1;
    const STATUS_PAYED = 2;
    const STATUS_ACTIVATED = 3;

    public static $realSubscribes = [
        self::STATUS_PAYED,
        self::STATUS_ACTIVATED,
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service_subscribe_status';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
        ];
    }
}
