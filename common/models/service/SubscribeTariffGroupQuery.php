<?php

namespace common\models\service;


use yii\db\ActiveQuery;

/**
 * Class SubscribeTariffQuery
 * @package common\models\service
 */
class SubscribeTariffGroupQuery extends ActiveQuery
{
    /**
     * @return $this
     */
    public function active()
    {
        return $this->andWhere(['is_active' => true]);
    }
}
