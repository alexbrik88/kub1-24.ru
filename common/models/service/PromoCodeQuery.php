<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 11/13/15
 * Time: 12:45 PM
 * Email: t.kanstantsin@gmail.com
 */

namespace common\models\service;


use yii\db\ActiveQuery;
use yii\db\Expression;

/**
 * Class PromoCodeQuery
 * @package common\models\service
 */
class PromoCodeQuery extends ActiveQuery
{

    /**
     * @param string $code
     * @return $this
     */
    public function byCode($code)
    {
        return $this->andWhere([
            'code' => $code,
        ]);
    }

    /**
     * @param bool $isUsed
     * @return $this
     */
    public function isUsed($isUsed)
    {
        return $this->andWhere(['OR',
            [
                'group_id' => PromoCodeGroup::GROUP_INDIVIDUAL,
                'is_used' => $isUsed,
            ],
        ]);
    }

    public function isFree()
    {
        return $this->andWhere(['OR',
            [
                'group_id' => PromoCodeGroup::GROUP_INDIVIDUAL,
                'is_used' => false,
            ],
            ['<>', 'group_id', PromoCodeGroup::GROUP_INDIVIDUAL],
        ]);
    }

    /**
     * @param bool $isStarted
     * @return $this
     */
    public function isStarted($isStarted = true)
    {
        return $this->andWhere([
            ($isStarted ? '<=' : '>='),  'started_at', new Expression('CURDATE()'),
        ]);
    }

    /**
     * @param bool $isExpired
     * @return $this
     */
    public function isExpired($isExpired = true)
    {
        return $this->andWhere([
            ($isExpired ? '<=' : '>='),  'expired_at', new Expression('CURDATE()'),
        ]);
    }

}