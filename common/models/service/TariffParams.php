<?php

namespace common\models\service;

use Yii;

/**
 * This is the model class for table "service_tariff_params".
 *
 * @property int $id
 * @property string $name
 *
 * @property ServiceTariffParamsValue[] $serviceTariffParamsValues
 * @property ServiceSubscribeTariff[] $tariffs
 */
class TariffParams extends \yii\db\ActiveRecord
{
    const PRICELIST_PROD_COUNT = 1;
    const PRICELIST_UPDATE = 2;
    const PRICELIST_VIEW_CARD = 3;
    const PRICELIST_CHECKOUT = 4;
    const PRICELIST_NOTIFICATION = 5;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'service_tariff_params';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * Gets query for [[ServiceTariffParamsValues]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getServiceTariffParamsValues()
    {
        return $this->hasMany(TariffParamsValue::className(), ['params_id' => 'id']);
    }

    /**
     * Gets query for [[Tariffs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTariffs()
    {
        return $this->hasMany(SubscribeTariff::className(), ['id' => 'tariff_id'])->viaTable('service_tariff_params_value', ['params_id' => 'id']);
    }
}
