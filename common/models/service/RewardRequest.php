<?php

namespace common\models\service;

use Yii;
use common\models\Company;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "reward_request".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $company_id
 * @property integer $sum
 * @property integer $type
 * @property integer $status
 *
 * @property Company $company
 */
class RewardRequest extends \yii\db\ActiveRecord
{
    /**
     *
     */
    const STATUS_DEFAULT = 1;
    /**
     *
     */
    const STATUS_PAYED = 2;
    /**
     *
     */
    const STATUS_REJECT = 3;

    /**
     *
     */
    const RS_TYPE = 1;
    /**
     *
     */
    const INDIVIDUAL_ACCOUNT_TYPE = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'reward_request';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'sum'], 'required'],
            [['company_id', 'sum', 'type', 'status', 'created_at'], 'integer'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'sum' => 'Sum',
            'type' => 'Type',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return bool
     */
    public function sendMessage()
    {
        return \Yii::$app->mailer->compose([
            'html' => 'system/reward/html',
            'text' => 'system/reward/text',
        ], [
            'subject' => 'Ваше вознаграждение оплачено!',
            'sum' => $this->sum,
            'type' => $this->type,
        ])
            ->setFrom([\Yii::$app->params['emailList']['info'] => \Yii::$app->params['emailFromName']])
            ->setTo($this->company->email)
            ->setSubject('Ваше вознаграждение оплачено!')
            ->send();
    }
}
