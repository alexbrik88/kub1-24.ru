<?php

namespace common\models\service;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "service_tariff".
 *
 * @todo: duplicate price in subscribe
 *
 * @property integer $id
 * @property integer $duration_month
 * @property integer $duration_day
 * @property integer $price
 *
 * @property DiscountByQuantity[] $discounts
 * @property SubscribeTariffGroup $tariffGroup
 */
class SubscribeTariff extends \yii\db\ActiveRecord
{
    const TARIFF_1 = 18;
    const TARIFF_2 = 19;
    const TARIFF_3 = 20;
    const TARIFF_TRIAL = 4;
    const TARIFF_PRICE_LIST_TRIAL = 39;
    const TARIFF_ANALYTICS_TRIAL = 70;

    const TARIFF_TAXROBOT_PAY_EXTRA = 28;

    const UNLIMIT_COUNT = 1000000000;

    protected static $_paidStandartIds;
    protected static $_paidStandartActiveIds;
    protected static $_paidActualIds;

    protected $_paramsValues;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service_subscribe_tariff';
    }

    /**
     * @return SubscribeTariffQuery
     */
    public static function find()
    {
        return new SubscribeTariffQuery(get_called_class());
    }

    /**
     * @return array
     */
    public static function paidActualIds()
    {
        if (self::$_paidActualIds === null) {
            self::$_paidActualIds = SubscribeTariff::find()->actual()->orderBy([
                'id' => SORT_ASC,
            ])->select('id')->column();
        }

        return self::$_paidActualIds;
    }

    /**
     * @return array
     */
    public static function paidStandartIds()
    {
        if (self::$_paidStandartIds === null) {
            self::$_paidStandartIds = SubscribeTariff::paidStandartQuery()->select('id')->column();
        }

        return self::$_paidStandartIds;
    }

    /**
     * @return array
     */
    public static function paidStandartActiveIds()
    {
        if (self::$_paidStandartActiveIds === null) {
            self::$_paidStandartActiveIds = SubscribeTariff::paidStandartQuery()->andWhere([
                'is_active' => true,
            ])->select('id')->column();
        }

        return self::$_paidStandartActiveIds;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_active'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'duration_month' => 'Длительность (месяцев)',
            'duration_day' => 'Длительность (дней)',
            'price' => 'Стоимость',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContainsTariffs()
    {
        return $this->hasMany(SubscribeTariff::className(), ['id' => 'contains_tariff_id'])->via('tariffContainsTariffs');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContainsStandartTariff()
    {
        return $this->hasOne(SubscribeTariff::className(), [
            'id' => 'contains_tariff_id',
        ])->via('tariffContainsTariffs')->andWhere([
            'tariff_group_id' => SubscribeTariffGroup::STANDART,
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContainsNotStandartTariffs()
    {
        return $this->getContainsTariffs()->andWhere([
            'not',
            ['tariff_group_id' => SubscribeTariffGroup::STANDART],
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParamsValues()
    {
        return $this->hasMany(TariffParamsValue::className(), ['tariff_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getParams()
    {
        return $this->hasMany(TariffParams::className(), ['id' => 'params_id'])->via('paramsValues');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariffContainsTariffs()
    {
        return $this->hasMany(SubscribeContainsTariff::className(), ['tariff_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscounts()
    {
        return $this->hasMany(DiscountByQuantity::className(), ['tariff_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariffGroup()
    {
        return $this->hasOne(SubscribeTariffGroup::className(), ['id' => 'tariff_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayExtraForTariff()
    {
        $query = $this->hasOne(self::className(), ['id' => 'pay_extra_for']);

        if (!$this->is_pay_extra) {
            $query->andWhere('0=1');
        }

        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function paidActualQuery()
    {
        return SubscribeTariff::find()->actual();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function paidStandartQuery()
    {
        return SubscribeTariff::find()->andWhere([
            'tariff_group_id' => SubscribeTariffGroup::STANDART,
        ])->andWhere(['>', 'price', 0]);
    }

    /**
     * @return number
     */
    public function discount($quantity)
    {
        return 1 * $this->getDiscounts()
            ->select(new \yii\db\Expression('IFNULL([[percent]], 0)'))
            ->andWhere(['<=', 'quantity', (int) $quantity])
            ->orderBy(['quantity' => SORT_DESC])
            ->scalar();
    }

    /**
     * @return string
     */
    public function getReadableDuration()
    {
        return SubscribeHelper::getReadableDuration($this);
    }

    /**
     * @return string
     */
    public function getTariffName()
    {
        $result = $this->getReadableDuration() . ($this->id == self::TARIFF_TRIAL ? ' (пробный период)' : '');
        $groupId = $this->tariff_group_id;

        if (in_array($groupId, SubscribeTariffGroup::$hasLimit)) {
            $result .= ' '.($this->label ?: $this->getLimitText());
        }

        return $result;
    }

    /**
     * @return string
     */
    public function getLimitText()
    {
        $result = '';
        $groupId = $this->tariff_group_id;

        if (in_array($groupId, SubscribeTariffGroup::$hasLimit)) {
            $itemsName = ArrayHelper::getValue(SubscribeTariffGroup::$limitItemLabel, [$groupId, 'of']);
            if ($this->tariff_limit === null) {
                $result .= "на без лимитное количество $itemsName";
            } else {
                $payPerQuantity = in_array($groupId, SubscribeTariffGroup::$payPerQuantity);
                if ($this->id == SubscribeTariff::TARIFF_TRIAL || $payPerQuantity) {
                    $result .= "на {$this->tariff_limit} $itemsName";
                } elseif ($this->duration_month == 1) {
                    $result .= "на {$this->tariff_limit} $itemsName в месяц";
                } else {
                    $result .= "по {$this->tariff_limit} $itemsName в месяц";
                }
            }
        }

        return $result;
    }

    /**
     * @param $tariff_id
     * @return array
     */
    public static function getTariffsByTariff($tariff_id)
    {
        $tariff = ($tariff_id) ? self::findOne($tariff_id) : null;

        if ($tariff) {
            return self::find()
                ->select('id')
                ->where(['tariff_group_id' => $tariff->tariff_group_id])
                ->andWhere(['!=', 'id', self::TARIFF_TRIAL])
                ->asArray()
                ->column();
        }

        return [];
    }

    /**
     * @param $paramId
     * @return string
     */
    public function getParamValue($paramId)
    {
        if (!isset($this->_paramsValues)) {
            $this->_paramsValues = $this->getParamsValues()->select('value')->indexBy('params_id')->column();
        }

        return ArrayHelper::getValue($this->_paramsValues, $paramId);
    }

    /**
     * @param $paramId
     * @return string
     */
    public function getNamedParamValue($paramName, $groupId)
    {
        $paramId = null;
        if (!isset($this->_paramsValues)) {
            $this->_paramsValues = $this->getParamsValues()->select('value')->indexBy('params_id')->column();
        }

        switch ($groupId.$paramName) {
            case SubscribeTariffGroup::PRICE_LIST . 'products_count':   $paramId = 1; break;
            case SubscribeTariffGroup::PRICE_LIST . 'has_auto_update':  $paramId = 2; break;
            case SubscribeTariffGroup::PRICE_LIST . 'has_product_view': $paramId = 3; break;
            case SubscribeTariffGroup::PRICE_LIST . 'has_checkout':     $paramId = 4; break;
            case SubscribeTariffGroup::PRICE_LIST . 'has_notification': $paramId = 5; break;
        }

        return ArrayHelper::getValue($this->_paramsValues, $paramId);
    }
}
