<?php

namespace common\models\service;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "service_statistic".
 *
 * @todo: remove hardcode in columns below (use elastic or other search engine for search and filtering).
 *
 * @property integer $id
 * @property string $year
 * @property integer $month
 * @property integer $invoice_count
 * @property integer $payed_sum
 * @property integer $payed_invoice_count
 * @property integer $payed_sum_by_novice
 * @property integer $payed_invoice_count_by_novice
 * @property integer $not_payed_sum
 * @property integer $not_payed_invoice_count
 * @property integer $tariff_1 // hardcode!
 * @property integer $tariff_2
 * @property integer $tariff_3
 * @property integer $payment_type_1 // hardcode!
 * @property integer $payment_type_2
 * @property integer $payment_type_3
 */
class Statistic extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service_statistic';
    }

    /**
     * @return StatisticQuery
     */
    public static function find()
    {
        return new StatisticQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            [
                'class' => TimestampBehavior::className(),
                'createdAtAttribute' => false,
                'updatedAtAttribute' => 'updated_at',
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['year', 'month'], 'required'],
            [
                ['year', 'month', 'invoice_count',
                    'payed_sum', 'payed_invoice_count',
                    'payed_sum_by_novice', 'payed_invoice_count_by_novice',
                    'not_payed_sum', 'not_payed_invoice_count',
                    'tariff_1', 'tariff_2', 'tariff_3',
                    'payment_type_1', 'payment_type_2', 'payment_type_3',
                ], 'integer'],
            [['year', 'month'], 'unique', 'targetAttribute' => ['year', 'month'], 'message' => 'Указанная комбинация года и месяца уже существует'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'year' => 'Год',
            'month' => 'Месяц',
            'invoice_count' => 'Выставлено счетов',
            'payed_sum' => 'Оплачено на сумму',
            'payed_invoice_count' => 'Оплачено счетов',
            'payed_sum_by_novice' => 'Оплачено новыми',
            'payed_invoice_count_by_novice' => 'Счетов от новых',
            'not_payed_sum' => 'Не оплачено на сумму',
            'not_payed_invoice_count' => 'Не оплчено счетов',
            'tariff_1' => 'Тариф 1', // 1 month
            'tariff_2' => 'Тариф 2', // 3 months
            'tariff_3' => 'Тариф 3', // 6 months
            'payment_type_1' => 'Тип платежа 1', // online
            'payment_type_2' => 'Тип платежа 2', // sberbank
            'payment_type_3' => 'Тип платежа 3', // invoice
        ];
    }
}
