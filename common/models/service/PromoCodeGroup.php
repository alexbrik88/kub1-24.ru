<?php

namespace common\models\service;

use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "service_payment_promo_code_group".
 *
 * @todo: rename into `type`.
 *
 * @property integer $id
 * @property string $name
 */
class PromoCodeGroup extends ActiveRecord
{
    const GROUP_INDIVIDUAL = 1;
    const GROUP_GROUPED = 2;

    const DEFAULT_GROUP = self::GROUP_GROUPED;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service_payment_promo_code_group';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
        ];
    }
}
