<?php

namespace common\models\service;

use common\components\date\DateHelper;
use common\models\Company;
use Yii;

/**
 * This is the model class for table "service_module_visit".
 *
 * @property int $id
 * @property int $module_id
 * @property int $company_id
 * @property string $date
 *
 * @property Company $company
 * @property ServiceModule $module
 */
class ServiceModuleVisit extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'service_module_visit';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['module_id', 'company_id', 'date'], 'required'],
            [['module_id', 'company_id'], 'integer'],
            [['date'], 'safe'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['module_id'], 'exist', 'skipOnError' => true, 'targetClass' => ServiceModule::className(), 'targetAttribute' => ['module_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'module_id' => 'Module ID',
            'company_id' => 'Company ID',
            'date' => 'Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getModule()
    {
        return $this->hasOne(ServiceModule::className(), ['id' => 'module_id']);
    }

    /**
     * @inheritdoc
     */
    public static function checkVisit(Company $company)
    {
        $moduleId = ServiceModule::getCurrentModuleId($company);

        if ($moduleId) {
            $visit = ServiceModuleVisit::find()
                ->andWhere(['and',
                    ['module_id' => $moduleId],
                    ['company_id' => $company->id],
                    ['date' => date(DateHelper::FORMAT_DATE)],
                ])->one();

            if ($visit === null) {

                Yii::$app->db->createCommand('INSERT IGNORE INTO `service_module_visit` (`module_id`, `company_id`, `date`) VALUES (:module_id, :company_id, :current_date)', [
                    'module_id' => $moduleId,
                    'company_id' => $company->id,
                    'current_date' => date(DateHelper::FORMAT_DATE)
                ])->execute();

                //$visit = new ServiceModuleVisit();
                //$visit->module_id = $moduleId;
                //$visit->company_id = $company->id;
                //$visit->date = date(DateHelper::FORMAT_DATE);
                //$visit->save(false);
            }
        }
    }
}
