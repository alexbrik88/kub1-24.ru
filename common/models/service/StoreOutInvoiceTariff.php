<?php

namespace common\models\service;

use Yii;

/**
 * This is the model class for table "service_store_out_invoice_tariff".
 *
 * @property integer $id
 * @property string $links_count
 * @property integer $cost_one_month_for_one_link
 * @property integer $total_amount
 * @property boolean $is_visible
 * @property integer $sort
 */
class StoreOutInvoiceTariff extends \yii\db\ActiveRecord
{
    const UNLIM_TARIFF = 3;

    const UNLIM_TIME_AND_LINKS_TARIFF = 4;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service_store_out_invoice_tariff';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['links_count', 'total_amount'], 'required'],
            [['cost_one_month_for_one_link', 'total_amount', 'sort'], 'integer'],
            [['links_count'], 'string', 'max' => 255],
            [['is_visible'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'links_count' => 'Links Count',
            'cost_one_month_for_one_link' => 'Cost One Month For One Link',
            'total_amount' => 'Total Amount',
        ];
    }
}
