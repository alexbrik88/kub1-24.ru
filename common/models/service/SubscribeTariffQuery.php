<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 11/26/15
 * Time: 4:58 AM
 * Email: t.kanstantsin@gmail.com
 */

namespace common\models\service;


use yii\db\ActiveQuery;

/**
 * Class SubscribeTariffQuery
 * @package common\models\service
 */
class SubscribeTariffQuery extends ActiveQuery
{

    /**
     * @return $this
     */
    public function nonTrial()
    {
        return $this->andWhere([
            '<>', 'id', SubscribeTariff::TARIFF_TRIAL,
        ]);
    }

    /**
     * @return $this
     */
    public function active()
    {
        return $this->andWhere(['is_active' => true]);
    }

    /**
     * @return $this
     */
    public function paid()
    {
        return $this->andWhere(['>', 'price', 0]);
    }

    /**
     * @return $this
     */
    public function payable()
    {
        return $this->andWhere(['>', 'price', 0]);
    }

    /**
     * @return $this
     */
    public function notExtra()
    {
        return $this->andWhere(['is_pay_extra' => 0]);
    }

    /**
     * @return $this
     */
    public function extra()
    {
        return $this->andWhere(['is_pay_extra' => true]);
    }

    /**
     * @return $this
     */
    public function actual()
    {
        return $this->active()->payable()->notExtra();
    }
}
