<?php

namespace common\models;

use common\models\company\CompanyType;
use common\models\service\SubscribeTariff;
use DateTime;
use frontend\models\Documents;
use Yii;

/**
 * This is the model class for table "discount_type".
 *
 * @property integer $id
 * @property string $description
 *
 * @property Discount[] $discounts
 */
class DiscountType extends \yii\db\ActiveRecord
{
    const TYPE_1 = 1;
    const TYPE_2 = 2;
    const TYPE_3 = 3;
    const TYPE_4 = 4;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'discount_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'required'],
            [['description'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDiscounts()
    {
        return $this->hasMany(Discount::className(), ['type_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public static function checkForType1(Company $company)
    {
        if (!in_array($company->company_type_id, [CompanyType::TYPE_OOO, CompanyType::TYPE_IP]) ||
            $company->getDiscounts()->andWhere(['type_id' => self::TYPE_1])->exists()
        ) {
            return false;
        }

        if ($company->isTrialNotPaid) {
            $created = (new DateTime())->setTimestamp($company->created_at)->modify('today');
            $today = new DateTime('today');
            $diff = $created->diff($today)->days;
            if ($company->id == 366 || $diff == 4) {
                return Discount::createType1($company, $today);
            }
            if ($diff < 10 && $company->getInvoices()->byDeleted()->byIOType(Documents::IO_TYPE_OUT)->offset(1)->exists()) {
                return Discount::createType1($company, new DateTime('tomorrow'));
            }
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public static function checkForType2(Company $company)
    {
        if ($company->isTrialNotPaid) {
            $time = time();
            $expired_at = $company->activeSubscribe->expired_at;
            if ($expired_at >= $time && $expired_at <= $time + (3600 * 24 * 2) &&
                !$company->getDiscounts()->andWhere(['type_id' => self::TYPE_2])->exists()
            ) {
                return Discount::createType2($company, $expired_at);
            }
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public static function checkForType4(Company $company)
    {
        if ($company->company_type_id == CompanyType::TYPE_IP) {
            if (empty($company->tax_authority_registration_date)) {
                $attributes = \common\components\DadataClient::getCompanyAttributes($company->inn);
                if (!empty($attributes['tax_authority_registration_date'])) {
                    $company->updateAttributes([
                        'tax_authority_registration_date' => $attributes['tax_authority_registration_date'],
                    ]);
                }
            }
            if (!empty($company->tax_authority_registration_date)) {
                $m = intval(date('m'));
                $regDate = date_create_from_format('Y-m-d|', $company->tax_authority_registration_date);
                $startDate = $m < 5 ? date_create('first day of january last year') : date_create('first day of january this year');

                if ($regDate >= $startDate) {
                    return Discount::createType4($company, $regDate);
                }
            }
        }

        return false;
    }
}
