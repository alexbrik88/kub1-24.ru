<?php

namespace common\models;

use Yii;
use common\models\employee\Employee;

/**
 * This is the model class for table "sent_email_start".
 *
 * @property int $sent_email_type_id
 * @property string $employee_id
 * @property string $date
 */
class SentEmailStart extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sent_email_start';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'sent_email_type_id' => 'Sent Email Type ID',
            'employee_id' => 'Employee ID',
            'date' => 'Date',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::class, ['id' => 'employee_id']);
    }

    /**
     * {@inheritdoc}
     */
    public static function start($type_id, $employee_id, $date = null) : void
    {
        if ($date instanceof \DateTime) {
            $date = $date->format('Y-m-d');
        } elseif (is_string($date) && $d = date_create($date)) {
            $date = $d->format('Y-m-d');
        } else {
            $date = date('Y-m-d');
        }

        $db = Yii::$app->db;
        if (is_array($employee_id)) {
            $columns = [
                'sent_email_type_id',
                'employee_id',
                'date',
            ];

            $rows = [];
            foreach ($employee_id as $item) {
                $rows[] = [
                    $type_id,
                    $item,
                    $date,
                ];
            }

            $sql = $db->createCommand()->batchInsert(self::tableName(), $columns, $rows)->getRawSql();
        } else {
            $columns = [
                'sent_email_type_id' => $type_id,
                'employee_id' => $employee_id,
                'date' => $date,
            ];

            $sql = $db->createCommand()->insert(self::tableName(), $columns)->getRawSql();
        }

        $sql = strtr($sql, ['INSERT INTO' => 'INSERT IGNORE INTO']);
        $db->createCommand($sql)->execute();
    }
}
