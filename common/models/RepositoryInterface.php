<?php

namespace common\models;

use yii\data\DataProviderInterface;
use yii\data\Sort;

interface RepositoryInterface
{
    /** @var int */
    public const DEFAULT_PAGE_SIZE = 100;

    /**
     * @return DataProviderInterface
     */
    public function getDataProvider(): DataProviderInterface;

    /**
     * @return Sort
     */
    public function getSort(): Sort;
}
