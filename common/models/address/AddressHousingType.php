<?php

namespace common\models\address;

use Yii;

/**
 * This is the model class for table "address_housing_type".
 *
 * @property integer $id
 * @property string $name
 * @property string $name_short
 */
class AddressHousingType extends \yii\db\ActiveRecord
{
    const TYPE_BLOCK = 1;
    const TYPE_BUILDING = 2;
    const TYPE_CONSTRUCTION = 3;
    const TYPE_LITER = 4;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'address_housing_type';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'name_short' => 'Сокращённое',
        ];
    }
}
