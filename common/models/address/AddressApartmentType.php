<?php

namespace common\models\address;

use Yii;

/**
 * This is the model class for table "address_apartment_type".
 *
 * @property integer $id
 * @property string $name
 * @property string $name_short
 */
class AddressApartmentType extends \yii\db\ActiveRecord
{
    const TYPE_APARTMENT = 1;
    const TYPE_ROOM = 2;
    const TYPE_OFFICE = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'address_apartment_type';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'name_short' => 'Сокращённое',
        ];
    }
}
