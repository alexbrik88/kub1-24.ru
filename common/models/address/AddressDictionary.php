<?php

namespace common\models\address;

use Yii;

/**
 * This is the model class for table "address_dictionary".
 *
 * @property integer $ACTSTATUS
 * @property string $AOGUID
 * @property string $AOID
 * @property integer $AOLEVEL
 * @property string $AREACODE
 * @property string $AUTOCODE
 * @property integer $CENTSTATUS
 * @property string $CITYCODE
 * @property string $CODE
 * @property integer $CURRSTATUS
 * @property string $ENDDATE
 * @property string $FORMALNAME
 * @property string $FULLNAME
 * @property string $IFNSFL
 * @property string $IFNSUL
 * @property string $NEXTID
 * @property string $OFFNAME
 * @property string $OKATO
 * @property string $OKTMO
 * @property integer $OPERSTATUS
 * @property string $PARENTGUID
 * @property string $PLACECODE
 * @property string $PLAINCODE
 * @property string $POSTALCODE
 * @property string $PREVID
 * @property string $REGIONCODE
 * @property string $SHORTNAME
 * @property string $STARTDATE
 * @property string $STREETCODE
 * @property string $TERRIFNSFL
 * @property string $TERRIFNSUL
 * @property string $UPDATEDATE
 * @property string $CTARCODE
 * @property string $EXTRCODE
 * @property string $SEXTCODE
 * @property integer $LIVESTATUS
 * @property string $NORMDOC
 */
class AddressDictionary extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'address_dictionary';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ACTSTATUS', 'AOLEVEL', 'CENTSTATUS', 'CURRSTATUS', 'OPERSTATUS', 'LIVESTATUS'], 'integer'],
            [['ENDDATE', 'STARTDATE', 'UPDATEDATE'], 'safe'],
            [['FULLNAME'], 'required'],
            [['AOGUID', 'AOID', 'NEXTID', 'PARENTGUID', 'PREVID', 'NORMDOC'], 'string', 'max' => 36],
            [['AREACODE', 'CITYCODE', 'PLACECODE', 'CTARCODE', 'SEXTCODE'], 'string', 'max' => 3],
            [['AUTOCODE'], 'string', 'max' => 1],
            [['CODE'], 'string', 'max' => 17],
            [['FORMALNAME', 'OFFNAME'], 'string', 'max' => 120],
            [['FULLNAME'], 'string', 'max' => 131],
            [['IFNSFL', 'IFNSUL', 'STREETCODE', 'TERRIFNSFL', 'TERRIFNSUL', 'EXTRCODE'], 'string', 'max' => 4],
            [['OKATO', 'OKTMO'], 'string', 'max' => 11],
            [['PLAINCODE'], 'string', 'max' => 15],
            [['POSTALCODE'], 'string', 'max' => 6],
            [['REGIONCODE'], 'string', 'max' => 2],
            [['SHORTNAME'], 'string', 'max' => 10],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ACTSTATUS' => 'Actstatus',
            'AOGUID' => 'Aoguid',
            'AOID' => 'Aoid',
            'AOLEVEL' => 'Aolevel',
            'AREACODE' => 'Areacode',
            'AUTOCODE' => 'Autocode',
            'CENTSTATUS' => 'Centstatus',
            'CITYCODE' => 'Citycode',
            'CODE' => 'Code',
            'CURRSTATUS' => 'Currstatus',
            'ENDDATE' => 'Enddate',
            'FORMALNAME' => 'Formalname',
            'FULLNAME' => 'FORMALNAME + \' \' + SHORTNAME',
            'IFNSFL' => 'Ifnsfl',
            'IFNSUL' => 'Ifnsul',
            'NEXTID' => 'Nextid',
            'OFFNAME' => 'Offname',
            'OKATO' => 'Okato',
            'OKTMO' => 'Oktmo',
            'OPERSTATUS' => 'Operstatus',
            'PARENTGUID' => 'Parentguid',
            'PLACECODE' => 'Placecode',
            'PLAINCODE' => 'Plaincode',
            'POSTALCODE' => 'Postalcode',
            'PREVID' => 'Previd',
            'REGIONCODE' => 'Regioncode',
            'SHORTNAME' => 'Shortname',
            'STARTDATE' => 'Startdate',
            'STREETCODE' => 'Streetcode',
            'TERRIFNSFL' => 'Terrifnsfl',
            'TERRIFNSUL' => 'Terrifnsul',
            'UPDATEDATE' => 'Updatedate',
            'CTARCODE' => 'Ctarcode',
            'EXTRCODE' => 'Extrcode',
            'SEXTCODE' => 'Sextcode',
            'LIVESTATUS' => 'Livestatus',
            'NORMDOC' => 'Normdoc',
        ];
    }
}
