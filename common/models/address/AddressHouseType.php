<?php

namespace common\models\address;

use Yii;

/**
 * This is the model class for table "address_house_type".
 *
 * @property integer $id
 * @property string $name
 */
class AddressHouseType extends \yii\db\ActiveRecord
{
    /**
     *
     */
    const TYPE_HOUSE = 1;
    /**
     * Владение
     */
    const TYPE_PROPERTY = 2;
    /**
     * Домовладение
     */
    const TYPE_HOUSE_AND_GROUNDS = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'address_house_type';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
        ];
    }
}
