<?php

namespace common\models\address;

use Yii;

/**
 * This is the model class for table "country".
 *
 * @property integer $id
 * @property string $code
 * @property string $name_short
 */
class Country extends \yii\db\ActiveRecord
{
    const COUNTRY_WITHOUT = 1;

    const COUNTRY_RUSSIA = 172;

    public static function getCountries()
    {
        return static::find()->orderBy('name_short ASC')->all();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'country';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Цифровой код',
            'name_short' => 'Кратоке наименование',
        ];
    }
}
