<?php

namespace common\models\address;

use yii\db\ActiveRecord;

/**
 * Справочник почтовых индексов (индекс - регион)
 * @property int $postalcode Почтовый индекс
 * @property int $regioncode Код региона
 */
class AddressClassifier extends ActiveRecord
{

    public static function tableName()
    {
        return '{{%address_classifier}}';
    }

    /**
     * Возвращает код региона, соответствующий почтовому индексу
     * @param int $postalcode Почтовый индекс
     * @return int|null
     */
    public static function  regionByPostal(int $postalcode)
    {
        $code = self::find()->select('regioncode')->where(['postalcode' => $postalcode])->scalar();
        return $code === false ? null : (int)$code;
    }

    public function rules()
    {
        return [
            [['postalcode', 'regioncode'], 'required'],
            ['postalcode', 'integer', 'min' => 100000, 'max' => 999999],
            ['regioncode', 'integer', 'min' => 1, 'max' => 99],
            ['postalcode', 'unique'],
        ];
    }
}
