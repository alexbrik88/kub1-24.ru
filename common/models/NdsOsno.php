<?php
/**
 * Created by PhpStorm.
 * User: valik
 * Date: 15.02.2016
 * Time: 12:40
 */

namespace common\models;


use yii\db\ActiveRecord;

/**
 * Class NdsOsno
 * @package common\models
 */
class NdsOsno extends ActiveRecord
{
    /**
     *
     */
    const WITH_NDS = 1;

    /**
     *
     */
    const WITHOUT_NDS = 2;

    /**
     * @param $id
     * @return bool
     */
    public static function hasNds($id)
    {
        return $id == NdsOsno::WITH_NDS;
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'nds_osno';
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Имя',
        ];
    }
}