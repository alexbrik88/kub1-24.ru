<?php

namespace common\models\template;

use common\models\Company;
use Yii;

/**
 * This is the model class for table "template".
 *
 * @property integer $id
 * @property integer $sequence
 * @property string $title
 *
 * @property TemplateFile[] $templateFiles
 */
class Template extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'template';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sequence'], 'integer'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sequence' => 'Ранг в выдаче',
            'title' => 'Заголовок блока',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplateFiles()
    {
        return $this->hasMany(TemplateFile::className(), ['template_id' => 'id']);
    }
}
