<?php

namespace common\models\template;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\template\Template;

/**
 * TemplateSearch represents the model behind the search form about `common\models\template\Template`.
 */
class TemplateSearch extends Template
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'sequence'], 'integer'],
            [['title'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Template::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $query->andFilterWhere([
            'id' => $this->id,
            'sequence' => $this->sequence,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title]);



        $dataProvider->sort->defaultOrder = ['sequence' => SORT_ASC];

        return $dataProvider;
    }
}
