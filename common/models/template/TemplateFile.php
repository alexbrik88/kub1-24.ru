<?php

namespace common\models\template;

use Yii;

/**
 * This is the model class for table "template_file".
 *
 * @property integer $id
 * @property string $file_name
 * @property integer $template_id
 *
 * @property Template $template
 */
class TemplateFile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public $hat_document;
    public static function tableName()
    {
        return 'template_file';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['template_id'], 'integer'],
            [['file_name'], 'string', 'max' => 255],
            [['hat_document'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'file_name' => 'File Name',
            'template_id' => 'Template ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTemplate()
    {
        return $this->hasOne(Template::className(), ['id' => 'template_id']);
    }

    public function getUploadPath($absolutePath = true)
    {
        $filePath = 'uploads' . DIRECTORY_SEPARATOR . $this->tableName() . DIRECTORY_SEPARATOR . $this->template_id;
        $fullPath = Yii::getAlias('@common') . DIRECTORY_SEPARATOR . $filePath;

        if (!file_exists($fullPath)) {
            $parentDir = substr($fullPath, 0, strrpos($fullPath, DIRECTORY_SEPARATOR));
            if (is_writable($parentDir)) {
                mkdir($fullPath);
            } else {
                throw new \Exception('Директория для записи не доступна.' . $fullPath);
            }
        }

        return $absolutePath ? $fullPath : DIRECTORY_SEPARATOR . $filePath;
    }

    public function getFilePath($fileName, $absolutePath = true)
    {
        return $this->getUploadPath($absolutePath) . DIRECTORY_SEPARATOR . $fileName;
    }

    public function getDirPath($id)
    {
        return Yii::getAlias('@common') . DIRECTORY_SEPARATOR .
            'uploads' . DIRECTORY_SEPARATOR .
            $this->tableName() . DIRECTORY_SEPARATOR .
            $id;
    }

    public function removeFile($file)
    {
        if (!file_exists($file)) {
            return false;
        }

        if (!unlink($file)) {
            return false;
        }

        return true;
    }

    public function removeDirectory($id)
    {
        $dirPath = $this->getDirPath($id);

        if ($handle = opendir($dirPath)) {
            while (false !== ($file = readdir($handle))) {
                if ($file != "." && $file != "..") {
                    unlink($dirPath . DIRECTORY_SEPARATOR . $file);
                }
            }
            closedir($handle);
        }

        return (rmdir($dirPath)) ? true : false;
    }
}
