<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "ifns".
 *
 * @property integer $ga
 * @property string $gb
 * @property string $g1
 * @property string $g2
 * @property string $g3
 * @property string $g4
 * @property string $g5
 * @property string $g6
 * @property string $g7
 * @property string $g8
 * @property string $g9
 * @property string $g10
 * @property string $g11
 * @property string $g12
 * @property string $g13
 * @property string $g14
 * @property string $g15
 * @property string $g16
 * @property string $g18
 * @property string $g19
 * @property string $g20
 * @property string $g21
 * @property integer $created_at
 * @property integer $updated_at
 */
class Ifns extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ifns';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['ga'], 'required'],
            [['ga'], 'integer'],
            [['g5'], 'string'],
            [['gb', 'g1', 'g2', 'g3', 'g4', 'g6', 'g7', 'g8', 'g9', 'g10', 'g11', 'g12', 'g13', 'g14', 'g15', 'g16', 'g18', 'g19', 'g20', 'g21'], 'string', 'max' => 500],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'ga' => 'Код ИФНС',
            'gb' => 'Полное наименование',
            'g1' => 'Адрес',
            'g2' => 'Телефон',
            'g4' => 'Получатель платежа',
            'g6' => 'ИНН получателя',
            'g7' => 'КПП получателя',
            'g8' => 'Банк получателя',
            'g9' => 'БИК банка получателя',
            'g11' => 'Номер счета получателя',
            'g3' => 'G3',
            'g5' => 'G5',
            'g10' => 'G10',
            'g12' => 'G12',
            'g13' => 'G13',
            'g14' => 'G14',
            'g15' => 'G15',
            'g16' => 'G16',
            'g18' => 'G18',
            'g19' => 'G19',
            'g20' => 'G20',
            'g21' => 'G21',
        ];
    }

    /**
     * @return string
     */
    public function getContractorName()
    {
        $replace = [
            'Инспекция Федеральной налоговой службы' => 'ИФНС',
            'Межрайонная инспекция Федеральной налоговой службы' => 'МРИ ФНС',
        ];

        return $this->g4 . ' (' . strtr($this->gb, $replace) . ')';
    }

    /**
     * @return array
     */
    public function toArray(array $fields = [], array $expand = [], $recursive = true)
    {
        return array_merge(parent::toArray($fields, $expand, $recursive), ['contractorName' => $this->contractorName]);
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return implode(', ', array_filter(explode(',', $this->g1)));
    }
}
