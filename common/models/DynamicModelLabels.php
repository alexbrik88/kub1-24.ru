<?php

namespace common\models;

use yii\base\DynamicModel;

/**
 * Дополнительная поддержка названий полей
 */
class DynamicModelLabels extends DynamicModel
{
    protected $label = [];

    public function setAttributeLabels(array $labels)
    {
        $this->label = $labels;
    }

    public function addLabel(string $attribute, string $label)
    {
        $this->label[$attribute] = $label;
    }

    public function getAttributeLabel($name)
    {
        return $this->label[$name] ?? $name;
    }
}