<?php

namespace common\models\companyStructure;

use Yii;
use common\models\Company;

/**
 * This is the model class for table "company_to_accounting_system_type".
 *
 * @property int $company_id
 * @property int $accounting_system_id
 *
 * @property AccountingSystemType $accountingSystem
 * @property Company $company
 */
class CompanyToAccountingSystemType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company_to_accounting_system_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'accounting_system_id'], 'required'],
            [['company_id', 'accounting_system_id'], 'integer'],
            [['company_id', 'accounting_system_id'], 'unique', 'targetAttribute' => ['company_id', 'accounting_system_id']],
            [['accounting_system_id'], 'exist', 'skipOnError' => true, 'targetClass' => AccountingSystemType::className(), 'targetAttribute' => ['accounting_system_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'company_id' => 'Company ID',
            'accounting_system_id' => 'Accounting System ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccountingSystem()
    {
        return $this->hasOne(AccountingSystemType::className(), ['id' => 'accounting_system_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
}
