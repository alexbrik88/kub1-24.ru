<?php

namespace common\models\companyStructure;

use Yii;
use common\models\Company;
use common\models\company\CheckingAccountant;

/**
 * This is the model class for table "company_to_acquiring".
 *
 * @property int $company_id
 * @property int $account_id
 *
 * @property CheckingAccountant $account
 * @property Company $company
 */
class CompanyToAcquiring extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company_to_acquiring';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'account_id'], 'required'],
            [['company_id', 'account_id'], 'integer'],
            [['company_id', 'account_id'], 'unique', 'targetAttribute' => ['company_id', 'account_id']],
            [['account_id'], 'exist', 'skipOnError' => true, 'targetClass' => CheckingAccountant::className(), 'targetAttribute' => ['account_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'company_id' => 'Company ID',
            'account_id' => 'Account ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAccount()
    {
        return $this->hasOne(CheckingAccountant::className(), ['id' => 'account_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
}
