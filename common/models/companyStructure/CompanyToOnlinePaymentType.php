<?php

namespace common\models\companyStructure;

use Yii;
use common\models\Company;

/**
 * This is the model class for table "company_to_online_payment_type".
 *
 * @property int $company_id
 * @property int $payment_type_id
 *
 * @property Company $company
 * @property OnlinePaymentType $paymentType
 */
class CompanyToOnlinePaymentType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company_to_online_payment_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'payment_type_id'], 'required'],
            [['company_id', 'payment_type_id'], 'integer'],
            [['company_id', 'payment_type_id'], 'unique', 'targetAttribute' => ['company_id', 'payment_type_id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['payment_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => OnlinePaymentType::className(), 'targetAttribute' => ['payment_type_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'company_id' => 'Company ID',
            'payment_type_id' => 'Payment Type ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentType()
    {
        return $this->hasOne(OnlinePaymentType::className(), ['id' => 'payment_type_id']);
    }
}
