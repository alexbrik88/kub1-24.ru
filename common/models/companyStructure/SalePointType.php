<?php

namespace common\models\companyStructure;

use Yii;

/**
 * This is the model class for table "sale_point_type".
 *
 * @property int $id
 * @property string $name
 * @property int $sort
 *
 * @property SalePoint[] $salePoints
 */
class SalePointType extends \yii\db\ActiveRecord
{
    /**
     * @var array
     */
    public static $icon = [
        1 => 'sale-point-department.png',
        2 => 'sale-point-shop.png',
        3 => 'sale-point-i-shop.png',
        4 => 'sale-point-restaurant.png',
        5 => 'sale-point-filial.png',
    ];

    public static $helpText = [
        1 => 'Если у вас есть менеджеры по продажам, которые выставляют счета клиентам, <br/>то добавьте отдел, укажите ваших менеджеров по продажам, что бы строить <br/>отчеты по каждому их них и по отделу целиком. ',
        2 => 'Если у вас есть магазин или торговая точка, то добавьте его. <br/>Необходимо будет указать название, адрес, наличие касс.',
        3 => 'Если у вас есть интернет магазин, то добавьте его. <br/>Необходимо будет указать название сайта и каким образом вы принимаете оплату от покупателя <br/>(если вы это еще не внесли на предыдущем шаге).',
        4 => 'Если у вас есть кафе, ресторан или другой тип общественного питания, <br/>то добавьте его. Необходимо будет указать название, адрес, наличие касс.',
        5 => 'Если у вас есть филиал в другом городе, то добавьте его. <br/>Необходимо будет указать город, название, ваших менеджеров по продажам.',
    ];

    public static $helpNewSalePointType = 'Если вы хотели бы указать, что-то свое, напишите нам';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sale_point_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['sort'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'sort' => 'Sort',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalePoints()
    {
        return $this->hasMany(SalePoint::className(), ['type_id' => 'id']);
    }
}
