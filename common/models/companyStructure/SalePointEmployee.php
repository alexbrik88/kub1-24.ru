<?php

namespace common\models\companyStructure;

use Yii;
use common\models\employee\Employee;

/**
 * This is the model class for table "sale_point_employee".
 *
 * @property int $sale_point_id
 * @property int $employee_id
 *
 * @property Employee $employee
 * @property SalePoint $salePoint
 */
class SalePointEmployee extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sale_point_employee';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sale_point_id', 'employee_id'], 'required'],
            [['sale_point_id', 'employee_id'], 'integer'],
            [['sale_point_id', 'employee_id'], 'unique', 'targetAttribute' => ['sale_point_id', 'employee_id']],
            [['employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['employee_id' => 'id']],
            [['sale_point_id'], 'exist', 'skipOnError' => true, 'targetClass' => SalePoint::className(), 'targetAttribute' => ['sale_point_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'sale_point_id' => 'Sale Point ID',
            'employee_id' => 'Employee ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalePoint()
    {
        return $this->hasOne(SalePoint::className(), ['id' => 'sale_point_id']);
    }
}
