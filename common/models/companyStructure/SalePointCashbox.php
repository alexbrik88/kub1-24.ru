<?php

namespace common\models\companyStructure;

use Yii;
use common\models\cash\Cashbox;

/**
 * This is the model class for table "sale_point_cashbox".
 *
 * @property int $sale_point_id
 * @property int $cashbox_id
 *
 * @property Cashbox $cashbox
 * @property SalePoint $salePoint
 */
class SalePointCashbox extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sale_point_cashbox';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['sale_point_id', 'cashbox_id'], 'required'],
            [['sale_point_id', 'cashbox_id'], 'integer'],
            [['sale_point_id', 'cashbox_id'], 'unique', 'targetAttribute' => ['sale_point_id', 'cashbox_id']],
            [['cashbox_id'], 'exist', 'skipOnError' => true, 'targetClass' => Cashbox::className(), 'targetAttribute' => ['cashbox_id' => 'id']],
            [['sale_point_id'], 'exist', 'skipOnError' => true, 'targetClass' => SalePoint::className(), 'targetAttribute' => ['sale_point_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'sale_point_id' => 'Sale Point ID',
            'cashbox_id' => 'Cashbox ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashbox()
    {
        return $this->hasOne(Cashbox::className(), ['id' => 'cashbox_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalePoint()
    {
        return $this->hasOne(SalePoint::className(), ['id' => 'sale_point_id']);
    }
}
