<?php

namespace common\models\companyStructure;

use common\components\helpers\ArrayHelper;
use Yii;
use common\models\Company;

/**
 * This is the model class for table "ofd_type".
 *
 * @property int $id
 * @property int $company_id
 * @property string $name
 * @property int $sort
 *
 * @property CompanyToOfdType[] $companyToOfdTypes
 * @property Company[] $companies
 * @property Company $company
 */
class OfdType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ofd_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'sort'], 'integer'],
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'name' => 'Name',
            'sort' => 'Sort',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyToOfdTypes()
    {
        return $this->hasMany(CompanyToOfdType::className(), ['ofd_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Company::className(), ['id' => 'company_id'])->viaTable('company_to_ofd_type', ['ofd_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    // temp
    public function getLogo()
    {
        $icons = [
            6 => [
                'title' => 'ПлатформаОФД',
                'logo' => '/img/icons/platforma.jpg',
            ],
            8 => [
                'title' => 'TaxCOM',
                'logo' => '/img/icons/taxcom.jpg',
            ],
            4 => [
                'title' => 'Первый ОФД',
                'logo' => '/img/icons/perviy.jpg',
            ],
            12 => [
                'title' => 'Яндекс ОФД',
                'logo' => '/img/icons/yandex_ofd.jpg',
            ],
            2 => [
                'title' => 'Калуга.Астрал',
                'logo' => '/img/icons/astral.jpg',
            ],
            //0 => [
            //    'title' => 'ИнитПРО',
            //    'logo' => '/img/icons/initpro.jpg',
            //],
            //0 => [
            //    'title' => 'ОФД.ru',
            //    'logo' => '/img/icons/ofdru.jpg',
            //],
            //0 => [
            //    'title' => 'ОФД Я',
            //    'logo' => '/img/icons/yarus.jpg',
            //],
            //0 => [
            //    'title' => 'Сбис',
            //    'logo' => '/img/icons/sbis.jpg',
            //],
        ];

        return ArrayHelper::getValue(ArrayHelper::getValue($icons, $this->id), 'logo');
    }
}
