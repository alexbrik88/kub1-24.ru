<?php

namespace common\models\companyStructure;

use Yii;
use common\models\Company;

/**
 * This is the model class for table "company_to_ofd_type".
 *
 * @property int $company_id
 * @property int $ofd_id
 *
 * @property Company $company
 * @property OfdType $ofd
 */
class CompanyToOfdType extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company_to_ofd_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'ofd_id'], 'required'],
            [['company_id', 'ofd_id'], 'integer'],
            [['company_id', 'ofd_id'], 'unique', 'targetAttribute' => ['company_id', 'ofd_id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['ofd_id'], 'exist', 'skipOnError' => true, 'targetClass' => OfdType::className(), 'targetAttribute' => ['ofd_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'company_id' => 'Company ID',
            'ofd_id' => 'Ofd ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfd()
    {
        return $this->hasOne(OfdType::className(), ['id' => 'ofd_id']);
    }
}
