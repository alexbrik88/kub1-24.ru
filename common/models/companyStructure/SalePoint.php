<?php

namespace common\models\companyStructure;

use common\models\cash\CashBankFlows;
use common\models\cash\CashBankForeignCurrencyFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashEmoneyForeignCurrencyFlows;
use common\models\cash\CashOrderFlows;
use common\models\cash\CashOrderForeignCurrencyFlows;
use common\models\document\GoodsCancellation;
use common\models\document\Invoice;
use common\modules\acquiring\models\AcquiringOperation;
use common\modules\cards\models\CardOperation;
use frontend\models\log\ILogMessage;
use frontend\models\log\Log;
use frontend\models\log\LogEvent;
use Yii;
use common\models\Company;
use common\models\product\Store;
use common\models\cash\Cashbox;
use common\models\employee\Employee;

/**
 * This is the model class for table "sale_point".
 *
 * @property int $id
 * @property int $company_id
 * @property int $responsible_employee_id
 * @property int $type_id
 * @property int $status_id
 * @property string $name
 * @property string $city
 * @property string $address
 * @property string $site
 * @property int $store_id
 * @property int $online_payment_type_id
 * @property int $sort
 *
 * @property Company $company
 * @property OnlinePaymentType $onlinePaymentType
 * @property SalePointType $type
 * @property Store $store
 * @property SalePointCashbox[] $salePointCashboxes
 * @property Cashbox[] $cashboxes
 * @property SalePointEmployee[] $salePointEmployers
 * @property Employee[] $employers
 * @property Employee $responsibleEmployee
 *
 * @property AcquiringOperation[] $acquiringOperations
 * @property CardOperation[] $cardOperations
 * @property CashBankFlows[] $cashBankFlows
 * @property CashBankForeignCurrencyFlows[] $cashBankForeignCurrencyFlows
 * @property CashEmoneyFlows[] $cashEmoneyFlows
 * @property CashEmoneyForeignCurrencyFlows[] $cashEmoneyForeignCurrencyFlows
 * @property CashOrderFlows[] $cashOrderFlows
 * @property CashOrderForeignCurrencyFlows[] $cashOrderForeignCurrencyFlows
 * @property GoodsCancellation[] $goodsCancellations
 * @property Invoice[] $invoices
 *
 */
class SalePoint extends \yii\db\ActiveRecord implements ILogMessage
{
    const TYPE_DEPARTMENT = 1;
    const TYPE_SHOP = 2;
    const TYPE_INTERNET_SHOP = 3;
    const TYPE_RESTAURANT = 4;
    const TYPE_FILIAL = 5;

    const MAX_CASHBOXES = 3;

    const STATUS_ACTIVE = 1;
    const STATUS_CLOSED = 2;
    const STRING_ACTIVE = 'В работе';
    const STRING_CLOSED = 'Закрыта';

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sale_point';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'responsible_employee_id', 'type_id'], 'required'],
            [['company_id', 'responsible_employee_id', 'type_id', 'store_id', 'online_payment_type_id', 'sort', 'status_id'], 'integer'],
            [['status_id'], 'default', 'value' => self::STATUS_ACTIVE],
            [['status_id'], 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_CLOSED]],
            [['name', 'city', 'address', 'site'], 'string', 'max' => 255],
            [['site'], 'required', 'when' => function(SalePoint $model) {
                return $model->type_id == self::TYPE_INTERNET_SHOP;
            }],
            [['name'], 'required'],
            [['store_id'], 'required', 'when' => function(SalePoint $model) {
                return true;
            }],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['online_payment_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => OnlinePaymentType::className(), 'targetAttribute' => ['online_payment_type_id' => 'id']],
            [['type_id'], 'exist', 'skipOnError' => true, 'targetClass' => SalePointType::className(), 'targetAttribute' => ['type_id' => 'id']],
            [['store_id'], 'exist', 'skipOnError' => true, 'targetClass' => Store::className(), 'targetAttribute' => ['store_id' => 'id']],
            [['responsible_employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::class, 'targetAttribute' => ['responsible_employee_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'type_id' => 'Тип точки',
            'name' => 'Название',
            'city' => 'Город',
            'address' => 'Адрес',
            'site' => 'Сайт',
            'store_id' => 'Склад',
            'online_payment_type_id' => 'Прием платежей на сайте',
            'sort' => 'Сортировка',
            'cashboxes' => 'Касса',
            'employers' => 'Сотрудники',
            'responsible_employee_id' => 'Ответственный сотрудник',
        ];
    }

    /**
     * @return string
     */
    public function getClassName()
    {
        return __CLASS__;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResponsibleEmployee()
    {
        return $this->hasOne(Employee::class, ['id' => 'responsible_employee_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Employee::class, ['id' => 'responsible_employee_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOnlinePaymentType()
    {
        return $this->hasOne(OnlinePaymentType::className(), ['id' => 'online_payment_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getType()
    {
        return $this->hasOne(SalePointType::className(), ['id' => 'type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStore()
    {
        return $this->hasOne(Store::className(), ['id' => 'store_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalePointCashboxes()
    {
        return $this->hasMany(SalePointCashbox::className(), ['sale_point_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashboxes()
    {
        return $this->hasMany(Cashbox::className(), ['id' => 'cashbox_id'])->viaTable('sale_point_cashbox', ['sale_point_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalePointEmployers()
    {
        return $this->hasMany(SalePointEmployee::className(), ['sale_point_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployers()
    {
        return $this->hasMany(Employee::className(), ['id' => 'employee_id'])->viaTable('sale_point_employee', ['sale_point_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcquiringOperations()
    {
        return $this->hasMany(AcquiringOperation::class, ['sale_point_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCardOperations()
    {
        return $this->hasMany(CardOperation::class, ['sale_point_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashBankFlows()
    {
        return $this->hasMany(CashBankFlows::class, ['sale_point_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashBankForeignCurrencyFlows()
    {
        return $this->hasMany(CashBankForeignCurrencyFlows::class, ['sale_point_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashEmoneyFlows()
    {
        return $this->hasMany(CashEmoneyFlows::class, ['sale_point_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashEmoneyForeignCurrencyFlows()
    {
        return $this->hasMany(CashEmoneyForeignCurrencyFlows::class, ['sale_point_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashOrderFlows()
    {
        return $this->hasMany(CashOrderFlows::class, ['sale_point_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashOrderForeignCurrencyFlows()
    {
        return $this->hasMany(CashOrderForeignCurrencyFlows::class, ['sale_point_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGoodsCancellations()
    {
        return $this->hasMany(GoodsCancellation::class, ['sale_point_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoices()
    {
        return $this->hasMany(Invoice::class, ['sale_point_id' => 'id']);
    }

    /**
     * @return |null
     */
    public function getIcon()
    {
        if ($this->type_id && isset(SalePointType::$icon[$this->type_id]))
            return SalePointType::$icon[$this->type_id];

        return null;
    }

    /**
     * @param bool $strictByEmployee
     * @return array
     */
    public static function getSelect2Data($strictByEmployee = false)
    {
        $employee = Yii::$app->user->identity;

        if ($strictByEmployee) {
            /** @var SalePoint $employeeSalePoint */
            $employeeSalePoint = SalePoint::find()
                ->joinWith('salePointEmployers')
                ->where(['sale_point_employee.employee_id' => $employee->id])
                ->andWhere(['sale_point.company_id' => $employee->company->id])
                ->one();

            if ($employeeSalePoint) {
                return [
                    $employeeSalePoint->id => $employeeSalePoint->name
                ];
            }
        }

        return [0 => 'Без точки продаж'] + SalePoint::find()
            ->where(['company_id' => $employee->company->id])
            ->select(['name'])
            ->indexBy('id')
            ->column();
    }

    /**
     * @return bool
     */
    public function hasMovement()
    {
        return
            $this->getAcquiringOperations()->exists() ||
            $this->getCardOperations()->exists() ||
            $this->getCashBankFlows()->exists() ||
            $this->getCashBankForeignCurrencyFlows()->exists() ||
            $this->getCashEmoneyFlows()->exists() ||
            $this->getCashEmoneyForeignCurrencyFlows()->exists() ||
            $this->getCashOrderFlows()->exists() ||
            $this->getCashOrderForeignCurrencyFlows()->exists() ||
            $this->getGoodsCancellations()->exists() ||
            $this->getInvoices()->exists();
    }

    /**
     * @return bool
     */
    public function isCashboxRequired()
    {
        return in_array($this->type_id, [
            self::TYPE_SHOP,
            self::TYPE_RESTAURANT
        ]);
    }

    // LOG

    /**
     * Get formatted log message from Log model
     * @param Log $log
     * @return string
     */
    public function getLogMessage(Log $log)
    {
        $text = 'Направление ' . $this->name;

        $link = \common\components\helpers\Html::a($text, [
            '/analytics/detailing/view-sale-point',
            'id' => $this->id,
        ]);

        switch ($log->log_event_id) {
            case LogEvent::LOG_EVENT_CREATE:
                return $link . '<span class="pl-1">была создана.</span>';
            case LogEvent::LOG_EVENT_DELETE:
                return $text . '<span class="pl-1">была удалена.</span>';
            case LogEvent::LOG_EVENT_UPDATE:
                return $link . '<span class="pl-1">была изменена.</span>';
            case LogEvent::LOG_EVENT_UPDATE_STATUS:
                $status = $log->getModelAttributeNew('status');
                $status_name = self::STATUS_ACTIVE == $status ? 'В работе' : 'Закрыта';

                return $link . ' статус "' . $status_name . '"';
            default:
                return $log->message;
        }
    }

    public function getLogIcon(Log $log)
    {
        return ['label-info', 'fa fa-file-text-o'];
    }
}
