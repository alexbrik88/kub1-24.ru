<?php

namespace common\models\companyStructure;

use common\components\helpers\ArrayHelper;
use Yii;
use common\models\Company;

/**
 * This is the model class for table "online_payment_type".
 *
 * @property int $id
 * @property int $company_id
 * @property string $name
 * @property int $sort
 *
 * @property CompanyToOnlinePaymentType[] $companyToOnlinePaymentTypes
 * @property Company[] $companies
 * @property Company $company
 */
class OnlinePaymentType extends \yii\db\ActiveRecord
{
    public $saved;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'online_payment_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'sort'], 'integer'],
            [['name'], 'required'],
            [['site'], 'required', 'when' => function(OnlinePaymentType $model) {
                return $model->company_id ? true : false;
            }],
            [['name', 'site'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [
                ['name'], 'unique',
                'filter' => [
                    'and',
                    [
                        'or',
                        ['company_id' => null],
                        ['company_id' => $this->company_id],
                    ]
                ]
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'name' => 'Название',
            'site' => 'Сайт сервиса',
            'sort' => 'Sort',
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $this->saved = true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyToOnlinePaymentTypes()
    {
        return $this->hasMany(CompanyToOnlinePaymentType::className(), ['payment_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Company::className(), ['id' => 'company_id'])->viaTable('company_to_online_payment_type', ['payment_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    // temp
    public function getLogo()
    {
        $icons = [
            1 => [
                'title' => 'Монета',
                'logo' => '/img/icons/moneta_ru.png',
            ],
            4 => [
                'title' => 'Робокасса',
                'logo' => '/img/icons/robokassa.png',
            ],
            5 => [
                'title' => 'ЮKassa',
                'logo' => '/img/yookassa/logo.png',
            ],
        ];

        return ArrayHelper::getValue(ArrayHelper::getValue($icons, $this->id), 'logo');
    }
}
