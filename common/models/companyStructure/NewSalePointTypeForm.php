<?php
namespace common\models\companyStructure;

use common\components\validators\PhoneValidator;
use common\models\employee\Employee;
use common\models\service\RewardRequest;
use yii\base\Model;
use yii\db\Connection;

/**
 * Class NewSalePointTypeForm
 */
class NewSalePointTypeForm extends Model
{
    /**
     * @var
     */
    public $description;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['description'], 'required'],
            [['description'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'description' => 'Описание',
        ];
    }
}