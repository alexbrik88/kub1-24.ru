<?php

namespace common\models\companyStructure;

use Yii;
use common\models\Company;

/**
 * This is the model class for table "accounting_system_type".
 *
 * @property int $id
 * @property int $company_id
 * @property string $name
 * @property int $sort
 *
 * @property Company $company
 * @property CompanyToAccountingSystemType[] $companyToAccountingSystemTypes
 * @property Company[] $companies
 */
class AccountingSystemType extends \yii\db\ActiveRecord
{
    public $saved;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'accounting_system_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'sort'], 'integer'],
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [
                ['name'], 'unique',
                'filter' => [
                    'and',
                    [
                        'or',
                        ['company_id' => null],
                        ['company_id' => $this->company_id],
                    ]
                ]
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'name' => 'Название',
            'sort' => 'Sort',
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $this->saved = true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyToAccountingSystemTypes()
    {
        return $this->hasMany(CompanyToAccountingSystemType::className(), ['accounting_system_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Company::className(), ['id' => 'company_id'])->viaTable('company_to_accounting_system_type', ['accounting_system_id' => 'id']);
    }
}
