<?php

namespace common\models\currency;

use php_rutils\RUtils;
use Yii;

/**
 * This is the model class for table "currency".
 *
 * @property string $id
 * @property string $name
 * @property string $label
 * @property boolean $is_default
 * @property integer $amount
 * @property string $old_value
 * @property string $current_value
 * @property string $update_date
 */
class Currency extends \yii\db\ActiveRecord
{
    const DEFAULT_ID = '643';
    const DEFAULT_USD = '840';
    const DEFAULT_FOREIGN_CURRENCY = 'USD';
    const DEFAULT_NAME = 'RUB';
    const DEFAULT_AMOUNT = 1;
    const DEFAULT_RATE = 1;
    const RATE_SERTAIN_DATE = 'certain_date';
    const RATE_PAYMENT_DATE = 'payment_date';
    const RATE_CUSTOM = 'custom_rate';
    const DEFAULT_RATE_TYPE = self::RATE_SERTAIN_DATE;

    public static $currencySymbols = [
        'EUR' => '€',
        'RUB' => '₽',
        'USD' => '$',
        'BYN' => 'Br',
        'KZT' => '₸',
        'UAH' => '₴',
        'GBP' => '£',
        'CNY' => '¥',
         null => '₽',
    ];

    public static $currencySymbolsById = [
        '978' => '€',
        '643' => '₽',
        '840' => '$',
        '933' => 'Br',
        '398' => '₸',
        '980' => '₴',
        '826' => '£',
        '156' => '¥',
        null => '₽',
    ];

    public static $currencyList = [
        '643' => 'RUB',
        '933' => 'BYN',
        '840' => 'USD',
        '978' => 'EUR',
        '398' => 'KZT',
        '980' => 'UAH',
        '826' => 'GBP',
        '156' => 'CNY',
    ];

    public static $currencyNameById = [
        '978' => 'EUR',
        '643' => 'RUB',
        '840' => 'USD',
        '933' => 'BYN',
        '398' => 'KZT',
        '980' => 'UAH',
        '826' => 'GBP',
        '156' => 'CNY',
        null => 'RUB',
    ];

    public static $rateTypeList = [
        self::RATE_SERTAIN_DATE,
        self::RATE_PAYMENT_DATE,
        self::RATE_CUSTOM,
    ];

    public static $nameArray = [
        'RUB',
        'USD',
        'EUR',
        'BYN',
        'UAH',
        'KZT',
        'GBP',
        'CNY',
    ];

    public static $foreignArray = [
        'USD',
        'EUR',
        'BYN',
        'UAH',
        'KZT',
        'GBP',
        'CNY',
    ];

    public static $format = [
        'RUB' => [
            'integer' => [
                'gender' => RUtils::MALE,
                'vars' => ['рубль', 'рубля', 'рублей'],
            ],
            'fraction' => [
                'gender' => RUtils::FEMALE,
                'vars' => ['копейка', 'копейки', 'копеек'],
            ],
        ],
        'USD' => [
            'integer' => [
                'gender' => RUtils::MALE,
                'vars' => ['доллар', 'доллара', 'долларов'],
            ],
            'fraction' => [
                'gender' => RUtils::MALE,
                'vars' => ['цент', 'цента', 'центов'],
            ],
        ],
        'EUR' => [
            'integer' => [
                'gender' => RUtils::MALE,
                'vars' => ['евро', 'евро', 'евро'],
            ],
            'fraction' => [
                'gender' => RUtils::FEMALE,
                'vars' => ['евроцент', 'евроцента', 'евроцентов'],
            ],
        ],
        'BYN' => [
            'integer' => [
                'gender' => RUtils::MALE,
                'vars' => ['рубль', 'рубля', 'рублей'],
            ],
            'fraction' => [
                'gender' => RUtils::FEMALE,
                'vars' => ['копейка', 'копейки', 'копеек'],
            ],
        ],
        'KZT' => [
            'integer' => [
                'gender' => RUtils::MALE,
                'vars' => ['тенге', 'тенге', 'тенге'],
            ],
            'fraction' => [
                'gender' => RUtils::MALE,
                'vars' => ['тиын', 'тиына', 'тиынов'],
            ],
        ],
        'UAH' => [
            'integer' => [
                'gender' => RUtils::FEMALE,
                'vars' => ['гривна', 'гривны', 'гривен'],
            ],
            'fraction' => [
                'gender' => RUtils::FEMALE,
                'vars' => ['копейка', 'копейки', 'копеек'],
            ],
        ],
        'GBP' => [
            'integer' => [
                'gender' => RUtils::MALE,
                'vars' => ['фунт стерлингов', 'фунта стерлингов', 'фунтов стерлингов'],
            ],
            'fraction' => [
                'gender' => RUtils::MALE,
                'vars' => ['пенни', 'пенса', 'пенсов'],
            ],
        ],
        'CNY' => [
            'integer' => [
                'gender' => RUtils::MALE,
                'vars' => ['юань', 'юаня', 'юаней'],
            ],
            'fraction' => [
                'gender' => RUtils::MALE,
                'vars' => ['фынь', 'фы́ня', 'фыней'],
            ],
        ],
    ];

    private static $_getAllCurrency;

    /**
     * URL for currency rate request
     * @var string
     */
    public static $url = 'http://www.cbr.ru/scripts/XML_daily.asp';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'currency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'old_value' => 'Old Value',
            'current_value' => 'Current Value',
            'update_date' => 'Update Date',
        ];
    }

    public static function getAllCurrency()
    {
        if (self::$_getAllCurrency === null) {
            self::$_getAllCurrency = Currency::find()->select([
                'name',
                'id',
            ])->indexBy('id')->column();
        }

        return self::$_getAllCurrency;
    }

    public static function name($id, $default = null)
    {
        return self::getAllCurrency()[$id] ?? $default;
    }

    public static function symbolById($id, $default = null)
    {
        return self::$currencySymbolsById[$id] ?? $default;
    }

    public function getSymbol()
    {
        return self::symbolById($this->id);
    }

    /**
     * @param  array|string $name currency code name
     * @param  DateTime $date
     * @return array
     */
    public static function getCurrencyRate($name = null, \DateTime $date = null)
    {
        $url = static::$url . ($date !== null ? "?date_req={$date->format('d/m/Y')}" : '');
        $currName = $name ? (array) $name : static::find()->select('name')->where(['is_default' => false])->column();
        $reader = new \XMLReader();

        $reader->open($url);
        $output = [];
        $name = '';

        while ($reader->read()) {
            if ($reader->nodeType == \XMLReader::ELEMENT) {
                if ($reader->localName == 'CharCode') {
                    $reader->read();
                    $name = $reader->value;
                }
                if (in_array($name, $currName) && $reader->localName == 'Nominal') {
                    $reader->read();
                    $output[$name]['amount'] = str_replace(',', '.', $reader->value);
                }
                if (in_array($name, $currName) && $reader->localName == 'Value') {
                    $reader->read();
                    $output[$name]['value'] = str_replace(',', '.', $reader->value);
                }
            }
        }

        return $output;
    }

    /**
     * @param  array|string $name currency code name
     * @param  DateTime $date
     * @return array
     */
    public static function textPrice($amount, $name)
    {
        if (!isset(self::$format[$name])) {
            return $amount;
        }

        $words = [];

        $amount = bcmul($amount, 1, 2);
        $ruble = floor($amount);
        $kopeck = bcmul(bcsub($amount, $ruble, 2), 100, 0);

        $words[] = RUtils::numeral()->sumString(
            $ruble,
            self::$format[$name]['integer']['gender'],
            self::$format[$name]['integer']['vars']
        );

        $words[] = sprintf('%02d', $kopeck);

        $words[] = RUtils::numeral()->choosePlural($kopeck, self::$format[$name]['fraction']['vars']);

        return join(' ', $words);
    }
}
