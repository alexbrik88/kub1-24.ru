<?php

namespace common\models\currency;

use common\components\curl\Curl;
use Yii;

/**
 * This is the model class for table "currency_rate".
 *
 * @property string $date
 * @property string $name
 * @property integer $amount
 * @property string $value
 */
class CurrencyRate extends \yii\db\ActiveRecord
{
    /**
     * URL for currency rate request
     * @var string
     */
    public static $url = 'http://www.cbr.ru/scripts/XML_daily.asp';

    private static $rateOnDate = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'currency_rate';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['date', 'name', 'amount', 'value'], 'required'],
            [['date'], 'date'],
            [['name'], 'string', 'max' => 3],
            [['amount'], 'integer'],
            [['value'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'date' => 'Дата',
            'name' => 'Код валюты',
            'amount' => 'Номинал в рублях',
            'value' => 'Курс',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function getCurrencyRate($name, \DateTime $date = null)
    {
        $data = self::getRateOnDate($date);

        return ($name && isset($data[$name])) ? $data[$name] : [];
    }

    /**
     * @inheritdoc
     */
    public static function getRateOnDate(\DateTime $date = null)
    {
        if ($date === null || $date->getTimestamp() > time()) {
            $date = new \DateTime();
        }
        $date->modify('today');
        $dateString = $date->format('Y-m-d');

        if (!isset(self::$rateOnDate[$dateString])) {
            $query = self::find()->where([
                'between',
                'date',
                $date->format('Y-01-01'),
                $date->format('Y-12-31'),
            ])->asArray();

            foreach ($query->all() as $key => $row) {
                 self::$rateOnDate[$row['date']][$row['name']] = $row;
            }

            if (empty(self::$rateOnDate[$dateString])) {
                $data = self::rateRequest($date);
                self::importRate($data, $date);
                $query = self::find()->where([
                    'date' => $dateString,
                ])->asArray();
                foreach ($query->all() as $key => $row) {
                    self::$rateOnDate[$dateString][$row['name']] = $row;
                }
            }
        }

        return self::$rateOnDate[$dateString] ?? null;
    }

    /**
     * @return array
     */
    public static function importRate($data, \DateTime $date = null)
    {
        $currentDate = $date ? : date_create('today');
        $currentDateString = $currentDate->format('Y-m-d');

        if (isset($data['@attributes']['Date'], $data['Valute']) && is_array($data['Valute'])) {
            $rateDate = date_create_from_format('d.m.Y|', $data['@attributes']['Date']);

            if ($rateDate) {
                $dateString = ($rateDate < $currentDate) ? $currentDate->format('Y-m-d') : $rateDate->format('Y-m-d');
                $rows = [];
                foreach ($data['Valute'] as $item) {
                    if (in_array($item['CharCode'], Currency::$nameArray)) {
                        $rows[] = [
                            $dateString,
                            $item['CharCode'],
                            $item['Nominal'],
                            str_replace(',', '.', $item['Value']),
                        ];
                    }
                }

                if (!empty($rows)) {
                    $db = Yii::$app->db;
                    $sql = $db->queryBuilder->batchInsert(CurrencyRate::tableName(), [
                        'date',
                        'name',
                        'amount',
                        'value',
                    ], $rows);
                    $execute = $db->createCommand($sql . '
                        ON DUPLICATE KEY UPDATE
                            [[amount]] = VALUES([[amount]]),
                            [[value]] = VALUES([[value]])
                    ')->execute();
                }
            }
        }
    }

    /**
     * @return array
     */
    public static function rateRequest(\DateTime $date = null)
    {
        $data = [];
        $url = self::$url . ($date ? "?date_req={$date->format('d.m.Y')}" : null);

        $curl = new Curl();
        $response = $curl->get($url);

        if (!$curl->errorCode) {
            $xml = simplexml_load_string($response);
            $json = json_encode($xml);
            $data = json_decode($json, true);
        }

        return $data;
    }
}
