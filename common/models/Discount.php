<?php

namespace common\models;

use common\models\service\SubscribeTariff;
use DateTime;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "discount".
 *
 * @property integer $id
 * @property integer $company_id
 * @property string $tariff_id
 * @property string $value
 * @property boolean $is_known
 * @property boolean $is_active
 * @property integer $active_from
 * @property integer $active_to
 *
 * @property Company $company
 */
class Discount extends \yii\db\ActiveRecord
{
    const TARIFF_ID = [1, 2, 3];
    const TYPE_4_TARIFF_ID = 12;

    const TYPE_1 = 20;
    const TYPE_2 = 20;
    const TYPE_3 = 20;
    const TYPE_4 = 50;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'discount';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'active_to', 'active_from'], 'integer'],
            [['tariff_id'], 'string'],
            [['value', 'active_to', 'active_from'], 'required'],
            [['value'], 'number'],
            [
                ['company_id'], 'exist', 'skipOnError' => true,
                'targetClass' => Company::className(),
                'targetAttribute' => ['company_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'tariff_id' => 'Tariff ID',
            'value' => 'Value',
            'active_to' => 'Valid To',
            'active_from' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();
        $this->value *= 1;
    }

    /**
     * @param array $idArray
     */
    public function setTariffId(array $idArray)
    {
        $this->tariff_id = implode(',', array_intersect(SubscribeTariff::paidActualIds(), $idArray));
    }

    /**
     * @return array
     */
    public function getTariffId()
    {
        return array_map('intval', explode(',', $this->tariff_id));
    }

    /**
     * @param  Company  $company
     * @param  DateTime $date
     * @return boolean
     */
    public static function createType1(Company $company, DateTime $date)
    {
        $discount = new Discount([
            'type_id' => DiscountType::TYPE_1,
            'company_id' => $company->id,
            'value' => self::TYPE_1,
            'tariff_id' => '1',
            'active_from' => $date->getTimestamp(),
            'active_to' => $date->modify('+5 days')->getTimestamp(),
        ]);

        return $discount->save();
    }

    /**
     * @param  Company  $company
     * @param  integer $expired_at [common\models\service\Subscribe::expired_at]
     * @return boolean
     */
    public static function createType2(Company $company, $expired_at)
    {
        $discount = new Discount([
            'type_id' => DiscountType::TYPE_2,
            'company_id' => $company->id,
            'value' => self::TYPE_2,
            'tariff_id' => '1',
            'active_from' => $expired_at - (3600 * 24 * 2),
            'active_to' => $expired_at,
        ]);

        return $discount->save();
    }

    /**
     * @param  Company  $company
     * @param  DateTime $dateFrom
     * @param  DateTime $dateTill
     * @return boolean
     */
    public static function createType3(Company $company, DateTime $dateFrom, DateTime $dateTill)
    {
        $discount = new Discount([
            'type_id' => DiscountType::TYPE_3,
            'company_id' => $company->id,
            'value' => self::TYPE_3,
            'tariff_id' => '2,3',
            'active_from' => $dateFrom->getTimestamp(),
            'active_to' => $dateTill->getTimestamp(),
        ]);

        return $discount->save();
    }

    /**
     * @param  Company  $company
     * @param  DateTime $dateFrom
     * @param  DateTime $dateTill
     * @return boolean
     */
    public static function createType4(Company $company, DateTime $regDate)
    {
        $query = Discount::find()->andWhere([
            'type_id' => DiscountType::TYPE_4,
            'company_id' => $company->id,
        ]);
        if (!$query->exists()) {
            $dateFrom = $regDate->modify('first day of january this year');
            $dateTill = (clone $regDate)->modify('last day of april next year');
            $discount = new Discount([
                'type_id' => DiscountType::TYPE_4,
                'company_id' => $company->id,
                'value' => self::TYPE_4,
                'tariff_id' => strval(self::TYPE_4_TARIFF_ID),
                'active_from' => $dateFrom->getTimestamp(),
                'active_to' => $dateTill->getTimestamp(),
                'is_one_time' => true,
            ]);

            if ($discount->save()) {
                return true;
            } else {
                \common\components\helpers\ModelHelper::logErrors($discount, __METHOD__);
            }
        }

        return false;
    }
}
