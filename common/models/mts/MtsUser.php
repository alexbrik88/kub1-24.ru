<?php

namespace common\models\mts;

use common\components\DadataClient;
use common\models\Company;
use common\models\company\CompanyNotification;
use common\models\company\CompanyTaxationType;
use common\models\company\CompanyType;
use common\models\company\RegistrationPageType;
use common\models\Contractor;
use common\models\EmployeeCompany;
use common\models\employee\Employee;
use frontend\models\RegistrationForm;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "mts_user".
 *
 * @property string $id
 * @property integer $employee_id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $mobile_phone
 * @property string $groups
 * @property integer $created_at
 *
 * @property Employee $employee
 */
class MtsUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\behaviors\TimestampBehavior',
                'updatedAtAttribute' => false,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mts_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'employee_id' => 'Employee ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'mobile_phone' => 'Mobile Phone',
            'groups' => 'Groups',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id'])->onCondition([
            'is_deleted' => false,
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMtsSubscribes()
    {
        return $this->hasMany(MtsSubscribe::className(), ['mts_user_id' => 'id'])->andOnCondition([
            'is_applied' => 0,
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhoneStr()
    {
        return '+7('.substr($this->phone,1,3).') '.substr($this->phone,4,3).'-'.substr($this->phone,7,2).'-'.substr($this->phone,9);
    }

    /**
     * @return Employee
     */
    public function createRegisteredEmployee()
    {
        if ($this->employee === null && Employee::findIdentityByLogin($this->email) === null) {
            $inn = ArrayHelper::getValue($this->mtsSubscribes, '0.paymentData.parameters.purchaseINN');
            $attributes = $inn ? DadataClient::getCompanyAttributes($inn) : [];
            //echo var_dump($attributes); die();
            $transaction = Yii::$app->db->beginTransaction();
            try {
                $employee = RegistrationForm::getNewEmployee([
                    'firstname' => $this->firstname,
                    'lastname' => $this->lastname,
                    'email' => $this->email,
                    'phone' => $this->phoneStr,
                    'is_mts_user' => 1,
                ]);
                if ($employee->save(false)) {
                    $company = RegistrationForm::getNewCompany(array_merge([
                        'owner_employee_id' => $employee->id,
                        'created_by' => $employee->id,
                        'company_type_id' => CompanyType::TYPE_EMPTY,
                        'email' => $this->email,
                        'phone' => $this->phoneStr,
                        'registration_page_type_id' => RegistrationPageType::PAGE_TYPE_MTS,
                        'name_full' => '(нет названия)',
                        'name_short' => '(нет названия)',
                    ], $attributes));
                    $companyTaxationType = new CompanyTaxationType([
                        'osno' => false,
                        'usn' => false,
                        'envd' => false,
                        'psn' => false,
                    ]);
                    $company->populateRelation('companyTaxationType', $companyTaxationType);
                    if ($company->save(false)) {
                        $company->updateAttributes(['main_id' => $company->id]);
                        $companyTaxationType->company_id = $company->id;
                        if ($companyTaxationType->save(false) &&
                            $company->createTrialSubscribe() &&
                            Contractor::createFounder($company)
                        ) {
                            $employeeCompany = new EmployeeCompany([
                                'company_id' => $company->id,
                            ]);
                            $employeeCompany->employee = $employee;
                            if ($employeeCompany->save(false)) {
                                $employee->updateAttributes([
                                    'company_id' => $company->id,
                                    'main_company_id' => $company->id,
                                ]);
                                $this->updateAttributes([
                                    'employee_id' => $employee->id,
                                ]);
                                if (!$company->strict_mode) {
                                    foreach ($this->mtsSubscribes as $mtsSubscribe) {
                                        if ($mtsSubscribe->company_number == 1 && $mtsSubscribe->purchase_inn == $company->inn) {
                                            $mtsSubscribe->createPayment($company, [$company->id]);
                                        }
                                    }
                                }
                                $transaction->commit();

                                return $employee;
                            }
                        } else {
                            \common\components\helpers\ModelHelper::logErrors($companyTaxationType, __METHOD__);
                        }
                    } else {
                        \common\components\helpers\ModelHelper::logErrors($company, __METHOD__);
                    }
                } else {
                    \common\components\helpers\ModelHelper::logErrors($employee, __METHOD__);
                }

            } catch (\Exception $e) {
                \Yii::error(__METHOD__."\n".\yii\helpers\VarDumper::dumpAsString($e), 'validation');
            } catch (\Throwable $e) {
                \Yii::error(__METHOD__."\n".\yii\helpers\VarDumper::dumpAsString($e), 'validation');
            }
            $transaction->rollBack();
        }

        return null;
    }
}
