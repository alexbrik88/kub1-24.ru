<?php

namespace common\models\mts;

use common\models\Company;
use common\models\service\Payment;
use common\models\service\PaymentOrder;
use common\models\service\PaymentType;
use common\models\service\SubscribeHelper;
use common\models\service\SubscribeTariff;
use frontend\modules\subscribe\forms\PaymentForm;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "mts_subscribe".
 *
 * @property integer $id
 * @property integer $status
 * @property string $mts_user_id
 * @property integer $tariff_id
 * @property integer $is_applied
 * @property integer $company_number
 * @property integer $payment_id
 * @property string $purchase_inn
 * @property string $price_code
 * @property string $subscription_id
 * @property string $data
 * @property string $request_id
 * @property integer $created_at
 * @property integer $updated_at
 */
class MtsSubscribe extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 'active';
    const STATUS_ERROR = 'error';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\behaviors\TimestampBehavior',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mts_subscribe';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_number'], 'required'],
            [['company_number'], 'integer', 'min' => 1],
            [
                'tariff_id', 'exist',
                'targetClass' => 'common\models\service\SubscribeTariff',
                'targetAttribute' => 'id',
                'filter' => [
                    'and',
                    ['is_active' => 1],
                    ['>', 'price', 0],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status' => 'MTS Status',
            'is_applied' => 'Is Applied',
            'company_number' => 'Количество компаний',
            'mts_user_id' => 'Mts User ID',
            'tariff_id' => 'Тариф',
            'price_code' => 'Price Code',
            'subscription_id' => 'ID МТС Подписки',
            'data' => 'Data',
            'request_id' => 'Request ID',
            'created_at' => 'Дата оплаты',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMtsUser()
    {
        return $this->hasOne(MtsUser::className(), ['id' => 'mts_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(SubscribeTariff::className(), ['id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayment()
    {
        return $this->hasOne(Payment::className(), ['id' => 'payment_id']);
    }

    /**
     * @inheritdoc
     */
    public function createPayment(Company $company, $companyList)
    {
        if ($this->payment !== null) {
            throw new \Exception("The Payment already exists");
        }
        if ($this->tariff === null) {
            throw new \Exception("The Tariff not found");
        }
        if ($company->strict_mode) {
            throw new \Exception("The Company is in strict mode");
        }

        $form = new PaymentForm($company, [
            'companyList' => $companyList,
            'tariffId' => $this->tariff_id,
            'paymentTypeId' => PaymentType::TYPE_ONLINE,
            'createInvoice' => false,
        ]);
        $payment = $form->makePayment() ? $form->payment : null;

        if ($payment !== null) {
            $payment->paid($this->created_at);
            $this->populateRelation('payment', $payment);
            $this->updateAttributes([
                'is_applied' => 1,
                'payment_id' => $payment->id,
            ]);

            return true;
        }

        return false;
    }

    /**
     * @return array|null
     */
    public function getPaymentData()
    {
        return \yii\helpers\Json::decode($this->data);
    }
}
