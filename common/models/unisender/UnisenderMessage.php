<?php

namespace common\models\unisender;

use common\models\Company;
use common\models\employee\Employee;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "unisender_message".
 *
 * @property int $id
 * @property int $company_id
 * @property int $employee_id
 * @property int $created_at
 * @property int $updated_at
 * @property int $unisender_id
 * @property int $start_date
 * @property string $status
 * @property int $message_id
 * @property int $list_id
 * @property string $subject
 * @property string $sender_name
 * @property string $sender_email
 * @property int $total_contacts_count
 * @property int $sent_messages_count
 * @property int $delivered_messages_count
 * @property int $read_unique_count
 * @property int $read_count
 * @property int $clicked_unique_count
 * @property int $clicked_count
 * @property int $unsubscribed_contacts_count
 * @property int $spam_contacts_count
 *
 * @property Company $company
 * @property Employee $employee
 */
class UnisenderMessage extends \yii\db\ActiveRecord
{
    const STATUSES_MAP = [
        'waits_censor' => 'рассылка ожидает проверки',
        'censor_hold' => 'рассмотрена администратором, но отложена для дальнейшей проверки',
        'declined' => 'рассылка отклонена администратором',
        'waits_schedule' => 'задание на постановку рассылки в очередь получено и рассылка ждёт постановки в очередь',
        'scheduled' => 'рассылка запланирована к запуску. Как только настанет время отправки, она будет запущена',
        'in_progress' => 'рассылка выполняется',
        'analysed' => 'все сообщения отправлены, идёт анализ результатов',
        'completed' => 'все сообщения отправлены и анализ результатов закончен',
        'stopped' => 'рассылка поставлена «на паузу»',
        'canceled' => 'рассылка отменена',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'unisender_message';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'employee_id', 'unisender_id', 'start_date', 'status', 'message_id', 'list_id', 'subject',
                'sender_name', 'sender_email', 'total_contacts_count', 'sent_messages_count', 'delivered_messages_count',
                'read_unique_count', 'read_count', 'clicked_unique_count', 'clicked_count', 'unsubscribed_contacts_count',
                'spam_contacts_count'], 'required'],
            [['company_id', 'employee_id', 'unisender_id', 'start_date', 'message_id', 'list_id', 'total_contacts_count',
                'sent_messages_count', 'delivered_messages_count', 'read_unique_count', 'read_count', 'clicked_unique_count',
                'clicked_count', 'unsubscribed_contacts_count', 'spam_contacts_count'], 'integer'],
            [['subject'], 'string'],
            [['status', 'sender_name', 'sender_email'], 'string', 'max' => 255],
            [['unisender_id'], 'unique'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['employee_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'employee_id' => 'Employee ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'unisender_id' => 'Unisender ID',
            'start_date' => 'Вермя старта рассылки',
            'status' => 'Статус',
            'message_id' => 'Message ID',
            'list_id' => 'List ID',
            'subject' => 'Название',
            'sender_name' => 'От кого',
            'sender_email' => 'С адреса',
            'total_contacts_count' => 'Количество контактов',
            'sent_messages_count' => 'Количество отправленных сообщений',
            'delivered_messages_count' => 'Количество доставленных сообщений',
            'read_unique_count' => 'Количество уникальных прочтений',
            'read_count' => 'Общее количество прочтений',
            'clicked_unique_count' => 'Количество уникальных переходов',
            'clicked_count' => 'Общее количество переходов',
            'unsubscribed_contacts_count' => 'Количество отписавшихся контактов',
            'spam_contacts_count' => 'Количество контактов, пожаловавшихся на спам',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }
}
