<?php

namespace common\models;

use Yii;
use common\models\cash\CashFactory;
use common\models\document\Invoice;
use common\models\project\Project;
use yii\db\Expression;

/**
 * This is the model class for table "contractor_auto_project".
 *
 * @property int $contractor_id
 * @property int $project_id
 * @property string $project_start
 * @property string $project_end
 * @property int $project_status
 * @property int|null $exists_invoices
 * @property int|null $exists_flows
 * @property int|null $new_invoices
 * @property int|null $new_flows
 *
 * @property Contractor $contractor
 * @property Project $project
 */
class ContractorAutoProject extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contractor_auto_project';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['contractor_id', 'project_id', 'project_start', 'project_end', 'project_status'], 'required'],
            [['contractor_id', 'project_id', 'project_status', 'exists_invoices', 'exists_flows', 'new_invoices', 'new_flows'], 'integer'],
            [['project_start', 'project_end'], 'safe'],
            [['contractor_id', 'project_id'], 'unique', 'targetAttribute' => ['contractor_id', 'project_id']],
            [['contractor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contractor::class, 'targetAttribute' => ['contractor_id' => 'id']],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::class, 'targetAttribute' => ['project_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'contractor_id' => 'Контрагент',
            'project_id' => 'Проект',
            'project_start' => 'Дата начала проекта',
            'project_end' => 'Дата окончания проекта',
            'project_status' => 'Статус проекта',
            'exists_invoices' => 'Привязать уже выставленные счета к проекту',
            'exists_flows' => 'Привязать уже имеющиеся приходы по деньгам к проекту',
            'new_invoices' => 'У всех новых счетов по умолчанию проставлять этот проект',
            'new_flows' => 'Все новые приходы привязывать к данному проекту',
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        $contractorId = $this->contractor_id;
        $dateFrom = $this->project_start;
        $dateTo = $this->project_end;

        if ($this->exists_invoices || array_key_exists('exists_invoices', $changedAttributes)) {
            $projectId = ($this->exists_invoices) ? $this->project_id : null;
            self::updateInvoicesProject($projectId, $contractorId, $dateFrom, $dateTo);
        }
        if ($this->exists_flows || array_key_exists('exists_flows', $changedAttributes)) {
            $projectId = ($this->exists_flows) ? $this->project_id : null;
            self::updateFlowsProject($projectId, $contractorId, $dateFrom, $dateTo);
        }

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContractor()
    {
        return $this->hasOne(Contractor::class, ['id' => 'contractor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::class, ['id' => 'project_id']);
    }

    public static function updateInvoicesProject($projectId, $contractorId, $dateFrom, $dateTo)
    {
        $companyId = Yii::$app->user->identity->company->id;

        Invoice::updateAll(['project_id' => $projectId], ['and',
            ['company_id' => $companyId],
            ['contractor_id' => $contractorId], // int
            ['between', 'document_date', $dateFrom, $dateTo],
        ]);
    }

    public static function updateFlowsProject($projectId, $contractorId, $dateFrom, $dateTo)
    {
        $companyId = Yii::$app->user->identity->company->id;

        foreach (CashFactory::getCashClasses() as $cashClass) {
            $cashClass::updateAll(['project_id' => $projectId], ['and',
                ['company_id' => $companyId],
                ['contractor_id' => new Expression("'{$contractorId}'")], // char!
                ['between', 'date', $dateFrom, $dateTo],
            ]);
        }
    }
}
