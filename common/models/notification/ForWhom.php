<?php
/**
 * Created by PhpStorm.
 * User: valik
 * Date: 27.01.2016
 * Time: 10:59
 */

namespace common\models\notification;

use common\models\Company;
use common\models\company\CompanyTaxationType;
use common\models\company\CompanyType;
use common\models\document\status\DocumentStatusBase;
use common\models\TaxationType;
use Yii;

/**
 * This is the model class for table "for_whom".
 *
 * @property integer $id
 * @property string $name
 */
class ForWhom extends DocumentStatusBase
{
    /**
     *
     */
    const NOTIFICATION_FOR_ALL = 1;
    /**
     *
     */
    const NOTIFICATION_FOR_OSNO = 2;
    /**
     *
     */
    const NOTIFICATION_FOR_USN_ENVD = 3;
    /**
     *
     */
    const NOTIFICATION_FOR_IP = 4;
    /**
     *
     */
    const NOTIFICATION_FOR_SELF_EMPLOYED = 5;

    /**
     * @var array
     */
    public static $notificationForWhom = [
        self::NOTIFICATION_FOR_ALL => 'Всем',
        self::NOTIFICATION_FOR_OSNO => 'Только ОСНО',
        self::NOTIFICATION_FOR_USN_ENVD => 'Только УСН, ЕНВД',
        self::NOTIFICATION_FOR_IP => 'Только для ИП',
        self::NOTIFICATION_FOR_SELF_EMPLOYED => 'Только для самозанятых',
    ];

    /**
     * @var array
     */
    public static $notificationForWhomToTaxationType = [
        self::NOTIFICATION_FOR_ALL => ['envd', 'osno', 'usn'],
        self::NOTIFICATION_FOR_OSNO => ['osno'],
        self::NOTIFICATION_FOR_USN_ENVD => ['envd', 'usn'],
    ];

    /**
     * @param Company $company
     * @return array
     */
    public static function getForWhomByTaxationType(Company $company)
    {
        $forWhomArray = [];
        foreach (static::$notificationForWhomToTaxationType as $forWhom => $taxationTypeArray) {
            foreach ($taxationTypeArray as $taxationTypeAttribute) {
                if ($company->companyTaxationType->$taxationTypeAttribute) {
                    $forWhomArray[] = $forWhom;
                    continue 2;
                }
            }
        }
        if ($company->company_type_id == CompanyType::TYPE_IP) {
            $forWhomArray[] = self::NOTIFICATION_FOR_IP;
        }
        if ($company->self_employed) {
            $forWhomArray[] = self::NOTIFICATION_FOR_SELF_EMPLOYED;
        }

        return $forWhomArray;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'for_whom';
    }
}