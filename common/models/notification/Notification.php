<?php

namespace common\models\notification;

use common\components\date\DateHelper;
use common\components\date\DatePickerFormatBehavior;
use common\models\Company;
use common\models\company\CompanyNotification;
use common\tasks\CreateCompanyNotificationFromNotification;
use frontend\models\NotificationQuery;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\Connection;

/**
 * This is the model class for table "notification".
 *
 * @property integer $id
 * @property integer $notification_type
 * @property string $event_date
 * @property string $activation_date
 * @property string $title
 * @property string $text
 * @property integer $for_whom
 * @property string $created_at
 * @property string $updated_at
 * @property string $fine
 * @property boolean $status
 *
 * @property CompanyNotification $companyNotification
 */
class Notification extends \yii\db\ActiveRecord
{
    /**
     *
     */
    const NOTIFICATION_TYPE_TAX = 1;
    /**
     *
     */
    const NOTIFICATION_TYPE_SYSTEM = 0;
    /**
     *
     */
    const STATUS_NOT_PASSED = 0;
    /**
     *
     */
    const STATUS_PASSED = 1;

    /**
     * @var array
     */
    public static $notificationTypeText = [
        self::NOTIFICATION_TYPE_TAX => 'налоговое',
        self::NOTIFICATION_TYPE_SYSTEM => 'системное',
    ];

    /**
     * @var array
     */
    public static $monthHelper = [
        1 => 'Январе',
        2 => 'Феврале',
        3 => 'Марте',
        4 => 'Апреле',
        5 => 'Мае',
        6 => 'Июне',
        7 => 'Июле',
        8 => 'Августе',
        9 => 'Сентябре',
        10 => 'Октябре',
        11 => 'Ноябре',
        12 => 'Декабре',
    ];

    /**
     * @return NotificationQuery
     */
    public static function find()
    {
        return new NotificationQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'notification';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => DatePickerFormatBehavior::className(),
                'attributes' => [
                    'event_date', 'activation_date',
                ],
            ],
            'class' => TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['notification_type', 'event_date', 'activation_date', 'for_whom'], 'required'],
            [['notification_type', 'for_whom'], 'integer'],
            [['event_date', 'activation_date'], 'compare', 'operator' => '>=', 'compareValue' => date(DateHelper::FORMAT_DATE),
                'message' => 'Дата должна быть больше или равна ' . date(DateHelper::FORMAT_USER_DATE),
                'whenClient' => 'function(){}',
            ],
            ['event_date', 'compare', 'operator' => '>=', 'compareAttribute' => 'activation_date',
                'whenClient' => 'function(){}',
            ],
            [['text', 'title', 'fine'], 'string'],
            [['status'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'notification_type' => 'Тип уведомления',
            'event_date' => 'Дата события',
            'activation_date' => 'Дата активизации',
            'text' => 'Текст',
            'for_whom' => 'Для кого',
            'title' => 'Заголовок',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
            'fine' => 'Штраф',
        ];
    }

    /**
     * @return $this
     */
    public function getCompanyNotification()
    {
        return $this->hasOne(CompanyNotification::className(), ['notification_id' => 'id'])
            ->onCondition(['company_id' => Yii::$app->user->identity->company->id]);
    }

    /**
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     */
    public function _save()
    {
        if (!$this->save()) {
            return false;
        }

        Yii::$app->queue->push(CreateCompanyNotificationFromNotification::create($this->id));

        return true;

    }

    /**
     * @return false|int
     * @throws \Exception
     * @throws \Throwable
     */
    public function _delete()
    {
        /* @var $companyNotification CompanyNotification */
        foreach (CompanyNotification::find()->andWhere(['notification_id' => $this->id])->all() as $companyNotification) {
            $companyNotification->delete();
        }

        return $this->delete();
    }
}
