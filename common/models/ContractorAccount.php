<?php

namespace common\models;

use common\models\currency\Currency;
use common\models\dictionary\bik\BikDictionary;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "contractor_account".
 *
 * @property integer $id
 * @property integer $contractor_id
 * @property integer $currency_id
 * @property string $rs
 * @property string $bank_name
 * @property string $bank_address
 * @property string $bank_city
 * @property string $bik
 * @property string $ks
 * @property integer $is_main
 * @property integer $created_at
 * @property string $corr_bank_name
 * @property string $corr_bank_address
 * @property string $corr_bank_swift
 * @property integer $is_foreign_bank
 * @property integer $no_mask
 *
 * @property Contractor $contractor
 */
class ContractorAccount extends \yii\db\ActiveRecord
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => 'yii\behaviors\TimestampBehavior',
                'updatedAtAttribute' => false,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'contractor_account';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bank_name', 'bank_city'], 'trim'],
            [['bik', 'rs'], 'required'],
            [['bank_name'], 'required', 'when' => function ($model) {
                return $model->isForeign;
            }],
            [
                [
                    'bik',
                    'rs',
                    'ks',
                    'corr_bank_name',
                    'corr_bank_address',
                    'corr_bank_swift',
                ], 'string', 'when' => function ($model) {
                return $model->isForeign;
            }],
            [['bik'], 'string', 'length' => 9, 'when' => function ($model) {
                return !$model->isForeign;
            }],
            [['bik'], 'exist',
                'targetClass' => BikDictionary::class,
                'filter' => ['is_active' => true],
                'when' => function ($model) {
                    return !$model->isForeign;
                }
            ],
            [['rs'], 'string', 'length' => 20, 'max' => 20, 'when' => function ($model) {
                return !$model->isForeign;
            }],
            ['rs', 'unique', 'targetAttribute' => ['contractor_id', 'bik', 'rs']],
            [['bank_name', 'bank_city'], 'string', 'max' => 255],
            [['bank_address'], 'string', 'max' => 1000],
            [
                [
                    'is_main',
                    'is_foreign_bank',
                ],
                'boolean',
            ],
            [
                [
                    'no_mask',
                ],
                'boolean',
                'when' => function ($model) {
                    return $model->currency_id == Currency::DEFAULT_ID && !$model->is_foreign_bank;
                },
            ],
            [['currency_id'], 'exist', 'targetClass' => Currency::class, 'targetAttribute' => ['currency_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'contractor_id' => 'Контрагент',
            'currency_id' => 'Валюта',
            'is_foreign_bank' => 'Иностранный банк',
            'rs' => 'Счет №',
            'bik' => $this->is_foreign_bank ? 'SWIFT' : 'БИК',
            'bank_name' => 'Банк',
            'bank_address' => 'Адрес банка',
            'bank_city' => 'Город',
            'is_main' => 'Основной',
            'created_at' => 'Created At',
            'ks' => $this->is_foreign_bank ? 'Счет в банке-корреспонденте / Account №' : 'Корреспондентский счет',
            'corr_bank_name' => 'Банк-корреспондент / Intermediary Bank',
            'corr_bank_address' => 'Адрес банка-корреспондента',
            'corr_bank_swift' => 'SWIFT банка-корреспондента / Intermediary Bank',
            'no_mask' => 'Отключить в поле Счет "810"',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContractor()
    {
        return $this->hasOne(Contractor::className(), ['id' => 'contractor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * @return boolean
     */
    public function getIsForeign()
    {
        return ArrayHelper::getValue($this, 'contractor.face_type') == Contractor::TYPE_FOREIGN_LEGAL_PERSON;
    }

    /**
     * @inheritdoc
     */
    public function afterValidate()
    {
        if (!$this->isForeign && $this->isAttributeChanged('bik')) {
            $bank = BikDictionary::find()->byBik($this->bik)->byActive()->one();
            $this->bank_name = $bank ? $bank->name : null;
            $this->bank_city = $bank ? $bank->city : null;
            $this->ks = $bank ? $bank->ks : null;
        }

        parent::afterValidate();
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert) && $this->contractor->company_id !== null) {
            $condition = [
                'and',
                ['contractor_id' => $this->contractor_id],
                ['is_main' => 1],
                ['not', ['id' => $this->id]],
            ];
            if (!self::find()->andWhere($condition)->exists()) {
                $this->is_main = 1;
            }
            if ($this->is_main === null) {
                $this->is_main = 0;
            }

            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete() && $this->contractor->company_id !== null) {
            return true;
        }

        return false;
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($this->is_main) {
            $condition = [
                'and',
                ['contractor_id' => $this->contractor_id],
                ['is_main' => 1],
                ['not', ['id' => $this->id]],
            ];
            if (self::find()->andWhere($condition)->exists()) {
                self::updateAll(['is_main' => 0], $condition);
            }
        }
    }

    /**
     * @return  string
     */
    public function getBankCity()
    {
        return !empty($this->bank_city) ? 'г. '.$this->bank_city : null;
    }
}
