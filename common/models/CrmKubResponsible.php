<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "crm_kub_responsible".
 *
 * @property int $employee_id
 * @property float $last_time_at
 * @property int $is_responsible
 */
class CrmKubResponsible extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'crm_kub_responsible';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'employee_id' => 'Employee ID',
            'last_time_at' => 'Last Time At',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function touch($employee_id)
    {
        $microtime = explode(' ', microtime());

        static::updateAll([
            'last_time_at' => array_sum($microtime),
        ], [
            'employee_id' => $employee_id,
        ]);
    }

    public static function getResponsibleQuery() : \yii\db\ActiveQuery
    {
        return Yii::$app->kubCompany->getEmployeeCompanies()->alias('employee_company')->joinWith([
            'employee employee',
            'crmKubResponsible crm_kub_responsible',
        ])->andWhere([
            'employee_company.is_working' => true,
            'employee.is_deleted' => false,
            'crm_kub_responsible.is_responsible' => true,
        ]);
    }

    public static function getEmployeeCompany() : ?EmployeeCompany
    {
        return self::getResponsibleQuery()->orderBy([
            'crm_kub_responsible.last_time_at' => SORT_ASC,
        ])->one();
    }

    public static function getEmployeeId() : ?int
    {
        return self::getResponsibleQuery()->select('employee_company.employee_id')->orderBy([
            'crm_kub_responsible.last_time_at' => SORT_ASC,
        ])->scalar();
    }
}
