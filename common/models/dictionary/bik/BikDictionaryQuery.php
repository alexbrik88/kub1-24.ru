<?php
/**
 * Created by konstantin.
 * Date: 22.7.15
 * Time: 16.20
 */

namespace common\models\dictionary\bik;


use yii\db\ActiveQuery;

/**
 * Class BikDictionaryQuery
 * @package common\models\dictionary\bik
 */
class BikDictionaryQuery extends ActiveQuery
{

    /**
     * @param string $bik
     * @return $this
     */
    public function byBik($bik)
    {
        return $this->andWhere([
            'bik' => $bik,
        ]);
    }

    /**
     * @param string $bik
     * @return $this
     */
    public function byBikFilter($bik)
    {
        return $this->andFilterWhere(['like', 'bik', $bik]);
    }

    /**
     * @param bool|true $isActive
     * @return $this
     */
    public function byActive($isActive = true)
    {
        return $this->andWhere([
            'is_active' => $isActive,
        ]);
    }

}