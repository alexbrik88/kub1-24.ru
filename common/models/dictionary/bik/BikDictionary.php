<?php

namespace common\models\dictionary\bik;

use Yii;

/**
 * This is the model class for table "bik_dictionary".
 *
 * @property string $bik
 * @property string $ks
 * @property string $name
 * @property string $namemini
 * @property string $index
 * @property string $city
 * @property string $address
 * @property string $phone
 * @property string $okato
 * @property string $okpo
 * @property string $regnum
 * @property string $srok
 * @property string $dateadd
 * @property string $datechange
 * @property string $object_guid
 */
class BikDictionary extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function find()
    {
        return new BikDictionaryQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bik_dictionary';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bik'], 'required'],
            [['dateadd', 'datechange'], 'safe'],
            [['ks'], 'string', 'max' => 20],
            ['object_guid','string'],
            [['name', 'namemini', 'index', 'city', 'address', 'phone', 'okato', 'okpo', 'regnum', 'srok'], 'string', 'max' => 255],
            [['bik'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'bik' => 'Bik',
            'ks' => 'Ks',
            'name' => 'Name',
            'namemini' => 'Namemini',
            'index' => 'Index',
            'city' => 'City',
            'address' => 'Address',
            'phone' => 'Phone',
            'okato' => 'Okato',
            'okpo' => 'Okpo',
            'regnum' => 'Regnum',
            'srok' => 'Srok',
            'dateadd' => 'Dateadd',
            'datechange' => 'Datechange',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function cityByBik($bik = null)
    {
        return $bik === null ? null : self::find()->select('city')->where(['bik' => $bik])->scalar();
    }
}
