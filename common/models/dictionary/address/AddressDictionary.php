<?php
/**
 * Created by konstantin.
 * Date: 23.7.15
 * Time: 14.45
 */

namespace common\models\dictionary\address;


use common\components\helpers\ArrayHelper;
use yii\base\InvalidConfigException;
use yii\db\Query;

/**
 * Class AddressDictionary
 * @package common\models\dictionary\address
 */
class AddressDictionary
{
    /**
     *
     */
    const SEARCH_CITY = 'city';
    /**
     *
     */
    const SEARCH_STREET = 'street';
    /**
     *
     */
    const SEARCH_POSTCODE = 'postcode';

    /**
     * @var array
     */
    public static $typeArray = [
        self::SEARCH_CITY,
        self::SEARCH_STREET,
        self::SEARCH_POSTCODE,
    ];

    /**
     * @var array
     */
    private $_aoLevelArray = [
        self::SEARCH_CITY => [1, 4, 6,],
        self::SEARCH_STREET => [7,],
        self::SEARCH_POSTCODE => [7,], // search by streets
    ];

    /**
     * @var array
     */
    private $_minLength = [
        self::SEARCH_CITY => 2,
        self::SEARCH_STREET => 2,
        self::SEARCH_POSTCODE => 0,
    ];

    /**
     * @var string
     */
    public $type;
    /**
     * @var string
     */
    public $query;
    /**
     * @var array
     */
    public $params;

    /**
     * @param $type
     * @param $query
     * @param array $params
     * @throws InvalidConfigException
     */
    public function __construct($type, $query, $params)
    {
        $this->type = $type;
        $this->query = rtrim(trim($query), '.,');
        $this->params = array_map(function ($value) {
            return rtrim(trim($value), '.,');
        }, (array)$params);

        if (!isset($this->_aoLevelArray[$type])) {
            throw new InvalidConfigException('AoLevel not found');
        }
    }

    /**
     * @return array
     */
    public function search()
    {
        if (mb_strlen($this->query) < $this->_minLength[$this->type]) {
            return [];
        }

        switch ($this->type) {
            default:
            case self::SEARCH_CITY:
                return $this->_searchCity($this->query);
            case self::SEARCH_STREET:
                return $this->_searchStreet($this->query);
            case self::SEARCH_POSTCODE:
                return $this->_searchPostcode($this->query);
        }
    }

    /**
     * @param $query
     * @return array
     */
    private function _searchCity($query)
    {
        return $this->_getBaseQuery(self::SEARCH_CITY, $query)->all();
    }

    /**
     * @param $query
     * @return array
     */
    private function _searchStreet($query)
    {
        $cityArray = $this->_searchCity(ArrayHelper::getValue($this->params, self::SEARCH_CITY));
        if (empty($cityArray)) {
            return [];
        }

        return $this->_getBaseQuery(self::SEARCH_STREET, $query)
            ->andWhere([
                'in', 'parentguid', ArrayHelper::getColumn($cityArray, 'aoguid'),
            ])
            ->all();
    }

    /**
     * @param $query
     * @return array
     */
    private function _searchPostcode($query)
    {
        $streetArray = $this->_searchStreet(ArrayHelper::getValue($this->params, self::SEARCH_STREET));
        if (empty($streetArray)) {
            return [];
        }

        if (empty($query)) {
            return array_filter($streetArray, function ($value) {
                return !empty($value['postalcode']);
            });
        }

        return $this->_getBaseQuery(self::SEARCH_POSTCODE, null, 'postalcode')
            ->andWhere([
                'in', 'aoguid', ArrayHelper::getColumn($streetArray, 'aoguid'),
            ])
            ->andWhere('postalcode <> ""')
            ->all();
    }

    /**
     * @param $type
     * @param $queryString
     * @param string $queryField
     * @return Query
     */
    private function _getBaseQuery($type, $queryString, $queryField = 'fullname')
    {
        $query = (new Query())
            ->select(['fullname', 'postalcode', 'aoguid'])
            ->from('address_dictionary')
            ->andWhere(['actstatus' => 1,])
            ->groupBy('fullname')
            ->limit(10);

        if (mb_strlen($queryString) !== 0) {
            $query->andWhere(['like', $queryField, $queryString . '%', false]);
        }

        if (!empty($this->_aoLevelArray[$type])) {
            $query->andWhere(['aolevel' => $this->_aoLevelArray[$type]]);
        }

        return $query;
    }

}