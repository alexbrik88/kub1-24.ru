<?php

namespace common\models\news;

use common\models\Company;
use Yii;

/**
 * This is the model class for table "news_reactions".
 *
 * @property int $company_id
 * @property int $news_id
 * @property int $reaction
 *
 * @property Company $company
 * @property News $news
 */
class NewsReaction extends \yii\db\ActiveRecord
{
    public const LIKE_REACTION = 0;
    public const DISLIKE_REACTION = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'news_reactions';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'news_id', 'reaction'], 'required'],
            [['company_id', 'news_id', 'reaction'], 'integer'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['company_id' => 'id']],
            [['news_id'], 'exist', 'skipOnError' => true, 'targetClass' => News::class, 'targetAttribute' => ['news_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'company_id' => 'Company ID',
            'news_id' => 'News ID',
            'reaction' => 'Reaction',
        ];
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * Gets query for [[News]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNews()
    {
        return $this->hasOne(News::class, ['id' => 'news_id']);
    }
}
