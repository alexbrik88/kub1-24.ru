<?php

namespace common\models\news;

use common\components\date\DatePickerFormatBehavior;
use common\models\Company;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "news".
 *
 * @property int $id
 * @property string $name
 * @property string $text
 * @property string $date
 * @property int $type
 * @property int $status
 * @property int $serial_number
 * @property int $likes_count
 * @property int $dislikes_count
 * @property int $created_at
 * @property int $updated_at
 *
 * @property NewsReaction[] $newsReactions
 */
class News extends ActiveRecord
{
    public const STATUS_ACTIVE = 0;
    public const STATUS_ARCHIVE = 1;

    public const TYPE_USEFUL = 0;
    public const TYPE_NEWS = 1;

    public static array $statusesMap = [
        self::STATUS_ACTIVE => 'Активно',
        self::STATUS_ARCHIVE => 'Архив',
    ];

    public static array $typesMap = [
        self::TYPE_USEFUL => 'Полезное',
        self::TYPE_NEWS => 'Новость',
    ];

    public static function tableName(): string
    {
        return 'news';
    }

    public function behaviors(): array
    {
        return [
            TimestampBehavior::class,
            [
                'class' => DatePickerFormatBehavior::class,
                'attributes' => [
                    'date' => [
                        'message' => 'Дата указана неверно.',
                    ],
                ],
            ],
        ];
    }

    public function rules(): array
    {
        return [
            [['name', 'text', 'type', 'status'], 'required'],
            [['text'], 'string'],
            [['date'], 'safe'],
            [['status'], 'in', 'range' => array_keys(self::$statusesMap)],
            [['type'], 'in', 'range' => array_keys(self::$typesMap)],
            [['serial_number', 'created_at', 'updated_at', 'likes_count', 'dislikes_count'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'name' => 'Заголовок',
            'text' => 'Текст',
            'date' => 'Дата',
            'type' => 'Тип',
            'status' => 'Статус',
            'serial_number' => 'Порядковый номер',
            'likes_count' => 'Лайки',
            'dislikes_count' => 'Дизлайки',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getNewsReactions(): ActiveQuery
    {
        return $this->hasMany(NewsReaction::class, ['news_id' => 'id']);
    }

    public function getLikesCount(): int
    {
        return $this->likes_count + (int)$this->getNewsReactions()->andWhere(['reaction' => NewsReaction::LIKE_REACTION])->count();
    }

    public function getDislikesCount(): int
    {
        return $this->dislikes_count + (int)$this->getNewsReactions()->andWhere(['reaction' => NewsReaction::DISLIKE_REACTION])->count();
    }

    public function hasCompanyReaction(Company $company, int $reaction): bool
    {
        return NewsReaction::find()
            ->andWhere(['news_id' => $this->id])
            ->andWhere(['company_id' => $company->id])
            ->andWhere(['reaction' => $reaction])
            ->exists();
    }
}
