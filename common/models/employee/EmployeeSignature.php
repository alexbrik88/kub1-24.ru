<?php

namespace common\models\employee;

use frontend\models\Documents;
use Yii;
use common\models\Company;
use common\models\document;
use common\models\EmployeeCompany;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\InvoiceFacture;
use common\models\document\PackingList;
use yii\helpers\FileHelper;

/**
 * This is the model class for table "employee_signature".
 *
 * @property integer $id
 * @property integer $employee_id
 * @property integer $company_id
 * @property string $file_name
 * @property integer $created_at
 * @property integer $status_id
 *
 * @property Act[] $acts
 * @property EmployeeCompany[] $employeeCompanies
 * @property Company $company
 * @property Employee $employee
 * @property Invoice[] $invoices
 * @property InvoiceFacture[] $invoiceFactures
 * @property PackingList[] $packingLists
 */
class EmployeeSignature extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;

    public static $applyDocList = [
        Documents::DOCUMENT_ACT,
        Documents::DOCUMENT_INVOICE,
        Documents::DOCUMENT_INVOICE_FACTURE,
        Documents::DOCUMENT_PACKING_LIST,
        Documents::DOCUMENT_UPD,
        Documents::DOCUMENT_AGREEMENT,
        Documents::DOCUMENT_PROXY,
        Documents::DOCUMENT_AGENT_REPORT,
        Documents::DOCUMENT_SALES_INVOICE,
        Documents::DOCUMENT_WAYBILL
    ];

    public static $hasToMany = [
        Documents::DOCUMENT_ACT,
        Documents::DOCUMENT_INVOICE_FACTURE,
        Documents::DOCUMENT_UPD,
    ];
    public static $hasCompanyId = [
        Documents::DOCUMENT_INVOICE,
        Documents::DOCUMENT_AGREEMENT,
        Documents::DOCUMENT_AGENT_REPORT,
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'employee_signature';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['employee_id', 'company_id', 'file_name', 'created_at'], 'required'],
            [['employee_id', 'company_id', 'created_at', 'status_id'], 'integer'],
            [['file_name'], 'string', 'max' => 50],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['employee_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'employee_id' => 'Employee ID',
            'company_id' => 'Company ID',
            'file_name' => 'File Name',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActs()
    {
        return $this->hasMany(document\Act::className(), ['signature_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployeeCompanies()
    {
        return $this->hasMany(EmployeeCompany::className(), ['signature_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoices()
    {
        return $this->hasMany(document\Invoice::className(), ['signature_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceFactures()
    {
        return $this->hasMany(document\InvoiceFacture::className(), ['signature_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPackingLists()
    {
        return $this->hasMany(document\PackingList::className(), ['signature_id' => 'id']);
    }

    /**
     * @return string
     */
    public function getUploadPath()
    {
        return Company::fileUploadPath($this->company_id) .
            DIRECTORY_SEPARATOR . 'signature' .
            DIRECTORY_SEPARATOR . $this->employee_id;
    }

    /**
     * @return string
     */
    public function getFilePath()
    {
        return $this->getUploadPath() . DIRECTORY_SEPARATOR . $this->file_name;
    }

    /**
     * @return string
     */
    public function getDir()
    {
        return $this->getUploadPath();
    }

    /**
     * @return string
     */
    public function getFile()
    {
        return $this->getFilePath();
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        $files = [];
        $basePath = $this->getUploadPath();
        if (file_exists($basePath)) {
            $fileName = $this->file_name;
            $files = FileHelper::findFiles($basePath, [
                'only' => ["/{$fileName}"],
                'recursive' => false,
            ]);
        }

        return reset($files);
    }

    /**
     *
     */
    public function deleteImage()
    {
        $basePath = $this->getUploadPath();
        $fileName = $this->file_name;
        if (is_dir($basePath)) {
            $files = FileHelper::findFiles($basePath, ['only' => ["/{$fileName}"], 'recursive' => false]);
            foreach ($files as $file) {
                if (is_file($file)) {
                    unlink($file);
                }
            }
        }
    }

    /**
     * @throws \yii\db\Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function applyForAll()
    {
        $params = [
            ':signature_id' => $this->id,
            ':company_id' => $this->company_id,
            ':employee_id' => $this->employee_id
        ];

        foreach (self::$applyDocList as $docTypeId) {

            $className = Documents::getBaseModel($docTypeId);
            $tableName = $className::tableName();

            if (in_array($docTypeId, self::$hasCompanyId)) {
                Yii::$app->db->createCommand("
                    UPDATE 
                      {{%{$tableName}}} AS {{%document}}
                    SET 
                      {{%document}}.[[signature_id]] = :signature_id
                    WHERE 
                      {{%document}}.[[company_id]] = :company_id AND 
                      {{%document}}.[[signed_by_employee_id]] = :employee_id
                ", $params)->execute();

            } elseif (in_array($docTypeId, self::$hasToMany)) {
                Yii::$app->db->createCommand("
                    UPDATE 
                      {{%{$tableName}}} AS {{%document}}
                    RIGHT JOIN {{%invoice_{$tableName}}} AS {{link}} 
                      ON {{link}}.[[{$tableName}_id]] = {{%document}}.[[id]]
                    RIGHT JOIN {{%invoice}} 
                      ON {{link}}.[[invoice_id]] = {{%invoice}}.[[id]]
                    SET 
                      {{%document}}.[[signature_id]] = :signature_id
                    WHERE 
                      {{%invoice}}.[[company_id]] = :company_id AND 
                      {{%document}}.[[signed_by_employee_id]] = :employee_id
                ", $params)->execute();

            } else {
                Yii::$app->db->createCommand("
                    UPDATE 
                      {{%{$tableName}}} AS {{%document}}
                    RIGHT JOIN {{%invoice}} 
                      ON {{%document}}.[[invoice_id]] = {{%invoice}}.[[id]]
                    SET 
                      {{%document}}.[[signature_id]] = :signature_id
                    WHERE 
                      {{%invoice}}.[[company_id]] = :company_id AND 
                      {{%document}}.[[signed_by_employee_id]] = :employee_id
                ", $params)->execute();
            }
        }
    }
}
