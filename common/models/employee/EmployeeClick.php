<?php

namespace common\models\employee;

use Yii;

/**
 * This is the model class for table "employee_click".
 *
 * @property integer $id
 * @property integer $employee_id
 * @property integer $click_type
 * @property string $click_date
 *
 * @property Employee $employee
 */
class EmployeeClick extends \yii\db\ActiveRecord
{
    const SALE_INCREASE_TYPE = 1;
    const PRE_VIEW_STORE_CABINET = 2;
    const PRE_VIEW_OUT_INVOICE = 3;

    public static $clickTypes = [
        self::SALE_INCREASE_TYPE,
        self::PRE_VIEW_STORE_CABINET,
        self::PRE_VIEW_OUT_INVOICE,
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'employee_click';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['employee_id', 'click_type', 'click_date'], 'required'],
            [['employee_id', 'click_type',], 'integer'],
            [['click_date'], 'safe'],
            [['click_type'], 'in', 'range' => self::$clickTypes],
            [['employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['employee_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'employee_id' => 'Employee ID',
            'click_type' => 'Click Type',
            'click_date' => 'Click Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }
}
