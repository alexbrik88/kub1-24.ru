<?php

namespace common\models\employee;

use frontend\rbac\UserRole;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "employee_role".
 *
 * @property integer $id
 * @property string $name
 */
class EmployeeRole extends ActiveRecord
{
    const ROLE_CHIEF      = 1;
    const ROLE_ACCOUNTANT = 2;
    const ROLE_MANAGER    = 3;
    const ROLE_ASSISTANT  = 4;
    const ROLE_SUPERVISOR  = 5;
    const ROLE_SUPERVISOR_VIEWER  = 6;
    const ROLE_EMPLOYEE  = 7;
    const ROLE_SALES_SUPERVISOR  = 8;
    const ROLE_SALES_SUPERVISOR_VIEWER  = 9;
    const ROLE_SALES_MANAGER = 10;
    const ROLE_FOUNDER = 11;
    const ROLE_MARKETER = 12;
    const ROLE_DEMO = 13;
    const ROLE_FINANCE_DIRECTOR = 14;
    const ROLE_FINANCE_ADVISER = 15;
    const ROLE_PARTNER_RELATIONS_MANAGER = 16;
    const ROLE_PURCHASE_SUPERVISOR = 17;

    /**
     * @var array
     */
    public static $roleArray = [
        self::ROLE_CHIEF      => UserRole::ROLE_CHIEF,
        self::ROLE_ACCOUNTANT => UserRole::ROLE_ACCOUNTANT,
        self::ROLE_MANAGER    => UserRole::ROLE_MANAGER,
        self::ROLE_ASSISTANT  => UserRole::ROLE_ASSISTANT,
        self::ROLE_SUPERVISOR  => UserRole::ROLE_SUPERVISOR,
        self::ROLE_SUPERVISOR_VIEWER  => UserRole::ROLE_SUPERVISOR_VIEWER,
        self::ROLE_EMPLOYEE  => UserRole::ROLE_EMPLOYEE,
        self::ROLE_SALES_SUPERVISOR  => UserRole::ROLE_SALES_SUPERVISOR,
        self::ROLE_SALES_SUPERVISOR_VIEWER  => UserRole::ROLE_SALES_SUPERVISOR_VIEWER,
        self::ROLE_SALES_MANAGER  => UserRole::ROLE_SALES_MANAGER,
        self::ROLE_FOUNDER  => UserRole::ROLE_FOUNDER,
        self::ROLE_MARKETER => UserRole::ROLE_MARKETER,
        self::ROLE_DEMO => UserRole::ROLE_DEMO,
        self::ROLE_FINANCE_DIRECTOR => UserRole::ROLE_FINANCE_DIRECTOR,
        self::ROLE_FINANCE_ADVISER => UserRole::ROLE_FINANCE_ADVISER,
        self::ROLE_PARTNER_RELATIONS_MANAGER => UserRole::ROLE_PARTNER_RELATIONS_MANAGER,
        self::ROLE_PURCHASE_SUPERVISOR => UserRole::ROLE_PURCHASE_SUPERVISOR,
    ];

    /**
     * @var array
     */
    public static $roleViewAll = [
        self::ROLE_CHIEF,
        self::ROLE_FOUNDER,
        self::ROLE_FINANCE_DIRECTOR,
        self::ROLE_FINANCE_ADVISER,
    ];

    /**
     * @var array
     */
    public static $description = [
        self::ROLE_CHIEF      => 'это максимальный уровень доступа',
        self::ROLE_ACCOUNTANT => 'видит все, кроме операций, которые помечены «Не для бухгалтерии».',
        self::ROLE_MANAGER    => 'может выставлять счета, видит только своих покупателей и поставщиков и свои счета им.
            Не видит блоки, связанные с финансами.',
        self::ROLE_ASSISTANT  => 'может вносить документы от ваших поставщиков.
            Счета вашим клиентам - не видит.',
        self::ROLE_SUPERVISOR  => 'видит все счета выставляемые менеджерами,
            может редактировать и удалять счета, акты, накладные и счета-фактуры.
            Не видит блоки, связанные с финансами.',
        self::ROLE_SUPERVISOR_VIEWER  => 'видит все счета выставляемые менеджерами.
            Не видит блоки, связанные с финансами.',
        self::ROLE_EMPLOYEE  => 'сотрудник, не имеющий доступа в сервис КУБ.',
        self::ROLE_SALES_SUPERVISOR  => 'видит все счета выставляемые менеджерами,
            может редактировать и удалять счета, акты, накладные, счета-фактуры и договоры.
            Не видит блоки, связанные с Финансами, "Покупки", "Поставщики", и не видят "цену покупки" у товаров и услуг.',
        self::ROLE_SALES_SUPERVISOR_VIEWER  => 'видит все счета выставляемые менеджерами.
            Не видит блоки, связанные с Финансами, "Покупки", "Поставщики", и не видят "цену покупки" у товаров и услуг.',
        self::ROLE_SALES_MANAGER  => 'может выставлять счета, видит только своих покупателей и свои выставленные счета.
            Не видит блоки, связанные с Финансами, "Покупки", "Поставщики", и не видят "цену покупки" у товаров и услуг.',
        self::ROLE_PURCHASE_SUPERVISOR  => 'видит все входящие счета и поставщиков,
            может редактировать и удалять счета, акты, накладные, счета-фактуры и договоры.
            Не видит блоки, связанные с Финансами, "Продажи", "Покупатели".',
        self::ROLE_FOUNDER  => 'видит всё, но только просмотр.',
        self::ROLE_MARKETER  => 'доступ только к разделу "Маркетинг".',
        self::ROLE_DEMO  => 'Пользователь для демонстрации сервиса',
        self::ROLE_FINANCE_DIRECTOR  => 'доступ ко всему разделу "ФинДиректор" и блоку Выставления счетов. 
            Не может добавлять и редактировать сотрудников.',
        self::ROLE_FINANCE_ADVISER  => 'доступ ко всему разделу "ФинДиректор" и блоку Выставления счетов. 
            Не может добавлять и редактировать сотрудников.
            Дополнительные возможности, которые согласовываются индивидуально.',
        self::ROLE_PARTNER_RELATIONS_MANAGER => 'доступ только к разделу "Кабинет партнера"',
    ];

    /**
     * @return EmployeeRoleQuery
     */
    public static function find()
    {
        return new EmployeeRoleQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'employee_role';
    }

    /**
     * @param $roleId
     * @return string
     * @throws NotFoundHttpException
     */
    public static function getRoleName($roleId)
    {
        if (!isset(self::$roleArray[$roleId])) {
            throw new NotFoundHttpException('Роль не найдена.');
        }

        return self::$roleArray[$roleId];

    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return ArrayHelper::getValue(self::$description, $this->id);
    }
}
