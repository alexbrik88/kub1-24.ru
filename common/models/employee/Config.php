<?php

namespace common\models\employee;

use common\models\product\ProductCategoryField;
use common\models\product\ProductField;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "user_config".
 *
 * @property integer $employee_id
 * @property integer $invoice_scan
 * @property integer $invoice_paylimit
 * @property integer $invoice_paydate
 * @property integer $invoice_act
 * @property integer $invoice_paclist
 * @property integer $invoice_waybill
 * @property integer $invoice_invfacture
 * @property integer $invoice_upd
 * @property integer $invoice_author
 * @property integer $invoice_comment
 * @property integer $invoice_expenditure_item
 * @property integer $invoice_income_item
 * @property integer $act_order_sum_without_nds
 * @property integer $act_order_sum_nds
 * @property integer $act_contractor_inn
 * @property integer $act_payment_date
 * @property integer $act_comment
 * @property integer $act_responsible
 * @property integer $act_source
 * @property integer $act_scan
 * @property integer $packing_list_order_sum_without_nds
 * @property integer $packing_list_order_sum_nds
 * @property integer $packing_list_contractor_inn
 * @property integer $packing_list_payment_date
 * @property integer $packing_list_responsible
 * @property integer $packing_list_source
 * @property integer $sales_invoice_order_sum_without_nds
 * @property integer $sales_invoice_order_sum_nds
 * @property integer $sales_invoice_contractor_inn
 * @property integer $sales_invoice_payment_date
 * @property integer $sales_invoice_responsible
 * @property integer $sales_invoice_source
 * @property integer $upd_order_sum_without_nds
 * @property integer $upd_order_sum_nds
 * @property integer $upd_contractor_inn
 * @property integer $upd_payment_date
 * @property integer $upd_responsible
 * @property integer $upd_source
 * @property integer $act_stat_by_payment
 * @property integer $act_stat_by_status
 * @property integer $act_stat_by_calculate_nds
 * @property integer $contractor_contact
 * @property integer $contractor_phone
 * @property integer $contractor_agreement
 * @property integer $contractor_payment_delay
 * @property integer $contractor_notpaidcount
 * @property integer $contractor_notpaidsum
 * @property integer $contractor_paidcount
 * @property integer $contractor_paidsum
 * @property integer $contractor_responsible
 * @property integer $contractor_new_invoice
 * @property integer $contractor_prepaymentsum
 * @property integer $contractor_balancesum
 * @property integer $contractor_docs_balancesum
 * @property integer $contractor_income_item
 * @property integer $contractor_expenditure_item
 *
 * @property integer $project_number
 * @property integer $project_direction
 * @property integer $project_clients
 * @property integer $project_end_date
 * @property integer $project_before_date
 * @property integer $project_result_sum
 * @property integer $project_income
 * @property integer $project_expense
 * @property integer $project_profit
 * @property integer $project_profitability
 * @property integer $project_responsible
 * @property integer $project_invoices_scan
 * @property integer $project_invoices_paylimit
 * @property integer $project_invoices_paydate
 * @property integer $project_invoices_act
 * @property integer $project_invoices_paclist
 * @property integer $project_invoices_invfacture
 * @property integer $project_invoices_upd
 * @property integer $project_invoices_responsible
 * @property integer $project_invoices_comment
 * @property integer $project_estimate_responsible
 * @property integer $project_estimate_item_article
 * @property integer $project_estimate_item_income_price
 * @property integer $project_estimate_item_income_sum
 * @property integer $project_estimate_item_sum_diff
 * @property integer $project_money_paying
 * @property integer $project_money_income_expense
 * @property integer $project_money_project
 * @property integer $project_money_sale_point
 * @property integer $project_money_industry
 * @property integer $project_money_recognition_date
 * @property integer $project_odds
 * @property integer $project_opiu
 * @property integer $project_odds_collapse
 * @property integer $project_opiu_collapse
 * @property integer $order_document_product_article
 * @property integer $order_document_product_reserve
 * @property integer $order_document_product_quantity
 * @property integer $order_document_product_weigh
 * @property integer $order_document_product_volume
 * @property integer $order_document_payment_sum
 * @property integer $order_document_supplier_document_date
 * @property integer $order_document_shipped
 * @property integer $order_document_reserve
 * @property integer $order_document_ship_up_to_date
 * @property integer $order_document_stock_id
 * @property integer $order_document_invoice
 * @property integer $order_document_act
 * @property integer $order_document_packing_list
 * @property integer $order_document_sales_invoice
 * @property integer $order_document_invoice_facture
 * @property integer $order_document_responsible_employee_id
 * @property integer $order_document_detailing
 * @property integer $order_document_upd
 * @property integer $payment_reminder_message_contractor_invoice_sum
 * @property integer $payment_reminder_message_contractor_message_1
 * @property integer $payment_reminder_message_contractor_message_2
 * @property integer $payment_reminder_message_contractor_message_3
 * @property integer $payment_reminder_message_contractor_message_4
 * @property integer $payment_reminder_message_contractor_message_5
 * @property integer $payment_reminder_message_contractor_message_6
 * @property integer $payment_reminder_message_contractor_message_7
 * @property integer $payment_reminder_message_contractor_message_8
 * @property integer $payment_reminder_message_contractor_message_9
 * @property integer $payment_reminder_message_contractor_message_10
 * @property integer $product_group
 * @property integer $product_purchase_price
 * @property integer $product_purchase_amount
 * @property integer $product_custom_field
 * @property string  $product_category_fields
 * @property string  $product_unit_type
 * @property integer $agreement_scan
 * @property integer $agreement_responsible_employee_id
 * @property integer $page_size
 * @property integer $report_stocks_help
 * @property integer $report_stocks_chart
 * @property integer $report_break_even_help
 * @property integer $report_break_even_chart
 * @property integer $report_odds_help
 * @property integer $report_odds_chart
 * @property integer $report_odds_active_tab
 * @property integer $report_debtor_help
 * @property integer $report_debtor_chart
 * @property integer $report_debtor_period_type
 * @property integer $report_pc_help
 * @property integer $report_pc_chart
 * @property integer $report_pc_priority
 * @property integer $report_expenses_help
 * @property integer $report_expenses_chart
 * @property integer $report_selling_speed_unit
 * @property integer $report_selling_speed_article
 * @property integer $report_selling_speed_group
 * @property integer $report_selling_speed_storage
 * @property integer $rent_categories
 * @property integer $rent_statuses
 * @property integer $rent_responsible
 * @property integer $table_rent_lists
 * @property integer $table_project_estimate
 * @property integer $table_project_estimate_item
 * @property integer $project_estimate_name
 * @property integer $table_view_payment_order
 * @property integer $table_view_autoinvoice
 * @property integer $table_view_invoice_facture
 * @property integer $turnover_group_id
 * @property integer $turnover_unit_id
 * @property bool    $table_view_rent_type      Учёт аренды: показать/скрыть столбец "тип"
 * @property bool    $table_view_rent_agreement Учёт аренды: показать/скрыть столбец "договор"
 * @property bool    $table_view_rent_invoice   Учёт аренды: показать/скрыть столбец "счёт"
 * @property integer $agreement_template_responsible_employee_id
 * @property integer $marketing_help
 * @property integer $marketing_chart
 * @property integer $table_view_marketing
 * @property integer $table_view_marketing_planning
 * @property integer $table_view_yandex_direct_by_campaign
 * @property integer $table_view_yandex_direct_by_ad_group
 * @property integer $table_view_yandex_direct_by_ad
 * @property integer $table_view_google_ads_by_campaign
 * @property integer $table_view_google_ads_by_ad_group
 * @property integer $table_view_google_ads_by_ad
 * @property integer $table_view_vk_ads_by_campaign
 * @property integer $table_view_vk_ads_by_ad_group
 * @property integer $yandex_direct_status
 * @property integer $yandex_direct_strategy
 * @property integer $yandex_direct_placement
 * @property integer $yandex_direct_budget
 * @property integer $table_view_logistics
 * @property integer $selling_speed_help
 * @property integer $selling_speed_chart
 * @property integer $report_odds_own_funds
 * @property integer $report_plan_fact_expanded
 * @property integer $theme_wide
 * @property integer $minimize_side_menu
 * @property integer $logistics_request_unloading_date
 * @property integer $logistics_request_goods
 * @property integer $logistics_request_customer_debt
 * @property integer $logistics_request_carrier_debt
 * @property integer $logistics_request_ttn
 * @property integer $logistics_request_author_id
 * @property int $credits_credit_table
 * @property int $credits_flow_table
 * @property int $credits_debt_table
 * @property int $credits_credit_first_date
 * @property int $credits_credit_amount
 * @property int $credits_contractor_id
 * @property int $credits_credit_type
 * @property int $credits_debt_amount
 * @property int $credits_debt_repaid
 * @property int $order_many_create_recognition_date
 * @property int $order_many_create_invoices_list
 * @property int $order_many_create_project
 * @property int $order_many_create_sale_point
 * @property int $order_many_create_direction
 *
 * @property int $bank_column_income_expense
 * @property int $bank_column_paying
 * @property int $bank_column_project
 * @property int $order_column_income_expense
 * @property int $order_column_paying
 * @property int $order_column_project
 * @property int $emoney_column_income_expense
 * @property int $emoney_column_paying
 * @property int $emoney_column_project
 * @property int $card_column_income_expense
 * @property int $card_column_project
 * @property int $acquiring_column_income_expense
 * @property int $acquiring_column_project
 * @property int $cash_index_hide_plan
 * @property int $cash_column_sale_point
 * @property int $bank_column_sale_point
 * @property int $order_column_sale_point
 * @property int $emoney_column_sale_point
 * @property int $acquiring_column_sale_point
 * @property int $card_column_sale_point
 * @property int $cash_column_industry
 * @property int $bank_column_industry
 * @property int $bank_column_recognition_date
 * @property int $order_column_industry
 * @property int $emoney_column_industry
 * @property int $acquiring_column_industry
 * @property int $card_column_industry
 *
 * @property int $report_odds_row_finance
 * @property int $report_marketing_planning_round_numbers
 * @property int $report_odds_row_investments
 * @property int $report_odds_row_bank
 * @property int $report_odds_row_order
 * @property int $report_odds_row_emoney
 * @property int $report_odds_row_acquiring
 * @property int $report_odds_row_card
 * @property int $report_odds_hide_zeroes
 * @property int $report_odds_income_percent
 * @property int $report_odds_expense_percent
 * @property int $report_odds_expense_percent_2
 * @property int $marketing_plan_by_budget
 * @property int $marketing_plan_by_total_expenses
 * @property int $goods_cancellation_responsible
 * @property int $goods_cancellation_pal_type
 * @property int $goods_cancellation_estimate
 * @property int $goods_cancellation_project
 * @property int $goods_cancellation_contractor
 * @property int $report_operational_efficiency_row_profit_and_loss
 * @property int $report_operational_efficiency_row_msfo_profit
 * @property int $report_operational_efficiency_row_revenue_profitability
 * @property int $report_operational_efficiency_row_return_on_investment
 * @property int $selling_report_search_by
 * @property int $selling_report_type
 * @property int $selling_report_by_contractor
 * @property int $selling_report_percent
 * @property int $selling_report_percent_total
 *
 * @property int $contractor_sale_point
 * @property int $contractor_industry
 * @property int $invoice_sale_point
 * @property int $invoice_industry
 * @property int $invoice_project
 * @property int $act_sale_point
 * @property int $act_industry
 * @property int $act_project
 * @property int $packing_list_sale_point
 * @property int $packing_list_industry
 * @property int $packing_list_project
 * @property int $upd_sale_point
 * @property int $upd_industry
 * @property int $upd_project
 * @property int $invoice_facture_sale_point
 * @property int $invoice_facture_industry
 * @property int $invoice_facture_project
 * @property int $crm_task_description
 * @property int $crm_task_result
 * @property int $crm_task_hide_completed
 *
 * @property int $report_detailing_industry_date
 * @property int $report_detailing_industry_type
 * @property int $report_detailing_industry_income
 * @property int $report_detailing_industry_expense
 * @property int $report_detailing_industry_revenue
 * @property int $report_detailing_industry_margin
 * @property int $report_detailing_industry_net
 * @property int $report_detailing_industry_profit
 * @property int $debts_cash_config_hide_zero
 * @property int $debts_cash_config_show_by_account
 * @property int $balance_article_purchased_at
 * @property int $balance_article_category
 * @property int $balance_article_amount
 * @property int $balance_article_useful_life_in_month
 * @property int $balance_article_month_in_use
 * @property int $balance_article_month_left
 * @property int $balance_article_write_off_date
 * @property int $balance_article_status
 * @property int $packing_list_scan
 * @property int $upd_scan
 *
 * @property Employee $employee
 */
class Config extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_config';
    }

    // Product category fields
    public $product_category_field_isbn;
    public $product_category_field_author;
    public $product_category_field_publishing_year;
    public $product_category_field_publishing_house;
    public $product_category_field_format;
    public $product_category_field_book_binding;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'invoice_scan',
                    'invoice_paylimit',
                    'invoice_paydate',
                    'invoice_act',
                    'invoice_paclist',
                    'invoice_waybill',
                    'invoice_invfacture',
                    'invoice_upd',
                    'invoice_author',
                    'invoice_comment',
                    'invoice_expenditure_item',
                    'invoice_income_item',
                    'act_order_sum_without_nds',
                    'act_order_sum_nds',
                    'act_contractor_inn',
                    'act_payment_date',
                    'act_comment',
                    'act_responsible',
                    'act_source',
                    'act_scan',
                    'packing_list_order_sum_without_nds',
                    'packing_list_order_sum_nds',
                    'packing_list_contractor_inn',
                    'packing_list_payment_date',
                    'packing_list_responsible',
                    'packing_list_source',
                    'packing_list_scan',
                    'sales_invoice_order_sum_without_nds',
                    'sales_invoice_order_sum_nds',
                    'sales_invoice_contractor_inn',
                    'sales_invoice_payment_date',
                    'sales_invoice_responsible',
                    'sales_invoice_source',
                    'upd_order_sum_without_nds',
                    'upd_order_sum_nds',
                    'upd_contractor_inn',
                    'upd_payment_date',
                    'upd_responsible',
                    'upd_source',
                    'upd_scan',
                    'act_stat_by_payment',
                    'act_stat_by_status',
                    'act_stat_by_calculate_nds',
                    'contr_inv_scan',
                    'contr_inv_paylimit',
                    'contr_inv_paydate',
                    'contr_inv_act',
                    'contr_inv_paclist',
                    'contr_inv_waybill',
                    'contr_inv_invfacture',
                    'contr_inv_upd',
                    'contr_inv_author',
                    'contr_inv_comment',
                    'contr_inv_income_item',
                    'contr_inv_expenditure_item',
                    'contr_inv_industry',
                    'contr_inv_sale_point',
                    'contr_inv_project',
                    'contractor_contact',
                    'contractor_group_name',
                    'contractor_phone',
                    'contractor_agreement',
                    'contractor_payment_delay',
                    'contractor_notpaidcount',
                    'contractor_notpaidsum',
                    'contractor_paidcount',
                    'contractor_paidsum',
                    'contractor_new_invoice',
                    'contractor_prepaymentsum',
                    'contractor_balancesum',
                    'contractor_docs_balancesum',
                    'contractor_income_item',
                    'contractor_expenditure_item',
                    'contractor_autionvoice',
                    'contractor_responsible',
                    'order_document_product_article',
                    'order_document_product_reserve',
                    'order_document_product_quantity',
                    'order_document_product_weigh',
                    'order_document_product_volume',
                    'order_document_payment_sum',
                    'order_document_supplier_document_date',
                    'order_document_shipped',
                    'order_document_reserve',
                    'order_document_ship_up_to_date',
                    'order_document_stock_id',
                    'order_document_invoice',
                    'order_document_act',
                    'order_document_packing_list',
                    'order_document_sales_invoice',
                    'order_document_invoice_facture',
                    'order_document_responsible_employee_id',
                    'order_document_detailing',
                    'order_document_upd',
                    'product_image',
                    'product_comment',
                    'product_title_en',
                    'product_article',
                    'product_group',
                    'product_zeroes_quantity',
                    'product_zeroes_turnover',
                    'product_purchase_price',
                    'product_purchase_amount',
                    'product_custom_field',
                    'product_category_fields',
                    'product_unit_type',
                    'invoice_form_article',
                    'payment_reminder_message_contractor_invoice_sum',
                    'payment_reminder_message_contractor_message_1',
                    'payment_reminder_message_contractor_message_2',
                    'payment_reminder_message_contractor_message_3',
                    'payment_reminder_message_contractor_message_4',
                    'payment_reminder_message_contractor_message_5',
                    'payment_reminder_message_contractor_message_6',
                    'payment_reminder_message_contractor_message_7',
                    'payment_reminder_message_contractor_message_8',
                    'payment_reminder_message_contractor_message_9',
                    'payment_reminder_message_contractor_message_10',
                    'agreement_scan',
                    'agreement_responsible_employee_id',
                    'agreement_template_responsible_employee_id',
                    'crm_client_comment',
                    'table_view_document',
                    'table_view_detailing',
                    'table_view_contractor_list',
                    'table_view_contractor',
                    'table_view_cash',
                    'table_view_product',
                    'table_view_finance_odds',
                    'table_view_finance_plan_fact',
                    'table_view_price_list',
                    'table_view_finance_profit_loss',
                    'table_view_finance_balance',
                    'table_view_operational_efficiency',
                    'table_view_finance_expenses',
                    'table_view_finance_stocks',
                    'table_view_finance_income',
                    'table_view_finance_pc',
                    'table_view_project_money',
                    'table_view_project_invoice',
                    'table_view_acquiring',
                    'table_view_logistics',
                    'page_size',
                    'report_stocks_help',
                    'report_stocks_chart',
                    'report_break_even_help',
                    'report_break_even_chart',
                    'report_odds_help',
                    'report_odds_chart',
                    'report_odds_active_tab',
                    'report_pc_help',
                    'report_pc_chart',
                    'report_pc_priority',
                    'report_expenses_help',
                    'report_expenses_chart',
                    'report_income_help',
                    'report_income_chart',
                    'report_plan_fact_help',
                    'report_plan_fact_chart',
                    'report_profit_loss_help',
                    'report_profit_loss_chart',
                    'report_operational_efficiency_chart',
                    'report_operational_efficiency_help',
                    'report_balance_help',
                    'report_balance_chart',
                    'report_debtor_help',
                    'report_debtor_chart',
                    'report_debtor_period_type',
                    'product_abc_help',
                    'product_abc_chart',
                    'table_view_report_client',
                    'table_view_report_employee',
                    'table_view_report_invoice',
                    'table_view_report_supplier',
                    'table_rent_list',
                    'table_project_estimate',
                    'table_project_estimate_item',
                    'table_view_payment_order',
                    'table_view_autoinvoice',
                    'table_view_invoice_facture',
                    'turnover_group_id',
                    'turnover_unit_id',
                    'table_view_rent_type',
                    'table_view_rent_agreement',
                    'table_view_rent_invoice',
                    'report_pc_chart_period',
                    'report_odds_chart_period',
                    'marketing_help',
                    'marketing_chart',
                    'table_view_marketing',
                    'table_view_marketing_planning',
                    'table_view_yandex_direct_by_campaign',
                    'table_view_yandex_direct_by_ad_group',
                    'table_view_yandex_direct_by_ad',
                    'table_view_google_ads_by_campaign',
                    'table_view_google_ads_by_ad_group',
                    'table_view_google_ads_by_ad',
                    'table_view_vk_ads_by_campaign',
                    'table_view_vk_ads_by_ad_group',
                    'product_category_field_isbn',
                    'product_category_field_author',
                    'product_category_field_publishing_year',
                    'product_category_field_publishing_house',
                    'product_category_field_format',
                    'product_category_field_book_binding',
                    'table_view_report_abc',
                    'report_abc_article',
                    'report_abc_cost_price',
                    'report_abc_group_margin',
                    'report_abc_number_of_sales',
                    'report_abc_average_check',
                    'report_abc_comment',
                    'report_abc_purchase_price',
                    'report_abc_selling_price',
                    'report_abc_show_groups',
                    'yandex_direct_status',
                    'yandex_direct_strategy',
                    'yandex_direct_placement',
                    'yandex_direct_budget',
                    'project_number',
                    'project_direction',
                    'project_clients',
                    'project_end_date',
                    'project_before_date',
                    'project_result_sum',
                    'project_income',
                    'project_expense',
                    'project_profit',
                    'project_profitability',
                    'project_responsible',
                    'project_invoices_scan',
                    'project_invoices_paylimit',
                    'project_invoices_paydate',
                    'project_invoices_act',
                    'project_invoices_paclist',
                    'project_invoices_invfacture',
                    'project_invoices_upd',
                    'project_invoices_responsible',
                    'project_invoices_comment',
                    'project_estimate_responsible',
                    'project_estimate_item_article',
                    'project_estimate_item_income_price',
                    'project_estimate_item_income_sum',
                    'project_estimate_item_sum_diff',
                    'project_estimate_name',
                    'project_money_income_expense',
                    'project_money_recognition_date',
                    'project_money_paying',
                    'project_money_project',
                    'project_money_sale_point',
                    'project_money_industry',
                    'project_odds',
                    'project_opiu',
                    'project_odds_collapse',
                    'project_opiu_collapse',
                    'crm_client',
                    'contractor_activity',
                    'crm_client_check',
                    'crm_client_reason',
                    'contractor_campaign',
                    'crm_client_created',
                    'crm_contact_name',
                    'crm_client_position',
                    'crm_contact_email',
                    'crm_deal',
                    'crm_deal_type',
                    'crm_deal_stage',
                    'crm_task',
                    'crm_task_date_end',
                    'crm_task_number',
                    'crm_task_time',
                    'crm_client_type',
                    'crm_task_contractor',
                    'crm_task_contact',
                    'crm_task_index_view',
                    'crm_task_description',
                    'crm_task_result',
                    'crm_task_hide_completed',
                    'report_selling_speed_unit',
                    'report_selling_speed_article',
                    'report_selling_speed_group',
                    'report_selling_speed_storage',
                    'rent_categories',
                    'rent_statuses',
                    'rent_responsible',
                    'selling_speed_help',
                    'selling_speed_chart',
                    'what_if_help',
                    'what_if_chart',
                    'scenario_analysis_help',
                    'scenario_analysis_chart',
                    'report_odds_own_funds',
                    'report_plan_fact_expanded',
                    'table_view_finance_model',
                    'finance_model_revenue',
                    'finance_model_net_profit',
                    'finance_model_margin',
                    'finance_model_sales_profit',
                    'finance_model_capital_profit',
                    'finance_model_comment',
                    'finance_model_employee',
                    'finance_model_tab_help',
                    'finance_model_tab_chart',
                    'finance_model_round_numbers',
                    'finance_model_amortization',
                    'finance_model_ebit',
                    'finance_model_percent_received',
                    'finance_model_percent_paid',
                    'finance_model_other_income',
                    'finance_model_other_expense',
                    'theme_wide',
                    'minimize_side_menu',
                    'cash_column_paying',
                    'cash_column_income_expense',
                    'cash_column_project',
                    'cash_column_priority',
                    'cash_column_wallet',
                    'cash_column_recognition_date',
                    'cash_operations_help',
                    'cash_operations_chart',
                    'cash_operations_chart_period',
                    'logistics_request_unloading_date',
                    'logistics_request_goods',
                    'logistics_request_customer_debt',
                    'logistics_request_carrier_debt',
                    'logistics_request_ttn',
                    'logistics_request_author_id',
                    'card_operation_table',
                    'credits_credit_table',
                    'credits_flow_table',
                    'credits_debt_table',
                    'credits_credit_first_date',
                    'credits_credit_amount',
                    'credits_currency_id',
                    'credits_contractor_id',
                    'credits_credit_type',
                    'credits_debt_amount',
                    'credits_debt_repaid',
                    'credits_interest_repaid',
                    'credits_interest_amount',
                    'order_many_create_recognition_date',
                    'order_many_create_invoices_list',
                    'order_many_create_project',
                    'order_many_create_sale_point',
                    'order_many_create_direction',
                    'cash_pieces_recognition_date',
                    'cash_pieces_invoices_list',
                    'cash_pieces_project',
                    'cash_pieces_sale_point',
                    'cash_pieces_direction',
                    'bank_column_income_expense',
                    'bank_column_paying',
                    'bank_column_project',
                    'order_column_income_expense',
                    'order_column_paying',
                    'order_column_project',
                    'emoney_column_income_expense',
                    'emoney_column_paying',
                    'emoney_column_project',
                    'card_column_income_expense',
                    'card_column_project',
                    'acquiring_column_income_expense',
                    'acquiring_column_project',
                    'cash_index_hide_plan',
                    'table_retail_sales_view',
                    'table_retail_sales_sort',
                    'table_retail_sales_zeroes',
                    'table_retail_receipt_expense',
                    'table_retail_receipt_payment',
                    'table_retail_receipt_store',
                    'table_retail_receipt_kkt',
                    'table_retail_receipt_number',
                    'table_retail_receipt_document',
                    'table_retail_receipt_shift',
                    'retail_sales_help',
                    'retail_sales_chart',
                    'report_odds_row_finance',
                    'report_marketing_planning_round_numbers',
                    'report_operational_efficiency_row_profit_and_loss',
                    'report_operational_efficiency_row_msfo_profit',
                    'report_operational_efficiency_row_revenue_profitability',
                    'report_operational_efficiency_row_return_on_investment',
                    'selling_report_search_by',
                    'selling_report_type',
                    'selling_report_by_contractor',
                    'selling_report_percent',
                    'selling_report_percent_total',
                    'report_odds_row_investments',
                    'report_odds_row_bank',
                    'report_odds_row_order',
                    'report_odds_row_emoney',
                    'report_odds_row_acquiring',
                    'report_odds_row_card',
                    'report_odds_hide_zeroes',
                    'report_odds_income_percent',
                    'report_odds_expense_percent',
                    'report_odds_expense_percent_2',
                    'marketing_plan_by_budget',
                    'marketing_plan_by_total_expenses',
                    'report_odds_expense_percent_2',
                    'goods_cancellation_responsible',
                    'goods_cancellation_pal_type',
                    'goods_cancellation_estimate',
                    'goods_cancellation_project',
                    'goods_cancellation_contractor',
                    'cash_column_sale_point',
                    'bank_column_sale_point',
                    'order_column_sale_point',
                    'emoney_column_sale_point',
                    'acquiring_column_sale_point',
                    'card_column_sale_point',
                    'cash_column_industry',
                    'bank_column_industry',
                    'bank_column_recognition_date',
                    'order_column_industry',
                    'emoney_column_industry',
                    'acquiring_column_industry',
                    'card_column_industry',
                    'table_view_finance_plan',
                    'finance_plan_date',
                    'finance_plan_revenue',
                    'finance_plan_currency',
                    'finance_plan_net_profit',
                    'finance_plan_margin',
                    'finance_plan_comment',
                    'finance_plan_employee',
                    'finance_plan_period',
                    'finance_plan_view_chart',
                    'finance_plan_view_help',
                    'contractor_sale_point',
                    'contractor_industry',
                    'invoice_sale_point',
                    'invoice_industry',
                    'invoice_project',
                    'act_sale_point',
                    'act_industry',
                    'act_project',
                    'packing_list_sale_point',
                    'packing_list_industry',
                    'packing_list_project',
                    'upd_sale_point',
                    'upd_industry',
                    'upd_project',
                    'invoice_facture_sale_point',
                    'invoice_facture_industry',
                    'invoice_facture_project',
                    'report_pc_project',
                    'report_pc_sale_point',
                    'report_pc_industry',
                    'report_pc_description',
                    'payments_request_payments_count',
                    'payments_request_payments_status',
                    'payments_request_author',
                    'payments_request_date',
                    'table_view_payments_request',
                    'payments_request_order_invoice',
                    'payments_request_order_project',
                    'payments_request_order_sale_point',
                    'payments_request_order_industry',
                    'payments_request_order_priority',
                    'payments_request_order_responsible',
                    'report_detailing_industry_date',
                    'report_detailing_industry_type',
                    'report_detailing_industry_income',
                    'report_detailing_industry_expense',
                    'report_detailing_industry_revenue',
                    'report_detailing_industry_margin',
                    'report_detailing_industry_net',
                    'report_detailing_industry_profit',
                    'detailing_index_chart_type',
                    'rent_entity_id',
                    'rent_entity_created',
                    'rent_entity_category',
                    'rent_entity_additional_status',
                    'rent_entity_ownership',
                    'debts_cash_config_hide_zero',
                    'debts_cash_config_show_by_account',
                    'balance_article_purchased_at',
                    'balance_article_category',
                    'balance_article_amount',
                    'balance_article_useful_life_in_month',
                    'balance_article_month_in_use',
                    'balance_article_month_left',
                    'balance_article_write_off_date',
                    'balance_article_status',
                    'document_date',
                    'document_name',
                    'document_type_id',
                    'agreement_template_id',
                ],
                'filter',
                'filter' => function ($value) {
                    return (int)$value;
                },
            ],
            [
                [
                    'invoice_scan',
                    'invoice_paylimit',
                    'invoice_paydate',
                    'invoice_act',
                    'invoice_paclist',
                    'invoice_waybill',
                    'invoice_invfacture',
                    'invoice_upd',
                    'invoice_author',
                    'invoice_comment',
                    'invoice_expenditure_item',
                    'invoice_income_item',
                    'act_order_sum_without_nds',
                    'act_order_sum_nds',
                    'act_contractor_inn',
                    'act_payment_date',
                    'act_comment',
                    'act_responsible',
                    'act_source',
                    'act_scan',
                    'contr_inv_scan',
                    'contr_inv_paylimit',
                    'contr_inv_paydate',
                    'contr_inv_act',
                    'contr_inv_paclist',
                    'contr_inv_waybill',
                    'contr_inv_invfacture',
                    'contr_inv_upd',
                    'contr_inv_author',
                    'contr_inv_comment',
                    'contr_inv_income_item',
                    'contr_inv_expenditure_item',
                    'contr_inv_industry',
                    'contr_inv_sale_point',
                    'contr_inv_project',
                    'contractor_contact',
                    'contractor_group_name',
                    'contractor_phone',
                    'contractor_agreement',
                    'contractor_payment_delay',
                    'contractor_notpaidcount',
                    'contractor_notpaidsum',
                    'contractor_paidcount',
                    'contractor_paidsum',
                    'contractor_new_invoice',
                    'contractor_prepaymentsum',
                    'contractor_balancesum',
                    'contractor_docs_balancesum',
                    'contractor_income_item',
                    'contractor_expenditure_item',
                    'contractor_autionvoice',
                    'contractor_responsible',
                    'order_document_product_article',
                    'order_document_product_reserve',
                    'order_document_product_quantity',
                    'order_document_product_weigh',
                    'order_document_product_volume',
                    'order_document_payment_sum',
                    'order_document_supplier_document_date',
                    'order_document_shipped',
                    'order_document_reserve',
                    'order_document_ship_up_to_date',
                    'order_document_stock_id',
                    'order_document_invoice',
                    'order_document_act',
                    'order_document_packing_list',
                    'order_document_invoice_facture',
                    'order_document_responsible_employee_id',
                    'order_document_detailing',
                    'order_document_upd',
                    'product_image',
                    'product_comment',
                    'product_title_en',
                    'product_article',
                    'product_group',
                    'product_zeroes_quantity',
                    'product_zeroes_turnover',
                    'product_purchase_price',
                    'product_purchase_amount',
                    'product_custom_field',
                    'product_category_fields',
                    'product_unit_type',
                    'invoice_form_article',
                    'payment_reminder_message_contractor_invoice_sum',
                    'payment_reminder_message_contractor_message_1',
                    'payment_reminder_message_contractor_message_2',
                    'payment_reminder_message_contractor_message_3',
                    'payment_reminder_message_contractor_message_4',
                    'payment_reminder_message_contractor_message_5',
                    'payment_reminder_message_contractor_message_6',
                    'payment_reminder_message_contractor_message_7',
                    'payment_reminder_message_contractor_message_8',
                    'payment_reminder_message_contractor_message_9',
                    'payment_reminder_message_contractor_message_10',
                    'agreement_scan',
                    'agreement_responsible_employee_id',
                    'agreement_template_responsible_employee_id',
                    'crm_client_comment',
                    'report_stocks_help',
                    'report_stocks_chart',
                    'report_break_even_help',
                    'report_break_even_chart',
                    'report_odds_help',
                    'report_odds_chart',
                    'report_pc_help',
                    'report_pc_chart',
                    'report_pc_priority',
                    'report_expenses_help',
                    'report_expenses_chart',
                    'report_income_help',
                    'report_income_chart',
                    'report_plan_fact_help',
                    'report_plan_fact_chart',
                    'report_profit_loss_help',
                    'report_profit_loss_chart',
                    'report_operational_efficiency_chart',
                    'report_operational_efficiency_help',
                    'report_balance_help',
                    'report_balance_chart',
                    'report_debtor_help',
                    'report_debtor_chart',
                    'product_abc_help',
                    'product_abc_chart',
                    'table_rent_list',
                    'table_project_estimate',
                    'table_project_estimate_item',
                    'table_view_payment_order',
                    'table_view_autoinvoice',
                    'table_view_invoice_facture',
                    'turnover_group_id',
                    'turnover_unit_id',
                    'table_view_rent_type',
                    'table_view_rent_agreement',
                    'table_view_rent_invoice',
                    'marketing_help',
                    'marketing_chart',
                    'table_view_marketing',
                    'table_view_marketing_planning',
                    'table_view_yandex_direct_by_campaign',
                    'table_view_yandex_direct_by_ad_group',
                    'table_view_yandex_direct_by_ad',
                    'table_view_google_ads_by_campaign',
                    'table_view_google_ads_by_ad_group',
                    'table_view_google_ads_by_ad',
                    'table_view_vk_ads_by_campaign',
                    'table_view_vk_ads_by_ad_group',
                    'table_view_logistics',
                    'product_category_field_isbn',
                    'product_category_field_author',
                    'product_category_field_publishing_year',
                    'product_category_field_publishing_house',
                    'product_category_field_format',
                    'product_category_field_book_binding',
                    'report_abc_article',
                    'report_abc_cost_price',
                    'report_abc_group_margin',
                    'report_abc_number_of_sales',
                    'report_abc_average_check',
                    'report_abc_comment',
                    'report_abc_purchase_price',
                    'report_abc_selling_price',
                    'report_abc_show_groups',
                    'yandex_direct_status',
                    'yandex_direct_strategy',
                    'yandex_direct_placement',
                    'yandex_direct_budget',
                    'project_number',
                    'project_direction',
                    'project_clients',
                    'project_end_date',
                    'project_before_date',
                    'project_result_sum',
                    'project_income',
                    'project_expense',
                    'project_profit',
                    'project_profitability',
                    'project_responsible',
                    'project_invoices_scan',
                    'project_invoices_paylimit',
                    'project_invoices_paydate',
                    'project_invoices_act',
                    'project_invoices_paclist',
                    'project_invoices_invfacture',
                    'project_invoices_upd',
                    'project_invoices_responsible',
                    'project_invoices_comment',
                    'project_estimate_responsible',
                    'project_estimate_item_article',
                    'project_estimate_item_income_price',
                    'project_estimate_item_income_sum',
                    'project_estimate_item_sum_diff',
                    'project_estimate_name',
                    'project_money_income_expense',
                    'project_money_recognition_date',
                    'project_money_paying',
                    'project_money_project',
                    'project_money_sale_point',
                    'project_money_industry',
                    'project_odds',
                    'project_opiu',
                    'project_odds_collapse',
                    'project_opiu_collapse',
                    'crm_client',
                    'contractor_activity',
                    'crm_client_check',
                    'crm_client_reason',
                    'contractor_campaign',
                    'crm_client_created',
                    'crm_contact_name',
                    'crm_client_position',
                    'crm_contact_email',
                    'crm_deal',
                    'crm_deal_type',
                    'crm_deal_stage',
                    'crm_task',
                    'crm_task_date_end',
                    'crm_task_number',
                    'crm_task_time',
                    'crm_client_type',
                    'crm_task_contractor',
                    'crm_task_contact',
                    'crm_task_index_view',
                    'crm_task_description',
                    'crm_task_result',
                    'crm_task_hide_completed',
                    'report_selling_speed_unit',
                    'report_selling_speed_article',
                    'report_selling_speed_group',
                    'report_selling_speed_storage',
                    'rent_categories',
                    'rent_statuses',
                    'rent_responsible',
                    'selling_speed_help',
                    'selling_speed_chart',
                    'what_if_help',
                    'what_if_chart',
                    'scenario_analysis_help',
                    'scenario_analysis_chart',
                    'report_odds_own_funds',
                    'report_plan_fact_expanded',
                    'finance_model_revenue',
                    'finance_model_net_profit',
                    'finance_model_margin',
                    'finance_model_sales_profit',
                    'finance_model_capital_profit',
                    'finance_model_comment',
                    'finance_model_employee',
                    'finance_model_tab_help',
                    'finance_model_tab_chart',
                    'finance_model_round_numbers',
                    'finance_model_amortization',
                    'finance_model_ebit',
                    'finance_model_percent_received',
                    'finance_model_percent_paid',
                    'finance_model_other_income',
                    'finance_model_other_expense',
                    'theme_wide',
                    'minimize_side_menu',
                    'cash_column_paying',
                    'cash_column_income_expense',
                    'cash_column_project',
                    'cash_column_priority',
                    'cash_column_wallet',
                    'cash_column_recognition_date',
                    'cash_operations_help',
                    'cash_operations_chart',
                    'cash_operations_chart_period',
                    'logistics_request_unloading_date',
                    'logistics_request_goods',
                    'logistics_request_customer_debt',
                    'logistics_request_carrier_debt',
                    'logistics_request_ttn',
                    'logistics_request_author_id',
                    'card_operation_table',
                    'credits_credit_table',
                    'credits_flow_table',
                    'credits_debt_table',
                    'credits_credit_first_date',
                    'credits_credit_amount',
                    'credits_currency_id',
                    'credits_contractor_id',
                    'credits_credit_type',
                    'credits_debt_amount',
                    'credits_debt_repaid',
                    'credits_interest_repaid',
                    'credits_interest_amount',
                    'order_many_create_recognition_date',
                    'order_many_create_invoices_list',
                    'order_many_create_project',
                    'order_many_create_sale_point',
                    'order_many_create_direction',
                    'cash_pieces_recognition_date',
                    'cash_pieces_invoices_list',
                    'cash_pieces_project',
                    'cash_pieces_sale_point',
                    'cash_pieces_direction',
                    'bank_column_income_expense',
                    'bank_column_paying',
                    'bank_column_project',
                    'order_column_income_expense',
                    'order_column_paying',
                    'order_column_project',
                    'emoney_column_income_expense',
                    'emoney_column_paying',
                    'emoney_column_project',
                    'card_column_income_expense',
                    'card_column_project',
                    'acquiring_column_income_expense',
                    'acquiring_column_project',
                    'cash_index_hide_plan',
                    'table_retail_sales_view',
                    'table_retail_sales_sort',
                    'table_retail_sales_zeroes',
                    'table_retail_receipt_expense',
                    'table_retail_receipt_payment',
                    'table_retail_receipt_store',
                    'table_retail_receipt_kkt',
                    'table_retail_receipt_number',
                    'table_retail_receipt_document',
                    'table_retail_receipt_shift',
                    'retail_sales_help',
                    'retail_sales_chart',
                    'report_odds_row_finance',
                    'report_marketing_planning_round_numbers',
                    'report_operational_efficiency_row_profit_and_loss',
                    'report_operational_efficiency_row_msfo_profit',
                    'report_operational_efficiency_row_revenue_profitability',
                    'report_operational_efficiency_row_return_on_investment',
                    'selling_report_search_by',
                    'selling_report_type',
                    'selling_report_by_contractor',
                    'selling_report_percent',
                    'selling_report_percent_total',
                    'report_odds_row_investments',
                    'report_odds_row_bank',
                    'report_odds_row_order',
                    'report_odds_row_emoney',
                    'report_odds_row_acquiring',
                    'report_odds_row_card',
                    'report_odds_hide_zeroes',
                    'report_odds_income_percent',
                    'report_odds_expense_percent',
                    'report_odds_expense_percent_2',
                    'marketing_plan_by_budget',
                    'marketing_plan_by_total_expenses',
                    'report_odds_expense_percent_2',
                    'goods_cancellation_responsible',
                    'goods_cancellation_pal_type',
                    'goods_cancellation_estimate',
                    'goods_cancellation_project',
                    'goods_cancellation_contractor',
                    'cash_column_sale_point',
                    'bank_column_sale_point',
                    'order_column_sale_point',
                    'emoney_column_sale_point',
                    'acquiring_column_sale_point',
                    'card_column_sale_point',
                    'cash_column_industry',
                    'bank_column_industry',
                    'bank_column_recognition_date',
                    'order_column_industry',
                    'emoney_column_industry',
                    'acquiring_column_industry',
                    'card_column_industry',
                    'table_view_finance_plan',
                    'finance_plan_date',
                    'finance_plan_revenue',
                    'finance_plan_currency',
                    'finance_plan_net_profit',
                    'finance_plan_margin',
                    'finance_plan_comment',
                    'finance_plan_employee',
                    'finance_plan_period',
                    'finance_plan_view_chart',
                    'finance_plan_view_help',
                    'contractor_sale_point',
                    'contractor_industry',
                    'invoice_sale_point',
                    'invoice_industry',
                    'invoice_project',
                    'act_sale_point',
                    'act_industry',
                    'act_project',
                    'packing_list_sale_point',
                    'packing_list_industry',
                    'packing_list_project',
                    'packing_list_scan',
                    'upd_sale_point',
                    'upd_industry',
                    'upd_project',
                    'upd_scan',
                    'invoice_facture_sale_point',
                    'invoice_facture_industry',
                    'invoice_facture_project',
                    'report_pc_project',
                    'report_pc_sale_point',
                    'report_pc_industry',
                    'report_pc_description',
                    'payments_request_payments_count',
                    'payments_request_payments_status',
                    'payments_request_author',
                    'payments_request_date',
                    'payments_request_order_invoice',
                    'payments_request_order_project',
                    'payments_request_order_sale_point',
                    'payments_request_order_industry',
                    'payments_request_order_priority',
                    'payments_request_order_responsible',
                    'report_detailing_industry_date',
                    'report_detailing_industry_type',
                    'report_detailing_industry_type',
                    'report_detailing_industry_income',
                    'report_detailing_industry_expense',
                    'report_detailing_industry_revenue',
                    'report_detailing_industry_margin',
                    'report_detailing_industry_net',
                    'report_detailing_industry_profit',
                    'rent_entity_id',
                    'rent_entity_created',
                    'rent_entity_category',
                    'rent_entity_additional_status',
                    'rent_entity_ownership',
                    'debts_cash_config_hide_zero',
                    'debts_cash_config_show_by_account',
                    'balance_article_purchased_at',
                    'balance_article_category',
                    'balance_article_amount',
                    'balance_article_useful_life_in_month',
                    'balance_article_month_in_use',
                    'balance_article_month_left',
                    'balance_article_write_off_date',
                    'balance_article_status',
                    'document_date',
                    'document_name',
                    'document_type_id',
                    'agreement_template_id',
                ],
                'integer',
            ],
            [
                [
                    'table_view_document',
                    'table_view_detailing',
                    'table_view_contractor_list',
                    'table_view_contractor',
                    'table_view_cash',
                    'table_view_product',
                    'table_view_finance_odds',
                    'table_view_finance_plan_fact',
                    'table_view_report_client',
                    'table_view_report_employee',
                    'table_view_report_invoice',
                    'table_view_report_supplier',
                    'table_view_finance_profit_loss',
                    'table_view_finance_balance',
                    'table_view_operational_efficiency',
                    'table_view_finance_expenses',
                    'table_view_finance_stocks',
                    'table_view_finance_income',
                    'table_view_finance_pc',
                    'table_view_project_money',
                    'table_view_project_invoice',
                    'table_view_acquiring',
                    'report_pc_chart_period',
                    'report_odds_chart_period',
                    'table_view_report_abc',
                    'table_view_logistics',
                    'table_selling_speed',
                    'table_view_finance_model',
                    'report_odds_active_tab',
                    'report_debtor_period_type',
                    'table_view_payments_request',
                    'detailing_index_chart_type'
                ],
                'integer'
            ],
            [
                [
                    'page_size',
                ],
                'integer'
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'employee_id' => 'Employee ID',
            'invoice_scan' => 'Скан',
            'invoice_paylimit' => 'Оплатить до',
            'invoice_paydate' => 'Дата оплаты',
            'invoice_act' => 'Акт',
            'invoice_paclist' => 'Товарная накладная',
            'invoice_waybill' => 'Транспортная накладная',
            'invoice_invfacture' => 'Счет-фактура',
            'invoice_upd' => 'УПД',
            'invoice_author' => 'Ответственный',
            'invoice_comment' => 'Комментарий',
            'invoice_expenditure_item' => 'Статья расходов',
            'invoice_income_item' => 'Статья приходов',
            'act_order_sum_without_nds' => 'Сумма актов без НДС',
            'act_order_sum_nds' => 'Сумма НДС актов',
            'act_contractor_inn' => 'ИНН контрагента',
            'act_payment_date' => 'Дата оплаты',
            'act_comment' => 'Комментарий',
            'act_responsible' => 'Ответственный',
            'act_source'  => 'Источник',
            'act_scan' => 'Скан',
            'packing_list_order_sum_without_nds' => 'Сумма ТН без НДС',
            'packing_list_order_sum_nds' => 'Сумма НДС ТН',
            'packing_list_contractor_inn' => 'ИНН контрагента',
            'packing_list_payment_date' => 'Дата оплаты',
            'packing_list_responsible' => 'Ответственный',
            'packing_list_source'  => 'Источник',
            'packing_list_scan'  => 'Скан',
            'sales_invoice_order_sum_without_nds' => 'Сумма РН без НДС',
            'sales_invoice_order_sum_nds' => 'Сумма НДС РН',
            'sales_invoice_contractor_inn' => 'ИНН контрагента',
            'sales_invoice_payment_date' => 'Дата оплаты',
            'sales_invoice_responsible' => 'Ответственный',
            'sales_invoice_source'  => 'Источник',
            'upd_order_sum_without_nds' => 'Сумма УПД без НДС',
            'upd_order_sum_nds' => 'Сумма НДС УПД',
            'upd_contractor_inn' => 'ИНН контрагента',
            'upd_payment_date' => 'Дата оплаты',
            'upd_responsible' => 'Ответственный',
            'upd_source'  => 'Источник',
            'upd_scan'  => 'Скан',
            'act_stat_by_payment' => 'Вывод по платежам',
            'act_stat_by_status' => 'Вывод по статусу',
            'act_stat_by_calculate_nds' => 'Подсчет НДС',
            'contr_inv_scan' => 'Скан',
            'contr_inv_paylimit' => 'Оплатить до',
            'contr_inv_paydate' => 'Дата оплаты',
            'contr_inv_act' => 'Акт',
            'contr_inv_paclist' => 'Товарная накладная',
            'contr_inv_waybill' => 'Транспортная накладная',
            'contr_inv_invfacture' => 'Счет-фактура',
            'contr_inv_upd' => 'УПД',
            'contr_inv_author' => 'Ответственный',
            'contr_inv_comment' => 'Комментарий',
            'contr_inv_income_item' => 'Статья приходов',
            'contr_inv_expenditure_item' => 'Статья расходов',
            'contr_inv_industry' => 'Направление',
            'contr_inv_sale_point' => 'Точка продаж',
            'contr_inv_project' => 'Проект',
            'contractor_contact' => 'Контакт',
            'contractor_group_name' => 'Группа',
            'contractor_phone' => 'Телефон',
            'contractor_agreement' => 'Договор',
            'contractor_payment_delay' => 'Отсрочка дн.',
            'contractor_notpaidcount' => 'Счетов не оплачено',
            'contractor_notpaidsum' => 'Не оплачено счетов на сумму',
            'contractor_paidcount' => 'Счетов оплачено',
            'contractor_paidsum' => 'Оплачено счетов на сумму',
            'contractor_new_invoice' => 'Выставить счет',
            'contractor_prepaymentsum' => 'Оплата без привязки',
            'contractor_balancesum' => 'Сальдо (Деньги - Счета)',
            'contractor_docs_balancesum' => 'Сальдо (Деньги - Акты/ТН/УПД)',
            'contractor_income_item' => 'Статья приходов',
            'contractor_expenditure_item' => 'Статья расходов',
            'contractor_autionvoice' => 'АвтоСчет',
            'contractor_responsible' => 'Ответственный',
            'order_document_product_article' => 'Артикул',
            'order_document_product_reserve' => 'Резерв',
            'order_document_product_quantity' => 'Остаток',
            'order_document_product_weigh' => 'Вес',
            'order_document_product_volume' => 'Объем',
            'order_document_payment_sum' => 'Оплачено',
            'order_document_supplier_document_date' => 'Дата заказа',
            'order_document_shipped' => 'Отгружено',
            'order_document_reserve' => 'Резерв',
            'order_document_ship_up_to_date' => 'План. дата отгрузки',
            'order_document_stock_id' => 'Со склада',
            'order_document_invoice' => 'Счет',
            'order_document_act' => 'Акт',
            'order_document_packing_list' => 'ТН',
            'order_document_sales_invoice' => ' РН',
            'order_document_invoice_facture' => 'СФ',
            'order_document_responsible_employee_id' => 'Ответственный',
            'order_document_detailing' => 'Детализация',
            'order_document_upd' => 'УПД',
            'product_image' => 'Картинка',
            'product_comment' => 'Описание',
            'product_title_en' => 'Name in English',
            'product_article' => 'Артикул',
            'product_group' => 'Группа товара',
            'product_zeroes_quantity' => 'Товары с нулевыми остатками',
            'product_zeroes_turnover' => 'Товары с нулевыми оборотами',
            'invoice_form_article' => 'Артикул',
            'product_purchase_price' => 'Цена покупки',
            'product_purchase_amount' => 'Стоимость закупки',
            'product_custom_field' => ProductField::getLabel(),
            'product_category_fields' => 'Поля категории',
            'payment_reminder_message_contractor_invoice_sum' => 'Долг',
            'payment_reminder_message_contractor_message_1' => 'Письмо №1',
            'payment_reminder_message_contractor_message_2' => 'Письмо №2',
            'payment_reminder_message_contractor_message_3' => 'Письмо №3',
            'payment_reminder_message_contractor_message_4' => 'Письмо №4',
            'payment_reminder_message_contractor_message_5' => 'Письмо №5',
            'payment_reminder_message_contractor_message_6' => 'Письмо №6',
            'payment_reminder_message_contractor_message_7' => 'Письмо №7',
            'payment_reminder_message_contractor_message_8' => 'Письмо №8',
            'payment_reminder_message_contractor_message_9' => 'Письмо №9',
            'payment_reminder_message_contractor_message_10' => 'Письмо №10',
            'agreement_scan' => 'Скан',
            'agreement_responsible_employee_id' => 'Ответственный',
            'agreement_template_responsible_employee_id' => 'Ответственный',
            'crm_client_comment' => 'Комментарий',
            'table_view_document' => 'Вид таблицы документов',
            'table_view_detailing' => 'Вид таблицы Аналитика/Финансы/Детализация',
            'table_view_contractor_list' => 'Вид таблицы списка контрагентов',
            'table_view_contractor' => 'Вид таблицы контрагента',
            'table_view_cash' => 'Вид таблицы банков/касс',
            'table_view_product' => 'Вид таблицы товаров/услуг',
            'table_view_finance_odds' => 'Вид таблицы ОДДС',
            'table_view_finance_plan_fact' => 'Вид таблицы План-Факт',
            'table_view_price_list' => 'Вид таблицы прайс-листов',
            'table_view_report_client' => 'Вид таблицы отчетов по клиентам',
            'table_view_report_employee' => 'Вид таблицы отчетов по сотрудникам',
            'table_view_report_invoice' => 'Вид таблицы отчетов по счетам',
            'table_view_report_supplier' => 'Вид таблицы отчетов по поставщикам',
            'table_view_finance_pc' => 'Вид таблицы Платежный Календарь',
            'table_view_article' => 'Вид таблицы статей',
            'table_view_project_money' => 'Вид таблицы денег проекта',
            'table_view_project_invoice' => 'Вид таблицы счетов проекта',
            'table_view_acquiring' => 'Вид таблицы интернет-эквайринга',
            'table_view_logistics' => 'Вид таблицы логистики',
            'page_size' => 'Кол-во записей на странице',
            'report_stocks_help' => 'Панель на странице Запасы',
            'report_stocks_chart' => 'Панель на странице Запасы',
            'report_break_even_help' => 'Панель на странице Точка Безубыточности',
            'report_break_even_chart' => 'Панель на странице Точка Безубыточности',
            'report_odds_help' => 'Панель на странице ОДДС',
            'report_odds_chart' => 'Панель на странице ОДДС',
            'report_odds_active_tab' => 'Активная вкладка на странице ОДДС',
            'report_pc_help' => 'Панель на странице Платежный Календарь',
            'report_pc_chart' => 'Панель на странице Платежный Календарь',
            'report_pc_priority' => 'Приоритет в оплате',
            'report_expenses_help' => 'Панель на странице Расходы',
            'report_expenses_chart' => 'Панель на странице Расходы',
            'report_debtor_help' => 'Панель на странице Отчет по поставщикам/клиентам',
            'report_debtor_chart' => 'Панель на странице Отчет по поставщикам/клиентам',
            'report_debtor_period_type' => 'Отчет "Нам должны -> Деньги -> По срокам", поиск по док-ам / счетам',
            'report_income_help' => 'Панель на странице Приходы',
            'report_income_chart' => 'Панель на странице Приходы',
            'report_plan_fact_help' => 'Панель на странице План-Факт',
            'report_plan_fact_chart' => 'Панель на странице План-Факт',
            'marketing_help' => 'Панель на странице маркетинг',
            'marketing_chart' => 'Панель на странице маркетинг',
            'table_view_marketing' => 'Вид таблицы маркетинг',
            'table_view_marketing_planning' => 'Вид таблицы прогноз по маркетингу',
            'table_view_yandex_direct_by_campaign' => 'Вид таблицы аналитики Яндекс.Директ по кампаниям',
            'table_view_yandex_direct_by_ad_group' => 'Вид таблицы аналитики Яндекс.Директ по группам объявлений',
            'table_view_yandex_direct_by_ad' => 'Вид таблицы аналитики Яндекс.Директ по объявлениям',
            'table_view_google_ads_by_campaign' => 'Вид таблицы аналитики Google Ads по кампаниям',
            'table_view_google_ads_by_ad_group' => 'Вид таблицы аналитики Google Ads по группам объявлений',
            'table_view_google_ads_by_ad' => 'Вид таблицы аналитики Google Ads по объявлениям',
            'table_view_vk_ads_by_campaign' => 'Вид таблицы аналитики Vk Ads по кампаниям',
            'table_view_vk_ads_by_ad_group' => 'Вид таблицы аналитики Vk Ads по группам объявлений',
            'product_category_field_isbn' => 'ISBN',
            'product_category_field_author' => 'Автор',
            'product_category_field_publishing_year' => 'Год издания',
            'product_category_field_publishing_house' => 'Издательство',
            'product_category_field_format' => 'Формат',
            'product_category_field_book_binding' => 'Переплет',
            'table_view_report_abc' => 'ABC анализ',
            'report_abc_article' => 'Артикул',
            'report_abc_cost_price' => 'Себестоимость',
            'report_abc_group_margin' => 'Доля маржи в группе',
            'report_abc_number_of_sales' => 'Кол-во продаж',
            'report_abc_average_check' => 'Средний чек',
            'report_abc_comment' => 'Комментарий',
            'report_abc_purchase_price' => 'Цена покупки',
            'report_abc_selling_price' => 'Цена продажи',
            'report_abc_show_groups' => 'Выводить по группам',
            'project_number' => '№ проекта',
            'project_direction' => 'Направление',
            'project_clients' => 'Заказчик',
            'project_end_date' => 'Дата завершения проекта',
            'yandex_direct_status' => 'Статус',
            'yandex_direct_strategy' => 'Стратегия',
            'yandex_direct_placement' => 'Место показов',
            'yandex_direct_budget' => 'Бюджет',
            'project_before_date' => 'Дней до завершения проекта',
            'project_income' => 'Приход',
            'project_expense' => 'Расход',
            'project_result_sum' => 'Выручка',
            'project_profit' => 'Прибыль',
            'project_profitability' => 'Рентабельность',
            'project_responsible' => 'Ответственный',
            'project_invoices_scan' => 'Скан',
            'project_invoices_paylimit' => 'Оплатить до',
            'project_invoices_paydate' => 'Дата платежа',
            'project_invoices_act' => 'Акт',
            'project_invoices_paclist' => 'Товарная накладная',
            'project_invoices_invfacture' => 'Счет фактура',
            'project_invoices_upd' => 'УПД',
            'project_invoices_responsible' => 'Ответственный',
            'project_invoices_comment' => 'Коментарии',
            'project_estimate_responsible' => 'Ответственный сметы',
            'project_estimate_item_article' => 'Артикул',
            'project_estimate_item_income_price' => 'Цена закупки',
            'project_estimate_item_income_sum' => 'Сумма закупки',
            'project_estimate_item_sum_diff' => 'Разница',
            'project_estimate_name' => 'Название сметы',
            'project_money_income_expense' => 'Вместо "Приход" и "Расход" столбец "Сумма"',
            'project_money_recognition_date' => 'Дата признания',
            'project_money_paying' => 'Опл.счета',
            'project_money_project' => 'Проект',
            'project_money_sale_point' => 'Точка продаж',
            'project_money_industry' => 'Направление',
            'project_odds' => 'ОДДС',
            'project_opiu' => 'ОПиУ',
            'report_selling_speed_unit' => 'Ед. измерения',
            'report_selling_speed_article' => 'Артикул',
            'report_selling_speed_group' => 'Группа товаров',
            'report_selling_speed_storage' => 'Склад',
            'rent_categories' => 'Категории аренды',
            'rent_statuses' => 'Статус аренды',
            'rent_responsible' => 'Ответственный',
            'selling_speed_help' => 'Описание отчета скорость продаж',
            'selling_speed_chart' => 'Графики отчета скорость продаж',
            'report_odds_own_funds' => 'Перемещение денег',
            'report_plan_fact_expanded' => 'Отображать доп. столбцы в отчете План-Факт',
            'finance_model_revenue' => 'Выручка',
            'finance_model_net_profit' => 'Чистая прибыль',
            'finance_model_margin' => 'Маржинальность',
            'finance_model_sales_profit' => 'Рентабельность продаж',
            'finance_model_capital_profit' => 'Рентабельность капитала',
            'finance_model_comment' => 'Комментарий',
            'finance_model_employee' => 'Ответственный',
            'finance_model_tab_help' => 'Описание',
            'finance_model_tab_chart' => 'График',
            'finance_model_round_numbers' => 'Округлять до целого',
            'finance_model_amortization' => 'Амортизация',
            'finance_model_ebit' => 'EBIT',
            'finance_model_other_income' => 'Другие доходы',
            'finance_model_other_expense' => 'Другие расходы',
            'finance_model_percent_received' => 'Проценты полученные',
            'finance_model_percent_paid' => 'Проценты уплаченные',
            'theme_wide' => 'Широкая тема',
            'minimize_side_menu' => 'Свернуть левое меню',
            'cash_column_paying' => 'Опл. счета',
            'cash_column_income_expense' => 'Вместо "Приход" и "Расход" столбец "Сумма"',
            'cash_column_project' => 'Проект',
            'cash_column_priority' => 'Приоритет в оплате',
            'cash_column_wallet' => 'Счета',
            'cash_column_recognition_date' => 'Дата признания',
            'logistics_request_unloading_date' => 'Дата разгрузки',
            'logistics_request_goods' => 'Груз',
            'logistics_request_customer_debt' => 'Долг заказчику',
            'logistics_request_carrier_debt' => 'Долг перевозчику',
            'logistics_request_ttn' => 'ТТН',
            'logistics_request_author_id' => 'Ответственный',
            'credits_credit_first_date' => 'Дата получения',
            'credits_credit_amount' => 'Сумма по договору',
            'credits_currency_id' => 'Валюта',
            'credits_contractor_id' => 'Кредитор',
            'credits_credit_type' => 'Вид кредита',
            'credits_debt_amount' => 'Получено',
            'credits_debt_repaid' => 'Погашено',
            'credits_interest_repaid' => 'Уплачено %%',
            'credits_interest_amount' => 'Сумма %% всего',
            'order_many_create_recognition_date' => 'Дата признания',
            'order_many_create_invoices_list' => 'Оплаченные счета',
            'order_many_create_project' => 'Проект',
            'order_many_create_sale_point' => 'Точка продаж',
            'order_many_create_direction' => 'Направление',
            'cash_pieces_recognition_date' => 'Дата признания',
            'cash_pieces_invoices_list' => 'Оплаченные счета',
            'cash_pieces_project' => 'Проект',
            'cash_pieces_sale_point' => 'Точка продаж',
            'cash_pieces_direction' => 'Направление',
            'bank_column_income_expense' => 'Вместо "Приход" и "Расход" столбец "Сумма"',
            'bank_column_paying' => 'Опл. счета',
            'bank_column_project' => 'Проект',
            'order_column_income_expense' => 'Вместо "Приход" и "Расход" столбец "Сумма"',
            'order_column_paying' => 'Опл. счета',
            'order_column_project' => 'Проект',
            'emoney_column_income_expense' => 'Вместо "Приход" и "Расход" столбец "Сумма"',
            'emoney_column_paying' => 'Опл. счета',
            'emoney_column_project' => 'Проект',
            'card_column_income_expense' => 'Вместо "Приход" и "Расход" столбец "Сумма"',
            'card_column_project' => 'Проект',
            'acquiring_column_income_expense' => 'Вместо "Приход" и "Расход" столбец "Сумма"',
            'acquiring_column_project' => 'Проект',
            'cash_index_hide_plan' => 'Не выводить Плановые операции',
            'table_retail_sales_view' => 'Вид таблицы Розничные продажи/Продажи',
            'table_retail_sales_sort' => 'Сортировка таблицы Розничные продажи/Продажи',
            'table_retail_sales_zeroes' => 'Не выводить столбцы где все нули в таблице Розничные продажи/Продажи',
            'table_retail_receipt_expense' => 'Расход',
            'table_retail_receipt_payment' => 'Тип оплаты',
            'table_retail_receipt_store' => 'Точка продаж',
            'table_retail_receipt_kkt' => 'Наименование ККТ',
            'table_retail_receipt_number' => 'Номер продажи',
            'table_retail_receipt_document' => 'Номер чека',
            'table_retail_receipt_shift' => 'Номер смены',
            'retail_sales_help' => 'Описание',
            'retail_sales_chart' => 'График',
            'report_odds_row_finance' => 'Финансовая деятельность',
            'report_marketing_planning_round_numbers' => 'Округлять до целого',
            'report_operational_efficiency_row_profit_and_loss' => 'Показатели ОПиУ',
            'report_operational_efficiency_row_msfo_profit' => 'Показатели прибыли по МСФО',
            'report_operational_efficiency_row_revenue_profitability' => 'Рентабельность выручки (продаж)',
            'report_operational_efficiency_row_return_on_investment' => 'Рентабельность инвестиций',
            'selling_report_search_by' => 'Выводить по (оплаты / счета)',
            'selling_report_type' => 'Выводить по счетам (все / оплаченные / неоплаченные)',
            'selling_report_by_contractor' => 'Группировать по заказчикам',
            'selling_report_percent' => '%% от предыдущего',
            'selling_report_percent_total' => '% итого',
            'report_odds_row_investments' => 'Инвестиционная деятельность',
            'report_odds_row_bank' => 'Банк',
            'report_odds_row_order' => 'Касса',
            'report_odds_row_emoney' => 'E-money',
            'report_odds_row_acquiring' => 'Интернет-эквайринг',
            'report_odds_row_card' => 'Карты',
            'report_odds_hide_zeroes' => 'Кошельки с нулями',
            'report_odds_income_percent' => 'Проценты',
            'report_odds_expense_percent' => '% от Расхода',
            'report_odds_expense_percent_2' => '%% от Прихода',
            'crm_client_type' => 'Тип отношений',
            'marketing_plan_by_budget' => 'Рекламному бюджету',
            'marketing_plan_by_total_expenses' => 'Итоговым затратам',
            'goods_cancellation_responsible' => 'Ответственный',
            'goods_cancellation_pal_type' => 'Раздел ОПиУ',
            'goods_cancellation_estimate' => 'Смета',
            'goods_cancellation_project' => 'Проект',
            'goods_cancellation_contractor' => 'Контрагент',
            'cash_column_sale_point' => 'Точка продаж',
            'bank_column_sale_point' => 'Точка продаж',
            'order_column_sale_point' => 'Точка продаж',
            'emoney_column_sale_point' => 'Точка продаж',
            'acquiring_column_sale_point' => 'Точка продаж',
            'card_column_sale_point' => 'Точка продаж',
            'finance_plan_date' => 'Дата создания',
            'cash_column_industry' => 'Направление',
            'bank_column_industry' => 'Направление',
            'bank_column_recognition_date' => 'Дата признания',
            'order_column_industry' => 'Направление',
            'emoney_column_industry' => 'Направление',
            'acquiring_column_industry' => 'Направление',
            'card_column_industry' => 'Направление',
            'finance_plan_revenue' => 'Выручка',
            'finance_plan_currency' => 'Валюта',
            'finance_plan_net_profit' => 'Чистая прибыль',
            'finance_plan_margin' => 'Маржинальность',
            'finance_plan_comment' => 'Комментарий',
            'finance_plan_employee' => 'Ответственный',
            'finance_plan_period' => 'Период',
            'finance_plan_view_chart' => 'График',
            'finance_plan_view_help' => 'Панель',
            'contractor_sale_point' => 'Точка продаж',
            'contractor_industry' => 'Направление',
            'invoice_sale_point' => 'Точка продаж',
            'invoice_industry' => 'Направление',
            'invoice_project' => 'Проект',
            'act_sale_point' => 'Точка продаж',
            'act_industry' => 'Направление',
            'act_project' => 'Проект',
            'packing_list_sale_point' => 'Точка продаж',
            'packing_list_industry' => 'Направление',
            'packing_list_project' => 'Проект',
            'upd_sale_point' => 'Точка продаж',
            'upd_industry' => 'Направление',
            'upd_project' => 'Проект',
            'invoice_facture_sale_point' => 'Точка продаж',
            'invoice_facture_industry' => 'Направление',
            'invoice_facture_project' => 'Проект',
            'report_pc_project' => 'Проект',
            'report_pc_sale_point' => 'Точка продаж',
            'report_pc_industry' => 'Направление',
            'report_pc_description' => 'Назначение',
            'payments_request_payments_count' => 'Кол-во платежей',
            'payments_request_payments_status' => 'Статус платежа',
            'payments_request_author' => 'Инициатор',
            'payments_request_date' => 'Дата заявки',
            'payments_request_order_invoice' => 'Счет',
            'payments_request_order_project' => 'Проект',
            'payments_request_order_sale_point' => 'Точка продаж',
            'payments_request_order_industry' => 'Направление',
            'payments_request_order_priority' => 'Приоритет в оплате',
            'payments_request_order_responsible' => 'Ответственный',
            'report_detailing_industry_date' => 'Дата добавления',
            'report_detailing_industry_type' => 'Тип',
            'report_detailing_industry_income' => 'Приход',
            'report_detailing_industry_expense' => 'Расход',
            'report_detailing_industry_revenue' => 'Выручка',
            'report_detailing_industry_margin' => 'Маржа',
            'report_detailing_industry_net' => 'ЧДП',
            'report_detailing_industry_profit' => 'Прибыль',
            'detailing_index_chart_type' => 'По умолчанию открывать',
            'rent_entity_id' => 'ID',
            'rent_entity_created' => 'Дата добавления',
            'rent_entity_category' => 'Категория',
            'rent_entity_additional_status' => 'Доп. статус',
            'rent_entity_ownership' => 'Собственность',
            'balance_article_purchased_at' => 'Дата приобретения',
            'balance_article_category' => 'Вид',
            'balance_article_amount' => 'Стоимость приобретения',
            'balance_article_useful_life_in_month' => 'Срок полезного использования',
            'balance_article_month_in_use' => 'Количество месяцев использования',
            'balance_article_month_left' => 'Осталось месяцев',
            'balance_article_write_off_date' => 'Дата полного списания',
            'balance_article_status' => 'Статус',
            'document_date' => "Дата",
            'document_name' => "Название",
            'document_type_id'=>"Тип документа",
            'agreement_template_id' =>'Шаблон',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if (!$insert && $this->isAttributeChanged('employee_id')) {
                return false;
            }

            $this->product_category_fields = json_encode([
                ProductCategoryField::ISBN => (int)$this->product_category_field_isbn,
                ProductCategoryField::AUTHOR => (int)$this->product_category_field_author,
                ProductCategoryField::PUBLISHING_YEAR => (int)$this->product_category_field_publishing_year,
                ProductCategoryField::PUBLISHING_HOUSE => (int)$this->product_category_field_publishing_house,
                ProductCategoryField::FORMAT => (int)$this->product_category_field_format,
                ProductCategoryField::BOOK_BINDING => (int)$this->product_category_field_book_binding,
            ]);

            return true;
        } else {
            return false;
        }
    }

    public function afterFind()
    {
        parent::afterFind(); // TODO: Change the autogenerated stub

        $rawProductCategoryFields = json_decode($this->product_category_fields);
        $this->product_category_field_isbn = ArrayHelper::getValue($rawProductCategoryFields, ProductCategoryField::ISBN, 0);
        $this->product_category_field_author = ArrayHelper::getValue($rawProductCategoryFields, ProductCategoryField::AUTHOR, 0);
        $this->product_category_field_publishing_year = ArrayHelper::getValue($rawProductCategoryFields, ProductCategoryField::PUBLISHING_YEAR, 0);
        $this->product_category_field_publishing_house = ArrayHelper::getValue($rawProductCategoryFields, ProductCategoryField::PUBLISHING_HOUSE, 0);
        $this->product_category_field_format = ArrayHelper::getValue($rawProductCategoryFields, ProductCategoryField::FORMAT, 0);
        $this->product_category_field_book_binding = ArrayHelper::getValue($rawProductCategoryFields, ProductCategoryField::BOOK_BINDING, 0);
    }

    /**
     * @param $param
     * @return string
     */
    public function getTableViewClass($param)
    {
        if (isset($this->{$param}) && $this->{$param})
            return ' table-compact ';

        return '';
    }
}
