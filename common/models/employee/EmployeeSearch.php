<?php

namespace common\models\employee;

use frontend\modules\documents\components\FilterHelper;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\helpers\ArrayHelper;

/**
 * EmployeeSearch represents the model behind the search form about `common\models\Employee`.
 */
class EmployeeSearch extends Employee
{
    /**
     *
     */
    const EMPLOYEE_STATUS_ALL = 0;
    /**
     *
     */
    const EMPLOYEE_STATUS_RESIGNED = 1;
    /**
     *
     */
    const EMPLOYEE_STATUS_WORKS = 2;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_working', 'employee_role_id'], 'integer'],
            [['is_working'], 'in', 'range' => [
                self::EMPLOYEE_STATUS_ALL,
                self::EMPLOYEE_STATUS_RESIGNED,
                self::EMPLOYEE_STATUS_WORKS,
            ],],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param integer|null $companyId
     * @return ActiveDataProvider
     */
    public function search($params, $companyId = null)
    {
        $companyId = $companyId !== null ? $companyId : Yii::$app->user->identity->company->id;
        $query = Employee::find()->joinWith('employeeCompany')
            ->andWhere(['employee_company.company_id' => $companyId]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 10,
            ],
            'sort' => [
                'defaultOrder' => [
                    'fio' => SORT_ASC,
                ],
                'attributes' => [
                    'fio' => [
                        'asc' => [
                            'lastname' => SORT_ASC,
                            'firstname' => SORT_ASC,
                            'patronymic' => SORT_ASC,
                        ],
                        'desc' => [
                            'lastname' => SORT_DESC,
                            'firstname' => SORT_DESC,
                            'patronymic' => SORT_DESC,
                        ],
                    ],
                    'position',
                ],
            ],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'employee_company.employee_role_id' => $this->employee_role_id,
        ]);

        $query->byIsDeleted(self::NOT_DELETED);

        if ($this->is_working != self::EMPLOYEE_STATUS_ALL) {
            if ($this->is_working == self::EMPLOYEE_STATUS_WORKS) {
                $query->andWhere($this->tableName() . '.is_working = 1');
            } elseif ($this->is_working == self::EMPLOYEE_STATUS_RESIGNED) {
                $query->andWhere($this->tableName() . '.is_working = 0');
            }
        }

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getEmployeeStatusArray()
    {
        return [
            self::EMPLOYEE_STATUS_ALL => 'Все',
            self::EMPLOYEE_STATUS_WORKS => 'работает',
            self::EMPLOYEE_STATUS_RESIGNED => 'уволен',
        ];
    }

    /**
     * @return array
     */
    public function getEmployeeRoleArray()
    {
        return ArrayHelper::map(EmployeeRole::find()->actual()->all(), 'id', 'name');
    }
}
