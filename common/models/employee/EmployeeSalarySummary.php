<?php

namespace common\models\employee;

use common\components\helpers\Month;
use common\models\Company;
use frontend\models\SalaryConfigForm;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "employee_salary_summary".
 *
 * @property integer $id
 * @property integer $company_id
 * @property string $date
 * @property integer $days_count
 * @property integer $work_days_count
 * @property integer $employees_count
 * @property string $prepay_date
 * @property string $pay_date
 * @property integer $total_amount
 * @property integer $total_tax_ndfl
 * @property integer $total_tax_social
 * @property integer $total_expenses
 * @property integer $total_prepay_sum
 * @property integer $total_pay_sum
 *
 * @property EmployeeSalary[] $employeeSalaries
 * @property EmployeeCompany[] $employeeCompanies
 * @property Company $company
 */
class EmployeeSalarySummary extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'employee_salary_summary';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'days_count',
                    'work_days_count',
                    'employees_count',
                    'total_amount',
                    'total_tax_ndfl',
                    'total_tax_social',
                    'total_expenses',
                    'total_prepay_sum',
                    'total_pay_sum',
                ],
                'required',
            ],
            [
                [
                    'days_count',
                    'work_days_count',
                    'employees_count',
                    'total_amount',
                    'total_tax_ndfl',
                    'total_tax_social',
                    'total_expenses',
                    'total_prepay_sum',
                    'total_pay_sum',
                ],
                'integer',
            ],
            [
                [
                    'prepayDate',
                    'payDate',
                ],
                'date',
                'format' => 'php:d.m.Y',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'date' => 'Месяц, год',
            'days_count' => 'Дней в месяце',
            'work_days_count' => 'Рабочих дней',
            'employees_count' => 'Кол-во сотрудников',
            'prepay_date' => 'Дата аванса',
            'pay_date' => 'Дата зарплаты',
            'total_amount' => 'Сумма выплат на руки',
            'total_tax_ndfl' => 'Сумма НДФЛ',
            'total_tax_social' => 'Сумма соц. налогов',
            'total_expenses' => 'Итого затраты для компании',
            'total_prepay_sum' => 'Сумма аванса',
            'total_pay_sum' => 'Сумма зарплат',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployeeSalaries()
    {
        return $this->hasMany(EmployeeSalary::className(), ['company_id' => 'company_id', 'date' => 'date']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployeeCompanies()
    {
        return $this->hasMany(EmployeeCompany::className(), ['employee_id' => 'employee_id', 'company_id' => 'company_id'])->viaTable('employee_salary', ['company_id' => 'company_id', 'date' => 'date']);
    }

    /**
     * @param string $value
     */
    public function setPrepayDate($value)
    {
        $this->prepay_date = ($date = date_create_from_format('d.m.Y', $value)) ? $date->format('Y-m-d') : null;
    }

    /**
     * @return string
     */
    public function getPrepayDate()
    {
        return $this->prepay_date ? date_create($this->prepay_date)->format('d.m.Y') : null ;
    }

    /**
     * @param string $value
     */
    public function setPayDate($value)
    {
        $this->pay_date = ($date = date_create_from_format('d.m.Y', $value)) ? $date->format('Y-m-d') : null;
    }

    /**
     * @return string
     */
    public function getPayDate()
    {
        return $this->pay_date ? date_create($this->pay_date)->format('d.m.Y') : null ;
    }

    /**
     * @inheritdoc
     */
    public function calculateTotalFields()
    {
        $employeeSalaries = $this->employeeSalaries;
        $this->employees_count = count($employeeSalaries);
        $this->total_amount = array_sum(ArrayHelper::getColumn($employeeSalaries, 'total_amount'));
        $this->total_tax_ndfl = array_sum(ArrayHelper::getColumn($employeeSalaries, 'total_tax_ndfl'));
        $this->total_tax_social = array_sum(ArrayHelper::getColumn($employeeSalaries, 'total_tax_social'));
        $this->total_expenses = array_sum(ArrayHelper::getColumn($employeeSalaries, 'total_expenses'));
        $this->total_prepay_sum = array_sum(ArrayHelper::getColumn($employeeSalaries, 'total_prepay_sum'));
        $this->total_pay_sum = array_sum(ArrayHelper::getColumn($employeeSalaries, 'total_pay_sum'));
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if ($this->isAttributeChanged('prepay_date')) {
            foreach ($this->employeeSalaries as $model) {
                $model->salary_1_prepay_date = $this->prepay_date;
            }
        }
        if ($this->isAttributeChanged('pay_date')) {
            foreach ($this->employeeSalaries as $model) {
                $model->salary_1_pay_date = $this->pay_date;
            }
        }
        foreach ($this->employeeSalaries as $model) {
            if (!$model->validate()) {
                $errors = $model->getFirstErrors();
                $this->addError('employeeSalaries', reset($errors));
            }
        }
        $this->calculateTotalFields();

        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        foreach ($this->employeeSalaries as $model) {
            if ($model->getDirtyAttributes()) {
                $model->save();
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function setData(Company $company, \DateTime $dateTime)
    {
        $date = clone $dateTime;
        $date->modify('first day of this month');
        $this_date = $date->format('Y-m-d');
        $days_count = $date->format('t');
        $work_days_count = Month::workDays($date);

        $prepay_day = $company->salary_1_prepay_day ? min($days_count, $company->salary_1_prepay_day) : null;
        $prepay_date = $prepay_day ? $date->format('Y-m-') . sprintf('%02d', $prepay_day) : null;

        if ($company->salary_1_pay_day) {
            $max_day = $days_count;
            if (($company->salary_1_prepay_day > $company->salary_1_pay_day) ||
                (!$company->salary_1_prepay_day && $company->salary_1_pay_day < $days_count)
            ) {
                $date->modify('+1 month');
                $max_day = $date->format('t');
            }
            $pay_date = $date->format('Y-m-') . sprintf('%02d', min($company->salary_1_pay_day, $max_day));
        } else {
            $pay_date = null;
        }

        $this->company_id = $company->id;
        $this->date = $this_date;
        $this->days_count = $days_count;
        $this->work_days_count = $work_days_count;
        $this->prepay_date = $prepay_date;
        $this->pay_date = $pay_date;
    }

    /**
     * @param Company $company
     * @param DateTime $dateTime
     * @return EmployeeSalarySummary
     */
    public static function new(Company $company, \DateTime $dateTime)
    {
        $date = clone $dateTime;
        $date->modify('first day of this month');

        $model = new EmployeeSalarySummary;
        $model->setData($company, $date);

        $year_month = $date->format('Y-m-');
        $firstDay = $year_month . '01';
        $lastDay = $year_month . $model->days_count;

        $employeeCompanies = $company->getEmployeeCompanies()->andWhere([
            'and',
            ['<=', 'date_hiring', $lastDay],
            [
                'or',
                ['date_dismissal' => null],
                ['between', 'date_dismissal', $firstDay, $lastDay],
            ]
        ])->all();

        $employeeSalaries = [];

        foreach ($employeeCompanies as $employeeCompany) {
            $employeeSalary = new EmployeeSalary([
                'employee_id' => $employeeCompany->employee_id,
                'company_id' => $company->id,
                'date' => $model->date,
                'days_worked' => $model->work_days_count,
            ]);

            $employeeSalary->setData($company, $employeeCompany);

            $employeeSalaries[] = $employeeSalary;
        }

        $model->populateRelation('employeeSalaries', $employeeSalaries);

        return $model;
    }
}
