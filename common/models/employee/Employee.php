<?php

namespace common\models\employee;

use common\components\date\DateHelper;
use common\components\date\DatePickerFormatBehavior;
use common\components\helpers\ArrayHelper;
use common\components\ImageHelper;
use common\components\validators\EmployeeEmailValidator;
use common\components\validators\PhoneValidator;
use common\models\bank\BankUser;
use common\models\cash\Cashbox;
use common\models\cash\Emoney;
use common\models\Chat;
use common\models\ChatVolume;
use common\models\Company;
use common\models\company\CompanyLastVisit;
use common\models\company\CompanyType;
use common\models\company\MenuItem;
use common\models\document\status\InvoiceStatus;
use common\models\EmployeeCompany;
use common\models\mts\MtsUser;
use common\models\file\FileDir;
use common\models\ofd\OfdUser;
use common\models\retail\RetailPoint;
use common\models\retail\Retailer;
use common\models\service\Payment;
use common\models\service\PaymentOrder;
use common\models\service\PaymentType;
use common\models\service\Subscribe;
use common\models\service\SubscribeStatus;
use common\models\service\SubscribeTariff;
use common\models\TimeZone;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\modules\analytics\models\AnalyticsMultiCompany;
use Yii;
use yii\base\InvalidArgumentException;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\web\IdentityInterface;

/**
 * This is the model class for table "employee".
 *
 * @property integer           $id
 * @property integer           $company_id
 * @property string            $email
 * @property string            $email_confirm_key
 * @property string            $is_email_confirmed
 * @property string            $password
 * @property string            $password_reset_token
 * @property string            $auth_key
 * @property string            $created_at
 * @property string            $author_id
 * @property string            $lastname
 * @property string            $firstname
 * @property string            $firstname_initial
 * @property string            $patronymic
 * @property string            $patronymic_initial
 * @property boolean           $has_no_patronymic
 * @property integer           $sex
 * @property string            $birthday
 * @property string            $date_hiring
 * @property string            $date_dismissal
 * @property string            $position
 * @property integer           $is_working
 * @property integer           $is_active
 * @property integer           $is_deleted
 * @property integer           $employee_role_id
 * @property string            $phone
 * @property integer           $notify_nearly_report
 * @property integer           $notify_new_features
 * @property integer           $is_registration_completed
 * @property string            $time_zone_id
 * @property string            $view_notification_date
 * @property integer           $main_company_id
 * @property string            $my_companies
 * @property integer           $wp_id
 * @property integer           $alert_close_count
 * @property integer           $alert_close_time
 * @property string            $statistic_range_date_from
 * @property string            $statistic_range_date_to
 * @property string            $statistic_range_name
 * @property string            $access_token
 * @property integer           $socket_id
 * @property string            $chat_photo
 * @property boolean           $chat_volume
 * @property boolean           $duplicate_notification_to_sms
 * @property boolean           $duplicate_notification_to_email
 * @property boolean           $push_notification_new_message
 * @property boolean           $push_notification_create_closest_document
 * @property boolean           $push_notification_overdue_invoice
 * @property string            $push_token
 * @property int               $show_scan_popup
 * @property string            $google_adsense_access_token
 * @property boolean           $is_old_kub_theme
 * @property boolean           $has_finished_unviewed_jobs
 *
 * @property Company           $company
 * @property Company           $mainCompany
 * @property Company[]         $companies
 * @property EmployeeRole      $employeeRole
 * @property Employee          $author
 * @property EmployeeCompany[] $employeeCompany
 * @property EmployeeCompany   $currentEmployeeCompany
 * @property Config            $config
 * @property EmployeeClick[]   $employeeClicks
 * @property MenuItem          $menuItem
 *
 * @property string            $fio
 * @property boolean           $showAlert
 * @mixin TimestampBehavior
 */
class Employee extends \yii\db\ActiveRecord implements IdentityInterface
{

    const INTEGRATION_AMOCRM = 'amocrm';
    const INTEGRATION_EMAIL = 'email';
    const INTEGRATION_EVOTOR = 'evotor';
    const INTEGRATION_FACEBOOK = 'facebook';
    const INTEGRATION_VK = 'vk';
    const INTEGRATION_UNISENDER = 'unisender';
    const INTEGRATION_INSALES = 'insales';
    const INTEGRATION_BITRIX24 = 'bitrix24';
    const INTEGRATION_GOOGLE_ADS = 'googleAds';
    const INTEGRATION_YANDEX_DIRECT = 'yandexDirect';
    const INTEGRATION_DREAMKAS = 'dreamkas';

    const STATUS_IS_WORKING = 1;
    const STATUS_IS_NOT_WORKING = 0;

    const DELETED = 1;
    const NOT_DELETED = 0;

    const MALE = 1;
    const FEMALE = 0;

    const ACTIVE = 1;
    const NOT_ACTIVE = 0;

    const PASS_LENGTH = 10;

    const SUPPORT_KUB_EMPLOYEE_ID = 2;

    const SCENARIO_UPDATE = 'update';
    const SCENARIO_CONTINUE_REGISTRATION = 'continue_registration';
    const SCENARIO_CREATE_PROXY = 'proxy';

    const NOT_VOLUME = 0;
    const HAS_VOLUME = 1;

    const NO_CHAT_PHOTO = 'no_sex.png';
    const DOC_NUMBER_AFTER_SHOW_SCAN_POPUP = 15;
    const DATE_FROM_SCAN_POPUP_SHOW = '2018-06-01 00:00:00';
    const SHOW_SCAN_POPUP_SESSION_KEY = 'show_scan_popup';

    protected $_company = false;
    protected $_menuItem = false;
    protected $_currentEmployeeCompany = [];
    protected $_canAddCompany = null;

    public static $status_message = [
        self::STATUS_IS_WORKING => 'работает',
        self::STATUS_IS_NOT_WORKING => 'уволен',
    ];

    public static $sex_message = [
        self::MALE => 'мужской',
        self::FEMALE => 'женский',
    ];

    public static $chatPhotos = [
        self::MALE => [
            1 => 'human_1.png',
            2 => 'human_2.png',
            3 => 'human_3.png',
            4 => 'human_4.png',
            5 => 'human_5.png',
            6 => 'human_6.png',
            7 => 'human_7.png',
            8 => 'human_8.png',
        ],
        self::FEMALE => [
            1 => 'woman_1.png',
            2 => 'woman_2.png',
            3 => 'woman_3.png',
            4 => 'woman_4.png',
            5 => 'woman_5.png',
            6 => 'woman_6.png',
            7 => 'woman_7.png',
            8 => 'woman_8.png',
        ],
    ];

    public static $linkColors = [
        0 => '#0097FD',
        1 => '#0097FD',
        2 => '#4A78B2',
        3 => '#0056B3'
    ];

    protected $_data = [];

    /**
     * @return EmployeeQuery
     */
    public static function find()
    {
        return new EmployeeQuery(get_called_class());
    }

    /**
     * @return EmployeeQuery
     */
    public static function findActive()
    {
        return self::find()->byIsActive(1)->byIsDeleted(0);
    }

    public static function flatList($companyId)
    {
        return ArrayHelper::map(
            self::findActive()->byCompany($companyId)->select(['id', 'lastname', 'firstname_initial', 'patronymic_initial'])->asArray()->all(),
            'id',
            function($model) {
                return join(' ', array_filter([
                    $model['lastname'],
                    $model['firstname_initial'] ? $model['firstname_initial'] . '.' : '',
                    $model['patronymic_initial'] ? $model['patronymic_initial'] . '.' : '',
                ]));
            }
        );
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            [
                'class' => DatePickerFormatBehavior::className(),
                'attributes' => [
                    'birthday' => [
                        'message' => 'Дата рождения указана неверно.',
                    ],
                    'date_hiring' => [
                        'message' => 'Дата принятия на работу указана неверно.',
                    ],
                    'date_dismissal' => [
                        'message' => 'Дата увольнения указана неверно.',
                    ],
                ],
                'dateFormatParams' => [
                    'whenClient' => 'function(){}',
                ],
            ],
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
            ],
            [
                'class' => 'common\components\PageSizeBehavior',
            ],
        ]);
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_DEFAULT => [
                'email',
                'lastname',
                'firstname',
                'patronymic',
                'position',
                'employee_role_id',
                'time_zone_id',
                'main_company_id',
                'date_hiring',
                'phone',
                'sex',
                'notify_nearly_report',
                'notify_new_features',
                'wp_id',
                'duplicate_notification_to_sms',
                'duplicate_notification_to_email',
                'push_notification_new_message',
                'push_notification_create_closest_document',
                'push_notification_overdue_invoice',
                'chat_volume',
                'birthday',
                'date_dismissal',
                'push_token',
                'statistic_range_date_from',
                'statistic_range_date_to',
                'statistic_range_name',
                'chat_photo',
            ],
            self::SCENARIO_CONTINUE_REGISTRATION => [
                'is_registration_completed',
                'email',
                'lastname',
                'firstname',
                'patronymic',
                'position',
                'employee_role_id',
                'time_zone_id',
                'main_company_id',
                'phone',
                'sex',
                'notify_nearly_report',
                'notify_new_features',
                'wp_id',
                'duplicate_notification_to_sms',
                'duplicate_notification_to_email',
                'push_notification_new_message',
                'push_notification_create_closest_document',
                'push_notification_overdue_invoice',
                'chat_volume',
                'birthday',
                'date_dismissal',
                'push_token',
                'statistic_range_date_from',
                'statistic_range_date_to',
                'statistic_range_name',
                'chat_photo',
            ],
            self::SCENARIO_UPDATE => [
                'name',
                'lastname',
                'firstname',
                'patronymic',
                'sex',
                'birthday',
                'phone',
                'time_zone_id',
                'push_notification_new_message',
                'push_notification_create_closest_document',
                'push_notification_overdue_invoice',
                'duplicate_notification_to_sms',
                'duplicate_notification_to_email',
            ],
            self::SCENARIO_CREATE_PROXY => [
                'email',
                'lastname',
                'firstname',
                'patronymic',
                'has_no_patronymic',
                'employee_role_id',
                'time_zone_id',
                'main_company_id',
                'notify_nearly_report',
                'notify_new_features',
                'wp_id',
                'duplicate_notification_to_sms',
                'duplicate_notification_to_email',
                'push_notification_new_message',
                'push_notification_create_closest_document',
                'push_notification_overdue_invoice',
                'chat_volume',
                'birthday',
                'date_dismissal',
                'push_token',
                'statistic_range_date_from',
                'statistic_range_date_to',
                'statistic_range_name',
                'chat_photo',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function getIsDemo()
    {
        $id = Yii::$app->params['service']['demo_employee_id'] ?? null;

        return $id && $id == $this->id;
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->is_active = self::ACTIVE;
                $this->is_working = self::STATUS_IS_WORKING;
                $this->is_deleted = self::NOT_DELETED;
                $this->generateAccessToken();
                if (!isset($this->is_mts_user) && Yii::$app->id == 'app-frontend') {
                    $this->is_mts_user = Yii::$app->user->getIsMtsUser();
                }
            }

            if ($this->isAttributeChanged('firstname')) {
                $this->firstname_initial = $this->firstname ? mb_substr($this->firstname, 0, 1) : null;
            }
            if ($this->isAttributeChanged('patronymic')) {
                $this->patronymic_initial = $this->patronymic ? mb_substr($this->patronymic, 0, 1) : null;
            }

            $this->is_working = empty($this->date_dismissal) ? self::STATUS_IS_WORKING : self::STATUS_IS_NOT_WORKING;

            if (($id = Yii::$app->params['service']['demo_employee_id'] ?? null) !== null) {
                if ($this->id == $id) {
                    $this->employee_role_id = EmployeeRole::ROLE_DEMO;
                }
            }

            return true;
        } else {
            return false;
        }
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert) {
            (new Config(['employee_id' => $this->id]))->loadDefaultValues()->save(false);

            /* @var $employeeCompany EmployeeCompany */
            foreach (EmployeeCompany::find()->andWhere([
                'and',
                ['company_id' => $this->company_id],
                ['not', ['employee_id' => $this->id]],
            ])->all() as $employeeCompany) {
                $chatVolume = new ChatVolume();
                $chatVolume->company_id = $this->company_id;
                $chatVolume->from_employee_id = $this->id;
                $chatVolume->to_employee_id = $employeeCompany->employee_id;
                $chatVolume->has_volume = true;
                $chatVolume->save();

                $chatVolumeFriend = new ChatVolume();
                $chatVolumeFriend->company_id = $this->company_id;
                $chatVolumeFriend->from_employee_id = $employeeCompany->employee_id;
                $chatVolumeFriend->to_employee_id = $this->id;
                $chatVolumeFriend->has_volume = true;
                $chatVolumeFriend->save();
            }

            if (!ChatVolume::find()->andWhere([
                'and',
                ['company_id' => $this->company_id],
                ['from_employee_id' => $this->id],
                ['to_employee_id' => self::SUPPORT_KUB_EMPLOYEE_ID],
            ])->exists()
            ) {
                $chatVolumeWithSupport = new ChatVolume();
                $chatVolumeWithSupport->company_id = $this->company_id;
                $chatVolumeWithSupport->from_employee_id = $this->id;
                $chatVolumeWithSupport->to_employee_id = self::SUPPORT_KUB_EMPLOYEE_ID;
                $chatVolumeWithSupport->has_volume = true;
                $chatVolumeWithSupport->save();

                $supportUser = Employee::findOne(self::SUPPORT_KUB_EMPLOYEE_ID);
                $chatVolumeSupport = new ChatVolume();
                $chatVolumeSupport->company_id = $supportUser->company_id;
                $chatVolumeSupport->from_employee_id = self::SUPPORT_KUB_EMPLOYEE_ID;
                $chatVolumeSupport->to_employee_id = $this->id;
                $chatVolumeSupport->has_volume = true;
                $chatVolumeSupport->save();
            }
        }

        if ($this->is_active == false) {
            $this->closeOpenSessions();
        }
    }

    /**
     * @param $length
     * @return string
     */
    public static function generatePassword($length)
    {
        $simbolsArray = [
            '0123456789',
            'abcdefghijklmnopqrstuvwxyz',
            'ABCDEFGHIJKLMNOPQRSTUVWXYZ',
        ];
        $password = '';

        foreach ($simbolsArray as $simbols) {
            $password .= substr(str_shuffle($simbols), 0, 1);
        }

        while (strlen($password) < $length) {
            $simbols = $simbolsArray[array_rand($simbolsArray)];
            $password .= substr(str_shuffle($simbols), 0, 1);
        }

        return str_shuffle($password);
    }

    /**
     * @return bool
     */
    public function sexIsMale()
    {
        return ($this->sex == self::MALE) ? true : false;
    }

    /**
     * @return bool
     */
    public function sexIsFemale()
    {
        return ($this->sex == self::FEMALE) ? true : false;
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'employee';
    }

    /**
     * @return string
     */
    public static function sessionTableName()
    {
        return 'session_frontend';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email', 'lastname', 'firstname', 'patronymic', 'position'], 'trim'],
            [
                [
                    'email',
                    'lastname',
                    'firstname',
                    //'patronymic',
                    'position',
                    'employee_role_id',
                    'time_zone_id',
                ],
                'required',
            ],
            [
                ['has_no_patronymic'],
                'boolean',
            ],
            [
                ['patronymic'],
                'required',
                'when' => function ($model) {
                    return (!$model->has_no_patronymic);
                },
                'whenClient' => 'function () {
                    return !$("#employee-has_no_patronymic").is(":checked");
                }',
            ],
            [
                ['date_hiring', 'phone', 'sex'],
                'required',
                'on' => [self::SCENARIO_DEFAULT],
            ],
            [
                [
                    'sex',
                    'employee_role_id',
                    'notify_nearly_report',
                    'notify_new_features',
                    'main_company_id',
                    'wp_id',
                    'duplicate_notification_to_sms',
                    'duplicate_notification_to_email',
                    'push_notification_new_message',
                    'push_notification_create_closest_document',
                    'push_notification_overdue_invoice',
                ],
                'integer',
            ],
            [
                [
                    'push_notification_new_message',
                    'push_notification_create_closest_document',
                    'push_notification_overdue_invoice',
                    'duplicate_notification_to_sms',
                    'duplicate_notification_to_email',
                ],
                'boolean',
            ],
            [['birthday', 'date_dismissal'], 'safe'],
            [['email'], 'email'],
            [['email'], 'unique', 'filter' => ['is_deleted' => false]],
            [['position', 'push_token', 'yandex_access_token'], 'string', 'max' => 255],
            [
                [
                    'lastname',
                    'firstname',
                    'patronymic',
                    'statistic_range_date_from',
                    'statistic_range_date_to',
                    'statistic_range_name',
                ],
                'string',
                'max' => 45,
            ],
            [['chat_photo'], 'string'],
            [['phone'], PhoneValidator::className()],

            [['is_registration_completed'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Компания',
            'email' => 'Email',
            'password' => 'Пароль',
            'created_at' => 'Дата создания',
            'name' => 'ФИО',
            'lastname' => 'Фамилия',
            'firstname' => 'Имя',
            'firstname_initial' => 'Инициал имени',
            'patronymic' => 'Отчество',
            'patronymic_initial' => 'Инициал отчества',
            'sex' => 'Пол',
            'birthday' => 'Дата рождения',
            'date_hiring' => 'Дата приёма на работу',
            'date_dismissal' => 'Дата увольнения',
            'position' => 'Должность',
            'is_working' => 'Работает',
            'is_deleted' => 'Удалён',
            'employee_role_id' => 'Роль',
            'phone' => 'Телефон',
            'notify_nearly_report' => 'Оповещать о приближающихся отчётах',
            'notify_new_features' => 'Оповещать о новых функциях сервиса',
            'time_zone_id' => 'Часовой пояс',
            'view_notification_date' => 'Дата просмотра новостей',
            'last_visit_at' => 'Дата последней активности',
            'wp_id' => 'Wp ID',
            'duplicate_notification_to_sms' => 'Дублировать уведомления по SMS',
            'duplicate_notification_to_email' => 'Дублировать уведомления по электронной почте',
            'push_notification_new_message' => 'Push-уведомление по новому сообщению в чате',
            'push_notification_create_closest_document' => 'Push-уведомление по необходимости создания закрывающего документа',
            'push_notification_overdue_invoice' => 'Push-уведомление по появлению просроченного счета',
            'push_token' => 'Push token',
            'has_no_patronymic' => 'Нет отчества',
        ];
    }

    /**
     * @return array the list of field names or field definitions.
     */
    public function fields()
    {
        $fields = [
            'id',
            'email',
            'created_at',
            'lastname',
            'firstname',
            'firstname_initial',
            'patronymic',
            'patronymic_initial',
            'sex',
            'birthday',
            'is_deleted',
            'phone',
            'time_zone_id',
            'last_visit_at',
            'push_notification_new_message',
            'push_notification_create_closest_document',
            'push_notification_overdue_invoice',
            'duplicate_notification_to_sms',
            'duplicate_notification_to_email',
        ];

        return array_combine($fields, $fields);
    }

    /**
     * @return array
     */
    public function attributeHints()
    {
        if (\Yii::$app->user->identity instanceof Employee) {
            if ($this->getScenario() === self::SCENARIO_CONTINUE_REGISTRATION) {
                $emailHint = '<p class="text-danger">Важно! Проверьте правильность вашего e-mail</p>
                                <ol>
                                    <li>Указанный e-mail является логином для доступа в систему.</li>
                                    <li>При отправке счетов вашим покупателям, они будут видеть данный e-mail, как отправителя и смогут отвечать на него</li>
                                </ol>';
            } else {
                $emailHint = Yii::$app->user->identity->company->id == Yii::$app->user->identity->company->main_id ? 'Указанный e-mail является также логином для доступа в систему.
                                При создании аккаунта на указанную почту будет выслано письмо,
                                содержащее сгенерированный пароль для входа в систему.' :
                    'Если указанный e-mail присутствует в базе пользователей КУБ,
                    то все данные подтянутся автоматически. Если нет,
                    то на указанную почту будет выслано письмо, содержащее сгенерированный пароль для входа в систему.';
            }

            return [
                'email' => $emailHint,
            ];
        } else {
            return [];
        }
    }

    /**
     * Set current user company
     *
     * @param integer|Company $company Company::id
     */
    public function setCompany($company)
    {
        if ($company === null) {
            $this->_company = null;
        } else {
            if ($company instanceof Company) {
                if ($this->getCompanies()->andWhere(['id' => $company->id])->exists()) {
                    $this->_company = $company;
                } else {
                    throw new \yii\base\InvalidArgumentException();
                }
            } else {
                if (($c = $this->getCompanies()->andWhere(['id' => $company])->one()) !== null) {
                    $this->_company = $c;
                } else {
                    throw new \yii\base\InvalidArgumentException();
                }
            }
        }

        return $this->_company;
    }

    /**
     * @return Company
     */
    public function getCompany($refresh = false)
    {
        if ($this->_company === false || $refresh) {
            $this->_company = $this->getCompanies()->andWhere([
                'company.id' => $this->company_id,
            ])->one();
        }

        return $this->_company;
    }

    /** RELATIONS */

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMainCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'main_company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAnalyticsMultiCompanies()
    {
        return $this->hasMany(AnalyticsMultiCompany::className(), ['employee_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMtsUser()
    {
        return $this->hasOne(MtsUser::className(), ['employee_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployeeRole()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_role_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployeeClicks()
    {
        return $this->hasMany(EmployeeClick::className(), ['employee_id' => 'id']);
    }

    /**
     * @return bool|string
     */
    public function getChatPhotoSrc()
    {
        return '/img/chat/' . $this->chat_photo;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Employee::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimeZone()
    {
        return $this->hasOne(TimeZone::className(), ['id' => 'time_zone_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConfig()
    {
        return $this->hasOne(Config::className(), ['employee_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyLastVisits()
    {
        return $this->hasMany(CompanyLastVisit::className(), ['employee_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBankUsers()
    {
        return $this->hasMany(BankUser::className(), ['employee_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfdUsers()
    {
        return $this->hasMany(OfdUser::className(), ['employee_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfdUser($ofd)
    {
        return $this->getOfdUsers()->andWhere([
            'company_id' => $this->company->id,
            'ofd_id' => is_object($ofd) ? $ofd->id : $ofd,
        ]);
    }

    /**
     * @return BankUser
     */
    public function getBankUser($alias)
    {
        return $this->getBankUsers()->andWhere(['bank_alias' => $alias])->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMenuItem()
    {
        if ($this->_menuItem === false) {
            if ($this->company) {
                $this->_menuItem = MenuItem::findOne([
                    'employee_id' => $this->id,
                    'company_id' => $this->company->id,
                ]);
                if ($this->_menuItem === null) {
                    $this->_menuItem = new MenuItem([
                        'employee_id' => $this->id,
                        'company_id' => $this->company->id,
                    ]);
                    $this->_menuItem->save(false);
                }
            } else {
                $this->_menuItem = null;
            }
        }

        return $this->_menuItem;
    }

    public function getFileDirs() {
        return $this->hasMany(FileDir::className(), ['id' => 'dir_id'])->viaTable('file_dir_employee', ['employee_id' => 'id'])
            ->andWhere(['company_id' => $this->company->id])->orderBy('ord');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashboxes($company_id = null)
    {
        $employeeCompany = $this->getCurrentEmployeeCompany($company_id);
        $query = $employeeCompany->company->getCashboxes();
        $role_id = $employeeCompany->employee_role_id;

        $query->andWhere([
            'cashbox.is_closed' => false,
        ]);

        if (!in_array($role_id, Cashbox::$rolesViewAll)) {
            if ($role_id == EmployeeRole::ROLE_ACCOUNTANT) {
                $query->andWhere([
                    'or',
                    ['and', ['cashbox.responsible_employee_id' => null], ['cashbox.is_main' => false]],
                    ['and', ['cashbox.responsible_employee_id' => null], ['cashbox.is_main' => true], ['cashbox.is_accounting' => true]],
                    ['cashbox.responsible_employee_id' => $this->id],
                ]);
            } else {
                $query->andWhere([
                    'or',
                    ['cashbox.responsible_employee_id' => $this->id],
                    ['cashbox.accessible_to_all' => true],
                ]);
            }
        }

        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmoneys($company_id = null)
    {
        $employeeCompany = $this->getCurrentEmployeeCompany($company_id);
        $query = $employeeCompany->company->getEmoneys();
        $role_id = $employeeCompany->employee_role_id;

        $query->andWhere([
            'emoney.is_closed' => false,
        ]);

        if (!in_array($role_id, Emoney::$rolesViewAll)) {
            if ($role_id == EmployeeRole::ROLE_ACCOUNTANT) {
                $query->andWhere([
                    'emoney.is_accounting' => true
                ]);
            } else {
                $query->andWhere('0=1');
            }
        }

        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStores($company_id = null)
    {
        $employeeCompany = $this->getCurrentEmployeeCompany($company_id);
        $query = $employeeCompany->company->getStores();
        $role_id = $employeeCompany->employee_role_id;

        $query->andWhere([
            'is_closed' => false,
        ]);

        if (!in_array($role_id, [EmployeeRole::ROLE_CHIEF, EmployeeRole::ROLE_SUPERVISOR])) {
            $query->andWhere([
                'store.responsible_employee_id' => $this->id,
            ]);
        }

        return $query;
    }

    /**
     * @return Cashbox|null
     */
    public function getCashbox($company_id = null)
    {
        return $this->getCashboxes($company_id)->orderBy([
            'IF([[responsible_employee_id]] = :uid, 0, 1)' => SORT_ASC,
            'is_main' => SORT_DESC,
        ])->params([':uid' => $this->id])->one();
    }

    /**
     * @param $date_from
     * @param $date_to
     * @return bool
     */
    public function setStatisticRangeDates($date_from, $date_to)
    {
        if ($date_from != null || $date_to != null) {
            $this->statistic_range_date_from = $date_from;
            $this->statistic_range_date_to = $date_to;
            $this->save(true, ['statistic_range_date_from', 'statistic_range_date_to']);

            return true;
        }

        return false;
    }

    /**
     * @param $name
     * @param $date_from
     * @param $date_to
     * @return bool
     */
    public function setStatisticRangeName($name, $date_from, $date_to)
    {
        if ($name != null) {
            $this->statistic_range_name = $name;
            if ($name == StatisticPeriod::CUSTOM_RANGE) {
                $date_from = DateHelper::format($date_from, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                $date_to = DateHelper::format($date_to, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                $this->statistic_range_name = $date_from . ' - ' . $date_to;
            }

            return $this->save(true, ['statistic_range_name']);
        }

        return false;
    }

    /**
     * @return boolean
     */
    public function getIsSetStatisticRange()
    {
        return $this->statistic_range_name && $this->statistic_range_date_from && $this->statistic_range_date_to;
    }

    /**
     * @return string
     */
    public function getStatisticRangeName()
    {
        return $this->statistic_range_name;
    }

    /**
     * @param null $range
     * @return bool|string
     */
    public function getStatisticRangeDates($range = null)
    {
        if ($range == 'from') {
            return $this->statistic_range_date_from;
        } elseif ($range == 'to') {
            return $this->statistic_range_date_to;
        } elseif ($this->statistic_range_date_from && $this->statistic_range_date_to) {
            return true;
        }

        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployeeCompany()
    {
        return $this->hasMany(EmployeeCompany::className(), ['employee_id' => 'id']);
    }

    /**
     * @param null $companyId
     * @return EmployeeCompany
     */
    public function getCurrentEmployeeCompany($companyId = null, $noCache = false)
    {
        if (empty($companyId)) {
            $companyId = $this->_company ? $this->_company->id : $this->company_id;
        }
        if (!array_key_exists($companyId, $this->_currentEmployeeCompany) || $noCache) {
            $employeeCompany = $this->getEmployeeCompany()->andWhere([
                'company_id' => $companyId,
                'is_working' => true,
            ])->one();

            if ($employeeCompany) {
                $employeeCompany->populateRelation('employee', $this);
                if ($this->_company && $this->_company->id == $employeeCompany->company_id) {
                    $employeeCompany->populateRelation('company', $this->_company);
                }
            }

            $this->_currentEmployeeCompany[$companyId] = $employeeCompany;
        }

        return $this->_currentEmployeeCompany[$companyId];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Company::className(), ['id' => 'company_id'])
            ->viaTable('employee_company', ['employee_id' => 'id'], function ($query) {
                $query->onCondition(['employee_company.is_working' => true]);
            })->andOnCondition([
                'company.blocked' => false,
            ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompaniesByChief()
    {
        return $this->hasMany(Company::className(), ['id' => 'company_id'])
            ->viaTable('employee_company', ['employee_id' => 'id'], function ($query) {
                $query->onCondition([
                    'employee_company.employee_role_id' => EmployeeRole::ROLE_CHIEF,
                    'employee_company.is_working' => true,
                ]);
            })->andOnCondition([
                'company.blocked' => false,
            ]);
    }

    /**
     * Компании, владельцем учетной записи которых, является пользователь
     *
     * @return \yii\db\ActiveQuery
     * @var $notBlocked
     */
    public function getOwnCompanies()
    {
        return $this->hasMany(Company::className(), ['id' => 'company_id'])
            ->viaTable('employee_company', ['employee_id' => 'id'], function ($query) {
                $query->onCondition(['employee_company.is_working' => true]);
            })->andOnCondition([
                'company.owner_employee_id' => $this->id,
                'company.blocked' => false,
            ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotPaidCompanies()
    {
        return $this->getCompanies()->joinWith('activeSubscribe subscribe')->andWhere([
            'or',
            ['subscribe.id' => null],
            ['subscribe.tariff_id' => SubscribeTariff::TARIFF_TRIAL],
            [
                'and',
                ['not', ['subscribe.tariff_id' => SubscribeTariff::TARIFF_TRIAL]],
                ['<', 'subscribe.expired_at', time()],
            ],
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNotPaidOwnCompanies()
    {
        return $this->getOwnCompanies()
            ->joinWith('activeSubscribe subscribe')
            ->leftJoin([
                'notActiveSubscribes' => Subscribe::find()
                    ->joinWith('payment payment')
                    ->andWhere(['payment.payment_for' => Payment::FOR_SUBSCRIBE])
                    ->andWhere(['payment.is_confirmed' => true])
                    ->andWhere(['status_id' => SubscribeStatus::STATUS_PAYED]),
            ], '{{company}}.[[id]] = {{notActiveSubscribes}}.[[company_id]]')
            ->andWhere([
                'or',
                ['subscribe.id' => null],
                [
                    'and',
                    ['subscribe.tariff_id' => SubscribeTariff::TARIFF_TRIAL],
                    ['notActiveSubscribes.id' => null],
                ],
                [
                    'and',
                    ['not', ['subscribe.tariff_id' => SubscribeTariff::TARIFF_TRIAL]],
                    ['<', 'subscribe.expired_at', time()],
                ],
            ]);
    }

    /**
     * @return Company
     */
    public function getCompanyByInn($inn, $kpp = null)
    {
        return $this->getCompanies()->andWhere([
            'inn' => $inn,
            'blocked' => false,
        ])->andFilterWhere([
            'kpp' => $kpp,
        ])->one();
    }

    /**
     * @return EmployeeRole
     */
    public function getCurrentRole()
    {
        return $this->currentEmployeeCompany->employeeRole;
    }

    /**
     * @return EmployeeRole
     */
    public function getCurrentRoleId()
    {
        return $this->currentEmployeeCompany->employee_role_id;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     *
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     *
     */
    public function generateAccessToken()
    {
        do {
            $this->access_token = Yii::$app->security->generateRandomString();
        } while (self::find()->where(['access_token' => $this->access_token])->exists());

        return $this->access_token;
    }

    /**
     * @return string
     */
    public function getAccessToken()
    {
        if (!$this->access_token) {
            $this->updateAttributes(['access_token' => $this->generateAccessToken()]);
        }

        return $this->access_token;
    }


    /** IDENTITY */

    public function getUsername()
    {
        return $this->fio;
    }

    /**
     * @param $userID
     * @return Chat[]
     */
    public function getMessages($userID)
    {
        return Chat::find()->andWhere([
            'or',
            [
                'and',
                ['from_employee_id' => $this->id],
                ['to_employee_id' => $userID],
            ],
            [
                'and',
                ['from_employee_id' => $userID],
                ['to_employee_id' => $this->id],
            ],
        ])->orderBy('created_at')->all();
    }

    /**
     * @param $userID
     * @return int|string
     */
    public function getUnreadMessagesCount($userID)
    {
        return Chat::find()->andWhere([
            'and',
            ['from_employee_id' => $userID],
            ['to_employee_id' => $this->id],
            ['is_viewed' => false],
        ])->count();
    }

    /**
     * @param $userID
     */
    public function readMessages($userID)
    {
        /* @var $unreadMessage Chat */
        foreach (Chat::find()->andWhere([
            'and',
            ['from_employee_id' => $userID],
            ['to_employee_id' => $this->id],
            [
                'or',
                ['is_new' => true],
                ['is_viewed' => false],
            ],
        ])->all() as $unreadMessage) {
            $unreadMessage->is_new = false;
            $unreadMessage->is_viewed = true;
            $unreadMessage->save(true, ['is_new', 'is_viewed']);
        }
    }

    /**
     * @param $userID
     * @return array
     */
    public function getMessageDates($userID)
    {
        return Chat::find()
            ->select(['date(from_unixtime(`created_at`)) as date'])
            ->andWhere([
                'or',
                [
                    'and',
                    ['from_employee_id' => $this->id],
                    ['to_employee_id' => $userID],
                ],
                [
                    'and',
                    ['from_employee_id' => $userID],
                    ['to_employee_id' => $this->id],
                ],
            ])->orderBy('created_at')
            ->groupBy('date')
            ->column();
    }

    /**
     * @return Employee[]
     */
    public function getUsersWithMessages()
    {
        return Employee::find()
            ->leftJoin('chat', 'chat.to_employee_id = employee.id OR chat.from_employee_id = employee.id')
            ->andWhere([
                'in',
                Employee::tableName() . '.id',
                Chat::find()
                    ->select(['from_employee_id'])
                    ->andWhere([
                        'and',
                        ['to_employee_id' => $this->id],
                        ['not', ['from_employee_id' => $this->id]],
                    ])
                    ->groupBy('from_employee_id')->column(),
            ])
            ->orderBy(['chat.created_at' => SORT_DESC])
            ->all();
    }

    /**
     * @return bool
     */
    public function isOnline()
    {
        return (time() - $this->last_visit_at) < 60 * 24;
    }

    /**
     * Finds an identity by the given ID.
     *
     * @param string|integer $id the ID to be looked for
     *
     * @return IdentityInterface the identity object that matches the given ID.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentity($id)
    {
        return self::find()->where([
            'id' => $id,
            'is_deleted' => false,
        ])->one();
    }

    /**
     * Finds an identity by the given token.
     *
     * @param mixed $token the token to be looked for
     * @param mixed $type  the type of the token. The value of this parameter depends on the implementation.
     *                     For example, [[\yii\filters\auth\HttpBearerAuth]] will set this parameter to be
     *                     `yii\filters\auth\HttpBearerAuth`.
     *
     * @return IdentityInterface the identity object that matches the given token.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     * @throws NotSupportedException
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return $token ? self::find()->andWhere([
            'access_token' => $token,
            'is_deleted' => false,
        ])->one() : null;
    }

    /**
     * @param BankUser|array $params
     *
     * @return IdentityInterface the identity object that matches the given BankUser params.
     * Null should be returned if such an identity cannot be found
     * or the identity is not in an active state (disabled, deleted, etc.)
     */
    public static function findIdentityByBankUser($bankUser)
    {
        return self::find()->joinWith('bankUsers bank_user', false)->andWhere([
            'employee.is_deleted' => false,
            'employee.is_active' => true,
            'bank_user.bank_alias' => ArrayHelper::getValue($bankUser, 'bank_alias'),
            'bank_user.user_uid' => ArrayHelper::getValue($bankUser, 'user_uid'),
        ])->one();
    }

    /**
     * @param string $email
     *
     * @return Employee
     *
     */
    public static function findIdentityByLogin($email)
    {
        return static::find()->andWhere([
            'email' => $email,
            'is_deleted' => false,
        ])->one();
    }

    /**
     * Returns an ID that can uniquely identify a user identity.
     *
     * @return string|integer an ID that uniquely identifies a user identity.
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * Returns a key that can be used to check the validity of a given identity ID.
     *
     * The key should be unique for each individual user, and should be persistent
     * so that it can be used to check the validity of the user identity.
     *
     * The space of such keys should be big enough to defeat potential identity attacks.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     *
     * @return string a key that is used to check the validity of a given identity ID.
     * @see validateAuthKey()
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * Validates the given auth key.
     *
     * This is required if [[User::enableAutoLogin]] is enabled.
     *
     * @param string $authKey the given auth key
     *
     * @return boolean whether the given auth key is valid.
     * @see getAuthKey()
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * @param $newPassword
     * @return bool
     */
    public function sendNewPasswordEmail($newPassword)
    {
        return \Yii::$app->mailer->compose([
            'html' => 'system/registration-email/html',
            'text' => 'system/registration-email/text',
        ], [
            'data' => [
                'baseUrl' => \Yii::$app->params['uniSender']['baseUrl'],
                'lkBaseUrl' => \Yii::$app->params['uniSender']['lkBaseUrl'],
                'domainName' => \Yii::$app->params['uniSender']['domainName'],
                'feedback' => \Yii::$app->params['uniSender']['feedback'],
                'login' => $this->email,
                'password' => $newPassword,
                'supportEmail' => \Yii::$app->params['emailList']['support'],
                'callUs' => \Yii::$app->params['uniSender']['phone'],
                'companyTypeShort' => ($this->company) ? $this->company->companyType->name_short : '',
                'companyName' => ($this->company) ? $this->company->name_short : '',
            ],
            'subject' => 'Регистрация в сервисе КУБ',
        ])
            ->setSubject('Регистрация в сервисе КУБ')
            ->setFrom([\Yii::$app->params['emailList']['support'] => \Yii::$app->params['emailFromName']])
            ->setTo($this->email)
            ->send();
    }

    public function sendNewCompanyEmail(Company $company)
    {
        return \Yii::$app->mailer->compose([
            'html' => 'system/add-to-company/html',
            'text' => 'system/add-to-company/text',
        ], [
            'user' => $this,
            'company' => $company,
            'subject' => 'Регистрация в сервисе КУБ',
        ])
            ->setSubject('Регистрация в сервисе КУБ')
            ->setFrom([\Yii::$app->params['emailList']['support'] => \Yii::$app->params['emailFromName']])
            ->setTo($this->email)
            ->send();
    }

    /**
     * Returns FIO
     *
     * @param bool|false $short
     * @return string
     */
    public function getFio($short = false)
    {
        return join(' ', array_filter([
            $this->lastname,
            $short ? ($this->firstname_initial ? $this->firstname_initial . '.' : '') : $this->firstname,
            $short ? ($this->patronymic_initial ? $this->patronymic_initial . '.' : '') : $this->patronymic,
        ]));
    }

    /**
     * @return string
     */
    public function getShortFio()
    {
        return join(' ', array_filter([
            $this->lastname,
            $this->firstname_initial ? $this->firstname_initial . '.' : '',
            $this->patronymic_initial ? $this->patronymic_initial . '.' : '',
        ]));
    }


    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username,]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::find()
            ->isActual()
            ->andWhere([
                'password_reset_token' => $token,
            ])
            ->one();
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int)end($parts);

        return $timestamp + $expire >= time();
    }

    /**
     * @return boolean
     */
    public function getShowAlert()
    {
        switch ($this->alert_close_count) {
            case 0:
                return true;
                break;
            case 1:
                if ($this->alert_close_time < (time() - (3 * 86400))) {
                    return true;
                }
                break;
            case 2:
                if ($this->alert_close_time < (time() - (7 * 86400))) {
                    return true;
                }
                break;
            case 3:
                if ($this->alert_close_time < (time() - (12 * 86400))) {
                    return true;
                }
                break;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function canViewServiceMoreStock()
    {
        $result = false;
        $company = $this->company;
        if ($company->activeSubscribe &&
            $company->activeSubscribe->tariff_id == SubscribeTariff::TARIFF_TRIAL &&
            $company->isActualActiveSubscribe() &&
            $company->serviceMoreStock == null &&
            empty($company->payments) &&
            in_array($company->company_type_id, [CompanyType::TYPE_OOO, CompanyType::TYPE_IP])
        ) {
            $eleventhDayOfTrial = strtotime('+11 days', $company->activeSubscribe->activated_at);
            $currentDate = date(DateHelper::FORMAT_USER_DATE);
            if ($company->getOutInvoiceCount() >= 2 && $company->show_service_more_stock_modal_first_rule) {
                $invoiceDate = $this->company->getInvoices()->andWhere([
                    'and',
                    ['type' => Documents::IO_TYPE_OUT],
                    ['is_deleted' => false],
                ])->orderBy(['created_at' => SORT_ASC])
                    ->limit(2)
                    ->max('created_at');
                $showModalFirstRuleDate = strtotime('+1 day', $invoiceDate);

                if (date(DateHelper::FORMAT_USER_DATE, $showModalFirstRuleDate) <= $currentDate &&
                    $currentDate <= date(DateHelper::FORMAT_USER_DATE, $eleventhDayOfTrial)) {
                    $result = true;
                    $company->show_service_more_stock_modal_first_rule = false;
                }
            }
            if ($company->show_service_more_stock_modal_second_rule) {
                $fifthDayOfTrial = strtotime('+5 days', $company->activeSubscribe->activated_at);
                if (date(DateHelper::FORMAT_USER_DATE, $fifthDayOfTrial) <= $currentDate &&
                    $currentDate <= date(DateHelper::FORMAT_USER_DATE, $eleventhDayOfTrial)) {
                    $result = true;
                    $company->show_service_more_stock_modal_second_rule = false;
                }
            }
            $company->save(true, ['show_service_more_stock_modal_first_rule', 'show_service_more_stock_modal_second_rule']);
        }

        return $result;
    }

    /**
     * @return bool
     */
    public function canShowScanPopup()
    {
        if ($this->show_scan_popup) {
            return false;
        }
        $count = $this->company->getInvoices()
            ->andWhere([
                'and',
                ['type' => Documents::IO_TYPE_OUT],
                ['is_deleted' => false],
                ['not', ['invoice_status_id' => InvoiceStatus::STATUS_AUTOINVOICE]],
            ])
            ->andWhere(['>', 'document_number', self::DOC_NUMBER_AFTER_SHOW_SCAN_POPUP])
            ->count();
        return $count == 1;
    }

    /**
     * @return bool
     */
    public function getCanAddCompany()
    {
        if ($this->_canAddCompany === null) {
            $this->_canAddCompany = !$this->getNotPaidOwnCompanies()->exists() || PaymentOrder::find()->andWhere([
                'employee_id' => $this->id,
                'company_id' => null,
            ])->exists();
        }

        return $this->_canAddCompany;
    }

    public function linkPaidTariffsWithCompany(Company $company, $returnIfError = false)
    {
        $paymentOrderArray = PaymentOrder::find()->andWhere([
            'employee_id' => $this->id,
            'company_id' => null,
        ])->groupBy([
            'payment_id',
            'tariff_id',
        ])->with([
            'payment',
            'tariff.tariffGroup',
        ])->all();

        foreach ($paymentOrderArray as $paymentOrder) {
            if ($paymentOrder->tariff->tariffGroup->activeByCompany($company)) {
                if (!$paymentOrder->associateWithCompany($company, false) && $returnIfError) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Deleting user sessions
     *
     * @param string|array $except
     */
    public function closeOpenSessions($except = null)
    {
        Yii::$app->db->createCommand()->delete(Employee::sessionTableName(), [
            'and',
            ['employee_id' => $this->id],
            ['not', ['id' => $except]],
        ])->execute();
    }

    /**
     * Возвращает настройки интеграции указанного модуля для текущей компании
     *
     * @param string $module
     * @return mixed|null Массив настроек или null, если модуль интеграции не подключён
     * @todo Удалить
     */
    public function integration(string $module)
    {
        return $this->company->integration($module);
    }

    /**
     * Устанавливает параметры интеграции для модуля для текущей компании
     *
     * @param string     $module
     * @param mixed|null $config Если NULL, то данные для соответствующего модуля будут удалены
     * @throws InvalidArgumentException
     * @todo Удалить
     */
    public function setIntegration(string $module, $config)
    {
        $this->company->setIntegration($module, $config);
    }

    /**
     * Сохраняет параметры интеграции для текущей компании в базе данных
     *
     * @param string     $module
     * @param mixed|null $config Если NULL, то данные для соответствующего модуля будут удалены
     * @return bool
     * @throws InvalidArgumentException
     */
    public function saveIntegration(string $module = null, $config = null): bool
    {
        return $this->company->saveIntegration($module, $config);
    }

    /**
     * Отправить кода подтверждения Email
     *
     * @return bool
     */
    public function sendEmailConfirmCode($from = null)
    {
        if ($this->email) {
            $code = $this->generatePIN(4);
            $subject = 'Код подтверждения Email';
            $mailer = clone \Yii::$app->mailer;
            $mailer->htmlLayout = 'layouts/html2';

            $sent = $mailer->compose([
                    'html' => 'system/confirm-email-code/html',
                    'text' => 'system/confirm-email-code/text',
                ], [
                    'subject' => $subject,
                    'code' => $code,
                ])
                ->setFrom([\Yii::$app->params['emailList']['support'] => $from ?? \Yii::$app->params['emailFromName']])
                ->setTo($this->email)
                ->setSubject($subject)
                ->send();

            if ($sent) {
                $this->updateAttributes([
                    'email_confirm_key' => $code,
                ]);

                return true;
            }
        }

        return false;
    }

    /**
     * Проверить код подтверждения Email
     *
     * @param string $code
     * @return bool
     */
    public function verifyEmailConfirmCode(string $code)
    {
        if ($code && $this->email && $this->email_confirm_key && $this->email_confirm_key === $code) {
            return true;
        }

        return false;
    }

    /**
     * Confirm Email
     */
    public function confirmEmail()
    {
        $this->updateAttributes([
            'email_confirm_key' => null,
            'is_email_confirmed' => true,
        ]);
    }

    public function generatePIN($digits = 4){
        $i = 0;
        $pin = "";
        while($i < $digits){
            $pin .= mt_rand(1, 9);
            $i++;
        }
        return $pin;
    }

    public function getLinksColor()
    {
        return self::$linkColors[$this->links_color ?? 0];
    }

    public function needCheckFinishedUnviewedJobs($val)
    {
        $this->updateAttributes([
            'has_finished_unviewed_jobs' => ($val ? true : false)
        ]);
    }

    public function getLast_visit_at()
    {
        if (!isset($this->_data['getLast_visit_at']) || !array_key_exists('getLast_visit_at', $this->_data)) {
            $this->_data['getLast_visit_at'] = $this->getCompanyLastVisits()->max('time');
        }

        return $this->_data['getLast_visit_at'];
    }
}
