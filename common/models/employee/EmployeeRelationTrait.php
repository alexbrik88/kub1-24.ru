<?php

namespace common\models\employee;

use yii\db\ActiveQuery;

/**
 * @property-read Employee $employee
 */
trait EmployeeRelationTrait
{
    /**
     * @var string[]
     */
    protected $employeeLink = ['id' => 'employee_id'];

    /**
     * @return Employee
     */
    public function getEmployee(): Employee
    {
        return $this->__get('employeeRelation');
    }

    /**
     * @return ActiveQuery
     */
    public function getEmployeeRelation(): ActiveQuery
    {
        return $this->hasOne(Employee::class, $this->employeeLink);
    }

    /**
     * @return bool
     */
    public function hasEmployee(): bool
    {
        if ($this->__get('employeeRelation')) {
            return true;
        }

        return false;
    }
}
