<?php

namespace common\models\employee;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "email_deny".
 *
 * @property int $id
 * @property string $email
 * @property int $created_at
 */
class EmailDeny extends \yii\db\ActiveRecord
{
    private static $_list;

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'email_deny';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email'], 'required'],
            [['email'], 'email'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'created_at' => 'Created At',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function list()
    {
        if (!isset(self::$_list)) {
            $list = self::find()->select([
                'e' => 'LOWER([[email]])',
            ])->column();

            self::$_list = array_combine($list, $list);
        }

        return self::$_list;
    }
}
