<?php

namespace common\models\employee;


use yii\db\ActiveQuery;

/**
 * Class EmployeeRoleQuery
 * @package common\models\employee
 */
class EmployeeRoleQuery extends ActiveQuery
{

    /**
     *
     * @param $companyId
     * @return EmployeeRoleQuery
     */
    public function actual()
    {
        return $this->andWhere([
            'not',
            [EmployeeRole::tableName() . '.id' => EmployeeRole::ROLE_DEMO],
        ]);
    }

    /**
     *
     * @param $companyId
     * @return EmployeeRoleQuery
     */
    public function excludeKUBRoles($companyId)
    {
        return ($companyId == \Yii::$app->params['service']['company_id']) ? $this : $this->andWhere([
            'not',
            [EmployeeRole::tableName() . '.id' => EmployeeRole::ROLE_PARTNER_RELATIONS_MANAGER],
        ]);
    }
}
