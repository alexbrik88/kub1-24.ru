<?php

namespace common\models\employee;

use common\models\Company;
use common\models\EmployeeCompany;
use Yii;

/**
 * This is the model class for table "employee_salary".
 *
 * @property integer $id
 * @property integer $employee_id
 * @property integer $company_id
 * @property string $date
 * @property integer $days_worked
 * @property integer $salary_1_amount
 * @property integer $bonus_1_amount
 * @property integer $salary_2_amount
 * @property integer $bonus_2_amount
 * @property integer $salary_1_prepay_sum
 * @property integer $bonus_1_prepay_sum
 * @property integer $salary_2_prepay_sum
 * @property integer $bonus_2_prepay_sum
 * @property integer $salary_1_pay_sum
 * @property integer $bonus_1_pay_sum
 * @property integer $salary_2_pay_sum
 * @property integer $bonus_2_pay_sum
 * @property integer $salary_1_expenses
 * @property integer $bonus_1_expenses
 * @property integer $salary_2_expenses
 * @property integer $bonus_2_expenses
 * @property string $salary_1_prepay_date
 * @property string $bonus_1_prepay_date
 * @property string $salary_2_prepay_date
 * @property string $bonus_2_prepay_date
 * @property string $salary_1_pay_date
 * @property string $bonus_1_pay_date
 * @property string $salary_2_pay_date
 * @property string $bonus_2_pay_date
 * @property integer $total_amount
 * @property integer $total_tax_ndfl
 * @property integer $total_tax_social
 * @property integer $total_expenses
 * @property integer $total_prepay_sum
 * @property integer $total_pay_sum
 *
 * @property Company $company
 * @property EmployeeCompany $employeeCompany
 * @property EmployeeSalarySummary $employeeSalarySummary
 */
class EmployeeSalary extends \yii\db\ActiveRecord
{
    use \frontend\components\SalaryFormTrait;

    const TAX_NDFL = 13;
    const TAX_SOCIAL = 30.2;

    protected $_paymentsAmount;
    protected $_taxableAmount;
    protected $_expensesAmount;
    protected $_salary1TaxNdfl;
    protected $_bonus1TaxNdfl;
    protected $_salary1TaxSocial;
    protected $_bonus1TaxSocial;
    protected $_day_count;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'employee_salary';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'salary1Amount',
                    'bonus1Amount',
                    'salary2Amount',
                    'bonus2Amount',
                    'salary1PrepaySum',
                    'bonus1PrepaySum',
                    'salary2PrepaySum',
                    'bonus2PrepaySum',
                    'salary1PrepayDate',
                    'bonus1PrepayDate',
                    'salary2PrepayDate',
                    'bonus2PrepayDate',
                    'salary1PayDate',
                    'bonus1PayDate',
                    'salary2PayDate',
                    'bonus2PayDate',
                    'salary_1_prepay_day',
                    'bonus_1_prepay_day',
                    'salary_2_prepay_day',
                    'bonus_2_prepay_day',
                    'salary_1_pay_day',
                    'bonus_1_pay_day',
                    'salary_2_pay_day',
                    'bonus_2_pay_day',
                ],
                'safe',
            ],
            [
                [
                    'days_worked',
                    'salary_1_amount',
                    'bonus_1_amount',
                    'salary_2_amount',
                    'bonus_2_amount',
                    'salary_1_prepay_sum',
                    'bonus_1_prepay_sum',
                    'salary_2_prepay_sum',
                    'bonus_2_prepay_sum',
                    'salary_1_pay_sum',
                    'bonus_1_pay_sum',
                    'salary_2_pay_sum',
                    'bonus_2_pay_sum',
                    'salary_1_expenses',
                    'bonus_1_expenses',
                    'salary_2_expenses',
                    'bonus_2_expenses',
                    'total_amount',
                    'total_tax_ndfl',
                    'total_tax_social',
                    'total_expenses',
                    'total_prepay_sum',
                    'total_pay_sum',
                ],
                'integer',
            ],
            [
                [
                    'salary_1_prepay_date',
                    'bonus_1_prepay_date',
                    'salary_2_prepay_date',
                    'bonus_2_prepay_date',
                    'salary_1_pay_date',
                    'bonus_1_pay_date',
                    'salary_2_pay_date',
                    'bonus_2_pay_date',
                ],
                'date',
                'format' => 'php:Y-m-d',
            ],
            [
                [
                    'days_worked',
                ],
                'daysWorkedValidator',
            ],
            [
                [
                    'salary_1_prepay_sum',
                    'bonus_1_prepay_sum',
                    'salary_2_prepay_sum',
                    'bonus_2_prepay_sum',
                ],
                'prepayValidator',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function daysWorkedValidator($attribte)
    {
        if ($this->$attribte > $this->getDayCount()) {
            $this->addError($attribte, 'Количество отработанных дней не может быть больше количества дней в месяце');
        }
    }

    /**
     * @inheritdoc
     */
    public function prepayValidator($attribte)
    {
        $depAttribute = strtr($attribte, ['prepay_sum' => 'amount']);
        if ($this->$attribte > $this->$depAttribute) {
            $this->addError($attribte, 'Аванс не может быть больше суммы на руки');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'employee_id' => 'Сотрудник',
            'company_id' => 'Company ID',
            'date' => 'Date',
            'days_worked' => 'Дней отработано',
            'salary_1_amount' => 'Оклад 1 - Сумма на руки',
            'bonus_1_amount' => 'Премия 1 - Сумма на руки',
            'salary_2_amount' => 'Оклад 2 - Сумма на руки',
            'bonus_2_amount' => 'Премия 2 - Сумма на руки',
            'salary_1_prepay_sum' => 'Оклад 1 - Аванса',
            'bonus_1_prepay_sum' => 'Премия 1 - Аванса',
            'salary_2_prepay_sum' => 'Оклад 2 - Аванса',
            'bonus_2_prepay_sum' => 'Премия 2 - Аванса',
            'salary_1_pay_sum' => 'Оклад 1 - Зарплата',
            'bonus_1_pay_sum' => 'Премия 1 - Зарплата',
            'salary_2_pay_sum' => 'Оклад 2 - Зарплата',
            'bonus_2_pay_sum' => 'Премия 2 - Зарплата',
            'salary_1_expenses' => 'Оклад 1 - Затраты для компании',
            'bonus_1_expenses' => 'Премия 1 - Затраты для компании',
            'salary_2_expenses' => 'Оклад 2 - Затраты для компании',
            'bonus_2_expenses' => 'Премия 2 - Затраты для компании',
            'salary_1_prepay_date' => 'Оклад 1 - Дата аванса',
            'bonus_1_prepay_date' => 'Премия 1 - Дата аванса',
            'salary_2_prepay_date' => 'Оклад 2 - Дата аванса',
            'bonus_2_prepay_date' => 'Премия 2 - Дата аванса',
            'salary_1_pay_date' => 'Оклад 1 - Дата зарплаты',
            'bonus_1_pay_date' => 'Премия 1 - Дата зарплаты',
            'salary_2_pay_date' => 'Оклад 2 - Дата зарплаты',
            'bonus_2_pay_date' => 'Премия 2 - Дата зарплаты',
            'total_amount' => 'Сумма выплат на руки',
            'total_tax_ndfl' => 'Сумма НДФЛ',
            'total_tax_social' => 'Сумма соц. налогов',
            'total_expenses' => 'Итого затраты для компании',
            'total_prepay_sum' => 'Сумма аванса',
            'total_pay_sum' => 'Сумма зарплат',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployeeCompany()
    {
        return $this->hasOne(EmployeeCompany::className(), ['employee_id' => 'employee_id', 'company_id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployeeSalarySummary()
    {
        return $this->hasOne(EmployeeSalarySummary::className(), ['company_id' => 'company_id', 'date' => 'date']);
    }

    /**
     * @return integer
     */
    public static function taxNdfl($value)
    {
        return round($value / (100 - self::TAX_NDFL) * self::TAX_NDFL);
    }

    /**
     * @return integer
     */
    public static function taxSocial($value)
    {
        return round($value / 100 * self::TAX_SOCIAL);
    }

    /**
     * @return integer
     */
    public function getDayCount()
    {
        if ($this->_day_count === null) {
            $this->_day_count = (int) date_create($this->date)->format('t');
        }

        return $this->_day_count;
    }

    /**
     * @param integer
     */
    public function setSalary_1_pay_day(int $value = null)
    {
        $this->salary_1_pay_date = $value ? substr_replace($this->date, sprintf('%02d', min($value, $this->getDayCount())), -2) : null;
    }

    /**
     * @return integer
     */
    public function getSalary_1_pay_day()
    {
        return $this->salary_1_pay_date ? (int) date_create($this->salary_1_pay_date)->format('j') : null;
    }

    /**
     * @param integer
     */
    public function setBonus_1_pay_day(int $value = null)
    {
        $this->bonus_1_pay_date = $value ? substr_replace($this->date, sprintf('%02d', min($value, $this->getDayCount())), -2) : null;
    }

    /**
     * @return integer
     */
    public function getBonus_1_pay_day()
    {
        return $this->bonus_1_pay_date ? (int) date_create($this->bonus_1_pay_date)->format('j') : null;
    }

    /**
     * @param integer
     */
    public function setSalary_2_pay_day(int $value = null)
    {
        $this->salary_2_pay_date = $value ? substr_replace($this->date, sprintf('%02d', min($value, $this->getDayCount())), -2) : null;
    }

    /**
     * @return integer
     */
    public function getSalary_2_pay_day()
    {
        return $this->salary_2_pay_date ? (int) date_create($this->salary_2_pay_date)->format('j') : null;
    }

    /**
     * @param integer
     */
    public function setBonus_2_pay_day(int $value = null)
    {
        $this->bonus_2_pay_date = $value ? substr_replace($this->date, sprintf('%02d', min($value, $this->getDayCount())), -2) : null;
    }

    /**
     * @return integer
     */
    public function getBonus_2_pay_day()
    {
        return $this->bonus_2_pay_date ? (int) date_create($this->bonus_2_pay_date)->format('j') : null;
    }

    /**
     * @param integer
     */
    public function setSalary_1_prepay_day(int $value = null)
    {
        $this->salary_1_prepay_date = $value ? substr_replace($this->date, sprintf('%02d', min($value, $this->getDayCount())), -2) : null;
    }

    /**
     * @return integer
     */
    public function getSalary_1_prepay_day()
    {
        return $this->salary_1_prepay_date ? (int) date_create($this->salary_1_prepay_date)->format('j') : null;
    }

    /**
     * @param integer
     */
    public function setBonus_1_prepay_day(int $value = null)
    {
        $this->bonus_1_prepay_date = $value ? substr_replace($this->date, sprintf('%02d', min($value, $this->getDayCount())), -2) : null;
    }

    /**
     * @return integer
     */
    public function getBonus_1_prepay_day()
    {
        return $this->bonus_1_prepay_date ? (int) date_create($this->bonus_1_prepay_date)->format('j') : null;
    }

    /**
     * @param integer
     */
    public function setSalary_2_prepay_day(int $value = null)
    {
        $this->salary_2_prepay_date = $value ? substr_replace($this->date, sprintf('%02d', min($value, $this->getDayCount())), -2) : null;
    }

    /**
     * @return integer
     */
    public function getSalary_2_prepay_day()
    {
        return $this->salary_2_prepay_date ? (int) date_create($this->salary_2_prepay_date)->format('j') : null;
    }

    /**
     * @param integer
     */
    public function setBonus_2_prepay_day(int $value = null)
    {
        $this->bonus_2_prepay_date = $value ? substr_replace($this->date, sprintf('%02d', min($value, $this->getDayCount())), -2) : null;
    }

    /**
     * @return integer
     */
    public function getBonus_2_prepay_day()
    {
        return $this->bonus_2_prepay_date ? (int) date_create($this->bonus_2_prepay_date)->format('j') : null;
    }

    /**
     * @return integer
     */
    public function getSalary1TaxNdfl()
    {
        if ($this->_salary1TaxNdfl === null) {
            $this->_salary1TaxNdfl = self::taxNdfl($this->salary_1_amount);
        }

        return $this->_salary1TaxNdfl;
    }

    /**
     * @return integer
     */
    public function getSalary1TaxSocial()
    {
        if ($this->_salary1TaxSocial === null) {
            $this->_salary1TaxSocial = self::taxSocial($this->salary_1_amount + $this->getSalary1TaxNdfl());
        }

        return $this->_salary1TaxSocial;
    }

    /**
     * @return integer
     */
    public function getBonus1TaxNdfl()
    {
        if ($this->_bonus1TaxNdfl === null) {
            $this->_bonus1TaxNdfl = self::taxNdfl($this->bonus_1_amount);
        }

        return $this->_bonus1TaxNdfl;
    }

    /**
     * @return integer
     */
    public function getBonus1TaxSocial()
    {
        if ($this->_bonus1TaxSocial === null) {
            $this->_bonus1TaxSocial = self::taxSocial($this->bonus_1_amount + $this->getBonus1TaxNdfl());
        }

        return $this->_bonus1TaxSocial;
    }

    /**
     * @inheritdoc
     */
    public function setData(Company $company, EmployeeCompany $employeeCompany = null)
    {
        $this->setSalary_1_prepay_day($company->salary_1_prepay_day ? : null);
        $this->setSalary_1_pay_day($company->salary_1_pay_day ? : null);
        $this->setBonus_1_prepay_day($company->bonus_1_prepay_day ? : null);
        $this->setBonus_1_pay_day($company->bonus_1_pay_day ? : null);
        $this->setSalary_2_prepay_day($company->salary_2_prepay_day ? : null);
        $this->setSalary_2_pay_day($company->salary_2_pay_day ? : null);
        $this->setBonus_2_prepay_day($company->bonus_2_prepay_day ? : null);
        $this->setBonus_2_pay_day($company->bonus_2_pay_day ? : null);

        if ($employeeCompany !== null) {
            if ($employeeCompany->has_salary_1) {
                $this->salary_1_amount = $employeeCompany->salary_1_amount;
                $this->salary_1_prepay_sum = $employeeCompany->salary_1_prepay_sum;
            } else {
                $this->salary_1_amount = null;
            }
            if ($employeeCompany->has_bonus_1) {
                $this->bonus_1_amount = $employeeCompany->bonus_1_amount;
                $this->bonus_1_prepay_sum = $employeeCompany->bonus_1_prepay_sum;
            } else {
                $this->bonus_1_amount = null;
            }
            if ($employeeCompany->has_salary_2) {
                $this->salary_2_amount = $employeeCompany->salary_2_amount;
                $this->salary_2_prepay_sum = $employeeCompany->salary_2_prepay_sum;
            } else {
                $this->salary_2_amount = null;
            }
            if ($employeeCompany->has_bonus_2) {
                $this->bonus_2_amount = $employeeCompany->bonus_2_amount;
                $this->bonus_2_prepay_sum = $employeeCompany->bonus_2_prepay_sum;
            } else {
                $this->bonus_2_amount = null;
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function calculateTotalFields()
    {
        if ($this->salary_1_amount === null) {
            $this->salary_1_prepay_sum =
            $this->salary_1_pay_sum =
            $this->salary_1_expenses = null;
        } else {
            $this->salary_1_pay_sum = $this->salary_1_amount - $this->salary_1_prepay_sum;
            $this->salary_1_expenses = $this->salary_1_amount +
                                       $this->getSalary1TaxNdfl() +
                                       $this->getSalary1TaxSocial();
        }
        if ($this->bonus_1_amount === null) {
            $this->bonus_1_prepay_sum =
            $this->bonus_1_pay_sum =
            $this->bonus_1_expenses = null;
        } else {
            $this->bonus_1_pay_sum = $this->bonus_1_amount - $this->bonus_1_prepay_sum;
            $this->bonus_1_expenses = $this->bonus_1_amount +
                                       $this->getBonus1TaxNdfl() +
                                       $this->getBonus1TaxSocial();
        }
        if ($this->salary_2_amount === null) {
            $this->salary_2_prepay_sum =
            $this->salary_2_pay_sum =
            $this->salary_2_expenses = null;
        } else {
            $this->salary_2_pay_sum = $this->salary_2_amount - $this->salary_2_prepay_sum;
            $this->salary_2_expenses = $this->salary_2_amount;
        }
        if ($this->bonus_2_amount === null) {
            $this->bonus_2_prepay_sum =
            $this->bonus_2_pay_sum =
            $this->bonus_2_expenses = null;
        } else {
            $this->bonus_2_pay_sum = $this->bonus_2_amount - $this->bonus_2_prepay_sum;
            $this->bonus_2_expenses = $this->bonus_2_amount;
        }

        $this->total_amount = $this->salary_1_amount +
                               $this->bonus_1_amount +
                               $this->salary_2_amount +
                               $this->bonus_2_amount;

        $this->total_prepay_sum = $this->salary_1_prepay_sum +
                               $this->bonus_1_prepay_sum +
                               $this->salary_2_prepay_sum +
                               $this->bonus_2_prepay_sum;

        $this->total_pay_sum = $this->salary_1_pay_sum +
                               $this->bonus_1_pay_sum +
                               $this->salary_2_pay_sum +
                               $this->bonus_2_pay_sum;

        $this->total_tax_ndfl = $this->getSalary1TaxNdfl() +
                                  $this->getBonus1TaxNdfl();

        $this->total_tax_social = $this->getSalary1TaxSocial() +
                                  $this->getBonus1TaxSocial();

        $this->total_expenses = $this->salary_1_expenses +
                                $this->bonus_1_expenses +
                                $this->salary_2_expenses +
                                $this->bonus_2_expenses;
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        $this->calculateTotalFields();

        return parent::beforeValidate();
    }
}
