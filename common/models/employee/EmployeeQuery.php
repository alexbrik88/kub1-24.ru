<?php

namespace common\models\employee;


use yii\db\ActiveQuery;

/**
 * Class EmployeeQuery
 * @package common\models\employee
 */
class EmployeeQuery extends ActiveQuery
{

    /**
     *
     * @param $companyId
     * @return EmployeeQuery
     */
    public function byCompany($companyId)
    {
        return $this->andWhere([
            Employee::tableName() . '.company_id' => $companyId,
        ]);
    }

    /**
     * @param $isDeleted
     * @return static
     */
    public function byIsDeleted($isDeleted)
    {
        return $this->andWhere([
            'is_deleted' => (bool) $isDeleted,
        ]);
    }

    /**
     * @param $isActive
     * @return static
     */
    public function byIsActive($isActive)
    {
        return $this->andWhere([
            'is_active' => (bool) $isActive,
        ]);
    }

    /**
     * @param $isWorking
     * @return static
     */
    public function byIsWorking($isWorking)
    {
        return $this->andWhere([
            'is_working' => (bool) $isWorking,
        ]);
    }


    /**
     * @return static
     */
    public function isActual()
    {
        return $this
            ->byIsDeleted(Employee::NOT_DELETED)
            ->byIsActive(Employee::ACTIVE)
            ->byIsWorking(Employee::STATUS_IS_WORKING);
    }

    /**
     * @param $email
     * @return $this
     */
    public function byEmail($email)
    {
        return $this->andWhere([
            'email' => $email,
        ]);
    }

    /**
     * @param $role
     * @return $this
     */
    public function byRole($role)
    {
        return $this->andWhere([
            'employee_role_id' => $role,
        ]);
    }

    /**
     * @param $email
     * @return $this
     */
    public function byEmailFilter($email)
    {
        return $this->andFilterWhere(['like', 'email', $email]);
    }

}
