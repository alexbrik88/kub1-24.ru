<?php

namespace common\models;


use yii\db\Query;

/**
 * Interface ICompanyStrictQuery
 *
 * @package common\models
 */
interface ICompanyStrictQuery
{
    /**
     * @param int $companyId
     * @return $this
     */
    public function byCompany($companyId);
}