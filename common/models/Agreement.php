<?php

namespace common\models;

use common\components\helpers\ModelHelper;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\InvoiceFacture;
use common\models\document\PackingList;
use common\models\document\SalesInvoice;
use common\models\document\Upd;
use frontend\models\log\ILogMessage;
use frontend\modules\export\models\one_c\OneCExport;
use Yii;
use common\components\date\DateHelper;
use common\components\UploadedFile;
use common\models\file\File;
use common\models\employee\Employee;
use yii\base\InvalidParamException;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

use common\components\helpers\Html;
use frontend\models\Documents;
use common\models\document\AbstractDocument;

use common\models\document\status\AgreementStatus;
use frontend\models\log\Log;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\models\log\LogEntityType;

use common\components\TextHelper;
use common\models\document\EmailFile;
use common\components\pdf\PdfRenderer;
use yii\db\ActiveQuery;
use yii\helpers\Url;
use store\assets\DocumentPrintAsset;

/**
 * This is the model class for table "agreement".
 *
 * @property integer $id
 * @property integer $company_id
 * @property integer $contractor_id
 * @property integer $title_template_id
 * @property string $document_date
 * @property string $document_number
 * @property string $document_name
 * @property integer $created_at
 * @property integer $created_by
 * @property string $document_date_input
 * @property string $object_guid
 *
 * @property integer $agreement_template_id
 * @property integer $type
 * @property string $uid
 * @property string $document_additional_number
 * @property integer $is_completed
 * @property integer $document_author_id
 * @property string $comment_internal
 * @property string $company_rs
 * @property string $payment_limit_date
 * @property string $payment_limit_date_input
 * @property integer $payment_delay
 * @property integer $document_type_id
 *
 * @property integer $status_id
 * @property integer $status_updated_at
 * @property integer $status_author_id
 *
 * @property integer $signed_by_employee_id
 * @property string $signed_by_name
 * @property integer $sign_document_type_id
 * @property string $sign_document_number
 * @property string $sign_document_date
 * @property integer $signature_id
 *
 * @property Employee $createdBy
 * @property Company $company
 * @property Contractor $contractor
 * @property AgreementType $agreementType
 * @property AgreementEssence $essence
 * @property-read AgreementTemplate|null $template
 * @property-read File[] $files
 */
class Agreement extends AbstractDocument implements ILogMessage
{
    const DEFAULT_NAME = 'Договор';

    const CREATE_AGREEMENT_FROM_TEMPLATE = 1;

    /**
     * The maximum uploaded file count
     */
    const MAX_FILES = 3;

    public static $uploadDirectory = 'agreement';
    public $files_upload;
    public $files_delete;

    /**
     * @var string
     */
    public $printablePrefix = self::DEFAULT_NAME;

    /**
     * @var string
     */
    public $urlPart = 'agreement';

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'documentCreatedAtBehavior' => [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
            ],
            'blameableBehavior' => [
                'class' => BlameableBehavior::className(),
                'updatedByAttribute' => false,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'agreement';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'document_number',
                    'contractor_id',
                    'title_template_id',
                    //'document_date_input',
                    //'document_date',
                    'document_type_id',
                ], 'required',
                'message' => 'Необходимо заполнить',
            ],
            [['document_name'], 'required', 'message' => 'Необходимо заполнить', 'when' => function ($model) {
                return $model->document_type_id == AgreementType::TYPE_AGREEMENT;
            }],
            [['document_number'], 'string', 'max' => 50],
            [['document_name'], 'string', 'max' => 255],
            [['document_date_input'], 'date', 'format' => 'php:d.m.Y', 'message' => 'Дата указана неверно.'],
            [['document_date'], 'date', 'format' => 'php:Y-m-d', 'message' => 'Дата указана неверно.'],
            [['payment_limit_date_input'], 'date', 'format' => 'php:d.m.Y', 'message' => 'Дата указана неверно.'],
            [['payment_limit_date'], 'date', 'format' => 'php:Y-m-d', 'message' => 'Дата указана неверно.'],
            [['company_rs'], 'string', 'length' => 20],
            [
                ['contractor_id'], 'exist',
                'targetClass' => Contractor::class,
                'targetAttribute' => 'id',
                'filter' => [
                    'or',
                    ['company_id' => $this->company_id],
                    ['company_id' => null],
                ]
            ],
            [
                ['title_template_id'], 'exist',
                'targetClass' => AgreementTitleTemplate::class,
                'targetAttribute' => 'id',
            ],
            [
                ['agreement_template_id'], 'exist',
                'targetClass' => AgreementTemplate::class,
                'targetAttribute' => 'id',
                'filter' => ['company_id' => $this->company_id]
            ],
            [
                ['document_type_id'], 'exist',
                'targetClass' => AgreementType::class,
                'targetAttribute' => 'id',
            ],
            [['files_delete'] , 'each', 'rule' => ['integer',],],
            [
                ['files_upload'] , 'file',
                'maxFiles' => self::MAX_FILES,
                'maxSize' => File::$maxFileSize,
                'extensions' => File::$fileExtensions,
            ],
            [['document_additional_number'], 'string', 'max' => 255],
            [['type'], 'safe'],
            [['company_rs', 'payment_limit_date_input', 'agreement_template_id'], 'required', 'message' => 'Необходимо заполнить', 'when' => function ($model) {
                return ($model->create_agreement_from == 1);
            }],
            [['created_by'], 'safe'],
            [['one_c_imported', 'one_c_exported'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'contractor_id' => 'Contractor ID',
            'document_date' => 'Дата',
            'document_date_input' => 'Дата',
            'document_number' => 'Номер документа',
            'document_name' => 'Название документа',
            'document_type_id' => 'Тип',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'comment_internal' => 'Комментарий',
            'payment_limit_date' => 'Дата окончания',
            'payment_limit_date_input' => 'Дата окончания',
            'payment_delay' => 'Отсрочка платежа',
            'agreement_template_id' => 'Шаблон документа',
            'company_rs' => 'Расчетный счет',
            'status_id' => 'Status ID',
            'status_updated_at' => 'Status Date',
            'status_author_id' => 'Status Author',
            'title_template_id' => 'Выводить в документах',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(Employee::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContractor()
    {
        return $this->hasOne(Contractor::className(), ['id' => 'contractor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreementType()
    {
        return $this->hasOne(AgreementType::className(), ['id' => 'document_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentType()
    {
        return $this->hasOne(AgreementType::className(), ['id' => 'document_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreementTitleTemplate()
    {
        return $this->hasOne(AgreementTitleTemplate::className(), ['id' => 'title_template_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(AgreementStatus::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentStatus()
    {
        return $this->getStatus();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusAuthor()
    {
        return $this->hasOne(Employee::className(), ['id' => 'status_author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasMany(File::className(), ['owner_id' => 'id'])
            ->onCondition(['owner_model' => Agreement::className(),]);
    }

    /**
     * @inheritdoc
     */
    public function getFile()
    {
        return $this->hasOne(File::className(), ['owner_id' => 'id'])
            ->onCondition(['owner_model' => static::className(),]);
    }

    /**
     * @inheritdoc
     */
    public function getListItemValue($withId = true)
    {
        $result = $this->name . '&' . $this->getFullNumber() . '&' . $this->document_date . '&' . $this->document_type_id;
        if ($withId) {
            $result .= '&' . $this->id;
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function getListItemName()
    {
        $date = DateHelper::format($this->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

        return ($this->document_date) ?
            "{$this->name} № {$this->getFullNumber()} от {$date}" :
            "{$this->name} № {$this->getFullNumber()}";
    }

    /**
     * @inheritdoc
     */
    public function load($data, $formName = null)
    {
        $load = parent::load($data, $formName);
        $this->files_upload = $formName === '' ?
                              UploadedFile::getInstancesByName('files_upload') :
                              UploadedFile::getInstances($this, 'files_upload');

        return $load || $this->files_upload !== [];
    }

    /**
     * @inheritdoc
     */
    public function setFiles_delete($value)
    {
        $this->_files_delete = is_string($value) ? explode(',', $value) : null;
    }

    /**
     * @inheritdoc
     */
    public function getFiles_delete()
    {
        return $this->_files_delete ? implode(',', $this->_files_delete) : null;
    }

    /**
     * @inheritdoc
     */
    public function setDocument_date_input($value)
    {
        $this->document_date = $value !== null && ($d = date_create($value)) ? $d->format('Y-m-d') : $value;
    }

    /**
     * @inheritdoc
     */
    public function getDocument_date_input()
    {
        return $this->document_date !== null && ($d = date_create($this->document_date)) ? $d->format('d.m.Y') : $this->document_date;
    }

    /**
     * @inheritdoc
     */
    public function setPayment_limit_date_input($value)
    {
        $this->payment_limit_date = $value !== null && ($d = date_create($value)) ? $d->format('Y-m-d') : $value;
    }

    /**
     * @inheritdoc
     */
    public function getPayment_limit_date_input()
    {
        return $this->payment_limit_date !== null && ($d = date_create($this->payment_limit_date)) ? $d->format('d.m.Y') : $this->payment_limit_date;
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            ModelHelper::HtmlEntitiesModelAttributes($this);
            if ($this->document_date_input && $d = date_create($this->document_date_input)) {
                $this->document_date = $d->format('Y-m-d');
            }

            if ($insert) {
                if (!Yii::$app->request->isConsoleRequest) {
                    $this->status_author_id = Yii::$app->user->id;
                    $this->status_id = AgreementStatus::STATUS_CREATED;
                    $this->status_updated_at = time();
                }
                if (empty($this->created_by)) {
                    $this->created_by = $this->document_author_id;
                }
            }

            if (empty($this->title_template_id)) {
                $this->title_template_id = AgreementTitleTemplate::TITLE_BY_TYPE;
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($this->files_delete) {
            $fileArray = $this->getFiles()->andWhere(['id' => $this->files_delete])->all();
            foreach ($fileArray as $file) {
                $file->delete();
            }
        }

        if ($this->files_upload) {
            for ($i = $this->getFiles()->count(); $i < 3; $i++) {
                $fileInstance = array_shift($this->files_upload);
                $file = new File();
                $file->uploadDirectoryPath = self::$uploadDirectory;
                $file->ownerModel = $this;
                $file->file = $fileInstance;
                $file->created_at_author_id = $this->created_by;
                $file->company_id = $this->company_id;
                $file->owner_model = Agreement::class;
                $file->owner_id = $this->id;
                $file->save();
            }
        }
    }

    /**
     * @return boolean
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            foreach ($this->getFiles()->all() as $file) {
                if ($file->delete() === false) {
                    return false;
                }
            }

            return true;
        } else {
            return false;
        }
    }


    /**
     *  ==== MXFI ====
     */

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEssence()
    {
        return $this->hasOne(AgreementEssence::className(), ['agreement_id' => 'id']);
    }

    public function generateEssence()
    {
        if ($this->agreement_template_id) {

            $essenceModel = new AgreementEssence();

            // AgreementTemplate info
            $tplModel = AgreementTemplate::findOne($this->agreement_template_id);
            // Contractor info
            $contractor = Contractor::findOne($this->contractor_id);
            // Company info
            $company = Company::findOne($this->company_id);

            // Company bank info
            $companyRs = $company->getCheckingAccountants()
                ->select(['bank_name', 'rs', 'ks', 'bik'])
                ->andWhere(['rs' => $this->company_rs])
                ->orderBy(['type' => SORT_ASC])
                ->indexBy('rs')
                ->asArray()->one();

            if ($tplModel && $contractor && $company && $companyRs) {
                $essenceModel->agreement_id = $this->id;
                $essenceModel->document_header = AgreementTemplate::replacePatterns($tplModel->document_header, $this, $company, $contractor, $companyRs);
                $essenceModel->document_body = AgreementTemplate::replacePatterns($tplModel->document_body, $this, $company, $contractor, $companyRs);
                $essenceModel->document_requisites_customer = AgreementTemplate::replacePatterns($tplModel->document_requisites_customer, $this, $company, $contractor, $companyRs);
                $essenceModel->document_requisites_executer = AgreementTemplate::replacePatterns($tplModel->document_requisites_executer, $this, $company, $contractor, $companyRs);
                if ($essenceModel->save()) {

                    $this->document_additional_number = $tplModel->document_additional_number;
                    $this->payment_delay = $tplModel->payment_delay;
                    $this->document_type_id = $tplModel->document_type_id;
                    $this->save();

                    $tplModel->documents_created += 1;
                    $tplModel->save();

                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @return string
     */
    public function getFullNumber()
    {

        if ($this->document_additional_number)
            return $this->document_additional_number . '-' . $this->document_number;

        return $this->document_number;
    }

    /**
     * @param Company|int $company
     * @return string
     */
    public function getNextDocumentNumber($company, $agreement_template = null)
    {
        $numQuery = self::find()->andWhere([
            'company_id' => ($company instanceof Company) ? $company->id : (is_int($company) ? $company : 0),
        ]);
        if ($agreement_template > 0) {
            $numQuery->andWhere(['agreement_template_id' => $agreement_template]);
        }
        $maxNumber = $numQuery->max('document_number * 1');

        return (string)(1 + (int)$maxNumber);
    }

    /**
     * Get formatted log message from Log model
     * @param Log $log
     * @return string
     */
    public function getLogMessage(Log $log)
    {
        $text = (($this->documentType->name ?? 'Договор') . ' №' . join(' ', array_filter([
                $log->getModelAttributeNew('document_number'),
                $log->getModelAttributeNew('document_additional_number'),
            ])));

        $link = Html::a($text, [
            '/documents/agreement/view',
            'id' => $log->getModelAttributeNew('id'),
        ]);

        $innerText = null;

        switch ($log->log_event_id) {
            case LogEvent::LOG_EVENT_CREATE:
                return $link . $innerText . ' был создан.';
            case LogEvent::LOG_EVENT_DELETE:
                return $text . $innerText . ' был удалён.';
            case LogEvent::LOG_EVENT_UPDATE:
                return $link . $innerText . ' был изменен.';
            case LogEvent::LOG_EVENT_UPDATE_STATUS:
                $status_id = $log->getModelAttributeNew('status_id');
                $status_name = AgreementStatus::findOne($status_id)->name;
                if ($status_id == AgreementStatus::STATUS_CREATED)
                    $status_name = 'В работе';

                return $link . $innerText . ' статус "' . $status_name . '"';
            default:
                return $log->message;
        }
    }

    /**
     * @inheritdoc
     */
    public function setStatusPrinted()
    {
        if ($this && in_array($this->status_id, [AgreementStatus::STATUS_CREATED, AgreementStatus::STATUS_PRINTED])) {
            LogHelper::save($this, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function ($model) {
                $model->status_id = AgreementStatus::STATUS_PRINTED;
                $model->status_updated_at = time();
                $model->status_author_id = \Yii::$app->user->id;

                return $model->save(true, ['status_id', 'status_updated_at', 'status_author_id']);
            });
        }
    }

    /**
     * ==== EMAILS ====
     */

    /**
     * @return string
     */
    public function getEmailSubject()
    {
        return $this->documentType->name . ' № ' . $this->getFullNumber()
            . ' от ' . DateHelper::format($this->document_date, 'd.m.y', DateHelper::FORMAT_DATE)
            . ' от ' . $this->company->getShortName();
    }

    /**
     * @return string
     */
    public function getEmailText()
    {
        $document_date = DateHelper::format($this->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

        $text = <<<EMAIL_TEXT
Здравствуйте!

{$this->documentType->name} № {$this->fullNumber} от {$document_date}.
EMAIL_TEXT;

        return $text;
    }


    /**
     * @param EmployeeCompany $sender
     * @param array|string $toEmail
     * @param null $emailText
     * @param null $subject
     * @param null $addStamp
     * @param null $addHtml
     * @param null $attach
     * @param bool $onlyMyAttach
     * @return bool
     * @throws Exception
     * @throws \MpdfException
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function sendAsEmail(EmployeeCompany $sender, $toEmail, $emailText = null, $subject = null, $addStamp = null, $addHtml = null, $attach = null, $onlyMyAttach = false)
    {

        if ($this->uid == null) {
            $this->updateAttributes([
                'uid' => self::generateUid(),
            ]);
        }

        $message = $this->getMailerMessage($sender, $toEmail, $emailText, $subject, $addHtml);

        if ($attach) {
            foreach ($attach as $file) {
                if ($file['is_file']) {
                    if (exif_imagetype($file['content']) === false) {
                        $message->attach($file['content'], [
                            'fileName' => $file['fileName'],
                            'contentType' => $file['contentType'],
                        ]);
                    } else {
                        $message->embed($file['content'], [
                            'fileName' => $file['fileName'],
                            'contentType' => $file['contentType'],
                        ]);
                    }
                } else {
                    $message->attachContent($file['content'], [
                        'fileName' => $file['fileName'],
                        'contentType' => $file['contentType'],
                    ]);
                }
            }
        }

        if ($message->send()) {
            //$this->updateSendStatus($sender->employee_id);

            return true;
        }


        return false;
    }

    /**
     * @return array
     * @throws \MpdfException
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function loadEmailFiles()
    {
        $files = [];

        $emailFile = new EmailFile();
        $emailFile->company_id = $this->company_id;
        $emailFile->file_name = str_replace(['\\', '/'], '_', $this->pdfFileName);
        $emailFile->ext = 'pdf';
        $emailFile->mime = 'application/pdf';
        $emailFile->size = 0;
        $emailFile->type = EmailFile::TYPE_AGREEMENT_DOCUMENT;

        if ($emailFile->save()) {
            $path = $emailFile->getFilePath() . DIRECTORY_SEPARATOR . $emailFile->file_name;

            $size = file_put_contents($path, self::getRenderer(null, $this, PdfRenderer::DESTINATION_STRING)->output(false));

            $emailFile->size = ceil($size / 1024);

            if ($emailFile->save(true, ['size'])) {
                $files[] = [
                    'id' => $emailFile->id,
                    'name' => $emailFile->file_name,
                    'previewImg' => '<img src="/img/email/emailPdf.png" class="preview-img">',
                    'size' => $emailFile->size,
                    'deleteUrl' => Url::to(['/email/delete-email-file', 'id' => $emailFile->id]),
                    'downloadUrl' => Url::to(['/email/download-file', 'id' => $emailFile->id]),
                ];
            }
        }

        return $files;
    }

    /**
     * @param $fileName
     * @param $doc
     * @return PdfRenderer
     */
    public static function getRenderer($fileName, $docModel, $destination = PdfRenderer::DESTINATION_FILE)
    {
        $renderer = new PdfRenderer([
            'view' => '@frontend/modules/documents/views/agreement/pdf-view',
            'params' => array_merge([
                'model' => $docModel,
                'ioType' => $docModel->type,
                'addStamp' => true,
            ]),

            'destination' => $destination,
            'filename' => $fileName,
            'displayMode' => PdfRenderer::DISPLAY_MODE_FULLPAGE,
            'format' => 'A4-P',
        ]);
        \Yii::$app->controller->view->params['asset'] = DocumentPrintAsset::className();

        return $renderer;
    }

    /**
     * @return string
     */
    public function getName()
    {
        switch ($this->title_template_id) {
            case AgreementTitleTemplate::TITLE_BY_TYPE:
                $name = $this->documentType->name;
                break;

            case AgreementTitleTemplate::TITLE_BY_NAME:
                $name = preg_replace("/[^[:alnum:][:space:]]/u", '', $this->document_name);
                break;

            default:
                $name = self::DEFAULT_NAME;
                break;
        }

        return $name;
    }

    /**
     * @return string
     */
    public function getPdfFileName()
    {

        return $this->getPrintTitle() . '.pdf';
    }

    /**
     * @return string
     */
    public function getPrintTitle()
    {
        $this->printablePrefix = $this->documentType->name;

        $document_date = DateHelper::format($this->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
        $company_name = $this->company->name_short;

        return $this->printablePrefix . '_' . $this->getFullNumber() . '_' . $document_date . '_' . str_replace(['"', ' '], '_', $company_name);
    }

    /**
     * @return Agreement
     */
    public function cloneSelf()
    {
        $clone = clone $this;
        $clone->isNewRecord = true;
        $clone->id = null;
        $clone->document_number = $clone->getNextDocumentNumber(Yii::$app->user->identity->company->id, $clone->agreement_template_id);
        $clone->document_date = date(DateHelper::FORMAT_DATE);
        $clone->is_completed = false;
        $clone->created_by = Yii::$app->user->identity->id;
        $clone->save();

        if ($this->essence) {
            $cloneEssence = clone AgreementEssence::findOne(['agreement_id' => $this->id]);
            $cloneEssence->isNewRecord = true;
            $cloneEssence->id = null;
            $cloneEssence->agreement_id = $clone->id;
            $cloneEssence->save();
        }

        return $clone;
    }

    // for modal selection
    public function setCreate_agreement_from($value)
    {
        Yii::$app->session->set('create_agreement_from', $value);
    }

    public function getCreate_agreement_from()
    {
        return (isset(Yii::$app->session)) ? Yii::$app->session->get('create_agreement_from', 0) : null;
    }

    public function hasLogo()
    {
        return $this->essence ?
            strpos($this->essence->document_header, '{Логотип}') !== false :
            false;
    }

    public function getLogoPosition()
    {

        // Check position by tag <p style="text-align: right;">...</p>
        $header_rows = explode("\n", $this->essence->document_header);
        foreach ($header_rows as $row) {
            if (strpos($row, '{Логотип}') !== false) {
                if (strpos($row, 'right') !== false)

                    return 'RIGHT';
            }
        }

        return 'LEFT';
    }

    public function hidePatterns($text)
    {
        return str_replace(['{Логотип}', '{Печать}', '{Подпись}'], '', $text);
    }

    public function replacePatterns($text, $images = null)
    {

        $logoLink = isset($images['logo']) ? $images['logo'] : null;
        $printLink = isset($images['print']) ? $images['print'] : null;
        $signatureLink = isset($images['signature']) ? $images['signature'] : null;

        $text = $signatureLink ?
            str_replace('{Подпись}', "<img style='position:absolute; float: left; margin: -1cm 0 0 -1.25cm' src='{$signatureLink}'/>", $text) :
            str_replace('{Подпись}', "", $text);

        $text = $printLink ?
            str_replace('{Печать}', "<img style='position:absolute; float: left; margin: -0.25cm 0 -3.25cm -1.25cm' src='{$printLink}'/>", $text) :
            str_replace('{Печать}', "", $text);

        $text = str_replace('{Логотип}', "", $text); // Logo added in head of document

        return $text;
    }

    /**
     * @param AbstractDocument $document
     * @param $counter
     * @param $type
     * @return array
     */
    public static function getAgreementObjectByDocument(AbstractDocument $document, $counter, $type)
    {
        if ($document instanceof Invoice) {
            $company = $document->company;
            $contractor = $document->contractor;
        } else {
            $company = $document->invoice->company;
            $contractor = $document->invoice->contractor;
        }
        $agreement = self::getAgreementByDocument($document);
        if ($agreement && (empty($agreement->object_guid))) {
            $agreement->object_guid = OneCExport::generateGUID();
            $agreement->save(false);
        } elseif (empty($contractor->document_guid)) {
            $contractor->document_guid = OneCExport::generateGUID();
            $contractor->save(false);
        }

        if (isset($agreement->one_c_exported) && !$agreement->one_c_exported) {
            $agreement->updateAttributes(['one_c_exported' => 1]);
        }

        $agreementDate = null;
        if ($agreement !== null) {
            $agreementDate = DateHelper::format($agreement->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
        }

        return [
            'guid' => $agreement ? $agreement->object_guid : $contractor->document_guid,
            'object' => [
                '@name' => 'Объект',
                '@attr' => [
                    'Нпп' => $counter,
                    'Тип' => 'СправочникСсылка.ДоговорыКонтрагентов',
                    'ИмяПравила' => 'ДоговорыКонтрагентов',
                ],
                '@child' => [
                    [
                        '@name' => 'Ссылка',
                        '@attr' => [
                            'Нпп' => $counter,
                        ],
                        '@child' => [
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => '{УникальныйИдентификатор}',
                                    'Тип' => 'Строка',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => $agreement ? $agreement->object_guid : $contractor->document_guid,
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Владелец',
                                    'Тип' => 'СправочникСсылка.Контрагенты',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Ссылка',
                                        '@attr' => [
                                            'Нпп' => '',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Свойство',
                                                '@attr' => [
                                                    'Имя' => '{УникальныйИдентификатор}',
                                                    'Тип' => 'Строка',
                                                ],
                                                '@child' => [
                                                    [
                                                        '@name' => 'Значение',
                                                        '@value' => $contractor->object_guid,
                                                    ],
                                                ],
                                            ],
                                            [
                                                '@name' => 'Свойство',
                                                '@attr' => [
                                                    'Имя' => 'ИНН',
                                                    'Тип' => 'Строка'
                                                ],
                                                '@child' => [
                                                    [
                                                        '@name' => 'Значение',
                                                        '@value' => $contractor->ITN,
                                                    ],
                                                ],
                                            ],
                                            !empty($contractor->PPC) ?
                                                [
                                                    '@name' => 'Свойство',
                                                    '@attr' => [
                                                        'Имя' => 'КПП',
                                                        'Тип' => 'Строка',
                                                    ],
                                                    '@child' => [
                                                        [
                                                            '@name' => 'Значение',
                                                            '@value' => $contractor->PPC,
                                                        ],
                                                    ],
                                                ] : [],
                                            empty($contractor->ITN) ?
                                                [
                                                    '@name' => 'Свойство',
                                                    '@attr' => [
                                                        'Имя' => 'НазваниеКонтрагента',
                                                        'Тип' => 'Строка',
                                                    ],
                                                    '@child' => [
                                                        [
                                                            '@name' => 'Значение',
                                                            '@value' => $contractor->getShortName(),
                                                        ],
                                                    ],
                                                ] : [],
                                        ],
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Организация',
                                    'Тип' => 'СправочникСсылка.Организации',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Ссылка',
                                        '@attr' => [
                                            'Нпп' => '',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Свойство',
                                                '@attr' => [
                                                    'Имя' => 'ИНН',
                                                    'Тип' => 'Строка',
                                                ],
                                                '@child' => [
                                                    [
                                                        '@name' => 'Значение',
                                                        '@value' => $company->inn,
                                                    ],
                                                ],
                                            ],
                                            [
                                                '@name' => 'Свойство',
                                                '@attr' => [
                                                    'Имя' => 'КПП',
                                                    'Тип' => 'Строка',
                                                ],
                                                '@child' => [
                                                    [
                                                        '@name' => 'Значение',
                                                        '@value' => $company->kpp,
                                                    ],
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Наименование',
                                    'Тип' => 'Строка',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' =>  $agreement ?
                                            "{$agreement->getFullNumber()} от {$agreementDate}"
                                            : 'Основной договор',
                                    ],
                                ],
                            ],
                            $agreement ? [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Номер',
                                    'Тип' => 'Строка',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => $agreement->getDocumentNumberOneC(),
                                    ],
                                ],
                            ] : [],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'Дата',
                                    'Тип' => 'Дата',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => $agreement ?
                                            date(OneCExport::XML_DATETIME_FORMAT, strtotime($agreement->document_date)) :
                                            date(OneCExport::XML_DATETIME_FORMAT),
                                    ]
                                ],
                            ],
                            [
                                '@name' => 'Свойство',
                                '@attr' => [
                                    'Имя' => 'ВидДоговора',
                                    'Тип' => 'ПеречислениеСсылка.ВидыДоговоровКонтрагентов',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Значение',
                                        '@value' => $type,
                                    ],
                                ],
                            ],
                        ],
                    ],
                    [
                        '@name' => 'Свойство',
                        '@attr' => [
                            'Имя' => 'ВалютаВзаиморасчетов',
                            'Тип' => 'СправочникСсылка.Валюты',
                        ],
                        '@child' => [
                            [
                                '@name' => 'Ссылка',
                                '@attr' => [
                                    'Нпп' => '',
                                ],
                                '@child' => [
                                    [
                                        '@name' => 'Свойство',
                                        '@attr' => [
                                            'Имя' => 'Код',
                                            'Тип' => 'Строка',
                                        ],
                                        '@child' => [
                                            [
                                                '@name' => 'Значение',
                                                '@value' => '643',
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @param AbstractDocument $document
     * @param $type
     * @return array
     */
    public static function getAgreementPropertyByDocument(AbstractDocument $document, $type)
    {
        if ($document instanceof Invoice) {
            $company = $document->company;
            $contractor = $document->contractor;
        } else {
            $company = $document->invoice->company;
            $contractor = $document->invoice->contractor;
        }
        $agreement = self::getAgreementByDocument($document);
        if ($agreement && (empty($agreement->object_guid))) {
            $agreement->object_guid = OneCExport::generateGUID();
            $agreement->save(false);
        } elseif (empty($contractor->document_guid)) {
            $contractor->document_guid = OneCExport::generateGUID();
            $contractor->save(false);
        }

        $agreementDate = null;
        if ($agreement !== null) {
            $agreementDate = DateHelper::format($agreement->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
        }

        return [
            '@name' => 'Свойство',
            '@attr' => [
                'Имя' => 'ДоговорКонтрагента',
                'Тип' => 'СправочникСсылка.ДоговорыКонтрагентов',
            ],
            '@child' => [
                [
                    '@name' => 'Ссылка',
                    '@attr' => [
                        'Нпп' => '',
                    ],
                    '@child' => [
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => '{УникальныйИдентификатор}',
                                'Тип' => 'Строка',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => $agreement ? $agreement->object_guid : $contractor->document_guid,
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Владелец',
                                'Тип' => 'СправочникСсылка.Контрагенты',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Ссылка',
                                    '@attr' => [
                                        'Нпп' => '',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Свойство',
                                            '@attr' => [
                                                'Имя' => '{УникальныйИдентификатор}',
                                                'Тип' => 'Строка',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Значение',
                                                    '@value' => $contractor->object_guid,
                                                ],
                                            ],
                                        ],
                                        [
                                            '@name' => 'Свойство',
                                            '@attr' => [
                                                'Имя' => 'ИНН',
                                                'Тип' => 'Строка'
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Значение',
                                                    '@value' => $contractor->ITN,
                                                ],
                                            ],
                                        ],
                                        !empty($contractor->PPC) ?
                                            [
                                                '@name' => 'Свойство',
                                                '@attr' => [
                                                    'Имя' => 'КПП',
                                                    'Тип' => 'Строка',
                                                ],
                                                '@child' => [
                                                    [
                                                        '@name' => 'Значение',
                                                        '@value' => $contractor->PPC,
                                                    ],
                                                ],
                                            ] : [],
                                        empty($contractor->ITN) ?
                                            [
                                                '@name' => 'Свойство',
                                                '@attr' => [
                                                    'Имя' => 'НазваниеКонтрагента',
                                                    'Тип' => 'Строка',
                                                ],
                                                '@child' => [
                                                    [
                                                        '@name' => 'Значение',
                                                        '@value' => $contractor->getShortName(),
                                                    ],
                                                ],
                                            ] : [],
                                    ],
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Организация',
                                'Тип' => 'СправочникСсылка.Организации',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Ссылка',
                                    '@attr' => [
                                        'Нпп' => '',
                                    ],
                                    '@child' => [
                                        [
                                            '@name' => 'Свойство',
                                            '@attr' => [
                                                'Имя' => 'ИНН',
                                                'Тип' => 'Строка',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Значение',
                                                    '@value' => $company->inn,
                                                ],
                                            ],
                                        ],
                                        [
                                            '@name' => 'Свойство',
                                            '@attr' => [
                                                'Имя' => 'КПП',
                                                'Тип' => 'Строка',
                                            ],
                                            '@child' => [
                                                [
                                                    '@name' => 'Значение',
                                                    '@value' => $company->kpp,
                                                ],
                                            ],
                                        ],
                                    ],
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'ВидДоговора',
                                'Тип' => 'ПеречислениеСсылка.ВидыДоговоровКонтрагентов',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => $type,
                                ],
                            ],
                        ],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Наименование',
                                'Тип' => 'Строка',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' =>  $agreement ?
                                        "{$agreement->getFullNumber()} от {$agreementDate}"
                                        : 'Основной договор',
                                ],
                            ],
                        ],
                        $agreement ? [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Номер',
                                'Тип' => 'Строка',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => $agreement->getDocumentNumberOneC(),
                                ],
                            ],
                        ] : [],
                        [
                            '@name' => 'Свойство',
                            '@attr' => [
                                'Имя' => 'Дата',
                                'Тип' => 'Дата',
                            ],
                            '@child' => [
                                [
                                    '@name' => 'Значение',
                                    '@value' => $agreement ?
                                        date(OneCExport::XML_DATETIME_FORMAT, strtotime($agreement->document_date)) :
                                        date(OneCExport::XML_DATETIME_FORMAT),
                                ]
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }

    /**
     * @param AbstractDocument $document
     * @return Agreement|null|\yii\db\ActiveRecord
     */
    public static function getAgreementByDocument(AbstractDocument $document)
    {
        if ($document instanceof Invoice) {
            $company = $document->company;
            $contractor = $document->contractor;
        } else {
            $company = $document->invoice->company;
            $contractor = $document->invoice->contractor;
        }
        switch (true) {
            case $document instanceof Invoice:
            case $document instanceof Act:
            case $document instanceof Upd:
            case $document instanceof PackingList:
            case $document instanceof SalesInvoice:
                $agreementNumber = $document->basis_document_number;
                $agreementDate = $document->basis_document_date;
                $agreementType = $document->basis_document_type_id;
                break;
            case $document instanceof InvoiceFacture:
                $agreementNumber = $document->invoice->basis_document_number;
                $agreementDate = $document->invoice->basis_document_date;
                $agreementType = $document->invoice->basis_document_type_id;
                break;
            default:
                throw new InvalidParamException('Invalid document param.');
        }

        return Agreement::find()
            ->andWhere(['and',
                ['company_id' => $company->id],
                ['contractor_id' => $contractor->id],
                ['type' => $document->type],
                ['document_date' => DateHelper::format($agreementDate, DateHelper::FORMAT_DATE, DateHelper::FORMAT_USER_DATE)],
                ['document_number' => $agreementNumber],
                ['document_type_id' => $agreementType],
            ])->one();
    }

    /**
     * @return ActiveQuery
     */
    public function getTemplate(): ActiveQuery
    {
        return $this->hasOne(AgreementTemplate::class, ['id' => 'agreement_template_id']);
    }

    /**
     * @param Contractor $contractor
     * @param mixed $id
     * @return static|null
     */
    public static function findById(Contractor $contractor, $id): ?self
    {
        if (is_scalar($id)) {
            return self::findOne([
                'id' => $id,
                'company_id' => $contractor->company->id,
                'contractor_id' => $contractor->id,
            ]);
        }

        return null;
    }
}
