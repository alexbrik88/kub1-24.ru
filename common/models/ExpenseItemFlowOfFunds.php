<?php

namespace common\models;

use Yii;
use common\models\document\InvoiceExpenditureItem;

/**
 * This is the model class for table "expense_item_flow_of_funds".
 *
 * @property integer $id
 * @property integer $company_id
 * @property integer $expense_item_id
 * @property integer $flow_of_funds_block
 * @property integer $break_even_block
 * @property bool    $is_visible
 * @property bool    $is_prepayment
 *
 *
 * @property Company $company
 * @property InvoiceExpenditureItem $expenseItem
 */
class ExpenseItemFlowOfFunds extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'expense_item_flow_of_funds';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'expense_item_id'], 'required'],
            [['company_id', 'expense_item_id', 'flow_of_funds_block', 'break_even_block'], 'integer'],
            [['is_visible', 'is_prepayment'], 'boolean'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['expense_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => InvoiceExpenditureItem::className(), 'targetAttribute' => ['expense_item_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'expense_item_id' => 'Expense Item ID',
            'flow_of_funds_block' => 'Flow Of Funds Block',
            'break_even_block' => 'Break Even Block',
            'is_visible' => 'Is Visible',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpenseItem()
    {
        return $this->hasOne(InvoiceExpenditureItem::className(), ['id' => 'expense_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItem()
    {
        return $this->hasOne(InvoiceExpenditureItem::className(), ['id' => 'expense_item_id']);
    }
}
