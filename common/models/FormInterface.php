<?php

namespace common\models;

use yii\base\Configurable;

interface FormInterface extends Configurable
{
    /**
     * @inheritDoc
     */
    public function load($data, $formName = null);

    /**
     * @inheritDoc
     */
    public function validate($attributeNames = null, $clearErrors = true);
}
