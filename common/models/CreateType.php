<?php

namespace common\models;

use Yii;
use common\models\document\Invoice;

/**
 * This is the model class for table "create_type".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Invoice[] $invoices
 * @property Contractor[] $contractors
 */
class CreateType extends \yii\db\ActiveRecord
{
    const TYPE_WEB = 1;
    const TYPE_API = 2;
    const TYPE_B2B = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'create_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoices()
    {
        return $this->hasMany(Invoice::className(), ['create_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContractors()
    {
        return $this->hasMany(Contractor::className(), ['create_type_id' => 'id']);
    }
}
