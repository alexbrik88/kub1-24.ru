<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "company_product_type".
 *
 * @property integer $id
 * @property string $name
 */
class CompanyProductType extends \yii\db\ActiveRecord
{
    const TYPE_GOODS = 1;
    const TYPE_SERVICES = 2;

    const ONE_PRODUCTION = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'company_product_type';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
        ];
    }
}
