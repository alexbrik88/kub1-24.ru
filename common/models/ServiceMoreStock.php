<?php

namespace common\models;

use common\models\company\Activities;
use Yii;
use common\models\company\CompanyType;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "service_more_stock".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $company_id
 * @property integer $activities_id
 * @property string $company_type_text
 * @property integer $specialization
 * @property integer $average_invoice_count
 * @property integer $employees_count
 * @property integer $accountant_id
 * @property integer $has_logo
 *
 * @property Company $company
 * @property Activities $activities
 */
class ServiceMoreStock extends \yii\db\ActiveRecord
{
    /**
     *
     */
    const SPECIALIZATION_PRODUCT = 1;
    /**
     *
     */
    const SPECIALIZATION_SERVICE = 2;
    /**
     *
     */
    const SPECIALIZATION_PRODUCT_AND_SERVICE = 3;
    /**
     *
     */
    const INVOICE_COUNT_1 = 1;
    /**
     *
     */
    const INVOICE_COUNT_2 = 2;
    /**
     *
     */
    const INVOICE_COUNT_3 = 3;
    /**
     *
     */
    const INVOICE_COUNT_4 = 4;
    /**
     *
     */
    const INVOICE_COUNT_5 = 5;
    /**
     *
     */
    const INVOICE_COUNT_6_10 = 6;
    /**
     *
     */
    const INVOICE_COUNT_11_20 = 7;
    /**
     *
     */
    const INVOICE_COUNT_21_50 = 8;
    /**
     *
     */
    const INVOICE_COUNT_51_100 = 9;
    /**
     *
     */
    const INVOICE_COUNT_MORE_100 = 10;
    /**
     *
     */
    const EMPLOYEE_COUNT_1 = 1;
    /**
     *
     */
    const EMPLOYEE_COUNT_2 = 2;
    /**
     *
     */
    const EMPLOYEE_COUNT_3 = 3;
    /**
     *
     */
    const EMPLOYEE_COUNT_4 = 4;
    /**
     *
     */
    const EMPLOYEE_COUNT_5 = 5;
    /**
     *
     */
    const EMPLOYEE_COUNT_6_10 = 6;
    /**
     *
     */
    const EMPLOYEE_COUNT_11_20 = 7;
    /**
     *
     */
    const EMPLOYEE_COUNT_21_50 = 8;
    /**
     *
     */
    const EMPLOYEE_COUNT_MORE_50 = 9;
    /**
     *
     */
    const INCOMING_ACCOUNTANT = 1;
    /**
     *
     */
    const ACCOUNTANT_COMPANY = 2;
    /**
     *
     */
    const STAFF_ACCOUNTANT = 3;
    /**
     *
     */
    const MYSELF_ACCOUNTANT = 4;
    /**
     *
     */
    const HAS_LOGO = 1;
    /**
     *
     */
    const NO_LOGO = 0;

    /**
     * @var array
     */
    public static $hasLogoArray = [
        self::HAS_LOGO => 'Да',
        self::NO_LOGO => 'Нет',
    ];

    /**
     * @var array
     */
    public static $specializationArray = [
        self::SPECIALIZATION_PRODUCT => 'Продажа товара',
        self::SPECIALIZATION_SERVICE => 'Оказание услуг',
        self::SPECIALIZATION_PRODUCT_AND_SERVICE => 'Продажа товара и оказание услуг',
    ];

    /**
     * @var array
     */
    public static $averageInvoiceCountArray = [
        self::INVOICE_COUNT_1 => 1,
        self::INVOICE_COUNT_2 => 2,
        self::INVOICE_COUNT_3 => 3,
        self::INVOICE_COUNT_4 => 4,
        self::INVOICE_COUNT_5 => 5,
        self::INVOICE_COUNT_6_10 => '6-10',
        self::INVOICE_COUNT_11_20 => '11-20',
        self::INVOICE_COUNT_21_50 => '21-50',
        self::INVOICE_COUNT_51_100 => '51-100',
        self::INVOICE_COUNT_MORE_100 => 'больше 100',
    ];

    /**
     * @var array
     */
    public static $employeesCountArray = [
        self::EMPLOYEE_COUNT_1 => 1,
        self::EMPLOYEE_COUNT_2 => 2,
        self::EMPLOYEE_COUNT_3 => 3,
        self::EMPLOYEE_COUNT_4 => 4,
        self::EMPLOYEE_COUNT_5 => 5,
        self::EMPLOYEE_COUNT_6_10 => '6-10',
        self::EMPLOYEE_COUNT_11_20 => '11-20',
        self::EMPLOYEE_COUNT_21_50 => '21-50',
        self::EMPLOYEE_COUNT_MORE_50 => 'больше 50',
    ];

    /**
     * @var array
     */
    public static $accountantArray = [
        self::INCOMING_ACCOUNTANT => 'Приходящий бухгалтер',
        self::ACCOUNTANT_COMPANY => 'Бухгалтерская компания',
        self::STAFF_ACCOUNTANT => 'Штатный бухгалтер',
        self::MYSELF_ACCOUNTANT => 'Я сам',
    ];

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'service_more_stock';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'activities_id', 'specialization', 'average_invoice_count', 'employees_count', 'accountant_id'], 'required', 'message' => 'Необходимо заполнить.'],
            [['company_id', 'activities_id', 'specialization', 'average_invoice_count', 'employees_count', 'accountant_id', 'has_logo'], 'integer'],
            [['company_type_text'], 'string', 'max' => 255],
            [['created_at'], 'integer'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
           // [['activities_id'], 'exist', 'skipOnError' => true, 'targetClass' => Activities::className(), 'targetAttribute' => ['activities_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'activities_id' => 'Вид деятельности',
            'company_type_text' => 'Свой вид деятельности',
            'specialization' => 'Специализация',
            'average_invoice_count' => 'Среднее количество счетов в месяц',
            'employees_count' => 'Количество сотрудников',
            'accountant_id' => 'Кто ведет бухгалтерию',
            'has_logo' => 'Has Logo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActivities()
    {
        return $this->hasOne(Activities::className(), ['id' => 'activities_id']);
    }
}
