<?php

namespace common\models;

use yii\db\ActiveQuery;

/**
 * @property-read Contractor|null $contractor
 */
trait ContractorRelationTrait
{
    /**
     * @var string[]
     */
    protected array $contractorLink = ['id' => 'contractor_id'];

    /**
     * @return Contractor|null
     */
    public function getContractor(): ?Contractor
    {
        return $this->__get('contractorRelation');
    }

    /**
     * @return ActiveQuery
     */
    public function getContractorRelation(): ActiveQuery
    {
        return $this->hasOne(Contractor::class, $this->contractorLink);
    }

    /**
     * @return bool
     */
    public function hasContractor(): bool
    {
        if ($this->__get('contractorRelation')) {
            return true;
        }

        return false;
    }
}
