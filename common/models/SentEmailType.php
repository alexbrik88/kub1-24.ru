<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sent_email_type".
 *
 * @property int $id
 * @property string $type
 * @property string|null $description
 *
 * @property SentEmail[] $sentEmails
 */
class SentEmailType extends \yii\db\ActiveRecord
{
    const INACTIVE_COMPANY = 1;
    const FINDIRECTOR = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'sent_email_type';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSentEmails()
    {
        return $this->hasMany(SentEmail::className(), ['email_type_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        parent::beforeSave($insert);

        return false;
    }

    /**
     * @inheritdoc
     */
    public function beforeDelete()
    {
        parent::beforeDelete();

        return false;
    }
}
