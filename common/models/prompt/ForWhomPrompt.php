<?php

namespace common\models\prompt;

use yii\db\ActiveRecord;

/**
 * Class ForWhomPrompt
 * @package common\models\prompt
 */
class ForWhomPrompt extends ActiveRecord
{
    /**
     *
     */
    const ALL = 1;
    /**
     *
     */
    const IP = 2;
    /**
     *
     */
    const OOO = 3;
    /**
     *
     */
    const ZAO = 4;
    /**
     *
     */
    const PAO = 5;
    /**
     *
     */
    const OAO = 6;

    public static $all = [
        self::IP,
        self::OOO,
        self::ZAO,
        self::PAO,
        self::OAO,
    ];

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'for_whom_prompt';
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
        ];
    }
}