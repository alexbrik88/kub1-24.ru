<?php

namespace common\models\prompt;

use yii\db\ActiveRecord;
use Yii;

/**
 * Class PageType
 * @package common\models\prompt
 */
class PageType extends ActiveRecord
{
    /**
     *
     */
    const TYPE_CUSTOMER = 3;
    /**
     *
     */
    const TYPE_PROVIDER = 4;
    /**
     *
     */
    const TYPE_INVOICE_OUT = 5;
    /**
     *
     */
    const TYPE_INVOICE_IN = 10;
    /**
     *
     */
    const TYPE_SERVICE = 18;
    /**
     *
     */
    const TYPE_PRODUCT = 19;
    /**
     *
     */
    const TYPE_EMPLOYEE = 20;

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'page_type';
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
        ];
    }
}