<?php

namespace common\models\jobs;

use common\models\Company;
use common\models\employee\Employee;
use common\tasks\ExportGoogleAdsAnalytics;
use common\tasks\ExportMonetaOperations;
use common\tasks\ExportUnisender;
use common\tasks\ExportVkAdsAnalytics;
use common\tasks\ExportYandexDirectAnalytics;
use frontend\modules\analytics\modules\marketing\tasks\ImportReportsFromFacebook;
use frontend\modules\analytics\modules\marketing\tasks\ImportReportsFromGoogleAds;
use frontend\modules\analytics\modules\marketing\tasks\ImportReportsFromVkAds;
use frontend\modules\analytics\modules\marketing\tasks\ImportReportsFromYandexDirect;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "jobs".
 *
 * @property-read int $id
 * @property int $company_id
 * @property int $employee_id
 * @property int $created_at
 * @property int $finished_at
 * @property int $type
 * @property array $params
 * @property string $result
 * @property string $error
 * @property bool $is_auto
 * @property bool $is_viewed
 *
 * @property-read Company $company
 * @property-read Employee $employee
 */
class Jobs extends ActiveRecord
{
    public const TYPE_EXPORT_MESSAGES_FROM_UNISENDER = 1;
    public const TYPE_EXPORT_OPERATIONS_FROM_MONETA = 2;
    public const TYPE_EXPORT_ANALYTICS_FROM_YANDEX_DIRECT = 3;
    public const TYPE_EXPORT_ANALYTICS_FROM_GOOGLE_ADS = 4;
    public const TYPE_EXPORT_ANALYTICS_FROM_VK_ADS = 5;
    public const TYPE_IMPORT_REPORT_FROM_YANDEX_DIRECT = 6;
    public const TYPE_IMPORT_REPORT_FROM_GOOGLE_ADS = 7;
    public const TYPE_IMPORT_REPORT_FROM_VK_ADS = 8;
    public const TYPE_IMPORT_REPORT_FROM_FACEBOOK = 9;

    /**
     * @var string[]
     */
    public static $typeMap = [
        self::TYPE_EXPORT_MESSAGES_FROM_UNISENDER => 'Выгрузка писем из Unisender',
        self::TYPE_EXPORT_OPERATIONS_FROM_MONETA => 'Выгрузка операций из Moneta.ru',
        self::TYPE_EXPORT_ANALYTICS_FROM_YANDEX_DIRECT => 'Выгрузка аналитики из Yandex Direct',
        self::TYPE_EXPORT_ANALYTICS_FROM_GOOGLE_ADS => 'Выгрузка аналитики из Google Ads',
        self::TYPE_EXPORT_ANALYTICS_FROM_VK_ADS => 'Выгрузка аналитики из VK',
        self::TYPE_IMPORT_REPORT_FROM_YANDEX_DIRECT => 'Импорт отчета из Яндекс.Директ',
        self::TYPE_IMPORT_REPORT_FROM_GOOGLE_ADS => 'Импорт отчета из Google Ads',
        self::TYPE_IMPORT_REPORT_FROM_VK_ADS => 'Импорт отчета из Vkontakte',
        self::TYPE_IMPORT_REPORT_FROM_FACEBOOK => 'Импорт отчета из Facebook',
    ];

    /**
     * @inheritDoc
     */
    public static function tableName()
    {
        return 'jobs';
    }

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => false,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'employee_id', 'type', 'result'], 'required'],
            [['company_id', 'employee_id', 'created_at', 'finished_at', 'type'], 'integer'],
            [['result'], 'string'],
            [['params'], 'safe'],
            [['is_auto', 'is_viewed'], 'boolean'],
            [['type'], 'in', 'range' => array_keys(self::$typeMap)],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['company_id' => 'id']],
            [['employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::class, 'targetAttribute' => ['employee_id' => 'id']],
        ];
    }

    public function afterFind()
    {
        parent::afterFind();
        if ($this->params !== null) {
            $this->params = json_decode($this->params, true);
        }
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if (is_array($this->params)) {
                $this->params = json_encode($this->params, JSON_UNESCAPED_UNICODE);
            }

            return true;
        }

        return false;
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert) {
            switch ($this->type) { // TODO: переделать в массив
                case self::TYPE_EXPORT_MESSAGES_FROM_UNISENDER:
                    Yii::$app->queue->push(ExportUnisender::create($this->id));
                    break;
                case self::TYPE_EXPORT_OPERATIONS_FROM_MONETA:
                    Yii::$app->queue->push(ExportMonetaOperations::create($this->id));
                    break;
                case self::TYPE_EXPORT_ANALYTICS_FROM_YANDEX_DIRECT:
                    Yii::$app->queue->push(ExportYandexDirectAnalytics::create($this->id));
                    break;
                case self::TYPE_EXPORT_ANALYTICS_FROM_GOOGLE_ADS:
                    Yii::$app->queue->push(ExportGoogleAdsAnalytics::create($this->id));
                    break;
                case self::TYPE_EXPORT_ANALYTICS_FROM_VK_ADS:
                    Yii::$app->queue->push(ExportVkAdsAnalytics::create($this->id));
                    break;

                case self::TYPE_IMPORT_REPORT_FROM_YANDEX_DIRECT:
                    Yii::$app->queue->push(ImportReportsFromYandexDirect::create($this->id));
                    break;
                case self::TYPE_IMPORT_REPORT_FROM_GOOGLE_ADS:
                    Yii::$app->queue->push(ImportReportsFromGoogleAds::create($this->id));
                    break;
                case self::TYPE_IMPORT_REPORT_FROM_VK_ADS:
                    Yii::$app->queue->push(ImportReportsFromVkAds::create($this->id));
                    break;
                case self::TYPE_IMPORT_REPORT_FROM_FACEBOOK:
                    Yii::$app->queue->push(ImportReportsFromFacebook::create($this->id));
                    break;
            }
        }

        $oldFinishedAt = false;
        foreach ($changedAttributes as $attribute => $value) {
            if ($attribute === 'finished_at') {
                $oldFinishedAt = $value;
            }
        }

    //    if (
    //        $oldFinishedAt === null
    //        && $this->finished_at !== null
    //        && (bool)$this->is_auto === false
    //        && $this->type === self::TYPE_EXPORT_OPERATIONS_FROM_MONETA
    //    ) {
    //        $client = new WAMPClient('http://127.0.0.1:' . Yii::$app->params['socket']['port']);
    //        $params = json_decode($this->params, true);
    //        try {
    //            $client->connect();
    //            $client->call(
    //                'send',
    //                [
    //                    'message' => [
    //                        'type' => 'finishedJob',
    //                        'jobId' => $this->id,
    //                        'employeeId' => $this->employee_id,
    //                        'jobType' => $this->type,
    //                        'operationsCount' => $params['count'],
    //                    ],
    //                ]);
    //            $client->disconnect();
    //        } catch (\Exception $e) {
    //            Yii::error($e);
    //        }
    //    }
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'employee_id' => 'Employee ID',
            'created_at' => 'Дата создания',
            'finished_at' => 'Дата завершения',
            'type' => 'Тип задачи',
            'params' => 'Параметры',
            'result' => 'Статус',
            'error' => 'Описание ошибки',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getCompany(): ActiveQuery
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getEmployee(): ActiveQuery
    {
        return $this->hasOne(Employee::class, ['id' => 'employee_id']);
    }
}
