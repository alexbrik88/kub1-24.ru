<?php

namespace common\models;

use common\models\Company;
use common\models\employee\Employee;
use common\models\product\Product;
use Yii;

/**
 * This is the model class for table "log_import_product".
 *
 * @property int $id
 * @property int $company_id
 * @property int $employee_id
 * @property int $inserted_rows
 * @property int $updated_rows
 * @property int $created_at
 *
 * @property Company $company
 * @property Employee $employee
 * @property Product[] $products
 */
class LogImportProduct extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'log_import_product';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'employee_id' => 'Employee ID',
            'production_type' => 'Production Type',
            'inserted_rows' => 'Inserted Rows',
            'updated_rows' => 'Updated Rows',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['log_import_product_id' => 'id']);
    }
}
