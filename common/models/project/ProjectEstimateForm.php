<?php


namespace common\models\project;

use common\models\Company;
use Yii;
use yii\web\ForbiddenHttpException;

class ProjectEstimateForm extends \yii\base\Model
{
    public $projectEstimateId;
    public $project_id;
    public $status;
    public $responsible;

    public $company;

    public function rules()
    {
        return [
            [['status', 'responsible'], 'safe'],
            [['status', 'responsible'], 'integer'],
            [['project_id'], 'safe'],
            [['project_id'], 'integer'],
            [['projectEstimateId'], 'safe'],
        ];
    }

    public function change() {
        if (!$this->validate()) {
            return false;
        }

        $projectEstimate = ProjectEstimate::findOne([
            'id' => $this->projectEstimateId,
            'project_id' => $this->project_id,
        ]);

        if ($projectEstimate->project->company_id !== $this->company->id) {
            return false;
        }

        if ($this->status) {
            $projectEstimate->status = $this->status;
        }

        if ($this->responsible) {
            $projectEstimate->responsible = $this->responsible;
        }

        return $projectEstimate->save();
    }

}