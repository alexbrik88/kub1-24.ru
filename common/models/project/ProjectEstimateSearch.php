<?php

namespace common\models\project;

use common\components\date\DateHelper;
use common\components\helpers\ArrayHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashOrderFlows;
use common\models\Contractor;
use common\models\document\Invoice;
use common\models\employee\Employee;
use common\models\product\Product;
use common\models\service\Payment;
use frontend\components\StatisticPeriod;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * Class ProjectEstimateSearch
 * @package common\models\project
 */
class ProjectEstimateSearch extends ProjectEstimate
{
    /**
     * @var
     */
    public $search;

    /**
     * @var ActiveQuery|null
     */
    public $query = null;

    /**
     * @var
     */
    public $project_id;

    /**
     * @var
     */
    public $status;

    /**
     * @var
     */
    public $responsible;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['search'], 'safe'],
            [['status', 'responsible'], 'safe'],
            [['project_id'], 'integer'],
        ];
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params):ActiveDataProvider
    {
        $query = $this->getBaseQuery($params);

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => \Yii::$app->request->get('per-page', 10),
            ],
            'sort' => [
                'attributes' => [
                    'date',
                    'number',
                    'name',
                    'sale_sum',
                    'income_sum',
                    'diff_sum',
                ],
                'defaultOrder' => [
                    'date' => SORT_DESC,
                    'number' => SORT_DESC,
                ],
            ],
        ]);
    }

    /**
     * @param $params
     * @return ActiveQuery
     */
    public function getBaseQuery($params = []):ActiveQuery
    {
        $this->load($params);

        $query = ProjectEstimate::find()
            ->addSelect([
                ProjectEstimate::tableName() . '.*',
                'SUM(IF(' . ProjectEstimateItem::tableName() . '.price,' . ProjectEstimateItem::tableName() . '.price * IF(hasNds && 0 = nds_out, (1 - rate / (1 + rate)), 1),' . Product::tableName() . '.price_for_sell_with_nds)' . ' * ' . ProjectEstimateItem::tableName().'.count) as sale_sum',
                'SUM(IF(' . ProjectEstimateItem::tableName() . '.price,' . ProjectEstimateItem::tableName() . '.price,' . Product::tableName() . '.price_for_sell_with_nds - ' . Product::tableName() . '.price_for_sell_with_nds * IF(hasNds, (rate), 1))' . ' * ' . ProjectEstimateItem::tableName().'.count) as income_estimate_sum',
                //'SUM(' . Product::tableName() . '.price_for_buy_with_nds' . ' * ' . ProjectEstimateItem::tableName().'.count) as income_sum',
                'SUM((' . Product::tableName() . '.price_for_buy_with_nds - ' . Product::tableName() . '.price_for_buy_with_nds * IF(hasNds, (rate), 0))' . ' * ' . ProjectEstimateItem::tableName().'.count) as income_sum',
                'SUM((' . Product::tableName() . '.price_for_buy_with_nds - ' . Product::tableName() . '.price_for_buy_with_nds * IF(hasNds, (rate), 0))' . ' * ' . ProjectEstimateItem::tableName().'.count) as expense_estimate_sum',
                'SUM(
                    IF('.ProjectEstimateItem::tableName() . '.price,' .
                        ProjectEstimateItem::tableName() . '.price * IF(hasNds && 0 = nds_out, (1 - rate / (1 + rate)), 1),' .
                        Product::tableName() . '.price_for_sell_with_nds) * ' . ProjectEstimateItem::tableName().'.count - (
                                (' . Product::tableName() . '.price_for_buy_with_nds - ' . Product::tableName() . '.price_for_buy_with_nds * IF(hasNds, (rate), 0))' . ' * ' . ProjectEstimateItem::tableName().'.count                
                            )) as diff_sum',
            ])
            ->joinWith('project', true, 'INNER JOIN')
            ->joinWith('projectEstimateItem')
            ->joinWith('projectEstimateItem.product')
            ->joinWith('projectEstimateItem.tax')

            ->andWhere(['and',
                [Project::tableName() . '.company_id' => Yii::$app->user->identity->company->id],
                [ProjectEstimate::tableName() . '.project_id' => $this->project_id],
            ])

            ->groupBy(ProjectEstimate::tableName() . '.id');
        ;

        if ($this->search) {
            $query->andFilterWhere(['or',
                [ProjectEstimate::tableName() . '.number' => $this->search],
                [ProjectEstimate::tableName() . '.name' => $this->search],
            ]);
        }

        if ($this->status) {
            $query->andFilterWhere([ProjectEstimate::tableName() . '.status' => $this->status]);
        }

        if ($this->responsible) {
            $query->andFilterWhere([ProjectEstimate::tableName() . '.responsible' => $this->responsible]);
        }

//        echo $query->createCommand()->getRawSql();die;

        $this->query = $query;

        return $query;
    }

    /**
     * @return ActiveQuery
     */
    public function getQuery():ActiveQuery
    {
        if (empty($this->query)) {
            $this->getBaseQuery();
        }

        return clone $this->query;
    }

    /**
     * @return array
     */
    public function getResponsibleFilter()
    {
        $query = $this->getQuery();

        $employers = $query
            ->joinWith('project.responsibleEmployee')
            ->groupBy(Employee::tableName() . '.id')
            ->all();

        $responsible = [];

        /** @var ProjectEstimate $employee */
        foreach ($employers as $employee) {
            if (isset($employee->employee)) {
                $responsible[$employee->employee->id] = $employee->employee->getShortFio();
            }
        }

        return [null => 'Все'] + $responsible;
    }

    /**
     * @param ActiveQuery $query
     * @param $sort
     * @return ActiveQuery
     */
    public function setSorting(ActiveQuery $query, $sort):ActiveQuery
    {
        $order = 'ASC';
        if (false !== strpos($sort, '-')) {
            $order = 'DESC';
            $sort = substr($sort, 1);
        }

        $query->orderBy($sort . ' ' . $order);

        return $query;
    }

}