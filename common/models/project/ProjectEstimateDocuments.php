<?php


namespace common\models\project;


use common\components\pdf\Printable;
use common\models\document\AbstractDocument;
use common\models\file\File;
use frontend\models\log\Log;

class ProjectEstimateDocuments extends AbstractDocument implements Printable
{
    public $printablePrefix = '';
    public static $uploadDirectory = '/project-estimate/';

    public static function tableName()
    {
        return 'project_estimate';
    }


    /**
     * @inheritDoc
     */
    public function getFile()
    {
        return $this->hasOne(File::class, ['owner_id' => 'id'])
            ->onCondition(['owner_model' => self::class,]);
    }

    /**
     * @inheritDoc
     */
    public function getFullNumber()
    {
        if (!$this->model instanceof ProjectEstimate)
        {
            return null;
        }

        return $this->model->number;
    }

    /**
     * @inheritDoc
     */
    public function getLogMessage(Log $log)
    {
        return $log->message;
    }

    /**
     * @inheritDoc
     */
    public function getPdfFileName()
    {
        if (!$this->model instanceof ProjectEstimate)
        {
            return 'project_estimate_document.pdf';
        }

        return $this->model->name . '.pdf';
    }

    /**
     * @inheritDoc
     */
    public function getPrintTitle()
    {
        if (!$this->model instanceof ProjectEstimate)
        {
            return 'Смета проекта';
        }

        return $this->model->name;
    }

}