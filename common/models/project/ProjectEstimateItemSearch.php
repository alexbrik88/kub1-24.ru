<?php

namespace common\models\project;

use common\components\date\DateHelper;
use common\components\helpers\ArrayHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashOrderFlows;
use common\models\Contractor;
use common\models\document\Invoice;
use common\models\document\Order;
use common\models\employee\Employee;
use common\models\product\Product;
use common\models\product\ProductUnit;
use common\models\service\Payment;
use frontend\components\StatisticPeriod;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * Class ProjectEstimateItemSearch
 * @package common\models\project
 */
class ProjectEstimateItemSearch extends ProjectCustomer
{
    /**
     * @var
     */
    public $search;

    /**
     * @var ActiveQuery|null
     */
    public $query = null;

    /**
     * @var
     */
    public $project_estimate_id;

    /**
     * @var
     */
    public $filterStatus;
    /**
     * @var
     */
    public $filterDate;
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['search'], 'safe'],
            [['project_estimate_id'], 'safe'],
        ];
    }

    /**
     * @param $params
     * @return ActiveDataProvider
     */
    public function search($params):ActiveDataProvider
    {
        $query = $this->getBaseQuery($params);

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => \Yii::$app->request->get('per-page', 10),
            ],
            'sort' => [
                'attributes' => [
                    'title',
                    'amount',
                    'number',
                    'income_price',
                    'sale_price',
                    'income_sum',
                    'sale_sum',
                    'diff_sum',
                ],
                'defaultOrder' => [
                    'number' => SORT_ASC,
                ]
            ],
        ]);
    }

    /**
     * @param $params
     * @return ActiveQuery
     */
    public function getBaseQuery($params = []):ActiveQuery
    {
        $this->load($params);

        $query = ProjectEstimateItem::find()
            ->addSelect([
                ProjectEstimateItem::tableName() . '.*',
                ProjectEstimateItem::tableName() . '.count as amount',
                ProjectEstimateItem::tableName() . '.number as number',
                Product::tableName() . '.article as article',
                ProductUnit::tableName() . '.name as unit',
                'IF(' . ProjectEstimateItem::tableName() . '.price, ' . ProjectEstimateItem::tableName() . '.price, ' . Product::tableName() . '.price_for_sell_with_nds) as sale_price',
                '(' . Product::tableName() . '.price_for_buy_with_nds) as income_price',
                'IF(' . ProjectEstimateItem::tableName() . '.price, (' . ProjectEstimateItem::tableName() . '.price * IF(hasNds && 1 = nds_out, (1 + rate), 1)  * ' . ProjectEstimateItem::tableName() . '.count), (' . Product::tableName() . '.price_for_sell_with_nds  * ' . ProjectEstimateItem::tableName() . '.count)) as sale_sum',
                '(' . Product::tableName() . '.price_for_buy_with_nds * ' . ProjectEstimateItem::tableName() . '.count) as income_sum',
                '((IF(' . ProjectEstimateItem::tableName() . '.price, ' . ProjectEstimateItem::tableName() . '.price * IF(hasNds && 1 = nds_out, (1 + rate), 1),' . Product::tableName() . '.price_for_sell_with_nds) * ' . ProjectEstimateItem::tableName() . '.count) - (' . Product::tableName() . '.price_for_buy_with_nds * ' . ProjectEstimateItem::tableName() . '.count)) as diff_sum'
            ])
            ->joinWith('projectEstimate')
            ->joinWith('projectEstimate.project')
            ->joinWith('product')
            ->joinWith('product.productUnit')
            ->joinWith('tax')
            ->andWhere(['and',
                [Project::tableName() . '.company_id' => Yii::$app->user->identity->company->id],
                [ProjectEstimateItem::tableName() . '.id_project_estimate' => $this->project_estimate_id],
            ])
        ;

        if ($this->search) {
            $query->andFilterWhere(['or',
                [ProjectEstimateItem::tableName() . '.number' => $this->search],
                [ProjectEstimateItem::tableName() . '.product_id' => $this->search],
                [ProjectEstimateItem::tableName() . '.title' => $this->search],
                [ProjectEstimate::tableName() . '.project_id' => $this->search],
                [ProjectEstimate::tableName() . '.name' => $this->search],
                [Project::tableName() . '.name' => $this->search],
            ]);
        }

//        echo $query->createCommand()->getRawSql();die;

        $this->query = $query;

        return $query;
    }

    /**
     * @return ActiveQuery
     */
    public function getQuery():ActiveQuery
    {
        if (empty($this->query)) {
            $this->getBaseQuery();
        }

        return clone $this->query;
    }

    public static function getInvoicesQuery($product_id, $project_id, $project_estimate_id, $type = Invoice::IO_TYPE_INCOME)
    {
        $query = Invoice::find()
            ->distinct()
            ->addSelect([
                Invoice::tableName() . '.*',
                Invoice::tableName() . '.project_id as project_id',
                Invoice::tableName() . '.id as invoice_id',
                Invoice::tableName() . '.document_number as invoice_number',
                Order::tableName() . '.product_id as product_id',
                ProjectEstimate::tableName() . '.id as projectEstimateId',
            ])
            ->joinWith('orders')
            ->leftJoin(Project::tableName(), Invoice::tableName() . '.project_id' . ' = ' . Project::tableName() . '.id')
            ->leftJoin(ProjectEstimate::tableName(), Project::tableName() . '.id' . ' = ' . ProjectEstimate::tableName() . '.project_id')
            ->andWhere(['and',
                [ProjectEstimate::tableName() . '.project_id' => $project_id],
                [Invoice::tableName() . '.type' => $type],
                [Invoice::tableName() . '.is_deleted' => 0],
                [Invoice::tableName() . '.project_estimate_id' => $project_estimate_id],
            ])
            ->andWhere([Order::tableName() . '.product_id' => $product_id])
            ->groupBy('order.product_id, order.invoice_id, project_estimate.id');

//        echo $query->createCommand()->rawSql;

        return $query;
    }
}