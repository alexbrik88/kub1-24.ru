<?php

namespace common\models\project;

use common\components\date\DateHelper;
use common\components\pdf\PdfRenderer;
use common\models\Company;
use common\models\Contractor;
use common\models\document\AgentReport;
use common\models\document\EmailFile;
use common\models\document\Invoice;
use common\models\document\Order;
use common\models\document\status\InvoiceStatus;
use common\models\employee\Employee;
use common\models\product\Product;
use common\models\product\ProductStore;
use common\models\product\Store;
use common\models\TaxRate;
use frontend\modules\documents\assets\DocumentPrintAsset;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * Class ProjectEstimate
 * @package common\models\project
 *
 * @property integer $id
 * @property integer $number
 * @property integer $addition_number
 * @property string $name
 * @property integer $date Timestamp
 * @property integer $status
 * @property integer $responsible
 * @property integer $customer_id
 * @property integer $project_id
 *
 * @property Project $project
 * @property ProjectEstimateItem $projectEstimateItem
 * @property ProjectEstimateItem[] $projectEstimateItems
 * @property Employee $employee
 * @property Contractor $customer
 *
 * @property Invoice[] $invoices
 */
class ProjectEstimate extends ActiveRecord
{
    const STATUS_DRAFT = 1;
    const STATUS_PLANNED = 2;
    const STATUS_APPROVED = 3;
    const STATUS_SIGNED = 4;

    public static $statuses = [
        self::STATUS_DRAFT => 'Черновик',
        self::STATUS_PLANNED => 'Плановая',
        self::STATUS_APPROVED => 'Утверждена',
        self::STATUS_SIGNED => 'Подписана',
    ];

    /**
     * @var ProjectEstimate
     */
    public $model;

    /**
     * @var string
     */
    public $printablePrefix = "";

    /**
     * @var string
     */
    public static $uploadDirectory = '/project-estimate/';

    public $sale_sum;
    public $income_sum;
    public $diff_sum;
    public $total_nds;

    public $priceForSellNds;
    public $priceForBuyNds;

    public $productUnit;
    public $productStores;

    public $orderArr;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{project_estimate}}';
    }

    /**
     * @return ActiveQuery
     */
    public static function find()
    {
        return new ProjectEstimateQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return
            ArrayHelper::merge(parent::behaviors(), [
                'timestamp' => [
                    'class' => TimestampBehavior::class,
                    'attributes' => [
                        ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                        ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                    ],
                ],
            ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number', 'date', 'project_id', 'customer_id'], 'required'],
            [['number', 'project_id', 'responsible'], 'integer'],
            [['nds_out'], 'integer'],
            [['addition_number', 'name'], 'string'],
            [['customer_id'], 'safe'],
            [['project_id'], 'exist', 'skipOnError' => false, 'skipOnEmpty' => false, 'targetClass' => Project::class, 'targetAttribute' => ['project_id' => 'id']],
            [['orderArr'], 'validateOrderArray', 'skipOnEmpty' => false],
            [['number'], 'validateNumber', 'skipOnEmpty' => false],
        ];
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        $this->orderArr = Yii::$app->request->post('orderArray');

        if (!is_array($this->orderArr)) {
            $this->orderArr = (array) $this->orderArr;
        }

        return parent::beforeValidate();
    }

    /**
     * @param $attribute
     * @return bool
     */
    public function validateOrderArray($attribute)
    {
        $fn = function($carry, $item) {
            $price = $item['price'] ?? 0;
            $count = $item['count'] ?? 0;
            $carry += (float)$price * (float)$count;
            return $carry;
        };

        if (empty($this->$attribute)) {
            $this->addError('orderArr', 'Смета не может быть пустой');
            return false;
        } elseif (0 >= array_reduce($this->$attribute, $fn, 0)) {
            $this->addError('orderArr', 'Сумма сметы должна быть больше 0');
            return false;
        }

        return true;
    }

    /**
     * @param $attribute
     * @return bool
     */
    public function validateNumber($attribute)
    {
        if (Yii::$app->user && Yii::$app->user->identity && Yii::$app->user->identity->company_id) {
            $existsNumbers = Project::find()
                ->leftJoin(ProjectEstimate::tableName(), 'project.id = project_estimate.project_id')
                ->where(['project.company_id' => Yii::$app->user->identity->company_id])
                ->andWhere(['not', ['project_estimate.id' => $this->id]])
                ->select('project_estimate.number')
                ->column();

            if ($existsNumbers && in_array($this->$attribute, $existsNumbers)) {
                $this->addError('number', 'Смета с таким номером уже существует');
            }

            return true;
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'number' => 'Номер',
            'name' => 'Наименование',
            'date' => 'Дата',
            'status' => 'Статус',
            'responsible' => 'Ответственный',
            'project_id' => 'Проект',
            'customer_id' => 'Покупатель'
        ];
    }

    public function beforeSave($insert)
    {
        $this->date = is_numeric($this->date) ? $this->date : strtotime($this->date);

        return parent::beforeSave($insert);
    }

    public function _save()
    {
        $transactions = Yii::$app->getDb()->beginTransaction();

        try {
            $orderArray = Yii::$app->request->post('orderArray');

            if (!$this->save()) {
                throw new \Exception('Ошибка сохранения сметы.');
            }

            $orderArrayProductIds = array_column($orderArray, 'product_id');

            foreach ($this->projectEstimateItems as $items) {
                if (!in_array($items->product_id, $orderArrayProductIds)) {
                    $items->delete();
                }
            }

            $number = 0;
            foreach ($orderArray as $key => $order) {

                if (!isset($order['product_id']))
                    continue;

                $taxRateId = $order['purchase_tax_rate_id'] ?? $order['sale_tax_rate_id'];
                /** @var ProjectEstimateItem $item */
                if (!($item = ProjectEstimateItem::find()->andWhere(['and',
                        ['product_id' => $order['product_id']],
                        ['id_project_estimate' => $this->id],
                    ])->one())) {
                    $item = new ProjectEstimateItem([
                        'id_project_estimate' => $this->id,
                        'number' => ++$number,
                        'product_id' => $order['product_id'],
                        'title' => $order['title'],
                        'price' => $order['price'] * 100,
                        'unit_id' => $order['unit_id'],
                        'count' => $order['count'],
                        'hasNds' => (int)($taxRateId != TaxRate::RATE_WITHOUT),
                        'tax_id' => $taxRateId,
                    ]);
                    $item->save();
                } else {
                    $item->id_project_estimate = $this->id;
                    $item->number = ++$number;
                    $item->product_id = $order['product_id'];
                    $item->title = $order['title'];
                    $item->price = $order['price'] * 100;
                    $item->unit_id = $order['unit_id'];
                    $item->count = $order['count'];
                    $item->hasNds = (int)($taxRateId != TaxRate::RATE_WITHOUT);
                    $item->tax_id = $taxRateId;
                    $item->save();
                }
            }

            if (!ProjectCustomer::find()
                ->andWhere(['and',
                    ['customer_id' => $this->customer_id],
                    ['project_id' => $this->project_id],
                ])
                ->exists()) {
                (new ProjectCustomer([
                    'project_id' => $this->project_id,
                    'customer_id' => $this->customer_id,
                    'date' => date(DateHelper::FORMAT_DATE),
                ]))
                ->save();
            };

            $transactions->commit();
        } catch (\Exception $e) {
            var_dump($e);exit;
            $transactions->rollBack();
            Yii::$app->session->setFlash('error', $e->getMessage());

            return false;
        }

        return true;
    }

    /**
     * @return ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::class, ['id' => 'project_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getProjectEstimateItem()
    {
        return $this->hasOne(ProjectEstimateItem::class, ['id_project_estimate' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getProjectEstimateItems()
    {
        return $this->hasMany(ProjectEstimateItem::class, ['id_project_estimate' => 'id'])
            ->orderBy(ProjectEstimateItem::tableName() . '.number ASC');
    }

    /**
     * @return ActiveQuery
     */
    public function getInvoices()
    {
        return $this->hasMany(Invoice::class, ['project_estimate_id' => 'id']);
    }

    public function getEmployee()
    {
        return $this->hasOne(Employee::class, ['id' => 'responsible']);
    }

    public function getCustomer()
    {
        return $this->hasOne(Contractor::class, ['id' => 'customer_id'])
            ->andWhere(['type' => Contractor::TYPE_CUSTOMER]);
    }

    /**
     * @param $id
     * @param $project_id
     * @return array|ActiveRecord[]
     */
    public static function payments($id, $project_id)
    {
        $clientsPayment = Invoice::find()
            ->addSelect([
                Invoice::tableName() . '.*',
                'SUM(' . Invoice::tableName() . '.payment_partial_amount) as sum',
            ])
            ->andWhere(['and',
                ['IN', Invoice::tableName() . '.invoice_status_id', [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_PAYED_PARTIAL]],
                [Invoice::tableName() . '.type' => Invoice::IO_TYPE_EXPENSE],
                [Invoice::tableName() . '.is_deleted' => 0]
            ])
            ->groupBy(Invoice::tableName() . '.project_id, ' . Invoice::tableName() . '.project_estimate_id')
        ;

        $customersPayment = Invoice::find()
            ->addSelect([
                Invoice::tableName() . '.*',
                'SUM(' . Invoice::tableName() . '.payment_partial_amount) as sum',
            ])
            ->andWhere(['and',
                ['IN', Invoice::tableName() . '.invoice_status_id', [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_PAYED_PARTIAL]],
                [Invoice::tableName() . '.type' => Invoice::IO_TYPE_INCOME],
                [Invoice::tableName() . '.is_deleted' => 0]
            ])
            ->groupBy(Invoice::tableName() . '.project_id, ' . Invoice::tableName() . '.project_estimate_id')
        ;

        $query = ProjectEstimateItem::find()
            ->addSelect([
                ProjectEstimateItem::tableName() . '.*',
                'SUM('.Product::tableName() . '.price_for_sell_with_nds) as price_for_sell_with_nds',
                'SUM('.Product::tableName() . '.price_for_buy_with_nds) as price_for_buy_with_nds',
                'SUM(count) as count',
                'SUM(IF('.ProjectEstimateItem::tableName().'.price, '.ProjectEstimateItem::tableName().'.price * IF(hasNds && 1 = nds_out, (1 + rate), 1), ' . Product::tableName() . '.price_for_sell_with_nds) * count) as sale_sum',
                'SUM(IF('.ProjectEstimateItem::tableName() . '.price,' . ProjectEstimateItem::tableName() . '.price,' . Product::tableName() . '.price_for_sell_with_nds - ' . Product::tableName() . '.price_for_sell_with_nds * IF(hasNds, (rate), 1)) * count) as income_estimate_sum',
                'SUM(' . Product::tableName() . '.price_for_buy_with_nds * count) as income_sum',
                'SUM((' . Product::tableName() . '.price_for_buy_with_nds - ' . Product::tableName() . '.price_for_buy_with_nds * IF(hasNds, (rate), 1))' . ' * ' . ProjectEstimateItem::tableName().'.count) as expense_estimate_sum',
                'clientsPayment.sum as clientsPaymentSum',
                'customersPayment.sum as customersPaymentSum',
            ])
            ->leftJoin(ProjectEstimate::tableName(), ProjectEstimateItem::tableName() . '.id_project_estimate' . ' = ' . ProjectEstimate::tableName() . '.id')
            ->leftJoin(Project::tableName(), ProjectEstimate::tableName() . '.project_id' . ' = ' . Project::tableName() . '.id')
            ->leftJoin(Product::tableName(), ProjectEstimateItem::tableName() . '.product_id' . ' = ' . Product::tableName() . '.id')
            ->leftJoin(TaxRate::tableName(), ProjectEstimateItem::tableName() . '.tax_id' . ' = ' . TaxRate::tableName() . '.id')
            ->leftJoin(['clientsPayment' => $clientsPayment], ProjectEstimate::tableName() . '.project_id' . ' = ' . 'clientsPayment.project_id AND ' . 'clientsPayment.project_estimate_id = ' . ProjectEstimate::tableName() . '.id')
            ->leftJoin(['customersPayment' => $customersPayment], ProjectEstimate::tableName() . '.project_id' . ' = ' . 'customersPayment.project_id AND ' . 'customersPayment.project_estimate_id = ' . ProjectEstimate::tableName() . '.id')
            ->andWhere(['and',
                [ProjectEstimate::tableName() . '.id' => $id],
                [ProjectEstimate::tableName() . '.project_id' => $project_id],
                [Product::tableName() . '.id' => new Expression(ProjectEstimateItem::tableName() . '.product_id')],
            ]);

//        echo $query->createCommand()->rawSql;

        return $query->all();
    }

    /**
     * @param Company $company
     * @param int $type
     * @return array
     */
    public function getEstimateProducts(Company $company, $type = Invoice::IO_TYPE_INCOME)
    {
        $invoiceAmount = Invoice::find()
            ->addSelect([
                Invoice::tableName() . '.*',
                Order::tableName() . '.product_id',
                'SUM(' . Order::tableName() . '.quantity) as quantity',
            ])
            ->joinWith('orders')
            ->andWhere(['and',
                ['IN', Invoice::tableName() . '.invoice_status_id', [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_PAYED_PARTIAL]],
                [Invoice::tableName() . '.type' => $type],
                [Invoice::tableName() . '.is_deleted' => 0]
            ])
            ->groupBy(Invoice::tableName() . '.project_id' . ', ' . Invoice::tableName() . '.project_estimate_id')
        ;

        $projectEstimate = ProjectEstimate::find()
            ->addSelect([
                Product::tableName() . '.*',
                ProjectEstimate::tableName() . '.id as id',
                ProjectEstimate::tableName() . '.project_id',
                ProjectEstimateItem::tableName() . '.price',
                ProjectEstimateItem::tableName() . '.hasNds',
                'IF(invoiceAmount.quantity, IF((SUM('.ProjectEstimateItem::tableName() . '.count) - invoiceAmount.quantity) <= 0, 0, SUM('.ProjectEstimateItem::tableName() . '.count) - invoiceAmount.quantity), SUM('.ProjectEstimateItem::tableName() . '.count)) as totalQuantity'
            ])
            ->joinWith('projectEstimateItems')
            ->joinWith('projectEstimateItems.product')
            ->joinWith('projectEstimateItems.priceForSellNds')
            ->joinWith('project')
            ->joinWith('projectEstimateItems.product.priceForBuyNds as priceForBuyNds')
            ->joinWith('projectEstimateItems.product.productUnit as productUnit')
            ->joinWith('projectEstimateItems.product.productStores')
            ->leftJoin(Store::tableName(), ProductStore::tableName() . '.store_id' . ' = ' . Store::tableName() . '.id')
            ->leftJoin(['invoiceAmount' => $invoiceAmount], 'invoiceAmount.project_id = ' . ProjectEstimate::tableName() . '.project_id' .
                ' AND invoiceAmount.project_estimate_id = ' . ProjectEstimate::tableName() . '.id' .
                ' AND invoiceAmount.product_id = ' . ProjectEstimateItem::tableName() . '.product_id'
            )
            ->andWhere(['and',
                [ProjectEstimate::tableName() . '.id' => $this->id],
                [ProjectEstimate::tableName() . '.project_id' => $this->project_id],
                [Project::tableName() . '.company_id' => $company->id],
                [Store::tableName() . '.is_main' => 1],
                ['=', Product::tableName() . '.id', new Expression(ProjectEstimateItem::tableName() . '.product_id')],
            ])
            ->groupBy(ProjectEstimateItem::tableName() . '.product_id')
            ->orderBy('product.title')
            ->asArray()
            ->one()
        ;

        $products = [];

        foreach ($projectEstimate['projectEstimateItems'] as $key => $items) {

            $quantityPrecision = (round($projectEstimate['totalQuantity']) == $projectEstimate['totalQuantity']) ? 0 : 3;

            $products[$key] = $items['product'];
            $products[$key]['hasEstimate'] = 1;
            $products[$key]['priceForSellNds'] = $items['priceForSellNds'];
            $products[$key]['price'] = $projectEstimate['price'];
            $products[$key]['hasNds'] = $projectEstimate['hasNds'];
            $products[$key]['totalQuantity'] = round($projectEstimate['totalQuantity'], $quantityPrecision);
        }

//        echo $projectEstimate->createCommand()->getRawSql(); die;

        return $products;
    }

    /**
     * @param $estimateId
     * @return array
     */
    public function getListItem($estimateId = null)
    {
        if (!$estimateId)
            $estimateId = $this->id;

        $items = ProjectEstimateItem::find()
            ->addSelect([
                ProjectEstimateItem::tableName() . '.*',
                Product::tableName() . '.production_type',
                ProjectEstimate::tableName() . '.nds_out as ndsOut',
                'SUM(price * count) as total',
                'SUM(IF(project_estimate.nds_out = 2, 0, IF (project_estimate.nds_out = 1, price * count * rate, price * count * (1 - 1 / (1 + rate))))) as totalNds',
                'SUM(IF (project_estimate.nds_out = 1, price * count * (1 + rate), price * count)) as totalWithNds'
            ])
            ->joinWith('projectEstimate')
            ->joinWith('product')
            ->joinWith('tax')
            ->andWhere(['and',
                [ProjectEstimate::tableName() . '.id' => (int)$estimateId],
            ])
            ->orderBy(ProjectEstimateItem::tableName() . '.number ASC')
            ->groupBy(ProjectEstimateItem::tableName() . '.product_id')
            ->all();

        $list = [];

        /** @var ProjectEstimateItem $item */
        foreach ($items as $key => $item) {
            $list[$item->product->production_type][$key]['id'] = $item->id;
            $list[$item->product->production_type][$key]['name'] = $item->title;
            $list[$item->product->production_type][$key]['article'] = $item->product->article;
            $list[$item->product->production_type][$key]['count'] = $item->count;
            $list[$item->product->production_type][$key]['unit'] = $item->product->productUnit ? $item->product->productUnit->name : Product::DEFAULT_VALUE;

            $incomeSum = $item->product->price_for_buy_with_nds * $item->count;
            $list[$item->product->production_type][$key]['income_sum'] = $incomeSum;

            $saleSum = $item->price;
            if ($item->hasNds) {
                $saleSum = $item->price * $item->count * ($item->ndsOut ? (1 + (float)$item->tax->rate) : 1);
            }

            $list[$item->product->production_type][$key]['sale_sum'] = $saleSum;

            $list['total'] = !isset($list['total']) ? $item->total : $list['total'] + $item->total;
            $list['totalNds'] = !isset($list['totalNds']) ? $item->totalNds : $list['totalNds'] + $item->totalNds;
            $list['totalWithNds'] = !isset($list['totalWithNds']) ? $item->totalWithNds : $list['totalWithNds'] + $item->totalWithNds;
        }

        return $list;
    }

    /**
     * @inheritdoc
     */
    public function getPdfFileName()
    {
        return $this->getPrintTitle() . '.pdf';
    }

    /**
     * @inheritdoc
     */
    public function getPrintTitle()
    {
        return $this->name;
    }

    /**
     * @param $fileName string
     * @param $outModel AgentReport
     * @param $destination string
     * @return PdfRenderer
     */
    public static function getRenderer($fileName, $outModel, $estimateId, $type, $destination = PdfRenderer::DESTINATION_FILE)
    {
        Yii::$app->controller->view->params['asset'] = DocumentPrintAsset::className();

        $renderer = new PdfRenderer([
            'view' => '@frontend/views/project/view/estimate/pdf-view',
            'params' => array_merge([
                'model' => $outModel,
                'estimateId' => $estimateId,
                'type' => $type,
            ]),
            'destination' => $destination,
            'filename' => $fileName,
            'displayMode' => PdfRenderer::DISPLAY_MODE_FULLPAGE,
            'format' => 'A4-L',
        ]);

        return $renderer;
    }

    public function clearEmailFiles()
    {
        return EmailFile::deleteAll(['and',
            ['company_id' => $this->project->company->id],
            ['type' => EmailFile::TYPE_PROJECT_ESTIMATE],
        ]);
    }


    public function loadEmailFiles($estimateId, $type)
    {
        $files = [];

        $this->clearEmailFiles();

        $emailFile = new EmailFile();
        $emailFile->company_id = $this->project->company->id;
        $emailFile->file_name = str_replace(['\\', '/'], '_', $this->getPdfFileName());
        $emailFile->ext = 'pdf';
        $emailFile->mime = 'application/pdf';
        $emailFile->size = 0;
        $emailFile->type = EmailFile::TYPE_PROJECT_ESTIMATE;

        if ($emailFile->save()) {
            $path = $emailFile->getFilePath() . DIRECTORY_SEPARATOR . $emailFile->file_name;

            $size = file_put_contents($path, self::getRenderer(null, $this, $estimateId, $type, PdfRenderer::DESTINATION_STRING)->output(false));

            $emailFile->size = ceil($size / 1024);

            if ($emailFile->save(true, ['size'])) {
                $files[] = [
                    'id' => $emailFile->id,
                    'name' => $emailFile->file_name,
                    'previewImg' => '<img src="/img/email/emailPdf.png" class="preview-img">',
                    'size' => $emailFile->size,
                    'deleteUrl' => Url::to(['/email/delete-email-file', 'id' => $emailFile->id]),
                    'downloadUrl' => Url::to(['/email/download-file', 'id' => $emailFile->id]),
                ];
            }
        }

        return $files;
    }

    /**
     * @return false|string
     */
    public function getDocument_date()
    {
        return date('Y-m-d', $this->date);
    }

    public function getFullNumber()
    {
        return $this->number . ' ' . $this->addition_number;
    }

    /**
     * @return int
     */
    public function getNextNumber()
    {
        return 1 + (int)self::find()->where(['project_id' => $this->project_id])->max('number');
    }

    public function getFullName($withDate = true)
    {
        return $this->name . ' № ' . $this->getFullNumber() . ($withDate ? (' от ' . DateHelper::format($this->document_date, 'd.m.Y', 'Y-m-d')) : '');
    }
}