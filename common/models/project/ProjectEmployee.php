<?php

namespace common\models\project;

use common\models\employee\Employee;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "project_employee".
 *
 * @property integer $id
 * @property integer $company_id
 * @property integer $project_id
 * @property integer $employee_id
 *
 * @property Employee $employee
 * @property Project $project
 */
class ProjectEmployee extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project_employee';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['project_id', 'employee_id'], 'required'],
            [['project_id', 'employee_id'], 'safe'],
            [['project_id', 'employee_id'], 'integer'],
            [['employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['employee_id' => 'id']],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::className(), 'targetAttribute' => ['project_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_id' => 'ID Проекта',
            'employee_id' => 'ID сотрудника',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }
}
