<?php

namespace common\models\project;

use common\models\ICompanyStrictQuery;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Class ProjectQuery
 *
 * @package common\models\document
 */
class ProjectQuery extends ActiveQuery implements ICompanyStrictQuery
{
    use \common\components\TableAliasTrait;

    /**
     * @param $companyId
     *
     * @return $this
     */
    public function byCompany($companyId)
    {
        return $this->andWhere([
            $this->getTableAlias().'.company_id' => $companyId,
        ]);
    }

    /**
     * @param $companyId
     *
     * @return $this
     */
    public function active()
    {
        return $this->andWhere([
            $this->getTableAlias().'.status' => Project::STATUS_INPROGRESS,
        ]);
    }
}
