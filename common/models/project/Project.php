<?php

namespace common\models\project;

use common\components\date\DateHelper;
use common\components\date\DatePickerFormatBehavior;
use common\components\helpers\ModelHelper;
use common\components\TextHelper;
use common\models\Agreement;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashOrderFlows;
use common\models\company\CompanyIndustry;
use common\models\Contractor;
use common\models\ContractorAutoProject;
use common\models\document\Invoice;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\status\AgreementStatus;
use frontend\models\Documents;
use frontend\models\log\ILogMessage;
use frontend\models\log\Log;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\modules\cash\components\CashStatisticInfo;
use frontend\modules\cash\models\CashFlowsProjectSearch;
use Yii;
use common\models\Company;
use common\models\employee\Employee;
use yii\behaviors\TimestampBehavior;
use yii\bootstrap\Html;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Connection;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use common\components\excel\Excel;
use DateTime;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "project".
 *
 * @property integer $id
 * @property integer $company_id
 * @property string $number
 * @property string $addition_number
 * @property integer $responsible
 * @property integer $customer_id
 * @property integer $document
 * @property string $name
 * @property integer $status
 * @property string $description
 * @property string $start_date
 * @property string $end_date
 * @property string $before_date
 * @property integer $money_amount
 * @property integer $money_expense
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $industry_id
 *
 * @property Company $company
 * @property Employee $responsibleEmployee
 * @property Invoice[] $invoices
 * @property Contractor $projectCustomer
 * @property Agreement[] $projectAgreement
 * @property ProjectCustomer[] $customers
 * @property Contractor[] $contractors
 * @property ProjectEstimate[] $estimate
 */
class Project extends \yii\db\ActiveRecord implements ILogMessage
{
    const STATUS_INPROGRESS = 0;
    const STATUS_CLOSED = 1;

    const STRING_INPROGRESS = 'В работе';
    const STRING_CLOSED = 'Завершен';

    public static $statuses = [
        self::STATUS_INPROGRESS => self::STRING_INPROGRESS,
        self::STATUS_CLOSED => self::STRING_CLOSED,
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{project}}';
    }

    /**
     * @return ActiveQuery
     */
    public static function find()
    {
        return new ProjectQuery(get_called_class());
    }

    public function getClassName()
    {
        return __CLASS__;
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return
            ArrayHelper::merge(parent::behaviors(), [
                [
                    'class' => DatePickerFormatBehavior::className(),
                    'attributes' => [
                        'start_date' => [
                            'message' => 'Дата начала указана неверно.',
                        ],
                        'end_date' => [
                            'message' => 'Дата окончания указана неверно.',
                        ],
                    ],
                ],
                'timestamp' => [
                    'class' => TimestampBehavior::className(),
                    'attributes' => [
                        ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                        ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                    ],
                ],
            ]);

    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['name', 'number', 'start_date', 'status', 'responsible', 'customer_id'], 'required'
            ],
            [['number', 'addition_number', 'responsible', 'customer_id', 'document', 'name', 'status', 'description',
                'start_date', 'end_date', 'money_amount', 'money_expense'], 'safe'],
            [['customer_id', 'document', 'responsible', 'industry_id'], 'integer'],
            [['description'], 'string'],
            [['number', 'name'], 'string', 'max' => 255],
            [['responsible'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['responsible' => 'id']],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contractor::className(), 'targetAttribute' => ['customer_id' => 'id']],
            [['document'], 'exist', 'skipOnError' => true, 'targetClass' => Agreement::className(), 'targetAttribute' => ['document' => 'id']],
            [['industry_id'], 'exist', 'skipOnError' => true, 'targetClass' => CompanyIndustry::className(), 'targetAttribute' => ['industry_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'ID компании',
            'number' => 'Номер проекта',
            'responsible' => 'Ответственный сотрудник',
            'customer_id' => 'Заказчик проекта',
            'document' => 'Основание',
            'name' => 'Название проекта',
            'status' => 'Статус проекта',
            'description' => 'Описание проекта',
            'start_date' => 'Дата начала проекта',
            'end_date' => 'Дата завершения проекта',
            'money_amount' => 'Стоимость проекта',
            'money_expense' => 'Запланированные расходы',
            'industry_id' => 'Направление',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Employee::className(), ['id' => 'responsible']);
    }

    /**
     * @return ActiveQuery
     */
    public function getResponsibleEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'responsible']);
    }

    /**
     * @return ActiveQuery
     */
    public function getProjectEmployees()
    {
        return $this->hasMany(ProjectEmployee::className(), ['project_id' => 'id', 'company_id' => 'company_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getProjectCustomer()
    {
        return $this->hasOne(Contractor::className(), ['id' => 'customer_id'])
            ->andWhere(['or',
                ['IS', Contractor::tableName().'.id', new Expression('NULL')],
                [Contractor::tableName().'.type' => Contractor::TYPE_CUSTOMER]
            ]);
    }

    /**
     * @return ActiveQuery
     */
    public function getCustomers()
    {
        return $this->hasMany(ProjectCustomer::className(), ['project_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getContractors()
    {
        return $this->hasMany(Contractor::className(), ['id' => 'customer_id'])
            ->viaTable(ProjectCustomer::tableName(), ['project_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getProjectDocument()
    {
        return $this->hasOne(Agreement::className(), ['id' => 'document']);
    }

    /**
     * @return ActiveQuery
     */
    public function getInvoices()
    {
        return $this->hasMany(Invoice::className(), ['project_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getProjectEstimate()
    {
        return $this->hasMany(ProjectEstimate::class, ['project_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndustry()
    {
        return $this->hasOne(CompanyIndustry::className(), ['id' => 'industry_id']);
    }

    /**
     * @param $company
     * @param null $number
     * @param null $date
     * @return int
     */
    public static function getNextProjectNumber(Company $company, $number = null, $date = null)
    {
        $numQuery = Project::find()
            ->andWhere(['company_id' => $company->id]);
        if ($date) {
            $numQuery->andWhere('YEAR(`start_date`) = :year', [':year' => date('Y', strtotime($date))]);
        }

        $lastNumber = $number ? $number : (int)$numQuery->max('(number * 1)');
        $nextNumber = 1 + $lastNumber;

        $existNumQuery = static::find()
            ->select('number')
            ->andWhere(['company_id' => $company->id]);

        if ($date) {
            $existNumQuery->andWhere('YEAR(`start_date`) = :year', [':year' => date('Y', strtotime($date))]);
        }

        $numArray = $existNumQuery->column();

        if (in_array($nextNumber, $numArray)) {
            return static::getNextProjectNumber($company, $nextNumber, $date);
        } else {
            return $nextNumber;
        }
    }

    /**
     * @return string
     */
    public function getProjectCompaniesList()
    {
        $result = '';
        if ($this->isNewRecord) {
            if ($this->clients) {
                foreach ($this->clients as $clientID) {
                    $contractor = Contractor::findOne($clientID);
                    $result .= Html::beginTag('li', [
                        'class' => 'select2-selection__choice item-' . $clientID,
                        'title' => $contractor->getShortName(),
                        'data-id' => $clientID,
                    ]);
                    $result .= Html::beginTag('span', [
                        'class' => 'select2-selection__choice__remove',
                        'role' => 'presentation',
                    ]);
                    $result .= '×';
                    $result .= Html::endTag('span');
                    $result .= $contractor->getShortName();
                    $result .= Html::endTag('li');
                }
            }
        } else {
            foreach ($this->projectCompanies as $projectCompany) {
                $result .= Html::beginTag('li', [
                    'class' => 'select2-selection__choice item-' . $projectCompany->contractor_id,
                    'title' => $projectCompany->contractor->getShortName(),
                    'data-id' => $projectCompany->contractor_id,
                ]);
                $result .= Html::beginTag('span', [
                    'class' => 'select2-selection__choice__remove',
                    'role' => 'presentation',
                ]);
                $result .= '×';
                $result .= Html::endTag('span');
                $result .= $projectCompany->contractor->getShortName();
                $result .= Html::endTag('li');
            }
        }

        return $result;
    }

    /**
     * @return string
     */
    public function getProjectContractorList()
    {
        $result = '';
        if ($this->isNewRecord) {
            if ($this->contractors) {
                foreach ($this->contractors as $contractorID) {
                    $contractor = Company::findOne($contractorID);
                    $result .= Html::beginTag('li', [
                        'class' => 'select2-selection__choice item-' . $contractorID,
                        'title' => $contractor->getShortName(),
                        'data-id' => $contractorID,
                    ]);
                    $result .= Html::beginTag('span', [
                        'class' => 'select2-selection__choice__remove',
                        'role' => 'presentation',
                    ]);
                    $result .= '×';
                    $result .= Html::endTag('span');
                    $result .= $contractor->getShortName();
                    $result .= Html::endTag('li');
                }
            }
        } else {
            foreach ($this->projectContractors as $projectContractor) {
                $result .= Html::beginTag('li', [
                    'class' => 'select2-selection__choice item-' . $projectContractor->contractor_id,
                    'title' => $projectContractor->contractor->getShortName(),
                    'data-id' => $projectContractor->contractor_id,
                ]);
                $result .= Html::beginTag('span', [
                    'class' => 'select2-selection__choice__remove',
                    'role' => 'presentation',
                ]);
                $result .= '×';
                $result .= Html::endTag('span');
                $result .= $projectContractor->contractor->getShortName();
                $result .= Html::endTag('li');
            }
        }

        return $result;
    }

    /**
     * @return string
     */
    public function getProjectExpenseItemList()
    {
        $result = '';
        if ($this->isNewRecord) {
            if ($this->expenseItems) {
                foreach ($this->expenseItems as $expenseItemID) {
                    $expenseItem = InvoiceExpenditureItem::findOne($expenseItemID);
                    $result .= Html::beginTag('li', [
                        'class' => 'select2-selection__choice item-' . $expenseItemID,
                        'title' => $expenseItem->name,
                        'data-id' => $expenseItemID,
                    ]);
                    $result .= Html::beginTag('span', [
                        'class' => 'select2-selection__choice__remove',
                        'role' => 'presentation',
                    ]);
                    $result .= '×';
                    $result .= Html::endTag('span');
                    $result .= $expenseItem->name;
                    $result .= Html::endTag('li');
                }
            }
        } else {
            foreach ($this->projectExpenditureItems as $projectExpenditureItem) {
                $result .= Html::beginTag('li', [
                    'class' => 'select2-selection__choice item-' . $projectExpenditureItem->expenditure_item_id,
                    'title' => $projectExpenditureItem->expenditureItem->name,
                    'data-id' => $projectExpenditureItem->expenditure_item_id,
                ]);
                $result .= Html::beginTag('span', [
                    'class' => 'select2-selection__choice__remove',
                    'role' => 'presentation',
                ]);
                $result .= '×';
                $result .= Html::endTag('span');
                $result .= $projectExpenditureItem->expenditureItem->name;
                $result .= Html::endTag('li');
            }
        }

        return $result;
    }

    /**
     * @param $dateRange
     * @return mixed
     */
    public function getIncomeSum($dateRange)
    {
        $income = CashStatisticInfo::getStatisticInfo(
            CashFlowsProjectSearch::TYPE_IO_IN,
            $this->company_id,
            $dateRange,
            $this->id
        );

        return $income['sum'];
    }

    /**
     * @param $dateRange
     * @param bool|false $withAdditionalExpense
     * @return int|mixed
     */
    public function getExpenseSum($dateRange, $withAdditionalExpense = false)
    {
        $income = CashStatisticInfo::getStatisticInfo(
            CashFlowsProjectSearch::TYPE_IO_OUT,
            $this->company_id,
            $dateRange,
            $this->id
        );

        return $income['sum'];
    }

    /**
     * @param $type
     * @param bool|true $addNew
     * @return array
     */
    public static function getAllContractorList($type, $addNew = true)
    {
        $addNewName = 'клиента';
        $filterArray = [];
        if ($addNew) {
            $filterArray["add-modal-contractor"] = '[ + Добавить ' . $addNewName . ' ]';
        }
        $contractorArray = ArrayHelper::map(
            Contractor::getSorted()
                ->byCompany(Yii::$app->user->identity->company->id)
                ->byContractor($type)
                ->byIsDeleted(Contractor::NOT_DELETED)
                ->byStatus(Contractor::ACTIVE)
                ->all(),
            'id',
            'shortName'
        );

        return ($filterArray + $contractorArray);
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        // change main customer
        if (!$this->isNewRecord) {
            if ($this->isAttributeChanged('customer_id', false)) {
                if ($this->customers) {
                    $this->customer_id = $this->customers[0]->customer_id;
                }
            }
        }

        return parent::beforeValidate();
    }

    /**
     * @param boolean $insert
     * @return boolean
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        ModelHelper::HtmlEntitiesModelAttributes($this);

        return true;
    }

    public function afterSave($insert, $changedAttributes)
    {
        if (!$insert) {
            if (array_key_exists('start_date', $changedAttributes)
                || array_key_exists('end_date', $changedAttributes)
                    || array_key_exists('status', $changedAttributes)) {

                ContractorAutoProject::updateAll([
                    'project_start' => $this->start_date,
                    'project_end' => $this->end_date,
                    'project_status' => $this->status
                ], [
                    'project_id' => $this->id
                ]);
            }
        }

        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @param $period
     * @param $project
     * @return string
     */
    public static function generateXlsTable($period, $project)
    {
        $excel = new Excel();

        $fileName = 'Проекты_' . DateHelper::format($period['from'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) .
            '-' . DateHelper::format($period['to'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

        return $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $project,
            'title' => 'Проекты',
            'columns' => [
                [
                    'attribute' => 'number',
                    'value' => function (Project $model) {
                        return $model->number;
                    },
                ],
                [
                    'attribute' => 'name',
                    'value' => function (Project $model) {
                        return $model->name;
                    },
                ],
                [
                    'attribute' => 'start_date',
                    'value' => function (Project $model) {
                        return DateHelper::format($model->start_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                    },
                ],
                [
                    'attribute' => 'end_date',
                    'value' => function (Project $model) {
                        return DateHelper::format($model->end_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                    },
                ],
                [
                    'attribute' => 'customer',
                    'value' => function (Project $model) {
                        $contractor = '';
                        foreach ($model->projectCustomer as $projectCustomer) {
                            $contractor .= $projectCustomer->nameWithType . "\n";
                        }

                        return $contractor;
                    },
                ],
                [
                    'attribute' => 'incomeSum',
                    'value' => function (Project $model) use ($period) {
                        return TextHelper::invoiceMoneyFormat($model->getIncomeSum($period));
                    },
                ],
                [
                    'attribute' => 'expenseSum',
                    'value' => function (Project $model) use ($period) {
                        return TextHelper::invoiceMoneyFormat($model->getExpenseSum($period));
                    },
                ],
                [
                    'attribute' => 'resultSum',
                    'value' => function (Project $model) use ($period) {
                        return TextHelper::invoiceMoneyFormat($model->getIncomeSum($period) - $model->getExpenseSum($period));
                    },
                ],
                [
                    'attribute' => 'responsible',
                    'value' => function (Project $model) {
                        return $model->responsibleEmployee->getShortFio();
                    },
                ],
            ],
            'headers' => [
                'number' => '№ проекта',
                'name' => 'Название',
                'customer_id' => 'Заказчик',
                'start_date' => 'Начало',
                'end_date' => 'Окончание',
                'incomeSum' => 'Приход',
                'expenseSum' => 'Расход',
                'resultSum' => 'Результат',
                'responsible' => 'Ответственный',
            ],
            'format' => 'Xlsx',
            'fileName' => $fileName,
        ]);
    }

    /**
     * Get formatted log message from Log model
     * @param Log $log
     * @return string
     */
    public function getLogMessage(Log $log)
    {
        $text = 'Проект №' . $this->number;

        $link = \common\components\helpers\Html::a($text, [
            '/project/view',
            'id' => $this->id,
        ]);

        switch ($log->log_event_id) {
            case LogEvent::LOG_EVENT_CREATE:
                return $link . '<span class="pl-1">был создан.</span>';
            case LogEvent::LOG_EVENT_DELETE:
                return $text . '<span class="pl-1">был удалён.</span>';
            case LogEvent::LOG_EVENT_UPDATE:
                return $link . '<span class="pl-1">был изменен.</span>';
            case LogEvent::LOG_EVENT_UPDATE_STATUS:
                $status = $log->getModelAttributeNew('status');
                $status_name = self::STATUS_INPROGRESS == $status ? 'В работе' : 'Завершен';

                return $link . ' статус "' . $status_name . '"';
            default:
                return $log->message;
        }
    }

    public function getLogIcon(Log $log)
    {
        return ['label-info', 'fa fa-file-text-o'];
    }

    public function getFullNumber()
    {
        return $this->number . ' ' . $this->addition_number;
    }

    public function getFullName($withDate = true)
    {
        return $this->name . ' № ' . $this->getFullNumber() . ($withDate ? (' от ' . DateHelper::format($this->start_date, 'd.m.Y', 'Y-m-d')) : '');
    }

    /**
     * @return array
     */
    public static function getSelect2Data()
    {
        $employee = Yii::$app->user->identity;

        return [0 => 'Без проекта'] + self::find()
                ->where(['company_id' => $employee->company->id])
                ->select(['name'])
                ->indexBy('id')
                ->column();
    }
}
