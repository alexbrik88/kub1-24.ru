<?php

namespace common\models\project;

use common\components\date\DatePickerFormatBehavior;
use common\models\Agreement;
use common\models\Contractor;
use common\models\document\Invoice;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "project_customer".
 *
 * @property integer $id
 * @property integer $project_id
 * @property integer $customer_id
 * @property integer $agreement_id
 * @property integer $amount
 *
 * @property Project $project
 * @property Contractor $customer
 * @property Agreement $agreement
 * @property Invoice $invoice
 */
class ProjectCustomer extends ActiveRecord
{
    public $paymentSum;
    public $customerName;
    public $client;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'project_customer';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return
            ArrayHelper::merge(parent::behaviors(), [
                'timestamp' => [
                    'class' => TimestampBehavior::className(),
                    'attributes' => [
                        ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                        ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                    ],
                ],
            ]);

    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['customer_id'], 'required'],
            [['customer_id', 'agreement_id'], 'integer'],
            [['amount'], 'integer', 'min' => 0, 'max' => 999999999999.99],
            [
                'customer_id',
                'exist',
                'skipOnError' => true,
                'targetClass' => Contractor::className(),
                'targetAttribute' => ['customer_id' => 'id'],
                'filter' => function ($query) {
                    $query->andWHere([
                        'or',
                        ['company_id' => null],
                        ['company_id' => $this->project->company_id],
                    ]);
                }
            ],
            [
                'agreement_id',
                'exist',
                'skipOnError' => true,
                'targetClass' => Agreement::className(),
                'targetAttribute' => ['agreement_id' => 'id'],
                'filter' => function ($query) {
                    $query->andWHere([
                        'company_id' => $this->project->company_id,
                    ]);
                }
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'project_id' => 'Проект',
            'customer_id' => 'Заказчик',
            'agreement_id' => 'Основание',
            'amount' => 'Сумма по договору',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Contractor::className(), ['id' => 'customer_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::className(), ['id' => 'project_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getAgreement()
    {
        return $this->hasOne(Agreement::className(), ['id' => 'agreement_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(Invoice::className(), ['project_id' => 'project_id']);
    }
}
