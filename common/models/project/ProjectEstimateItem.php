<?php

namespace common\models\project;

use common\models\product\Product;
use common\models\TaxRate;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * Class ProjectEstimateItem
 * @package common\models\project
 *
 * @property integer $id_project_estimate
 * @property integer $number
 * @property string $product_id
 * @property string $title
 * @property integer $count
 * @property integer $hasNds
 * @property integer $tax_id
 * @property integer $price
 * @property integer $unit_id
 *
 * @property ProjectEstimate $projectEstimate
 * @property Product $product
 * @property TaxRate $tax
 * @property TaxRate $priceForSellNds
 */
class ProjectEstimateItem extends ActiveRecord
{
    const TYPE_CLIENTSIDE = 1;
    const TYPE_SELFSIDE = 2;

    public $income_price;
    public $sale_price;
    public $income_sum;
    public $sale_sum;
    public $diff_sum;

    public $income_estimate_sum;
    public $expense_estimate_sum;

    public $clientsPaymentSum;
    public $customersPaymentSum;

    public $clientsPaymentSumNoNds;
    public $customersPaymentSumNoNds;

    //Product
    public $article;
    public $unit;

    public $ndsOut;

    public $total;
    public $totalNds;
    public $totalWithNds;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{project_estimate_item}}';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return
            ArrayHelper::merge(parent::behaviors(), [
                'timestamp' => [
                    'class' => TimestampBehavior::class,
                    'attributes' => [
                        ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                        ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                    ],
                ],
            ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['number', 'product_id'], 'integer'],
            [['id_project_estimate'], 'integer'],
            [['price', 'count'], 'integer'],
            [['hasNds', 'tax_id', 'unit_id'], 'integer'],
            [['title'], 'string', 'max' => 700],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'number' => 'Номер',
            'title' => 'Наименование',
            'date' => 'Дата',
            'status' => 'Статус',
            'responsible' => 'Ответственный',
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getProjectEstimate()
    {
        return $this->hasOne(ProjectEstimate::class, ['id' => 'id_project_estimate']);
    }

    /**
     * @return ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::class, ['id' => 'product_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getTax()
    {
        return $this->hasOne(TaxRate::class, ['id' => 'tax_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getPriceForSellNds()
    {
        return $this->hasOne(TaxRate::className(), ['id' => 'tax_id'])
            ->from(['ndsSell' => TaxRate::tableName()]);
    }

}