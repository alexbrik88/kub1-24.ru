<?php

namespace common\models\project;

use common\components\date\DateHelper;
use common\components\helpers\ArrayHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashOrderFlows;
use common\models\Contractor;
use common\models\document\Invoice;
use common\models\employee\Employee;
use common\models\service\Payment;
use frontend\components\StatisticPeriod;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\ArrayDataProvider;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * Class ProjectCustomerSearch
 * @package common\models\project
 */
class ProjectCustomerSearch extends ProjectCustomer
{
    /**
     * @var
     */
    public $search;

    /**
     * @var ActiveQuery|null
     */
    public $query = null;

    /**
     * @var
     */
    public $client;

    /**
     * @var
     */
    public $sort;

    /**
     * @var
     */
    public $customerName;

    /**
     * @var
     */
    public $amount;

    /**
     * @var
     */
    public $paymentSum;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['amount', 'paymentSum',], 'safe'],
            [['client',], 'safe'],
            [['sort',], 'safe'],
        ];
    }

    /**
     * @param $params
     * @param bool $withPayments
     * @return ActiveDataProvider
     */
    public function search($params, $withPayments = false):ActiveDataProvider
    {
        $query = $this->getBaseQuery($params, $withPayments);

        return new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => \Yii::$app->request->get('per-page', 10),
            ],
            'sort' => [
                'attributes' => [
                    'amount',
                    'paymentSum',
                ],
//                'defaultOrder' => [
//                    'id' => SORT_DESC,
//                    'date' => SORT_DESC,
//                ],
            ],
        ]);
    }

    /**
     * @param $params
     * @return ActiveQuery
     */
    public function getBaseQuery($params = [], $withPayments = false):ActiveQuery
    {

        $this->load($params);

        if ($withPayments) {
            $bankPayments = Contractor::find()
                ->select([
                    Contractor::tableName().'.id',
                    CashBankFlows::tableName().'.project_id',
                    'SUM(amount) as bankAmount'
                ])
                ->joinWith('cashBankFlows')
                ->andWhere(['and',
                    [Contractor::tableName().'.type' => Contractor::TYPE_CUSTOMER]
                ])
                ->groupBy(Contractor::tableName().'.id' . ', ' . CashBankFlows::tableName().'.project_id')
            ;
            $orderPayments = Contractor::find()
                ->select([
                    Contractor::tableName().'.id',
                    CashOrderFlows::tableName().'.project_id',
                    'SUM(amount) as orderAmount'
                ])
                ->joinWith('cashOrderFlows')
                ->andWhere(['and',
                    [Contractor::tableName().'.type' => Contractor::TYPE_CUSTOMER]
                ])
                ->groupBy(Contractor::tableName().'.id' . ', ' . CashOrderFlows::tableName().'.project_id')
            ;
            $emoneyPayments = Contractor::find()
                ->select([
                    Contractor::tableName().'.id',
                    CashEmoneyFlows::tableName().'.project_id',
                    'SUM(amount) as emoneyAmount'
                ])
                ->joinWith('cashEmoneyFlows')
                ->andWhere(['and',
                    [Contractor::tableName().'.type' => Contractor::TYPE_CUSTOMER]
                ])
                ->groupBy(Contractor::tableName().'.id' . ', ' . CashEmoneyFlows::tableName().'.project_id')
            ;
        }

        $query = ProjectCustomer::find()
            ->addSelect([
                ProjectCustomer::tableName() . '.*',
                'IF(company_type.name_short IS NULL, contractor.name, CONCAT(company_type.name_short, " ", contractor.name)) as customerName',
            ])
            ->addSelect([($withPayments
                ? 'IFNULL(bankPayments.bankAmount, 0) + IFNULL(orderPayments.orderAmount, 0) + IFNULL(emoneyPayments.emoneyAmount, 0) AS `paymentSum`'
                : '0 AS `paymentSum`')
            ])
            ->joinWith('project', true, 'INNER JOIN')
            ->joinWith('customer.companyType')
            ->andWhere(['and',
                [Project::tableName() . '.company_id' => Yii::$app->user->identity->company->id],
                [ProjectCustomer::tableName() . '.project_id' => (isset($params['id']) ? $params['id'] : 0)],
            ])
            ->groupBy(ProjectCustomer::tableName() . '.customer_id');

        if ($withPayments) {
            $query->leftJoin(['bankPayments' => $bankPayments], ProjectCustomer::tableName() . '.customer_id = bankPayments.id AND ' . ProjectCustomer::tableName().'.project_id' . ' = ' . 'bankPayments.project_id');
            $query->leftJoin(['orderPayments' => $orderPayments], ProjectCustomer::tableName() . '.customer_id = orderPayments.id AND ' . ProjectCustomer::tableName().'.project_id' . ' = ' . 'orderPayments.project_id');
            $query->leftJoin(['emoneyPayments' => $emoneyPayments], ProjectCustomer::tableName() . '.customer_id = emoneyPayments.id AND ' . ProjectCustomer::tableName().'.project_id' . ' = ' . 'emoneyPayments.project_id');
        }

        if ($this->client) {
            $query->andFilterWhere([ProjectCustomer::tableName() . '.customer_id' => $this->client]);
        }

        if (isset($params['sort'])) {
            $query = $this->setSorting($query, $params['sort']);
        } else {
            $query->orderBy('id');
        }

        $query->groupBy([
            ProjectCustomer::tableName() . '.customer_id',
        ]);

        $this->query = $query;

        return $query;
    }

    /**
     * @return ActiveQuery
     */
    public function getQuery():ActiveQuery
    {
        if (empty($this->query)) {
            $this->getBaseQuery();
        }

        return clone $this->query;
    }

    /**
     * @return array
     */
    public function getCustomerFilter()
    {
        $query = $this->getQuery();

        return ArrayHelper::merge([null => 'Все'], ArrayHelper::map(
            $query
                ->addSelect(['contractor.id as id'])
                ->andWhere(['not', ['contractor.id' => null]])
                ->orderBy([
                    "company_type.name_short" => SORT_ASC,
                    "contractor.name" => SORT_ASC,
                ])
                ->groupBy('contractor.id')
                ->asArray()
                ->all(),
            'id', 'customerName'));

    }

    /**
     * @param ActiveQuery $query
     * @param $sort
     * @return ActiveQuery
     */
    public function setSorting(ActiveQuery $query, $sort):ActiveQuery
    {
        $order = 'ASC';
        if (false !== strpos($sort, '-')) {
            $order = 'DESC';
            $sort = substr($sort, 1);
        }

        $query->orderBy($sort . ' ' . $order);

        return $query;
    }
}