<?php

namespace common\models\project;


use common\components\date\DateHelper;
use common\components\helpers\ArrayHelper;
use common\models\cash\CashFlowsBase;
use common\models\Contractor;
use common\models\employee\Employee;
use frontend\components\StatisticPeriod;
use frontend\modules\analytics\models\AnalyticsArticleForm;
use frontend\modules\analytics\models\OLAP;
use frontend\modules\analytics\models\ProfitAndLossSearchModel;
use frontend\rbac\permissions;
use Yii;
use yii\data\ArrayDataProvider;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * Class ProjectSearch
 * @package frontend\models
 *
 * @property Project[] $project
 */
class ProjectSearch extends Project
{
    public const SEARCH_STATUS_INPROGRESS = 1;
    public const SEARCH_STATUS_CLOSED = 2;

    public static array $statusMap = [
        self::SEARCH_STATUS_INPROGRESS => Project::STATUS_INPROGRESS,
        self::SEARCH_STATUS_CLOSED => Project::STATUS_CLOSED
    ];

    /**
     * @var
     */
    public $searchProject;

    /**
     * @var ActiveQuery|null
     */
    public $query = null;

    /**
     * @var
     */
    public $clients;

    /**
     * @var
     */
    public $contractors;

    /**
     * @var
     */
    public $incomeSum;

    /**
     * @var
     */
    public $expenseSum;

    /**
     * @var
     */
    public $resultSum;

    /**
     * @var
     */
    public $responsible;

    /**
     * @var
     */
    public $status;

    /**
     *
     */
    public static ProfitAndLossSearchModel $pal;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['searchProject'], 'trim'],
            [['searchProject', 'clients', 'responsible', 'status',], 'safe'],
            [['contractors', 'incomeSum', 'expenseSum', 'resultSum', 'industry_id'], 'integer'],
        ];
    }

    /**
     * @param $params
     * @param bool|false $forExport
     * @return array|ArrayDataProvider|ActiveRecord[]
     */
    public function search($params, $forExport = false)
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $dateRange = StatisticPeriod::getSessionPeriod();

        $this->query = $projects = Project::find()
            ->joinWith('projectCustomer')
            ->joinWith('projectCustomer.companyType')
            ->andWhere([Project::tableName() . '.company_id' => $user->company->id])
            ->andWhere(['or',
                ['between', Project::tableName() . '.start_date', $dateRange['from'], $dateRange['to']],
                ['between', Project::tableName() . '.end_date', $dateRange['from'], $dateRange['to']],
                ['and', ['<', 'start_date', $dateRange['from']], ['>', 'end_date', $dateRange['to']]]
            ]);

        $this->load($params);

        if ($this->searchProject) {
            $projects->andFilterWhere(['or',
                ['like', Project::tableName() . '.number', $this->searchProject],
                ['like', Project::tableName() . '.name', $this->searchProject],
                ['like', Contractor::tableName() . '.ITN', $this->searchProject],
            ]);
        }

        if ($this->clients) {
            $projects->andFilterWhere(['customer_id' => $this->clients]);
        }

        if ($this->responsible) {
            $projects->andFilterWhere(['responsible' => $this->responsible]);
        }

        if ($this->status) {
            $projects->andFilterWhere(['project.status' => self::$statusMap[$this->status]]);
        }

        if ($this->industry_id) {
            $projects->andFilterWhere(['project.industry_id' => $this->industry_id]);
        }

        if (!Yii::$app->user->can(permissions\Project::INDEX_ALL)) {
            $projects->joinWith('projectEmployees project_employee');
            $projects->andWhere([
                'or',
                ['project.responsible' => Yii::$app->user->id],
                ['project_employee.employee_id' => Yii::$app->user->id],
            ]);
        }

        $projects->groupBy([
            Project::tableName() . '.id',
        ]);

        if ($forExport) {
            return $projects->all();
        }

        $amounts = $this->getProjectsAmounts($dateRange);
        $palData = $this->_getProfitAndLossPeriodData($dateRange);

        $models = [];
        /* @var Project $project */
        foreach ($projects->all() as $project) {
            $models[$project->id] = $project->getAttributes([
                'id', 'number', 'addition_number', 'name', 'start_date', 'end_date', 'responsible', 'status', 'industry_id'
            ]);

            $currDate = new \DateTime();
            $endDate = date_create_from_format('Y-m-d', $models[$project->id]['end_date']);

            $models[$project->id]['before_date'] = ($currDate && $endDate)
                ? date_diff($currDate, $endDate)->format('%r%a')
                : null;

            $models[$project->id]['clients'] = [];
            $models[$project->id]['income_sum'] = 0;
            $models[$project->id]['expense_sum'] = 0;
            $models[$project->id]['result_sum'] = 0;
            $models[$project->id]['contractors'] = [];
            $models[$project->id]['pal_revenue'] = ArrayHelper::getValue($palData, "revenue.{$project->id}", 0);
            $models[$project->id]['pal_costs'] = ArrayHelper::getValue($palData, "primeCosts.{$project->id}", 0);
            $models[$project->id]['pal_net_income_loss'] = ArrayHelper::getValue($palData, "netIncomeLoss.{$project->id}", 0);
            $models[$project->id]['pal_profitability'] = ArrayHelper::getValue($palData, "profitability.{$project->id}", 0);

            /** @var ProjectCustomer $customer */
            foreach ($customers = $project->getCustomers()->all() as $customer) {
                $models[$project->id]['clients'][] = !empty($customer)
                    ? [
                        'id' => $customer->customer->id,
                        'name' => $customer->customer->getNameWithType(),
                        'type' => $customer->customer->type,
                    ]
                    : [];
            }

            //$incomeSum = $project->getIncomeSum($dateRange);
            //$expenseSum = $project->getExpenseSum($dateRange, true);
            $incomeSum = ArrayHelper::getValue($amounts, CashFlowsBase::FLOW_TYPE_INCOME . '.' . $project->id, 0);
            $expenseSum = ArrayHelper::getValue($amounts, CashFlowsBase::FLOW_TYPE_EXPENSE . '.' . $project->id, 0);
            $models[$project->id]['income_sum'] = (int)$incomeSum;
            $models[$project->id]['expense_sum'] = (int)$expenseSum;
            $models[$project->id]['result_sum'] = (int)($incomeSum - $expenseSum);
        }

        return new ArrayDataProvider([
            'allModels' => $models,
            'pagination' => [
                'pageSize' => \Yii::$app->request->get('per-page', 10),
            ],
            'sort' => [
                'attributes' => [
                    'number',
                    'name',
                    'start_date',
                    'end_date',
                    'before_date',
                    'status',
                    'income_sum',
                    'expense_sum',
                    'result_sum',
                    'clients',
                    'responsible',
                    'pal_revenue',
                    'pal_costs',
                    'pal_net_income_loss',
                    'pal_profitability',
                ],
                'defaultOrder' => [
                    'start_date' => SORT_DESC,
                    'name' => SORT_ASC,
                    'end_date' => SORT_DESC,
                ],
            ],
        ]);
    }

    /**
     * @param $dateRange
     * @return array
     */
    private static function _getProfitAndLossPeriodData($dateRange)
    {
        try {
            self::$pal = new ProfitAndLossSearchModel();
            self::$pal->setUserOption('monthGroupBy', AnalyticsArticleForm::MONTH_GROUP_PROJECT);
            self::$pal->setUserOption('revenueRuleGroup', AnalyticsArticleForm::REVENUE_RULE_GROUP_UNSET);
            self::$pal->setUserOption('revenueRule', AnalyticsArticleForm::REVENUE_RULE_UNSET);
            self::$pal->setUserOption('calcWithoutNds', false);
            self::$pal->disableMultiCompanyMode();
            self::$pal->handleItems();

            $revenue = self::$pal->getPeriodSumGroupedByMonth('totalRevenue', $dateRange['from'], $dateRange['to']);
            $netIncomeLoss = self::$pal->getPeriodSumGroupedByMonth('netIncomeLoss', $dateRange['from'], $dateRange['to']);
            $primeCosts = [];
            $profitability = [];
            foreach (array_keys($revenue) as $pid) {
                $n = ArrayHelper::getValue($netIncomeLoss, $pid, 0);
                $r = ArrayHelper::getValue($revenue, $pid, 0);
                $primeCosts[$pid] = $r - $n;
                $profitability[$pid] = 100 * $n / ($r ?: 9E99);
            }

        } catch (\Throwable $e) {
            $revenue = 0;
            $primeCosts = 0;
            $netIncomeLoss = 0;
            $profitability = 0;
        }


        return [
            'revenue' => $revenue,
            'primeCosts' => $primeCosts,
            'netIncomeLoss' => $netIncomeLoss,
            'profitability' => $profitability
        ];
    }

    private function getProjectsAmounts($dateRange)
    {
        $table = OLAP::TABLE_OLAP_FLOWS;
        $companyIds = implode(',', [Yii::$app->user->identity->company->id]);
        $exceptOwnFlowsContractors = implode(',', OLAP::$ownFlowContractorType);

        $query = "
            SELECT
              IFNULL(t.project_id, 0) project_id,                   
              t.type,
              SUM(IF(t.has_tin_parent, t.tin_child_amount, IF(t.has_tin_children, 0, t.amount))) amount
            FROM `{$table}` t
            WHERE
              t.company_id IN ({$companyIds})
              AND t.date BETWEEN '{$dateRange['from']}' AND '{$dateRange['to']}'
              AND t.contractor_id NOT IN ({$exceptOwnFlowsContractors})
            GROUP BY t.project_id, t.type
          ";

        $rawData = Yii::$app->db2->createCommand($query)->queryAll();
        $retData = [
            CashFlowsBase::FLOW_TYPE_INCOME => [],
            CashFlowsBase::FLOW_TYPE_EXPENSE => [],
        ];
        foreach ($rawData as $row) {
            $retData[$row['type']][$row['project_id']] = $row['amount'];
        }

        return $retData;
    }

    /**
     * @return array
     */
    public function getClientsFilter()
    {
        $query = clone $this->query;

        return ArrayHelper::merge([null => 'Все'], ArrayHelper::map(
            $query
                ->addSelect([
                    Project::tableName() . '.*',
                    'contractor.id',
                    'IF(company_type.name_short IS NULL, contractor.name, CONCAT(company_type.name_short, " ", contractor.name)) as contractorFullName',
                ])
                ->andWhere(['not', ['contractor.id' => null]])
                ->orderBy([
                    new \yii\db\Expression("ISNULL({{company_type}}.[[name_short]])"),
                    "company_type.name_short" => SORT_ASC,
                    "contractor.name" => SORT_ASC,
                ])
                ->groupBy('contractor.id')
                ->asArray()
                ->all(),
            'id', 'contractorFullName'));

    }

    /**
     * @return array
     */
    public function getResponsibleEmployeeFilter()
    {
        $query = clone $this->query;

        return ArrayHelper::merge([null => 'Все'], ArrayHelper::map($query
            ->joinWith('responsibleEmployee')
            ->groupBy('responsible')
            ->orderBy(['lastname' => SORT_ASC, 'firstname' => SORT_ASC, 'patronymic' => SORT_ASC])
            ->all(), 'responsible', function(Project $p) {
                $responsible = \common\models\EmployeeCompany::item($p->responsible, $p->company_id);

                return $responsible ? $responsible->getFio(true) : '-';
            }
        ));
    }

    /**
     * @return array
     */
    public function getIndustryFilter()
    {
        $query = clone $this->query;

        return ArrayHelper::merge([null => 'Все'], ArrayHelper::map($query
            ->joinWith('industry')
            ->groupBy('industry_id')
            ->orderBy(['name' => SORT_ASC])
            ->andWhere(['>', 'industry_id', '0'])
            ->all(), 'industry.id', 'industry.name'));
    }

    public function getStatusFilter()
    {
        return ArrayHelper::merge([null => 'Все'], [
            self::SEARCH_STATUS_INPROGRESS => Project::STRING_INPROGRESS,
            self::SEARCH_STATUS_CLOSED => Project::STRING_CLOSED,
        ]);
    }

    public static function getSelect2Data($alwaysVisibleProjectIds = null)
    {
        $query = Project::find()
            ->select('name')
            ->andWhere(['company_id' => Yii::$app->user->identity->company_id])
            ->andWhere('status = ' . intval(Project::STATUS_INPROGRESS));
            //->andWhere('start_date <= ' . (new Expression('current_date()')))
            //->andWhere('end_date >= ' . (new Expression('current_date()')));

        if (!empty($alwaysVisibleProjectIds)) {
            $alwaysVisibleProjectIds = (array)$alwaysVisibleProjectIds;
            $query->orWhere([
                'company_id' => Yii::$app->user->identity->company_id,
                'id' => array_filter($alwaysVisibleProjectIds, function($id) { return (int)$id; })
            ]);
        }

        $projectsArray = $query->orderBy('name')->indexBy('id')->column();

        return [0 => 'Без проекта'] + $projectsArray;
    }
}