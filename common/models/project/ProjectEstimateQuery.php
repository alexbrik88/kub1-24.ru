<?php

namespace common\models\project;

use common\models\ICompanyStrictQuery;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Class ProjectEstimateQuery
 *
 * @package common\models\document
 */
class ProjectEstimateQuery extends ActiveQuery implements ICompanyStrictQuery
{
    /**
     * @param $companyId
     *
     * @return $this
     */
    public function byCompany($companyId)
    {
        return $this->joinWith([
            'project' => function ($query) use ($companyId) {
                /* @var ProjectQuery $query */
                $query->byCompany($companyId);
            },
        ]);
    }
}
