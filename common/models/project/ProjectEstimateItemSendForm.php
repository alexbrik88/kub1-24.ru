<?php


namespace common\models\project;


use common\models\document\EmailFile;
use common\models\employee\Employee;
use Exception;
use Yii;
use yii\base\Model;
use yii\db\StaleObjectException;
use yii\helpers\Html;

class ProjectEstimateItemSendForm extends Model
{
    public $emailTo;
    public $emailFrom;

    public $emailText;

    public $subject;
    public $signature;

    public $attachment;

    public $template;
    public $templateName;

    public $projectEstimateId;
    public $projectEstimateItems;

    public function rules() {
        return [
            [['emailTo', 'emailFrom'], 'required'],
            [['subject'], 'required'],
            [['attachment', 'emailText'], 'safe'],
        ];
    }

    public function attributeLabels()
    {
        return [
            'emailFrom' => 'От кого',
            'emailTo' => 'Кому',
            'subject' => 'Тема',
            'emailText' => 'Сообщение',
            'attachment' => 'Вложения',
        ];
    }

    /**
     * @return array|bool
     * @throws StaleObjectException
     */
    public function send()
    {
        /** @var Employee $sender */
        $sender = \Yii::$app->user->identity;
        $company = $sender->company;

        $attachFiles = $this->getAttachFiles();

        $emailMessagePath = [
            'html' => 'system/project-estimate/html',
            'text' => 'system/project-estimate/text',
        ];

        $params = [
            'model' => null, // todo,
            'employee' => $sender,
            'employeeCompany' => $sender->currentEmployeeCompany,
            'company' => $company,
            'supportEmail' => Html::a('support@kub-24.ru', 'mailto:support@kub-24.ru'),
            'userMessage' => $this->emailText,
            'subject' => $this->subject,
        ];

        if (isset($requestType)) {
            $params['requestType'] = $requestType;
        }

        $message = Yii::$app->mailer->compose($emailMessagePath, $params)
            ->setFrom($this->emailFrom)
            ->setTo($this->emailTo)
            ->setSubject($this->subject);

        foreach ($attachFiles as $file) {
            if ($file['is_file']) {
                if (exif_imagetype($file['content']) === false) {
                    $message->attach($file['content'], [
                        'fileName' => $file['fileName'],
                        'contentType' => $file['contentType'],
                    ]);
                } else {
                    $message->embed($file['content'], [
                        'fileName' => $file['fileName'],
                        'contentType' => $file['contentType'],
                    ]);
                }
            } else {
                $message->attachContent($file['content'], [
                    'fileName' => $file['fileName'],
                    'contentType' => $file['contentType'],
                ]);
            }
        }

        if ($message->send()) {
            $this->clearAttachFiles();

            return true;
        } else {
            $message->getErrors();
        }

        throw new Exception('Email failed.');
    }


    /**
     * @return array
     */
    public function getAttachFiles()
    {
        $attachFiles = [];

        if ($this->attachment) {
            $emailFilesID = explode(', ', $this->attachment);
            foreach ($emailFilesID as $emailFileID) {
                /* @var $emailFile EmailFile */
                $emailFile = EmailFile::findOne($emailFileID);
                if ($emailFile) {
                    $attachFiles[] = [
                        'content' => $emailFile->getFilePath() . DIRECTORY_SEPARATOR . $emailFile->file_name,
                        'fileName' => $emailFile->file_name,
                        'contentType' => $emailFile->mime,
                        'is_file' => true,
                    ];
                }
            }
        }

        return $attachFiles;
    }

    /**
     * @throws StaleObjectException
     */
    public function clearAttachFiles()
    {
        if ($this->attachment) {
            $emailFilesID = explode(', ', $this->attachment);
            foreach ($emailFilesID as $emailFileID) {
                /* @var $emailFile EmailFile */
                $emailFile = EmailFile::findOne($emailFileID);
                if ($emailFile) {
                    $emailFile->_delete();
                }
            }
        }
    }

}