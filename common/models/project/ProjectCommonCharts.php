<?php

namespace common\models\project;

use Yii;
use frontend\modules\analytics\models\AnalyticsArticleForm;
use frontend\modules\analytics\models\AnalyticsSimpleSearch;
use frontend\modules\analytics\models\detailing\DetailingUserConfig as Config;
use frontend\modules\analytics\models\ProfitAndLossSearchModel;

class ProjectCommonCharts {

    public static function getAnalyticsModelsIndexChart()
    {
        $customChartType = Config::getIndexChartType();

        switch ($customChartType) {
            case Config::CHART_TYPE_INCOME:
                $analyticsModel = self::getAnalyticsModel();
                break;
            case Config::CHART_TYPE_EXPENSE:
                $analyticsModel = self::getAnalyticsModel();
                break;
            case Config::CHART_TYPE_REVENUE:
                $palModel = self::getPalModel();
                break;
            case Config::CHART_TYPE_PROFIT:
                $palModel = self::getPalModel();
                break;
            default:
                $analyticsModel = self::getAnalyticsModel();
                $palModel = self::getPalModel();
                break;
        }

        return [
            $palModel ?? null,
            $analyticsModel ?? null,
        ];
    }    

    public static function getAnalyticsModel(): AnalyticsSimpleSearch
    {
        $company = Yii::$app->user->identity->company;
        return new AnalyticsSimpleSearch($company);
    }

    public static function getPalModel(): ProfitAndLossSearchModel
    {
        /////////////////////////////////////////////////////
        $groupBy = AnalyticsArticleForm::MONTH_GROUP_PROJECT;
        /////////////////////////////////////////////////////

        $palModel = new ProfitAndLossSearchModel();
        $palModel->activeTab = (string)ProfitAndLossSearchModel::TAB_ALL_OPERATIONS;
        $palModel->disableMultiCompanyMode();
        //$palModel->setYearFilter([$palModel->year]);
        $palModel->setUserOption('revenueRuleGroup', AnalyticsArticleForm::REVENUE_RULE_GROUP_UNSET);
        $palModel->setUserOption('revenueRule', AnalyticsArticleForm::REVENUE_RULE_UNSET);
        $palModel->setUserOption('calcWithoutNds', false);
        $palModel->setUserOption('monthGroupBy', $groupBy);
        $palModel->handleItems();

        return $palModel;
    }
}