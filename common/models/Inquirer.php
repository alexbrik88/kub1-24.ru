<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "inquirer".
 *
 * @property integer $id
 * @property integer $company_id
 * @property integer $employee_id
 * @property integer $rating_id
 * @property integer $plan_id
 * @property string $difficulty
 * @property string $problem
 *
 * @property Employee $employee
 * @property Company $company
 */
class Inquirer extends \yii\db\ActiveRecord
{
    const PLAN_YES = 1;
    const PLAN_NO = 2;
    const PLAN_UNKNOWN = 3;

    public static $planLabels = [
        self::PLAN_YES => 'Да',
        self::PLAN_NO => 'Нет',
        self::PLAN_UNKNOWN => 'Еще не определился',
    ];

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'inquirer';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['difficulty', 'problem'], 'trim'],
            [['rating'], 'required'],
            [['rating'], 'integer', 'min' => 1, 'max' => 5],
            [['plan_id'], 'in', 'range' => [
                self::PLAN_YES,
                self::PLAN_NO,
                self::PLAN_UNKNOWN,
            ]],
            [['difficulty', 'problem'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Компания',
            'employee_id' => 'Пользователь',
            'rating' => 'Рейтинг',
            'plan_id' => 'План',
            'difficulty' => 'Трудности',
            'problem' => 'Решаемые проблемы',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(employee\Employee::className(), ['id' => 'employee_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return string
     */
    public function getPlan()
    {
        return isset(self::$planLabels[$this->plan_id]) ? self::$planLabels[$this->plan_id] : '';
    }

    /**
     * @return boolean
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        return ($this->rating || $this->plan_id || $this->difficulty || $this->problem);
    }
}
