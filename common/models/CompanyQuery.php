<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 12/11/15
 * Time: 12:13 PM
 * Email: t.kanstantsin@gmail.com
 */

namespace common\models;

use yii\db\ActiveQuery;

/**
 * Class CompanyQuery
 * @package common\models
 */
class CompanyQuery extends ActiveQuery
{

    /**
     * @param int $blocked
     * @return $this
     */
    public function isBlocked($blocked = Company::BLOCKED)
    {
        return $this->andWhere([
            Company::tableName() . '.blocked' => $blocked,
        ]);
    }

    /**
     * @param int $test
     * @return $this
     */
    public function isTest($test = Company::TEST_COMPANY)
    {
        return $this->andWhere([
            'test' => $test,
        ]);
    }
}
