<?php

namespace common\models;

use common\components\date\DateHelper;
use common\models\chat\CrmUser;
use Yii;
use common\models\employee\Employee;
use yii\behaviors\TimestampBehavior;
use yii\bootstrap\Html;
use yii\helpers\FileHelper;
use yii\helpers\Url;

/**
 * This is the model class for table "chat".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $from_employee_id
 * @property integer $to_employee_id
 * @property string $message
 * @property boolean $is_new
 * @property boolean $is_finished
 * @property boolean $is_viewed
 *
 * @property EmployeeCompany $fromEmployee
 * @property Employee $toEmployee
 * @property CrmUser $fromEmployeeCrm
 */
class Chat extends \yii\db\ActiveRecord
{
    const NEW_MESSAGE = 1;
    const FINISHED = 1;

    const TYPE_MESSAGE = 'text_message';
    const TYPE_FILE = 'file_message';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'chat';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
            ],
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->checkOnUrl();

            return true;
        }
        return false;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['from_employee_id', 'to_employee_id', 'message'], 'required'],
            [['created_at',], 'integer'],
            [['from_employee_id', 'to_employee_id'], 'safe'],
            [['message'], 'string'],
            [['is_new', 'is_finished', 'is_viewed'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'create_at' => 'Create At',
            'from_employee_id' => 'From Employee ID',
            'to_employee_id' => 'To Employee ID',
            'message' => 'Message',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFromEmployee()
    {
        return Employee::findOne($this->from_employee_id)->currentEmployeeCompany;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getToEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'to_employee_id']);
    }

    /**
     * @return bool|string
     */
    public function getFormattedDateDelimiterText()
    {
        $date = date(DateHelper::FORMAT_USER_DATE, $this->created_at);
        if ($date == date(DateHelper::FORMAT_USER_DATE)) {
            return 'Сегодня';
        } elseif ($date == date(DateHelper::FORMAT_USER_DATE, strtotime('-1 day'))) {
            return 'Вчера';
        }

        return $date;
    }

    /**
     * @return string
     */
    public function getUploadPath()
    {
        $filePath = DIRECTORY_SEPARATOR . $this->tableName()
            . DIRECTORY_SEPARATOR . $this->id;
        $fullPath = Yii::getAlias('@common') . DIRECTORY_SEPARATOR . 'uploads' . $filePath;

        if (!file_exists($fullPath)) {
            FileHelper::createDirectory($fullPath);
        }

        return $fullPath;
    }

    /**
     *
     */
    public function checkOnUrl()
    {
        $this->message = preg_replace("~(http|https|ftp|ftps)://(.*?)(\s|\n|[,.?!](\s|\n)|$)~", '<a href="$1://$2" target="_blank">$1://$2</a>$3', $this->message);
    }

    /**
     * @return bool
     */
    public function isFromEmployeeCrm()
    {
        return stristr($this->from_employee_id, 'crm') !== false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFromEmployeeCrm()
    {
        return CrmUser::findOne(['user_id' => preg_replace("/[^0-9]/", '', $this->from_employee_id)]);
    }
}
