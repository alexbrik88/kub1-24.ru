<?php

namespace common\models\selling;

use common\models\service\Subscribe;
use Yii;
use common\models\Company;

/**
 * This is the model class for table "selling_subscribe".
 *
 * @property integer $id
 * @property integer $company_id
 * @property integer $type
 * @property integer $main_type
 * @property string $invoice_number
 * @property integer $creation_date
 * @property integer $activation_date
 * @property integer $end_date
 * @property integer $subscribe_id
 *
 * @property Company $company
 * @property Subscribe $subscribe
 */
class SellingSubscribe extends \yii\db\ActiveRecord
{
    /**
     *
     */
    const TYPE_TRIAL = 1;

    /**
     *
     */
    const TYPE_PROMO_CODE_INDIVIDUAL = 2;

    /**
     *
     */
    const TYPE_PROMO_CODE_GROUPED = 3;

    /**
     *
     */
    const TYPE_TARIFF_1 = 4;

    /**
     *
     */
    const TYPE_TARIFF_2 = 5;

    /**
     *
     */
    const TYPE_TARIFF_3 = 6;

    /**
     *
     */
    const TYPE_TARIFF_FREE = 7;

    /**
     *
     */
    const TYPE_PROMO_CODE_77777 = 8;

    /**
     *
     */
    const TYPE_MAIN_FREE = 1;

    /**
     *
     */
    const TYPE_MAIN_PAY = 2;

    /**
     * @var array
     */
    public $typeArray = [
        self::TYPE_TRIAL => '14 дней (пробный период)',
        self::TYPE_PROMO_CODE_INDIVIDUAL => 'Промокод (личный)',
        self::TYPE_PROMO_CODE_GROUPED => 'Промокод (групповой)',
        self::TYPE_TARIFF_1 => '1 месяц',
        self::TYPE_TARIFF_2 => '4 месяца',
        self::TYPE_TARIFF_3 => '12 месяцев',
        self::TYPE_TARIFF_FREE => 'Бесплатный тариф',
        self::TYPE_PROMO_CODE_77777 => 'Промокод 77777',
    ];

    /**
     * @var array
     */
    public $mainTypeArray = [
        self::TYPE_MAIN_FREE => 'Бесплатный',
        self::TYPE_MAIN_PAY => 'Платный',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'selling_subscribe';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'type', 'main_type', 'creation_date'], 'required'],
            [['company_id', 'type', 'main_type', 'creation_date', 'activation_date', 'end_date', 'subscribe_id'], 'integer'],
            [['invoice_number'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['subscribe_id'], 'exist', 'skipOnError' => true, 'targetClass' => Subscribe::className(), 'targetAttribute' => ['subscribe_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'type' => 'Type',
            'main_type' => 'Main Type',
            'invoice_number' => 'Invoice Number',
            'creation_date' => 'Creation Date',
            'activation_date' => 'Activation Date',
            'end_date' => 'End Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscribe()
    {
        return $this->hasOne(Subscribe::className(), ['id' => 'subscribe_id']);
    }
}
