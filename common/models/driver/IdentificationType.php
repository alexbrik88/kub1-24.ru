<?php

namespace common\models\driver;

use Yii;

/**
 * This is the model class for table "identification_type".
 *
 * @property integer $id
 * @property string $name
 * @property integer $sort
 *
 * @property Driver[] $drivers
 */
class IdentificationType extends \yii\db\ActiveRecord
{
    /**
     * Паспорт
     */
    const TYPE_PASSPORT = 6;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'identification_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['sort'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'sort' => 'Sort',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDrivers()
    {
        return $this->hasMany(Driver::className(), ['identification_type_id' => 'id']);
    }
}
