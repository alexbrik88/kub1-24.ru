<?php

namespace common\models\driver;

use Yii;

/**
 * This is the model class for table "phone_type".
 *
 * @property integer $id
 * @property string $name
 *
 * @property DriverPhone[] $driverPhones
 */
class PhoneType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'phone_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDriverPhones()
    {
        return $this->hasMany(DriverPhone::className(), ['phone_type_id' => 'id']);
    }
}
