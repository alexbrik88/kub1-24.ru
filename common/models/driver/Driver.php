<?php

namespace common\models\driver;

use common\components\date\DateHelper;
use common\components\date\DatePickerFormatBehavior;
use common\models\vehicle\Vehicle;
use Yii;
use common\models\employee\Employee;
use common\models\Company;
use common\models\Contractor;
use yii\behaviors\TimestampBehavior;
use yii\db\Connection;

/**
 * This is the model class for table "driver".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $company_id
 * @property integer $author_id
 * @property string  $first_name
 * @property string  $last_name
 * @property string  $patronymic
 * @property boolean $has_no_patronymic
 * @property integer $contractor_id
 * @property integer $vehicle_id
 * @property string  $birthday_date
 * @property integer $identification_type_id
 * @property string  $identification_series
 * @property string  $identification_number
 * @property string  $identification_date
 * @property string  $identification_issued_by
 * @property string  $driver_license_series
 * @property string  $driver_license_number
 * @property string  $driver_license_date
 * @property string  $driver_license_issued_by
 * @property string  $home_town
 * @property string  $residential_address
 * @property string  $registration_address
 * @property string  $comment
 *
 * @property Employee $author
 * @property Company $company
 * @property Contractor $contractor
 * @property IdentificationType $identificationType
 * @property DriverPhone[] $driverPhones
 * @property DriverPhone $mainDriverPhone
 * @property Vehicle $vehicle
 */
class Driver extends \yii\db\ActiveRecord
{
    /**
     * @var
     */
    public $phone;

    /**
     * @var string
     */
    public static $uploadDirectory = 'driver';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'driver';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            [
                'class' => DatePickerFormatBehavior::class,
                'attributes' => [
                    'birthday_date' => [
                        'message' => 'Дата рождения указана неверно.',
                    ],
                    'identification_date' => [
                        'message' => 'Дата выдачи указана неверно.',
                    ],
                    'driver_license_date' => [
                        'message' => 'Дата выдачи указана неверно.',
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'author_id', 'first_name', 'last_name', 'contractor_id', 'identification_type_id',
                'identification_series', 'identification_number', 'identification_date', 'identification_issued_by',
                'driver_license_series', 'driver_license_number', 'driver_license_date', 'driver_license_issued_by',
                'home_town', 'residential_address', 'registration_address', 'phone'], 'required'],
            [['created_at', 'updated_at', 'company_id', 'author_id', 'contractor_id', 'vehicle_id',
                'identification_type_id'], 'integer'],
            [['birthday_date', 'identification_date', 'driver_license_date',], 'safe'],
            [['comment'], 'string'],
            [['first_name', 'last_name', 'patronymic', 'identification_series', 'identification_number',
                'identification_issued_by', 'driver_license_series', 'driver_license_number', 'driver_license_issued_by',
                'home_town', 'residential_address', 'registration_address'], 'string', 'max' => 255],
            [['has_no_patronymic'], 'boolean'],
            [
                ['patronymic'],
                'required',
                'when' => function ($model) {
                    return (!$model->has_no_patronymic);
                },
                'whenClient' => 'function () {
                    return !$("#driver-has_no_patronymic").is(":checked");
                }',
            ],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::class, 'targetAttribute' => ['author_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['company_id' => 'id']],
            [['contractor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contractor::class, 'targetAttribute' => ['contractor_id' => 'id']],
            [['identification_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => IdentificationType::class, 'targetAttribute' => ['identification_type_id' => 'id']],
            [['vehicle_id'], 'exist', 'skipOnError' => true, 'targetClass' => Vehicle::class, 'targetAttribute' => ['vehicle_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
            'company_id' => 'Компания',
            'author_id' => 'Автор',
            'first_name' => 'Имя',
            'last_name' => 'Фамилия',
            'patronymic' => 'Отчество',
            'has_no_patronymic' => 'Нет отчества',
            'contractor_id' => 'Где работает',
            'vehicle_id' => 'Транспортное средство',
            'birthday_date' => 'Дата рождения',
            'identification_type_id' => 'Документ',
            'identification_series' => 'Серия',
            'identification_number' => 'Номер',
            'identification_date' => 'Дата выдачи',
            'identification_issued_by' => 'Кем выдан',
            'driver_license_series' => 'Серия',
            'driver_license_number' => 'Номер',
            'driver_license_date' => 'Дата выдачи',
            'driver_license_issued_by' => 'Кем выдан',
            'home_town' => 'Город базирования',
            'residential_address' => 'Адрес проживания',
            'registration_address' => 'Адрес регистрации',
            'comment' => 'Комментарий',
            'phone' => 'Телефон',
        ];
    }

    /**
     *
     */
    public function afterFind()
    {

        parent::afterFind();

        $this->phone = $this->mainDriverPhone ? $this->mainDriverPhone->phone : null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Employee::class, ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContractor()
    {
        return $this->hasOne(Contractor::class, ['id' => 'contractor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIdentificationType()
    {
        return $this->hasOne(IdentificationType::class, ['id' => 'identification_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDriverPhones()
    {
        return $this->hasMany(DriverPhone::class, ['driver_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMainDriverPhone()
    {
        return $this->hasOne(DriverPhone::class, ['driver_id' => 'id'])
            ->onCondition(['is_main' => DriverPhone::MAIN]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehicle()
    {
        return $this->hasOne(Vehicle::class, ['id' => 'vehicle_id']);
    }

    /**
     * @return string
     */
    public function getFio()
    {
        return join(' ', array_filter([
            $this->last_name, $this->first_name, $this->patronymic,
        ]));
    }

    /**
     * @return string
     */
    public function getEmailSubject()
    {
        return 'Водитель';
    }

    /**
     * @return string
     */
    public function getEmailText()
    {
        $identificationDocument = $this->identificationType ? $this->identificationType->name : null;
        $identificationDate = DateHelper::format($this->identification_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
        $driverDate = DateHelper::format($this->driver_license_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
        $vehicle = $this->vehicle ? ('Транспортное средство: ' . str_replace('<br>', "\r\n", $this->vehicle->getVehicleTitle())) : null;

        $text = <<<EMAIL_TEXT
Здравствуйте!

Водитель: {$this->getFio()}

Документ: {$identificationDocument}
Серия: {$this->identification_series}
Номер: {$this->identification_number}
Дата выдачи: {$identificationDate}
Кем выдан: {$this->identification_issued_by}

Водительское удостоверение:
Серия: {$this->driver_license_series}
Номер: {$this->driver_license_number}
Дата выдачи: {$driverDate}
Кем выдан: {$this->driver_license_issued_by}

{$vehicle}
EMAIL_TEXT;

        return $text;
    }

    /**
     * @return mixed
     * @throws \Throwable
     */
    public function _save()
    {
        $model = $this;
        return Yii::$app->db->transaction(function (Connection $db) use ($model) {
            if ($model->save()) {
                /* @var $vehicle Vehicle */
                if ($this->vehicle_id) {
                    $vehicle = Vehicle::find()
                        ->andWhere(['id' => $this->vehicle_id])
                        ->andWhere(['company_id' => $this->company_id])
                        ->one();
                    if ($vehicle && $vehicle->driver_id !== $this->id) {
                        $vehicle->driver_id = $this->id;

                        if (!$vehicle->save(true, ['driver_id'])) {
                            return false;
                        }
                    }
                } elseif (($vehicle = Vehicle::find()
                        ->andWhere(['driver_id' => $this->id])
                        ->andWhere(['company_id' => $this->company_id])
                        ->one()) !== null) {
                    $vehicle->driver_id = null;

                    if (!$vehicle->save(true, ['driver_id'])) {
                        return false;
                    }
                }

                $saved = false;
                $phoneData = Yii::$app->request->post('DriverPhone');
                $newPhones = [];
                if (isset($phoneData['new'])) {
                    $newPhones = $phoneData['new'];
                    unset($phoneData['new']);
                }

                $savedPhones = array_keys($phoneData);
                $existsPhones = $model->getDriverPhones()->column();
                $deletePhones = array_diff($existsPhones, $savedPhones);
                /* @var $deletePhone DriverPhone */
                foreach ($deletePhones as $deletePhone) {
                    $deletePhone = DriverPhone::findOne($deletePhone);
                    $deletePhone->delete();
                }
                foreach ($phoneData as $id => $attributes) {
                    /* @var $driverPhone DriverPhone */
                    $driverPhone = DriverPhone::findOne($id);
                    foreach ($attributes as $attribute => $value) {
                        $driverPhone->$attribute = $value;
                    }
                    if ($driverPhone->save()) {
                        $saved = true;
                    }
                }
                foreach ($newPhones as $phone) {
                    $driverPhone = new DriverPhone();
                    $driverPhone->driver_id = $model->id;
                    foreach ($phone as $attribute => $value) {
                        $driverPhone->$attribute = $value;
                    }
                    if ($driverPhone->save()) {
                        $saved = true;
                    }
                }
                if (!$saved) {
                    $model->addError('phone', 'Необходимо заполнить «Телефон».');

                    $db->transaction->rollBack();
                    return false;
                }
                if (!$model->getMainDriverPhone()->exists()) {
                    /* @var $randomPhone DriverPhone */
                    $randomPhone = $model->getDriverPhones()->one();
                    $randomPhone->is_main = DriverPhone::MAIN;
                    $randomPhone->save();
                }
                return true;
            }

            return false;
        });
    }

    /**
     * @return mixed
     * @throws \Throwable
     */
    public function _delete()
    {
        $model = $this;

        return Yii::$app->db->transaction(function (Connection $db) use ($model) {
            foreach ($model->driverPhones as $driverPhone) {
                if (!$driverPhone->delete()) {
                    $db->transaction->rollBack();

                    return false;
                }
            }
            if ($model->vehicle) {
                $vehicle = $model->vehicle;
                $vehicle->driver_id = null;
                if (!$vehicle->save(true, ['driver_id'])) {
                    $db->transaction->rollBack();

                    return false;
                }
            }
            if (!$model->delete()) {
                $db->transaction->rollBack();

                return false;
            }

            return true;
        });
    }
}
