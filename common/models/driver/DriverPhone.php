<?php

namespace common\models\driver;

use common\components\validators\PhoneValidator;
use Yii;

/**
 * This is the model class for table "driver_phone".
 *
 * @property integer $id
 * @property integer $driver_id
 * @property integer $phone_type_id
 * @property string $phone
 * @property integer $is_main
 *
 * @property Driver $driver
 * @property PhoneType $phoneType
 */
class DriverPhone extends \yii\db\ActiveRecord
{
    /**
     *
     */
    const MAIN = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'driver_phone';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['driver_id', 'phone_type_id', 'phone'], 'required'],
            [['driver_id', 'phone_type_id', 'is_main'], 'integer'],
            [['phone'], PhoneValidator::className()],
            [['driver_id'], 'exist', 'skipOnError' => true, 'targetClass' => Driver::className(), 'targetAttribute' => ['driver_id' => 'id']],
            [['phone_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => PhoneType::className(), 'targetAttribute' => ['phone_type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'driver_id' => 'Driver ID',
            'phone_type_id' => 'Phone Type ID',
            'phone' => 'Phone',
            'is_main' => 'Основной',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDriver()
    {
        return $this->hasOne(Driver::className(), ['id' => 'driver_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhoneType()
    {
        return $this->hasOne(PhoneType::className(), ['id' => 'phone_type_id']);
    }
}
