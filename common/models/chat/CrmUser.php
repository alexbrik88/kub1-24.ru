<?php

namespace common\models\chat;

use Yii;

/**
 * This is the model class for table "crm_user".
 *
 * @property integer $id
 * @property string $user_fio
 * @property integer $user_id
 * @property string $company_name
 * @property string $chat_photo
 * @property integer $is_online
 * @property string $phone
 */
class CrmUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'crm_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_fio', 'user_id', 'company_name', 'chat_photo'], 'required'],
            [['user_id'], 'integer'],
            [['is_online'], 'boolean'],
            [['user_fio', 'company_name', 'chat_photo', 'phone'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_fio' => 'User Fio',
            'user_id' => 'User ID',
            'company_name' => 'Company Name',
            'chat_photo' => 'Chat Photo',
            'is_online' => 'Is Online',
        ];
    }
}
