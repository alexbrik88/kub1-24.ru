<?php

namespace common\models\helpArticle;

use Yii;
use yii\db\Expression;

/**
 * This is the model class for table "help_article".
 *
 * @property integer $id
 * @property integer $sequence
 * @property string $title
 * @property string $text
 */
class HelpArticle extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'help_article';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sequence'], 'integer', 'max' => ($this->isNewRecord ? $this->getMaxSequence() + 1 : $this->getMaxSequence())],
            [['text'], 'string'],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sequence' => 'Порядок выдачи',
            'title' => 'Загловок',
            'text' => 'Текст',
        ];
    }

    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $maxSequence = $this->getMaxSequence();
            $minSequence = $this->getMinSequence();

            if (empty($maxSequence)) {
                $this->sequence = 1;
            }

            if ($this->sequence > $maxSequence) {
                $this->sequence = $maxSequence + 1;
            }

            if ($this->sequence <= $maxSequence && $this->sequence >= $minSequence) {
                if ($insert) {
                    $this->updateSequence($maxSequence, $this->sequence);
                } else {
                    $this->updateSequence($maxSequence, $this->sequence, $this->getOldAttribute('sequence'));
                }
            }

            return true;
        }

        return false;
    }

    public function getMinSequence()
    {
        return self::find()->min('sequence');
    }

    public function getMaxSequence()
    {
        return self::find()->max('sequence');
    }

    public function updateSequence($maxSequence, $currentSequence, $oldSequence = null)
    {
        if (empty($oldSequence)) {
            self::updateAll(['sequence' => new Expression('sequence + 1')], [
                'AND',
                'sequence >= ' . $currentSequence,
                'sequence <= ' . $maxSequence,
            ]);
        } else {
            self::updateAll(['sequence' =>
                new Expression(($currentSequence < $oldSequence) ? 'sequence + 1' : 'sequence - 1')], [
                'AND',
                'sequence ' . (($currentSequence < $oldSequence) ? '>= ' . $currentSequence : '> ' . $oldSequence),
                'sequence ' . (($currentSequence < $oldSequence) ? '< ' . $oldSequence : '<= ' . $currentSequence),
            ]);
        }
    }
}
