<?php

namespace common\models\helpArticle;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\helpArticle\HelpArticle;

/**
 * HelpArticleSearch represents the model behind the search form about `common\models\helpArticle\HelpArticle`.
 */
class HelpArticleSearch extends HelpArticle
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'sequence'], 'integer'],
            [['title', 'text'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = HelpArticle::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'sequence' => $this->sequence,
        ]);

        $query->andFilterWhere(['like', 'title', $this->title])
            ->andFilterWhere(['like', 'text', $this->text]);

        $dataProvider->sort->defaultOrder = ['sequence' => SORT_ASC];

        return $dataProvider;
    }
}
