<?php

namespace common\models;

use Yii;
use common\components\date\DateHelper;
use common\models\employee\Employee;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use frontend\models\log\Log;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\models\log\ILogMessage;
use common\components\helpers\Html;
use frontend\models\Documents;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\components\pdf\Printable;
use common\components\SendDocument;
use common\models\EmployeeCompany;
use common\models\employee\EmployeeSignature;
use common\models\file\File;
use yii\db\Expression;
use yii\db\Query;
use yii\swiftmailer\Message;
use yii\web\NotFoundHttpException;
use common\components\TextHelper;
use common\models\document\EmailFile;
use common\components\pdf\PdfRenderer;
use yii\helpers\Url;
use store\assets\DocumentPrintAsset;

/**
 * This is the model class for table "agreement_essence".
 *
 * @property integer $id
 * @property integer $agreement_id
  * @property string $document_header
 * @property string $document_body
 * @property string $document_requisites_customer
 * @property string $document_requisites_executer

 *
 * @property \common\models\employee\Employee $createdBy
 * @property \common\models\Company $company
 * @property \common\models\Contractor $contractor
 */
class AgreementEssence extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'documentCreatedAtBehavior' => [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
            ],
            'blameableBehavior' => [
                'class' => BlameableBehavior::className(),
                'updatedByAttribute' => false,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'agreement_essence';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['document_header', 'document_body'], 'required'],
            [['document_requisites_customer', 'document_requisites_executer'], 'required', 'when' => function ($model) {
                return $model->agreement->document_type_id == AgreementType::TYPE_AGREEMENT;
            }],
            [['document_body'], 'string', 'min' => 1],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'document_header' => 'Шапка договора',
            'document_body' => 'Предмет договора',
            'document_requisites_customer' => 'Реквизиты заказчика',
            'document_requisites_executer' => 'Реквизиты исполнителя',
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        return parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @return boolean
     */
    public function beforeDelete()
    {
        return parent::beforeDelete();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreement()
    {
        return $this->hasOne(Agreement::className(), ['id' => 'agreement_id']);
    }
}
