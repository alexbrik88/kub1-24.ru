<?php

namespace common\models;

use common\components\validators\PhoneValidator;
use Yii;
use common\models\employee\Employee;

/**
 * This is the model class for table "phone_feedback".
 *
 * @property integer $id
 * @property integer $company_id
 * @property integer $employee_id
 * @property integer $popup_type
 * @property string $phone
 * @property string $question
 *
 * @property Company $company
 * @property Employee $employee
 */
class PhoneFeedback extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'phone_feedback';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'employee_id', 'popup_type', 'phone'], 'required'],
            [['company_id', 'employee_id', 'popup_type'], 'integer'],
            [['phone'], PhoneValidator::className()],
            [['question'], 'string'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['employee_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'employee_id' => 'Employee ID',
            'popup_type' => 'Popup Type',
            'phone' => 'Phone',
            'question' => 'Question',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }
}
