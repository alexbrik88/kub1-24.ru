<?php

namespace common\models;

/**
 * @property-write Company|null $company
 */
trait RepositoryCompanyTrait
{
    /**
     * @var Company|null
     */
    protected ?Company $company = null;

    /**
     * @param Company|null $company
     */
    public function setCompany(?Company $company): void
    {
        $this->company = $company;
    }
}
