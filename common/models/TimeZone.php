<?php


namespace common\models;


use yii\db\ActiveRecord;
use Yii;

/**
 * This is the model class for table "time_zone".
 *
 * @property integer $id
 * @property string $code
 * @property string $time_zone
 *
 */
class TimeZone extends ActiveRecord
{
    const DEFAULT_TIME_ZONE = 57; // Moscow

    const TEXT_DEFAULT_TIME_ZONE = 'Europe/Moscow';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'time_zone';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'out_time_zone' => ' Часовой пояс',
            'time_zone' => 'Часовой пояс',
        ];
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getList()
    {
        return self::find()->orderBy(['priority' => SORT_ASC])->all();
    }
}