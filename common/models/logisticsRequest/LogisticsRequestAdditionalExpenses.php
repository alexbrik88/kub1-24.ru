<?php

namespace common\models\logisticsRequest;

use Yii;
use common\models\Company;

/**
 * This is the model class for table "logistics_request_additional_expenses".
 *
 * @property integer $id
 * @property integer $company_id
 * @property string $name
 *
 * @property LogisticsRequest[] $customerLogisticsRequests
 * @property LogisticsRequest[] $carrierLogisticsRequests
 * @property Company $company
 */
class LogisticsRequestAdditionalExpenses extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'logistics_request_additional_expenses';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id'], 'integer'],
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerLogisticsRequests()
    {
        return $this->hasMany(LogisticsRequest::className(), ['carrier_additional_expenses_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarrierLogisticsRequests()
    {
        return $this->hasMany(LogisticsRequest::className(), ['customer_additional_expenses_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @param $company_id
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getList($company_id)
    {
        return self::find()
            ->andWhere([
                'or',
                ['company_id' => null],
                ['company_id' => $company_id],
            ])
            ->orderBy(['name' => SORT_ASC])
            ->indexBy('id')
            ->all();
    }

    /**
     * @param $company_id
     * @return array
     */
    public static function getSelect2Data($company_id)
    {
        $list = [];
        $options = [];
        foreach (static::getList($company_id) as $item) {
            $list[] = [
                'id' => $item->id,
                'name' => $item->name,
                'editable' => $item->company_id == $company_id,
            ];
            $options[$item['id']] = [
                'data-editable' => (int)($item->company_id == $company_id),
            ];
        }

        return ['list' => $list, 'options' => $options];
    }
}
