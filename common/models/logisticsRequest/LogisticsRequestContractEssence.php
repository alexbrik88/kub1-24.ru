<?php

namespace common\models\logisticsRequest;

use Yii;
use common\models\Company;
use common\models\Contractor;

/**
 * This is the model class for table "logistics_request_contract_essence".
 *
 * @property integer $id
 * @property integer $type
 * @property integer $company_id
 * @property integer $contractor_id
 * @property string $text
 * @property integer $is_checked
 * @property integer $for_contractor
 *
 * @property Company $company
 * @property Contractor $contractor
 */
class LogisticsRequestContractEssence extends \yii\db\ActiveRecord
{
    const TYPE_CUSTOMER = 0;
    const TYPE_CARRIER = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'logistics_request_contract_essence';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'company_id', 'contractor_id', 'is_checked', 'for_contractor'], 'integer'],
            [['company_id'], 'required'],
            [['text'], 'string'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['contractor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contractor::className(), 'targetAttribute' => ['contractor_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'company_id' => 'Company ID',
            'contractor_id' => 'Contractor ID',
            'text' => 'Text',
            'is_checked' => 'Is Checked',
            'for_contractor' => 'For Contractor',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContractor()
    {
        return $this->hasOne(Contractor::className(), ['id' => 'contractor_id']);
    }
}
