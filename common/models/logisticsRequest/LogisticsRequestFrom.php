<?php

namespace common\models\logisticsRequest;

use Yii;

/**
 * This is the model class for table "logistics_request_from".
 *
 * @property integer $id
 * @property string $name
 *
 * @property LogisticsRequest[] $logisticsRequests
 * @property LogisticsRequest[] $logisticsRequests0
 */
class LogisticsRequestFrom extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'logistics_request_from';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogisticsRequests()
    {
        return $this->hasMany(LogisticsRequest::className(), ['carrier_form_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogisticsRequests0()
    {
        return $this->hasMany(LogisticsRequest::className(), ['customer_form_id' => 'id']);
    }
}
