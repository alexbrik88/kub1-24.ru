<?php

namespace common\models\logisticsRequest;

use common\components\date\DatePickerFormatBehavior;
use common\components\validators\PhoneValidator;
use Yii;

/**
 * This is the model class for table "logistics_request_loading_and_unloading".
 *
 * @property integer $id
 * @property integer $logistics_request_id
 * @property integer $type
 * @property string $date
 * @property string $time
 * @property string $city
 * @property string $address
 * @property string $contact_person
 * @property string $contact_person_phone
 * @property string $method
 *
 * @property LogisticsRequest $logisticsRequest
 */
class LogisticsRequestLoadingAndUnloading extends \yii\db\ActiveRecord
{
    const TYPE_LOADING = 0;
    const TYPE_UNLOADING = 1;

    public static $types = [
        self::TYPE_LOADING => 'Погрузка',
        self::TYPE_UNLOADING => 'Разгрузка',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'logistics_request_loading_and_unloading';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => DatePickerFormatBehavior::className(),
                'attributes' => [
                    'date' => [
                        'message' => 'Дата указана неверно.',
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['logistics_request_id'], 'required'],
            [['logistics_request_id', 'type'], 'integer'],
            [['date', 'time'], 'safe'],
            [['contact_person_phone'], PhoneValidator::className()],
            [['city', 'address', 'contact_person', 'method',], 'string', 'max' => 255],
            [['logistics_request_id'], 'exist', 'skipOnError' => true, 'targetClass' => LogisticsRequest::className(), 'targetAttribute' => ['logistics_request_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'logistics_request_id' => 'Logistics Request ID',
            'type' => 'Вид',
            'date' => 'Дата',
            'time' => 'Время',
            'city' => 'Город',
            'address' => 'Адрес',
            'contact_person' => 'Контактное лицо',
            'contact_person_phone' => 'Телеофн',
            'method' => 'Способ Погрузки/Разгрузки',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogisticsRequest()
    {
        return $this->hasOne(LogisticsRequest::className(), ['id' => 'logistics_request_id']);
    }
}
