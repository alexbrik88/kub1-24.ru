<?php

namespace common\models\logisticsRequest;

use Yii;

/**
 * This is the model class for table "logistics_request_status".
 *
 * @property integer $id
 * @property string $name
 *
 * @property LogisticsRequest[] $logisticsRequests
 */
class LogisticsRequestStatus extends \yii\db\ActiveRecord
{
    const STATUS_ON_LOADING = 1;
    const STATUS_LOADING_END = 2;
    const STATUS_ON_UNLOADING = 3;
    const STATUS_UNLOADING_END = 4;
    const STATUS_REJECTED = 5;
    const STATUS_PROBLEM = 6;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'logistics_request_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLogisticsRequests()
    {
        return $this->hasMany(LogisticsRequest::className(), ['status_id' => 'id']);
    }
}
