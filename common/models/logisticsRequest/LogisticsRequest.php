<?php

namespace common\models\logisticsRequest;

use common\components\date\DateHelper;
use common\components\date\DatePickerFormatBehavior;
use common\components\helpers\Html;
use common\components\pdf\Printable;
use common\models\driver\Driver;
use common\models\vehicle\Vehicle;
use frontend\models\Documents;
use frontend\models\log\Log;
use frontend\models\log\LogEvent;
use Yii;
use common\models\employee\Employee;
use common\models\AgreementType;
use common\models\Contractor;
use common\models\Company;
use yii\behaviors\TimestampBehavior;
use yii\db\Connection;
use frontend\models\log\ILogMessage;

/**
 * This is the model class for table "logistics_request".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $company_id
 * @property integer $author_id
 * @property string $document_number
 * @property string $document_date
 * @property integer $customer_id
 * @property string $customer_contact_person
 * @property string $customer_agreement_document_name
 * @property string $customer_agreement_document_number
 * @property string $customer_agreement_document_date
 * @property integer $customer_agreement_document_type_id
 * @property string $customer_request_number
 * @property string $customer_request_date
 * @property string $customer_rate
 * @property integer $customer_form_id
 * @property integer $customer_condition_id
 * @property integer $customer_delay
 * @property integer $customer_additional_expenses_id
 * @property string $customer_amount
 * @property string $customer_request_essence
 * @property integer $carrier_id
 * @property string $carrier_contact_person
 * @property string $carrier_agreement_document_name
 * @property string $carrier_agreement_document_number
 * @property string $carrier_agreement_document_date
 * @property integer $carrier_agreement_document_type_id
 * @property string $carrier_request_number
 * @property string $carrier_request_date
 * @property integer $driver_id
 * @property integer $vehicle_id
 * @property string $carrier_rate
 * @property integer $carrier_form_id
 * @property integer $carrier_condition_id
 * @property integer $carrier_delay
 * @property integer $carrier_additional_expenses_id
 * @property string $carrier_amount
 * @property string $carrier_request_essence
 * @property integer $status_id
 * @property integer $is_own
 * @property integer $status_updated_at
 * @property string $goods_name
 * @property string $goods_weight
 * @property integer $goods_length
 * @property integer $goods_width
 * @property integer $goods_height
 * @property integer $goods_count
 *
 * @property Employee $author
 * @property LogisticsRequestAdditionalExpenses $carrierAdditionalExpenses
 * @property AgreementType $carrierAgreementDocumentType
 * @property LogisticsRequestCondition $carrierCondition
 * @property LogisticsRequestFrom $carrierForm
 * @property Contractor $carrier
 * @property Company $company
 * @property LogisticsRequestAdditionalExpenses $customerAdditionalExpenses
 * @property AgreementType $customerAgreementDocumentType
 * @property LogisticsRequestCondition $customerCondition
 * @property LogisticsRequestFrom $customerForm
 * @property Contractor $customer
 * @property LogisticsRequestLoadingAndUnloading $loading
 * @property LogisticsRequestLoadingAndUnloading $unloading
 * @property LogisticsRequestStatus $status
 * @property Driver $driver
 * @property Vehicle $vehicle
 */
class LogisticsRequest extends \yii\db\ActiveRecord implements ILogMessage, Printable
{
    const TYPE_CUSTOMER = 0;
    const TYPE_CARRIER = 1;

    /**
     * @var string
     */
    public $customerAgreement;

    /**
     * @var string
     */
    public $carrierAgreement;

    /**
     * @var
     */
    public $changeStatusDate;

    /**
     * @var string
     */
    public static $uploadDirectory = 'logistics_request';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'logistics_request';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
            [
                'class' => DatePickerFormatBehavior::className(),
                'attributes' => [
                    'document_date' => [
                        'message' => 'Дата указана неверно.',
                    ],
                    'customer_agreement_document_date' => [
                        'message' => 'Дата основания указана неверно.',
                    ],
                    'carrier_agreement_document_date' => [
                        'message' => 'Дата основания указана неверно.',
                    ],
                    'customer_request_date' => [
                        'message' => 'Дата заявки заказчика указана неверно.',
                    ],
                    'carrier_request_date' => [
                        'message' => 'Дата заявки перевозчика указана неверно.',
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'author_id', 'document_number', 'document_date', 'customer_id', 'customer_rate', 'driver_id',
                'vehicle_id', 'goods_name', 'goods_weight', 'goods_length', 'goods_width', 'goods_height',
                'goods_count',], 'required'],
            [['carrier_id', 'carrier_rate'], 'required',
                'when' => function (LogisticsRequest $model) {
                    return (bool)!$model->is_own;
                }
            ],
            [['company_id', 'author_id', 'customer_id', 'customer_agreement_document_type_id', 'customer_form_id',
                'customer_condition_id', 'customer_delay', 'customer_additional_expenses_id', 'carrier_id',
                'carrier_agreement_document_type_id', 'driver_id', 'vehicle_id', 'carrier_form_id',
                'carrier_condition_id', 'carrier_delay', 'carrier_additional_expenses_id', 'is_own', 'goods_length',
                'goods_width', 'goods_height', 'goods_count',], 'integer'],
            [['document_date', 'customer_agreement_document_date', 'customer_request_date',
                'carrier_agreement_document_date', 'carrier_request_date', 'changeStatusDate'], 'safe'],
            [['customer_request_essence', 'carrier_request_essence', 'customerAgreement', 'carrierAgreement',], 'string'],
            [['document_number', 'customer_contact_person', 'customer_agreement_document_name',
                'customer_agreement_document_number', 'customer_request_number', 'customer_rate', 'customer_amount',
                'carrier_contact_person', 'carrier_agreement_document_name', 'carrier_agreement_document_number',
                'carrier_request_number', 'carrier_rate', 'carrier_amount', 'goods_name', 'goods_weight',], 'string', 'max' => 255],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['author_id' => 'id']],
            [['carrier_additional_expenses_id'], 'exist', 'skipOnError' => true, 'targetClass' => LogisticsRequestAdditionalExpenses::className(), 'targetAttribute' => ['carrier_additional_expenses_id' => 'id']],
            [['carrier_agreement_document_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => AgreementType::className(), 'targetAttribute' => ['carrier_agreement_document_type_id' => 'id']],
            [['carrier_condition_id'], 'exist', 'skipOnError' => true, 'targetClass' => LogisticsRequestCondition::className(), 'targetAttribute' => ['carrier_condition_id' => 'id']],
            [['carrier_form_id'], 'exist', 'skipOnError' => true, 'targetClass' => LogisticsRequestFrom::className(), 'targetAttribute' => ['carrier_form_id' => 'id']],
            [['carrier_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contractor::className(), 'targetAttribute' => ['carrier_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['customer_additional_expenses_id'], 'exist', 'skipOnError' => true, 'targetClass' => LogisticsRequestAdditionalExpenses::className(), 'targetAttribute' => ['customer_additional_expenses_id' => 'id']],
            [['customer_agreement_document_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => AgreementType::className(), 'targetAttribute' => ['customer_agreement_document_type_id' => 'id']],
            [['customer_condition_id'], 'exist', 'skipOnError' => true, 'targetClass' => LogisticsRequestCondition::className(), 'targetAttribute' => ['customer_condition_id' => 'id']],
            [['customer_form_id'], 'exist', 'skipOnError' => true, 'targetClass' => LogisticsRequestFrom::className(), 'targetAttribute' => ['customer_form_id' => 'id']],
            [['customer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contractor::className(), 'targetAttribute' => ['customer_id' => 'id']],
            [['driver_id'], 'exist', 'skipOnError' => true, 'targetClass' => Driver::className(), 'targetAttribute' => ['driver_id' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => LogisticsRequestStatus::className(), 'targetAttribute' => ['status_id' => 'id']],
            [['vehicle_id'], 'exist', 'skipOnError' => true, 'targetClass' => Vehicle::className(), 'targetAttribute' => ['vehicle_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'company_id' => 'Company ID',
            'author_id' => 'Author ID',
            'document_number' => 'Номер',
            'document_date' => 'Дата',
            'customer_id' => 'Заказчик',
            'customer_contact_person' => 'Контактное лицо заказчика',
            'customer_agreement_document_name' => 'Название договора',
            'customer_agreement_document_number' => 'Номер договора',
            'customer_agreement_document_date' => 'Дата договора',
            'customer_agreement_document_type_id' => 'Тип договора',
            'customer_request_number' => '№ заявки заказчика',
            'customer_request_date' => 'Дата заявки заказчика',
            'customer_rate' => 'Ставка',
            'customer_form_id' => 'Форма',
            'customer_condition_id' => 'Условие',
            'customer_delay' => 'Отсрочка',
            'customer_additional_expenses_id' => 'Доп. расходы',
            'customer_amount' => 'Сумма',
            'customer_request_essence' => 'Предмет договора с закачиком',
            'carrier_id' => 'Перевозчик',
            'carrier_contact_person' => 'Контактное лицо перевозчика',
            'carrier_agreement_document_name' => 'Название договора',
            'carrier_agreement_document_number' => 'Номер договора',
            'carrier_agreement_document_date' => 'Дата договора',
            'carrier_agreement_document_type_id' => 'Тип договора',
            'carrier_request_number' => '№ заявки перевозчика',
            'carrier_request_date' => 'Дата заявки перевозчика',
            'driver_id' => 'Водитель',
            'vehicle_id' => 'Транспорт',
            'carrier_rate' => 'Ставка',
            'carrier_form_id' => 'Форма',
            'carrier_condition_id' => 'Условие',
            'carrier_delay' => 'Отсрочка',
            'carrier_additional_expenses_id' => 'Доп. расходы',
            'carrier_amount' => 'Сумма',
            'carrier_request_essence' => 'Предмет договора с перевозчиком',
            'is_own' => 'Собственный транспорт',
            'goods_name' => 'Груз',
            'goods_weight' => 'Вес (тонн)',
            'goods_length' => 'Д',
            'goods_width' => 'Ш',
            'goods_height' => 'В',
            'goods_count' => 'Кол-во (шт.)',
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->is_own) {
                $this->carrier_id = null;
                $this->carrier_agreement_document_name = null;
                $this->carrier_agreement_document_number = null;
                $this->carrier_agreement_document_date = null;
                $this->carrier_agreement_document_type_id = null;
                $this->carrier_rate = null;
                $this->carrier_form_id = null;
                $this->carrier_condition_id = null;
                $this->carrier_delay = null;
                $this->carrier_amount = null;
            }

            if ($insert || $this->isAttributeChanged('status_id', false)) {
                if (!$this->changeStatusDate) {
                    $this->status_updated_at = time();
                }
            }

            return true;
        }
        return false;
    }

    /**
     *
     */
    public function afterFind()
    {
        if ($this->customer_agreement_document_number) {
            $customerAgreementDate = DateHelper::format($this->customer_agreement_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
            $this->customerAgreement = "{$this->customer_agreement_document_name} № {$this->customer_agreement_document_number} от {$customerAgreementDate}";
        }

        if ($this->carrier_agreement_document_number) {
            $carrierAgreementDate = DateHelper::format($this->carrier_agreement_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
            $this->carrierAgreement = "{$this->carrier_agreement_document_name} № {$this->carrier_agreement_document_number} от {$carrierAgreementDate}";
        }

        parent::afterFind();
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        $customerAgreement = explode('&', $this->customerAgreement);
        if (isset($customerAgreement[0]) && isset($customerAgreement[1]) && isset($customerAgreement[2])) {
            $this->customer_agreement_document_name = $customerAgreement[0];
            $this->customer_agreement_document_number = $customerAgreement[1];
            $this->customer_agreement_document_date = $customerAgreement[2];
            $this->customer_agreement_document_type_id = isset($customerAgreement[3]) ? $customerAgreement[3] : null;
        } else {
            $this->customer_agreement_document_name = null;
            $this->customer_agreement_document_number = null;
            $this->customer_agreement_document_date = null;
            $this->customer_agreement_document_type_id = null;
        }

        $carrierAgreement = explode('&', $this->carrierAgreement);
        if (isset($carrierAgreement[0]) && isset($carrierAgreement[1]) && isset($carrierAgreement[2])) {
            $this->carrier_agreement_document_name = $carrierAgreement[0];
            $this->carrier_agreement_document_number = $carrierAgreement[1];
            $this->carrier_agreement_document_date = $carrierAgreement[2];
            $this->carrier_agreement_document_type_id = isset($carrierAgreement[3]) ? $carrierAgreement[3] : null;
        } else {
            $this->carrier_agreement_document_name = null;
            $this->carrier_agreement_document_number = null;
            $this->carrier_agreement_document_date = null;
            $this->carrier_agreement_document_type_id = null;
        }

        return parent::beforeValidate();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Employee::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarrierAdditionalExpenses()
    {
        return $this->hasOne(LogisticsRequestAdditionalExpenses::className(), ['id' => 'carrier_additional_expenses_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarrierAgreementDocumentType()
    {
        return $this->hasOne(AgreementType::className(), ['id' => 'carrier_agreement_document_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarrierCondition()
    {
        return $this->hasOne(LogisticsRequestCondition::className(), ['id' => 'carrier_condition_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarrierForm()
    {
        return $this->hasOne(LogisticsRequestFrom::className(), ['id' => 'carrier_form_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCarrier()
    {
        return $this->hasOne(Contractor::className(), ['id' => 'carrier_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDriver()
    {
        return $this->hasOne(Driver::className(), ['id' => 'driver_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getVehicle()
    {
        return $this->hasOne(Vehicle::className(), ['id' => 'vehicle_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerAdditionalExpenses()
    {
        return $this->hasOne(LogisticsRequestAdditionalExpenses::className(), ['id' => 'customer_additional_expenses_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerAgreementDocumentType()
    {
        return $this->hasOne(AgreementType::className(), ['id' => 'customer_agreement_document_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerCondition()
    {
        return $this->hasOne(LogisticsRequestCondition::className(), ['id' => 'customer_condition_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomerForm()
    {
        return $this->hasOne(LogisticsRequestFrom::className(), ['id' => 'customer_form_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomer()
    {
        return $this->hasOne(Contractor::className(), ['id' => 'customer_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoading()
    {
        return $this->hasOne(LogisticsRequestLoadingAndUnloading::className(), ['logistics_request_id' => 'id'])
            ->onCondition(['type' => LogisticsRequestLoadingAndUnloading::TYPE_LOADING]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnloading()
    {
        return $this->hasOne(LogisticsRequestLoadingAndUnloading::className(), ['logistics_request_id' => 'id'])
            ->onCondition(['type' => LogisticsRequestLoadingAndUnloading::TYPE_UNLOADING]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(LogisticsRequestStatus::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentStatus()
    {
        return $this->getStatus();
    }

    /**
     * @param Company $company
     * @param null $number
     * @return int|null
     */
    public static function getNextDocumentNumber(Company $company, $number = null)
    {
        $numQuery = static::find()
            ->andWhere(['company_id' => $company->id]);
        $lastNumber = $number ? $number : (int)$numQuery->max('(document_number * 1)');
        $nextNumber = 1 + $lastNumber;
        $existNumQuery = static::find()
            ->select('document_number')
            ->andWhere(['company_id' => $company->id]);
        $numArray = $existNumQuery->column();
        if (in_array($nextNumber, $numArray)) {
            return static::getNextDocumentNumber($company, $nextNumber);
        } else {
            return $nextNumber;
        }
    }

    /**
     * @param $type
     * @return string
     */
    public function getTitle($type)
    {
        $dateWithNumber = '№' . $this->document_number . ' от ' . DateHelper::format($this->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
        if ($type == static::TYPE_CUSTOMER && $this->customerAgreement) {
            return 'Заявка ' . $dateWithNumber . ' к Договору №' . $this->customer_agreement_document_number .
                ' от ' . DateHelper::format($this->customer_agreement_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
        } elseif ($type == static::TYPE_CARRIER && $this->carrierAgreement) {
            return 'Заявка ' . $dateWithNumber . ' к Договору №' . $this->carrier_agreement_document_number .
                ' от ' . DateHelper::format($this->carrier_agreement_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
        }

        return 'Договор-заявка ' . $dateWithNumber;
    }

    /**
     * @param Log $log
     * @return array
     */
    public function getLogIcon(Log $log)
    {
        return Documents::getLogIcon($log);
    }

    /**
     * @param Log $log
     * @return string
     */
    public function getLogMessage(Log $log)
    {
        $text = 'Договор-заявка №' . $log->getModelAttributeNew('document_number');
        $link = Html::a($text, ['/documents/invoice/view', 'id' => $log->getModelAttributeNew('id')]);

        switch ($log->log_event_id) {
            case LogEvent::LOG_EVENT_CREATE:
                return $link . ', был создан.';
            case LogEvent::LOG_EVENT_DELETE:
                return $text . ', был удалён.';
            case LogEvent::LOG_EVENT_UPDATE:
                return $link . ', был изменен.';
            case LogEvent::LOG_EVENT_UPDATE_STATUS:
                /* @var LogisticsRequestStatus $status */
                $status = LogisticsRequestStatus::findOne($log->getModelAttributeNew('status_id'));

                return $status !== null ? $link . ', статус "' . $status->name . '"' : '';
            default:
                return $log->message;
        }
    }

    /**
     * @return string
     */
    public function getStatusIcon()
    {
        $ico = 'icon-doc';
        switch ($this->status_id) {
            case LogisticsRequestStatus::STATUS_ON_LOADING:
                $ico = 'flaticon-cart';
                break;
            case LogisticsRequestStatus::STATUS_LOADING_END:
                $ico = 'flaticon-truck-1';
                break;
            case LogisticsRequestStatus::STATUS_ON_UNLOADING:
                $ico = 'flaticon-trolley-1';
                break;
            case LogisticsRequestStatus::STATUS_UNLOADING_END:
                $ico = 'flaticon-truck';
                break;
            case LogisticsRequestStatus::STATUS_REJECTED:
                $ico = 'icon-ban';
                break;
            case LogisticsRequestStatus::STATUS_PROBLEM:
                $ico = 'flaticon-caution';
                break;
        }

        return $ico;
    }

    public function getStatusIconNew() {
        $ico = 'icon-doc';
        switch ($this->status_id) {
            case LogisticsRequestStatus::STATUS_ON_LOADING:
                $ico = 'truck-loading';
                break;
            case LogisticsRequestStatus::STATUS_LOADING_END:
                $ico = 'truck-loading-end';
                break;
            case LogisticsRequestStatus::STATUS_ON_UNLOADING:
                $ico = 'truck-unloading';
                break;
            case LogisticsRequestStatus::STATUS_UNLOADING_END:
                $ico = 'truck-unloading-end';
                break;
            case LogisticsRequestStatus::STATUS_REJECTED:
                $ico = 'truck-rejected';
                break;
            case LogisticsRequestStatus::STATUS_PROBLEM:
                $ico = 'truck-problem';
                break;
        }

        return $ico;
    }

    /**
     * @return string
     */
    public function getStatusColor()
    {
        $color = 'darkblue';
        if (in_array($this->status_id, [LogisticsRequestStatus::STATUS_REJECTED, LogisticsRequestStatus::STATUS_UNLOADING_END])) {
            $color = 'green';
        } elseif ($this->status_id == LogisticsRequestStatus::STATUS_PROBLEM) {
            $color = 'red';
        }

        return $color;
    }

    /**
     * @param int $requestType
     * @return string
     */
    public function getEmailSubject($requestType = self::TYPE_CUSTOMER)
    {
        $headerText = 'Договор-заявка';
        $numberWithDate = ' №' . $this->document_number . ' от ' . DateHelper::format($this->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
        if ($requestType == LogisticsRequest::TYPE_CUSTOMER) {
            if ($this->customerAgreement) {
                $headerText = 'Заявка';
            }
            $headerText .= $numberWithDate;
            $headerText .= ' с Заказчиком';
        } else {
            if ($this->carrierAgreement) {
                $headerText = 'Заявка';
            }
            $headerText .= $numberWithDate;
            $headerText .= ' с Перевозчиком';
        }

        return $headerText;
    }

    /**
     * @param int $requestType
     * @return string
     */
    public function getEmailText($requestType = self::TYPE_CUSTOMER)
    {
        $title = $this->getEmailSubject($requestType);

        $text = <<<EMAIL_TEXT
Здравствуйте!

{$title}
EMAIL_TEXT;

        return $text;
    }

    /**
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function getPdfFileName()
    {
        return $this->getPrintTitle() . '.pdf';
    }

    /**
     * @return mixed
     * @throws \yii\web\NotFoundHttpException
     */
    public function getPrintTitle()
    {
        return str_replace([' ', '"'], ['_'],
            'Заявка' . '_№' . $this->document_number . '_' .
            date_format(date_create($this->document_date), 'd.m.Y') . '_' .
            $this->company->getTitle(true)
        );
    }

    /**
     * @throws \Throwable
     */
    public function _delete()
    {
        $model = $this;
        return Yii::$app->db->transaction(function (Connection $db) use ($model) {
            if ($this->loading && !$this->loading->delete()) {
                $db->transaction->rollBack();

                return false;
            }
            if ($this->unloading && !$this->unloading->delete()) {
                $db->transaction->rollBack();

                return false;
            }
            return $this->delete();
        });
    }
}
