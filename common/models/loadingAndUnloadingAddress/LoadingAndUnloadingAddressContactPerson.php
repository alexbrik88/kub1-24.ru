<?php

namespace common\models\loadingAndUnloadingAddress;

use Yii;
use common\models\driver\PhoneType;

/**
 * This is the model class for table "loading_and_unloading_address_contact_person".
 *
 * @property integer $id
 * @property integer $address_id
 * @property string $contact_person
 * @property integer $phone_type_id
 * @property string $phone
 * @property integer $is_main
 *
 * @property LoadingAndUnloadingAddress $address
 * @property PhoneType $phoneType
 */
class LoadingAndUnloadingAddressContactPerson extends \yii\db\ActiveRecord
{
    const MAIN = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'loading_and_unloading_address_contact_person';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['address_id', 'contact_person', 'phone_type_id', 'phone'], 'required'],
            [['address_id', 'phone_type_id', 'is_main'], 'integer'],
            [['contact_person', 'phone'], 'string', 'max' => 255],
            [['address_id'], 'exist', 'skipOnError' => true, 'targetClass' => LoadingAndUnloadingAddress::className(), 'targetAttribute' => ['address_id' => 'id']],
            [['phone_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => PhoneType::className(), 'targetAttribute' => ['phone_type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'address_id' => 'Address ID',
            'contact_person' => 'Contact Person',
            'phone_type_id' => 'Phone Type ID',
            'phone' => 'Phone',
            'is_main' => 'Is Main',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAddress()
    {
        return $this->hasOne(LoadingAndUnloadingAddress::className(), ['id' => 'address_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPhoneType()
    {
        return $this->hasOne(PhoneType::className(), ['id' => 'phone_type_id']);
    }
}
