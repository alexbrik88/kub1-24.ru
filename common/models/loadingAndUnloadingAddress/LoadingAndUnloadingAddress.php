<?php

namespace common\models\loadingAndUnloadingAddress;

use common\components\date\DatePickerFormatBehavior;
use Yii;
use common\models\employee\Employee;
use common\models\Company;
use common\models\Contractor;
use yii\behaviors\TimestampBehavior;
use yii\db\Connection;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "loading_and_unloading_address".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $company_id
 * @property integer $author_id
 * @property string  $number
 * @property string  $date
 * @property integer $address_type
 * @property integer $contractor_id
 * @property string  $address
 * @property integer $consignor_id
 * @property integer $consignee_id
 * @property integer $status
 * @property string  $map_width
 * @property string  $map_longitude
 *
 * @property Employee $author
 * @property Company $company
 * @property Contractor $contractor
 * @property LoadingAndUnloadingAddressContactPerson[] $loadingAndUnloadingAddressContactPeople
 * @property LoadingAndUnloadingAddressContactPerson $mainLoadingAndUnloadingAddressContactPerson
 * @property Contractor $consignor
 * @property Contractor $consignee
 */
class LoadingAndUnloadingAddress extends \yii\db\ActiveRecord
{
    /**
     *
     */
    const TYPE_LOADING = 0;
    /**
     *
     */
    const TYPE_UNLOADING = 1;

    /**
     *
     */
    const STATUS_IN_WORK = 1;
    /**
     *
     */
    const STATUS_IN_ARCHIVE = 2;

    /**
     * @var array
     */
    public static $types = [
        self::TYPE_LOADING => 'Погрузка',
        self::TYPE_UNLOADING => 'Разгрузка',
    ];

    /**
     * @var array
     */
    public static $statuses = [
        self::STATUS_IN_WORK => 'В работе',
        self::STATUS_IN_ARCHIVE => 'Архив',
    ];

    /**
     * @var
     */
    public $contactPerson;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'loading_and_unloading_address';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::class,
            [
                'class' => DatePickerFormatBehavior::class,
                'attributes' => [
                    'date' => [
                        'message' => 'Дата указана неверно.',
                    ],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'author_id', 'number', 'date', 'contractor_id', 'address', 'status',
                'contactPerson'], 'required'],
            [['created_at', 'updated_at', 'company_id', 'author_id', 'address_type', 'contractor_id', 'status'], 'integer'],
            [['address'], 'validateAddress'],
            [['date'], 'safe'],
            [['consignor_id', 'consignee_id'], 'exist', 'targetClass' => Contractor::class, 'targetAttribute' => 'id'],
            [['number', 'address', 'map_width', 'map_longitude'], 'string', 'max' => 255],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::class, 'targetAttribute' => ['author_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['company_id' => 'id']],
            [['contractor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contractor::class, 'targetAttribute' => ['contractor_id' => 'id']],
        ];
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function validateAddress($attribute, $params)
    {
        preg_match("/^(\d+)(.*)/u", $this->$attribute, $matches);

        if (empty($matches[1])) {
            $this->addError($attribute, 'Адрес должен начинаться с почтового индекса');
        } elseif (strlen($matches[1]) != 6) {
            $this->addError($attribute, 'Почтовый индекс заполнен не правильно');
        }

        $address = isset($matches[2]) ? mb_ereg_replace("[^A-zА-я]", "", $matches[2]) : '';
        if (strlen($address) < 3) {
            $this->addError($attribute, 'Адрес заполнен не правильно');
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'company_id' => 'Company ID',
            'author_id' => 'Author ID',
            'number' => 'Адрес №',
            'date' => 'Дата',
            'address_type' => 'Тип',
            'contractor_id' => 'Заказчик',
            'address' => 'Адрес',
            'consignor_id' => 'Грузоотправитель',
            'consignee_id' => 'Грузополучатель',
            'status' => 'Статус',
            'contactPerson' => 'Контактное лицо и телефон',
            'map_width' => 'Ширина',
            'map_longitude' => 'Долгота',
        ];
    }

    /**
     *
     */
    public function afterFind()
    {

        parent::afterFind();

        $this->contactPerson = $this->mainLoadingAndUnloadingAddressContactPerson ? $this->mainLoadingAndUnloadingAddressContactPerson->contact_person : null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Employee::class, ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContractor()
    {
        return $this->hasOne(Contractor::class, ['id' => 'contractor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConsignor()
    {
        return $this->hasOne(Contractor::class, ['id' => 'consignor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConsignee()
    {
        return $this->hasOne(Contractor::class, ['id' => 'consignee_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLoadingAndUnloadingAddressContactPeople()
    {
        return $this->hasMany(LoadingAndUnloadingAddressContactPerson::class, ['address_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMainLoadingAndUnloadingAddressContactPerson()
    {
        return $this->hasOne(LoadingAndUnloadingAddressContactPerson::class, ['address_id' => 'id'])
            ->onCondition(['is_main' => true]);
    }

    /**
     * @param Company $company
     * @param null $number
     * @return int|null
     */
    public static function getNextDocumentNumber(Company $company, $number = null)
    {
        $numQuery = static::find()
            ->andWhere(['company_id' => $company->id]);
        $lastNumber = $number ? $number : (int)$numQuery->max('(number * 1)');
        $nextNumber = 1 + $lastNumber;
        $existNumQuery = static::find()
            ->select('number')
            ->andWhere(['company_id' => $company->id]);
        $numArray = $existNumQuery->column();
        if (in_array($nextNumber, $numArray)) {
            return static::getNextDocumentNumber($company, $nextNumber);
        } else {
            return $nextNumber;
        }
    }

    /**
     * @return array
     */
    public function getConsignorArray()
    {
        $query = Contractor::getSorted()
            ->byCompany($this->company_id)
            ->byDeleted()
            ->andWhere(['or', [Contractor::tableName() . '.is_seller' => 1], [Contractor::tableName() . '.is_customer' => 1]])
            ->andWhere(['NOT', [Contractor::tableName() . '.id' => $this->contractor_id]])
            ->all();

        return ArrayHelper::merge(['' => 'Он же'], ArrayHelper::map($query, 'id', 'shortName'));
    }

    /**
     * @return mixed
     * @throws \Throwable
     */
    public function _save()
    {
        $model = $this;
        return Yii::$app->db->transaction(function (Connection $db) use ($model) {
            if ($model->save()) {
                $saved = false;
                $contactPersonData = Yii::$app->request->post('LoadingAndUnloadingAddressContactPerson');
                $newContactPersons = [];
                if (isset($contactPersonData['new'])) {
                    $newContactPersons = $contactPersonData['new'];
                    unset($contactPersonData['new']);
                }

                $savedContactPersons = array_keys($contactPersonData);
                $existsContactPersons = $model->getLoadingAndUnloadingAddressContactPeople()->column();
                $deleteContactPersons = array_diff($existsContactPersons, $savedContactPersons);
                /* @var $deleteContactPerson LoadingAndUnloadingAddressContactPerson */
                foreach ($deleteContactPersons as $deleteContactPerson) {
                    $deleteContactPerson = LoadingAndUnloadingAddressContactPerson::findOne($deleteContactPerson);
                    $deleteContactPerson->delete();
                }
                foreach ($contactPersonData as $id => $attributes) {
                    /* @var $addressContactPerson LoadingAndUnloadingAddressContactPerson */
                    $addressContactPerson = LoadingAndUnloadingAddressContactPerson::findOne($id);
                    foreach ($attributes as $attribute => $value) {
                        $addressContactPerson->$attribute = $value;
                    }
                    if ($addressContactPerson->save()) {
                        $saved = true;
                    }
                }
                foreach ($newContactPersons as $newContactPerson) {
                    $addressContactPerson = new LoadingAndUnloadingAddressContactPerson();
                    $addressContactPerson->address_id = $model->id;
                    foreach ($newContactPerson as $attribute => $value) {
                        $addressContactPerson->$attribute = $value;
                    }
                    if ($addressContactPerson->save()) {
                        $saved = true;
                    }
                }
                if (!$saved) {
                    $model->addError('contactPhone', 'Необходимо заполнить «Контактное лицо и телефон».');

                    $db->transaction->rollBack();
                    return false;
                }
                if (!$model->getMainLoadingAndUnloadingAddressContactPerson()->exists()) {
                    /* @var $randomContactPerson LoadingAndUnloadingAddressContactPerson */
                    $randomContactPerson = $model->getLoadingAndUnloadingAddressContactPeople()->one();
                    $randomContactPerson->is_main = LoadingAndUnloadingAddressContactPerson::MAIN;
                    $randomContactPerson->save();
                }
                return true;
            }

            return false;
        });
    }

    /**
     * @return mixed
     * @throws \Throwable
     */
    public function _delete()
    {
        $model = $this;
        return Yii::$app->db->transaction(function (Connection $db) use ($model) {
            foreach ($model->loadingAndUnloadingAddressContactPeople as $loadingAndUnloadingAddressContactPerson) {
                if (!$loadingAndUnloadingAddressContactPerson->delete()) {
                    $db->transaction->rollBack();

                    return false;
                }
            }
            if (!$model->delete()) {
                $db->transaction->rollBack();

                return false;
            }

            return true;
        });
    }
}
