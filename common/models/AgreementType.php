<?php

namespace common\models;

use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\PackingList;
use Yii;

/**
 * This is the model class for table "agreement_type".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Act[] $acts
 * @property Agreement[] $agreements
 * @property Invoice[] $invoices
 * @property PackingList[] $packingLists
 */
class AgreementType extends \yii\db\ActiveRecord
{
    const TYPE_AGREEMENT = 1;
    const TYPE_CUSTOMER = 2;
    const TYPE_AGREEMENT_NAME = 'Договор';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'agreement_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Тип документа',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActs()
    {
        return $this->hasMany(Act::className(), ['basis_document_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreements()
    {
        return $this->hasMany(Agreement::className(), ['document_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoices()
    {
        return $this->hasMany(Invoice::className(), ['basis_document_type_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPackingLists()
    {
        return $this->hasMany(PackingList::className(), ['basis_document_type_id' => 'id']);
    }
}
