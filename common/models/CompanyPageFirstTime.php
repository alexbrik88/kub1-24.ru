<?php

namespace common\models;

use Yii;
use yii\base\Action;

/**
 * This is the model class for table "company_page_first_time".
 *
 * Date and time the company first visited the page
 *
 * @property int $id
 * @property int $company_id
 * @property string $action_unique_id
 * @property int $created_date
 * @property int $created_at
 *
 * @property Company $company
 */
class CompanyPageFirstTime extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'company_page_first_time';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'action_unique_id' => 'Action Unique ID',
            'created_date' => 'Created Date',
            'created_at' => 'Created At',
        ];
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * Check if this is the first visit for this company and this action
     *
     * @param  $company
     * @param  $action
     * @return boolean
     */
    public static function isFirst(Company $company, Action $action)
    {
        if (self::find()->where([
            'company_id' => $company->id,
            'action_unique_id' => $action->getUniqueId(),
        ])->exists()) {
            return false;
        }

        $model = new CompanyPageFirstTime([
            'company_id' => $company->id,
            'action_unique_id' => $action->getUniqueId(),
            'created_date' => date('Y-m-d'),
            'created_at' => time(),
        ]);

        $model->save(false);

        return true;
    }
}
