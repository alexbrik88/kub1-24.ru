<?php

namespace common\models\api;

use common\components\date\DatePickerFormatBehavior;
use common\components\validators\PhoneValidator;
use Yii;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "api_partners".
 *
 * @property integer $id
 * @property string $partner_name
 * @property string $partner_site
 * @property string $api_key
 * @property string $start_date
 * @property string $end_date
 * @property string $contract
 * @property string $contact_person_fio
 * @property string $phone
 * @property string $email
 * @property integer $is_blocked
 */
class ApiPartners extends \yii\db\ActiveRecord
{
    /**
     *
     */
    const BLOCKED = 1;
    /**
     *
     */
    const UNBLOCKED = 0;

    /**
     * @var UploadedFile
     */
    public $contractFile;

    /**
     * @var null
     */
    public $generatedApiKey = null;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'api_partners';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => DatePickerFormatBehavior::className(),
                'attributes' => [
                    'start_date',
                    'end_date',
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['email'], 'trim'],
            [['start_date', 'partner_name', 'partner_site', 'contact_person_fio', 'phone', 'email'], 'required'],
            [['start_date', 'end_date'], 'safe'],
            [['is_blocked'], 'integer'],
            [['partner_name', 'partner_site', 'api_key', 'contract', 'contact_person_fio', 'generatedApiKey'], 'string', 'max' => 255],
            [['phone'], PhoneValidator::className()],
            [['email'], 'email'],
            [['contractFile'], 'file'],
        ];
    }

    /**
     * @return string
     * @throws \yii\base\Exception
     */
    public function getUploadPath()
    {
        $filePath = DIRECTORY_SEPARATOR . $this->tableName() . DIRECTORY_SEPARATOR . $this->id;
        $fullPath = Yii::getAlias('@common') . DIRECTORY_SEPARATOR . 'uploads' . $filePath;

        if (!file_exists($fullPath)) {
            FileHelper::createDirectory($fullPath);
        }

        return $fullPath;
    }

    /**
     * @return bool
     */
    public function _load()
    {
        if ($this->load(Yii::$app->request->post())) {
            $this->api_key = $this->generatedApiKey;
            $this->contractFile = UploadedFile::getInstance($this, 'contractFile');

            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function _save()
    {
        if ($this->save()) {
            if ($this->contractFile) {
                if ($this->contract) {
                    $oldLogo = $this->getUploadPath() . $this->contract;
                    if (file_exists($oldLogo)) {
                        $this->contract = null;
                        unlink($oldLogo);
                    }
                }
                $basePath = $this->getUploadPath();
                $fileName = DIRECTORY_SEPARATOR . $this->contractFile->name;
                $imagePath = $basePath . $fileName;
                if ($this->contractFile->saveAs($imagePath)) {
                    $this->contract = $fileName;
                }
            }

            return $this->save(true, ['contract']);
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'partner_name' => 'Партнер',
            'partner_site' => 'Сайт',
            'api_key' => 'API',
            'start_date' => 'Дата начала',
            'end_date' => 'Дата завершения',
            'contract' => 'Договор',
            'contact_person_fio' => 'Контактное лицо (ФИО)',
            'phone' => 'Телефон',
            'email' => 'Email',
            'is_blocked' => 'Is Blocked',
        ];
    }

    /**
     * @return yii\db\ActiveQuery
     */
    public static function findByApiKey($key)
    {
        return ApiPartners::find()->andWhere(['and',
            ['api_key' => $key],
            ['not', ['api_key' => null]],
            ['<=', 'start_date', date('Y-m-d')],
            ['or',
                ['end_date' => null],
                ['>=', 'end_date', date('Y-m-d')],
            ],
            ['is_blocked' => ApiPartners::UNBLOCKED],
        ]);
    }
}
