<?php

namespace common\models\ofd;

use Yii;
use common\models\Company;
use common\models\employee\Employee;
use frontend\models\Documents;
use frontend\models\log\Log;
use frontend\models\log\ILogMessage;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "ofd_statement_upload".
 *
 * @property int $id
 * @property int $company_id
 * @property int $ofd_id
 * @property int|null $store_id
 * @property int|null $kkt_id
 * @property int $source
 * @property string|null $period_from
 * @property string|null $period_till
 * @property int $created_by
 * @property int $created_at
 * @property int $saved_count
 *
 * @property Company $company
 * @property Employee $createdBy
 * @property Ofd $ofd
 */
class OfdStatementUpload extends \yii\db\ActiveRecord implements ILogMessage
{
    const SOURCE_FILE = 1;
    const SOURCE_OFD = 2;
    const SOURCE_OFD_AUTO = 3;
    const SOURCE_DEFAULT = self::SOURCE_OFD;

    public static $sources = [
        self::SOURCE_FILE,
        self::SOURCE_OFD,
        self::SOURCE_OFD_AUTO,
    ];

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
            ],
            [
                'class' => BlameableBehavior::className(),
                'updatedByAttribute' => false,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ofd_statement_upload';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'ofd_id' => 'Ofd ID',
            'store_id' => 'Store ID',
            'kkt_id' => 'Kkt ID',
            'source' => 'Source',
            'period_from' => 'Period From',
            'period_till' => 'Period Till',
            'created_by' => 'Created By',
            'created_at' => 'Created At',
            'saved_count' => 'Saved Count',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->getCreatedBy();
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * Gets query for [[CreatedBy]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(Employee::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->getCreatedBy();
    }

    /**
     * Gets query for [[Ofd]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOfd()
    {
        return $this->hasOne(Ofd::className(), ['id' => 'ofd_id']);
    }

    /**
     * Gets query for [[OfdKkt]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOfdKkt()
    {
        return $this->hasOne(OfdKkt::className(), ['id' => 'kkt_id']);
    }

    /**
     * Gets query for [[OfdStore]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOfdStore()
    {
        return $this->hasOne(OfdStore::className(), ['id' => 'store_id']);
    }

    /**
     * Get formatted log message from Log model
     * @param Log $log
     * @return string
     */
    public function getLogMessage(Log $log)
    {
        return "Чеки из {$this->ofd->name} загружены";
    }

    /**
     * Returns array for set color and icon. Used on displaying log messages.
     * @example: ['label-danger', 'fa fa-file-text-o']
     * @param Log $log
     * @return array
     */
    public function getLogIcon(Log $log)
    {
        return Documents::getLogIcon($log);
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if (is_a(Yii::$app,'yii\console\Application')) {
                $this->created_by = $this->company->employeeChief->id;
            }
            if (!in_array($this->source, self::$sources)) {
                $this->source = self::SOURCE_DEFAULT;
            }

            return true;
        }

        return false;
    }
}
