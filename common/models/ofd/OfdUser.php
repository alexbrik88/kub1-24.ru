<?php

namespace common\models\ofd;

use common\models\Company;
use common\models\employee\Employee;
use Yii;
use yii\helpers\Json;

/**
 * This is the model class for table "ofd_user".
 *
 * @property int $employee_id
 * @property int $company_id
 * @property int $ofd_id
 * @property string|null $auth_key
 * @property int $created_at
 * @property int $updated_at
 * @property string $user_uid
 * @property string $company_uid
 * @property string|null $_data
 */
class OfdUser extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ofd_user';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'employee_id' => 'Employee ID',
            'company_id' => 'Company ID',
            'ofd_id' => 'Ofd ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            '_data' => 'Data',
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function setData(array $value)
    {
        $this->_data = Json::encode($value);
    }

    /**
     * {@inheritdoc}
     */
    public function getData()
    {
        return Json::decode($this->_data) ?: [];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                do {
                    $this->auth_key = Yii::$app->security->generateRandomString().'_'.time();
                } while (self::find()->where(['auth_key' => $this->auth_key])->exists());

                if (!$this->created_at) {
                    $this->created_at = time();
                }
                if (!$this->updated_at) {
                    $this->updated_at = $this->created_at;
                }
            } else {
                $this->updated_at = time();
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfd()
    {
        return $this->hasOne(Ofd::className(), ['id' => 'ofd_id']);
    }

    /**
     * {@inheritdoc}
     */
    public function setDataParam(string $param, $value)
    {
        $data = $this->getData();
        $data[$param] = $value;
        $this->setData($data);
    }

    /**
     * {@inheritdoc}
     */
    public function getDataParam(string $param, $defaultValue = null)
    {
        return ArrayHelper::getValue($this->getData(), $param, $defaultValue);
    }
}
