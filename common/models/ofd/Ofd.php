<?php

namespace common\models\ofd;

use common\models\Company;
use common\models\employee\Employee;
use Yii;

/**
 * This is the model class for table "ofd".
 *
 * @property int $id
 * @property string $alias
 * @property string $name
 * @property string $url
 * @property int $is_active
 * @property string|null $logo
 */
class Ofd extends \yii\db\ActiveRecord
{
    const ASTRAL = 1;
    const INITPRO = 2;
    const KONTUR = 3;
    const OFDRU = 4;
    const PERVIY = 5;
    const PLATFORMA = 6;
    const SBIS = 7;
    const TAXCOM = 8;
    const YANDEX = 9;
    const YARUS = 10;

    const AUTOLOAD_MODE_NO = 0;
    const AUTOLOAD_MODE_DAILY = 1;

    protected $_helper;
    protected static $_active;

    public static $receiptAutoloadJobClass = [
        self::PLATFORMA => 'frontend\modules\ofd\modules\platforma\components\ReceiptAutoloadJob',
        self::TAXCOM => 'frontend\modules\ofd\modules\taxcom\components\ReceiptAutoloadJob',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ofd';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['alias', 'name', 'url'], 'required'],
            [['is_active'], 'integer'],
            [['alias', 'name', 'url', 'logo'], 'string', 'max' => 255],
            [['alias'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'alias' => 'Alias',
            'name' => 'Name',
            'url' => 'Url',
            'is_active' => 'Is Active',
            'logo' => 'Logo',
        ];
    }

    /**
     * @return OfdHelper
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function getHelper(Employee $employee, Company $company)
    {
        if ($this->_helper === null) {
            $class = "frontend\\modules\\ofd\\modules\\{$this->alias}\\components\\Helper";
            $this->_helper = new $class($this, $employee, $company);
        }

        return $this->_helper;
    }

    /**
     * @return array
     */
    public static function active()
    {
        if (self::$_active === null) {
            self::$_active = self::find()->andWhere([
                'is_active' => true,
            ])->orderBy([
                'name' => SORT_ASC,
            ])->all();
        }

        return self::$_active;
    }
}
