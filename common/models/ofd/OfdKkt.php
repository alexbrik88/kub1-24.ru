<?php

namespace common\models\ofd;

use Yii;

/**
 * This is the model class for table "ofd_kkt".
 *
 * @property int $id
 * @property string $uid
 * @property string $store_uid
 * @property int $company_id
 * @property int $ofd_id
 * @property string $name
 * @property string|null $address
 * @property string $registration_number
 * @property string $fiscal_number
 * @property string $other_fiscal_numbers
 */
class OfdKkt extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ofd_kkt';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uid' => 'Uid',
            'store_uid' => 'Store Uid',
            'company_id' => 'Company ID',
            'ofd_id' => 'Ofd ID',
            'name' => 'Наименование',
            'address' => 'Адрес установки ККТ',
            'registration_number' => 'Регистрационный номер ККТ',
            'format_fd' => 'Формат ФД',
            'model' => 'Модель ККТ',
            'factory_number' => 'Заводской номер ККТ',
            'registration_status' => 'Статус регистрации в ФНС',
            'activated_at' => 'Добавление в ЛК клиента ОФД',
            'place' => 'Место расчетов',
            'fiscal_number' => 'Номер фискального накопителя'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfdImportKktCashbox()
    {
        return $this->hasOne(OfdImportKktCashbox::className(), [
            'company_id' => 'company_id',
            'ofd_kkt_id' => 'id',
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStore()
    {
        return $this->hasOne(OfdStore::className(), [
            'id' => 'store_id',
            'company_id' => 'company_id',
            'ofd_id' => 'ofd_id',
        ]);
    }
}
