<?php

namespace common\models\ofd;

use Yii;
use common\models\Company;
use common\models\cash\Cashbox;


/**
 * This is the model class for table "ofd_import_config".
 *
 * @property int $company_id
 * @property int $import_config_id
 */
class OfdImportConfig extends \yii\db\ActiveRecord
{
    const NO_CASHBOX = 1;
    const STORE_CASHBOX = 2;
    const KKT_CASHBOX = 3;
    const GENERAL_CASHBOX = 4;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ofd_import_config';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'company_id' => 'Company ID',
            'import_config_id' => 'Ofd Import Config',
        ];
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return Cashbox|null
     */
    public function cashbox($store = null, $kkt = null)
    {
        $method = sprintf('_cashbox_%s', $this->import_config_id);

        if (method_exists($this, $method)) {
            return $this->{$method}($store, $kkt);
        }

        return null;
    }

    private function _cashbox_1()
    {
        return null;
    }

    private function _cashbox_2($store = null, $kkt = null)
    {
        if ($store) {
            $ofdImportStoreCashbox = OfdImportStoreCashbox::findOne([
                'company_id' => $this->company_id,
                'ofd_store_id' => $store->id,
            ]);

            return $ofdImportStoreCashbox ? $ofdImportStoreCashbox->cashbox : null;
        }

        return null;
    }

    private function _cashbox_3($store = null, $kkt = null)
    {
        if ($kkt) {
            $ofdImportKktCashbox = OfdImportKktCashbox::findOne([
                'company_id' => $this->company_id,
                'ofd_kkt_id' => $kkt->id,
            ]);

            return $ofdImportKktCashbox ? $ofdImportKktCashbox->cashbox : null;
        }

        return null;
    }

    private function _cashbox_4($store = null, $kkt = null)
    {
        $ofdImportGeneralCashbox = OfdImportGeneralCashbox::findOne([
            'company_id' => $this->company_id,
        ]);

        return $ofdImportGeneralCashbox ? $ofdImportGeneralCashbox->cashbox : null;
    }
}
