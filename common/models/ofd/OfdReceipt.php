<?php

namespace common\models\ofd;

use Yii;
use common\models\cash\CashFlowsBase;

/**
 * This is the model class for table "ofd_receipt".
 *
 * @property int $id
 * @property string $date_time 1012 дата, время
 * @property string $document_number 1040 номер фискального документа
 * @property string $kkt_registration_number 1037 регистрационный номер ККТ
 * @property string $kkt_uid Идентификатор смарт-терминала в ОФД
 * @property int $company_id
 * @property int $ofd_id
 * @property int $operation_type_id
 * @property int $shift_number 1038 Номер смены
 * @property int $number 1042 Номер чека за смену
 * @property string $address 1009 Адрес расчетов
 * @property string $place 1187 Место расчетов
 * @property int $total_sum 1020 Сумма расчета
 * @property int $cash_sum 1031 Сумма по чеку наличными
 * @property int $cashless_sum 1081 Сумма по чеку безналичными
 * @property int $nds_no 1105 Сумма расчета по чеку без НДС
 * @property int $nds_0 1104 Сумма НДС чека по ставке 0%
 * @property int $nds_10 1103 Сумма НДС чека по ставке 10%
 * @property int $nds_18 Сумма НДС чека по ставке 18%
 * @property int $nds_20 1102 Сумма НДС чека по ставке 20%
 * @property string|null $operator_name 1021 Кассир
 * @property string|null $operator_inn 1203 ИНН кассира
 * @property string|null $fiscal_tag 1077 Фискальный признак документа
 * @property string|null $contractor_name 1227 Наименование покупателя или клиента
 * @property string|null $contractor_inn 1228 ИНН покупателя или клиента
 * @property int|null $created_at
 *
 * @property-read OfdReceiptItem $items
 */
class OfdReceipt extends \yii\db\ActiveRecord
{
    const PAY_CASH = 1;
    const PAY_CASHLESS = 2;
    const PAY_BOTH = 3;

    /**
     * @var string
     */
    public $printablePrefix = 'Чек';
    public $shortPrefix = 'Чек';

    public static $payTypeList = [
        self::PAY_CASH => 'Наличные',
        self::PAY_CASHLESS => 'Безналичные',
        self::PAY_BOTH => 'Смешанный',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ofd_receipt';
    }

    /**
     * {@inheritdoc}
     */
    public function getFLowType()
    {
        return OfdOperationType::flowType($this->operation_type_id);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_time' => 'Дата',
            'document_number' => 'Номер чека',
            'kkt_registration_number' => 'Регистрационный номер ККТ',
            'kkt_uid' => 'Идентификатор ККТ',
            'company_id' => 'Company ID',
            'ofd_id' => 'ofd ID',
            'operation_type_id' => 'Тип операции',
            'shift_number' => 'Номер смены',
            'number' => 'Номер продажи',
            'address' => 'Адрес',
            'place' => 'Место расчета',
            'total_sum' => 'Сумма чека',
            'cash_sum' => 'Сумма наличными',
            'cashless_sum' => 'Сумма безналичными',
            'nds_no' => 'Nds No',
            'nds_0' => 'Nds 0',
            'nds_10' => 'Nds 10',
            'nds_18' => 'Nds 18',
            'nds_20' => 'Nds 20',
            'operator_name' => 'Operator Name',
            'operator_inn' => 'Operator Inn',
            'fiscal_tag' => 'Fiscal Tag',
            'contractor_name' => 'Contractor Name',
            'contractor_inn' => 'Contractor Inn',
            'created_at' => 'Created At',
            'payment_type_id' => 'Тип оплаты',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKkt()
    {
        return $this->hasOne(OfdKkt::className(), [
            'id' => 'kkt_id',
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStore()
    {
        return $this->hasOne(OfdStore::className(), [
            'id' => 'store_id',
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(OfdReceiptItem::className(), [
            'receipt_id' => 'id',
            'date_time' => 'date_time',
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperationType()
    {
        return $this->hasOne(OfdOperationType::className(), [
            'id' => 'operation_type_id',
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOfd()
    {
        return $this->hasOne(Ofd::className(), [
            'id' => 'ofd_id',
        ]);
    }

    /**
     * @return integer
     */
    public function getPayTypeId()
    {
        $hasCash = $this->cash_sum > 0;
        $hasCashless = $this->cashless_sum > 0;

        if ($hasCash && $hasCashless) {
            return self::PAY_BOTH;
        } elseif ($hasCash) {
            return self::PAY_CASH;
        } elseif ($hasCashless) {
            return self::PAY_CASHLESS;
        }

        return null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayTypeLabel()
    {
        return self::$payTypeList[$this->getPayTypeId()] ?? '';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFullNumber()
    {
        return $this->document_number;
    }
}
