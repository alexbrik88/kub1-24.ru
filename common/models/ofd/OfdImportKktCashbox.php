<?php

namespace common\models\ofd;

use Yii;
use common\models\Company;
use common\models\cash\Cashbox;

/**
 * This is the model class for table "ofd_import_kkt_cashbox".
 *
 * @property int $company_id
 * @property int $ofd_kkt_id
 * @property int $cashbox_id
 */
class OfdImportKktCashbox extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ofd_import_kkt_cashbox';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'company_id' => 'Company ID',
            'ofd_kkt_id' => 'Ofd Kkt ID',
            'cashbox_id' => 'Cashbox ID',
        ];
    }

    /**
     * Gets query for [[Cashbox]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCashbox()
    {
        return $this->hasOne(Cashbox::className(), ['id' => 'cashbox_id']);
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
}
