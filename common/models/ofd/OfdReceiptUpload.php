<?php

namespace common\models\ofd;

use Yii;

/**
 * This is the model class for table "ofd_receipt_upload".
 *
 * @property int $id
 * @property int $kkt_id
 * @property int $created_at
 * @property string|null $period_from
 * @property string|null $period_till
 * @property int|null $receipt_total_sum
 * @property int|null $receipt_total_count
 * @property int|null $receipt_insert_count
 * @property string|null $last_receipt_number
 * @property string|null $last_receipt_time
 *
 * @property OfdKkt $kkt
 */
class OfdReceiptUpload extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ofd_receipt_upload';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kkt_id' => 'Kkt ID',
            'created_at' => 'Created At',
            'period_from' => 'Period From',
            'period_till' => 'Period Till',
            'receipt_total_sum' => 'Receipt Total Sum',
            'receipt_total_count' => 'Receipt Total Count',
            'receipt_insert_count' => 'Receipt Insert Count',
            'last_receipt_number' => 'Last Receipt Number',
            'last_receipt_time' => 'Last Receipt Time',
        ];
    }

    /**
     * Gets query for [[Kkt]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getKkt()
    {
        return $this->hasOne(OfdKkt::className(), ['id' => 'kkt_id']);
    }
}
