<?php

namespace common\models\ofd;

use Yii;
use common\models\cash\CashFlowsBase;

/**
 * This is the model class for table "ofd_operation_type".
 *
 * @property int $id
 * @property string $name
 */
class OfdOperationType extends \yii\db\ActiveRecord
{
    /**
     * Признак расчёта:
     *   1 — Приход;            - приход
     *   2 — Возврат прихода;   - расход
     *   3 — Расход;            - расход
     *   4 — Возврат расхода.   - приход
     */
    const INCOME = 1;
    const INCOME_RETUSN = 2;
    const EXPENSE = 3;
    const EXPENSE_RETUSN = 4;

    public static $typeList = [
        self::INCOME,
        self::INCOME_RETUSN,
        self::EXPENSE,
        self::EXPENSE_RETUSN,
    ];

    public static $incomes = [
        self::INCOME,
        self::EXPENSE_RETUSN,
    ];

    public static $expenses = [
        self::EXPENSE,
        self::INCOME_RETUSN,
    ];

    /**
     * @var int $operation_type_id Признак расчёта
     */
    public static function flowType($operation_type_id)
    {
        switch ($operation_type_id) {
            case self::INCOME:
            case self::EXPENSE_RETUSN:
                return CashFlowsBase::FLOW_TYPE_INCOME;
                break;

            case self::EXPENSE:
            case self::INCOME_RETUSN:
                return CashFlowsBase::FLOW_TYPE_EXPENSE;
                break;

            default:
                return null;
                break;
        }
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ofd_operation_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
}
