<?php

namespace common\models\ofd;

use Yii;
use common\models\Company;
use common\models\companyStructure\SalePoint;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "ofd_store".
 *
 * @property int $id
 * @property string $uid
 * @property string $parent_uid
 * @property int $company_id
 * @property int $ofd_id
 * @property string $name
 * @property string|null $address
 * @property int|null $created_at
 *
 * @property Company $company
 * @property Ofd $ofd
 * @property OfdKkt[] $kkts
 * @property SalePoint[] $salePoint
 */
class OfdStore extends \yii\db\ActiveRecord
{
    const TYPE_STORE = 1;
    const TYPE_SITE = 2;

    public static $typeList = [
        self::TYPE_STORE => 'Магазин',
        self::TYPE_SITE => 'Сайт',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ofd_store';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['local_name'], 'trim'],
            [['local_name', 'store_type_id'], 'required'],
            [['store_type_id'], 'in', 'range' => array_keys(self::$typeList)],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uid' => 'Uid',
            'parent_uid' => 'Parent Uid',
            'company_id' => 'Company ID',
            'ofd_id' => 'Ofd ID',
            'name' => 'Name',
            'address' => 'Address',
            'created_at' => 'Created At',
            'local_name' => 'Название Точки продаж',
            'store_type_id' => 'Тип точки продаж',
            'storeTypeLabel' => 'Тип точки продаж',
        ];
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * Gets query for [[Ofd]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOfd()
    {
        return $this->hasOne(Ofd::className(), ['id' => 'ofd_id']);
    }

    /**
     * Gets query for [[Ofd]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOfdImportStoreCashbox()
    {
        return $this->hasOne(OfdImportStoreCashbox::className(), [
            'company_id' => 'company_id',
            'ofd_store_id' => 'id',
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKkts()
    {
        return $this->hasMany(OfdKkt::className(), [
            'store_id' => 'id',
            'company_id' => 'company_id',
            'ofd_id' => 'ofd_id',
        ]);
    }

    /**
     * Gets query for [[SalePoint]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSalePoint()
    {
        return $this->hasOne(SalePoint::className(), [
            'company_id' => 'company_id',
            'ofd_store_uid' => 'uid',
        ]);
    }

    /**
     * @return string
     */
    public function getStoreTypeLabel()
    {
        return ArrayHelper::getValue(self::$typeList, $this->store_type_id, '');
    }
}
