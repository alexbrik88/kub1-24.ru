<?php

namespace common\models\ofd;

use Yii;

/**
 * This is the model class for table "ofd_receipt_item".
 *
 * @property int $id
 * @property string $date_time 1012 дата, время
 * @property int $receipt_id
 * @property int $sum
 * @property int $price
 * @property string $quantity
 * @property int $nds_no
 * @property int $nds_0
 * @property int $nds_10
 * @property int $nds_18
 * @property int $nds_20
 * @property string|null $barcode
 * @property string|null $name
 * @property int|null $created_at
 */
class OfdReceiptItem extends \yii\db\ActiveRecord
{
    /**
     * `product_type` values
     */
    const TYPE_PRODUCT = 1;
    const TYPE_WORK = 3;
    const TYPE_SERVICE = 4;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ofd_receipt_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'date_time' => 'Date Time',
            'receipt_id' => 'Receipt ID',
            'sum' => 'Sum',
            'price' => 'Price',
            'quantity' => 'Quantity',
            'nds_no' => 'Nds No',
            'nds_0' => 'Nds 0',
            'nds_10' => 'Nds 10',
            'nds_18' => 'Nds 18',
            'nds_20' => 'Nds 20',
            'barcode' => 'Barcode',
            'name' => 'Name',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReceipt()
    {
        return $this->hasOne(OfdReceipt::className(), [
            'id' => 'receipt_id',
            'date_time' => 'date_time',
        ]);
    }
}
