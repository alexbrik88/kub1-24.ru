<?php

namespace common\models;

use Yii;
use common\models\employee\Employee;

/**
 * This is the model class for table "chat_volume".
 *
 * @property integer $id
 * @property integer $company_id
 * @property integer $from_employee_id
 * @property integer $to_employee_id
 * @property integer $has_volume
 *
 * @property Company $company
 * @property Employee $fromEmployee
 * @property Employee $toEmployee
 */
class ChatVolume extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'chat_volume';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'from_employee_id', 'to_employee_id'], 'required'],
            [['company_id', 'from_employee_id'], 'integer'],
            [['to_employee_id'], 'string'],
            [['has_volume'], 'boolean'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['from_employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['from_employee_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'from_employee_id' => 'From Employee ID',
            'to_employee_id' => 'To Employee ID',
            'has_volume' => 'Has Volume',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFromEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'from_employee_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getToEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'to_employee_id']);
    }
}
