<?php

namespace common\models\balance;

use Carbon\Carbon;
use common\components\date\DateHelper;
use common\components\excel\Excel;
use common\components\helpers\Month;
use common\models\Company;
use frontend\modules\reference\models\BalanceArticlesCategories;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "balance_article".
 *
 * @property int $id
 * @property int $company_id
 * @property int $created_at
 * @property int $updated_at
 * @property int $type
 * @property int $status
 * @property string $name
 * @property int $category
 * @property int $subcategory
 * @property int $useful_life_in_month
 * @property int $count
 * @property string $purchased_at
 * @property string $sold_at
 * @property string $written_off_at
 * @property string $amount
 * @property string $description
 *
 * @property Company $company
 */
class BalanceArticle extends \yii\db\ActiveRecord
{
    const TYPE_FIXED_ASSERTS = 0;
    const TYPE_INTANGIBLE_ASSETS = 1;

    const STATUS_ON_BALANCE = 0;
    const STATUS_WRITTEN_OFF = 1;
    const STATUS_SOLD = 2;

    const STATUS_MAP = [
        self::STATUS_ON_BALANCE => 'На балансе',
        self::STATUS_WRITTEN_OFF => 'Списано',
        self::STATUS_SOLD => 'Продано',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'balance_article';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            ['class' => TimestampBehavior::class],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'type' => 'Type',
            'status' => 'Status',
            'name' => 'Name',
            'category' => 'Category',
            'subcategory' => 'Subcategory',
            'useful_life_in_month' => 'Useful Life In Month',
            'count' => 'Count',
            'purchased_at' => 'Purchased At',
            'sold_at' => 'Sold At',
            'written_off_at' => 'Written Of At',
            'amount' => 'Amount',
            'description' => 'Description',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    public function calcMonthInUse(): int
    {
        $calcFromDate = Carbon::parse($this->purchased_at)->endOfMonth()->addDay();

        if ($this->written_off_at)
            $calcToDate = Carbon::parse($this->written_off_at)->endOfMonth();
        elseif ($this->sold_at)
            $calcToDate = Carbon::parse($this->sold_at)->endOfMonth();
        else
            $calcToDate = Carbon::now();

        return round($calcToDate->diffInMonths($calcFromDate));
    }

    public function calcMonthLeft(): int
    {
        $monthLeft = $this->useful_life_in_month - $this->calcMonthInUse();
        return ($monthLeft > 0) ? $monthLeft : 0;
    }

    public function getAmortizationScheme(): array
    {
        $amountPerMonth = round($this->amount / ($this->useful_life_in_month ?: 9E99));
        $monthsTotal = $this->useful_life_in_month;
        $calcFromDate = Carbon::parse($this->purchased_at)->endOfMonth()->addDay();
        $currDate = (clone $calcFromDate)->endOfMonth();
        $writeOffDate = ($this->written_off_at)
            ? Carbon::parse($this->written_off_at)->startOfMonth()
            : null;
        $soldDate = ($this->sold_at)
            ? Carbon::parse($this->sold_at)->startOfMonth()
            : null;
        $nowDate = Carbon::now();

        $ret = [];
        while (true) {

            $currDate->endOfMonth();
            if ($currDate->gt($nowDate))
                break;

            $ym = $currDate->format('Ym');
            $ret[$ym] = ($monthsTotal > 1)
                ? $amountPerMonth
                : $this->amount - ($amountPerMonth * ($this->useful_life_in_month - 1));

            $currDate->addDay();
            $monthsTotal--;

            if ($monthsTotal <= 0) {
                break;
            }
            if ($writeOffDate && $currDate->gt($writeOffDate)
                || $soldDate && $currDate->gt($soldDate)) {
                $ret[$ym] = $this->amount - ($amountPerMonth * ($this->useful_life_in_month - $monthsTotal - 1));
                break;
            }
        }

        return $ret;
    }

    public function calcAmortization()
    {
        $monthInUse = min($this->useful_life_in_month, $this->calcMonthInUse());
        $amountPerMonth = round($this->amount / ($this->useful_life_in_month ?: 9E99));

        // при продаже или списании ОС или НМА в столбце "Амортизация по..." должна отражаться стоимость приобретения
        if ($this->written_off_at || $this->sold_at)
            return $this->amount;

        // Амортизация за последний месяц полезного использования = Стоимость приобретения - Амортизация по месяц предшествующий месяцу полезного использования
        if ($this->calcMonthLeft() === 1)
            return $amountPerMonth * ($this->useful_life_in_month - 1);

        return min($this->amount, $monthInUse * $amountPerMonth);
    }

    public function getWriteOffDate(): string
    {
        if ($this->written_off_at)
            $lastDate = Carbon::parse($this->written_off_at);
        elseif ($this->sold_at)
            $lastDate = Carbon::parse($this->sold_at);
        else
            $lastDate = (Carbon::parse($this->purchased_at))->addMonths($this->useful_life_in_month);

        return $lastDate->format(DateHelper::FORMAT_USER_DATE);
    }

    public function getRemainingAmountOnDate(string $onDate): int
    {
        $remaining = $this->amount;
        $onYm = ($_ymd = explode('-', $onDate)) ? ($_ymd[0].$_ymd[1]) : null;
        foreach ($this->getAmortizationScheme() as $ym => $amount)
            if ($ym < $onYm)
                $remaining -= $amount;

        return $remaining;
    }

    /**
     * @param $period
     * @param $models
     */
    public static function generateXlsTable($type, $period, $models)
    {
        $excel = new Excel();

        $title = ($type == BalanceArticle::TYPE_FIXED_ASSERTS)
            ? 'Основные_средства'
            : 'Нематериальные_активы';
        $fileName = $title . '_' . DateHelper::format($period['from'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) .
            '-' . DateHelper::format($period['to'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
        $currDate = Month::$monthFullRU[date('n')].' '.date('Y');
        
        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $models,
            'title' => str_replace($title, '_', ' '),
            'headers' => [
                'purchased_at' => 'Дата приобретения',
                'name' => 'Наименование',
                'count' => 'Кол-во',
                'category' => 'Вид',
                'amount' => 'Стоимость приобретения',
                'depreciation_for_today' => "Амортизация по {$currDate}",
                'cost_for_today' => "Стоимость на {$currDate}",
                'useful_life_in_month' => 'Срок полезного использования (мес.)',
                'month_in_use' => 'Кол-во месяцев использования',
                'month_left' => 'Осталось месяцев',
                'write_off_date' => 'Дата полного списания',
                'status' => 'Статус',
            ],            
            'columns' => [
                [
                    'attribute' => 'purchased_at',
                    'value' => function (BalanceArticle $model) {
                        return DateHelper::format($model->purchased_at, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                    },
                ],
                [
                    'attribute' => 'name',
                    'value' => function (BalanceArticle $model) {
                        return $model->name;
                    },
                ],
                [
                    'attribute' => 'count',
                    'styleArray' => ['numberformat' => ['code' => '0']],
                    'value' => function (BalanceArticle $model) {
                        return $model->count;
                    },
                ],
                [
                    'attribute' => 'category',
                    'value' => function (BalanceArticle $model) {
                        return BalanceArticlesCategories::MAP[$model->category];
                    },
                ],
                [
                    'attribute' => 'amount',
                    'styleArray' => ['numberformat' => ['code' => '# ##0.00']],
                    'dataType' => 'n',
                    'value' => function (BalanceArticle $model) {
                        return bcdiv($model->amount, 100, 2);
                    },
                ],
                [
                    'attribute' => 'depreciation_for_today',
                    'styleArray' => ['numberformat' => ['code' => '# ##0.00']],
                    'dataType' => 'n',
                    'value' => function (BalanceArticle $model) {
                        return bcdiv($model->calcAmortization(), 100, 2);
                    },
                ],
                [
                    'attribute' => 'cost_for_today',
                    'styleArray' => ['numberformat' => ['code' => '# ##0.00']],
                    'dataType' => 'n',
                    'value' => function (BalanceArticle $model) {
                        return bcdiv(max(0, $model->amount - $model->calcAmortization()), 100, 2);
                    },
                ],
                [
                    'attribute' => 'useful_life_in_month',
                    'styleArray' => ['numberformat' => ['code' => '0']],
                    'value' => function (BalanceArticle $model) {
                        return $model->useful_life_in_month;
                    },
                ],
                [
                    'attribute' => 'month_in_use',
                    'styleArray' => ['numberformat' => ['code' => '0']],
                    'value' => function (BalanceArticle $model) {
                        return $model->calcMonthInUse();
                    },
                ],
                [
                    'attribute' => 'month_left',
                    'styleArray' => ['numberformat' => ['code' => '0']],
                    'value' => function (BalanceArticle $model) {
                        $monthLeft = $model->useful_life_in_month >= $model->calcMonthInUse();
                        return ($monthLeft > 0) ? (int)$monthLeft : 0;
                    },
                ],
                [
                    'attribute' => 'write_off_date',
                    'value' => function (BalanceArticle $model) {
                        return $model->getWriteOffDate();
                    },
                ],
                [
                    'attribute' => 'status',
                    'value' => function (BalanceArticle $model) {
                        return BalanceArticle::STATUS_MAP[$model->status];
                    },
                ],
            ],
            'format' => 'Xlsx',
            'fileName' => $fileName,
        ]);
    }
}
