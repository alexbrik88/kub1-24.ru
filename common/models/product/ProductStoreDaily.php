<?php

namespace common\models\product;

use common\components\date\DateHelper;
use common\models\Company;
use Exception;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\web\ForbiddenHttpException;

/**
 * Class ProductStoreDaily
 * @package common\models\product
 *
 * @property integer $product_id
 * @property integer $store_id
 * @property string $date
 * @property integer $initial_quantity
 * @property integer $irreducible_quantity
 * @property integer $quantity
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Product $product
 * @property Store $store
 */
class ProductStoreDaily extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_store_daily';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['quantity', 'initial_quantity', 'irreducible_quantity'], 'number'],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['store_id'], 'exist', 'skipOnError' => true, 'targetClass' => Store::className(), 'targetAttribute' => ['store_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'Product ID',
            'store_id' => 'Store ID',
            'quantity' => 'Количество',
            'initial_quantity' => 'Количество на начало дня',
            'irreducible_quantity' => 'Нераспределенное количетсво',
            'created_at' => 'Создано',
            'updated_at' => 'Обновлено',
        ];
    }

    public function beforeSave($insert)
    {
        $this->date = $this->date ?: date(DateHelper::FORMAT_DATE);
        return parent::beforeSave($insert);
    }

    /**
     * @return ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getStore()
    {
        return $this->hasOne(Store::className(), ['id' => 'store_id']);
    }

    /**
     * @param $product_id
     * @param $store_id
     * @param Company $company
     * @return ProductStoreDaily|ActiveRecord
     * @throws \yii\base\Exception
     */
    public static function initial($product_id, $store_id, Company $company)
    {
        $productStoreDaily = self::getProductStoreDaily($product_id, $store_id, $company);

        if ($productStoreDaily && $productStoreDaily->date == date(DateHelper::FORMAT_DATE)) {
            return $productStoreDaily;
        }

        if ($productStoreDaily && $productStoreDaily->date !== date(DateHelper::FORMAT_DATE)) {
            $storeDaily = new static([
                'company_id' => $company->id,
                'product_id' => $product_id,
                'store_id' => $store_id,
                'initial_quantity' => $productStoreDaily->quantity,
                'irreducible_quantity' => $productStoreDaily->irreducible_quantity,
                'quantity' => $productStoreDaily->quantity
            ]);
        } elseif (!$productStoreDaily && $productStores = ProductStore::find()
            ->andWhere(['and',
                ['product_id' => $product_id],
                ['!=', 'quantity', new Expression(0)],
            ])
            ->all()) {

            foreach ($productStores as $productStore) {
                (new static([
                    'company_id' => $company->id,
                    'product_id' => $product_id,
                    'store_id' => $productStore->store_id,
                    'initial_quantity' => $productStore->quantity,
                    'quantity' => $productStore->quantity,
                    'irreducible_quantity' => $productStore->irreducible_quantity,
                ]))
                ->save();

                $productStoreDaily = self::getProductStoreDaily($product_id, $productStores[0]->store_id, $company);
            }
        }

        if (!$productStoreDaily) {
            $storeDaily = new static([
                'company_id' => $company->id,
                'product_id' => $product_id,
                'store_id' => $store_id,
                'irreducible_quantity' => 0,
                'initial_quantity' => 0,
                'quantity' => 0
            ]);
        }

        if (isset($storeDaily) && $storeDaily->save()) {
            return $storeDaily;
        }

        return $productStoreDaily;
    }

    /**
     * @param $product_id
     * @param $store_id
     * @param $company
     * @return ProductStoreDaily|ActiveRecord
     */
    public static function getProductStoreDaily($product_id, $store_id, $company) {
        return static::find()
            ->andWhere(['and',
                ['company_id' => $company->id],
                ['product_id' => $product_id],
                ['store_id' => $store_id],
            ])
            ->orderBy('date DESC')
            ->one();
    }

    /**
     * @param $amount
     * @return bool
     */
    public function subtract($amount)
    {
        try {
            $this->updateAttributes(['quantity' => ($this->quantity - $amount)]);
        } catch (Exception $e) {
            Yii::warning($e->getMessage());
            Yii::warning($e->getTraceAsString());
        }

        return true;
    }

    /**
     * @param $amount
     * @return bool
     */
    public function addition($amount)
    {
        try {
            $this->updateAttributes(['quantity' => ($this->quantity + $amount)]);
        } catch (Exception $e) {
            Yii::warning($e->getMessage());
            Yii::warning($e->getTraceAsString());
        }

        return true;
    }

}
