<?php

namespace common\models\product;

use Yii;

/**
 * This is the model class for table "price_list_order".
 *
 * @property integer $id
 * @property integer $price_list_id
 * @property integer $product_id
 * @property string $name
 * @property string $article
 * @property integer $product_group_id
 * @property integer $quantity
 * @property integer $product_unit_id
 * @property string $description
 * @property string $price
 * @property string $price_for_sell
 * @property int $production_type
 * @property int $sort
 *
 * @property PriceList $priceList
 * @property ProductGroup $productGroup
 * @property Product $product
 * @property ProductUnit $productUnit
 */
class PriceListOrder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'price_list_order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['price_list_id', 'product_id', 'name', 'price_for_sell'], 'required'],
            [['price_list_id', 'product_id', 'product_group_id', 'product_unit_id'], 'integer'],
            [['description'], 'string'],
            [['price', 'quantity', 'price_for_sell'], 'filter', 'filter' => function ($value) {
                return str_replace(",", ".", $value);
            }],
            [['name', 'article', 'price_for_sell', 'price', 'quantity'], 'string', 'max' => 255],
            [['price_list_id'], 'exist', 'skipOnError' => true, 'targetClass' => PriceList::className(), 'targetAttribute' => ['price_list_id' => 'id']],
            [['product_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductGroup::className(), 'targetAttribute' => ['product_group_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['product_unit_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductUnit::className(), 'targetAttribute' => ['product_unit_id' => 'id']],
            [['production_type'], 'in', 'range' => [
                Product::PRODUCTION_TYPE_GOODS,
                Product::PRODUCTION_TYPE_SERVICE,
            ]],
            [['sort'], 'integer']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'price_list_id' => 'Price List ID',
            'product_id' => 'Product ID',
            'name' => 'Name',
            'article' => 'Article',
            'product_group_id' => 'Product Group ID',
            'quantity' => 'Quantity',
            'product_unit_id' => 'Product Unit ID',
            'description' => 'Description',
            'price' => 'Price',
            'price_for_sell' => 'Price For Sell',
            'production_type' => 'Production Type'
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceList()
    {
        return $this->hasOne(PriceList::className(), ['id' => 'price_list_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductGroup()
    {
        return $this->hasOne(ProductGroup::className(), ['id' => 'product_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductUnit()
    {
        return $this->hasOne(ProductUnit::className(), ['id' => 'product_unit_id']);
    }

    public function getProductionType()
    {
        return $this->production_type;
    }
}
