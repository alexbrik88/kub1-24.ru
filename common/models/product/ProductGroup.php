<?php

namespace common\models\product;

use common\models\employee\Employee;
use common\models\store\StoreUser;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "product_unit".
 *
 * @property integer $id
 * @property string $title
 * @property string $code_okei
 * @property integer $production_type
 */
class ProductGroup extends \yii\db\ActiveRecord
{
    const WITHOUT = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'trim'],
            [['title'], 'filter', 'filter' => function ($value) {
                return mb_strtoupper(mb_substr($value, 0, 1)) . mb_substr($value, 1);
            }],
            [['title'], 'required'],
            [['title'], 'string', 'max' => 60],
            [
                ['title'], 'unique', 'filter' => [
                'and',
                [
                    'or',
                    ['company_id' => null],
                    ['company_id' => $this->company_id],
                ],
                [
                    'or',
                    ['production_type' => null],
                    ['production_type' => $this->production_type],
                ],
            ],
                'message' => 'Такая группа товара уже существует',
            ],
            [['production_type'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Наименование',
            'company_id' => 'Компания',
            'production_type' => 'Тип',
        ];
    }

    /**
     * @param  boolean $insert
     * @return boolean
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert) || $this->id == self::WITHOUT) {
            return false;
        }

        return true;
    }

    public function beforeDelete()
    {
        if (!parent::beforeDelete() || $this->id == self::WITHOUT || $this->getProducts()
                ->andWhere(['is_deleted' => false])
                ->exists()) {
            return false;
        }
        /* @var $product Product */
        foreach ($this->getProducts()->all() as $product) {
            $product->group_id = ProductGroup::WITHOUT;
            $product->save(true, ['group_id']);
        }

        return true;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['group_id' => 'id']);
    }

    /**
     * @param null $product_unit_id
     * @param int $productionType
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getGroups($product_unit_id = null, $productionType = Product::PRODUCTION_TYPE_GOODS)
    {
        $company = null;
        if (Yii::$app->user->isGuest) {
            $company = null;
        } elseif (Yii::$app->user->identity instanceof Employee) {
            $company = Yii::$app->user->identity->company->id;
        } elseif (Yii::$app->user->identity instanceof StoreUser) {
            $company = Yii::$app->user->identity->company ?
                Yii::$app->user->identity->company->id :
                Yii::$app->user->identity->currentKubContractor->company_id;
        }
        $group = ProductGroup::find()
            ->andWhere(['and',
                [
                    'or',
                    ['production_type' => $productionType],
                    ['production_type' => null],
                ],
                [
                    'or',
                    ['id' => ProductGroup::WITHOUT],
                    ['company_id' => $company],
                ],
            ]);
        if (isset($product_unit_id)) {
            $group->andWhere(['in', 'id', array_unique(ArrayHelper::getColumn(Product::find()->select('group_id')->where(['product_unit_id' => (int)$product_unit_id])->asArray()->all(), 'group_id'))]);
        }
        $group->orderBy([
            'IF([[id]] = 1, 0, 1)' => SORT_ASC,
            'title' => SORT_ASC,
        ]);

        return $group->all();
    }
}
