<?php

namespace common\models\product;

use Yii;

/**
 * This is the model class for table "product_category_item".
 *
 * @property int $product_id
 * @property int $field_id
 * @property string|null $value
 *
 * @property ProductCategoryField $field
 * @property Product $product
 */
class ProductCategoryItem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_category_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['product_id', 'field_id'], 'required'],
            [['product_id', 'field_id'], 'integer'],
            [['value'], 'string', 'max' => 255],
            [['product_id', 'field_id'], 'unique', 'targetAttribute' => ['product_id', 'field_id']],
            [['field_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductCategoryField::className(), 'targetAttribute' => ['field_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'Product ID',
            'field_id' => 'Field ID',
            'value' => 'Value',
        ];
    }

    /**
     * Gets query for [[Field]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getField()
    {
        return $this->hasOne(ProductCategoryField::className(), ['id' => 'field_id']);
    }

    /**
     * Gets query for [[Product]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
