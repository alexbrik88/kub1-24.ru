<?php

namespace common\models\product;

use common\components\date\DateHelper;
use common\components\date\DatePickerFormatBehavior;
use common\models\document\Order;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "product_initial_balance".
 *
 * @property int $product_id
 * @property int|null $product_unit_id
 * @property int|null $price
 * @property string|null $date
 *
 * @property Product $product
 * @property ProductUnit $productUnit
 */
class ProductInitialBalance extends \yii\db\ActiveRecord
{
    const MAX_PRICE = 999999999999999999.9999;
    const MIN_PRICE = 0;

    private $_is_price_multiplied = false;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_initial_balance';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'documentDateBehavior' => [
                'class' => DatePickerFormatBehavior::className(),
                'formatTo' => DateHelper::FORMAT_DATE,
                'attributes' => [
                    'date' => [
                        'message' => 'Дата указана неверно.',
                    ],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['price', 'date'], 'required'],
            [['product_id', 'price'], 'integer'],
            [['product_unit_id', 'price'], 'integer'],
            [['date'], 'safe'],
            [['price'],
                'compare',
                'operator' => '>=',
                'compareValue' => 0,
                'whenClient' => 'function(){}', // because of `123,00` - not valid float number (with comma) and there doesn't exist any methods to parse it on client.
            ],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['product_unit_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductUnit::className(), 'targetAttribute' => ['product_unit_id' => 'id']],
        ];
    }

    public function beforeValidate()
    {
        if (strpos($this->date, '.')) {
            $this->date = DateHelper::format($this->date, DateHelper::FORMAT_DATE, DateHelper::FORMAT_USER_DATE);
        }
        if (strlen((string)$this->price) > 0) {
            if ($this->isAttributeChanged('price', false)) {
                if (!$this->_is_price_multiplied) {
                    $this->price = $this->price * 100;
                    $this->_is_price_multiplied = true;
                }
            }
        }

        return parent::beforeValidate(); // TODO: Change the autogenerated stub
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'Product ID',
            'product_unit_id' => 'Product Unit ID',
            'price' => 'Цена покупки',
            'date' => 'Дата',
        ];
    }

    /**
     * Gets query for [[Product]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * Gets query for [[ProductUnit]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductUnit()
    {
        return $this->hasOne(ProductUnit::className(), ['id' => 'product_unit_id']);
    }

    public function getOrder()
    {
        $order = new Order();
        $order->id = null;
        $order->product_id = $this->product_id;
        $order->unit_id = $this->product_unit_id;
        $order->quantity = $this->getQuantity();

        return $order;
    }

    public function getQuantity()
    {
        return ProductStore::find()->andWhere([
            'product_id' => $this->product_id,
        ])->sum('initial_quantity') * 1;
    }

    public function getPrintOrderAmount($nullOrderId)
    {
        $quantity = $this->getQuantity();
        $price = $this->price;

        return $quantity * $price;
    }

}
