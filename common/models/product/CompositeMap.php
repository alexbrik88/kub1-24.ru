<?php

namespace common\models\product;

use Yii;

/**
 * This is the model class for table "product_composite_map".
 *
 * @property int $composite_product_id
 * @property int $simple_product_id
 * @property float $quantity
 */
class CompositeMap extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_composite_map';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function afterFind()
    {
        $this->quantity = +$this->quantity;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'composite_product_id' => 'Composite Product ID',
            'simple_product_id' => 'Simple Product ID',
            'quantity' => 'Quantity',
        ];
    }

    /**
     * @return \common\models\document\query\OrderQuery
     */
    public function getCompositeProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'composite_product_id']);
    }

    /**
     * @return \common\models\document\query\OrderQuery
     */
    public function getSimpleProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'simple_product_id']);
    }

    /**
     * @return \common\models\document\query\OrderQuery
     */
    public function getSimpleProductionType()
    {
        return $this->simpleProduct->production_type;
    }
}
