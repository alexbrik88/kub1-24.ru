<?php

namespace common\models\product;

use Yii;

/**
 * This is the model class for table "product_category_field".
 *
 * @property int $id
 * @property int $category_id
 * @property string $name
 * @property int|null $sort
 *
 * @property ProductCategory $category
 * @property ProductCategoryItem[] $productCategoryItems
 * @property Product[] $products
 */
class ProductCategoryField extends \yii\db\ActiveRecord
{
    const ISBN = 1;
    const AUTHOR = 2;
    const PUBLISHING_YEAR = 3;
    const PUBLISHING_HOUSE = 4;
    const FORMAT = 5;
    const BOOK_BINDING = 6;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_category_field';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['category_id', 'name'], 'required'],
            [['category_id', 'sort'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductCategory::className(), 'targetAttribute' => ['category_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'category_id' => 'Category ID',
            'name' => 'Name',
            'sort' => 'Sort',
        ];
    }

    /**
     * Gets query for [[Category]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ProductCategory::className(), ['id' => 'category_id']);
    }

    /**
     * Gets query for [[ProductCategoryItems]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductCategoryItems()
    {
        return $this->hasMany(ProductCategoryItem::className(), ['field_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['id' => 'product_id'])->viaTable('product_category_item', ['field_id' => 'id']);
    }

    /**
     * @return bool
     */
    public static function showConfig()
    {
        if (\Yii::$app->user && \Yii::$app->user->identity && Yii::$app->user->identity->company) {

            return Product::find()
                ->where(['company_id' => Yii::$app->user->identity->company->id])
                ->andWhere(['not', ['category_id' => NULL]])
                ->exists();
        }

        return false;
    }
}
