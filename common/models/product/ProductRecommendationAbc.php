<?php

namespace common\models\product;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "product_recommendation_abc".
 *
 * @property int $id
 * @property string $key
 * @property string|null $irreducible_quantity
 * @property string|null $advertising
 * @property string|null $discount
 * @property string|null $action
 * @property string|null $selling_price
 * @property string|null $out_from_range
 * @property string|null $purchase_price
 * @property string|null $irreducible_quantity_txt
 * @property string|null $advertising_txt
 * @property string|null $discount_txt
 * @property string|null $action_txt
 * @property string|null $selling_price_txt
 * @property string|null $out_from_range_txt
 * @property string|null $purchase_price_txt
 * @property int $created_at
 * @property int $updated_at
 */
class ProductRecommendationAbc extends \yii\db\ActiveRecord
{
    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'class' => TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_recommendation_abc';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['key'], 'required'],
            [['key'], 'unique'],
            [['key'], 'string', 'min' => 3, 'max' => 4],
            [['irreducible_quantity_txt', 'advertising_txt', 'discount_txt', 'action_txt', 'selling_price_txt', 'out_from_range_txt', 'purchase_price_txt'], 'string'],
            [['created_at', 'updated_at'], 'integer'],
            [['irreducible_quantity', 'advertising', 'discount', 'action', 'selling_price', 'out_from_range', 'purchase_price'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'key' => 'Комбинация ABCX',
            'irreducible_quantity' => 'Неснижаемый остаток',
            'advertising' => 'Реклама',
            'discount' => 'Скидка',
            'action' => 'Акция',
            'selling_price' => 'Цена продажи',
            'out_from_range' => 'Вывод из ассортимента',
            'purchase_price' => 'Цена закупки',
            'irreducible_quantity_txt' => 'Неснижаемый остаток',
            'advertising_txt' => 'Реклама',
            'discount_txt' => 'Скидка',
            'action_txt' => 'Акция',
            'selling_price_txt' => 'Цена продажи',
            'out_from_range_txt' => 'Вывод из ассортимента',
            'purchase_price_txt' => 'Цена закупки',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
