<?php

namespace common\models\product;

use common\components\helpers\ArrayHelper;
use common\components\TextHelper;
use common\models\document\Invoice;
use common\models\document\Order;
use common\models\document\OrderDocument;
use common\models\document\OrderDocumentProduct;
use common\models\document\status\InvoiceStatus;
use common\models\document\status\OrderDocumentStatus;
use common\models\document\status\TaxDeclarationStatus;
use common\models\service\Subscribe;
use common\models\service\SubscribeTariffGroup;
use frontend\models\log\ILogMessage;
use frontend\models\log\Log;
use frontend\models\log\LogEvent;
use Yii;
use common\models\employee\Employee;
use common\models\Company;
use yii\behaviors\TimestampBehavior;
use yii\db\Exception;
use yii\db\Query;
use yii\helpers\Html;

/**
 * This is the model class for table "price_list".
 *
 * @property integer $id
 * @property string $uid
 * @property integer $company_id
 * @property integer $author_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $name
 * @property integer $include_type_id
 * @property integer $include_name_column
 * @property integer $include_article_column
 * @property integer $include_product_group_column
 * @property integer $include_reminder_column
 * @property integer $include_product_unit_column
 * @property integer $include_description_column
 * @property integer $include_image_column
 * @property integer $include_price_column
 * @property integer $include_count_in_package_column
 * @property string $include_product_category_columns
 * @property integer $include_box_type_column
 * @property integer $include_custom_field_column
 * @property integer $include_brutto_column
 * @property integer $include_netto_column
 * @property integer $include_weight_column
 * @property integer $sort_type
 * @property integer $production_type
 * @property integer $is_finished
 * @property boolean $is_deleted
 * @property integer $status_id
 * @property integer $status_updated_at
 * @property integer $status_author_id
 * @property string $comment_internal
 * @property float $discount
 * @property float $markup
 * @property int $has_discount
 * @property int $has_markup
 * @property int $auto_update_prices
 * @property int $auto_update_quantity
 * @property int $send_count
 * @property int $can_checkout
 * @property int $can_checkout_invoice
 * @property int $use_notifications
 * @property int $notify_to_telegram
 * @property int $is_contractor_physical
 * @property int $has_discount_from_sum
 * @property int $discount_from_sum
 * @property float $discount_from_sum_percent
 * @property int $has_markup_from_sum
 * @property int $markup_from_sum
 * @property float $markup_from_sum_percent
 *
 * @property Employee $author
 * @property Company $company
 * @property PriceListOrder[] $priceListOrders
 * @property PriceListCheckedProducts $priceListCheckedProducts
 * @property PriceListContact $priceListContact
 * @property array $productCategoryColumns
 */
class PriceList extends \yii\db\ActiveRecord implements ILogMessage
{
    const FREE_TARIFF_MAX_ORDERS_COUNT = 5;

    const SORT_BY_NAME = 0;
    const SORT_BY_GROUP = 1;
    const SORT_CUSTOM = 2;

    const INCLUDE_ALL_PRODUCTS = 1;
    const INCLUDE_PRODUCTS_IN_STOCK = 2;
    const INCLUDE_SELECTIVELY_PRODUCTS = 3;

    const FOR_PRODUCTS = 1;
    const FOR_PRODUCTS_IN_STOCK = 2;
    const FOR_SELECTIVELY = 3;
    const FOR_ALL = 4;
    const FOR_ALL_IN_STOCK = 5;
    const FOR_SERVICES = 6;

    const MAX_DISCOUNT = 99.999999;
    const MAX_MARKUP = 9999.9999;

    const IMAGE_THUMB_WIDTH = 645;
    const IMAGE_THUMB_HEIGHT = 645;

    public static $productIncludeTypes = [
        self::INCLUDE_ALL_PRODUCTS => 'Весь товар',
        self::INCLUDE_PRODUCTS_IN_STOCK => 'Товар в наличии',
        self::INCLUDE_SELECTIVELY_PRODUCTS => 'Настроить',
    ];

    public static $serviceIncludeTypes = [
        self::INCLUDE_ALL_PRODUCTS => 'Все услуги',
        self::INCLUDE_SELECTIVELY_PRODUCTS => 'Настроить',
    ];

    public static $includeTypes = [
        self::FOR_PRODUCTS => 'Весь товар',
        self::FOR_PRODUCTS_IN_STOCK => 'Товар в наличие',
        self::FOR_ALL => 'Весь товар и услуги',
        self::FOR_ALL_IN_STOCK => 'Товар в наличие и Услуги',
        self::FOR_SERVICES => 'Услуги',
        self::FOR_SELECTIVELY => 'Выбрать вручную',
    ];

    /**
     * @var array
     */
    public $orders = [];

    /**
     * @var int
     */
    public $_price_list_type_id;

    /**
     * @var bool
     */
    public $is_custom_price_list;

    protected $_is_discount_from_sum_percent_multiplied = false;
    protected $_is_markup_from_sum_percent_multiplied = false;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'price_list';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'author_id', 'name', 'include_type_id', 'production_type'], 'required'],
            [['company_id', 'author_id', 'include_type_id', 'sort_type', 'production_type',
                'include_name_column',
                'include_article_column',
                'include_product_group_column',
                'include_reminder_column',
                'include_product_unit_column',
                'include_description_column',
                'include_image_column',
                'include_price_column',
                'include_count_in_package_column',
                'include_box_type_column',
                'include_custom_field_column',
                'include_brutto_column',
                'include_netto_column',
                'include_weight_column',
            ], 'integer'],
            [['include_product_category_columns'], 'string', 'max' => 255],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique', 'targetAttribute' => ['name', 'company_id', 'is_deleted'], 'message' => 'Прайс-лист с таким названием уже существует', 'when' => function ($model) {
                return $model->is_custom_price_list;
            }],
            [['comment_internal'], 'string', 'max' => 65535],
            [['uid'], 'string', 'max' => 16],
            [['created_at', 'updated_at', 'status_updated_at'], 'safe'],
            [['production_type'], 'in', 'range' => [
                Product::PRODUCTION_TYPE_GOODS,
                Product::PRODUCTION_TYPE_SERVICE,
                Product::PRODUCTION_TYPE_ALL,
            ]],
            [['include_type_id'], 'in', 'range' => [
                self::INCLUDE_ALL_PRODUCTS,
                self::INCLUDE_PRODUCTS_IN_STOCK,
                self::INCLUDE_SELECTIVELY_PRODUCTS,
            ]],
            [['sort_type'], 'in', 'range' => [
                self::SORT_BY_NAME,
                self::SORT_BY_GROUP,
                self::SORT_CUSTOM
            ]],
            [['orders', 'stores', '_price_list_type_id', 'is_custom_price_list'], 'safe'],
            [['is_deleted'], 'boolean'],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['author_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['status_author_id'], 'exist', 'skipOnError' => true, 'skipOnEmpty' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['status_author_id' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => PriceListStatus::className(), 'targetAttribute' => ['status_id' => 'id']],
            [['discount', 'markup', 'discount_from_sum', 'discount_from_sum_percent', 'markup_from_sum_percent'], 'default', 'value' => 0],
            [['discount', 'markup', 'discount_from_sum', 'discount_from_sum_percent', 'markup_from_sum_percent'], 'filter', 'filter' => function ($value) {
                return str_replace(",", ".", $value);
            }],
            [['discount'], 'number', 'numberPattern' => '/^\d+(\.\d*)?$/', 'min' => 0, 'max' => self::MAX_DISCOUNT],
            [['markup'], 'number', 'numberPattern' => '/^\d+(\.\d*)?$/', 'min' => 0, 'max' => self::MAX_MARKUP],
            [['discount', 'markup', 'discount_from_sum_percent', 'markup_from_sum_percent'], 'filter', 'filter' => function ($value) {
                return round($value, 6);
            }],
            [['has_discount', 'has_markup', 'auto_update_prices', 'auto_update_quantity', 'can_checkout', 'can_checkout_invoice', 'use_notifications', 'notify_to_telegram', 'is_contractor_physical', 'has_discount_from_sum', 'has_markup_from_sum'], 'boolean'],
            [['has_discount', 'has_markup', 'auto_update_prices', 'auto_update_quantity', 'can_checkout', 'can_checkout_invoice', 'use_notifications', 'notify_to_telegram', 'is_contractor_physical', 'has_discount_from_sum', 'has_markup_from_sum'], 'filter',
                'filter' => function ($value) { return (int)$value; }
            ],
            [['discount_from_sum', 'markup_from_sum'],
                'number',
                'numberPattern' => Yii::$app->params['numberPattern'],
                'max' => Yii::$app->params['maxCashSum'] * 100, // with kopeck
                'tooBig' => 'Значение «{attribute}» не должно превышать ' . TextHelper::moneyFormat(Yii::$app->params['maxCashSum'], 2) . '.',
                'whenClient' => 'function(){}', // because of `123,00` - not valid float number (with comma) and there doesn't exist any methods to parse it on client.
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Компания',
            'author_id' => 'Автор',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'name' => 'Название',
            'include_type_id' => 'Включить в прайс-лист',
            'include_name_column' => 'Название',
            'include_article_column' => 'Артикул',
            'include_product_group_column' => 'Группа',
            'include_reminder_column' => 'Остаток',
            'include_product_unit_column' => 'Ед. измерения',
            'include_description_column' => 'Описание',
            'include_image_column' => 'Изображение',
            'include_price_column' => 'Цена',
            'include_count_in_package_column' => 'Количество в упаковке',
            'include_box_type_column' => 'Вид упаковки',
            'include_custom_field_column' => 'Ваше название',
            'include_brutto_column' => 'Масса брутто',
            'include_netto_column' => 'Масса нетто',
            'include_weight_column' => 'Вес (кг)',
            'sort_type' => 'Сортировка',
            'discount' => 'Скидка',
            'markup' => 'Наценка',
            'auto_update_prices' => 'Автоматически обновлять цены',
            'auto_update_quantity' => 'Автоматически обновлять остатки',
            'can_checkout' => 'Оформление только заказа из прайс-листа',
            'can_checkout_invoice' => 'Оформление заказа и счета из прайс-листа',
            'use_notifications' => 'Получать уведомления об открытии',
            'notify_to_telegram' => 'Получать уведомления в Telegram о новом заказе и счете',
            'is_contractor_physical' => 'Продавать только физ. лицам'
        ];
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        $this->include_name_column = 1;
        $this->include_product_unit_column = 1;

        if (!$this->has_discount) {
            $this->discount = 0;
        }
        if (!$this->has_markup) {
            $this->markup = 0;
        }
        if (!$this->has_discount_from_sum) {
            $this->discount_from_sum = 0;
            $this->discount_from_sum_percent = 0;
        }
        if (!$this->has_markup_from_sum) {
            $this->markup_from_sum = 0;
            $this->markup_from_sum_percent = 0;
        }
        if ($this->isAttributeChanged('discount_from_sum')) {
            if ($this->_is_discount_from_sum_percent_multiplied == false) {
                $this->_is_discount_from_sum_percent_multiplied  = true;

                $this->discount_from_sum = round($this->discount_from_sum * 100);
            }
        }
        if ($this->isAttributeChanged('markup_from_sum')) {
            if ($this->_is_markup_from_sum_percent_multiplied == false) {
                $this->_is_markup_from_sum_percent_multiplied  = true;

                $this->markup_from_sum = round($this->markup_from_sum * 100);
            }
        }

        return parent::beforeValidate();
    }

    /**
     * @throws Exception
     */
    public function afterValidate()
    {
        parent::afterValidate();

        $this->_filterByTariffRules();

        if ($this->has_discount) {
            $this->discount = max(0, min(self::MAX_DISCOUNT, $this->discount));
            $this->markup = 0;
        }
        if ($this->has_markup) {
            $this->markup = max(0, min(self::MAX_MARKUP, $this->markup));
            $this->discount = 0;
        }
        if ($this->has_discount_from_sum) {
            $this->discount_from_sum_percent = max(0, min(self::MAX_DISCOUNT, $this->discount_from_sum_percent));
            $this->markup_from_sum = $this->markup_from_sum_percent = 0;
        }
        if ($this->has_markup_from_sum) {
            $this->markup_from_sum_percent = max(0, min(self::MAX_MARKUP, $this->markup_from_sum_percent));
            $this->discount_from_sum = $this->discount_from_sum_percent = 0;
        }
    }

    /**
     *
     */
    public function afterFind()
    {
        parent::afterFind();

        $this->_checkExpireSubscribe();
    }

    /**
     *
     */
    private function _checkExpireSubscribe()
    {
        /** @var Subscribe $subscribe */
        $subscribe = ($this->company) ? $this->company->getActualSubscription(SubscribeTariffGroup::PRICE_LIST) : null;

        if (!$subscribe) {
            $this->updateAttributes(['status_id' => PriceListStatus::STATUS_ARCHIVE]);
        }
    }

    /**
     * @throws Exception
     */
    private function _filterByTariffRules()
    {
        /** @var Subscribe $subscribe */
        $subscribe = ($this->company) ? $this->company->getActualSubscription(SubscribeTariffGroup::PRICE_LIST) : null;

        if (!$subscribe)
            throw new Exception('Price-list subscribe not found');

        $tariff = $subscribe->tariff;

        if ($tariff) {
            $this->auto_update_prices &= (bool)$tariff->getNamedParamValue('has_auto_update', SubscribeTariffGroup::PRICE_LIST);
            $this->auto_update_quantity &= (bool)$tariff->getNamedParamValue('has_auto_update', SubscribeTariffGroup::PRICE_LIST);
            $this->include_description_column &= (bool)$tariff->getNamedParamValue('has_product_view', SubscribeTariffGroup::PRICE_LIST);
            $this->include_image_column &= (bool)$tariff->getNamedParamValue('has_product_view', SubscribeTariffGroup::PRICE_LIST);
            $this->can_checkout &= (bool)$tariff->getNamedParamValue('has_checkout', SubscribeTariffGroup::PRICE_LIST);
            $this->can_checkout_invoice &= (bool)$tariff->getNamedParamValue('has_checkout', SubscribeTariffGroup::PRICE_LIST);
            $this->use_notifications &= (bool)$tariff->getNamedParamValue('has_notification', SubscribeTariffGroup::PRICE_LIST);
            $this->notify_to_telegram &= (bool)$tariff->getNamedParamValue('has_notification', SubscribeTariffGroup::PRICE_LIST);
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Employee::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceListOrders()
    {
        return $this->hasMany(PriceListOrder::className(), ['price_list_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceListCheckedProducts()
    {
        return $this->hasOne(PriceListCheckedProducts::className(), ['price_list_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceListContact()
    {
        return $this->hasOne(PriceListContact::className(), ['price_list_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContact()
    {
        return $this->getPriceListContact();
    }

    /**
     * @return \yii\db\ActiveQuery
     * @throws \yii\base\InvalidConfigException
     */
    public function getStores()
    {
        return $this->hasMany(Store::className(), ['id' => 'store_id'])->viaTable('price_list_to_store', ['price_list_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->getPriceListOrders();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(PriceListStatus::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentStatus()
    {
        return $this->getStatus();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusAuthor()
    {
        return $this->hasOne(Employee::className(), ['id' => 'status_author_id']);
    }

    /**
     * @return string
     */
    public static function generateUid()
    {
        $query = new \yii\db\Query;
        $query->select('id')->from(static::tableName())->where('[[uid]]=:uid');

        do {
            $uid = bin2hex(random_bytes(8));
        } while ($query->params([':uid' => $uid])->exists());

        return $uid;
    }

    /**
 * @return string
 * @throws \yii\web\NotFoundHttpException
 */
    public function getEmailSubject()
    {
        return 'Прайс-лист' . ' от ' . $this->company->getTitle(true) . '.';
    }

    /**
     * @return string
     */
    public function getEmailText()
    {
        $message = 'Высылаю прайс-лист.';
        $text = <<<EMAIL_TEXT
Здравствуйте!

{$message}
EMAIL_TEXT;

        return $text;
    }

    private function getProductionTypes()
    {
        if ($this->production_type == Product::PRODUCTION_TYPE_ALL) {
            return [Product::PRODUCTION_TYPE_GOODS, Product::PRODUCTION_TYPE_SERVICE];
        }

        return $this->production_type;
    }


    /**
     * @param $store
     * @param null $products
     * @param null $prices
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function loadOrders($store, $products = null, $prices = null, $productsLimit = null)
    {
        if ($products) {
            $products = Product::find()
                ->andWhere(['id' => $products])
                ->byCompany($this->company_id)
                ->notForSale(false)
                ->byStatus(Product::ACTIVE)
                ->byProductionType($this->getProductionTypes())
                ->byDeleted()
                ->all();
        } else {
            switch ($this->include_type_id) {
                case self::INCLUDE_ALL_PRODUCTS:
                    $products = ProductSearch::find()
                        ->byCompany($this->company_id)
                        ->notForSale(false)
                        ->byStatus(Product::ACTIVE)
                        ->byProductionType($this->getProductionTypes())
                        ->byDeleted()
                        ->all();
                    break;
                case self::INCLUDE_PRODUCTS_IN_STOCK:
                    $storeQuery = ProductStore::find()->alias('ps')
                        ->joinWith('store store', false)
                        ->select(['ps.product_id', 'total' => 'SUM({{ps}}.[[quantity]])'])
                        ->andWhere(['store.company_id' => $this->company_id])
                        ->andWhere(['store.is_closed' => false])
                        ->groupBy('ps.product_id');
                    $query = new Query();
                    $subQuery1 = OrderDocumentProduct::find()->alias('odp')
                        ->leftJoin(['od' => OrderDocument::tableName()], '{{odp}}.[[order_document_id]] = {{od}}.[[id]]')
                        ->leftJoin(['invoice' => Invoice::tableName()], '{{od}}.[[invoice_id]] = {{invoice}}.[[id]]')
                        ->select(['odp.product_id', 'count' => 'odp.quantity'])
                        ->andWhere(['od.company_id' => $this->company_id])
                        ->andWhere(['od.is_deleted' => false])
                        ->andWhere(['not', ['od.status_id' => [OrderDocumentStatus::STATUS_RETURN, OrderDocumentStatus::STATUS_CANCELED]]])
                        ->andWhere([
                            'or',
                            ['invoice.id' => null],
                            ['invoice.is_deleted' => true],
                            ['invoice.invoice_status_id' => InvoiceStatus::STATUS_REJECTED],
                        ]);
                    $subQuery2 = Order::find()->alias('or')
                        ->select(['or.product_id', 'count' => 'or.reserve'])
                        ->joinWith(['product pr'], false)
                        ->andWhere(['pr.company_id' => $this->company_id])
                        ->andWhere(['pr.production_type' => Product::PRODUCTION_TYPE_GOODS]);
                    $query->select(['t.product_id', 'total' => 'SUM({{t}}.[[count]])'])
                        ->from(['t' => $subQuery1->union($subQuery2, true)])
                        ->groupBy('product_id');
                    $products = ProductSearch::find()
                        ->select([
                            '*',
                            'quantity' => 'GREATEST(IFNULL({{s}}.[[total]], 0) - IFNULL({{r}}.[[total]], 0), 0)',
                        ])
                        ->leftJoin(['s' => $storeQuery], '{{product}}.[[id]] = {{s}}.[[product_id]]')
                        ->leftJoin(['r' => $query], '{{product}}.[[id]] = {{r}}.[[product_id]]')
                        ->byCompany($this->company_id)
                        ->notForSale(false)
                        ->byStatus(Product::ACTIVE)
                        ->andWhere(['production_type' => Product::PRODUCTION_TYPE_GOODS])
                        ->andWhere(['is_deleted' => false])
                        ->andHaving(['>', 'quantity', 0])
                        ->all();

                    if ($this->production_type == Product::PRODUCTION_TYPE_ALL) {
                        $products = array_merge(
                            $products,
                            ProductSearch::find()
                                ->byCompany($this->company_id)
                                ->notForSale(false)
                                ->byStatus(Product::ACTIVE)
                                ->byProductionType(Product::PRODUCTION_TYPE_SERVICE)
                                ->byDeleted()
                                ->all());
                    }

                    break;
                case self::INCLUDE_SELECTIVELY_PRODUCTS:
                    return true;
                    break;
            }
        }

        if (!empty($products)) {

            // Limit
            $limit = 0;

            // Deleted rows
            if (!$this->isNewRecord) {
                $oldIds = $this->getPriceListOrders()->select('product_id')->column();
                $newIds = ArrayHelper::getColumn($products, 'id');
                foreach ($oldIds as $oldId) {
                    if (!in_array($oldId, $newIds)) {
                        $priceListOrder = PriceListOrder::findOne([
                            'price_list_id' => $this->id,
                            'product_id' => $oldId
                        ]);
                        if ($priceListOrder) {
                            $priceListOrder->delete();
                            $limit--;
                        }
                    }
                }
            }

            /* @var $product \common\models\product\Product */
            foreach ($products as $product) {
                $productBasePrice = ($prices && isset($prices[$product->id])) ?
                    (int)bcmul($prices[$product->id], 100) :
                    (int)$product->price_for_sell_with_nds;

                if (!$this->isNewRecord) {
                    $priceListOrder = PriceListOrder::findOne([
                        'price_list_id' => $this->id,
                        'product_id' => $product->id
                    ]);

                    // refresh products data
                    if ($priceListOrder !== null) {
                        $priceListOrder->product_group_id = $product->group_id;
                        $priceListOrder->article = $product->article;
                        $priceListOrder->product_unit_id = $product->product_unit_id;
                    }
                }

                if (empty($priceListOrder)) {
                    $priceListOrder = new PriceListOrder([
                        'name' => $product->title,
                        'article' => $product->article,
                        'product_group_id' => $product->group_id,
                        'quantity' => $product->production_type ? (int)array_sum((array)$product->getQuantity($store)) : 0,
                        'product_unit_id' => $product->product_unit_id,
                        'production_type' => $product->production_type
                    ]);
                }

                $priceListOrder->price = (int)$productBasePrice;
                $priceListOrder->price_for_sell = (int)$productBasePrice;
                $priceListOrder->price_list_id = $this->id;
                $priceListOrder->product_id = $product->id;
                $priceListOrder->description = $product->comment_photo;

                // Discount/Markup
                if ($this->has_discount) {
                    $priceListOrder->price_for_sell = (string)round($priceListOrder->price * (100 - $this->discount) / 100);
                }
                if ($this->has_markup) {
                    $priceListOrder->price_for_sell = (string)round($priceListOrder->price * (100 + $this->markup) / 100);
                }

                if (!$priceListOrder->save()) {
                    \common\components\helpers\ModelHelper::logErrors($priceListOrder, __METHOD__);
                }

                if ($productsLimit && $limit >= $productsLimit) {
                    $priceListOrder->delete();
                }

                $limit++;
            }

            $this->updateAttributes(['is_finished' => true]);

            return true;
        } else {
            Yii::$app->session->setFlash('error', 'Прайс-лист не может быть пустым.');
        }
        $this->delete();

        return false;
    }

    public function getProductId() {
        return ArrayHelper::getColumn($this->priceListOrders, 'product_id');
    }

    public function getLogIcon(Log $log)
    {
        return ['label-info', 'fa fa-file-text-o'];
    }

    /**
     * Get formatted log message from Log model
     * @param Log $log
     * @return string
     */
    public function getLogMessage(Log $log)
    {
        $text = 'Прайс-лист ' . $log->getModelAttributeNew('name');

        $link = Html::a($text, [
            '/price-list/view',
            'id' => $log->getModelAttributeNew('id'),
        ]);

        $innerText = null;

        switch ($log->log_event_id) {
            case LogEvent::LOG_EVENT_CREATE:
                return $link . $innerText . ' был создан.';
            case LogEvent::LOG_EVENT_DELETE:
                return $text . $innerText . ' был удалён.';
            case LogEvent::LOG_EVENT_UPDATE:
                return $link . $innerText . ' был изменен.';
            case LogEvent::LOG_EVENT_UPDATE_STATUS:
                $status_id = $log->getModelAttributeNew('status_id');
                $status_name = PriceListStatus::findOne($status_id)->name;
                return $link . $innerText . ' статус "' . $status_name . '"';
            default:
                return $log->message;
        }
    }


    public function setPriceListType()
    {
        switch ($this->_price_list_type_id) {
            case self::FOR_PRODUCTS:
                $this->include_type_id = self::INCLUDE_ALL_PRODUCTS;
                $this->production_type = Product::PRODUCTION_TYPE_GOODS;
                break;
            case self::FOR_PRODUCTS_IN_STOCK:
                $this->include_type_id = self::INCLUDE_PRODUCTS_IN_STOCK;
                $this->production_type = Product::PRODUCTION_TYPE_GOODS;
                break;
            case self::FOR_ALL:
                $this->include_type_id = self::INCLUDE_ALL_PRODUCTS;
                $this->production_type = Product::PRODUCTION_TYPE_ALL;
                break;
            case self::FOR_ALL_IN_STOCK:
                $this->include_type_id = self::INCLUDE_PRODUCTS_IN_STOCK;
                $this->production_type = Product::PRODUCTION_TYPE_ALL;
                break;
            case self::FOR_SERVICES:
                $this->include_type_id = self::INCLUDE_ALL_PRODUCTS;
                $this->production_type = Product::PRODUCTION_TYPE_SERVICE;
                break;
            case self::FOR_SELECTIVELY:
                $this->include_type_id = self::INCLUDE_SELECTIVELY_PRODUCTS;
                $this->production_type = Product::PRODUCTION_TYPE_ALL;
                break;
            default:
                $this->include_type_id = self::INCLUDE_ALL_PRODUCTS;
                $this->production_type = Product::PRODUCTION_TYPE_GOODS;
                break;
        }
    }

    public function getPriceListType()
    {
        if ($this->include_type_id == self::INCLUDE_ALL_PRODUCTS && $this->production_type == Product::PRODUCTION_TYPE_GOODS)
            return self::FOR_PRODUCTS;
        if ($this->include_type_id == self::INCLUDE_PRODUCTS_IN_STOCK && $this->production_type == Product::PRODUCTION_TYPE_GOODS)
            return self::FOR_PRODUCTS_IN_STOCK;
        if ($this->include_type_id == self::INCLUDE_ALL_PRODUCTS && $this->production_type == Product::PRODUCTION_TYPE_ALL)
            return self::FOR_ALL;
        if ($this->include_type_id == self::INCLUDE_PRODUCTS_IN_STOCK && $this->production_type == Product::PRODUCTION_TYPE_ALL)
            return self::FOR_ALL_IN_STOCK;
        if ($this->include_type_id == self::INCLUDE_ALL_PRODUCTS && $this->production_type == Product::PRODUCTION_TYPE_SERVICE)
            return self::FOR_SERVICES;
        if ($this->include_type_id == self::INCLUDE_SELECTIVELY_PRODUCTS && $this->production_type == Product::PRODUCTION_TYPE_ALL)
            return self::FOR_SELECTIVELY;

        return 1;
    }

    public function loadProductCategoryColumns()
    {
        $checkboxes = (array)Yii::$app->request->post('include_product_category_columns', []);
        $res = [];
        foreach ($checkboxes as $fieldId => $checked) {
            $res[] = $fieldId;
        }

        $this->include_product_category_columns = implode(',', $res);
    }

    public function getProductCategoryColumns() {
        return (!empty($this->include_product_category_columns)) ? explode(',', $this->include_product_category_columns) : [];
    }

    public function autoUpdatePrices()
    {
        if (!$this->auto_update_prices)
            return false;

        foreach ($this->priceListOrders as $order)
        {
            $order->price = (int)$order->product->price_for_sell_with_nds;

            // Discount/Markup
            if ($this->has_discount) {
                $order->price_for_sell = (string)round($order->price * (100 - $this->discount) / 100);
            }
            elseif ($this->has_markup) {
                $order->price_for_sell = (string)round($order->price * (100 + $this->markup) / 100);
            }
            else {
                $order->price_for_sell = $order->price;
            }

            $order->save(false);
        }

        return true;
    }

    public function autoUpdateQuantity()
    {
        if (!$this->auto_update_quantity)
            return false;

        foreach ($this->priceListOrders as $order)
        {
            $storesIds = ArrayHelper::getColumn($this->stores, 'id');
            $order->quantity = array_sum((array) $order->product->getQuantity($storesIds));
            $order->save(false);
        }

        return true;
    }

    /**
     * @return PriceList
     */
    public function cloneSelf()
    {
        $clone = clone $this;
        $clone->isNewRecord = true;
        unset($clone->id);
        unset($clone->uid);
        $clone->author_id = Yii::$app->user->id;
        $clone->created_at = time();
        $clone->updated_at = time();
        $clone->name .= ' КОПИЯ';
        $clone->status_id = PriceListStatus::STATUS_CREATED;
        $clone->status_author_id = null;
        $clone->status_updated_at = null;
        $clone->comment_internal = "";
        $clone->send_count = 0;

        return $clone;
    }


    public function getPdfFileName()
    {
        return $this->name . '.pdf';
    }

    public function getXlsFileName()
    {
        return $this->name . '.xlsx';
    }

    public function getIs_archive()
    {
        return $this->status_id == PriceListStatus::STATUS_ARCHIVE;
    }

    public function getNotificationUid($email)
    {
        return PriceListNotification::find()
            ->where(['price_list_id' => $this->id, 'contractor_email' => $email])
            ->select('uid')
            ->scalar();
    }

    public function setNotification($uid = null)
    {
        if ($this->id && $uid) {
            $notification = PriceListNotification::findOne([
                'company_id' => $this->company_id,
                'price_list_id' => $this->id,
                'uid' => $uid
            ]);

            // SHOW ONE TIME
            if ($notification && !$notification->is_viewed) {

                $notification->updateAttributes([
                    'is_viewed' => 1,
                    'viewed_at' => time()
                ]);

                $this->company->updateAttributes([
                    'has_price_list_notifications' => 1
                ]);
            }
        }
    }

    public function getPageOrientation()
    {
        $LANDSCAPE_MIN_COLUMNS = 8;

        return $LANDSCAPE_MIN_COLUMNS < (
            $this->include_name_column +
            $this->include_article_column +
            $this->include_product_group_column +
            $this->include_reminder_column +
            $this->include_product_unit_column +
            $this->include_price_column +
            $this->include_count_in_package_column +
            $this->include_box_type_column +
            $this->include_custom_field_column +
            $this->include_brutto_column +
            $this->include_netto_column +
            $this->include_weight_column +
            ($this->include_product_category_columns ?
                count(explode(',', $this->include_product_category_columns)) : 0)

        ) ? 'landscape' : 'portrait';
   }

   public function getSortAttributeName()
   {
       switch ($this->sort_type) {
           case PriceList::SORT_CUSTOM:
               return 'sort';
           case PriceList::SORT_BY_GROUP:
               return 'product_group.title';
           case PriceList::SORT_BY_NAME:
           default:
               return 'name';
       }
   }
}
