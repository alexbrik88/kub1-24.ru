<?php

namespace common\models\product;

use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "product_store".
 *
 * @property integer $product_id
 * @property integer $store_id
 * @property integer $quantity
 * @property string $initial_quantity
 * @property string $irreducible_quantity
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Product $product
 * @property Store $store
 */
class ProductStore extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->quantity = $this->quantity * 1;
        $this->initial_quantity = $this->initial_quantity * 1;
        $this->irreducible_quantity = $this->irreducible_quantity * 1;
        if (empty($this->created_at)) {
            $this->created_at = time();
        }
        if (empty($this->updated_at)) {
            $this->updated_at = time();
        }
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_store';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['quantity', 'initial_quantity', 'irreducible_quantity'], 'number'],
            //[['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            //[['store_id'], 'exist', 'skipOnError' => true, 'targetClass' => Store::className(), 'targetAttribute' => ['store_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'Product ID',
            'store_id' => 'Store ID',
            'quantity' => 'Quantity',
            'initial_quantity' => 'Initial Quantity',
            'irreducible_quantity' => 'Irreducible Quantity',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStore()
    {
        return $this->hasOne(Store::className(), ['id' => 'store_id']);
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();

        $this->quantity = $this->quantity * 1;
        $this->initial_quantity = $this->initial_quantity * 1;
        $this->irreducible_quantity = $this->irreducible_quantity * 1;
    }

    /**
     * пересчет остатка товара на складе по ТН и УПД и Расх.накладным
     *
     * @param  integer $productId
     */
    public function recalculateProductQuantity(int $productId): void
    {
        $storeId = $this->store_id;
        $companyId = $this->store->company_id;
        $isMainStore = $this->store->is_main;
        $initialQuantity = $this->initial_quantity;

        if (!$companyId || !$storeId || !$productId) {
            return;
        }

        $command = Yii::$app->db->createCommand('
        
                SELECT SUM([[quantity]]) FROM (

                    # quantity initial
                    SELECT 
                       :initial_quantity [[quantity]],
                       :product_id AS [[product_id]]                    
    
                    UNION ALL
    
                    # quantity by packing list
                    SELECT 
                        SUM(IF({{packing_list}}.[[type]] = 1, 1, -1) * {{order_packing_list}}.[[quantity]]) AS [[quantity]],
                        {{order_packing_list}}.[[product_id]] 
                    FROM {{order_packing_list}}
                        INNER JOIN {{packing_list}} ON {{packing_list}}.[[id]] = {{order_packing_list}}.[[packing_list_id]]
                        INNER JOIN {{invoice}} ON {{invoice}}.[[id]] = {{packing_list}}.[[invoice_id]]
                    WHERE {{invoice}}.[[company_id]] = :company_id 
                        AND ({{invoice}}.[[store_id]] = :store_id'.($isMainStore ? ' OR {{invoice}}.[[store_id]] IS NULL' : '').')                
                        AND {{invoice}}.[[invoice_status_id]] NOT IN (5, 9) 
                        AND {{invoice}}.[[is_deleted]] = false
                        AND ({{packing_list}}.[[status_out_id]] IS NULL OR {{packing_list}}.[[status_out_id]] <> 5)
                        AND {{order_packing_list}}.[[product_id]] = :product_id                    
                    GROUP BY {{order_packing_list}}.[[product_id]]
                    
                    UNION ALL
                    
                    # quantity by upd
                    SELECT
                        SUM(IF({{upd}}.[[type]] = 1, 1, -1) * {{order_upd}}.[[quantity]]) AS [[quantity]],
                        {{order_upd}}.[[product_id]]
                    FROM {{order_upd}}
                        INNER JOIN {{upd}} ON {{upd}}.[[id]] = {{order_upd}}.[[upd_id]]
                    WHERE {{order_upd}}.[[product_id]] = :product_id AND {{upd}}.[[id]] IN (                    
                        SELECT u.id FROM {{upd}} u
                            INNER JOIN {{invoice_upd}} ON {{invoice_upd}}.[[upd_id]] = u.id
                            INNER JOIN {{invoice}} ON {{invoice}}.[[id]] = {{invoice_upd}}.[[invoice_id]] 
                        WHERE {{invoice}}.[[company_id]] = :company_id
                            AND ({{invoice}}.[[store_id]] = :store_id'.($isMainStore ? ' OR {{invoice}}.[[store_id]] IS NULL' : '').')
                            AND {{invoice}}.[[invoice_status_id]] NOT IN (5, 9)
                            AND {{invoice}}.[[is_deleted]] = false
                        )                    
                    GROUP BY {{order_upd}}.[[product_id]] 
                    
                    UNION ALL
                    
                    # quantity by sales invoices
                    SELECT 
                        SUM(IF({{sales_invoice}}.[[type]] = 1, 1, -1) * {{order_sales_invoice}}.[[quantity]]) AS [[quantity]],
                        {{order_sales_invoice}}.[[product_id]] 
                    FROM {{order_sales_invoice}}
                        INNER JOIN {{sales_invoice}} ON {{sales_invoice}}.[[id]] = {{order_sales_invoice}}.[[sales_invoice_id]]
                        INNER JOIN {{invoice}} ON {{invoice}}.[[id]] = {{sales_invoice}}.[[invoice_id]]
                    WHERE {{invoice}}.[[company_id]] = :company_id 
                        AND ({{invoice}}.[[store_id]] = :store_id'.($isMainStore ? ' OR {{invoice}}.[[store_id]] IS NULL' : '').')                
                        AND {{invoice}}.[[invoice_status_id]] NOT IN (5, 9) 
                        AND {{invoice}}.[[is_deleted]] = false
                        AND ({{sales_invoice}}.[[status_out_id]] IS NULL OR {{sales_invoice}}.[[status_out_id]] <> 5)
                        AND {{order_sales_invoice}}.[[product_id]] = :product_id                    
                    GROUP BY {{order_sales_invoice}}.[[product_id]]                    
                    
                ) calculatedQuantity
        ', [
            ':company_id' => $companyId,
            ':product_id' => $productId,
            ':store_id' => $storeId,
            ':initial_quantity' => $initialQuantity
        ]);

        try {
            $calculatedQuantity = $command->queryScalar();
            if ($calculatedQuantity != $this->quantity) {
                $this->updateAttributes(['quantity' => $calculatedQuantity]);
            }
        } catch (\Exception $e) {
            //
        }
    }

    /**
     * пересчет остатка товара на главном складе компании по ТН и УПД
     *
     * @param  integer $company_id
     */
    public static function recalculateAllByCompanyId($company_id)
    {
        $command = Yii::$app->db->createCommand('
            UPDATE {{product_store}}
            INNER JOIN {{store}} ON {{store}}.[[id]] = {{product_store}}.[[store_id]] and {{store}}.[[is_main]] = 1
            INNER JOIN {{product}} ON {{product}}.[[id]] = {{product_store}}.[[product_id]]

            # init count all stores
            LEFT JOIN (
                SELECT {{product_store}}.[[product_id]], SUM({{product_store}}.[[initial_quantity]]) AS [[total]]
                FROM {{product}}
                INNER JOIN {{product_store}} ON {{product}}.[[id]] = {{product_store}}.[[product_id]]
                WHERE {{product}}.[[company_id]] = :company_id
                AND {{product}}.[[production_type]] = 1
                GROUP BY {{product_store}}.[[product_id]]
            ) {{init}} ON {{init}}.[[product_id]] = {{product_store}}.[[product_id]]

            # current count all stores not main
            LEFT JOIN (
                SELECT {{product_store}}.[[product_id]], SUM({{product_store}}.[[quantity]]) AS [[total]]
                FROM {{product}}
                INNER JOIN {{product_store}} ON {{product}}.[[id]] = {{product_store}}.[[product_id]]
                INNER JOIN {{store}} ON {{store}}.[[id]] = {{product_store}}.[[store_id]]
                WHERE {{product}}.[[company_id]] = :company_id
                AND {{product}}.[[production_type]] = 1
                AND {{store}}.[[is_main]] = 0
                GROUP BY {{product_store}}.[[product_id]]
            ) {{other}} ON {{other}}.[[product_id]] = {{product_store}}.[[product_id]]

            # total count by income packing list
            LEFT JOIN (
                SELECT {{order_packing_list}}.[[product_id]], SUM({{order_packing_list}}.[[quantity]]) AS [[total]]
                FROM {{order_packing_list}}
                INNER JOIN {{packing_list}} ON {{packing_list}}.[[id]] = {{order_packing_list}}.[[packing_list_id]]
                INNER JOIN {{invoice}} ON {{invoice}}.[[id]] = {{packing_list}}.[[invoice_id]]
                WHERE {{invoice}}.[[company_id]] = :company_id
                AND {{invoice}}.[[is_deleted]] = false
                AND {{invoice}}.[[invoice_status_id]] NOT IN (5, 9)
                AND {{packing_list}}.[[type]] = 1
                GROUP BY {{order_packing_list}}.[[product_id]]
            ) {{buy}} ON {{buy}}.[[product_id]] = {{product_store}}.[[product_id]]

            # total count by out packing list
            LEFT JOIN (
                SELECT {{order_packing_list}}.[[product_id]], SUM({{order_packing_list}}.[[quantity]]) AS [[total]]
                FROM {{order_packing_list}}
                INNER JOIN {{packing_list}} ON {{packing_list}}.[[id]] = {{order_packing_list}}.[[packing_list_id]]
                INNER JOIN {{invoice}} ON {{invoice}}.[[id]] = {{packing_list}}.[[invoice_id]]
                WHERE {{invoice}}.[[company_id]] = :company_id
                AND {{invoice}}.[[is_deleted]] = false
                AND {{invoice}}.[[invoice_status_id]] NOT IN (5, 9)
                AND {{packing_list}}.[[type]] = 2
                AND {{packing_list}}.[[status_out_id]] <> 5
                GROUP BY {{order_packing_list}}.[[product_id]]
            ) {{sell}} ON {{sell}}.[[product_id]] = {{product_store}}.[[product_id]]

            # total count by income upd
            LEFT JOIN (
                SELECT {{order_upd}}.[[product_id]], SUM({{order_upd}}.[[quantity]]) AS [[total]]
                FROM {{product}}
                INNER JOIN {{order_upd}} ON {{order_upd}}.[[product_id]] = {{product}}.[[id]]
                INNER JOIN {{upd}} ON {{upd}}.[[id]] = {{order_upd}}.[[upd_id]]
                INNER JOIN {{invoice}} ON {{invoice}}.[[id]] = {{upd}}.[[invoice_id]]
                WHERE {{product}}.[[production_type]] = 1
                AND {{product}}.[[company_id]] = :company_id
                AND {{upd}}.[[type]] = 1
                AND {{invoice}}.[[is_deleted]] = false
                AND {{invoice}}.[[invoice_status_id]] NOT IN (5, 9)
            ) {{buy2}} ON {{buy2}}.[[product_id]] = {{product_store}}.[[product_id]]

            # total count by out upd
            LEFT JOIN (
                SELECT {{order_upd}}.[[product_id]], SUM({{order_upd}}.[[quantity]]) AS [[total]]
                FROM {{product}}
                INNER JOIN {{order_upd}} ON {{order_upd}}.[[product_id]] = {{product}}.[[id]]
                INNER JOIN {{upd}} ON {{upd}}.[[id]] = {{order_upd}}.[[upd_id]]
                INNER JOIN {{invoice}} ON {{invoice}}.[[id]] = {{upd}}.[[invoice_id]]
                WHERE {{product}}.[[production_type]] = 1
                AND {{product}}.[[company_id]] = :company_id
                AND {{upd}}.[[type]] = 2
                AND {{upd}}.[[status_out_id]] <> 5
                AND {{invoice}}.[[is_deleted]] = false
                AND {{invoice}}.[[invoice_status_id]] NOT IN (5, 9)
            ) {{sell2}} ON {{sell2}}.[[product_id]] = {{product_store}}.[[product_id]]

            SET {{product_store}}.[[quantity]] = IFNULL({{init}}.[[total]], 0)
                                    - IFNULL({{other}}.[[total]], 0)
                                    + IFNULL({{buy}}.[[total]], 0)
                                    + IFNULL({{buy2}}.[[total]], 0)
                                    - IFNULL({{sell}}.[[total]], 0)
                                    - IFNULL({{sell2}}.[[total]], 0)
            WHERE {{store}}.[[company_id]] = :company_id
            AND {{product}}.[[production_type]] = 1
        ', [
            ':company_id' => $company_id
        ]);

        return $command->execute();
    }
}
