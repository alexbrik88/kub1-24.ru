<?php

namespace common\models\product;

use Yii;

/**
 * This is the model class for table "product_category".
 *
 * @property int $id
 * @property string $name
 * @property int|null $sort
 *
 * @property ProductCategoryField[] $productCategoryFields
 */
class ProductCategory extends \yii\db\ActiveRecord
{
    const TYPE_BOOK_PUBLISHER = 1;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_category';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['sort'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'sort' => 'Sort',
        ];
    }

    /**
     * Gets query for [[ProductCategoryFields]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductCategoryFields()
    {
        return $this->hasMany(ProductCategoryField::className(), ['category_id' => 'id']);
    }
}
