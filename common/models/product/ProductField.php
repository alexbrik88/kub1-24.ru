<?php

namespace common\models\product;

use Yii;
use common\models\Company;

/**
 * This is the model class for table "product_field".
 *
 * @property int $id
 * @property int $company_id
 * @property string $title
 *
 * @property Company $company
 */
class ProductField extends \yii\db\ActiveRecord
{
    /**
     * @var null
     */
    public static $label = null;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_field';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id'], 'integer'],
            [['title'], 'required'],
            [['title'], 'string', 'max' => 45],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'title' => 'Название',
        ];
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return string
     */
    public static function getLabel()
    {
        if (self::$label === null) {
            $company = \yii\helpers\ArrayHelper::getValue(Yii::$app->user, ['identity', 'company']);
            if ($company && ($productField = self::findOne(['company_id' => $company->id]))) {
                self::$label = strval($productField->title);
            } else {
                self::$label = 'Ваше название';
            }
        }

        return self::$label;
    }
}
