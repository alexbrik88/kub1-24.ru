<?php

namespace common\models\product;

use Yii;

/**
 * This is the model class for table "price_list_status".
 *
 * @property int $id
 * @property string $name
 *
 * @property PriceList[] $priceLists
 */
class PriceListStatus extends \yii\db\ActiveRecord
{
    /**
     * Статус - "Создан"
     */
    const STATUS_CREATED = 1;
    /**
     * Статус - "Отправлен"
     */
    const STATUS_SEND = 2;
    /**
     * Статус - "В архиве"
     */
    const STATUS_ARCHIVE = 3;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'price_list_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceLists()
    {
        return $this->hasMany(PriceList::className(), ['status_id' => 'id']);
    }
}
