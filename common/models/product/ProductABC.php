<?php

namespace common\models\product;

use common\components\date\DateHelper;
use common\models\Company;
use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;
use frontend\models\Documents;
use frontend\modules\reports\models\AbstractFinance;
use frontend\modules\reports\models\productAnalysis\TurnoverColumn;
use Yii;
use yii\base\Exception;
use yii\base\Model;
use yii\db\Expression;
use yii\db\Query;

/**
 * Class ProductAnalysisSearch
 */
class ProductABC extends Model
{
    const MIN_REPORT_DATE = '2000-01-01';

    const GROUP_A = 1;
    const GROUP_B = 2;
    const GROUP_C = 3;

    public $product_id;
    public $title;
    public $article;
    public $group_id;

    protected $_company;
    protected $_yearMonth;
    protected $_year;

    public $dateStart;
    public $dateEnd;

    public $recommendationsMonth;

    private static $tmpTableABC_1 = 'tmp_analysis_margin_abc_1';
    private static $tmpTableABC_2 = 'tmp_analysis_margin_abc_2';
    private static $tmpTableABC_3 = 'tmp_analysis_margin_abc_3';
    private static $tmpTableXYZ = 'tmp_analysis_xyz';

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['id', 'title', 'group_id', 'year', 'recommendationsMonth'], 'safe'],
        ];
    }

    public function __construct($config)
    {
        parent::__construct($config);

        $this->year = Yii::$app->session->get('product.ABC.Y', date('Y'));
        $this->yearMonth = $this->_year . date('m');
        $this->recommendationsMonth = $this->_year . date('m');
    }

    public function __destruct()
    {
        Yii::$app->session->set('product.ABC.Ym', $this->yearMonth);
        Yii::$app->session->set('product.ABC.Y', $this->year);
    }

    public function search($params = [])
    {
        $this->load($params);

        $select = new Expression('
            product.id AS product_id,
            product.group_id,
            product.title,
            product.article,

            orderDoc.quantity,
            order.amount_sales_with_vat,
            product.price_for_buy_with_nds,
            invoice.id AS invoice_id,
            product.created_at AS first_date
        ');

        $where = [
            'and',
            ['invoice.company_id' => $this->_company->id],
            ['invoice.type' => Documents::IO_TYPE_OUT],
            ['between', 'document.document_date', $this->dateStart->format('Y-m-d'), $this->dateEnd->format('Y-m-d')],
        ];

        /////////////////

        $selectMain = [
            new Expression("'{$this->yearMonth}' AS period"),
            'group_id',
            'product_id',
            'title',
            'article',
            'turnover' => 'SUM([[amount_sales_with_vat]])',
            'margin' => 'SUM([[amount_sales_with_vat]]) - price_for_buy_with_nds * SUM([[quantity]])',
            'margin_percent' => '(SUM([[amount_sales_with_vat]]) - price_for_buy_with_nds * SUM([[quantity]])) / SUM([[amount_sales_with_vat]])',
            'selling_quantity' => 'SUM([[quantity]])',
            'first_date' => 'first_date'
        ];

        /// ABC /////////////
        $groupA = 1;
        $groupB = 2;
        $groupC = 3;

        $tmpTableABC_1 = self::$tmpTableABC_1;
        $tmpTableABC_2 = self::$tmpTableABC_2;
        $tmpTableABC_3 = self::$tmpTableABC_3;
        
        $abcQuery = (new Query)
            ->select([
                'pid' => 'product_id',
                'margin' => 'SUM([[amount_sales_with_vat]]) - price_for_buy_with_nds * SUM([[quantity]])',
                'margin_percent' => '100 * (SUM([[amount_sales_with_vat]]) - price_for_buy_with_nds * SUM([[quantity]])) / SUM([[amount_sales_with_vat]])',
                'selling_quantity' => 'SUM([[quantity]])',
            ])
            ->from(['t' => TurnoverColumn::query($select, $where)])
            ->groupBy('product_id');

        $totalCount = (clone ($abcQuery))->count('pid') ?: 9E9;
        $thresholdA = ceil(20/100 * $totalCount);
        $thresholdB = ceil(30/100 * $totalCount);
        $thresholdC = $totalCount - $thresholdA - $thresholdB;

        $abcQuery1 = clone $abcQuery;
        $abcQuery2 = clone $abcQuery;
        $abcQuery3 = clone $abcQuery;

        $abcQuery1->addSelect(['abc' => "IF (ROW_NUMBER() OVER ( ORDER BY margin DESC ) <= {$thresholdA}, {$groupA}, IF (ROW_NUMBER() OVER ( ORDER BY margin DESC ) <= {$thresholdB}, {$groupB}, {$groupC}))"]);
        $abcQuery2->addSelect(['abc' => "IF (ROW_NUMBER() OVER ( ORDER BY margin_percent DESC ) <= {$thresholdA}, {$groupA}, IF (ROW_NUMBER() OVER ( ORDER BY margin_percent DESC ) <= {$thresholdB}, {$groupB}, {$groupC}))"]);
        $abcQuery3->addSelect(['abc' => "IF (ROW_NUMBER() OVER ( ORDER BY selling_quantity DESC ) <= {$thresholdA}, {$groupA}, IF (ROW_NUMBER() OVER ( ORDER BY selling_quantity DESC ) <= {$thresholdB}, {$groupB}, {$groupC}))"]);

        Yii::$app->db2->createCommand(" CREATE TEMPORARY TABLE IF NOT EXISTS {{{$tmpTableABC_1}}} AS ({$abcQuery1->createCommand()->rawSql}) ")->execute();
        Yii::$app->db2->createCommand(" CREATE TEMPORARY TABLE IF NOT EXISTS {{{$tmpTableABC_2}}} AS ({$abcQuery2->createCommand()->rawSql}) ")->execute();
        Yii::$app->db2->createCommand(" CREATE TEMPORARY TABLE IF NOT EXISTS {{{$tmpTableABC_3}}} AS ({$abcQuery3->createCommand()->rawSql}) ")->execute();
        /////////////////////

        /// XYZ ////////////
        $tmpTableXYZ = self::$tmpTableXYZ;
        $months = self::_getPeriodsFromY(11, 0, $this->year);
        $dateStartXYZ = self::_getDateFromYM($months[0]);
        $dateEndXYZ = self::_getDateFromYM($months[count($months)-1], true);

        $xyzQuery = (new Query())
            ->select(new Expression('
                order.product_id AS pid,
                DATE_FORMAT(document_date, "%Y%m") AS period,
                SUM(quantity) AS q,
                "0.00000000000000000000000000000000" AS avg_q,
                0 AS cnt_q,
                0 AS total_cnt_q,
                '.count($months).' AS main_select_cnt_q
                '))
            ->from('invoice')
            ->leftJoin('order', 'invoice.id = order.invoice_id')
            ->andWhere([
                'invoice.company_id' => $this->company->id,
                'invoice.type' => Documents::IO_TYPE_OUT,
                'invoice.is_deleted' => false,
                'invoice.invoice_status_id' => [InvoiceStatus::STATUS_PAYED_PARTIAL, InvoiceStatus::STATUS_PAYED],
            ])
            ->andWhere([
                'between',
                'invoice.document_date',
                $dateStartXYZ,
                $dateEndXYZ,
            ])
            ->andFilterWhere(['order.product_id' => $this->product_id])
            ->groupBy(['pid', 'DATE_FORMAT(document_date, "%Y%m")']);

        Yii::$app->db2->createCommand("CREATE TEMPORARY TABLE IF NOT EXISTS {{{$tmpTableXYZ}}} AS ({$xyzQuery->createCommand()->rawSql})")->execute();

        Yii::$app->db2->createCommand("UPDATE {$tmpTableXYZ} x SET 
            x.avg_q = (SELECT SUM(y.q) / (1 + PERIOD_DIFF({$months[count($months)-1]}, MIN(period))) FROM {$tmpTableXYZ} y WHERE x.pid = y.pid GROUP BY y.pid),
            x.cnt_q = (SELECT (1 + PERIOD_DIFF(MAX(period), MIN(period))) FROM {$tmpTableXYZ} z WHERE x.pid = z.pid GROUP BY z.pid),
            x.total_cnt_q = (SELECT (1 + PERIOD_DIFF({$months[count($months)-1]}, MIN(period))) FROM {$tmpTableXYZ} z WHERE x.pid = z.pid GROUP BY z.pid)
        ")->execute();
        /////////////////////

        $query = (new Query)
            ->select($selectMain)
            ->addSelect(['variation_coefficient' => new Expression("
                (SELECT 100 * SQRT((total_cnt_q - cnt_q) / main_select_cnt_q * POW(0 - avg_q, 2) + SUM(POW(q - avg_q, 2)) / (cnt_q - 1)) / avg_q FROM {$tmpTableXYZ} coef WHERE coef.pid = t.product_id) 
            ")])
            ->addSelect(['abc' => new Expression('CONCAT(abc1.abc, abc2.abc, abc3.abc)')])
            ->from(['t' => TurnoverColumn::query($select, $where)])
            ->leftJoin(['abc1' => $tmpTableABC_1], 'abc1.pid = t.product_id')
            ->leftJoin(['abc2' => $tmpTableABC_2], 'abc2.pid = t.product_id')
            ->leftJoin(['abc3' => $tmpTableABC_3], 'abc3.pid = t.product_id')
            ->groupBy('product_id');

        $query->andFilterWhere(['product_id' => $this->product_id]);

        if ($this->title) {
            $query->andWhere(['or', ['like', 'title', $this->title], ['like', 'article', $this->title]]);
        }

        return $query->all(Yii::$app->db2);
    }

    public function removeTmpTables()
    {
        Yii::$app->db2->createCommand("DROP TEMPORARY TABLE ".self::$tmpTableABC_1)->execute();
        Yii::$app->db2->createCommand("DROP TEMPORARY TABLE ".self::$tmpTableABC_2)->execute();
        Yii::$app->db2->createCommand("DROP TEMPORARY TABLE ".self::$tmpTableABC_3)->execute();
        Yii::$app->db2->createCommand("DROP TEMPORARY TABLE ".self::$tmpTableXYZ)->execute();
    }

    /**
     * @param Company $company
     */
    public function setCompany(Company $company)
    {
        $this->_company = $company;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->_company;
    }

    public function setYearMonth($value)
    {
        $this->_yearMonth = $value;

        $year = substr($value, 0, 4);
        $month = substr($value, -2, 2);

        $this->dateStart = date_create_from_format('Y-m-d', "{$year}-{$month}-01");
        $this->dateEnd = date_create_from_format('Y-m-d', "{$year}-{$month}-" . cal_days_in_month(CAL_GREGORIAN, $month, $year));
    }

    public function getYearMonth()
    {
        return $this->_yearMonth;
    }

    public function setYear($value)
    {
        $this->_year = $value;
    }

    public function getYear()
    {
        return $this->_year;
    }

    /**
     * @return array
     */
    public function getYearFilter()
    {
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        $range = [];
        $invoiceMinDate = Invoice::find()
            ->byCompany($company->id)
            ->min('document_date', Yii::$app->db2);

        $minDates = [];

        if ($invoiceMinDate) $minDates[] = $invoiceMinDate;

        $minCashDate = !empty($minDates) ? max(self::MIN_REPORT_DATE, min($minDates)) : date(DateHelper::FORMAT_DATE);
        $registrationYear = DateHelper::format($minCashDate, 'Y', DateHelper::FORMAT_DATE);
        $currentYear = date('Y');
        foreach (range($registrationYear, $currentYear) as $value) {
            $range[$value] = $value;
        }
        arsort($range);

        return $range;
    }

    /**
     * @return array
     */
    public function getRecommendationsMonthFilter()
    {
        $ret = [];

        foreach (AbstractFinance::$month as $m => $name) {
            if ($this->_year.$m > date('Ym'))
                break;

            $ret[$m] = $name;
        };

        return array_reverse($ret, true);
    }

    ////////////////////////////////////////////// HELPERS ////////////////////////////////////////////////////////////

    public static function _getPeriodsFromY($left, $right, $year)
    {
        $month = ($year == date('Y')) ? date('m') : '12';
        $start = date_create_from_format('Ymd', $year.$month.'01');

        $curr = $start->modify("-{$left} month");

        $ret = [];
        for ($i=0; $i<=($left+$right); $i++) {

            if ($curr->format('Ym') < date('Ym'))
                $ret[] = $curr->format('Ym');
            
            $curr = $curr->modify('last day of this month')->setTime(23, 59, 59);
            $curr = $curr->modify("+1 second");
        }

        return $ret;
    }

    public static function _getDateFromYM($ym, $endOfMonth = false)
    {
        if (strlen($ym) != 6 || $ym != (int)$ym)
            throw new Exception('_getDateFromYM: $ym not found');

        $year = substr($ym, 0, 4);
        $month = substr($ym, -2, 2);
        $day = ($endOfMonth) ? cal_days_in_month(CAL_GREGORIAN, $month, $year) : "01";

        return "{$year}-{$month}-{$day}";
    }

    public function sort($products)
    {
        $sort = Yii::$app->request->get('sort', '-period');
        if (strlen($sort) != strlen(trim($sort, '-'))) {
            // desc
            $sort = trim($sort, '-');
            uasort($products, function ($a, $b) use ($sort) {
                return $b[$sort] <=> $a[$sort];
            });
        } else {
            // asc
            uasort($products, function ($a, $b) use ($sort) {
                return $a[$sort] <=> $b[$sort];
            });
        }

        return $products;
    }

    public static function getRecommendations($abc, $variationCoef)
    {
        $key = str_replace([1,2,3], ['A', 'B', 'C'], $abc);
        $key .= ($variationCoef <= 10) ? 'X' : ($variationCoef > 25 ? 'Z' : 'Y');

        if (strlen($key) == 4 && strlen(str_replace(['A','B','C','X','Y','Z'], '', $key)) == 0) {

            return ProductRecommendationAbc::find()->where(['key' => $key])->asArray()->one();
        }

        return [];
    }
}
