<?php

namespace common\models\product;

use Yii;

/**
 * This is the model class for table "price_list_contact".
 *
 * @property int $id
 * @property int $price_list_id
 * @property string $lastname
 * @property string $firstname
 * @property string $patronymic
 * @property string $email
 * @property string $site
 * @property string $phone
 * @property int $no_patronymic
 *
 * @property PriceList $priceList
 */
class PriceListContact extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'price_list_contact';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['price_list_id'], 'required'],
            [['price_list_id', 'no_patronymic'], 'integer'],
            [['lastname', 'firstname', 'patronymic', 'email', 'site', 'phone'], 'string', 'max' => 255],
            [['price_list_id'], 'exist', 'skipOnError' => true, 'targetClass' => PriceList::className(), 'targetAttribute' => ['price_list_id' => 'id']],
            [
                ['patronymic'],
                'required',
                'when' => function (PriceListContact $model) {
                    return (!$model->no_patronymic && ($model->firstname || $model->lastname));
                },
                'whenClient' => 'function () {
                    return contractorIsPhysical() && !$("#contractor-physical_no_patronymic").is(":checked");
                }',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'price_list_id' => 'Price List ID',
            'lastname' => 'Фамилия',
            'firstname' => 'Имя',
            'patronymic' => 'Отчество',
            'email' => 'Email',
            'site' => 'Сайт',
            'phone' => 'Телефон',
            'no_patronymic' => 'Нет отчества',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceList()
    {
        return $this->hasOne(PriceList::className(), ['id' => 'price_list_id']);
    }

    /**
     * @return string
     */
    public function getFio()
    {
        return join(' ', array_filter([
            $this->lastname, $this->firstname, $this->patronymic,
        ]));
    }
}
