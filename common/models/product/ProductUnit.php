<?php

namespace common\models\product;

use common\models\document\Order;
use frontend\modules\export\models\one_c\OneCExport;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "product_unit".
 *
 * @property integer $id
 * @property string $name
 * @property string $code_okei
 * @property string $intl_short_name
 * @property string $intl_code
 */
class ProductUnit extends \yii\db\ActiveRecord
{
    const UNIT_COUNT = 1;
    const UNIT_KG = 2;
    const UNIT_METER = 3;
    const UNIT_LITER = 4;
    const UNIT_PACKAGE = 5;
    const UNIT_SET = 6;
    const UNIT_PAIR = 7;
    const UNIT_MONTH = 8;
    const UNIT_TON = 9;
    const UNIT_RUN_METER = 10;
    const UNIT_HOUR = 11;
    const UNIT_MINUTE = 12;
    const UNIT_SQUARE_METER = 13;
    const UNIT_CUBIC_METER = 14;
    const UNIT_KILOMETER = 15;
    const UNIT_DAY = 21;

    private static $_getSorted;
    private static $_productIdArray;
    private static $_serviceIdArray;

    public static $countableUnits = [
        ProductUnit::UNIT_KG => 1,
        ProductUnit::UNIT_TON => 1000,
    ];

    public static $productUnits = [
        ProductUnit::UNIT_COUNT,
        ProductUnit::UNIT_KG,
        ProductUnit::UNIT_TON,
        ProductUnit::UNIT_METER,
        ProductUnit::UNIT_LITER,
        ProductUnit::UNIT_PACKAGE,
        ProductUnit::UNIT_SET,
        ProductUnit::UNIT_PAIR,
        ProductUnit::UNIT_MONTH,
        ProductUnit::UNIT_RUN_METER,
        ProductUnit::UNIT_SQUARE_METER,
        ProductUnit::UNIT_CUBIC_METER,
        ProductUnit::UNIT_KILOMETER,
    ];

    public static $serviceUnits = [
        ProductUnit::UNIT_COUNT,
        ProductUnit::UNIT_MINUTE,
        ProductUnit::UNIT_HOUR,
        ProductUnit::UNIT_MONTH,
        ProductUnit::UNIT_SQUARE_METER,
        ProductUnit::UNIT_CUBIC_METER,
        ProductUnit::UNIT_TON,
        ProductUnit::UNIT_RUN_METER,
        ProductUnit::UNIT_KILOMETER,
    ];

    public static $crossUnits = [
        ProductUnit::UNIT_COUNT,
        ProductUnit::UNIT_SQUARE_METER,
        ProductUnit::UNIT_CUBIC_METER,
        ProductUnit::UNIT_MONTH,
        ProductUnit::UNIT_TON,
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product_unit';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'title'], 'required'],
            [['goods', 'services'], 'boolean'],
            [['name'], 'string', 'max' => 45],
            [['title'], 'string', 'max' => 45],
            [['code_okei'], 'string', 'max' => 4],
            [['intl_short_name'], 'string', 'max' => 45],
            [['intl_code'], 'string', 'max' => 3],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Сокращение, для док-ов',
            'title' => 'Полное наименование',
            'code_okei' => 'Код ОКЕИ',
            'goods' => 'Товары',
            'services' => 'Услуги',
            'object_guid' => 'Guid',
            'intl_short_name' => 'Международное сокращение',
            'intl_code' => 'Международный код',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['unit_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['product_unit_id' => 'id']);
    }

    /**
     * @return boolean
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if (!$this->object_guid) {
            do {
                $this->object_guid = OneCExport::generateGUID();
            } while (self::find()->where(['object_guid' => $this->object_guid])->exists());
        }

        if (!$this->code_okei) {
            $this->code_okei = Product::DEFAULT_VALUE;
        }

        return true;
    }

    /**
     * @return boolean
     */
    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }

        if ($this->getOrders()->exists() || $this->getProducts()->exists()) {
            Yii::$app->session->setFlash('error', 'Эту единицу измерения нельзя удалить.');

            return false;
        }

        return true;
    }

    public function getForeignName()
    {
        return trim($this->intl_short_name, '-') ?: $this->name;
    }

    public static function getUnits($group_id = null)
    {
        (Yii::$app->user->isGuest) ? $company = 0 : $company = Yii::$app->user->identity->company->id;
        if (isset($group_id)) {
            $unit = ProductUnit::find()->Where(['in', 'id', array_unique(ArrayHelper::getColumn(Product::find()->select('product_unit_id')->where(['group_id' => (int)$group_id])->andWhere(['company_id' => $company])->asArray()->all(), 'product_unit_id'))]);
        } else {
            $unit = ProductUnit::find();
        }

        return $unit->all();
    }

    public static function findSorted()
    {
        return static::find()->orderBy([
            'IF({{product_unit}}.[[name]] = "---", 1, 0)' => SORT_ASC,
            'IF({{product_unit}}.[[id]] = 1, 0, 1)' => SORT_ASC,
            'product_unit.name' => SORT_ASC,
        ]);
    }

    public static function getSortedAll()
    {
        if (static::$_getSorted === null) {
            static::$_getSorted = static::findSorted()->all();
        }

        return static::$_getSorted;
    }

    public static function productUnitsIdArray()
    {
        if (static::$_productIdArray === null) {
            static::$_productIdArray = static::find()->select('id')->where(['goods' => 1])->column();
        }

        return static::$_productIdArray;
    }

    public static function serviceUnitsIdArray()
    {
        if (static::$_serviceIdArray === null) {
            static::$_serviceIdArray = static::find()->select('id')->where(['services' => 1])->column();
        }

        return static::$_serviceIdArray;
    }
}
