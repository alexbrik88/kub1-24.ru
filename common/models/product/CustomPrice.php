<?php

namespace common\models\product;

use common\models\TaxRate;
use Yii;

/**
 * This is the model class for table "custom_price".
 *
 * @property int $product_id
 * @property int $price_group_id
 * @property string $price
 * @property int $nds_id
 *
 * @property CustomPriceGroup $priceGroup
 * @property Product $product
 * @property TaxRate $nds
 */
class CustomPrice extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'custom_price';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['price_group_id', 'price', 'nds_id'], 'required'],
            [['product_id', 'price_group_id', 'nds_id'], 'integer'],
            [['price'], 'integer', 'min' => 1],
            [['price_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => CustomPriceGroup::className(), 'targetAttribute' => ['price_group_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['nds_id'], 'exist', 'skipOnError' => true, 'targetClass' => TaxRate::className(), 'targetAttribute' => ['nds_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'Product ID',
            'price_group_id' => 'Price Group ID',
            'price' => 'Price',
            'nds_id' => 'Nds ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceGroup()
    {
        return $this->hasOne(CustomPriceGroup::className(), ['id' => 'price_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNds()
    {
        return $this->hasOne(TaxRate::className(), ['id' => 'nds_id']);
    }
}
