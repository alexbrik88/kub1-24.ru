<?php

namespace common\models\product;

use common\models\document\GoodsCancellation;
use Yii;
use common\models\Company;
use common\models\Contractor;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\PackingList;
use common\models\document\SalesInvoice;
use common\models\document\Upd;
use common\models\ofd\OfdReceipt;

/**
 * This is the model class for table "product_turnover".
 *
 * @property int $order_id
 * @property string $order_table
 * @property int $document_id
 * @property string $document_table
 * @property int $company_id
 * @property string $date
 * @property int $invoice_id
 * @property int $contractor_id
 * @property int $type
 * @property int|null $production_type
 * @property int|null $is_invoice_actual
 * @property int|null $is_document_actual
 * @property float $purchase_price
 * @property float $price_one
 * @property float $quantity
 * @property float $total_amount
 * @property float $purchase_amount
 * @property int $year
 * @property int $month
 * @property int $product_id
 */
class ProductTurnover extends \yii\db\ActiveRecord
{
    public static $docClassMap = [
        'act' => Act::class,
        'packing_list' => PackingList::class,
        'upd' => Upd::class,
        'sales_invoice' => SalesInvoice::class,
        'ofd_receipt' => OfdReceipt::class,
        'goods_cancellation' => GoodsCancellation::class
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_turnover';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function beforeSave($insert)
    {
        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'order_id' => 'Order ID',
            'order_table' => 'Order Table',
            'document_id' => 'Document ID',
            'document_table' => 'Document Table',
            'company_id' => 'Company ID',
            'date' => 'Date',
            'invoice_id' => 'Invoice ID',
            'contractor_id' => 'Contractor ID',
            'type' => 'Type',
            'production_type' => 'Production Type',
            'is_invoice_actual' => 'Is Invoice Actual',
            'is_document_actual' => 'Is Document Actual',
            'purchase_price' => 'Purchase Price',
            'price_one' => 'Price One',
            'quantity' => 'Quantity',
            'total_amount' => 'Total Amount',
            'purchase_amount' => 'Purchase Amount',
            'year' => 'Year',
            'month' => 'Month',
            'product_id' => 'Product ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContractor()
    {
        return $this->hasOne(Contractor::className(), ['id' => 'contractor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocument()
    {
        if ($class = $this->getDocumentClass()) {
            return $this->hasOne($class, ['id' => 'document_id']);
        }

        return null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(Invoice::className(), ['id' => 'invoice_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return string
     */
    public function getDocumentClass()
    {
        return self::$docClassMap[$this->document_table] ?? null;
    }
}
