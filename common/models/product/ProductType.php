<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 12.04.2016
 * Time: 10:51
 */

namespace common\models\product;


use yii\db\ActiveRecord;

class ProductType extends ActiveRecord
{
    const UNIT_COUNT = 1;
    const UNIT_KG = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'production_type';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
            'object_guid' => 'УИД',
        ];
    }
}