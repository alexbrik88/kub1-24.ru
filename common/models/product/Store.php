<?php

namespace common\models\product;

use common\models\Company;
use common\models\document\Invoice;
use common\models\employee\Employee;
use Yii;

/**
 * This is the model class for table "store".
 *
 * @property integer $id
 * @property integer $company_id
 * @property integer $responsible_employee_id
 * @property integer $is_main
 * @property integer $is_closed
 * @property string $name
 *
 * @property ProductStore[] $productStores
 * @property Product[] $products
 * @property Company $company
 * @property Employee $responsibleEmployee
 */
class Store extends \yii\db\ActiveRecord
{
    public $saved = false;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'store';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_closed', 'is_main'], 'filter', 'filter' => function($value) {
                return (int) $value;
            }],
            [['responsible_employee_id'], 'filter', 'filter' => function($value) {
                return (int) $value ? : null;
            }],
            [['name'], 'required'],
            [
                ['name'],
                'unique',
                'filter' => ['company_id' => $this->company_id],
                'message' => 'Склад с таким названием уже есть. Выберите другое название.'
            ],
            [['is_closed'], function ($attribute, $params) {
                if ($this->$attribute && $this->is_main) {
                    $this->addError($attribute, 'Основной склад не может быть закрыт.');
                }
            }],
            [['responsible_employee_id', 'is_closed', 'is_main'], 'integer'],
            [['name'], 'string', 'max' => 50],
            [
                ['responsible_employee_id'], 'exist', 'skipOnError' => true,
                'targetClass' => Employee::className(),
                'targetAttribute' => ['responsible_employee_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'responsible_employee_id' => 'Ответственный',
            'responsibleEmployee.fio' => 'Ответственный',
            'is_main' => 'Основной',
            'is_closed' => 'Склад закрыт',
            'name' => 'Склад',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductStores()
    {
        return $this->hasMany(ProductStore::className(), ['store_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoices()
    {
        return $this->hasMany(Invoice::className(), ['store_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Product::className(), ['id' => 'product_id'])->viaTable('product_store', ['store_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResponsibleEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'responsible_employee_id']);
    }

    /**
     * @return boolean
     */
    public function beforeDelete()
    {
        if (!parent::beforeDelete()) {
            return false;
        }

        return $this->canDelete();
    }

    /**
     * @param boolean $insert
     * @return boolean
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if ($this->is_main) {
            $this->responsible_employee_id = null;
            $this->is_closed = false;
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $condition = [
            'and',
            ['company_id' => $this->company_id],
            ['is_main' => 1],
            ['not', ['id' => $this->id]],
        ];

        if ($this->is_main && self::find()->where($condition)->exists()) {
            self::updateAll(['is_main' => 0], $condition);
        }

        $this->saved = true;
    }

    /**
     * @return boolean
     */
    public function canDelete()
    {
        //return !($this->is_main || $this->getProductStores()->exists());
        return !($this->is_main || $this->getInvoices()->byDeleted()->exists());
    }
}
