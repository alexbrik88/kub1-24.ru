<?php

namespace common\models\product;

use common\components\date\DateHelper;
use common\models\Contractor;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\Order;
use common\models\document\PackingList;
use common\models\document\status\InvoiceStatus;
use common\models\document\Upd;
use frontend\models\Documents;
use Yii;
use yii\db\Query;

/**
 * This is the model class for table "product_to_supplier".
 *
 * @property int $product_id
 * @property int $supplier_id
 * @property string $price
 * @property string $article
 *
 * @property Contractor $supplier
 * @property Product $product
 */
class ProductToSupplier extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'product_to_supplier';
    }

    /**
     * @var
     */
    public $lastDocumentDate;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['supplier_id', 'price'], 'required'],
            [['product_id', 'supplier_id', 'price'], 'integer'],
            [['article'], 'string', 'max' => 32],
            [['product_id', 'supplier_id'], 'unique', 'targetAttribute' => ['product_id', 'supplier_id']],
            [['supplier_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contractor::class, 'targetAttribute' => ['supplier_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::class, 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'product_id' => 'Product ID',
            'supplier_id' => 'Supplier ID',
            'price' => 'price',
            'article' => 'Article',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSupplier()
    {
        return $this->hasOne(Contractor::className(), ['id' => 'supplier_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @param Product $product
     * @return array
     */
    public static function getFromDocuments(Product $product)
    {
        $tUpd = Upd::tableName();
        $tAct = Act::tableName();
        $tPls = PackingList::tableName();
        $tInvoice = Invoice::tableName();
        $tOrder = Order::tableName();

        $upd = Upd::find()
            ->leftJoin($tInvoice, "$tInvoice.id = $tUpd.invoice_id")
            ->leftJoin($tOrder, "{{%$tInvoice}}.[[id]] = {{%$tOrder}}.[[invoice_id]]")
            ->where(["$tInvoice.company_id" => $product->company_id])
            ->andWhere(["$tInvoice.type" => Documents::IO_TYPE_IN])
            ->andWhere(["<>", "$tInvoice.invoice_status_id", InvoiceStatus::STATUS_REJECTED])
            ->andWhere(["$tInvoice.is_deleted" => 0])
            ->andWhere(["$tOrder.product_id" => $product->id])
            ->select(["$tInvoice.contractor_id", "$tUpd.document_date", "$tOrder.article", "$tOrder.purchase_price_with_vat AS price"]);
        $act = Act::find()
            ->leftJoin($tInvoice, "$tInvoice.id = $tAct.invoice_id")
            ->leftJoin($tOrder, "{{%$tInvoice}}.[[id]] = {{%$tOrder}}.[[invoice_id]]")
            ->where(["$tInvoice.company_id" => $product->company_id])
            ->andWhere(["$tInvoice.type" => Documents::IO_TYPE_IN])
            ->andWhere(["<>", "$tInvoice.invoice_status_id", InvoiceStatus::STATUS_REJECTED])
            ->andWhere(["$tInvoice.is_deleted" => 0])
            ->andWhere(["$tOrder.product_id" => $product->id])
            ->select(["$tInvoice.contractor_id", "$tAct.document_date", "$tOrder.article", "$tOrder.purchase_price_with_vat AS price"]);
        $pls = PackingList::find()
            ->leftJoin($tInvoice, "$tInvoice.id = $tPls.invoice_id")
            ->leftJoin($tOrder, "{{%$tInvoice}}.[[id]] = {{%$tOrder}}.[[invoice_id]]")
            ->where(["$tInvoice.company_id" => $product->company_id])
            ->andWhere(["$tInvoice.type" => Documents::IO_TYPE_IN])
            ->andWhere(["<>", "$tInvoice.invoice_status_id", InvoiceStatus::STATUS_REJECTED])
            ->andWhere(["$tInvoice.is_deleted" => 0])
            ->andWhere(["$tOrder.product_id" => $product->id])
            ->select(["$tInvoice.contractor_id", "$tPls.document_date", "$tOrder.article", "$tOrder.purchase_price_with_vat AS price"]);

        $suppliersData = (new Query)->select(['contractor_id', 'document_date', 'article', 'price'])->from($upd->union($act, true)->union($pls, true))->all();

        $result = [];
        foreach ($suppliersData as $data) {
            $result[$data['contractor_id']] = new ProductToSupplier([
                'product_id' => $product->id,
                'supplier_id' => $data['contractor_id'],
                'price' => $data['price'],
                'article' => $data['article'],
                'lastDocumentDate' => DateHelper::format($data['document_date'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
            ]);
        }

        return $result;
    }
}
