<?php

namespace common\models\product;

use common\models\ICompanyStrictQuery;
use frontend\rbac\UserRole;
use Yii;
use yii\db\ActiveQuery;
use yii\web\User;

/**
 * Class ProductQuery
 *
 * @package common\models\document
 */
class ProductQuery extends ActiveQuery implements ICompanyStrictQuery
{
    use \common\components\TableAliasTrait;

    /**
     * @param $companyId
     *
     * @return ProductQuery
     */
    public function byCompany($companyId)
    {
        return $this->andWhere([
            self::getTableAlias() . '.company_id' => $companyId,
        ]);
    }

    /**
     * @param bool $isDeleted
     * @return ProductQuery
     */
    public function byDeleted($isDeleted = false)
    {
        if ($isDeleted) {
            return $this
                    ->andWhere([self::getTableAlias() . '.status' => Product::DELETED])
                    ->andWhere([self::getTableAlias() . '.is_deleted' => true]);
        } else {
            return $this
                    ->andWhere(['not', [self::getTableAlias() . '.status' => Product::DELETED]])
                    ->andWhere([self::getTableAlias() . '.is_deleted' => false]);
        }
    }

    /**
     * @param integer $status
     * @return ProductQuery
     */
    public function byStatus($status)
    {
        return $this->andWhere([self::getTableAlias() . '.status' => $status]);
    }

    /**
     * @param bool $type
     * @return ProductQuery
     */
    public function byProductionType($type = false)
    {
        return $this->andWhere([
            self::getTableAlias() . '.production_type' => $type,
        ]);
    }

    public function byBalance($date, $fieldname)
    {
        $subQueryBy = $this->getSQL($date->format('Y-m-d'),1);
        $subQuerySale = $this->getSQL($date->format('Y-m-d'),2);
        return $this->addSelect('((' . $subQueryBy . ') - (' . $subQuerySale . ')) as ' . $fieldname);
    }

    public function byTurn($date_start, $date_end, $buy)
    {
        ($buy) ? $type = 1 : $type = 2;
        $subQueryStart = $this->getSQL($date_start->format('Y-m-d'),$type);
        $subQueryEnd = $this->getSQL($date_end->format('Y-m-d'),$type);
        ($buy) ? $fieldname = 'turnBuy' : $fieldname = 'turnSale';
        return $this->addSelect('((' . $subQueryEnd . ') - (' . $subQueryStart . ')) as ' . $fieldname);
    }

    protected function getSQL($date, $type)
    {
        $subQuery = (new ActiveQuery('common\models\document\Order'))
            ->select('CASE WHEN SUM(order.quantity) IS NULL THEN 0 ELSE SUM(order.quantity) END')->from('{{%order}}')
            ->where('order.product_id = product.id')->join('INNER JOIN', 'invoice', 'invoice.id = order.invoice_id')
            ->andWhere(['{{%invoice}}.type' => $type])
            ->andWhere(['<=', '{{%invoice}}.document_date', $date]);
        return $subQuery->createCommand()->getRawSql();
    }

    /**
     * @param boolean $value
     * @return ProductQuery
     */
    public function notForSale($value = null)
    {
        if ($value !== null) {
            return $this->andWhere([
                self::getTableAlias() . '.not_for_sale' => $value,
            ]);
        }

        return $this;
    }

    /**
     * @param boolean $value
     * @return ProductQuery
     */
    public function byUser()
    {
        if (Yii::$app->id == 'app-frontend' &&
            !Yii::$app->user->can(UserRole::ROLE_CHIEF) &&
            !Yii::$app->user->can(UserRole::ROLE_ACCOUNTANT)
        ) {
            $this->andWhere([
                self::getTableAlias() . '.not_for_sale' => false,
            ]);
        }

        return $this;
    }
}
