<?php

namespace common\models\product;

use common\components\date\DateHelper;
use common\models\document\Invoice;
use common\models\document\Order;
use common\models\document\OrderDocument;
use common\models\document\OrderDocumentProduct;
use common\models\document\OrderPackingList;
use common\models\document\PackingList;
use common\models\document\status\InvoiceStatus;
use common\models\document\status\OrderDocumentStatus;
use common\models\employee\Employee;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\web\Cookie;

class ProductSearch extends Product
{
    public $balanceStart;
    public $balanseEnd;

    public $balanceStartValue;
    public $balanceEndValue;
    public $periodBuyValue;
    public $periodSaleValue;

    public $documentType;
    public $exclude;

    public $amountForBuy;
    public $amountForSell;

    public $reserveCount;
    public $availableCount;

    public $store_id;
    public $quantity;
    public $reserve;
    public $available;

    public $filterStatus = self::ALL;
    public $filterImage = self::ALL;
    public $filterComment = self::ALL;
    public $filterDate = self::ALL;
    public $storeArchive = false;

    public $dateFrom;
    public $dateTo;

    public $turnoverType = self::TURNOVER_BY_COUNT;

    const ALL = 0;

    const IN_ARCHIVE = 1;
    const IN_WORK = 2;
    const IN_RESERVE = 3;

    const HAS_IMAGE = 1;
    const NO_IMAGE = 2;

    const HAS_COMMENT = 1;
    const NO_COMMENT = 2;

    const FILTER_YESTERDAY = 1;
    const FILTER_TODAY = 2;
    const FILTER_WEEK = 3;
    const FILTER_MONTH = 4;


    const DEFAULT_SORTING_TITLE = 'title';
    const DEFAULT_SORTING_TITLE_EN = 'title_en';
    const DEFAULT_SORTING_GROUP_ID = 'group_id';

    const TURNOVER_BY_COUNT = 1;
    const TURNOVER_BY_AMOUNT = 2;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'production_type',
                    'title',
                    'article',
                    'group_id',
                    'store_id',
                    'product_unit_id',
                    'documentType',
                    'exclude',
                    'balanceStart',
                    'balanseEnd',
                    'amountForBuy',
                    'amountForSell',
                    'reserveCount',
                    'availableCount',
                    'filterStatus',
                    'defaultSorting',
                    'filterImage',
                    'filterComment',
                    'filterDate',
                    'dateFrom',
                    'dateTo',
                    'turnoverType',
                ],
                'safe',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'quantity' => 'Количество на складе',
        ]);
    }

    /**
     * @return ProductQuery
     */
    public function baseQuery()
    {
        $query = ProductSearch::find()->alias('product')
            ->byCompany($this->company_id)
            ->byUser()
            ->notForSale($this->documentType == Documents::IO_TYPE_OUT ? false : null)
            ->andWhere(['product.production_type' => $this->production_type])
            ->andWhere(['product.is_deleted' => false])
            ->andWhere(['not', ['product.status' => Product::DELETED]])
            ->leftJoin(['r' => Product::reserveQuery($this->company_id)], '{{product}}.[[id]] = {{r}}.[[product_id]]');

        if (!empty($this->exclude)) {
            $query->andWhere(['not', ['product.id' => $this->exclude]]);
        }

        $query->andFilterWhere([
            'group_id' => $this->group_id,
            'product_unit_id' => $this->product_unit_id,
            'article' => $this->article,
        ]);

        if ($this->storeArchive) {
            $query->byStatus(Product::ARCHIVE);
        } else {
            switch ($this->filterStatus) {
                case self::IN_ARCHIVE:
                    $query->byStatus(Product::ARCHIVE);
                    break;
                case self::IN_RESERVE:
                    $query->andWhere(['>', 'r.total', 0])
                        ->byStatus(Product::ACTIVE);
                    break;
                case self::ALL:
                    break;
                case self::IN_WORK:
                default:
                    $query->byStatus(Product::ACTIVE);
                    break;
        }
        }

        if ((int)$this->filterDate !== self::ALL) {
            $query->andWhere(['between',
                'date(from_unixtime(' . Product::tableName() . '.created_at))',
                DateHelper::format($this->dateFrom, DateHelper::FORMAT_DATE, DateHelper::FORMAT_USER_DATE),
                DateHelper::format($this->dateTo, DateHelper::FORMAT_DATE, DateHelper::FORMAT_USER_DATE),
            ]);
        }

        if ($this->production_type == Product::PRODUCTION_TYPE_GOODS) {
            if ($this->filterImage == self::HAS_IMAGE) {
                $query->andWhere(['has_photo' => true]);
            } elseif ($this->filterImage == self::NO_IMAGE) {
                $query->andWhere(['has_photo' => false]);
            }

            if ($this->filterComment == self::HAS_COMMENT) {
                $query->andWhere(['and',
                    ['not', ['comment_photo' => null]],
                    ['not', ['comment_photo' => '']],
                ]);
            } elseif ($this->filterComment == self::NO_COMMENT) {
                $query->andWhere(['or',
                    ['comment_photo' => null],
                    ['comment_photo' => ''],
                ]);
            }
        }

        if ($this->title) {
            $query->andWhere([
                'or',
                ['like', Product::tableName() . '.title', $this->title],
                ['like', Product::tableName() . '.article', $this->title],
                //"MATCH({{%product}}.[[title]]) AGAINST (:search)"
            ]);
            //$query->addParams([':search' => $this->title]);
        }

        return $query;
    }

    /**
     * @return ActiveQuery
     */
    public function storeQuery()
    {
        $stireId = $this->store_id ?: $this->company->getStores()->select('id')->column();

        $query = ProductStore::find()
            ->select(['product_id', 'total' => 'SUM([[quantity]])'])
            ->andWhere(['store_id' => $stireId])
            ->groupBy('product_id');

        return $query;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     * @param string $formName
     * @param bool $hideZeroesQuantityProducts
     *
     * @return ActiveDataProvider
     */
    public function search($params, $formName = null, $hideZeroesQuantityProducts = false)
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;

        $this->load($params, $formName);

        $query = $this->baseQuery()
            ->select([
                'product.*',
                'quantity' => 's.total',
                'reserve' => 'r.total',
                'available' => 'GREATEST(IFNULL({{s}}.[[total]], 0) - IFNULL({{r}}.[[total]], 0), 0)',
                'amountForBuy' => '(IFNULL({{product}}.[[price_for_buy_with_nds]], 0) * IFNULL({{s}}.[[total]], 0))',
                'amountForSell' => '(IFNULL({{product}}.[[price_for_sell_with_nds]], 0) * IFNULL({{s}}.[[total]], 0))',
            ])
            ->leftJoin(['s' => $this->storeQuery()], '{{product}}.[[id]] = {{s}}.[[product_id]]')
            ->joinWith('group')
            ->with('productUnit');

        $query->groupBy(Product::tableName() . '.id');

        $defaultSortingAttr = isset($params['defaultSorting']) ? $params['defaultSorting'] : self::DEFAULT_SORTING_TITLE;

        $sortAttributes = [
            'title',
            'title_en',
            'article',
            'quantity',
            'reserve',
            'available',
            'amountForSell',
            'reserveCount',
            'availableCount',
            'comment_photo',
            'price_for_buy_nds_id',
            'price_for_sell_with_nds',
            'price_for_sell_nds_id',
            'group_id' => [
                'asc' => ['product_group.title' => SORT_ASC,],
                'desc' => ['product_group.title' => SORT_DESC,],
            ],
        ];

        if ($user->currentEmployeeCompany && $user->currentEmployeeCompany->can_view_price_for_buy) {
            $sortAttributes[] = 'price_for_buy_with_nds';
            $sortAttributes[] = 'amountForBuy';
        }

        if ($hideZeroesQuantityProducts) {
            $query->having(['>', 'quantity', 0]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'totalCount' => $this->baseQuery()->limit(-1)->offset(-1)->orderBy([])->count('product.id'),
            'pagination' => [
                'pageSize' => Yii::$app->request->get('per-page'),
            ],
            'sort' => [
                'attributes' => $sortAttributes,
                'defaultOrder' => [
                    $defaultSortingAttr => SORT_ASC,
                ],
            ]
        ]);

        return $dataProvider;
    }

    /**
     * @return float
     */
    public function getAllQuantity()
    {
        $query = $this->baseQuery()
            ->leftJoin(['s' => $this->storeQuery()], '{{product}}.[[id]] = {{s}}.[[product_id]]');

        return $query->sum('s.total');
    }

    /**
     * @return float
     */
    public function getAllReserve()
    {
        $query = $this->baseQuery();
        // ->leftJoin(['r' => Product::reserveQuery($this->company_id)], '{{product}}.[[id]] = {{r}}.[[product_id]]');

        return $query->sum('r.total');
    }

    /**
     * @return float
     */
    public function getAllReserveAmount()
    {
        $query = $this->baseQuery();
        // ->leftJoin(['r' => Product::reserveQuery($this->company_id)], '{{product}}.[[id]] = {{r}}.[[product_id]]');

        return $query->sum('IFNULL({{product}}.[[price_for_sell_with_nds]], 0) * IFNULL({{r}}.[[total]], 0)');
    }

    /**
     * @return integer
     */
    public function getAllAmountBuy()
    {
        $query = $this->baseQuery()
            ->leftJoin(['s' => $this->storeQuery()], '{{product}}.[[id]] = {{s}}.[[product_id]]');

        return $query->sum('IFNULL({{product}}.[[price_for_buy_with_nds]], 0) * IFNULL({{s}}.[[total]], 0)');
    }

    /**
     * @return integer
     */
    public function getAllAmountSell()
    {
        $query = $this->baseQuery()
            ->leftJoin(['s' => $this->storeQuery()], '{{product}}.[[id]] = {{s}}.[[product_id]]');

        return $query->sum('IFNULL({{product}}.[[price_for_sell_with_nds]], 0) * IFNULL({{s}}.[[total]], 0)');
    }

    /**
     * @param array $params
     * @param bool $hideZeroesTurnoverProducts
     * @return ActiveDataProvider
     */
    public function turnoverSearch($params, $hideZeroesTurnoverProducts = false)
    {
        $this->load($params);
        $this->turnoverType = isset($params['turnoverType']) ? $params['turnoverType'] : $this->turnoverType;

        $startDate = $this->dateStart->format('Y-m-d');
        $endDate = $this->dateEnd->format('Y-m-d');

        $query = $this->baseQuery()
            ->select($this->turnoverType == self::TURNOVER_BY_COUNT ? [
                'product.*',
                'periodBuyValue' => "(
                    @periodBuyValue := (
                        IFNULL({{tPeriodBuy}}.[[totalCount]], 0) +
                        IF(
                            {{initQuantityDate}}.[[date]] BETWEEN '$startDate' AND '$endDate',
                            IFNULL({{initQuantity}}.[[total]], 0),
                            0
                        )
                    )
                )",
                'periodSaleValue' => "(@periodSaleValue := IFNULL({{tPeriodSale}}.[[totalCount]], 0))",
                'balanceStartValue' => "(@balanceStartValue := IF({{initQuantityDate}}.[[date]] < '$startDate', IFNULL({{initQuantity}}.[[total]], 0), 0) +
                                       IFNULL({{tStartBuy}}.[[totalCount]], 0) - IFNULL({{tStartSale}}.[[totalCount]], 0))",
                'balanceEndValue' => "(@balanceStartValue + @periodBuyValue - @periodSaleValue)",
            ] : [
                'product.*',
                'periodBuyValue' => "(
                    @periodBuyValue := (
                        IFNULL({{tPeriodBuy}}.[[totalCount]] * {{product}}.[[price_for_buy_with_nds]], 0) +
                        IF(
                            {{initQuantityDate}}.[[date]] BETWEEN '$startDate' AND '$endDate',
                            IFNULL({{initQuantity}}.[[total]] * {{product}}.[[price_for_buy_with_nds]], 0),
                            0
                        )
                    )
                )",
                'periodSaleValue' => "(@periodSaleValue := IFNULL({{tPeriodSale}}.[[totalCount]] * {{product}}.[[price_for_buy_with_nds]], 0))",
                'balanceStartValue' => "(@balanceStartValue := IF({{initQuantityDate}}.[[date]] < '$startDate', IFNULL({{initQuantity}}.[[total]] * {{product}}.[[price_for_buy_with_nds]], 0), 0) +
                                       IFNULL({{tStartBuy}}.[[totalCount]] * {{product}}.[[price_for_buy_with_nds]], 0) - IFNULL({{tStartSale}}.[[totalCount]] * {{product}}.[[price_for_buy_with_nds]], 0))",
                'balanceEndValue' => "(@balanceStartValue + @periodBuyValue - @periodSaleValue)",
            ])
            ->leftJoin([
                'tStartBuy' => $this->startBalanceQuery($this->dateStart, Documents::IO_TYPE_IN)
            ], "{{product}}.[[id]] = {{tStartBuy}}.[[product_id]]")
            ->leftJoin([
                'tStartSale' => $this->startBalanceQuery($this->dateStart, Documents::IO_TYPE_OUT)
            ], "{{product}}.[[id]] = {{tStartSale}}.[[product_id]]")
            ->leftJoin([
                'tPeriodBuy' => $this->periodBalanceQuery($this->dateStart, $this->dateEnd, Documents::IO_TYPE_IN)
            ], "{{product}}.[[id]] = {{tPeriodBuy}}.[[product_id]]")
            ->leftJoin([
                'tPeriodSale' => $this->periodBalanceQuery($this->dateStart, $this->dateEnd, Documents::IO_TYPE_OUT)
            ], "{{product}}.[[id]] = {{tPeriodSale}}.[[product_id]]")
            ->leftJoin([
                'initQuantity' => $this->initQuantityQuery()
            ], "{{product}}.[[id]] = {{initQuantity}}.[[product_id]]")
            ->leftJoin([
                'initQuantityDate' => ProductInitialBalance::tableName()
            ], "{{product}}.[[id]] = {{initQuantityDate}}.[[product_id]]")
        ;

        if ($hideZeroesTurnoverProducts) {
            $query->having(['>', 'periodBuyValue', 0]);
            $query->orHaving(['>', 'periodSaleValue', 0]);
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'title',
                    'balanceStartValue',
                    'periodBuyValue',
                    'periodSaleValue',
                    'balanceEndValue',
                ],
                'defaultOrder' => [
                    'title' => SORT_ASC,
                ]
            ],
        ]);

        return $dataProvider;
    }

    public function startBalanceQuery($dateStart, $type)
    {
        $select = ['order.product_id', 'order.quantity'];
        $where = [
            'and',
            ['invoice.company_id' => $this->company_id],
            ['invoice.type' => $type],
            ['<', 'document.document_date', $dateStart->format('Y-m-d')],
        ];
        $query = new Query;
        $query
            ->select(['product_id', 'totalCount' => 'SUM([[quantity]])'])
            ->from(['t' => ProductTurnoverSearch::turnoverQuery($select, $where)])
            ->groupBy('product_id');

        return $query;
    }

    public function periodBalanceQuery($dateStart, $dateEnd, $type)
    {
        $select = ['order.product_id', 'order.quantity'];
        $where = [
            'and',
            ['invoice.company_id' => $this->company_id],
            ['invoice.type' => $type],
            ['between', 'document.document_date', $dateStart->format('Y-m-d'), $dateEnd->format('Y-m-d')],
        ];
        $query = new Query;
        $query
            ->select(['product_id', 'totalCount' => 'SUM([[quantity]])'])
            ->from(['t' => ProductTurnoverSearch::turnoverQuery($select, $where)])
            ->groupBy('product_id');

        return $query;
    }

    public function initQuantityQuery()
    {
        return ProductStore::find()->alias('p')
            ->select(['p.product_id', 'total' => 'SUM({{p}}.[[initial_quantity]])'])
            ->innerJoin(['s' => Store::tableName()], '{{p}}.[[store_id]] = {{s}}.[[id]]')
            ->andWhere(['s.company_id' => $this->company_id])
            ->groupBy('p.product_id');
    }
}
