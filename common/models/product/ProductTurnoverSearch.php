<?php

namespace common\models\product;

use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\Order;
use common\models\document\OrderAct;
use common\models\document\OrderPackingList;
use common\models\document\OrderUpd;
use common\models\document\PackingList;
use common\models\document\status\ActStatus;
use common\models\document\Upd;
use common\models\document\status\InvoiceStatus;
use common\models\document\status\PackingListStatus;
use common\models\document\status\UpdStatus;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\db\Query;

/**
 * ProductTurnoverSearch
 */
class ProductTurnoverSearch extends Product
{
    public $date;
    public $coming;
    public $selling;
    public $contractor;
    public $basis;
    public $basis_id;
    public $product_id;
    public $company_id;
    public $contractor_id;
    public $dateRange;
    public $type;
    public $searchByProductionType = Product::PRODUCTION_TYPE_GOODS;

    public static $byDocument = [
        'packing-list' => 'ТН',
        'upd' => 'УПД',
        'act' => 'Акт'
    ];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contractor_id'], 'safe'],
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'date' => 'Дата',
            'coming' => 'Закуплено',
            'selling' => 'Продано',
            'contractor' => 'Контрагент',
            'basis' => 'Документ',
        ];
    }

    /**
     * @return yii\db\Query
     */
    public static function turnoverQuery($select, $where = null, $searchByProductionType = Product::PRODUCTION_TYPE_GOODS, $initialSelect = null, $initialWhere = null)
    {
        if ($searchByProductionType == Product::PRODUCTION_TYPE_GOODS) {
            // оборот по товарным накладным
            $query1 = OrderPackingList::find()->alias('order')->select($select)
                ->addSelect(['by_document' => new Expression('"packing-list"')])
                ->leftJoin(['document' => PackingList::tableName()], '{{document}}.[[id]] = {{order}}.[[packing_list_id]]')
                ->leftJoin(['invoice' => Invoice::tableName()], '{{invoice}}.[[id]] = {{document}}.[[invoice_id]]')
                ->leftJoin(['product' => Product::tableName()], '{{product}}.[[id]] = {{order}}.[[product_id]]')
                ->leftJoin(['order_main' => Order::tableName()], '{{order_main}}.[[id]] = {{order}}.[[order_id]]')
                ->andWhere([
                    'product.production_type' => $searchByProductionType,
                    'invoice.is_deleted' => false,
                    'invoice.invoice_status_id' => InvoiceStatus::$validInvoices,
                ])
                ->andWhere([
                    'or',
                    ['document.status_out_id' => PackingListStatus::$turmoverStatus],
                    ['document.status_out_id' => null],
                ]);
        } else {
            // оборот актам
            $query1 = OrderAct::find()->alias('order')->select($select)
                ->addSelect(['by_document' => new Expression('"act"')])
                ->leftJoin(['document' => Act::tableName()], '{{document}}.[[id]] = {{order}}.[[act_id]]')
                ->leftJoin(['invoice' => Invoice::tableName()], '{{invoice}}.[[id]] = {{document}}.[[invoice_id]]')
                ->leftJoin(['product' => Product::tableName()], '{{product}}.[[id]] = {{order}}.[[product_id]]')
                ->leftJoin(['order_main' => Order::tableName()], '{{order_main}}.[[id]] = {{order}}.[[order_id]]')
                ->andWhere([
                    'product.production_type' => $searchByProductionType,
                    'invoice.is_deleted' => false,
                    'invoice.invoice_status_id' => InvoiceStatus::$validInvoices,
                ])
                ->andWhere([
                    'or',
                    ['document.status_out_id' => ActStatus::$turmoverStatus],
                    ['document.status_out_id' => null],
                ]);
        }
        // оборот по УПД
        $query2 = OrderUpd::find()->alias('order')->select($select)
            ->addSelect(['by_document' => new Expression('"upd"')])
            ->leftJoin(['document' => Upd::tableName()], '{{document}}.[[id]] = {{order}}.[[upd_id]]')
            ->leftJoin(['invoice' => Invoice::tableName()], '{{invoice}}.[[id]] = {{document}}.[[invoice_id]]')
            ->leftJoin(['product' => Product::tableName()], '{{product}}.[[id]] = {{order}}.[[product_id]]')
            ->leftJoin(['order_main' => Order::tableName()], '{{order_main}}.[[id]] = {{order}}.[[order_id]]')
            ->andWhere([
                'product.production_type' => $searchByProductionType,
                'invoice.is_deleted' => false,
                'invoice.invoice_status_id' => InvoiceStatus::$validInvoices,
            ])
            ->andWhere([
                'or',
                ['document.status_out_id' => UpdStatus::$turmoverStatus],
                ['document.status_out_id' => null],
            ]);

        if ($where) {
            $query1->andWhere($where);
            $query2->andWhere($where);
        }

        if ($initialSelect && $initialWhere) {

            $initialBalanceQuery = (new Query())
                ->select($initialSelect)
                ->addSelect(['by_document' => new Expression('"product-initial-balance"')])
                ->where($initialWhere)
                ->from('product_initial_balance');

            $query = $query1->union($query2, true)->union($initialBalanceQuery, true);

        } else {

            $query = $query1->union($query2, true);
        }

        return $query;
    }

    /**
     * @return yii\db\ActiveQuery
     */
    public function searchQuery()
    {
        $select = [
            'document.type',
            'document_id' => 'document.id',
            'document.document_date',
            'document.document_number',
            'document.document_additional_number',
            'order.quantity',
            'invoice.contractor_id',
            'contractor' => 'invoice.contractor_name_short',
            'invoice_id' => 'invoice.id',
            'invoice_date' => 'invoice.document_date',
            'invoice_number' => 'invoice.document_number',
            'invoice_additional_number' => 'invoice.document_additional_number',
        ];
        $where = [
            'and',
            ['order.product_id' => $this->product_id],
            ['between', 'document.document_date', $this->dateRange['from'], $this->dateRange['to']],
        ];

        if (($product = Product::find()->where(['id' => $this->product_id])->one()) && array_sum($product->initQuantity) > 0) {

            $initialSelect = "
                null AS `type`,
                {$this->product_id} AS `document_id`,
                `date` AS `document_date`,
                null AS `document_number`,
                null AS `document_additional_number`,
                null AS `quantity`,
                null AS `contractor_id`,
                null AS `contractor`,
                null AS `invoice_id`,
                null AS `invoice_date`,
                null AS `invoice_number`,
                null AS `invoice_additional_number`            
            ";

            $initialWhere = [
                'and',
                ['product_id' => $this->product_id],
                ['between', 'date', $this->dateRange['from'], $this->dateRange['to']],
            ];
        } else {

            $initialSelect = $initialWhere = null;
        }

        $query = new Query;
        $query->from(['t' => ProductTurnoverSearch::turnoverQuery($select, $where, $this->searchByProductionType, $initialSelect, $initialWhere)]);

        return $query;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $this->load($params);

        $query = $this->searchQuery()
            ->select([
                'type',
                'date' => '(@date := DATE_FORMAT([[document_date]], "%d.%m.%Y"))',
                'invoice_date' => '(@invoice_date := DATE_FORMAT([[invoice_date]], "%d.%m.%Y"))',
                'coming' => 'IF([[type]] = 1, TRIM(TRAILING "." FROM TRIM(TRAILING "0" from [[quantity]])), null)',
                'selling' => 'IF([[type]] = 2, TRIM(TRAILING "." FROM TRIM(TRAILING "0" from [[quantity]])), null)',
                'contractor',
                'contractor_id',
                'by_document',
                'basis' => 'CONCAT("№ ", [[document_number]], " ", IFNULL([[document_additional_number]], ""), " от ", @date)',
                'basis_id' => 'document_id',
                'invoice' => 'CONCAT("№ ", [[invoice_number]], " ", IFNULL([[invoice_additional_number]], ""), " от ", @invoice_date)',
                'invoice_id'
            ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'date' => [
                        'asc' => ['document_date' => SORT_ASC],
                        'desc' => ['document_date' => SORT_DESC],
                    ],
                    'coming' => [
                        'asc' => ['ISNULL([[coming]])' => SORT_ASC, '[[coming]] * 1' => SORT_ASC],
                        'desc' => ['ISNULL([[coming]])' => SORT_ASC, '[[coming]] * 1' => SORT_DESC],
                    ],
                    'selling' => [
                        'asc' => ['ISNULL([[selling]])' => SORT_ASC, '[[selling]] * 1' => SORT_ASC],
                        'desc' => ['ISNULL([[selling]])' => SORT_ASC, '[[selling]] * 1' => SORT_DESC],
                    ],
                    //'contractor',
                    'basis',
                    'invoice'
                ],
                'defaultOrder' => [
                    'date' => SORT_DESC,
                ],
            ],
        ]);

        $query->andFilterWhere(['contractor_id' => $this->contractor_id]);

        return $dataProvider;
    }

    public static function getDocumentModel($by_document, $id, $type) {
        switch ($by_document) {
            case 'packing-list':
                $doc = PackingList::findOne(['id' => $id, 'type' => $type]);
                break;
            case 'act':
                $doc = Act::findOne(['id' => $id, 'type' => $type]);
                break;
            case 'upd':
                $doc = Upd::findOne(['id' => $id, 'type' => $type]);
                break;
            default:
                $doc = null;
        }

        return $doc;
    }
}
