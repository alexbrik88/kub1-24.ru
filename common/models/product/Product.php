<?php

namespace common\models\product;

use common\components\calculator\CalculatorHelper;
use common\components\date\DateHelper;
use common\components\excel\Excel;
use common\components\helpers\Html;
use common\components\helpers\ModelHelper;
use common\components\image\EasyThumbnailImage;
use common\components\TextHelper;
use common\components\UploadedFile;
use common\models\address\Country;
use common\models\Company;
use common\models\CompanyProductType;
use common\models\Contractor;
use common\models\document\Act;
use common\models\document\Order;
use common\models\document\OrderAct;
use common\models\document\OrderPackingList;
use common\models\document\PackingList;
use common\models\document\Invoice;
use common\models\document\InvoiceAuto;
use common\models\document\OrderDocument;
use common\models\document\OrderDocumentProduct;
use common\models\document\OrderUpd;
use common\models\document\status\InvoiceStatus;
use common\models\document\status\OrderDocumentStatus;
use common\models\document\Upd;
use common\models\employee\Config;
use common\models\employee\Employee;
use common\models\TaxationType;
use common\models\EmployeeCompany;
use common\models\file\File;
use common\models\ofd\OfdReceiptItem;
use common\models\out\OutInvoiceProduct;
use common\models\TaxRate;
use common\models\product\ProductGroup;
use DateTime;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\rbac\permissions;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Connection;
use yii\db\Exception;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "product".
 *
 * @property integer $id
 * @property string $created_at
 * @property string $archived_at
 * @property integer $creator_id
 * @property integer $company_id
 * @property integer $production_type
 * @property integer $group_id
 * @property integer $product_count
 * @property string $title
 * @property string $title_en
 * @property string $code
 * @property string $code_tn_ved
 * @property string $article
 * @property string $barcode
 * @property string $item_type_code
 * @property integer $product_unit_id
 * @property string $box_type
 * @property string $count_in_package
 * @property string $count_in_place
 * @property string $place_count
 * @property string $mass_gross
 * @property string $mass_net
 * @property integer $has_excise
 * @property string $excise
 * @property integer $price_for_buy_nds_id
 * @property string $price_for_buy_with_nds
 * @property integer $price_for_sell_nds_id
 * @property string $price_for_sell_with_nds
 * @property integer $country_origin_id
 * @property integer $customs_declaration_number
 * @property integer $is_deleted
 * @property integer $not_for_sale
 * @property integer $provider_id
 * @property string $comment
 * @property string $comment_photo
 * @property integer $has_photo
 * @property string $custom_field_value
 * @property integer $category_id
 * @property integer $is_traceable
 *
 * @property Order[] $orders
 * @property Invoice[] $invoices
 * @property Country $countryOrigin
 * @property Company $company
 * @property Employee $creator
 * @property ProductUnit $productUnit
 * @property TaxRate $priceForBuyNds
 * @property TaxRate $priceForSellNds
 * @property OrderDocumentProduct[] $orderDocumentProducts
 * @property OrderDocument[] $orderDocuments
 * @property PriceListOrder $orderPriceLists
 * @property ProductGroup $group
 * @property PriceListOrder[] $priceLists
 * @property Contractor[] $suppliers
 * @property CustomPrice[] $customPrices
 * @property ProductInitialBalance $initialBalance
 * @property ProductCategoryItem $categoryItem
 *
 * @property integer $priceForBuyWithoutNds
 * @property integer $priceForSellWithoutNds
 * @property integer $totalQuantity
 * @property integer $totalInitQuantity
 */
class Product extends \yii\db\ActiveRecord
{
    /**
     * product status
     */
    const DELETED = 0;
    const ACTIVE = 1;
    const ARCHIVE = 2;

    /**
     * product unit type Simple or Composite
     */
    const UNIT_TYPE_SIMPLE = 1;
    const UNIT_TYPE_COMPOSITE = 2;

    /**
     *
     */
    const PRODUCTION_TYPE_SERVICE = 0;

    /**
     *
     */
    const PRODUCTION_TYPE_GOODS = 1;

    /**
     *
     */
    const PRODUCTION_TYPE_ALL = 2;

    /**
     *
     */
    const SCENARIO_PROD_TOVAR = 'prodtovar';
    /**
     *
     */
    const SCENARIO_ZAKUP_TOVAR = 'zakuptovar';
    /**
     *
     */
    const SCENARIO_PROD_USLUG = 'produslug';
    /**
     *
     */
    const SCENARIO_ZAKUP_USLUG = 'zakupuslug';
    /**
     *
     */
    const SCENARIO_CREATE = 'create';
    /**
     *
     */
    const SCENARIO_UPDATE = 'update';

    /**
     *
     */
    const MOVE_ALL = 0;
    /**
     *
     */
    const MOVE_PARTIAL = 1;

    /**
     * @var array
     */
    public static $moveTypes = [
        self::MOVE_ALL => 'Все',
        self::MOVE_PARTIAL => 'Частично',
    ];

    /**
     * @var array
     */
    public static $unitTypes = [
        self::UNIT_TYPE_SIMPLE => 'Простая',
        self::UNIT_TYPE_COMPOSITE => 'Составная',
    ];

    /**
     * @var
     */
    public $img_del = 0;

    /**
     * @var
     */
    public $image;

    /**
     * @var
     */
    public $imageUrl;

    /**
     * @var int
     */
    public $importInitCount = 0;

    /**
     * @var array
     */
    public static $uploadFolder = 'product';

    /**
     * @var array
     */
    public static $productionTypes = [
        self::PRODUCTION_TYPE_SERVICE => 'Услуги',
        self::PRODUCTION_TYPE_GOODS => 'Товары',
        self::PRODUCTION_TYPE_ALL => 'Товары, Услуги',
    ];
    /**
     * @var array
     */
    public static $productionTypesOne = [
        self::PRODUCTION_TYPE_SERVICE => 'Услуга',
        self::PRODUCTION_TYPE_GOODS => 'Товар',
        self::PRODUCTION_TYPE_ALL => 'Товар, Услуга',
    ];

    /**
     * @var array
     */
    public static $companyProductionTypeToProductProductionType = [
        CompanyProductType::TYPE_GOODS => self::PRODUCTION_TYPE_GOODS,
        CompanyProductType::TYPE_SERVICES => self::PRODUCTION_TYPE_SERVICE,
    ];

    /**
     *
     */
    const DEFAULT_VALUE = '---';

    /**
     * @var null
     */
    protected $_dateStart = null;

    /**
     * @var null
     */
    protected $_dateEnd = null;

    /**
     * @var
     */
    protected $_quantity;

    /**
     * @var
     */
    protected $_initQuantity;

    /**
     * @var
     */
    protected $_irreducibleQuantity;

    /**
     * @var
     */
    protected $_deleteError;

    /**
     * @var
     */
    protected $_loadedSuppliers = [];

    /**
     * @var
     */
    protected $_loadedCustomPrices = [];

    /**
     * @var
     */
    protected $_loadedCategoryItems = [];

    /**
     * @var
     */
    protected $_loadedInitialBalance;

    /**
     * @var
     */
    protected $_data = [];

    /**
     * @var
     */
    public $daysOfInventory;
    /**
     * @var
     */
    public $sellingSpeed;
    /**
     * @var
     */
    public $remainder;

    public $soldDuringPeriod;

    public $storeName;

    public $storeId;

    public $invoice_id;

    public $lastOrderQuantity;

    /**
     *
     */
    public function init()
    {
        //Костыль для записей у которых НДС null
        if (empty($this->price_for_buy_nds_id)) {
            $this->price_for_buy_nds_id = TaxRate::RATE_WITHOUT;
        }
        if (empty($this->price_for_sell_nds_id)) {
            $this->price_for_sell_nds_id = TaxRate::RATE_WITHOUT;
        }
        //кноец костыля

        parent::init();
    }

    /**
     * @param $companyId
     * @param $productId
     * @return Product
     */
    public static function getById($companyId, $productId)
    {
        return static::find()
            ->byCompany($companyId)
            ->byDeleted(false)
            ->andWhere([
                'id' => $productId,
            ])
            ->one();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'product';
    }

    /**
     * @inheritdoc
     */
    public function formName()
    {
        return $this->unit_type == Product::UNIT_TYPE_COMPOSITE ? 'ProductComposite' : parent::formName();
    }

    /**
     * @return ProductQuery
     */
    public static function find()
    {
        return new ProductQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return
            ArrayHelper::merge(parent::behaviors(), [
                [
                    'class' => TimestampBehavior::className(),
                    'updatedAtAttribute' => false,
                ],
            ]);
    }

    /**
     * @return array
     */
    public function fields()
    {
        $fields = parent::fields();
        $fields['price_for_buy_no_nds'] = 'price_for_buy_no_nds';
        $fields['price_for_sell_no_nds'] = 'price_for_sell_no_nds';

        return $fields;
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        return array_merge(parent::scenarios(), [
            self::SCENARIO_PROD_TOVAR => [
                'production_type',
                'title',
                'title_en',
                'price_for_sell_with_nds',
                'price_for_sell_nds_id',
                'product_unit_id',
                'price_for_buy_with_nds',
                'price_for_buy_nds_id',
                'code',
                'code_tn_ved',
                'item_type_code',
                'box_type',
                'count_in_package',
                'count_in_place',
                'place_count',
                'mass_gross',
                'mass_net',
                'country_origin_id',
                'customs_declaration_number',
                'initQuantity',
                'irreducibleQuantity',
                'article',
                'barcode',
                'custom_field_value',
                'category_id',
            ],
            self::SCENARIO_ZAKUP_TOVAR => [
                'production_type',
                'title',
                'title_en',
                'price_for_sell_with_nds',
                'price_for_sell_nds_id',
                'product_unit_id',
                'price_for_buy_with_nds',
                'price_for_buy_nds_id',
                'code',
                'code_tn_ved',
                'item_type_code',
                'box_type',
                'count_in_package',
                'count_in_place',
                'place_count',
                'mass_gross',
                'mass_net',
                'country_origin_id',
                'customs_declaration_number',
                'not_for_sale',
                'initQuantity',
                'irreducibleQuantity',
                'article',
                'barcode',
                'custom_field_value',
                'category_id',
            ],
            self::SCENARIO_PROD_USLUG => [
                'production_type',
                'title',
                'title_en',
                'price_for_sell_with_nds',
                'price_for_sell_nds_id',
                'product_unit_id',
                'price_for_buy_with_nds',
                'price_for_buy_nds_id',
            ],
            self::SCENARIO_ZAKUP_USLUG => [
                'production_type',
                'title',
                'title_en',
                'price_for_sell_with_nds',
                'price_for_sell_nds_id',
                'product_unit_id',
                'price_for_buy_with_nds',
                'price_for_buy_nds_id',
                'not_for_sale',
            ],
            self::SCENARIO_CREATE => [
                'production_type',
                'title',
                'title_en',
                'group_id',
                'price_for_sell_with_nds',
                'price_for_sell_nds_id',
                'product_unit_id',
                'price_for_buy_with_nds',
                'price_for_buy_nds_id',
                'code',
                'code_tn_ved',
                'item_type_code',
                'box_type',
                'count_in_package',
                'count_in_place',
                'place_count',
                'mass_gross',
                'mass_net',
                'country_origin_id',
                'customs_declaration_number',
                'not_for_sale',
                'initQuantity',
                'irreducibleQuantity',
                'provider_id',
                'comment',
                'image',
                'img_del',
                'article',
                'barcode',
                'weight',
                'volume',
                'comment_photo',
                'custom_field_value',
                'category_id',
                'one_c_imported',
                'one_c_exported',
                'is_traceable'
            ],
            self::SCENARIO_UPDATE => [
                'title',
                'title_en',
                'group_id',
                'price_for_sell_with_nds',
                'price_for_sell_nds_id',
                'product_unit_id',
                'price_for_buy_with_nds',
                'price_for_buy_nds_id',
                'code',
                'code_tn_ved',
                'item_type_code',
                'box_type',
                'count_in_package',
                'count_in_place',
                'place_count',
                'mass_gross',
                'mass_net',
                'country_origin_id',
                'customs_declaration_number',
                'not_for_sale',
                'initQuantity',
                'irreducibleQuantity',
                'provider_id',
                'comment',
                'image',
                'img_del',
                'article',
                'barcode',
                'weight',
                'volume',
                'comment_photo',
                'custom_field_value',
                'category_id',
                'one_c_imported',
                'one_c_exported',
                'is_traceable'
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        if (!Yii::$app->request->isConsoleRequest)
            $isBuyPrice = Yii::$app->user->can(permissions\document\Document::DOCUMENTS_IN);
        else
            $isBuyPrice = 1;

        return [
            [
                [
                    'production_type',
                    'title',
                    'price_for_sell_nds_id',
                    'price_for_buy_nds_id',
                ],
                'required',
                'on' => self::SCENARIO_CREATE,
            ],
            [
                [
                    'title',
                ],
                'required',
                'on' => self::SCENARIO_UPDATE,
            ],
            [
                [
                    'price_for_buy_with_nds',
                ],
                'required',
                'on' => self::SCENARIO_CREATE,
                'when' => function () use ($isBuyPrice) {
                    return $isBuyPrice;
                },
                'whenClient' => 'function(){
                    return ' . ($isBuyPrice ? 'true' : 'false') . ';
                }',
            ],
            [
                [
                    'price_for_sell_with_nds',
                ],
                'required',
                'on' => self::SCENARIO_CREATE,
                'when' => function () {
                    return !$this->not_for_sale;
                },
                'whenClient' => 'function(){
                    return !$("#product-not_for_sale").is(":checked");
                }',
            ],
            [
                [
                    'production_type',
                    'title',
                    'price_for_sell_with_nds',
                    'product_unit_id',
                ],
                'required',
                'on' => self::SCENARIO_PROD_TOVAR,
            ],
            [
                [
                    'price_for_sell_nds_id',
                ],
                'required',
                'when' => function () {
                    return Yii::$app->user->identity->company->hasNds();
                },
                'whenClient' => 'function(){
                    return true;
                }',
                'on' => self::SCENARIO_PROD_TOVAR,
            ],
            [
                [
                    'production_type',
                    'title',
                    'price_for_buy_with_nds',
                    'product_unit_id',
                ],
                'required',
                'on' => self::SCENARIO_ZAKUP_TOVAR,
            ],
            [
                [
                    'price_for_buy_nds_id',
                ],
                'required',
                'when' => function () {
                    return Yii::$app->user->identity->company->hasNds();
                },
                'whenClient' => 'function(){
                    return true;
                }',
                'on' => self::SCENARIO_ZAKUP_TOVAR,
            ],
            [
                [
                    'production_type',
                    'title',
                    'price_for_sell_with_nds',
                ],
                'required',
                'on' => self::SCENARIO_PROD_USLUG,
            ],
            [
                [
                    'price_for_sell_nds_id',
                ],
                'required',
                'when' => function (Product $model) {
                    return Yii::$app->user->identity->company->hasNds();
                },
                'whenClient' => 'function(){
                    return true;
                }',
                'on' => self::SCENARIO_PROD_USLUG,
            ],
            [
                [
                    'production_type',
                    'title',
                    'price_for_buy_with_nds',
                ],
                'required',
                'on' => self::SCENARIO_ZAKUP_USLUG,
            ],
            [
                [
                    'price_for_buy_nds_id',
                ],
                'required',
                'when' => function (Product $model) {
                    return Yii::$app->user->identity->company->hasNds();
                },
                'whenClient' => 'function(){
                    return true;
                }',
                'on' => self::SCENARIO_ZAKUP_USLUG,
            ],
            [['production_type'], 'in', 'range' => [
                Product::PRODUCTION_TYPE_GOODS,
                Product::PRODUCTION_TYPE_SERVICE,
            ]],
            //[['price_for_buy_nds_id'], 'exist', 'skipOnError' => true, 'targetClass' => TaxRate::className(), 'targetAttribute' => ['price_for_buy_nds_id' => 'id']],
            //[['price_for_sell_nds_id'], 'exist', 'skipOnError' => true, 'targetClass' => TaxRate::className(), 'targetAttribute' => ['price_for_sell_nds_id' => 'id']],
            [['price_for_buy_nds_id'], 'in', 'range' => TaxRate::taxRatesIdArray()],
            [['price_for_sell_nds_id'], 'in', 'range' => TaxRate::taxRatesIdArray()],
            [['created_at', 'archived_at'], 'safe'],
//            [
//                [
//                    'creator_id', 'company_id',
//                    'title',
//                    'price_for_buy_nds_id', 'price_for_sell_nds_id',
//                    'price_for_buy_with_nds', 'price_for_sell_with_nds',
//                ], 'required',
//            ],
            //[['group_id'], 'required'],
            [
                ['product_unit_id'],
                'required',
                'when' => function (Product $product) {
                    return $product->production_type == Product::PRODUCTION_TYPE_GOODS;
                },
                'whenClient' => 'productIsGoods',
                'message' => 'Для типа продукции "товар" поле {attribute} является обязательным.',
            ],
            //    [
            //        ['quantity_in_stock'],
            //        'required',
            //        'when' => function (Product $product) {
            //            return $product->production_type == Product::PRODUCTION_TYPE_GOODS;
            //        },
            //        'whenClient' => 'productIsGoods',
            //        'message' => 'Для типа продукции "товар" поле {attribute} является обязательным.',
            //    ],
            [
                ['product_unit_id'],
                'in',
                'range' => ProductUnit::productUnitsIdArray(),
                'when' => function (Product $product) {
                    return $product->production_type == Product::PRODUCTION_TYPE_GOODS;
                },
                'whenClient' => 'productIsGoods',
                'message' => 'Для типа продукции "товар" поле {attribute} является обязательным.',
            ],
            [
                ['product_unit_id'],
                'in',
                'range' => ProductUnit::serviceUnitsIdArray(),
                'when' => function (Product $product) {
                    return $product->production_type == Product::PRODUCTION_TYPE_SERVICE;
                },
                'whenClient' => 'productIsService',
                'message' => 'Для типа продукции "услуга" поле {attribute} может иметь значение "шт" или "час".',
            ],
            [
                ['creator_id', 'company_id', 'has_excise', 'country_origin_id',
                    'price_for_buy_nds_id', 'price_for_sell_nds_id', 'category_id'
                ], 'integer',
            ],
            [['title', 'title_en'], 'string', 'max' => 700],
            //    ['quantity_in_stock', 'integer', 'min' => 0],
            [
                ['code', 'box_type', 'count_in_place', 'count_in_package', 'place_count', 'excise',
                    'customs_declaration_number', 'custom_field_value'
                ], 'string', 'max' => 45,
            ],
            [
                ['code_tn_ved'], 'string', 'max' => 13,
            ],
            [['price_for_buy_with_nds', 'price_for_sell_with_nds'],
                'number',
                'numberPattern' => Yii::$app->params['numberPattern'],
                'max' => Yii::$app->params['maxCashSum'] * 100, // with kopeck
                'tooBig' => 'Значение «{attribute}» не должно превышать ' . TextHelper::moneyFormat(Yii::$app->params['maxCashSum'], 2) . '.',
                'whenClient' => 'function(){}', // because of `123,00` - not valid float number (with comma) and there doesn't exist any methods to parse it on client.
            ],
            [['price_for_buy_with_nds', 'price_for_sell_with_nds'],
                'compare',
                'operator' => '>=',
                'compareValue' => 0,
                'whenClient' => 'function(){}', // because of `123,00` - not valid float number (with comma) and there doesn't exist any methods to parse it on client.
            ],
            [['box_type', 'count_in_place', 'count_in_package', 'place_count', 'mass_gross', 'mass_net', 'customs_declaration_number'], 'default', 'value' => self::DEFAULT_VALUE],
            [['is_deleted', 'not_for_sale', 'has_photo', 'is_traceable'], 'boolean'],
            [['mass_gross'], 'number', 'when' => function (Product $product) {
                return isset(ProductUnit::$countableUnits[$product->product_unit_id]) && $product->mass_gross != self::DEFAULT_VALUE;
            }, 'enableClientValidation' => false,],
            [['mass_net'], 'number', 'when' => function (Product $product) {
                return isset(ProductUnit::$countableUnits[$product->product_unit_id]) && $product->mass_net != self::DEFAULT_VALUE;
            }, 'enableClientValidation' => false,],
            [
                ['item_type_code'], 'string',
                'max' => 45,
                'when' => function (Product $product) {
                    return $product->production_type == Product::PRODUCTION_TYPE_GOODS;
                },
                'whenClient' => 'productIsGoods',
            ],
            [['initQuantity', 'irreducibleQuantity'], 'safe'],

            [['initQuantity', 'irreducibleQuantity'], 'filter', 'filter' => function ($value) {
                return str_replace(",", ".", $value);
            }],
            [
                ['provider_id'], 'exist', 'skipOnError' => true,
                'targetClass' => Contractor::class,
                'targetAttribute' => ['provider_id' => 'id'],
                'filter' => [
                    'and',
                    ['type' => Contractor::TYPE_SELLER],
                    [
                        'or',
                        ['company_id' => null],
                        ['company_id' => $this->company_id],
                    ]
                ]
            ],
            [['comment', 'comment_photo'], 'string'],
            [['article'], 'string', 'max' => 50],
            [['barcode'], 'string', 'max' => 30],
            [['image'], 'file', 'maxSize' => 3 * 1024 * 1024, 'mimeTypes' => 'image/*'],
            [['img_del'], 'boolean'],
            [['weight', 'volume'], 'filter', 'filter' => function($v) { return (is_numeric(self::sanitizeFloatValue($v))) ? self::sanitizeFloatValue($v) : $v; }],
            [['weight', 'volume'], 'number', 'min' => 0, 'max' => 9999999999.9999999999],
            [['category_id'], 'exist', 'skipOnError' => true, 'skipOnEmpty' => true, 'targetClass' => ProductCategory::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['one_c_imported', 'one_c_exported'], 'safe']
        ];
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if (strlen((string)$this->price_for_sell_with_nds) > 0) {
            $this->price_for_sell_with_nds = TextHelper::parseMoneyInput($this->price_for_sell_with_nds);
        }
        if (strlen((string)$this->price_for_buy_with_nds) > 0) {
            $this->price_for_buy_with_nds = TextHelper::parseMoneyInput($this->price_for_buy_with_nds);
        }
        if (strlen((string)$this->price_for_sell_with_nds) > 0) {
            if ($this->isAttributeChanged('price_for_sell_with_nds', false)) {
                $this->price_for_sell_with_nds = round($this->price_for_sell_with_nds * 100);
            }
        }
        if (strlen((string)$this->price_for_buy_with_nds) > 0) {
            if ($this->isAttributeChanged('price_for_buy_with_nds', false)) {
                $this->price_for_buy_with_nds = round($this->price_for_buy_with_nds * 100);
            }
        }
        $company = $this->company;
        if (empty($this->price_for_buy_nds_id)) {
            $this->price_for_buy_nds_id = TaxRate::RATE_WITHOUT;
        }

        if (empty($this->price_for_sell_nds_id)) {
            $this->price_for_sell_nds_id = TaxRate::RATE_WITHOUT;
        }

        if (!isset($this->group_id) || !$this->group_id || $this->group_id == 0) {
            $this->group_id = 1;
        }


        return parent::beforeValidate();
    }

    /**
     * @param bool $insert
     *
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isNewRecord) {
                $this->created_at = time();
                if (!isset($this->group_id)) {
                    $this->group_id = 1;
                }
            }

            if ($this->has_excise === null) {
                $this->has_excise = false;
            }
            if (!$this->has_excise) {
                $this->excise = 'Без акциза';
            }

            if ($this->production_type == self::PRODUCTION_TYPE_SERVICE) {
                $this->code = null;
                $this->box_type = $this->count_in_package = $this->count_in_place = $this->place_count = $this->mass_gross = $this->mass_net = $this->customs_declaration_number = self::DEFAULT_VALUE;
            }

            if ($this->not_for_sale === null) {
                $this->not_for_sale = false;
            }
            if ($this->not_for_sale) {
                $this->price_for_sell_with_nds = null;
            }

            if ($this->is_traceable === null) {
                $this->is_traceable = false;
            }

            if (!in_array($this->status, [self::DELETED, self::ACTIVE, self::ARCHIVE])) {
                $this->status = self::ACTIVE;
            }

            ModelHelper::HtmlEntitiesModelAttributes($this);

            return true;
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Дата создания',
            'creator_id' => 'Автор',
            'company_id' => 'Компания',
            'production_type' => 'Тип продукции',
            'title' => 'Наименование',
            'title_en' => 'Название на английском языке (Name in English)',
            'code' => 'Код',
            'code_tn_ved' => 'Код ТН ВЭД',
            'item_type_code' => 'Код вида товара',
            'product_unit_id' => 'Единица измерения',
            'initQuantity' => 'Количество на складе',
            'totalQuantity' => 'Количество на складе текущее',
            'group_id' => 'Группа товара',
            'box_type' => 'Вид упаковки',
            'count_in_package' => 'Количество в упаковке',
            'count_in_place' => 'Количество в одном месте',
            'place_count' => 'Количество мест, штук',
            'mass_gross' => 'Масса брутто',
            'mass_net' => 'Масса нетто',
            'has_excise' => 'Имеет акциз',
            'excise' => 'Акциз',
            'price_for_buy_nds_id' => 'Включая НДС',
            'price_for_buy_with_nds' => 'Цена покупки',
            'price_for_sell_nds_id' => 'Включая НДС',
            'price_for_sell_with_nds' => 'Цена продажи',
            'country_origin_id' => 'Страна происхождения товара',
            'customs_declaration_number' => '№ таможенной декларации',
            'dateStart' => 'Дата начала периода',
            'dateEnd' => 'Дата окончания периода',
            'not_for_sale' => 'Не для продажи',
            'provider_id' => 'Поставщик',
            'comment' => 'Комментарий',
            'image' => 'Изображение',
            'weight' => 'Вес (кг)',
            'volume' => 'Объем (м3)',
            'article' => 'Артикул',
            'barcode' => 'Штрих код',
            'status' => 'Статус',
            'custom_field_value' => 'Значение дополнительной колонки',
            'category_id' => 'Категория',
            'daysOfInventory' => 'Количество дней товарного запаса',
            'sellingSpeed' => 'Скорость продаж',
            'remainder' => 'Остаток',
            'lastOrderQuantity' => 'Количество последнего заказа',
            'unitProduct' => 'Еденица измерения',
            'is_traceable' => 'Товар подлежит прослеживаемости'
        ];
    }

    /**
     * @return \common\models\document\query\OrderQuery
     */
    public function getCompositeMaps()
    {
        return $this->hasMany(CompositeMap::className(), ['composite_product_id' => 'id']);
    }

    /**
     * @return \common\models\document\query\OrderQuery
     */
    public function getCompositeProducts()
    {
        return $this->hasMany(Product::className(), ['id' => 'composite_product_id'])->viaTable('{{%product_composite_map}}', [
            'simple_product_id' => 'id',
        ])->andOnCondition([
            'unit_type' => self::UNIT_TYPE_COMPOSITE,
        ]);
    }

    /**
     * @return \common\models\document\query\OrderQuery
     */
    public function getCompositeServices()
    {
        return $this->getCompositeProducts()->andOnCondition([
            'production_type' => self::PRODUCTION_TYPE_SERVICE,
        ]);
    }

    /**
     * @return \common\models\document\query\OrderQuery
     */
    public function getSimpleProducts()
    {
        return $this->hasMany(Product::className(), ['id' => 'simple_product_id'])->via('compositeMaps')->andOnCondition([
            'unit_type' => self::UNIT_TYPE_SIMPLE,
        ]);
    }

    /**
     * @return \common\models\document\query\OrderQuery
     */
    public function getSimpleServices()
    {
        return $this->getSimpleProducts()->andOnCondition([
            'production_type' => self::PRODUCTION_TYPE_SERVICE,
        ]);
    }

    /**
     * @return \common\models\document\query\OrderQuery
     */
    public function getSimpleGoods()
    {
        return $this->getSimpleProducts()->andOnCondition([
            'production_type' => self::PRODUCTION_TYPE_GOODS,
        ]);
    }

    /**
     * @return \common\models\document\query\OrderQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['product_id' => 'id']);
    }

    /**
     * @return [\common\models\document\Invoice]
     */
    public function getInvoices()
    {
        return $this->hasMany(Invoice::className(), ['id' => 'invoice_id'])->via('orders');
    }

    /**
     * @return $this
     */
    public function getOrderDocuments()
    {
        return $this->hasMany(OrderDocument::className(), ['id' => 'order_document_id'])->via('orderDocumentProducts');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceLists()
    {
        return $this->hasMany(PriceList::className(), ['id' => 'price_list_id'])->via('orderPriceLists');
    }

    public function getOfdReceiptItems()
    {
        return $this->hasMany(OfdReceiptItem::className(), ['product_id' => 'id']);
    }

    /**
     * @return [\common\models\document\Invoice]
     */
    public function getInvoicesAuto()
    {
        return $this->hasMany(InvoiceAuto::className(), ['id' => 'invoice_id'])->via('orders');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSuppliers()
    {
        return $this->hasMany(Contractor::class, ['id' => 'supplier_id'])
            ->viaTable(ProductToSupplier::tableName(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCustomPrices()
    {
        return $this->hasMany(CustomPrice::class, ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountryOrigin()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_origin_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(ProductGroup::className(), ['id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(Employee::className(), ['id' => 'creator_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductUnit()
    {
        return $this->hasOne(ProductUnit::className(), ['id' => 'product_unit_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceForBuyNds()
    {
        return $this->hasOne(TaxRate::className(), ['id' => 'price_for_buy_nds_id'])
            ->from(['ndsBuy' => TaxRate::tableName()]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceForSellNds()
    {
        return $this->hasOne(TaxRate::className(), ['id' => 'price_for_sell_nds_id'])
            ->from(['ndsSell' => TaxRate::tableName()]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(ProductCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductStores()
    {
        return $this->hasMany(ProductStore::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductStoresDaily()
    {
        return $this->hasMany(ProductStoreDaily::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProvider()
    {
        return $this->hasOne(Contractor::className(), ['id' => 'provider_id']);
    }

    /**
     * @inheritdoc
     */
    public function getFile()
    {
        return $this->hasOne(File::className(), ['owner_id' => 'id'])
            ->onCondition(['owner_model' => Product::class,]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderPackingLists()
    {
        return $this->hasMany(OrderPackingList::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderDocumentProducts()
    {
        return $this->hasMany(OrderDocumentProduct::className(), ['product_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderPriceLists()
    {
        return $this->hasMany(PriceListOrder::className(), ['product_id' => 'id']);
    }

    public function getInitialBalance()
    {
        return $this->hasOne(ProductInitialBalance::className(), ['product_id' => 'id']);
    }

    /**
     * @param $fieldId
     * @return \yii\db\ActiveQuery
     */
    public function getCategoryItem($fieldId)
    {
        return $this->hasOne(ProductCategoryItem::className(), ['product_id' => 'id'])
            ->andOnCondition(['field_id' => $fieldId]);
    }

    /**
     * @return string
     */
    public function getPriceForBuyWithoutNds()
    {
        return CalculatorHelper::getPriceWithoutNds($this->price_for_buy_with_nds, $this->price_for_buy_nds_id);
    }

    /**
     * @return string
     */
    public function getPriceForSellWithoutNds()
    {
        return CalculatorHelper::getPriceWithoutNds($this->price_for_sell_with_nds, $this->price_for_sell_nds_id);
    }

    /**
     * @param $price
     * @param $taxRateId
     *
     * @return string
     */
    public function getPriceWithoutNds($price, $taxRateId)
    {
        $priceWithoutNds = $price;
        if ($taxRateId != TaxRate::RATE_WITHOUT) {
            /** @var TaxRate $taxRate */
            $taxRate = TaxRate::findOne(['id' => $taxRateId]);
            $priceWithoutNds = $priceWithoutNds / (1 + round($taxRate->rate, 2));
        }

        return $priceWithoutNds;
    }

    /**
     * @return null|static
     */
    public function getType()
    {
        return ProductType::findOne($this->production_type + 1);
    }

    /**
     * @return null|static
     */
    public function getUnit()
    {
        return ProductUnit::findOne($this->product_unit_id);
    }

    /**
     * @param integer $productionType
     * @param integer $companyId
     *
     * @param string $searchProductTitle
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getProductsByType($productionType, $companyId, $searchProductTitle)
    {
        return self::find()
            ->byCompany($companyId)
            ->byDeleted(false)
            ->andwhere(['production_type' => $productionType])
            ->andWhere(['like', 'title', $searchProductTitle])
            ->asArray()
            ->all();
    }

    /**
     * @param $productionType
     * @return int|string
     */
    public static function getCountProductsByTypeCount($productionType)
    {
        return self::find()
            ->byCompany(Yii::$app->user->identity->company->id)
            ->byDeleted()
            ->andWhere(['production_type' => $productionType])
            ->count();
    }

    /**
     * @param $idArray
     * @param $companyId
     * @param bool|false $toOrderDocument
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getProductsByIds($idArray, $companyId, $toOrderDocument = false, $type = 2, $contractorId = null)
    {
        $products = $this->find()
            ->select([
                Product::tableName() . '.*',
                'SUM(product_store.quantity) * 1 as totalQuantity',
            ])
            ->joinWith([
                'priceForSellNds',
                'priceForBuyNds',
                'productUnit',
                'productStores',
            ])
            ->byCompany($companyId)
            ->byDeleted(false)
            ->andWhere([$this->tableName() . '.id' => $idArray])
            ->groupBy($this->tableName() . '.id')
            ->orderBy('product.title')
            ->asArray()
            ->all();

        foreach ($products as $key => $product) {
            $products[$key]['group_name'] = ($group = ProductGroup::findOne($product['group_id'])) ? $group->title : 'Без группы';
            if ($product['category_id']) {
                $products[$key]['categoryColumnValues'] = ProductCategoryItem::find()
                    ->where(['product_id' => $product['id']])
                    ->select('value')
                    ->indexBy('field_id')
                    ->column();
            }

            if ($contractorId !== null) {
                /** @var ProductToSupplier $productToSupplier */
                $productToSupplier = ProductToSupplier::find()
                    ->andWhere(['product_id' => $product['id']])
                    ->andWhere(['supplier_id' => $contractorId])
                    ->one();
                if ($productToSupplier !== null) {
                    $products[$key]['price_for_buy_with_nds'] = $productToSupplier->price;
                }
            }
        }

        if (!$toOrderDocument) {
            return $products;
        }

        foreach ($products as $key => $product) {
            $reserve = OrderDocumentProduct::find()
                    ->joinWith('orderDocument')
                    ->andWhere(['and',
                        [OrderDocument::tableName() . '.company_id' => $companyId],
                        [OrderDocument::tableName() . '.is_deleted' => false],
                        ['in', OrderDocument::tableName() . '.status_id', [OrderDocumentStatus::STATUS_NEW,
                            OrderDocumentStatus::STATUS_RECEIVED, OrderDocumentStatus::STATUS_SEND,
                            OrderDocumentStatus::STATUS_CONFIRMED, OrderDocumentStatus::STATUS_IN_PROCESSING,
                            OrderDocumentStatus::STATUS_ASSEMBLED,]],
                        [OrderDocumentProduct::tableName() . '.product_id' => $product['id']],
                    ])->sum('quantity') * 1;
            $available = (Documents::IO_TYPE_OUT == $type ? ($product['totalQuantity'] - $reserve) : ($product['totalQuantity'] + $reserve));
            $products[$key]['reserve'] = $reserve;
            $products[$key]['available'] = $available;
        }

        return $products;
    }

    /**
     * @return bool
     */
    public function _prepareFromInvoice()
    {
        if ($this->price_for_sell_with_nds == null) {
            $this->price_for_sell_with_nds = 0;
        } elseif ($this->price_for_buy_with_nds == null) {
            $this->price_for_buy_with_nds = 0;
        }

        return true;
    }

    /**
     * @param int $count
     * @param null $store_id
     * @return integer
     * @throws ForbiddenHttpException
     */
    public function setCountOrderSale($count, $store_id = null)
    {
        if ($productStore = $this->getProductStoreByStore($store_id)) {
            $productStore->quantity = $productStore->quantity - $count;
        }

        if ($productStoreDaily = $this->getProductStoreDailyByStore($store_id)) {
            $productStoreDaily->subtract($count);
        }

        return $productStore && $productStore->save(false) ? $productStore->quantity : 0;
    }

    /**
     * @param int $count
     * @param null $store_id
     * @return integer
     */
    public function setCountOrderBuy($count, $store_id = null)
    {
        if ($productStore = $this->getProductStoreByStore($store_id)) {
            $productStore->quantity = $productStore->quantity + $count;
        }

        if ($productStoreDaily = $this->getProductStoreDailyByStore($store_id)) {
            $productStoreDaily->addition($count);
        }

        return $productStore && $productStore->save(false) ? $productStore->quantity : 0;
    }

    /**
     * @param $store_id
     * @return ProductStoreDaily|ActiveRecord|null
     */
    public function getProductStoreByStore($store_id = null)
    {
        if ($this->company) {
            $store = $this->company->getStore($store_id)->one();
            if ($store) {
                $productStore = ProductStore::findOne([
                    'product_id' => $this->id,
                    'store_id' => $store->id,
                ]);
                if ($productStore === null) {
                    $productStore = new ProductStore([
                        'product_id' => $this->id,
                        'store_id' => $store->id,
                    ]);
                }

                return $productStore;
            }
        }

        return null;
    }

    /**
     * @param null $store_id
     * @return ProductStoreDaily|ActiveRecord
     * @throws ForbiddenHttpException
     * @throws \yii\base\Exception
     */
    public function getProductStoreDailyByStore($store_id = null)
    {
        if ($this->company) {
            $store = $this->company->getStore($store_id)->one();
            if ($store) {
                return ProductStoreDaily::initial($this->id, $store->id, $this->company);
            }

            throw new ForbiddenHttpException('Доступ к некоторым функциям недоступен');
        }

        throw new ForbiddenHttpException('Доступ к некоторым функциям недоступен');
    }

    /**
     * @param bool|false $start
     * @return bool|mixed
     */
    public function getBalance($start = false)
    {
        $balance = $this->getBuySale($start, true) - $this->getBuySale($start, false);
        ($start) ? $date = $this->dateStart : $date = $this->dateEnd;
        $begin_count = ($this->totalInitCount !== null && $this->created_at <= $date->getTimestamp()) ? $this->totalInitCount : 0;

        return $balance + $begin_count;
    }

    /**
     * @param bool|false $start
     * @return bool|mixed
     */
    public function getQuantityStatisticForPeriod(\DateTimeInterface $fromDate, \DateTimeInterface $tillDate)
    {
        $from = $fromDate->format('Y-m-d');
        $till = $tillDate->format('Y-m-d');
        $key = $from.'_'.$till;
        if (!isset($this->_data['getStatisticForPeriod'][$key])) {
            $data = ProductTurnover::find()->andWhere([
                'company_id' => $this->company_id,
                'product_id' => $this->id,
                'is_invoice_actual' => 1,
                'is_document_actual' => 1,
            ])->andWhere([
                '<=', 'date', $till,
            ])->select([
                'purchase1' => 'SUM(IF([[date]]<:date AND [[type]]<>2, [[quantity]], 0))',
                'sale1' => 'SUM(IF([[date]]<:date AND [[type]]=2, [[quantity]], 0))',
                'purchase2' => 'SUM(IF([[date]]>=:date AND [[type]]<>2, [[quantity]], 0))',
                'sale2' => 'SUM(IF([[date]]>=:date AND [[type]]=2, [[quantity]], 0))',
            ])->addParams([
                ':date' => $from,
            ])->groupBy('product_id')->asArray()->one() ?: [
                'purchase1' => 0,
                'sale1' => 0,
                'purchase2' => 0,
                'sale2' => 0,
            ];
            $data['balance1'] = $data['purchase1'] - $data['sale1'];
            $data['balance2'] = $data['purchase2'] - $data['sale2'];
            $data['balanceTotal'] = $data['balance1'] + $data['balance2'];
            $this->_data['getStatisticForPeriod'][$key] = $data;
        }

        return $this->_data['getStatisticForPeriod'][$key];
    }

    /**
     * @param bool|false $buy
     * @return bool|mixed
     */
    public function getTurnOver($buy = false)
    {
        $result = $this->getBuySale(false, $buy) - $this->getBuySale(true, $buy);
        $begin_count = ($buy && $this->totalInitCount !== null && $this->created_at <= $this->dateEnd->getTimestamp() && $this->created_at >= $this->dateStart->getTimestamp()) ? $this->totalInitCount : 0;

        return $result + $begin_count;
    }

    /**
     * @param bool|false $start
     * @param bool|false $buy
     * @return bool|mixed
     */
    protected function getBuySale($start = false, $buy = false)
    {
        ($start) ? $date = $this->dateStart : $date = $this->dateEnd;
        ($query) ? $type = 1 : $type = 2;
        $select = ['order.quantity'];
        $where = [
            'and',
            ['order.product_id' => $this->id,],
            ['invoice.type' => $type],
            ['<=', 'document.document_date', $date->format('Y-m-d')]
        ];
        $query = new Query;
        $query->from(['t' => ProductTurnoverSearch::turnoverQuery($select, $where)]);

        return $query->sum('quantity');
    }

    /**
     * @return string
     */
    public function getDocumentMinDate()
    {
        $select = ['document.document_date'];
        $where = [
            'and',
            ['order.product_id' => $this->id]
        ];
        $query = new Query;
        $query->from(['t' => ProductTurnoverSearch::turnoverQuery($select, $where)]);

        return $query->min('document_date') ?: date('Y-m-d');
    }

    protected function getAllBuySale($start = false, $buy = false, $idArray = null, $count = true, $productionType = 1)
    {
        $key = \yii\helpers\Json::encode(func_get_args());
        if (!isset($this->_data['getAllBuySale'][$key])) {
            ($start) ? $date = $this->dateStart : $date = $this->dateEnd;
            ($buy) ? $type = 1 : $type = 2;
            $select = $count ?
                ['`order`.`quantity` as total'] :
                ['`order`.`quantity` * `product`.`price_for_buy_with_nds` as total'];
            $where = [
                'and',
                ['invoice.company_id' => $this->company_id],
                ['invoice.type' => $type],
                [($start) ? '<' : '<=', 'document.document_date', $date->format('Y-m-d')]
            ];
            if ($idArray !== null) {
                $where[] = ['order.product_id' => $idArray];
            }
            $query = new Query;
            $query->from(['t' => ProductTurnoverSearch::turnoverQuery($select, $where, $productionType)]);

            $this->_data['getAllBuySale'][$key] = $query->sum('total') ?: 0;
        }

        return $this->_data['getAllBuySale'][$key];
    }

    protected function getAllBuySaleSellPrice($start = false, $buy = false, $idArray = null, $count = true, $productionType = 1, $db = null)
    {
        ($start) ? $date = $this->dateStart : $date = $this->dateEnd;
        ($buy) ? $type = 1 : $type = 2;
        $select = $count ?
            ['`order`.`quantity` as total'] :
            ['`order`.`quantity` * `order_main`.`selling_price_with_vat` as total'];
        $where = [
            'and',
            ['invoice.company_id' => $this->company_id],
            ['invoice.type' => $type],
            [($start) ? '<' : '<=', 'document.document_date', $date->format('Y-m-d')]
        ];
        if ($idArray !== null) {
            $where[] = ['order.product_id' => $idArray];
        }
        $query = new Query;
        $query->from(['t' => ProductTurnoverSearch::turnoverQuery($select, $where, $productionType)]);

        return $query->sum('total', $db);
    }

    protected function getAllBuySalePurchasePrice($start = false, $buy = false, $idArray = null, $count = true, $productionType = 1, $db = null)
    {
        ($start) ? $date = $this->dateStart : $date = $this->dateEnd;
        ($buy) ? $type = 1 : $type = 2;
        $select = $count ?
            ['`order`.`quantity` as total'] :
            ['`order`.`quantity` * `order_main`.`purchase_price_with_vat` as total'];
        $where = [
            'and',
            ['invoice.company_id' => $this->company_id],
            ['invoice.type' => $type],
            [($start) ? '<' : '<=', 'document.document_date', $date->format('Y-m-d')]
        ];
        if ($idArray !== null) {
            $where[] = ['order.product_id' => $idArray];
        }
        $query = new Query;
        $query->from(['t' => ProductTurnoverSearch::turnoverQuery($select, $where, $productionType)]);

        return $query->sum('total', $db);
    }

    /**
     * @return null
     */
    public function getDateStart()
    {
        if ($this->_dateStart === null) {
            $date = StatisticPeriod::getSessionPeriod();
            $this->_dateStart = new DateTime($date['from']);
            $this->_dateEnd = new DateTime($date['to']);
        }

        return $this->_dateStart;
    }

    /**
     * @param DateTime $value
     */
    public function setDateStart(DateTime $value)
    {
        $this->_dateStart = $value;
    }

    /**
     * @return null
     */
    public function getDateEnd()
    {
        if ($this->_dateEnd === null) {
            $date = StatisticPeriod::getSessionPeriod();
            $this->_dateStart = new DateTime($date['from']);
            $this->_dateEnd = new DateTime($date['to']);
        }

        return $this->_dateEnd;
    }

    /**
     * @param DateTime $value
     */
    public function setDateEnd(DateTime $value)
    {
        $this->_dateEnd = $value;
    }


    /**
     * @return bool|mixed
     */
    public function getbalanceStart()
    {
        return $this->getBalance(true);
    }

    /**
     * @return bool|mixed
     */
    public function getbalanceEnd()
    {
        return $this->getBalance(false);
    }

    /**
     * @return bool|mixed
     */
    public function getturnBuy()
    {
        return $this->getTurnOver(true);
    }

    /**
     * @return bool|mixed
     */
    public function getturnSale()
    {
        return $this->getTurnOver(false);
    }

    public function balanceAtDate($start = false, $idArray = null, $asCount = true)
    {
        $buy = $this->getAllBuySale($start, true, $idArray, $asCount);
        $sale = $this->getAllBuySale($start, false, $idArray, $asCount);
        $begin = $this->beginCount($start, $idArray, $asCount);

        return $buy - $sale + $begin;
    }

    public function turnAtPeriod($buy = false, $idArray = null, $asCount = true, $productionType = 1)
    {
        $buyStart = $this->getAllBuySale(true, $buy, $idArray, $asCount, $productionType);
        $buyEnd = $this->getAllBuySale(false, $buy, $idArray, $asCount, $productionType);
        if ($productionType == 1) {
            $beginCountStart = $this->beginCount(true, $idArray, $asCount);
            $beginCountEnd = $this->beginCount(false, $idArray, $asCount);
            $beginCount = ($buy) ? $beginCountEnd - $beginCountStart : 0;
        } else {
            $beginCount = 0; // for services
        }

        return $buyEnd - $buyStart + $beginCount;
    }

    public function marginAtPeriod($idArray = null, $asCount = true, $productionType = 1, $db = null)
    {
        $buyStart = $this->getAllBuySale(true, false, $idArray, $asCount, $productionType, $db);
        $buyEnd = $this->getAllBuySale(false, false, $idArray, $asCount, $productionType, $db);
        $sellStart = $this->getAllBuySaleSellPrice(true, false, $idArray, $asCount, $productionType, $db);
        $sellEnd = $this->getAllBuySaleSellPrice(false, false, $idArray, $asCount, $productionType, $db);
        if ($buyEnd - $buyStart > 0)
            return (($sellEnd - $sellStart) - ($buyEnd - $buyStart));

        return 0;
    }

    public function turnAtPeriodSellPrice($buy = false, $idArray = null, $asCount = true, $productionType = 1, $db = null)
    {
        $buyStart = $this->getAllBuySaleSellPrice(true, $buy, $idArray, $asCount, $productionType, $db);
        $buyEnd = $this->getAllBuySaleSellPrice(false, $buy, $idArray, $asCount, $productionType, $db);
        if ($productionType == 1) {
            $beginCountStart = $this->beginCount(true, $idArray, $asCount, $db);
            $beginCountEnd = $this->beginCount(false, $idArray, $asCount, $db);
            $beginCount = ($buy) ? $beginCountEnd - $beginCountStart : 0;
        } else {
            $beginCount = 0; // for services
        }

        return $buyEnd - $buyStart + $beginCount;
    }

    public function turnAtPeriodPurchasePrice($buy = false, $idArray = null, $asCount = true, $productionType = 1, $db = null)
    {
        $buyStart = $this->getAllBuySalePurchasePrice(true, $buy, $idArray, $asCount, $productionType, $db);
        $buyEnd = $this->getAllBuySalePurchasePrice(false, $buy, $idArray, $asCount, $productionType, $db);
        if ($productionType == 1) {
            $beginCountStart = $this->beginCount(true, $idArray, $asCount, $db);
            $beginCountEnd = $this->beginCount(false, $idArray, $asCount, $db);
            $beginCount = ($buy) ? $beginCountEnd - $beginCountStart : 0;
        } else {
            $beginCount = 0; // for services
        }

        return $buyEnd - $buyStart + $beginCount;
    }

    protected function beginCount($start = false, $idArray = null, $asCount = true, $db= null)
    {
        $key = \yii\helpers\Json::encode(func_get_args());
        if (!isset($this->_data['beginCount'][$key])) {
            ($start) ? $date = $this->dateStart : $date = $this->dateEnd;
            $count = Product::find()
                ->joinWith('productStores store')
                ->leftJoin(['pib' => ProductInitialBalance::tableName()], 'product.id = pib.product_id')
                ->byUser()
                ->where([
                    'product.company_id' => $this->company_id,
                    'product.production_type' => 1,
                ])
                ->andWhere([($start) ? '<' : '<=', 'pib.date', $date->format('Y-m-d')]);
            if ($idArray !== null) {
                $count->andWhere(['product.id' => $idArray]);
            }

            $this->_data['beginCount'][$key] = $count->sum($asCount ?
                    '`store`.`initial_quantity`' :
                    '`store`.`initial_quantity` * `product`.`price_for_buy_with_nds`') * 1;

        }

        return $this->_data['beginCount'][$key];
    }

    /**
     * @param $orderArray
     * @param Company $company
     * @param $productType
     * @return array
     */
    public static function createProductByLanding($orderArray, Company $company, $productType)
    {
        $productsArray = [];
        foreach ($orderArray as $productArray) {
            $product = new Product([
                'production_type' => $productType,
                'company_id' => $company->id,
                'country_origin_id' => Country::COUNTRY_WITHOUT,
                'title' => $productArray['title'],
                'product_unit_id' => $productArray['product_unit_id'],
                'price_for_sell_nds_id' => $company->companyTaxationType->osno ? $productArray['price_for_sell_nds_id'] : TaxRate::RATE_WITHOUT,
                'price_for_sell_with_nds' => $productArray['priceOneWithVat'],
                'price_for_buy_nds_id' => $company->companyTaxationType->osno ? $productArray['price_for_sell_nds_id'] : TaxRate::RATE_WITHOUT,
                'price_for_buy_with_nds' => 0,
                'scenario' => Product::SCENARIO_CREATE,
                'mass_gross' => 1,
            ]);
            $product->initQuantity = $productArray['count'];
            if (!$product->save()) {
                return [
                    'result' => false,
                    'errors' => $product->getErrors(),
                ];
            } else {
                $productsArray[(int)$product->id] = $productArray['count'];
            }
        }

        return [
            'result' => true,
            $productsArray,
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();

        if ($this->weight !== null) {
            $this->weight += 0;
        }
        if ($this->volume !== null) {
            $this->volume += 0;
        }
        if ($this->not_for_sale) {
            $this->price_for_sell_with_nds = null;
        }
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert && $this->company) {
            switch ($this->production_type) {
                case self::PRODUCTION_TYPE_GOODS:
                    \common\models\company\CompanyFirstEvent::checkEvent($this->company, 5);
                    break;
                case self::PRODUCTION_TYPE_SERVICE:
                    \common\models\company\CompanyFirstEvent::checkEvent($this->company, 6);
                    break;
            }
        }

        if ($this->production_type == self::PRODUCTION_TYPE_GOODS) {
            /* @var $productStore ProductStore */
            if (is_array($this->_initQuantity)) {
                $storeIds = $this->company->getStores()->select('id')->column();
                foreach ($storeIds as $store_id) {
                    $productStore = $this->getProductStoreByStore($store_id);
                    $oldValue = $productStore->initial_quantity;
                    $newValue = empty($this->_initQuantity[$store_id]) ? 0 : (float)$this->_initQuantity[$store_id];
                    $productStore->quantity = $productStore->quantity + ($newValue - $oldValue);
                    $productStore->initial_quantity = $newValue;
                    $productStore->save(false);

                    $productStoreDaily = $this->getProductStoreDailyByStore($store_id);
                    $quantityValue = empty($this->_initQuantity[$store_id]) ? 0 : (float)$this->_initQuantity[$store_id];
                    $productStoreDaily->addition(($quantityValue - $productStore->initial_quantity));
                    $productStoreDaily->save(false);
                }
            }
            if (is_array($this->_irreducibleQuantity)) {
                $storeIds = $this->company->getStores()->select('id')->column();
                foreach ($storeIds as $store_id) {
                    $productStore = $this->getProductStoreByStore($store_id);
                    $newValue = empty($this->_irreducibleQuantity[$store_id]) ? 0 : $this->_irreducibleQuantity[$store_id];
                    $productStore->irreducible_quantity = $newValue;
                    $productStore->save(false);

                    $productStoreDaily = $this->getProductStoreDailyByStore($store_id);
                    $irreducibleQuantity = empty($this->_irreducibleQuantity[$store_id]) ? 0 : $this->_irreducibleQuantity[$store_id];
                    $productStoreDaily->irreducible_quantity = $irreducibleQuantity;
                    $productStoreDaily->save(false);
                }
            }
        }


        $uploadPath = $this->getUploadPath();
        $oldFiles = file_exists($uploadPath) && ($this->img_del || $this->image instanceof UploadedFile) ?
            FileHelper::findFiles($uploadPath) :
            [];

        if ($this->image instanceof UploadedFile) {
            $file = $uploadPath . DIRECTORY_SEPARATOR . uniqid() . '.' . $this->image->getExtension();
            if (!file_exists($uploadPath)) {
                FileHelper::createDirectory($uploadPath);
            }
            $this->image->saveAs($file);
        }
        if ($oldFiles) {
            foreach ($oldFiles as $oldFile) {
                @unlink($oldFile);
            }
        }
        $hasPhoto = (int)($this->getUploadedImage() || $this->hasUploadedPictures());
        Yii::$app->db->createCommand("UPDATE `product` SET `has_photo` = {$hasPhoto} WHERE `product`.`id` = {$this->id};")->execute();
    }

    /**
     * @return array
     */
    public function getQuantity($store_id = null)
    {
        if ($this->_quantity === null) {
            $this->_quantity = $this->getProductStores()->select([
                'q' => 'TRIM(TRAILING "." FROM TRIM(TRAILING "0" FROM [[quantity]]))',
            ])->indexBy('store_id')->column();
        }

        if ($store_id && is_array($store_id)) {
            $_quantity = 0;
            foreach ($store_id as $id)
                $_quantity += (isset($this->_quantity[$id]) ? ($this->_quantity[$id] + 0) : 0);

            return $_quantity;
        }

        if ($store_id) {
            return (isset($this->_quantity[$store_id])) ? ($this->_quantity[$store_id] + 0) : 0;
        }

        return $this->_quantity;
    }

    /**
     * @return number
     */
    public function getTotalQuantity()
    {
        return array_sum($this->getQuantity());
    }

    /**
     * @param array $value
     */
    public function setInitQuantity($value)
    {
        if (!is_array($value)) {
            $store_id = $this->company->getStores()->select('id')->andWhere(['is_main' => true])->scalar();
            $this->_initQuantity[$store_id] = $value;
        } else {
            $this->_initQuantity = $value;
        }
    }

    /**
     * @param array $value
     */
    public function setIrreducibleQuantity($value)
    {
        if (!is_array($value)) {
            $store_id = $this->company->getStores()->select('id')->andWhere(['is_main' => true])->scalar();
            $this->_irreducibleQuantity[$store_id] = $value;
        } else {
            $this->_irreducibleQuantity = $value;
        }
    }

    /**
     * @param null $store_id
     * @return array|int
     */
    public function getInitQuantity($store_id = null)
    {
        if ($this->_initQuantity === null) {
            $this->_initQuantity = $this->getProductStores()->select([
                'q' => 'TRIM(TRAILING "." FROM TRIM(TRAILING "0" FROM [[initial_quantity]]))',
            ])->indexBy('store_id')->column();
        }

        if ($store_id) {
            return (isset($this->_initQuantity[$store_id])) ? ((float)$this->_initQuantity[$store_id] + 0) : 0;
        }

        return $this->_initQuantity;
    }

    /**
     * @param null $store_id
     * @return array|int
     */
    public function getIrreducibleQuantity($store_id = null, $db = null)
    {
        if ($this->_irreducibleQuantity === null) {
            $this->_irreducibleQuantity = $this->getProductStores()->select(['irreducible_quantity'])->indexBy('store_id')->column($db);
        }

        foreach ($this->_irreducibleQuantity as $key => $value) {
            $this->_irreducibleQuantity[$key] = is_numeric($value) ? $value * 1 : 0;
        }

        if ($store_id) {
            return (isset($this->_irreducibleQuantity[$store_id])) ? $this->_irreducibleQuantity[$store_id] : 0;
        }

        return $this->_irreducibleQuantity;
    }

    /**
     * @return number
     */
    public function getTotalInitQuantity()
    {
        return array_sum($this->getInitQuantity());
    }

    /**
     * @return Query
     */
    public static function reserveQuery($company_id, $product_id = null)
    {
        $query = new Query;
        $subQuery1 = OrderDocumentProduct::find()->alias('odp')->joinWith(['orderDocument od', 'orderDocument.invoice'], false)
            ->select(['odp.product_id', 'count' => 'odp.quantity'])
            ->andWhere(['od.company_id' => $company_id])
            ->andWhere(['od.is_deleted' => false])
            ->andWhere(['not', ['od.status_id' => [OrderDocumentStatus::STATUS_RETURN, OrderDocumentStatus::STATUS_CANCELED]]])
            ->andWhere([
                'or',
                ['od.invoice_id' => null],
                ['invoice.is_deleted' => true],
                ['invoice.invoice_status_id' => InvoiceStatus::STATUS_REJECTED],
            ]);
        $subQuery2 = Order::find()->alias('or')
            ->select(['or.product_id', 'count' => 'or.reserve'])
            ->joinWith(['product pr', 'invoice invoice'], false)
            ->andWhere(['invoice.is_deleted' => false])
            ->andWhere(['invoice.invoice_status_id' => InvoiceStatus::$validInvoices])
            ->andWhere(['pr.company_id' => $company_id])
            ->andWhere(['pr.production_type' => Product::PRODUCTION_TYPE_GOODS]);

        if ($product_id !== null) {
            $subQuery1->andWhere(['odp.product_id' => $product_id]);
            $subQuery2->andWhere(['or.product_id' => $product_id]);
        }

        $query->select(['t.product_id', 'total' => 'SUM({{t}}.[[count]])'])
            ->from(['t' => $subQuery1->union($subQuery2, true)])
            ->groupBy('product_id');

        return $query;
    }

    /**
     * @return number
     */
    public function getReserveQuantity()
    {
        $query = self::reserveQuery($this->company_id, $this->id);

        return $query->sum('{{t}}.[[count]]');
    }

    /**
     * @return Query
     */
    public function getReserveDocuments()
    {
        $invoices = Invoice::find()->alias('invoice')
            ->joinWith(['orders order'], false)
            ->andWhere(['invoice.company_id' => $this->company_id])
            ->andWhere(['invoice.is_deleted' => false])
            ->andWhere(['invoice.invoice_status_id' => InvoiceStatus::$validInvoices])
            ->andWhere(['order.product_id' => $this->id])
            ->andWhere(['>', 'order.reserve', 0])
            ->groupBy('invoice.id')
            ->all();

        $orderDocuments = OrderDocument::find()->alias('order_document')
            ->joinWith(['orderDocumentProducts', 'invoice invoice'], false)
            ->andWhere(['order_document_product.product_id' => $this->id])
            ->andWhere(['order_document.company_id' => $this->company_id])
            ->andWhere(['order_document.is_deleted' => false])
            ->andWhere([
                'not', [
                    'order_document.status_id' => [
                        OrderDocumentStatus::STATUS_RETURN,
                        OrderDocumentStatus::STATUS_CANCELED
                    ]
                ]
            ])
            ->andWhere([
                'or',
                ['order_document.invoice_id' => null],
                ['invoice.is_deleted' => true],
                ['invoice.invoice_status_id' => InvoiceStatus::STATUS_REJECTED],
            ])
            ->groupBy('order_document.id')
            ->all();

        return [
            'invoices' => $invoices,
            'orderDocuments' => $orderDocuments,
        ];
    }

    /**
     * @inheritdoc
     */
    public function load($data, $formName = null)
    {
        $load = parent::load($data, $formName);
        $this->image = $formName === '' ?
            UploadedFile::getInstanceByName('image') :
            UploadedFile::getInstance($this, 'image');

        return $load || $this->image !== null;
    }

    /**
     * @inheritdoc
     */
    public function getImageThumb($width = 200, $height = 200, $mode = EasyThumbnailImage::THUMBNAIL_INSET)
    {
        if ($file = $this->getUploadedImage()) {
            return EasyThumbnailImage::thumbnailSrc($file, $width, $height, $mode);
        }

        return '';
    }

    /**
     * @inheritdoc
     */
    public function getUploadedImage()
    {
        if ($this->isNewRecord) {
            return null;
        }

        $files = [];
        $uploadPath = $this->getUploadPath();
        if (file_exists($uploadPath)) {
            $files = FileHelper::findFiles($uploadPath);
        }

        return reset($files);
    }

    /**
     * @inheritdoc
     */
    public function getUploadPath($id = null)
    {
        return Company::fileUploadPath($this->company_id) .
            DIRECTORY_SEPARATOR . self::$uploadFolder .
            DIRECTORY_SEPARATOR . ($id ?: $this->id);
    }

    /**
     * @param $models
     * @param $productionType
     * @return string
     */
    public static function generateXlsTable($models, $productionType)
    {
        $excel = new Excel();
        /* @var $userConfig Config */
        $userConfig = Yii::$app->user->identity->config;
        /* @var $currentEmployeeCompany EmployeeCompany */
        $currentEmployeeCompany = Yii::$app->user->identity->currentEmployeeCompany;

        return $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $models,
            'title' => ($productionType == self::PRODUCTION_TYPE_GOODS) ? 'Товары' : 'Услуги',
            'rangeHeader' => range('A', 'P'),
            'columns' => [
                $userConfig->product_article ? [
                    'attribute' => 'article',
                    'value' => function (Product $model) {
                        return $model->article;
                    }
                ] : null,
                [
                    'attribute' => 'title',
                    'value' => function (Product $model) {
                        return $model->title;
                    },
                ],
                $productionType == Product::PRODUCTION_TYPE_GOODS ? [
                    'attribute' => 'group_id',
                    'value' => function (Product $model) {
                        return $model->group->title ?? '';
                    },
                ] : null,
                $productionType == Product::PRODUCTION_TYPE_GOODS ? [
                    'attribute' => 'available',
                    'value' => function (Product $model) {
                        return $model->available * 1;
                    },
                ] : null,
                $productionType == Product::PRODUCTION_TYPE_GOODS ? [
                    'attribute' => 'reserve',
                    'value' => function (Product $model) {
                        return $model->reserve * 1;
                    },
                ] : null,
                $productionType == Product::PRODUCTION_TYPE_GOODS ? [
                    'attribute' => 'quantity',
                    'value' => function (Product $model) {
                        return $model->quantity * 1;
                    },
                ] : null,
                [
                    'attribute' => 'product_unit_id',
                    'value' => function (Product $model) {
                        return $model->productUnit->name ?? '';
                    },
                ],
                $productionType == Product::PRODUCTION_TYPE_GOODS && $userConfig->product_image ? [
                    'attribute' => 'image',
                    'image' => true,
                    'value' => function (Product $model) {
                        $thumb = $model->getImageThumb(100, 100);
                        return $thumb ?
                            Yii::getAlias('@frontend/web') . $thumb :
                            '';
                    },
                ] : null,
                $productionType == Product::PRODUCTION_TYPE_GOODS && $userConfig->product_comment ? [
                    'attribute' => 'comment_photo',
                    'value' => function (Product $model) {
                        return $model->comment_photo;
                    },
                ] : null,
                [
                    'attribute' => 'price_for_buy_with_nds',
                    'value' => function (Product $model) use ($currentEmployeeCompany) {
                        $canViewPriceForBuy = $currentEmployeeCompany->can_view_price_for_buy ||
                            (!$currentEmployeeCompany->can_view_price_for_buy &&
                                $currentEmployeeCompany->is_product_admin &&
                                $model->creator_id == $currentEmployeeCompany->employee_id);

                        return $canViewPriceForBuy ?
                            TextHelper::invoiceMoneyFormat($model->price_for_buy_with_nds, 2) :
                            Product::DEFAULT_VALUE;
                    },
                ],
                Yii::$app->user->identity->company->hasNds() ? [
                    'attribute' => 'price_for_buy_nds_id',
                    'value' => function (Product $model) {
                        return $model->priceForBuyNds->name;
                    },
                ] : null,
                $productionType == Product::PRODUCTION_TYPE_GOODS ? [
                    'attribute' => 'amountForBuy',
                    'value' => function (Product $model) use ($currentEmployeeCompany) {
                        $canViewPriceForBuy = $currentEmployeeCompany->can_view_price_for_buy ||
                            (!$currentEmployeeCompany->can_view_price_for_buy &&
                                $currentEmployeeCompany->is_product_admin &&
                                $model->creator_id == $currentEmployeeCompany->employee_id);

                        return $canViewPriceForBuy ?
                            TextHelper::invoiceMoneyFormat($model->amountForBuy, 2) :
                            Product::DEFAULT_VALUE;
                    },
                ] : null,
                [
                    'attribute' => 'price_for_sell_with_nds',
                    'value' => function (Product $model) {
                        return $model->not_for_sale ? 'Не для продажи' :
                            TextHelper::invoiceMoneyFormat($model->price_for_sell_with_nds, 2);
                    },
                ],
                Yii::$app->user->identity->company->hasNds() ? [
                    'attribute' => 'price_for_sell_nds_id',
                    'value' => function (Product $model) {
                        return $model->not_for_sale ? 'Не для продажи' :
                            ($model->priceForSellNds ? $model->priceForSellNds->name : '---');
                    },
                ] : null,
                $productionType == Product::PRODUCTION_TYPE_GOODS ? [
                    'attribute' => 'amountForSell',
                    'value' => function (Product $model) {
                        return TextHelper::invoiceMoneyFormat($model->amountForSell, 2);
                    },
                ] : null,
            ],
            'headers' => [
                'article' => 'Артикул',
                'title' => 'Наименование',
                'group_id' => 'Группа товара',
                'available' => 'Доступно',
                'reserve' => 'Резерв',
                'quantity' => 'Остаток',
                'product_unit_id' => 'Ед. измерения',
                'image' => 'Картинка',
                'comment_photo' => 'Описание',
                'price_for_buy_with_nds' => 'Цена покупки',
                'price_for_buy_nds_id' => 'НДС покупки',
                'amountForBuy' => 'Стоимость покупки',
                'price_for_sell_with_nds' => 'Цена продажи',
                'price_for_sell_nds_id' => 'НДС продажи',
                'amountForSell' => 'Стоимость продажи',
            ],
            'format' => 'Xlsx',
            'fileName' => ($productionType == self::PRODUCTION_TYPE_GOODS) ? 'Товары' : 'Услуги',
        ]);
    }

    /**
     * @param $searchModel
     * @param $dataProvider
     * @return string
     */
    public static function generateTurnoverXlsTable($searchModel, $dataProvider)
    {
        $productionType = $searchModel->production_type;
        $excel = new Excel();
        /* @var $currentEmployeeCompany EmployeeCompany */
        $currentEmployeeCompany = Yii::$app->user->identity->currentEmployeeCompany;
        $title = 'Оборот ' . ($productionType == self::PRODUCTION_TYPE_GOODS ? 'товаров' : 'услуг');

        return $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $dataProvider->models,
            'title' => $title,
            'rangeHeader' => range('A', 'G'),
            'columns' => [
                [
                    'attribute' => 'title',
                    'value' => function ($data) {
                        return ArrayHelper::getValue($data, 'title');
                    },
                ],
                [
                    'attribute' => 'group_id',
                    'value' => function ($data) use ($searchModel) {
                        return $searchModel->getGroupFilter()[ArrayHelper::getValue($data, 'group_id')] ?? '';
                    },
                    'visible' => ($productionType == Product::PRODUCTION_TYPE_SERVICE ? false : true),
                ],
                [
                    'attribute' => 'product_unit_id',
                    'value' => function ($data) use ($searchModel) {
                        return $searchModel->getUnitFilter()[ArrayHelper::getValue($data, 'product_unit_id')] ?? '';
                    },
                    'visible' => ($productionType == Product::PRODUCTION_TYPE_SERVICE || $searchModel->turnoverType == ProductSearch::TURNOVER_BY_AMOUNT ? false : true),
                ],
                [
                    'attribute' => 'balance_start',
                    'value' => function ($data) use ($searchModel) {
                        $result = ArrayHelper::getValue($data, 'balance_start') * 1;

                        return $searchModel->turnoverType == ProductSearch::TURNOVER_BY_AMOUNT ? TextHelper::invoiceMoneyFormat($result, 2) : $result;
                    },
                ],
                [
                    'attribute' => 'balance_in',
                    'value' => function ($data) use ($searchModel) {
                        $result = ArrayHelper::getValue($data, 'balance_in') * 1;

                        return $searchModel->turnoverType == ProductSearch::TURNOVER_BY_AMOUNT ? TextHelper::invoiceMoneyFormat($result, 2) : $result;
                    },
                ],
                [
                    'attribute' => 'balance_out',
                    'value' => function ($data) use ($searchModel) {
                        $result = ArrayHelper::getValue($data, 'balance_out') * 1;

                        return $searchModel->turnoverType == ProductSearch::TURNOVER_BY_AMOUNT ? TextHelper::invoiceMoneyFormat($result, 2) : $result;
                    },
                ],
                [
                    'attribute' => 'balance_end',
                    'value' => function ($data) use ($searchModel) {
                        $result = ArrayHelper::getValue($data, 'balance_end') * 1;

                        return $searchModel->turnoverType == ProductSearch::TURNOVER_BY_AMOUNT ? TextHelper::invoiceMoneyFormat($result, 2) : $result;
                    },
                ],
            ],
            'headers' => [
                'title' => 'Итого единиц ',
                'group_id' => 'Группа товара',
                'product_unit_id' => 'Ед.измерения',
                'balance_start' => 'Остаток на начало периода',
                'balance_in' => 'Закуплено',
                'balance_out' => 'Продано',
                'balance_end' => 'Остаток на конец периода',
            ],
            'format' => 'Xlsx',
            'fileName' => $title . '.xlsx',
        ]);
    }

    /**
     * Combine selected products to one
     *
     * @param array $ids Selected products id array
     */
    public function combineProductsToThis(array $ids)
    {
        $combinedList = [];

        $combined = Yii::$app->db->transaction(function (Connection $db) use ($ids, &$combinedList) {
            if ($ids) {
                $productArray = Product::find()->where([
                    'id' => $ids,
                    'company_id' => $this->company_id,
                ])->andWhere(['not', ['id' => $this->id]])->all();
                $ids = ArrayHelper::getColumn($productArray, 'id', false);

                if ($productArray) {
                    $thisStoreArray = $this->getProductStores()->indexBy('store_id')->all();
                    foreach ($productArray as $product) {
                        if ($product->product_unit_id != $this->product_unit_id) {
                            continue;
                        }
                        $productStoreArray = $product->getProductStores()->all();
                        foreach ($productStoreArray as $productStore) {
                            if ($productStore->initial_quantity != 0 || $productStore->quantity != 0) {
                                $thisStore = isset($thisStoreArray[$productStore->store_id]) ?
                                    $thisStoreArray[$productStore->store_id] :
                                    new ProductStore(['product_id' => $this->id, 'store_id' => $productStore->store_id]);
                                $thisStore->initial_quantity += $productStore->initial_quantity;
                                $thisStore->quantity += $productStore->quantity;
                                $thisStoreArray[$productStore->store_id] = $thisStore;
                            }
                            $productStore->delete();
                        }
                        $product->status = Product::DELETED;
                        $product->is_deleted = true;
                        if (!$product->save(true, ['status', 'is_deleted'])) {
                            $db->getTransaction()->rollBack();

                            return false;
                        }
                        $combinedList[] = $product->title;
                    }
                    foreach ($thisStoreArray as $thisStore) {
                        if (!$thisStore->save(false, ['initial_quantity', 'quantity'])) {
                            $db->getTransaction()->rollBack();

                            return false;
                        }
                    }

                    //Update orders of documents
                    Yii::$app->db->createCommand()->update(Order::tableName(), [
                        'product_id' => $this->id
                    ], ['product_id' => $ids])->execute();
                    Yii::$app->db->createCommand()->update(OrderAct::tableName(), [
                        'product_id' => $this->id
                    ], ['product_id' => $ids])->execute();
                    Yii::$app->db->createCommand()->update(OrderPackingList::tableName(), [
                        'product_id' => $this->id
                    ], ['product_id' => $ids])->execute();
                    Yii::$app->db->createCommand()->update(OrderUpd::tableName(), [
                        'product_id' => $this->id
                    ], ['product_id' => $ids])->execute();
                    Yii::$app->db->createCommand()->update(OrderDocumentProduct::tableName(), [
                        'product_id' => $this->id
                    ], ['product_id' => $ids])->execute();

                    //Update products of OutInvoice
                    $outIdExists = OutInvoiceProduct::find()->select('out_invoice_id')->distinct()->where([
                        'product_id' => $this->id,
                    ])->column();
                    $outIdArray = OutInvoiceProduct::find()->select('out_invoice_id')->distinct()->where([
                        'product_id' => $ids,
                    ])->andWhere(['not', ['out_invoice_id' => $outIdExists]])->column();
                    if ($outIdArray) {
                        $insert = [];
                        foreach ($outIdArray as $outId) {
                            $insert[] = [$outId, $this->id];
                        }
                        Yii::$app->db->createCommand()->batchInsert(OutInvoiceProduct::tableName(), [
                            'out_invoice_id',
                            'product_id',
                        ], $insert)->execute();
                    }
                    Yii::$app->db->createCommand()->delete(OutInvoiceProduct::tableName(), ['product_id' => $ids])->execute();

                    return true;
                }
            }

            $db->getTransaction()->rollBack();

            return false;
        });

        return $combined ? $combinedList : [];
    }

    /**
     * @return string Not emty if can not delete
     */
    public function getDeleteError()
    {
        if ($this->_deleteError === null) {
            $product = $this->production_type == self::PRODUCTION_TYPE_GOODS ? 'товары' : 'услуги';
            if ($this->getInvoices()->andWhere([Invoice::tableName() . '.is_deleted' => false])->exists()) {
                $this->_deleteError = "Вы не можете удалять {$product}, если они присутствуют в счете";
            } elseif ($this->getInvoicesAuto()->andWhere([Invoice::tableName() . '.is_deleted' => false])->exists()) {
                $this->_deleteError = "Вы не можете удалять {$product}, если они присутствуют в шаблоне автосчета";
            } elseif ($this->getOrderDocuments()->andWhere([OrderDocument::tableName() . '.is_deleted' => false])->exists()) {
                $this->_deleteError = "Вы не можете удалять {$product}, если они присутствуют в заказе";
            } elseif ($this->getPriceLists()->andWhere([PriceList::tableName() . '.is_deleted' => false])->exists()) {
                $this->_deleteError = "Вы не можете удалять {$product}, если они присутствуют в прайс-листе";
            } elseif ($this->getOfdReceiptItems()->exists()) {
                $this->_deleteError = "Вы не можете удалять {$product}, если они присутствуют в чеках";
            } else {
                $this->_deleteError = '';
            }
        }

        return $this->_deleteError;
    }

    /**
     * @return bool|false|int
     * @throws \yii\db\StaleObjectException
     */
    public function delete()
    {
        if ($error = $this->getDeleteError()) {
            $this->addError('id', $error);

            return false;
        }

        if ($this->getOrders()->exists() || $this->getOrderDocumentProducts()->exists() || $this->getOrderPriceLists()->exists()) {
            return $this->updateAttributes([
                'status' => Product::DELETED,
                'is_deleted' => true,
            ]);
        } else {
            return parent::delete();
        }
    }

    /**
     * @return integer
     */
    public function getPrice_for_buy_no_nds()
    {
        $rate = $this->price_for_buy_nds_id ? TaxRate::get($this->price_for_buy_nds_id)->rate : 0;

        return round($this->price_for_buy_with_nds / (floatval($rate) + 1));
    }

    /**
     * @return integer
     */
    public function getPrice_for_sell_no_nds()
    {
        $rate = $this->price_for_sell_nds_id ? TaxRate::get($this->price_for_sell_nds_id)->rate : 0;

        return round($this->price_for_sell_with_nds / (floatval($rate) + 1));
    }

    public function loadSuppliers($supplierData)
    {
        if (is_array($supplierData)) {
            foreach ($supplierData as $data) {
                $supplier_id = ArrayHelper::getValue($data, 'id');
                if ($pts = ProductToSupplier::findOne(['product_id' => $this->id, 'supplier_id' => $supplier_id])) {
                    $pts->price = bcmul(str_replace(',', '.', $data['price']), 100);
                    $pts->article = $data['article'];
                } else {
                    $pts = new ProductToSupplier();
                    $pts->product_id = $this->id;
                    $pts->supplier_id = $supplier_id;
                    $pts->price = bcmul(str_replace(',', '.', $data['price']), 100);
                    $pts->article = $data['article'];
                }

                $this->_loadedSuppliers[] = $pts;
            }
        }

        return true;
    }

    public function getLoadedInitialBalance()
    {
        return $this->_loadedInitialBalance;
    }

    public function loadInitialBalance($data)
    {
        $pb = ProductInitialBalance::findOne(['product_id' => $this->id]);

        if (!$pb) {
            $pb = new ProductInitialBalance([
                'product_id' => null,
                'product_unit_id' => null,
                'price' => 0,
                'date' => date('Y-m-d')
            ]);
        }

        $pb->load($data);

        $pb->product_unit_id = $this->product_unit_id;
        $this->_loadedInitialBalance = $pb;

        return true;
    }

    public function saveInitialBalance()
    {
        $pb = $this->_loadedInitialBalance;
        $required = is_array($this->initQuantity) && array_sum($this->initQuantity) > 0;
        if ($pb) {
            $pb->product_id = $this->id;
            if (!$pb->validate() && $required) {
                $pb->price = 0;
                $pb->date = date('Y-m-d', $this->created_at);
            }

            $pb->save(false);
            return true;
        }

        return false;
    }

    public function validateInitialBalance()
    {
        if (is_array($this->initQuantity) && array_sum($this->initQuantity) > 0) {

            $pb = $this->_loadedInitialBalance;
            if ($pb && !$pb->validate()) {

                $this->addError('loadedSuppliers', 'Неверное значение «Начальные остатки»');

                return false;
            }
        }

        return true;
    }

    public function validateSuppliers()
    {
        if (is_array($this->_loadedSuppliers)) {
            foreach ($this->_loadedSuppliers as $pts) {
                if (!$pts->validate()) {
                    if ($pts->getFirstError('price'))
                        $this->addError('loadedSuppliers', 'Неверное значение «Цена закупки» для поставщика ' . ($pts->supplier->getShortName()));
                    //elseif ($pts->getFirstError('article'))
                    //    $this->addError('loadedSuppliers', 'Неверное значение «Артикул» для поставщика ' . ($pts->supplier->getShortName()));
                    elseif ($pts->getFirstError('product_id'))
                        $this->addError('loadedSuppliers', 'Необходимо заполнить «Продукт»');
                    elseif ($pts->getFirstError('supplier_id'))
                        $this->addError('loadedSuppliers', 'Поставщик не найден');

                    return false;
                }
            }
        }

        return true;
    }

    public function saveSuppliers()
    {
        if (!$this->id)
            throw new NotFoundHttpException('Продукт не создан.');

        $removed = ArrayHelper::index($this->suppliers, 'id');
        if (is_array($this->_loadedSuppliers)) {
            foreach ($this->_loadedSuppliers as $pts) {
                if (isset($removed[$pts->supplier_id]))
                    unset($removed[$pts->supplier_id]);

                // for product insert
                $pts->product_id = $this->id;

                if (!$pts->save()) {
                    \common\components\helpers\ModelHelper::logErrors($pts, __METHOD__);

                    return false;
                }
            }
        }

        foreach ($removed as $data) {
            $supplier_id = ArrayHelper::getValue($data, 'id');
            if ($pts = ProductToSupplier::findOne(['product_id' => $this->id, 'supplier_id' => $supplier_id])) {
                $pts->delete();
            }
        }

        return true;
    }

    public function saveCategoryItems()
    {
        if (!$this->id)
            throw new NotFoundHttpException('Продукт не создан.');

        if (is_array($this->_loadedCategoryItems)) {
            foreach ($this->_loadedCategoryItems as $pci) {

                // for product insert
                $pci->product_id = $this->id;

                if (!$pci->save()) {
                    \common\components\helpers\ModelHelper::logErrors($pci, __METHOD__);

                    return false;
                }
            }
        }

        return true;
    }

    public function getLoadedSuppliers()
    {
        return $this->_loadedSuppliers;
    }

    public function getSuppliersData($suppliersIds, $productType = self::PRODUCTION_TYPE_GOODS)
    {
        if (!$this->id)
            return [];

        if ($productType == self::PRODUCTION_TYPE_GOODS) {
            $T = PackingList::tableName();
        } else {
            $T = Act::tableName();
        }
        $O = Order::tableName();
        $I = Invoice::tableName();

        $supplierData = [];
        foreach ($suppliersIds as $supplierId) {
            $query = new Query;
            $res = $query->select([
                "$T.id",
                "$I.contractor_id",
                "$T.document_date",
                "$O.product_id",
                "$O.base_price_no_vat",
                "$O.base_price_with_vat",
            ])->from($T)
                ->leftJoin($I, "{{%$I}}.[[id]] = {{%$T}}.[[invoice_id]]")
                ->leftJoin($O, "{{%$I}}.[[id]] = {{%$O}}.[[invoice_id]]")
                ->andWhere(["<>", "$I.invoice_status_id", InvoiceStatus::STATUS_REJECTED])
                ->andWhere(["$I.contractor_id" => $supplierId])
                ->andWhere(["$O.product_id" => $this->id])
                ->orderBy(["$T.document_date" => SORT_DESC])->limit(1)->one();

            if ($res)
                $supplierData[$res['contractor_id']] = [
                    'date' => DateHelper::format($res['document_date'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                    'price' => TextHelper::invoiceMoneyFormat($res['base_price_with_vat'], 2)
                ];
        }

        return $supplierData;
    }

    /**
     * @return int
     */
    public function getGrowingPriceForBuy()
    {
        if (!$this->id)
            return 0;

        $tUpd = Upd::tableName();
        $tAct = Act::tableName();
        $tPls = PackingList::tableName();
        $tInvoice = Invoice::tableName();
        $tOrder = Order::tableName();

        $upd = Upd::find()
            ->leftJoin($tInvoice, "$tInvoice.id = $tUpd.invoice_id")
            ->leftJoin($tOrder, "{{%$tInvoice}}.[[id]] = {{%$tOrder}}.[[invoice_id]]")
            ->where(["$tInvoice.company_id" => $this->company_id])
            ->andWhere(["<>", "$tInvoice.invoice_status_id", InvoiceStatus::STATUS_REJECTED])
            ->andWhere(["$tInvoice.is_deleted" => 0])
            ->andWhere(["$tOrder.product_id" => $this->id])
            ->select(["$tInvoice.type", "$tUpd.document_date", "$tUpd.created_at", "$tOrder.purchase_price_with_vat AS price", "$tOrder.quantity"]);
        $act = Act::find()
            ->leftJoin($tInvoice, "$tInvoice.id = $tAct.invoice_id")
            ->leftJoin($tOrder, "{{%$tInvoice}}.[[id]] = {{%$tOrder}}.[[invoice_id]]")
            ->where(["$tInvoice.company_id" => $this->company_id])
            ->andWhere(["<>", "$tInvoice.invoice_status_id", InvoiceStatus::STATUS_REJECTED])
            ->andWhere(["$tInvoice.is_deleted" => 0])
            ->andWhere(["$tOrder.product_id" => $this->id])
            ->select(["$tInvoice.type", "$tAct.document_date", "$tAct.created_at", "$tOrder.purchase_price_with_vat AS price", "$tOrder.quantity"]);
        $pls = PackingList::find()
            ->leftJoin($tInvoice, "$tInvoice.id = $tPls.invoice_id")
            ->leftJoin($tOrder, "{{%$tInvoice}}.[[id]] = {{%$tOrder}}.[[invoice_id]]")
            ->where(["$tInvoice.company_id" => $this->company_id])
            ->andWhere(["<>", "$tInvoice.invoice_status_id", InvoiceStatus::STATUS_REJECTED])
            ->andWhere(["$tInvoice.is_deleted" => 0])
            ->andWhere(["$tOrder.product_id" => $this->id])
            ->select(["$tInvoice.type", "$tPls.document_date", "$tPls.created_at", "$tOrder.purchase_price_with_vat AS price", "$tOrder.quantity"]);

        $pricesData = (new Query)->select(['type', 'document_date', 'created_at', 'price', 'quantity'])
            ->from($upd->union($act, true)->union($pls, true))
            ->orderBy(['document_date' => SORT_ASC, 'created_at' => SORT_ASC])
            ->all();

        if ($this->initialBalance && $this->initialBalance->price && is_array($this->_initQuantity) && array_sum($this->_initQuantity) > 0) {
            $growingQuantity = array_sum($this->_initQuantity);
            $growingPrice = $this->initialBalance->price;
        } else {
            $growingQuantity = 0;
            $growingPrice = $this->price_for_buy_with_nds;
        }

        foreach ($pricesData as $data) {

            if ($data['type'] == Documents::IO_TYPE_IN) {
                $growingPrice = ($growingQuantity * $growingPrice + $data['quantity'] * $data['price']) / ($growingQuantity + $data['quantity']);
                $growingQuantity += $data['quantity'];
            } else {
                $growingQuantity -= $data['quantity'];
            }

            if ($growingQuantity < 0)
                $growingQuantity = 0;
        }

        return (int)round($growingPrice);
    }

    /**
     * @param Invoice $invoice
     * @return bool
     */
    public static function updateGrowingPriceForBuy(Invoice $invoice)
    {
        foreach ($invoice->orders as $order) {
            /** @var Product $product */
            if ($product = Product::find()->where(['company_id' => $invoice->company_id, 'id' => $order->product_id])->one()) {
                $growingPrice = $product->getGrowingPriceForBuy();
                $product->updateAttributes(['price_for_buy_with_nds' => $growingPrice]);
            }
        }

        return true;
    }

    public function loadCustomPrices($pricesData)
    {
        if (is_array($pricesData)) {
            foreach ($pricesData as $data) {
                $group_id = ArrayHelper::getValue($data, 'id');
                if ($cp = CustomPrice::findOne(['product_id' => $this->id, 'price_group_id' => $group_id])) {
                    $cp->price = bcmul(str_replace(',', '.', $data['price']), 100);
                    $cp->nds_id = $data['nds_id'];
                } else {
                    $cp = new CustomPrice();
                    $cp->product_id = $this->id;
                    $cp->price_group_id = $group_id;
                    $cp->price = bcmul(str_replace(',', '.', $data['price']), 100);
                    $cp->nds_id = $data['nds_id'];
                }

                $this->_loadedCustomPrices[] = $cp;
            }
        }

        return true;
    }

    public function loadCategoryItems($categoryData)
    {
        if (is_array($categoryData)) {
            foreach ($categoryData as $categoryId => $fieldData) {
                foreach ($fieldData as $fieldId => $value) {
                    if ($ci = ProductCategoryItem::findOne(['product_id' => $this->id, 'field_id' => $fieldId])) {
                        $ci->value = $value;
                    } else {
                        $ci = new ProductCategoryItem();
                        $ci->product_id = $this->id;
                        $ci->field_id = $fieldId;
                        $ci->value = $value;
                    }

                    $this->_loadedCategoryItems[] = $ci;
                }
            }
        }

        return true;
    }

    public function validateCustomPrices()
    {
        if (is_array($this->_loadedCustomPrices)) {
            foreach ($this->_loadedCustomPrices as $cp) {
                if (!$cp->validate()) {
                    if ($cp->getFirstError('price'))
                        $this->addError('loadedCustomPrices', 'Неверное значение «Цена Р» для цены ' . $cp->priceGroup->name);
                    elseif ($cp->getFirstError('nds_id'))
                        $this->addError('loadedCustomPrices', 'Неверное значение «НДС» для цены ' . $cp->priceGroup->name);
                    elseif ($cp->getFirstError('price_group_id'))
                        $this->addError('loadedCustomPrices', 'Цена не найдена');

                    return false;
                }
            }
        }

        return true;
    }

    public function saveCustomPrices()
    {
        if (!$this->id)
            throw new NotFoundHttpException('Продукт не создан.');

        $removed = ArrayHelper::index($this->customPrices, 'price_group_id');
        if (is_array($this->_loadedCustomPrices)) {
            foreach ($this->_loadedCustomPrices as $cp) {
                if (isset($removed[$cp->price_group_id]))
                    unset($removed[$cp->price_group_id]);

                // for product insert
                $cp->product_id = $this->id;

                if (!$cp->save()) {
                    \common\components\helpers\ModelHelper::logErrors($cp, __METHOD__);

                    return false;
                }
            }
        }

        foreach ($removed as $data) {
            $group_id = ArrayHelper::getValue($data, 'price_group_id');
            if ($cp = CustomPrice::findOne(['product_id' => $this->id, 'price_group_id' => $group_id])) {
                $cp->delete();
            }
        }

        return true;
    }

    public function getLoadedCustomPrices()
    {
        return $this->_loadedCustomPrices;
    }

    public function savePictures($fromZeroDir = false)
    {
        // delete old
        $deletePics = Yii::$app->request->post('delete_pic');
        if (is_array($deletePics)) {
            foreach ($deletePics as $number => $delete) {
                if ($delete)
                    $this->deletePicture($number);
            }
        }

        // save new
        $picPath = $this->getUploadPath() . DIRECTORY_SEPARATOR;
        if ($fromZeroDir) {
            $tempPath = $this->getZeroUploadPath() . DIRECTORY_SEPARATOR . 'temp';
            if (!file_exists($picPath)) {
                FileHelper::createDirectory($picPath);
            }
        } else {
            $tempPath = $this->getUploadPath() . DIRECTORY_SEPARATOR . 'temp';
        }

        $tmpFiles = file_exists($tempPath) ? FileHelper::findFiles($tempPath) : [];
        if ($tmpFiles) {
            foreach ($tmpFiles as $file) {
                rename($file, $picPath . basename($file));
            }
        }

        $hasPhoto = (file_exists($picPath) && count(FileHelper::findFiles($picPath))) ? 1 : 0;
        Yii::$app->db->createCommand("UPDATE `product` SET `has_photo` = {$hasPhoto} WHERE `product`.`id` = {$this->id};")->execute();

        return true;
    }

    /**
     * @inheritdoc
     */
    public function getPictureThumb($number, $width = 200, $height = 200, $mode = EasyThumbnailImage::THUMBNAIL_INSET)
    {
        if (!$this->id)
            return '';

        $picPath = $this->getUploadPath() . DIRECTORY_SEPARATOR;
        $picFiles = file_exists($picPath) ? FileHelper::findFiles($picPath) : [];
        if ($picFiles) {
            foreach ($picFiles as $file) {
                $name_ext = explode('.', basename($file));
                $name = reset($name_ext);
                if ($name == "pic{$number}")
                    return EasyThumbnailImage::thumbnailSrc($file, $width, $height, $mode);
            }
        }

        return '';
    }

    public function hasUploadedPictures()
    {
        $picPath = $this->getUploadPath() . DIRECTORY_SEPARATOR;

        return file_exists($picPath) ? (bool)FileHelper::findFiles($picPath) : false;
    }

    /**
     * @inheritdoc
     */
    public function deletePicture($number)
    {
        $picPath = $this->getUploadPath() . DIRECTORY_SEPARATOR;
        $picFiles = file_exists($picPath) ? FileHelper::findFiles($picPath, ['recursive' => false]) : [];
        if ($picFiles) {
            foreach ($picFiles as $file) {
                $name_ext = explode('.', basename($file));
                $name = reset($name_ext);
                if ($name == "pic{$number}")
                    @unlink($file);
            }
        }

        return '';
    }

    /**
     * @inheritdoc
     */
    public function clearPicturesTempDir()
    {
        $tmpPath = $this->getUploadPath() . DIRECTORY_SEPARATOR . 'temp';
        $tmpFiles = file_exists($tmpPath) ? FileHelper::findFiles($tmpPath) : [];
        if ($tmpFiles) {
            foreach ($tmpFiles as $file) {
                @unlink($file);
            }
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function getZeroUploadPath()
    {
        return Company::fileUploadPath($this->company_id) .
            DIRECTORY_SEPARATOR . self::$uploadFolder .
            DIRECTORY_SEPARATOR . "0";
    }

    public function setDataFromCopiedProduct($id)
    {
        if ($copiedProduct = Product::findOne($id)) {
            $this->title = $copiedProduct->title;
            $this->group_id = $copiedProduct->group_id;
            $this->code = $copiedProduct->code;
            $this->article = $copiedProduct->article;
            $this->barcode = $copiedProduct->barcode;
            $this->product_unit_id = $copiedProduct->product_unit_id;
            $this->weight = $copiedProduct->weight;
            $this->volume = $copiedProduct->volume;
            $this->place_count = $copiedProduct->place_count;
            $this->count_in_package = $copiedProduct->count_in_package;
            $this->count_in_place = $copiedProduct->count_in_place;
            $this->mass_gross = $copiedProduct->mass_gross;
            $this->mass_net = $copiedProduct->mass_net;
            $this->box_type = $copiedProduct->box_type;
            $this->country_origin_id = $copiedProduct->country_origin_id;
            $this->customs_declaration_number = $copiedProduct->customs_declaration_number;
            $this->item_type_code = $copiedProduct->item_type_code;
            $this->custom_field_value = $copiedProduct->custom_field_value;
            $this->comment_photo = $copiedProduct->comment_photo;

            if ($copiedProduct->suppliers && ($supplier = $copiedProduct->suppliers[0])) {
                $pts = new ProductToSupplier;
                $pts->supplier_id = $supplier->id;
                $this->_loadedSuppliers[] = $pts;
            }
        }
    }

    /**
     * @return array
     */
    public static function getCategoryNames()
    {
        return ProductCategory::find()
            ->select('name')
            ->indexBy('id')
            ->orderBy(['sort' => SORT_ASC])
            ->column();
    }

    /**
     * @return array
     */
    public static function getCategoryFieldNames()
    {
        return [
            ProductCategory::TYPE_BOOK_PUBLISHER => ProductCategoryField::find()
                ->where(['category_id' => ProductCategory::TYPE_BOOK_PUBLISHER])
                ->select('name')
                ->indexBy('id')
                ->orderBy(['sort' => SORT_ASC])
                ->column()
        ];
    }

    /**
     * @return array
     */
    public function getCategoryFieldValues($fillLikeTemplateByCategory = null)
    {
        $values = ProductCategoryItem::find()
            ->where(['product_id' => $this->id])
            ->select('value')
            ->indexBy('field_id')
            ->column();

        if (empty($values) && $fillLikeTemplateByCategory) {
            $values = ProductCategoryField::find()
                ->where(['category_id' => $fillLikeTemplateByCategory])
                ->select(new Expression('"" AS value'))
                ->indexBy('id')
                ->orderBy(['sort' => SORT_ASC])
                ->column();
        }

        return $values;
    }

    public function getTitle_chunked($maxStrLen = 70)
    {
        $arr = explode(' ', (string)$this->title);
        foreach ($arr as $k => $str) {
            if (mb_strlen($str) > $maxStrLen) {
                $arr[$k] = implode(' ', mb_str_split($str, $maxStrLen));
            }
        }

        return implode(' ', $arr);
    }

    public function getBalanceOnDate($date = null)
    {
        if (!($date instanceof \DateTime)) {
            $date = date_create($date);
        }

        if (!$date) {
            return 0;
        }

        $date = $date->format('Y-m-d');

        if (!isset($this->_data['getBalanceOnDate'][$date])) {
            $query = (new \yii\db\Query())
                ->select('SUM(IF([[type]]=2,-[[quantity]],[[quantity]]))')
                ->from([
                    't' => ProductTurnover::tableName()
                ])
                ->andWhere([
                    't.product_id' => $this->id,
                    't.is_invoice_actual' => 1,
                    't.is_document_actual' => 1,
                ])
                ->andWhere([
                    '<=',
                    't.date',
                    date('Y-m-d'),
                ]);

            $this->_data['getBalanceOnDate'][$date] = $query->scalar() * 1;
        }

        return $this->_data['getBalanceOnDate'][$date];
    }

    public function getUnitType()
    {
        return self::$unitTypes[$this->unit_type] ?? null;
    }

    public function getSimpleServicesCostPrice()
    {
        if (!isset($this->data['getSimpleServicesCostPrice'])) {
            $this->_data['getSimpleServicesCostPrice'] = $this->getCompositeMaps()->joinWith('simpleProduct')->select([
                'cost_price' => 'SUM(IFNULL({{product_composite_map}}.[[quantity]],0)*IFNULL({{product}}.[[price_for_buy_with_nds]],0))',
            ])->andWhere([
                'product.production_type' => Product::PRODUCTION_TYPE_SERVICE,
            ])->scalar();
        }

        return $this->_data['getSimpleServicesCostPrice'];
    }

    public function getSimpleGoodsCostPrice()
    {
        if (!isset($this->data['getSimpleGoodsCostPrice'])) {
            $this->_data['getSimpleGoodsCostPrice'] = $this->getCompositeMaps()->joinWith('simpleProduct')->select([
                'cost_price' => 'SUM(IFNULL({{product_composite_map}}.[[quantity]],0)*IFNULL({{product}}.[[price_for_buy_with_nds]],0))',
            ])->andWhere([
                'product.production_type' => Product::PRODUCTION_TYPE_GOODS,
            ])->scalar();
        }

        return $this->_data['getSimpleGoodsCostPrice'];
    }

    public function getSimpleProductsCostPrice()
    {
        if (!isset($this->data['getSimpleProductsCostPrice'])) {
            $this->_data['getSimpleProductsCostPrice'] = $this->getCompositeMaps()->joinWith('simpleProduct')->select([
                'cost_price' => 'SUM(IFNULL({{product_composite_map}}.[[quantity]],0)*IFNULL({{product}}.[[price_for_buy_with_nds]],0))',
            ])->scalar();
        }

        return $this->_data['getSimpleProductsCostPrice'];
    }

    public static function sanitizeFloatValue($val)
    {
        return str_replace([',',' '], ['.',''], $val);
    }
}
