<?php

namespace common\models\product;

use Yii;

/**
 * This is the model class for table "price_list_checked_products".
 *
 * @property integer $id
 * @property integer $price_list_id
 * @property string $checked_products
 *
 * @property PriceList $priceList
 */
class PriceListCheckedProducts extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'price_list_checked_products';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['price_list_id', 'checked_products'], 'required'],
            [['price_list_id'], 'integer'],
            [['checked_products'], 'string'],
            [['price_list_id'], 'exist', 'skipOnError' => true, 'targetClass' => PriceList::className(), 'targetAttribute' => ['price_list_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'price_list_id' => 'Price List ID',
            'checked_products' => 'Checked Products',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceList()
    {
        return $this->hasOne(PriceList::className(), ['id' => 'price_list_id']);
    }
}
