<?php

namespace common\models\product;

use Yii;
use common\models\Company;
use common\models\Contractor;

/**
 * This is the model class for table "price_list_notification".
 *
 * @property int $id
 * @property string $uid
 * @property int $price_list_id
 * @property int|null $company_id
 * @property string|null $contractor_email
 * @property int|null $contractor_id
 * @property int $is_viewed
 * @property int $is_notification_viewed
 * @property int|null $created_at
 * @property int|null $viewed_at
 * @property int|null $notification_viewed_at
 *
 * @property Company $company
 * @property Contractor $contractor
 * @property PriceList $priceList
 */
class PriceListNotification extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'price_list_notification';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['uid', 'price_list_id'], 'required'],
            [['price_list_id', 'company_id', 'contractor_id', 'is_viewed', 'is_notification_viewed', 'created_at', 'viewed_at', 'notification_viewed_at'], 'integer'],
            [['uid'], 'string', 'max' => 16],
            [['contractor_email'], 'string', 'max' => 64],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['contractor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contractor::className(), 'targetAttribute' => ['contractor_id' => 'id']],
            [['price_list_id'], 'exist', 'skipOnError' => true, 'targetClass' => PriceList::className(), 'targetAttribute' => ['price_list_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uid' => 'Uid',
            'price_list_id' => 'Price List ID',
            'company_id' => 'Company ID',
            'contractor_email' => 'Contractor Email',
            'contractor_id' => 'Contractor ID',
            'is_viewed' => 'Is Viewed',
            'is_notification_viewed' => 'Is Notification Viewed',
            'created_at' => 'Created At',
            'viewed_at' => 'Viewed At',
            'notification_viewed_at' => 'Notification Viewed At',
        ];
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * Gets query for [[Contractor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getContractor()
    {
        return $this->hasOne(Contractor::className(), ['id' => 'contractor_id']);
    }

    /**
     * Gets query for [[PriceList]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPriceList()
    {
        return $this->hasOne(PriceList::className(), ['id' => 'price_list_id']);
    }

    /**
     * @param Company $company
     * @return string
     * @throws \Exception
     */
    public static function generateUid(Company $company)
    {
        $query = new \yii\db\Query;
        $query->select('id')->from(static::tableName())->where('[[uid]]=:uid')->andWhere(['company_id' => $company->id]);

        do {
            $uid = bin2hex(random_bytes(8));
        } while ($query->params([':uid' => $uid])->exists());

        return $uid;
    }
}
