<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "agreement_title_template".
 *
 * @property int $id
 * @property string $name
 *
 * @property Agreement[] $agreements
 */
class AgreementTitleTemplate extends \yii\db\ActiveRecord
{
    const TITLE_BY_TYPE = 1;
    const TITLE_BY_NAME = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'agreement_title_template';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreements()
    {
        return $this->hasMany(Agreement::className(), ['title_template_id' => 'id']);
    }
}
