<?php

namespace common\models;

use Yii;
use common\models\document\InvoiceIncomeItem;

/**
 * This is the model class for table "income_item_flow_of_funds".
 *
 * @property integer $id
 * @property integer $company_id
 * @property integer $income_item_id
 * @property integer $flow_of_funds_block
 * @property bool    $is_visible
 * @property bool    $is_prepayment
 *
 * @property Company $company
 * @property InvoiceIncomeItem $incomeItem
 */
class IncomeItemFlowOfFunds extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'income_item_flow_of_funds';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'income_item_id'], 'required'],
            [['company_id', 'income_item_id', 'flow_of_funds_block'], 'integer'],
            [['is_visible', 'is_prepayment'], 'boolean'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['income_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => InvoiceIncomeItem::className(), 'targetAttribute' => ['income_item_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'income_item_id' => 'Income Item ID',
            'flow_of_funds_block' => 'Flow Of Funds Block',
            'is_visible' => 'Is Visible',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIncomeItem()
    {
        return $this->hasOne(InvoiceIncomeItem::className(), ['id' => 'income_item_id']);
    }
}
