<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "taxation_type".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Company[] $companies
 */
class TaxationType extends \yii\db\ActiveRecord
{
    /**
     *
     */
    const TYPE_OSNO = 1;
    /**
     *
     */
    const TYPE_USN_6 = 2;
    /**
     *
     */
    const TYPE_USN_15 = 3;
    /**
     *
     */
    const TYPE_ENVD = 4;
    /**
     *
     */
    const TYPE_USN_6_TEXT = '6%';
    /**
     *
     */
    const TYPE_USN_15_TEXT = '15%';
    /**
     * @var array
     */
    public static $withNdsArray = [
        self::TYPE_OSNO,
    ];

    /**
     * @var array
     */
    public static $withoutNdsArray = [
        self::TYPE_USN_6, self::TYPE_USN_15, self::TYPE_ENVD,
    ];

    /**
     * @var array
     */
    public static $createArray = [
        self::TYPE_OSNO, self::TYPE_USN_6, self::TYPE_USN_15, self::TYPE_ENVD,
    ];

    /**
     * @param $id
     * @param $isNewRecord
     * @param bool|false $creatingCompany
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function findByNds($id, $isNewRecord, $creatingCompany = false)
    {
        if ($creatingCompany) {
            $idArray = [$id];
        } else {
            if ($isNewRecord) {
                $idArray = self::$createArray;
            } else {
                $idArray = self::hasNds($id) ? self::$withNdsArray : self::$withoutNdsArray;
            }
        }

        return static::find()->andWhere([
            'in', 'id', $idArray,
        ])->all();
    }

    /**
     * @param $id
     * @return bool
     */
    public static function hasNds($id)
    {
        return in_array($id, TaxationType::$withNdsArray);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'taxation_type';
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Наименование',
        ];
    }
}
