<?php

namespace common\models\scanRecognize;

use common\components\entera\Entera;
use common\components\helpers\ArrayHelper;
use common\models\Company;
use common\models\Contractor;
use common\models\file\File;
use frontend\models\Documents;
use frontend\modules\documents\components\UploadManagerHelper;
use Yii;
use common\models\document\ScanDocument;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "scan_recognize".
 *
 * @property integer $id
 * @property integer $scan_id
 * @property integer $created_at
 * @property integer $finish_at
 * @property integer $is_recognized
 * @property integer $balance_cost
 * @property string $space_guid
 * @property string $guid
 *
 * @property ScanDocument $scan
 * @property ScanRecognizeDocument[] $scanRecognizeDocuments
 */
class ScanRecognize extends \yii\db\ActiveRecord
{
    /** @var $provider Entera */
    protected $provider;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'scan_recognize';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['scan_id'], 'required'],
            [['scan_id', 'created_at', 'finish_at', 'balance_cost', 'is_recognized'], 'integer'],
            [['space_guid', 'guid'], 'string', 'max' => 36],
            [['scan_id'], 'exist', 'skipOnError' => true, 'targetClass' => ScanDocument::className(), 'targetAttribute' => ['scan_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'scan_id' => 'Scan ID',
            'created_at' => 'Created At',
            'finish_at' => 'Finish At',
            'balance_cost' => 'Balance Cost',
            'space_guid' => 'Space Guid',
            'guid' => 'Guid',
        ];
    }

    public function init()
    {
        $this->provider = new Entera();
        parent::init();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScan()
    {
        return $this->hasOne(ScanDocument::className(), ['id' => 'scan_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScanRecognizeDocuments()
    {
        return $this->hasMany(ScanRecognizeDocument::className(), ['scan_recognize_id' => 'id']);
    }

    /**
     * @param File $file
     * @return bool
     */
    public function send(File $file)
    {
        $filepath = $file->getPath();
        if (!$filepath || !is_file($filepath)) {
            throw new NotFoundHttpException('Файл не найден.');
        }

        if ($this->provider->isValidToken()) {
            if ($taskGuid = $this->provider->send($file)) {
                $this->guid = $taskGuid;
                $this->space_guid = $this->provider::$space_guid;
                $this->created_at = time();
                return true;
            }
        }

        return false;
    }


    /**
     * @param Company $company
     * @return bool
     */
    public function getData(Company $company)
    {
        if ($this->provider->isValidToken()) {
            if ($rTask = $this->provider->get($this->guid)) {

                $state = ArrayHelper::getValue($rTask, 'state');
                if ($state != 'RECOGNIZED')
                    return false;

                $this->clearRecognitionData();
                
                $rDocuments = ArrayHelper::getValue($rTask, 'documents');
                foreach ((array)$rDocuments as $rDocument) {

                    $rSupplier = ArrayHelper::getValue($rDocument, 'supplier');
                    $rCustomer = ArrayHelper::getValue($rDocument, 'customer');
                    $rSupplierAccount = ArrayHelper::getValue($rDocument, 'recognizedSupplierAccountDetails');
                    $rCustomerAccount = ArrayHelper::getValue($rDocument, 'recognizedCustomerAccountDetails');
                    $rProducts = ArrayHelper::getValue($rDocument,'items');

                    // Document
                    $document = new ScanRecognizeDocument();
                    $document->scan_recognize_id = $this->id;
                    $document->document_number = (string)ArrayHelper::getValue($rDocument, 'number');
                    $document->document_date = (string)ArrayHelper::getValue($rDocument, 'date');
                    $document->document_type = (string)ArrayHelper::getValue($rDocument, 'type');
                    $document->amount = (string)ArrayHelper::getValue($rDocument, 'amount');
                    $document->amount_without_vat = (string)ArrayHelper::getValue($rDocument, 'amountWithoutVat');
                    $document->vat = (string)ArrayHelper::getValue($rDocument, 'vat');
                    $document->guid = ArrayHelper::getValue($rDocument, 'id');

                    if ($this->identifyCompany($company, $rCustomer)) {
                        $document->io_type = Documents::IO_TYPE_IN;
                    } elseif ($this->identifyCompany($company, $rSupplier)) {
                        $document->io_type = Documents::IO_TYPE_OUT;
                    }

                    $document->save();

                    // Supplier
                    if ($rSupplier) {
                        $contractor = new ScanRecognizeContractor();
                        $contractor->scan_recognize_document_id = $document->id;
                        $contractor->company_id = $company->id;
                        $contractor->type = Contractor::TYPE_SELLER;
                        $contractor->name = ArrayHelper::getValue($rSupplier, 'name');
                        $contractor->short_name = ArrayHelper::getValue($rSupplier, 'shortName');
                        $contractor->inn = (string)ArrayHelper::getValue($rSupplier, 'inn');
                        $contractor->kpp = (string)ArrayHelper::getValue($rSupplier, 'kpp');
                        $contractor->guid = ArrayHelper::getValue($rSupplier, 'id');

                        // supplier account
                        //if ($rSupplierAccount) {
                        //    $contractor->bank_bic = ArrayHelper::getValue($rSupplierAccount, 'bic');
                        //    $contractor->bank_rs = ArrayHelper::getValue($rSupplierAccount, 'account');
                        //}

                        if ($document->io_type != Documents::IO_TYPE_OUT) {
                            $contractor->contractor_id = $this->identifyContractor($company, $rSupplier, Contractor::TYPE_SELLER);
                        }

                        $contractor->save();
                    }

                    // Customer
                    if ($rCustomer) {
                        $contractor = new ScanRecognizeContractor();
                        $contractor->scan_recognize_document_id = $document->id;
                        $contractor->company_id = $company->id;
                        $contractor->type = Contractor::TYPE_CUSTOMER;
                        $contractor->name = ArrayHelper::getValue($rCustomer, 'name');
                        $contractor->short_name = ArrayHelper::getValue($rCustomer, 'shortName');
                        $contractor->inn = (string)ArrayHelper::getValue($rCustomer, 'inn');
                        $contractor->kpp = (string)ArrayHelper::getValue($rCustomer, 'kpp');
                        $contractor->guid = ArrayHelper::getValue($rCustomer, 'id');

                        // customer account
                        //if ($rCustomerAccount) {
                        //    $contractor->bank_bic = ArrayHelper::getValue($rCustomerAccount, 'bic');
                        //    $contractor->bank_rs = ArrayHelper::getValue($rCustomerAccount, 'account');
                        //}

                        if ($document->io_type != Documents::IO_TYPE_IN) {
                            $contractor->contractor_id = $this->identifyContractor($company, $rCustomer, Contractor::TYPE_CUSTOMER);
                        }

                        $contractor->save();
                    }

                    // Products
                    if ($rProducts) {
                        foreach ((array)$rProducts as $rProduct) {
                            $product = new ScanRecognizeProduct();
                            $product->scan_recognize_document_id = $document->id;
                            $product->company_id = $company->id;
                            $product->name = (string)ArrayHelper::getValue($rProduct, 'name');
                            $product->code = (string)ArrayHelper::getValue($rProduct, 'code');
                            $product->unit_code = (string)ArrayHelper::getValue($rProduct, 'unit');
                            $product->unit_name = (string)ArrayHelper::getValue($rProduct, 'unitName');
                            $product->quantity = (string)ArrayHelper::getValue($rProduct, 'quantity');
                            $product->price = (string)ArrayHelper::getValue($rProduct, 'price');
                            $product->price_without_vat = (string)ArrayHelper::getValue($rProduct, 'amountWithoutVat');
                            $product->vat_rate = (string)ArrayHelper::getValue($rProduct, 'vatRate');
                            $product->vat = (string)ArrayHelper::getValue($rProduct, 'vat');
                            $product->excize = (string)ArrayHelper::getValue($rProduct, 'excize');
                            $product->amount = (string)ArrayHelper::getValue($rProduct, 'amount');
                            $product->guid = (string)ArrayHelper::getValue($rProduct, 'id');

                            if (($productId = (string)UploadManagerHelper::findProductId($product->code))
                            ||  ($productId = (string)UploadManagerHelper::findProductIdByName($product->name))
                            ) {
                                $product->product_id = $productId;
                            }

                            $product->save();
                        }
                    }
                }
                
                $this->finish_at = strtotime($rTask['finishDate']);
                $this->is_recognized = true;
                $this->balance_cost = $rTask['balanceCost'];
                $this->updateAttributes(['finish_at', 'is_recognized', 'balance_cost']);

                return true;
            }
        }

        return false;
    }

    /**
     * @param Company $company
     * @param $rContractor
     * @param $type
     * @return int|null
     */
    public function identifyContractor(Company $company, $rContractor, $type)
    {
        $inn = (string)ArrayHelper::getValue($rContractor, 'inn');
        $kpp = (string)ArrayHelper::getValue($rContractor, 'kpp');
        $name = ArrayHelper::getValue($rContractor, 'name');
        $shortName = ArrayHelper::getValue($rContractor, 'shortName');
        $id = null;

        // find by inn
        if ($inn) {
            $id = UploadManagerHelper::findContractorId($type, $inn, $kpp);
        }
        // find by name
        if (!$id) {
            $allContractors = $company->getContractors()->select('name')->andWhere(['type' => $type])->indexBy('id')->column();

            foreach ($allContractors as $contractorId=>$contractorName) {
                if (UploadManagerHelper::inString($contractorName, $name)
                || UploadManagerHelper::inString($contractorName, $shortName)) {

                    $id = $contractorId;
                    break;
                }
            }
        }

        return $id;
    }

    /**
     * @param Company $company
     * @param $rContractor
     * @return bool
     */
    public function identifyCompany(Company $company, $rContractor)
    {
        if ($rContractor) {

            $inn = (string)ArrayHelper::getValue($rContractor, 'inn');
            $name = ArrayHelper::getValue($rContractor, 'name');
            $shortName = ArrayHelper::getValue($rContractor, 'shortName');

            if ($inn && $company->inn == $inn
            || UploadManagerHelper::inString($company->name_full, $name)
            || UploadManagerHelper::inString($company->name_short, $shortName))

                return true;
        }

        return false;
    }

    protected function clearRecognitionData() 
    {
        $oldRecognizeDocuments = ScanRecognizeDocument::find()->where(['scan_recognize_id' => $this->id])->all();
        foreach ($oldRecognizeDocuments as $oldRecognizeDocument)
            $oldRecognizeDocument->delete();        
    }
}
