<?php

namespace common\models\scanRecognize;

use Yii;
use frontend\models\Documents;

/**
 * This is the model class for table "scan_recognize_document".
 *
 * @property integer $id
 * @property integer $scan_recognize_id
 * @property integer $io_type
 * @property string $document_type
 * @property string $document_number
 * @property string $document_date
 * @property string $amount
 * @property string $amount_without_vat
 * @property string $vat
 * @property string $guid
 *
 * @property ScanRecognizeContractor[] $scanRecognizeContractors
 * @property ScanRecognize $scanRecognize
 * @property ScanRecognizeProduct[] $scanRecognizeProducts
 */
class ScanRecognizeDocument extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'scan_recognize_document';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['scan_recognize_id'], 'required'],
            [['scan_recognize_id', 'io_type'], 'integer'],
            [['document_number', 'document_date', 'document_type', 'amount', 'amount_without_vat', 'vat'], 'string', 'max' => 16],
            [['guid'], 'string', 'max' => 36],
            [['scan_recognize_id'], 'exist', 'skipOnError' => true, 'targetClass' => ScanRecognize::className(), 'targetAttribute' => ['scan_recognize_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'scan_recognize_id' => 'Scan Recognize ID',
            'document_number' => 'Document Number',
            'document_date' => 'Document Date',
            'io_type' => 'IO Type',
            'document_type' => 'Document Type',
            'guid' => 'Guid',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScanRecognizeContractors()
    {
        return $this->hasMany(ScanRecognizeContractor::className(), ['scan_recognize_document_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScanRecognize()
    {
        return $this->hasOne(ScanRecognize::className(), ['id' => 'scan_recognize_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScanRecognizeProducts()
    {
        return $this->hasMany(ScanRecognizeProduct::className(), ['scan_recognize_document_id' => 'id']);
    }

    /**
     * @return string
     */
    public function getDocumentTypeName()
    {
        switch ($this->document_type) {
            case 'OFFER':
                $name = 'Счет';
                break;
            case 'CERTIFICATE':
                $name = 'Акт';
                break;
            case 'TORG12':
                $name = 'ТН';
                break;
            case 'UTD':
                $name = 'УПД';
                break;
            case 'VAT_INVOICE':
                $name = 'СФ';
                break;
            case 'UNKNOWN':
                $name = 'Документ не распознан';
                break;
            default:
                $name = 'Документ';
                break;
        }

        return $name;
    }

    public function getDocumentTypeId()
    {
        switch ($this->document_type) {
            case 'OFFER':
                $id = Documents::DOCUMENT_INVOICE;
                break;
            case 'CERTIFICATE':
                $id = Documents::DOCUMENT_ACT;
                break;
            case 'TORG12':
                $id = Documents::DOCUMENT_PACKING_LIST;
                break;
            case 'UTD':
                $id = Documents::DOCUMENT_UPD;
                break;
            case 'VAT_INVOICE':
                $id = Documents::DOCUMENT_INVOICE_FACTURE;
                break;
            case 'UNKNOWN':
                $id = null;
                break;
            default:
                $id = null;
                break;
        }

        return $id;
    }
}
