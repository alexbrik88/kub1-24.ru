<?php

namespace common\models\scanRecognize;

use Yii;

/**
 * This is the model class for table "scan_recognize_contractor".
 *
 * @property integer $id
 * @property integer $scan_recognize_document_id
 * @property integer $company_id
 * @property integer $contractor_id
 * @property integer $type
 * @property string $name
 * @property string $short_name
 * @property string $inn
 * @property string $kpp
 * @property string $bank_bic
 * @property string $bank_rs
 * @property string $guid
 *
 * @property ScanRecognizeDocument $scanRecognizeDocument
 */
class ScanRecognizeContractor extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'scan_recognize_contractor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['scan_recognize_document_id', 'type'], 'required'],
            [['scan_recognize_document_id', 'company_id', 'contractor_id', 'type'], 'integer'],
            [['name', 'short_name'], 'string', 'max' => 255],
            [['inn'], 'string', 'max' => 12],
            [['kpp', 'bank_bic'], 'string', 'max' => 9],
            [['bank_rs'], 'string', 'max' => 20],
            [['guid'], 'string', 'max' => 36],
            [['scan_recognize_document_id'], 'exist', 'skipOnError' => true, 'targetClass' => ScanRecognizeDocument::className(), 'targetAttribute' => ['scan_recognize_document_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'scan_recognize_document_id' => 'Scan Recognize Document ID',
            'name' => 'Name',
            'type' => 'Type',
            'short_name' => 'Short Name',
            'inn' => 'Inn',
            'kpp' => 'Kpp',
            'bank_bic' => 'Bank Bic',
            'bank_rs' => 'Bank Rs',
            'guid' => 'Guid',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScanRecognizeDocument()
    {
        return $this->hasOne(ScanRecognizeDocument::className(), ['id' => 'scan_recognize_document_id']);
    }
}
