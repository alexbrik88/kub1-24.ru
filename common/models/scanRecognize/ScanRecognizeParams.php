<?php

namespace common\models\scanRecognize;

use Yii;

/**
 * This is the model class for table "scan_recognize_params".
 *
 * @property integer $id
 * @property string $param_name
 * @property string $param_value
 * @property integer $created_at
 * @property integer $updated_at
 */
class ScanRecognizeParams extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'scan_recognize_params';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['param_name', 'param_value'], 'required'],
            [['created_at', 'updated_at'], 'integer'],
            [['param_name', 'param_value'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'param_name' => 'Param Name',
            'param_value' => 'Param Value',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @inheritdoc
     */
    public static function setValue($paramName, $paramValue)
    {
        if (!empty($paramName)) {
            $model = static::findOne([
                'param_name' => $paramName,
            ]);
            if ($model === null) {
                $model = new static([
                    'param_name' => $paramName,
                    'created_at' => time()
                ]);
            }
            $model->param_value = (string) $paramValue;
            $model->updated_at = time();

            return $model->save();
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public static function getValue($paramName, $default = null, $attribute = 'param_value')
    {
        $model = static::findOne([
            'param_name' => $paramName,
        ]);

        return $model !== null ? $model->$attribute : $default;
    }

    /**
     * @inheritdoc
     */
    public static function deleteValue($paramName)
    {
        return Yii::$app->db->createCommand()->delete(self::tableName(), [
            'param_name' => $paramName,
        ])->execute();
    }
}
