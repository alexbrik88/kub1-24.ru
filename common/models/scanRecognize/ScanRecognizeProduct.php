<?php

namespace common\models\scanRecognize;

use common\models\TaxRate;
use Yii;

/**
 * This is the model class for table "scan_recognize_product".
 *
 * @property integer $id
 * @property integer $scan_recognize_document_id
 * @property integer $company_id
 * @property integer $product_id
 * @property string $name
 * @property string $code
 * @property string $unit_name
 * @property string $unit_code
 * @property string $quantity
 * @property string $price
 * @property string $price_without_vat
 * @property string $vat_rate
 * @property string $vat
 * @property string $excize
 * @property string $amount
 * @property string $guid
 *
 * @property ScanRecognizeDocument $scanRecognizeDocument
 */
class ScanRecognizeProduct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'scan_recognize_product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['scan_recognize_document_id'], 'required'],
            [['scan_recognize_document_id', 'company_id', 'product_id'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['code'], 'string', 'max' => 64],
            [['unit_name', 'unit_code', 'quantity', 'vat_rate'], 'string', 'max' => 16],
            [['price', 'price_without_vat', 'vat', 'excize', 'amount'], 'string', 'max' => 24],
            [['guid'], 'string', 'max' => 36],
            [['scan_recognize_document_id'], 'exist', 'skipOnError' => true, 'targetClass' => ScanRecognizeDocument::className(), 'targetAttribute' => ['scan_recognize_document_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'scan_recognize_document_id' => 'Scan Recognize Document ID',
            'name' => 'Name',
            'code' => 'Code',
            'unit_name' => 'Unit Name',
            'unit_code' => 'Unit Code',
            'quantity' => 'Quantity',
            'price' => 'Price',
            'price_without_vat' => 'Price Without Vat',
            'vat_rate' => 'Vat Rate',
            'vat' => 'Vat',
            'excize' => 'Excize',
            'amount' => 'Amount',
            'guid' => 'Guid',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScanRecognizeDocument()
    {
        return $this->hasOne(ScanRecognizeDocument::className(), ['id' => 'scan_recognize_document_id']);
    }

    /**
     * @return int
     */
    public function getTaxRateId()
    {
        switch ($this->vat_rate) {
            case 'PERCENT_20':
                return TaxRate::RATE_20;
            case 'PERCENT_18':
                return TaxRate::RATE_18;
            case 'PERCENT_10':
                return TaxRate::RATE_10;
            case 'PERCENT_0':
                return TaxRate::RATE_0;
            case 'NO_VAT':
            default:
                return TaxRate::RATE_WITHOUT;
        }
    }

    /**
     * @return array
     */
    public function getTaxRateData()
    {
        switch ($this->vat_rate) {
            case 'PERCENT_20':
                return ['id' => 5, 'name' => '20%', 'rate' => '0.2'];
            case 'PERCENT_18':
                return ['id' => 1, 'name' => '18%', 'rate' => '0.18'];
            case 'PERCENT_10':
                return ['id' => 2, 'name' => '10%', 'rate' => '0.1'];
            case 'PERCENT_0':
                return ['id' => 3, 'name' => '0%', 'rate' => '0'];
            case 'NO_VAT':
            default:
                return ['id' => 4, 'name' => 'Без НДС', 'rate' => ''];
        }
    }
}
