<?php

namespace common\models\marketing;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Class MarketingCalculatingPlanningSetting
 * @package common\models\marketing
 *
 * @property int      $marketing_plan_id
 * @property int      $channel
 * @property string   $attribute
 * @property string   $start_month
 * @property int      $start_year
 * @property int      $action
 * @property int|null $start_amount
 * @property int|null $amount
 * @property int|null $limit_amount
 *
 * Relations:
 * @property MarketingPlan $marketingPlan
 */
class MarketingCalculatingPlanningSetting extends ActiveRecord
{
    public static function tableName()
    {
        return 'marketing_calculating_planning_settings';
    }

    public function getMarketingPlan(): ActiveQuery
    {
        return $this->hasOne(MarketingPlan::class, ['id' => 'marketing_plan_id']);
    }
}
