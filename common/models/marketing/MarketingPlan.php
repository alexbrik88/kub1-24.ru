<?php

namespace common\models\marketing;

use Carbon\Carbon;
use common\models\Company;
use common\models\employee\Employee;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "marketing_plan".
 *
 * @property int $id
 * @property int $company_id
 * @property string $name
 * @property string $start_date
 * @property string $end_date
 * @property string|null $comment
 * @property string|null $template
 * @property int $responsible_employee_id
 * @property int|null $financial_result
 * @property int|null $currency
 * @property int $created_at
 *
 * @property Company $company
 * @property MarketingPlanItem[] $marketingPlanItems
 * @property Employee $responsibleEmployee
 */
class MarketingPlan extends \yii\db\ActiveRecord
{
    public const CHANNEL_YANDEX_DIRECT = 0;
    public const CHANNEL_GOOGLE_ADS = 1;
    public const CHANNEL_VK = 2;
    public const CHANNEL_FACEBOOK = 3;
    public const REPEATED_PURCHASES = 4;

    public const TEMPLATE_ONLINE_SERVICE = 0;
    public const TEMPLATE_ONLINE_STORE = 1;
    public const TEMPLATE_LEAD_GENERATION = 2;

    public const CURRENCY_RUB = 0;
    public const CURRENCY_USD = 1;
    public const CURRENCY_BYN = 2;
    public const CURRENCY_EUR = 3;
    public const CURRENCY_KZT = 4;
    public const CURRENCY_UAH = 5;
    public const CURRENCY_MDL = 6;
    public const CURRENCY_JPY = 7;
    public const CURRENCY_GBP = 8;
    public const CURRENCY_CNY = 9;
    public const CURRENCY_UZS = 10;

    public static array $currencyMap = [
        self::CURRENCY_RUB => [
            'name' => 'Российский рубль',
            'code' => 'RUB',
            'symbol' => '₽',
        ],
        self::CURRENCY_BYN => [
            'name' => 'Белорусский рубль',
            'code' => 'BYN',
            'symbol' => 'Br',
        ],
        self::CURRENCY_USD => [
            'name' => 'Доллар США',
            'code' => 'USD',
            'symbol' => '$',
        ],
        self::CURRENCY_EUR => [
            'name' => 'Евро',
            'code' => 'EUR',
            'symbol' => '€',
        ],
        self::CURRENCY_JPY => [
            'name' => 'Йена',
            'code' => 'JPY',
            'symbol' => '¥',
        ],
        self::CURRENCY_KZT => [
            'name' => 'Казахстанский тенге',
            'code' => 'KZT',
            'symbol' => '₸',
        ],
        self::CURRENCY_MDL => [
            'name' => 'Молдавский лей',
            'code' => 'MDL',
            'symbol' => 'L',
        ],
        self::CURRENCY_UZS => [
            'name' => 'Узбекский сум',
            'code' => 'UZS',
            'symbol' => 'So\'m',
        ],
        self::CURRENCY_UAH => [
            'name' => 'Украинская гривна',
            'code' => 'UAH',
            'symbol' => '₴',
        ],
        self::CURRENCY_GBP => [
            'name' => 'Фунт стерлингов',
            'code' => 'GBP',
            'symbol' => '£',
        ],
        self::CURRENCY_CNY => [
            'name' => 'Юань',
            'code' => 'CNY',
            'symbol' => '¥',
        ],
    ];

    public static array $templateMap = [
        self::TEMPLATE_LEAD_GENERATION => 'Шаблон лидогенерация',
        self::TEMPLATE_ONLINE_SERVICE => 'Шаблон для онлайн сервиса',
        self::TEMPLATE_ONLINE_STORE => 'Шаблон для интернет магазина',
    ];

    public static array $repeatedPurchasesTemplates = [
        self::TEMPLATE_ONLINE_SERVICE,
        self::TEMPLATE_ONLINE_STORE,
    ];

    public static array $channelMap = [
        self::CHANNEL_YANDEX_DIRECT => 'Яндекс.Директ',
        self::CHANNEL_GOOGLE_ADS => 'Google.Ads',
        self::CHANNEL_VK => 'Вконтакте',
        self::CHANNEL_FACEBOOK => 'Facebook',
    ];

    public static function tableName(): string
    {
        return 'marketing_plan';
    }

    public function behaviors(): array
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => false,
            ],
        ];
    }

    public function rules(): array
    {
        return [
            [['company_id', 'name', 'start_date', 'end_date', 'responsible_employee_id'], 'required'],
            [['company_id', 'responsible_employee_id', 'financial_result', 'created_at'], 'integer'],
            [['comment'], 'string'],
            [['name', 'template'], 'string', 'max' => 255],
            [['start_date', 'end_date'], 'string', 'max' => 7],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['company_id' => 'id']],
            [['responsible_employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::class, 'targetAttribute' => ['responsible_employee_id' => 'id']],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'name' => 'Name',
            'start_date' => 'Start Date',
            'end_date' => 'End Date',
            'comment' => 'Comment',
            'template' => 'Template',
            'responsible_employee_id' => 'Responsible Employee ID',
            'financial_result' => 'Financial Result',
            'created_at' => 'Created At',
        ];
    }

    public function getCompany(): ActiveQuery
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    public function getMarketingPlanItems(): ActiveQuery
    {
        return $this->hasMany(MarketingPlanItem::class, ['marketing_plan_id' => 'id']);
    }

    public function getResponsibleEmployee(): ActiveQuery
    {
        return $this->hasOne(Employee::class, ['id' => 'responsible_employee_id']);
    }

    public function getPeriod()
    {
        $dateFrom = date_create_from_format('d.m.Y', "01.{$this->start_date}");
        $dateTill = date_create_from_format('d.m.Y', "01.{$this->end_date}");
        if ($dateFrom && $dateTill) {
            $diff = date_diff($dateFrom, $dateTill);
            return 1 + $diff->format('%y') * 12 + $diff->format('%m');
        }

        return 0;
    }

    public function getCurrency(): int {
        if ($this->currency === null) {
            return self::CURRENCY_RUB;
        }

        return $this->currency;
    }

    public function getCurrencySymbol(): string {
        return self::$currencyMap[$this->getCurrency()]['symbol'];
    }

    public function getQuarters()
    {
        $startDate = Carbon::createFromFormat('m.Y', $this->start_date);
        $endDate = Carbon::createFromFormat('m.Y', $this->end_date);
        $ret = [];
        for ($y = $startDate->format('Y'); $y <= $endDate->format('Y'); $y++) {
            for ($m = 1; $m <= 12; $m++) {
                if ($y == $startDate->format('Y') && $m < $startDate->format('n')) continue;
                if ($y == $endDate->format('Y') && $m > $endDate->format('n')) break;

                $quarterNum = str_pad((int)ceil($m / 3), 2, "0", STR_PAD_LEFT);
                $monthNum = str_pad($m, 2, "0", STR_PAD_LEFT);

                if (!isset($ret[$y . $quarterNum]))
                    $ret[$y . $quarterNum] = [];

                $ret[$y . $quarterNum][] = $y . $monthNum;
            }
        }

        return $ret;
    }
}
