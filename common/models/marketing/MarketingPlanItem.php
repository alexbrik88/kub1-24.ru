<?php

namespace common\models\marketing;

use common\models\Company;
use Yii;
use yii\db\ActiveQuery;

/**
 * This is the model class for table "marketing_plan_item".
 *
 * @property int $id
 * @property int $company_id
 * @property int $marketing_plan_id
 * @property int $channel
 * @property string $month
 * @property int $year
 * @property int|null $budget_amount
 * @property int|null $marketing_service_amount
 * @property int|null $additional_expenses_amount
 * @property int|null $total_additional_expenses_amount
 * @property int|null $additional_expenses_amount_for_one_sale
 * @property int|null $clicks_count
 * @property int|null $click_amount
 * @property float|null $registration_conversion
 * @property int|null $registration_count
 * @property int|null $registration_amount
 * @property float|null $active_user_conversion
 * @property int|null $active_users_count
 * @property int|null $active_user_amount
 * @property float|null $purchase_conversion
 * @property int|null $purchases_count
 * @property int|null $sale_amount
 * @property int|null $average_check
 * @property int|null $proceeds_from_new_amount
 * @property float|null $churn_rate
 * @property int|null $repeated_purchases_amount
 * @property int|null $total_proceeds_amount
 * @property int|null $financial_result
 * @property int|null $cumulative_financial_amount
 * @property int|null $repeated_purchases_count
 * @property int|null $total_purchases_count
 * @property int|null $salesperson_salary_amount
 *
 * @property Company $company
 * @property MarketingPlan $marketingPlan
 */
class MarketingPlanItem extends \yii\db\ActiveRecord
{
    public static function tableName()
    {
        return 'marketing_plan_item';
    }

    public function getCompany(): ActiveQuery
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    public function getMarketingPlan(): ActiveQuery
    {
        return $this->hasOne(MarketingPlan::class, ['id' => 'marketing_plan_id']);
    }
}
