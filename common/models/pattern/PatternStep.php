<?php

namespace common\models\pattern;

use common\models\company\Event;
use Yii;

/**
 * This is the model class for table "pattern_step".
 *
 * @property integer $pattern_id
 * @property integer $step
 *
 * @property Pattern $pattern
 * @property PatternStepEvent[] $patternStepEvents
 * @property Event[] $events
 */
class PatternStep extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pattern_step';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['pattern_id', 'step'], 'required'],
            [['pattern_id', 'step'], 'integer'],
            [['pattern_id'], 'exist', 'skipOnError' => true, 'targetClass' => Pattern::className(), 'targetAttribute' => ['pattern_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'pattern_id' => 'Pattern ID',
            'step' => 'Step',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPattern()
    {
        return $this->hasOne(Pattern::className(), ['id' => 'pattern_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatternStepEvents()
    {
        return $this->hasMany(PatternStepEvent::className(), ['step_pattern_id' => 'pattern_id', 'step_step' => 'step']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvents()
    {
        return $this->hasMany(Event::className(), ['id' => 'event_id'])->viaTable('pattern_step_event', ['step_pattern_id' => 'pattern_id', 'step_step' => 'step']);
    }
}
