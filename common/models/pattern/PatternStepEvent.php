<?php

namespace common\models\pattern;

use common\models\company\Event;
use Yii;

/**
 * This is the model class for table "pattern_step_event".
 *
 * @property integer $step_pattern_id
 * @property integer $step_step
 * @property integer $event_id
 * @property integer $event_count
 *
 * @property Event $event
 * @property PatternStep $stepPattern
 */
class PatternStepEvent extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pattern_step_event';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['step_pattern_id', 'step_step', 'event_id', 'event_count'], 'required'],
            [['step_pattern_id', 'step_step', 'event_id', 'event_count'], 'integer'],
            [['event_id'], 'exist', 'skipOnError' => true, 'targetClass' => Event::className(), 'targetAttribute' => ['event_id' => 'id']],
            [['step_pattern_id', 'step_step'], 'exist', 'skipOnError' => true, 'targetClass' => PatternStep::className(), 'targetAttribute' => ['step_pattern_id' => 'pattern_id', 'step_step' => 'step']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'step_pattern_id' => 'Step Pattern ID',
            'step_step' => 'Step Step',
            'event_id' => 'Event ID',
            'event_count' => 'Event Count',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEvent()
    {
        return $this->hasOne(Event::className(), ['id' => 'event_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStepPattern()
    {
        return $this->hasOne(PatternStep::className(), ['pattern_id' => 'step_pattern_id', 'step' => 'step_step']);
    }
}
