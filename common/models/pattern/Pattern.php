<?php

namespace common\models\pattern;

use Yii;

/**
 * This is the model class for table "pattern".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $signup_count
 * @property integer $payment_count
 * @property string $date_start
 * @property string $date_end
 * @property integer $interval
 *
 * @property PatternStep[] $patternSteps
 */
class Pattern extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pattern';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['date_start', 'end'], 'required'],
            //[['date_end', 'end'], 'date'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Создан',
            'signup_count' => 'Регистраций',
            'payment_count' => 'Первых оплат',
            'date_start' => 'Начало отчета',
            'date_end' => 'Конец отчета',
            'interval' => 'Период дн.',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPatternSteps()
    {
        return $this->hasMany(PatternStep::className(), ['pattern_id' => 'id']);
    }
}
