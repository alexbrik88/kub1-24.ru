<?php

namespace common\models\bank;

use common\models\company\ApplicationToBank;
use frontend\modules\cash\modules\banking\components\Banking;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "bank".
 *
 * @property integer $id
 * @property string $bik
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $bank_name
 * @property string $description
 * @property string $url
 * @property string $email
 * @property string $phone
 * @property string $contact_person
 * @property string $logo_link
 * @property integer $request_count
 * @property integer $is_blocked
 * @property integer $is_special_offer
 *
 * @property ApplicationToBank[] $applicationToBanks
 * @property BankingParams[] $bankingParams
 */
class Bank extends \yii\db\ActiveRecord
{
    /**
     *
     */
    const LOGO_IMAGE = 'logo';
    /**
     *
     */
    const LITTLE_LOGO_IMAGE = 'little_logo';
    /**
     *
     */
    const BLOCKED = 1;

    protected static $_icons = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bank';
    }

    /**
     * @inheritdoc
     */
    public static function getLogoPath(string $bik, $small = false)
    {
        if (empty($bik)) {
            return null;
        }

        $bank = self::findOne([
            'bik' => $bik,
            'is_blocked' => false,
        ]) ? : Banking::getBankByBik($bik);

        if ($bank !== null) {
            $file = ArrayHelper::getValue($bank, $small ? 'little_logo_link' : 'logo_link') ? : null;
            $dir = $bank->getUploadDirectory();

            if ($file && is_file($dir . $file)) {
                return $dir . $file;
            }
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bik', 'created_at', 'updated_at', 'bank_name'], 'required'],
            [['created_at', 'updated_at', 'request_count', 'is_blocked', 'is_special_offer'], 'integer'],
            [['description'], 'string'],
            [['bik'], 'string', 'max' => 9],
            [['bank_name', 'url', 'email', 'contact_person', 'logo_link'], 'string', 'max' => 255],
            [['phone'], 'string', 'max' => 130],
            [['bik'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bik' => 'Bik',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'bank_name' => 'Bank Name',
            'description' => 'Description',
            'url' => 'Url',
            'email' => 'Email',
            'phone' => 'Phone',
            'contact_person' => 'Contact Person',
            'logo_link' => 'Logo Link',
            'request_count' => 'Request Count',
            'is_blocked' => 'Is Blocked',
            'is_special_offer' => 'Is Special Offer',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApplicationToBanks()
    {
        return $this->hasMany(ApplicationToBank::className(), ['bank_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBankingParams()
    {
        return $this->hasMany(BankingParams::className(), ['bank_bik' => 'bik']);
    }

    /**
     * @return string
     * @throws Exception
     */
    public function getUploadDirectory()
    {
        return Yii::getAlias('@common/uploads') . DIRECTORY_SEPARATOR .
                $this->tableName(). DIRECTORY_SEPARATOR . $this->id;
    }
}
