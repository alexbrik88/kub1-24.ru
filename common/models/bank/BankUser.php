<?php

namespace common\models\bank;

use common\models\Company;
use common\models\employee\Employee;
use Yii;

/**
 * This is the model class for table "bank_user".
 *
 * @property integer $id
 * @property string $bank_alias
 * @property string $user_uid
 * @property integer $employee_id
 * @property integer $company_id
 * @property string $inn
 * @property string $kpp
 * @property integer $is_chief
 *
 * @property Company $company
 * @property Employee $employee
 */
class BankUser extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bank_user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['bank_alias', 'user_uid'], 'required'],
            [['employee_id', 'company_id'], 'integer'],
            [['is_chief'], 'boolean'],
            [['bank_alias', 'user_uid', 'company_uid'], 'string', 'max' => 255],
            [['inn'], 'string', 'max' => 12],
            [['kpp'], 'string', 'max' => 9],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'bank_alias' => 'Bank Alias',
            'user_uid' => 'User Uid',
            'employee_id' => 'Employee ID',
            'company_id' => 'Company ID',
            'inn' => 'Inn',
            'kpp' => 'Kpp',
            'is_chief' => 'Is Chief',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }
}
