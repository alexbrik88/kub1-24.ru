<?php

namespace common\models\bank;

use DateTime;
use Yii;

/**
 * This is the model class for table "bank_statement_autoload_mode".
 *
 * @property integer $id
 * @property string $name
 *
 * @property CheckingAccountant[] $checkingAccountants
 */
class StatementAutoloadMode extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'bank_statement_autoload_mode';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCheckingAccountants()
    {
        return $this->hasMany(CheckingAccountant::className(), ['autoload_mode_id' => 'id']);
    }

    /**
     * @return DateTime
     */
    public function getPreviousDate(DateTime $date = null)
    {
        $weekDays = [
            1 => 'Monday',
            2 => 'Tuesday',
            3 => 'Wednesday',
            4 => 'Thursday',
            5 => 'Friday',
            6 => 'Saturday',
            7 => 'Sunday',
        ];
        $days = [];
        foreach (explode(',', $this->week_days) as $day) {
            $days[$day] = $weekDays[$day];
        }
        $weekdayNumber = $date ? $date->format('N') : date('N');

        reset($days);
        while(key($days) != $weekdayNumber) next($days);
        $dayName = prev($days);
        if (!$dayName) {
            $dayName = end($days);
        }

        return (new DateTime("last $dayName"));
    }
}
