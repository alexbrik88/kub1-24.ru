<?php

namespace common\models\log;

use common\models\Company;
use common\models\employee\Employee;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "log".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $author_id
 * @property integer $company_id
 * @property string $model_name
 * @property integer $model_id
 * @property string $data
 *
 * @property Employee $author
 * @property Company $company
 */
class Log extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                [
                    'class' => TimestampBehavior::className(),
                    'updatedAtAttribute' => false,
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['author_id', 'model_name', 'model_id'], 'required'],
            [['author_id', 'model_id'], 'integer'],
            [['model_name'], 'string', 'max' => 1024],
            [['data'], 'string', 'max' => 4096],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Дата создания',
            'author_id' => 'Автор',
            'company_id' => 'Компания',
            'model_name' => 'Model Name',
            'model_id' => 'Model ID',
            'data' => 'Дополнительные данные',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Employee::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
}
