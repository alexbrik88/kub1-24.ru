<?php
namespace common\models\store;

use common\components\date\DateHelper;
use common\models\Company;
use common\models\Contractor;
use common\models\TimeZone;
use common\components\validators\PhoneValidator;
use frontend\components\StatisticPeriod;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * StoreUser model
 *
 * @property integer $id
 * @property integer $status
 * @property integer $store_company_id
 * @property integer $contractor_id
 * @property string $lastname
 * @property string $firstname
 * @property string $patronymic
 * @property string $email
 * @property string $phone
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $auth_key
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 * @property integer $company_id
 * @property string $login_password
 * @property string $login_key
 *
 * @property string $nameFull
 * @property boolean $isContractorActive
 *
 * @property TimeZone $timeZone
 * @property StoreCompany[] $storeCompanies
 * @property StoreCompany $currentStoreCompany
 * @property Company $company
 */
class StoreUser extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;

    protected $_currentStoreCompany;
    protected $_currentKubContractor;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%store_user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['phone', 'default', 'value' => ''],
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            [['time_zone_id'], 'integer'],
            [['email', 'auth_key', 'password_hash', 'time_zone_id'], 'required'],
            [['statistic_range_date_from', 'statistic_range_date_to'], 'date', 'format' => 'php:Y-m-d'],
            [['lastname', 'firstname', 'patronymic', 'email', 'login_password', 'login_key'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['email'], 'email'],
            [['email'], 'unique'],
            [['phone'], PhoneValidator::className()],
            [['password_reset_token'], 'unique'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Имя',
            'email' => 'Email',
            'phone' => 'Телефон',
            'password' => 'Пароль',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTimeZone()
    {
        return $this->hasOne(TimeZone::className(), ['id' => 'time_zone_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoreUserCompanies()
    {
        return $this->hasMany(StoreUserCompany::className(), ['store_user_id' => 'id'])
            ->andOnCondition([StoreUserCompany::tableName() . '.status' => StoreUserCompany::STATUS_ACTIVE]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoreCompanies()
    {
        return $this->hasMany(StoreCompany::className(), ['id' => 'store_company_id'])
            ->via('storeUserCompanies')
            ->andOnCondition([StoreCompany::tableName() . '.status' => StoreCompany::STATUS_ACTIVE]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoreCompanyContractors()
    {
        return $this->hasMany(StoreCompanyContractor::className(), ['store_company_id' => 'id'])
            ->via('storeCompanies')
            ->andOnCondition([StoreCompanyContractor::tableName() . '.status' => StoreCompanyContractor::STATUS_ACTIVE]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getKubContractors()
    {
        return $this->hasMany(Contractor::className(), ['id' => 'contractor_id'])
            ->via('storeCompanyContractors')
            ->andOnCondition([
                Contractor::tableName() . '.status' => Contractor::ACTIVE,
                Contractor::tableName() . '.is_deleted' => Contractor::NOT_DELETED,
            ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewRequests()
    {
        return StoreCompanyContractor::find()
            ->andWhere([
                'status' => StoreCompanyContractor::STATUS_NEW,
                'store_company_id' => $this->currentStoreCompany ? $this->currentStoreCompany->id : null,
            ]);
    }

    /**
     * @return StoreCompany
     */
    public function renewCurrentCompany()
    {
        $store_company_id = null;
        $contractor_id = null;

        $this->_currentStoreCompany = StoreCompany::find()->alias('company')
            ->rightJoin(['link' => StoreUserCompany::tableName()], 'company.id = link.store_company_id')
            ->where(['link.store_user_id' => $this->id])
            ->andWhere(['link.status' => StoreUserCompany::STATUS_ACTIVE])
            ->andWhere(['company.status' => StoreCompany::STATUS_ACTIVE])
            ->orderBy(['IF(company.id = :id, 0, 1)' => SORT_ASC])
            ->params([':id' => $this->store_company_id])
            ->one();

        if ($this->_currentStoreCompany !== null) {
            $store_company_id = $this->_currentStoreCompany->id;

            $this->_currentKubContractor = Contractor::find()->alias('contractor')
                ->rightJoin(['link' => StoreCompanyContractor::tableName()], 'contractor.id = link.contractor_id')
                ->where(['link.store_company_id' => $this->_currentStoreCompany->id])
                ->andWhere(['contractor.status' => Contractor::ACTIVE])
                ->andWhere(['contractor.is_deleted' => false])
                ->orderBy(['IF(contractor.id = :id, 0, 1)' => SORT_ASC])
                ->params([':id' => $this->contractor_id])
                ->one();

            if ($this->_currentKubContractor !== null) {
                $contractor_id = $this->_currentKubContractor->id;
            }
        }

        if ($store_company_id != $this->store_company_id || $contractor_id != $this->contractor_id) {
            $this->updateAttributes([
                'store_company_id' => $store_company_id,
                'contractor_id' => $contractor_id,
            ]);
        }
    }

    /**
     * @return StoreCompany
     */
    public function getCurrentStoreCompany()
    {
        return $this->_currentStoreCompany;
    }

    /**
     * @return StoreCompany
     */
    public function getCurrentKubContractor()
    {
        return $this->_currentKubContractor;
    }

    /**
     * @return StoreCompany
     */
    public function getCurrentKubCompany()
    {
        return $this->_currentKubContractor !== null ? $this->_currentKubContractor->company : null;
    }

    /**
     * @return StoreCompany
     */
    public function getIsContractorActive()
    {
        if ($this->_currentKubContractor &&
            $this->_currentKubContractor->is_deleted == Contractor::NOT_DELETED &&
            $this->_currentKubContractor->status == Contractor::ACTIVE
        ) {
            return StoreCompanyContractor::find()->where([
                'store_company_id' => $this->_currentStoreCompany->id,
                'contractor_id' => $this->_currentKubContractor->id,
                'status' => StoreCompanyContractor::STATUS_ACTIVE,
            ])->exists();
        }

        return false;
    }

    /**
     * @return string
     */
    public function getNameFull()
    {
        $name = array_filter([
            $this->lastname,
            $this->firstname,
            $this->patronymic,
        ]);

        return implode(' ', $name);
    }

    /**
     * @param $date_from
     * @param $date_to
     * @return bool
     */
    public function setStatisticRangeDates($date_from, $date_to)
    {
        if ($date_from != null && $date_to != null) {
            $this->statistic_range_date_from = $date_from;
            $this->statistic_range_date_to = $date_to;
            $this->save(true, ['statistic_range_date_from', 'statistic_range_date_to']);


            return true;
        }

        return false;
    }


    /**
     * @param $name
     * @param $date_from
     * @param $date_to
     * @return bool
     */
    public function setStatisticRangeName($name, $date_from, $date_to)
    {
        if ($name != null) {
            $this->statistic_range_name = $name;
            if ($name == StatisticPeriod::CUSTOM_RANGE) {
                $date_from = DateHelper::format($date_from, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                $date_to = DateHelper::format($date_to, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                $this->statistic_range_name = $date_from . ' - ' . $date_to;
            }

            return $this->save(true, ['statistic_range_name']);
        }

        return false;
    }

    /**
     * @return boolean
     */
    public function getIsSetStatisticRange()
    {
        return $this->statistic_range_name && $this->statistic_range_date_from && $this->statistic_range_date_to;
    }

    /**
     * @return string
     */
    public function getStatisticRangeName()
    {
        return $this->statistic_range_name;
    }

    /**
     * @param null $range
     * @return bool|string
     */
    public function getStatisticRangeDates($range = null)
    {
        if ($range == 'from') {
            return $this->statistic_range_date_from;
        } elseif ($range == 'to') {
            return $this->statistic_range_date_to;
        } elseif ($this->statistic_range_date_from && $this->statistic_range_date_to) {
            return true;
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        $identity = static::findOne(['id' => $id, 'status' => static::STATUS_ACTIVE]);

        if ($identity !== null) {
            $identity->renewCurrentCompany();
        }

        return $identity;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by email
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
}
