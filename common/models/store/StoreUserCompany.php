<?php

namespace common\models\store;

use Yii;

/**
 * This is the model class for table "store_user_company".
 *
 * @property integer $store_company_id
 * @property integer $store_user_id
 * @property integer $role_id
 * @property integer $status
 * @property integer $created_at
 *
 * @property StoreUser $storeUser
 * @property StoreCompany $storeCompany
 * @property StoreUserRole $role
 */
class StoreUserCompany extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 10;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'store_user_company';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['store_company_id', 'store_user_id', 'role_id'], 'required'],
            [['store_company_id', 'store_user_id', 'role_id', 'status', 'created_at'], 'integer'],
            [['store_user_id'], 'exist', 'skipOnError' => true, 'targetClass' => StoreUser::className(), 'targetAttribute' => ['store_user_id' => 'id']],
            [['store_company_id'], 'exist', 'skipOnError' => true, 'targetClass' => StoreCompany::className(), 'targetAttribute' => ['store_company_id' => 'id']],
            [['role_id'], 'exist', 'skipOnError' => true, 'targetClass' => StoreUserRole::className(), 'targetAttribute' => ['role_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'store_company_id' => 'Store Company ID',
            'store_user_id' => 'Store User ID',
            'role_id' => 'Role ID',
            'status' => 'Status',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoreUser()
    {
        return $this->hasOne(StoreUser::className(), ['id' => 'store_user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoreCompany()
    {
        return $this->hasOne(StoreCompany::className(), ['id' => 'store_company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole()
    {
        return $this->hasOne(StoreUserRole::className(), ['id' => 'role_id']);
    }
}
