<?php

namespace common\models\store;

use common\models\company\CompanyType;
use common\models\Contractor;
use Yii;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "store_company".
 *
 * @property integer $id
 * @property integer $company_type_id
 * @property string $name
 * @property string $inn
 * @property string $kpp
 * @property string $address
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property CompanyType $companyType
 * @property StoreCompanyContractor[] $storeCompanyContractors
 * @property Contractor[] $contractors
 * @property StoreUserCompany[] $storeUserCompanies
 * @property StoreUser[] $storeUsers
 * @property StoreCompanyContractor $currentStoreCompanyContractor
 */
class StoreCompany extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 10;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'store_company';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_type_id', 'name', 'inn'], 'required'],
            [['company_type_id', 'status', 'created_at', 'updated_at'], 'integer'],
            [['name', 'address'], 'string', 'max' => 255],
            [['inn'], 'string', 'max' => 12],
            [['kpp'], 'string', 'max' => 9],
            [['company_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => CompanyType::className(), 'targetAttribute' => ['company_type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_type_id' => 'Company Type ID',
            'name' => 'Name',
            'inn' => 'Inn',
            'kpp' => 'Kpp',
            'address' => 'Address',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyType()
    {
        return $this->hasOne(CompanyType::className(), ['id' => 'company_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoreCompanyContractors()
    {
        return $this->hasMany(StoreCompanyContractor::className(), ['store_company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrentStoreCompanyContractor()
    {
        return $this->hasOne(StoreCompanyContractor::className(), ['store_company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContractors()
    {
        return $this->hasMany(Contractor::className(), ['id' => 'contractor_id'])
            ->viaTable('store_company_contractor', ['store_company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActiveContractors()
    {
        return $this->hasMany(Contractor::className(), ['id' => 'contractor_id'])
            ->viaTable('store_company_contractor', ['store_company_id' => 'id'], function($query) {
                $query->andOnCondition(['status' => StoreCompanyContractor::STATUS_ACTIVE]);
            })
            ->andOnCondition([
                'status' => Contractor::ACTIVE,
                'is_deleted' => Contractor::NOT_DELETED,
            ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoreUserCompanies()
    {
        return $this->hasMany(StoreUserCompany::className(), ['store_company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoreUsers()
    {
        return $this->hasMany(StoreUser::className(), ['id' => 'store_user_id'])->viaTable('store_user_company', ['store_company_id' => 'id']);
    }

    /**
     * @return StoreUserCompany|null
     */
    public function getChiefUser()
    {
        return $this->getStoreUserCompanies()
            ->alias('link')
            ->joinWith('storeUser user')
            ->andWhere([
                'link.role_id' => StoreUserRole::CHIEF,
                'link.status' => StoreUserCompany::STATUS_ACTIVE,
                'user.status' => StoreUser::STATUS_ACTIVE,
            ])
            ->orderBy(['created_at' => SORT_ASC])
            ->one();
    }

    /**
     * @return StoreCompanyContractor[]
     */
    public function getNewRequests()
    {
        return $this
            ->getStoreCompanyContractors()
            ->with('contractor', 'contractor.company')
            ->andWhere(['status' => StoreCompanyContractor::STATUS_NEW])
            ->all();
    }

    /**
     * @param bool|false $short
     * @return string
     */
    public function getTitle($short = false)
    {
        if ($this->companyType) {
            $companyTypeName = $short ? $this->companyType->name_short : $this->companyType->name_full;
            if ($this->company_type_id == CompanyType::TYPE_IP) {
                return $companyTypeName . ' ' . $this->name;
            }

            return $companyTypeName . ' "' . $this->name . '"';
        }

        return $this->name;
    }
}
