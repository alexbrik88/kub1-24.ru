<?php

namespace common\models\store;

use common\models\Contractor;
use Yii;

/**
 * This is the model class for table "store_company_contractor".
 *
 * @property integer $store_company_id
 * @property integer $contractor_id
 * @property integer $status
 * @property integer $created_at
 *
 * @property Contractor $contractor
 * @property StoreCompany $storeCompany
 */
class StoreCompanyContractor extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 10;
    const STATUS_NEW = 1;
    const STATUS_BLOCKED = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'store_company_contractor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['store_company_id', 'contractor_id'], 'required'],
            [['store_company_id', 'contractor_id', 'status', 'created_at'], 'integer'],
            [['contractor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contractor::className(), 'targetAttribute' => ['contractor_id' => 'id']],
            [['store_company_id'], 'exist', 'skipOnError' => true, 'targetClass' => StoreCompany::className(), 'targetAttribute' => ['store_company_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'store_company_id' => 'Store Company ID',
            'contractor_id' => 'Contractor ID',
            'status' => 'Status',
            'created_at' => 'Created At',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContractor()
    {
        return $this->hasOne(Contractor::className(), ['id' => 'contractor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoreCompany()
    {
        return $this->hasOne(StoreCompany::className(), ['id' => 'store_company_id']);
    }

    /**
     * @inheritdoc
     */
    public function accept($accept)
    {
        $message = 'Предложение от ' . $this->contractor->company->getTitle(true);

        switch ($accept) {
            case '0':
                $this->updateAttributes([
                    'status' => self::STATUS_BLOCKED,
                ]);
                Yii::$app->session->setFlash('success', $message . ' отклонено');
                break;

            case '1':
                $this->updateAttributes([
                    'status' => self::STATUS_ACTIVE,
                ]);
                Yii::$app->session->setFlash('success', $message . ' принято');
                break;
        }
    }
}
