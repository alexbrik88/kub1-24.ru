<?php

namespace common\models\store;

use Yii;

/**
 * This is the model class for table "store_user_role".
 *
 * @property integer $id
 * @property string $name
 *
 * @property StoreUserCompany[] $storeUserCompanies
 */
class StoreUserRole extends \yii\db\ActiveRecord
{
    const CHIEF = 1;
    const MANAGER = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'store_user_role';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoreUserCompanies()
    {
        return $this->hasMany(StoreUserCompany::className(), ['role_id' => 'id']);
    }
}
