<?php

namespace common\models\file;

use Yii;
use common\models\employee\Employee;
use common\models\Company;
use yii\db\Query;

/**
 * This is the model class for table "file_dir".
 *
 * @property integer $id
 * @property integer $company_id
 * @property string $name
 * @property integer $created_at
 * @property integer $created_at_author_id
 * @property integer $ord
 *
 * @property Employee $createdAtAuthor
 * @property Company $company
 */
class FileDir extends \yii\db\ActiveRecord
{
    protected $_hasEmployers;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'file_dir';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'name', 'hasEmployers'], 'required'],
            [['company_id', 'created_at', 'created_at_author_id', 'ord'], 'integer'],
            [['name'], 'string', 'max' => 255],
            [['created_at_author_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['created_at_author_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['name'], 'unique', 'targetAttribute' => ['company_id', 'name'],
                'message' => 'Папка с таким названием уже существует',
            ],
            ['hasEmployers', 'each', 'rule' => ['integer', 'skipOnEmpty' => false]]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'name' => 'Название',
            'created_at' => 'Created At',
            'created_at_author_id' => 'Created At Author ID',
            'ord' => 'Order',
            'hasEmployers' => 'Доступ'
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {

            if ($insert) {
                $this->created_at = time();
                $this->created_at_author_id = Yii::$app->user->id;
                $this->ord = 1 + $this->getMaxOrderValue();
            }

            return true;
        }

        return false;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedAtAuthor()
    {
        return $this->hasOne(Employee::className(), ['id' => 'created_at_author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    public function getMaxOrderValue()
    {
        return $this->find()
            ->where(['company_id' => Yii::$app->user->identity->company->id])
            ->max('ord') ?: 0;

    }

    public function move($direction)
    {
        if (!in_array($direction, ['up', 'down']))
            return false;

        if ($direction == 'up') {
            $swapModel = self::find()->where([
                'company_id' => Yii::$app->user->identity->company->id])
                ->andWhere(['<', 'ord', $this->ord])
                ->orderBy(['ord' => SORT_DESC])
                ->one();
        } else {
            $swapModel = self::find()->where([
                'company_id' => Yii::$app->user->identity->company->id])
                ->andWhere(['>', 'ord', $this->ord])
                ->orderBy(['ord' => SORT_ASC])
                ->one();
        }

        if ($swapModel) {
            list($this->ord, $swapModel->ord) = array($swapModel->ord, $this->ord);
            $this->updateAttributes(['ord']);
            $swapModel->updateAttributes(['ord']);
        }

        return ($swapModel) ? $swapModel->id : null;
    }

    public function getEmployers() {
        return $this->hasMany(Employee::className(), ['id' => 'employee_id'])->viaTable('file_dir_employee', ['dir_id' => 'id']);
    }

    public function getHasEmployers() {
        return $this->_hasEmployers;
    }

    public function setHasEmployers($value) {
        $this->_hasEmployers = $value;
    }

    public function insertEmployersLinks()
    {
        foreach ($this->_hasEmployers as $employee_id) {
            if ($employee = Employee::findOne(['company_id' => $this->company_id, 'id' => $employee_id])) {
                $this->link('employers', $employee);
            }
        }

        return true;
    }

    public function updateEmployersLinks()
    {
        $this->unlinkAll('employers', true);

        if (!is_array($this->_hasEmployers))
            return true;

        foreach ($this->_hasEmployers as $employee_id) {
            if ($employee = Employee::findOne(['company_id' => $this->company_id, 'id' => $employee_id])) {
                $this->link('employers', $employee);
            }
        }

        return true;
    }

    public function deleteEmployersLinks()
    {
        $this->unlinkAll('employers', true);
        return true;
    }

    public function checkEmployeeAccess($user_id = null) {

        if (!$user_id)
            $user_id = Yii::$app->user->id;

        $isChief = Yii::$app->user->identity->company->getEmployeeChief()->id == Yii::$app->user->id;

        $query = new Query();
        return $isChief || $query->select(['id'])
            ->from('file_dir_employee')
            ->where(['dir_id' => $this->id, 'employee_id' => $user_id])->exists();
    }

    public static function getEmployeeAllowedDirs()
    {
        if (Yii::$app->user->identity->company->getEmployeeChief()->id == Yii::$app->user->id) {
            $dirs = self::find()->where(['company_id' => Yii::$app->user->identity->company->id])->orderBy('ord')->all();
        } else {
            $dirs = Yii::$app->user->identity->fileDirs;
        }

        return $dirs;
    }

    public function getFiles() {
        return $this->hasMany(File::className(), ['directory_id' => 'id']);
    }
}
