<?php

namespace common\models\file;

use Yii;

/**
 * This is the model class for table "file_upload_type".
 *
 * @property integer $id
 * @property string $name
 *
 * @property File[] $files
 */
class FileUploadType extends \yii\db\ActiveRecord
{
    const TYPE_PC = 1;
    const TYPE_API = 2;
    const TYPE_EMAIL = 3;

    public static $nameById = [
        1 => 'С компьютера',
        2 => 'Моб. прилож-е',
        3 => 'По e-mail'
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'file_upload_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasMany(File::className(), ['upload_type_id' => 'id']);
    }
}
