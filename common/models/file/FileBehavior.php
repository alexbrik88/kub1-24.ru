<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 10/15/15
 * Time: 1:16 PM
 * Email: t.kanstantsin@gmail.com
 */

namespace common\models\file;

use Exception;
use Yii;
use Yii\base\Behavior;
use yii\base\Event;
use Yii\db\ActiveRecord;
use yii\helpers\FileHelper;

/**
 * Class FileBehavior
 *
 * @property File $owner
 */
class FileBehavior extends Behavior
{

    /**
     * @var string
     */
    public $filePathField;
    /**
     * @var string
     */
    public $fileField;

    /**
     * @var bool
     */
    public $subFolder = null;

    /**
     * @var string
     */
    public $primaryKey = 'id';

    /**
     * @inheritdoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_DELETE => 'afterDelete',
            ActiveRecord::EVENT_AFTER_INSERT => 'afterInsert',
        ];
    }


    /**
     * Saved uploaded file.
     * @param Event $event
     * @throws Exception
     * @return bool
     */
    public function afterInsert($event)
    {

        if (!empty($this->owner->{$this->fileField})) {
            $uploadPath = $this->owner->getUploadPath();

            if (!file_exists($uploadPath)) {
                FileHelper::createDirectory($uploadPath);
            }

            $filePath = $uploadPath . DIRECTORY_SEPARATOR . $this->owner->{$this->primaryKey};

            if (!$this->owner->{$this->fileField}->saveAs($filePath)) {
                throw new Exception('Error in file saving');
            }
        }
    }

    /**
     * @param Event $event
     */
    public function afterDelete($event)
    {
        $path = $this->owner->getFilePath();
        if ($this->fileExist($path)) {
            // TODO: mark file as `deleted` and clear by schedule.
            unlink($path);
        }
    }

    /**
     * @param string $uploadPath
     * @return bool
     */
    protected function savePath($uploadPath)
    {
        return FileHelper::createDirectory($uploadPath);
    }

    /**
     * @param string $sourcePath
     * @param string $uploadPath
     * @return bool
     */
    protected function saveFile($sourcePath, $uploadPath)
    {
        return move_uploaded_file($sourcePath, $uploadPath . DIRECTORY_SEPARATOR . $this->owner->{$this->primaryKey});
    }


    /**
     * @return string
     */
    public function getPath()
    {
        return $this->owner->getFilePath();
    }

    /**
     * @param string $filePath absolute image path
     * @return bool
     */
    public function fileExist($filePath = null)
    {
        $filePath = $filePath ?: $this->getPath();

        return file_exists($filePath);
    }

    public function getFileSize()
    {
        if ($this->fileExist()) {
            return filesize($this->getPath());
        } else {
            return 0;
        }
    }

}
