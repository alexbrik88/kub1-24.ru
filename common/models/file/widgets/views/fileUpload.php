<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 21.10.15
 * Time: 18.25
 * Email: t.kanstantsin@gmail.com
 */

use common\models\file\File;
use yii\bootstrap\Html;

/* @var \yii\web\View $this */

/* @var \common\models\file\widgets\FileUpload $widget */
$widget = $this->context;
?>

<div class="upload-area" style="margin: 7px 0;"
     data-upload-url="<?= $widget->uploadUrl ?>"
     data-delete-url="<?= $widget->deleteUrl; ?>"
     data-list-url="<?= $widget->listUrl; ?>"
     data-csrf-parameter="<?= Yii::$app->request->csrfParam; ?>"
     data-csrf-token="<?= Yii::$app->request->csrfToken; ?>"
>
    <div class="file-list"></div>
    <div id="file-ajax-loading" style="display: none;"><img src="/img/loading.gif"></div>


    <?= Html::button(Html::icon('download-alt', [
        'class' => 'upload-button',
    ]), [
        'class' => 'upload-button hide btn yellow btn-sm',
        'style'=>"width:33px; height: 27px;",
    ]); ?>
</div>