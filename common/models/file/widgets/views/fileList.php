<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 21.10.15
 * Time: 18.25
 * Email: t.kanstantsin@gmail.com
 */

use common\widgets\Modal;
use yii\bootstrap\Html;

/* @var \yii\web\View $this */

/* @var \common\models\file\widgets\FileList $widget */
$widget = $this->context;
?>

<div class="file-list">
    <?php foreach ($widget->files as $file): ?>
        <?php $fileLinkClass = null;
        $tooltipId = null;
        $contentPreview = null;
        if (in_array($file->ext, ['jpeg', 'jpg', 'png', 'gif', 'bmp', 'pdf'])) {
            $fileLinkClass = ' file-link-preview';
            $tooltipId = 'file-link-preview-' . $file->id;
            $thumb = $file->getImageThumb(400, 600);
            if ($thumb) {
                $contentPreview .= Html::beginTag('div', ['class' => 'hidden', 'style' => 'display: inline-block;']);
                $contentPreview .= Html::beginTag('span', ['id' => $tooltipId]);
                $contentPreview .= Html::img($thumb, ['alt' => '']);
                $contentPreview .= Html::endTag('span');
                $contentPreview .= Html::endTag('div');
            }
        }; ?>
        <div id="file-<?= $file->id; ?>" class="file <?= $file->isNewRecord ? 'file-template' : ''; ?>"
             data-id="<?= $file->id; ?>"
            >
            <?php
            if ($widget->viewAccessCallback === null || call_user_func($widget->viewAccessCallback, $file)) {
                echo '<span class="icon icon-paper-clip m-r-10' . $fileLinkClass . '" data-tooltip-content="#' . $tooltipId . '"></span>' . $contentPreview;
                echo Html::a($file->filename_full, call_user_func($widget->fileLinkCallback, $file, $widget->ownerModel), [
                    'class' => 'file-link',
                    'target' => '_blank',
                    'download' => '',
                ]);
            } else {
                echo $filename;
            }

            if ($widget->deleteAccessCallback === null || call_user_func($widget->deleteAccessCallback, $file)) {
                echo '<span class="icon icon-close m-l-10" style="cursor:pointer;"
                        data-toggle="modal"
                        data-target="#delete-file-modal-' . $file->id . '"
                        ></span>';
                echo Modal::widget([
                    'id' => 'delete-file-modal-' . $file->id,
                    'toggleButton' => false,
                    'options' => [
                        'class' => 'confirm-modal fade',
                    ],
                    'header' => null,
                    'closeButton' => false,
                    'content' => join('', [
                        '<div class="form-body"><div class="row">Вы уверены, что хотите удалить файл?</div></div>',
                        Html::tag('div', join('', [
                            Html::tag('div', Html::button('ДА', [
                                'class' => 'delete-file btn darkblue pull-right',
                                'data-file' => $file->id,
                                'data-dismiss' => 'modal',
                            ]), ['class' => 'col-md-6',]),
                            Html::tag('div', Html::button('НЕТ', [
                                'class' => 'btn darkblue',
                                'data-dismiss' => 'modal',
                            ]), ['class' => 'col-md-6',]),
                        ]), [
                            'class' => 'form-actions row',
                        ]),
                    ]),
                ]);
            } ?>
        </div>
    <?php endforeach; ?>
</div>
