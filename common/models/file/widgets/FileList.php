<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 21.10.15
 * Time: 18.24
 * Email: t.kanstantsin@gmail.com
 */

namespace common\models\file\widgets;


use Closure;
use common\models\file\File;
use yii\base\InvalidConfigException;
use yii\base\Widget;

class FileList extends Widget
{
    /**
     * @var File[]
     */
    public $files;
    /**
     * @var Model
     */
    public $ownerModel;
    /**
     * @var Closure
     */
    public $fileLinkCallback;
    /**
     * @var Closure
     */
    public $viewAccessCallback;
    /**
     * @var Closure
     */
    public $deleteAccessCallback;

    /**
     * @inheritdoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if ($this->fileLinkCallback === null) {
            throw new InvalidConfigException('Ссылка на файл не может быть создана');
        }
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        return $this->render('fileList');
    }
}