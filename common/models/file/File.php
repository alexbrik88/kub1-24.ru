<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 10/15/15
 * Time: 12:53 PM
 * Email: t.kanstantsin@gmail.com
 */

namespace common\models\file;

use common\components\helpers\ArrayHelper;
use common\models\Company;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\InvoiceFacture;
use common\models\document\PackingList;
use common\models\employee\Employee;
use frontend\modules\api\components\ApiController;
use himiklab\thumbnail\EasyThumbnailImage;
use himiklab\thumbnail\FileNotFoundException;
use Imagine\Image\Box;
use Imagine\Image\ManipulatorInterface;
use Yii;
use yii\base\Exception;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\imagine\Image;
use yii\web\UploadedFile;

/**
 * This is the model class for table "file".
 *
 * @property integer $id
 * @property string $filename
 * @property string $ext
 * @property string $filename_full
 * @property string $filepath
 * @property string $hash
 * @property integer $created_at
 * @property integer $created_at_author_id
 * @property string $owner_model
 * @property string $owner_table
 * @property integer $owner_id
 * @property integer $company_id
 * @property integer $is_deleted
 * @property integer $directory_id
 * @property integer $upload_type_id
 *
 * @property Employee $createdAtAuthor
 *
 * @property string $subFolderName
 */
class File extends ActiveRecord
{
    /**
     * Owner model. Needed to saving.
     * @var ActiveRecord
     */
    public $ownerModel;

    /**
     * Fake field for file uploading
     * @var UploadedFile
     */
    public $file;

    /**
     * Path for file saving
     * Must be defined in extended class.
     * @var string
     */
    public $uploadDirectoryPath;

    /**
     * @var integer
     */
    public static $maxFileSize = 3 * 1024 * 1024;

    /**
     * @var integer
     */
    public static $fileExtensions = 'jpeg, jpg, png, gif, bmp, pdf, doc, docx, xlsx, txt, xls, xml';

    public static $lifetimeInTrash = 2592000; // 30 days

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'file';
    }

    /**
     * @return FileQuery
     */
    public static function find()
    {
        return new FileQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
            ],
            'fileBehavior' => [
                'class' => FileBehavior::className(),
                'filePathField' => 'uploadDirectoryPath',
                'fileField' => 'file',
                'subFolder' => 'subFolderName',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // defaults
            [
                ['owner_model'], 'default', 'value' => function (File $model, $attribute) {
                if ($this->ownerModel !== null) {
                    $model->$attribute = $model->ownerModel->className();
                }
            },
            ],
            [
                ['owner_id'], 'default',
                'value' => function (File $model, $attribute) {
                    if ($this->ownerModel !== null) {
                        $model->$attribute = $model->ownerModel->id;
                    }
                },
            ],

            // db rules
            [['file', 'owner_model', 'owner_id',], 'required',],
            [['owner_model'], 'string', 'max' => 255],
            [['owner_id', 'company_id', 'directory_id', 'upload_type_id'], 'integer'],
//            [['filename', 'filename_full', 'filepath', 'hash'], 'required',],
//            [['filename'], 'string', 'max' => 1023],
//            [['ext', 'hash', 'owner_model'], 'string', 'max' => 255],
//            [['filename_full'], 'string', 'max' => 1278],
//            [['filepath'], 'string', 'max' => 5000],
            // file rules
            [['uploadDirectoryPath'], 'required'],
            [
                ['uploadDirectoryPath'],
                function ($attribute, $params) {
                    // check if path exists or can be created.
                    try {
                        FileHelper::createDirectory(Yii::getAlias(Yii::$app->params['uploadDir']) . DIRECTORY_SEPARATOR . $this->$attribute);
                    } catch (Exception $e) {
                        $this->addError($attribute, $e->getMessage());
                    }
                },
                'when' => function (File $model) {
                    return $model->uploadDirectoryPath !== null;
                },
            ],
            'file' => [
                ['file'], 'file',
                'extensions' => self::$fileExtensions,
                'maxSize' => self::$maxFileSize,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'filename' => 'Имя файла',
            'ext' => 'Расширение',
            'filename_full' => 'Полное имя файла',
            'filepath' => 'Путь к файлу',
            'hash' => 'Hash',
            'created_at' => 'Дата создания',
            'created_at_author_id' => 'Автор файла',
            'owner_model' => 'Модель файла',
            'owner_id' => 'ID модели файла',

            'file' => 'Файл',
            'filesize' => 'Размер'
        ];
    }

    /**
     * @inheritdoc
     */
    public function fields()
    {
        $fields = [
            'id',
            'filename',
            'ext',
            'filename_full',
            'hash',
            'created_at',
            'created_at_author_id',
            'owner_id',
            'upload_type_id'
        ];

        return array_combine($fields, $fields);
    }

    /**
     * @inheritdoc
     */
    public function afterValidate()
    {
        if (!$this->hasErrors()) {
            $this->filename = $this->file->baseName;
            $this->ext = $this->file->extension;
            $this->filename_full = $this->file->name;
            $this->filepath = $this->uploadDirectoryPath . DIRECTORY_SEPARATOR . $this->owner_id;
            $this->filesize = $this->file->size;
            $this->upload_type_id = ArrayHelper::getValue(Yii::$app->params, 'upload_type_id', 1);

            $this->hash = hash_file('md5', $this->file->tempName);
        }

        parent::afterValidate();
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        $this->owner_table = $this->owner_model::tableName();

        if (Yii::$app->controller instanceof ApiController) {
            $user = Yii::$app->controller->user;
        } else {
            $user = Yii::$app->user->identity;
        }

        if ($insert && $this->created_at_author_id === null) {
            $this->created_at_author_id = $user->id;
        }
        if ($insert && $this->company_id === null) {
            $this->company_id = $user->company->id;
        }

        return parent::beforeSave($insert);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedAtAuthor()
    {
        return $this->hasOne(Employee::className(), ['id' => 'created_at_author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDirectory()
    {
        return $this->hasOne(FileDir::className(), ['id' => 'directory_id']);
    }

    /**
     * Subfolder for grouping uploaded files.
     * By default files goups by owner's id.
     * @return string
     */
    public function getSubFolderName()
    {
        return $this->owner_id;
    }

    /**
     * @inheritdoc
     */
    public function getUploadPath()
    {
        return Company::fileUploadPath($this->company_id) . DIRECTORY_SEPARATOR . $this->filepath;
    }

    /**
     * @return string
     */
    public function getFilePath()
    {
        return $this->getUploadPath() . DIRECTORY_SEPARATOR . $this->id;
    }

    /**
     * get image thumbnail
     *
     * @param  integer $width
     * @param  integer $height
     * @param  string  $mode    EasyThumbnailImage::THUMBNAIL_INSET, the original image
     * @param  integer $quality
     * @return string
     */
    public function getImageThumb($width = 200, $height = 200, $mode = EasyThumbnailImage::THUMBNAIL_INSET, $quality = 100)
    {
        try {
            if (in_array($this->ext, ['jpeg', 'jpg', 'png', 'gif', 'bmp', 'pdf']) && $this->company_id) {
                $thumbnailSrc = '';
                $ds = DIRECTORY_SEPARATOR;
                $filename = $this->getFilePath();
                if (file_exists($filename)) {
                    $pdfConvertedPath = null;
                    $cachePath = Yii::getAlias("@runtime") . "{$ds}cache{$ds}thumbnails{$ds}{$this->company_id}";
                    $thumbnailFileName = md5($filename . $width . $height . $mode . filemtime($filename));
                    $thumbnailExt = $this->ext == 'pdf' ? 'jpg' : $this->ext;
                    $thumbnailFile = $cachePath . $ds . substr($thumbnailFileName, 0, 2) . "{$ds}{$thumbnailFileName}.{$thumbnailExt}";
                    $thumbnailSrc = Url::to(['/thumbnail/index', 'filename' => "{$thumbnailFileName}.{$thumbnailExt}"]);

                    if (file_exists($thumbnailFile)) {
                        if (Yii::$app->thumbnail->cacheExpire !== 0 && (time() - filemtime($thumbnailFile)) > Yii::$app->thumbnail->cacheExpire) {
                            unlink($thumbnailFile);
                        } else {
                            return $thumbnailSrc;
                        }
                    }
                    if ($this->ext == 'pdf') {
                        $newFileName = 'convertedFromPDF';
                        $pdfConvertedPath = Yii::getAlias('@common') . $ds . 'uploads' . $ds . $this->filepath . $ds . $newFileName;
                        exec("convert -density 300 \"{$filename}\"[0] \"{$pdfConvertedPath}\"", $output, $returnVar);
                        if (!file_exists($pdfConvertedPath)) {
                            return '';
                        }
                        $filename = $pdfConvertedPath;
                    }

                    self::thumbnailFile($filename, $thumbnailFile, $width, $height, $mode, $quality);

                    if ($pdfConvertedPath) {
                        unlink($pdfConvertedPath);
                    }

                    return $thumbnailSrc;
                }
            }
        } catch (\Exception $e) {
        }

        return '';
    }

    /**
     * Creates and caches the image thumbnail and returns full path from thumbnail file.
     *
     * @param string $filename the sourse image file path
     * @param string $thumbnailFile the result image file path
     * @param integer $width the width in pixels to create the thumbnail
     * @param integer $height the height in pixels to create the thumbnail
     * @param string $mode EasyThumbnailImage::THUMBNAIL_INSET, the original image
     * @param integer $quality
     * @return string
     * @throws FileNotFoundException
     * @throws \Imagine\Exception\InvalidArgumentException
     * @throws \Imagine\Exception\RuntimeException
     * @throws \yii\base\InvalidParamException
     */
    protected static function thumbnailFile($filename, $thumbnailFile, $width, $height, $mode = self::THUMBNAIL_OUTBOUND, $quality = null)
    {
        $thumbnailDir = dirname($thumbnailFile);
        if (!is_dir($thumbnailDir)) {
            mkdir($thumbnailDir, EasyThumbnailImage::MKDIR_MODE, true);
        }

        $box = new Box($width, $height);
        $image = Image::getImagine()->open($filename);
        $image = $image->thumbnail($box, $mode);

        $options = [
            'quality' => $quality === null ? EasyThumbnailImage::QUALITY : $quality
        ];
        $image->save($thumbnailFile, $options);

        return $thumbnailFile;
    }

    /**
     * @param Company $company
     * @return bool
     */
    public static function getNewFilesCount(Company $company)
    {
        return File::find()
            ->andWhere(['company_id' => $company->id])
            ->andWhere(['is_deleted' => 0])
            ->andWhere(['is', 'directory_id', null])
            ->andWhere(['owner_table' => 'scan_document'])
            ->count();
    }
}
