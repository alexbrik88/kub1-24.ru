<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 12/5/15
 * Time: 3:47 AM
 * Email: t.kanstantsin@gmail.com
 */

namespace common\models\file;

use yii\db\ActiveQuery;

/**
 * Class FileQuery
 * @package common\models\file
 */
class FileQuery extends ActiveQuery
{

    /**
     * @param string $ownerModel
     * @return $this
     */
    public function byOwnerModel($ownerModel)
    {
        return $this->andWhere([
            'owner_model' => $ownerModel,
        ]);
    }

    /**
     * @param integer $author_id
     * @return $this
     */
    public function byCreatedAtAuthorId($author_id)
    {
        return $this->andWhere(['in', 'created_at_author_id', $author_id,]);
    }

}
