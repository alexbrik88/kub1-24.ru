<?php

namespace common\models\file\actions;

use common\models\file\File;
use common\models\file\FileHeaderHelper;
use common\models\file\widgets\FileList;
use common\models\ICompanyStrictQuery;
use Yii;
use yii\base\Action;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

/**
 * Class ListAction
 * @package common\components\fileUpload
 *
 * @example acton definition:
 * public function actions()
 * {
 *      return [
 *          'file-list' => [
 *              'class' => \common\models\file\actions\FileListAction::className(),
 *              'model' => Act::className(),
 *              'fileNameField' => 'document_filename',
 *              'folderPath' => DIRECTORY_SEPARATOR . 'documents' . DIRECTORY_SEPARATOR . 'act',
 *          ],
 *      ];
 * }
 *
 * @example url path: /path/to/controller/get-file?id=1&filename=example.jpg
 *
 * @todo: create class/interface/behavior to detect `right` models.
 */
class FileListAction extends Action
{
    /**
     * Class name
     * @var string
     */
    public $model;

    public $idField = 'id';

    /**
     * Whether to check company in getting model.
     * For example it is unnecessary for admin.
     * @var bool
     */
    public $strictByCompany = true;

    /**
     * @var
     */
    public $fileLinkCallback;


    /**
     * Initializes the action.
     * @throws InvalidConfigException if the font file does not exist.
     */
    public function init()
    {
    }

    /**
     * Runs the action.
     * This method displays the view requested by the user.
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function run()
    {
        $id = Yii::$app->request->getQueryParam('id');

        $model = $this->findModel($id);

        $files = File::find()->andWhere([
            'owner_id' => $model->id,
            'owner_model' => $model->className(),
            'is_deleted' => 0
        ])->all();

        Yii::$app->response->content = FileList::widget([
            'files' => $files,
            'ownerModel' => $model,
            'fileLinkCallback' => $this->fileLinkCallback,
        ]);
        Yii::$app->end();
    }

    /**
     * @param $id
     * @return ActiveRecord
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    private function findModel($id)
    {
        if ($id === null) {
            throw new BadRequestHttpException('`id` not passed into parameters.');
        }

        /* @var ActiveRecord $class */
        $class = $this->model;
        $query = $class::find();

        if ($this->strictByCompany) {
            if ($query instanceof ICompanyStrictQuery) {
                $query->byCompany(Yii::$app->user->identity->company->id);
            } else {
                $query->andWhere([
                    $class::tableName() . '.company_id' => Yii::$app->user->identity->company->id,
                ]);
            }
        }

        $query->andWhere([
            $class::tableName() . '.' . $this->idField => $id,
        ]);

        $model = $query->one();

        if ($model === null) {
            throw new NotFoundHttpException('Owner model not found.');
        }

        return $model;
    }
}
