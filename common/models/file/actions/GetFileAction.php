<?php

namespace common\models\file\actions;

use common\models\file\FileHeaderHelper;
use common\models\ICompanyStrictQuery;
use Yii;
use yii\base\Action;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;
use yii\helpers\FileHelper;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

/**
 * Class GetFileAction
 * @package common\components\fileUpload
 *
 * @example acton definition:
 * public function actions()
 * {
 *      return [
 *          'get-file' => [
 *              'class' => \common\components\fileUpload\GetFileAction::className(),
 *              'model' => ActIn::className(),
 *              'fileNameField' => 'document_filename',
 *              'folderPath' => DIRECTORY_SEPARATOR . 'documents' . DIRECTORY_SEPARATOR . 'act',
 *          ],
 *      ];
 * }
 *
 * @example url path: /path/to/controller/get-file?id=1&filename=example.jpg
 */
class GetFileAction extends Action
{
    /**
     * Class name
     * @var string
     */
    public $model;
    /**
     * @var callable a PHP callable that will be called to return the model corresponding
     * to the specified primary key value. If not set, [[findModel()]] will be used instead.
     * The signature of the callable should be:
     *
     * ```php
     * function ($id, $action) {
     *     // $id is the primary key value.
     *     // $action is the action object currently running
     * }
     * ```
     *
     * The callable should return the model found, or throw an exception if not found.
     */
    public $findModel;
    /**
     * Whether to check company in getting model.
     * For example it is unnecessary for admin.
     * @var bool
     */
    public $strictByCompany = true;
    /**
     * @var string
     */
    public $idField = 'id';
    /**
     * @var string
     */
    public $extField = 'ext';
    /**
     * @var string
     */
    public $idParam = 'id';
    /**
     * @var string
     */
    public $inline = false;

    /**
     * Name of attribute of model that store file name, or callable that returns this name.
     * @var string|callable
     */
    public $fileNameField;

    /**
     * @var callable
     */
    public $fileNameCallback;

    /**
     * Path to base folder with all files, relative to document root
     * @var string
     */
    public $fileBasePath;

    /**
     * Folder path, relative to base path
     * @var string|callable
     */
    public $folderPath;

    /**
     * Perform some actions with file before render.
     * @var callable
     */
    public $getFileCallback;


    /**
     * Initializes the action.
     * @throws InvalidConfigException if the font file does not exist.
     */
    public function init()
    {
        if ($this->fileBasePath === null) {
            $this->fileBasePath = Yii::getAlias(Yii::$app->params['uploadDir']);
        }
        if ($this->fileNameField === null) {
            throw new InvalidConfigException('The file name field does not set.');
        }
        if ($this->model === null || !class_exists($this->model)) {
            throw new InvalidConfigException('The model file does not exist: ' . $this->model);
        }
        if ($this->getFileCallback !== null && !is_callable($this->getFileCallback)) {
            throw new InvalidConfigException('File callback must be callable.');
        }
    }

    /**
     * Runs the action.
     * This method displays the view requested by the user.
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function run()
    {
        $id = Yii::$app->request->getQueryParam('id');
        $fileId = Yii::$app->request->getQueryParam($this->idParam);

        $fileName = Yii::$app->request->getQueryParam('filename');
        $model = $this->findModel($id, $fileId, $fileName);
        $filePath = $this->getFilePath($model) . DIRECTORY_SEPARATOR . $this->getRealFileName($model);

        if (!file_exists($filePath) || !is_readable($filePath)) {
            throw new NotFoundHttpException('Requested file not found.');
        }

        if (empty($fileName)) {
            $fileName = $this->getFileName($model);
        }

        $options = [
            'inline' => (bool) Yii::$app->request->getQueryParam('inline', $this->inline),
        ];

        if (Yii::$app->request->getQueryParam('pdf')) {
            $options['mimeType'] = 'application/pdf';
        } elseif ($mimeType = FileHelper::getMimeTypeByExtension($fileName)) {
            $options['mimeType'] = $mimeType;
        }

        if ($this->getFileCallback !== null) {
            return call_user_func($this->getFileCallback, $filePath, $this);
        } else {
            return \Yii::$app->response->sendFile($filePath, $fileName, $options);
        }
    }

    /**
     * Returns full file path
     * @param ActiveRecord $model
     * @param bool $absolute
     * @return string
     */
    private function getFilePath(ActiveRecord $model, $absolute = false)
    {
        if (is_callable($this->folderPath)) {
            return call_user_func($this->folderPath, $model);
        } else {
            return $this->fileBasePath . $this->folderPath;
        }
    }

    /**
     * @param $id
     * @param $fileName
     * @return null|ActiveRecord
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    private function findModel($ownerId, $fileId, $fileName = null)
    {
        if ($fileId === null) {
            throw new BadRequestHttpException('Neither `fileId` not passed into parameters.');
        }

        if ($this->findModel !== null) {
            return call_user_func($this->findModel, $ownerId, $fileId, $fileName, $this);
        }

        /* @var ActiveRecord $class */
        $class = $this->model;
        $query = $class::find();

        if ($this->strictByCompany) {
            if ($query instanceof ICompanyStrictQuery) {
                $query->byCompany(Yii::$app->user->identity->company->id);
            } else {
                $query->andWhere([
                    $class::tableName() . '.company_id' => Yii::$app->user->identity->company->id,
                ]);
            }
        }

        $query->andWhere([
            $class::tableName() . '.' . $this->idField => $fileId,
        ]);

        $query->andFilterWhere([
            $class::tableName() . '.' . $this->fileNameField => $fileName,
        ]);

        $model = $query->one();

        if ($model === null) {
            throw new NotFoundHttpException('Model not found.');
        }

        return $model;
    }

    /**
     * Returns file name on filesystem
     * @param ActiveRecord $model
     * @return string
     */
    private function getRealFileName(ActiveRecord $model)
    {
        if ($this->fileNameCallback !== null) {
            return call_user_func($this->fileNameCallback, $model);
        } else {
            return $model->{$this->idField};
        }
    }

    /**
     * @param ActiveRecord $model
     * @return mixed
     */
    private function getFileName(ActiveRecord $model)
    {
        return $model->{$this->fileNameField};
    }
}
