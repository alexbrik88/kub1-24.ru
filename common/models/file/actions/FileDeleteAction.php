<?php

namespace common\models\file\actions;

use common\models\file;
use common\models\ICompanyStrictQuery;
use Yii;
use yii\base\Action;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;

/**
 * Class DeleteAction
 * @package common\components\fileUpload
 *
 * @example acton definition:
 * public function actions()
 * {
 *      return [
 *          'file-delete' => [
 *              'class' => file\actions\FileDeleteAction::className(),
 *              'model' => Model::className(),
 *          ],
 *      ];
 * }
 *
 * @example url path: /path/to/controller/get-file?id=1&filename=example.jpg
 *
 * @todo: create class/interface/behavior to detect `right` models.
 */
class FileDeleteAction extends Action
{
    /**
     * Class name
     * @var string
     */
    public $model;

    public $modelIdParam = 'id';
    public $fileIdParam = 'file-id';

    /**
     * @var string
     */
    public $idField = 'id';

    /**
     * Initializes the action.
     * @throws InvalidConfigException if the font file does not exist.
     */
    public function init()
    {
    }

    /**
     * Runs the action.
     * This method displays the view requested by the user.
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function run()
    {
        $modelId = Yii::$app->request->get($this->modelIdParam);
        $fileId = Yii::$app->request->post($this->fileIdParam);
        /* @var file\File $file */
        $file = file\File::find()->andWhere([
            'id' => $fileId,
            'owner_id' => $modelId,
            'owner_model' => $this->model,
            'company_id' => Yii::$app->user->identity->company->id,
        ])->one();
        $model = $this->model::findOne([
            $this->idField => $modelId,
        ]);

        if ($file) {
            $file->delete(); // file itself will be deleted by behavior
            if ($model && $model->hasAttribute('has_file')) {
                $model->updateHasFile();
            }
            $path = Yii::getAlias('@common/uploads') . DIRECTORY_SEPARATOR . $file->filepath;

            if (file_exists($path . DIRECTORY_SEPARATOR . $fileId)) {
                unlink($path . DIRECTORY_SEPARATOR . $fileId);
            }
        } else {
            // throw new NotFoundHttpException('Файл не найден.' . $fileId . ' - ' . $modelId . ' - ' . $this->model);
        }

        Yii::$app->end();
    }
}
