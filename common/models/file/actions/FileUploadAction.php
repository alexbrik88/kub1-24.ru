<?php

namespace common\models\file\actions;

use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\InvoiceAuto;
use common\models\document\InvoiceFacture;
use common\models\document\OrderDocument;
use common\models\document\PackingList;
use common\models\document\Upd;
use common\models\employee\Employee;
use common\models\file;
use common\models\ICompanyStrictQuery;
use common\models\store\StoreUser;
use frontend\models\Documents;
use Yii;
use yii\base\Action;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\web\UploadedFile;

/**
 * Class UploadAction
 * @package common\components\fileUpload
 *
 * @example acton definition:
 * public function actions()
 * {
 *      return [
 *          'file-upload' => [
 *              'class' => \common\models\file\actions\FileUploadAction::className(),
 *              'model' => Model::className(),
 *          ],
 *      ];
 * }
 *
 * @example url path: /path/to/controller/get-file?id=1&filename=example.jpg
 *
 * @todo: create class/interface/behavior to detect `right` models.
 */
class FileUploadAction extends Action
{
    /**
     * Class name
     * @var string
     */
    public $model;

    /**
     * Whether to check company in getting model.
     * For example it is unnecessary for admin.
     * @var bool
     */
    public $strictByCompany = true;

    /**
     * @var string
     */
    public $modelIdParam = 'id';
    /**
     * @var string
     */
    public $fileIdParam = 'file-id';

    /**
     * @var string
     */
    public $idField = 'id';

    public $folderPath;

    /**
     * @var int
     */
    public $maxFileCount = 3;

    /**
     * @var int
     */
    public $maxFileSize;

    /**
     * @var boolean
     */
    public $noLimitCountFile = false;

    /**
     * Initializes the action.
     * @throws InvalidConfigException if the font file does not exist.
     */
    public function init()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
    }

    /**
     * Runs the action.
     * This method displays the view requested by the user.
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    public function run()
    {
        /* @var $user Employee|StoreUser */
        $user = Yii::$app->user->identity;
        $company = $user instanceof Employee ?
            $user->company :
            $user->currentStoreCompany->currentStoreCompanyContractor->contractor->company;

        if (!$company->uploadAllowed()) {
            return [
                'success' => false,
                'msg' => "У вас закончилось место на диске для хранения сканов и документов. Использовано более 1 ГБ. Освободите место или перейдите на платный тариф.",
            ];
        }

        $modelId = Yii::$app->request->get($this->modelIdParam);
        $documentModel = $this->findModel($modelId);
        $documentModelClass = $this->model;

        $fileCount = \common\models\file\File::find()->andWhere([
            'owner_model' => $documentModel->className(),
            'owner_id' => $documentModel->{$this->idField},
        ])->count();
        $scanCount = \common\models\document\ScanDocument::find()->andWhere([
            'owner_model' => $documentModel->className(),
            'owner_id' => $documentModel->{$this->idField},
        ])->count();

        if (($fileCount + $scanCount) >= $this->maxFileCount) {
            return [
                'success' => false,
                'msg' => 'Максимальное количество файлов: ' . $this->maxFileCount . '.',
            ];
        }

        if ($this->maxFileSize !== null) {
            file\File::$maxFileSize = $this->maxFileSize;
        }
        $file = new file\File();
        $file->owner_model = $documentModelClass;
        $file->owner_id = $modelId;
        $file->uploadDirectoryPath = $this->folderPath;
        $file->file = UploadedFile::getInstanceByName('file');
        $file->filesize = $file->file->size;

        if ($file->save()) {
            if ($documentModel->hasAttribute('has_file') && !$documentModel->has_file) {
                $documentModel->updateAttributes(['has_file' => true]);
            }
            $event = null;
            switch (true) {
                case $documentModel instanceof Invoice:
                    $event = $documentModel->type == Documents::IO_TYPE_IN ? 39 :
                            ($documentModel->type == Documents::IO_TYPE_OUT ? 17 : null);
                    break;
                case $documentModel instanceof Act:
                    $event = $documentModel->type == Documents::IO_TYPE_IN ? 41 : null;
                    break;
                case $documentModel instanceof PackingList:
                    $event = $documentModel->type == Documents::IO_TYPE_IN ? 43 : null;
                    break;
                case $documentModel instanceof InvoiceFacture:
                    $event = $documentModel->type == Documents::IO_TYPE_IN ? 45 : null;
                    break;
            }
            if ($event) {
                \common\models\company\CompanyFirstEvent::checkEvent($company, $event);
            }

            return [
                'success' => true,
                'msg' => 'Файл успешно загружен',
            ];
        } else {
            $size = round(file\File::$maxFileSize / (1024*1024), 2);
            $error = current($file->firstErrors);

            return [
                'success' => false,
                'msg' => $error ? : 'Ошибка при загрузке файла.' .
                        " Разрешена загрузка файлов размером до {$size}МБ и только со следующими расширениями: " .
                        $file->fileExtensions,
            ];
        }
    }

    /**
     * @param $id
     * @return ActiveRecord
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     */
    private function findModel($id)
    {
        if ($id === null) {
            throw new BadRequestHttpException('`id` not passed into parameters.');
        }

        /* @var ActiveRecord $class */
        $class = $this->model;
        $query = $class::find();

        if ($this->strictByCompany) {
            if ($query instanceof ICompanyStrictQuery) {
                $query->byCompany(Yii::$app->user->identity->company->id);
            } else {
                $query->andWhere([
                    $class::tableName() . '.company_id' => Yii::$app->user->identity->company->id,
                ]);
            }
        }

        $query->andWhere([
            $class::tableName() . '.' . $this->idField => $id,
        ]);

        $model = $query->one();

        if ($model === null) {
            throw new NotFoundHttpException('Owner model not found.');
        }

        return $model;
    }
}
