<?php

namespace common\models\file;


use yii\db\ActiveRecord;

class FileHeaderHelper
{
    /**
     * @param ActiveRecord $model
     * @param $filePath
     */
    public static function contentHeaders(ActiveRecord $model, $filePath, $fileName)
    {
        $contentType = finfo_file(finfo_open(FILEINFO_MIME_TYPE), $filePath);

        header("Content-type: " . $contentType);
        header('Content-disposition: filename="' . $fileName . '"');
    }

    /**
     * Renders caching headers and ends app if content is cached.
     * @param $filePath
     */
    public static function cachingHeaders($filePath)
    {
        $timestamp = filemtime($filePath);

        $gmt_microTime = gmdate('r', $timestamp);

        header('ETag: "' . md5($timestamp . $filePath) . '"');
        header('Last-Modified: ' . $gmt_microTime . ' GMT');
        header('Cache-Control: public');

        if (isset($_SERVER['HTTP_IF_MODIFIED_SINCE'])
            || isset($_SERVER['HTTP_IF_NONE_MATCH'])
        ) {
            if ($_SERVER['HTTP_IF_MODIFIED_SINCE'] == $gmt_microTime
                || str_replace('"', '', stripslashes($_SERVER['HTTP_IF_NONE_MATCH'])) == md5($timestamp . $filePath)
            ) {
                header('HTTP/1.1 304 Not Modified');
                exit;
            }
        }
    }
}
