<?php
namespace common\models;

use common\components\date\DateHelper;
use frontend\components\StatisticPeriod;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $username
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $auth_key
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 * @property string $statistic_range_date_from
 * @property string $statistic_range_date_to
 * @property string $statistic_range_name
 */
class User extends ActiveRecord implements IdentityInterface
{
    const STATUS_DELETED = 0;
    const STATUS_ACTIVE = 10;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * @param $login
     * @return User
     */
    public static function findByLogin($login)
    {
        $field = strpos($login, '@') > -1 ? 'email' : 'username';

        return static::findOne([
            $field => $login,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
            [
                'class' => 'common\components\PageSizeBehavior',
            ],
        ];
    }

    /**
     * @param $date_from
     * @param $date_to
     * @return bool
     */
    public function setStatisticRangeDates($date_from, $date_to)
    {
        if ($date_from != null || $date_to != null) {

            $this->statistic_range_date_from = $date_from;
            $this->statistic_range_date_to = $date_to;
            $this->save(true, ['statistic_range_date_from', 'statistic_range_date_to']);


            return true;
        }

        return false;
    }


    /**
     * @param $name
     * @param $date_from
     * @param $date_to
     * @return bool
     */
    public function setStatisticRangeName($name, $date_from, $date_to)
    {
        if ($name != null) {
            $this->statistic_range_name = $name;
            if ($name == StatisticPeriod::CUSTOM_RANGE) {
                $date_from = DateHelper::format($date_from, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                $date_to = DateHelper::format($date_to, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                $this->statistic_range_name = $date_from . ' - ' . $date_to;
            }

            return $this->save(true, ['statistic_range_name']);
        }

        return false;
    }

    /**
     * @return boolean
     */
    public function getIsSetStatisticRange()
    {
        return $this->statistic_range_name && $this->statistic_range_date_from && $this->statistic_range_date_to;
    }

    /**
     * @return string
     */
    public function getStatisticRangeName()
    {
        return $this->statistic_range_name;
    }

    /**
     * @param null $range
     * @return bool|string
     */
    public function getStatisticRangeDates($range = null)
    {
        if ($range == 'from') {
            return $this->statistic_range_date_from;
        } elseif ($range == 'to') {
            return $this->statistic_range_date_to;
        } elseif ($this->statistic_range_date_from && $this->statistic_range_date_to) {
            return true;
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['status', 'default', 'value' => self::STATUS_ACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_DELETED]],
            [['statistic_range_date_from', 'statistic_range_date_to', 'statistic_range_name'], 'string', 'max' => 45]
        ];
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /**
     * Finds user by username
     *
     * @param string $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return boolean
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        $parts = explode('_', $token);
        $timestamp = (int) end($parts);

        return $timestamp + $expire >= time();
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }


    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
}
