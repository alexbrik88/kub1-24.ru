<?php

namespace common\models;

interface ListInterface
{
    /**
     * @return string[]
     */
    public function getItems(): array;
}
