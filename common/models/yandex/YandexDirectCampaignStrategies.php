<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 16.01.2020
 * Time: 0:08
 */

namespace common\models\yandex;

final class YandexDirectCampaignStrategies
{
    const AVERAGE_CPA = 'AVERAGE_CPA';
    const AVERAGE_CPC = 'AVERAGE_CPC';
    const AVERAGE_CPI = 'AVERAGE_CPI';
    const AVERAGE_ROI = 'AVERAGE_ROI';
    const CP_DECREASED_PRICE_FOR_REPEATED_IMPRESSIONS = 'CP_DECREASED_PRICE_FOR_REPEATED_IMPRESSIONS';
    const CP_MAXIMUM_IMPRESSIONS = 'CP_MAXIMUM_IMPRESSIONS';
    const HIGHEST_POSITION = 'HIGHEST_POSITION';
    const MANUAL_CPM = 'MANUAL_CPM';
    const MAXIMUM_COVERAGE = 'MAXIMUM_COVERAGE';
    const NETWORK_DEFAULT = 'NETWORK_DEFAULT';
    const WB_DECREASED_PRICE_FOR_REPEATED_IMPRESSIONS = 'WB_DECREASED_PRICE_FOR_REPEATED_IMPRESSIONS';
    const WB_MAXIMUM_APP_INSTALLS = 'WB_MAXIMUM_APP_INSTALLS';
    const WB_MAXIMUM_CLICKS = 'WB_MAXIMUM_CLICKS';
    const WB_MAXIMUM_CONVERSION_RATE = 'WB_MAXIMUM_CONVERSION_RATE';
    const WB_MAXIMUM_IMPRESSIONS = 'WB_MAXIMUM_IMPRESSIONS';
    const WEEKLY_CLICK_PACKAGE = 'WEEKLY_CLICK_PACKAGE';

    public static $namesMap = [
        self::AVERAGE_CPA => 'Оптимизация конверсий',
        self::AVERAGE_CPC => 'Оптимизация кликов',
        self::AVERAGE_CPI => 'Оптимизация количества установок приложения',
        self::AVERAGE_ROI => 'Оптимизация рентабельности',
        self::CP_DECREASED_PRICE_FOR_REPEATED_IMPRESSIONS => 'Снижение цены повторных показов',
        self::CP_MAXIMUM_IMPRESSIONS => 'Максимум показов по минимальной цене',
        self::HIGHEST_POSITION => 'Ручное управление ставками с оптимизацией',
        self::MANUAL_CPM => 'Ручное управление ставками',
        self::MAXIMUM_COVERAGE => 'Ручное управление ставками с оптимизацией',
        self::NETWORK_DEFAULT => 'Настройки показов в сетях в зависимости от настроек на поиске',
        self::WB_DECREASED_PRICE_FOR_REPEATED_IMPRESSIONS => 'Снижение цены повторных показов',
        self::WB_MAXIMUM_APP_INSTALLS => 'Оптимизация количества установок приложения',
        self::WB_MAXIMUM_CLICKS => 'Оптимизация кликов',
        self::WB_MAXIMUM_CONVERSION_RATE => 'Оптимизация конверсий',
        self::WB_MAXIMUM_IMPRESSIONS => 'Максимум показов по минимальной цене',
        self::WEEKLY_CLICK_PACKAGE => 'Оптимизация кликов',
    ];
}