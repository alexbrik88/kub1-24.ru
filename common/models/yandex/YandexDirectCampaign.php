<?php

namespace common\models\yandex;

use Yii;
use common\models\Company;

/**
 * This is the model class for table "yandex_direct_campaign".
 *
 * @property int $id
 * @property int $company_id
 * @property string $name
 * @property string $status
 * @property string $strategy
 * @property string $showing_place
 * @property double $daily_budget_amount
 *
 * @property YandexDirectAd[] $yandexDirectAds
 * @property YandexDirectAdGroup[] $yandexDirectAdGroups
 * @property YandexDirectAdGroupReport[] $yandexDirectAdGroupReports
 * @property YandexDirectAdReport[] $yandexDirectAdReports
 * @property Company $company
 * @property YandexDirectCampaignReport[] $yandexDirectCampaignReports
 */
class YandexDirectCampaign extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yandex_direct_campaign';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'company_id', 'name'], 'required'],
            [['id', 'company_id'], 'integer'],
            [['daily_budget_amount'], 'number'],
            [['name', 'status', 'strategy', 'showing_place'], 'string', 'max' => 255],
            [['id', 'company_id'], 'unique', 'targetAttribute' => ['id', 'company_id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'name' => 'Name',
            'status' => 'Status',
            'strategy' => 'Strategy',
            'showing_place' => 'Showing Place',
            'daily_budget_amount' => 'Daily Budget Amount',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getYandexDirectAds()
    {
        return $this->hasMany(YandexDirectAd::class, ['campaign_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getYandexDirectAdGroups()
    {
        return $this->hasMany(YandexDirectAdGroup::class, ['campaign_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getYandexDirectAdGroupReports()
    {
        return $this->hasMany(YandexDirectAdGroupReport::class, ['campaign_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getYandexDirectAdReports()
    {
        return $this->hasMany(YandexDirectAdReport::class, ['campaign_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getYandexDirectCampaignReports()
    {
        return $this->hasMany(YandexDirectCampaignReport::class, ['campaign_id' => 'id']);
    }
}
