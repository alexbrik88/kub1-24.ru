<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 17.01.2020
 * Time: 0:02
 */

namespace common\models\yandex;

final class YandexDirectAdGroupStatus
{
    const DRAFT = 'DRAFT';
    const MODERATION = 'MODERATION';
    const PREACCEPTED = 'PREACCEPTED';
    const ACCEPTED = 'ACCEPTED';
    const REJECTED = 'REJECTED';

    public static $namesMap = [
        self::DRAFT => 'Не отправлена на модерацию',
        self::MODERATION => 'На модерации',
        self::PREACCEPTED => 'Допущена к показам автоматически, но будет дополнительно проверена модератором',
        self::ACCEPTED => 'Активна',
        self::REJECTED => 'Отклонена',
    ];
}