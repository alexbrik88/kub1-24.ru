<?php

namespace common\models\yandex;

use common\models\Company;
use Yii;

/**
 * This is the model class for table "yandex_direct_ad".
 *
 * @property int $id
 * @property int $ad_group_id
 * @property int $campaign_id
 * @property int $company_id
 * @property string $name
 * @property string $status
 *
 * @property YandexDirectAdGroup $adGroup
 * @property YandexDirectCampaign $campaign
 * @property Company $company
 * @property YandexDirectAdReport[] $yandexDirectAdReports
 */
class YandexDirectAd extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yandex_direct_ad';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'ad_group_id', 'campaign_id', 'company_id'], 'required'],
            [['id', 'ad_group_id', 'campaign_id', 'company_id'], 'integer'],
            [['name', 'status'], 'string', 'max' => 255],
            [['id', 'ad_group_id', 'campaign_id', 'company_id'], 'unique', 'targetAttribute' => ['id', 'ad_group_id', 'campaign_id', 'company_id']],
            [['ad_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => YandexDirectAdGroup::class, 'targetAttribute' => ['ad_group_id' => 'id']],
            [['campaign_id'], 'exist', 'skipOnError' => true, 'targetClass' => YandexDirectCampaign::class, 'targetAttribute' => ['campaign_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ad_group_id' => 'Ad Group ID',
            'campaign_id' => 'Campaign ID',
            'company_id' => 'Company ID',
            'name' => 'Name',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdGroup()
    {
        return $this->hasOne(YandexDirectAdGroup::class, ['id' => 'ad_group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCampaign()
    {
        return $this->hasOne(YandexDirectCampaign::class, ['id' => 'campaign_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getYandexDirectAdReports()
    {
        return $this->hasMany(YandexDirectAdReport::class, ['ad_id' => 'id']);
    }
}
