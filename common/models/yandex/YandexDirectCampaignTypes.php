<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 16.01.2020
 * Time: 0:00
 */

namespace common\models\yandex;

final class YandexDirectCampaignTypes
{
    const TEXT_CAMPAIGN = 'TEXT_CAMPAIGN';
    const MOBILE_APP_CAMPAIGN = 'MOBILE_APP_CAMPAIGN';
    const DYNAMIC_TEXT_CAMPAIGN = 'DYNAMIC_TEXT_CAMPAIGN';
    const SMART_CAMPAIGN = 'SMART_CAMPAIGN';
    const MCBANNER_CAMPAIGN = 'MCBANNER_CAMPAIGN';
    const CPM_BANNER_CAMPAIGN = 'CPM_BANNER_CAMPAIGN';
    const CPM_DEALS_CAMPAIGN = 'CPM_DEALS_CAMPAIGN';
    const CPM_FRONTPAGE_CAMPAIGN = 'CPM_FRONTPAGE_CAMPAIGN';
    const SERVING_OFF = 'SERVING_OFF';
    const UNKNOWN = 'UNKNOWN';

    public static $typesToFieldNamesMap = [
        self::TEXT_CAMPAIGN => 'TextCampaign',
        self::MOBILE_APP_CAMPAIGN => 'MobileAppCampaign',
        self::DYNAMIC_TEXT_CAMPAIGN => 'DynamicTextCampaign',
        self::CPM_BANNER_CAMPAIGN => 'CpmBannerCampaign',
    ];
}