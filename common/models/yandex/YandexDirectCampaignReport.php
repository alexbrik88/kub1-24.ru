<?php

namespace common\models\yandex;

use common\components\helpers\ArrayHelper;

/**
 * This is the model class for table "yandex_direct_report".
 *
 * @property int $campaign_id
 * @property int $impressions_count
 * @property int $clicks_count
 * @property double $ctr
 * @property double $cost
 * @property double $avg_cpc
 * @property double $avg_page_views_count
 * @property double $conversion_rate
 * @property double $revenue
 * @property int $conversions_count
 * @property double $goals_roi
 * @property double $profit
 * @property double $cost_per_conversion
 *
 * @property YandexDirectCampaign $campaign
 */
class YandexDirectCampaignReport extends BaseYandexDirectReport
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yandex_direct_campaign_report';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['campaign_id', 'impressions_count', 'clicks_count', 'ctr', 'cost', 'profit', 'revenue'], 'required'],
            [['campaign_id', 'impressions_count', 'clicks_count', 'conversions_count'], 'integer'],
            [['ctr', 'cost', 'avg_cpc', 'avg_page_views_count', 'conversion_rate', 'goals_roi', 'profit', 'revenue',
                'cost_per_conversion'], 'number'],
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return ArrayHelper::merge(parent::attributeLabels(), [
            'campaign_id' => 'Campaign ID',
            'impressions_count' => 'Показы',
            'clicks_count' => 'Клики',
            'ctr' => 'CTR (%)',
            'cost' => 'Расход (руб.)',
            'avg_cpc' => 'Ср. цена клика (руб.)',
            'avg_page_views_count' => 'Глубина (стр.)',
            'conversion_rate' => 'Конверсия (%)',
            'revenue' => 'Доход (руб.)',
            'cost_per_conversion' => 'Цена цели (руб.)',
            'conversions_count' => 'Конверсии',
            'goals_roi' => 'Рентабельность',
            'profit' => 'Прибыль (руб.)',
        ]);
    }

    public function getYandexDirectGroupedObjectId()
    {
        return $this->campaign_id;
    }

    public function getCampaign()
    {
        return $this->hasOne(YandexDirectCampaign::class, ['id' => 'campaign_id', 'company_id' => 'company_id']);
    }
}
