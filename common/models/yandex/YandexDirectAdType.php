<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 17.01.2020
 * Time: 1:24
 */

namespace common\models\yandex;

final class YandexDirectAdType
{
    const TYPE_TEXT_AD = 'TEXT_AD';
    const TYPE_MOBILE_APP_AD = 'MOBILE_APP_AD';
    const TYPE_DYNAMIC_TEXT_AD = 'DYNAMIC_TEXT_AD';
    const TYPE_IMAGE_AD = 'IMAGE_AD';
    const TYPE_CPC_VIDEO_AD = 'CPC_VIDEO_AD';
    const TYPE_CPM_BANNER_AD = 'CPM_BANNER_AD';
    const TYPE_CPM_VIDEO_AD = 'CPM_VIDEO_AD';

    const SUBTYPE_TEXT_IMAGE_AD = 'TEXT_IMAGE_AD';
    const SUBTYPE_MOBILE_APP_IMAGE_AD = 'MOBILE_APP_IMAGE_AD';
    const SUBTYPE_TEXT_AD_BUILDER_AD = 'TEXT_AD_BUILDER_AD';
    const SUBTYPE_MOBILE_APP_AD_BUILDER_AD = 'MOBILE_APP_AD_BUILDER_AD';

    public static $typesToFieldNamesMap = [
        self::TYPE_TEXT_AD => 'TextAd',
        self::TYPE_MOBILE_APP_AD => 'MobileAppAd',
        self::TYPE_DYNAMIC_TEXT_AD => 'DynamicTextAd',
        self::TYPE_IMAGE_AD => [
            self::SUBTYPE_TEXT_IMAGE_AD => 'TextImageAd',
            self::SUBTYPE_MOBILE_APP_IMAGE_AD => 'MobileAppImageAd',
            self::SUBTYPE_TEXT_AD_BUILDER_AD => 'TextAdBuilderAd',
            self::SUBTYPE_MOBILE_APP_AD_BUILDER_AD => 'MobileAppAdBuilderAd',
        ],
        self::TYPE_CPC_VIDEO_AD => 'CpcVideoAdBuilderAd',
        self::TYPE_CPM_BANNER_AD => 'CpmBannerAdBuilderAd',
        self::TYPE_CPM_VIDEO_AD => 'CpmVideoAdBuilderAd',
    ];
}