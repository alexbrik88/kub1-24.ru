<?php

namespace common\models\yandex;

use common\models\Company;
use Yii;

/**
 * This is the model class for table "yandex_direct_ad_group".
 *
 * @property int $id
 * @property int $campaign_id
 * @property int $company_id
 * @property string $name
 * @property string $status
 *
 * @property YandexDirectAd[] $yandexDirectAds
 * @property YandexDirectCampaign $campaign
 * @property Company $company
 * @property YandexDirectAdGroupReport[] $yandexDirectAdGroupReports
 * @property YandexDirectAdReport[] $yandexDirectAdReports
 */
class YandexDirectAdGroup extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yandex_direct_ad_group';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'campaign_id', 'company_id', 'name'], 'required'],
            [['id', 'campaign_id', 'company_id'], 'integer'],
            [['name', 'status'], 'string', 'max' => 255],
            [['id', 'campaign_id', 'company_id'], 'unique', 'targetAttribute' => ['id', 'campaign_id', 'company_id']],
            [['campaign_id'], 'exist', 'skipOnError' => true, 'targetClass' => YandexDirectCampaign::class, 'targetAttribute' => ['campaign_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'campaign_id' => 'Campaign ID',
            'company_id' => 'Company ID',
            'name' => 'Name',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getYandexDirectAds()
    {
        return $this->hasMany(YandexDirectAd::class, ['ad_group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCampaign()
    {
        return $this->hasOne(YandexDirectCampaign::class, ['id' => 'campaign_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getYandexDirectAdGroupReports()
    {
        return $this->hasMany(YandexDirectAdGroupReport::class, ['ad_group_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getYandexDirectAdReports()
    {
        return $this->hasMany(YandexDirectAdReport::class, ['ad_group_id' => 'id']);
    }
}
