<?php

namespace common\models\yandex;

use Yii;

/**
 * This is the model class for table "yandex_direct_ad_report".
 *
 * @property int $ad_id
 * @property int $ad_group_id
 * @property int $campaign_id
 * @property int $impressions_count
 * @property int $clicks_count
 * @property double $ctr
 * @property double $cost
 * @property double $avg_cpc
 * @property double $avg_page_views_count
 * @property double $conversion_rate
 * @property double $bounce_rate
 * @property double $revenue
 * @property int $conversions_count
 * @property double $goals_roi
 * @property double $profit
 * @property double $cost_per_conversion
 *
 * @property YandexDirectCampaign $campaign
 * @property YandexDirectAdGroup $adGroup
 * @property YandexDirectAd $ad
 */
class YandexDirectAdReport extends BaseYandexDirectReport
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'yandex_direct_ad_report';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ad_id', 'ad_group_id', 'campaign_id', 'impressions_count', 'clicks_count', 'ctr', 'cost', 'revenue', 'profit'], 'required'],
            [['ad_id', 'ad_group_id', 'campaign_id', 'impressions_count', 'clicks_count', 'conversions_count'], 'integer'],
            [['ctr', 'cost', 'avg_cpc', 'avg_page_views_count', 'conversion_rate', 'bounce_rate', 'revenue',
                'goals_roi', 'profit', 'cost_per_conversion'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ad_id' => 'Ad ID',
            'ad_group_id' => 'Ad Group ID',
            'campaign_id' => 'Campaign ID',
            'impressions_count' => 'Impressions Count',
            'clicks_count' => 'Clicks Count',
            'ctr' => 'Ctr',
            'cost' => 'Cost',
            'avg_cpc' => 'Avg Cpc',
            'avg_page_views_count' => 'Avg Page Views Count',
            'conversion_rate' => 'Conversion Rate',
            'bounce_rate' => 'Bounce Rate',
            'revenue' => 'Revenue',
            'conversions_count' => 'Conversions Count',
            'goals_roi' => 'Goals Roi',
            'profit' => 'Profit',
            'cost_per_conversion' => 'Cost Per Conversion',
        ];
    }

    public function getYandexDirectGroupedObjectId()
    {
        return $this->ad_id;
    }

    public function getCampaign()
    {
        return $this->hasOne(YandexDirectCampaign::class, ['id' => 'campaign_id', 'company_id' => 'company_id']);
    }

    public function getAdGroup()
    {
        return $this->hasOne(YandexDirectAdGroup::class, [
            'id' => 'ad_group_id',
            'campaign_id' => 'campaign_id',
            'company_id' => 'company_id',
        ]);
    }

    public function getAd()
    {
        return $this->hasOne(YandexDirectAd::class, [
            'id' => 'ad_id',
            'ad_group_id' => 'ad_group_id',
            'campaign_id' => 'campaign_id',
            'company_id' => 'company_id',
        ]);
    }
}
