<?php
declare(strict_types=1);

namespace common\models;

use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * @property int $id
 * @property int $company_id
 * @property int $duration
 * @property int $created_at
 *
 * @property Company $company
 */
final class UrotdelVisit extends ActiveRecord {

    public static function tableName(): string {
        return 'urotdel_visits';
    }

    public function behaviors(): array {
        return [
            [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => false,
            ],
        ];
    }

    public function getCompany(): ActiveQuery {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

}
