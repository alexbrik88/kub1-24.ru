<?php

namespace common\models;

use common\components\validators\SnilsValidator;
use common\models\Contractor;
use common\models\cash\Emoney;
use common\models\company\CompanyType;
use common\models\company\MenuItem;
use common\models\company\RegistrationPageType;
use common\modules\acquiring\models\Acquiring;
use common\modules\cards\models\CardAccount;
use Yii;
use common\components\date\DatePickerFormatBehavior;
use common\components\helpers\ArrayHelper;
use common\components\validators\PhoneValidator;
use common\models\document\DocumentType;
use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;
use common\models\employee\Employee;
use common\models\employee\EmployeeInfoRole;
use common\models\employee\EmployeeRole;
use common\models\employee\EmployeeSignature;
use yii\behaviors\TimestampBehavior;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "employee_company".
 *
 * @property integer $id
 * @property integer $employee_id
 * @property integer $company_id
 * @property integer $employee_role_id
 * @property integer $can_invoice_add_flow
 * @property integer $can_view_price_for_buy
 * @property boolean $has_no_patronymic
 * @property string $inn
 * @property string $snils
 * @property integer $in_szvm
 *
 * @property integer $passport_isRf
 * @property string $passport_country
 * @property string $passport_series
 * @property string $passport_issued_by
 * @property string $passport_department
 * @property string $passport_address
 * @property string $passport_number
 * @property string $passport_date_output
 * @property string $last_rs
 * @property integer $last_emoney_id
 * @property integer $info_role_id
 * @property integer $last_acquiring_id
 * @property int $last_card_account_id
 * @property integer $can_business_analytics
 * @property integer $can_business_analytics_time
 * @property integer $can_ba_all
 * @property integer $can_ba_finance
 * @property integer $can_ba_marketing
 * @property integer $can_ba_product
 * @property integer $can_ba_sales
 * @property integer $news_viewed_at
 * @property boolean $analytics_multi_company
 * @property boolean $document_access_own_only
 * @property bool $crm_deal_type_id
 *
 * @property Company $company
 * @property Employee $employee
 * @property EmployeeRole $employeeRole
 * @property EmployeeSignature $employeeSignature
 * @property InfoRole $infoRole
 */
class EmployeeCompany extends \yii\db\ActiveRecord
{
    const SCENARIO_CREATE_PROXY = 'proxy';

    const SCENARIO_CREATE_SALE_POINT = 'sale-point';

    const IMAGE_SIGNATURE_NAME = 'signature';

    protected $_email;
    protected $_newPassword;
    public $send_email = false;
    public $deleteSignature_file = false;

    public $apply_signature = false;

    public $new_responsible_employee_id;

    /**
     * Роли, для которых всегда есть возможность добавления оплаты по счетам
     * @var array
     */
    public static $addFlowRoles = [
        EmployeeRole::ROLE_CHIEF,
        EmployeeRole::ROLE_ACCOUNTANT,
    ];

    /**
     * Роли, для которых настраивается возможность добавления оплаты по счетам
     * @var array
     */
    public static $addFlowRolesNeedConfig = [
        EmployeeRole::ROLE_SUPERVISOR,
        EmployeeRole::ROLE_SUPERVISOR_VIEWER,
        EmployeeRole::ROLE_SALES_SUPERVISOR,
        EmployeeRole::ROLE_SALES_SUPERVISOR_VIEWER,
        EmployeeRole::ROLE_MANAGER,
        EmployeeRole::ROLE_SALES_MANAGER,
        EmployeeRole::ROLE_ASSISTANT,
    ];

    /**
     * Роли, для которых настраивается возможность добавления оплаты по счетам
     * @var array
     */
    public static $addFlowRolesDefaultOff = [
        EmployeeRole::ROLE_MANAGER,
        EmployeeRole::ROLE_SALES_MANAGER,
        EmployeeRole::ROLE_ASSISTANT,
    ];

    /**
     * Роли, для которых по умолчанию доступ только к своим документам
     * @var array
     */
    public static $docAccessOwnOnly = [
        EmployeeRole::ROLE_MANAGER,
        EmployeeRole::ROLE_SALES_MANAGER,
    ];

    /**
     * Роли, для которых доступ к чужим документам всегда открыт
     * @var array
     */
    public static $docAccessAll = [
        EmployeeRole::ROLE_CHIEF,
        EmployeeRole::ROLE_ACCOUNTANT,
        EmployeeRole::ROLE_FOUNDER,
        EmployeeRole::ROLE_FINANCE_DIRECTOR,
        EmployeeRole::ROLE_FINANCE_ADVISER,
    ];

    /**
     * @var array
     */
    public static $imageAttrArray = [
        'signature_file',
    ];

    /**
     * @var array
     */
    public static $imageDataArray = [
        'signature_file' => [
            'file_name' => self::IMAGE_SIGNATURE_NAME,
            'width' => 165,
            'height' => 50,
            'label' => 'Подпись',
            'crop_text' => 'Выбранная область с подписью будет выводиться в счете и акте',
            'preview_text' => 'Так будет выглядеть подпись на ваших документах',
            'dummy_text' => 'Рекомендуемый размер для загрузки: 165х50',
        ],
    ];

    private static $_items = [];

    public static function item($employee_id, $company_id)
    {
        if ($employee_id && $employee_id) {
            $key = $employee_id.'_'.$company_id;
            if (!array_key_exists($key, self::$_items)) {
                self::$_items[$key] = self::findOne([
                    'employee_id' => $employee_id,
                    'company_id' => $company_id,
                ]);
            }

            return self::$_items[$key];
        }

        return null;
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            [
                'class' => DatePickerFormatBehavior::className(),
                'attributes' => [
                    'birthday' => [
                        'message' => 'Дата рождения указана неверно.',
                    ],
                    'date_hiring' => [
                        'message' => 'Дата принятия на работу указана неверно.',
                    ],
                    'date_dismissal' => [
                        'message' => 'Дата увольнения указана неверно.',
                    ],
                    'sign_document_date' => [
                        'message' => 'Дата документа указана неверно.',
                    ],
                    'passport_date_output' => [
                        'message' => 'Дата выдачи паспорта указана неверно.',
                    ],
                ],
                'dateFormatParams' => [
                    'whenClient' => 'function(){}',
                ],
            ],
            [
                'class' => TimestampBehavior::className(),
            ]
        ]);
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'employee_company';
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['email'], 'trim', 'on' => 'create'],
            [['email'], 'required', 'on' => 'create', 'message' => 'Необходимо заполнить',],
            [['email'], 'email', 'on' => 'create'],
            [['email'], 'existEmployeeValidator', 'on' => 'create'],
            [
                [
                    'lastname',
                    'firstname',
                    //'patronymic',
                    'position',
                    'employee_role_id',
                    'time_zone_id',
                    'date_hiring',
                    'phone',
                    'sex'
                ],
                'required',
                'message' => 'Необходимо заполнить',
            ],
            [
                [
                    'has_no_patronymic',
                    'can_business_analytics',
                    'document_access_own_only',
                ], 'boolean'
            ],
            [
                ['patronymic'],
                'required',
                'when' => function ($model) {
                    return (!$model->has_no_patronymic);
                },
                'whenClient' => 'function () {
                    return !$("#employeecompany-has_no_patronymic").is(":checked");
                }',
                'message' => 'Необходимо заполнить',
            ],
            [['sex', 'sign_document_type_id', 'in_szvm', 'last_emoney_id', 'last_acquiring_id', 'news_viewed_at'], 'integer'],
            [['sex'], 'in', 'range' => [Employee::MALE, Employee::FEMALE]],
            [['position', 'snils', 'inn'], 'string', 'max' => 255],
            [
                ['employee_role_id'], 'exist',
                'skipOnError' => true,
                'targetClass' => EmployeeRole::className(),
                'targetAttribute' => ['employee_role_id' => 'id'],
            ],
            [['lastname', 'firstname', 'patronymic'], 'string', 'max' => 45],
            [['phone'], PhoneValidator::className()],
            [['snils'], SnilsValidator::className()],
            [
                [
                    'can_sign',
                    'is_product_admin',
                    'can_view_price_for_buy',
                    'can_product_write_off',
                    'deleteSignature_file',
                ],
                'boolean',
            ],
            [
                ['can_invoice_add_flow'], 'boolean',
                'when' => function ($model) {
                    return in_array($model->employee_role_id, self::$addFlowRolesNeedConfig);
                },
            ],
            [
                ['sign_document_type_id', 'sign_document_number', 'sign_document_date'], 'required',
                'when' => function ($model) {
                    return (boolean)$model->can_sign;
                },
                'whenClient' => 'function (attribute, value) {return $("#employeecompany-can_sign").is(":checked");}',
                'message' => 'Необходимо заполнить',
            ],
            [
                ['sign_document_type_id'], 'exist',
                'skipOnError' => true,
                'targetClass' => DocumentType::className(),
                'targetAttribute' => ['sign_document_type_id' => 'id'],
                'when' => function ($model) {
                    return (boolean)$model->can_sign;
                },
                'whenClient' => 'function (attribute, value) {return $("#employeecompany-can_sign").is(":checked");}',
            ],

            [['email'], 'required', 'on' => self::SCENARIO_CREATE_PROXY],
            [['email'], 'email', 'on' => self::SCENARIO_CREATE_PROXY],
            [['email'], 'existEmployeeValidator', 'on' => self::SCENARIO_CREATE_PROXY],

            // Passport
            [
                ['passport_isRf'], 'filter', 'filter' => function ($value) {
                    return is_numeric($value) ? $value * 1 : $value;
                }
            ],
            ['passport_isRf', 'integer',
                'when' => function (EmployeeCompany $model) {
                    if ($this->passport_isRf == 1) {
                        $this->passport_country = '';
                    }
                    return true;
                },
            ],
            ['passport_country', 'string', 'max' => 25],
            ['passport_series', 'string', 'max' => 25,
                'when' => function (EmployeeCompany $model, $attr) {
                    if ($model->passport_isRf == 1) {
                        $pattern = preg_match('/^([0-9]{2}) ([0-9]{2})/', $model->$attr);
                    } else {
                        $pattern = preg_match('/^[a-zа-яё\d\s]/i', $model->$attr);
                    }

                    $attributeLabels = $model->attributeLabels();
                    $message = isset($attributeLabels[$attr]) ? '"' . $attributeLabels[$attr] . '"' : '';

                    if (!$pattern) {
                        $this->addError($attr, "Значение $message неверно.");
                    }

                    return true;
                },
            ],
            ['passport_issued_by', 'string', 'max' => 160],
            ['passport_department', 'string', 'max' => 255,
                'when' => function (EmployeeCompany $model, $attr) {
                    if ($model->passport_isRf == 1) {
                        $pattern = preg_match('/^([0-9]{3})-([0-9]{3})/', $model->$attr);
                    } else {
                        $pattern = preg_match('/^[a-zа-яё\d\s]/i', $model->$attr);
                    }

                    $attributeLabels = $model->attributeLabels();
                    $message = isset($attributeLabels[$attr]) ? '"' . $attributeLabels[$attr] . '"' : '';

                    if (!$pattern) {
                        $this->addError($attr, "Значение $message неверно.");
                    }

                    return true;
                }
            ],
            ['passport_address', 'string', 'max' => 255],
            ['passport_number', 'string', 'max' => 25,
                'when' => function (EmployeeCompany $model, $attr) {
                    if ($model->passport_isRf == 1) {
                        $pattern = preg_match('/^([0-9]{6})$/', $model->$attr);
                    } else {
                        $pattern = preg_match('/^[a-zа-яё\d\s]/i', $model->$attr);
                    }

                    $attributeLabels = $model->attributeLabels();
                    $message = isset($attributeLabels[$attr]) ? '"' . $attributeLabels[$attr] . '"' : '';

                    if (!$pattern) {
                        $this->addError($attr, "Значение $message неверно.");
                    }

                    return true;
                }
            ],

            //[['email'], 'required', 'on' => self::SCENARIO_CREATE_PROXY],
            [['email'], 'email', 'on' => self::SCENARIO_CREATE_PROXY],
            [['email'], 'existEmployeeValidator', 'on' => self::SCENARIO_CREATE_PROXY],
            [
                ['passport_isRf',
                'passport_issued_by',
                //'passport_department',
                //'passport_address',
                'passport_number',
                'passport_date_output'
                ],
                'required', 'on' => self::SCENARIO_CREATE_PROXY,
                'message' => 'Необходимо заполнить',
            ],
            ['passport_series', 'default', 'value' => function (EmployeeCompany $model, $attr) {
                return ($model->passport_isRf) ? '00 00' : null;
            }],
            [
                [
                    'passport_series',
                ],
                'required',
                'message' => 'Необходимо заполнить',
                'on' => self::SCENARIO_CREATE_PROXY,
                'when' => function (EmployeeCompany $model, $attr) {
                    return (bool) $model->passport_isRf;
                },
                'whenClient' => 'function (attribute, value) {return $("#employeecompany-passport_isrf input:checked").val() == "1";}',
            ],
            [['passport_country'], 'required', 'on' => self::SCENARIO_CREATE_PROXY,
                'when' => function (EmployeeCompany $model) {
                return ($this->passport_isRf != 1);
            },
                'message' => 'Необходимо заполнить',
                ],
            [['last_rs'], 'safe'],
            [['lastname', 'positon'], 'required', 'on' => self::SCENARIO_CREATE_SALE_POINT],
            [['last_emoney_id'], 'exist', 'skipOnError' => true, 'targetClass' => Emoney::class, 'targetAttribute' => ['last_emoney_id' => 'id']],
            [['last_acquiring_id'], 'exist', 'skipOnError' => true, 'targetClass' => Acquiring::class, 'targetAttribute' => ['last_acquiring_id' => 'id']],
            [['can_ba_all', 'can_ba_finance', 'can_ba_marketing', 'can_ba_product', 'can_ba_sales',], 'boolean'],
            [['can_business_analytics'],'selectedBusinessAnalyticsSections'],
            [['last_card_account_id'], 'integer'],
            [
                ['last_card_account_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => CardAccount::class,
                'targetAttribute' => ['last_card_account_id' => 'id', 'company_id' => 'company_id'],
            ],
            [
                ['new_responsible_employee_id'],
                'exist',
                'skipOnError' => true,
                'targetClass' => EmployeeCompany::class,
                'targetAttribute' => 'employee_id',
                'filter' => function ($query) {
                    $query->andWhere([
                        'company_id' => $this->company_id,
                        'is_working' => true,
                    ])->andWhere([
                        'not',
                        ['employee_id' => $this->employee_id],
                    ]);
                }
            ],
        ];
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_CREATE_PROXY] = [
            'email',
            'firstname',
            'lastname',
            'patronymic',
            'has_no_patronymic',
            'passport_isRf',
            'passport_country',
            'passport_series',
            'passport_issued_by',
            'passport_department',
            'passport_address',
            'passport_number',
            'passport_date_output'
        ];
        $scenarios[self::SCENARIO_CREATE_SALE_POINT] = [
            'email',
            'firstname',
            'lastname',
            'patronymic',
            'has_no_patronymic',
            'position'
        ];
        return $scenarios;
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'email' => 'Email',
            'name' => 'ФИО',
            'lastname' => 'Фамилия',
            'firstname' => 'Имя',
            'patronymic' => 'Отчество',
            'sex' => 'Пол',
            'birthday' => 'Дата рождения',
            'date_hiring' => 'Дата приёма на работу',
            'date_dismissal' => 'Дата увольнения',
            'position' => 'Должность',
            'employee_role_id' => 'Роль',
            'phone' => 'Телефон',
            'sign_document_type_id' => 'Наименование документа',
            'sign_document_number' => 'Номер документа',
            'sign_document_date' => 'Дата документа',
            'sign_file' => 'Подпись',
            'time_zone_id' => 'Часовой пояс',
            'can_invoice_add_flow' => 'Может добавлять оплату по счетам',
            'can_view_price_for_buy' => 'Видит закупочные цены',
            'passport_isRf' => 'Паспорт',
            'passport_country' => 'Страна',
            'passport_series' => 'Серия',
            'passport_issued_by' => 'Кем выдан',
            'passport_department' => 'Код подразделения',
            'passport_address' => 'Адрес регистрации',
            'passport_number' => 'Номер',
            'passport_date_output' => 'Дата выдачи',
            'has_no_patronymic' => 'Нет отчества',
            'inn' => 'ИНН сотрудника',
            'snils' => 'СНИЛС сотрудника',
            'in_szvm' => 'Добавить сотрудника в отчет',
            'can_business_analytics' => 'Доступ к Бизнес Аналитике',
            'can_ba_all' => 'Доступ ко всем разделам в Бизнес Аналитике',
            'can_ba_finance' => 'Доступ к разделу Финансы в Бизнес Аналитике',
            'can_ba_marketing' => 'Доступ к разделу Маркетинг в Бизнес Аналитике',
            'can_ba_product' => 'Доступ к разделу Товары в Бизнес Аналитике',
            'can_ba_sales' => 'Доступ к разделу Продажи в Бизнес Аналитике',
            'document_access_own_only' => 'Видит ТОЛЬКО счета, которые ВЫСТАВИЛ САМ',
            'can_product_write_off' => 'Может формировать списания',
        ];
    }

    /**
     * @return bool
     */
    public function existEmployeeValidator($attribute, $params)
    {
        if ($this->company->getEmployees()->andWhere(['is_deleted' => false, 'email' => $this->$attribute])->exists()) {
            $this->addError($attribute, 'Сотрудник с таким значением "Email" уже существует.');
        }
    }

    /**
     * @return bool
     */
    public function selectedBusinessAnalyticsSections($attribute, $params)
    {
        if ($this->can_business_analytics) {
            if (!($this->can_ba_all || $this->can_ba_finance || $this->can_ba_marketing || $this->can_ba_product || $this->can_ba_sales)) {
                $this->addError($attribute, 'Необходимо выбрать к каким разделам Бизнес Аналитики будет доступ у Сотрудника');
            }
        }
    }

    /**
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                if (($employee = Employee::findOne(['email' => $this->email, 'is_deleted' => false])) === null) {
                    $employee = $this->getNewEmployee();
                    $this->_newPassword = Employee::generatePassword(Employee::PASS_LENGTH);
                    $employee->password = Yii::$app->security->generatePasswordHash($this->_newPassword);

                    if (!$employee->save(false)) {
                        return false;
                    }
                }
                $this->employee_id = $employee->id;
                $this->populateRelation('employee', $employee);
            } elseif ($this->isAttributeChanged('employee_id') || $this->isAttributeChanged('company_id')) {
                return false;
            }
            if (!$insert && $this->employee_id == $this->company->owner_employee_id) {
                if ($this->employee_role_id != EmployeeRole::ROLE_CHIEF) {
                    Yii::$app->session->setFlash('error', 'Владелец учетной записи компании может иметь роль только руководителя.');

                    return false;
                }
                if ($this->date_dismissal) {
                    Yii::$app->session->setFlash('error', 'Владелец учетной записи компании не может быть уволен.');

                    return false;
                }
            }
            $employee = $this->employee;
            if ($this->getOldAttribute('sex') !== (int)$this->sex && $this->sex !== null) {
                $employee->chat_photo = Employee::$chatPhotos[$this->sex][array_rand(Employee::$chatPhotos[$this->sex])];
                $employee->save(true, ['chat_photo']);
            } elseif ($this->sex == null) {
                $employee->chat_photo = Employee::NO_CHAT_PHOTO;
                $employee->save(true, ['chat_photo']);
            }

            if (!$insert && $this->employee_role_id == EmployeeRole::ROLE_CHIEF && $this->isAttributeChanged('date_dismissal')) {
                if ($this->date_dismissal) {
                    if (Yii::$app->id == 'app-frontend' && !Yii::$app->user->isGuest && $this->employee_id == Yii::$app->user->id) {
                        $this->date_dismissal = null;
                        Yii::$app->session->setFlash('error', 'Нельзя уволить самого себя.');

                        return false;
                    }
                    $chiefArray = $this->company->getEmployeeChief(true);
                    if (count($chiefArray) == 1 && $chiefArray[0]->id == $this->employee_id) {
                        $this->date_dismissal = null;
                        Yii::$app->session->setFlash('error', 'Нельзя уволить единственного руководителя.');

                        return false;
                    }
                }
            }
            $this->is_working = empty($this->date_dismissal) ? 1 : 0;
            //if (!$this->is_working) {
            //    /* @var $responsibleContractors Contractor[] */
            //    $responsibleContractors = $this->company->getContractors()
            //        ->andWhere(['responsible_employee_id' => $this->employee_id])
            //        ->all();
            //    if ($responsibleContractors) {
            //        foreach ($responsibleContractors as $responsibleContractor) {
            //            $chief = $this->company->getEmployeeChief();
            //            $responsibleContractor->responsible_employee_id = $chief->id;
            //            $responsibleContractor->save(true, ['responsible_employee_id']);
            //        }
            //    }
            //}

            $this->firstname_initial = mb_strtoupper(mb_substr($this->firstname, 0, 1));
            $this->patronymic_initial = mb_strtoupper(mb_substr($this->patronymic, 0, 1));

            if (($id = Yii::$app->params['service']['demo_employee_id'] ?? null) !== null) {
                if ($this->employee_id == $id) {
                    $this->employee_role_id = EmployeeRole::ROLE_DEMO;
                }
            }

            if (in_array($this->employee_role_id, self::$addFlowRoles)) {
                $this->can_invoice_add_flow = true;
            } elseif (!in_array($this->employee_role_id, self::$addFlowRolesNeedConfig)) {
                $this->can_invoice_add_flow = false;
            }

            if ($insert || $this->isAttributeChanged('can_business_analytics', false)) {
                $this->can_business_analytics_time = $this->can_business_analytics ? time() : null;
            }

            if (!isset($this->document_access_own_only)) {
                $this->document_access_own_only = in_array($this->employee_role_id, self::$docAccessOwnOnly);
            } elseif (in_array($this->employee_role_id, self::$docAccessAll)) {
                $this->document_access_own_only = false;
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     * @throws \yii\base\Exception
     * @throws \yii\db\StaleObjectException
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($this->can_sign) {
            foreach (self::$imageAttrArray as $attribute) {
                $this->moveFromTmp($attribute);
            }
            foreach (self::$imageAttrArray as $attribute) {
                if ($this->{'delete' . ucfirst($attribute)} && $this->employeeSignature) {
                    //$this->employeeSignature->deleteImage();
                    $this->employeeSignature->updateAttributes(['status_id' => null]);
                    $this->updateAttributes(['signature_id' => null]);
                }
            }
        }

        if ($insert) {
            \common\models\company\CompanyFirstEvent::checkEvent($this->company, 46);
            $menuItem = new MenuItem();
            $menuItem->company_id = $this->company_id;
            $menuItem->employee_id = $this->employee_id;
            switch ($this->company->registration_page_type_id) {
                case RegistrationPageType::PAGE_TYPE_BANK_ROBOT:
                case RegistrationPageType::PAGE_TYPE_DECLARATION_TEMPLATE_IP:
                case RegistrationPageType::PAGE_TYPE_NULL_DECLARATION_TEMPLATE_IP:
                case RegistrationPageType::PAGE_TYPE_NULL_LANDING:
                    if ($this->company->company_type_id == CompanyType::TYPE_IP && $this->company->companyTaxationType->usn) {
                        $menuItem->accountant_item = true;
                    }
                    break;
                case RegistrationPageType::PAGE_TYPE_B2B_MODULE:
                    $menuItem->b2b_item = true;
                    break;
                default:
                    $menuItem->invoice_item = true;
                    if ($this->company->company_type_id == CompanyType::TYPE_IP && $this->company->companyTaxationType->usn) {
                        $menuItem->accountant_item = true;
                    }
                    break;
            }
            $menuItem->project_item = true;

            if (!MenuItem::find()->where(['company_id' => $this->company_id, 'employee_id' => $this->employee_id])->exists()) {
                $menuItem->save();
            }
        } else {
            if (array_key_exists('is_working', $changedAttributes) &&
                $changedAttributes['is_working'] &&
                !$this->is_working &&
                $this->new_responsible_employee_id
            ) {
                Invoice::updateAll([
                    'responsible_employee_id' => $this->new_responsible_employee_id
                ], [
                    'is_deleted' => 0,
                    'company_id' => $this->company_id,
                    'responsible_employee_id' => $this->employee_id,
                    'invoice_status_id' => InvoiceStatus::$payAllowed,
                ]);
                Contractor::updateAll([
                    'responsible_employee_id' => $this->new_responsible_employee_id
                ], [
                    'company_id' => $this->company_id,
                    'responsible_employee_id' => $this->employee_id,
                ]);
            }
        }

        if ($insert && $this->send_email) {
            if ($this->_newPassword) {
                $this->employee->sendNewPasswordEmail($this->_newPassword);
            } else {
                $this->employee->sendNewCompanyEmail($this->company);
            }
        }
    }

    /**
     * @return Employee
     */
    protected function getNewEmployee()
    {
        return new Employee([
            'main_company_id' => $this->company->main_id ? $this->company->main_id : $this->company->id,
            'company_id' => $this->company->id,
            'email' => $this->email,
            'lastname' => $this->lastname,
            'firstname' => $this->firstname,
            'patronymic' => $this->patronymic,
            'has_no_patronymic' => $this->has_no_patronymic,
            'firstname_initial' => mb_strtoupper(mb_substr($this->firstname, 0, 1)),
            'patronymic_initial' => mb_strtoupper(mb_substr($this->patronymic, 0, 1)),
            'time_zone_id' => $this->time_zone_id,
            'sex' => $this->sex,
            'birthday' => $this->birthday,
            'phone' => $this->phone,
            'employee_role_id' => $this->employee_role_id,
            'date_hiring' => $this->date_hiring,
            'position' => $this->position,
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCrmKubResponsible()
    {
        return $this->hasOne(CrmKubResponsible::className(), ['employee_id' => 'employee_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployeeRole()
    {
        return $this->hasOne(EmployeeRole::className(), ['id' => 'employee_role_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSignBasisDocument()
    {
        return $this->hasOne(DocumentType::className(), ['id' => 'sign_document_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployeeSignature()
    {
        return $this->hasOne(EmployeeSignature::className(), ['id' => 'signature_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInfoRole()
    {
        return $this->hasOne(EmployeeInfoRole::className(), ['id' => 'info_role_id']);
    }

    /**
     * @return bool|string
     */
    public function getChatPhotoSrc()
    {
        return '/img/chat/' . $this->employee->chat_photo;
    }

    /**
     * @param $friendID
     * @return ChatVolume|null
     */
    public function getChatVolume($friendID)
    {
        return ChatVolume::find()->andWhere(['and',
            ['company_id' => $this->company_id],
            ['from_employee_id' => $this->employee_id],
            ['to_employee_id' => $friendID],
        ])->one();
    }

    /**
     * @return string
     */
    public function setEmail($email)
    {
        return $this->_email = $email;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        if ($this->_email === null && $this->employee !== null) {
            $this->_email = $this->employee->email;
        }

        return $this->_email;
    }

    /**
     * Returns FIO
     * @param bool|false $short
     * @return string
     */
    public function getFio($short = false)
    {
        return join(' ', array_filter([
            $this->lastname,
            $short ? ($this->firstname_initial ? $this->firstname_initial . '.' : '') : $this->firstname,
            $short ? ($this->patronymic_initial ? $this->patronymic_initial . '.' : '') : $this->patronymic,
        ]));
    }

    /**
     * @return string
     */
    public function getShortFio()
    {
        return join(' ', array_filter([
            $this->lastname,
            $this->firstname_initial ? $this->firstname_initial . '.' : '',
            $this->patronymic_initial ? $this->patronymic_initial . '.' : '',
        ]));
    }

    /**
     * @return integer
     */
    public function getHasSalary()
    {
        return $this->has_salary_1 || $this->has_bonus_1 || $this->has_salary_2 || $this->has_bonus_2;
    }

    /**
     * @return integer
     */
    public function getTotalAmount()
    {
        return ($this->has_salary_1 ? $this->salary_1_amount : 0) +
               ($this->has_bonus_1 ? $this->bonus_1_amount : 0) +
               ($this->has_salary_2 ? $this->salary_2_amount : 0) +
               ($this->has_bonus_2 ? $this->bonus_2_amount : 0);
    }

    public function setEmployee(Employee $employee)
    {
        $this->employee_id = $employee->id;
        $this->lastname = $employee->lastname;
        $this->firstname = $employee->firstname;
        $this->patronymic = $employee->patronymic;
        $this->time_zone_id = $employee->time_zone_id;
        $this->sex = $employee->sex;
        $this->birthday = $employee->birthday;
        $this->date_hiring = $employee->date_hiring;
        $this->date_dismissal = $employee->date_dismissal;
        $this->position = $employee->position;
        $this->employee_role_id = $employee->employee_role_id;
        $this->phone = $employee->phone;
    }

    /**
     * @param string $attribute
     * @return string|null
     */
    public function getImageName($attribute)
    {
        if (isset(self::$imageDataArray[$attribute])) {

            $nextNumber = EmployeeSignature::find()->andWhere([
                    'company_id' => $this->company_id,
                    'employee_id' => $this->employee_id
                ])->count() + 1;

            return self::$imageDataArray[$attribute]['file_name'] . $nextNumber;
        }

        return null;
    }

    /**
     * @return string
     */
    public function getTmpPath()
    {
        return Company::fileUploadPath($this->company_id) .
            DIRECTORY_SEPARATOR . 'signature' .
            DIRECTORY_SEPARATOR . 'tmp' . Yii::$app->user->identity->id;
    }

    /**
     * @param $attribute
     * @return mixed
     */
    public function getTmpImage($attribute)
    {
        $files = [];
        $basePath = $this->getTmpPath();
        if (file_exists($basePath)) {
            $fileName = $this->getImageName($attribute);
            $files = FileHelper::findFiles($basePath, [
                'only' => ["/{$fileName}.*"],
                'recursive' => false,
            ]);
        }

        return reset($files);
    }

    /**
     * @param $attribute
     * @param UploadedFile $file
     * @return bool
     * @throws \yii\base\Exception
     */
    public function saveImage($attribute, UploadedFile $file)
    {
        if (!$file->hasError && in_array($file->extension, ['jpeg', 'jpg', 'png'])) {
            $this->deleteImage($attribute);
            $basePath = $this->getTmpPath();
            $imageName = $this->getImageName($attribute);
            $fileName = $imageName . '.' . $file->extension;
            if (!file_exists($basePath)) {
                FileHelper::createDirectory($basePath);
            }
            $path = $basePath . DIRECTORY_SEPARATOR . $fileName;
            if (file_exists($path) && $fileName) {
                $pathInfo = pathinfo($path);
                $newFileName = DIRECTORY_SEPARATOR . $pathInfo['filename'] . '.png';
                $newPath = $basePath . $newFileName;

                exec("convert \"{$path}\" -fill none -fuzz 20% -draw \"matte 0,0 floodfill\" -flop  -draw \"matte 0,0 floodfill\" -flop \"{$newPath}\"");
                exec("convert \"{$newPath}\" -fuzz 4% -transparent white \"{$newPath}\"");
                if ($pathInfo['extension'] !== 'png' && file_exists($path)) {
                    unlink($path);
                }
            }

            return $file->saveAs($basePath . DIRECTORY_SEPARATOR . $fileName);
        }

        return false;
    }

    /**
     * @param $attribute
     * @throws \yii\base\Exception
     * @throws \yii\db\StaleObjectException
     */
    public function moveFromTmp($attribute)
    {
        $tmpPath = $this->getTmpPath();
        $imageName = $this->getImageName($attribute);
        $files = is_dir($tmpPath) ? FileHelper::findFiles($tmpPath, ['only' => ["/{$imageName}.*"], 'recursive' => false]) : [];
        if ($files) {
            $file = $files[0];
            $fileName = basename($file);

            if ($this->employeeSignature) {
                $this->employeeSignature->updateAttributes(['status_id' => null]);
            }

            $signature = new EmployeeSignature([
                'employee_id' => $this->employee_id,
                'company_id' => $this->company_id,
                'status_id' => EmployeeSignature::STATUS_ACTIVE
            ]);

            $signature->file_name = $fileName;
            $signature->created_at = time();
            if ($signature->save()) {
                if ((is_dir($signature->dir) || FileHelper::createDirectory($signature->dir))) {
                    //$signature->deleteImage();
                    $basePath = $signature->getUploadPath();
                    rename($file, $basePath . DIRECTORY_SEPARATOR . $fileName);
                    $this->updateAttributes(['signature_id' => $signature->id]);

                    if ($this->apply_signature)
                        $signature->applyForAll();

                    $this->deleteImage($attribute);
                } else {
                    $signature->delete();
                }
            }
        }
    }

    /**
     * @param $attribute
     */
    public function deleteImage($attribute)
    {
        if ($this->employeeSignature) {
            $basePath = $this->getTmpPath();
            $fileName = $this->getImageName($attribute);
            if (is_dir($basePath)) {
                $files = FileHelper::findFiles($basePath, ['only' => ["/{$fileName}.*"], 'recursive' => false]);
                foreach ($files as $file) {
                    if (is_file($file)) {
                        unlink($file);
                    }
                }
            }
        }
    }

    /**
     *
     */
    public function getCanWriteOff() : bool
    {
        $role = EmployeeRole::$roleArray[$this->employee_role_id] ?? null;

        if ($role) {
            if (in_array($role, \frontend\rbac\permissions\Product::$writeOffRoles)) {
                return true;
            } elseif (in_array($role, \frontend\rbac\permissions\Product::$writeOffLimitedRoles)) {
                return (bool) $this->can_product_write_off;
            }
        }

        return false;
    }
}
