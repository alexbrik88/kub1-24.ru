<?php

namespace common\models\reference;

use common\models\Company;
use common\models\employee\Employee;
use Yii;

/**
 * This is the model class for table "pixel".
 *
 * @property string $id
 * @property string $email
 * @property integer $company_id
 * @property integer $created_at
 *
 * @property Company $company
 */
class Pixel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pixel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['company_id'], 'exist', 'skipOnError' => true,
                'targetClass' => Company::className(),
                'targetAttribute' => ['company_id' => 'id'],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'email' => 'Email',
            'company_id' => 'От компании',
            'created_at' => 'Дата',
            'isClient' => 'Стал клиентом',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getClient()
    {
        return $this->hasOne(Employee::className(), ['email' => 'email']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIsClient()
    {
        return $this->getClient()->alias('e')->andOnCondition(['e.is_deleted' => false])->exists();
    }

    /**
     * @param boolean
     * @return boolean
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->created_at = time();
                do {
                    $this->id = $this->el = bin2hex(openssl_random_pseudo_bytes(18));
                } while (self::find()->where(['id' => $this->id])->exists());
            }

            return true;
        } else {
            return false;
        }
    }
}
