<?php

namespace common\models\reference;

use Yii;

/**
 * This is the model class for table "code_okved_section".
 *
 * @property integer $id
 * @property string $name
 * @property string $description
 *
 * @property OkvedCode[] $okvedCodes
 */
class CodeOkvedSection extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'code_okved_section';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description'], 'string'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'description' => 'Описание',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOkvedCodes()
    {
        return $this->hasMany(OkvedCode::className(), ['section_id' => 'id']);
    }
}
