<?php

namespace common\models\reference;

use Yii;

/**
 * This is the model class for table "okved_code".
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 * @property integer $category_id
 * @property integer $section_id
 * @property string $class
 * @property string $class_name
 * @property string $class_description
 *
 * @property OkvedCodeCategory $category
 * @property CodeOkvedSection $section
 */
class OkvedCode extends \yii\db\ActiveRecord
{
    /**
     * @var null
     */
    public $sectionDescription = null;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'okved_code';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['code', 'name', 'category_id'], 'required'],
            [['category_id', 'section_id'], 'integer'],
            [['code', 'name', 'class', 'class_name'], 'string', 'max' => 255],
            [['class_description'], 'string'],
            [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => OkvedCodeCategory::className(), 'targetAttribute' => ['category_id' => 'id']],
            [['section_id'], 'exist', 'skipOnError' => true, 'targetClass' => CodeOkvedSection::className(), 'targetAttribute' => ['section_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Код',
            'name' => 'Название',
            'category_id' => 'Категория',
            'section_id' => 'Раздел',
            'sectionDescription' => 'Описание раздела',
            'class' => 'Класс',
            'class_name' => 'Название класса',
            'class_description' => 'Описание класса',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(OkvedCodeCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSection()
    {
        return $this->hasOne(CodeOkvedSection::className(), ['id' => 'section_id']);
    }
}
