<?php

namespace common\models\reference;

use Yii;

/**
 * This is the model class for table "fixed_payments_ip".
 *
 * @property integer $year
 * @property integer $pfr
 * @property integer $oms
 * @property integer $limit
 */
class FixedPaymentsIp extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'fixed_payments_ip';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['year', 'form_pfr', 'form_oms', 'form_limit'], 'required'],
            [['year', 'form_pfr', 'form_oms', 'form_limit'], 'integer'],
            [['year'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'year' => 'Год',
            'pfr' => 'ПФР',
            'oms' => 'ОМС',
            'limit' => 'Лимит',
            'form_pfr' => 'ПФР',
            'form_oms' => 'ОМС',
            'form_limit' => 'Лимит = ПФР х 8',
        ];
    }

    /**
     * @inheritdoc
     */
    public function setForm_pfr($val)
    {
        $this->pfr = is_numeric($val) ? $val * 100 : ($val ? : null);
    }

    /**
     * @inheritdoc
     */
    public function getForm_pfr()
    {
        return is_int($this->pfr) ? $this->pfr / 100 : $this->pfr;
    }

    /**
     * @inheritdoc
     */
    public function setForm_oms($val)
    {
        $this->oms = is_numeric($val) ? $val * 100 : ($val ? : null);
    }

    /**
     * @inheritdoc
     */
    public function getForm_oms()
    {
        return is_int($this->oms) ? $this->oms / 100 : $this->oms;
    }

    /**
     * @inheritdoc
     */
    public function setForm_limit($val)
    {
        $this->limit = is_numeric($val) ? $val * 100 : ($val ? : null);
    }

    /**
     * @inheritdoc
     */
    public function getForm_limit()
    {
        return is_int($this->limit) ? $this->limit / 100 : $this->limit;
    }
}
