<?php

namespace common\models\paymentReminder;

use Yii;
use common\models\Company;
use yii\behaviors\TimestampBehavior;

/**
 * This is the model class for table "payment_reminder_message".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $company_id
 * @property integer $number
 * @property integer $category_id
 * @property string $event_header
 * @property string $event_body
 * @property integer $days
 * @property string $message_template_subject
 * @property string $message_template_body
 * @property integer $status
 * @property integer $send_for_chief
 * @property integer $send_for_contact
 * @property integer $send_for_all
 * @property string $time
 * @property string $for_email_list
 * @property string $debt_sum
 * @property integer $not_send_where_order_payment
 * @property integer $not_send_where_emoney_payment
 * @property integer $not_send_where_partial_payment
 *
 * @property PaymentReminderMessageCategory $category
 * @property Company $company
 */
class PaymentReminderMessage extends \yii\db\ActiveRecord
{
    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 0;
    const MESSAGE_1 = 1;
    const MESSAGE_2 = 2;
    const MESSAGE_3 = 3;
    const MESSAGE_4 = 4;
    const MESSAGE_5 = 5;
    const MESSAGE_6 = 6;
    const MESSAGE_7 = 7;
    const MESSAGE_8 = 8;
    const MESSAGE_9 = 9;
    const MESSAGE_10 = 10;

    public $activate = self::STATUS_INACTIVE;
    public $timeInput;
    public $emailArray;
    public $email1;
    public $email2;
    public $email3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_reminder_message';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['message_template_subject', 'message_template_body'], 'required'],
            [['days'], 'required',
                'when' => function (PaymentReminderMessage $model) {
                    return $model->status == self::STATUS_ACTIVE && in_array($model->number, [self::MESSAGE_1,
                        self::MESSAGE_2, self::MESSAGE_3, self::MESSAGE_4, self::MESSAGE_5, self::MESSAGE_7,
                        self::MESSAGE_8, self::MESSAGE_9]);
                },
                'whenClient' => 'function () {
                    var $messages = [1, 2, 3, 4, 5, 7, 8, 9];
                    var $block = $(".modal:visible .form-body");

                    return ($block.find("#paymentremindermessage-status").val() == 1 ||
                            $block.find("#paymentremindermessage-activate").val() == 1) &&
                            $messages.indexOf(+$block.find("#paymentremindermessage-number").val()) != -1;
                }',
                'message' => 'Необходимо заполнить.',
            ],
            [['timeInput'], 'required',
                'when' => function (PaymentReminderMessage $model) {
                    return $model->status == self::STATUS_ACTIVE && in_array($model->number, [self::MESSAGE_1,
                        self::MESSAGE_2, self::MESSAGE_3, self::MESSAGE_4, self::MESSAGE_5, self::MESSAGE_6,
                        self::MESSAGE_7, self::MESSAGE_8, self::MESSAGE_9]);
                },
                'whenClient' => 'function () {
                    var $messages = [1, 2, 3, 4, 5, 6, 7, 8, 9];
                    var $block = $(".modal:visible .form-body");

                    return ($block.find("#paymentremindermessage-status").val() == 1 ||
                            $block.find("#paymentremindermessage-activate").val() == 1) &&
                            $messages.indexOf(+$block.find("#paymentremindermessage-number").val()) != -1;
                }',
                'message' => 'Необходимо заполнить.',
            ],
            [['timeInput'], 'date', 'type' => 'time', 'format' => 'HH:mm'],
            [['send_for_chief'], 'required',
                'requiredValue' => 1,
                'when' => function (PaymentReminderMessage $model) {
                    return $model->status == self::STATUS_ACTIVE && !$model->send_for_all && !$model->send_for_contact &&
                    in_array($model->number, [self::MESSAGE_1, self::MESSAGE_2, self::MESSAGE_3, self::MESSAGE_4,
                        self::MESSAGE_6, self::MESSAGE_7, self::MESSAGE_8, self::MESSAGE_10]);
                },
                'whenClient' => 'function () {
                    var $messages = [1, 2, 3, 4, 6, 7, 8, 10];
                    var $block = $(".modal:visible .form-body");

                    return ($block.find("#paymentremindermessage-status").val() == 1 ||
                            $block.find("#paymentremindermessage-activate").val() == 1) &&
                            $messages.indexOf(+$block.find("#paymentremindermessage-number").val()) != -1 &&
                            !$block.find("#paymentremindermessage-send_for_all").is(":checked") &&
                            !$block.find("#paymentremindermessage-send_for_contact").is(":checked");
                }',
                'message' => '',
            ],
            [['send_for_contact'], 'required',
                'requiredValue' => 1,
                'when' => function (PaymentReminderMessage $model) {
                    return $model->status == self::STATUS_ACTIVE && !$model->send_for_chief && !$model->send_for_all &&
                    in_array($model->number, [self::MESSAGE_1, self::MESSAGE_2, self::MESSAGE_3, self::MESSAGE_4,
                        self::MESSAGE_6, self::MESSAGE_7, self::MESSAGE_8, self::MESSAGE_10]);
                },
                'whenClient' => 'function () {
                    var $messages = [1, 2, 3, 4, 6, 7, 8, 10];
                    var $block = $(".modal:visible .form-body");

                    return ($block.find("#paymentremindermessage-status").val() == 1 ||
                            $block.find("#paymentremindermessage-activate").val() == 1) &&
                            $messages.indexOf(+$block.find("#paymentremindermessage-number").val()) != -1 &&
                            !$block.find("#paymentremindermessage-send_for_chief").is(":checked") &&
                            !$block.find("#paymentremindermessage-send_for_all").is(":checked");
                }',
                'message' => '',
            ],
            [['send_for_all'], 'required',
                'requiredValue' => 1,
                'when' => function (PaymentReminderMessage $model) {
                    return $model->status == self::STATUS_ACTIVE && !$model->send_for_chief && !$model->send_for_contact &&
                    in_array($model->number, [self::MESSAGE_1, self::MESSAGE_2, self::MESSAGE_3, self::MESSAGE_4,
                        self::MESSAGE_6, self::MESSAGE_7, self::MESSAGE_8, self::MESSAGE_10]);
                },
                'whenClient' => 'function () {
                    var $messages = [1, 2, 3, 4, 6, 7, 8, 10];
                    var $block = $(".modal:visible .form-body");

                    return ($block.find("#paymentremindermessage-status").val() == 1 ||
                            $block.find("#paymentremindermessage-activate").val() == 1) &&
                            $messages.indexOf(+$block.find("#paymentremindermessage-number").val()) != -1 &&
                            !$block.find("#paymentremindermessage-send_for_chief").is(":checked") &&
                            !$block.find("#paymentremindermessage-send_for_contact").is(":checked");
                }',
                'message' => 'Нужно выбрать хотябы один вариант.',
            ],
            [
                ['email1', 'email2', 'email3'], 'email',
                'message' => 'Значение «{value}» не является правильным email адресом.',
                'when' => function (PaymentReminderMessage $model) {
                    return in_array($model->number, [self::MESSAGE_5, self::MESSAGE_9]);
                }
            ],
            [
                ['email1'], 'required',
                'when' => function (PaymentReminderMessage $model) {
                    return in_array($model->number, [self::MESSAGE_5, self::MESSAGE_9]) &&
                            !$model->email1 && !$model->email2 && !$model->email3;
                },
                'message' => 'Необходимо заполнить хотябы один Email.',
            ],
            [['days', 'activate'], 'integer'],
            [['status', 'send_for_chief', 'send_for_contact', 'send_for_all', 'not_send_where_order_payment',
                'not_send_where_emoney_payment', 'not_send_where_partial_payment'], 'boolean'],
            [['event_body', 'message_template_body', 'for_email_list'], 'string'],
            [['event_header', 'message_template_subject', 'debt_sum'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Create At',
            'updated_at' => 'Updated At',
            'company_id' => 'Company ID',
            'number' => 'Номер',
            'category_id' => 'Категория',
            'event_header' => 'Event Header',
            'event_body' => 'Event Body',
            'days' => 'Days',
            'message_template_subject' => 'Message Template Subject',
            'message_template_body' => 'Message Template Body',
            'status' => 'Status',
            'send_for_chief' => 'Send For Chief',
            'send_for_contact' => 'Send For Contact',
            'send_for_all' => 'Send For All',
            'time' => 'Time',
            'for_email_list' => 'For Email List',
            'debt_sum' => 'Debt Sum',
            'not_send_where_order_payment' => 'Not Send Where Order Payment',
            'not_send_where_emoney_payment' => 'Not Send Where Emoney Payment',
            'not_send_where_partial_payment' => 'Not Send Where Partial Payment',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(PaymentReminderMessageCategory::className(), ['id' => 'category_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return int|string
     */
    public function getContractorCount()
    {
        return PaymentReminderMessageContractor::find()
            ->andWhere(['company_id' => Yii::$app->user->identity->company->id])
            ->andWhere(['message_' . $this->number => true])
            ->count();
    }

    /**
     * @return bool
     */
    public function _save()
    {
        $this->for_email_list = serialize($this->for_email_list);
        if ($this->activate) {
            $this->status = self::STATUS_ACTIVE;
        }

        return $this->save();
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->timeInput) {
                $this->time = $this->timeInput.':00';
            } else {
                $this->time = null;
            }
            $this->for_email_list = implode(',', array_filter([
                $this->email1,
                $this->email2,
                $this->email3,
            ]));

            return true;
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();

        if ($this->time) {
            $time = explode(':', $this->time);
            unset($time[2]);
            $this->timeInput = implode(':', $time);
        }

        if ($this->for_email_list) {
            $emailArray = explode(',', $this->for_email_list);
            $this->email1 = $emailArray[0] ?? null;
            $this->email2 = $emailArray[1] ?? null;
            $this->email3 = $emailArray[2] ?? null;
        }
    }
}
