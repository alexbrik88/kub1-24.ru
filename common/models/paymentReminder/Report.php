<?php

namespace common\models\paymentReminder;

use common\models\Company;
use common\models\Contractor;
use common\models\document\Invoice;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "payment_reminder_report".
 *
 * @property int $id
 * @property int $company_id
 * @property int $contractor_id
 * @property int $message_id
 * @property int $category_id
 * @property string $date
 * @property int $days_overdue
 * @property int $debt_amount
 * @property string $invoice
 * @property int $is_sent
 * @property string $subject
 * @property string $event_header
 * @property string $event_body
 * @property string $email
 *
 * @property PaymentReminderMessageCategory $category
 * @property Company $company
 * @property Contractor $contractor
 * @property PaymentReminderMessage $message
 * @property ReportInvoice[] $reportInvoices
 * @property Invoice[] $invoices
 */
class Report extends \yii\db\ActiveRecord
{
    const IS_SENT = 1;
    const IS_NOT_SENT = 0;

    public static $isSentItems = [
        self::IS_SENT => 'Отправлено',
        self::IS_NOT_SENT => 'Не отправлено',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payment_reminder_report';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'contractor_id' => 'Покупатель',
            'message_id' => 'Message ID',
            'category_id' => 'Категория',
            'date' => 'Дата отправки',
            'days_overdue' => 'Дней просрочки',
            'debt_amount' => 'Задолженность',
            'invoice' => 'По счету',
            'is_sent' => 'Статус письма',
            'subject' => 'Тема письма',
            'event' => 'Событие',
            'event_header' => 'Event Header',
            'event_body' => 'Event Body',
        ];
    }

    /**
     * Gets query for [[Category]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCategory()
    {
        return $this->hasOne(PaymentReminderMessageCategory::className(), ['id' => 'category_id']);
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * Gets query for [[Contractor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getContractor()
    {
        return $this->hasOne(Contractor::className(), ['id' => 'contractor_id']);
    }

    /**
     * Gets query for [[Message]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMessage()
    {
        return $this->hasOne(PaymentReminderMessage::className(), ['id' => 'message_id']);
    }

    /**
     * Gets query for [[ReportInvoices]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getReportInvoices()
    {
        return $this->hasMany(ReportInvoice::className(), ['report_id' => 'id']);
    }

    /**
     * Gets query for [[Invoices]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInvoices()
    {
        return $this->hasMany(Invoice::className(), ['id' => 'invoice_id'])->viaTable('payment_reminder_report_invoice', ['report_id' => 'id']);
    }

    /**
     * @return string
     */
    public function getEvent()
    {
        return Html::tag('div', $this->event_header, ['class' => 'event_header']).
               Html::tag('div', $this->event_body, ['class' => 'event_body']);
    }

    /**
     * @return string
     */
    public function getIsSentText()
    {
        return ArrayHelper::getValue(self::$isSentItems, $this->is_sent, '');
    }
}
