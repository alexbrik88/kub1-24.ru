<?php

namespace common\models\paymentReminder;

use Yii;

/**
 * This is the model class for table "payment_reminder_message_category".
 *
 * @property integer $id
 * @property string $name
 *
 * @property PaymentReminderMessage[] $paymentReminderMessages
 */
class PaymentReminderMessageCategory extends \yii\db\ActiveRecord
{
    const CATEGORY_DEBT = 1;
    const CATEGORY_PAID = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_reminder_message_category';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentReminderMessages()
    {
        return $this->hasMany(PaymentReminderMessage::className(), ['category_id' => 'id']);
    }
}
