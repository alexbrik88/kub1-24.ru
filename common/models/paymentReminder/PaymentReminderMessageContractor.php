<?php

namespace common\models\paymentReminder;

use Yii;
use common\models\Company;
use common\models\Contractor;

/**
 * This is the model class for table "payment_reminder_message_contractor".
 *
 * @property integer $id
 * @property integer $company_id
 * @property integer $contractor_id
 * @property integer $message_1
 * @property integer $message_2
 * @property integer $message_3
 * @property integer $message_4
 * @property integer $message_5
 * @property integer $message_6
 * @property integer $message_7
 * @property integer $message_8
 * @property integer $message_9
 * @property integer $message_10
 * @property string $last_message_date_1
 * @property string $last_message_date_2
 * @property string $last_message_date_3
 * @property string $last_message_date_4
 * @property string $last_message_date_5
 * @property string $last_message_date_6
 * @property string $last_message_date_7
 * @property string $last_message_date_8
 * @property string $last_message_date_9
 * @property string $last_message_date_10
 * @property string $last_message_contractor_email_1
 * @property string $last_message_contractor_email_2
 * @property string $last_message_contractor_email_3
 * @property string $last_message_contractor_email_4
 * @property string $last_message_contractor_email_5
 * @property string $last_message_contractor_email_6
 * @property string $last_message_contractor_email_7
 * @property string $last_message_contractor_email_8
 * @property string $last_message_contractor_email_9
 * @property string $last_message_contractor_email_10
 *
 * @property Company $company
 * @property Contractor $contractor
 */
class PaymentReminderMessageContractor extends \yii\db\ActiveRecord
{
    /**
     *
     */
    const MESSAGE_ACTIVE = 1;
    /**
     *
     */
    const MESSAGE_INACTIVE = 0;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_reminder_message_contractor';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                [
                    'message_1',
                    'message_2',
                    'message_3',
                    'message_4',
                    'message_5',
                    'message_6',
                    'message_7',
                    'message_8',
                    'message_9',
                    'message_10',
                ],
                'boolean',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'contractor_id' => 'Покупатель',
            'message_1' => 'Message 1',
            'message_2' => 'Message 2',
            'message_3' => 'Message 3',
            'message_4' => 'Message 4',
            'message_5' => 'Message 5',
            'message_6' => 'Message 6',
            'message_7' => 'Message 7',
            'message_8' => 'Message 8',
            'message_9' => 'Message 9',
            'message_10' => 'Message 10',
            'last_message_date_1' => 'Last Message Date 1',
            'last_message_date_2' => 'Last Message Date 2',
            'last_message_date_3' => 'Last Message Date 3',
            'last_message_date_4' => 'Last Message Date 4',
            'last_message_date_5' => 'Last Message Date 5',
            'last_message_date_6' => 'Last Message Date 6',
            'last_message_date_7' => 'Last Message Date 7',
            'last_message_date_8' => 'Last Message Date 8',
            'last_message_date_9' => 'Last Message Date 9',
            'last_message_date_10' => 'Last Message Date 10',
            'last_message_contractor_email_1' => 'Last Message Contractor Email 1',
            'last_message_contractor_email_2' => 'Last Message Contractor Email 2',
            'last_message_contractor_email_3' => 'Last Message Contractor Email 3',
            'last_message_contractor_email_4' => 'Last Message Contractor Email 4',
            'last_message_contractor_email_5' => 'Last Message Contractor Email 5',
            'last_message_contractor_email_6' => 'Last Message Contractor Email 6',
            'last_message_contractor_email_7' => 'Last Message Contractor Email 7',
            'last_message_contractor_email_8' => 'Last Message Contractor Email 8',
            'last_message_contractor_email_9' => 'Last Message Contractor Email 9',
            'last_message_contractor_email_10' => 'Last Message Contractor Email 10',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContractor()
    {
        return $this->hasOne(Contractor::className(), ['id' => 'contractor_id']);
    }

    /**
     * @param $number
     * @param null $companyID
     * @return null|PaymentReminderMessage
     */
    public static function getMessageTemplate($number, $companyID = null)
    {
        return PaymentReminderMessage::find()
            ->andWhere(['company_id' => $companyID ? $companyID : Yii::$app->user->identity->company->id])
            ->andWhere(['number' => $number])
            ->one();
    }

    /**
     * @param $number
     * @param null $companyID
     * @return bool|int
     */
    public static function getMessageTemplateStatus($number, $companyID = null)
    {
        /* @var $paymentReminderMessage PaymentReminderMessage */
        $paymentReminderMessage = self::getMessageTemplate($number, $companyID);

        return $paymentReminderMessage ? $paymentReminderMessage->status : false;
    }

    /**
     * @param $number
     * @return bool
     */
    public static function isAllContractorsChecked($number)
    {
        return PaymentReminderMessageContractor::find()
                ->joinWith('contractor')
                ->andWhere([PaymentReminderMessageContractor::tableName() . '.company_id' => Yii::$app->user->identity->company->id])
                ->andWhere(['message_' . $number => false])
                ->andWhere([Contractor::tableName() . '.status' => Contractor::ACTIVE])
                ->count() == 0;
    }
}
