<?php

namespace common\models\paymentReminder;

use common\models\Company;
use Yii;

/**
 * This is the model class for table "payment_reminder_settings".
 *
 * @property int $id
 * @property int $company_id
 * @property int $send_mode
 * @property int $overdue_invoice
 * @property int $exceeded_limit
 * @property int $invoice_paid
 * @property int $add_exceptions
 * @property string $exclude_ids
 *
 * @property Company $company
 */
class Settings extends \yii\db\ActiveRecord
{
    const SEND_DISABLE = 0;
    const SEND_BY_SUITABLE = 1;
    const SEND_TO_SELECTED = 2;

    public static $modeItems = [
        self::SEND_DISABLE => 'Не отправлять',
        self::SEND_BY_SUITABLE => 'Отправлять покупателям, у которых',
        self::SEND_TO_SELECTED => 'Отправлять покупателям, выбранным вручную',
    ];

    public static $activeModes = [
        self::SEND_BY_SUITABLE,
        self::SEND_TO_SELECTED,
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'payment_reminder_settings';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['send_mode'], 'required'],
            [['send_mode'], 'in', 'range' => array_keys(self::$modeItems)],
            [['overdue_invoice', 'exceeded_limit', 'invoice_paid', 'add_exceptions'], 'boolean'],
            [['excludeIds'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'send_mode' => 'Send Mode',
            'overdue_invoice' => 'Имеется просрочка оплаты счета. Серия писем «Счет НЕ оплачен»',
            'exceeded_limit' => 'Сумма долга превысила допустимый лимит. Серия писем «Сумма просроченного долга больше суммы N.»',
            'invoice_paid' => 'Счет оплачен. Серия писем «Счет оплачен»',
            'add_exceptions' => 'Добавить исключения из правил',
            'exclude_ids' => 'Не отправлять письма этим покупателям',
            'excludeIds' => 'Не отправлять письма этим покупателям',
        ];
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * Set Exclude Contractor ID array
     *
     * @param array $ids
     */
    public function setExcludeIds($ids)
    {
        if (is_array($ids)) {
            $ids = array_filter($ids);
            $ids = array_unique($ids);
            $this->exclude_ids = implode(',', $ids);
        } else {
            $this->exclude_ids = '';
        }
    }

    /**
     * Get Exclude Contractor ID array
     *
     * @return array
     */
    public function getExcludeIds()
    {
        return explode(',', $this->exclude_ids);
    }
}
