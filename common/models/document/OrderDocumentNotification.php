<?php

namespace common\models\document;

use common\components\date\DateHelper;
use frontend\modules\telegram\components\NotificationInterface;

class OrderDocumentNotification implements NotificationInterface
{
    use NotificationLimitTrait;

    /**
     * @var OrderDocument
     */
    private $orderDocument;

    /**
     * @param OrderDocument $orderDocument
     */
    public function __construct(OrderDocument $orderDocument)
    {
        $this->orderDocument = $orderDocument;
    }

    /**
     * @inheritDoc
     */
    public function isEnabled(): bool
    {
        return ($this->orderDocument->priceList && $this->orderDocument->priceList->notify_to_telegram);
    }

    /**
     * @inheritDoc
     */
    public function getMessage(): string
    {
        return sprintf(
            "Заказ № %d от %s\nПокупатель: %s\nКоличество позиций: %d\nСумма: %01.2f руб.",
            $this->orderDocument->document_number,
            $this->getDocumentDate(),
            $this->orderDocument->contractor_name_short,
            $this->orderDocument->total_order_count,
            $this->orderDocument->total_amount / 100
        );
    }

    /**
     * @inheritDoc
     */
    public function getCompanyId(): int
    {
        return $this->orderDocument->company_id;
    }

    /**
     * @return string
     */
    private function getDocumentDate(): string
    {
        return DateHelper::format(
            $this->orderDocument->document_date,
            DateHelper::FORMAT_USER_DATE,
            DateHelper::FORMAT_DATE
        );
    }
}
