<?php

namespace common\models\document;

use yii\db\ActiveQuery;

/**
 * @property-read InvoiceIncomeItem $incomeItem
 */
trait IncomeItemRelationTrait
{
    /**
     * @var string[]
     */
    protected array $incomeItemLink = ['id' => 'income_item_id'];

    /**
     * @return InvoiceIncomeItem
     */
    public function getIncomeItem(): InvoiceIncomeItem
    {
        return $this->__get('incomeItemRelation');
    }

    /**
     * @return ActiveQuery
     */
    public function getIncomeItemRelation(): ActiveQuery
    {
        return $this->hasOne(InvoiceIncomeItem::class, $this->incomeItemLink);
    }

    /**
     * @return bool
     */
    public function hasIncomeItem(): bool
    {
        if ($this->__get('incomeItemRelation')) {
            return true;
        }

        return false;
    }
}
