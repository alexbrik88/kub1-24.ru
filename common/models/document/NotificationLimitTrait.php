<?php

namespace common\models\document;

use common\models\service\SubscribeTariffGroup;
use frontend\modules\telegram\components\NotificationInterface;
use frontend\modules\telegram\jobs\SendNotificationJob;
use frontend\modules\telegram\models\Identity;

trait NotificationLimitTrait
{
    /** @var int Лимит бесплатных сообщений */
    protected $freeLimitCount = 5;

    /** @var string Формат сообщения */
    protected $freeLimitMessage = <<<TXT
Для компании %s исчерпан лимит бесплатных уведомлений.
Чтобы продолжить получать уведомления, перейдите на платный тариф.
TXT;

    /** @var int Задержка отправки */
    protected $pushDelay = 5;

    /** @var int[] Идентификаторы групп тарифов */
    protected $tariffGroupId = [
        SubscribeTariffGroup::B2B_PAYMENT,
        SubscribeTariffGroup::PRICE_LIST,
    ];

    /**
     * @see NotificationInterface::onDispatch()
     * @inheritDoc
     */
    public function onDispatch(Identity $identity): bool
    {
        foreach ($this->tariffGroupId as $tariffGroupId) {
            $subscribe = $identity->company->getActualSubscription($tariffGroupId);

            if ($subscribe && !$subscribe->isTrial) {
                return true;
            }
        }

        if ($identity->notification_count < $this->freeLimitCount) {
            return true;
        }

        if ($identity->notification_count == $this->freeLimitCount) {
            $text = sprintf($this->freeLimitMessage, $identity->company->name_short);
            SendNotificationJob::dispatchJob($identity, $text, $this->pushDelay);

            return true;
        }

        return false;
    }
}
