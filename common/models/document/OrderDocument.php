<?php

namespace common\models\document;

use common\components\date\DateHelper;
use common\components\date\DatePickerFormatBehavior;
use common\components\helpers\ArrayHelper;
use common\components\pdf\PdfRenderer;
use common\components\TextHelper;
use common\models\AgreementType;
use common\models\bank\Bank;
use common\models\employee\EmployeeRole;
use common\models\EmployeeCompany;
use common\models\file\File;
use common\models\product\PriceList;
use common\models\product\Product;
use common\models\product\Store;
use common\models\store\StoreUser;
use common\models\TaxRate;
use frontend\components\StatisticPeriod;
use frontend\models\Documents;
use frontend\models\log\Log;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\modules\documents\assets\InvoicePrintAsset;
use frontend\modules\documents\components\InvoiceHelper;
use frontend\modules\telegram\behaviors\NotificationBehavior;
use frontend\modules\telegram\behaviors\NotificationFactoryInterface;
use frontend\modules\telegram\components\NotificationInterface;
use Yii;
use common\models\employee\Employee;
use common\models\Company;
use common\models\Contractor;
use common\models\document\status\InvoiceStatus;
use common\models\document\status\OrderDocumentStatus;
use yii\behaviors\TimestampBehavior;
use yii\db\Connection;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\VarDumper;

/**
 * This is the model class for table "order_document".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $author_id
 * @property integer $store_author_id
 * @property integer $company_id
 * @property integer $contractor_id
 * @property integer $status_id
 * @property integer $status_author_id
 * @property integer $status_store_author_id
 * @property integer $status_updated_at
 * @property string $production_type
 * @property string $document_number
 * @property string $document_additional_number
 * @property string $document_date
 * @property string $basis_document_number
 * @property string $basis_document_name
 * @property string $basis_document_date
 * @property integer $basis_document_type_id
 * @property string $ship_up_to_date
 * @property integer $is_deleted
 * @property string $comment
 * @property integer $has_nds
 * @property integer $email_messages_count
 * @property string $total_amount
 * @property integer $total_mass
 * @property integer $total_order_count
 * @property string $contractor_name_short
 * @property string $contractor_name_full
 * @property string $contractor_director_name
 * @property string $contractor_director_post_name
 * @property string $contractor_address_legal_full
 * @property string $contractor_bank_name
 * @property string $contractor_bank_city
 * @property string $contractor_bik
 * @property string $contractor_inn
 * @property string $contractor_kpp
 * @property string $contractor_ks
 * @property string $contractor_rs
 * @property boolean $has_discount
 * @property boolean $has_markup
 * @property string $total_mass_gross
 * @property string $total_place_count
 * @property integer $document_author_id
 * @property integer $invoice_id
 * @property string $comment_internal
 * @property string $uid
 * @property integer $store_id
 * @property boolean $from_store
 * @property integer $price_list_id
 * @property integer $type
 * @property integer $nds_view_type_id
 * @property integer $view_total_nds // calculated
 * @property integer $view_total_amount // calculated
 *
 *
 * @property Employee $author
 * @property StoreUser $storeAuthor
 * @property Company $company
 * @property Contractor $contractor
 * @property Employee $statusAuthor
 * @property StoreUser $statusStoreAuthor
 * @property OrderDocumentStatus $status
 * @property OrderDocumentProduct[] $orderDocumentProducts
 * @property AgreementType $agreementType
 * @property Invoice $invoice
 * @property boolean $isRejected
 * @property string $emailText
 * @property Bank $contractorBank
 * @property Store $store
 * @property PriceList $priceList
 *
 * @property boolean $createFromOut
 * @property array $outProducts
 */
class OrderDocument extends AbstractDocument implements NotificationFactoryInterface
{
    /**
     *
     */
    const HAS_NDS = 1;
    /**
     *
     */
    const HAS_NO_NDS = 0;
    /**
     *
     */
    const IS_DELETED = 1;
    /**
     *
     */
    const NOT_IS_DELETED = 0;
    /**
     *
     */
    const CREATED_FROM_STORE = 1;

    /**
     * order-document NDS view type constants
     */
    const NDS_VIEW_IN = 0;
    /**
     *
     */
    const NDS_VIEW_OUT = 1;
    /**
     *
     */
    const NDS_VIEW_WITHOUT = 2;
    /**
     * @var array
     * order-document NDS view values
     */
    public static $ndsViewList = [
        self::NDS_VIEW_IN => 'В том числе НДС',
        self::NDS_VIEW_OUT => 'НДС сверху',
        self::NDS_VIEW_WITHOUT => 'Без налога (НДС)',
    ];

    /**
     * @var string
     */
    public $printablePrefix = 'Заказ';

    /**
     * @var string
     */
    public static $uploadDirectory = 'order-document';

    /**
     * @var string
     */
    public $agreement;

    /**
     * @var OrderDocumentProduct[]
     */
    public $removedOrders = [];

    /**
     * @var string
     */
    public $emailTemplate = 'order-document';

    public $createFromOut = false;
    public $outProducts = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_document';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return
            ArrayHelper::merge(parent::behaviors(), [
                [
                    'class' => DatePickerFormatBehavior::className(),
                    'attributes' => [
                        'ship_up_to_date' => [
                            'message' => 'Дата "Отгрузить до" указана неверно.',
                        ],
                        'basis_document_date' => [
                            'message' => 'Дата основания указана неверно.',
                        ],
                        'document_date' => [
                            'message' => 'Дата документа указана неверно.',
                        ],
                    ],
                ],
                'class' => TimestampBehavior::className(),
                'notification' => [
                    'class' => NotificationBehavior::class,
                    'events' => [self::EVENT_AFTER_INSERT],
                ],
            ]);

    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['comment', 'comment_internal'], 'trim'],
            [
                ['orderDocumentProducts'], 'required',
                'message' => 'Не найдено ни одного товара/услуги.',
            ],
            [
                ['ship_up_to_date'],
                function ($attribute) {
                    if ((new \DateTime($this->$attribute)) < (new \DateTime($this->document_date))) {
                        $this->addError($attribute, 'Значение «Отгрузить до» не должно быть меньше даты заказа.');
                    }
                },
            ],
            [['has_nds',], 'boolean', 'when' => function (OrderDocument $model) {
                return (boolean)$model->company->companyTaxationType->usn;
            }],
            [['nds_view_type_id',], 'required'],
            [['nds_view_type_id',], 'in', 'range' => array_keys(self::$ndsViewList)],
            [['document_number'], 'unique', 'targetAttribute' => ['company_id', 'document_number', 'document_additional_number'],
                'filter' => function ($query) {
                    $year = date('Y', strtotime($this->document_date));
                    $query->andWhere([
                        'is_deleted' => self::NOT_IS_DELETED,
                        'type' => $this->type
                    ])->andWhere([
                        'between',
                        'document_date',
                        $year . '-01-01',
                        $year . '-12-31',
                    ]);

                    return $query;
                },
                'message' => 'Заказ с номером {value} уже существует. Свободный порядковый номер ' .
                    $this->getNextDocumentNumber($this->company ? $this->company : Yii::$app->user->identity->company, null, date(DateHelper::FORMAT_DATE)),
            ],
            [['is_deleted', 'from_store',], 'boolean'],
            [['comment_internal'], 'string'],
            [['company_id', 'contractor_id', 'status_id', 'store_id', 'document_number', 'document_date',
                'type'], 'required'],
            [['created_at', 'updated_at', 'author_id', 'company_id', 'contractor_id', 'status_id', 'status_author_id',
                'status_updated_at', 'basis_document_type_id', 'email_messages_count', 'total_amount',
                'total_mass', 'store_author_id', 'status_store_author_id'], 'integer'],
            [['document_additional_number', 'comment', 'agreement'], 'string'],
            [['document_date', 'basis_document_date', 'ship_up_to_date'], 'safe'],
            [['has_discount', 'has_markup'], 'boolean'],
            [['total_place_count', 'total_mass_gross'], 'string', 'max' => 255,],
            [['total_order_count'], 'integer', 'min' => 1, 'max' => 999999,],
            [['production_type', 'document_number', 'basis_document_name', 'contractor_director_name',
                'contractor_director_post_name', 'contractor_address_legal_full'], 'string', 'max' => 255],
            [['contractor_name_short', 'contractor_name_full',], 'string'],
            [['basis_document_number'], 'string', 'max' => 50],
            [['contractor_bank_name', 'contractor_bank_city', 'contractor_inn', 'contractor_kpp'], 'string', 'max' => 45],
            [['contractor_bik'], 'string', 'max' => 9],
            [['contractor_ks', 'contractor_rs'], 'string', 'max' => 20],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['author_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['contractor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contractor::className(), 'targetAttribute' => ['contractor_id' => 'id']],
            [['status_author_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['status_author_id' => 'id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderDocumentStatus::className(), 'targetAttribute' => ['status_id' => 'id']],
            [['invoice_id'], 'exist', 'skipOnError' => true, 'targetClass' => Invoice::className(), 'targetAttribute' => ['invoice_id' => 'id']],
            [['store_id'], 'exist', 'skipOnError' => true, 'targetClass' => Store::className(), 'targetAttribute' => ['store_id' => 'id']],
            [['store_author_id'], 'exist', 'skipOnError' => true, 'targetClass' => StoreUser::className(), 'targetAttribute' => ['store_author_id' => 'id']],
            [['status_store_author_id'], 'exist', 'skipOnError' => true, 'targetClass' => StoreUser::className(), 'targetAttribute' => ['status_store_author_id' => 'id']],
            [['price_list_id'], 'exist', 'skipOnEmpty' => true, 'skipOnError' => true, 'targetClass' => PriceList::className(), 'targetAttribute' => ['price_list_id' => 'id']],
            [['createFromOut'], 'filter', 'filter' => function ($value) {
                return (int)$value;
            }],
            [['createFromOut'], 'boolean'],
            [['outProducts'], 'required', 'when' => function (OrderDocument $model) {
                return (bool)$model->createFromOut;
            }],
            [['outProducts'], 'outProductsValidator', 'skipOnError' => true, 'when' => function (OrderDocument $model) {
                return $model->createFromOut;
            }],
        ]);
    }

    // validate min quantity by out document
    public function outProductsValidator($attribute, $params)
    {
        foreach ($this->outProducts as $outProductId => $outPos) {
            $isExists = false;
            foreach ($this->orderDocumentProducts as $order) {
                if ($order->product_id == $outProductId) {
                    $isExists = true;
                    if (bccomp(array_sum($outPos['quantity']), $order->quantity, 4) > 0) {
                        $errorText = 'Минимальное кол-во для позиции "' . ($order->product_title) . '": ';
                        $errorText .= number_format(array_sum($outPos['quantity']), 4);
                        $errorText .= ' ' . $order->product->productUnit->name;
                        $order->addError('quantity', $errorText);
                        $this->addErrors($order->errors);
                        return false;
                    }
                }
            }
            if (!$isExists) {
                $errorText = 'Товары входящего заказа не соответствуют товарам исходящего заказа';
                $this->addError('outProducts', $errorText);
                return false;
            }
        }

        return true;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'author_id' => 'Author ID',
            'company_id' => 'Company ID',
            'contractor_id' => 'Покупатель/Поставщик',
            'status_id' => 'Status ID',
            'status_author_id' => 'Status Author ID',
            'status_updated_at' => 'Status Updated At',
            'production_type' => 'Production Type',
            'document_number' => 'Номер документа',
            'document_additional_number' => 'Document Additional Number',
            'document_date' => 'Document Date',
            'basis_document_number' => 'Basis Document Number',
            'basis_document_name' => 'Basis Document Name',
            'basis_document_date' => 'Basis Document Date',
            'basis_document_type_id' => 'Basis Document Type ID',
            'ship_up_to_date' => 'Отгрузить до',
            'is_deleted' => 'Is Deleted',
            'comment' => 'Comment',
            'has_nds' => 'Has Nds',
            'email_messages_count' => 'Email Messages Count',
            'total_amount' => 'Total Amount',
            'total_mass' => 'Total Mass',
            'total_order_count' => 'Количество наименований',
            'contractor_name_short' => 'Contractor Name Short',
            'contractor_name_full' => 'Contractor Name Full',
            'contractor_director_name' => 'Contractor Director Name',
            'contractor_director_post_name' => 'Contractor Director Post Name',
            'contractor_address_legal_full' => 'Contractor Address Legal Full',
            'contractor_bank_name' => 'Contractor Bank Name',
            'contractor_bank_city' => 'Contractor Bank City',
            'contractor_bik' => 'Contractor Bik',
            'contractor_inn' => 'Contractor Inn',
            'contractor_kpp' => 'Contractor Kpp',
            'contractor_ks' => 'Contractor Ks',
            'contractor_rs' => 'Contractor Rs',
            'has_discount' => 'Has Discount',
            'has_markup' => 'Has Markup',
            'total_place_count' => 'Total Place Count',
            'total_mass_gross' => 'Total Mass Gross',
            'store_id' => 'Склад',
            'price_list_id' => 'Прайс-лист',
            'nds_view_type_id' => 'Вид НДС'
        ];
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();

        $this->agreement = "{$this->basis_document_name}&{$this->basis_document_number}&{$this->basis_document_date}&{$this->basis_document_type_id}";
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if ($this->contractor_id && ($contractor = Contractor::findOne($this->contractor_id)) !== null) {
            if ($this->isNewRecord || $this->isAttributeChanged('contractor_id')) {
                $this->setContractor($contractor);
            }
            $contractor = Contractor::findOne($this->contractor_id);
            $this->populateRelation('contractor', $contractor);
        }
        $this->total_amount = 0;
        $this->total_order_count = count($this->orderDocumentProducts);
        foreach ($this->orderDocumentProducts as $order)
        {
            $amountWithVat = ($this->type == Documents::IO_TYPE_OUT)
                ? $order->amount_sales_with_vat
                : $order->amount_purchase_with_vat;

            $this->total_amount += (float)$amountWithVat;
        }
        $agreement = explode('&', $this->agreement);
        if (isset($agreement[0]) && isset($agreement[1]) && isset($agreement[2])) {
            $this->basis_document_name = $agreement[0];
            $this->basis_document_number = $agreement[1];
            $this->basis_document_date = $agreement[2];
            $this->basis_document_type_id = isset($agreement[3]) ? $agreement[3] : null;
        } else {
            $this->basis_document_name = null;
            $this->basis_document_number = null;
            $this->basis_document_date = null;
            $this->basis_document_type_id = null;
        }
        if (!$this->company || $this->company->strict_mode == Company::ON_STRICT_MODE) {
            $this->addError('company_id', 'Необходимо заполнить реквизиты вашей компании');
        }

        if ($this->has_discount) {
            $this->has_markup = 0;
        }
        if ($this->has_markup) {
            $this->has_discount = 0;
        }

        return parent::beforeValidate();
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            /* @var $user Employee|StoreUser */
            $user = Yii::$app->user->identity;
            if ($insert || $this->isAttributeChanged('status_id')) {
                $this->status_updated_at = time();
                if ($user instanceof Employee) {
                    $this->status_author_id = $user->id;
                } elseif ($user) {
                    $this->status_store_author_id = $user->id;
                }
            }
            if ($this->has_nds === null) {
                $this->has_nds = $this->has_nds = ($this->nds_view_type_id == self::NDS_VIEW_WITHOUT) ? self::HAS_NO_NDS : self::HAS_NDS;
            }
            if ($insert) {
                $this->uid = static::generateUid();
            }
            if ($this->isAttributeChanged('status_id') && $this->status_id == OrderDocumentStatus::STATUS_CANCELED) {
                if (!$this->canCancel()) {
                    return false;
                }
                if ($this->invoice && $this->invoice->invoice_status_id != InvoiceStatus::STATUS_REJECTED) {
                    if ($this->invoice->invoiceStatus->rejectAllowed()) {
                        $this->invoice->invoice_status_id = InvoiceStatus::STATUS_REJECTED;

                        $saved = LogHelper::save($this->invoice, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function ($model) {
                            if ($model->save(true, ['invoice_status_id', 'invoice_status_updated_at', 'invoice_status_author_id'])) {

                                return $model->rejectDacuments();
                            }

                            return false;
                        });
                        if (!$saved) {
                            return false;
                        }
                    } else {
                        return false;
                    }
                }
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert) {
            $this->contractor->last_basis_document = $this->agreement;
            if ($this->contractor->isAttributeChanged('last_basis_document')) {
                $this->contractor->save(false, ['last_basis_document']);
            }
            if ($this->contractor->responsible_employee_id == null) {
                $this->contractor->updateAttributes([
                    'responsible_employee_id' => $this->document_author_id,
                ]);
            }
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Employee::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStoreAuthor()
    {
        return $this->hasOne(StoreUser::className(), ['id' => 'store_author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContractor()
    {
        return $this->hasOne(Contractor::className(), ['id' => 'contractor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusAuthor()
    {
        return $this->hasOne(Employee::className(), ['id' => 'status_author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusStoreAuthor()
    {
        return $this->hasOne(StoreUser::className(), ['id' => 'status_store_author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(OrderDocumentStatus::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentStatus()
    {
        return $this->getStatus();
    }

    /**
     * @return $this
     */
    public function getFile()
    {
        return $this->hasOne(File::className(), ['owner_id' => 'id'])
            ->onCondition(['owner_model' => OrderDocument::className(),]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderDocumentProducts()
    {
        return $this->hasMany(OrderDocumentProduct::class, ['order_document_id' => 'id'])
            ->orderBy(OrderDocumentProduct::tableName() . '.number ASC');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreementType()
    {
        return $this->hasOne(AgreementType::className(), ['id' => 'basis_document_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(Invoice::className(), ['id' => 'invoice_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStore()
    {
        return $this->hasOne(Store::className(), ['id' => 'store_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPriceList()
    {
        return $this->hasOne(PriceList::className(), ['id' => 'price_list_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNdsViewType()
    {
        return $this->hasOne(NdsViewType::className(), ['id' => 'nds_view_type_id']);
    }

    /**
     * Full order document number
     *
     * @return bool
     */
    public function getFullNumber()
    {
        return $this->document_number . $this->document_additional_number;
    }

    /**
     * @return string
     */
    public function getPrintTitle()
    {
        return str_replace([' ', '"'], ['_'],
            $this->printablePrefix . '_№' . $this->getFullNumber() . '_' .
            date_format(date_create($this->document_date), 'd.m.Y') . '_' .
            $this->company->getTitle(true)
        );
    }

    /**
     * @return string
     */
    public function getEmailSubject()
    {
        return 'Заказ No ' . $this->fullNumber
            . ' от ' . DateHelper::format($this->document_date, 'd.m.y', DateHelper::FORMAT_DATE)
            . ' от ' . $this->company->getTitle(true);
    }

    /**
     * @return boolean
     */
    public function getIsRejected()
    {
        return $this->status_id == OrderDocumentStatus::STATUS_CANCELED;
    }

    /**
     * @return boolean
     */
    public function getContractorBank()
    {
        return $this->contractor_bik ? Bank::findOne([
            'bik' => $this->contractor_bik,
            'is_blocked' => false,
        ]) : null;
    }

    /**
     * Get formatted log message from Log model
     *
     * @param Log $log
     *
     * @return string
     */
    public function getLogMessage(Log $log)
    {
        $title = 'Заказ №';
        $invoiceText = '';
        /* @var Invoice $invoice */
        $invoice = Invoice::findOne($log->getModelAttributeNew('invoice_id'));
        $title .= $log->getModelAttributeNew('document_number') . ' ' . $log->getModelAttributeNew('document_additional_number');
        $link = Html::a($title, [
            '/documents/order-document/view',
            'id' => $log->getModelAttributeNew('id'),
        ]);

        if ($invoice) {
            $amount = TextHelper::invoiceMoneyFormat($invoice->total_amount_with_nds, 2) . ' руб.';
            $contractorName = $invoice->contractor ? ', ' . $invoice->contractor->getTitle(true) : '';
            $invoiceText = ', ' . $amount . $contractorName . ',';
        }

        switch ($log->log_event_id) {
            case LogEvent::LOG_EVENT_CREATE:
                return $link . $invoiceText . ' был создан.';
            case LogEvent::LOG_EVENT_DELETE:
                return $title . $invoiceText . ' был удален.';
            case LogEvent::LOG_EVENT_UPDATE:
                return $link . $invoiceText . ' был изменен.';
            case LogEvent::LOG_EVENT_UPDATE_STATUS:
                return $link . $invoiceText . ' статус "' . OrderDocumentStatus::findOne($log->getModelAttributeNew('status_id'))->name . '"';
            default:
                return $log->message;
        }
    }

    /**
     * @return boolean
     */
    public function getDiscountSum()
    {
        return $this->getOrderDocumentProducts()->sum('[[base_price_with_vat]] * [[quantity]]') - $this->total_amount;
    }

    /**
     * @return boolean
     */
    public function getMarkupSum()
    {
        return $this->total_amount - $this->getOrderDocumentProducts()->sum('[[base_price_with_vat]] * [[quantity]]');
    }

    /**
     * @param Company $company
     * @param null $number
     * @param null $date
     * @return int
     */
    public static function getNextDocumentNumber(Company $company, $number = null, $date = null, $type = null)
    {
        $year = $date ? date('Y', strtotime($date)) : null;
        $numQuery = static::find()
            ->andWhere(['and',
                ['company_id' => $company->id],
                ['is_deleted' => false],
            ]);
        if ($year) {
            $numQuery->andWhere([
                'between',
                'document_date',
                $year . '-01-01',
                $year . '-12-31',
            ]);
        }
        if ($type !== null) {
            $numQuery->andWhere(['type' => $type]);
        }

        $lastNumber = $number ? $number : (int)$numQuery->max('(document_number * 1)');
        $nextNumber = 1 + $lastNumber;

        $existNumQuery = static::find()
            ->select('document_number')
            ->andWhere(['and',
                ['company_id' => $company->id],
                ['is_deleted' => false],
            ]);

        if ($type !== null) {
            $existNumQuery->andWhere(['type' => $type]);
        }

        if ($year) {
            $existNumQuery->andWhere([
                'between',
                'document_date',
                $year . '-01-01',
                $year . '-12-31',
            ]);
        }

        $numArray = $existNumQuery->column();

        if (in_array($nextNumber, $numArray)) {
            return static::getNextDocumentNumber($company, $nextNumber, $date);
        } else {
            return $nextNumber;
        }
    }

    /**
     * @param null $previousStatus
     */
    public function checkStatus($previousStatus = null)
    {
        if ($previousStatus !== OrderDocumentStatus::STATUS_SEND && $this->status_id == OrderDocumentStatus::STATUS_SEND) {
            if (!$this->invoice) {
                if ($this->createInvoice() && $this->invoice->getCanAddPackingList()) {
                    $this->invoice->createPackingList();
                }
            } elseif ($this->invoice->getCanAddPackingList()) {
                $this->invoice->createPackingList();
            }
        }
    }

    /**
     * @return bool
     * @throws \Exception
     * @throws \Throwable
     */
    public function createInvoice()
    {
        $company = $this->company;
        $model = new Invoice([
            'company_id' => $company->id,
        ]);
        $model->scenario = 'insert';
        $model->type = $this->type ?? Documents::IO_TYPE_OUT;
        $model->document_date = date(DateHelper::FORMAT_DATE);
        $model->contractor_id = $this->contractor_id;
        $model->isAutoinvoice = 0;
        $model->populateRelation('company', $company);
        $model->document_number = (string)Invoice::getNextDocumentNumber($company->id, $model->type, null, $model->document_date);
        $payment_delay = $model->type === Documents::IO_TYPE_IN ? $this->contractor->seller_payment_delay : $this->contractor->customer_payment_delay;
        $model->agreement = $this->agreement;
        $model->populateRelation('contractor', $this->contractor);
        $model->nds_view_type_id = $this->contractor->ndsViewTypeId;
        $model->has_discount = $this->has_discount;
        $model->has_markup = $this->has_markup;
        $model->payment_limit_date = date(DateHelper::FORMAT_DATE, strtotime("+$payment_delay day"));
        $model->price_precision = 2;
        $model->from_store_cabinet = $this->from_store;
        $invoiceSaved = Yii::$app->db->transaction(function (Connection $db) use ($model, $company) {
            foreach ($this->orderDocumentProducts as $orderDocumentProduct) {
                $product = Product::getById($model->company_id, $orderDocumentProduct->product_id);
                if ($product !== null) {
                    $order = OrderHelper::createOrderByProduct(
                        $product,
                        $model,
                        $orderDocumentProduct->quantity,
                        $orderDocumentProduct->selling_price_with_vat,
                        ($model->has_discount ? $orderDocumentProduct->discount : 0),
                        ($model->has_markup ? $orderDocumentProduct->markup : 0)
                    );
                    $order->product_title = $orderDocumentProduct->product_title;
                    $orderArray[] = $order;
                }
            }
            array_walk($orderArray, function (&$order, $key) {
                $order->number = $key + 1;
            });
            $model->populateRelation('orders', $orderArray);

            $model->total_mass_gross = (string)array_sum(ArrayHelper::getColumn($model->orders, 'mass_gross'));
            $model->total_place_count = (string)array_sum(ArrayHelper::getColumn($model->orders, 'place_count'));
            if (!InvoiceHelper::save($model)) {
                $db->transaction->rollBack();

                return false;
            }
            $this->invoice_id = $model->id;
            $this->save(true, ['invoice_id']);

            return true;
        });
        if ($invoiceSaved) {
            $this->populateRelation('invoice', $model);

            return true;
        }

        return false;
    }

    /**
     * @return string
     */
    public function getProductionTypeByOrderArray()
    {
        $typeArray = [];
        foreach ($this->orderDocumentProducts as $order) {
            $typeArray[$order->product->production_type] = $order->product->production_type;
        }
        sort($typeArray);

        return implode(', ', $typeArray);
    }

    /**
     * @return mixed
     */
    public function getTotalWeight()
    {
        return $this->getOrderDocumentProducts()
                ->joinWith('product')
                ->sum(OrderDocumentProduct::tableName() . '.quantity * ' . Product::tableName() . '.weight') * 1;
    }

    /**
     * @param Contractor $contractor
     */
    public function setContractor(Contractor $contractor)
    {
        $this->contractor_name_short = $contractor->getTitle(true);
        $this->contractor_name_full = $contractor->getTitle(false);
        $this->contractor_director_name = $contractor->director_name;
        $this->contractor_director_post_name = $contractor->director_post_name;
        $this->contractor_address_legal_full = ($contractor->face_type == Contractor::TYPE_LEGAL_PERSON ? $contractor->legal_address : $contractor->physical_address);
        $this->contractor_bank_name = $contractor->bank_name;
        $this->contractor_bank_city = $contractor->bank_city;
        $this->contractor_bik = $contractor->BIC;
        $this->contractor_inn = $contractor->ITN;
        $this->contractor_kpp = $contractor->PPC;
        $this->contractor_ks = $contractor->corresp_account;
        $this->contractor_rs = $contractor->current_account;
    }

    /**
     * @return bool
     */
    public function checkContractor()
    {
        $contractor = $this->contractor;
        $contractorNameShort = $contractor->getTitle(true);
        $contractorNameFull = $contractor->getTitle(false);
        $contractorAddressLegalFull = ($contractor->face_type == Contractor::TYPE_LEGAL_PERSON ? $contractor->legal_address : $contractor->physical_address);

        $contractorFields = [
            'contractor_name_short' => $contractorNameShort,
            'contractor_name_full' => $contractorNameFull,
            'contractor_director_name' => 'director_name',
            'contractor_director_post_name' => 'director_post_name',
            'contractor_address_legal_full' => $contractorAddressLegalFull,
            'contractor_bank_name' => 'bank_name',
            'contractor_bank_city' => 'bank_city',
            'contractor_bik' => 'BIC',
            'contractor_inn' => 'ITN',
            'contractor_kpp' => 'PPC',
            'contractor_ks' => 'corresp_account',
            'contractor_rs' => 'current_account',
        ];
        foreach ($contractorFields as $invoiceContractorField => $contractorField) {
            if ($contractor->hasAttribute($contractorField)) {
                if ($this->$invoiceContractorField !== $contractor->$contractorField) {
                    return true;
                }
            } elseif ($this->$invoiceContractorField !== $contractorField) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param $fileName
     * @param $orderDocument
     * @param string $destination
     * @return PdfRenderer
     */
    public static function getRenderer($fileName, $orderDocument, $destination = PdfRenderer::DESTINATION_FILE)
    {
        $renderer = new PdfRenderer([
            'view' => '@frontend/modules/documents/views/order-document/pdf-view',
            'params' => array_merge([
                'model' => $orderDocument,
            ]),

            'destination' => $destination,
            'filename' => $fileName,
            'displayMode' => PdfRenderer::DISPLAY_MODE_FULLPAGE,
            'format' => 'A4-P',
        ]);
        Yii::$app->controller->view->params['asset'] = InvoicePrintAsset::className();

        return $renderer;
    }

    /**
     * @return string
     */
    public function getEmailText()
    {
        $document_date = DateHelper::format($this->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
        $ship_up_to_date = DateHelper::format($this->ship_up_to_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
        $sum = TextHelper::invoiceMoneyFormat($this->total_amount, 2);
        $text = <<<EMAIL_TEXT
Здравствуйте!

Заказ № {$this->fullNumber} от {$document_date} на сумму {$sum} р.
Отгрузить до {$ship_up_to_date}г.
EMAIL_TEXT;

        return $text;

    }

    /**
     * @param null $contractorID
     * @return array|null|\yii\db\ActiveRecord
     */
    public function getPreviousOrderDocument($contractorID = null)
    {
        $year = date('Y', strtotime($this->document_date));
        /* @var $company Company */
        $company = $this->company;
        $previousOrderDocumentNumber = OrderDocument::find()
            ->select('MAX(`document_number` * 1) as maxDocumentNumber')
            ->andFilterWhere(['contractor_id' => $contractorID])
            ->andWhere(['is_deleted' => false])
            ->andWhere(['company_id' => $company->id])
            ->andWhere('`document_number` * 1 < ' . intval($this->document_number))
            ->andWhere([
                'between',
                'document_date',
                $year . '-01-01',
                $year . '-12-31',
            ])
            ->column();
        $previousOrderDocument = OrderDocument::find()
            ->andFilterWhere(['contractor_id' => $contractorID])
            ->andWhere(['company_id' => $company->id])
            ->andWhere(['document_number' => $previousOrderDocumentNumber])
            ->andWhere([
                'between',
                'document_date',
                $year . '-01-01',
                $year . '-12-31',
            ])
            ->one();
        if ($previousOrderDocument) {
            return $previousOrderDocument;
        }
        $existsYears = OrderDocument::find()
            ->select('YEAR(`document_date`) as year')
            ->andFilterWhere(['contractor_id' => $contractorID])
            ->andWhere(['company_id' => $company->id])
            ->andHaving(['<', 'year', $year])
            ->groupBy('year')
            ->orderBy(['year' => SORT_DESC])
            ->asArray()
            ->all();
        foreach ($existsYears as $year) {
            $previousOrderDocumentNumber = OrderDocument::find()
                ->select('MAX(`document_number` * 1) as maxDocumentNumber')
                ->andFilterWhere(['contractor_id' => $contractorID])
                ->andWhere(['is_deleted' => false])
                ->andWhere(['company_id' => $company->id])
                ->andWhere([
                    'between',
                    'document_date',
                    $year['year'] . '-01-01',
                    $year['year'] . '-12-31',
                ])
                ->column();
            $previousOrderDocument = OrderDocument::find()
                ->andFilterWhere(['contractor_id' => $contractorID])
                ->andWhere(['document_number' => $previousOrderDocumentNumber])
                ->andWhere(['company_id' => $company->id])
                ->andWhere([
                    'between',
                    'document_date',
                    $year['year'] . '-01-01',
                    $year['year'] . '-12-31',
                ])
                ->one();
            if ($previousOrderDocument) {
                return $previousOrderDocument;
            }
        }

        return null;
    }

    /**
     * @param null $contractorID
     * @return array|null|\yii\db\ActiveRecord
     */
    public function getNextOrderDocument($contractorID = null)
    {
        $year = date('Y', strtotime($this->document_date));
        /* @var $company Company */
        $company = $this->company;
        $nextOrderDocumentNumber = OrderDocument::find()
            ->select('MIN(`document_number` * 1) as minDocumentNumber')
            ->andFilterWhere(['contractor_id' => $contractorID])
            ->andWhere(['is_deleted' => false])
            ->andWhere(['company_id' => $company->id])
            ->andWhere('`document_number` * 1 > ' . intval($this->document_number))
            ->andWhere([
                'between',
                'document_date',
                $year . '-01-01',
                $year . '-12-31',
            ])
            ->column();
        $nextOrderDocument = OrderDocument::find()
            ->andFilterWhere(['contractor_id' => $contractorID])
            ->andWhere(['document_number' => $nextOrderDocumentNumber])
            ->andWhere(['company_id' => $company->id])
            ->andWhere([
                'between',
                'document_date',
                $year . '-01-01',
                $year . '-12-31',
            ])
            ->one();
        if ($nextOrderDocument) {
            return $nextOrderDocument;
        }
        $existsYears = OrderDocument::find()
            ->select('YEAR(`document_date`) as year')
            ->andFilterWhere(['contractor_id' => $contractorID])
            ->andWhere(['company_id' => $company->id])
            ->andHaving(['>', 'year', date('Y', strtotime($this->document_date))])
            ->groupBy('year')
            ->orderBy(['year' => SORT_ASC])
            ->asArray()
            ->all();
        foreach ($existsYears as $year) {
            $previousOrderDocumentNumber = OrderDocument::find()
                ->select('MAX(`document_number` * 1) as maxDocumentNumber')
                ->andFilterWhere(['contractor_id' => $contractorID])
                ->andWhere(['company_id' => $company->id])
                ->andWhere(['is_deleted' => false])
                ->andWhere([
                    'between',
                    'document_date',
                    $year['year'] . '-01-01',
                    $year['year'] . '-12-31',
                ])
                ->column();
            $previousOrderDocument = OrderDocument::find()
                ->andFilterWhere(['contractor_id' => $contractorID])
                ->andWhere(['company_id' => $company->id])
                ->andWhere(['document_number' => $previousOrderDocumentNumber])
                ->andWhere([
                    'between',
                    'document_date',
                    $year['year'] . '-01-01',
                    $year['year'] . '-12-31',
                ])
                ->one();
            if ($previousOrderDocument) {
                return $previousOrderDocument;
            }
        }

        return null;
    }

    /**
     * @return array
     */
    public static function getDashBordData($ioType = 2)
    {
        $data = [];
        $dateRange = StatisticPeriod::getSessionPeriod();
        /* @var $company Company */
        $company = Yii::$app->user->identity->company;
        foreach ([OrderDocumentStatus::STATUS_NEW, OrderDocumentStatus::STATUS_DELIVERED, OrderDocumentStatus::STATUS_PAID] as $status) {
            $status = OrderDocumentStatus::findOne($status);
            $data[] = [
                'id' => $status->id,
                'color' => $status->color,
                'name' => OrderDocumentStatus::$statusName[$status->id],
                'amount' => OrderDocument::find()
                    ->andWhere(['and',
                        ['company_id' => $company->id],
                        ['is_deleted' => false],
                        ['between', 'document_date', $dateRange['from'], $dateRange['to']],
                        ['status_id' => $status->id],
                        ['type' => $ioType],
                    ])
                    ->sum('total_amount'),
                'count' => OrderDocument::find()
                    ->andWhere(['and',
                        ['company_id' => $company->id],
                        ['is_deleted' => false],
                        ['between', 'document_date', $dateRange['from'], $dateRange['to']],
                        ['status_id' => $status->id],
                        ['type' => $ioType],
                    ])
                    ->count(),
            ];
        }

        return $data;
    }

    /**
     * @param StoreUser $storeUser
     * @return array
     */
    public static function getStoreDashBoardData(StoreUser $storeUser)
    {
        $data = [];
        $dateRange = StatisticPeriod::getSessionPeriod();
        foreach ([OrderDocumentStatus::STATUS_NEW, 'not-paid', OrderDocumentStatus::STATUS_DELIVERED] as $status) {
            $contractor = $storeUser->currentKubContractor;
            $company = $storeUser->company ? $storeUser->company : $storeUser->currentKubCompany;
            if ($status == 'not-paid') {
                $data[] = [
                    'id' => 'not-paid',
                    'color' => '#f3565d',
                    'name' => 'Неоплаченные',
                    'amount' => OrderDocument::find()
                        ->andWhere(['and',
                            ['company_id' => $company->id],
                            $contractor ? ['contractor_id' => $contractor->id] : [],
                            ['is_deleted' => false],
                            ['between', 'document_date', $dateRange['from'], $dateRange['to']],
                            ['not', ['status_id' => OrderDocumentStatus::STATUS_PAID]],
                        ])->sum('total_amount'),
                    'count' => OrderDocument::find()
                        ->andWhere(['and',
                            ['company_id' => $company->id],
                            $contractor ? ['contractor_id' => $contractor->id] : [],
                            ['is_deleted' => false],
                            ['between', 'document_date', $dateRange['from'], $dateRange['to']],
                            ['not', ['status_id' => OrderDocumentStatus::STATUS_PAID]],
                        ])->count(),
                ];
            } else {
                $status = OrderDocumentStatus::findOne($status);
                $data[] = [
                    'id' => $status->id,
                    'color' => $status->color,
                    'name' => OrderDocumentStatus::$statusName[$status->id],
                    'amount' => OrderDocument::find()
                        ->andWhere(['and',
                            ['company_id' => $company->id],
                            $contractor ? ['contractor_id' => $contractor->id] : [],
                            ['is_deleted' => false],
                            ['between', 'document_date', $dateRange['from'], $dateRange['to']],
                            ['status_id' => $status->id],
                        ])->sum('total_amount'),
                    'count' => OrderDocument::find()
                        ->andWhere(['and',
                            ['company_id' => $company->id],
                            $contractor ? ['contractor_id' => $contractor->id] : [],
                            ['is_deleted' => false],
                            ['between', 'document_date', $dateRange['from'], $dateRange['to']],
                            ['status_id' => $status->id],
                        ])->count(),
                ];
            }
        }

        return $data;
    }

    /**
     * @throws \MpdfException
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function loadEmailFiles()
    {
        $files = [];

        $emailFile = new EmailFile();
        $emailFile->company_id = $this->company_id;
        $emailFile->file_name = str_replace(['\\', '/'], '_', $this->pdfFileName);
        $emailFile->ext = 'pdf';
        $emailFile->mime = 'application/pdf';
        $emailFile->size = 0;
        $emailFile->type = EmailFile::TYPE_ORDER_DOCUMENT;
        if ($emailFile->save()) {
            $path = $emailFile->getFilePath() . DIRECTORY_SEPARATOR . $emailFile->file_name;
            $size = file_put_contents($path, self::getRenderer(null, $this, PdfRenderer::DESTINATION_STRING, null)->output(false));
            $emailFile->size = ceil($size / 1024);
            if ($emailFile->save(true, ['size'])) {
                $files[] = [
                    'id' => $emailFile->id,
                    'name' => $emailFile->file_name,
                    'documentPdf' => true,
                    'previewImg' => '<img src="/img/email/emailPdf.png" class="preview-img">',
                    'size' => $emailFile->size,
                    'deleteUrl' => Url::to(['/email/delete-email-file', 'id' => $emailFile->id]),
                    'downloadUrl' => Url::to(['/email/download-file', 'id' => $emailFile->id]),
                ];
            }
        }

        return $files;
    }

    /**
     * @param EmployeeCompany $sender
     * @param array|string $toEmail
     * @param null $emailText
     * @param null $subject
     * @param null $addStamp
     * @return bool
     */
    public function sendAsEmail(EmployeeCompany $sender, $toEmail, $emailText = null, $subject = null, $addStamp = null)
    {
        if ($this->uid == null) {
            $this->updateAttributes([
                'uid' => self::generateUid(),
            ]);
        }
        $message = $this->getMailerMessage($sender, $toEmail, $emailText, $subject)
            ->attachContent(self::getRenderer(null, $this, PdfRenderer::DESTINATION_STRING, $addStamp)->output(false), [
                'fileName' => $this->getPdfFileName(),
                'contentType' => 'application/pdf',
            ]);

        if ($message->send()) {
            $this->updateSendStatus($sender->employee_id);

            return true;
        }

        return false;
    }

    /**
     * @param integer $employeeId
     * @return bool
     * @throws \yii\base\Exception
     */
    public function updateSendStatus($employeeId)
    {
        $this->updateAttributes([
            'status_id' => OrderDocumentStatus::STATUS_SEND,
            'status_updated_at' => time(),
            'status_author_id' => $employeeId,
        ]);
        LogHelper::save($this, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function ($model) {
            return true;
        });
    }

    /**
     * @return array
     */
    public static function getStoreArray()
    {
        /* @var $user Employee|StoreUser
         * @var $responsibleStore Store
         */
        $user = Yii::$app->user->identity;
        $company = $user instanceof Employee ? $user->company : $user->currentStoreCompany->currentStoreCompanyContractor->contractor->company;
        $storesQuery = $company->getStores();
        if ($user instanceof StoreUser || in_array($user->currentEmployeeCompany->employee_role_id, [EmployeeRole::ROLE_CHIEF, EmployeeRole::ROLE_SUPERVISOR])) {
            $storesQuery = $storesQuery->orderBy('is_main');
        } elseif (($responsibleStore = $company->getStores()->andWhere(['responsible_employee_id' => $user->id])->one()) !== null) {
            return [$responsibleStore->id => $responsibleStore->name];
        }

        return ArrayHelper::map($storesQuery->all(), 'id', 'name');
    }

    /**
     * @return int|mixed
     */
    public function getShippedCount()
    {
        return $this->invoice ?
            $this->invoice->getPackingLists()
                ->leftJoin(['packingListOrder' => OrderPackingList::tableName()],
                    'packingListOrder.packing_list_id = ' . PackingList::tableName() . '.id')
                ->sum('packingListOrder.quantity') * 1 : 0;
    }

    /**
     * @return string
     */
    public function getBasisDocString()
    {
        $str = '';
        if ($this->basis_document_name && $this->basis_document_number && $this->basis_document_date) {
            $str .= $this->basis_document_name;
            $str .= ' № ' . $this->basis_document_number;
            $str .= ' от ' . date_create_from_format('Y-m-d', $this->basis_document_date)->format('d.m.Y');
        }

        return $str;
    }

    /**
     * @return bool
     */
    public function canUpdateInvoice()
    {
        $attributes = ['contractor_id', 'comment_internal', 'agreement'];
        $invoice = $this->invoice;

        if ($this->getOrderDocumentProducts()->count() !== $invoice->getOrders()->count()) {
            return true;
        }
        foreach ($attributes as $attribute) {
            if ($invoice->$attribute != str_replace('&&&', '', $this->$attribute)) { // fix "&&&" agreement
                return true;
            }
        }
        foreach ($this->orderDocumentProducts as $orderDocumentProduct) {

            if ($this->type == Documents::IO_TYPE_OUT) {
                $ordersExists = $invoice->getOrders()
                    ->andWhere(['product_id' => $orderDocumentProduct->product_id])
                    ->andWhere(['quantity' => $orderDocumentProduct->quantity])
                    ->andWhere(['product_title' => $orderDocumentProduct->product_title])
                    ->andWhere(['discount' => $orderDocumentProduct->discount])
                    ->andWhere(['and',
                        ['selling_price_no_vat' => $orderDocumentProduct->selling_price_no_vat],
                        ['selling_price_with_vat' => $orderDocumentProduct->selling_price_with_vat],
                        ['amount_sales_no_vat' => $orderDocumentProduct->amount_sales_no_vat],
                        ['amount_sales_with_vat' => $orderDocumentProduct->amount_sales_with_vat],
                    ])->exists();
            } else {
                $ordersExists = $invoice->getOrders()
                    ->andWhere(['product_id' => $orderDocumentProduct->product_id])
                    ->andWhere(['quantity' => $orderDocumentProduct->quantity])
                    ->andWhere(['product_title' => $orderDocumentProduct->product_title])
                    ->andWhere(['discount' => $orderDocumentProduct->discount])
                    ->andWhere(['and',
                        ['purchase_price_no_vat' => $orderDocumentProduct->purchase_price_no_vat],
                        ['purchase_price_with_vat' => $orderDocumentProduct->purchase_price_with_vat],
                        ['amount_purchase_no_vat' => $orderDocumentProduct->amount_purchase_no_vat],
                        ['amount_purchase_with_vat' => $orderDocumentProduct->amount_purchase_with_vat],
                    ])->exists();
            }

            if (!$ordersExists) {
                return true;
            }
        }

        return false;
    }

    /**
     * @return int|mixed
     */
    public function canCancel()
    {
        $statusArray = [
            InvoiceStatus::STATUS_PAYED_PARTIAL,
            InvoiceStatus::STATUS_PAYED,
        ];

        return $this->invoice && in_array($this->invoice->invoice_status_id, $statusArray) ? false : true;
    }

    /**
     * @inheritDoc
     */
    public function createNotification(): NotificationInterface
    {
        return new OrderDocumentNotification($this);
    }

    /**
     *
     */
    public function deleteRelated()
    {
        if ($this->id) {
            OrderDocumentInToOut::deleteAll(($this->type == Documents::IO_TYPE_OUT)
                ? ['out_order_document' => $this->id]
                : ['in_order_document' => $this->id]
            );
        }
    }

    /**
     * @return bool
     */
    public function hasOutOrderDocuments()
    {
        if ($this->id)
            return $this->type == Documents::IO_TYPE_IN
                && OrderDocumentInToOut::find()->where(['in_order_document' => $this->id])->exists();

        return false;
    }

    /**
     * @return bool
     */
    public function hasInOrderDocuments()
    {
        if ($this->id)
            return $this->type == Documents::IO_TYPE_OUT
                && OrderDocumentInToOut::find()->where(['out_order_document' => $this->id])->exists();

        return false;
    }

    // emulate invoice NDS attributes

    // @var
    public function getView_total_nds()
    {
        return $this->getViewTotalNds();
    }

    // @var
    public function getView_total_amount()
    {
        return $this->getViewTotalAmount();
    }

    public function getViewTotalNds()
    {
        $ret = 0;
        foreach ($this->orderDocumentProducts as $o) {
            $ret += ($this->type == Documents::IO_TYPE_OUT)
                ? $o->sale_tax
                : $o->purchase_tax;
        }

        return $ret;
    }

    public function getViewTotalAmount()
    {
        $ret = 0;
        foreach ($this->orderDocumentProducts as $o) {
            if ($this->nds_view_type_id == self::NDS_VIEW_OUT) {
                $ret += ($this->type == Documents::IO_TYPE_OUT)
                    ? $o->amount_sales_no_vat
                    : $o->amount_purchase_no_vat;
            } else {
                $ret += ($this->type == Documents::IO_TYPE_OUT)
                    ? $o->amount_sales_with_vat
                    : $o->amount_purchase_with_vat;
            }
        }

        return $ret;
    }
}
