<?php

namespace common\models\document;

use common\components\date\DateHelper;
use common\components\date\DatePickerFormatBehavior;
use common\components\helpers\ArrayHelper;
use common\components\helpers\Html;
use common\components\helpers\Month;
use common\components\pdf\PdfRenderer;
use common\components\TextHelper;
use common\models\Agreement;
use common\models\Company;
use common\models\Contractor;
use common\models\document\query\AgentReportQuery;
use common\models\document\status\InvoiceStatus;
use common\models\EmployeeCompany;
use common\models\file\File;
use common\models\TaxRate;
use frontend\models\Documents;
use frontend\models\log\Log;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\modules\documents\assets\DocumentPrintAsset;
use frontend\modules\documents\components\Message;
use frontend\modules\documents\forms\InvoiceSendForm;
use Yii;
use common\models\AgreementType;
use common\models\employee\EmployeeSignature;
use common\models\employee\Employee;
use common\models\document\status\AgentReportStatus;
use yii\behaviors\TimestampBehavior;
use yii\helpers\Url;

/**
 * This is the model class for table "agent_report".
 *
 * @property integer $id
 * @property string $uid
 * @property integer $type
 * @property integer $invoice_id
 * @property integer $created_at
 * @property integer $document_author_id
 * @property integer $status_out_id
 * @property integer $status_out_self_id
 * @property integer $status_out_updated_at
 * @property integer $status_out_author_id
 * @property string $document_date
 * @property integer $document_number
 *
 * @property integer $company_id
 * @property integer $agent_id
 * @property integer $agreement_id
 * @property integer $total_sum
 * @property integer $total_sum_without_nds
 * @property integer $total_nds
 * @property integer $has_invoice
 *
 * @property string $comment_internal
 * @property string $basis_name
 * @property string $basis_document_number
 * @property string $basis_document_date
 * @property integer $basis_document_type_id
 * @property integer $signed_by_employee_id
 * @property string $signed_by_name
 * @property integer $sign_document_type_id
 * @property string $sign_document_number
 * @property string $sign_document_date
 * @property integer $signature_id
 * @property integer $add_stamp
 * @property integer $has_file
 * @property string $pay_up_date
 *
 * @property AgreementType $basisDocumentType
 * @property EmployeeSignature $signature
 * @property DocumentType $signDocumentType
 * @property Employee $signedByEmployee
 * @property Contractor $agent
 * @property AgentReportStatus $statusOut
 * @property Employee $documentAuthor
 * @property Invoice $invoice
 * @property Employee $statusOutAuthor
 * @property AgentReportOrder[] $agentReportOrders
 * @property AgentReportOrder[] $orderAgentReports
 * @property Agreement $agreement
 */
class AgentReport extends AbstractDocument
{
    /**
     * @var string
     */
    public static $uploadDirectory = 'agent-report';

    /**
     * @var string
     */
    public $printablePrefix = 'Агентский отчет';

    /**
     * @var string
     */
    public $emailTemplate = 'agent-report';

    /**
     * @var string
     */
    public $urlPart = 'agent-report';

    /**
     * @var array
     */
    public $removedOrders = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'agent_report';
    }


    /**
     * @return AgentReportQuery
     */
    public static function find()
    {
        return new AgentReportQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type', 'invoice_id', 'created_at', 'document_author_id', 'status_out_id', 'status_out_self_id', 'status_out_updated_at', 'status_out_author_id', 'document_number', 'agent_id', 'total_sum', 'total_sum_without_nds', 'total_nds', 'basis_document_type_id', 'signed_by_employee_id', 'sign_document_type_id', 'signature_id', 'add_stamp', 'has_file'], 'integer'],
            [['created_at', 'document_author_id', 'document_date', 'agent_id', 'pay_up_date'], 'required'],
            [['document_date', 'basis_document_date', 'sign_document_date', 'pay_up_date'], 'date', 'format' => 'php:Y-m-d'],
            [
                ['document_date'],
                function ($attribute, $params) {
                    if ($oldModel = AgentReport::findOne(['id' => $this->id])) {
                        $oldDate = date('Y-m', strtotime($oldModel->document_date));
                        $newDate = date('Y-m', strtotime($this->document_date));
                        @list($year, $month) = explode('-', $oldDate);

                        if ($oldDate != $newDate)
                            $this->addError($attribute, 'Значение «Дата» должно быть в периоде "' . Month::$monthFullRU[(int)$month].' '.$year.' года"');
                    }
                }
            ],
            [
                ['pay_up_date'],
                function ($attribute, $params) {
                    if ((new \DateTime($this->$attribute)) < (new \DateTime($this->document_date))) {
                        $this->addError($attribute, 'Значение «Оплатить до» не должно быть меньше даты отчета.');
                    }
                },
            ],
            [['has_invoice'], 'boolean'],
            [['comment_internal'], 'string'],
            [['uid'], 'string', 'max' => 5],
            [['basis_name'], 'string', 'max' => 255],
            [['basis_document_number', 'signed_by_name', 'sign_document_number'], 'string', 'max' => 50],
            [['basis_document_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => AgreementType::className(), 'targetAttribute' => ['basis_document_type_id' => 'id']],
            [['signature_id'], 'exist', 'skipOnError' => true, 'targetClass' => EmployeeSignature::className(), 'targetAttribute' => ['signature_id' => 'id']],
            [['sign_document_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => DocumentType::className(), 'targetAttribute' => ['sign_document_type_id' => 'id']],
            [['signed_by_employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['signed_by_employee_id' => 'id']],
            [['agent_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contractor::className(), 'targetAttribute' => ['agent_id' => 'id']],
            [['status_out_id'], 'exist', 'skipOnError' => true, 'targetClass' => AgentReportStatus::className(), 'targetAttribute' => ['status_out_id' => 'id']],
            [['status_out_self_id'], 'exist', 'skipOnError' => true, 'targetClass' => AgentReportStatus::className(), 'targetAttribute' => ['status_out_self_id' => 'id']],
            [['document_author_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['document_author_id' => 'id']],
            [['invoice_id'], 'exist', 'skipOnError' => true, 'targetClass' => Invoice::className(), 'targetAttribute' => ['invoice_id' => 'id']],
            [['status_out_author_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['status_out_author_id' => 'id']],
            [['agreement_id'], 'exist', 'skipOnError' => true, 'targetClass' => Agreement::className(), 'targetAttribute' => ['agreement_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uid' => 'Uid',
            'type' => 'Type',
            'invoice_id' => 'Invoice ID',
            'created_at' => 'Created At',
            'document_author_id' => 'Document Author ID',
            'status_out_id' => 'Статус',
            'status_out_self_id' => 'Статус до оплаты счета',
            'status_out_updated_at' => 'Status Out Updated At',
            'status_out_author_id' => 'Status Out Author ID',
            'document_date' => 'Дата документа',
            'document_number' => 'Номер документа',
            'agent_id' => 'Agent ID',
            'total_sum' => 'Сумма',
            'total_sum_without_nds' => 'Сумма без НДС',
            'total_nds' => 'НДС',
            'comment_internal' => 'Комментарий',
            'basis_name' => 'Basis Name',
            'basis_document_number' => 'Basis Document Number',
            'basis_document_date' => 'Basis Document Date',
            'basis_document_type_id' => 'Basis Document Type ID',
            'signed_by_employee_id' => 'Signed By Employee ID',
            'signed_by_name' => 'Signed By Name',
            'sign_document_type_id' => 'Sign Document Type ID',
            'sign_document_number' => 'Sign Document Number',
            'sign_document_date' => 'Sign Document Date',
            'signature_id' => 'Signature ID',
            'add_stamp' => 'Add Stamp',
            'has_file' => 'Has File',
            'pay_up_date' => 'Оплатить до'
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'documentDateBehavior' => [
                'class' => DatePickerFormatBehavior::className(),
                'attributes' => [
                    'document_date' => [
                        'message' => 'Дата документа указана неверно.',
                    ],
                    'pay_up_date' => [
                        'message' => 'Дата "Оплатить до" указана неверно.',
                    ],
                ],
            ],
            'documentCreatedAtBehavior' => [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
            ],
        ];
    }

    /**
     * NDS 20% always
     * @return int
     */
    public function getNdsRate() {
        return TaxRate::get(TaxRate::RATE_20)->rate * 100;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBasisDocumentType()
    {
        return $this->hasOne(AgreementType::className(), ['id' => 'basis_document_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreement()
    {
        return $this->hasOne(Agreement::className(), ['id' => 'agreement_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSignature()
    {
        return $this->hasOne(EmployeeSignature::className(), ['id' => 'signature_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSignDocumentType()
    {
        return $this->hasOne(DocumentType::className(), ['id' => 'sign_document_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSignedByEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'signed_by_employee_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgent()
    {
        return $this->hasOne(Contractor::className(), ['id' => 'agent_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusOut()
    {
        return $this->hasOne(AgentReportStatus::className(), ['id' => 'status_out_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->getStatusOut();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusSelf()
    {
        return $this->hasOne(AgentReportStatus::className(), ['id' => 'status_out_self_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentStatus()
    {
        return $this->getStatusOut();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentAuthor()
    {
        return $this->hasOne(Employee::className(), ['id' => 'document_author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(Invoice::className(), ['id' => 'invoice_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusOutAuthor()
    {
        return $this->hasOne(Employee::className(), ['id' => 'status_out_author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgentReportOrders()
    {
        return $this->hasMany(AgentReportOrder::className(), ['agent_report_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderAgentReports()
    {
        return $this->getAgentReportOrders();
    }

    /**
     * Full document number
     *
     * @return string
     */
    public function getFullNumber()
    {
        return $this->document_number;
    }

    /**
     * @inheritdoc
     */
    public function getFile()
    {
        return $this->hasOne(File::className(), ['owner_id' => 'id'])
            ->onCondition(['owner_model' => AgentReport::className(),]);
    }

    /**
     * @inheritdoc
     */
    public function getFiles()
    {
        return $this->hasMany(File::className(), ['owner_id' => 'id'])
            ->onCondition([File::tableName() . '.owner_model' => AgentReport::className(),]);
    }

    /**
     * @inheritdoc
     */
    public function getScanDocuments()
    {
        return $this->hasMany(ScanDocument::className(), ['owner_id' => 'id'])
            ->onCondition([ScanDocument::tableName() . '.owner_model' => AgentReport::className(),]);
    }

    /**
     * Get formatted log message from Log model
     * @param Log $log
     * @return string
     */
    public function getLogMessage(Log $log)
    {
        $title = 'Агентский отчет №';

        $ioType = $log->getModelAttributeNew('type');
        if ($ioType == Documents::IO_TYPE_IN) {
            $title .= $log->getModelAttributeNew('document_number');
        } else {
            $title .= $log->getModelAttributeNew('document_number');
        }

        $link = Html::a($title, [
            '/documents/agent-report/view',
            'type' => $log->getModelAttributeNew('type'),
            'id' => $log->getModelAttributeNew('id'),
        ]);

        switch ($log->log_event_id) {
            case LogEvent::LOG_EVENT_CREATE:
                return $link . ' был создан.';
            case LogEvent::LOG_EVENT_DELETE:
                return $title . ' был удалён.';
            case LogEvent::LOG_EVENT_UPDATE:
                return $link . ' был изменен.';
            case LogEvent::LOG_EVENT_UPDATE_STATUS:
                return $link . ' статус "' . AgentReportStatus::findOne($log->getModelAttributeNew('status_out_id'))->name . '"';
            default:
                return $log->message;
        }
    }

    /**
     * @return null|string
     */
    public function getBasisName()
    {
        if ($this->basis_document_number && $this->basis_document_date) {
            return ($this->agreementType ? $this->agreementType->name : 'Договор') .
                ' № ' . Html::encode($this->basis_document_number) . ' от ' .
                DateHelper::format($this->basis_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
        }

        return null;
    }


    /**
     * @return string
     */
    public function getEmailSubject()
    {
        $date = $this->getReadableDate();
        $name = $this->agent->getShortName();

        return 'Агентский отчет за ' . $date . ' ' . $name;
    }

    /**
     * @return string
     */
    public function getEmailText()
    {
        $date = $this->getReadableDate();
        $name = $this->agent->getShortName();

        $text = <<<EMAIL_TEXT
Здравствуйте!

Агентский отчет за {$date}
{$name}

EMAIL_TEXT;

        return $text;
    }

    /**
     * @param $fileName string
     * @param $outModel AgentReport
     * @param $destination string
     * @return PdfRenderer
     */
    public static function getRenderer($fileName, $outModel, $destination = PdfRenderer::DESTINATION_FILE)
    {
        $renderer = new PdfRenderer([
            'view' => '@documents/views/agent-report/pdf-view',
            'params' => array_merge([
                'model' => $outModel,
                'message' => new Message(Documents::DOCUMENT_AGENT_REPORT, $outModel->type),
                'ioType' => $outModel->type,
                'addStamp' => false,
            ]),

            'destination' => $destination,
            'filename' => $fileName,
            'displayMode' => PdfRenderer::DISPLAY_MODE_FULLPAGE,
            'format' => 'A4-P',
        ]);
        \Yii::$app->controller->view->params['asset'] = DocumentPrintAsset::className();

        return $renderer;
    }

    public function loadEmailFiles()
    {
        $files = [];

        $emailFile = new EmailFile();
        $emailFile->company_id = $this->agent->company_id;
        $emailFile->file_name = str_replace(['\\', '/'], '_', 'Агентский_отчет');
        $emailFile->ext = 'pdf';
        $emailFile->mime = 'application/pdf';
        $emailFile->size = 0;
        $emailFile->type = EmailFile::TYPE_AGENT_REPORT;

        if ($emailFile->save()) {
            $path = $emailFile->getFilePath() . DIRECTORY_SEPARATOR . $emailFile->file_name;

            $size = file_put_contents($path, self::getRenderer(null, $this, PdfRenderer::DESTINATION_STRING)->output(false));

            $emailFile->size = ceil($size / 1024);

            if ($emailFile->save(true, ['size'])) {
                $files[] = [
                    'id' => $emailFile->id,
                    'name' => $emailFile->file_name,
                    'previewImg' => '<img src="/img/email/emailPdf.png" class="preview-img">',
                    'size' => $emailFile->size,
                    'deleteUrl' => Url::to(['/email/delete-email-file', 'id' => $emailFile->id]),
                    'downloadUrl' => Url::to(['/email/download-file', 'id' => $emailFile->id]),
                ];
            }
        }

        return $files;
    }

    /**
     * @return string
     */
    public function getPrintTitle($escape = false)
    {
        $date = $this->getReadableDate();
        $name = $this->agent->getShortName();
        $title = "Агентский отчет за {$date} {$name}";
        return ($escape) ? str_replace([' ', '"'], ['_'], $title) : $title;
    }

    /**
     * @return string
     */
    public function getPdfPrintTitle()
    {
        $date = $this->getReadableDate();
        $name = $this->agent->getShortName();
        return "Агентский отчет за {$date}<br/>{$name}";
    }

    /**
     * @return mixed
     */
    public function getPdfFileName()
    {
        $date = $this->getReadableDate();
        $name = $this->agent->getShortName();
        $filename = "Агентский_отчет_{$date}_{$name}.pdf";

        return str_replace([' ', '"', ',', '/', '\\'], ['_'], $filename);
    }

    public function getReadableDate()
    {
        $month = DateHelper::format($this->document_date, 'm', DateHelper::FORMAT_DATE);
        $year = DateHelper::format($this->document_date, 'Y', DateHelper::FORMAT_DATE);
        return mb_strtolower(ArrayHelper::getValue(Month::$monthFullRU, (int)$month, $month), 'utf-8') . ' ' . $year . ' г.';
    }

    public function getAgreementName()
    {
        return $this->agreement ?
            ($this->agreement->name .
                ' № ' . $this->agreement->fullNumber .
                ' от ' . DateHelper::format($this->agreement->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE)) : '';
    }


    /**
     * @param EmployeeCompany $sender
     * @param $toEmail
     * @param $type
     * @param $AgentReportIDs
     * @return bool
     * @throws \MpdfException
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public static function manySend(EmployeeCompany $sender, $toEmail, $type, $AgentReportIDs)
    {
        /* @var $models AgentReport[] */
        $models = [];
        $emailSubject = [];
        $companyID = null;
        $emailTemplate = null;
        $companyNameShort = null;
        if ($type == Documents::IO_TYPE_OUT) {
            foreach ($AgentReportIDs as $AgentReportID) {
                $AgentReport = is_object($AgentReportID) ? $AgentReportID : self::findOne($AgentReportID);
                if ($AgentReport->uid == null) {
                    $AgentReport->updateAttributes([
                        'uid' => self::generateUid(),
                    ]);
                }
                $models[] = $AgentReport;
                $emailSubject[] = 'Агентский отчет № ' . $AgentReport->fullNumber
                    . ' от ' . DateHelper::format($AgentReport->document_date, 'd.m.y', DateHelper::FORMAT_DATE);
                $companyID = $AgentReport->agent->company_id;
                $companyNameShort = $AgentReport->agent->company->getShortName();
            }
            $emailSubject = implode(', ', $emailSubject);
            $emailSubject .= (' от ' . $companyNameShort);
            $params = [
                'model' => $models,
                'employee' => $sender->employee,
                'employeeCompany' => $sender,
                'subject' => $emailSubject,
                'pixel' => [
                    'company_id' => $companyID,
                    'email' => is_array($toEmail) ? implode(',', $toEmail) : $toEmail,
                ],
            ];
            Yii::$app->mailer->htmlLayout = 'layouts/document-html';
            $message = Yii::$app->mailer->compose([
                'html' => 'system/agent-report/html',
                'text' => 'system/agent-report/text',
            ], $params)
                ->setFrom([Yii::$app->params['emailList']['docs'] => $sender->getFio()])
                ->setReplyTo([$sender->email => $sender->getFio(true)])
                ->setSubject($emailSubject)
                ->setTo($toEmail);
            foreach ($models as $model) {
                $message->attachContent(AgentReport::getRenderer(null, $model, PdfRenderer::DESTINATION_STRING)->output(false), [
                    'fileName' => $model->pdfFileName,
                    'contentType' => 'application/pdf',
                ]);
            }
            if ($message->send()) {
                foreach ($models as $model) {
                    InvoiceSendForm::setAgentReportSendStatus($model);
                }

                return true;
            }
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function setStatusPrinted()
    {
        if ($this && in_array($this->status_out_id, [AgentReportStatus::STATUS_CREATED, AgentReportStatus::STATUS_PRINTED])) {
            LogHelper::save($this, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function ($model) {
                $model->status_out_id = AgentReportStatus::STATUS_PRINTED;
                $model->status_out_updated_at = time();
                $model->status_out_author_id = \Yii::$app->user->id;

                return $model->save(true, ['status_out_id', 'status_out_updated_at', 'status_out_author_id']);
            });
        }
    }

    /**
     * @inheritdoc
     */
    public function ordersLoad($ordersData)
    {
        $orderArray = [];
        $this->removedOrders = ArrayHelper::index($this->agentReportOrders, 'id');
        if (is_array($ordersData)) {
            foreach ($ordersData as $data) {
                $orderId = ArrayHelper::remove($data, 'id');
                $order = AgentReportOrder::findOne([
                    'agent_report_id' => $this->id,
                    'id' => $orderId,
                ]);
                unset($this->removedOrders[$orderId]);
                $order->load($data, '');
                $this->calculateOrder($order);
                $orderArray[$orderId] = $order;
            }
        }

        $this->populateRelation('agentReportOrders', $orderArray);
    }

    public function calculateOrder(&$order)
    {
        /** @var AgentReportOrder $order */
        $order->agent_sum = round($order->payments_sum * $order->agent_percent / 100);
    }

    public function calculateTotals() {
        $hasNds = $this->agent->taxation_system == Contractor::WITH_NDS;
        $ndsRate = $this->getNdsRate();
        $precision = 2;
        $total = 0;
        $totalNds = 0;
        foreach ($this->agentReportOrders as $order) {
            $total += $order->agent_sum;
        }

        if ($hasNds) {
            $totalNds = round($total * $ndsRate / (100 + $ndsRate));
        }

        $total = round($total);
        $this->total_sum = $total;
        $this->total_nds = $totalNds;
        $this->total_sum_without_nds = $total - $totalNds;

        return true;
    }

    public function setStatusByInvoice() {
        $invoice = $this->invoice;
        if ($invoice) {
            // set invoice status
            if (in_array($invoice->invoice_status_id, [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_PAYED_PARTIAL, InvoiceStatus::STATUS_OVERDUE])) {
                switch ($invoice->invoice_status_id) {
                    case InvoiceStatus::STATUS_PAYED:
                        $reportPaidStatus = AgentReportStatus::STATUS_PAYED;
                        break;
                    case InvoiceStatus::STATUS_PAYED_PARTIAL:
                        $reportPaidStatus = AgentReportStatus::STATUS_PAYED_PARTIAL;
                        break;
                    case InvoiceStatus::STATUS_OVERDUE:
                        $reportPaidStatus = AgentReportStatus::STATUS_OVERDUE;
                        break;
                    default:
                        $reportPaidStatus = $this->status_out_id;
                        break;
                }

                if ($reportPaidStatus == $this->status_out_id)
                    return;

                if (in_array($this->status_out_id, [AgentReportStatus::STATUS_CREATED, AgentReportStatus::STATUS_SEND, AgentReportStatus::STATUS_PRINTED])) {
                    $this->updateAttributes(['status_out_self_id' => $this->status_out_id]);
                }

                LogHelper::save($this, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function ($model) use ($reportPaidStatus, $invoice) {
                    $model->status_out_id = $reportPaidStatus;
                    $model->status_out_updated_at = $invoice->invoice_status_updated_at;
                    $model->status_out_author_id = $invoice->invoice_status_author_id;

                    return $model->save(true, ['status_out_id', 'status_out_updated_at', 'status_out_author_id']);
                });

            } elseif (!in_array($invoice->invoice_status_id, [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_PAYED_PARTIAL, InvoiceStatus::STATUS_OVERDUE])
                    && in_array($this->status_out_id, [AgentReportStatus::STATUS_PAYED, AgentReportStatus::STATUS_PAYED_PARTIAL, AgentReportStatus::STATUS_OVERDUE])) {

                $this->unsetStatusByInvoice();
            }
        } else {
            // unset invoice status
            if (in_array($this->status_out_id, [AgentReportStatus::STATUS_PAYED, AgentReportStatus::STATUS_PAYED_PARTIAL, AgentReportStatus::STATUS_OVERDUE])) {
                $this->unsetStatusByInvoice();
            }
        }

        return;
    }

    public function unsetStatusByInvoice()
    {
        $this->status_out_id = $this->status_out_self_id;
        $this->status_out_updated_at = time();
        $this->status_out_author_id = \Yii::$app->user->id;
        $this->updateAttributes(['status_out_id', 'status_out_updated_at', 'status_out_author_id']);

        return;
    }
}