<?php

namespace common\models\document;


use common\models\cash\CashFlowsBase;
use yii\base\InvalidParamException;
use yii\web\NotFoundHttpException;

/**
 * @class InvoiceFlowCalculator
 * @package common\models\document
 */
class InvoiceFlowCalculator
{
    /**
     * @var int Сумма за счёт
     */
    public $amount = 0;
    /**
     * @var int Сумма оплат
     */
    public $payed = 0;
    /**
     * @var int Остаток по счёту
     */
    public $rest = 0;

    /**
     * @var Invoice
     */
    private $_invoice;

    /**
     * @var CashFlowsBase[]
     */
    private $_cashFlowArray;

    /**
     * @param Invoice $invoice
     */
    public function __construct(Invoice $invoice)
    {
        if ($invoice === null) {
            throw new NotFoundHttpException('Счёт не найден.');
        }

        $this->invoice = $invoice;
        $this->amount = $this->invoice->total_amount_with_nds;

        /* @var CashFlowsBase[] $cashFlowArray */
        $this->_cashFlowArray = array_merge($this->invoice->cashBankFlows,
            $this->invoice->cashEmoneyFlows,
            $this->invoice->cashOrderFlows
        );

        $this->_calc();
    }

    /**
     * @return Invoice
     */
    public function getInvoice()
    {
        return $this->_invoice;
    }

    /**
     * @param Invoice $invoice
     */
    public function setInvoice(Invoice $invoice)
    {
        $this->_invoice = $invoice;
    }

    /**
     * Check if invoice is completely payed
     * @return bool
     */
    public function isPayed()
    {
        return $this->payed >= $this->amount;
    }

    /**
     * Check if flow amount isn't greater than rest to pay
     * @param int $amount
     * @return bool
     */
    public function canAddFlow($amount)
    {
        return ($this->payed + $amount) <= $this->amount;
    }

    /**
     * Check if flow exists in current invoice.
     * @param CashFlowsBase $flow
     * @return bool
     * @internal param int $amount
     */
    public function canDeleteFlow(CashFlowsBase $flow)
    {
        return $this->_findFlow($flow) !== null;
    }

    /**
     * Adds flow to array and recalculates
     * @param CashFlowsBase $flow
     * @return bool
     */
    public function addFlow(CashFlowsBase $flow)
    {
        $amount = $flow->getAmountForInvoice($this->invoice);

        if ($this->canAddFlow($amount)) {
            $this->_cashFlowArray[] = $flow;
            $this->payed += $amount;

            return true;
        }

        return false;
    }

    /**
     * Adds flow to array and recalculates
     * @param CashFlowsBase $flow
     * @return bool
     */
    public function deleteFlow(CashFlowsBase $flow)
    {
        if ($this->canDeleteFlow($flow)) {
            if (($foundFlow = $this->_findFlow($flow)) !== null) {
                unset($this->_cashFlowArray[$foundFlow[0]]);
            } else {
                throw new InvalidParamException('Операция не найдена.');
            }

            $this->payed -= $flow->getAmountForInvoice($this->invoice);

            return true;
        }

        return false;
    }

    /**
     * Searches for flow in flow array.
     * If flow is new record - search by link, else - id.
     * @param CashFlowsBase $flow
     * @return array|null
     */
    private function _findFlow(CashFlowsBase $flow)
    {
        foreach ($this->_cashFlowArray as $key => $cashFlow) {
            if (($flow->isNewRecord && $flow === $cashFlow) // same object
                || (!$flow->isNewRecord && !$cashFlow->isNewRecord && $flow->id == $cashFlow->id) // same id
            ) {
                return [$key, $flow,];
            }
        }

        return null;
    }

    /**
     * Prepares invoice
     */
    private function _calc()
    {
        foreach ($this->_cashFlowArray as $cashFlow) {
            $this->payed += $cashFlow->getAmountForInvoice($this->invoice);
        }

        $this->rest = $this->amount - $this->payed;
    }
}
