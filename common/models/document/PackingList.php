<?php

namespace common\models\document;

use common\components\excel\Excel;
use common\components\pdf\PdfRenderer;
use common\components\TextHelper;
use common\components\date\DateHelper;
use common\components\date\DatePickerFormatBehavior;
use common\models\Agreement;
use common\models\AgreementType;
use common\models\Contractor;
use common\models\document\DocumentType;
use common\models\document\query\PackingListQuery;
use common\models\document\status\PackingListStatus;
use common\models\employee\Employee;
use common\models\EmployeeCompany;
use common\models\file\File;
use common\models\product\Product;
use frontend\models\Documents;
use frontend\models\log\Log;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\modules\analytics\models\PlanCashContractor;
use frontend\modules\documents\assets\DocumentPrintAsset;
use frontend\modules\documents\components\InvoiceHelper;
use frontend\modules\documents\components\Message;
use frontend\modules\documents\forms\InvoiceSendForm;
use frontend\modules\export\models\export\Export;
use frontend\modules\export\models\one_c\OneCExport;
use Yii;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This is the model class for table "packing_list".
 *
 * @property string $created_at
 * @property integer $type
 * @property integer $company_id
 * @property integer $contractor_id
 * @property integer $invoice_id
 * @property integer $status_out_id
 * @property string $status_out_updated_at
 * @property integer $status_out_author_id
 * @property string $document_number
 * @property string $document_additional_number
 * @property string $waybill_number
 * @property string $waybill_date
 * @property string $basis_name
 * @property string $basis_document_number
 * @property string $basis_document_date
 * @property integer $consignor_id
 * @property integer $consignee_id
 * @property integer $orders_sum
 * @property integer $order_nds
 * @property integer $proxy_number
 * @property integer $proxy_date
 * @property boolean $contractor_address
 * @property string $given_out_position
 * @property string $given_out_fio
 * @property integer $remaining_amount
 * @property string $cargo_accepted_position
 * @property string $cargo_accepted_fullname
 * @property string $cargo_received_position
 * @property string $cargo_received_fullname
 *
 * @property Invoice $invoice
 * @property status\PackingListStatus $statusOut
 * @property Employee $statusOutAuthor
 * @property OrderPackingList[] $orderPackingLists
 * @property Contractor $consignee
 * @property Order[] $orders
 *
 * @property string $fullNumber
 */
class PackingList extends AbstractDocument implements BasisDocumentInterface
{
    use BasisDocumentTrait;

    /**
     * @var string
     */
    public static $uploadDirectory = 'packing_list';

    /**
     * @var string
     */
    public $printablePrefix = 'Товарная-накладная';
    public $shortPrefix = 'ТН';

    /**
     * @var string
     */
    public $urlPart = 'packing-list';

    /**
     * @var string
     */
    public $agreement;
    /**
     * @var string
     */
    public $payment_date;

    const CONTRACTOR_ADDRESS_LEGAL = 0;
    const CONTRACTOR_ADDRESS_ACTUAL = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'packing_list';
    }

    /**
     * @return PackingListQuery
     */
    public static function find()
    {
        return new PackingListQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'documentDateBehavior' => [
                'class' => DatePickerFormatBehavior::className(),
                'attributes' => [
                    'waybill_date' => [
                        'message' => 'Дата транспортной накладной указана неверно.',
                    ],
                    'basis_document_date' => [
                        'message' => 'Дата основания указана неверно.',
                    ],
                    'proxy_date' => [
                        'message' => 'Дата доверенности указана неверно.',
                    ],
                ],
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $outClosure = function (PackingList $model) {
            return $model->type == Documents::IO_TYPE_OUT;
        };
        $inClosure = function (PackingList $model) {
            return $model->type == Documents::IO_TYPE_IN;
        };

        return ArrayHelper::merge(parent::rules(), [
            [
                ['orderPackingLists'], 'required',
                'message' => 'ТН не может быть пустой.',
            ],
            [
                ['document_date'], 'required',
            ],

            /** COMMON */
            [['basis_document_number', 'document_additional_number', 'agreement',], 'string'],

            // type depending
            // out
            [
                ['consignor_id', 'consignee_id'], 'exist', 'when' => $outClosure,
                'targetClass' => Contractor::class,
                'targetAttribute' => 'id',
                'filter' => [
                    'or',
                    ['company_id' => null],
                    ['company_id' => $this->invoice->company_id],
                ]
            ],
            [['document_additional_number'], 'string', 'max' => 45, 'when' => $outClosure,],
            [['waybill_number'], 'string', 'max' => 45, 'when' => $outClosure,],
            [['waybill_date', 'basis_document_date', 'proxy_date'], 'safe'],
            [['proxy_number'], 'string', 'max' => 50, 'when' => $outClosure],
            [['given_out_position', 'given_out_fio'], 'string', 'max' => 255, 'when' => $outClosure],
            [['document_number',], 'integer', 'when' => $outClosure],
            [['document_number'], 'outDocumentNumberValidator', 'when' => $outClosure],
            [['contractor_address'], 'integer'],
            // in
            [['basis_name'], 'string', 'max' => 255],
            [['document_number'], 'inDocumentNumberValidator', 'when' => $inClosure],
            [['orders_sum', 'order_nds'], 'filter', 'filter' => function ($value) {
                return round($value);
            }],
            [['orders_sum', 'order_nds'], 'integer'],
            // invoice fields
            [['company_id', 'contractor_id'], 'safe'],
            [['remaining_amount'], 'integer'],
            // cargo responsible person
            [['cargo_accepted_position', 'cargo_accepted_fullname',
              'cargo_received_position', 'cargo_received_fullname'], 'safe'],
        ]);
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function inDocumentNumberValidator($attribute, $params)
    {
        $query = $this->invoice->getPackingLists()
            ->andWhere(['!=', 'id', $this->id])
            ->andWhere([
                'document_number' => $this->$attribute,
            ]);

        if (!empty($this->document_date)) {
            $year = date('Y', strtotime($this->document_date));
            $query->andWhere([
                'between',
                'document_date',
                $year.'-01-01',
                $year.'-12-31',
            ]);
        }

        if ($this->document_additional_number) {
            $query->andWhere(['document_additional_number' => $this->document_additional_number]);
        } else {
            $query->andWhere(['or',
                ['document_additional_number' => ''],
                ['document_additional_number' => null],
            ]);
        }

        if ($query->exists()) {
            $this->addError($attribute, 'Такой номер уже существует, одинаковые номера не допустимы');
        }
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function outDocumentNumberValidator($attribute, $params)
    {
        $isNumberExists = function ($number) {
            $query1 = self::find()->alias('doc')->joinWith(['invoice'])
                ->andWhere(['not', ['doc.id' => $this->id]])
                ->andWhere([
                    'invoice.is_deleted' => false,
                    'invoice.company_id' => $this->invoice->company_id,
                    'doc.type' => $this->type,
                    'doc.document_number' => $number,
                ]);
            $query2 = Act::find()->alias('doc')->joinWith(['invoice'])
                ->andWhere(['!=', 'doc.invoice_id', $this->invoice->id])
                ->andWhere([
                    'invoice.is_deleted' => false,
                    'invoice.company_id' => $this->invoice->company_id,
                    'doc.type' => $this->type,
                    'doc.document_number' => $number,
                ]);
            $query3 = InvoiceFacture::find()->alias('doc')->joinWith(['invoice'])
                ->andWhere(['!=', 'doc.invoice_id', $this->invoice->id])
                ->andWhere([
                    'invoice.is_deleted' => false,
                    'invoice.company_id' => $this->invoice->company_id,
                    'doc.type' => $this->type,
                    'doc.document_number' => $number,
                ]);
            $query4 = Upd::find()->alias('doc')->joinWith(['invoice'])
                ->andWhere(['!=', 'doc.invoice_id', $this->invoice->id])
                ->andWhere([
                    'invoice.is_deleted' => false,
                    'invoice.company_id' => $this->invoice->company_id,
                    'doc.type' => $this->type,
                    'doc.document_number' => $number,
                ]);

            if ($this->document_date) {
                $year = date('Y', strtotime($this->document_date));
                $query1->andWhere(['between', 'doc.document_date', $year.'-01-01', $year.'-12-31']);
                $query2->andWhere(['between', 'doc.document_date', $year.'-01-01', $year.'-12-31']);
                $query3->andWhere(['between', 'doc.document_date', $year.'-01-01', $year.'-12-31']);
                $query4->andWhere(['between', 'doc.document_date', $year.'-01-01', $year.'-12-31']);
            }
            if ($this->document_additional_number) {
                $query1->andWhere(['doc.document_additional_number' => $this->document_additional_number]);
                $query2->andWhere(['doc.document_additional_number' => $this->document_additional_number]);
                $query3->andWhere(['doc.document_additional_number' => $this->document_additional_number]);
                $query4->andWhere(['doc.document_additional_number' => $this->document_additional_number]);
            } else {
                $query1->andWhere(['or',
                    ['doc.document_additional_number' => ''],
                    ['doc.document_additional_number' => null],
                ]);
                $query2->andWhere(['or',
                    ['doc.document_additional_number' => ''],
                    ['doc.document_additional_number' => null],
                ]);
                $query3->andWhere(['or',
                    ['doc.document_additional_number' => ''],
                    ['doc.document_additional_number' => null],
                ]);
                $query4->andWhere(['or',
                    ['doc.document_additional_number' => ''],
                    ['doc.document_additional_number' => null],
                ]);
            }

            return $query1->exists() || $query2->exists() || $query3->exists() || $query4->exists();
        };

        if ($isNumberExists($this->$attribute)) {
            if ($this->findFreeNumber) {
                $nextNumber = $this->$attribute;
                do {
                    $nextNumber++;
                } while ($isNumberExists($nextNumber));
            } else {
                $nextNumber = null;
            }
            $this->addError($attribute, "Номер {$this->$attribute} занят. Укажите свободный номер {$nextNumber}");
        }
    }

    /**
     * Generate document number
     */
    public function newDocumentNumber()
    {
        $this->document_number = $this->document_additional_number = null;

        $query1 = $this->invoice->getActs()
            ->select(new Expression('[[document_number]], IF([[document_additional_number]] = "", NULL, [[document_additional_number]]) [[additional_number]]'));
        $query2 = $this->invoice->getInvoiceFactures()
            ->select(new Expression('[[document_number]], IF([[document_additional_number]] = "", NULL, [[document_additional_number]]) [[additional_number]]'));
        $query3 = $this->invoice->getUpds()
            ->select(new Expression('[[document_number]], IF([[document_additional_number]] = "", NULL, [[document_additional_number]]) [[additional_number]]'));
        $query = (new Query)
            ->select(['document_number', 'additional_number'])
            ->from(['t' => $query1->union($query2)->union($query3)])
            ->orderBy([
                'document_number' => SORT_ASC,
                'additional_number' => SORT_ASC,
            ]);

        if ($rowArray = $query->all()) {
            foreach ($rowArray as $row) {
                $this->document_number = $row['document_number'];
                $this->document_additional_number = $row['additional_number'];
                if ($this->validate(['document_number'])) {
                    break;
                } else {
                    $this->document_number = $this->document_additional_number = null;
                }
            }
        }

        if (!$this->document_number) {
            $this->document_number = $this->getMaxDocumentNumber();
            do {
                $this->document_number++;
                $this->document_number .= '';
            } while (!$this->validate(['document_number']));
        }
    }

    /**
     * @return boolean
     */
    public function getIsEditableNumber()
    {
        if ($this->isNewRecord || $this->type == Documents::IO_TYPE_IN) {
            return true;
        } else {
            $query1 = $this->invoice->getActs()->andWhere(['document_number' => $this->document_number]);
            $query2 = $this->invoice->getInvoiceFactures()->andWhere(['document_number' => $this->document_number]);
            $query3 = $this->invoice->getUpds()->andWhere(['document_number' => $this->document_number]);
            if ($this->document_additional_number) {
                $query1->andWhere(['document_additional_number' => $this->document_additional_number]);
                $query2->andWhere(['document_additional_number' => $this->document_additional_number]);
                $query3->andWhere(['document_additional_number' => $this->document_additional_number]);
            } else {
                $query1->andWhere(['or',
                    ['document_additional_number' => ''],
                    ['document_additional_number' => null],
                ]);
                $query2->andWhere(['or',
                    ['document_additional_number' => ''],
                    ['document_additional_number' => null],
                ]);
                $query3->andWhere(['or',
                    ['document_additional_number' => ''],
                    ['document_additional_number' => null],
                ]);
            }

            return (!$query1->exists() && !$query2->exists() && !$query3->exists());
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Creation Date',
            'document_author_id' => 'Author',
            'invoice_id' => 'Invoice ID',
            'status_out_id' => 'Packing List Status ID',
            'status_out_updated_at' => 'Status Date',
            'status_out_author_id' => 'Status Author',
            'document_date' => 'Дата документа',
            'document_additional_number' => 'Additional Number',
            'waybill_number' => 'Номер транспортной накладной',
            'waybill_date' => 'Дата транспортной накладной',
            'basis_name' => 'Основание',
            'basis_document_number' => 'Номер основания',
            'basis_document_date' => 'Дата основания',
            'consignor_id' => 'Грузоотправитель',
            'consignee_id' => 'Грузополучатель',
            'document_number' => 'Номер документа',
            'given_out_position' => 'Должность',
            'given_out_fio' => 'ФИО',
            'payment_date' => 'Дата оплаты',
            'cargo_accepted_position' => 'Груз принял (должность)',
            'cargo_accepted_fullname' => 'Груз принял (ФИО)',
            'cargo_received_position' => 'Груз получил (должность)',
            'cargo_received_fullname' => 'Груз получил (ФИО)',
        ];
    }


    /**
     * @return OrderPackingList
     */
    public function createOrderPackingList()
    {
        $model = new OrderPackingList();
        $model->packing_list_id = $this->id;


        return $model;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusOut()
    {
        return $this->hasOne(status\PackingListStatus::className(), ['id' => 'status_out_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentStatus()
    {
        return $this->getStatusOut();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(Invoice::className(), ['id' => 'invoice_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(status\PackingListStatus::className(), ['id' => 'status_out_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusOutAuthor()
    {
        return $this->hasOne(Employee::className(), ['id' => 'status_out_author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreementType()
    {
        return $this->hasOne(AgreementType::className(), ['id' => 'basis_document_type_id']);
    }

    /**
     * @inheritdoc
     */
    public function getFile()
    {
        return $this->hasOne(File::className(), ['owner_id' => 'id'])
            ->onCondition(['owner_model' => PackingList::className(),]);
    }

    /**
     * @inheritdoc
     */
    public function getFiles()
    {
        return $this->hasMany(File::className(), ['owner_id' => 'id'])
            ->onCondition([File::tableName() . '.owner_model' => PackingList::className(),]);
    }

    /**
     * @inheritdoc
     */
    public function getScanDocuments()
    {
        return $this->hasMany(ScanDocument::className(), ['owner_id' => 'id'])
            ->onCondition([ScanDocument::tableName() . '.owner_model' => PackingList::className(),]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPackingListGoodsCancellations()
    {
        return $this->hasMany(PackingListGoodsCancellation::className(), ['document_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGoodsCancellations()
    {
        return $this->hasMany(GoodsCancellation::className(), ['id' => 'cancellation_id'])->via('packingListGoodsCancellations');
    }

    /**
     * @inheritdoc
     */
    public function ordersLoad($ordersData)
    {
        $orderArray = [];
        $this->removedOrders = ArrayHelper::index($this->orderPackingLists, 'order_id');
        if (is_array($ordersData)) {
            foreach ($ordersData as $data) {
                $orderId = ArrayHelper::remove($data, 'order_id');
                $order = $orderId ? Order::findOne(['id' => $orderId, 'invoice_id' => $this->invoice_id]) : null;
                if ($order !== null) {
                    $orderPackingList = OrderPackingList::findOne([
                        'order_id' => $order->id,
                        'packing_list_id' => $this->id,
                    ]);
                    if ($orderPackingList === null) {
                        $orderPackingList = new OrderPackingList([
                            'order_id' => $order->id,
                            'packing_list_id' => $this->id,
                            'product_id' => $order->product_id,
                        ]);
                    } else {
                        unset($this->removedOrders[$orderId]);
                    }
                    $orderPackingList->load($data, '');

                    $orderArray[$orderId] = $orderPackingList;
                }
            }
        }

        $this->populateRelation('orderPackingLists', $orderArray);
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        $agreement = explode('&', $this->agreement);
        if (is_array($agreement)) {
            $this->basis_name = isset($agreement[0]) ? $agreement[0] : null;
            $this->basis_document_number = isset($agreement[1]) ? $agreement[1] : null;
            $this->basis_document_date = isset($agreement[2]) ? $agreement[2] : null;
            $this->basis_document_type_id = isset($agreement[3]) ? $agreement[3] : null;
        } else {
            $this->basis_name = null;
            $this->basis_document_number = null;
            $this->basis_document_date = null;
            $this->basis_document_type_id = null;
        }

        return parent::beforeValidate();
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                if ($this->document_date === null) {
                    $this->document_date = date(DateHelper::FORMAT_DATE);
                }
                if (empty($this->document_number)) {
                    $this->newDocumentNumber();
                    $this->ordinal_document_number = $this->document_number;
                }
            }

            if ($this->type == Documents::IO_TYPE_OUT) {
                if ($insert) {
                    $this->status_out_id = status\PackingListStatus::STATUS_CREATED;
                }

                if ($insert || $this->isAttributeChanged('status_out_id')) {
                    if (Yii::$app->id == 'app-frontend') {
                        $userId = Yii::$app->user->id;
                    } else {
                        $userId = $this->invoice->document_author_id;
                        if ($this->document_author_id === null) {
                            $this->document_author_id = $userId;
                        }
                    }

                    $this->status_out_updated_at = time();
                    $this->status_out_author_id = $userId;
                }
            }

            $this->object_guid = OneCExport::generateGUID();

            if ($this->isAttributeChanged('status_out_id') && $this->status_out_id == status\PackingListStatus::STATUS_REJECTED) {
                foreach ($this->orderPackingLists as $order) {
                    $order->resetProductCount();
                }
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            foreach ($this->getOrderPackingLists()->all() as $order) {
                if (!$order->delete()) {
                    return false;
                }
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if (!$insert) {
            InvoiceHelper::checkForPackingList($this->invoice_id);

            if ($modelArray = $this->invoice->invoiceFactures) {
                foreach ($modelArray as $model) {
                    if ($this->getTotalAmountWithNds() == $model->getTotalAmountWithNds()) {
                        $model->updateAttributes([
                            'consignor_id' => $this->consignor_id,
                            'consignee_id' => $this->consignee_id,
                        ]);
                    }
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();

        InvoiceHelper::checkForPackingList($this->invoice_id);

        // remove plan flow
        if (in_array($this->invoice->payment_limit_rule, [Invoice::PAYMENT_LIMIT_RULE_MIXED, Invoice::PAYMENT_LIMIT_RULE_DOCUMENT])) {
            if ($invoiceWithoutThisDoc = Invoice::findOne($this->invoice_id)) {
                PlanCashContractor::updateFlowByInvoice($invoiceWithoutThisDoc, Documents::DOCUMENT_PACKING_LIST);
            }
        }
    }

    /**
     *
     */
    public function generateDocumentNumber()
    {
        $year = date('Y', strtotime($this->document_date));
        $productionTypeArray = explode(', ', $this->invoice->production_type);
        $lastPackingListId = self::find()
            ->byCompany(Yii::$app->user->identity->company->id)
            ->byIOType($this->type)
            ->andWhere([
                'between',
                self::tableName().'.document_date',
                $year.'-01-01',
                $year.'-12-31',
            ])
            ->max('ordinal_document_number');
        $lastActId = Act::find()
            ->byCompany(Yii::$app->user->identity->company->id)
            ->byIOType($this->type)
            ->andWhere([
                'between',
                Act::tableName().'.document_date',
                $year.'-01-01',
                $year.'-12-31',
            ])
            ->max('ordinal_document_number');
        if (in_array(Product::PRODUCTION_TYPE_GOODS, $productionTypeArray)) {
            if ($this->invoice->act !== null) {
                $this->ordinal_document_number = $lastPackingListId + 1;
                $this->document_number = $lastPackingListId + 1;
                $this->document_date = $this->invoice->act->document_date;
                $this->document_additional_number = $this->invoice->act->document_additional_number;
            } else {
                if ($lastActId > $lastPackingListId) {
                    $documentNumber = $lastActId + 1;
                } else {
                    $documentNumber = $lastPackingListId + 1;
                }
                $number = $this->checkOnEmptyDocumentNumber($documentNumber);
                $this->document_number = $number;
                $this->ordinal_document_number = $number;
            }
        } else {
            $number = $this->checkOnEmptyDocumentNumber($lastActId + 1);
            $this->document_number = $number;
            $this->ordinal_document_number = $number;
        }
    }

    /**
     * @param $number
     * @return mixed
     */
    public function checkOnEmptyDocumentNumber($number)
    {
        $forbiddenId = [];
        $query = Act::find()
            ->byCompany(Yii::$app->user->identity->company->id)
            ->byIOType($this->type);
        $subQuery = new Query();
        $subQuery->select([
            Invoice::tableName() . '.id',
            Invoice::tableName() . '.production_type',
        ]);
        $subQuery->from(Invoice::tableName());
        $query->leftJoin(['u' => $subQuery], 'u.id = ' . Act::tableName() . '.invoice_id')->andWhere(['u.production_type' => Product::PRODUCTION_TYPE_SERVICE . ', ' . Product::PRODUCTION_TYPE_GOODS]);
        $actArray = $query->all();
        foreach ($actArray as $act) {
            /* @var PackingList $act */
            if ($act->invoice->packingList == null) {
                $forbiddenId[] = $act->ordinal_document_number;
            }
        }
        if (in_array($number, $forbiddenId)) {
            $number = $this->checkOnEmptyDocumentNumber($number + 1);
        }

        return $number;
    }

    /**
     * @inheritdoc
     */
    public function getFullNumber()
    {
        if ($this->document_number !== null) {
            $documentNumber = $this->document_number;
        } else {
            $documentNumber = $this->invoice->ordinal_document_number;
        }

        if ($this->type == Documents::IO_TYPE_OUT) {
            if ($this->invoice && $this->invoice->is_additional_number_before) {
                $documentNumber = $this->document_additional_number . $documentNumber;
            } else {
                $documentNumber = $documentNumber . $this->document_additional_number;
            }
        }

        return $documentNumber;
    }

    /**
     * @return string
     */
    public function getPrintTitle()
    {
        return str_replace([' ', '"'], ['_'],
            $this->printablePrefix . '_№' . $this->getFullNumber() . '_' .
            date_format(date_create($this->document_date), 'd.m.Y') . '_' .
            $this->invoice->company_name_short
        );
    }

    /**
     * Get formatted log message from Log model
     * @param Log $log
     * @return string
     */
    public function getLogMessage(Log $log)
    {
        $title = 'Товарная накладная №';
        $invoiceText = '';
        $invoice = Invoice::findOne($log->getModelAttributeNew('invoice_id'));

        $ioType = $log->getModelAttributeNew('type');
        if ($ioType == Documents::IO_TYPE_IN) {
            $title .= $log->getModelAttributeNew('document_number');
        } else {
            $title .= $log->getModelAttributeNew('document_number') . ' ' . $log->getModelAttributeNew('document_additional_number');
        }

        $link = Html::a($title, [
            '/documents/packing-list/view',
            'type' => $log->getModelAttributeNew('type'),
            'id' => $log->getModelAttributeNew('id'),
        ]);

        if ($log->log_event_id == LogEvent::LOG_EVENT_RESPONSIBLE) {
            $message = $link . ', изменен ответственный';
            if ($this->invoice->responsible) {
                $message .= ' на '.$this->invoice->responsible->getFio(true).'.';
            } else {
                $message .= '.';
            }

            return $message;
        }

        if ($invoice) {
            $amount = TextHelper::invoiceMoneyFormat($invoice->total_amount_with_nds, 2) . ' руб.';
            $contractorName = $invoice->contractor ? ', ' . $invoice->contractor->getTitle(true) : '';
            $invoiceText = ', ' . $amount . $contractorName . ',';
        }

        switch ($log->log_event_id) {
            case LogEvent::LOG_EVENT_CREATE:
                return $link . $invoiceText . ' была создана.';
            case LogEvent::LOG_EVENT_DELETE:
                return $title . $invoiceText . ' была удалена.';
            case LogEvent::LOG_EVENT_UPDATE:
                return $link . $invoiceText . ' была изменена.';
            case LogEvent::LOG_EVENT_UPDATE_STATUS:
                return $link . $invoiceText . ' статус "' . PackingListStatus::findOne($log->getModelAttributeNew('status_out_id'))->name . '"';
            default:
                return $log->message;
        }
    }

    /**
     * @param bool|false $withNds
     * @return int
     */
    public function getTotalAmount($withNds = false)
    {
        return $withNds ? $this->getTotalAmountWithNds() : $this->getTotalAmountNoNds();
    }

    /**
     * @return int
     */
    public function getTotalNds()
    {
        return $this->getTotalPLNds();
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        $orders = $this->invoice->getOrdersByProductType(Product::PRODUCTION_TYPE_GOODS);
        $quantity = 0;

        foreach ($orders as $order) {
            $quantity += $order->quantity;
        }

        return $quantity;
    }

    /**
     * @return array
     */
    public function getConsignorArray()
    {
        $query = Contractor::getSorted()
            ->byCompany($this->invoice->company_id)
            ->byDeleted()
            ->andWhere(['or', [Contractor::tableName() . '.is_seller' => 1], [Contractor::tableName() . '.is_customer' => 1]])
            ->andWhere(['!=', Contractor::tableName() . '.id', $this->invoice->contractor_id])
            ->all();

        return ArrayHelper::merge(['' => 'Он же'], ArrayHelper::map($query, 'id', 'shortName'));
    }

    /**
     * @return null|static
     */
    public function getConsignor()
    {
        return Contractor::findOne([$this->consignor_id]);
    }

    /**
     * @return null|static
     */
    public function getConsignee()
    {
        return Contractor::findOne([$this->consignee_id]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['id' => 'order_id'])->viaTable('order_packing_list', ['packing_list_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderPackingLists()
    {
        return $this->hasMany(OrderPackingList::className(), ['packing_list_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOwnOrders()
    {
        return $this->getOrderPackingLists();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSignBasisDocument()
    {
        return $this->hasOne(DocumentType::className(), ['id' => 'sign_document_type_id']);
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();

        $this->agreement = "{$this->basis_name}&{$this->basis_document_number}&{$this->basis_document_date}&{$this->basis_document_type_id}";
    }

    /**
     * @inheritdoc
     */
    public function basisDropDownList($list = [])
    {
        $list[$this->invoice->getListItemValue()] = $this->invoice->getListItemName();
        $agreementArray = $this->invoice->contractor->getAgreements()->orderBy(['document_date' => SORT_DESC])->all();
        foreach ($agreementArray as $agreement) {
            $list[$agreement->getListItemValue(false)] = $agreement->getListItemName();
        }

        return $list;
    }

    /**
     * @param $fileName
     * @param $outPackingList
     * @return PdfRenderer
     */
    public static function getRenderer($fileName, $outPackingList, $destination = PdfRenderer::DESTINATION_FILE)
    {
        $renderer = new PdfRenderer([
            'view' => '@documents/views/packing-list/pdf-view',
            'params' => array_merge([
                'model' => $outPackingList,
                'message' => new Message(Documents::DOCUMENT_PACKING_LIST, $outPackingList->type),
                'ioType' => $outPackingList->type,
                'addStamp' => true,
            ]),

            'destination' => $destination,
            'filename' => $fileName,
            'displayMode' => PdfRenderer::DISPLAY_MODE_FULLPAGE,
            'format' => 'A4-L',
        ]);
        \Yii::$app->controller->view->params['asset'] = DocumentPrintAsset::className();

        return $renderer;
    }

    /**
     * @inheritdoc
     */
    public function setStatusPrinted()
    {
        if ($this && in_array($this->status_out_id, [PackingListStatus::STATUS_CREATED, PackingListStatus::STATUS_PRINTED])) {
            LogHelper::save($this, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function ($model) {
                $model->status_out_id = PackingListStatus::STATUS_PRINTED;
                $model->status_out_updated_at = time();
                $model->status_out_author_id = \Yii::$app->user->id;

                return $model->save(true, ['status_out_id', 'status_out_updated_at', 'status_out_author_id']);
            });
        }
    }

    /**
     * @return integer
     */
    public function getTotalAmountWithNds()
    {
        $amount = 0;
        foreach ($this->orderPackingLists as $key => $value) {
            $amount += $value->amountWithNds;
        }

        return $amount;
    }

    /**
     * @return integer
     */
    public function getTotalAmountNoNds()
    {
        $amount = 0;
        foreach ($this->orderPackingLists as $key => $value) {
            $amount += $value->amountNoNds;
        }

        return $amount;
    }

    /**
     * @return integer
     */
    public function getTotalPLNds()
    {
        return $this->totalAmountWithNds - $this->totalAmountNoNds;
    }

    /**
     * @param $period
     * @param $packingLists
     */
    public static function generateXlsTable($period, $packingLists)
    {
        $excel = new Excel();

        $fileName = 'Товарные_накладные_' . DateHelper::format($period['from'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) .
            '-' . DateHelper::format($period['to'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $packingLists,
            'title' => 'Товарные накладные',
            'columns' => [
                [
                    'attribute' => 'document_date',
                    'value' => function (PackingList $data) {
                        return DateHelper::format($data->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                    },
                ],
                [
                    'attribute' => 'document_number',
                    'value' => function (PackingList $data) {
                        return $data->fullNumber;
                    },
                ],
                [
                    'attribute' => 'invoice.total_amount_with_nds',
                    'styleArray' => [
                        'numberformat' => [
                            'code' => '# ##0.00',
                        ]
                    ],
                    'dataType' => 'n',
                    'value' => function ($model) {
                        return bcdiv($model->totalAmountWithNds, 100, 2);
                    },
                ],
                [
                    'attribute' => 'contractor_id',
                    'value' => function (PackingList $data) {
                        return $data->invoice->contractor_name_short;
                    },
                ],
                [
                    'attribute' => 'status_out_id',
                    'value' => function (PackingList $data) {
                        if ($data->type == 2)
                            return ($data->statusOut) ? $data->statusOut->name : '';
                        else
                            return $data->is_original ? 'Оригинал' : 'Скан';
                    },
                ],
                [
                    'attribute' => 'invoice_document_number',
                    'value' => function (PackingList $data) {
                        return $data->invoice->fullNumber;
                    },
                ],
                [
                    'attribute' => 'nomenclature',
                    'wrap' => true,
                    'value' => function (PackingList $model) {

                        $list = [];
                        foreach ($model->orders as $order) {
                            $unitName = $order->unit ? $order->unit->name : Product::DEFAULT_VALUE;
                            $list[] = $order->product_title . ' ' .
                                TextHelper::numberFormat($order->quantity, 2) . ' ' .
                                $unitName . ' x ' .
                                TextHelper::invoiceMoneyFormat($order->view_price_one, 2) . 'р. = ' .
                                TextHelper::invoiceMoneyFormat($order->view_total_amount, 2) . 'р.';
                        }

                        return $list ? implode("\r", $list) : 'НЕТ';
                    },
                ]
            ],
            'headers' => [
                'document_date' => 'Дата т/н',
                'document_number' => '№ т/н',
                'invoice.total_amount_with_nds' => 'Сумма',
                'contractor_id' => 'Контрагент',
                'status_out_id' => 'Статус',
                'invoice_document_number' => 'Счёт №',
                'nomenclature' => 'Номенклатурная часть'
            ],
            'format' => 'Xlsx',
            'fileName' => $fileName,
        ]);
    }

    /**
     * @return bool|string
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     */
    public function generateOneCFile()
    {
        $exportModel = new Export(['company_id' => Yii::$app->user->identity->company->id]);

        $oneCExport = new OneCExport($exportModel);
        if ($oneCExport->findDataObjectPackingList($this->id, Documents::IO_TYPE_IN)) {
            $path = Yii::getAlias('@frontend/runtime/export') . DIRECTORY_SEPARATOR . $oneCExport->createExportFile();
            Yii::$app->db->createCommand("DELETE FROM `export` WHERE `export`.`id` = {$exportModel->id}")->execute();

            return $path;
        }
        return false;
    }

    /**
     * @return string
     */
    public function getOneCFileName()
    {
        return 'Товарная накладная №' . $this->getFullNumber() . '.xml';
    }

    /**
     * @return string
     */
    public function getEmailSubject()
    {
        return 'Товарная накладная № ' . $this->fullNumber
            . ' от ' . DateHelper::format($this->document_date, 'd.m.y', DateHelper::FORMAT_DATE)
            . ' от ' . $this->invoice->company_name_short;
    }

    /**
     * @return string
     */
    public function getEmailText()
    {
        $document_date = DateHelper::format($this->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
        $sum = TextHelper::invoiceMoneyFormat($this->totalAmountWithNds, 2);

        $text = <<<EMAIL_TEXT
Здравствуйте!

Товарная накладная № {$this->fullNumber} от {$document_date} на сумму {$sum} р.
EMAIL_TEXT;

        return $text;
    }

    /**
     * @return array
     * @throws \MpdfException
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function loadEmailFiles()
    {
        $files = [];

        $emailFile = new EmailFile();
        $emailFile->company_id = $this->invoice->company_id;
        $emailFile->file_name = str_replace(['\\', '/'], '_', $this->pdfFileName);
        $emailFile->ext = 'pdf';
        $emailFile->mime = 'application/pdf';
        $emailFile->size = 0;
        $emailFile->type = EmailFile::TYPE_PACKING_LIST_DOCUMENT;
        if ($emailFile->save()) {
            $path = $emailFile->getFilePath() . DIRECTORY_SEPARATOR . $emailFile->file_name;
            $size = file_put_contents($path, self::getRenderer(null, $this, PdfRenderer::DESTINATION_STRING)->output(false));
            $emailFile->size = ceil($size / 1024);
            if ($emailFile->save(true, ['size'])) {
                $files[] = [
                    'id' => $emailFile->id,
                    'name' => $emailFile->file_name,
                    'previewImg' => '<img src="/img/email/emailPdf.png" class="preview-img">',
                    'size' => $emailFile->size,
                    'deleteUrl' => Url::to(['/email/delete-email-file', 'id' => $emailFile->id]),
                    'downloadUrl' => Url::to(['/email/download-file', 'id' => $emailFile->id]),
                ];
            }
        }

        $oneCFilePath = $this->generateOneCFile();
        if ($oneCFilePath) {
            $fileName = 'Для загрузки в 1С ' . $this->getOneCFileName();
            $emailFile = new EmailFile();
            $emailFile->company_id = $this->invoice->company_id;
            $emailFile->file_name = $fileName;
            $emailFile->ext = 'xml';
            $emailFile->mime = 'application/xml';
            $emailFile->size = ceil(filesize($oneCFilePath) / 1024);
            $emailFile->type = EmailFile::TYPE_ONE_C_FILE;
            if ($emailFile->save()) {
                $files[] = [
                    'id' => $emailFile->id,
                    'name' => $emailFile->file_name,
                    'previewImg' => '<img src="/img/email/emailUnknown.png" class="preview-img">',
                    'size' => $emailFile->size,
                    'deleteUrl' => Url::to(['/email/delete-email-file', 'id' => $emailFile->id]),
                ];
            }
        }

        return $files;
    }

    /**
     * @param EmployeeCompany $sender
     * @param $toEmail
     * @param $type
     * @param $packingListIDs
     * @return bool
     * @throws \MpdfException
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public static function manySend(EmployeeCompany $sender, $toEmail, $type, $packingListIDs)
    {
        /* @var $models PackingList[] */
        $models = [];
        $emailSubject = [];
        $companyID = null;
        $emailTemplate = null;
        $companyNameShort = null;
        if ($type == Documents::IO_TYPE_OUT) {
            foreach ($packingListIDs as $packingListID) {
                $packingList = is_object($packingListID) ? $packingListID : self::findOne($packingListID);
                if ($packingList->uid == null) {
                    $packingList->updateAttributes([
                        'uid' => self::generateUid(),
                    ]);
                }
                $models[] = $packingList;
                $emailSubject[] = 'Товарная накладная № ' . $packingList->fullNumber
                    . ' от ' . DateHelper::format($packingList->document_date, 'd.m.y', DateHelper::FORMAT_DATE);
                $companyID = $packingList->invoice->company_id;
                $companyNameShort = $packingList->invoice->company_name_short;
            }
            $emailSubject = implode(', ', $emailSubject);
            $emailSubject .= (' от ' . $companyNameShort);
            $params = [
                'model' => $models,
                'employee' => $sender->employee,
                'employeeCompany' => $sender,
                'subject' => $emailSubject,
                'pixel' => [
                    'company_id' => $companyID,
                    'email' => is_array($toEmail) ? implode(',', $toEmail) : $toEmail,
                ],
            ];
            Yii::$app->mailer->htmlLayout = 'layouts/document-html';
            $message = Yii::$app->mailer->compose([
                'html' => 'system/documents/packing-list-out/html',
                'text' => 'system/documents/packing-list-out/text',
            ], $params)
                ->setFrom([Yii::$app->params['emailList']['docs'] => $sender->getFio()])
                ->setReplyTo([$sender->email => $sender->getFio(true)])
                ->setSubject($emailSubject)
                ->setTo($toEmail);
            foreach ($models as $model) {
                $message->attachContent(PackingList::getRenderer(null, $model, PdfRenderer::DESTINATION_STRING)->output(false), [
                    'fileName' => $model->pdfFileName,
                    'contentType' => 'application/pdf',
                ]);
            }
            if ($message->send()) {
                foreach ($models as $model) {
                    InvoiceSendForm::setPackingListSendStatus($model);
                }

                return true;
            }
        }

        return false;
    }

    /**
     * @param $byProducts
     * @return int
     */
    public function getTotalPurchaseAmountOnDate($byProducts = true)
    {
        $id = $this->id;
        $table = 'packing_list';
        $byProducts = ($byProducts) ? 1 : 0;

        try {
            return (int)Yii::$app->db->createCommand("
                SELECT SUM(IFNULL(`purchase_amount`, 0)) 
                FROM `product_turnover` 
                WHERE `document_id` = {$id} AND `document_table` = '{$table}' AND `production_type` = '{$byProducts}'
                GROUP BY `document_id`
            ")->queryScalar();
        } catch (\Throwable $e) {}

        return 0;
    }
}
