<?php

namespace common\models\document;

use common\models\FilterInterface;
use common\models\Company;
use common\models\RepositoryCompanyTrait;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\DataProviderInterface;
use yii\data\Sort;
use yii\db\ActiveQueryInterface;
use yii\db\ActiveRecordInterface;

class IncomeItemRepository extends Model implements FilterInterface
{
    use RepositoryCompanyTrait;

    /**
     * @var string|null
     */
    public $name;

    /**
     * @param string $reason
     * @param Company $company
     * @return InvoiceIncomeItem
     */
    public static function createIncomeItem(string $reason, Company $company): InvoiceIncomeItem
    {
        return new InvoiceIncomeItem([
            'company_id' => $company->id,
            'name' => $reason,
        ]);
    }

    /**
     * @param string $reason
     * @param Company $company
     * @return ActiveRecordInterface|InvoiceIncomeItem|null
     */
    public static function findByReason(string $reason, Company $company): ?InvoiceIncomeItem
    {
        $repository = new static([
            'company' => $company,
            'name' => $reason,
        ]);

        return $repository->getQuery()->one();
    }

    /**
     * @inheritDoc
     */
    public function getDataProvider(): DataProviderInterface
    {
        return new ActiveDataProvider([
            'query' => $this->getQuery(),
            'sort' => $this->getSort(),
            'pagination' => false,
        ]);

    }

    /**
     * @return ActiveQueryInterface
     */
    public function getQuery(): ActiveQueryInterface
    {
        return InvoiceIncomeItem::find()->andFilterWhere([
            'company_id' => $this->company ? $this->company->id : null,
            'name' => $this->name,
        ]);
    }

    /**
     * @inheritDoc
     */
    public function getSort(): Sort
    {
        return new Sort(['defaultOrder' => ['name' => SORT_ASC, 'id' => SORT_ASC]]);
    }
}
