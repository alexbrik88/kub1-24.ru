<?php

namespace common\models\document;

use Yii;
use common\models\Company;

/**
 * This is the model class for table "invoice_essence".
 *
 * @property integer $id
 * @property integer $company_id
 * @property string $text
 * @property integer $is_checked
 * @property string $text_en
 * @property integer $is_checked_en
 *
 * @property Company $company
 */
class InvoiceEssence extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invoice_essence';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_checked', 'is_checked_en'], 'boolean'],
            [['text', 'text_en'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'text' => 'Text',
            'is_checked' => 'Is Checked',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
}
