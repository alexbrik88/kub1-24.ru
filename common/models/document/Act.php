<?php

namespace common\models\document;

use common\components\excel\Excel;
use common\components\pdf\PdfRenderer;
use common\components\TextHelper;
use common\components\date\DateHelper;
use common\components\helpers\ArrayHelper;
use common\models\AgreementType;
use common\models\Company;
use common\models\company\CompanyFirstEvent;
use common\models\Contractor;
use common\models\currency\Currency;
use common\models\document\query\ActQuery;
use common\models\document\status\ActStatus;
use common\models\EmployeeCompany;
use common\models\employee\Employee;
use common\models\file\File;
use common\models\product\Product;
use frontend\models\Documents;
use frontend\models\log\Log;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\modules\analytics\models\PlanCashContractor;
use frontend\modules\documents\assets\DocumentPrintAsset;
use frontend\modules\documents\components\InvoiceHelper;
use frontend\modules\documents\components\Message;
use frontend\modules\export\models\export\Export;
use frontend\modules\export\models\one_c\OneCExport;
use Yii;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This is the model class for table "act".
 *
 * @property string $created_at
 * @property integer $type
 * @property integer $company_id
 * @property integer $contractor_id
 * @property integer $invoice_id
 * @property integer $status_out_id
 * @property string $status_out_updated_at
 * @property integer $status_out_author_id
 * @property string $document_number
 * @property string $document_additional_number
 * @property integer $order_sum
 * @property integer $order_nds
 * @property integer $order_sum_without_nds
 * @property string $comment
 * @property string $comment_internal
 * @property integer $auto_created
 * @property string $object_guid
 * @property integer $remaining_amount
 *
 * @property status\ActStatus $statusOut
 * @property Employee $statusOutAuthor
 * @property Invoice $invoice
 * @property Invoice[] $invoices
 * @property Company $company
 * @property OrderAct[] $orderActs
 * @property Order[] $orders
 * @property InvoiceAct[] $invoiceActs
 * @property EmailFile[] $emailFiles
 *
 * @property string $fullNumber
 * @property string $totalNds
 * @property-read OrderAct[] $ownOrders
 */
class Act extends AbstractDocument implements BasisDocumentInterface
{
    use BasisDocumentTrait;

    /**
     * @var string
     */
    public static $uploadDirectory = 'act';

    /**
     * @var string
     */
    public $printablePrefix = 'Акт';
    public $shortPrefix = 'Акт';

    /**
     * @var string
     */
    public $emailTemplate = 'act-out';

    /**
     * @var string
     */
    public $urlPart = 'act';

    /**
     * @var string
     */
    public $agreement;

    /**
     * @var array
     */
    public $removedOrders = [];

    private $beforeDeleteInvoices = [];

    public $isCreateForSeveralInvoices;

    public $isGroupOwnOrdersByProduct;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'act';
    }


    /**
     * @return ActQuery
     */
    public static function find()
    {
        return new ActQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $outClosure = function (Act $model) {
            return $model->type == Documents::IO_TYPE_OUT;
        };
        $inClosure = function (Act $model) {
            return $model->type == Documents::IO_TYPE_IN;
        };

        return ArrayHelper::merge(parent::rules(), [
            /** INSERT */
            [
                ['orderActs'], 'required',
                'message' => 'Акт не может быть пустым.',
            ],
            [['document_date',], 'required'],
            [['invoice_id'], 'required', 'on' => 'insert',],
            [['invoice_id'], 'integer', 'on' => 'insert',],
            [['comment', 'agreement', 'comment_internal'], 'string'],
            [['comment', 'comment_internal'], 'trim'],
            /** COMMON */
            [['order_sum', 'order_nds', 'order_sum_without_nds'], 'filter', 'filter' => function ($value) {
                return round($value);
            }],
            [['order_sum', 'order_nds', 'order_sum_without_nds'], 'integer'],
            // type depending
            // out
            [['document_additional_number'], 'string', 'max' => 45, 'when' => $outClosure,],
            [['document_number',], 'integer', 'when' => $outClosure],
            ['document_number', 'outDocumentNumberValidator', 'when' => $outClosure],
            [['status_out_id'], 'integer', 'when' => $outClosure,],
            // in
            [['document_number',], 'inDocumentNumberValidator', 'when' => $inClosure,],
            // invoice fields
            [['company_id', 'contractor_id'], 'safe'],
            [['remaining_amount'], 'integer']
        ]);
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function inDocumentNumberValidator($attribute, $params)
    {
        $query = $this->invoice->getActs()
            ->andWhere(['!=', 'id', $this->id])
            ->andWhere([
                'document_number' => $this->$attribute,
            ]);

        if (!empty($this->document_date)) {
            $year = date('Y', strtotime($this->document_date));
            $query->andWhere([
                'between',
                'document_date',
                $year.'-01-01',
                $year.'-12-31',
            ]);
        }

        if ($this->document_additional_number) {
            $query->andWhere(['document_additional_number' => $this->document_additional_number]);
        } else {
            $query->andWhere(['or',
                ['document_additional_number' => ''],
                ['document_additional_number' => null],
            ]);
        }

        if ($query->exists()) {
            $this->addError($attribute, 'Такой номер уже существует, одинаковые номера не допустимы');
        }
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function outDocumentNumberValidator($attribute, $params)
    {
        if ($this->isNumberExists($this->$attribute)) {
            if ($this->findFreeNumber) {
                $nextNumber = $this->$attribute;
                do {
                    $nextNumber++;
                } while ($this->isNumberExists($nextNumber));
            } else {
                $nextNumber = null;
            }
            $this->addError($attribute, "Номер {$this->$attribute} занят. Укажите свободный номер {$nextNumber}");
        }
    }

    /**
     * @param $number
     * @return bool
     */
    public function isNumberExists($number)
    {
        $query1 = self::find()->alias('doc')->joinWith(['invoice'])
            ->andWhere(['not', ['doc.id' => $this->id]])
            ->andWhere([
                'invoice.is_deleted' => false,
                'invoice.company_id' => $this->invoice->company_id,
                'doc.type' => $this->type,
                'doc.document_number' => $number,
            ]);
        $query2 = PackingList::find()->alias('doc')->joinWith(['invoice'])
            ->andWhere(['!=', 'doc.invoice_id', $this->invoice->id])
            ->andWhere([
                'invoice.is_deleted' => false,
                'invoice.company_id' => $this->invoice->company_id,
                'doc.type' => $this->type,
                'doc.document_number' => $number,
            ]);
        $query3 = InvoiceFacture::find()->alias('doc')->joinWith(['invoice'])
            ->andWhere(['!=', 'doc.invoice_id', $this->invoice->id])
            ->andWhere([
                'invoice.is_deleted' => false,
                'invoice.company_id' => $this->invoice->company_id,
                'doc.type' => $this->type,
                'doc.document_number' => $number,
            ]);
        $query4 = Upd::find()->alias('doc')->joinWith(['invoice'])
            ->andWhere(['!=', 'doc.invoice_id', $this->invoice->id])
            ->andWhere([
                'invoice.is_deleted' => false,
                'invoice.company_id' => $this->invoice->company_id,
                'doc.type' => $this->type,
                'doc.document_number' => $number,
            ]);

        if ($this->document_date) {
            $year = date_create($this->document_date)->format('Y');
            $query1->andWhere(['between', 'doc.document_date', $year.'-01-01', $year.'-12-31']);
            $query2->andWhere(['between', 'doc.document_date', $year.'-01-01', $year.'-12-31']);
            $query3->andWhere(['between', 'doc.document_date', $year.'-01-01', $year.'-12-31']);
            $query4->andWhere(['between', 'doc.document_date', $year.'-01-01', $year.'-12-31']);
        }
        if ($this->document_additional_number) {
            $query1->andWhere(['doc.document_additional_number' => $this->document_additional_number]);
            $query2->andWhere(['doc.document_additional_number' => $this->document_additional_number]);
            $query3->andWhere(['doc.document_additional_number' => $this->document_additional_number]);
            $query4->andWhere(['doc.document_additional_number' => $this->document_additional_number]);
        } else {
            $query1->andWhere(['or',
                ['doc.document_additional_number' => ''],
                ['doc.document_additional_number' => null],
            ]);
            $query2->andWhere(['or',
                ['doc.document_additional_number' => ''],
                ['doc.document_additional_number' => null],
            ]);
            $query3->andWhere(['or',
                ['doc.document_additional_number' => ''],
                ['doc.document_additional_number' => null],
            ]);
            $query4->andWhere(['or',
                ['doc.document_additional_number' => ''],
                ['doc.document_additional_number' => null],
            ]);
        }

        //return $query1->exists() || $query2->exists() || $query3->exists() || $query4->exists();
        return $query1->select('doc.id')->union($query2->select('doc.id'))->union($query3->select('doc.id'))->union($query4->select('doc.id'))->exists();
    }

    /**
     * Generate document number
     */
    public function newDocumentNumber()
    {
        $this->document_number = $this->document_additional_number = null;

        if ($this->type == Documents::IO_TYPE_IN && $this->invoice->is_subscribe_invoice) {
            $acts = ArrayHelper::getValue($this->invoice, 'payment.outInvoice.acts');
            if ($acts) {
                $act = reset($acts);
                $this->document_number = $act->document_number;
                $this->document_additional_number = $act->document_additional_number;

                return;
            }
        }

        $query1 = $this->invoice->getPackingLists()
            ->select(new Expression('[[document_number]], IF([[document_additional_number]] = "", NULL, [[document_additional_number]]) [[additional_number]]'));
        $query2 = $this->invoice->getInvoiceFactures()
            ->select(new Expression('[[document_number]], IF([[document_additional_number]] = "", NULL, [[document_additional_number]]) [[additional_number]]'));
        $query3 = $this->invoice->getUpds()
            ->select(new Expression('[[document_number]], IF([[document_additional_number]] = "", NULL, [[document_additional_number]]) [[additional_number]]'));
        $query = (new Query)
            ->select(['document_number', 'additional_number'])
            ->from(['t' => $query1->union($query2)->union($query3)])
            ->orderBy([
                'document_number' => SORT_ASC,
                'additional_number' => SORT_ASC,
            ]);
        if ($rowArray = $query->all()) {
            foreach ($rowArray as $row) {
                $this->document_number = $row['document_number'];
                $this->document_additional_number = $row['additional_number'];
                if ($this->validate(['document_number'])) {
                    break;
                } else {
                    $this->document_number = $this->document_additional_number = null;
                }
            }
        }

        if (!$this->document_number || $this->isNumberExists($this->document_number)) {
            $this->document_number = $this->getMaxDocumentNumber();
            do {
                $this->document_number++;
                $this->document_number .= '';
            } while (!$this->validate(['document_number']));
        }
    }

    /**
     * @return boolean
     */
    public function getIsEditableNumber()
    {
        if ($this->isNewRecord || $this->type == Documents::IO_TYPE_IN) {
            return true;
        } else {
            $query1 = $this->invoice->getPackingLists()->andWhere(['document_number' => $this->document_number]);
            $query2 = $this->invoice->getInvoiceFactures()->andWhere(['document_number' => $this->document_number]);
            $query3 = $this->invoice->getUpds()->andWhere(['document_number' => $this->document_number]);
            if ($this->document_additional_number) {
                $query1->andWhere(['document_additional_number' => $this->document_additional_number]);
                $query2->andWhere(['document_additional_number' => $this->document_additional_number]);
                $query3->andWhere(['document_additional_number' => $this->document_additional_number]);
            } else {
                $query1->andWhere(['or',
                    ['document_additional_number' => ''],
                    ['document_additional_number' => null],
                ]);
                $query2->andWhere(['or',
                    ['document_additional_number' => ''],
                    ['document_additional_number' => null],
                ]);
                $query3->andWhere(['or',
                    ['document_additional_number' => ''],
                    ['document_additional_number' => null],
                ]);
            }

            return (!$query1->exists() && !$query2->exists() && !$query3->exists());
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Io Type',
            'created_at' => 'Created At',
            'document_author_id' => 'Document Author ID',
            'invoice_id' => 'Invoice ID',
            'status_out_id' => 'Status Out ID',
            'status_out_updated_at' => 'Status Out Updated At',
            'status_out_author_id' => 'Status Out Author ID',
            'document_date' => 'Дата документа',
            'document_number' => 'Номер документа',
            'document_additional_number' => 'Document Additional Number',
            'comment_internal' => 'Комментарий',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(Invoice::className(), ['id' => 'invoice_id'])->viaTable('invoice_act', ['act_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceActs()
    {
        return $this->hasMany(InvoiceAct::className(), ['act_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoices()
    {
        return $this->hasMany(Invoice::className(), ['id' => 'invoice_id'])->viaTable('invoice_act', ['act_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusOut()
    {
        return $this->hasOne(status\ActStatus::className(), ['id' => 'status_out_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentStatus()
    {
        return $this->getStatusOut();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusOutAuthor()
    {
        return $this->hasOne(Employee::className(), ['id' => 'status_out_author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreementType()
    {
        return $this->hasOne(AgreementType::className(), ['id' => 'basis_document_type_id']);
    }

    /**
     * @inheritdoc
     */
    public function getFile()
    {
        return $this->hasOne(File::className(), ['owner_id' => 'id'])
            ->onCondition(['owner_model' => Act::className(),]);
    }

    /**
     * @inheritdoc
     */
    public function getFiles()
    {
        return $this->hasMany(File::className(), ['owner_id' => 'id'])
            ->onCondition([File::tableName() . '.owner_model' => Act::className(),]);
    }

    /**
     * @inheritdoc
     */
    public function getScanDocuments()
    {
        return $this->hasMany(ScanDocument::className(), ['owner_id' => 'id'])
            ->onCondition([ScanDocument::tableName() . '.owner_model' => Act::className(),]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActGoodsCancellations()
    {
        return $this->hasMany(ActGoodsCancellation::className(), ['document_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGoodsCancellations()
    {
        return $this->hasMany(GoodsCancellation::className(), ['id' => 'cancellation_id'])->via('actGoodsCancellations');
    }

    /**
     * @inheritdoc
     */
    public function ordersLoad($ordersData)
    {
        $orderArray = [];
        $this->removedOrders = ArrayHelper::index($this->orderActs, 'order_id');
        $this->surchargeInvoiceOrders = [];
        $invoices = ArrayHelper::getColumn($this->invoices, 'id');
        if (is_array($ordersData)) {
            foreach ($ordersData as $data) {
                $orderId = ArrayHelper::remove($data, 'order_id');
                /* @var $orderInvoice Order */
                $orderInvoice = $orderId ? Order::findOne(['id' => $orderId, 'invoice_id' => $invoices]) : null;
                if ($orderInvoice !== null) {
                    $orderAct = OrderAct::findOne([
                        'act_id' => $this->id,
                        'order_id' => $orderId,
                    ]);
                    if ($orderAct === null) {
                        $orderAct = new OrderAct([
                            'act_id' => $this->id,
                            'invoice_id' => $orderInvoice->invoice_id,
                            'order_id' => $orderInvoice->id,
                            'product_id' => $orderInvoice->product_id,
                        ]);
                    } else {
                        unset($this->removedOrders[$orderId]);
                    }
                    $orderAct->load($data, '');

                    $quantity = $data['quantity'];
                    $availableQuantity = $orderAct::availableQuantity($orderInvoice, $this->id);
                    if ($quantity > $availableQuantity) {
                        $this->surchargeInvoiceOrders[$orderId] = $quantity - $availableQuantity;
                    }

                    $orderArray[$orderId] = $orderAct;
                }
            }
        }

        $this->populateRelation('orderActs', $orderArray);
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        $agreement = explode('&', $this->agreement);
        if (is_array($agreement)) {
            $this->basis_document_name = isset($agreement[0]) ? $agreement[0] : null;
            $this->basis_document_number = isset($agreement[1]) ? $agreement[1] : null;
            $this->basis_document_date = isset($agreement[2]) ? $agreement[2] : null;
            $this->basis_document_type_id = isset($agreement[3]) ? $agreement[3] : null;
        } else {
            $this->basis_document_name = null;
            $this->basis_document_number = null;
            $this->basis_document_date = null;
            $this->basis_document_type_id = null;
        }

        return parent::beforeValidate();
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        /* @var $invoice Invoice */
        $invoice = $this->invoice;
        if (parent::beforeSave($insert) && $invoice) {
            if ($insert) {
                if ($this->document_date === null) {
                    $this->document_date = date(DateHelper::FORMAT_DATE);
                }
                if (empty($this->document_number)) {
                    $this->newDocumentNumber();
                    $this->ordinal_document_number = $this->document_number;
                }
                $this->ordinal_document_number = substr(strval(intval($this->ordinal_document_number)), -9);
                if ($invoice->company->actEssence && $invoice->company->actEssence->is_checked) {
                    $this->comment = $invoice->company->actEssence->text;
                } else {
                    $this->comment = 'Вышеперечисленные услуги выполнены полностью и в срок. Заказчик претензий по объему, качеству и срокам оказания услуг не имеет.';
                }
            }

            if ($this->type == Documents::IO_TYPE_OUT) {
                if ($insert) {
                    $this->status_out_id = status\ActStatus::STATUS_CREATED;
                }

                if ($insert || $this->isAttributeChanged('status_out_id')) {
                    $this->status_out_updated_at = time();
                    $this->status_out_author_id = $invoice->is_subscribe_invoice ?
                        $invoice->company->getEmployeeChief()->id :
                        (Yii::$app->request->isConsoleRequest ? $invoice->invoice_status_author_id : Yii::$app->user->id);
                }
            }

            $this->object_guid = OneCExport::generateGUID();

            return true;
        } else {
            return false;
        }
    }

    /**
     * @return bool
     * @throws \yii\db\StaleObjectException
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $this->beforeDeleteInvoices = $this->invoices;
            foreach ($this->getOrderActs()->all() as $order) {
                if (!$order->delete()) {
                    return false;
                }
            }
            foreach ($this->emailFiles as $emailFile) {
                if (!$emailFile->delete()) {
                    return false;
                }
            }
            foreach ($this->goodsCancellations as $item) {
                $this->unlink('goodsCancellations', $item, true);
                if (!LogHelper::delete($item, LogEntityType::TYPE_DOCUMENT)) {
                    return false;
                }
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();

        $this->agreement = "{$this->basis_document_name}&{$this->basis_document_number}&{$this->basis_document_date}&{$this->basis_document_type_id}";
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert) {
            if ($this->invoice_id && !$this->isCreateForSeveralInvoices) {
                $invoiceAct = new InvoiceAct();
                $invoiceAct->invoice_id = $this->invoice_id;
                $invoiceAct->act_id = $this->id;
                $invoiceAct->save();
            }
        }

        if (!$insert) {
            foreach ($this->invoices as $invoice) {
                InvoiceHelper::checkForAct($invoice->id, $this->invoices);
            }
        }
    }

    /**
     *
     */
    public function afterDelete()
    {
        parent::afterDelete();

        foreach ($this->beforeDeleteInvoices as $invoice) {
            InvoiceHelper::checkForAct($invoice->id,  $this->beforeDeleteInvoices);

            // remove plan flow
            if (in_array($invoice->payment_limit_rule, [Invoice::PAYMENT_LIMIT_RULE_MIXED, Invoice::PAYMENT_LIMIT_RULE_DOCUMENT])) {
                if ($invoiceWithoutThisDoc = Invoice::findOne($invoice->id)) {
                    PlanCashContractor::updateFlowByInvoice($invoiceWithoutThisDoc, Documents::DOCUMENT_ACT);
                }
            }
        }
    }

    /**
     * Generate document number
     */
    public function generateDocumentNumber()
    {
        $year = date('Y', strtotime($this->document_date));
        $productionTypeArray = explode(', ', $this->invoice->production_type);
        $lastActId = self::find()
            ->byCompany($this->invoice->company_id)
            ->byIOType($this->type)
            ->andWhere([
                'between',
                self::tableName().'.document_date',
                $year.'-01-01',
                $year.'-12-31',
            ])
            ->max('ordinal_document_number');
        $lastPackingListId = PackingList::find()
            ->byCompany($this->invoice->company_id)
            ->byIOType($this->type)
            ->andWhere([
                'between',
                PackingList::tableName().'.document_date',
                $year.'-01-01',
                $year.'-12-31',
            ])
            ->max('ordinal_document_number');
        if (in_array(Product::PRODUCTION_TYPE_GOODS, $productionTypeArray)) {
            if ($this->invoice->packingList !== null) {
                $this->ordinal_document_number = $this->invoice->packingList->ordinal_document_number;
                $this->document_number = $this->invoice->packingList->document_number;
                $this->document_date = $this->invoice->packingList->document_date;
                $this->document_additional_number = $this->invoice->packingList->document_additional_number;
            } else {
                if ($lastActId > $lastPackingListId) {
                    $documentNumber = $lastActId + 1;
                } else {
                    $documentNumber = $lastPackingListId + 1;
                }
                $number = $this->checkOnEmptyDocumentNumber($documentNumber);
                $this->document_number = $number;
                $this->ordinal_document_number = $number;
            }
        } else {
            $number = $this->checkOnEmptyDocumentNumber($lastActId + 1);
            $this->document_number = $number;
            $this->ordinal_document_number = $number;
        }
    }

    /**
     * @param $number
     * @return mixed
     */
    public function checkOnEmptyDocumentNumber($number)
    {
        $forbiddenId = [];
        $query = PackingList::find()
            ->byCompany($this->invoice->company_id)
            ->byIOType($this->type);
        $subQuery = new Query();
        $subQuery->select([
            Invoice::tableName() . '.id',
            Invoice::tableName() . '.production_type',
        ]);
        $subQuery->from(Invoice::tableName());
        $query->leftJoin(['u' => $subQuery], 'u.id = ' . PackingList::tableName() . '.invoice_id')->andWhere(['u.production_type' => Product::PRODUCTION_TYPE_SERVICE . ', ' . Product::PRODUCTION_TYPE_GOODS]);
        $packingListArray = $query->all();
        foreach ($packingListArray as $packingList) {
            /* @var PackingList $packingList */
            if ($packingList->invoice->act == null) {
                $forbiddenId[] = $packingList->ordinal_document_number;
            }
        }

        if (in_array($number, $forbiddenId)) {
            $number = $this->checkOnEmptyDocumentNumber($number + 1);
        }

        return $number;
    }

    /**
     * Full document number
     *
     * @return string
     */
    public function getFullNumber()
    {
        if ($this->document_number !== null) {
            $documentNumber = $this->document_number;
        } else {
            $documentNumber = $this->invoice->ordinal_document_number;
        }

        if ($this->type == Documents::IO_TYPE_OUT) {
            if ($this->invoice && $this->invoice->is_additional_number_before) {
                $documentNumber = $this->document_additional_number . $documentNumber;
            } else {
                $documentNumber = $documentNumber . $this->document_additional_number;
            }
        }

        return $documentNumber;
    }

    /**
     * @return string
     */
    public function getPrintTitle()
    {
        return str_replace([' ', '"'], ['_'],
            $this->printablePrefix . '_№' . $this->getFullNumber() . '_' .
            date_format(date_create($this->document_date), 'd.m.Y') . '_' .
            $this->invoice->company_name_short
        );
    }

    /**
     * Get formatted log message from Log model
     * @param Log $log
     * @return string
     */
    public function getLogMessage(Log $log)
    {
        $title = 'Акт №';
        $invoiceText = '';
        /* @var Invoice $invoice */
        $invoice = Invoice::findOne($log->getModelAttributeNew('invoice_id'));

        $ioType = $log->getModelAttributeNew('type');
        if ($ioType == Documents::IO_TYPE_IN) {
            $title .= $log->getModelAttributeNew('document_number');
        } else {
            $title .= $log->getModelAttributeNew('document_number') . ' ' . $log->getModelAttributeNew('document_additional_number');
        }

        $link = Html::a($title, [
            '/documents/act/view',
            'type' => $log->getModelAttributeNew('type'),
            'id' => $log->getModelAttributeNew('id'),
        ]);

        if ($log->log_event_id == LogEvent::LOG_EVENT_RESPONSIBLE) {
            $message = $link . ', изменен ответственный';
            if ($this->invoice->responsible) {
                $message .= ' на '.$this->invoice->responsible->getFio(true).'.';
            } else {
                $message .= '.';
            }

            return $message;
        }

        if ($invoice) {
            $amount = TextHelper::invoiceMoneyFormat($invoice->total_amount_with_nds, 2) . ' руб.';
            $contractorName = $invoice->contractor ? ', ' . $invoice->contractor->getTitle(true) : '';
            $invoiceText = ', ' . $amount . $contractorName . ',';
        }

        switch ($log->log_event_id) {
            case LogEvent::LOG_EVENT_CREATE:
                return $link . $invoiceText . ' был создан.';
            case LogEvent::LOG_EVENT_DELETE:
                return $title . $invoiceText . ' был удалён.';
            case LogEvent::LOG_EVENT_UPDATE:
                return $link . $invoiceText . ' был изменен.';
            case LogEvent::LOG_EVENT_UPDATE_STATUS:
                return $link . $invoiceText . ' статус "' . ActStatus::findOne($log->getModelAttributeNew('status_out_id'))->name . '"';
            default:
                return $log->message;
        }
    }

    /**
     * @param bool|false $withNds
     * @return int
     */
    public function getTotalAmount($withNds = false)
    {
        return $withNds ? $this->getTotalAmountWithNds() : $this->getTotalAmountNoNds();
    }

    /**
     * @return int
     */
    public function getTotalNds()
    {
        return $this->getTotalActNds();
    }

    /**
     * @param $fileName
     * @param $outAct
     * @return PdfRenderer
     */
    public static function getRenderer($fileName, $outAct, $destination = PdfRenderer::DESTINATION_FILE)
    {
        $renderer = new PdfRenderer([
            'view' => '@frontend/modules/documents/views/act/pdf-view',
            'params' => array_merge([
                'model' => $outAct,
                'message' => new Message(Documents::DOCUMENT_ACT, $outAct->type),
                'ioType' => $outAct->type,
                'addStamp' => $outAct->invoice->company->pdf_act_send_signed,
            ]),

            'destination' => $destination,
            'filename' => $fileName,
            'displayMode' => PdfRenderer::DISPLAY_MODE_FULLPAGE,
            'format' => 'A4-P',
        ]);
        \Yii::$app->controller->view->params['asset'] = DocumentPrintAsset::className();

        return $renderer;
    }

    /**
     * @inheritdoc
     */
    public function setStatusPrinted()
    {
        if ($this && in_array($this->status_out_id, [ActStatus::STATUS_CREATED, ActStatus::STATUS_PRINTED])) {
            LogHelper::save($this, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function ($model) {
                $model->status_out_id = ActStatus::STATUS_PRINTED;
                $model->status_out_updated_at = time();
                $model->status_out_author_id = \Yii::$app->user->id;

                return $model->save(true, ['status_out_id', 'status_out_updated_at', 'status_out_author_id']);
            });
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSignBasisDocument()
    {
        return $this->hasOne(DocumentType::className(), ['id' => 'sign_document_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderActs()
    {
        return $this->hasMany(OrderAct::className(), ['act_id' => 'id']);
    }

    /**
     * @return array
     */
    public function getGroupedOrderActs()
    {
        $orders = $this->getOrderActs()->with('order')->all();

        return $this->groupOwnOrdersByProduct($orders);
    }

    /**
     * @return \yii\db\ActiveQuery|array
     */
    public function getOwnOrders()
    {
        // don't group foreign currency invoices
        if (isset($this->invoice) && $this->invoice->currency_name != Currency::DEFAULT_NAME)
            return $this->getOrderActs();

        return ($this->isGroupOwnOrdersByProduct) ? $this->getGroupedOrderActs() : $this->getOrderActs();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['id' => 'order_id'])->viaTable('order_act', ['act_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmailFiles()
    {
        return $this->hasMany(EmailFile::className(), ['act_id' => 'id']);
    }

    /**
     * @return integer
     */
    public function getTotalAmountWithNds()
    {
        $amount = 0;
        foreach ($this->orderActs as $key => $value) {
            $amount += $value->amountWithNds;
        }

        return $amount;
    }

    /**
     * @return integer
     */
    public function getTotalAmountNoNds()
    {
        $amount = 0;
        foreach ($this->orderActs as $key => $value) {
            $amount += $value->amountNoNds;
        }

        return $amount;
    }

    /**
     * @return integer
     */
    public function getTotalActNds()
    {
        return $this->totalAmountWithNds - $this->totalAmountNoNds;
    }

    /**
     * @param $period
     * @param $acts
     */
    public static function generateXlsTable($period, $acts)
    {
        $excel = new Excel();

        $fileName = 'Акты_' . DateHelper::format($period['from'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) .
            '-' . DateHelper::format($period['to'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $acts,
            'title' => 'Акты',
            'rangeHeader' => range('A', 'G'),
            'columns' => [
                [
                    'attribute' => 'document_date',
                    'value' => function (Act $data) {
                        return DateHelper::format($data->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                    },
                ],
                [
                    'attribute' => 'document_number',
                    'value' => function (Act $data) {
                        return $data->fullNumber;
                    },
                ],
                [
                    'attribute' => 'invoice.total_amount_with_nds',
                    'styleArray' => [
                        'numberformat' => [
                            'code' => '# ##0.00',
                        ]
                    ],
                    'dataType' => 'n',
                    'value' => function ($model) {
                        return bcdiv($model->totalAmountWithNds, 100, 2);
                    },
                ],
                [
                    'attribute' => 'invoice.total_amount_no_nds',
                    'styleArray' => [
                        'numberformat' => [
                            'code' => '# ##0.00',
                        ]
                    ],
                    'dataType' => 'n',
                    'value' => function ($model) {
                        return bcdiv($model->totalAmountNoNds, 100, 2);
                    },
                ],
                [
                    'attribute' => 'invoice.total_amount_nds',
                    'styleArray' => [
                        'numberformat' => [
                            'code' => '# ##0.00',
                        ]
                    ],
                    'dataType' => 'n',
                    'value' => function ($model) {
                        return bcdiv($model->getTotalActNds(), 100, 2);
                    },
                ],
                [
                    'attribute' => 'contractor_id',
                    'value' => function (Act $data) {
                        return $data->invoice->contractor_name_short;
                    },
                ],
                [
                    'attribute' => 'contractor_inn',
                    'value' => function (Act $data) {
                        return $data->invoice->contractor_inn;
                    },
                ],
                [
                    'attribute' => 'status_out_id',
                    'value' => function (Act $data) {
                        return ($data->statusOut) ? $data->statusOut->name : '';
                    },
                ],
                [
                    'attribute' => 'invoice_document_number',
                    'value' => function (Act $data) {
                        return $data->invoice->fullNumber;
                    },
                ],
                [
                    'attribute' => 'nomenclature',
                    'wrap' => true,
                    'value' => function (Act $model) {

                        $list = [];
                        foreach ($model->orders as $order) {
                            $unitName = $order->unit ? $order->unit->name : Product::DEFAULT_VALUE;
                            $list[] = $order->product_title . ' ' .
                                TextHelper::numberFormat($order->quantity, 2) . ' ' .
                                $unitName . ' x ' .
                                TextHelper::invoiceMoneyFormat($order->view_price_one, 2) . 'р. = ' .
                                TextHelper::invoiceMoneyFormat($order->view_total_amount, 2) . 'р.';
                        }

                        return $list ? implode("\r", $list) : 'НЕТ';
                    },
                ],
                [
                    'attribute' => 'act_payment_date',
                    'wrap' => true,
                    'value' => function (Act $data) {
                        return DateHelper::format($data->invoice->payment_limit_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                    },
                ],
                [
                    'attribute' => 'act_responsible',
                    'wrap' => true,
                    'value' => function (Act $data) {
                        $employee = Employee::findOne(['id' => $data->document_author_id]);
                        return $employee->getFio();
                    },
                ],
            ],
            'headers' => [
                'document_date' => 'Дата акта',
                'document_number' => '№ акта',
                'invoice.total_amount_with_nds' => 'Сумма',
                'contractor_id' => 'Контрагент',
                'contractor_inn' => 'ИНН контрагента',
                'status_out_id' => 'Статус',
                'invoice_document_number' => 'Счёт №',
                'nomenclature' => 'Номенклатурная часть',
                'act_payment_date' => 'Дата платежа',
            ],
            'format' => 'Xlsx',
            'fileName' => $fileName,
        ]);
    }

    /**
     * @return bool|string
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     */
    public function generateOneCFile()
    {
        $exportModel = new Export([
            'company_id' => $this->company->id,
            'user_id' => $this->document_author_id,
        ]);

        $oneCExport = new OneCExport($exportModel);
        if ($oneCExport->findDataObjectAct($this->id, Documents::IO_TYPE_IN)) {
            $path = Yii::getAlias('@frontend/runtime/export') . DIRECTORY_SEPARATOR . $oneCExport->createExportFile();
            Yii::$app->db->createCommand("DELETE FROM `export` WHERE `export`.`id` = {$exportModel->id}")->execute();

            return $path;
        }
        return false;
    }

    /**
     * @return string
     */
    public function getOneCFileName()
    {
        return 'Акт №' . $this->getFullNumber() . '.xml';
    }

    /**
     * @return string
     */
    public function getEmailSubject()
    {
        return 'Акт № ' . $this->fullNumber
            . ' от ' . date_format(date_create($this->document_date), 'd.m.Y')
            . ' от ' . $this->invoice->company_name_short;
    }

    /**
     * @param EmployeeCompany $sender
     * @param array|string $toEmail
     * @param null $emailText
     * @param null $subject
     * @param null $attach
     * @param bool $onlyMyAttach
     * @return bool
     * @throws \MpdfException
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function sendAsEmail(EmployeeCompany $sender, $toEmail, $emailText = null, $subject = null, $attach = null, $onlyMyAttach = false)
    {
        if ($this->type == Documents::IO_TYPE_OUT) {
            if ($this->uid == null) {
                $this->updateAttributes([
                    'uid' => self::generateUid(),
                ]);
            }

            $message = $this->getMailerMessage($sender, $toEmail, $emailText, $subject);

            if (!$onlyMyAttach) {
                $message->attachContent(self::getRenderer(null, $this, PdfRenderer::DESTINATION_STRING)->output(false), [
                    'fileName' => str_replace(['\\', '/'], '_', $this->pdfFileName),
                    'contentType' => 'application/pdf',
                ]);
                $oneCFilePath = $this->generateOneCFile();
                if ($oneCFilePath) {
                    $message->attach($oneCFilePath, [
                        'fileName' => 'Для загрузки в 1С ' . $this->getOneCFileName(),
                        'mime' => 'application/xml',
                    ]);
                }
            }
            if ($attach) {
                foreach ($attach as $file) {
                    if ($file['is_file']) {
                        if (exif_imagetype($file['content']) === false) {
                            $message->attach($file['content'], [
                                'fileName' => $file['fileName'],
                                'contentType' => $file['contentType'],
                            ]);
                        } else {
                            $message->embed($file['content'], [
                                'fileName' => $file['fileName'],
                                'contentType' => $file['contentType'],
                            ]);
                        }
                    } else {
                        $message->attachContent($file['content'], [
                            'fileName' => $file['fileName'],
                            'contentType' => $file['contentType'],
                        ]);
                    }
                }
            }

            if ($message->send()) {
                $this->updateSendStatus($sender->employee_id);
                \common\models\company\CompanyFirstEvent::checkEvent($this->company, 19);

                return true;
            }
        }

        return false;
    }

    /**
     * @param EmployeeCompany $sender
     * @param $toEmail
     * @param $type
     * @param $actIDs
     * @return bool
     * @throws \MpdfException
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     * @throws \Exception
     */
    public static function manySend(EmployeeCompany $sender, $toEmail, $type, $actIDs)
    {
        /* @var $models Act[] */
        $models = [];
        $emailSubject = [];
        $companyID = null;
        $emailTemplate = null;
        $companyNameShort = null;
        if ($type == Documents::IO_TYPE_OUT) {
            foreach ($actIDs as $actID) {
                $act = is_object($actID) ? $actID : self::findOne($actID);
                if ($act->uid == null) {
                    $act->updateAttributes([
                        'uid' => self::generateUid(),
                    ]);
                }
                $models[] = $act;
                $emailSubject[] = 'Акт № ' . $act->fullNumber
                    . ' от ' . date_format(date_create($act->document_date), 'd.m.Y');
                $companyID = $act->company->id;
                $emailTemplate = $act->emailTemplate;
                $companyNameShort = $act->invoice->company_name_short;
            }
            $emailSubject = implode(', ', $emailSubject);
            $emailSubject .= (' от ' . $companyNameShort);
            $params = [
                'model' => $models,
                'employeeCompany' => $sender,
                'employee' => $sender->employee,
                'subject' => $emailSubject,
                'toEmail' => $toEmail,
                'emailText' => null,
                'addHtml' => null,
                'pixel' => [
                    'company_id' => $companyID,
                    'email' => is_array($toEmail) ? implode(',', $toEmail) : $toEmail,
                ],
            ];
            Yii::$app->mailer->htmlLayout = 'layouts/document-html';
            $message = Yii::$app->mailer->compose([
                'html' => "system/documents/{$emailTemplate}/html",
                'text' => "system/documents/{$emailTemplate}/text",
            ], $params)
                ->setFrom([\Yii::$app->params['emailList']['docs'] => $sender->getFio()])
                ->setReplyTo([$sender->employee->email => $sender->getFio(true)])
                ->setSubject($emailSubject)
                ->setTo($toEmail);
            foreach ($models as $model) {
                $message->attachContent(self::getRenderer(null, $model, PdfRenderer::DESTINATION_STRING)->output(false), [
                    'fileName' => str_replace(['\\', '/'], '_', $model->pdfFileName),
                    'contentType' => 'application/pdf',
                ]);
            }
            if ($message->send()) {
                foreach ($models as $model) {
                    $model->updateSendStatus($sender->employee_id);
                    CompanyFirstEvent::checkEvent($model->company, 19);
                }

                return true;
            }
        }

        return false;
    }

    /**
     * @return array
     * @throws \MpdfException
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function loadEmailFiles()
    {
        $files = [];

        $emailFile = new EmailFile();
        $emailFile->company_id = $this->invoice->company_id;
        $emailFile->file_name = str_replace(['\\', '/'], '_', $this->pdfFileName);
        $emailFile->ext = 'pdf';
        $emailFile->mime = 'application/pdf';
        $emailFile->size = 0;
        $emailFile->type = EmailFile::TYPE_ACT_DOCUMENT;
        if ($emailFile->save()) {
            $path = $emailFile->getFilePath() . DIRECTORY_SEPARATOR . $emailFile->file_name;
            $size = file_put_contents($path, self::getRenderer(null, $this, PdfRenderer::DESTINATION_STRING)->output(false));
            $emailFile->size = ceil($size / 1024);
            if ($emailFile->save(true, ['size'])) {
                $files[] = [
                    'id' => $emailFile->id,
                    'name' => $emailFile->file_name,
                    'previewImg' => '<img src="/img/email/emailPdf.png" class="preview-img">',
                    'size' => $emailFile->size,
                    'deleteUrl' => Url::to(['/email/delete-email-file', 'id' => $emailFile->id]),
                    'downloadUrl' => Url::to(['/email/download-file', 'id' => $emailFile->id]),
                ];
            }
        }

        $oneCFilePath = $this->generateOneCFile();
        if ($oneCFilePath) {
            $fileName = 'Для загрузки в 1С ' . $this->getOneCFileName();
            $emailFile = new EmailFile();
            $emailFile->company_id = $this->invoice->company_id;
            $emailFile->file_name = $fileName;
            $emailFile->ext = 'xml';
            $emailFile->mime = 'application/xml';
            $emailFile->size = ceil(filesize($oneCFilePath) / 1024);
            $emailFile->type = EmailFile::TYPE_ONE_C_FILE;
            if ($emailFile->save()) {
                $files[] = [
                    'id' => $emailFile->id,
                    'name' => $emailFile->file_name,
                    'previewImg' => '<img src="/img/email/emailUnknown.png" class="preview-img">',
                    'size' => $emailFile->size,
                    'deleteUrl' => Url::to(['/email/delete-email-file', 'id' => $emailFile->id]),
                ];
            }
        }

        return $files;
    }

    /**
     * @param $employeeId
     * @throws \Exception
     */
    public function updateSendStatus($employeeId)
    {
        if (in_array($this->status_out_id, [ActStatus::STATUS_CREATED, ActStatus::STATUS_PRINTED, ActStatus::STATUS_SEND]) || !$this->getIsWasSent()) {
            LogHelper::save($this, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function ($model) use ($employeeId) {
                return (boolean)$model->updateAttributes([
                    'status_out_id' => ActStatus::STATUS_SEND,
                    'status_out_updated_at' => time(),
                    'status_out_author_id' => $employeeId,
                ]);
            });
        } else {
            $currentStatus = $this->status_out_id;
            $this->status_out_id = ActStatus::STATUS_SEND;
            LogHelper::log($this, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS);
            $this->status_out_id = $currentStatus;
        }
    }

    /**
     * @return string
     */
    public function getCompanyFullRequisites()
    {
        return $this->invoice->getCompanyFullRequisites();
    }

    /**
     * @return string
     */
    public function getContractorFullRequisites()
    {
        return $this->invoice->getContractorFullRequisites();
    }

    /**
     * @return string
     */
    public function getExecutorFullRequisites()
    {
        return $this->type == Documents::IO_TYPE_IN ? $this->getContractorFullRequisites() : $this->getCompanyFullRequisites();
    }

    /**
     * @return string
     */
    public function getCustomerFullRequisites()
    {
        return $this->type == Documents::IO_TYPE_IN ? $this->getCompanyFullRequisites() : $this->getContractorFullRequisites();
    }

    /**
     * @return string
     */
    public function getEmailText()
    {
        $document_date = DateHelper::format($this->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
        $sum = TextHelper::invoiceMoneyFormat($this->totalAmountWithNds, 2);

        $text = <<<EMAIL_TEXT
Здравствуйте!

Акт № {$this->fullNumber} от {$document_date} на сумму {$sum} р.
EMAIL_TEXT;

        return $text;
    }

    /**
     * @param $byProducts
     * @return int
     */
    public function getTotalPurchaseAmountOnDate($byProducts = false)
    {
        $id = $this->id;
        $table = 'act';
        $byProducts = ($byProducts) ? 1 : 0;

        try {
            return (int)Yii::$app->db->createCommand("
                SELECT SUM(IFNULL(`purchase_amount`, 0)) 
                FROM `product_turnover` 
                WHERE `document_id` = {$id} AND `document_table` = '{$table}' AND `production_type` = '{$byProducts}'
                GROUP BY `document_id`
            ")->queryScalar();
        } catch (\Throwable $e) {}

        return 0;
    }
}
