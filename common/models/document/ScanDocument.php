<?php

namespace common\models\document;

use common\components\UploadedFile;
use common\models\Company;
use common\models\Contractor;
use common\models\employee\Employee;
use common\models\file\File;
use common\models\scanRecognize\ScanRecognize;
use frontend\models\Documents;
use Yii;
use yii\base\ErrorException;
use yii\imagine\Image;

/**
 * This is the model class for table "scan_document".
 *
 * @property integer $id
 * @property integer $company_id
 * @property integer $employee_id
 * @property string $owner_model
 * @property string $owner_table
 * @property integer $owner_id
 * @property integer $contractor_id
 * @property integer $document_type_id
 * @property integer $created_at
 * @property string $name
 * @property string $description
 *
 * @property Company $company
 * @property Employee $employee
 * @property ScanRecognize $recognize
 */
class ScanDocument extends \yii\db\ActiveRecord
{
    const SCENARIO_CREATE = 'create';
    const MAX_SIZE = 3*1024*1024;
    const FILE_EXT = 'jpg, jpeg, png, bmp, pdf';

    public $upload;

    public static $maxUploadSize = 20*1024*1024;
    public static $maxFileSize = self::MAX_SIZE;
    public static $fileExtensions = self::FILE_EXT;

    private $_data = [];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'scan_document';
    }

    /**
     * @inheritdoc
     */
    public function transactions()
    {
        return [
            self::SCENARIO_DEFAULT => self::OP_ALL,
            self::SCENARIO_CREATE => self::OP_ALL,
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description'], 'default', 'value' => ''],
            [['name'], 'string', 'max' => 50],
            [['description'], 'string', 'max' => 250],
            [['contractor_id', 'document_type_id'], 'integer'],
            [['document_type_id'], 'in', 'range' => array_keys(Documents::$docMap)],
            [
                ['contractor_id'], 'exist',
                'targetClass' => 'common\models\Contractor',
                'targetAttribute' => 'id',
                'filter' => [
                    'or',
                    ['company_id' => null],
                    ['company_id' => $this->company_id],
                ],
            ],
            [
                ['upload'], 'required',
                'message' => 'Необходимо выбрать файл',
                'on' => self::SCENARIO_CREATE,
            ],
            [
                ['upload'], 'file',
                'skipOnEmpty' => true,
                'extensions' => self::$fileExtensions,
                'maxSize' => self::$maxUploadSize,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'employee_id' => 'Employee ID',
            'created_at' => 'Дата загрузки',
            'name' => 'Название',
            'description' => 'Описание',
            'owner' => 'Прикреплен',
            'contractor_id' => 'Контрагент',
            'document_type_id' => 'Тип документа',
        ];
    }

    /**
     * @inheritdoc
     */
    public function fields()
    {
        $fields = [
            "id",
            "company_id",
            "employee_id",
            "owner_id",
            "contractor_id",
            "document_type_id",
            "created_at",
            "name",
            "description",
        ];

        return array_combine($fields, $fields);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContractor()
    {
        return $this->hasOne(Contractor::className(), ['id' => 'contractor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRecognize()
    {
        return $this->hasOne(ScanRecognize::className(), ['scan_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function getFile()
    {
        return $this->hasOne(ScanDocumentFile::className(), ['owner_id' => 'id'])
            ->onCondition(['owner_model' => ScanDocument::className()]);
    }

    /**
     * @return \yii\db\ActiveRecord|null
     */
    public function getOwner()
    {
        if (!array_key_exists('getOwner', $this->_data)) {
            $model = null;
            if ($this->owner_model && $this->owner_id && class_exists($this->owner_model)) {
                $model = $this->owner_model::findOne($this->owner_id);
            }
            if ($model === null && ($this->owner_model || $this->owner_id )) {
                $this->updateAttributes([
                    'owner_model' => null,
                    'owner_id' => null,
                ]);
            }

            $this->_data['getOwner'] = $model;
        }

        return $this->_data['getOwner'];
    }


    /**
     * @return string
     */
    public function getTypeId()
    {
        return Documents::typeIdByClass($this->owner_model);
    }

    /**
     * @inheritdoc
     */
    public function load($data, $formName = null)
    {
        $load = parent::load($data, $formName);
        $this->upload = $formName === '' ?
                              UploadedFile::getInstanceByName('upload') :
                              UploadedFile::getInstance($this, 'upload');

        return $load || $this->upload !== null;
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            $this->owner_table = $this->owner_model ? $this->owner_model::tableName() : null;
            if (!$insert && $this->owner_id) {
                if ($this->isAttributeChanged('contractor_id', false)) {
                    $this->addError('contractor_id', 'Нельзя поменять контрагента, если скан прикреплен');

                    return false;
                }
                if ($this->isAttributeChanged('document_type_id', false)) {
                    $this->addError('document_type_id', 'Нельзя поменять тип документа, если скан прикреплен');

                    return false;
                }
            }
            if ($this->upload !== null) {
                $this->created_at = time();
                $upload = $this->upload;
                $filePath = $upload->tempName;
                if (strpos((string) mime_content_type($filePath), 'image') === 0 && $upload->size > self::$maxFileSize) {
                    $r = $upload->size / self::$maxFileSize;
                    list($width, $height) = getimagesize($filePath);
                    $l = (int) max($width, $height) / $r;
                    $img = Image::resize($filePath, $l, $l);
                    $img->save();
                    clearstatcache(true, $filePath);
                    $upload->size = filesize($filePath);
                }

                if ($upload->size > self::$maxFileSize) {
                    $message = Yii::t('yii', 'The file "{file}" is too big. Its size cannot exceed {formattedLimit}.');
                    $params = [
                        'file' => $upload->name,
                        'formattedLimit' => Yii::$app->formatter->asShortSize(self::$maxFileSize),
                    ];
                    $this->addError('upload', Yii::$app->getI18n()->format($message, $params, Yii::$app->language));

                    return false;
                }
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($this->upload !== null) {
            if (!$insert && $this->file !== null) {
                $this->file->delete();
            }

            ScanDocumentFile::$maxFileSize = self::$maxFileSize;
            ScanDocumentFile::$fileExtensions = self::$fileExtensions;

            $file = new ScanDocumentFile();
            $file->ownerModel = $this;
            $file->owner_model = ScanDocument::class;
            $file->owner_id = $this->id;
            $file->file = $this->upload;

            if (!$file->save()) {
                throw new ErrorException('Не удалось сохранить файл.');
            }
        }
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            if ($this->file) {
                return (boolean) $this->file->delete();
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @param Company $company
     * @return bool
     */
    public static function getNewScansCount(Company $company)
    {
        $query = File::find()
            ->andWhere(['company_id' => $company->id])
            ->andWhere(['is_deleted' => 0])
            ->andWhere(['is', 'directory_id', null])
            ->andWhere(['owner_table' => 'scan_document']);

        $query2 = ScanDocument::find();
        $query2->andWhere(['company_id' => $company->id]);
        $query2->andWhere(['owner_id' => null]);
        if ($freeScansIds = $query2->asArray()->select('id')->column()) {
            $query->andWhere(['owner_id' => $freeScansIds]);
        } else {
            return 0;
        }

        return $query->count();
    }
}
