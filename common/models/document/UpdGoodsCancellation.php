<?php

namespace common\models\document;

use Yii;

/**
 * This is the model class for table "upd_goods_cancellation".
 *
 * @property int $document_id
 * @property int $cancellation_id
 *
 * @property GoodsCancellation $cancellation
 * @property Upd $document
 */
class UpdGoodsCancellation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'upd_goods_cancellation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'document_id' => 'Document ID',
            'cancellation_id' => 'Cancellation ID',
        ];
    }

    /**
     * Gets query for [[Cancellation]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCancellation()
    {
        return $this->hasOne(GoodsCancellation::className(), ['id' => 'cancellation_id']);
    }

    /**
     * Gets query for [[Document]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDocument()
    {
        return $this->hasOne(Upd::className(), ['id' => 'document_id']);
    }
}
