<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 08.02.2019
 * Time: 16:31
 */

namespace common\models\document;


use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\company\CompanyTaxationType;
use common\models\company\CompanyType;
use common\models\Contractor;
use frontend\modules\export\models\one_c\OneCExport;
use Yii;

/**
 * Class DocumentHelper
 * @package common\models\document
 */
class DocumentHelper
{
    /**
     * @param Company $company
     * @return Contractor
     */
    public static function getContractorByCompany(Company $company)
    {
        $contractor = new Contractor([
            'type' => Contractor::TYPE_SELLER,
            'status' => Contractor::ACTIVE,
            'company_type_id' => $company->company_type_id,
            'name' => !empty($company->name_full) ? $company->name_full : $company->name_short,
            'director_name' => $company->getChiefFio(),
            'director_email' => $company->email,
            'chief_accountant_is_director' => true,
            'contact_is_director' => true,
            'legal_address' => $company->getAddressLegalFull(),
            'actual_address' => $company->getAddressActualFull(),
            'BIN' => ($company->company_type_id == CompanyType::TYPE_IP) ? ($company->egrip ? $company->egrip : '') : $company->ogrn,
            'ITN' => $company->inn,
            'PPC' => $company->kpp,
            'current_account' => $company->mainCheckingAccountant->rs,
            'bank_name' => $company->mainCheckingAccountant->bank_name,
            'corresp_account' => $company->mainCheckingAccountant->ks,
            'BIC' => $company->mainCheckingAccountant->bik,
            'taxation_system' => $company->companyTaxationType->osno
                ? Contractor::WITH_NDS : Contractor::WITHOUT_NDS,
            'is_deleted' => false,
            'object_guid' => OneCExport::generateGUID(),
            'created_from_company_id' => $company->id,
        ]);

        return $contractor;
    }

    /**
     * @param Contractor $contractor
     * @return Company
     */
    public static function getCompanyByContractor(Contractor $contractor)
    {
        $company = new Company([
            'id' => 1,
            'company_type_id' => $contractor->company_type_id,
            'name_full' => $contractor->name,
            'name_short' => $contractor->name,
            'email' => $contractor->director_email,
            'chief_is_chief_accountant' => true,
            'address_legal' => $contractor->legal_address,
            'address_actual' => $contractor->actual_address,
            'egrip' => $contractor->BIN,
            'ogrn' => $contractor->BIN,
            'inn' => $contractor->ITN,
            'kpp' => $contractor->PPC,
            'ip_lastname' => $contractor->name,
            'rs' => $contractor->current_account,
        ]);
        $mainCheckingAccountant = new CheckingAccountant([
            'rs' => $contractor->current_account,
            'bank_name' => $contractor->bank_name,
            'ks' => $contractor->corresp_account,
            'bik' => $contractor->BIC,
            'type' => CheckingAccountant::TYPE_MAIN,
        ]);
        $companyTaxationType = new CompanyTaxationType([
            'osno' => $contractor->taxation_system == Contractor::WITH_NDS,
            'usn' => $contractor->taxation_system == Contractor::WITHOUT_NDS,
        ]);

        $company->populateRelation('companyType', $contractor->companyType);
        $company->populateRelation('mainCheckingAccountant', $mainCheckingAccountant);
        $company->populateRelation('companyTaxationType', $companyTaxationType);

        return $company;
    }
}
