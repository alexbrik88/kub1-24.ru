<?php

namespace common\models\document;

/**
 * This is the model class for table "file".
 *
 * @property integer $id
 * @property string $filename
 * @property string $ext
 * @property string $filename_full
 * @property string $filepath
 * @property string $hash
 * @property integer $created_at
 * @property integer $created_at_author_id
 * @property string $owner_model
 * @property integer $owner_id
 * @property integer $company_id
 *
 * @property Employee $createdAtAuthor
 *
 * @property string $subFolderName
 */
class ScanDocumentFile extends \common\models\file\File
{
    /**
     * Path for file saving
     * Must be defined in extended class.
     * @var string
     */
    public $uploadDirectoryPath = 'documents' . DIRECTORY_SEPARATOR . 'scan';

    /**
     * @return array
     */
    public function fields()
    {
        return [
            "filename",
            "ext",
            "filename_full",
        ];
    }
}
