<?php

namespace common\models\document;

use common\components\helpers\ArrayHelper;
use common\models\product\Product;
use common\models\product\ProductUnit;
use common\models\TaxRate;
use frontend\models\Documents;

/**
 * This is the model class for table "order_act".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $act_id
 * @property integer $product_id
 * @property integer $invoice_id
 * @property integer $quantity
 *
 * @property Act $act
 * @property Invoice $invoice
 * @property Order $order
 * @property Product $product
 */
class OrderAct extends AbstractOrder
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_act';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['quantity'], 'filter', 'filter' => function ($value) {
                return str_replace(",", ".", $value);
            },],
            [['quantity'], 'number', 'numberPattern' => '/^\d+(\.\d*)?$/'],
            [['quantity'], function ($attribute, $params) {
                if ($this->hasErrors($attribute)) return;
                $title = 'для «' . $this->invoiceOrder->product_title . '»';
                $max = self::availableQuantity($this->invoiceOrder, $this->act_id);
                if ($this->$attribute <= 0) {
                    $this->addError($attribute, "Количество {$title} должно быть больше 0.");
                }
                //elseif ($this->$attribute > $max) {
                //    $this->addError($attribute, "Максимальное возможное количество {$title}: {$max}.");
                //}
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'act_id' => 'Act ID',
            'product_id' => 'Product ID',
            'invoice_id' => 'Invoice ID',
            'quantity' => 'Количество',
        ];
    }


    /**
     * @param $act_id
     * @param Order $order
     * @return OrderAct|null
     */
    public static function createFromOrder($act_id, Order $order)
    {
        if ($order->product->production_type == Product::PRODUCTION_TYPE_SERVICE && ($quantity = OrderAct::compareWithOrderByQuantity($order))) {

            $order_act = new OrderAct();
            $order_act->order_id = $order->id;
            $order_act->act_id = $act_id;
            $order_act->product_id = $order->product_id;
            $order_act->invoice_id = $order->invoice_id;
            $order_act->quantity = $quantity;
            return $order_act;
        }

        return null;
    }


    /**
     * @param $actId
     * @return bool
     */
    public static function deleteByAct($actId)
    {
        if (OrderAct::deleteAll(['act_id' => $actId]) > 0) {
            return true;
        } else {
            return false;
        }

    }


    /**
     * @param $actId
     * @return int
     */
    public static function countByAct($actId)
    {
        return count(OrderAct::findAll(['act_id' => $actId]));
    }

    /**
     * @param $order_id
     * @param $act_id
     * @param $quantity
     */
    public static function setQuantity($order_id, $act_id, $quantity)
    {
        $order_act = OrderAct::findOne(['order_id' => $order_id, 'act_id' => $act_id]);
        $order = Order::findOne(['id' => $order_id]);
        if ($order_act != null) {
            $order_act->quantity = $quantity;
            $order_act->save();
        }
    }

    /**
     * @param $order_id
     * @param $act_id
     * @return bool
     */
    public static function ifExist($order_id, $act_id)
    {
        return OrderAct::findOne(['order_id' => $order_id, 'act_id' => $act_id]) ? true : false;
    }

    /**
     * @param Order $order
     * @return int|null
     */
    public static function compareWithOrderByQuantity(Order $order)
    {
        $acts_quantity = (int) OrderAct::find()->where(['order_id' => $order->id])->sum('quantity');
        $available = $order->quantity - $acts_quantity;

        return max(0, $available);
    }

    /**
     * @param Invoice $invoice
     * @return int
     */
    public static function compareWithInvoiceBySum(Invoice $invoice)
    {
        $oa = 0;
        $ordersSum = 0;
        if ($invoice->orders != null) {
            foreach ($invoice->orders as $order) {
                if ($order->product->production_type == Product::PRODUCTION_TYPE_SERVICE) {
                    $ordersSum += $order->quantity;
                    $oa += OrderAct::find()->where(['order_id' => $order->id])->sum('quantity');
                }
            }
        }
        if ($ordersSum > $oa) {
            return $ordersSum - $oa;
        } elseif ($ordersSum == $oa) {
            return 0;
        } else {
            return 0;
        }
    }

    /**
     * @param $act_id
     * @return mixed
     */
    public static function allQuantitySum($act_id)
    {
        return OrderAct::find()->where(['act_id' => $act_id])->sum('quantity');
    }

    /**
     * @param $act_id
     * @param $sum_type
     * @return mixed
     */
    public static function allActSum($act_id, $sum_type)
    {
        return OrderAct::find()->where(['act_id' => $act_id])->sum('amount_' . $sum_type . '_with_vat');
    }

    /**
     * @param $act_id
     * @return array
     */
    public static function getAvailable($act_id)
    {
        $act = Act::findOne(['id' => $act_id]);
        $invoice = Invoice::findOne(['id' => $act->invoice_id]);
        $available = [];
        if ($invoice->orders != null) {
            foreach ($invoice->orders as $order) {
                if ($order->product->production_type == Product::PRODUCTION_TYPE_SERVICE) {
                    $order_act = OrderAct::findOne(['order_id' => $order->id, 'act_id' => $act_id]);
                    if ($order_act == null && OrderAct::compareWithOrderByQuantity($order) != 0) {
                        $available[$order->product_title] = OrderAct::createFromOrder($act_id, $order)->quantity;
                    }
                }
            }
        }

        return $available;
    }

    /**
     * @param Order $order
     * @param integer $act_id
     * @return numder
     */
    public static function availableQuantity(Order $order, $act_id = null)
    {
        $ordersIds = Invoice::find()
            ->joinWith(['invoiceActs', 'orders'])
            ->where(['order.product_id' => $order->product_id])
            ->andWhere(['invoice_act.act_id' => ArrayHelper::getColumn($order->invoice->acts, 'id')])
            ->select('order.id')
            ->distinct()
            ->column() ?: [$order->id];

        $orders_quantity = Order::find()->where(['id' => $ordersIds])->sum('quantity');

        $used_quantity = OrderAct::find()
            ->andWhere(['order_id' => $ordersIds])
            ->andWhere(['not', ['act_id' => $act_id]])
            ->sum('quantity');

        $available = $orders_quantity - $used_quantity;

        return max(0, $available);
    }

    /**
     * @return int
     */
    public function getAvailableQuantity()
    {
        $order_quantity = $this->order->quantity;

        $acts_quantity = OrderAct::find()->where(['order_id' => $this->order_id])->andWhere(['!=','id',$this->id])->sum('quantity');

        $result = $order_quantity - $acts_quantity;

        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAct()
    {
        return $this->hasOne(Act::className(), ['id' => 'act_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocument()
    {
        return $this->getAct();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id'])
                ->andOnCondition(['invoice_id' => $this->invoice_id]);
    }
}
