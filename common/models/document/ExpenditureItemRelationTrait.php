<?php

namespace common\models\document;

use yii\db\ActiveQuery;

/**
 * @property-read InvoiceExpenditureItem $expenditureItem
 */
trait ExpenditureItemRelationTrait
{
    /**
     * @var string[]
     */
    protected array $expenditureItemLink = ['id' => 'expenditure_item_id'];

    /**
     * @return InvoiceExpenditureItem
     */
    public function getExpenditureItem(): InvoiceExpenditureItem
    {
        return $this->__get('expenditureItemRelation');
    }

    /**
     * @return ActiveQuery
     */
    public function getExpenditureItemRelation(): ActiveQuery
    {
        return $this->hasOne(InvoiceExpenditureItem::class, $this->expenditureItemLink);
    }

    /**
     * @return bool
     */
    public function hasExpenditureItem(): bool
    {
        if ($this->__get('expenditureItemRelation')) {
            return true;
        }

        return false;
    }
}
