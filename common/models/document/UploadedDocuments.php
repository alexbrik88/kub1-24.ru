<?php

namespace common\models\document;

use common\components\date\DateHelper;
use common\components\date\DatePickerFormatBehavior;
use common\components\validators\BikValidator;
use common\models\address\Country;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\dictionary\bik\BikDictionary;
use common\models\document\status\InvoiceStatus;
use common\models\product\Product;
use common\models\product\ProductGroup;
use common\models\product\ProductUnit;
use common\models\TaxRate;
use frontend\models\Documents;
use GoogleCloudVisionPHP\GoogleCloudVision;
use Yii;
use common\models\employee\Employee;
use common\models\Company;
use yii\behaviors\TimestampBehavior;
use yii\db\Connection;
use yii\helpers\FileHelper;
use Intervention\Image\ImageManagerStatic as Image;
use yii\helpers\VarDumper;
use yii\web\NotFoundHttpException;

/**
 * This is the model class for table "uploaded_documents".
 *
 * @property integer $id
 * @property integer $created_at
 * @property integer $company_id
 * @property integer $author_id
 * @property string $file_name
 * @property string $ext
 * @property integer $size
 * @property string $type
 * @property string $document_number
 * @property string $document_date
 * @property string $inn
 * @property string $contractor_name
 * @property integer $contractor_type_id
 * @property string $kpp
 * @property string $rs
 * @property string $bik
 * @property string $bank_name
 * @property string $ks
 * @property string $chief_name
 * @property string $chief_position
 * @property string $base
 * @property string $base_number
 * @property string $base_date
 * @property string $legal_address
 * @property boolean $is_complete
 * @property string $real_file_name
 * @property string $total_amount_has_nds
 * @property string $total_amount_no_nds
 * @property string $total_amount_with_nds
 * @property string $total_amount_nds
 * @property integer $nds_view_type_id
 *
 *
 * @property Employee $author
 * @property Company $company
 * @property UploadedDocumentOrder[] $uploadedDocumentOrders
 * @property CompanyType $contractorType
 */
class UploadedDocuments extends \yii\db\ActiveRecord
{
    /**
     *
     */
    const TAB_REQUISITES = 1;
    /**
     *
     */
    const TAB_ORDERS = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'uploaded_documents';
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
            ],
            [
                'class' => DatePickerFormatBehavior::className(),
                'attributes' => [
                    'document_date',
                    'base_date',
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'author_id', 'document_number', 'document_date', 'inn',
                'contractor_type_id', 'kpp', 'rs', 'bik', 'bank_name',
                'chief_name', 'legal_address', 'contractor_name', 'total_amount_with_nds', 'total_amount_no_nds', 'nds_view_type_id'], 'required'],
            [['total_amount_nds'], 'required',
                'when' => function (UploadedDocuments $model) {
                    return $model->nds_view_type_id != Invoice::NDS_VIEW_WITHOUT;
                },
                'whenClient' => 'function () {
                    return $("#uploadeddocuments-nds_view_type_id").val() != ' . Invoice::NDS_VIEW_WITHOUT . ';
                }',
            ],
            [['created_at', 'company_id', 'author_id', 'size', 'contractor_type_id', 'nds_view_type_id',
                'total_amount_nds'], 'integer'],
            [['total_amount_no_nds', 'total_amount_with_nds'], 'integer', 'min' => 1],
            [['is_complete'], 'boolean'],
            [['file_name', 'ext', 'type', 'contractor_name', 'chief_name',
                'chief_position', 'base', 'base_number', 'legal_address', 'real_file_name'], 'string', 'max' => 255],
            [['total_amount_has_nds'], 'boolean'],
            [['document_number', 'bank_name'], 'string', 'max' => 45],
            [['document_date', 'base_date'], 'safe'],
            [['inn'], 'string', 'max' => 12],
            [['inn'], 'string',
                'length' => 10,
                'when' => function (UploadedDocuments $model) {
                    return ($model->contractor_type_id != CompanyType::TYPE_IP);
                },
                'whenClient' => 'function () {
                    return $("#uploadeddocuments-contractor_type_id").val() != ' . CompanyType::TYPE_IP . ';
                }',
            ],
            [['inn'], 'string',
                'length' => 12,
                'when' => function (UploadedDocuments $model) {
                    return ($model->contractor_type_id == CompanyType::TYPE_IP);
                },
                'whenClient' => 'function () {
                    return $("#uploadeddocuments-contractor_type_id").val() == ' . CompanyType::TYPE_IP . ';
                }',
            ],
            [['kpp', 'bik'], 'string', 'length' => 9, 'max' => 9,],
            [['ks'], 'string', 'length' => 20, 'max' => 20],
            [
                'bik',
                BikValidator::className(),
                'related' => [
                    'bank_name' => 'name',
                    'ks' => 'ks',
                ],
            ],
            [['rs'], 'string', 'length' => 20, 'max' => 20,],
            [['contractor_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => CompanyType::className(), 'targetAttribute' => ['contractor_type_id' => 'id']],
            [['author_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::className(), 'targetAttribute' => ['author_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Created At',
            'company_id' => 'Company ID',
            'author_id' => 'Author ID',
            'file_name' => 'File Name',
            'ext' => 'Ext',
            'size' => 'Size',
            'type' => 'Type',
            'inn' => 'ИНН',
            'contractor_name' => 'Название контрагента',
            'contractor_type_id' => 'Форма',
            'kpp' => 'КПП',
            'rs' => 'р/с',
            'bik' => 'БИК',
            'bank_name' => 'Наименование банка',
            'ks' => 'к/с',
            'legal_address' => 'Юр. адрес',
            'chief_name' => 'ФИО',
            'chief_position' => 'Должность',
            'base_number' => '№',
            'base_date' => 'от',
            'base' => 'Основание',
            'total_amount_no_nds' => 'Итого',
            'total_amount_with_nds' => 'Всего к оплате',
            'total_amount_nds' => 'Сумма НДС',
            'document_number' => 'Счет №',
            'document_date' => 'от',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Employee::className(), ['id' => 'author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUploadedDocumentOrders()
    {
        return $this->hasMany(UploadedDocumentOrder::className(), ['uploaded_document_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContractorType()
    {
        return $this->hasOne(CompanyType::className(), ['id' => 'contractor_type_id']);
    }

    /**
     * @return string
     */
    public function getFullContractorName()
    {
        $contractorTypeName = $this->contractorType ? $this->contractorType->name_short : '';

        return $contractorTypeName . ' "' . $this->contractor_name . '"';
    }

    /**
     * @return string
     */
    public function getUploadPath()
    {
        return Company::fileUploadPath($this->company_id) .
               DIRECTORY_SEPARATOR . $this->tableName() .
               DIRECTORY_SEPARATOR . $this->id;
    }

    /**
     * @return string
     * @throws \yii\base\Exception
     */
    public function getUploadDirectory()
    {
        $path = $this->getUploadPath();

        if (!file_exists($path)) {
            FileHelper::createDirectory($path);
        }

        return $path;
    }

    /**
     * @return string
     */
    public function getFilePath()
    {
        return $this->getUploadDirectory() . DIRECTORY_SEPARATOR . $this->file_name;
    }

    /**
     * @return string
     */
    public function getResizeFilePath()
    {
        return $this->getUploadDirectory() . DIRECTORY_SEPARATOR . 'resize-' . $this->file_name;
    }

    /**
     * @return array
     */
    public function getPDFFilesPath()
    {
        $files = scandir($this->getUploadDirectory());
        if (count($files) > 3) {
            $result = [];
            foreach (scandir($this->getUploadDirectory()) as $fileName) {
                if (in_array($fileName, ['.', '..', 'resize-convertedFromPDF.jpg'])) {
                    continue;
                }
                $result[] = $this->getUploadDirectory() . DIRECTORY_SEPARATOR . $fileName;
            }

            return $result;
        }

        return [$this->getFilePath()];
    }

    /**
     * @param bool|false $api
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     */
    public static function uploadFiles($api = false)
    {
        ini_set('memory_limit', '-1');
        $result = [];
        $formattedFilesData = [];
        foreach (current($_FILES) as $attribute => $dataValue) {
            foreach ($dataValue as $key => $value) {
                $formattedFilesData[$key][$attribute] = $value;
            }
        }

        return Yii::$app->db->transaction(function (Connection $db) use ($formattedFilesData, $api, $result) {
            foreach ($formattedFilesData as $formattedFileData) {
                $ext = pathinfo($formattedFileData['name'], PATHINFO_EXTENSION);
                if (in_array($ext, ['jpg', 'jpeg', 'png', 'pdf', 'JPG', 'JPEG', 'PNG', 'PDF']) && $formattedFileData['size'] <= 3 * 1024 * 1024) {
                    $uploadedDocument = new self;
                    $uploadedDocument->company_id = Yii::$app->user->identity->company->id;
                    $uploadedDocument->author_id = Yii::$app->user->identity->id;
                    $uploadedDocument->real_file_name = $formattedFileData['name'];
                    if (!$uploadedDocument->save(false, ['company_id', 'author_id', 'created_at', 'real_file_name'])) {
                        $db->transaction->rollBack();

                        return false;
                    }
                    if (file_exists($uploadedDocument->getUploadDirectory())) {
                        if ($files = glob($uploadedDocument->getUploadDirectory() . DIRECTORY_SEPARATOR . '*')) {
                            foreach ($files as $file) {
                                unlink($file);
                            }
                        }
                    }
                    $filePath = $uploadedDocument->getUploadDirectory() . DIRECTORY_SEPARATOR . $formattedFileData['name'];
                    if (move_uploaded_file($formattedFileData['tmp_name'], $filePath)) {
                        if (in_array($ext, ['pdf', 'PDF'])) {
                            $newFileName = 'convertedFromPDF.jpg';
                            $formattedFileData['name'] = $newFileName;
                            $ext = 'jpg';
                            $newPath = $uploadedDocument->getUploadDirectory() . DIRECTORY_SEPARATOR . $newFileName;
                            exec("convert -density 300 \"{$filePath}\" \"{$newPath}\"", $abc, $bca);
                            unlink($filePath);
                            $filePath = count(scandir($uploadedDocument->getUploadDirectory())) > 3 ? $uploadedDocument->getUploadDirectory() . DIRECTORY_SEPARATOR . 'convertedFromPDF-0.jpg' : $newPath;
                        }
                        $resizeImage = Image::make($filePath);
                        $resizeImagePath = $uploadedDocument->getUploadDirectory() . DIRECTORY_SEPARATOR . 'resize-' . $formattedFileData['name'];
                        $resizeImage->resize(130, 185)->save($resizeImagePath, 100);
                        $uploadedDocument->file_name = $formattedFileData['name'];
                        $uploadedDocument->ext = $ext;
                        $uploadedDocument->size = $formattedFileData['size'];
                        $uploadedDocument->type = $formattedFileData['type'];
                        if (!$uploadedDocument->save(false, ['file_name', 'ext', 'size', 'type'])) {
                            $db->transaction->rollBack();

                            return false;
                        }
                    }

                    if (!$api) {
                        if (!self::getTextByImage($uploadedDocument)) {
                            $db->transaction->rollBack();

                            return false;
                        }
                    } else {
                        $model = self::getTextByImage($uploadedDocument, $api);
                        if ($model->save(false)) {
                            $result[$uploadedDocument->id] = $uploadedDocument->attributes;
                        } else {
                            $db->transaction->rollBack();

                            return false;
                        }
                    }
                }
            }

            return $api ? $result : true;
        });
    }


    /**
     * @param UploadedDocuments $uploadedDocument
     * @param bool|false $api
     * @return bool|UploadedDocuments
     * @throws \Exception
     */
    public static function getTextByImage(UploadedDocuments $uploadedDocument, $api = false)
    {
        $result = [];
        if ($uploadedDocument->isPDF()) {
            foreach (scandir($uploadedDocument->getUploadDirectory()) as $fileName) {
                if (in_array($fileName, ['.', '..', 'resize-convertedFromPDF.jpg'])) {
                    continue;
                }
                $gcv = new GoogleCloudVision();
                $gcv->setKey('AIzaSyBJbD9J_Si9jBd4hcH_fpKxBbg7b88tq0I');
                $gcv->setImage(file_get_contents($uploadedDocument->getUploadDirectory() . DIRECTORY_SEPARATOR . $fileName), 'RAW');
                $gcv->addFeature('DOCUMENT_TEXT_DETECTION');
                $gcv->setImageContext([
                    'languageHints' => [
                        'ru',
                    ],
                ]);
                $response = $gcv->request();
                $result = self::detectByBlocks($response, $result);
            }
        } else {
            $gcv = new GoogleCloudVision();
            $gcv->setKey('AIzaSyBJbD9J_Si9jBd4hcH_fpKxBbg7b88tq0I');
            $gcv->setImage(file_get_contents($uploadedDocument->getFilePath()), 'RAW');
            $gcv->addFeature('DOCUMENT_TEXT_DETECTION');
            $gcv->setImageContext([
                'languageHints' => [
                    'ru',
                ],
            ]);
            $response = $gcv->request();
            $result = self::detectByBlocks($response);
        }

        if (isset($result['inn'])) {
            $uploadedDocument->detectInn($result['inn']);
        } elseif (isset($result['bik'])) {
            $uploadedDocument->detectInn($result['bik']);
        }

        $contractor = $uploadedDocument->detectContractorByINN();

        if ($contractor !== false) {
            $uploadedDocument->kpp = $contractor->PPC;
            $uploadedDocument->contractor_name = $contractor->name;
            $uploadedDocument->contractor_type_id = $contractor->company_type_id;
            $uploadedDocument->legal_address = $contractor->legal_address;
            $uploadedDocument->chief_name = $contractor->director_name;

            if ($contractor->current_account) {
                $uploadedDocument->rs = $contractor->current_account;
            } elseif (isset($result['rs'])) {
                $uploadedDocument->detectRs($result['rs']);
                if ($uploadedDocument->rs == null) {
                    $uploadedDocument->detectRs($result['bik']);
                }
            }
            if ($uploadedDocument->rs == null && isset($result['bik'])) {
                $uploadedDocument->detectRs($result['bik']);
            }
            if ($uploadedDocument->rs == null && isset($result['ks'])) {
                $uploadedDocument->detectRs($result['ks']);
            }

            if (isset($result['bik'])) {
                $uploadedDocument->detectBik($result['bik']);
            }
            if ($uploadedDocument->bik == null && isset($result['ks'])) {
                $uploadedDocument->detectBik($result['ks']);
            }

            if ($uploadedDocument->bik == $contractor->BIC) {
                $uploadedDocument->bank_name = $contractor->bank_name;
                $uploadedDocument->ks = $contractor->corresp_account;
            } elseif (!$uploadedDocument->validate(['bik'])) {
                $uploadedDocument->bik = $contractor->BIC;
                $uploadedDocument->bank_name = $contractor->bank_name;
                $uploadedDocument->ks = $contractor->corresp_account;
            }
        } else {
            if (isset($result['bik'])) {
                $uploadedDocument->detectBik($result['bik']);
            }
            if ($uploadedDocument->bik == null && isset($result['ks'])) {
                $uploadedDocument->detectBik($result['ks']);
            }
            if (isset($result['rs'])) {
                $uploadedDocument->detectRs($result['rs']);
                if ($uploadedDocument->rs == null) {
                    $uploadedDocument->detectRs($result['bik']);
                }
            }
            if ($uploadedDocument->rs == null && isset($result['bik'])) {
                $uploadedDocument->detectRs($result['bik']);
            }
            if ($uploadedDocument->rs == null && isset($result['ks'])) {
                $uploadedDocument->detectRs($result['ks']);
            }
            if (isset($result['contractor-name'])) {
                $uploadedDocument->detectContractorName($result['contractor-name']);
            } elseif (isset($result['inn'])) {
                $uploadedDocument->detectContractorName($result['inn']);
            }
            if (isset($result['kpp'])) {
                $uploadedDocument->detectKpp($result['kpp']);
            }
        }
        $uploadedDocument->chief_position = 'Генеральный директор';

        if (isset($result['number-and-date'])) {
            $uploadedDocument->detectInvoiceNumberAndDate($result['number-and-date']);
        }
        if (isset($result['base'])) {
            $uploadedDocument->detectBaseFields($result['base']);
        }
        if (isset($result['orders-title'])) {
            $uploadedDocument->detectOrders($result['orders-title'], isset($result['orders-amount']) ? $result['orders-amount'] : null);
        }
        if (isset($result['total-amount'])) {
            $uploadedDocument->detectAmount($result['total-amount']);
        }

        /* @var $uploadedDocumentOrder UploadedDocumentOrder */
        if (isset($result['orders-count']) && $result['orders-count'] == 1) {
            $uploadedDocumentOrder = $uploadedDocument->getUploadedDocumentOrders()->one();
            if ($uploadedDocument->total_amount_no_nds) {
                $uploadedDocumentOrder->amount = $uploadedDocument->total_amount_no_nds;
            }
            if ($uploadedDocument->total_amount_no_nds) {
                $uploadedDocumentOrder->price = $uploadedDocument->total_amount_no_nds;
                $uploadedDocumentOrder->count = 1;
            }
            if (!empty($uploadedDocumentOrder->price) && $uploadedDocumentOrder->amount !== null) {
                $uploadedDocumentOrder->count = round($uploadedDocument->total_amount_no_nds / $uploadedDocumentOrder->price);
            }
            $uploadedDocumentOrder->save(false, ['amount', 'price', 'count']);
        }
        if ($uploadedDocument->total_amount_no_nds == null && $uploadedDocument->total_amount_with_nds !== null) {
            $uploadedDocument->total_amount_no_nds = $uploadedDocument->total_amount_with_nds;
            $uploadedDocument->save(false, ['total_amount_no_nds']);
        }

        if ($uploadedDocument->validate() && $uploadedDocument->getUploadedDocumentOrders()->count()) {
            $uploadedDocument->is_complete = true;
            foreach ($uploadedDocument->getUploadedDocumentOrders()->all() as $uploadedDocumentOrder) {
                if (!$uploadedDocumentOrder->validate()) {
                    $uploadedDocument->is_complete = false;
                }
            }
        }

        return $api ? $uploadedDocument : $uploadedDocument->save(false);
    }

    /**
     * @param $response
     * @param array $result
     * @return array
     */
    public static function detectByBlocks($response, $result = [])
    {
        $addNumberAndDate = true;
        $existKs = false;
        if (isset($response['responses'])) {
            foreach ($response['responses'][0]['fullTextAnnotation']['pages'] as $page) {
                $existOrderTitle = false;
                foreach ($page['blocks'] as $key => $block) {
                    $data = [];
                    foreach ($block['paragraphs'] as $paragraph) {
                        foreach ($paragraph['words'] as $word) {
                            $detectedWord = '';
                            foreach ($word['symbols'] as $symbol) {
                                $detectedWord .= $symbol['text'];
                            }
                            $data[] = $detectedWord;
                        }
                    }
                    if (self::inArrayLower('бик', $data)) {
                        $key = 'bik';
                    } elseif (self::inArrayLower('инн', $data)) {
                        $existInn = false;
                        if (isset($result['inn'])) {
                            foreach ($result['inn'] as $value) {
                                if (in_array(strlen(preg_replace("/[^0-9]/", '', $value)), [10, 12])) {
                                    $existInn = true;
                                }
                            }
                        }
                        if (!$existInn) {
                            $key = 'inn';
                        }
                    } elseif ((self::inArrayLower('кпп', $data) || self::inArrayLower('knn', $data)) && !isset($result['kpp'])) {
                        $key = 'kpp';
                    } elseif (self::inArrayLower(['счет', 'счet', 'счetNo'], $data)) {
                        if ($addNumberAndDate) {
                            $key = 'number-and-date';
                            $addNumberAndDate = false;
                        }
                    } elseif (self::inArrayLower('основание', $data)) {
                        $key = 'base';
                    } elseif (self::inArrayLower(['наименование', 'товары', 'товар'], $data) && !self::inArrayLower('Внимание', $data)) {
                        $key = 'orders-title';
                    } elseif (self::inArrayLower(['итого', 'итого:', 'ндс'], $data)) {
                        $key = 'total-amount';
                    } elseif (self::inArrayLower('сумма', $data) && !self::inArrayLower('наименований', $data)) {
                        if (($sumPosition = array_search('Сумма', $data)) !== false) {
                            unset($data[$sumPosition]);
                            $data = array_values($data);
                        }
                        $key = 'orders-amount';
                    } elseif (self::inArrayLower('наименований', $data)) {
                        $orderCount = $data[array_search('наименований', $data) + 1];
                        if (is_numeric($orderCount)) {
                            $data = (int)$orderCount;
                            $key = 'orders-count';
                        } else {
                            break 2;
                        }
                    } elseif (count($data) == 1) {
                        if (is_numeric(current($data))) {
                            if (strlen(current($data)) == 9) {
                                $key = 'bik';
                            } elseif (strlen(current($data)) == 20) {
                                $key = 'rs';
                            }
                        }
                    } elseif (is_numeric(current($data)) && strlen(current($data)) == 9) {
                        $key = 'bik';
                    }
                    if (is_numeric($key)) {
                        end($result);
                        $lastKey = key($result);
                        if ($lastKey === 'base') {
                            $key = 'base';
                        } elseif ($lastKey === 'bik') {
                            foreach ($result['bik'] as $value) {
                                if (strlen(preg_replace("/[^0-9]/", '', $value)) == 20) {
                                    $existKs = true;
                                }
                            }
                            if (!$existKs) {
                                $key = 'ks';
                            }
                        } elseif ($lastKey === 'orders-title') {
                            if (!empty($data)) {
                                if (!$existOrderTitle) {
                                    if ($data[0] != 1 && !self::inArrayLower('кол', $data)) {
                                        $key = 'orders-title';
                                    } elseif (self::inArrayLower('кол', $data)) {
                                        $key = 'orders-amount';
                                    } else {
                                        $key = 'orders-title';
                                        $existOrderTitle = true;
                                    }
                                } else {
                                    if (self::inArrayLower(['итого', 'итого:'], $data)) {
                                        $key = 'total-amount';
                                    } else {
                                        $key = 'orders-amount';
                                    }
                                }
                            }
                        } elseif ($lastKey === 'orders-amount') {
                            if (is_numeric($data[0]) && isset($data[1]) && in_array($data[1], ['|', '/'])) {
                                $key = 'orders-title';
                            } else {
                                $key = 'orders-amount';
                            }
                        } elseif ($lastKey === 'total-amount') {
                            $key = 'total-amount';
                        } elseif ($lastKey === 'kpp') {
                            $key = 'rs';
                        } elseif ($lastKey === 'number-and-date') {
                            if (!self::inArrayLower(['поставщик', 'плательщик'], $data)) {
                                $key = 'number-and-date';
                            }
                        } elseif ($lastKey === 'inn') {
                            $key = 'kpp';
                        } elseif ($lastKey === 'rs') {
                            if (!self::inArrayLower('получатель', $data)) {
                                $key = 'contractor-name';
                            }
                        } elseif ($lastKey === 'ks' && isset($result['bik'])) {
                            if (count($result['bik']) == 1 && mb_strtolower(current($result['bik'])) == 'бик') {
                                unset($result['ks']);
                                $key = 'bik';
                            }
                        } elseif ($lastKey === 'orders-count') {
                            break 2;
                        }
                    }

                    if ($key !== null && !is_numeric($key)) {
                        if (isset($result[$key])) {
                            foreach ($data as $value) {
                                $result[$key][] = $value;
                            }
                        } else {
                            $result[$key] = $data;
                        }
                    }
                }
            }
        }

        return $result;
    }

    /**
     * @param $data
     */
    public function detectRs($data)
    {
        if (self::inArrayLower('получатель', $data)) {
            $i = array_search('Получатель', $data);
            if ($i) {
                do {
                    if (is_numeric($data[$i])) {
                        $this->rs .= $data[$i];
                    }
                    $i++;
                } while (isset($data[$i]) && $data[$i] !== 'Банк');
            }
        } else {
            foreach ($data as $value) {
                $this->rs .= preg_replace("/[^0-9]/", '', $value);
                if (strlen($this->rs) >= 20) {
                    break;
                }
            }
        }
    }

    /**
     * @param $data
     */
    public function detectBik($data)
    {
        foreach ($data as $value) {
            $numericValue = preg_replace("/[^0-9]/", '', $value);
            if (strlen($numericValue) == 9) {
                $this->bik = $numericValue;
            }
        }
        /* @var $bikDictionary BikDictionary */
        $bikDictionary = BikDictionary::find()->andWhere(['bik' => $this->bik])->one();
        if ($bikDictionary !== null) {
            $this->bank_name = $bikDictionary->name;
            $this->ks = $bikDictionary->ks;
        }
    }

    /**
     * @param $data
     */
    public function detectContractorName($data)
    {
        $contractorNameStartPosition = null;
        if (array_search('ИНН', $data) !== false) {
            $contractorNameStartPosition = array_search('ИНН', $data) + 2;
        } elseif (array_search('Инн', $data) !== false) {
            $contractorNameStartPosition = array_search('Инн', $data) + 2;
        } else {
            $contractorNameStartPosition = 0;
        }
        $contractorType = self::getContractorTypeFromText($data, $contractorNameStartPosition);
        $this->contractor_type_id = $contractorType ? $contractorType->id : null;
        $i = 1;
        do {
            $this->contractor_name .= $data[$contractorNameStartPosition + $i];
            $i++;
        } while (isset($data[$contractorNameStartPosition + $i]) && mb_strtolower($data[$contractorNameStartPosition + $i]) !== 'получатель');
        $this->contractor_name = str_replace(['"', '«', '»'], '', $this->contractor_name);
    }

    /**
     *  Распознаем номер и дату загружаемого счета
     * @param $data
     */
    public function detectInvoiceNumberAndDate($data)
    {
        $invoiceNumberText = array_search('Счет', $data);
        $i = 2;
        if ($data[$invoiceNumberText + 1] == 'на' && $data[$invoiceNumberText + 2] == 'оплату') {
            $i = 4;
        }
        if ($data[$invoiceNumberText + $i] == 'от') { // проверяем корректно ли разобрало символ №, бывает гугл ебется в глаза и распознает как N3 - где N - это наш символ №, а 3 - номер счета
            $this->document_number = preg_replace("/[^0-9]/", '', $data[$invoiceNumberText + $i - 1]); // записываем номер счета
            $documentDateStartPosition = $invoiceNumberText + $i + 1;
        } else {
            do {
                $this->document_number .= $data[$invoiceNumberText + $i];
                $i++;
            } while (mb_strtolower($data[$invoiceNumberText + $i]) !== 'от');
            $documentDateStartPosition = $invoiceNumberText + $i + 1;
        }
        if (strtotime($data[$documentDateStartPosition])) { // проверяем в каком формате дата
            $this->document_date = date(DateHelper::FORMAT_DATE, strtotime($data[$documentDateStartPosition])); // если без русских слов, то конвертируем
        } else {
            $date = '';
            $i = 0;
            do {
                $value = $data[$documentDateStartPosition + $i];
                if (!is_numeric($value) && $value !== '.') {
                    $value = ' ' . self::translateMonth(mb_strtolower($value)) . ' ';
                }
                $date .= $value;
                if (is_numeric($value) && strlen($value) == 4) {
                    break;
                }
                $i++;
            } while (isset($data[$documentDateStartPosition + $i]));
            $this->document_date = date(DateHelper::FORMAT_DATE, strtotime($date));
        }
    }

    /**
     *  Распознаем ИНН
     * @param $data
     */
    public function detectInn($data)
    {
        $innStartPosition = null;
        if (array_search('ИНН', $data) !== false) {
            $innStartPosition = array_search('ИНН', $data);
        } elseif (array_search('Инн', $data) !== false) {
            $innStartPosition = array_search('Инн', $data);
        } elseif (array_search('инн', $data) !== false) {
            $innStartPosition = array_search('инн', $data);
        }

        if ($innStartPosition !== false) {
            $inn = $data[$innStartPosition + 1];
            if (in_array(strlen($inn), [10, 12])) {
                $this->inn = $inn;
            }
        }
    }

    /**
     * @param $data
     */
    public function detectKpp($data)
    {
        foreach ($data as $value) {
            $this->kpp .= preg_replace("/[^0-9]/", '', $value);
        }
    }

    /**
     * @param $data
     */
    public function detectBaseFields($data)
    {
        $baseTextPosition = null;
        $existNumberSymbol = false;
        $existBaseText = true;
        if (array_search('Основание', $data) !== false) {
            $baseTextPosition = array_search('Основание', $data);
        } elseif (array_search('Основание:', $data) !== false) {
            $baseTextPosition = array_search('Основание:', $data);
        }
        if ($data[$baseTextPosition + 1] == ':') {
            $i = 2;
        } else {
            $i = 1;
        }
        foreach (['№', 'No', 'N3', 'Nb', 'N'] as $numberText) {
            if (array_search($numberText, $data)) {
                $existNumberSymbol = true;
            }
            if (stristr($data[$baseTextPosition + $i], $numberText) !== false) {
                $existBaseText = false;
                break;
            }
        }
        if ($existBaseText) {
            if ($existNumberSymbol) {
                do {
                    $this->base .= $data[$baseTextPosition + $i];
                    $i++;
                } while (isset($data[$baseTextPosition + $i]) && !in_array($data[$baseTextPosition + $i], ['№', 'No', 'N3', 'Nb', 'N']) &&
                    stristr($data[$baseTextPosition + $i], 'N') === false &&
                    !is_numeric($data[$baseTextPosition + $i]));
            } else {
                $this->base = $data[$baseTextPosition + $i];
                $i++;
            }
        }
        $baseNumberTextNumber = $baseTextPosition + $i;
        $i = 1;
        if (!in_array($data[$baseTextPosition + $i], ['№', 'No', 'N3', 'Nb'])) {
            $i = 0;
        }

        do {
            $this->base_number .= $data[$baseNumberTextNumber + $i];
            $i++;
        } while (isset($data[$baseNumberTextNumber + $i]) && mb_strtolower($data[$baseNumberTextNumber + $i]) !== 'от');
        $this->base_number = str_replace(['№', 'No', 'N3', 'Nb', 'N'], '', $this->base_number);
        $i++;
        if (isset($data[$baseNumberTextNumber + $i])) {
            do {
                $this->base_date .= $data[$baseNumberTextNumber + $i];
                $i++;
            } while (isset($data[$baseNumberTextNumber + $i]));
            $this->base_date = date(DateHelper::FORMAT_DATE, strtotime($this->base_date));
        }
    }

    /**
     * @return bool|Contractor
     */
    public function detectContractorByINN()
    {
        if ($this->inn) {
            /* @var $contractor Contractor */
            $contractor = Contractor::find()->andWhere(['and',
                ['ITN' => $this->inn],
                ['company_id' => $this->company_id],
                ['is_deleted' => false],
                ['type' => Contractor::TYPE_SELLER],
            ])->andFilterWhere(['PPC' => $this->kpp])->one();
            if (!$contractor) {
                $response = \Yii::$app->dadataSuggestApi->getParty([
                    'query' => $this->inn,
                    'count' => 1
                ]);
                if (!$response->isError && !empty($response->data['suggestions'])) {
                    $data = $response->data['suggestions'][0];
                    $contractor = new Contractor();
                    $contractor->ITN = $this->inn;
                    /* @var $contractorType CompanyType */
                    $contractorType = CompanyType::find()
                        ->andWhere(['in_contractor' => true])
                        ->andWhere(['or',
                            ['name_short' => $data['data']['opf']['short']],
                            ['name_full' => $data['data']['opf']['short']],
                        ])->one();
                    if ($contractorType) {
                        $contractor->company_type_id = $contractorType->id;
                    }
                    $contractor->name = isset($data['data']['name']['full']) ? $data['data']['name']['full'] : null;
                    $contractor->PPC = isset($data['data']['kpp']) ? $data['data']['kpp'] : null;
                    $contractor->legal_address = isset($data['data']['address']['value']) ? $data['data']['address']['value'] : null;
                    $contractor->director_name = isset($data['data']['management']['name']) ? $data['data']['management']['name'] : null;
                } else {
                    return false;
                }
            }

            return $contractor;
        }

        return false;
    }

    /**
     * @param $ordersTitle
     * @param null $ordersAmount
     */
    public function detectOrders($ordersTitle, $ordersAmount = null)
    {
        $ordersArray = [];
        $withoutFirstNumber = false;
        $i = 0;
        do {
            if ($ordersTitle[$i] == 'рения' && isset($ordersTitle[$i + 1]) && $ordersTitle[$i + 1] !== '|') {
                $withoutFirstNumber = true;
                unset($ordersTitle[$i]);
                break;
            } elseif ($ordersTitle[$i] == 'Ед' && isset($ordersTitle[$i + 1]) && $ordersTitle[$i + 1] == '.' &&
                $ordersTitle[$i + 2] !== '|' && !(isset($ordersTitle[$i + 2]) && $ordersTitle[$i + 2] == 'Цена')
            ) {
                $withoutFirstNumber = true;
                unset($ordersTitle[$i]);
                unset($ordersTitle[$i + 1]);
                break;
            } elseif ($ordersTitle[$i] == 'Сумма') {
                unset($ordersTitle[$i]);
                break;
            }
            unset($ordersTitle[$i]);
            $i++;
        } while (isset($ordersTitle[$i]) && !is_numeric($ordersTitle[$i]));

        if ($ordersAmount !== null && (self::inArrayLower('кол', $ordersAmount) || self::inArrayLower('чество', $ordersAmount))) {
            $startPosition = false;
            if (array_search('Кол', $ordersAmount) !== false) {
                $startPosition = array_search('Кол', $ordersAmount);
            } elseif (array_search('кол', $ordersAmount) !== false) {
                $startPosition = array_search('кол', $ordersAmount);
            } elseif (array_search('чество', $ordersAmount) !== false) {
                $startPosition = array_search('чество', $ordersAmount);
                $withoutFirstNumber = true;
            } elseif (array_search('Чество', $ordersAmount) !== false) {
                $startPosition = array_search('Чество', $ordersAmount);
                $withoutFirstNumber = true;
            }
            if ($startPosition) {
                if (isset($ordersAmount[$startPosition - 2])) {
                    $i = 0;
                    do {
                        if (!in_array($ordersAmount[$i], ['рения', 'чество', 'Цена'])) {
                            $ordersTitle[] = $ordersAmount[$i];
                        }
                        unset($ordersAmount[$i]);
                        $i++;
                    } while (isset($ordersAmount[$i]) && mb_strtolower($ordersAmount[$i]) !== 'кол');
                    $ordersAmount = array_values($ordersAmount);
                }
            }
        }
        $ordersCount = 0;
        foreach ($ordersTitle as $key => $value) {
            if (in_array($ordersTitle[$key], ['|'])) {
                continue;
            } elseif (($value == $ordersCount + 1 && $ordersCount == 0) ||
                ($value == 'з' && $ordersCount == 2) ||
                ($value == $ordersCount + 1 && in_array($ordersTitle[$key + 1], ['|', '/'])) ||
                ($ordersCount > 10 && 1 . $value == $ordersCount + 1)
            ) {
                if (isset($ordersTitle[$key + 2]) && !ProductUnit::find()->andWhere([
                        'name' => mb_strtolower($ordersTitle[$key + 2]),
                    ])->exists()
                ) {
                    $ordersCount++;
                } else {
                    $ordersArray[$ordersCount]['name'][] = $value;
                }
            } elseif (ProductUnit::find()->andWhere([
                'name' => mb_strtolower($ordersTitle[$key]),
            ])->exists()
            ) {
                $i = 0;
                do {
                    $i++;
                } while (isset($ordersTitle[$key + $i]) && $ordersTitle[$key + $i] == '|');
                $ordersArray[$ordersCount]['name'][] = $value;
                if (!is_numeric($ordersTitle[$key + $i]) && !in_array($ordersTitle[$key + $i], ['.'])) {
                    $ordersCount++;
                }
            } else {
                if ($withoutFirstNumber) {
                    $ordersCount++;
                    $withoutFirstNumber = false;
                } elseif ($value == $ordersCount + 1) {
                    $ordersCount++;
                }
                $ordersArray[$ordersCount]['name'][] = $value;
            }
        }
        $priceInTitleArray = [];
        foreach ($ordersArray as $key => $order) {
            $price = [];
            $hasAmount = false;
            if (isset($order['name'])) {
                foreach ($order['name'] as $wordKey => $titleWord) {
                    if (is_numeric($titleWord)) {
                        if (!$hasAmount && $titleWord !== '00' && mb_substr($titleWord, 0, 1) == 0) {
                            $hasAmount = false;
                            $price = [];
                        } else {
                            $hasAmount = true;
                            end($price);
                            if (!empty($price) && $price[key($price)] == ',') {
                                if (strlen($titleWord) <= 2 && ((isset($order['name'][$wordKey + 1]) &&
                                            !in_array($order['name'][$wordKey + 1], ['/', '.'])) || (!isset($order['name'][$wordKey + 1]) &&
                                            $titleWord == '00'))
                                ) {
                                    $price[] = $titleWord;
                                    $priceInTitleArray[$key][] = $price;
                                }
                                $price = [];
                                $hasAmount = false;
                            } else {
                                $price[] = $titleWord;
                            }
                        }
                    } elseif (in_array($titleWord, [',', '-', '.'])) {
                        if ($hasAmount) {
                            $price[] = ',';
                        }
                    } else {
                        $hasAmount = false;
                        $price = [];
                    }
                    /* @var $productUnit ProductUnit */
                    if (($productUnit = ProductUnit::find()->andWhere([
                            'name' => mb_strtolower($titleWord),
                        ])->one()) !== null
                    ) {
                        $ordersArray[$key]['product_unit_id'] = $productUnit->id;
                        unset($ordersArray[$key]['name'][$wordKey]);
                        if (isset($order['name'][$wordKey - 1]) && is_numeric($order['name'][$wordKey - 1])) {
                            $ordersArray[$key]['count'] = $order['name'][$wordKey - 1];
                            unset($ordersArray[$key]['name'][$wordKey - 1]);

                        }
                    }
                }
            }
        }
        if (!empty($priceInTitleArray)) {
            foreach ($priceInTitleArray as $key => $amountArray) {
                $priceInTitleArray[$key]['countCommas'] = count($amountArray) > 1 ? 3 : 1;
                foreach ($amountArray as $amountKey => $amount) {
                    foreach ($amount as $oneAmount) {
                        if (is_numeric($oneAmount)) {
                            $priceInTitleArray[$key]['priceArray'][] = $oneAmount;
                        } else {
                            end($priceInTitleArray[$key]['priceArray']);
                            $priceInTitleArray[$key]['priceArray'][key($priceInTitleArray[$key]['priceArray'])] .= ',';
                        }
                    }
                }
            }
        }
        if (!empty($ordersAmount)) {
            $i = 0;
            if (!is_numeric($ordersAmount[$i])) {
                do {
                    unset($ordersAmount[$i]);
                    $i++;
                } while (isset($ordersAmount[$i]) && !is_numeric($ordersAmount[$i]));
            }
            $ordersCount = 1;
            if (isset($ordersAmount[$i])) {
                do {
                    /* @var $productUnit ProductUnit */
                    if (($productUnit = ProductUnit::find()->andWhere([
                            'name' => mb_strtolower($ordersAmount[$i + 1]),
                        ])->one()) !== null ||
                        ($ordersAmount[$i + 1] == '|' && ($productUnit = ProductUnit::find()->andWhere([
                                'name' => mb_strtolower($ordersAmount[$i + 2]),
                            ])->one()) !== null)
                    ) {
                        if (is_numeric($ordersAmount[$i])) {
                            $ordersArray[$ordersCount]['count'] = $ordersAmount[$i];
                        }
                        $ordersArray[$ordersCount]['product_unit_id'] = $productUnit->id;
                        $i++;
                        do {
                            $i++;
                        } while (!is_numeric($ordersAmount[$i]));
                    } elseif (!is_numeric($ordersAmount[$i + 1]) && !is_numeric($ordersAmount[$i + 2])) {
                        $ordersArray[$ordersCount]['count'] = $ordersAmount[$i];
                        $i++;
                        do {
                            $i++;
                        } while (!is_numeric($ordersAmount[$i]));
                    }
                    $priceArray = isset($priceInTitleArray[$ordersCount]['priceArray']) ?
                        $priceInTitleArray[$ordersCount]['priceArray'] : [];
                    $countCommas = isset($priceInTitleArray[$ordersCount]['countCommas']) ?
                        $priceInTitleArray[$ordersCount]['countCommas'] : 0;

                    do {
                        $value = $ordersAmount[$i];
                        if ($countCommas == 2) {
                            $countCommas++;
                        }
                        if (in_array($value, [',', '-', '.'])) {
                            end($priceArray);
                            if (isset($ordersAmount[$i + 2]) && in_array($ordersAmount[$i + 2], [',', '-', '.'])) {
                                $ordersArray[$ordersCount]['count'] = $priceArray[key($priceArray)];
                                unset($priceArray[key($priceArray)]);
                            } else {
                                $countCommas++;
                                $priceArray[key($priceArray)] .= ',';
                            }
                        } elseif (!in_array($value, [',', '-', '.']) && !is_numeric($value) && $value !== '|') {
                            end($priceArray);
                            $ordersArray[$ordersCount]['count'] = $priceArray[key($priceArray)];
                            unset($priceArray[key($priceArray)]);
                        } elseif ($value !== '|') {
                            $priceArray[] = $value;
                        }
                        $i++;
                    } while (isset($ordersAmount[$i]) && $countCommas < 3);

                    if ($countCommas == 1) { // если только одна сумма, то считаем, что количество = 1 и цену делаем такой же
                        $newPriceArray = $priceArray;
                        foreach ($priceArray as $onePrice) {
                            $newPriceArray[] = $onePrice;
                        }
                        $priceArray = $newPriceArray;
                    }

                    $ordersArray[$ordersCount]['price'] = $priceArray;
                    if (count($ordersArray) == 1 && isset($ordersAmount[$i]) && !is_numeric($ordersAmount[$i])) {
                        do {
                            $ordersArray[$ordersCount]['name'][] = $ordersAmount[$i];
                            $i++;
                        } while (isset($ordersAmount[$i]));
                    } elseif (isset($ordersAmount[$i]) && !is_numeric($ordersAmount[$i])) {
                        do {
                            $i++;
                        } while (isset($ordersAmount[$i]) && !is_numeric($ordersAmount[$i]));
                    }
                    $ordersCount++;
                } while (isset($ordersAmount[$i]));
            }
        }
        foreach ($ordersArray as $key => $oneOrderData) {
            $uploadedDocumentOrder = new UploadedDocumentOrder();
            $uploadedDocumentOrder->uploaded_document_id = $this->id;
            $uploadedDocumentOrder->company_id = $this->company_id;
            if (isset($oneOrderData['name'])) {
                foreach ($oneOrderData['name'] as $name) {
                    $space = ' ';
                    if (in_array($name, [',', '.', '/']) || $uploadedDocumentOrder->name === null) {
                        $space = '';
                    }
                    $uploadedDocumentOrder->name .= $space . $name;
                }
            }
            if (isset($oneOrderData['product_unit_id'])) {
                $uploadedDocumentOrder->product_unit_id = $oneOrderData['product_unit_id'];
            } else {
                $uploadedDocumentOrder->product_unit_id = ProductUnit::UNIT_COUNT;
            }
            if (isset($oneOrderData['count'])) {
                $uploadedDocumentOrder->count = $oneOrderData['count'];
            }
            if (isset($oneOrderData['price'])) {
                $uploadedDocumentOrder = $this->calculateOrderAmount($uploadedDocumentOrder, $oneOrderData['price']);
            } elseif (isset($priceInTitleArray[$key]['priceArray'])) {
                $uploadedDocumentOrder = $this->calculateOrderAmount($uploadedDocumentOrder, $priceInTitleArray[$key]['priceArray']);
            }
            $uploadedDocumentOrder = $this->setOrderProductionType($uploadedDocumentOrder);
            $uploadedDocumentOrder->save(false);
        }
    }

    /**
     * @param $data
     */
    public function detectAmount($data)
    {
        $position = null;

        if (array_search('Итого', $data) !== false) {
            $position = array_search('Итого', $data);
        } elseif (array_search('Итого:', $data) !== false) {
            $position = array_search('Итого:', $data);
        } elseif (array_search('ИТОГО:', $data) !== false) {
            $position = array_search('ИТОГО:', $data);
        } elseif (array_search('ИТОГО', $data) !== false) {
            $position = array_search('ИТОГО', $data);
        }

        if ($position !== null) {
            $i = 1;
            $amountData = [];
            $countCommas = 0;
            do {
                $dataValue = $data[$position + $i];
                $value = $dataValue;
                if (is_numeric($value) || in_array($value, [',', '-', '.'])) {
                    if (in_array($value, [',', '-', '.'])) {
                        $countCommas++;
                        end($amountData);
                        $amountData[key($amountData)] .= ',';
                    } else {
                        $amountData[] = $dataValue;
                    }
                }
                $i++;
            } while (isset($data[$position + $i]));

            if ($countCommas == 3) {
                $this->total_amount_has_nds = true;
                $this->nds_view_type_id = Invoice::NDS_VIEW_IN;
                $attributes = ['total_amount_no_nds', 'total_amount_nds', 'total_amount_with_nds'];
            } elseif ($countCommas > 3) {
                $this->total_amount_has_nds = true;
                $this->nds_view_type_id = Invoice::NDS_VIEW_IN;
                $attributes = ['total_amount_no_nds', 'total_amount_nds', 'total_amount_with_nds'];
                $deleteAmountCount = $countCommas - 3;
                foreach ($amountData as $key => $value) {
                    if ($deleteAmountCount > 0) {
                        unset($amountData[$key]);
                        if (!is_numeric($value)) {
                            $deleteAmountCount--;
                        }
                    } elseif ($deleteAmountCount == 0) {
                        unset($amountData[$key]);
                        $deleteAmountCount--;
                    } else {
                        break;
                    }
                }
            } else {
                $this->total_amount_nds = 0;
                $this->nds_view_type_id = Invoice::NDS_VIEW_WITHOUT;
                $attributes = ['total_amount_no_nds', 'total_amount_with_nds'];
            }

            foreach ($attributes as $attribute) {
                foreach ($amountData as $key => $value) {
                    if (stristr($this->$attribute, ',') !== false) {
                        $this->$attribute .= $value;
                        unset($amountData[$key]);
                        break;
                    } else {
                        $this->$attribute .= $value;
                        unset($amountData[$key]);
                    }
                }
            }
            $this->total_amount_no_nds = (int)preg_replace("/[^0-9]/", '', $this->total_amount_no_nds);
            $this->total_amount_nds = (int)preg_replace("/[^0-9]/", '', $this->total_amount_nds);
            $this->total_amount_with_nds = (int)preg_replace("/[^0-9]/", '', $this->total_amount_with_nds);
        } elseif (array_search('ндс', $data) !== false) {
            $position = array_search('ндс', $data);
            $amountData = [];
            $countCommas = 0;
            do {
                $dataValue = $data[$position];
                $value = $dataValue;
                if (is_numeric($value) || in_array($value, [',', '-', '.'])) {
                    if (in_array($value, [',', '-', '.'])) {
                        $countCommas++;
                        end($amountData);
                        $amountData[key($amountData)] .= ',';
                    } else {
                        if (isset($data[$position + 1])) {
                            if ($data[$position + 1] !== '%') {
                                $amountData[] = $dataValue;
                            }
                        } else {
                            $amountData[] = $dataValue;
                        }
                    }
                }
                $position++;
            } while (isset($data[$position]));

            $this->total_amount_has_nds = true;
            $this->nds_view_type_id = Invoice::NDS_VIEW_IN;
            $attributes = ['total_amount_nds', 'total_amount_with_nds'];

            foreach ($attributes as $attribute) {
                foreach ($amountData as $key => $value) {
                    if (stristr($this->$attribute, ',') !== false) {
                        $this->$attribute .= $value;
                        unset($amountData[$key]);
                        break;
                    } else {
                        $this->$attribute .= $value;
                        unset($amountData[$key]);
                    }
                }
            }
            $this->total_amount_nds = (int)preg_replace("/[^0-9]/", '', $this->total_amount_nds);
            $this->total_amount_with_nds = (int)preg_replace("/[^0-9]/", '', $this->total_amount_with_nds);
        }
    }

    /**
     * @param $needle
     * @param $haystack
     * @return bool
     */
    public static function inArrayLower($needle, $haystack)
    {
        if (is_array($needle)) {
            foreach ($needle as $value) {
                if (in_array(mb_strtolower($value), array_map('mb_strtolower', $haystack))) {
                    return true;
                }
            }

            return false;
        }

        return in_array(mb_strtolower($needle), array_map('mb_strtolower', $haystack));
    }


    /**
     * @param $data
     * @param $position
     * @return CompanyType|null
     */
    public static function getContractorTypeFromText($data, $position)
    {
        $value = mb_strtoupper(str_replace(['0', 'O'], 'О', $data[$position])); // поправить
        $value = str_replace('A', 'А', $value);

        return $contractorType = CompanyType::find()->where(['or',
            ['name_short' => $value],
            ['name_full' => $value],
        ])->one();
    }

    /**
     * @param UploadedDocumentOrder $uploadedDocumentOrder
     * @param $priceArray
     * @return UploadedDocumentOrder
     */
    public function calculateOrderAmount(UploadedDocumentOrder $uploadedDocumentOrder, $priceArray)
    {
        $priceArrayLength = count($priceArray);
        if ($priceArrayLength % 2 == 1) {
            $priceLength = (int)floor($priceArrayLength / 2);
        } else {
            $priceLength = (int)$priceArrayLength / 2;
        }
        foreach ($priceArray as $key => $onePricePart) {
            if (++$key <= $priceLength) {
                if ($key == $priceLength) {
                    $delimiter = '';
                } else {
                    $delimiter = ' ';
                }
                $uploadedDocumentOrder->price .= $onePricePart . $delimiter;
            } else {
                if ($key == $priceArrayLength) {
                    $delimiter = '';
                } else {
                    $delimiter = ' ';
                }
                $uploadedDocumentOrder->amount .= $onePricePart . $delimiter;
            }
        }
        $uploadedDocumentOrder->price = str_replace([' ', ','], '', $uploadedDocumentOrder->price);
        $uploadedDocumentOrder->amount = str_replace([' ', ','], '', $uploadedDocumentOrder->amount);

        if ($uploadedDocumentOrder->count === null && $uploadedDocumentOrder->price != 0) {
            $uploadedDocumentOrder->count = $uploadedDocumentOrder->amount / $uploadedDocumentOrder->price;
        }

        return $uploadedDocumentOrder;
    }

    /**
     * @param $periodName
     * @return array|string
     */
    public static function translateMonth($periodName)
    {
        $ruMonth = ['января', 'февраля', 'марта', 'апреля', 'мая', 'июня', 'июля', 'августа', 'сентября', 'октября', 'ноября', 'декабря'];
        $enMonth = ['january', 'february', 'march', 'april', 'may', 'june', 'july', 'august', 'september', 'october', 'november', 'december'];

        return str_replace($ruMonth, $enMonth, $periodName);
    }

    /**
     * @param $uploadedDocumentOrderData
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     */
    public function _save($uploadedDocumentOrderData)
    {
        $model = $this;
        $model->is_complete = true;

        return Yii::$app->db->transaction(function (Connection $db) use ($uploadedDocumentOrderData, $model) {
            if ($model->save()) {
                $newUploadedDocumentOrderData = null;
                if (isset($uploadedDocumentOrderData['new'])) {
                    $newUploadedDocumentOrderData = $uploadedDocumentOrderData['new'];
                }
                unset($uploadedDocumentOrderData['new']);

                $deleteUploadedDocumentOrdersID = array_diff($this->getUploadedDocumentOrders()->column(), array_keys($uploadedDocumentOrderData));
                foreach ($deleteUploadedDocumentOrdersID as $deleteUploadedDocumentOrderID) {
                    $deleteUploadedDocumentOrder = UploadedDocumentOrder::findOne($deleteUploadedDocumentOrderID);
                    if ($deleteUploadedDocumentOrder) {
                        $deleteUploadedDocumentOrder->delete();
                    }
                }

                if ($newUploadedDocumentOrderData) {
                    foreach ($newUploadedDocumentOrderData as $key => $oneUploadedDocumentData) {
                        $newUploadedDocumentOrder = new UploadedDocumentOrder();
                        $newUploadedDocumentOrder->uploaded_document_id = $model->id;
                        $newUploadedDocumentOrder->company_id = $model->company_id;
                        $newUploadedDocumentOrder->name = $oneUploadedDocumentData['name'];
                        $newUploadedDocumentOrder->count = $oneUploadedDocumentData['count'];
                        $newUploadedDocumentOrder->product_unit_id = $oneUploadedDocumentData['product_unit_id'];
                        $newUploadedDocumentOrder->price = str_replace([' ', ','], '', $oneUploadedDocumentData['price']);
                        $newUploadedDocumentOrder->amount = str_replace([' ', ','], '', $oneUploadedDocumentData['amount']);
                        $newUploadedDocumentOrder->production_type_id = $oneUploadedDocumentData['production_type_id'];
                        if (!$newUploadedDocumentOrder->save()) {
                            $db->transaction->rollBack();

                            return false;
                        }
                    }
                }
                unset($uploadedDocumentOrderData['new']);
                foreach ($uploadedDocumentOrderData as $key => $oneUploadedDocumentOrderData) {
                    $oldUploadedDocumentOrderData = UploadedDocumentOrder::findOne($key);
                    if ($oldUploadedDocumentOrderData === null) {
                        throw new NotFoundHttpException('Запрошенная страница не существует.');
                    }
                    $oldUploadedDocumentOrderData->uploaded_document_id = $model->id;
                    $oldUploadedDocumentOrderData->company_id = $model->company_id;
                    $oldUploadedDocumentOrderData->name = $oneUploadedDocumentOrderData['name'];
                    $oldUploadedDocumentOrderData->count = $oneUploadedDocumentOrderData['count'];
                    $oldUploadedDocumentOrderData->product_unit_id = $oneUploadedDocumentOrderData['product_unit_id'];
                    $oldUploadedDocumentOrderData->price = str_replace([' ', ','], '', $oneUploadedDocumentOrderData['price']);
                    $oldUploadedDocumentOrderData->amount = str_replace([' ', ','], '', $oneUploadedDocumentOrderData['amount']);
                    $oldUploadedDocumentOrderData->production_type_id = $oneUploadedDocumentOrderData['production_type_id'];
                    if (!$oldUploadedDocumentOrderData->save()) {
                        $db->transaction->rollBack();

                        return false;
                    }
                }

                return true;
            }

            return false;
        });
    }

    /**
     * @param $uploadedDocumentOrderData
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     */
    public function updateOrders($uploadedDocumentOrderData)
    {
        $model = $this;

        return Yii::$app->db->transaction(function (Connection $db) use ($uploadedDocumentOrderData, $model) {
            $newUploadedDocumentOrderData = null;
            if (isset($uploadedDocumentOrderData['new'])) {
                $newUploadedDocumentOrderData = $uploadedDocumentOrderData['new'];
            }
            unset($uploadedDocumentOrderData['new']);

            $deleteUploadedDocumentOrdersID = array_diff($this->getUploadedDocumentOrders()->column(), array_keys($uploadedDocumentOrderData));
            foreach ($deleteUploadedDocumentOrdersID as $deleteUploadedDocumentOrderID) {
                $deleteUploadedDocumentOrder = UploadedDocumentOrder::findOne($deleteUploadedDocumentOrderID);
                if ($deleteUploadedDocumentOrder) {
                    $deleteUploadedDocumentOrder->delete();
                }
            }

            if ($newUploadedDocumentOrderData) {
                foreach ($newUploadedDocumentOrderData as $key => $oneUploadedDocumentData) {
                    $newUploadedDocumentOrder = new UploadedDocumentOrder();
                    $newUploadedDocumentOrder->uploaded_document_id = $model->id;
                    $newUploadedDocumentOrder->company_id = $model->company_id;
                    $newUploadedDocumentOrder->name = $oneUploadedDocumentData['name'];
                    $newUploadedDocumentOrder->count = $oneUploadedDocumentData['count'];
                    $newUploadedDocumentOrder->product_unit_id = $oneUploadedDocumentData['product_unit_id'];
                    $newUploadedDocumentOrder->price = $this->convertAmount($oneUploadedDocumentData['price']);
                    $newUploadedDocumentOrder->amount = $this->convertAmount($oneUploadedDocumentData['amount']);
                    $newUploadedDocumentOrder->production_type_id = $oneUploadedDocumentData['production_type_id'];
                    if (!$newUploadedDocumentOrder->save()) {
                        $db->transaction->rollBack();

                        return false;
                    }
                }
            }

            foreach ($uploadedDocumentOrderData as $key => $oneUploadedDocumentOrderData) {
                $oldUploadedDocumentOrderData = UploadedDocumentOrder::findOne($key);
                if ($oldUploadedDocumentOrderData === null) {
                    throw new NotFoundHttpException('Запрошенная страница не существует.');
                }
                $oldUploadedDocumentOrderData->uploaded_document_id = $model->id;
                $oldUploadedDocumentOrderData->company_id = $model->company_id;
                $oldUploadedDocumentOrderData->name = $oneUploadedDocumentOrderData['name'];
                $oldUploadedDocumentOrderData->count = $oneUploadedDocumentOrderData['count'];
                $oldUploadedDocumentOrderData->product_unit_id = $oneUploadedDocumentOrderData['product_unit_id'];
                $oldUploadedDocumentOrderData->price = $this->convertAmount($oneUploadedDocumentOrderData['price']);
                $oldUploadedDocumentOrderData->amount = $this->convertAmount($oneUploadedDocumentOrderData['amount']);
                $oldUploadedDocumentOrderData->production_type_id = $oneUploadedDocumentOrderData['production_type_id'];
                if (!$oldUploadedDocumentOrderData->save()) {
                    $db->transaction->rollBack();

                    return false;
                }
            }

            return true;
        });
    }

    /**
     * @return bool
     */
    public function isPDF()
    {
        return $this->type == 'application/pdf';
    }

    /**
     * @return mixed
     * @throws \Exception
     * @throws \Throwable
     */
    public function createInvoice()
    {
        $model = $this;

        return Yii::$app->db->transaction(function (Connection $db) use ($model) {
            $invoice = new Invoice();
            $invoice->type = Documents::IO_TYPE_IN;
            $invoice->invoice_status_id = InvoiceStatus::STATUS_CREATED;
            $invoice->invoice_status_author_id = Yii::$app->user->identity->id;
            $invoice->document_author_id = Yii::$app->user->identity->id;
            $invoice->document_date = $model->document_date;
            $invoice->document_number = $model->document_number;
            $invoice->payment_limit_date = date(DateHelper::FORMAT_DATE, strtotime('+10 days', strtotime($model->document_date)));
            $invoice->invoice_expenditure_item_id = InvoiceExpenditureItem::ITEM_CONTRACTOR_PAYMENT;
            $invoice->isAutoinvoice = 0;
            $invoice->production_type = implode($model->getUploadedDocumentOrders()
                ->select('production_type_id')
                ->groupBy('production_type_id')
                ->column(), ', ');

            $invoice->company_id = $model->company_id;
            $invoice->company_inn = $model->company->inn;
            $invoice->company_kpp = $model->company->kpp;
            $invoice->company_egrip = $model->company->egrip;
            $invoice->company_okpo = $model->company->okpo;
            $invoice->company_name_full = $model->company->getTitle();
            $invoice->company_name_short = $model->company->getTitle(true, true);
            $invoice->company_address_legal_full = $model->company->getAddressLegalFull();
            $invoice->company_phone = $model->company->phone;
            $invoice->company_chief_post_name = $model->company->chief_post_name;
            $invoice->company_chief_lastname = $model->company->chief_lastname;
            $invoice->company_chief_firstname_initials = $model->company->chief_firstname_initials;
            $invoice->company_chief_patronymic_initials = $model->company->chief_patronymic_initials;
            $invoice->company_chief_accountant_lastname = $model->company->chief_accountant_lastname;
            $invoice->company_chief_accountant_firstname_initials = $model->company->chief_accountant_firstname_initials;
            $invoice->company_chief_accountant_patronymic_initials = $model->company->chief_accountant_patronymic_initials;
            $invoice->company_print_filename = $model->company->print_link;
            $invoice->company_chief_signature_filename = $model->company->chief_signature_link;
            $invoice->price_precision = 2;

            if (($contractor = Contractor::find()->andWhere(['and',
                    ['ITN' => $model->inn],
                    ['company_id' => $model->company_id],
                    ['is_deleted' => false],
                    ['type' => Contractor::TYPE_SELLER],
                ])->andFilterWhere(['PPC' => $model->kpp])->one()) == null
            ) {
                $contractor = new Contractor();
                $contractor->company_id = $model->company_id;
                $contractor->type = Contractor::TYPE_SELLER;
                $contractor->status = Contractor::ACTIVE;
                $contractor->company_type_id = $model->contractor_type_id;
                $contractor->name = $model->contractor_name;
                $contractor->director_name = $model->chief_name;
                $contractor->chief_accountant_is_director = true;
                $contractor->legal_address = $model->legal_address;
                $contractor->ITN = $model->inn;
                $contractor->PPC = $model->kpp;
                $contractor->current_account = $model->rs;
                $contractor->bank_name = $model->bank_name;
                $contractor->corresp_account = $model->ks;
                $contractor->BIC = $model->bik;
                $contractor->contact_is_director = true;
                $contractor->taxation_system = $model->total_amount_has_nds ? Contractor::WITH_NDS : Contractor::WITHOUT_NDS;
                $contractor->face_type = Contractor::TYPE_LEGAL_PERSON;
                if (!$contractor->save()) {
                    $db->transaction->rollBack();

                    return false;
                }
            }

            $invoice->contractor_id = $contractor->id;
            $invoice->contractor_name_short = $contractor->getTitle(true);
            $invoice->contractor_name_full = $contractor->getTitle(false);
            $invoice->contractor_address_legal_full = ($contractor->face_type == Contractor::TYPE_LEGAL_PERSON ? $contractor->legal_address : $contractor->physical_address);
            $invoice->contractor_bank_name = $contractor->bank_name;
            $invoice->contractor_bank_city = $contractor->bank_city;
            $invoice->contractor_bik = $contractor->BIC;
            $invoice->contractor_inn = $contractor->ITN;
            $invoice->contractor_kpp = $contractor->PPC;
            $invoice->contractor_ks = $contractor->corresp_account;
            $invoice->contractor_rs = $contractor->current_account;

            $invoice->total_amount_no_nds = $model->total_amount_no_nds;
            $invoice->total_amount_with_nds = $model->total_amount_with_nds;
            $invoice->total_amount_has_nds = $model->total_amount_has_nds;
            $invoice->total_amount_nds = $model->total_amount_nds;
            $invoice->total_order_count = $model->getUploadedDocumentOrders()->count();
            $invoice->company_checking_accountant_id = $model->company->mainCheckingAccountant->id;
            $invoice->nds_view_type_id = $model->nds_view_type_id;
            $invoice->basis_document_name = $model->base;
            $invoice->basis_document_number = $model->base_number;
            $invoice->basis_document_date = $model->base_date;

            $orderArray = [];
            foreach ($model->uploadedDocumentOrders as $uploadedDocumentOrder) {
                if (($product = Product::find()->andWhere(['like', 'title', $uploadedDocumentOrder->name])->one()) == null) {
                    $product = new Product();
                    $product->creator_id = Yii::$app->user->identity->id;
                    $product->company_id = $model->company_id;
                    $product->production_type = $uploadedDocumentOrder->production_type_id - 1;
                    $product->title = $uploadedDocumentOrder->name;
                    $product->product_unit_id = $uploadedDocumentOrder->product_unit_id;
                    $product->has_excise = 0;
                    $product->price_for_buy_nds_id = $model->total_amount_has_nds ?
                                                    (date('Y') > 2018 ? TaxRate::RATE_20 : TaxRate::RATE_18) :
                                                    TaxRate::RATE_WITHOUT;
                    $product->price_for_buy_with_nds = $uploadedDocumentOrder->price;
                    $product->price_for_sell_nds_id = $model->total_amount_has_nds ?
                                                    (date('Y') > 2018 ? TaxRate::RATE_20 : TaxRate::RATE_18) :
                                                    TaxRate::RATE_WITHOUT;;
                    $product->price_for_sell_with_nds = $uploadedDocumentOrder->price;
                    $product->country_origin_id = Country::COUNTRY_WITHOUT;
                    $product->group_id = ProductGroup::WITHOUT;
                    if (!$product->save()) {
                        $db->transaction->rollBack();

                        return false;
                    }
                }
                $order = OrderHelper::createOrderByProduct($product, $invoice, $uploadedDocumentOrder->count, $uploadedDocumentOrder->price);
                $orderArray[] = $order;
            }
            array_walk($orderArray, function ($order, $key) {
                $order->number = $key + 1;
            });
            $invoice->populateRelation('orders', $orderArray);
            if (!$invoice->save()) {
                $db->transaction->rollBack();

                return false;
            }
            foreach ($invoice->orders as $order) {
                $order->invoice_id = $invoice->id;
                if (!$order->save()) {
                    $db->transaction->rollBack();

                    return false;
                }
            }
            foreach ($model->uploadedDocumentOrders as $uploadedDocumentOrder) {
                $uploadedDocumentOrder->delete();
            }
            $model->delete();

            return $invoice->id;
        });
    }

    /**
     * @return bool
     */
    public function checkOnComplete()
    {
        if ($this->validate() && $this->getUploadedDocumentOrders()->exists()) {
            /* @var $uploadedDocumentOrder UploadedDocumentOrder */
            foreach ($this->getUploadedDocumentOrders()->all() as $uploadedDocumentOrder) {
                if (!$uploadedDocumentOrder->validate()) {
                    $this->is_complete = false;
                }
            }
            $this->is_complete = true;
        }

        return $this->save(true, ['is_complete']);
    }

    /**
     * @param $amount
     * @return int|string
     */
    public function convertAmount($amount)
    {
        if (is_numeric($amount)) {
            return intval($amount) . '00';
        } else {
            return intval(str_replace([',', ' '], '', $amount));
        }
    }

    /**
     * @param UploadedDocumentOrder $uploadedDocumentOrder
     * @return UploadedDocumentOrder
     */
    public function setOrderProductionType(UploadedDocumentOrder $uploadedDocumentOrder)
    {
        $serviceKeyWords = ['автоперевозка', 'анализ', 'аренда', 'ведение', 'выезд',
            'выполненные', 'грузоперевозки', 'дизайн', 'доставка', 'заправка',
            'изготовление', 'консультации', 'монтаж', 'настройка', 'обработка',
            'обслуживание', 'обучение', 'оказание', 'организация', 'планировка',
            'подключение', 'помощь', 'проведение', 'продвижение', 'проектирование',
            'проживание', 'работы', 'разгрузка', 'размещение', 'разработка', 'ремонт',
            'создание', 'техподдержка', 'уборка', 'услуг', 'хостинг', 'хранение',
            'шиномонтаж',];

        /* @var $product Product */
        if ($uploadedDocumentOrder->name && ($product = Product::find()
                ->andWhere(['company_id' => $uploadedDocumentOrder->company_id])
                ->andWhere(['like', 'title', $uploadedDocumentOrder->name])->one()) !== null
        ) {
            $uploadedDocumentOrder->production_type_id = $product->production_type + 1;
        } else {
            if ($uploadedDocumentOrder->name && (in_array($uploadedDocumentOrder->product_unit_id, ProductUnit::$crossUnits) || $uploadedDocumentOrder->product_unit_id == null)) {
                foreach ($serviceKeyWords as $keyWord) {
                    if (stristr($uploadedDocumentOrder->name, $keyWord) !== false) {
                        $uploadedDocumentOrder->production_type_id = Product::PRODUCTION_TYPE_SERVICE + 1;
                        break;
                    }
                }
                if ($uploadedDocumentOrder->production_type_id === null) {
                    $uploadedDocumentOrder->production_type_id = Product::PRODUCTION_TYPE_GOODS + 1;
                }
            } else {
                if (in_array($uploadedDocumentOrder->product_unit_id, ProductUnit::serviceUnitsIdArray())) {
                    $uploadedDocumentOrder->production_type_id = Product::PRODUCTION_TYPE_SERVICE + 1;
                } else {
                    $uploadedDocumentOrder->production_type_id = Product::PRODUCTION_TYPE_GOODS + 1;
                }
            }
        }

        return $uploadedDocumentOrder;
    }
}
