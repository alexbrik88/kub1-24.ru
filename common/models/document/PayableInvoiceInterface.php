<?php

namespace common\models\document;

/**
 * class PayableInvoiceInterface
 */
interface PayableInvoiceInterface
{
    /**
     * @return integer
     */
    public function getAvailablePaymentAmount();
    /**
     * @return integer
     */
    public function getPaidAmount();
    /**
     * @return integer
     */
    public function checkPaymentStatus(\common\models\cash\CashFlowsBase $flow);
}
