<?php

namespace common\models\document;

use common\components\DadataClient;
use common\components\date\DateHelper;
use common\components\date\DatePickerFormatBehavior;
use common\components\helpers\ModelHelper;
use common\components\pdf\PdfRenderer;
use common\components\pdf\Printable;
use common\components\TextHelper;
use common\components\TaxRobotHelper;
use common\models\cash\CashBankFlows;
use common\models\Company;
use common\models\company\CompanyType;
use common\models\company\CheckingAccountant;
use common\models\Contractor;
use common\models\dictionary\bik\BikDictionary;
use common\models\document\query\PaymentOrderQuery;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\status\PaymentOrderStatus;
use common\models\employee\Employee;
use common\models\TaxKbk;
use frontend\models\Documents;
use frontend\models\log\ILogMessage;
use frontend\models\log\Log;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\modules\cash\modules\banking\components\vidimus\VidimusBuilder;
use frontend\modules\cash\modules\banking\components\vidimus\VidimusUploader;
use frontend\modules\documents\assets\DocumentPrintAsset;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "payment_order".
 *
 * @property integer $id
 * @property integer $uid
 * @property integer $company_id
 * @property string $created_at
 * @property integer $document_author_id
 * @property string $document_number
 * @property string $document_date
 * @property integer $relation_with_in_invoice
 * @property integer $invoice_id
 * @property string $company_name
 * @property string $company_inn
 * @property string $company_kpp
 * @property string $company_bik
 * @property string $company_bank_name
 * @property string $company_bank_city
 * @property string $company_ks
 * @property string $company_rs
 * @property string $contractor_name
 * @property integer $contractor_inn
 * @property string $contractor_bank_name
 * @property string $contractor_bank_city
 * @property integer $contractor_current_account
 * @property integer $contractor_corresponding_account
 * @property integer $contractor_bik
 * @property integer $contractor_kpp
 * @property double $sum
 * @property string $sum_in_words
 * @property integer $ranking_of_payment
 * @property integer $operation_type_id
 * @property string $purpose_of_payment_for
 * @property string $purpose_of_payment
 * @property integer $presence_status_budget_payment
 * @property integer $taxpayers_status_id
 * @property integer $kbk
 * @property integer $oktmo_code
 * @property integer $payment_details_id
 * @property string $tax_period_code
 * @property string $document_number_budget_payment
 * @property string $document_date_budget_payment
 * @property integer $payment_type_id
 * @property integer $uin_code
 * @property string $payment_limit_date
 * @property integer $contractor_id
 * @property integer $payment_order_status_id
 * @property integer $payment_order_status_updated_at
 * @property integer $created_by_taxrobot
 * @property integer $sum_changed_by_user
 * @property string $banking_guid
 *
 * @property string $dateBudgetPayment
 *
 *
 * @property Company $company
 * @property Employee $documentAuthor
 * @property Invoice $invoice
 * @property OperationType $operationType
 * @property PaymentDetails $paymentDetails
 * @property PaymentType $paymentType
 * @property TaxpayersStatus $taxpayersStatus
 * @property PaymentOrderInvoice[] $paymentOrderInvoices
 * @property PaymentOrderInvoice $firstPaymentOrderInvoice
 * @property Contractor $contractor
 * @property PaymentOrderStatus $paymentOrderStatus
 */
class PaymentOrder extends \yii\db\ActiveRecord implements ILogMessage, Printable
{
    /**
     * @var bool
     */
    private $_date_zero_value = false;
    public $invoiceIDs = null;
    public $taxMaxSum;
    public $type = Documents::IO_TYPE_IN;

    /**
     * @param Company|int $company
     * @return string
     */
    public static function getNextDocumentNumber($company, $date = null)
    {
        $numQuery = self::find()->andWhere([
            'company_id' => is_int($company) ? $company : $company->id,
        ]);
        if ($date) {
            $year = date('Y', strtotime($date));
            $numQuery->andWhere([
                'between',
                'document_date',
                $year.'-01-01',
                $year.'-12-31',
            ]);
        }
        $maxNumber = $numQuery->max('document_number * 1');

        return (string)(1 + (int)$maxNumber);
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_order';
    }

    public static function find()
    {
        return new PaymentOrderQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
            ],
            [
                'class' => DatePickerFormatBehavior::className(),
                'attributes' => [
                    'document_date' => [
                        'message' => 'Дата документа указана неверно.',
                    ],
                    'payment_limit_date' => [
                        'message' => 'Срок платежа указан неверно.',
                    ],
                ],
            ],
        ];
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        $this->sum = TextHelper::parseMoneyInput($this->sum);

        if ($this->isAttributeChanged('sum', false)) {
            $this->sum = TextHelper::fixNumber($this->sum) * 100;
        }

        return parent::beforeValidate();
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert && !$this->document_author_id) {
                $this->document_author_id = Yii::$app->user->id;
            }
            $this->sum_changed_by_user = false;
            if ($this->isAttributeChanged('sum')) {
                $this->sum = round($this->sum);
                $this->sum_in_words = TextHelper::mb_ucfirst(trim(TextHelper::amountToWords($this->sum / 100)));
                if (!$insert && $this->getOldAttribute('sum') != $this->sum) {
                    $this->sum_changed_by_user = true;
                }
            }

            if ($insert || $this->isAttributeChanged('payment_order_status_id')) {
                $this->payment_order_status_updated_at = time();
            }
            if (!$this->company_name) {
                $this->company_name = $this->company->companyType->name_short . ' ' . $this->company->name_full;
            }
            if (!$this->company_inn) {
                $this->company_inn = $this->company->inn;
            }
            if (strval($this->company_kpp) === '') {
                $this->company_kpp = strlen($this->company->kpp) === 9 ? $this->company->kpp : 0;
            }

            if ($this->company_bik && !$this->company_bank_city && ($city = BikDictionary::cityByBik($this->company_bik))) {
                $this->company_bank_city = $city;
            }
            if ($this->contractor_bik && !$this->contractor_bank_city && ($city = BikDictionary::cityByBik($this->contractor_bik))) {
                $this->contractor_bank_city = $city;
            }

            return true;
        }

        return false;
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::beforeSave($insert, $changedAttributes);

        if ($insert) {
            LogHelper::log($this, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE);
        }
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [
                ['document_number', 'sum', 'taxpayers_status_id',
                    'contractor_inn', 'contractor_name', 'contractor_bank_name',
                    'contractor_bik', 'contractor_current_account',
                    'operation_type_id', 'purpose_of_payment', 'ranking_of_payment', 'payment_order_status_id',
                ], 'required',
            ],
            [
                ['contractor_id'], 'required',
                'when' => function (PaymentOrder $model) {
                    return $model->relation_with_in_invoice;
                },
                'whenClient' => 'function(){}',
            ],
            [
                ['kbk', 'oktmo_code', 'payment_details_id', 'tax_period_code',
                    'document_number_budget_payment', 'payment_type_id', 'uin_code',
                ], 'required',
                'when' => function (PaymentOrder $model) {
                    return $model->presence_status_budget_payment;
                },
                'whenClient' => 'function(){}',
            ],
            [
                ['dateBudgetPayment'], 'required',
                'when' => function (PaymentOrder $model) {
                    return $model->presence_status_budget_payment;
                },
                'whenClient' => 'function(){}',
            ],
            [['relation_with_in_invoice'], 'filter', 'filter' => function ($value) {
                return (int)$value;
            }],
            [
                ['relation_with_in_invoice', 'invoice_id',
                    'ranking_of_payment', 'operation_type_id', 'presence_status_budget_payment',
                    'taxpayers_status_id', 'kbk', 'payment_details_id',
                    'payment_type_id', 'contractor_id', 'payment_order_status_id',
                ], 'integer',
            ],
            [['kbk'], 'string', 'length' => 20, 'max' => 20,],
            [
                ['oktmo_code'], 'match', 'pattern' => '#^(\d{8}|\d{11}|0)$#',
                'message' => 'Значение «{attribute}» должно состоять из 8 или 11 цифр или должно быть равно 0.',
                'when' => function (PaymentOrder $model) {
                    return $model->presence_status_budget_payment &&
                        in_array($model->taxpayers_status_id, [TaxpayersStatus::DEFAULT_IP, TaxpayersStatus::DEFAULT_OOO]);
                },
            ],
            [['document_date', 'payment_limit_date', 'invoiceIDs'], 'safe'],
            [
                ['sum'],
                'number',
                'max' => Yii::$app->params['maxCashSum'] * 100, // with kopeck
                'tooBig' => 'Значение «{attribute}» не должно превышать ' . TextHelper::moneyFormat(Yii::$app->params['maxCashSum'], 2) . '.',
                'whenClient' => 'function(){}', // because of `123,00` - not valid float number (with comma) and there doesn't exist any methods to parse it on client.
            ],
            [['sum'], 'compare', 'operator' => '>', 'compareValue' => '0',
                'whenClient' => 'function(){}', // because of `123,00` - not valid float number (with comma) and there doesn't exist any methods to parse it on client.
            ],
            ['sum', function ($attribute, $params) {
                if ($this->taxMaxSum && $this->taxMaxSum < $this->$attribute) {
                    $message = Yii::t('yii', 'Сумма должна быть меньше или равна "{value}".');
                    $error = Yii::$app->getI18n()->format($message, [
                        'value' => number_format($this->taxMaxSum/100, 2, '.', ' '),
                    ], Yii::$app->language);
                    $this->addError($attribute, $error);
                }
            }],
            [
                ['document_number', 'contractor_name', 'contractor_bank_name',
                    'sum_in_words', 'purpose_of_payment_for', 'purpose_of_payment',
                    'tax_period_code', 'document_number_budget_payment',
                ], 'string', 'max' => 255,
            ],
            ['document_number', 'uniqueCompany'],

            [['uin_code'], 'string', 'max' => 10],
            [['contractor_inn', 'contractor_kpp', 'contractor_current_account', 'contractor_bik', 'contractor_corresponding_account',], 'integer'],
            [['contractor_current_account', 'contractor_corresponding_account'], 'string', 'length' => 20, 'max' => 20,],
            [ // inn
                ['contractor_inn'], 'string', 'min' => 10, 'max' => 12,
                'tooShort' => 'Значение ИНН должно содержать для ООО - 10 символов, для ИП - 12 символов.',
                'tooLong' => 'Значение ИНН должно содержать для ООО - 10 символов, для ИП - 12 символов.',
            ],
            [
                ['contractor_kpp'], 'required',
                'when' => function (PaymentOrder $model) {
                    if ($model->relation_with_in_invoice) {
                        /* @var $paymentOrderInvoice PaymentOrderInvoice */
                        $paymentOrderInvoice = $model->getPaymentOrderInvoices()->one();
                        if ($paymentOrderInvoice && $paymentOrderInvoice->invoice !== null
                            && $paymentOrderInvoice->invoice->contractor !== null
                            && $paymentOrderInvoice->invoice->contractor->company_type_id != CompanyType::TYPE_IP
                        ) {
                            return true;
                        }
                    }

                    return false;
                },
                'whenClient' => 'function(){}',
            ],
            [['company_bik', 'company_bank_name', 'company_rs'], 'required'],
            [['company_bik', 'company_bank_name', 'company_bank_city', 'contractor_bank_city', 'company_ks', 'company_rs'], 'string', 'max' => 255],
            [
                ['company_rs'], 'exist', 'skipOnError' => true,
                'targetClass' => CheckingAccountant::class,
                'targetAttribute' => [
                    'company_rs' => 'rs',
                    'company_ks' => 'ks',
                    'company_bik' => 'bik',
                    'company_id' => 'company_id',
                ],
                'message' => 'Значение «Реквизиты плательщика» неверно.'
            ],
        ];
    }

    /**
     *
     */
    public function afterFind()
    {
        parent::afterFind();

        $this->invoiceIDs = serialize($this->getPaymentOrderInvoices()->select(['invoice_id'])->column());
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function uniqueCompany($attribute, $params)
    {
        $query = self::find()
            ->byCompany(Yii::$app->user->identity->company->id)
            ->andWhere(['!=', self::tableName() . '.id', $this->id])
            ->andWhere([self::tableName() . '.document_number' => $this->$attribute]);

        if (!empty($this->document_date)) {
            $year = date('Y', strtotime($this->document_date));
            $query->andWhere(['between', self::tableName().'.document_date', $year.'-01-01', $year.'-12-31']);
        }
        $paymentOrder = $query->one();
        if ($paymentOrder !== null) {
            $this->addError($attribute, 'Такой номер уже существует, одинаковые номера не допустимы');
        }
    }

    /**
     * @return string
     */
    public static function generateUid()
    {
        $query = new \yii\db\Query;
        $query->select('id')->from(static::tableName())->where('[[uid]]=:uid');

        do {
            $uid = bin2hex(random_bytes(8));
        } while ($query->params([':uid' => $uid])->exists());

        return $uid;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Компания',
            'created_at' => 'Дата создания',
            'document_author_id' => 'Автор документа',
            'document_number' => 'Номер платёжного поручения',
            'document_date' => 'Дата документа',
            'relation_with_in_invoice' => 'Связано со счётом',
            'invoice_id' => 'Счёт',
            'contractor_name' => 'Название контрагента',
            'contractor_inn' => 'ИНН контрагента',
            'contractor_bank_name' => 'Название банка контрагента',
            'contractor_current_account' => 'Расчётный счёт контрагента',
            'contractor_corresponding_account' => 'Корреспондентский счёт контрагента',
            'contractor_bik' => 'БИК контрагента',
            'contractor_kpp' => 'КПП контрагента',
            'sum' => 'Сумма',
            'sum_in_words' => 'Сумма прописью',
            'ranking_of_payment' => 'Отчерёдность платежа',
            'operation_type_id' => 'Вид операции',
            'purpose_of_payment_for' => 'Назначение платежа "за"',
            'purpose_of_payment' => 'Назначение платежа',
            'presence_status_budget_payment' => 'Бюджетный платёж',
            'taxpayers_status_id' => 'Статус плательщика',
            'kbk' => 'КБК',
            'oktmo_code' => 'Код ОКТМО',
            'payment_details_id' => 'Основание платежа',
            'tax_period_code' => 'Код налогового периода',
            'document_number_budget_payment' => 'Документа',
            'document_date_budget_payment' => 'Дата платежа',
            'dateBudgetPayment' => 'Дата бюджетного платежа',
            'payment_type_id' => 'Тип платежа',
            'uin_code' => 'Код УИН',
            'payment_limit_date' => 'Срок платежа',
            'company_bik' => 'БИК плательщика',
            'company_bank_name' => 'Банк плательщика',
            'company_ks' => '',
            'company_rs' => 'Счет плательщика',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return common\models\employee\Employee
     */
    public function getAuthor()
    {
        if (Yii::$app->id == 'app-frontend' && $this->company->getEmployees()->andWhere(['id' => Yii::$app->user->id])->exists()) {
            return Yii::$app->user->getIdentity();
        } else {
            return $this->company->getEmployeeChief();
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentAuthor()
    {
        return $this->hasOne(Employee::className(), ['id' => 'document_author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(Invoice::className(), ['id' => 'invoice_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentOrderStatus()
    {
        return $this->hasOne(PaymentOrderStatus::className(), ['id' => 'payment_order_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentStatus()
    {
        return $this->getPaymentOrderStatus();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFirstPaymentOrderInvoice()
    {
        return $this->hasOne(PaymentOrderInvoice::className(), ['payment_order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentOrderInvoices()
    {
        return $this->hasMany(PaymentOrderInvoice::className(), ['payment_order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoices()
    {
        return $this->hasMany(Invoice::className(), ['id' => 'invoice_id'])->via('paymentOrderInvoices');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOperationType()
    {
        return $this->hasOne(OperationType::className(), ['id' => 'operation_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentDetails()
    {
        return $this->hasOne(PaymentDetails::className(), ['id' => 'payment_details_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContractor()
    {
        return $this->hasOne(Contractor::className(), ['id' => 'contractor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentType()
    {
        return $this->hasOne(PaymentType::className(), ['id' => 'payment_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxpayersStatus()
    {
        return $this->hasOne(TaxpayersStatus::className(), ['id' => 'taxpayers_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxKbk()
    {
        return $this->hasOne(TaxKbk::className(), ['id' => 'kbk']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpenditureItem()
    {
        $query = $this->hasOne(InvoiceExpenditureItem::className(), [
            'id' => 'expenditure_item_id',
        ])->viaTable(TaxKbk::tableName(), ['id' => 'kbk']);

        if ($this->kbk == '18210202140061110160') {
            $query->orderBy([
                'id' => mb_strpos($this->purpose_of_payment, '300') === false ? SORT_DESC : SORT_ASC,
            ]);
        }

        return $query;
    }

    /**
     * @param Log $log
     * @return string
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function getLogMessage(Log $log)
    {
        $title = 'Платёжное поручение №';
        $invoiceText = '';
        $invoice = Invoice::findOne($log->getModelAttributeNew('invoice_id'));

        if ($invoice) {
            $title .= $invoice->document_number . ' ' . $log->getModelAttributeNew('document_additional_number');
            $amount = TextHelper::invoiceMoneyFormat($invoice->total_amount_with_nds, 2) . ' руб.';
            $contractorName = $invoice->contractor ? ', ' . $invoice->contractor->getTitle(true) : '';
            $invoiceText = ', ' . $amount . $contractorName . ',';
        } else {
            $title .= $this->document_number;
        }

        $link = Html::a($title, ['/documents/payment-order/view',
            'type' => $log->getModelAttributeNew('type'),
            'id' => $log->getModelAttributeNew('id'),
        ]);

        switch ($log->log_event_id) {
            case LogEvent::LOG_EVENT_CREATE:
                return $link . $invoiceText . ' было создано.';
            case LogEvent::LOG_EVENT_DELETE:
                return $title . $invoiceText . ' было удалено.';
            case LogEvent::LOG_EVENT_UPDATE:
                return $link . $invoiceText . ' было изменено.';
            default:
                return $log->message;
        }
    }

    /**
     * @inheritdoc
     */
    public function getLogIcon(Log $log)
    {
        return Documents::getLogIcon($log);
    }

    /**
     * @inheritdoc
     */
    public function updateStatus($status, CashBankFlows $cashBankFlows = null)
    {
        if (in_array($status, PaymentOrderStatus::$statusArray)) {
            $deny = ArrayHelper::getValue(PaymentOrderStatus::$statusUpdateDanyAllow, $status);
            if (is_array($deny) && !in_array($this->payment_order_status_id, $deny)) {
                $updateAttributes = [
                    'payment_order_status_id' => $status,
                    'payment_order_status_updated_at' => time(),
                ];
                if ($cashBankFlows && !PaymentOrder::find()->where(['cash_bank_flows_id' => $cashBankFlows->id])->exists()) {
                    $updateAttributes['cash_bank_flows_id'] = $cashBankFlows->id;
                }

                if ($this->updateAttributes($updateAttributes)) {
                    LogHelper::log($this, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS);

                    return true;
                }
            }
        }

        return false;
    }

    /**
     * @return PaymentOrder
     */
    public function cloneSelf()
    {
        $clone = clone $this;
        $clone->isNewRecord = true;
        unset($clone->id);
        $clone->document_date = date(DateHelper::FORMAT_DATE);
        $clone->document_number = PaymentOrder::getNextDocumentNumber(Yii::$app->user->identity->company->id, $clone->document_date);
        $clone->document_author_id = Yii::$app->user->id;

        return $clone;
    }

    /**
     * @return string
     */
    public function getPdfFileName()
    {
        return $this->getPrintTitle() . '.pdf';
    }

    /**
     * @param $fileName
     * @param $outInvoice
     * @param string $destination
     * @param null $addStamp
     * @return PdfRenderer
     *
     */
    public static function getRenderer($fileName, $model, $destination = PdfRenderer::DESTINATION_STRING)
    {
        $renderer = new PdfRenderer([
            'view' => '@frontend/modules/documents/views/payment-order/pdf-view',
            'params' => array_merge([
                'model' => $model,
            ]),
            'destination' => $destination,
            'filename' => $fileName,
            'displayMode' => PdfRenderer::DISPLAY_MODE_FULLPAGE,
            'format' => 'A4-P',
        ]);
        \Yii::$app->controller->view->params['asset'] = DocumentPrintAsset::className();

        return $renderer;
    }

    public function getPrintTitle()
    {
        return str_replace(
            [' ', '"'],
            ['_'],
            'Платежное_поручение_№' . $this->document_number . '_' .
            date_format(date_create($this->document_date), 'd.m.Y') . '_' .
            $this->company_name ? : $this->company->getTitle(true)
        );
    }

    public function getAmountFormated()
    {
        return number_format($this->sum / 100, 2, '.', '');
    }

    public function getTaxpayersStatusCode()
    {
        return $this->taxpayersStatus ? $this->taxpayersStatus->code : '';
    }

    public function getOperationTypeCode()
    {
        return $this->operationType ? $this->operationType->code : '';
    }

    public function getReasonCode()
    {
        return $this->paymentDetails ? $this->paymentDetails->code : '';
    }

    public function getTypeCode()
    {
        return $this->paymentType ? $this->paymentType->code : '';
    }

    public function setDateBudgetPayment($value)
    {
        $this->document_date_budget_payment = ($date = date_create_from_format('d.m.Y', $value)) ? $date->format('Y-m-d') : null;
    }

    public function getDateBudgetPayment()
    {
        $date = null;
        if ($this->presence_status_budget_payment) {
            if ($this->document_date_budget_payment) {
                $date = date_create($this->document_date_budget_payment)->format('d.m.Y');
            } else {
                $date = '0';
            }
        }

        return $date;
    }

    /**
     * @throws \Exception
     * @throws \Throwable
     */
    public function loadInvoices()
    {
        $invoices = unserialize($this->invoiceIDs);
        $deleteInvoices = $this->getPaymentOrderInvoices()->andWhere(['not', ['invoice_id' => $invoices]])->all();
        foreach ($deleteInvoices as $deleteInvoice) {
            $deleteInvoice->delete();
        }
        if ($invoices) {
            foreach ($invoices as $invoiceID) {
                if (!$this->getPaymentOrderInvoices()
                    ->andWhere(['invoice_id' => $invoiceID])
                    ->andWhere(['payment_order_id' => $this->id])
                    ->exists()
                ) {
                    $paymentOrderInvoice = new PaymentOrderInvoice();
                    $paymentOrderInvoice->invoice_id = $invoiceID;
                    $paymentOrderInvoice->payment_order_id = $this->id;
                    $paymentOrderInvoice->save();
                }
            }
        }
    }

    /**
     * @return string
     */
    public function getUploadDirectory()
    {
        $filePath = DIRECTORY_SEPARATOR . self::tableName()
            . DIRECTORY_SEPARATOR . $this->id;
        $fullPath = Yii::getAlias('@common') . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'documents' . $filePath;
        if (!file_exists($fullPath)) {
            $parentDir = substr($fullPath, 0, strrpos($fullPath, DIRECTORY_SEPARATOR));
            if (!file_exists($parentDir)) {
                mkdir($parentDir);
            }
            mkdir($fullPath);
        }

        return $fullPath;
    }

    /**
     * @return false|int
     * @throws \Exception
     * @throws \Throwable
     */
    public function _delete()
    {
        foreach ($this->paymentOrderInvoices as $paymentOrderInvoice) {
            $paymentOrderInvoice->delete();
        }

        return $this->delete();
    }

    /**
     * @return false|int
     * @throws \Exception
     * @throws \Throwable
     */
    public function makePaid(\DateTime $date = null)
    {
        if ($date === null) {
            $date = new \DateTime();
        }
        $done = false;
        if ($this->payment_order_status_id != PaymentOrderStatus::STATUS_PAID) {
            $useTransaction = LogHelper::$useTransaction;
            LogHelper::$useTransaction = false;
            $done = Yii::$app->db->transaction(function ($db) use ($date) {
                if (($model = $this->createCashBankFlows($date)) !== null &&
                    $this->updateStatus(PaymentOrderStatus::STATUS_PAID, $model)
                ) {
                    return true;
                }
                $db->transaction->rollBack();

                return false;
            });
            LogHelper::$useTransaction = $useTransaction;

        }

        return $done;
    }

    /**
     * @return Contractor|null
     */
    protected function getCashContractor()
    {
        $type = Contractor::TYPE_SELLER;
        $name = $this->contractor_name;
        $inn = $this->contractor_inn;
        $kpp = $this->contractor_kpp;

        $model = VidimusBuilder::getVidimuseContractor($inn, $this->company->id, $name, $kpp, $type);

        if ($model === null) {
            $model = new Contractor();
            $model->loadDefaultValues();

            if ($contractorAttributes = DadataClient::getContractorAttributes($inn, $kpp)) {
                $model->setAttributes($contractorAttributes);
            } else {
                list($type_id, $contractorName) = VidimusUploader::getTypeName($name);
                $model->name = $contractorName;
                $model->PPC = $kpp;
                $model->ITN = $inn;
                $model->company_type_id = $type_id;
                $model->face_type = ($type_id || strlen($inn) === 10) ?
                                         Contractor::TYPE_LEGAL_PERSON :
                                         Contractor::TYPE_PHYSICAL_PERSON;
                $model->director_name = ($type_id == CompanyType::TYPE_IP) ?
                                             $contractorName :
                                             Contractor::DEFAULT_DIRECTOR_NAME;
                $model->legal_address = Contractor::DEFAULT_LEGAL_ADDRESS;

                if ($model->face_type == Contractor::TYPE_PHYSICAL_PERSON) {
                    $physical_name = explode(' ', $contractorName);
                    $model->physical_lastname = ArrayHelper::getValue($physical_name, 0, '');
                    $model->physical_firstname = ArrayHelper::getValue($physical_name, 1, '');
                    $model->physical_patronymic = ArrayHelper::getValue($physical_name, 2, '');
                    if (!$model->physical_patronymic) {
                        $model->physical_no_patronymic = true;
                    }
                }
            }

            $model->populateRelation('company', $this->company);
            $model->company_id = $this->company->id;
            $model->type = $type;

            $model->bank_name = $this->contractor_bank_name;
            $model->BIC = $this->contractor_bik;
            $model->corresp_account = $this->contractor_corresponding_account;
            $model->current_account = $this->contractor_current_account;

            $model->chief_accountant_is_director = true;
            $model->contact_is_director = true;
            $model->taxation_system = Contractor::WITHOUT_NDS;
        }
        $model->invoice_expenditure_item_id = $this->expenditureItem ?
            $this->expenditureItem->id : InvoiceExpenditureItem::ITEM_CONTRACTOR_PAYMENT;

        if ($model->company_id && ($model->isAttributeChanged('invoice_expenditure_item_id') || $model->getIsNewRecord())) {
            $model->save(false);
        }

        $bik = $this->contractor_bik;
        $rs = $this->contractor_current_account;
        if (!$model->getIsNewRecord() && $bik && $rs && !$model->hasAccount($bik, $rs)) {
            $model->addAccount($bik, $rs);
        }

        return !$model->getIsNewRecord() ? $model : null;
    }

    /**
     * @return CashBankFlows|null
     */
    protected function createCashBankFlows(\DateTime $date = null)
    {
        $model = CashBankFlows::find()->alias('flow')->leftJoin([
            'contractor' => Contractor::tableName(),
        ], '{{flow}}.[[contractor_id]] = {{contractor}}.[[id]]')->andWhere([
            'flow.company_id' => $this->company_id,
            'flow.amount' => $this->sum,
            'flow.description' => $this->purpose_of_payment,
            'contractor.ITN' => $this->contractor_inn,
        ])->andFilterWhere([
            'flow.kbk' => $this->kbk,
            'flow.tax_period_code' => $this->tax_period_code,
            'contractor.PPC' => $this->contractor_kpp,
        ])->one();

        if ($model !== null) {
            return $model;
        }

        $company = $this->company;
        if ($contractor = $this->getCashContractor()) {
            $model = new CashBankFlows([
                'scenario' => 'create',
                'flow_type' => CashBankFlows::FLOW_TYPE_EXPENSE,
                'company_id' => $this->company_id,
                'date' => $date ? $date->format('d.m.Y') : date_create($this->document_date)->format('d.m.Y'),
                'recognition_date' => $this->document_date,
                'amount' => bcdiv($this->sum, 100, 2),
                'description' => $this->purpose_of_payment,
                'expenditure_item_id' => $this->expenditureItem ?
                    $this->expenditureItem->id : InvoiceExpenditureItem::ITEM_CONTRACTOR_PAYMENT,
                'contractor_id' => $contractor->id,
                'payment_order_number' => (string) $this->document_number,
                'bank_name' => $this->company_bank_name,
                'rs' => $this->company_rs,
                'taxpayers_status_id' => $this->taxpayers_status_id,
                'kbk' => $this->kbk,
                'oktmo_code' => $this->oktmo_code,
                'payment_details_id' => $this->payment_details_id,
                'tax_period_code' => $this->tax_period_code,
                'document_number_budget_payment' => $this->document_number_budget_payment,
                'document_date_budget_payment' => $this->document_date_budget_payment,
                'payment_type_id' => $this->payment_type_id,
                'uin_code' => $this->uin_code,
            ]);

            if (LogHelper::save($model, LogEntityType::TYPE_CASH, LogEvent::LOG_EVENT_CREATE)) {
                return $model;
            } else {
                \common\components\helpers\ModelHelper::logErrors($model, __METHOD__);
            }
        }

        return null;
    }

    /**
     * @param Invoice $invoice In Invoice
     * @return PaymentOrder
     */
    public static function newByInInvoice(Invoice $invoice)
    {
        /* @var $contractor Contractor */
        $contractor = $invoice->contractor;
        $model = new PaymentOrder();
        $model->company_id = $invoice->company_id;
        $model->company_name = $invoice->company_name_short;
        $model->company_inn = $invoice->company_inn;
        $model->company_kpp = $invoice->company_kpp;
        $model->company_bik = $invoice->company_bik;
        $model->company_bank_name = $invoice->company_bank_name;
        $model->company_bank_city = $invoice->company_bank_city;
        $model->company_ks = $invoice->company_ks;
        $model->company_rs = $invoice->company_rs;
        $model->payment_order_status_id = PaymentOrderStatus::STATUS_CREATED;
        $model->document_author_id = Yii::$app->user->identity->id;
        $model->document_date = date(DateHelper::FORMAT_DATE);
        $model->document_number = PaymentOrder::getNextDocumentNumber($invoice->company, $model->document_date);
        $model->relation_with_in_invoice = true;
        $model->contractor_id = $contractor->id;
        $model->contractor_name = $contractor->getTitle(true);
        $model->contractor_inn = $contractor->ITN;
        $model->contractor_bank_name = $contractor->bank_name;
        $model->contractor_bank_city = $contractor->bank_city;
        $model->contractor_current_account = $contractor->current_account;
        $model->contractor_corresponding_account = $contractor->corresp_account;
        $model->contractor_bik = $contractor->BIC;
        $model->contractor_kpp = $contractor->PPC;
        $model->sum = $invoice->remaining_amount / 100;
        $model->ranking_of_payment = 5;
        $model->operation_type_id = OperationType::TYPE_PAYMENT_ORDER;
        $model->purpose_of_payment = $invoice->getPurposeOfPayment();
        $model->taxpayers_status_id = $invoice->company->getIsLikeIP() ?
                                      TaxpayersStatus::DEFAULT_IP :
                                      TaxpayersStatus::DEFAULT_OOO;

        return $model;
    }

    /**
     * @param Invoice $invoice Out Invoice
     * @return PaymentOrder
     */
    public static function createFromOutInvoice(Invoice $invoice)
    {
        return new PaymentOrder([
            'document_number' => 1,
            'company_name' => $invoice->contractor_name_short,
            'company_inn' => $invoice->contractor_inn,
            'company_kpp' => $invoice->contractor_kpp,
            'company_bik' => $invoice->contractor_bik,
            'company_bank_name' => $invoice->contractor_bank_name,
            'company_bank_city' => $invoice->contractor_bank_city,
            'company_rs' => $invoice->contractor_rs,
            'contractor_name' => $invoice->company_name_short,
            'contractor_inn' => $invoice->company_inn,
            'contractor_bank_name' => $invoice->company_bank_name,
            'contractor_bank_city' => $invoice->company_bank_city,
            'contractor_current_account' => $invoice->company_rs,
            'contractor_corresponding_account' => $invoice->company_ks,
            'contractor_bik' => $invoice->company_bik,
            'contractor_kpp' => $invoice->company_kpp,
            'sum' => $invoice->total_amount_with_nds,
            'sum_in_words' => TextHelper::mb_ucfirst(TextHelper::amountToWords($invoice->total_amount_with_nds / 100)),
            'ranking_of_payment' => 5,
            'operation_type_id' => OperationType::TYPE_PAYMENT_ORDER,
            'purpose_of_payment' => $invoice->getPurposeOfPayment(),
            'taxpayers_status_id' => $invoice->contractor->getIsLikeIP() ?
                                     TaxpayersStatus::DEFAULT_IP :
                                     TaxpayersStatus::DEFAULT_OOO,
        ]);
    }

    /**
     * @return  string
     */
    public function getCompanyBankCity()
    {
        return !empty($this->company_bank_city) ? 'г. '.$this->company_bank_city : null;
    }

    /**
     * @return  string
     */
    public function getContractorBankCity()
    {
        return !empty($this->contractor_bank_city) ? 'г. '.$this->contractor_bank_city : null;
    }
}
