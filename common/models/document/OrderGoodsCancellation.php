<?php

namespace common\models\document;

use common\models\TaxRate;
use Yii;
use common\models\product\Product;
use common\models\product\ProductUnit;
use yii\helpers\Html;

/**
 * This is the model class for table "order_goods_cancellation".
 *
 * @property int $id
 * @property int $goods_cancellation_id
 * @property int $product_id
 * @property int|null $unit_id
 * @property float $quantity
 * @property float|null $price_with_vat
 * @property int $tax_rate_id
 * @property string $product_title
 * @property string $product_code
 * @property string $article
 * @property int|null $country_id
 * @property string $custom_declaration_number
 * @property int $price
 * @property int $number
 *
 * @property GoodsCancellation $goodsCancellation
 * @property Product $product
 * @property ProductUnit $unit
 * @property TaxRate $taxRate
 */
class OrderGoodsCancellation extends \yii\db\ActiveRecord
{
    const MAX_PRICE = 999999999999999999.9999;
    const MIN_PRICE = 0;
    const MAX_QUANTITY = 9999999999.9999999999;
    const MIN_QUANTITY = 0.0000000001;
    const MAX_DISCOUNT = 0;
    const MAX_MARKUP = 0;

    public $max_product_count;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_goods_cancellation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['number', 'price', 'count', 'title'], 'required'],
            [['price', 'quantity'], 'filter', 'filter' => function ($value) {
                return str_replace(",", ".", $value);
            }],
            [
                ['price'], 'number',
                'numberPattern' => '/^\d+(\.\d*)?$/',
                'min' => self::MIN_PRICE,
                'max' => self::MAX_PRICE,
            ],
            [
                ['quantity'], 'number',
                'numberPattern' => '/^\d+(\.\d*)?$/',
                'min' => self::MIN_QUANTITY,
                'max' => self::MAX_QUANTITY,
            ],
            [['goods_cancellation_id', 'product_id', 'unit_id', 'tax_rate_id'], 'integer'],
            [['goods_cancellation_id'], 'exist', 'skipOnError' => true, 'targetClass' => GoodsCancellation::class, 'targetAttribute' => ['goods_cancellation_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::class, 'targetAttribute' => ['product_id' => 'id']],
            [['unit_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductUnit::class, 'targetAttribute' => ['unit_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::class, 'targetAttribute' => ['product_id' => 'id']],
            [['tax_rate_id'], 'exist', 'skipOnEmpty' => true, 'skipOnError' => true, 'targetClass' => TaxRate::class, 'targetAttribute' => ['tax_rate_id' => 'id']],
            [['product_title', 'product_code', 'article', 'country_id', 'custom_declaration_number'], 'safe'],
            [['unit_id', 'tax_rate_id'], 'filter', 'filter' => function ($value) {
                return intval($value) ?: null;
            }],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'goods_cancellation_id' => 'ID документа списания',
            'product_id' => 'Товар',
            'unit_id' => 'Ед. измерения',
            'quantity' => 'Количество',
            'price' => 'Цена закупки',
            'tax_rate_id' => 'НДС',
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     * @throws \yii\web\ForbiddenHttpException
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->product->setCountOrderSale($this->quantity, $this->goodsCancellation->store_id);
            } else {
                $quantityDiff = $this->quantity - $this->getOldAttribute('quantity');
                if ($quantityDiff !== 0) {
                    $this->product->setCountOrderSale($quantityDiff, $this->goodsCancellation->store_id);
                }
            }

            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $this->product->setCountOrderBuy($this->quantity, $this->goodsCancellation->store_id);

            return true;
        }

        return false;
    }

    /**
     * Gets query for [[GoodsCancellation]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGoodsCancellation()
    {
        return $this->hasOne(GoodsCancellation::class, ['id' => 'goods_cancellation_id']);
    }

    /**
     * Gets query for [[Product]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::class, ['id' => 'product_id']);
    }

    /**
     * Gets query for [[Unit]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUnit()
    {
        return $this->hasOne(ProductUnit::class, ['id' => 'unit_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxRate()
    {
        return $this->hasOne(TaxRate::class, ['id' => 'tax_rate_id']);
    }

    /**
     * @return int
     */
    public function getProductionType()
    {
        return Product::PRODUCTION_TYPE_GOODS;
    }

    /**
     * @return bool
     */
    public function getIsDeleteAllowed()
    {
        return true;
    }

    /**
     * @return string
     */
    public function getProductTitle()
    {
        return Html::encode($this->product_title);
    }

    /**
     * @return string
     */
    public function getNdsRate()
    {
        return $this->taxRate ? $this->taxRate->rate : 0;
    }


    /**
     * @param $value
     */
    public function setPrice($value)
    {
        $owner = $this->goodsCancellation;
        $ndsViewType = $owner->nds_view_type_id;
        $hasNds = $ndsViewType != Invoice::NDS_VIEW_WITHOUT;
        $ndsOut = $ndsViewType == Invoice::NDS_VIEW_OUT;
        $ndsRate = floatval($this->getNdsRate());

        $value = strtr($value, [',' => '.']);
        if (!is_numeric($value)) {
            $value = 0;
        }

        $this->view_price_base = max(0, $value * 100);
        //$price = $owner->toRub($this->view_price_base); // todo
        $price = $this->view_price_base;

        $this->base_price_no_vat = round($ndsOut ? $price : ($hasNds ? $price / (1 + $ndsRate) : $price), 2);
        $this->base_price_with_vat = round(!$ndsOut ? $price : ($hasNds ? $price * (1 + $ndsRate) : $price), 2);
    }

    /**
     * @return string
     */
    public function getPrice()
    {
        return $this->view_price_base / 100;
    }

    /**
     * @return string
     */
    public function getBasePrice()
    {
        if ($this->goodsCancellation->nds_view_type_id == Invoice::NDS_VIEW_OUT) {
            return $this->base_price_no_vat;
        } else {
            return $this->base_price_with_vat;
        }
    }

    /**
     * @return integer
     */
    public function getPriceOne()
    {
        $owner = $this->goodsCancellation;
        if ($owner->nds_view_type_id == Invoice::NDS_VIEW_OUT) {
            return $this->purchase_price_no_vat;
        } else {
            return $this->purchase_price_with_vat;
        }
    }

    /**
     * @param $value
     */
    public function setTitle($value)
    {
        $this->product_title = $value;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->product_title;
    }

    /**
     * @param $value
     */
    public function setCount($value)
    {
        $this->quantity = $value;
    }

    /**
     * @return float
     */
    public function getCount()
    {
        return $this->quantity;
    }

    /**
     * @param $price
     * @param $discount
     * @param $markup
     * @param int $discountType
     * @return float|int
     */
    public static function priceOne($price, $discount, $markup, $discountType = Invoice::DISCOUNT_TYPE_PERCENT)
    {
        if ($discount > 0) {
            if ($discountType == Invoice::DISCOUNT_TYPE_PERCENT) {
                return $price * (100 - $discount) / 100;
            } else {
                return $price - ($discount * 100);
            }
        } elseif ($markup > 0) {
            return $price * (100 + $markup) / 100;
        }

        return $price;
    }

    /**
     *
     */
    public function calculateOrder()
    {
        $ndsViewType = $this->goodsCancellation->nds_view_type_id;
        $hasNds = $ndsViewType != Invoice::NDS_VIEW_WITHOUT;
        $ndsOut = $ndsViewType == Invoice::NDS_VIEW_OUT;
        $rate = $this->getNdsRate();
        $precision = $this->goodsCancellation->price_precision == 4 ? 2 : 0;

        $price = $this->calculatePrice($this->view_price_base, $precision);
        $this->view_price_one = $price['priceOne'];
        $this->view_total_base = $price['totalBase'];
        $this->view_total_amount = $price['totalAmount'];
        $this->view_total_no_nds = $price['totalNoNds'];
        $this->view_total_nds = $price['totalNds'];
        $this->view_total_with_nds = $price['totalWithNds'];

        if ($this->goodsCancellation->has_markup) {
            $this->markup = max(0, min(self::MAX_MARKUP, $this->markup));
            $this->discount = 0;
        } else {
            $this->markup = 0;
            if ($this->goodsCancellation->has_discount) {
                if ($this->goodsCancellation->discount_type == Invoice::DISCOUNT_TYPE_PERCENT) {
                    $this->discount = max(0, min(self::MAX_DISCOUNT, $this->discount));
                }
            } else {
                $this->discount = 0;
            }
        }
        $discount = $this->discount;
        $markup = $this->markup;
        $priceOne = round(self::priceOne($this->getBasePrice(), $discount, $markup, $this->goodsCancellation->discount_type), $precision);
        $priceDetails = Order::calculateOrderPriceDetails(
            $priceOne,
            $this->quantity,
            $this->goodsCancellation->nds_view_type_id,
            $this->getNdsRate(),
            $precision
        );

        $this->purchase_price_no_vat = $priceDetails['priceOneNoNds'];
        $this->purchase_price_with_vat = $priceDetails['priceOneWithNds'];
        $this->amount_purchase_no_vat = $priceDetails['totalNoNds'];
        $this->purchase_tax = $priceDetails['totalNds'];
        $this->amount_purchase_with_vat = $priceDetails['totalWithNds'];
    }

    /**
     * @return array
     */
    public function calculatePrice($priceBase, $precision)
    {
        $discount = 0;
        $markup = 0;
        $discountType = $this->goodsCancellation->discount_type;
        $quantity = floatval($this->quantity) == intval($this->quantity) ? intval($this->quantity) : floatval($this->quantity);
        if ($this->goodsCancellation->has_markup) {
            $markup = max(0, min(self::MAX_MARKUP, $this->markup));
        } else {
            if ($this->goodsCancellation->has_discount) {
                if ($discountType == Invoice::DISCOUNT_TYPE_PERCENT) {
                    $discount = max(0, min(self::MAX_DISCOUNT, $this->discount));
                } else {
                    $discount = $this->discount;
                }
            } else {
                $discount = 0;
            }
        }

        $priceOne = round(self::priceOne($priceBase, $discount, $markup, $this->goodsCancellation->discount_type), $precision);

        $priceDetails = self::calculateOrderPriceDetails(
            $priceOne,
            $quantity,
            $this->goodsCancellation->nds_view_type_id,
            $this->getNdsRate(),
            $precision
        );

        return [
            'priceBase' => $priceBase,
            'totalBase' => round($priceBase * $quantity, $precision),
            'priceOne' => $priceDetails['priceOne'],
            'totalAmount' => $priceDetails['totalAmount'],
            'totalNoNds' => $priceDetails['totalNoNds'],
            'totalNds' => $priceDetails['totalNds'],
            'totalWithNds' => $priceDetails['totalWithNds'],
        ];
    }

    /**
     * @return array
     */
    public static function calculateOrderPriceDetails($price, $quantity, $ndsViewType, $ndsRate, $precision = 2)
    {
        if ($ndsRate && $ndsViewType == Invoice::NDS_VIEW_WITHOUT) {
            $ndsRate = 0;
        }
        $quantity = floatval($quantity) == intval($quantity) ? intval($quantity) : floatval($quantity);
        $ndsRate = floatval($ndsRate);
        $hasNds = $ndsViewType != Invoice::NDS_VIEW_WITHOUT;
        $ndsOut = $ndsViewType == Invoice::NDS_VIEW_OUT;
        $totalAmount = round($price * $quantity, (int) $precision);
        if ($ndsOut) {
            $totalNoNds = $totalAmount;
            $totalNds = $hasNds ? round($totalNoNds * $ndsRate, $precision) : 0;
            $totalWithNds = round($totalNoNds + $totalNds, $precision);
        } else {
            $totalWithNds = $totalAmount;
            $totalNds = $hasNds ? round($totalWithNds * $ndsRate / (1 + $ndsRate), $precision) : 0;
            $totalNoNds = round($totalWithNds - $totalNds, $precision);
        }

        return [
            'priceOne' => $price,
            'priceOneNoNds' => round($ndsOut ? $price : ($hasNds ? $price / (1 + $ndsRate) : $price), $precision),
            'priceOneWithNds' => round(!$ndsOut ? $price : ($hasNds ? $price * (1 + $ndsRate) : $price), $precision),
            'totalAmount' => $totalAmount,
            'totalNoNds' => $totalNoNds,
            'totalNds' => $totalNds,
            'totalWithNds' => $totalWithNds,
        ];
    }
    
    /**
     * Unused properties
     */
    public function setDiscount($value){ return; }
    public function setMarkup($value){ return; }
    public function setWeight($value){ return; }
    public function setVolume($value){ return; }
    public function getDiscount(){ return 0; }
    public function getMarkup(){ return 0; }
    public function getWeight(){ return 0; }
    public function getVolume(){ return 0; }
}
