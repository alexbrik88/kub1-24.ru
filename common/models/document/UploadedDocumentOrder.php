<?php

namespace common\models\document;

use common\models\product\ProductType;
use common\models\product\ProductUnit;
use Yii;
use common\models\Company;

/**
 * This is the model class for table "uploaded_document_order".
 *
 * @property integer $id
 * @property integer $company_id
 * @property integer $uploaded_document_id
 * @property string $name
 * @property integer $count
 * @property integer $product_unit_id
 * @property string $price
 * @property string $amount
 * @property integer $production_type_id
 *
 * @property Company $company
 * @property UploadedDocuments $uploadedDocument
 * @property ProductType $productType
 * @property ProductUnit $productUnit
 */
class UploadedDocumentOrder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'uploaded_document_order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'uploaded_document_id', 'name', 'count', 'price', 'amount', 'product_unit_id', 'production_type_id'], 'required'],
            [['company_id', 'uploaded_document_id', 'product_unit_id', 'production_type_id'], 'integer'],
            [['count'], 'integer', 'min' => 1],
            [['price', 'amount'], 'integer', 'min' => 1],
            [['name'], 'string', 'max' => 700],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['uploaded_document_id'], 'exist', 'skipOnError' => true, 'targetClass' => UploadedDocuments::className(), 'targetAttribute' => ['uploaded_document_id' => 'id']],
            [['production_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductType::className(), 'targetAttribute' => ['production_type_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'uploaded_document_id' => 'Uploaded Document ID',
            'name' => 'Name',
            'count' => 'Count',
            'product_unit_id' => 'Product Unit ID',
            'price' => 'Price',
            'amount' => 'Amount',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUploadedDocument()
    {
        return $this->hasOne(UploadedDocuments::className(), ['id' => 'uploaded_document_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductType()
    {
        return $this->hasOne(ProductType::className(), ['id' => 'production_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProductUnit()
    {
        return $this->hasOne(ProductUnit::className(), ['id' => 'product_unit_id']);
    }
}
