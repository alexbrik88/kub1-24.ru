<?php

namespace common\models\document;

use common\components\helpers\ModelHelper;
use common\models\currency\Currency;
use common\models\EmployeeCompany;
use common\models\product\Product;
use DateTime;
use common\components\excel\Excel;
use common\components\pdf\PdfRenderer;
use common\components\TextHelper;
use common\components\date\DateHelper;
use common\components\date\DatePickerFormatBehavior;
use common\components\helpers\ArrayHelper;
use common\models\Contractor;
use common\models\document\DocumentType;
use common\models\document\query\InvoiceFactureQuery;
use common\models\document\status\InvoiceFactureStatus;
use common\models\employee\Employee;
use common\models\file\File;
use frontend\models\Documents;
use frontend\models\log\Log;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\modules\documents\assets\DocumentPrintAsset;
use frontend\modules\documents\components\InvoiceHelper;
use frontend\modules\documents\components\Message;
use frontend\modules\documents\forms\InvoiceSendForm;
use frontend\modules\export\models\export\Export;
use frontend\modules\export\models\one_c\OneCExport;
use Yii;
use yii\base\Exception;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This is the model class for table "invoice_facture".
 *
 * @property integer $invoice_id // both
 * @property integer $created_at // both
 * @property integer $status_out_id // out
 * @property string $status_out_updated_at // out
 * @property string $status_out_author_id // out
 * @property string $document_number // in
 * @property string $document_additional_number // out
 * @property string $state_contract
 * @property string $show_service_units
 * @property boolean $contractor_address
 * @property boolean $add_stamp
 * @property integer $auto_created
 *
 * Relations
 * @property Invoice $invoice
 * @property status\InvoiceFactureStatus $statusOut
 * @property Employee $statusOutAuthor
 * @property InvoiceFacturePaymentDocument $paymentDocuments
 * @property Contractor $consignee
 * @property Contractor $consignor
 * @property InvoiceInvoiceFacture[] $invoiceInvoiceFactures
 * @property Invoice[] $invoices
 *
 * Additional properties
 * @property string $fullNumber
 * @property string $printablePaymentDocuments
 * @property string $printableShippingDocuments
 */
class InvoiceFacture extends AbstractDocument
{
    const PAYMENT_DOCUMENT_DEFAULT_VALUE = '---';

    /**
     * @var string
     */
    public static $uploadDirectory = 'invoice_facture';

    /**
     * @var string
     */
    public $printablePrefix = 'Счет-фактура';
    public $shortPrefix = 'СФ';

    /**
     * @var string
     */
    public $urlPart = 'invoice-facture';

    private $beforeDeleteInvoices = [];

    const CONTRACTOR_ADDRESS_LEGAL = 0;
    const CONTRACTOR_ADDRESS_ACTUAL = 1;

    public $_isGroupOwnOrdersByProduct;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invoice_facture';
    }

    /**
     * @return InvoiceFactureQuery
     */
    public static function find()
    {
        return new InvoiceFactureQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'documentDateBehavior' => [
                'class' => DatePickerFormatBehavior::className(),
                'attributes' => [
                    /*'payment_document_date' => [
                        'message' => 'Дата оплаты указана неверно.',
                    ],*/
                ],
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            /** INSERT */
            [['invoice_id'], 'required', 'on' => 'insert',],
            [['invoice_id'], 'integer', 'on' => 'insert',],

            // type depending
            // out
            [['status_out_id', 'contractor_address'], 'integer',
                'when' => function (InvoiceFacture $model) {
                    return $model->type == Documents::IO_TYPE_OUT;
                },
            ],
            [['document_additional_number'], 'string', 'max' => 45,
                'when' => function (InvoiceFacture $model) {
                    return $model->type == Documents::IO_TYPE_OUT;
                },
            ],
            [['state_contract'], 'string', 'max' => 25,
                'when' => function (InvoiceFacture $model) {
                    return $model->type == Documents::IO_TYPE_OUT;
                },
            ],
            [['show_service_units'], 'boolean',
                'when' => function (InvoiceFacture $model) {
                    return $model->type == Documents::IO_TYPE_OUT;
                },
            ],
            [['document_number',], 'integer',
                'when' => function (InvoiceFacture $model) {
                    return $model->type == Documents::IO_TYPE_OUT;
                },
            ],
            [['document_number',], 'outDocumentNumberValidator',
                'when' => function (InvoiceFacture $model) {
                    return $model->type == Documents::IO_TYPE_OUT;
                },
            ],
            [['consignor_id', 'consignee_id'], 'integer',
                'when' => function (InvoiceFacture $model) {
                    return $model->type == Documents::IO_TYPE_OUT;
                },
            ],
            // in
            [['document_number',], 'inDocumentNumberValidator',
                'when' => function (InvoiceFacture $model) {
                    return $model->type == Documents::IO_TYPE_IN;
                },
            ],
        ]);
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function inDocumentNumberValidator($attribute, $params)
    {
        $query = $this->invoice->getInvoiceFactures()
            ->andWhere(['!=', 'id', $this->id])
            ->andWhere([
                'document_number' => $this->$attribute,
            ]);

        if ($this->document_date) {
            $year = date('Y', strtotime($this->document_date));
            $query->andWhere([
                'between',
                'document_date',
                $year.'-01-01',
                $year.'-12-31',
            ]);
        }

        if ($this->document_additional_number) {
            $query->andWhere(['document_additional_number' => $this->document_additional_number]);
        } else {
            $query->andWhere(['or',
                ['document_additional_number' => ''],
                ['document_additional_number' => null],
            ]);
        }

        if ($query->exists()) {
            $this->addError($attribute, 'Такой номер уже существует, одинаковые номера не допустимы');
        }
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function outDocumentNumberValidator($attribute, $params)
    {
        $invoiceId = $this->isNewRecord ? $this->invoice_id : ArrayHelper::getColumn($this->invoices, 'id');
        $companyId = $this->invoice->company_id;
        $isNumberExists = function ($number) use ($companyId, $invoiceId) {
            $query1 = self::find()->alias('doc')->joinWith(['invoices'])
                ->andFilterWhere(['not', ['doc.id' => $this->id]])
                ->andWhere([
                    'invoice.is_deleted' => false,
                    'invoice.company_id' => $companyId,
                    'doc.type' => $this->type,
                    'doc.document_number' => $number,
                ]);
            $query2 = Act::find()->alias('doc')->joinWith(['invoices'])
                ->andWhere(['not', ['invoice.id' => $invoiceId]])
                ->andWhere([
                    'invoice.is_deleted' => false,
                    'invoice.company_id' => $companyId,
                    'doc.type' => $this->type,
                    'doc.document_number' => $number,
                ]);
            $query3 = PackingList::find()->alias('doc')->joinWith(['invoice'])
                ->andWhere(['not', ['invoice.id' => $invoiceId]])
                ->andWhere([
                    'invoice.is_deleted' => false,
                    'invoice.company_id' => $companyId,
                    'doc.type' => $this->type,
                    'doc.document_number' => $number,
                ]);
            $query4 = Upd::find()->alias('doc')->joinWith(['invoice'])
                ->andWhere(['not', ['invoice.id' => $invoiceId]])
                ->andWhere([
                    'invoice.is_deleted' => false,
                    'invoice.company_id' => $companyId,
                    'doc.type' => $this->type,
                    'doc.document_number' => $number,
                ]);

            if ($this->document_date) {
                $year = date('Y', strtotime($this->document_date));
                $query1->andWhere(['between', 'doc.document_date', $year.'-01-01', $year.'-12-31']);
                $query2->andWhere(['between', 'doc.document_date', $year.'-01-01', $year.'-12-31']);
                $query3->andWhere(['between', 'doc.document_date', $year.'-01-01', $year.'-12-31']);
                $query4->andWhere(['between', 'doc.document_date', $year.'-01-01', $year.'-12-31']);
            }
            if ($this->document_additional_number) {
                $query1->andWhere(['doc.document_additional_number' => $this->document_additional_number]);
                $query2->andWhere(['doc.document_additional_number' => $this->document_additional_number]);
                $query3->andWhere(['doc.document_additional_number' => $this->document_additional_number]);
                $query4->andWhere(['doc.document_additional_number' => $this->document_additional_number]);
            } else {
                $query1->andWhere(['or',
                    ['doc.document_additional_number' => ''],
                    ['doc.document_additional_number' => null],
                ]);
                $query2->andWhere(['or',
                    ['doc.document_additional_number' => ''],
                    ['doc.document_additional_number' => null],
                ]);
                $query3->andWhere(['or',
                    ['doc.document_additional_number' => ''],
                    ['doc.document_additional_number' => null],
                ]);
                $query4->andWhere(['or',
                    ['doc.document_additional_number' => ''],
                    ['doc.document_additional_number' => null],
                ]);
            }

            return $query1->exists() || $query2->exists() || $query3->exists() || $query4->exists();
        };

        if ($isNumberExists($this->$attribute)) {
            if ($this->findFreeNumber) {
                $nextNumber = $this->$attribute;
                do {
                    $nextNumber++;
                } while ($isNumberExists($nextNumber));
            } else {
                $nextNumber = null;
            }
            $this->addError($attribute, "Номер {$this->$attribute} занят. Укажите свободный номер {$nextNumber}");
        }
    }

    /**
     * Generate document number
     */
    public function newDocumentNumber()
    {
        $this->document_number = $this->document_additional_number = null;

        $query1 = $this->invoice->getActs()
            ->select(new Expression('[[document_number]], IF([[document_additional_number]] = "", NULL, [[document_additional_number]]) [[additional_number]]'));
        $query2 = $this->invoice->getPackingLists()
            ->select(new Expression('[[document_number]], IF([[document_additional_number]] = "", NULL, [[document_additional_number]]) [[additional_number]]'));
        $query3 = $this->invoice->getUpds()
            ->select(new Expression('[[document_number]], IF([[document_additional_number]] = "", NULL, [[document_additional_number]]) [[additional_number]]'));
        $query = (new Query)
            ->select(['document_number', 'additional_number'])
            ->from(['t' => $query1->union($query2)->union($query3)])
            ->orderBy([
                'document_number' => SORT_ASC,
                'additional_number' => SORT_ASC,
            ]);

        if ($rowArray = $query->all()) {
            foreach ($rowArray as $row) {
                $this->document_number = $row['document_number'];
                $this->document_additional_number = $row['additional_number'];
                if ($this->validate(['document_number'])) {
                    break;
                } else {
                    $this->document_number = $this->document_additional_number = null;
                }
            }
        }

        if (!$this->document_number) {
            $this->document_number = $this->getMaxDocumentNumber();
            do {
                $this->document_number++;
                $this->document_number .= '';
            } while (!$this->validate(['document_number']));
        }
    }

    /**
     * @return boolean
     */
    public function getIsEditableNumber()
    {
        if ($this->isNewRecord || $this->type == Documents::IO_TYPE_IN) {
            return true;
        } else {
            $query1 = $this->invoice->getActs()->andWhere(['document_number' => $this->document_number]);
            $query2 = $this->invoice->getPackingLists()->andWhere(['document_number' => $this->document_number]);
            $query3 = $this->invoice->getUpds()->andWhere(['document_number' => $this->document_number]);
            if ($this->document_additional_number) {
                $query1->andWhere(['document_additional_number' => $this->document_additional_number]);
                $query2->andWhere(['document_additional_number' => $this->document_additional_number]);
                $query3->andWhere(['document_additional_number' => $this->document_additional_number]);
            } else {
                $query1->andWhere(['or',
                    ['document_additional_number' => ''],
                    ['document_additional_number' => null],
                ]);
                $query2->andWhere(['or',
                    ['document_additional_number' => ''],
                    ['document_additional_number' => null],
                ]);
                $query3->andWhere(['or',
                    ['document_additional_number' => ''],
                    ['document_additional_number' => null],
                ]);
            }

            return (!$query1->exists() && !$query2->exists() && !$query3->exists());
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Тип', // in/out
            'invoice_id' => 'Счёт', // both
            'status_out_id' => 'Статус счёт-фактуры', // for out
            'status_out_updated_at' => 'Дата изменения статуса', // for out
            'status_out_author_id' => 'Автор изменения статуса', // for out
            'document_date' => 'Дата счёт-фактуры', // both
            'document_number' => 'Номер счёта', // both
            'document_additional_number' => 'Дополнительный номер счёта', // for out
            'state_contract' => 'Гос.контракт, договор (соглашение)',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(Invoice::className(), ['id' => 'invoice_id'])->viaTable('invoice_invoice_facture', ['invoice_facture_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceInvoiceFactures()
    {
        return $this->hasMany(InvoiceInvoiceFacture::className(), ['invoice_facture_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoices()
    {
        return $this->hasMany(Invoice::className(), ['id' => 'invoice_id'])->viaTable('invoice_invoice_facture', ['invoice_facture_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusOut()
    {
        return $this->hasOne(status\InvoiceFactureStatus::className(), ['id' => 'status_out_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentStatus()
    {
        return $this->getStatusOut();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusOutAuthor()
    {
        return $this->hasOne(Employee::className(), ['id' => 'status_out_author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSignBasisDocument()
    {
        return $this->hasOne(DocumentType::className(), ['id' => 'sign_document_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentDocuments()
    {
        return $this->hasMany(InvoiceFacturePaymentDocument::className(), ['invoice_facture_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['id' => 'order_id'])
            ->viaTable('order_invoice_facture', ['invoice_facture_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderInvoiceFactures()
    {
        return $this->hasMany(OrderInvoiceFacture::className(), ['invoice_facture_id' => 'id']);
    }

    /**
     * @return array
     */
    public function getGroupedOrderInvoiceFactures()
    {
        $orders = $this->getOrderInvoiceFactures()->with('order')->all();

        return $this->groupOwnOrdersByProduct($orders);
    }

    /**
     * @return \yii\db\ActiveQuery|array
     */
    public function getOwnOrders()
    {
        // don't group foreign currency invoices
        if (isset($this->invoice) && $this->invoice->currency_name != Currency::DEFAULT_NAME)
            return $this->getOrderInvoiceFactures();

        return ($this->isGroupOwnOrdersByProduct) ? $this->getGroupedOrderInvoiceFactures() : $this->getOrderInvoiceFactures();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConsignor()
    {
        return $this->hasOne(Contractor::className(), ['id' => 'consignor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getConsignee()
    {
        return $this->hasOne(Contractor::className(), ['id' => 'consignee_id']);
    }

    /**
     * @return array
     */
    public function getConsignors()
    {
        return Contractor::find()->sorted()
            ->byCompany($this->invoice->company_id)
            ->byDeleted()
            ->andWhere(['or', [Contractor::tableName() . '.is_seller' => 1], [Contractor::tableName() . '.is_customer' => 1]])
            ->andWhere(['!=', Contractor::tableName() . '.id', $this->invoice->contractor_id]);
    }

    /**
     * @return integer
     */
    public function getTotalAmountWithNds()
    {
        $amount = 0;
        foreach ($this->ownOrders as $key => $value) {
            $amount += $value->amountWithNds;
        }

        return $amount;
    }

    /**
     * @return integer
     */
    public function getOrders_sum()
    {
        return $this->getTotalAmountWithNds();
    }

    /**
     * @return integer
     */
    public function getTotalAmountNoNds()
    {
        $amount = 0;
        foreach ($this->ownOrders as $key => $value) {
            $amount += $value->amountNoNds;
        }

        return $amount;
    }

    /**
     * @return integer
     */
    public function getTotalNds()
    {
        return $this->totalAmountWithNds - $this->totalAmountNoNds;
    }

    /**
     * @return array
     */
    public function getPosibleOrders()
    {
        $invoices = ArrayHelper::getColumn($this->invoices, 'id');
        return Order::find()
            ->andWhere(['in', 'order.invoice_id', $invoices])
            ->leftJoin(
                '{{%order_invoice_facture}}',
                '{{%order_invoice_facture}}.[[order_id]] = {{order}}.[[id]] AND {{%order_invoice_facture}}.[[invoice_facture_id]] != :id',
                [':id' => $this->id]
            )
            ->having(['>', 'order.quantity', new Expression('SUM(IFNULL({{%order_invoice_facture}}.[[quantity]], 0))')])
            ->groupBy('order.id');
    }

    /**
     * @param bool $insert
     *
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                if ($this->document_date === null) {
                    $this->document_date = date(DateHelper::FORMAT_DATE);
                }
                if (empty($this->document_number)) {
                    $this->newDocumentNumber();
                    $this->ordinal_document_number = $this->document_number;
                }
            }

            if ($this->type == Documents::IO_TYPE_OUT) {
                if ($insert) {
                    $this->status_out_id = status\ActStatus::STATUS_CREATED;
                }

                if ($insert || $this->isAttributeChanged('status_out_id')) {
                    $this->status_out_updated_at = time();
                    if (Yii::$app->id == 'app-frontend') {
                        $this->status_out_author_id = Yii::$app->user->id;
                    } else {
                        if ($this->document_author_id === null) {
                            $this->document_author_id = $this->invoice->document_author_id;
                        }

                        if ($this->status_out_author_id === null) {
                            $this->status_out_author_id = $this->invoice->document_author_id;
                        }
                    }
                }
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert) {
            if ($this->invoice_id) {
                $invoiceInvoiceFacture = new InvoiceInvoiceFacture();
                $invoiceInvoiceFacture->invoice_id = $this->invoice_id;
                $invoiceInvoiceFacture->invoice_facture_id = $this->id;
                $invoiceInvoiceFacture->save();
            }
        }

        if (!$insert) {
            foreach ($this->invoices as $invoice) {
                InvoiceHelper::checkForInvoiceFacture($invoice->id, $this->invoices);
            }

            if ($modelArray = $this->invoice->packingLists) {
                foreach ($modelArray as $model) {
                    if ($this->getTotalAmountWithNds() == $model->getTotalAmountWithNds()) {
                        $model->updateAttributes([
                            'consignor_id' => $this->consignor_id,
                            'consignee_id' => $this->consignee_id,
                        ]);
                    }
                }
            }
        }
    }

    /**
     * @return bool
     * @throws \yii\db\StaleObjectException
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $this->beforeDeleteInvoices = $this->invoices;
            foreach ($this->getOwnOrders()->all() as $order) {
                if (!$order->delete()) {
                    return false;
                }
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();

        foreach ($this->beforeDeleteInvoices as $invoice) {
            InvoiceHelper::checkForInvoiceFacture($invoice->id, $this->beforeDeleteInvoices);
        }
    }

    /**
     * @param array $orderParams
     * @param array $paymentParams
     * @return mixed
     * @throws \Throwable
     */
    public function updateAndSave($orderParams = [], $paymentParams = [])
    {
        return Yii::$app->db->transaction(function ($db) use ($orderParams, $paymentParams) {
            if (!$this->save()) {
                Yii::$app->session->setFlash('error', Html::errorSummary($this));
                $db->transaction->rollBack();

                return false;
            }

            $this->unlinkAll('orders', true);
            $ownOrderArray = [];
            $invoices = ArrayHelper::getColumn($this->invoices, 'id');
            if (is_array($orderParams)) {
                foreach ($orderParams as $orderId => $params) {
                    /* @var $order Order */
                    $order = $orderId ? Order::findOne(['id' => $orderId, 'invoice_id' => $invoices]) : null;
                    $quantity = ArrayHelper::getValue($params, 'quantity');
                    if (!empty($quantity) && $order) {
                        $ownOrderArray[] = new OrderInvoiceFacture([
                            'invoice_facture_id' => $this->id,
                            'order_id' => $order->id,
                            'quantity' => min(OrderInvoiceFacture::availableQuantity($order), max(0, $quantity)),
                            'country_id' => ArrayHelper::getValue($params, 'country_id'),
                            'custom_declaration_number' => ArrayHelper::getValue($params, 'custom_declaration_number'),
                        ]);
                    }
                }
            }
            if ($ownOrderArray) {
                foreach ($ownOrderArray as $ownOrder) {
                    $ownOrder->quantity = number_format($ownOrder->quantity, 10, '.', '');
                    if (!$ownOrder->save()) {
                        Yii::$app->session->setFlash('error', Html::errorSummary($ownOrder));
                        $db->transaction->rollBack();

                        return false;
                    }
                }
            } else {
                Yii::$app->session->setFlash('error', 'Счет-фактура не может быть пустой');
                $db->transaction->rollBack();

                return false;
            }
            foreach ($this->invoices as $invoice) {
                InvoiceHelper::checkForInvoiceFacture($invoice->id, $this->invoices);
            }

            InvoiceFacturePaymentDocument::deleteAll(['invoice_facture_id' => $this->id]);
            if (isset($paymentParams['number']) && is_array($paymentParams['number'])) {
                foreach ($paymentParams['number'] as $key => $value) {
                    if (($value = trim($value)) && ($date = DateTime::createFromFormat('d.m.Y', $paymentParams['date'][$key]))) {
                        $payment = new InvoiceFacturePaymentDocument([
                            'invoice_facture_id' => $this->id,
                            'payment_document_number' => $value,
                            'payment_document_date' => $date->format('Y-m-d'),
                        ]);
                        if (!$payment->save()) {
                            Yii::$app->session->setFlash('error', Html::errorSummary($payment));
                            $db->transaction->rollBack();

                            return false;
                        }
                    }
                }
            }

            return true;
        });
    }

    /**
     * @inheritdoc
     */
    public function getFile()
    {
        return $this->hasOne(File::className(), ['owner_id' => 'id'])
            ->onCondition(['owner_model' => InvoiceFacture::className(),]);
    }

    /**
     * @inheritdoc
     */
    public function getFiles()
    {
        return $this->hasMany(File::className(), ['owner_id' => 'id'])
            ->onCondition([File::tableName() . '.owner_model' => InvoiceFacture::className(),]);
    }

    /**
     * @inheritdoc
     */
    public function getScanDocuments()
    {
        return $this->hasMany(ScanDocument::className(), ['owner_id' => 'id'])
            ->onCondition([ScanDocument::tableName() . '.owner_model' => InvoiceFacture::className(),]);
    }

    /**
     * Full invoice number
     *
     * @return string
     * @throws Exception
     */
    public function getFullNumber()
    {
        if ($this->document_number !== null) {
            $documentNumber = $this->document_number;
        } else {
            $documentNumber = $this->invoice->ordinal_document_number;
        }

        if ($this->type == Documents::IO_TYPE_OUT) {
            if ($this->invoice && $this->invoice->is_additional_number_before) {
                $documentNumber = $this->document_additional_number . $documentNumber;
            } else {
                $documentNumber = $documentNumber . $this->document_additional_number;
            }
        }

        return $documentNumber;
    }

    /**
     * @return string
     */
    public function getPrintTitle()
    {
        return str_replace([' ', '"'], ['_'],
            $this->printablePrefix . '_№' . $this->getFullNumber() . '_' .
            date_format(date_create($this->document_date), 'd.m.Y') . '_' .
            $this->invoice->company_name_short
        );
    }

    /**
     * Get formatted log message from Log model
     *
     * @param Log $log
     *
     * @return string
     */
    public function getLogMessage(Log $log)
    {
        $title = 'Счет-фактура №';
        $invoiceText = '';
        /* @var Invoice $invoice */
        $invoice = Invoice::findOne($log->getModelAttributeNew('invoice_id'));

        $ioType = $log->getModelAttributeNew('type');
        if ($ioType == Documents::IO_TYPE_IN) {
            $title .= $log->getModelAttributeNew('document_number');
        } else {
            $title .= $log->getModelAttributeNew('document_number') . ' ' . $log->getModelAttributeNew('document_additional_number');
        }

        $link = Html::a($title, [
            '/documents/invoice-facture/view',
            'type' => $log->getModelAttributeNew('type'),
            'id' => $log->getModelAttributeNew('id'),
        ]);

        if ($log->log_event_id == LogEvent::LOG_EVENT_RESPONSIBLE) {
            $message = $link . ', изменен ответственный';
            if ($this->invoice->responsible) {
                $message .= ' на '.$this->invoice->responsible->getFio(true).'.';
            } else {
                $message .= '.';
            }

            return $message;
        }

        if ($invoice) {
            $amount = TextHelper::invoiceMoneyFormat($invoice->total_amount_with_nds, 2) . ' руб.';
            $contractorName = $invoice->contractor ? ', ' . $invoice->contractor->getTitle(true) : '';
            $invoiceText = ', ' . $amount . $contractorName . ',';
        }

        switch ($log->log_event_id) {
            case LogEvent::LOG_EVENT_CREATE:
                return $link . $invoiceText . ' была создана.';
            case LogEvent::LOG_EVENT_DELETE:
                return $title . $invoiceText . ' была удалена.';
            case LogEvent::LOG_EVENT_UPDATE:
                return $link . $invoiceText . ' была изменена.';
            case LogEvent::LOG_EVENT_UPDATE_STATUS:
                return $link . $invoiceText . ' статус "' . InvoiceFactureStatus::findOne($log->getModelAttributeNew('status_out_id'))->name . '"';
            default:
                return $log->message;
        }
    }

    /**
     * @param $fileName
     * @param $outInvoiceFacture
     * @return PdfRenderer
     */
    public static function getRenderer($fileName, $outInvoiceFacture, $destination = PdfRenderer::DESTINATION_FILE)
    {
        $renderer = new PdfRenderer([
            'view' => '@frontend/modules/documents/views/invoice-facture/pdf-view',
            'params' => array_merge([
                'model' => $outInvoiceFacture,
                'message' => new Message(Documents::DOCUMENT_INVOICE_FACTURE, $outInvoiceFacture->type),
                'ioType' => $outInvoiceFacture->type,
                'addStamp' => true,
            ]),

            'destination' => $destination,
            'filename' => $fileName,
            'displayMode' => PdfRenderer::DISPLAY_MODE_FULLPAGE,
            'format' => 'A4-L',
        ]);
        \Yii::$app->controller->view->params['asset'] = DocumentPrintAsset::className();

        return $renderer;
    }

    /**
     * @inheritdoc
     */
    public function setStatusPrinted()
    {
        if ($this && in_array($this->status_out_id, [InvoiceFactureStatus::STATUS_CREATED, InvoiceFactureStatus::STATUS_PRINTED])) {
            LogHelper::save($this, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function ($model) {
                $model->status_out_id = InvoiceFactureStatus::STATUS_PRINTED;
                $model->status_out_updated_at = time();
                $model->status_out_author_id = \Yii::$app->user->id;

                return $model->save(true, ['status_out_id', 'status_out_updated_at', 'status_out_author_id']);
            });
        }
    }

    /**
     * @param $period
     * @param $invoiceFactures
     */
    public static function generateXlsTable($period, $invoiceFactures)
    {
        $excel = new Excel();

        $fileName = 'Счета_фактуры_' . DateHelper::format($period['from'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) .
            '-' . DateHelper::format($period['to'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $invoiceFactures,
            'title' => 'Счета фактуры',
            'columns' => [
                [
                    'attribute' => 'document_date',
                    'value' => function (InvoiceFacture $data) {
                        return DateHelper::format($data->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                    },
                ],
                [
                    'attribute' => 'document_number',
                    'value' => function (InvoiceFacture $data) {
                        return $data->fullNumber;
                    },
                ],
                [
                    'attribute' => 'invoice.total_amount_with_nds',
                    'styleArray' => [
                        'numberformat' => [
                            'code' => '# ##0.00',
                        ]
                    ],
                    'dataType' => 'n',
                    'value' => function ($model) {
                        return bcdiv($model->totalAmountWithNds, 100, 2);
                    },
                ],
                [
                    'attribute' => 'contractor_id',
                    'value' => function (InvoiceFacture $data) {
                        return $data->invoice->contractor_name_short ?? '';
                    },
                ],
                [
                    'attribute' => 'status_out_id',
                    'value' => function (InvoiceFacture $data) {
                        return $data->statusOut->name ?? '';
                    },
                ],
                [
                    'attribute' => 'invoice_document_number',
                    'value' => function (InvoiceFacture $data) {
                        return $data->invoice->fullNumber ?? '';
                    },
                ],
                [
                    'attribute' => 'nomenclature',
                    'wrap' => true,
                    'value' => function (InvoiceFacture $model) {

                        $list = [];
                        foreach ($model->orders as $order) {
                            $unitName = $order->unit ? $order->unit->name : Product::DEFAULT_VALUE;
                            $list[] = $order->product_title . ' ' .
                                TextHelper::numberFormat($order->quantity, 2) . ' ' .
                                $unitName . ' x ' .
                                TextHelper::invoiceMoneyFormat($order->view_price_one, 2) . 'р. = ' .
                                TextHelper::invoiceMoneyFormat($order->view_total_amount, 2) . 'р.';
                        }

                        return $list ? implode("\r", $list) : 'НЕТ';
                    },
                ]
            ],
            'headers' => [
                'document_date' => 'Дата СФ',
                'document_number' => '№ СФ',
                'invoice.total_amount_with_nds' => 'Сумма',
                'contractor_id' => 'Контрагент',
                'status_out_id' => 'Статус',
                'invoice_document_number' => 'Счёт №',
                'nomenclature' => 'Номенклатурная часть'
            ],
            'format' => 'Xlsx',
            'fileName' => $fileName,
        ]);
    }

    /**
     * @return bool|string
     * @throws Exception
     * @throws \yii\db\Exception
     */
    public function generateOneCFile()
    {
        $exportModel = new Export([
            'company_id' => $this->company->id,
            'user_id' => $this->document_author_id,
        ]);

        $oneCExport = new OneCExport($exportModel);
        if ($oneCExport->findDataObjectInvoiceFacture($this->id, Documents::IO_TYPE_IN)) {
            $path = Yii::getAlias('@frontend/runtime/export') . DIRECTORY_SEPARATOR . $oneCExport->createExportFile();
            Yii::$app->db->createCommand("DELETE FROM `export` WHERE `export`.`id` = {$exportModel->id}")->execute();

            return $path;

        }
        return false;
    }

    /**
     * @return string
     */
    public function getOneCFileName()
    {
        return 'Счет-фактура №' . $this->getFullNumber() . '.xml';
    }

    /**
     * @return string
     */
    public function getEmailSubject()
    {
        return 'Счет фактура № ' . $this->fullNumber
            . ' от ' . DateHelper::format($this->document_date, 'd.m.y', DateHelper::FORMAT_DATE)
            . ' от ' . $this->invoice->company_name_short;
    }

    /**
     * @return string
     */
    public function getEmailText()
    {
        $document_date = DateHelper::format($this->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
        $sum = TextHelper::invoiceMoneyFormat($this->totalAmountWithNds, 2);

        $text = <<<EMAIL_TEXT
Здравствуйте!

Счет фактура № {$this->fullNumber} от {$document_date} на сумму {$sum} р.
EMAIL_TEXT;

        return $text;
    }

    /**
     * @return array
     * @throws \MpdfException
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function loadEmailFiles()
    {
        $files = [];

        $emailFile = new EmailFile();
        $emailFile->company_id = $this->invoice->company_id;
        $emailFile->file_name = str_replace(['\\', '/'], '_', $this->pdfFileName);
        $emailFile->ext = 'pdf';
        $emailFile->mime = 'application/pdf';
        $emailFile->size = 0;
        $emailFile->type = EmailFile::TYPE_INVOICE_FACTURE_DOCUMENT;
        if ($emailFile->save()) {
            $path = $emailFile->getFilePath() . DIRECTORY_SEPARATOR . $emailFile->file_name;
            $size = file_put_contents($path, self::getRenderer(null, $this, PdfRenderer::DESTINATION_STRING)->output(false));
            $emailFile->size = ceil($size / 1024);
            if ($emailFile->save(true, ['size'])) {
                $files[] = [
                    'id' => $emailFile->id,
                    'name' => $emailFile->file_name,
                    'previewImg' => '<img src="/img/email/emailPdf.png" class="preview-img">',
                    'size' => $emailFile->size,
                    'deleteUrl' => Url::to(['/email/delete-email-file', 'id' => $emailFile->id]),
                    'downloadUrl' => Url::to(['/email/download-file', 'id' => $emailFile->id]),
                ];
            }
        }

        $oneCFilePath = $this->generateOneCFile();
        if ($oneCFilePath) {
            $fileName = 'Для загрузки в 1С ' . $this->getOneCFileName();
            $emailFile = new EmailFile();
            $emailFile->company_id = $this->invoice->company_id;
            $emailFile->file_name = $fileName;
            $emailFile->ext = 'xml';
            $emailFile->mime = 'application/xml';
            $emailFile->size = ceil(filesize($oneCFilePath) / 1024);
            $emailFile->type = EmailFile::TYPE_ONE_C_FILE;
            if ($emailFile->save()) {
                $files[] = [
                    'id' => $emailFile->id,
                    'name' => $emailFile->file_name,
                    'previewImg' => '<img src="/img/email/emailUnknown.png" class="preview-img">',
                    'size' => $emailFile->size,
                    'deleteUrl' => Url::to(['/email/delete-email-file', 'id' => $emailFile->id]),
                ];
            }
        }

        return $files;
    }

    /**
     * @param EmployeeCompany $sender
     * @param $toEmail
     * @param $type
     * @param $invoiceFactureIDs
     * @return bool
     * @throws \MpdfException
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     * @throws \Exception
     */
    public static function manySend(EmployeeCompany $sender, $toEmail, $type, $invoiceFactureIDs)
    {
        /* @var $models InvoiceFacture[] */
        $models = [];
        $emailSubject = [];
        $companyID = null;
        $emailTemplate = null;
        $companyNameShort = null;
        if ($type == Documents::IO_TYPE_OUT) {
            foreach ($invoiceFactureIDs as $invoiceFactureID) {
                $invoiceFacture = is_object($invoiceFactureID) ? $invoiceFactureID : self::findOne($invoiceFactureID);
                if ($invoiceFacture->uid == null) {
                    $invoiceFacture->updateAttributes([
                        'uid' => self::generateUid(),
                    ]);
                }
                $models[] = $invoiceFacture;
                $emailSubject[] = 'Счет фактура № ' . $invoiceFacture->fullNumber
                    . ' от ' . DateHelper::format($invoiceFacture->document_date, 'd.m.y', DateHelper::FORMAT_DATE);
                $companyID = $invoiceFacture->invoice->company_id;
                $companyNameShort = $invoiceFacture->invoice->company_name_short;
            }
            $emailSubject = implode(', ', $emailSubject);
            $emailSubject .= (' от ' . $companyNameShort);
            $params = [
                'model' => $models,
                'employee' => $sender->employee,
                'employeeCompany' => $sender,
                'subject' => 'Счет фактура',
                'pixel' => [
                    'company_id' => $companyID,
                    'email' => is_array($toEmail) ? implode(',', $toEmail) : $toEmail,
                ],
            ];
            Yii::$app->mailer->htmlLayout = 'layouts/document-html';
            $message = Yii::$app->mailer->compose([
                'html' => 'system/documents/invoice-facture-out/html',
                'text' => 'system/documents/invoice-facture-out/text',
            ], $params)
                ->setFrom([Yii::$app->params['emailList']['docs'] => $sender->getFio()])
                ->setReplyTo([$sender->email => $sender->getFio(true)])
                ->setSubject($emailSubject)
                ->setTo($toEmail);
            foreach ($models as $model) {
                $message->attachContent(InvoiceFacture::getRenderer(null, $model, PdfRenderer::DESTINATION_STRING)->output(false), [
                    'fileName' => $model->pdfFileName,
                    'contentType' => 'application/pdf',
                ]);
            }
            if ($message->send()) {
                foreach ($models as $model) {
                    InvoiceSendForm::setInvoiceFactureSendStatus($model);
                }

                return true;
            }
        }

        return false;
    }

    /**
     * @param $employeeId
     * @throws \Exception
     */
    public function updateSendStatus($employeeId)
    {
        if (in_array($this->status_out_id, [InvoiceFactureStatus::STATUS_CREATED, InvoiceFactureStatus::STATUS_PRINTED, InvoiceFactureStatus::STATUS_DELIVERED])) {
            LogHelper::save($this, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function ($model) use ($employeeId) {
                return (boolean)$model->updateAttributes([
                    'status_out_id' => InvoiceFactureStatus::STATUS_DELIVERED,
                    'status_out_updated_at' => time(),
                    'status_out_author_id' => $employeeId,
                ]);
            });
        } else {
            $currentStatus = $this->status_out_id;
            $this->status_out_id = InvoiceFactureStatus::STATUS_DELIVERED;
            LogHelper::log($this, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS);
            $this->status_out_id = $currentStatus;
        }
    }

    public function setIsGroupOwnOrdersByProduct($value)
    {
        $this->_isGroupOwnOrdersByProduct = $value;
    }

    public function getIsGroupOwnOrdersByProduct()
    {
        return ($this->document_date < '2021-07-01')
            ? $this->_isGroupOwnOrdersByProduct
            : false; // disable grouped products since 1 jule 2021
    }

    public function getPrintablePaymentDocuments()
    {
        $paymentDocuments = [];
        foreach ($this->paymentDocuments as $doc) {
            $date = date('d.m.Y', strtotime($doc->payment_document_date));
            $paymentDocuments[] = "№ {$doc->payment_document_number} от {$date}";
        }

        return $paymentDocuments ? join('; ', $paymentDocuments) : '№ — от —';
    }

    public function getPrintableShippingDocuments()
    {
        $shippingDocuments = [];
        $num = 1;
        foreach ($this->getSortedShippingDocuments() as $doc) {
            $date = date('d.m.Y', strtotime($doc->document_date));
            $positionNumber = "{$num}-" . ($num += count($doc->ownOrders) - 1);
            $shippingDocuments[] = "№ п/п {$positionNumber} № {$doc->fullNumber} от {$date}";
            $num++;
        }

        return $shippingDocuments ? join('; ', $shippingDocuments) : '№ — от —';
    }

    /**
     * @return \yii\db\ActiveQuery|array
     */
    public function getOwnOrdersByShippingDocuments()
    {
        $ownOrders = $this->getOrderInvoiceFactures()->indexBy('order_id')->all();

        $shippingOrders = [];
        foreach ($this->getSortedShippingDocuments() as $doc) {
            foreach ($doc->orders as $order)
                $shippingOrders[$order->id] = $order;
        }

        $pos = 0;
        $retOwnOrders = [];
        foreach ($shippingOrders as $orderId => $order) {
            if (isset($ownOrders[$orderId])) {
                $retOwnOrders[++$pos] = $ownOrders[$orderId];
                unset($ownOrders[$orderId]);
            }
        }

        if (!empty($ownOrders))
            foreach ($ownOrders as $ownOrder)
                $retOwnOrders[++$pos] = $ownOrder;

        return $retOwnOrders;
    }

    private function getSortedShippingDocuments()
    {
        $shippingDocuments = [];
        foreach ($this->invoices as $invoice) {
            foreach (array_merge($invoice->packingLists, $invoice->upds, $invoice->acts) as $doc) {
                $shippingDocuments[] = $doc;
            }
        }

        uasort($shippingDocuments, function ($a, $b) {
            return $a->document_number <> $b->document_number;
        });

        return $shippingDocuments;
    }
}
