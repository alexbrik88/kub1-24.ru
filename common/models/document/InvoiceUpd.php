<?php

namespace common\models\document;

use Yii;

/**
 * This is the model class for table "invoice_upd".
 *
 * @property integer $invoice_id
 * @property integer $upd_id
 *
 * @property Invoice $invoice
 * @property Upd $upd
 */
class InvoiceUpd extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invoice_upd';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['invoice_id', 'upd_id'], 'required'],
            [['invoice_id', 'upd_id'], 'integer'],
            [['invoice_id'], 'exist', 'skipOnError' => true, 'targetClass' => Invoice::className(), 'targetAttribute' => ['invoice_id' => 'id']],
            [['upd_id'], 'exist', 'skipOnError' => true, 'targetClass' => Upd::className(), 'targetAttribute' => ['upd_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'invoice_id' => 'Invoice ID',
            'upd_id' => 'Upd ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(Invoice::className(), ['id' => 'invoice_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpd()
    {
        return $this->hasOne(Upd::className(), ['id' => 'upd_id']);
    }
}
