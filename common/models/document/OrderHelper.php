<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 11/25/15
 * Time: 1:12 AM
 * Email: t.kanstantsin@gmail.com
 */

namespace common\models\document;


use common\models\Company;
use common\models\product\Product;
use common\models\TaxRate;
use frontend\models\Documents;

class OrderHelper
{
    public static function createOrderByProduct(Product $product, Invoice $invoice, $count = 1, $price = null, $discount = 0, $markup = 0, $taxRateId = null)
    {
        $order = new Order();
        $order->populateRelation('invoice', $invoice);
        $order->populateRelation('product', $product);

        // sale tax rate
        if ($saleTaxRate = ($invoice->type == Documents::IO_TYPE_OUT) ? TaxRate::findOne($taxRateId) : null) {
            $order->populateRelation('saleTaxRate', $saleTaxRate);
        } else {
            $order->populateRelation('saleTaxRate', $product->priceForSellNds);
        }
        // purchase tax rate
        if ($purchaseTaxRate = ($invoice->type == Documents::IO_TYPE_IN) ? TaxRate::findOne($taxRateId) : null) {
            $order->populateRelation('purchaseTaxRate', $purchaseTaxRate);
        } else {
            $order->populateRelation('purchaseTaxRate', $product->priceForBuyNds);
        }

        $order->purchase_tax_rate_id = ($purchaseTaxRate)
            ? $purchaseTaxRate->id
            : ($product->price_for_buy_nds_id ? $product->price_for_buy_nds_id : 4);
        $order->sale_tax_rate_id = ($saleTaxRate)
            ? $saleTaxRate->id
            : ($product->price_for_sell_nds_id ? $product->price_for_sell_nds_id : 4);
        $order->product_id = $product->id;
        $order->product_title = $product->title;
        $order->product_code = $product->code;
        $order->article = $product->article;
        $order->box_type = $product->box_type;
        $order->unit_id = $product->product_unit_id;
        $order->place_count = $product->place_count;
        $order->count_in_place = $product->count_in_place;
        $order->mass_gross = $product->mass_gross;
        $order->excise = $product->has_excise;
        $order->excise_price = $product->excise;
        $order->country_id = $product->country_origin_id;
        $order->custom_declaration_number = $product->customs_declaration_number;

        if ($price === null) {
            if ($invoice->type == Documents::IO_TYPE_IN) {
                if ($invoice->nds_view_type_id == Invoice::NDS_VIEW_OUT && $product->priceForBuyNds) {
                    $price = $product->price_for_buy_with_nds / (1 + floatval($product->priceForBuyNds->rate));
                } else {
                    $price = $product->price_for_buy_with_nds;
                }
            } else {
                if ($invoice->nds_view_type_id == Invoice::NDS_VIEW_OUT && $product->priceForSellNds) {
                    $price = $product->price_for_sell_with_nds / (1 + floatval($product->priceForSellNds->rate));
                } else {
                    $price = $product->price_for_sell_with_nds;
                }
            }
        }

        $order->price = $price / 100;
        $order->quantity = $count;
        $order->discount = $discount;
        $order->markup = $markup;
        $order->number = 1;

        $order->calculateOrder();

        return $order;
    }
}
