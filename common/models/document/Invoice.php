<?php

namespace common\models\document;

use backend\models\Bank;
use common\components\date\DateHelper;
use common\components\date\DatePickerFormatBehavior;
use common\components\excel\Excel;
use common\components\pdf\PdfRenderer;
use common\components\TextHelper;
use common\models\AgreementType;
use common\models\amocrm\AmocrmWidgetInvoice;
use common\models\cash\CashBankFlows;
use common\models\cash\CashBankFlowToInvoice;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashEmoneyFlowToInvoice;
use common\models\cash\CashFactory;
use common\models\cash\CashOrderFlows;
use common\models\cash\CashOrderFlowToInvoice;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\company\CompanyIndustry;
use common\models\companyStructure\SalePoint;
use common\models\Contractor;
use common\models\currency\Currency;
use common\models\currency\CurrencyRate;
use common\models\document\DocumentType;
use common\models\document\status\ActStatus;
use common\models\document\status\InvoiceFactureStatus;
use common\models\document\status\InvoiceStatus;
use common\models\document\status\PackingListStatus;
use common\models\document\status\UpdStatus;
use common\models\document\status\WaybillStatus;
use common\models\EmployeeCompany;
use common\models\employee\Employee;
use common\models\employee\EmployeeSignature;
use common\models\file\File;
use common\models\out\OutInvoice;
use common\models\paymentReminder\PaymentReminderMessage;
use common\models\paymentReminder\PaymentReminderMessageContractor;
use common\models\paymentReminder\Settings;
use common\models\product\Product;
use common\models\product\ProductUnit;
use common\models\product\Store;
use common\models\project\Project;
use common\models\project\ProjectEstimate;
use common\models\rent\RentAgreement;
use common\models\service\Payment;
use common\models\service\Subscribe;
use common\models\TaxRate;
use console\components\PaymentReminder;
use Faker\Provider\DateTime;
use frontend\models\Documents;
use frontend\models\log\Log;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\modules\analytics\models\PlanCashContractor;
use frontend\modules\api\components\ApiController;
use frontend\modules\cash\models\CashContractorType;
use frontend\modules\crm\behaviors\PotentialClientBehavior;
use frontend\modules\documents\assets\InvoicePrintAsset;
use frontend\modules\documents\components\InvoiceHelper;
use frontend\modules\documents\components\Message;
use frontend\modules\export\models\export\Export;
use frontend\modules\export\models\one_c\OneCExport;
use frontend\modules\telegram\behaviors\NotificationBehavior;
use frontend\modules\telegram\behaviors\NotificationFactoryInterface;
use frontend\modules\telegram\components\NotificationInterface;
use frontend\rbac\permissions\document\Document as DocumentPermissions;
use Yii;
use yii\base\Exception;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\helpers\VarDumper;
use yii\web\BadRequestHttpException;

/**
 * This is the model class for table "invoice".
 *
 * @property integer $uid
 * @property integer $invoice_status_id
 * @property string $invoice_status_updated_at
 * @property integer $invoice_status_author_id
 * @property string $created_at
 * @property integer $document_number
 * @property string $document_additional_number
 * @property integer $payment_limit_rule
 * @property string $payment_limit_date
 * @property integer $payment_limit_days
 * @property integer $payment_limit_percent
 * @property string $related_document_payment_limit_date
 * @property integer $related_document_payment_limit_days
 * @property integer $related_document_payment_limit_percent
 * @property integer $production_type
 * @property integer $invoice_expenditure_item_id
 * @property integer $invoice_income_item_id
 * @property integer $company_id
 * @property string $company_name_full
 * @property string $company_name_short
 * @property string $company_bank_name
 * @property string $company_bank_city
 * @property string $company_inn
 * @property string $company_kpp
 * @property string $company_egrip
 * @property string $company_okpo
 * @property string $company_bik
 * @property string $company_ks
 * @property string $company_rs
 * @property string $company_phone
 * @property string $company_address_legal_full
 * @property string $company_chief_post_name
 * @property string $company_chief_lastname
 * @property string $company_chief_firstname_initials
 * @property string $company_chief_patronymic_initials
 * @property string $company_print_filename
 * @property string $company_chief_signature_filename
 * @property string $company_chief_accountant_lastname
 * @property string $company_chief_accountant_firstname_initials
 * @property string $company_chief_accountant_patronymic_initials
 * @property integer $contractor_id
 * @property string $contractor_name_full
 * @property string $contractor_name_short
 * @property string $contractor_bank_name
 * @property string $contractor_bank_city
 * @property string $contractor_address_legal_full
 * @property string $contractor_inn
 * @property string $contractor_kpp
 * @property string $contractor_bik
 * @property string $contractor_ks
 * @property string $contractor_rs
 * @property string $remind_contractor
 * @property string $total_place_count
 * @property string $total_mass_gross
 * @property integer $total_amount_no_nds
 * @property integer $total_amount_with_nds
 * @property integer $total_amount_has_nds
 * @property integer $total_amount_nds
 * @property integer $total_order_count
 * @property integer $has_discount
 * @property integer $has_markup
 * @property integer $has_weight
 * @property integer $has_volume
 * @property integer $payment_form_id
 * @property integer $payment_partial_amount
 * @property boolean $is_deleted
 * @property boolean $is_subscribe_invoice
 * @property integer $subscribe_id
 * @property integer $email_messages
 * @property integer $company_checking_accountant_id
 * @property integer $nds_view_type_id
 * @property integer $autoinvoice_id
 * @property boolean $show_popup
 * @property integer $remaining_amount
 * @property integer $price_precision
 * @property boolean $from_store_cabinet
 * @property boolean $from_out_invoice
 * @property boolean $from_demo_out_invoice
 * @property string $currency_name
 * @property integer $currency_amount
 * @property float $currency_rate
 * @property string $currency_rate_type
 * @property string $currency_rate_date
 * @property integer $currency_rate_amount
 * @property float $currency_rate_value
 * @property boolean $show_paylimit_info
 * @property boolean $is_invoice_contract
 * @property string $contract_essence
 * @property integer $contract_essence_template
 * @property integer $out_view_count
 * @property integer $out_save_count
 * @property integer $out_pay_count
 * @property integer $out_download_count
 * @property integer $agreement_new_id
 * @property integer $discount_type
 * @property integer $is_hidden_discount
 * @property integer $need_act
 * @property integer $need_packing_list
 * @property integer $need_upd
 * @property integer $create_type_id
 * @property integer $create_api_id
 * @property integer $store_id
 * @property integer $updated_at
 * @property integer $is_surcharge
 * @property string $max_related_document_date
 * @property integer $price_round_rule
 * @property float $price_round_precision
 *
 * Relations
 * @property Company $company
 * @property Contractor $contractor
 * @property PaymentForm $paymentForm
 * @property InvoiceExpenditureItem $invoiceExpenditureItem
 * @property Order[] $orders
 * @property Act $act
 * @property InvoiceFacture $invoiceFacture
 * @property PaymentOrder $paymentOrder
 * @property PackingList $packingList
 * @property SalesInvoice $salesInvoice
 * @property Employee $invoiceStatusAuthor
 * @property Employee $documentAuthor
 * @property status\InvoiceStatus $invoiceStatus
 * @property CashOrderFlows[] $cashBankFlows
 * @property CashBankFlows[] $possibleBankFlows
 * @property CashEmoneyFlows[] $cashEmoneyFlows
 * @property CashOrderFlows[] $cashOrderFlows
 * @property File[] $files
 * @property DocumentType $signBasisDocument
 * @property EmployeeSignature $employeeSignature
 * @property Bank $contractorBank
 * @property Act[] $acts
 * @property PackingList[] $packingLists
 * @property Upd $upd
 * @property Subscribe $subscribe
 * @property Payment $payment
 * @property OrderDocument $orderDocument
 * @property NdsViewType $ndsViewType
 * @property PaymentOrderInvoice[] $paymentOrderInvoices
 * @property InvoiceAct[] $invoiceActs
 * @property InvoiceUpd[] $invoiceUpds
 * @property integer $weightPrecision
 * @property integer $volumePrecision
 * @property integer $project_id
 * @property AgentReport $agentReport
 * @property integer $sale_point_id
 * @property integer $industry_id
 * @property CompanyIndustry $industry
 * @property SalePoint $salePoint
 * @property Project $project
 *
 * Additional properties
 * @property string $fullNumber
 * @property string $ndsViewValue
 * @property string $paymentIcon
 * @property boolean $isChangeContractorAllowed
 * @property string $emailText
 * @property boolean $isRejected
 *
 * @property Proxy[] $proxies
 * @property SalesInvoice[] $salesInvoices
 * @property InvoiceFacture[] $invoiceFactures
 * @property Upd[] $upds
 * @property Waybill[] $waybills
 * @property Waybill $waybill
 * @property bool $is_by_out_link
 * @property string $by_out_link_id
 * @property-read OutInvoice $outInvoice
 * @property-read RentAgreement $rent
 * @property-read string $agreementFullName
 * @property-read bool $hasNds
 * @property string $object_guid
 *
 * @mixin TimestampBehavior
 */
class Invoice extends AbstractDocument implements NotificationFactoryInterface, PayableInvoiceInterface, BasisDocumentInterface
{
    use BasisDocumentTrait;

    const IO_TYPE_EXPENSE = 1;
    const IO_TYPE_INCOME = 2;

    /**
     * Отсрочка только ПО СЧЕТУ
     */
    const PAYMENT_LIMIT_RULE_INVOICE = 1;

    /**
     * Отсрочка только ПО Акту, ТН или УПД
     */
    const PAYMENT_LIMIT_RULE_DOCUMENT = 2;

    /**
     * Отсрочка и ПО СЧЕТУ и ПО Акту, ТН или УПД
     */
    const PAYMENT_LIMIT_RULE_MIXED = 3;

    /**
     *
     */
    const SCENARIO_UPDATE_SUBSCRIBE = 'update-subscribe';

    /**
     * Срок оплаты счёта - сегодня
     */
    const DOCUMENT_PAYMENT_DATE_TODAY = 1;
    /**
     * До оплаты счёта ещё есть время
     */
    const DOCUMENT_PAYMENT_DATE_IN_FUTURE = 2;
    /**
     * Счёт просрочен
     */
    const DOCUMENT_PAYMENT_DATE_OVERDUE = 3;
    /**
     * invoice payment type
     */
    const PAYMENT_TYPE_BANK = 1;
    /**
     *
     */
    const PAYMENT_TYPE_ORDER = 2;
    /**
     *
     */
    const PAYMENT_TYPE_EMONEY = 3;

    const DISCOUNT_TYPE_PERCENT = 0;
    const DISCOUNT_TYPE_RUBLE = 1;

    const CONTRACT_ESSENCE_TEMPLATE_EMPTY = 0;
    const CONTRACT_ESSENCE_TEMPLATE_SERVICES = 1;
    const CONTRACT_ESSENCE_TEMPLATE_GOODS = 2;

    const NEED_DOCUMENT_TYPE_ACT = 1;
    const NEED_DOCUMENT_TYPE_PACKING_LIST = 2;
    const NEED_DOCUMENT_TYPE_UPD = 3;

    /**
     * Price round
     */
    const ROUND_RULE_NO = 1;
    const ROUND_RULE_MATH = 2;
    const ROUND_RULE_UP = 3;
    const ROUND_RULE_DOWN = 4;
    const ROUND_RULE_DEFAULT = self::ROUND_RULE_NO;
    const ROUND_PRECISION_001 = 1;
    const ROUND_PRECISION_01 = 2;
    const ROUND_PRECISION_1 = 3;
    const ROUND_PRECISION_DEFAULT = self::ROUND_PRECISION_001;
    public static $roundRules = [
        self::ROUND_RULE_NO => 'Без округления',
        self::ROUND_RULE_MATH => 'Математическое',
        self::ROUND_RULE_UP => 'Вверх',
        self::ROUND_RULE_DOWN => 'Вниз',
    ];
    public static $roundPrecisions = [
        self::ROUND_PRECISION_001 => '0.01',
        self::ROUND_PRECISION_01 => '0.1',
        self::ROUND_PRECISION_1 => '1',
    ];
    public static $roundPrecisionValues = [
        self::ROUND_PRECISION_001 => 0.01,
        self::ROUND_PRECISION_01 => 0.1,
        self::ROUND_PRECISION_1 => 1,
    ];

    public $currencyAmountCustom;
    public $currencyRateCustom;

    /**
     * @var string
     */
    public $agreement;
    /**
     * @var int
     */
    public $isAutoinvoice = 0;
    /**
     * @var boolean
     */
    public $allowed_strict_mode = false;

    /**
     * @var
     */
    public $rewards_percent;

    /**
     * @var
     */
    public $rewards_amount;

    /**
     * @var array
     */
    public static $needDocumentTypesAttribute = [
        self::NEED_DOCUMENT_TYPE_ACT => 'need_act',
        self::NEED_DOCUMENT_TYPE_PACKING_LIST => 'need_packing_list',
        self::NEED_DOCUMENT_TYPE_UPD => 'need_upd',
    ];

    public static $canAddDocumentGetter = [
        self::NEED_DOCUMENT_TYPE_ACT => 'canAddAct',
        self::NEED_DOCUMENT_TYPE_PACKING_LIST => 'canAddPackingList',
        self::NEED_DOCUMENT_TYPE_UPD => 'canAddUpd',
    ];

    /**
     * @var array
     */
    public static $invoicePaymentIcon = [
        self::PAYMENT_TYPE_BANK => '<i class="fa fa-bank m-r-sm"></i>',
        self::PAYMENT_TYPE_ORDER => '<i class="fa fa-money m-r-sm"></i>',
        self::PAYMENT_TYPE_EMONEY => '<i class="flaticon-wallet31 m-r-sm" style="margin-left: -2px;"></i>',
    ];

    /**
     * @var array
     */
    public static $invoicePaymentLabel = [
        self::PAYMENT_TYPE_BANK => 'Банк',
        self::PAYMENT_TYPE_ORDER => 'Касса',
        self::PAYMENT_TYPE_EMONEY => 'E-money',
    ];

    /**
     * @var array
     */
    public static $contractEssenceTemplates = [
        self::CONTRACT_ESSENCE_TEMPLATE_GOODS => 'Поставка товаров',
        self::CONTRACT_ESSENCE_TEMPLATE_SERVICES => 'Оказание услуг',
        self::CONTRACT_ESSENCE_TEMPLATE_EMPTY => 'Пустой бланк',
    ];

    public static $contractEssenceTemplatesText = [
        self::CONTRACT_ESSENCE_TEMPLATE_GOODS => '
1.	Предметом настоящего Счёта-договора является поставка товарно-материальных ценностей (далее - "товар").
2.	Оплата настоящего Счёта-договора означает согласие Покупателя с условиями оплаты и поставки товара.
3.	Настоящий Счёт-договор действителен в течение 5 (пяти) банковских дней от даты его составления включительно. При отсутствии оплаты в указанный срок настоящий Счёт-договор признается недействительным.
4.	Оплата Счёта-договора третьими лицами (сторонами), а также неполная (частичная) оплата Счёта-договора не допускается. Покупатель не имеет права производить выборочную оплату позиций счета и требовать поставку товара по выбранным позициям.
5.	Поставщик вправе не выполнять поставку товара до зачисления оплаты на расчетный счет.
6.	Оплаченный товар доставляется Покупателю силами Поставщика по адресу {Адрес_Контрагента}.
7.	Поставщик обязан доставить оплаченный товар и передать его Покупателю в течение 3 (трёх) рабочих дней с момента зачисления оплаты на расчетный счет
8.	Покупатель обязан принять оплаченный товар лично или через уполномоченного представителя. Передача товара осуществляется при предъявлении документа, удостоверяющего личность и/или доверенности, оформленной в установленном порядке.
9.	Подписание Покупателем или его уполномоченным представителем товарной накладной означает согласие Покупателя с комплектностью и надлежащим качеством товара.',
        self::CONTRACT_ESSENCE_TEMPLATE_SERVICES => '
1.	Настоящий Счёт-договор является офертой {Наименование компании Исполнителя} (далее Исполнитель) Заказчику на оказание услуг {Наименование услуги}.
2.	Акцепт оферты осуществляется путем оплаты Заказчиком Счёта-договора и означает полное и безоговорочное согласие Заказчика с условиями оказания услуг, определённых Счётом-договором.
3.	Настоящий Счёт-договор действителен в течение 7 (семи) банковских дней от даты его составления включительно. При отсутствии оплаты в указанный срок настоящий Счёт-договор признается недействительным.
4.	Оплата Счёта-договора третьими лицами (сторонами), а также неполная (частичная) оплата Счёта-договора не допускается. Заказчик не имеет права производить выборочную оплату позиций счета и требовать поставку товара по выбранным позициям.
5.	Исполнитель вправе не выполнять услуги до зачисления оплаты на расчетный счет.
6.	Оплата услуг осуществляется в рублях путем перечисления соответствующих денежных средств на расчетный счет Исполнителя.
7.	При оплате ссылка на номер и дату Счета-договора обязательны.
8.	Исполнитель обязуется выполнить услуги в течение 7 (семи) рабочих дней с момента зачисления оплаты на расчетный счет.
9.	После оказания услуг Исполнитель и Заказчик подписывают Акт оказания услуг (выполненных работ) на месте оказания услуг.
	Подписание Заказчиком или его уполномоченным представителем данного Акта означает согласие Заказчика с тем,
	что услуги выполнены Исполнителем полностью и  претензий по объему, количеству и срокам оказания услуг Заказчик не имеет.',
        self::CONTRACT_ESSENCE_TEMPLATE_EMPTY => '',
    ];

    /**
     * @var array
     */
    public static $paymentTypeData = [
        self::PAYMENT_TYPE_BANK => [
            'alias' => CashContractorType::BANK_TEXT,
            'label' => 'Банк',
            'icon' => '<i class="fa fa-bank m-r-sm"></i>',
        ],
        self::PAYMENT_TYPE_ORDER => [
            'alias' => CashContractorType::ORDER_TEXT,
            'label' => 'Касса',
            'icon' => '<i class="fa fa-money m-r-sm"></i>',
        ],
        self::PAYMENT_TYPE_EMONEY => [
            'alias' => CashContractorType::EMONEY_TEXT,
            'label' => 'E-money',
            'icon' => '<i class="flaticon-wallet31 m-r-sm" style="margin-left: -2px;"></i>',
        ],
    ];

    /**
     * @var string
     */
    public static $uploadDirectory = 'invoice';

    /**
     * @var array
     */
    public static $documentPaymentStyle = [
        self::DOCUMENT_PAYMENT_DATE_TODAY => 'green',
        self::DOCUMENT_PAYMENT_DATE_IN_FUTURE => 'yellow',
        self::DOCUMENT_PAYMENT_DATE_OVERDUE => 'red',
    ];

    /**
     * @var array
     */
    public static $precisions = [
        '2',
        '4',
    ];

    /**
     *
     */
    const IS_DELETED = 1;
    /**
     *
     */
    const NOT_IS_DELETED = 0;

    /**
     * invoice NDS view type constants
     */
    const NDS_VIEW_IN = 0;
    /**
     *
     */
    const NDS_VIEW_OUT = 1;
    /**
     *
     */
    const NDS_VIEW_WITHOUT = 2;

    /**
     *
     */
    const HAS_NDS = 1;
    /**
     *
     */
    const HAS_NO_NDS = 0;

    /**
     * invoice NDS view values
     * @var array
     */
    public static $ndsViewList = [
        self::NDS_VIEW_IN => 'В том числе НДС',
        self::NDS_VIEW_OUT => 'НДС сверху',
        self::NDS_VIEW_WITHOUT => 'Без налога (НДС)',
    ];

    /**
     * @var Order[]
     */
    public $removedOrders = [];

    /**
     * @var
     */
    public $includedNDS;

    /**
     * @var string
     */
    public $printablePrefix = 'Счет';

    /**
     * @var string
     */
    public $emailTemplate = 'invoice-out';

    /**
     * @var string
     */
    public $urlPart = 'invoice';

    /**
     * permissions
     * @var boolean
     */
    protected $_canView;
    /**
     * @var
     */
    protected $_canCreate;

    protected $_weightPrecision;
    protected $_volumePrecision;
    protected $_data = [];

    public $price_for_sell_with_nds;
    public $price_for_buy_with_nds;
    public $amount;

    /**
     * @var
     */
    public $lastCreatedDocumentError;

    /**
     * @var int
     */
    public $createdFromInInvoiceId;

    /**
     * @return boolean
     */
    public function getCanView()
    {
        if ($this->_canView === null) {
            $this->_canView = Yii::$app->user->can(DocumentPermissions::VIEW, [
                'ioType' => $this->type,
                'model' => $this,
            ]);
        }

        return $this->_canView;
    }

    /**
     * @return boolean
     */
    public function getCanCreate()
    {
        if ($this->_canCreate === null) {
            $this->_canCreate = Yii::$app->user->can(DocumentPermissions::CREATE, [
                'ioType' => $this->type,
                'model' => $this,
            ]);
        }

        return $this->_canCreate;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invoice';
    }

    /**
     * @return array the list of expandable field names or field definitions. Please refer
     * to [[fields()]] on the format of the return value.
     * @see toArray()
     * @see fields()
     */
    public function extraFields()
    {
        return [
            'orders',
            'contractor',
            'company',
            'acts',
            'packingLists',
            'invoiceFactures',
            'upds',
        ];
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            [
                'class' => DatePickerFormatBehavior::className(),
                'attributes' => [
                    'payment_limit_date' => [
                        'message' => 'Дата "Оплатить до" указана неверно.',
                    ],
                    'basis_document_date' => [
                        'message' => 'Дата основания указана неверно.',
                    ],
                    'related_document_payment_limit_date' => [
                        'message' => 'Дата "Оплатить до (первичные документы)" указана неверно.',
                    ],
                ],
            ],
            'documentCreatedAtBehavior' => [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => 'updated_at',
            ],
            'notification' => [
                'class' => NotificationBehavior::class,
                'events' => [self::EVENT_AFTER_INSERT],
            ],
            'potentialClient' => [
                'class' => PotentialClientBehavior::class,
            ],
        ]);
    }

    /**
     * Initializes the object.
     */
    public function init()
    {
        $this->currency_name = Currency::DEFAULT_NAME;
        $this->currency_amount = $this->currency_rate_amount = Currency::DEFAULT_AMOUNT;
        $this->currency_rate = $this->currency_rate_value = Currency::DEFAULT_RATE;
        $this->currency_rate_type = Currency::RATE_SERTAIN_DATE;
        $this->currency_rate_date = date('Y-m-d');

        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function fields()
    {
        $fields = parent::fields();
        $fields['agreement'] = 'agreement';
        $fields['basisDocString'] = 'basisDocString';
        $fields['viewLink'] = 'viewLink';

        return $fields;
    }

    /**
     * @return query\InvoiceQuery
     */
    public static function find($config = [])
    {
        return new query\InvoiceQuery(get_called_class(), $config);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $outClosure = function (Invoice $model) {
            return $model->type == Documents::IO_TYPE_OUT;
        };
        $inClosure = function (Invoice $model) {
            return $model->type == Documents::IO_TYPE_IN;
        };
        $contractorScenario = $this->invoice_status_id == InvoiceStatus::STATUS_CREATED ? ['insert', 'default'] : 'insert';

        return ArrayHelper::merge(parent::rules(), [
            [['invoice_expenditure_item_id'], 'filter', 'when' => $inClosure, 'filter' => function ($value) {
                return intval($value) ?: null;
            }],
            [['invoice_income_item_id'], 'filter', 'when' => $outClosure, 'filter' => function ($value) {
                return intval($value) ?: null;
            }],
            [['sale_point_id', 'industry_id', 'project_id'], 'filter', 'filter' => function($value) {
                return intval($value) ?: null;
            }],
            [['show_article'], 'default', 'value' => 0],
            [['not_for_bookkeeping'], 'safe'],
            [['comment', 'comment_internal'], 'trim'],
            /* INSERT */
            [
                ['contractor_id'],
                'required', 'on' => $contractorScenario,
            ],
            [
                ['contractor_id'],
                'compare',
                'compareValue' => $this->getOldAttribute('contractor_id'),
                'when' => function ($model) {
                    return $model->type == Documents::IO_TYPE_OUT && !$model->isNewRecord && $model->company->isFreeTariff;
                },
                'message' => 'Вы не можете изменить контрагента на бесплатном тарифе.',
                'enableClientValidation' => false,
            ],
            [
                ['orders'], 'required',
                'message' => 'Не найдено ни одного товара/услуги.',
            ],
            [
                ['contractor_id',], 'exist',
                'targetClass' => Contractor::className(),
                'targetAttribute' => 'id',
                'filter' => [
                    'or',
                    ['company_id' => $this->company_id],
                    ['company_id' => null],
                ],
            ],
            [
                ['project_estimate_id'], 'exist',
                'targetClass' => ProjectEstimate::className(),
                'targetAttribute' => 'id',
            ],
            ['production_type', 'string', 'on' => 'insert',],
            ['isAutoinvoice', 'boolean', 'on' => 'insert',],
            [
                ['sale_point_id'], 'exist',
                'targetClass' => SalePoint::class,
                'targetAttribute' => 'id',
                'filter' => ['company_id' => $this->company_id],
            ],
            [
                ['industry_id'], 'exist',
                'targetClass' => CompanyIndustry::class,
                'targetAttribute' => 'id'
            ],

            /* COMMON */
            [
                ['invoice_status_id', 'document_number',], 'required',
            ],
            [['invoice_status_id', 'payment_form_id', 'remaining_amount', 'project_id', 'sale_point_id', 'industry_id',
                'payment_partial_amount', 'company_checking_accountant_id', 'autoinvoice_id', 'out_view_count',
                'out_save_count', 'out_pay_count', 'out_download_count', 'discount_type', 'contract_essence_template',
                'is_hidden_discount'], 'integer',],
            [['project_id', 'need_act', 'need_packing_list', 'need_upd',], 'safe'],
            [['document_date'], 'required'],
            [['payment_limit_date'], 'required',
                'when' => function (Invoice $model) {
                    return !$model->isAutoinvoice && $model->payment_limit_rule != self::PAYMENT_LIMIT_RULE_DOCUMENT;
                },
            ],
            [
                ['payment_limit_date'],
                function ($attribute, $params) {
                    if ((new \DateTime($this->$attribute)) < (new \DateTime($this->document_date))) {
                        $this->addError($attribute, 'Значение «Оплатить до» не должно быть меньше даты счета.');
                    }
                },
                'when' => function (Invoice $model) {
                    return !$model->isAutoinvoice;
                },
            ],
            [['is_deleted', 'show_popup', 'remind_contractor', 'has_discount', 'has_markup', 'has_weight', 'has_volume', 'show_article',
                'from_store_cabinet', 'from_out_invoice', 'from_demo_out_invoice', 'show_paylimit_info', 'is_invoice_contract',], 'boolean',],
            [
                ['total_amount_nds', 'total_amount_no_nds',],
                'number',
                'max' => Yii::$app->params['maxCashSum'] * 100, // with kopeck
                'tooBig' => 'Значение «{attribute}» не должно превышать ' . TextHelper::moneyFormat(Yii::$app->params['maxCashSum'], 2) . '.',
            ],
            [
                ['total_amount_with_nds'],
                'number',
                'min' => 1,
                'max' => Yii::$app->params['maxCashSum'] * 100, // with kopeck
                'tooBig' => 'Сумма счета не должна превышать ' .
                    TextHelper::moneyFormat(Yii::$app->params['maxCashSum'], 2) . '.',
                'tooSmall' => 'Сумма счета должна быть больше 0.',
            ],
            [['total_place_count', 'total_mass_gross'], 'string', 'max' => 255,],
            [['total_order_count'], 'integer', 'min' => 1, 'max' => 999999,],

            // company rs
            [
                ['company_rs'], 'exist',
                'skipOnEmpty' => true,
                'targetClass' => CheckingAccountant::class,
                'targetAttribute' => 'rs',
                'filter' => ['company_id' => $this->company_id],
            ],

            // type depending
            // out
            [['document_number',], 'integer', 'when' => $outClosure,],
            [['document_additional_number',], 'string', 'max' => 255, 'when' => $outClosure,],
            // in
            [['document_number'], 'string',
                'max' => 45,
                'when' => $inClosure,
            ],
            [['invoice_expenditure_item_id'], 'required', 'enableClientValidation' => false,
                //    'on' => [self::SCENARIO_UPDATE_SUBSCRIBE, self::SCENARIO_DEFAULT],
                'when' => $inClosure,
            ],
            [['invoice_expenditure_item_id'], 'exist',
                'targetClass' => InvoiceExpenditureItem::class,
                'targetAttribute' => 'id',
                'when' => $inClosure,
            ],
            [['invoice_income_item_id'], 'exist',
                'targetClass' => InvoiceIncomeItem::class,
                'targetAttribute' => 'id',
                'when' => $outClosure,
            ],
            [['nds_view_type_id',], 'required'],
            [['nds_view_type_id',], 'in', 'range' => array_keys(self::$ndsViewList)],
            [['document_number'], 'unique', 'targetAttribute' => ['company_id', 'document_number', 'document_additional_number'],
                'when' => function (Invoice $model) {
                    return $model->type == Documents::IO_TYPE_OUT && $model->invoice_status_id != InvoiceStatus::STATUS_AUTOINVOICE;
                },
                'filter' => function ($query) {
                    $year = date('Y', strtotime($this->document_date));
                    $query->andWhere([
                        'type' => Documents::IO_TYPE_OUT,
                        'is_deleted' => self::NOT_IS_DELETED,
                    ])
                        ->andWhere(['not', ['invoice_status_id' => InvoiceStatus::STATUS_AUTOINVOICE]])
                        ->andWhere([
                            'between',
                            'document_date',
                            $year.'-01-01',
                            $year.'-12-31',
                        ]);

                    return $query;
                },
                'message' => 'Исходящий счет с таким номером уже существует',
            ],
            [['agreement'], 'string', 'when' => $outClosure,],
            [['basis_document_name'], 'string', 'max' => 255, 'when' => $outClosure,],
            [['basis_document_number'], 'string', 'max' => 50, 'when' => $outClosure,],
            [['comment',], 'string', 'max' => 1000],
            [['contract_essence',], 'string', 'max' => 20000],
            [['comment_internal'], 'string', 'max' => 255],
            [['price_precision'], 'in', 'range' => self::$precisions],

            //Invoice Currency
            [
                [
                    'currency_name',
                    'currency_amount',
                    'currency_rate',
                    'currency_rate_type',
                    'currency_rate_date',
                    'currency_rate_amount',
                    'currency_rate_value',
                    'currency_rateDate',
                ],
                'required',
            ],
            [
                ['currencyAmountCustom', 'currencyRateCustom'], 'required',
                'when' => function ($model) {
                    return $model->currency_rate_type == Currency::RATE_CUSTOM;
                },
                'message' => 'Необходимо установить другой обменный курс',
            ],
            [
                ['currency_name'], 'exist',
                'targetClass' => Currency::className(),
                'targetAttribute' => 'name',
            ],
            [['currency_rate_type'], 'in', 'range' => Currency::$rateTypeList],
            [
                ['currency_amount', 'currency_rate_amount', 'currencyAmountCustom'],
                'integer',
                'min' => 1,
                'integerPattern' => '/^\s*1[0]*\s*$/',
                'message' => 'Значение «{attribute}» должно быть 1 или кратным 10.',
            ],
            [
                ['currency_rate', 'currency_rate_value', 'currencyRateCustom'],
                'number',
                'min' => 0.0001,
                'max' => 999999999.9999,
                'numberPattern' => '/^\s*\d{1,9}(\.\d{0,4})?\s*$/',
                'message' => 'Значение «{attribute}» должно быть числом и иметь не более 4 десятичных знаков.',
            ],
            [['currency_rate_date'], 'date', 'format' => 'php:Y-m-d'],
            [['currency_rateDate'], 'date', 'format' => 'php:d.m.Y'],
            [['price_round_rule'], 'default', 'value' => self::ROUND_RULE_DEFAULT],
            [['price_round_precision'], 'default', 'value' => self::ROUND_PRECISION_DEFAULT],
            [['price_round_rule'], 'in', 'range' => array_keys(self::$roundRules)],
            [
                ['price_round_precision'], 'in', 'range' => array_keys(self::$roundPrecisions),
                'when' => function (Invoice $model) {
                    return $model->price_round_rule != self::ROUND_RULE_NO;
                },
            ],
            [['store_id'], 'safe'],
            // payment limit variants
            [
                ['payment_limit_days'], 'required',
                'when' => function (Invoice $model) {
                    return in_array($model->payment_limit_rule, [self::PAYMENT_LIMIT_RULE_MIXED]);
                },
                'message' => 'Необходимо установить отсрочку оплаты в днях от даты счета'
            ],
            [
                ['related_document_payment_limit_days'], 'required',
                'when' => function (Invoice $model) {
                    return in_array($model->payment_limit_rule, [self::PAYMENT_LIMIT_RULE_MIXED, self::PAYMENT_LIMIT_RULE_DOCUMENT]);
                },
                'message' => 'Необходимо установить отсрочку оплаты в днях от даты документа'
            ],
            [
                ['payment_limit_percent'], 'required',
                'when' => function (Invoice $model) {
                    return in_array($model->payment_limit_rule, [self::PAYMENT_LIMIT_RULE_MIXED]);
                },
                'message' => 'Необходимо установить процент аванса от суммы счета'
            ],
            [
                ['related_document_payment_limit_percent'], 'required',
                'when' => function (Invoice $model) {
                    return in_array($model->payment_limit_rule, [self::PAYMENT_LIMIT_RULE_MIXED]);
                },
                'message' => 'Необходимо установить процент оставшейся суммы оплаты после выставления Акта, ТН или УПД'
            ],
            [['payment_limit_rule'], 'default', 'value' => 1],
            [['payment_limit_percent'], 'default', 'value' => 100],
            [['related_document_payment_limit_percent'], 'default', 'value' => 0],
            [['payment_limit_rule'], 'in', 'range' => [
                self::PAYMENT_LIMIT_RULE_INVOICE,
                self::PAYMENT_LIMIT_RULE_DOCUMENT,
                self::PAYMENT_LIMIT_RULE_MIXED,
            ]],
            [['payment_limit_days', 'related_document_payment_limit_days'], 'integer', 'min' => 0],
            [['payment_limit_percent', 'related_document_payment_limit_percent'], 'integer', 'min' => 0, 'max' => 100],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Тип (входящий/исходящий)',
            'invoice_status_id' => 'Статус',
            'invoice_status_updated_at' => 'Дата обнволения статуса',
            'invoice_status_author_id' => 'Автор статуса',
            'created_at' => 'Дата создания',
            'document_author_id' => 'Автор документа',
            'document_date' => 'Дата документа',
            'document_number' => 'Номер документа',
            'document_additional_number' => 'Дополнительный номер документа',
            'payment_limit_date' => 'Оплатить до',
            'production_type' => 'Тип продукции',
            'invoice_expenditure_item_id' => 'Статья расходов',
            'invoice_income_item_id' => 'Статья приходов',
            'company_id' => 'Компания',
            'company_name_full' => 'Полное название компании',
            'company_name_short' => 'Краткое название компании',
            'company_bank_name' => 'Название банка компании',
            'company_bank_city' => 'Город банка компании',
            'company_inn' => 'ИНН компании',
            'company_kpp' => 'КПП компании',
            'company_egrip' => 'ЕГРИП компании',
            'company_okpo' => 'ОКПО компании',
            'company_bik' => 'БИК компании',
            'company_ks' => 'КС компании',
            'company_rs' => 'Мой рас.счет',
            'company_phone' => 'Телефон компании',
            'company_address_legal_full' => 'Юридический адрес компании',
            'company_chief_post_name' => 'Должность руководителя компании',
            'company_chief_lastname' => 'Фамилия руководителя компании',
            'company_chief_firstname_initials' => 'Company Chief Firstname Initials',
            'company_chief_patronymic_initials' => 'Company Chief Patronymic Initials',
            'company_print_filename' => 'Печать компании',
            'company_chief_signature_filename' => 'Company Chief Signature Filename',
            'company_chief_accountant_lastname' => 'Company Chief Accountant Lastname',
            'company_chief_accountant_firstname_initials' => 'Company Chief Accountant Firstname Initials',
            'company_chief_accountant_patronymic_initials' => 'Company Chief Accountant Patronymic Initials',
            'contractor_id' => $this->getContractorLabel(),
            'contractor_name_full' => 'Contractor Name Full',
            'contractor_name_short' => 'Contractor Name Short',
            'contractor_bank_name' => 'Contractor Bank Name',
            'contractor_bank_city' => 'Contractor Bank City',
            'contractor_address_legal_full' => 'Contractor Address Legal Full',
            'contractor_inn' => 'Contractor Inn',
            'contractor_kpp' => 'Contractor Kpp',
            'contractor_bik' => 'Contractor Bik',
            'contractor_ks' => 'Contractor Ks',
            'contractor_rs' => 'Contractor Rs',
            'remind_contractor' => 'Отправить напоминание',
            'total_place_count' => 'Total Place Count',
            'total_mass_gross' => 'Total Mass Gross',
            'total_amount_no_nds' => 'Всего без НДС',
            'total_amount_with_nds' => 'Всего с НДС',
            'total_amount_has_nds' => 'Total Amount Has Nds',
            'total_amount_nds' => 'В том числе НДС',
            'total_order_count' => 'Количество наименований',
            'payment_form_id' => 'Payment Form ID',
            'payment_partial_amount' => 'Payment Partial Amount',
            'is_deleted' => 'Удалён',
            'email_messages' => 'Колличество отправленных писем',
            'company_checking_accountant_id' => 'Расчетный счет',
            'basis_document_name' => 'Основание',
            'basis_document_number' => 'Номер основания',
            'basis_document_date' => 'Дата основания',
            'remaining_amount' => 'Оставшаяся для оплаты сумма',
            'comment' => 'Комментарий в счете',
            'comment_internal' => 'Комментарий',
            'price_precision' => 'Количество знаков после запятой',
            'has_discount' => 'Указать скидку',
            'has_markup' => 'Указать наценку',
            'has_weight' => 'Указать вес',
            'has_volume' => 'Указать объем',
            'currency_name' => 'Валюта счета',
            'currency_rate_type' => 'Метод установки курса',
            'currency_rate_date' => 'Дата установки курса валюты',
            'currency_rateDate' => 'Дата установки курса валюты',
            'currency_amount' => 'Номинал валюты',
            'currency_rate_amount' => 'Номинал валюты',
            'currencyAmountCustom' => 'Номинал валюты',
            'currency_rate' => 'Обменный курс валюты',
            'currency_rate_value' => 'Обменный курс валюты',
            'currencyRateCustom' => 'Другой обменный курс',
            'show_paylimit_info' => 'Другой обменный курс',
            'contract_essence' => 'Предмет договора',
            'project_id' => 'Проект',
            'project_estimate_id' => 'Смета',
            'sale_point_id' => 'Точка продаж',
            'industry_id' => 'Направление',
            'payment_limit_rule' => 'Правила оплаты',
            'payment_limit_percent' => 'Процент суммы аванса от суммы счета',
            'related_document_payment_limit_percent' => 'Процент оставщейся суммы оплаты',
            'payment_limit_days' => 'Отсрочка оплаты в днях от даты счета',
            'related_document_payment_limit_days' => 'Отсрочка оплаты в днях от даты Акта, ТН, УПД',
        ];
    }

    /**
     * @return string
     */
    public function getContractorLabel()
    {
        switch ($this->type) {
            case Documents::IO_TYPE_IN:
                return 'Поставщик';
                break;

            case Documents::IO_TYPE_OUT:
                return 'Покупатель';
                break;

            default:
                return 'Контрагент';
                break;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAmocrmWidgetInvoices()
    {
        return $this->hasMany(AmocrmWidgetInvoice::className(), ['invoice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContractor()
    {
        return $this->hasOne(Contractor::className(), ['id' => 'contractor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentForm()
    {
        return $this->hasOne(PaymentForm::className(), ['id' => 'payment_form_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceStatus()
    {
        return $this->hasOne(status\InvoiceStatus::className(), ['id' => 'invoice_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentStatus()
    {
        return $this->getInvoiceStatus();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceStatusAuthor()
    {
        return $this->hasOne(Employee::className(), ['id' => 'invoice_status_author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceExpenditureItem()
    {
        return $this->hasOne(InvoiceExpenditureItem::className(), ['id' => 'invoice_expenditure_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceIncomeItem()
    {
        return $this->hasOne(InvoiceIncomeItem::className(), ['id' => 'invoice_income_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentAuthor()
    {
        return $this->hasOne(Employee::className(), ['id' => 'document_author_id']);
    }

    /**
     * @return query\OrderQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['invoice_id' => 'id'])
            ->orderNumber();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSignBasisDocument()
    {
        return $this->hasOne(DocumentType::className(), ['id' => 'sign_document_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSignEmployeeCompany()
    {
        return $this->hasOne(EmployeeCompany::className(), ['employee_id' => 'signed_by_employee_id'])
            ->andWhere(['company_id' => $this->company_id]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashBankFlows()
    {
        return $this->hasMany(CashBankFlows::className(), ['id' => 'flow_id'])
            ->viaTable(CashBankFlowToInvoice::tableName(), ['invoice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashEmoneyFlows()
    {
        return $this->hasMany(CashEmoneyFlows::className(), ['id' => 'flow_id'])
            ->viaTable(CashEmoneyFlowToInvoice::tableName(), ['invoice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashOrderFlows()
    {
        return $this->hasMany(CashOrderFlows::className(), ['id' => 'flow_id'])
            ->viaTable(CashOrderFlowToInvoice::tableName(), ['invoice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBankPayments()
    {
        return $this->hasMany(CashBankFlowToInvoice::className(), ['invoice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmoneyPayments()
    {
        return $this->hasMany(CashEmoneyFlowToInvoice::className(), ['invoice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderPayments()
    {
        return $this->hasMany(CashOrderFlowToInvoice::className(), ['invoice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreementType()
    {
        return $this->hasOne(AgreementType::className(), ['id' => 'basis_document_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNdsViewType()
    {
        return $this->hasOne(NdsViewType::className(), ['id' => 'nds_view_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutInvoice()
    {
        return $this->hasOne(OutInvoice::className(), ['id' => 'by_out_link_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRentAgreements()
    {
        return $this->hasMany(RentAgreement::className(), ['company_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGoodsCancellations()
    {
        if (!isset($this->_data['getGoodsCancellations'])) {
            $data = [];
            if ($this->has_act) {
                foreach ($this->acts as $doc) {
                    $data = array_merge($data, $doc->goodsCancellations);
                }
            }
            if ($this->has_upd) {
                foreach ($this->upds as $doc) {
                    $data = array_merge($data, $doc->goodsCancellations);
                }
            }
            $this->_data['getGoodsCancellations'] = $data;
        }

        return $this->_data['getGoodsCancellations'];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRent()
    {
        return $this->getRentAgreements();
    }

    /**
     * @return boolean
     */
    public function getHasFlows()
    {
        return $this->getCashBankFlows()->exists() || $this->getCashEmoneyFlows()->exists() || $this->getCashOrderFlows()->exists();
    }

    /**
     * @return array common\models\cash\CashFlowsBase
     */
    public function getAllFlows()
    {
        return $this->getCashBankFlows()->all() + $this->getCashEmoneyFlows()->all() + $this->getCashOrderFlows()->all();
    }

    /**
     * @return array common\models\cash\CashFlowsBase
     */
    public function allFlowsQuery($select = [], $where = [])
    {
        return $this->getCashBankFlows()->select([
            'flow' => new \yii\db\Expression('"bank"'),
        ])->addSelect($select)->andWhere($where)->union($this->getCashEmoneyFlows()->select([
            'flow' => new \yii\db\Expression('"emoney"'),
        ])->addSelect($select)->andWhere($where), true)->union($this->getCashOrderFlows()->select([
            'flow' => new \yii\db\Expression('"order"'),
        ])->addSelect($select)->andWhere($where), true);
    }

    /**
     * @return yii\db\Query
     */
    public function allPaymentsQuery($select = [], $where = [])
    {
        return $this->getEmoneyPayments()->select([
            'flow' => new \yii\db\Expression('"bank"'),
        ])->addSelect($select)->andWhere($where)->union($this->getOrderPayments()->select([
            'flow' => new \yii\db\Expression('"order"'),
        ])->addSelect($select)->andWhere($where), true)->union($this->getEmoneyPayments()->select([
            'flow' => new \yii\db\Expression('"emoney"'),
        ])->addSelect($select)->andWhere($where), true);
    }

    /**
     * @param $class
     * @return static
     */
    public function getPaidAmount()
    {
        return (int)$this->getBankPayments()->select('amount')
            ->union($this->getEmoneyPayments()->select('amount'), true)
            ->union($this->getOrderPayments()->select('amount'), true)
            ->sum('amount');
    }

    /**
     * @param $class
     * @return static
     */
    public function getAvailablePaymentAmount()
    {
        return max(0, $this->total_amount_with_nds - $this->getPaidAmount());
    }

    /**
     * @param Invoice $invoice
     * @return int
     */
    public function getIsFullyPaid()
    {
        if ($this->total_amount_with_nds == 0 && !$this->getCashBankFlows()->exists() &&
            !$this->getCashOrderFlows()->exists() && !$this->getCashEmoneyFlows()->exists()) {
            return false;
        }
        return !$this->getAvailablePaymentAmount();
    }

    /**
     * @param Invoice $invoice
     * @return int
     */
    public function getLastPaymentDate()
    {
        $date = $this->getCashBankFlows()->select('date')->union(
            $this->getCashOrderFlows()->select('date')
        )->union(
            $this->getCashEmoneyFlows()->select('date')
        )->max('date');

        return $date ? strtotime($date . ' ' . date('H:i')) : 0;
    }

    /**
     * @param Invoice $invoice
     * @return int
     */
    public function checkPaymentStatus($cashFlow = null)
    {
        $paid = min($this->total_amount_with_nds, $this->getPaidAmount());
        $remaining = $this->total_amount_with_nds - $paid;
        if ($paid == 0) {
            if (in_array($this->invoice_status_id, [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_PAYED_PARTIAL])) {
                $this->invoice_status_id = $this->type == Documents::IO_TYPE_OUT ?
                    InvoiceStatus::STATUS_SEND :
                    InvoiceStatus::STATUS_CREATED;
            }
        } else {
            $lastPaymentTimestamp = $this->getLastPaymentDate();
            $this->invoice_status_id = ($remaining == 0) ? InvoiceStatus::STATUS_PAYED : InvoiceStatus::STATUS_PAYED_PARTIAL;
            $this->invoice_status_updated_at = $lastPaymentTimestamp ?: time();
        }

        if ($paid != $this->payment_partial_amount || $remaining != $this->remaining_amount) {
            $this->updatePaid($paid, $remaining);
            $this->updateDocumentsPaid();
        }

        if ($this->isAttributeChanged('invoice_status_id')) {
            if (Yii::$app->id != 'app-console') {
                LogHelper::save($this, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function (Invoice $model) {
                    return $model->save(true, ['invoice_status_id', 'invoice_status_updated_at', 'invoice_status_author_id']);
                });
            } else {
                $this->save(true, ['invoice_status_id', 'invoice_status_updated_at', 'invoice_status_author_id']);
            }

            if ($paid > 0 &&
                $this->type == Documents::IO_TYPE_OUT &&
                !$this->has_act &&
                $this->canAddAct &&
                $this->company->autoact->rule == Autoact::BY_PAYMENT
            ) {
                $autoact = $this->company->autoact;
                if ((!$autoact->no_pay_partial || $remaining == 0) &&
                    (!$autoact->no_pay_order || !$this->getOrderPayments()->exists()) &&
                    (!$autoact->no_pay_emoney || !$this->getEmoneyPayments()->exists())
                ) {
                    $this->createAct(date('d.m.Y', $lastPaymentTimestamp));
                    if ($this->act && $autoact->send_auto && ($sendTo = $this->contractor->someEmail)) {
                        $sender = EmployeeCompany::findOne([
                            'company_id' => $this->company_id,
                            'employee_id' => $this->act->document_author_id,
                        ]);
                        if ($sender) {
                            $this->act->sendAsEmail($sender, $sendTo);
                        }
                    }
                }
            }
            if ($cashFlow !== null &&
                in_array($this->invoice_status_id, [
                    InvoiceStatus::STATUS_PAYED,
                    InvoiceStatus::STATUS_PAYED_PARTIAL
                ])
            ) {
                $this->checkForPaymentReminder($cashFlow);
            }
        }

        return true;
    }

    /**
     * @param integer $paid
     * @param integer $remaining
     */
    public function updatePaid($paid, $remaining)
    {
        $this->updateAttributes([
            'payment_partial_amount' => $paid,
            'remaining_amount' => $remaining,
        ]);
    }

    /**
     * Calc remaining amount for each related document
     */
    public function updateDocumentsPaid()
    {
        $paymentSum = $this->payment_partial_amount;
        $allDocs = array_merge($this->acts, $this->packingLists, $this->upds);
        usort($allDocs, function($a, $b){
            return $a->document_date > $b->document_date;
        });

        // todo: move to self method
        $maxDocumentDate = ($allDocs) ? ArrayHelper::getValue($allDocs[count($allDocs)-1], 'document_date') : null;
        if ($maxDocumentDate != $this->max_related_document_date) {
            try {
                $limitDays = intval($this->related_document_payment_limit_days) ?: 10;
                $paymentLimitDocumentDate = date_create($maxDocumentDate)->modify("+ {$limitDays} day")->format('Y-m-d');
            } catch (\Exception $e) {
                $paymentLimitDocumentDate = null;
                Yii::error(VarDumper::dumpAsString($e), 'validation');
            }
            $this->updateAttributes([
                'max_related_document_date' => $maxDocumentDate,
                'related_document_payment_limit_date' => $paymentLimitDocumentDate
            ]);
        }

        foreach ($allDocs as $doc) {
            $remainingAmount = 0;
            // PACKING LIST
            if ($doc instanceof PackingList) {
                // calc current invoice
                if ($paymentSum >= $doc->orders_sum) {
                    $paymentSum -= $doc->orders_sum;
                } else {
                    $remainingAmount += $doc->orders_sum - $paymentSum;
                    $paymentSum = 0;
                }
                if ($remainingAmount != $doc->remaining_amount) {
                    $doc->updateAttributes(['remaining_amount' => $remainingAmount]);
                }
            } elseif ($doc instanceof Upd) {
                // UPD
                foreach ($doc->invoices as $invoice) {
                    $partSum = (count($doc->invoices) === 1) ? $doc->orders_sum : $invoice->total_amount_with_nds;
                    // calc current invoice
                    if ($invoice->id == $this->id) {
                        if ($paymentSum >= $partSum) {
                            $paymentSum -= $partSum;
                        } else {
                            $remainingAmount += $partSum - $paymentSum;
                            $paymentSum = 0;
                        }
                        // calc other related invoices
                    } else {
                        $remainingAmount += $invoice->remaining_amount;
                    }
                }
                if ($remainingAmount != $doc->remaining_amount) {
                    $doc->updateAttributes(['remaining_amount' => $remainingAmount]);
                }
            } elseif ($doc instanceof Act) {
                // ACT
                foreach ($doc->invoices as $invoice) {
                    $hasGoods = count(explode(',', $invoice->production_type)) > 1;
                    if ($hasGoods) {
                        $servicesPartSum = array_reduce(
                            $invoice->getOrdersByProductType(Product::PRODUCTION_TYPE_SERVICE),
                            function($carry, $item) use ($invoice) {
                                return $carry += ($invoice->type == 2) ? $item->amount_sales_with_vat : $item->amount_purchase_with_vat;
                            }, 0);
                        $goodsPartSum = $invoice->total_amount_with_nds - $servicesPartSum;

                    } else {
                        $servicesPartSum = $invoice->total_amount_with_nds;
                        $goodsPartSum = 0;
                    }

                    $partSum = (count($doc->invoices) === 1) ? $doc->order_sum : $servicesPartSum;

                    // current invoice
                    if ($invoice->id == $this->id) {
                        if ($paymentSum >= $partSum) {
                            $paymentSum -= $partSum;
                        } else {
                            $remainingAmount += $partSum - $paymentSum;
                            $paymentSum = 0;
                        }
                        // other related invoices
                    } else {
                        $remainingAmount += max(0, $invoice->remaining_amount - $goodsPartSum);
                    }
                }

                if ($remainingAmount != $doc->remaining_amount) {
                    $doc->updateAttributes(['remaining_amount' => $remainingAmount]);
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    private function checkForPaymentReminder($cashFlow)
    {
        if (in_array($this->payment_limit_rule, [self::PAYMENT_LIMIT_RULE_DOCUMENT, self::PAYMENT_LIMIT_RULE_MIXED])) {
            // todo: add payment reminder by related document
            return;
        }

        if ($this->invoice_status_updated_at > strtotime($this->payment_limit_date)) {
            return;
        }

        $reminderMessage = $this->company->getActualPaymentReminderMessages()->andWhere([
            'number' => PaymentReminderMessage::MESSAGE_10,
        ])->one();

        if ($reminderMessage) {
            if ($reminderMessage->not_send_where_order_payment && $cashFlow instanceof CashOrderFlows) {
                return;
            }
            if ($reminderMessage->not_send_where_emoney_payment && $cashFlow instanceof CashEmoneyFlows) {
                return;
            }
            if ($reminderMessage->not_send_where_partial_payment && $this->remaining_amount > 0) {
                return;
            }
            if ($settings = $this->company->paymentReminderSettings) {
                switch ($settings->send_mode) {
                    case Settings::SEND_BY_SUITABLE:
                        if ($settings->invoice_paid && !in_array($this->contractor_id, $settings->excludeIds)) {
                            PaymentReminder::sendForPaidInvoice($this, $reminderMessage);
                        }
                        break;
                    case Settings::SEND_TO_SELECTED:
                        if (PaymentReminderMessageContractor::find()->andWhere([
                            'company_id' => $this->company_id,
                            'contractor_id' => $this->contractor_id,
                            'message_'.$reminderMessage->number => 1,
                        ])->exists()) {
                            PaymentReminder::sendForPaidInvoice($this, $reminderMessage);
                        }
                        break;
                }
            }
        }
    }

    /**
     * @param $class
     * @return static
     */
    private function getCashFlow($class)
    {
        return $this->hasMany($class, ['invoice_id' => 'id'])
            ->andWhere(['flow_type' => CashFactory::$documentToFlowType[$this->type],]);
    }

    /**
     * @param $class
     * @return static
     */
    public function getPaymentType()
    {
        if ($this->getCashBankFlows()->exists()) {
            return 1;
        }
        if ($this->getCashOrderFlows()->exists()) {
            return 2;
        }
        if ($this->getCashEmoneyFlows()->exists()) {
            return 3;
        }

        return 0;
    }

    /**
     * @return string
     */
    public function getPaymentIcon()
    {
        $type = $this->getPaymentType();

        if (isset(static::$invoicePaymentIcon[$type])) {
            return static::$invoicePaymentIcon[$type];
        }

        return '';
    }

    /**
     * @return string
     */
    public function getPaymentLabel()
    {
        $type = $this->getPaymentType();

        if (isset(static::$invoicePaymentLabel[$type])) {
            return static::$invoicePaymentLabel[$type];
        }

        return '';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceFacture()
    {
        return $this->hasOne(InvoiceFacture::className(), ['id' => 'invoice_facture_id'])->viaTable('invoice_invoice_facture', ['invoice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceFactures()
    {
        return $this->hasMany(InvoiceFacture::className(), ['id' => 'invoice_facture_id'])->viaTable('invoice_invoice_facture', ['invoice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscribe()
    {
        return $this->hasOne(Subscribe::className(), ['id' => 'subscribe_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPayment()
    {
        return $this->hasOne(Payment::className(), ['id' => 'service_payment_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentOrder()
    {
        return $this->hasOne(PaymentOrder::className(), ['invoice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderDocument()
    {
        return $this->hasOne(OrderDocument::className(), ['invoice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgentReport()
    {
        return $this->hasOne(AgentReport::className(), ['invoice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPackingList()
    {
        return $this->hasOne(Documents::getModel(Documents::DOCUMENT_PACKING_LIST), ['invoice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPackingLists()
    {
        return $this->hasMany(Documents::getModel(Documents::DOCUMENT_PACKING_LIST), ['invoice_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     * @throws NotFoundHttpException
     */
    public function getSalesInvoice()
    {
        return $this->hasOne(Documents::getModel(Documents::DOCUMENT_SALES_INVOICE), ['invoice_id' => 'id']);
    }

    /**
     * @return ActiveQuery
     * @throws NotFoundHttpException
     */
    public function getSalesInvoices()
    {
        return $this->hasMany(Documents::getModel(Documents::DOCUMENT_SALES_INVOICE), ['invoice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProxy()
    {
        return $this->hasOne(Documents::getModel(Documents::DOCUMENT_PROXY), ['invoice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProxies()
    {
        return $this->hasMany(Documents::getModel(Documents::DOCUMENT_PROXY), ['invoice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWaybill()
    {
        return $this->hasOne(Documents::getModel(Documents::DOCUMENT_WAYBILL), ['invoice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWaybills()
    {
        return $this->hasMany(Documents::getModel(Documents::DOCUMENT_WAYBILL), ['invoice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAct()
    {
        return $this->hasOne(Act::className(), ['id' => 'act_id'])->viaTable('invoice_act', ['invoice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActs()
    {
        return $this->hasMany(Act::className(), ['id' => 'act_id'])->viaTable('invoice_act', ['invoice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceActs()
    {
        return $this->hasMany(InvoiceAct::className(), ['invoice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceUpds()
    {
        return $this->hasMany(InvoiceUpd::className(), ['invoice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpd()
    {
        return $this->hasOne(Upd::className(), ['id' => 'upd_id'])->viaTable('invoice_upd', ['invoice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpds()
    {
        return $this->hasMany(Upd::className(), ['id' => 'upd_id'])->viaTable('invoice_upd', ['invoice_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function getFile()
    {
        return $this->hasOne(File::className(), ['owner_id' => 'id'])
            ->onCondition(['owner_model' => Invoice::className()]);
    }

    /**
     * @inheritdoc
     */
    public function getFiles()
    {
        return $this->hasMany(File::className(), ['owner_id' => 'id'])
            ->onCondition([File::tableName() . '.owner_model' => Invoice::className()]);
    }

    /**
     * @inheritdoc
     */
    public function getScanDocuments()
    {
        return $this->hasMany(ScanDocument::className(), ['owner_id' => 'id'])
            ->onCondition([ScanDocument::tableName() . '.owner_model' => Invoice::className(),]);
    }

    /**
     * @param array $value
     */
    public function setContractorPassport($value)
    {
        $data = [];
        foreach (Contractor::$passportAttributes as $attr) {
            $data[$attr] = ArrayHelper::getValue($value, $attr, '');
        }

        $this->contractor_passport = Json::encode($data);
    }

    /**
     * @return array
     */
    public function getContractorPassport()
    {
        $value = Json::decode($this->contractor_passport);
        $data = [];
        foreach (Contractor::$passportAttributes as $attr) {
            $data[$attr] = ArrayHelper::getValue($value, $attr, '');
        }

        return $data;
    }

    /**
     * @inheritdoc
     */
    public function ordersLoad($ordersData)
    {
        $orderArray = [];
        $orderDataArray = [];
        $noNds = true;
        $this->removedOrders = $this->getOrders()->indexBy('id')->all();
        if (is_array($ordersData)) {
            foreach ($ordersData as $data) {
                $productId = ArrayHelper::getValue($data, 'product_id');
                $productQuery = Product::find()->andWhere([
                    'id' => $productId,
                    'company_id' => $this->company_id,
                    'not_for_sale' => $this->type == Documents::IO_TYPE_IN ? [0, 1] : 0,
                ]);

                $orderQuery = Order::find()->andWhere([
                    'id' => ArrayHelper::getValue($data, 'id'),
                    'invoice_id' => $this->id,
                    'product_id' => $productId,
                ]);

                if ($order = $orderQuery->one()) {
                    $order->populateRelation('invoice', $this);
                } elseif ($product = $productQuery->one()) {
                    if ($this->type == Documents::IO_TYPE_OUT && isset($data['sale_tax_rate_id'])) {
                        $product->price_for_sell_nds_id = $data['sale_tax_rate_id'];
                    } elseif ($this->type == Documents::IO_TYPE_IN && isset($data['purchase_tax_rate_id'])) {
                        $product->price_for_buy_nds_id = $data['purchase_tax_rate_id'];
                    }
                    $order = OrderHelper::createOrderByProduct($product, $this, null);
                } else {
                    $order = null;
                }

                $hasNewSaleTaxRate = (isset($data['sale_tax_rate_id'])) ? $data['sale_tax_rate_id'] != TaxRate::RATE_WITHOUT : false;
                $hasNewPurchaseTaxRate = (isset($data['purchase_tax_rate_id'])) ? $data['purchase_tax_rate_id'] != TaxRate::RATE_WITHOUT : false;

                if ($order !== null) {
                    if (($this->type == Documents::IO_TYPE_IN && (!isset($data['purchase_tax_rate_id']) && $order->purchase_tax_rate_id != TaxRate::RATE_WITHOUT || $hasNewPurchaseTaxRate)) ||
                        ($this->type == Documents::IO_TYPE_OUT && (!isset($data['sale_tax_rate_id']) && $order->sale_tax_rate_id != TaxRate::RATE_WITHOUT || $hasNewSaleTaxRate))
                    ) {
                        $noNds = false;
                    }
                    $orderArray[] = $order;
                    $orderDataArray[] = $data;
                    if (isset($this->removedOrders[$order->id])) {
                        unset($this->removedOrders[$order->id]);
                    }
                }
            }
            if ($orderArray && $noNds) {
                $this->nds_view_type_id = NdsViewType::NDS_VIEW_WITHOUT;
            } else {
                $postInvoice = Yii::$app->request->post('Invoice');
                $this->nds_view_type_id = ArrayHelper::getValue($postInvoice, 'nds_view_type_id', $this->company->nds_view_type_id);
            }

            array_walk($orderArray, function ($order, $key) use ($orderDataArray) {
                $order->load($orderDataArray[$key], '');
                $order->calculateOrder();
                $order->number = $key + 1;
            });
        }

        $this->populateRelation('orders', $orderArray);
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if ($this->isNewRecord) {
            $this->invoice_status_id = $this->isAutoinvoice ? InvoiceStatus::STATUS_AUTOINVOICE : InvoiceStatus::STATUS_CREATED;
        }
        if (empty($this->price_round_precision)) {
            $this->price_round_precision = self::ROUND_PRECISION_DEFAULT;
        }

        // contractor
        if ($this->contractor_id && ($contractor = Contractor::findOne($this->contractor_id)) !== null) {
            if ($this->getIsContractorEditable()) {
                $this->setContractor($contractor);
                $this->populateRelation('contractor', $contractor);
            } else {
                $this->contractor_id = $this->getOldAttribute('contractor_id');
            }

            if ($this->invoice_income_item_id === null)
                $this->invoice_income_item_id = $contractor->invoice_income_item_id;
        }

        // calculate total amount
        $this->calculateTotalAmount();

        $agreement = explode('&', $this->agreement);
        if (count($agreement) >= 3) {
            $this->basis_document_name = ArrayHelper::getValue($agreement, 0);
            $this->basis_document_number = ArrayHelper::getValue($agreement, 1);
            $this->basis_document_date = ArrayHelper::getValue($agreement, 2) ?: null;
            $this->basis_document_type_id = ArrayHelper::getValue($agreement, 3) ?: null;
            $this->agreement_new_id = ArrayHelper::getValue($agreement, 4) ?: null;
        } else {
            $this->basis_document_name = null;
            $this->basis_document_number = null;
            $this->basis_document_date = null;
            $this->basis_document_type_id = null;
            $this->agreement_new_id = null;
        }

        $this->isAutoinvoice = (int)$this->isAutoinvoice;

        if (!$this->company || (!$this->allowed_strict_mode && $this->company->strict_mode == Company::ON_STRICT_MODE)) {
            $this->addError('company_id', 'Необходимо заполнить реквизиты вашей компании');
        }

        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->payment_limit_date === '') {
                $this->payment_limit_date = null;
            }
            if ($this->is_deleted && ($flowsArray = $this->getAllFlows())) {
                foreach ($flowsArray as $flow) {
                    $flow->unlinkInvoice($this);
                }
            }

            if (Yii::$app->id == 'app-frontend' &&
                !Yii::$app->user->isGuest &&
                $this->company_id == Yii::$app->user->identity->company->id
            ) {
                $user = Yii::$app->user->identity;
            } else {
                $user = $this->company->employeeChief;
            }

            if ($insert || $this->isAttributeChanged('invoice_status_id', false)) {
                if ($this->isAutoinvoice) {
                    if ($insert) {
                        $this->invoice_status_updated_at = time();
                        $this->invoice_status_author_id = $user->id;
                        $this->document_author_id = $user->id;
                    }
                    $this->invoice_status_id = InvoiceStatus::STATUS_AUTOINVOICE;
                } else {
                    $statusArray = [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_PAYED_PARTIAL, InvoiceStatus::STATUS_OVERDUE];
                    if (!in_array($this->invoice_status_id, $statusArray)) {
                        $this->invoice_status_updated_at = time();
                    }
                    $this->invoice_status_author_id = (Yii::$app->id == 'app-console' || $user === null) ? $this->document_author_id : $user->id;
                }
            }

            $this->has_services = 0;
            $this->has_goods = 0;
            foreach ($this->orders as $order) {
                switch ($order->product->production_type) {
                    case Product::PRODUCTION_TYPE_SERVICE:
                        $this->has_services = 1;
                        break;
                    case Product::PRODUCTION_TYPE_GOODS:
                        $this->has_goods = 1;
                        break;
                }
            }
            if ($insert) {
                $this->setCompany($this->company);
                $this->uid = static::generateUid();
                $this->can_add_act = $this->has_services;
                $this->can_add_packing_list = $this->has_goods;
                $this->can_add_sales_invoice = $this->has_goods;
                $this->can_add_proxy = $this->has_goods;
            }

            if (empty($this->object_guid))
                $this->object_guid = OneCExport::generateGUID();

            $this->checkOverdue();

            if (($this->isAttributeChanged('is_deleted') && $this->is_deleted) ||
                ($this->isAttributeChanged('invoice_status_id') && $this->invoice_status_id == InvoiceStatus::STATUS_REJECTED)
            ) {
                foreach ($this->orders as $order) {
                    if ($order->reserve > 0) {
                        $order->updateAttributes([
                            'reserve' => 0,
                        ]);
                    }
                }

                if ($this->invoice_status_id == InvoiceStatus::STATUS_REJECTED && !$this->rejectDacuments()) {
                    return false;
                }
            }

            $rs = $this->company->getCheckingAccountants()
                ->andWhere(['rs' => $this->company_rs])
                ->orderBy(['type' => SORT_ASC])
                ->one();
            if ($rs) {
                $this->company_checking_accountant_id = $rs->id;
                $this->company_bank_name = $rs->bank_name;
                $this->company_bank_city = $rs->bank_city;
                $this->company_bik = $rs->bik;
                $this->company_ks = $rs->ks;
            } else {
                $this->setCheckingAccountant($this->company);
            }

            if ($this->is_additional_number_before === null) {
                $this->is_additional_number_before = $this->company->is_additional_number_before;
            }

            if ($insert) {
                $this->payment_partial_amount = 0;
                $this->remaining_amount = $this->total_amount_with_nds;
            } else {
                $this->payment_partial_amount = min($this->total_amount_with_nds, $this->getPaidAmount());
                $this->remaining_amount = $this->total_amount_with_nds - $this->payment_partial_amount;
            }

            $this->is_invoice_contract = (boolean)$this->is_invoice_contract;

            $this->create_type_id = ArrayHelper::getValue(Yii::$app->params, 'create_type_id', 1);
            $this->create_api_id = ArrayHelper::getValue(Yii::$app->params, 'create_api_id', null);

            if ($this->store_id) {
                if (Store::find()->where(['id' => $this->store_id, 'is_main' => 1])->exists()) {
                    $this->store_id = null;
                }
            }
            //if ($store = Store::findOne(['company_id' => $this->company_id, 'responsible_employee_id' => $this->document_author_id]))
            //    $this->store_id = $store->id;

            if (empty(trim($this->basis_document_date)) || date_create_from_format('Y-m-d', $this->basis_document_date) === false) {
                $this->basis_document_date = null;
            }

            if (empty($this->responsible_employee_id)) {
                $this->responsible_employee_id = $this->document_author_id;
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function delete()
    {
        if ($this->beforeDelete() && $this->deletingAll()) {
            $this->afterDelete();

            return 1;
        }

        return false;
    }

    /**
     * Deleting the invoice and all documents
     *
     * @return string
     */
    public function deletingAll()
    {
        if (in_array($this->invoice_status_id, InvoiceStatus::$deleteAllowed)) {
            return LogHelper::save($this, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_DELETE, function (Invoice $model) {
                $model->is_deleted = true;
                \common\models\company\CompanyFirstEvent::checkEvent($model->company, 14);

                if ($model->deleteDocuments() && $model->save(false, ['is_deleted'])) {
                    // Delete Autoplan Flow
                    \frontend\modules\analytics\models\PlanCashContractor::deleteAllFlowsByInvoice($model);

                    return true;
                }
            });
        }

        return false;
    }

    /**
     * @return bool
     * @throws \yii\db\StaleObjectException
     */
    public function deleteDocuments()
    {
        $modelArray = array_merge(
            $this->getActs()->all(),
            $this->getPackingLists()->all(),
            $this->getWaybills()->all(),
            $this->getInvoiceFactures()->all(),
            $this->getUpds()->all(),
            $this->getPaymentOrder()->all(),
            $this->getSalesInvoices()->all()
        );
        if ($modelArray) {
            foreach ($modelArray as $model) {
                if ($model instanceof Act) {
                    foreach ($model->emailFiles as $emailFile) {
                        $emailFile->delete();
                    }
                }
                if (!LogHelper::delete($model, LogEntityType::TYPE_DOCUMENT)) {
                    $this->addError('id', "Связанный документ {$model->printablePrefix} № {$model->fullNumber} не может быть удален.");

                    return false;
                }
            }
        }
        if ($this->orderDocument) {
            $orderDocument = $this->orderDocument;
            $orderDocument->invoice_id = null;
            $orderDocument->save(true, ['invoice_id']);
        }
        if ($this->agentReport) {
            $agentReport = $this->agentReport;
            $agentReport->unsetStatusByInvoice();
            $agentReport->updateAttributes(['invoice_id' => null, 'has_invoice' => 0]);
        }
        if ($this->rent) {
            $this->rent->invoice_id = null;
            $this->rent->save(false, ['invoice_id']);
        }

        return true;
    }

    /**
     * @return boolean
     */
    public function rejectDacuments()
    {
        if ($modelArray = $this->getActs()->andWhere(['not', ['status_out_id' => ActStatus::STATUS_REJECTED]])->all()) {
            foreach ($modelArray as $model) {
                $model->status_out_id = ActStatus::STATUS_REJECTED;
                $saved = LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function ($model) {
                    return $model->save(true, ['status_out_id', 'status_out_updated_at', 'status_out_author_id']);
                });

                if (!$saved) {
                    return false;
                }
            }
        }
        if ($modelArray = $this->getPackingLists()->andWhere(['not', ['status_out_id' => PackingListStatus::STATUS_REJECTED]])->all()) {
            foreach ($modelArray as $model) {
                $model->status_out_id = PackingListStatus::STATUS_REJECTED;
                $saved = LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function ($model) {
                    return $model->save(true, ['status_out_id', 'status_out_updated_at', 'status_out_author_id']);
                });

                if (!$saved) {
                    return false;
                }
            }
        }
        if ($modelArray = $this->getWaybills()->andWhere(['not', ['status_out_id' => WaybillStatus::STATUS_REJECTED]])->all()) {
            foreach ($modelArray as $model) {
                $model->status_out_id = WaybillStatus::STATUS_REJECTED;
                $saved = LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function ($model) {
                    return $model->save(true, ['status_out_id', 'status_out_updated_at', 'status_out_author_id']);
                });

                if (!$saved) {
                    return false;
                }
            }
        }
        if ($modelArray = $this->getInvoiceFactures()->andWhere(['not', ['status_out_id' => InvoiceFactureStatus::STATUS_REJECTED]])->all()) {
            foreach ($modelArray as $model) {
                $model->status_out_id = InvoiceFactureStatus::STATUS_REJECTED;
                $saved = LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function ($model) {
                    return $model->save(true, ['status_out_id', 'status_out_updated_at', 'status_out_author_id']);
                });

                if (!$saved) {
                    return false;
                }
            }
        }
        if ($modelArray = $this->getUpds()->andWhere(['not', ['status_out_id' => UpdStatus::STATUS_REJECTED]])->all()) {
            foreach ($modelArray as $model) {
                $model->status_out_id = UpdStatus::STATUS_REJECTED;
                $saved = LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function ($model) {
                    return $model->save(true, ['status_out_id', 'status_out_updated_at', 'status_out_author_id']);
                });

                if (!$saved) {
                    return false;
                }
            }
        }

        return true;
    }

    /**
     * Full invoice number
     *
     * @return string
     */
    public function getFullNumber()
    {
        if ($this->is_additional_number_before) {
            return $this->document_additional_number . $this->document_number;
        } else {
            return $this->document_number . $this->document_additional_number;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentOrderInvoices()
    {
        return $this->hasMany(PaymentOrderInvoice::className(), ['invoice_id' => 'id']);
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        if ($this->is_invoice_contract && $this->type == Documents::IO_TYPE_OUT) {
            $this->printablePrefix = 'Счет-договор';
        }

        $date = date_format(date_create($this->document_date), 'd.m.Y');

        return "{$this->printablePrefix} № {$this->getFullNumber()} от {$date}";
    }

    /**
     * @return string
     */
    public function getPrintTitle()
    {
        if ($this->is_invoice_contract && $this->type == Documents::IO_TYPE_OUT) {
            $this->printablePrefix = 'Счет-договор';
        }

        return str_replace([' ', '"'], ['_'],
            $this->printablePrefix . '_№' . $this->getFullNumber() . '_' .
            date_format(date_create($this->document_date), 'd.m.Y') . '_' .
            $this->company_name_short
        );
    }

    /**
     * @return string
     */
    public function getEmailSubject()
    {
        if ($this->is_invoice_contract && $this->type == Documents::IO_TYPE_OUT) {
            $this->printablePrefix = 'Счет-договор';
        }

        return $this->printablePrefix . ' № ' . $this->fullNumber
            . ' от ' . date_format(date_create($this->document_date), 'd.m.Y')
            . ' от ' . $this->company_name_short;
    }

    /**
     * @return Act|PackingList
     */
    public function getActOrPackingList()
    {
        return $this->production_type == Product::PRODUCTION_TYPE_SERVICE
            ? $this->act : $this->packingList;
    }

    /**
     * @return null|static
     */
    public function ActOrPackingList1c()
    {
        $act = Act::findOne(['invoice_id' => $this->id]);
        $packingList = PackingList::findOne(['invoice_id' => $this->id]);
        if ($this->production_type === Product::PRODUCTION_TYPE_SERVICE) {
            $model = $act;
        } elseif ($this->production_type === Product::PRODUCTION_TYPE_GOODS) {
            $model = $packingList;
        } else {
            if ($act !== null) {
                $model = $act;
            } else {
                $model = $packingList;
            }
        }

        return $model;
    }

    /**
     * @param $productType
     * @return mixed
     */
    public function getOrdersByProductType($productType)
    {
        return $this->hasMany(Order::className(), ['invoice_id' => 'id'])
            ->joinWith('product')
            ->andWhere([Product::tableName() . '.production_type' => $productType])
            ->orderNumber()
            ->all();
    }

    /**
     * @param $productionType
     * @param bool|true $retLink
     * @param bool|false $useContractor
     * @return string
     */
    public function getActPackingListLink($productionType, $retLink = true, $useContractor = false)
    {
        $productionType == Product::PRODUCTION_TYPE_SERVICE ?
            $model = $this->act :
            $model = $this->packingList;

        $routeBase = '/documents/' . ($productionType == Product::PRODUCTION_TYPE_SERVICE ? 'act' : 'packing-list');
        $modelExists = $model !== null;
        if ($modelExists) {
            $routeBase .= '/view';
            $route = Url::to([
                $routeBase,
                'type' => $this->type,
                'id' => $model->id,
            ]);
        } else {
            $routeBase .= '/create';
            $route = Url::to([
                $routeBase,
                'type' => $this->type,
                'invoiceId' => $this->id,
                'contractorId' => ($useContractor ? $this->contractor_id : null),
            ]);
        }


        return $retLink
            ? Html::a($modelExists ? $model->fullNumber : 'Добавить', $route, [
                'class' => $modelExists ? '' : 'btn btn-sm yellow' . ($this->isRejected ? ' disabled' : ''),
                'style' => (!$modelExists && $this->isRejected) ? 'background-color: #a2a2a2;' : '',
            ])
            : $route;
    }

    /**
     * @return string
     */
    public function getInvoiceFactureUrl()
    {
        return empty($this->invoiceFacture) ? '#'
            : Url::toRoute([
                '/documents/invoice-facture/view', 'type' => $this->type, 'id' => $this->invoiceFacture->id,
            ]);
    }

    /**
     * @return string
     */
    public function getUpdUrl()
    {
        return !$this->upd ? '#' : Url::toRoute([
            '/documents/upd/view', 'type' => $this->type, 'id' => $this->upd->id,
        ]);
    }

    /**
     * @param $company
     * @param $ioType
     * @param null|integer $number
     * @return int
     */
    public static function getNextDocumentNumber($company, $ioType, $number = null, $date = null)
    {
        $year = $date ? date('Y', strtotime($date)) : null;
        $companyId = ($company instanceof Company) ? $company->id : $company;
        $unique = [];
        $numQuery = Invoice::find()
            ->byCompany($companyId)
            ->byIOType($ioType)
            ->byDeleted();
        if ($year) {
            $numQuery->andWhere([
                'between',
                'document_date',
                $year.'-01-01',
                $year.'-12-31',
            ]);
        }

        $numQuery->having('CHAR_LENGTH(`document_number`) <= ' . AbstractDocument::MAX_INCREMENTED_DOCUMENT_NUMBER_LENGTH);

        $lastNumber = $number ? $number : (int)$numQuery->max('(document_number * 1)');
        $nextNumber = 1 + $lastNumber;

        $existNumQuery = static::find()
            ->select('document_number')
            ->byCompany($companyId)
            ->byIOType($ioType)
            ->byDeleted();

        if ($year) {
            $existNumQuery->andWhere([
                'between',
                'document_date',
                $year.'-01-01',
                $year.'-12-31',
            ]);
        }

        $numArray = $existNumQuery->column();

        if (in_array($nextNumber, $numArray)) {
            return static::getNextDocumentNumber($company, $ioType, $nextNumber, $date);
        } else {
            return $nextNumber;
        }
    }

    /**
     * @param $company
     * @param $ioType
     * @param null|integer $number
     * @return int
     */
    public static function getNextCreatedDocumentNumber($company, $ioType, $number = null, $date = null)
    {
        $companyId = ($company instanceof Company) ? $company->id : $company;
        $year = date('Y', strtotime($date));
        $t0 = Invoice::tableName();
        $t1 = Act::tableName();
        $t2 = PackingList::tableName();
        $t3 = InvoiceFacture::tableName();
        $t4 = Upd::tableName();
        $t5 = Proxy::tableName();
        $t6 = SalesInvoice::tableName();
        $query1 = Act::find()->byCompany($companyId)
            ->select(new Expression("{{{$t1}}}.[[document_number]], YEAR({{{$t1}}}.[[document_date]]) [[document_year]]"))
            ->andWhere(["$t0.is_deleted" => false, "$t0.type" => $ioType])
            ->having(['document_year' => $year])->andHaving('CHAR_LENGTH([[document_number]]) <= ' . self::MAX_INCREMENTED_DOCUMENT_NUMBER_LENGTH);
        $query2 = PackingList::find()->byCompany($companyId)
            ->select(new Expression("{{{$t2}}}.[[document_number]], YEAR({{{$t2}}}.[[document_date]]) [[document_year]]"))
            ->andWhere(["$t0.is_deleted" => false, "$t0.type" => $ioType])
            ->having(['document_year' => $year])->andHaving('CHAR_LENGTH([[document_number]]) <= ' . self::MAX_INCREMENTED_DOCUMENT_NUMBER_LENGTH);
        $query3 = InvoiceFacture::find()->byCompany($companyId)
            ->select(new Expression("{{{$t3}}}.[[document_number]], YEAR({{{$t3}}}.[[document_date]]) [[document_year]]"))
            ->andWhere(["$t0.is_deleted" => false, "$t0.type" => $ioType])
            ->having(['document_year' => $year])->andHaving('CHAR_LENGTH([[document_number]]) <= ' . self::MAX_INCREMENTED_DOCUMENT_NUMBER_LENGTH);
        $query4 = Upd::find()->byCompany($companyId)
            ->select(new Expression("{{{$t4}}}.[[document_number]], YEAR({{{$t4}}}.[[document_date]]) [[document_year]]"))
            ->andWhere(["$t0.is_deleted" => false, "$t0.type" => $ioType])
            ->having(['document_year' => $year])->andHaving('CHAR_LENGTH([[document_number]]) <= ' . self::MAX_INCREMENTED_DOCUMENT_NUMBER_LENGTH);
        $query5 = Proxy::find()->byCompany($companyId)
            ->select(new Expression("{{{$t5}}}.[[document_number]], YEAR({{{$t5}}}.[[document_date]]) [[document_year]]"))
            ->andWhere(["$t0.is_deleted" => false, "$t0.type" => $ioType])
            ->having(['document_year' => $year])->andHaving('CHAR_LENGTH([[document_number]]) <= ' . self::MAX_INCREMENTED_DOCUMENT_NUMBER_LENGTH);
        $query6 = SalesInvoice::find()->byCompany($companyId)
            ->select(new Expression("{{{$t6}}}.[[document_number]], YEAR({{{$t6}}}.[[document_date]]) [[document_year]]"))
            ->andWhere(["$t0.is_deleted" => false, "$t0.type" => $ioType])
            ->having(['document_year' => $year])->andHaving('CHAR_LENGTH([[document_number]]) <= ' . self::MAX_INCREMENTED_DOCUMENT_NUMBER_LENGTH);

        $query = (new yii\db\Query)->from(['t' => $query1->union($query2)->union($query3)->union($query4)->union($query5)->union($query6)]);

        $number = (integer)$query->max('[[document_number]] * 1') + 1;

        return (string)$number;
    }

    /**
     * @param Contractor $contractor
     */
    public function setContractor(Contractor $contractor)
    {
        $this->contractor_name_short = $contractor->getTitle(true);
        $this->contractor_name_full = $contractor->getTitle(false);
        $this->contractor_director_name = $contractor->director_name;
        $this->contractor_director_post_name = $contractor->director_post_name;
        $this->contractor_address_legal_full = ($contractor->face_type == Contractor::TYPE_LEGAL_PERSON ? $contractor->legal_address : $contractor->physical_address);
        $this->contractor_bank_name = $contractor->bank_name;
        $this->contractor_bank_city = $contractor->bank_city;
        $this->contractor_bik = $contractor->BIC;
        $this->contractor_inn = $contractor->ITN;
        $this->contractor_kpp = $contractor->PPC;
        $this->contractor_ks = $contractor->corresp_account;
        $this->contractor_rs = $contractor->current_account;
        if ($contractor->face_type == Contractor::TYPE_PHYSICAL_PERSON) {
            $this->setContractorPassport($contractor->getAttributes(Contractor::$passportAttributes));
        }
    }

    /**
     * @return bool
     */
    public function checkContractor()
    {
        $contractor = $this->contractor;
        $contractorNameShort = $contractor->getTitle(true);
        $contractorNameFull = $contractor->getTitle(false);
        $contractorAddressLegalFull = ($contractor->face_type == Contractor::TYPE_LEGAL_PERSON ? $contractor->legal_address : $contractor->physical_address);

        $contractorFields = [
            'contractor_name_short' => $contractorNameShort,
            'contractor_name_full' => $contractorNameFull,
            'contractor_director_name' => 'director_name',
            'contractor_director_post_name' => 'director_post_name',
            'contractor_address_legal_full' => $contractorAddressLegalFull,
            'contractor_bank_name' => 'bank_name',
            'contractor_bank_city' => 'bank_city',
            'contractor_bik' => 'BIC',
            'contractor_inn' => 'ITN',
            'contractor_kpp' => 'PPC',
            'contractor_ks' => 'corresp_account',
            'contractor_rs' => 'current_account',
        ];
        $defaultContractorFields = $contractor->fields();
        foreach ($contractorFields as $invoiceContractorField => $contractorField) {
            if (in_array($contractorField, $defaultContractorFields)) {
                if ($this->$invoiceContractorField !== $contractor->$contractorField) {

                    if ($this->$invoiceContractorField === "" && $contractor->$contractorField === null)
                        continue;

                    return true;
                }
            } elseif ($this->$invoiceContractorField !== $contractorField) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param Company $company
     *
     * @throws Exception
     */
    public function setCompany(Company $company)
    {
        if ($company === null) {
            throw new Exception('Компания не установлена');
        }

        $this->company_id = $company->id;
        $this->company_inn = $company->inn;
        $this->company_kpp = $company->kpp;
        $this->company_egrip = $company->egrip;
        $this->company_okpo = $company->okpo;
        $this->company_name_full = $company->getTitle();
        $this->company_name_short = $company->getTitle(true, true);
        $this->company_address_legal_full = $company->getAddressLegalFull();
        $this->company_phone = $company->phone;
        $this->company_chief_post_name = $company->chief_post_name;
        $this->company_chief_lastname = $company->chief_lastname;
        $this->company_chief_firstname_initials = $company->chief_firstname_initials;
        $this->company_chief_patronymic_initials = $company->chief_patronymic_initials;
        $this->company_chief_accountant_lastname = $company->chief_accountant_lastname;
        $this->company_chief_accountant_firstname_initials = $company->chief_accountant_firstname_initials;
        $this->company_chief_accountant_patronymic_initials = $company->chief_accountant_patronymic_initials;
        $this->company_print_filename = $company->print_link;
        $this->company_chief_signature_filename = $company->chief_signature_link;
    }

    /**
     * @param Company $company
     * @return bool
     */
    public function setCheckingAccountant(Company $company)
    {
        /* @var $checkingAccountant CheckingAccountant */
        if ($checkingAccountant = $company->mainCheckingAccountant) {
            $this->company_checking_accountant_id = $checkingAccountant->id;
            $this->company_bank_name = $checkingAccountant->bank_name;
            $this->company_bank_city = $checkingAccountant->bank_city;
            $this->company_bik = $checkingAccountant->bik;
            $this->company_ks = $checkingAccountant->ks;
            $this->company_rs = $checkingAccountant->rs;
        }

        return true;
    }

    /**
     * @return bool
     */
    public function updateAllowed()
    {
        return $this->invoice_status_id != status\InvoiceStatus::STATUS_PAYED;
    }

    /**
     * Get formatted log message from Log model
     * @param Log $log
     * @return string
     */
    public function getLogMessage(Log $log)
    {
        $auto = $log->getModelAttributeNew('invoice_status_id') == InvoiceStatus::STATUS_AUTOINVOICE;

        $text = ($auto ? 'Шаблон №' : 'Счёт №') . join(' ', array_filter([
                $log->getModelAttributeNew('document_number'),
                $log->getModelAttributeNew('document_additional_number'),
            ]));

        if ($this->is_deleted) {
            $link = $text;
        } else {
            $link = Html::a($text, [
                '/documents/invoice/view' . ($auto ? '-auto' : ''),
                'type' => $auto ? null : $log->getModelAttributeNew('type'),
                'id' => $log->getModelAttributeNew('id'),
            ]);
        }

        if ($log->log_event_id == LogEvent::LOG_EVENT_RESPONSIBLE) {
            $message = $link . ', изменен ответственный';
            if ($this->responsible) {
                $message .= ' на '.$this->responsible->getFio(true).'.';
            } else {
                $message .= '.';
            }

            return $message;
        }

        if ($log->log_event_id == LogEvent::LOG_EVENT_INDUSTRY) {
            $message = $link . ', изменено направление';

            return $message;
        }

        $currencyName = $log->getModelAttributeNew('currency_name');
        $currency = $currencyName == Currency::DEFAULT_NAME ? 'руб.' : $currencyName;
        $amount = TextHelper::invoiceMoneyFormat($log->getModelAttributeNew('view_total_with_nds'), 2) . " {$currency}";
        $contractor = Contractor::findOne($log->getModelAttributeNew('contractor_id'));
        $contractorName = $contractor ? ', ' . $contractor->getTitle(true) : '';
        $invoiceText = ', ' . $amount . $contractorName . ',';

        switch ($log->log_event_id) {
            case LogEvent::LOG_EVENT_CREATE:
                return $link . $invoiceText . ' был создан.';
            case LogEvent::LOG_EVENT_DELETE:
                return $text . $invoiceText . ' был удалён.';
            case LogEvent::LOG_EVENT_UPDATE:
                return $link . $invoiceText . ' был изменен.';
            case LogEvent::LOG_EVENT_UPDATE_STATUS:
                /* @var InvoiceStatus $status */
                $status = InvoiceStatus::findOne($log->getModelAttributeNew('invoice_status_id'));

                return $status !== null
                    ? $link . $invoiceText . ' статус "' . $status->name . '"'
                    : '';
            default:
                return $log->message;
        }
    }

    /**
     * Updates belongs documents in `document_number` is changed.
     */
    public function updateBelongDocuments()
    {
        if ($this->type != Documents::IO_TYPE_OUT || $this->isNewRecord || !$this->isAttributeChanged('document_number')) {
            return;
        }

        $actOrPackingList = $this->getActOrPackingList();
        if ($actOrPackingList !== null) {
            $actOrPackingList->document_number = $this->document_number;
            $actOrPackingList->save(false, ['document_number']);
        }

        if ($this->invoiceFacture !== null) {
            $this->invoiceFacture->document_number = $this->document_number;
            $this->invoiceFacture->save(false, ['document_number']);
        }
    }

    /**
     * @return string
     */
    public function getPurposeOfPayment()
    {
        $result = 'Оплата по Счету №' . $this->document_number . ' от ' . DateHelper::format($this->document_date,
                DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

        if (isset($this->orders[0])) {
            $result .= ", за {$this->orders[0]->product_title}.";
        }
        $result .= ' ' . $this->ndsViewType->name . ($this->hasNds ? TextHelper::invoiceMoneyFormat($this->total_amount_nds, 2) : '');

        return $result;
    }

    /**
     * @param $id
     * @return string
     */
    public static function getPurposeOfPaymentForInvoiceArray($invoiceArray)
    {
        $fullName = count($invoiceArray) < 4;
        $nds = 0;
        $purposeOfPayment = 'Оплата по счету';
        if (!$fullName) {
            $purposeOfPayment .= ' №';
        }
        foreach ($invoiceArray as $invoice) {
            $nds += ($invoice->hasNds ? $invoice->total_amount_nds : 0);
            if ($fullName) {
                $purposeOfPayment .= (' №' . $invoice->fullNumber . ' от ' .
                    DateHelper::format($invoice->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . ',');
            } else {
                $purposeOfPayment .= ' ' . $invoice->fullNumber . ',';
            }

        }
        $invoice = reset($invoiceArray);
        $purposeOfPayment .= (" за {$invoice->orders[0]->product_title}.");
        $purposeOfPayment .= ($nds > 0 ? ' В том числе НДС ' . TextHelper::invoiceMoneyFormat($nds, 2) . ' руб.' : ' Без налога НДС');

        return $purposeOfPayment;
    }

    /**
     * @param $id
     * @return string
     */
    public static function getPurposeOfPaymentManyInvoices($id)
    {
        /* @var $invoiceArray Invoice[] */
        $invoiceArray = self::find()->andWhere(['id' => $id])->all();

        return self::getPurposeOfPaymentInvoiceArray($invoiceArray);
    }

    /**
     * Whether invoice overdue or not.
     * @return bool
     */
    public function isOverdue()
    {
        return ($this->payment_limit_date < date(DateHelper::FORMAT_DATE) && !$this->getIsFullyPaid())
            ||
            ($this->payment_limit_date < date(DateHelper::FORMAT_DATE, $this->invoice_status_updated_at) && $this->getIsFullyPaid());
    }

    /**
     * @param bool $short
     *
     * @param bool $reverse
     * @return string
     */
    public function getCompanyChiefFio($short = false, $reverse = false)
    {
        return $this->getFio('company_chief', $short, $reverse);
    }

    /**
     * @param bool|false $short
     * @param bool|false $reverse
     * @return string
     */
    public function getCompanyChiefAccountantFio($short = false, $reverse = false)
    {
        return $this->getFio($this->company->chief_is_chief_accountant ? 'company_chief' : 'company_chief_accountant', $short, $reverse);
    }

    /**
     * @param $prefix
     * @param $short
     * @param $reverse
     * @return string
     */
    public function getFio($prefix, $short, $reverse)
    {
        $fioArray = array_filter([
            $this->{$prefix . '_lastname'},
            trim($this->{$prefix . '_firstname_initials'}, '\t\n\r\0\x0B\.') . '.',
            $this->{$prefix . '_patronymic_initials'} ? trim($this->{$prefix . '_patronymic_initials'}, '\t\n\r\0\x0B\.') . '.' : '',
        ]);

        if ($reverse) {
            $fioArray[] = array_shift($fioArray);
        }

        return join(' ', $fioArray);
    }

    /**
     * @param Order[] $orders
     * @return Order[]
     */
    public function getMassCountableOrders($orders)
    {
        return array_filter($orders, function (Order $order) {
            return isset(ProductUnit::$countableUnits[$order->product->product_unit_id]);
        });
    }

    /**
     * @return float
     */
    public function getRealMassGross()
    {
        return $this->getRealMass('mass_gross');
    }

    /**
     * @return float
     */
    public function getRealMassNet()
    {
        return $this->getRealMass('quantity');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutoinvoice()
    {
        return $this->hasOne(Autoinvoice::className(), ['id' => 'autoinvoice_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuto()
    {
        return $this->hasOne(Autoinvoice::className(), ['id' => 'autoinvoice_id'])
            ->andOnCondition(['id' => null]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(EmployeeCompany::className(), [
            'company_id' => 'company_id',
            'employee_id' => 'document_author_id',
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResponsible()
    {
        return $this->hasOne(EmployeeCompany::className(), [
            'company_id' => 'company_id',
            'employee_id' => 'responsible_employee_id',
        ]);
    }

    /**
     * @return array|int
     */
    public function getProductionTypeByOrderArray()
    {
        $typeArray = [];
        foreach ($this->orders as $order) {
            $typeArray[$order->product->production_type] = $order->product->production_type;
        }
        sort($typeArray);

        return implode(', ', $typeArray);
    }

    /**
     * @return string
     */
    public function getProductionType()
    {
        $productionTypeArray = explode(', ', $this->production_type);
        $result = '';

        foreach ($productionTypeArray as $productionType) {
            empty($result) ?
                $result .= Product::$productionTypes[$productionType] :
                $result .= ', ' . Product::$productionTypes[$productionType];
        }

        return $result;
    }

    /**
     * @param string $fieldName
     * @return number
     */
    private function getRealMass($fieldName)
    {
        $countedOrders = $this->getMassCountableOrders($this->orders);
        if (count($countedOrders) === count($this->orders)) {
            return array_sum(ArrayHelper::getColumn($this->getMassCountableOrders($this->orders),
                function (Order $order) use ($fieldName) {
                    return bcmul(
                        is_numeric($order->$fieldName) ? (int)$order->$fieldName : 0,
                        (int)ProductUnit::$countableUnits[$order->product->product_unit_id],
                        5
                    );
                }));
        } else {
            return 0;
        }
    }

    /**
     * @return string
     */
    public function getNdsViewValue()
    {
        return self::$ndsViewList[$this->nds_view_type_id];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPossibleBankFlows()
    {
        $existsFlowIdArray = $this->getBankPayments()->select('flow_id')->column();
        $query = CashBankFlows::find()
            ->alias('flow')
            ->leftJoin([
                'paid' => CashBankFlowToInvoice::find()
                    ->select(['flow_id', 'invoices_amount' => new Expression("SUM(amount)")])
                    ->groupBy('flow_id')
            ], 'flow.id = paid.flow_id')
            ->byFlowType(CashFactory::$documentToFlowType[$this->type])
            ->byCompany($this->company_id)
            ->andWhere(['flow.contractor_id' => $this->contractor_id])
            ->andWhere(['>', 'flow.amount', new Expression("IFNULL(invoices_amount, 0)")]);

        if ($existsFlowIdArray) {
            $query->andWhere(['not', ['flow.id' => $existsFlowIdArray]]);
        }

        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPossibleOrderFlows()
    {
        $existsFlowIdArray = $this->getOrderPayments()->select('flow_id')->column();
        $query = CashOrderFlows::find()->alias('flow')
            ->leftJoin([
                'paid' => CashOrderFlowToInvoice::find()
                    ->select(['flow_id', 'invoices_amount' => new Expression("SUM(amount)")])
                    ->groupBy('flow_id')
            ], 'flow.id = paid.flow_id')
            ->byFlowType(CashFactory::$documentToFlowType[$this->type])
            ->byCompany($this->company_id)
            ->andWhere(['flow.contractor_id' => $this->contractor_id])
            ->andWhere(['>', 'flow.amount', new Expression("IFNULL(invoices_amount, 0)")]);

        if ($existsFlowIdArray) {
            $query->andWhere(['not', ['flow.id' => $existsFlowIdArray]]);
        }

        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPossibleEmoneyFlows()
    {
        $existsFlowIdArray = $this->getEmoneyPayments()->select('flow_id')->column();
        $query = CashEmoneyFlows::find()->alias('flow')
            ->leftJoin([
                'paid' => CashEmoneyFlowToInvoice::find()
                    ->select(['flow_id', 'invoices_amount' => new Expression("SUM(amount)")])
                    ->groupBy('flow_id')
            ], 'flow.id = paid.flow_id')
            ->byFlowType(CashFactory::$documentToFlowType[$this->type])
            ->byCompany($this->company_id)
            ->andWhere(['flow.contractor_id' => $this->contractor_id])
            ->andWhere(['>', 'flow.amount', new Expression("IFNULL(invoices_amount, 0)")]);

        if ($existsFlowIdArray) {
            $query->andWhere(['not', ['flow.id' => $existsFlowIdArray]]);
        }

        return $query;
    }

    /**
     * @return boolean
     */
    public function getIsAllStatusesComplete()
    {
        $productionTypeArray = explode(', ', $this->production_type);
        $hasService = in_array(Product::PRODUCTION_TYPE_SERVICE, $productionTypeArray);
        $hasGoods = in_array(Product::PRODUCTION_TYPE_GOODS, $productionTypeArray);

        return $this->type == Documents::IO_TYPE_OUT &&
            $this->invoice_status_id == InvoiceStatus::STATUS_PAYED &&
            ($hasService ? ($this->act && $this->act->status_out_id == ActStatus::STATUS_RECEIVED) : true) &&
            ($hasGoods ? ($this->packingList && $this->packingList->status_out_id == PackingListStatus::STATUS_RECEIVED) : true) &&
            ($hasGoods ? ($this->waybill && $this->waybill->status_out_id == WaybillStatus::STATUS_RECEIVED) : true) &&
            ($this->invoiceFacture && $this->invoiceFacture->status_out_id == InvoiceFactureStatus::STATUS_DELIVERED);
    }

    /**
     * @param boolean $save
     */
    public function checkOverdue($save = false)
    {
        if (!$this->isAutoinvoice) {

            // set overdue
            $isStatus = in_array($this->invoice_status_id, [
                InvoiceStatus::STATUS_CREATED,
                InvoiceStatus::STATUS_SEND,
                InvoiceStatus::STATUS_VIEWED,
                InvoiceStatus::STATUS_APPROVED,
            ]);

            switch ($this->payment_limit_rule) {
                case self::PAYMENT_LIMIT_RULE_MIXED:
                case self::PAYMENT_LIMIT_RULE_DOCUMENT:
                    $isDate = ($this->has_act || $this->has_packing_list || $this->has_upd)
                        && ($this->related_document_payment_limit_date)
                        && (date_create($this->related_document_payment_limit_date) < date_create(date('Y-m-d')));
                    break;
                case self::PAYMENT_LIMIT_RULE_INVOICE:
                default:
                    $isDate = (date_create($this->payment_limit_date) < date_create(date('Y-m-d')));
                    break;
            }

            if ($isStatus && $isDate) {
                $this->invoice_status_id = InvoiceStatus::STATUS_OVERDUE;
                $this->invoice_status_updated_at = strtotime("{$this->payment_limit_date} +1 day");
                if ($save) {
                    $this->save(false, ['invoice_status_id', 'invoice_status_updated_at']);
                }
            }

            // revert overdue
            $isStatusRevert = $this->invoice_status_id == InvoiceStatus::STATUS_OVERDUE;
            switch ($this->payment_limit_rule) {
                case self::PAYMENT_LIMIT_RULE_MIXED:
                case self::PAYMENT_LIMIT_RULE_DOCUMENT:
                    $isDateRevert = (!$this->has_act && !$this->has_packing_list && !$this->has_upd)
                        || (date_create(date('Y-m-d')) <= date_create($this->related_document_payment_limit_date));
                    break;
                case self::PAYMENT_LIMIT_RULE_INVOICE:
                default:
                    $isDateRevert = date_create(date('Y-m-d')) <= date_create($this->payment_limit_date);
                    break;
            }

            if ($isStatusRevert && $isDateRevert) {
                $this->invoice_status_id = InvoiceStatus::STATUS_CREATED;
                $this->invoice_status_updated_at = time();
                if ($save) {
                    $this->save(false, ['invoice_status_id', 'invoice_status_updated_at']);
                }
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();

        $this->currencyAmountCustom = $this->currency_amount;
        $this->currencyRateCustom = $this->currency_rate;

        if ($this->invoice_status_id == InvoiceStatus::STATUS_AUTOINVOICE) {
            $this->isAutoinvoice = 1;
        } else {
            $this->checkOverdue(true);
        }
        if ($this->basis_document_name && $this->basis_document_number /*&& $this->basis_document_date*/) {
            $agreement = [
                $this->basis_document_name,
                $this->basis_document_number,
                $this->basis_document_date,
            ];
            if ($this->basis_document_type_id) {
                $agreement[] = $this->basis_document_type_id;
                if ($this->agreement_new_id) {
                    $agreement[] = $this->agreement_new_id;
                }
            }
            $this->agreement = implode('&', $agreement);
        }
    }

    /**
     * @param Company $company
     * @param $type
     * @return $this
     */
    public function getBaseInvoice(Company $company, $type)
    {
        $this->scenario = 'insert';
        $this->type = $type;
        $this->document_date = date(DateHelper::FORMAT_DATE);
        $this->populateRelation('company', $company);
        $this->payment_limit_date = date(DateHelper::FORMAT_DATE, strtotime('+10 day'));
        $this->invoice_status_id = InvoiceStatus::STATUS_CREATED;

        return $this;
    }

    /**
     * @param Company $company
     * @param $productsArray
     * @return array|bool
     */
    public function createInvoiceByLanding(Company $company, $productsArray)
    {
        /* @var $product Product */
        foreach (Product::findAll(['company_id' => $company->id]) as $product) {
            /* @var $order Order */
            $order = OrderHelper::createOrderByProduct(
                $product, $this, $productsArray[$product->id], $product->price_for_sell_with_nds ? $product->price_for_sell_with_nds : null
            );

            $order->product_title = $product['title'];
            $order->calculateOrder();

            $orderArray[] = $order;
        }
        array_walk($orderArray, function ($order, $key) {
            $order->number = $key + 1;
        });
        $this->total_amount_has_nds = $this->total_amount_nds > 0;
        $this->populateRelation('orders', $orderArray);
        $this->total_mass_gross = (string)array_sum(ArrayHelper::getColumn($this->orders, 'mass_gross'));
        $this->total_place_count = (string)array_sum(ArrayHelper::getColumn($this->orders, 'place_count'));
        $this->populateRelation('company', $company);

        foreach ($this->orders as $order) {
            if (!$order->validate()) {
                return $order->getErrors();
            }
        }
        $this->production_type = $this->getProductionTypeByOrderArray();
        if (!$this->save()) {
            return $this->getErrors();
        }
        foreach ($this->orders as $order) {
            $order->invoice_id = $this->id;
            if (!$order->save()) {
                return $order->getErrors();
            }
        }

        return true;
    }

    /**
     * @return boolean
     */
    public function getHasService()
    {
        $O = Order::tableName();
        $P = Product::tableName();

        return $this->getOrders()
            ->leftJoin($P, "{{%$O}}.[[product_id]] = {{%$P}}.[[id]]")
            ->andWhere([
                "$P.production_type" => Product::PRODUCTION_TYPE_SERVICE,
            ])->exists();
    }

    /**
     * @return Bank
     */
    public function getContractorBank()
    {
        return $this->contractor_bik ? Bank::findOne([
            'bik' => $this->contractor_bik,
            'is_blocked' => false,
        ]) : null;
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $contractorAttributes = [];
        if ($insert) {
            if ($this->contractor->last_basis_document != $this->agreement) {
                $contractorAttributes['last_basis_document'] = $this->agreement;
            }
            if ($this->contractor->responsible_employee_id == null) {
                $isEmployeeExists = EmployeeCompany::find()->where([
                    'company_id' => $this->company_id,
                    'employee_id' => $this->responsible_employee_id,
                    'is_working' => true,
                ])->exists();
                if ($isEmployeeExists) {
                    $contractorAttributes['responsible_employee_id'] = $this->responsible_employee_id;
                }
            }
        } else {
            if (!$this->hasNds && $this->invoiceFactures) {
                foreach ($this->invoiceFactures as $invoiceFacture) {
                    LogHelper::delete($invoiceFacture, LogEntityType::TYPE_DOCUMENT);
                }
            }
        }
        if ($this->type == Documents::IO_TYPE_IN &&
            array_key_exists('invoice_expenditure_item_id', $changedAttributes) &&
            $this->contractor->invoice_expenditure_item_id != $this->invoice_expenditure_item_id
        ) {
            $contractorAttributes['invoice_expenditure_item_id'] = $this->invoice_expenditure_item_id;
            // set payment_priority
            if (in_array($this->invoice_expenditure_item_id,
                \frontend\modules\analytics\models\PlanCashContractor::EXPENDITURE_ITEMS_HIGH_PRIORITY))
                $contractorAttributes['payment_priority'] = Contractor::PAYMENT_PRIORITY_HIGH;
        }
        if ($this->type == Documents::IO_TYPE_OUT &&
            array_key_exists('invoice_income_item_id', $changedAttributes) &&
            $this->contractor->invoice_income_item_id != $this->invoice_income_item_id
        ) {
            $contractorAttributes['invoice_income_item_id'] = $this->invoice_income_item_id;
        }

        if ($this->type == Documents::IO_TYPE_IN &&
            $this->contractor->nds_view_type_id != $this->nds_view_type_id
        ) {
            $contractorAttributes['nds_view_type_id'] = $this->nds_view_type_id;
        }

        if (array_key_exists('industry_id', $changedAttributes)) {
            if ($this->type == Documents::IO_TYPE_OUT && $this->contractor->customer_industry_id != $this->industry_id) {
                $contractorAttributes['customer_industry_id'] = $this->industry_id;
            } elseif ($this->type == Documents::IO_TYPE_IN && $this->contractor->seller_industry_id != $this->industry_id) {
                $contractorAttributes['seller_industry_id'] = $this->industry_id;
            }
        }

        if (array_key_exists('sale_point_id', $changedAttributes)) {
            if ($this->type == Documents::IO_TYPE_OUT && $this->contractor->customer_sale_point_id != $this->sale_point_id) {
                $contractorAttributes['customer_sale_point_id'] = $this->sale_point_id;
            } elseif ($this->type == Documents::IO_TYPE_IN && $this->contractor->seller_sale_point_id != $this->sale_point_id) {
                $contractorAttributes['seller_sale_point_id'] = $this->sale_point_id;
            }
        }


        if ($this->type == Documents::IO_TYPE_OUT && in_array($this->payment_limit_rule, [self::PAYMENT_LIMIT_RULE_DOCUMENT, self::PAYMENT_LIMIT_RULE_MIXED])) {
            if ($this->contractor->customer_payment_delay != $this->payment_limit_days)
                if ($this->payment_limit_days > 0)
                    $contractorAttributes['customer_payment_delay'] = $this->payment_limit_days;
        }

        if ($contractorAttributes) {
            if ($this->contractor->isKub()) {
                $kubContractorAttributes = [];
                if (isset($contractorAttributes['invoice_income_item_id']))
                    $kubContractorAttributes['invoice_income_item_id'] = $contractorAttributes['invoice_income_item_id'];
                if (isset($contractorAttributes['invoice_expenditure_item_id']))
                    $kubContractorAttributes['invoice_expenditure_item_id'] = $contractorAttributes['invoice_expenditure_item_id'];
                if ($kubContractorAttributes) {
                    $this->contractor->getKubItem($this->company_id)->updateAttributes($kubContractorAttributes);
                }
            } else {
                $this->contractor->updateAttributes($contractorAttributes);
            }
        }

        if ($this->type == Documents::IO_TYPE_OUT && $this->createdFromInInvoiceId) {
            InvoiceInToInvoiceOut::linkInvoices($this->company_id, $this->id, $this->createdFromInInvoiceId);
        }
    }

    /**
     * @return boolean
     */
    public function getIsChangeContractorAllowed()
    {
        return $this->isNewRecord
            || (
                $this->invoice_status_id == InvoiceStatus::STATUS_CREATED
                && !$this->act
                && !$this->invoiceFacture
                && !$this->packingList
                && !$this->waybill
            );
    }

    /**
     * @return string
     */
    public function getEmailText()
    {
        $text = <<<EMAIL_TEXT
Здравствуйте!
EMAIL_TEXT;

        return $text;
    }

    public function getEmailPermanentText()
    {
        $document_date = DateHelper::format($this->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
        $payment_limit_date = DateHelper::format($this->payment_limit_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
        $sum = TextHelper::invoiceMoneyFormat($this->view_total_with_nds, 2);

        $currency = $this->currency_name == Currency::DEFAULT_NAME ? 'руб' : $this->currency_name;

        return "Счёт № {$this->fullNumber} от {$document_date} на сумму {$sum} {$currency}.<br>
        Оплатить до {$payment_limit_date}г.";
    }

    /**
     * @return boolean
     */
    public function getCanAddDocs()
    {
        return !$this->has_upd && in_array($this->invoice_status_id, InvoiceStatus::$validInvoices);
    }

    /**
     * @return boolean
     */
    public function getCanAddAct()
    {
        return $this->has_services && $this->can_add_act && $this->getCanAddDocs();
    }

    /**
     * @return boolean
     */
    public function getCanAddPackingList()
    {
        return $this->has_goods && $this->can_add_packing_list && $this->getCanAddDocs();
    }

    /**
     * @return boolean
     */
    public function getCanAddSalesInvoice()
    {
        return $this->has_goods && $this->can_add_sales_invoice && $this->getCanAddDocs();
    }

    /**
     * @return boolean
     */
    public function getCanAddInvoiceFacture()
    {
        return $this->can_add_invoice_facture && $this->hasNds && $this->getCanAddDocs();
    }

    /**
     * @return boolean
     */
    public function getCanAddUpd()
    {
        return $this->can_add_upd && !$this->hasDocs && in_array($this->invoice_status_id, InvoiceStatus::$validInvoices);
    }

    /**
     * @return boolean
     */
    public function getCanAddWaybill()
    {
        if (!$this->has_goods || !$this->can_add_waybill) {
            $this->lastCreatedDocumentError = "Нельзя создать ТТН без товаров.";
            return false;
        }

        //if ($this->has_upd) {
        //    $this->lastCreatedDocumentError = "Нельзя создать ТТН, т.к. для Счета уже создан УПД.";
        //    return false;
        //}

        //if (!$this->getCanAddDocs()) {
        if (!in_array($this->invoice_status_id, InvoiceStatus::$validInvoices)) {
            $this->lastCreatedDocumentError = "Нельзя создать ТТН, т.к. статус Счета " . $this->invoiceStatus->name;
            return false;
        }

        return true;
    }

    /**
     * @return boolean
     */
    public function getCanAddProxy()
    {
        //var_dump($this->has_goods, $this->can_add_proxy, $this->getCanAddDocs());die;
        return $this->has_goods && $this->can_add_proxy && in_array($this->invoice_status_id, InvoiceStatus::$validInvoices);
    }

    /**
     * @return string
     */
    public function getExecutorFullRequisites()
    {
        return $this->type == Documents::IO_TYPE_IN ? $this->getContractorFullRequisites() : $this->getCompanyFullRequisites();
    }

    /**
     * @return string
     */
    public function getCustomerFullRequisites()
    {
        return $this->type == Documents::IO_TYPE_IN ? $this->getCompanyFullRequisites() : $this->getContractorFullRequisites();
    }

    /**
     * @return string
     */
    public function getContractorFullRequisites()
    {
        $isLegal = ($this->contractor->face_type == Contractor::TYPE_LEGAL_PERSON);
        $result = strpos($this->contractor_name_short, 'ИП ') === 0 ? $this->contractor_name_full : $this->contractor_name_short;
        $result .= ($isLegal && $this->contractor_inn) ? ', ИНН ' . $this->contractor_inn : '';
        $result .= ($isLegal && $this->contractor_kpp) ? ', КПП ' . $this->contractor_kpp : '';
        $result .= ($this->contractor_address_legal_full) ? ', ' . $this->contractor_address_legal_full : '';
        $result .= ($isLegal && $this->contractor_rs) ? ', р/с ' . $this->contractor_rs : '';
        $result .= ($isLegal && $this->contractor_bank_name) ? ' в банке ' . $this->contractor_bank_name : '';
        $result .= ($isLegal && $this->contractor_bik) ? ', БИК ' . $this->contractor_bik : '';
        $result .= ($isLegal && $this->contractor_ks) ? ', к/с ' . $this->contractor_ks : '';
        if (!$isLegal) {
            $passport = $this->getContractorPassport();
            if ($passport['physical_passport_series'] && $passport['physical_passport_number']) {
                $result .= ", Паспорт серия {$passport['physical_passport_series']} № {$passport['physical_passport_number']}";
            }
        }

        return $result;
    }

    /**
     * @return string
     */
    public function getCompanyFullRequisites()
    {
        $result = $this->company->getIsLikeIP() ? $this->company_name_full : $this->company_name_short;
        $result .= $this->company_inn ? ', ИНН ' . $this->company_inn : '';
        $result .= $this->company_kpp ? ', КПП ' . $this->company_kpp : '';
        $result .= $this->company_address_legal_full ? ', ' . $this->company_address_legal_full : '';
        $result .= $this->company_rs ? ', р/с ' . $this->company_rs : '';
        $result .= $this->company_bank_name ? ' в банке ' . $this->company_bank_name : '';
        $result .= $this->company_bik ? ', БИК ' . $this->company_bik : '';
        $result .= $this->company_ks ? ', к/с ' . $this->company_ks : '';

        return $result;
    }

    /**
     * @return string
     */
    public function getDocumentName()
    {
        if ($this->is_invoice_contract && $this->type == Documents::IO_TYPE_OUT) {
            return 'Счет-договор';
        } else {
            return 'Счет';
        }
    }

    /**
     * @inheritdoc
     */
    public function getListItemValue()
    {
        return $this->getDocumentName() . '&' . $this->getFullNumber() . '&' . $this->document_date . '&';
    }

    /**
     * @inheritdoc
     */
    public function getListItemName()
    {
        $date = DateHelper::format($this->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

        return "{$this->getDocumentName()} № {$this->getFullNumber()} от {$date}";
    }

    /**
     * @param null $date
     * @param null $documentNumber
     * @return bool
     * @throws \Exception
     */
    public function createAct($date = null, $documentNumber = null, $isAuto = false)
    {
        $orderArray = array_filter($this->orders, function ($order) {
            return $order->product->production_type == Product::PRODUCTION_TYPE_SERVICE;
        });
        if (count($orderArray) === 0) {
            return false;
        }

        $documentDate = \DateTime::createFromFormat('d.m.Y', $date);
        $model = new Act([
            'invoice_id' => $this->id,
            'company_id' => $this->company_id,
            'contractor_id' => $this->contractor_id,
            'type' => $this->type,
            'document_date' => $documentDate ? $documentDate->format('Y-m-d') : date('Y-m-d'),
            'auto_created' => (boolean) $isAuto,
            'document_author_id' => $isAuto ? $this->document_author_id : null,
        ]);
        if ($this->basis_document_name && $this->basis_document_number /*&& $this->basis_document_date*/) {
            $model->agreement = $this->agreement;
        } else {
            $model->agreement = (($this->is_invoice_contract && $this->type == Documents::IO_TYPE_OUT) ?
                    'Счет-договор' : 'Счет') . "&{$this->fullNumber}&{$this->document_date}&";
        }
        $model->populateRelation('invoice', $this);

        $sum = 0;
        $sumNds = 0;
        $orderActArray = [];
        foreach ($orderArray as $order) {
            if ($orderAct = OrderAct::createFromOrder(null, $order)) {
                $orderAct->populateRelation('act', $model);
                $orderActArray[] = $orderAct;
                $sum += $orderAct->amountWithNds;
                $sumNds += $orderAct->amountNds;
            }
        }

        $model->populateRelation('orderActs', $orderActArray);
        $model->order_sum = $sum;
        $model->order_nds = $sumNds;

        $employeeCompany = EmployeeCompany::findOne([
            'employee_id' => $this->document_author_id,
            'company_id' => $this->company_id,
        ]);
        $employeeCompany->populateRelation('company', $this->company);

        if (Yii::$app->request->isConsoleRequest) {
            $model->document_author_id = $this->document_author_id;
            $model->status_out_author_id = $this->document_author_id;
            if ($documentNumber) {
                $model->document_number = $documentNumber;
                if ($this->document_additional_number) {
                    $model->document_additional_number = $this->document_additional_number;
                }
            } else {
                $model->newDocumentNumber();
            }
            if (count($model->orderActs) && $model->save()) {
                foreach ($model->orderActs as $orderAct) {
                    $orderAct->act_id = $model->id;
                    $orderAct->quantity = number_format($orderAct->quantity, 10, '.', '');
                    if ($orderAct->save()) {
                        continue;
                    }
                    \common\components\helpers\ModelHelper::logErrors($orderAct, __METHOD__);

                    return false;
                }
                $this->populateRelation('act', $model);
                $model->populateRelation('invoice', $this);
                $model->populateRelation('orderActs', $orderActArray);
                $this->updateAttributes([
                    'has_act' => true,
                    'can_add_act' => false,
                ]);

                $this->updateDocumentsPaid();
                PlanCashContractor::createFlowByInvoice($this, Documents::DOCUMENT_ACT);

                $autoIF = Autoinvoicefacture::findOne($this->company_id);

                if ($this->canAddInvoiceFacture &&
                    ((!$isAuto && $autoIF->rule == Autoinvoicefacture::BY_MANUAL_DOC) ||
                    ($isAuto && $autoIF->rule == Autoinvoicefacture::BY_AUTO_DOC))
                ) {
                    $this->createInvoiceFacture($date, null, true);
                }

                GoodsCancellation::cancellationByAct($employeeCompany, $model);

                return true;
            }
            \common\components\helpers\ModelHelper::logErrors($model, __METHOD__);

            return false;
        } else {
            if (LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE, function (Act $model) use ($documentNumber) {
                if ($documentNumber) {
                    $model->document_number = $documentNumber;
                    if ($this->document_additional_number) {
                        $model->document_additional_number = $this->document_additional_number;
                    }
                } else {
                    $model->newDocumentNumber();
                }
                if (count($model->orderActs) && $model->save()) {
                    foreach ($model->orderActs as $orderAct) {
                        $orderAct->act_id = $model->id;
                        if ($orderAct->save()) {
                            continue;
                        }
                        \common\components\helpers\ModelHelper::logErrors($orderAct, __METHOD__);

                        return false;
                    }

                    return true;
                }
                \common\components\helpers\ModelHelper::logErrors($model, __METHOD__);

                return false;
            })) {
                $this->populateRelation('act', $model);
                $model->populateRelation('invoice', $this);
                $this->updateAttributes([
                    'has_act' => true,
                    'can_add_act' => false,
                ]);

                $this->updateDocumentsPaid();
                PlanCashContractor::createFlowByInvoice($this, Documents::DOCUMENT_ACT);

                $autoIF = Autoinvoicefacture::findOne($this->company_id);

                if ($this->canAddInvoiceFacture &&
                    ((!$isAuto && $autoIF->rule == Autoinvoicefacture::BY_MANUAL_DOC) ||
                    ($isAuto && $autoIF->rule == Autoinvoicefacture::BY_AUTO_DOC))
                ) {
                    $this->createInvoiceFacture($date, null, true);
                }

                GoodsCancellation::cancellationByAct($employeeCompany, $model);

                return true;
            } else {
                return false;
            }
        }
    }

    /**
     * @param Invoice[] $invoices
     * @param $type
     * @param null $date
     * @return bool
     * @throws BadRequestHttpException
     * @throws \Exception
     */
    public static function createActForSeveralInvoices($invoices, $type, $date = null)
    {
        if (!is_array($invoices)) {
            throw new BadRequestHttpException();
        }
        $invoice = $invoices[0];
        if (!($invoice instanceof Invoice)) {
            throw new BadRequestHttpException();
        }
        $orderArray = [];
        foreach ($invoices as $invoice) {

            foreach ($invoice->orders as $order) {
                if ($order->product->production_type == Product::PRODUCTION_TYPE_SERVICE) {
                    $orderArray[] = $order;
                }
            }
        }
        if (count($orderArray) === 0) {
            return false;
        }

        $documentDate = \DateTime::createFromFormat('d.m.Y', $date);
        $model = new Act([
            'type' => $type,
            'company_id' => $invoice->company_id,
            'contractor_id' => $invoice->contractor_id,
            'document_date' => $documentDate ? $documentDate->format('Y-m-d') : date('Y-m-d'),
            'invoice_id' => $invoice->id,
            'isCreateForSeveralInvoices' => true
        ]);
        $model->populateRelation('invoices', $invoices);
        $model->populateRelation('invoice', $invoice);
        $model->newDocumentNumber();

        $sum = 0;
        $sumNds = 0;
        $orderActArray = [];
        foreach ($orderArray as $order) {
            if ($orderAct = OrderAct::createFromOrder(null, $order)) {
                $orderAct->populateRelation('act', $model);
                $orderActArray[] = $orderAct;
                $sum += $orderAct->amountWithNds;
                $sumNds += $orderAct->amountNds;
            }
        }
        $model->populateRelation('orderActs', $orderActArray);
        $model->order_sum = $sum;
        $model->order_nds = $sumNds;

        if (LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE, function (Act $model) use ($invoices) {
            if (count($model->orderActs) && $model->save()) {

                // move here for trigger `olap_documents_insert_order_upd`
                foreach ($invoices as $invoice) {
                    $invoiceAct = new InvoiceAct();
                    $invoiceAct->invoice_id = $invoice->id;
                    $invoiceAct->act_id = $model->id;
                    $invoiceAct->save();
                }

                foreach ($model->orderActs as $orderAct) {
                    $orderAct->act_id = $model->id;
                    $orderAct->quantity = number_format($orderAct->quantity, 10, '.', '');
                    if ($orderAct->save()) {
                        continue;
                    }

                    return false;
                }

                return true;
            }

            return false;
        })
        ) {
            foreach ($invoices as $invoice) {
                $invoice->populateRelation('act', $model);
                $invoice->updateAttributes([
                    'has_act' => true,
                    'can_add_act' => false,
                ]);
            }

            $invoice->updateDocumentsPaid();
            //PlanCashContractor::createFlowByInvoice($invoice, Documents::DOCUMENT_ACT);

            return $model->id;
        } else {
            \common\components\helpers\ModelHelper::logErrors($model, __METHOD__);

            return false;
        }
    }

    /**
     * @param null $date
     * @param null $documentNumber
     * @return bool
     * @throws \Exception
     */
    public function createPackingList($date = null, $documentNumber = null, $isAuto = false)
    {
        $orderArray = array_filter($this->orders, function ($order) {
            return $order->product->production_type == Product::PRODUCTION_TYPE_GOODS;
        });
        if (count($orderArray) === 0) {
            return false;
        }

        $documentDate = \DateTime::createFromFormat('d.m.Y', $date);
        $model = new PackingList([
            'invoice_id' => $this->id,
            'company_id' => $this->company_id,
            'contractor_id' => $this->contractor_id,
            'type' => $this->type,
            'document_date' => $documentDate ? $documentDate->format('Y-m-d') : date('Y-m-d'),
        ]);
        if ($this->basis_document_name && $this->basis_document_number /*&& $this->basis_document_date*/) {
            $model->agreement = $this->agreement;
        } else {
            $model->agreement = (($this->is_invoice_contract && $this->type == Documents::IO_TYPE_OUT) ?
                    'Счет-договор' : 'Счет') . "&{$this->fullNumber}&{$this->document_date}&";
        }
        $model->populateRelation('invoice', $this);
        if ($documentNumber) {
            $model->document_number = $documentNumber;
        } else {
            $model->newDocumentNumber();
        }
        if ($this->document_additional_number) {
            $model->document_additional_number = $this->document_additional_number;
        }

        $sum = 0;
        $sumNds = 0;
        $orderPLArray = [];
        foreach ($orderArray as $order) {
            if ($orderPL = OrderPackingList::createFromOrder(null, $order)) {
                $orderPL->populateRelation('packingList', $model);
                $orderPLArray[] = $orderPL;
                $sum += $orderPL->amountWithNds;
                $sumNds += $orderPL->amountNds;
            }
        }

        $model->populateRelation('orderPackingLists', $orderPLArray);
        $model->orders_sum = $sum;
        $model->order_nds = $sumNds;

        if (LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE, function (PackingList $model) {
            if (count($model->orderPackingLists) && $model->save()) {
                foreach ($model->orderPackingLists as $orderPL) {
                    $orderPL->packing_list_id = $model->id;
                    $orderPL->quantity = number_format($orderPL->quantity, 10, '.', '');
                    if ($orderPL->save()) {
                        continue;
                    } else {
                        \common\components\helpers\ModelHelper::logErrors($orderPL, __METHOD__);
                    }

                    return false;
                }

                return true;
            }

            return false;
        })
        ) {
            $this->populateRelation('packingList', $model);
            $model->populateRelation('invoice', $this);
            $this->updateAttributes([
                'has_packing_list' => true,
                'can_add_packing_list' => false,
            ]);

            $this->updateDocumentsPaid();
            PlanCashContractor::createFlowByInvoice($this, Documents::DOCUMENT_PACKING_LIST);

            $autoIF = Autoinvoicefacture::findOne($this->company_id);

            if ($this->canAddInvoiceFacture &&
                ((!$isAuto && $autoIF->rule == Autoinvoicefacture::BY_MANUAL_DOC) ||
                ($isAuto && $autoIF->rule == Autoinvoicefacture::BY_AUTO_DOC))
            ) {
                $this->createInvoiceFacture($date, null, true);
            }

            return true;
        } else {
            \common\components\helpers\ModelHelper::logErrors($model, __METHOD__);

            return false;
        }
    }

    /**
     * @param null $date
     * @param null $documentNumber
     * @return bool
     * @throws \Exception
     */
    public function createSalesInvoice($date = null, $documentNumber = null)
    {
        $orderArray = array_filter($this->orders, function ($order) {
            return $order->product->production_type == Product::PRODUCTION_TYPE_GOODS;
        });
        if (count($orderArray) === 0) {
            return false;
        }

        $documentDate = \DateTime::createFromFormat('d.m.Y', $date);
        $model = new SalesInvoice([
            'invoice_id' => $this->id,
            'type' => $this->type,
            'document_date' => $documentDate ? $documentDate->format('Y-m-d') : date('Y-m-d'),
        ]);
        if ($this->basis_document_name && $this->basis_document_number) {
            $model->agreement = $this->agreement;
        } else {
            $model->agreement = (($this->is_invoice_contract && $this->type == Documents::IO_TYPE_OUT) ?
                    'Счет-договор' : 'Счет') . "&{$this->fullNumber}&{$this->document_date}&";
        }
        $model->populateRelation('invoice', $this);
        if ($documentNumber) {
            $model->document_number = $documentNumber;
        } else {
            $model->newDocumentNumber();
        }
        if ($this->document_additional_number) {
            $model->document_additional_number = $this->document_additional_number;
        }

        $orderPLArray = [];
        foreach ($orderArray as $order) {
            if ($orderPL = OrderSalesInvoice::createFromOrder(null, $order)) {
                $orderPL->populateRelation('salesInvoice', $model);
                $orderPLArray[] = $orderPL;
            }
        }

        $model->populateRelation('orderSalesInvoices', $orderPLArray);
        $model->orders_sum = $model->totalAmountWithNds;
        $model->order_nds = $model->totalNds;

        if (LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE, function (SalesInvoice $model) {
            if (count($model->orderSalesInvoices) && $model->save()) {
                foreach ($model->orderSalesInvoices as $orderPL) {
                    $orderPL->sales_invoice_id = $model->id;
                    $orderPL->quantity = number_format($orderPL->quantity, 10, '.', '');
                    if ($orderPL->save()) {
                        continue;
                    } else {
                        ModelHelper::logErrors($orderPL, __METHOD__);
                    }

                    return false;
                }

                return true;
            }

            return false;
        })
        ) {
            $this->populateRelation('salesInvoice', $model);
            $this->updateAttributes([
                'has_sales_invoice' => true,
                'can_add_sales_invoice' => false,
            ]);

            return true;
        } else {
            \common\components\helpers\ModelHelper::logErrors($model, __METHOD__);

            return false;
        }
    }

    /**
     * @param null $date
     * @param null $documentNumber
     * @param null $limitDate
     * @param null $proxyPersonId
     * @return bool
     * @throws \Exception
     */
    public function createProxy($date = null, $documentNumber = null, $limitDate = null, $proxyPersonId = null)
    {
        $orderArray = array_filter($this->orders, function ($order) {
            return $order->product->production_type == Product::PRODUCTION_TYPE_GOODS;
        });
        if (count($orderArray) === 0) {
            $this->lastCreatedDocumentError = "Нельзя создать ТТН без товаров.";
            return false;
        }

        $documentDate = \DateTime::createFromFormat('d.m.Y', $date);
        if (!$limitDate)
            $limitDate = $documentDate ? $documentDate->modify('+5 day')->format('Y-m-d') : date('Y-m-d', time() + 3600 * 24 * 5);
        if (!$proxyPersonId)
            $proxyPersonId =  Yii::$app->user->id;
        else {
            $query = EmployeeCompany::find()->where([
                'employee_id' => $proxyPersonId,
                'company_id' => $this->company_id,
                'is_working' => true,
            ]);
            if (!$query->exists()) {
                Yii::$app->session->setFlash('warning', 'Сотрудник не найден.');
                return false;
            }
        }

        $model = new Proxy([
            'invoice_id' => $this->id,
            'type' => $this->type,
            'document_date' => $documentDate ? $documentDate->format('Y-m-d') : date('Y-m-d'),
            'limit_date' => $limitDate,
            'proxy_person_id' => $proxyPersonId
        ]);

        if ($this->basis_document_name && $this->basis_document_number /*&& $this->basis_document_date*/) {
            $model->basis_name = $this->basis_document_name;
            $model->basis_document_number = $this->basis_document_number;
            $model->basis_document_date = $this->basis_document_date;
            $model->basis_document_type_id = $this->basis_document_type_id;
        } else {
            $model->basis_name = 'Счет';
            $model->basis_document_number = $this->fullNumber;
            $model->basis_document_date = $this->document_date;
        }
        $model->populateRelation('invoice', $this);
        if ($documentNumber) {
            $model->document_number = $documentNumber;
        } else {
            $model->newDocumentNumber();
        }
        if ($this->document_additional_number) {
            $model->document_additional_number = $this->document_additional_number;
        }

        $orderPLArray = [];
        foreach ($orderArray as $order) {
            if ($orderPL = OrderProxy::createFromOrder(null, $order)) {
                $orderPL->populateRelation('proxy', $model);
                $orderPLArray[] = $orderPL;
            }
        }

        $model->populateRelation('orderProxies', $orderPLArray);
        $model->orders_sum = $model->totalAmountWithNds;

        if (LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE, function (Proxy $model) {

            if (count($model->orderProxies) && $model->save()) {
                foreach ($model->orderProxies as $orderPL) {
                    $orderPL->proxy_id = $model->id;
                    if ($orderPL->save()) {
                        continue;
                    }

                    foreach ($orderPL->getErrors() as $attribute => $error) {
                        $this->lastCreatedDocumentError .= "{$attribute} - {$error[0]} <br/>";
                    }

                    return false;
                }

                return true;
            }

            foreach ($model->getErrors() as $attribute => $error) {
                $this->lastCreatedDocumentError .= "{$attribute} - {$error[0]} <br/>";
            }

            return false;
        })
        ) {
            $this->populateRelation('proxy', $model);
            $this->updateAttributes([
                'has_proxy' => true,
                'can_add_proxy' => false,
            ]);

            return true;
        } else {
            \common\components\helpers\ModelHelper::logErrors($model, __METHOD__);

            return false;
        }
    }

    /**
     * @param null $date
     * @param null $documentNumber
     * @return bool
     * @throws \Exception
     */
    public function createWaybill($date = null, $documentNumber = null)
    {
        $orderArray = array_filter($this->orders, function ($order) {
            return $order->product->production_type == Product::PRODUCTION_TYPE_GOODS;
        });
        if (count($orderArray) === 0) {
            return false;
        }

        $documentDate = \DateTime::createFromFormat('d.m.Y', $date);
        $model = new Waybill([
            'invoice_id' => $this->id,
            'type' => $this->type,
            'document_date' => $documentDate ? $documentDate->format('Y-m-d') : date('Y-m-d'),
        ]);
        if ($this->basis_document_name && $this->basis_document_number && $this->basis_document_date) {
            $model->basis_name = $this->basis_document_name;
            $model->basis_document_number = $this->basis_document_number;
            $model->basis_document_date = $this->basis_document_date;
            $model->basis_document_type_id = $this->basis_document_type_id;
        } else {
            $model->basis_name = 'Счет';
            $model->basis_document_number = $this->fullNumber;
            $model->basis_document_date = $this->document_date;
        }
        $model->populateRelation('invoice', $this);
        if ($documentNumber) {
            $model->document_number = $documentNumber;
        } else {
            $model->newDocumentNumber();
        }
        if ($this->document_additional_number) {
            $model->document_additional_number = $this->document_additional_number;
        }

        $orderPLArray = [];
        foreach ($orderArray as $order) {
            if ($orderPL = OrderWaybill::createFromOrder(null, $order)) {
                $orderPL->populateRelation('waybill', $model);
                $orderPLArray[] = $orderPL;
            }
        }

        $model->populateRelation('orderWaybills', $orderPLArray);
        $model->orders_sum = $model->totalAmountWithNds;

        if (LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE, function (Waybill $model) {
            if (count($model->orderWaybills) && $model->save()) {
                foreach ($model->orderWaybills as $orderPL) {
                    $orderPL->waybill_id = $model->id;
                    if ($orderPL->save()) {
                        continue;
                    }

                    return false;
                }

                return true;
            }

            return false;
        })
        ) {
            $this->populateRelation('waybill', $model);
            $this->updateAttributes([
                'has_waybill' => true,
                'can_add_waybill' => false,
            ]);

            return true;
        } else {
            \common\components\helpers\ModelHelper::logErrors($model, __METHOD__);

            return false;
        }
    }

    /**
     * @param null $documentDate
     * @param null $documentNumber
     * @return bool
     * @throws \Exception
     */
    public function createInvoiceFacture($documentDate = null, $documentNumber = null, $isAuto = false)
    {
        if ($this->act) {
            $document_date = $this->act->document_date;
        } elseif ($this->packingList) {
            $document_date = $this->packingList->document_date;
        } else {
            $document_date = $documentDate ? $documentDate : date(DateHelper::FORMAT_DATE);
        }

        $model = new InvoiceFacture;
        $model->type = $this->type;
        $model->invoice_id = $this->id;
        $model->document_date = $document_date;
        $model->document_author_id =  $isAuto ? $this->document_author_id : null;
        $model->auto_created = (boolean) $isAuto;
        $model->populateRelation('invoice', $this);
        if ($documentNumber) {
            $model->document_number = $documentNumber;
        } else {
            $model->newDocumentNumber();
        }
        if ($this->document_additional_number) {
            $model->document_additional_number = $this->document_additional_number;
        }

        $orderArray = [];
        foreach ($this->orders as $invoiceOrder) {
            if ($order = OrderInvoiceFacture::createFromOrder(null, $invoiceOrder)) {
                $order->populateRelation('invoiceFacture', $model);
                $orderArray[] = $order;
            }
        }
        if (!$orderArray) {
            return null;
        }

        $model->populateRelation('ownOrders', $orderArray);

        $BankFlows = $this->getCashBankFlows()->andWhere([
            'and',
            ['not', ['payment_order_number' => null]],
            ['not', ['payment_order_number' => '']],
        ])->asArray()->all();

        $paymentOrderArray = [];
        if ($BankFlows) {
            foreach ($BankFlows as $flow) {
                $paymentOrder = new InvoiceFacturePaymentDocument([
                    'payment_document_number' => $flow['payment_order_number'],
                    'payment_document_date' => $flow['date'],
                ]);
                $paymentOrderArray[] = $paymentOrder;
            }
        }

        $model->populateRelation('paymentDocuments', $paymentOrderArray);

        $saveFunction = function ($model) {
            if ($model->ownOrders && $model->save()) {
                foreach ($model->ownOrders as $order) {
                    $order->invoice_facture_id = $model->id;
                    $order->quantity = number_format($order->quantity, 10, '.', '');
                    if (!$order->save()) {
                        return false;
                    }
                }
                foreach ($model->paymentDocuments as $paymentDocument) {
                    $paymentDocument->invoice_facture_id = $model->id;
                    if (!$paymentDocument->save()) {
                        return false;
                    }
                }

                return true;
            }

            return false;
        };

        if (LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE, $saveFunction)) {
            $this->populateRelation('invoiceFacture', $model);
            $this->updateAttributes([
                'has_invoice_facture' => true,
                'can_add_invoice_facture' => false,
            ]);

            return true;
        }
        \common\components\helpers\ModelHelper::logErrors($model, __METHOD__);

        return false;
    }

    /**
     * @param Invoice[] $invoices
     * @param $type
     * @param null $date
     * @return bool
     * @throws BadRequestHttpException
     * @throws \Exception
     */
    public static function createInvoiceFactureForSeveralInvoices($invoices, $type, $date = null)
    {
        if (!is_array($invoices)) {
            throw new BadRequestHttpException();
        }
        $invoice = $invoices[0];
        if (!($invoice instanceof Invoice)) {
            throw new BadRequestHttpException();
        }
        $orderArray = [];
        foreach ($invoices as $invoice) {
            foreach ($invoice->orders as $order) {
                $orderArray[] = $order;
            }
        }
        if (count($orderArray) === 0) {
            return false;
        }
        $model = new InvoiceFacture([
            'type' => $type,
            'document_date' => $date ? $date : date(DateHelper::FORMAT_DATE),
        ]);
        $model->populateRelation('invoices', $invoices);
        $model->populateRelation('invoice', $invoice);
        $model->newDocumentNumber();
        $orderInvoiceFactureArray = [];
        foreach ($orderArray as $order) {
            if ($orderInvoiceFacture = OrderInvoiceFacture::createFromOrder(null, $order)) {
                $orderInvoiceFacture->populateRelation('invoiceFacture', $model);
                $orderInvoiceFactureArray[] = $orderInvoiceFacture;
            }
        }
        $model->populateRelation('ownOrders', $orderInvoiceFactureArray);
        if (LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE, function (InvoiceFacture $model) {
            if (count($model->ownOrders) && $model->save()) {
                foreach ($model->ownOrders as $invoiceFactureOrder) {
                    $invoiceFactureOrder->invoice_facture_id = $model->id;
                    $invoiceFactureOrder->quantity = number_format($invoiceFactureOrder->quantity, 10, '.', '');
                    if ($invoiceFactureOrder->save()) {
                        continue;
                    }

                    return false;
                }

                return true;
            }

            return false;
        })
        ) {
            foreach ($invoices as $invoice) {
                $invoice->populateRelation('invoiceFacture', $model);
                $invoice->updateAttributes([
                    'has_invoice_facture' => true,
                    'can_add_invoice_facture' => false,
                ]);
                $invoiceInvoiceFacture = new InvoiceInvoiceFacture();
                $invoiceInvoiceFacture->invoice_id = $invoice->id;
                $invoiceInvoiceFacture->invoice_facture_id = $model->id;
                $invoiceInvoiceFacture->save();
            }

            return $model->id;
        } else {
            \common\components\helpers\ModelHelper::logErrors($model, __METHOD__);

            return false;
        }
    }

    /**
     * @param null $documentDate
     * @param null $documentNumber
     * @return bool|null
     * @throws \Exception
     */
    public function createUpd($documentDate = null, $documentNumber = null)
    {
        if (!$this->canAddUpd) {
            return null;
        }

        $model = new Upd;
        $model->type = $this->type;
        $model->company_id = $this->company_id;
        $model->contractor_id = $this->contractor_id;
        $model->invoice_id = $this->id;
        $model->populateRelation('invoice', $this);
        $model->document_date = is_string($documentDate) && ($d = date_create($documentDate)) ? $d->format('Y-m-d') : date('Y-m-d');
        if ($documentNumber) {
            $model->document_number = $documentNumber;
        } else {
            $model->newDocumentNumber();
        }
        if ($this->document_additional_number) {
            $model->document_additional_number = $this->document_additional_number;
        }

        $sum = 0;
        $sumNds = 0;
        $orderArray = [];
        foreach ($this->orders as $invoiceOrder) {
            if ($order = OrderUpd::createFromOrder(null, $invoiceOrder)) {
                $order->populateRelation('upd', $model);
                $orderArray[] = $order;
                $sum += $order->amountWithNds;
                $sumNds += $order->amountNds;
            }
        }
        if (!$orderArray) {
            return null;
        }

        $model->populateRelation('ownOrders', $orderArray);
        $model->orders_sum = $sum;
        $model->order_nds = $sumNds;

        $paymentDocuments = [];
        $paymentDocumentsData = [];

        $BankFlows = $this->getCashBankFlows()->andWhere([
            'and',
            ['not', ['payment_order_number' => null]],
            ['not', ['payment_order_number' => '']],
        ])->asArray()->all();
        if ($BankFlows) {
            foreach ($BankFlows as $flow) {
                $paymentDocumentsData[] = [
                    'payment_document_number' => $flow['payment_order_number'],
                    'payment_document_date' => $flow['date'],
                ];
            }
        }

        $orderFlows = $this->getCashOrderFlows()->andWhere([
            'and',
            ['not', ['number' => null]],
            ['not', ['number' => '']],
        ])->asArray()->all();
        if ($orderFlows) {
            foreach ($orderFlows as $flow) {
                $paymentDocumentsData[] = [
                    'payment_document_number' => $flow['number'],
                    'payment_document_date' => $flow['date'],
                ];
            }
        }

        $paymentDocumentsData = array_map("unserialize", array_unique(array_map("serialize", $paymentDocumentsData)));

        if (!empty($paymentDocumentsData)) {
            foreach ($paymentDocumentsData as $data) {
                $paymentDocuments[] = new UpdPaymentDocument([
                    'payment_document_number' => $data['payment_document_number'],
                    'payment_document_date' => $data['payment_document_date'],
                ]);
            }
        }

        if ($this->agreement) {
            $model->agreement = $this->agreement;
        } else {
            $model->agreement = "Счет&{$this->fullNumber}&{$this->document_date}&";
        }

        $saveFunction = function ($model) use ($paymentDocuments) {
            if ($model->ownOrders && $model->save()) {
                foreach ($model->ownOrders as $order) {
                    $order->upd_id = $model->id;
                    $order->quantity = number_format($order->quantity, 10, '.', '');
                    if (!$order->save()) {
                        $model->addErrors($order->errors);

                        return false;
                    }
                }
                foreach ($paymentDocuments as $paymentDocument) {
                    $paymentDocument->upd_id = $model->id;
                    if (!$paymentDocument->save()) {
                        $model->addErrors($paymentDocument->errors);

                        return false;
                    }
                }
                $model->populateRelation('paymentDocuments', $paymentDocuments);

                return true;
            }

            return false;
        };
        if (LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE, $saveFunction)) {
            $this->populateRelation('upd', $model);
            $this->updateAttributes([
                'has_upd' => true,
                'can_add_upd' => false,
            ]);

            $this->updateDocumentsPaid();
            PlanCashContractor::createFlowByInvoice($this, Documents::DOCUMENT_UPD);

            $employeeCompany = EmployeeCompany::findOne([
                'employee_id' => $this->document_author_id,
                'company_id' => $this->company_id,
            ]);
            $employeeCompany->populateRelation('company', $this->company);

            GoodsCancellation::cancellationByUpd($employeeCompany, $model);

            return true;
        }
        \common\components\helpers\ModelHelper::logErrors($model, __METHOD__);

        return false;
    }

    /**
     * @param Invoice[] $invoices
     * @param $type
     * @param null $date
     * @return bool
     * @throws BadRequestHttpException
     * @throws \Exception
     */
    public static function createUpdForSeveralInvoices($invoices, $type, $date = null)
    {
        if (!is_array($invoices)) {
            throw new BadRequestHttpException();
        }
        $invoice = $invoices[0];
        if (!($invoice instanceof Invoice)) {
            throw new BadRequestHttpException();
        }
        $orderArray = [];
        foreach ($invoices as $invoice) {
            foreach ($invoice->orders as $order) {
                $orderArray[] = $order;
            }
        }
        if (count($orderArray) === 0) {
            return false;
        }
        $model = new Upd([
            'type' => $type,
            'documentDate' => $date ? $date : date('d.m.Y'),
            'invoice_id' => $invoice->id,
            'company_id' => $invoice->company_id,
            'contractor_id' => $invoice->contractor_id,
            'isCreateForSeveralInvoices' => true
        ]);
        $model->populateRelation('invoices', $invoices);
        $model->populateRelation('invoice', $invoice);
        $model->newDocumentNumber();
        $orderUpdArray = [];

        $sum = 0;
        $sumNds = 0;
        foreach ($orderArray as $order) {
            if ($orderAct = OrderUpd::createFromOrder(null, $order)) {
                $orderAct->populateRelation('upd', $model);
                $orderUpdArray[] = $orderAct;
                $sum += $orderAct->amountWithNds;
                $sumNds += $orderAct->amountNds;
            }
        }

        $model->populateRelation('orderUpds', $orderUpdArray);
        $model->orders_sum = $sum;
        $model->order_nds = $sumNds;

        if (LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE, function (Upd $model) use ($invoices) {
            if (count($model->orderUpds) && $model->save()) {

                // move here for trigger `olap_documents_insert_order_upd`
                foreach ($invoices as $invoice) {
                    $invoiceAct = new InvoiceUpd();
                    $invoiceAct->invoice_id = $invoice->id;
                    $invoiceAct->upd_id = $model->id;
                    $invoiceAct->save();
                }

                foreach ($model->orderUpds as $orderUpd) {
                    $orderUpd->upd_id = $model->id;
                    $orderUpd->quantity = number_format($orderUpd->quantity, 10, '.', '');
                    if ($orderUpd->save()) {
                        continue;
                    }

                    return false;
                }

                return true;
            }

            return false;
        })
        ) {

            foreach ($invoices as $invoice) {
                $invoice->populateRelation('upd', $model);
                $invoice->updateAttributes([
                    'has_upd' => true,
                    'can_add_upd' => false,
                ]);
            }

            $invoice->updateDocumentsPaid();
            // PlanCashContractor::createFlowByInvoice($invoice, Documents::DOCUMENT_UPD);

            return $model->id;
        } else {
            return false;
        }
    }

    /**
     * @return boolean
     */
    public function getIsRejected()
    {
        return $this->invoice_status_id == InvoiceStatus::STATUS_REJECTED;
    }

    /**
     * @return boolean
     */
    public function getDiscountSum($withNds)
    {
        if ($withNds) {
            return $this->getOrders()->sum('[[base_price_with_vat]] * [[quantity]]') - $this->total_amount_with_nds;
        } else {
            return $this->getOrders()->sum('[[base_price_no_vat]] * [[quantity]]') - $this->total_amount_no_nds;
        }
    }

    /**
     * @return boolean
     */
    public function getHasDocs()
    {
        return (boolean)($this->has_act || $this->has_packing_list || $this->has_invoice_facture || $this->has_waybill);
    }

    /**
     * @param $fileName
     * @param $outInvoice
     * @param string $destination
     * @param null $addStamp
     * @return PdfRenderer
     *
     */
    public static function getRenderer($fileName, $outInvoice, $destination = PdfRenderer::DESTINATION_FILE, $addStamp = null)
    {
        $renderer = new PdfRenderer([
            'view' => '@frontend/modules/documents/views/invoice/pdf-view',
            'params' => array_merge([
                'model' => $outInvoice,
                'message' => new Message(Documents::DOCUMENT_INVOICE, $outInvoice->type),
                'ioType' => $outInvoice->type,
                'addStamp' => isset($addStamp) ? $addStamp : $outInvoice->company->pdf_send_signed,
            ]),
            'destination' => $destination,
            'filename' => $fileName,
            'displayMode' => PdfRenderer::DISPLAY_MODE_FULLPAGE,
            'format' => 'A4-P',
        ]);
        \Yii::$app->controller->view->params['asset'] = InvoicePrintAsset::className();

        return $renderer;
    }

    /**
     * @param $period
     * @param $invoices
     * @return string
     */
    public static function generateXlsTable($period, $invoices)
    {
        $excel = new Excel();
        $columns = [
            [
                'attribute' => 'document_date',
                'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
            ],
            [
                'attribute' => 'document_number',
                'value' => function (Invoice $model) {
                    return $model->fullNumber;
                },
            ],
            [
                'attribute' => 'total_amount_with_nds',
                'styleArray' => [
                    'numberformat' => [
                        'code' => '# ##0.00',
                    ]
                ],
                'dataType' => 'n',
                'value' => function (Invoice $model) {
                    return bcdiv($model->total_amount_with_nds, 100, 2);
                },
            ],
        ];
        $fileName = 'Счета_' . DateHelper::format($period['from'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) .
            '-' . DateHelper::format($period['to'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
        $userConfig = Yii::$app->user->identity->config;
        $tabConfig = [
            'paydate' => $userConfig->invoice_paydate,
            'paylimit' => $userConfig->invoice_paylimit,
            'act' => $userConfig->invoice_act,
            'paclist' => $userConfig->invoice_paclist,
            'invfacture' => $userConfig->invoice_invfacture,
            'upd' => $userConfig->invoice_upd,
            'author' => $userConfig->invoice_author,
        ];
        if ($tabConfig['paylimit']) {
            $columns[] = [
                'attribute' => 'payment_limit_date',
                'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
            ];
        }
        $columns[] = [
            'attribute' => 'contractor_id',
            'value' => 'contractor_name_short',
        ];
        $columns[] = [
            'attribute' => 'invoice_status_id',
            'value' => function (Invoice $model) {
                return $model->invoiceStatus->name;
            },
        ];
        if ($tabConfig['paydate']) {
            $columns[] = [
                'attribute' => 'payDate',
                'value' => function (Invoice $model) {
                    $payDate = $model->invoice_status_updated_at;

                    return in_array($model->invoice_status_id, [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_PAYED_PARTIAL])
                        ? date('d.m.Y', $payDate) : '';
                },
            ];
        }
        if ($tabConfig['act']) {
            $columns[] = [
                'attribute' => 'act',
                'value' => function (Invoice $model) {
                    if ($model->has_services && !$model->has_upd) {
                        $list = [];
                        foreach ($model->acts as $doc) {
                            $list[] = '№ ' . $doc->fullNumber;
                        }

                        return $list ? implode(', ', $list) : 'НЕТ';
                    }

                    return '---';
                },
            ];
        }
        if ($tabConfig['paclist']) {
            $columns[] = [
                'attribute' => 'packingList',
                'value' => function (Invoice $model) {
                    if ($model->has_goods && !$model->has_upd) {
                        $list = [];
                        foreach ($model->packingLists as $doc) {
                            $list[] = '№ ' . $doc->fullNumber;
                        }

                        return $list ? implode(', ', $list) : 'НЕТ';
                    }

                    return '---';
                }
            ];
        }
        if ($tabConfig['invfacture']) {
            $columns[] = [
                'attribute' => 'invoceFactureState',
                'visible' => Yii::$app->user->identity->company->hasNds(),
                'value' => function (Invoice $model) {
                    if (!$model->has_upd && $model->hasNds) {
                        $list = [];
                        foreach ($model->invoiceFactures as $doc) {
                            $list[] = '№ ' . $doc->fullNumber;
                        }

                        return $list ? implode(', ', $list) : 'НЕТ';
                    }

                    return '---';
                },
            ];
        }
        if ($tabConfig['upd']) {
            $columns[] = [
                'attribute' => 'upd',
                'value' => function (Invoice $model) {
                    if (!$model->hasDocs) {
                        $list = [];
                        foreach ($model->upds as $doc) {
                            $list[] = '№ ' . $doc->fullNumber;
                        }

                        return $list ? implode(', ', $list) : 'НЕТ';
                    }

                    return '---';
                },
            ];
        }

        $columns[] = [
            'attribute' => 'nomenclature',
            'wrap' => true,
            'value' => function (Invoice $model) {

                    $list = [];
                    foreach ($model->orders as $order) {
                        $unitName = $order->unit ? $order->unit->name : Product::DEFAULT_VALUE;
                        $list[] = $order->product_title . ' ' .
                            TextHelper::numberFormat($order->quantity, 2) . ' ' .
                            $unitName . ' x ' .
                            TextHelper::invoiceMoneyFormat($order->view_price_one, 2) . 'р. = ' .
                            TextHelper::invoiceMoneyFormat($order->view_total_amount, 2) . 'р.';
                    }

                    return $list ? implode("\r", $list) : 'НЕТ';
            },
        ];

        if ($tabConfig['author']) {
            $columns[] = [
                'attribute' => 'document_author_id',
                'value' => function (Invoice $model) {
                    return $model->author ? $model->author->getFio(true) : '';
                },
            ];
        }

        return $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $invoices,
            'title' => 'Счета',
            'columns' => $columns,
            'rangeHeader' => range('A', chr(ord('A') + count($columns))),
            'headers' => [
                'document_date' => 'Дата счета',
                'document_number' => '№ счета',
                'total_amount_with_nds' => 'Сумма',
                'payment_limit_date' => 'Оплатить до',
                'contractor_id' => 'Контрагент',
                'invoice_status_id' => 'Статус',
                'payDate' => 'Дата оплаты',
                'act' => 'Акт',
                'packingList' => 'Товарная накладная',
                'invoceFactureState' => 'Счет фактура',
                'upd' => 'УПД',
                'document_author_id' => 'Ответственный',
                'nomenclature' => 'Номенклатурная часть'
            ],
            'format' => 'Xlsx',
            'fileName' => $fileName,
        ]);
    }

    /**
     * @return bool|string
     * @throws \yii\db\Exception
     */
    public function generateOneCFile()
    {
        $exportModel = new Export([
            'company_id' => $this->company_id,
            'user_id' => $this->document_author_id,
        ]);

        $oneCExport = new OneCExport($exportModel);

        $chief = $this->company->getChiefEmployeeCompany();

        if ($oneCExport->findDataObjectInvoice($this->id, Documents::IO_TYPE_IN)) {
            $path = Yii::getAlias('@frontend/runtime/export') . DIRECTORY_SEPARATOR . $oneCExport->createExportFile($chief->employee_id);
            Yii::$app->db->createCommand("DELETE FROM `export` WHERE `export`.`id` = {$exportModel->id}")->execute();

            return $path;
        }
        return false;
    }

    /**
     * @return string
     */
    public function getOneCFileName()
    {
        return 'Счет №' . $this->getFullNumber() . '.xml';
    }

    /**
     * @throws \MpdfException
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function loadEmailFiles()
    {
        $files = [];

        $emailFile = new EmailFile();
        $emailFile->company_id = $this->company_id;
        $emailFile->file_name = str_replace(['\\', '/'], '_', $this->pdfFileName);
        $emailFile->ext = 'pdf';
        $emailFile->mime = 'application/pdf';
        $emailFile->size = 0;
        $emailFile->type = EmailFile::TYPE_INVOICE_DOCUMENT;
        if ($emailFile->save()) {
            $path = $emailFile->getFilePath() . DIRECTORY_SEPARATOR . $emailFile->file_name;
            $size = file_put_contents($path, self::getRenderer(null, $this, PdfRenderer::DESTINATION_STRING, null)->output(false));
            $emailFile->size = ceil($size / 1024);
            if ($emailFile->save(true, ['size'])) {
                $files[] = [
                    'id' => $emailFile->id,
                    'name' => $emailFile->file_name,
                    'documentPdf' => true,
                    'previewImg' => '<img src="/img/email/emailPdf.png" class="preview-img">',
                    'size' => $emailFile->size,
                    'deleteUrl' => Url::to(['/email/delete-email-file', 'id' => $emailFile->id]),
                    'downloadUrl' => Url::to(['/email/download-file', 'id' => $emailFile->id]),
                ];
            }
        }
        if ($this->company->autoact->send_together && $this->acts) {
            foreach ($this->acts as $act) {
                $emailFile = new EmailFile();
                $emailFile->company_id = $this->company_id;
                $emailFile->file_name = str_replace(['\\', '/'], '_', $act->pdfFileName);
                $emailFile->ext = 'pdf';
                $emailFile->mime = 'application/pdf';
                $emailFile->size = 0;
                $emailFile->act_id = $act->id;
                $emailFile->type = EmailFile::TYPE_ACT_DOCUMENT;
                if ($emailFile->save()) {
                    $path = $emailFile->getFilePath() . DIRECTORY_SEPARATOR . $emailFile->file_name;
                    $size = file_put_contents($path, Act::getRenderer(null, $act, PdfRenderer::DESTINATION_STRING)->output(false));
                    $emailFile->size = ceil($size / 1024);
                    if ($emailFile->save(true, ['size'])) {
                        $files[] = [
                            'id' => $emailFile->id,
                            'name' => $emailFile->file_name,
                            'previewImg' => '<img src="/img/email/emailPdf.png" class="preview-img">',
                            'size' => $emailFile->size,
                            'deleteUrl' => Url::to(['/email/delete-email-file', 'id' => $emailFile->id]),
                            'downloadUrl' => Url::to(['/email/download-file', 'id' => $emailFile->id]),
                        ];
                    }
                }
            }
        }
        if ($this->contractor_bik && $this->contractor_rs) {
            $name = ($this->is_invoice_contract && $this->type == Documents::IO_TYPE_OUT) ? 'счет-договора' : 'счета';
            $emailFile = new EmailFile();
            $emailFile->company_id = $this->company_id;
            $emailFile->file_name = 'Платежное_поручение_для_' . $name . '_№' .
                mb_ereg_replace("([^\w\s\d\-_])", '', $this->fullNumber) . '.txt';
            $emailFile->ext = 'txt';
            $emailFile->mime = 'text/plain';
            $emailFile->size = 0;
            $emailFile->type = EmailFile::TYPE_INVOICE_PAYMENT_ORDER;
            if ($emailFile->save()) {
                $path = $emailFile->getFilePath() . DIRECTORY_SEPARATOR . $emailFile->file_name;
                $size = file_put_contents($path, Yii::$app->view->render('@frontend/modules/documents/views/invoice/1C_payment_order.php', [
                    'model' => $this,
                ]));
                if (file_exists($path)) {
                    unlink($path);
                }
                $emailFile->size = ceil($size / 1024);
                if ($emailFile->save(true, ['size'])) {
                    $files[] = [
                        'id' => $emailFile->id,
                        'name' => $emailFile->file_name,
                        'previewImg' => '<img src="/img/email/emailTxt.png" class="preview-img">',
                        'size' => $emailFile->size,
                        'deleteUrl' => Url::to(['/email/delete-email-file', 'id' => $emailFile->id]),
                    ];
                }
            }
        }

        return $files;
    }

    /**
     * @param EmployeeCompany $sender
     * @param array|string $toEmail
     * @param null $emailText
     * @param null $subject
     * @param null $addStamp
     * @param null $addHtml
     * @param null $attach
     * @param bool $onlyMyAttach
     * @return bool
     * @throws Exception
     * @throws \MpdfException
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function sendAsEmail(EmployeeCompany $sender, $toEmail, $emailText = null, $subject = null, $addStamp = null, $addHtml = null, $attach = null, $onlyMyAttach = false, $sendWithDocuments = false)
    {
        if ($this->type == Documents::IO_TYPE_OUT) {
            if ($this->uid == null) {
                $this->updateAttributes([
                    'uid' => self::generateUid(),
                ]);
            }

            $message = $this->getMailerMessage($sender, $toEmail, $emailText, $subject, $addHtml);

            if (!$onlyMyAttach) {
                $message->attachContent(self::getRenderer(null, $this, PdfRenderer::DESTINATION_STRING, $addStamp)->output(false), [
                    'fileName' => str_replace(['\\', '/'], '_', $this->pdfFileName),
                    'contentType' => 'application/pdf',
                ]);
                if ($this->company->autoact->send_together && $this->acts) {
                    foreach ($this->acts as $act) {
                        $message->attachContent(Act::getRenderer(null, $act, PdfRenderer::DESTINATION_STRING)->output(false), [
                            'fileName' => str_replace(['\\', '/'], '_', $act->pdfFileName),
                            'contentType' => 'application/pdf',
                        ]);
                    }
                }
                if ($sendWithDocuments) {
                    foreach ($this->acts as $doc) {
                        $message->attachContent(Act::getRenderer(null, $doc, PdfRenderer::DESTINATION_STRING)->output(false), [
                            'fileName' => str_replace(['\\', '/'], '_', $doc->pdfFileName),
                            'contentType' => 'application/pdf',
                        ]);
                    }
                    foreach ($this->packingLists as $doc) {
                        $message->attachContent(PackingList::getRenderer(null, $doc, PdfRenderer::DESTINATION_STRING)->output(false), [
                            'fileName' => str_replace(['\\', '/'], '_', $doc->pdfFileName),
                            'contentType' => 'application/pdf',
                        ]);
                    }
                    foreach ($this->invoiceFactures as $doc) {
                        $message->attachContent(InvoiceFacture::getRenderer(null, $doc, PdfRenderer::DESTINATION_STRING)->output(false), [
                            'fileName' => str_replace(['\\', '/'], '_', $doc->pdfFileName),
                            'contentType' => 'application/pdf',
                        ]);
                    }
                    foreach ($this->upds as $doc) {
                        $message->attachContent(Upd::getRenderer(null, $doc, PdfRenderer::DESTINATION_STRING)->output(false), [
                            'fileName' => str_replace(['\\', '/'], '_', $doc->pdfFileName),
                            'contentType' => 'application/pdf',
                        ]);
                    }
                }
                if ($this->contractor_bik && $this->contractor_rs) {
                    $content = Yii::$app->view->render('@frontend/modules/documents/views/invoice/1C_payment_order.php', [
                        'model' => $this,
                    ]);
                    $name = ($this->is_invoice_contract && $this->type == Documents::IO_TYPE_OUT) ? 'счет-договора' : 'счета';
                    $message->attachContent($content, [
                        'fileName' => 'Платежное_поручение_для_' . $name . '_№' .
                            mb_ereg_replace("([^\w\s\d\-_])", '', $this->fullNumber) . '.txt',
                        'contentType' => 'text/plain',
                    ]);
                }
            }

            if ($attach) {
                foreach ($attach as $file) {
                    if ($file['is_file']) {
                        if (exif_imagetype($file['content']) === false) {
                            $message->attach($file['content'], [
                                'fileName' => $file['fileName'],
                                'contentType' => $file['contentType'],
                            ]);
                        } else {
                            $message->embed($file['content'], [
                                'fileName' => $file['fileName'],
                                'contentType' => $file['contentType'],
                            ]);
                        }
                    } else {
                        $message->attachContent($file['content'], [
                            'fileName' => $file['fileName'],
                            'contentType' => $file['contentType'],
                        ]);
                    }
                }
            }

            if ($message->send()) {
                $this->updateSendStatus($sender->employee_id);

                if ($this->company->first_send_invoice_date == null) {
                    $this->company->updateAttributes([
                        'first_send_invoice_date' => time(),
                    ]);
                }
                \common\models\company\CompanyFirstEvent::checkEvent($this->company, 11);

                return true;
            }
        }

        return false;
    }

    /**
     * @param $employeeId
     * @throws Exception
     */
    public function updateSendStatus($employeeId)
    {
        if (in_array($this->invoice_status_id, [InvoiceStatus::STATUS_CREATED, InvoiceStatus::STATUS_SEND])) {
            $this->setAttributes([
                'invoice_status_id' => InvoiceStatus::STATUS_SEND,
                'invoice_status_updated_at' => time(),
                'invoice_status_author_id' => $employeeId,
                'email_messages' => $this->email_messages + 1,
            ]);
            LogHelper::save($this, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function ($model) {
                return $model->save(false, [
                    'invoice_status_id',
                    'invoice_status_updated_at',
                    'invoice_status_author_id',
                    'email_messages',
                ]);
            });
        } else {
            $currentStatus = $this->invoice_status_id;
            $this->invoice_status_id = InvoiceStatus::STATUS_SEND;
            LogHelper::log($this, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS);
            $this->invoice_status_id = $currentStatus;
            $this->updateAttributes([
                'email_messages' => $this->email_messages + 1,
            ]);
        }

    }

    /**
     * @return string
     */
    public function getBasisDocString()
    {
        $str = '';
        if ($this->basis_document_name && $this->basis_document_number /*&& $this->basis_document_date*/) {
            $str .= $this->basis_document_name;
            $str .= ' № ' . $this->basis_document_number;
            $str .= ($this->basis_document_date) ? (' от ' . date_create_from_format('Y-m-d', $this->basis_document_date)->format('d.m.Y')) : '';
        }

        return $str;
    }

    /**
     * @return StoreCompany|null
     */
    public function getContractorRequisites()
    {
        $str = $this->contractor_name_short;
        $str .= $this->contractor_inn ? ", ИНН {$this->contractor_inn}" : '';
        $str .= $this->contractor_kpp ? ", КПП {$this->contractor_kpp}" : '';
        $str .= $this->contractor_address_legal_full ? ", {$this->contractor_address_legal_full}" : '';

        return $str;
    }

    /**
     * @return boolean
     */
    public function getHasNds()
    {
        return $this->nds_view_type_id != NdsViewType::NDS_VIEW_WITHOUT;
    }

    /**
     * Convert currency to rub
     * @return number
     */
    public function toRub($value)
    {
        return $value * $this->currency_rate / $this->currency_amount;
    }

    /**
     * Convert rub to currency
     * @return number
     */
    public function toCurrency($value)
    {
        return $value * $this->currency_amount / $this->currency_rate;
    }

    /**
     * Convert rub to currency
     * @return number
     */
    public function setCurrency_rateDate($value)
    {
        $this->currency_rate_date = implode('-', array_reverse(explode('.', $value)));
    }

    /**
     * Convert rub to currency
     * @return number
     */
    public function getCurrency_rateDate()
    {
        return implode('.', array_reverse(explode('-', $this->currency_rate_date)));
    }

    /**
     * @return mixed|string
     */
    public function getContractEssenceText()
    {
        if ($this->company->invoiceContractEssence && $this->company->invoiceContractEssence->is_checked &&
            $this->isNewRecord && $this->type == Documents::IO_TYPE_OUT) {
            return $this->company->invoiceContractEssence->text;
        }
        if ($this->contract_essence) {
            return $this->contract_essence;
        }
        if ($this->contract_essence_template === null) {
            $this->contract_essence_template = self::CONTRACT_ESSENCE_TEMPLATE_GOODS;
        }

        return self::$contractEssenceTemplatesText[$this->contract_essence_template];
    }

    /**
     * @inheritdoc
     */
    public function calculateTotalAmount()
    {
        $this->view_total_base =
        $this->view_total_discount =
        $this->view_total_amount =
        $this->view_total_no_nds =
        $this->view_total_nds =
        $this->view_total_with_nds = 0;

        $this->total_amount_with_nds =
        $this->total_amount_nds =
        $this->total_amount_no_nds = 0;

        $this->total_order_count = count($this->orders);

        foreach ($this->orders as $order) {
            $this->view_total_base += $order->view_total_base;
            $this->view_total_amount += $order->view_total_amount;
            $this->view_total_no_nds += $order->view_total_no_nds;
            $this->view_total_nds += $order->view_total_nds;
            $this->view_total_with_nds += $order->view_total_with_nds;

            $this->total_amount_with_nds += $this->type == Documents::IO_TYPE_IN
                ? $order->amount_purchase_with_vat : $order->amount_sales_with_vat;
            $this->total_amount_nds += ($this->type == Documents::IO_TYPE_IN
                ? $order->purchase_tax : $order->sale_tax);
            $this->total_amount_no_nds += $this->type == Documents::IO_TYPE_IN
                ? $order->amount_purchase_no_vat : $order->amount_sales_no_vat;
        }

        $this->view_total_base = round($this->view_total_base);
        $this->view_total_amount = round($this->view_total_amount);
        $this->view_total_no_nds = round($this->view_total_no_nds);
        $this->view_total_nds = round($this->view_total_nds);
        $this->view_total_with_nds = round($this->view_total_with_nds);

        $this->total_amount_with_nds = round($this->total_amount_with_nds);
        $this->total_amount_nds = round($this->total_amount_nds);
        $this->total_amount_no_nds = round($this->total_amount_no_nds);
        $this->total_amount_has_nds = $this->total_amount_nds > 0;

        $this->view_total_discount = $this->view_total_base - $this->view_total_amount;
    }

    /**
     * Recalculating the Invoice amount by currency rate
     *
     * @param $amount integer Currency::amount
     * @param $rate float Currency::rate
     * @param $name string Currency::name
     */
    public function recalculateByCarrencyRate($amount, $rate, $name = null)
    {
        if ($name !== null && Currency::find()->where(['name' => $name])->exists()) {
            $this->currency_name = $name;
        }

        if ($this->currency_name == Currency::DEFAULT_NAME) {
            $this->currency_amount = $this->currency_rate = 1;
        } else {
            $this->currency_amount = $amount;
            $this->currency_rate = $rate;
        }
        $orders = $this->orders;
        array_walk($orders, function (&$order) {
            $order->populateRelation('invoice', $this);
            $order->price = $order->price;
            $order->calculateOrder();
        });
        $this->populateRelation('orders', $orders);
        $this->calculateTotalAmount();
    }

    /**
     * Recalculating the Invoice amount by currency rate on date
     *
     * @param $date \DateTime
     */
    public function recalculateAmountOnDate(\DateTime $date)
    {
        if ($this->currency_name != Currency::DEFAULT_NAME && $this->currency_rate_type == Currency::RATE_PAYMENT_DATE) {
            $rateArray = CurrencyRate::getRateOnDate($date);
            if ($rateArray && isset($rateArray[$this->currency_name])) {
                $amount = $rateArray[$this->currency_name]['amount'];
                $rate = $rateArray[$this->currency_name]['value'];
                $this->recalculateByCarrencyRate($amount, $rate);

                return true;
            }
        }

        return false;
    }

    /**
     * @return array
     */
    public function getContractEssenceTemplates()
    {
        $templates = self::$contractEssenceTemplatesText;
        $serviceTemplate = $templates[self::CONTRACT_ESSENCE_TEMPLATE_SERVICES];
        $serviceTemplate = str_replace('{Наименование компании Исполнителя}', $this->company->getShortName(), $serviceTemplate);
        $templates[self::CONTRACT_ESSENCE_TEMPLATE_SERVICES] = $serviceTemplate;

        return $templates;
    }

    /**
     * @return integer
     */
    public function getViewLink()
    {
        return Yii::$app->urlManager->createAbsoluteUrl([
            '/bill/invoice/' . $this->uid,
        ]);
    }

    /**
     * @return int
     */
    public function getWeightPrecision() {
        if (!$this->_weightPrecision) {
            $this->_weightPrecision = 2;
            foreach ($this->orders as $order) {
                if (strpos($order->weight, '.') !== false) {
                    $wArr = explode('.', $order->weight);
                    if (!empty($wArr[1]))
                        $this->_weightPrecision = max($this->_weightPrecision, strlen($wArr[1]));
                }
            }
        }

        return $this->_weightPrecision;
    }

    /**
     * @return int
     */
    public function getVolumePrecision() {
        if (!$this->_volumePrecision) {
            $this->_volumePrecision = 2;
            foreach ($this->orders as $order) {
                if (strpos($order->volume, '.') !== false) {
                    $vArr = explode('.', $order->volume);
                    if (!empty($vArr[1]))
                        $this->_volumePrecision = max($this->_volumePrecision, strlen($vArr[1]));
                }
            }
        }

        return $this->_volumePrecision;
    }

    /**
     * @return boolean
     */
    public function getIsContractorEditable() {

        if ($this->getIsNewRecord()) {
            return true;
        }

        if (
            //$this->getHasDocs() ||
            //$this->has_upd ||
            //$this->getHasFlows() ||
            $this->is_surcharge ||
            ($this->type == Documents::IO_TYPE_OUT && $this->company->getIsFreeTariff())
        ) {
            return false;
        }

        return true;
    }

    /**
     * @return integer
     */
    public function getTotalAmountWithNds()
    {
        return $this->total_amount_with_nds;
    }

    /**
     * @return  string
     */
    public function getCompanyBankCity()
    {
        return !empty($this->company_bank_city) ? 'г. '.$this->company_bank_city : null;
    }

    /**
     * @return  string
     */
    public function getContractorBankCity()
    {
        return !empty($this->contractor_bank_city) ? 'г. '.$this->contractor_bank_city : null;
    }

    /**
     * @inheritDoc
     */
    public function createNotification(): NotificationInterface
    {
        return new InvoiceNotification($this);
    }

    public function getPayDate()
    {
        return in_array($this->invoice_status_id, [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_PAYED_PARTIAL])
            ? $this->invoice_status_updated_at : null;
    }

    public function getAgreementFullName()
    {
        if ($this->agreement_new_id) {
            /** @var \common\models\Agreement $agreement */
            if ($agreement = \common\models\Agreement::find()->where(['id' => $this->agreement_new_id, 'company_id' => $this->company_id])->one()) {
                if (trim($agreement->agreementType->name) !== trim($agreement->document_name)) {
                    return $agreement->document_name;
                }
            }
        }

        return '';
    }

    /**
     * @param $employeeCompany
     */
    public function setResponsible(EmployeeCompany $employeeCompany) : bool
    {
        if ($this->company_id == $employeeCompany->company_id) {
            if ($this->updateAttributes(['responsible_employee_id' => $employeeCompany->employee_id])) {
                $this->populateRelation('responsible', $employeeCompany);
                LogHelper::log($this, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_RESPONSIBLE);

                $modelArray = array_merge(
                    $this->acts,
                    $this->packingLists,
                    $this->invoiceFactures,
                    $this->upds,
                );
                foreach ($modelArray as $model) {
                    $model->populateRelation('invoice', $this);
                    LogHelper::log($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_RESPONSIBLE);
                }

                return true;
            }
        }

        return false;
    }

    public function getPriceRoundPrecision()
    {
        return self::$roundPrecisionValues[$this->price_round_precision] ?? self::$roundPrecisionValues[self::ROUND_PRECISION_DEFAULT];
    }

    public function getIndustry()
    {
        return $this->hasOne(CompanyIndustry::class, ['id' => 'industry_id']);
    }

    public function getSalePoint()
    {
        return $this->hasOne(SalePoint::class, ['id' => 'sale_point_id']);
    }

    public function getProject()
    {
        return $this->hasOne(Project::class, ['id' => 'project_id']);
    }
}
