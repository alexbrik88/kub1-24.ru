<?php

namespace common\models\document;

use Yii;

/**
 * This is the model class for table "invoice_expenditure_group".
 *
 * @property integer $id
 * @property string $name
 *
 * @property InvoiceExpenditureItem[] $invoiceExpenditureItems
 */
class InvoiceExpenditureGroup extends \yii\db\ActiveRecord
{
    const TAXES = 1;
    const RENT = 2;
    const ADVERTISING = 3;
    const EMPLOYEE = 4;

    const TAXES_NAME = 'Налоги';
    const RENT_NAME = 'Аренда';
    const ADVERTISING_NAME = 'Реклама';
    const EMPLOYEE_NAME = 'Сотрудники';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invoice_expenditure_group';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceExpenditureItems()
    {
        return $this->hasMany(InvoiceExpenditureItem::className(), ['group_id' => 'id']);
    }
}
