<?php
namespace common\models\document;

class DocumentImportType {

    const TYPE_KUB = 0;
    const TYPE_1C = 1;
    const TYPE_MOYSKLAD = 2;

    public static function getName($one_c_imported)
    {
        switch ($one_c_imported) {
            case self::TYPE_MOYSKLAD:
                return 'МойСклад';
            case self::TYPE_1C:
                return '1C';
            case self::TYPE_KUB:
            default:
                return 'КУБ24';
        }
    }

    public static function getList()
    {
        return [
            self::TYPE_KUB => 'КУБ24',
            self::TYPE_1C => '1C',
            self::TYPE_MOYSKLAD => 'МойСклад'
        ];
    }
}