<?php

namespace common\models\document;

use Yii;

/**
 * This is the model class for table "payment_details".
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 *
 * @property PaymentOrder[] $paymentOrders
 */
class PaymentDetails extends \yii\db\ActiveRecord
{
    const DETAILS_0 = 14;
    const DETAILS_1 = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_details';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string'],
            [['code'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentOrders()
    {
        return $this->hasMany(PaymentOrder::className(), ['payment_details_id' => 'id']);
    }
}
