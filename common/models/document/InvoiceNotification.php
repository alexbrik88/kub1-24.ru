<?php

namespace common\models\document;

use common\components\date\DateHelper;
use common\models\currency\Currency;
use frontend\modules\telegram\components\NotificationInterface;

class InvoiceNotification implements NotificationInterface
{
    use NotificationLimitTrait;

    /**
     * @var Invoice
     */
    private $invoice;

    /**
     * @param Invoice $invoice
     */
    public function __construct(Invoice $invoice)
    {
        $this->invoice = $invoice;
    }

    /**
     * @inheritDoc
     */
    public function isEnabled(): bool
    {
        if ($this->invoice->outInvoice) {
            return $this->invoice->outInvoice->notify_to_telegram;
        }

        if ($this->invoice->orderDocument) {
            return $this->invoice->orderDocument->createNotification()->isEnabled();
        }

        return false;
    }

    /**
     * @inheritDoc
     */
    public function getMessage(): string
    {
        return sprintf(
            "Счёт № %d от %s\nПокупатель: %s\nКоличество позиций: %d\nСумма: %01.2f %s",
            $this->invoice->document_number,
            $this->getDocumentDate(),
            $this->invoice->contractor_name_short,
            $this->invoice->total_order_count,
            $this->invoice->total_amount_with_nds / 100,
            $this->getCurrencyName(),
        );
    }

    /**
     * @inheritDoc
     */
    public function getCompanyId(): int
    {
        return $this->invoice->company_id;
    }

    /**
     * @return string
     */
    private function getCurrencyName(): string
    {
        $name = $this->invoice->currency_name;

        if ($name == Currency::DEFAULT_NAME) {
            return 'руб.';
        }

        return $name;
    }

    /**
     * @return string
     */
    private function getDocumentDate(): string
    {
        return DateHelper::format($this->invoice->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
    }
}
