<?php

namespace common\models\document;

interface BasisDocumentInterface
{
    /**
     * @return string
     */
    public function getBasisDocumentDate(): string;

    /**
     * @return string
     */
    public function getBasisDocumentName(): string;

    /**
     * @return string
     */
    public function getBasisDocumentNumber(): string;

    /**
     * @return bool
     */
    public function hasBasisDocumentParams(): bool;
}
