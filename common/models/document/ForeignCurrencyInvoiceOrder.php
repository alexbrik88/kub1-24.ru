<?php

namespace common\models\document;

use Yii;
use common\models\TaxRate;
use common\models\product\Product;
use common\models\product\ProductUnit;
use frontend\models\Documents;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "foreign_currency_invoice_order".
 *
 * @property int $id
 * @property int $foreign_currency_invoice_id
 * @property int $product_id
 * @property int $unit_id
 * @property int $tax_rate_id
 * @property int $number
 * @property string|null $article
 * @property string|null $product_title
 * @property float $quantity
 * @property float $reserve
 * @property int|null $tax_rate_value
 * @property float $discount
 * @property float $markup
 * @property float $price_base Цена до применения скидки или наценки
 * @property float $price_one
 * @property float $amount
 * @property float $nds_amount Сумма НДС
 * @property float $weight Вес, кг
 * @property float $volume Объем, м
 *
 * @property ForeignCurrencyInvoice $foreignCurrencyInvoice
 * @property Product $product
 * @property ProductUnit $productUnit
 * @property TaxRate $taxRate
 */
class ForeignCurrencyInvoiceOrder extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'foreign_currency_invoice_order';
    }

    /**
     * @return string
     */
    public function formName()
    {
        return 'Order';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['unit_id', 'tax_rate_id'], 'filter', 'filter' => function ($value) {
                return intval($value) ?: null;
            }],
            [['number', 'price', 'quantity', 'count', 'title'], 'required',],
            [['discount', 'markup', 'weight', 'volume'], 'default', 'value' => 0],
            [['number'], 'integer'],
            [['price', 'quantity', 'discount', 'markup', 'volume', 'weight'], 'filter', 'filter' => function ($value) {
                return str_replace(",", ".", $value);
            },],
            [
                ['price'], 'number',
                'numberPattern' => '/^\d+(\.\d*)?$/',
                'min' => Order::MIN_PRICE,
                'max' => Order::MAX_PRICE,
            ],
            [
                ['quantity'], 'number',
                'numberPattern' => '/^\d+(\.\d*)?$/',
                'min' => Order::MIN_QUANTITY,
                'max' => Order::MAX_QUANTITY,
            ],
            [['discount'], 'number', 'numberPattern' => '/^\d+(\.\d*)?$/', 'min' => 0, 'max' => Order::MAX_DISCOUNT,
                'when' => function ($model) {
                    return $model->invoice->discount_type == Invoice::DISCOUNT_TYPE_PERCENT;
                }],
            [['discount'], 'number', 'numberPattern' => '/^\d+(\.\d*)?$/', 'min' => 0, 'max' => Order::MAX_PRICE,
                'when' => function ($model) {
                    return $model->invoice->discount_type == Invoice::DISCOUNT_TYPE_RUBLE;
                }],
            [['markup'], 'number', 'numberPattern' => '/^\d+(\.\d*)?$/', 'min' => 0, 'max' => Order::MAX_MARKUP],
            [['weight', 'volume'], 'number', 'numberPattern' => '/^\d+(\.\d*)?$/', 'min' => 0],
            [['discount', 'markup', 'weight', 'volume'], 'filter', 'filter' => function ($value) {
                return round($value, 6);
            },],
            [['unit_id'], 'exist', 'skipOnEmpty' => true, 'skipOnError' => true, 'targetClass' => ProductUnit::className(), 'targetAttribute' => 'id'],
            [['tax_rate_id'], 'exist', 'skipOnEmpty' => true, 'skipOnError' => true, 'targetClass' => TaxRate::className(), 'targetAttribute' => 'id'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'foreign_currency_invoice_id' => 'Foreign Currency Invoice ID',
            'product_id' => 'Product ID',
            'unit_id' => 'Product Unit ID',
            'tax_rate_id' => 'Tax Rate ID',
            'number' => 'Number',
            'article' => 'Article',
            'product_title' => 'Product Name',
            'quantity' => 'Quantity',
            'reserve' => 'Reserve',
            'tax_rate_value' => 'Tax Rate Value',
            'discount' => 'Discount',
            'markup' => 'Markup',
            'price_base' => 'Price Base',
            'price_one' => 'Price One',
            'amount' => 'Amount',
            'nds_amount' => 'Nds Amount',
            'weight' => 'Weight',
            'volume' => 'Volume',
        ];
    }

    /**
     * @param $value
     */
    public function setPrice($value)
    {
        $invoice = $this->invoice;
        $value = floatval($value);
        $value = round($value, $invoice->price_precision);

        $this->price_base = max(0, $value * 100);
    }

    /**
     * @return string
     */
    public function getPrice()
    {
        return $this->price_base / 100;
    }

    /**
     * @param $value
     */
    public function setCount($value)
    {
        $this->quantity = $value;
    }

    /**
     * @return float
     */
    public function getCount()
    {
        return $this->quantity;
    }

    /**
     * @param $value
     */
    public function setTitle($value)
    {
        $this->product_title = $value;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->product_title;
    }

    /**
     * Gets query for [[ForeignCurrencyInvoice]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(ForeignCurrencyInvoice::className(), ['id' => 'foreign_currency_invoice_id']);
    }

    /**
     * Gets query for [[Product]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * Gets query for [[ProductUnit]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUnit()
    {
        return $this->hasOne(ProductUnit::className(), ['id' => 'unit_id']);
    }

    /**
     * Gets query for [[TaxRate]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTaxRate()
    {
        return $this->hasOne(TaxRate::className(), ['id' => 'tax_rate_id']);
    }

    /**
     * @return integer
     */
    public function getProductionType()
    {
        return $this->product->production_type;
    }

    /**
     * @return string
     */
    public function getProductTitle()
    {
        return Html::encode($this->product_title);
    }

    /**
     * @return boolean
     */
    public function getIsHour()
    {
        return $this->unit_id == ProductUnit::UNIT_HOUR;
    }

    /**
     * @return boolean
     */
    public function getDiscountViewValue()
    {
        $parts = explode('.', strval($this->discount * 1));

        return ArrayHelper::getValue($parts, 0, '0') .','.str_pad(ArrayHelper::getValue($parts, 1, ''), 2, '0');
    }

    /**
     * @return boolean
     */
    public function getIsDeleteAllowed()
    {
        return true;
    }

    /**
     * [createFromProduct description]
     * @param  Product $product  [description]
     * @param  Invoice $invoice  [description]
     * @param  integer $count    [description]
     * @param  [type]  $price    [description]
     * @param  integer $discount [description]
     * @param  integer $markup   [description]
     * @return [type]            [description]
     */
    public static function createFromProduct(Product $product, ForeignCurrencyInvoice $invoice, $count = 1, $price = null, $discount = 0, $markup = 0)
    {
        $order = new ForeignCurrencyInvoiceOrder();
        $order->populateRelation('invoice', $invoice);
        $order->populateRelation('product', $product);
        $taxRate = $invoice->type == Documents::IO_TYPE_IN ? $product->priceForBuyNds : $product->priceForSellNds;
        if ($taxRate) {
            $order->populateRelation('taxRate', $taxRate);
        }

        $order->tax_rate_id = $taxRate ? $taxRate->id : TaxRate::RATE_WITHOUT;
        $order->product_id = $product->id;
        $order->product_title = $product->title_en ?: $product->title;
        $order->product_code = $product->code;
        $order->article = $product->article;
        $order->box_type = $product->box_type;
        $order->unit_id = $product->product_unit_id;
        $order->place_count = $product->place_count;
        $order->count_in_place = $product->count_in_place;
        $order->mass_gross = $product->mass_gross;
        $order->excise = $product->has_excise;
        $order->excise_price = $product->excise;
        $order->country_id = $product->country_origin_id;
        $order->custom_declaration_number = $product->customs_declaration_number;

        if ($price === null) {
            if ($invoice->type == Documents::IO_TYPE_IN) {
                if ($invoice->nds_view_type_id == Invoice::NDS_VIEW_OUT) {
                    $price = $product->price_for_buy_with_nds / (1 + floatval($product->priceForBuyNds->rate));
                } else {
                    $price = $product->price_for_buy_with_nds;
                }
            } else {
                if ($invoice->nds_view_type_id == Invoice::NDS_VIEW_OUT) {
                    $price = $product->price_for_sell_with_nds / (1 + floatval($product->priceForBuyNds->rate));
                } else {
                    $price = $product->price_for_sell_with_nds;
                }
            }
        }

        $order->price = bcdiv($price, 100, 2);
        $order->quantity = $count;
        $order->discount = $discount;
        $order->markup = $markup;
        $order->number = 1;

        $order->calculateOrder();

        return $order;
    }

    /**
     * {@inheritdoc}
     */
    public function calculateOrder()
    {
        $invoice = $this->invoice;
        $quantity = $this->quantity;
        $precision = $invoice->price_precision == 4 ? 2 : 0;
        $ndsViewType = $invoice->nds_view_type_id;
        $hasNds = $ndsViewType != Invoice::NDS_VIEW_WITHOUT;
        $ndsOut = $ndsViewType == Invoice::NDS_VIEW_OUT;
        $ndsRate = $this->taxRate ? floatval($this->taxRate->rate) : 0;

        if ($invoice->has_markup) {
            $this->markup = max(0, min(Order::MAX_MARKUP, $this->markup));
            $this->discount = 0;
        } else {
            $this->markup = 0;
            if ($invoice->has_discount) {
                if ($invoice->discount_type == Invoice::DISCOUNT_TYPE_PERCENT) {
                    $this->discount = max(0, min(Order::MAX_DISCOUNT, $this->discount));
                }
            } else {
                $this->discount = 0;
            }
        }
        $discount = $this->discount;
        $markup = $this->markup;

        $this->price_one = round(Order::priceOne($this->price_base, $discount, $markup, $invoice->discount_type), $precision);

        $quantity = floatval($quantity) == intval($quantity) ? intval($quantity) : floatval($quantity);

        $this->amount = round($this->price_one * $quantity, $precision);
        $this->base_amount = round($this->price_base * $quantity, $precision);

        if ($hasNds) {
            if ($ndsOut) {
                $this->nds_amount = round($this->amount * $ndsRate, $precision);
            } else {
                $this->nds_amount = round($this->amount * $ndsRate / (1 + $ndsRate), $precision);
            }
        } else {
            $this->nds_amount = 0;
        }
    }
}
