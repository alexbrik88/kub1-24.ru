<?php

namespace common\models\document;

use Yii;
use common\models\EmployeeCompany;

/**
 * This is the model class for table "document_type".
 *
 * @property integer $id
 * @property string $name
 * @property string $name2
 *
 * @property EmployeeCompany[] $employeeCompanies
 */
class DocumentType extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'document_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'name2'], 'required'],
            [['name', 'name2'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'name2' => 'Название (родительный падеж)',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployeeCompanies()
    {
        return $this->hasMany(EmployeeCompany::className(), ['sign_document_type_id' => 'id']);
    }
}
