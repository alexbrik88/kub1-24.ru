<?php

namespace common\models\document;

use common\components\date\DateHelper;
use common\components\date\DatePickerFormatBehavior;
use common\components\helpers\ArrayHelper;
use common\components\pdf\PdfRenderer;
use common\components\pdf\Printable;
use common\components\SendDocument;
use common\models\Agreement;
use common\models\company\DetailsFile;
use common\models\company\DetailsFileType;
use common\models\currency\Currency;
use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;
use common\models\EmployeeCompany;
use common\models\employee\Employee;
use common\models\employee\EmployeeSignature;
use common\models\file\File;
use common\models\file\FileDir;
use frontend\models\Documents;
use frontend\models\log\ILogMessage;
use frontend\models\log\Log;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\modules\documents\components\UploadManagerHelper;
use frontend\modules\tax\models\Kudir;
use frontend\modules\tax\models\TaxDeclaration;
use Yii;
use yii\base\InvalidConfigException;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\Html;
use yii\swiftmailer\Message;

/**
 * This is the model class for table "act".
 *
 * @property integer $id
 * @property integer $type
 * @property string $created_at
 * @property integer $document_author_id
 * @property string $document_date
 *
 * @property Employee $documentAuthor
 * @property File $file
 *
 * @property string $fullNumber
 */
abstract class AbstractDocument extends \yii\db\ActiveRecord implements ILogMessage, Printable, SendDocument
{
    const MAX_INCREMENTED_DOCUMENT_NUMBER_LENGTH = 16;

    /**
     * @var array
     */
    public static $additionalFields = [];

    /**
     * @var string
     */
    public static $uploadDirectory;

    /**
     * @var string
     */
    public $emailTemplate = '';

    /**
     * @var string
     */
    public $urlPart = '';

    /**
     * @var array
     */
    public $removedOrders;

    /**
     * @var array
     */
    public $surchargeInvoiceOrders;
    public $surchargeNewProducts;

    /**
     * @var
     */
    public $deletedCompany;

    /**
     * @var string
     */
    protected $printablePrefix;

    /**
     * @var integer
     */
    protected $_fileCount;

    /**
     * @var
     */
    public $findFreeNumber = false;

    public function init()
    {
        if ($this->printablePrefix === null) {
            throw new InvalidConfigException('Printable config must be defined.');
        }

        if (static::$uploadDirectory === null) {
            throw new InvalidConfigException('Upload directory not defined.');
        }
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'documentDateBehavior' => [
                'class' => DatePickerFormatBehavior::className(),
                'attributes' => [
                    'document_date' => [
                        'message' => 'Дата документа указана неверно.',
                    ],
                ],
            ],
            'documentCreatedAtBehavior' => [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['type'], 'required', 'on' => 'insert',],
            [['type'], 'integer', 'on' => 'insert',],

            [['document_date',], 'safe'],
            [['document_number'], 'required'],
            [['document_number'], 'string', 'max' => 45],
            [['document_additional_number'], 'string', 'max' => 45],
            [['one_c_imported', 'one_c_exported'], 'safe'],
            [['findFreeNumber'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function fields()
    {
        $fields = parent::fields();

        if (self::$additionalFields) {
            foreach ((array) self::$additionalFields as $fieldName) {
                if (property_exists($this, $fieldName) ||
                    method_exists($this, $fieldName) ||
                    method_exists($this, 'get' . $fieldName)
                ) {
                    $fields[$fieldName] = $fieldName;
                }
            }
        }

        return $fields;
    }

    /**
     * @return string
     */
    public static function generateUid()
    {
        $query = new \yii\db\Query;
        $query->select('id')->from(static::tableName())->where('[[uid]]=:uid');

        do {
            $uid = bin2hex(random_bytes(16));
        } while ($query->params([':uid' => $uid])->exists());

        return $uid;
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                foreach (DetailsFile::$applyTypeList as $typeId) {
                    $attribute = DetailsFileType::$fieldNameList[$typeId];
                    if ($this->hasAttribute($attribute)) {
                        $imageModel = $this->company->getImageModel($attribute);
                        $this->$attribute = $imageModel ? $imageModel->id : null;
                    }
                }
                if ($this->hasAttribute('ordinal_document_number')) {
                    if (empty($this->ordinal_document_number)) {
                        $this->ordinal_document_number = $this->document_number;
                    }
                    $this->ordinal_document_number = substr(strval(intval($this->ordinal_document_number)), -9);
                }
                if ($this->hasAttribute('status_out_updated_at') && !isset($this->status_out_updated_at)) {
                    $this->status_out_updated_at = time();
                }
            }
            if (!Yii::$app->request->isConsoleRequest) {
                $employee = Yii::$app->id == 'app-frontend' && !Yii::$app->user->isGuest && $this->company->isEmployee(Yii::$app->user->identity) ?
                    Yii::$app->user->identity : $this->company->employeeChief;

                if ($insert) {
                    if (!$this->document_author_id) {
                        $this->document_author_id = $employee->id;
                    }
                    $employeeCompany = EmployeeCompany::findOne([
                        'employee_id' => $this->document_author_id,
                        'company_id' => $this->company->id,
                    ]);
                    if ($employeeCompany && $employeeCompany->can_sign &&
                        !($this instanceof TaxDeclaration) &&
                        !($this instanceof Kudir)
                    ) {
                        $this->setSignatory($employeeCompany);
                    }
                } else {
                    if ($this->hasAttribute('is_original') && $this->isAttributeChanged('is_original')) {
                        $this->is_original_updated_at = time();
                    }
                }
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @param Company $company
     * @param EmployeeCompany $employeeCompany
     */
    private function setSignatory(EmployeeCompany $employeeCompany)
    {
        if ($this->hasAttribute('signed_by_employee_id')) {
            $this->signed_by_employee_id = $employeeCompany->employee_id;
            $this->signed_by_name = $employeeCompany->getFio(true);
            $this->sign_document_type_id = $employeeCompany->sign_document_type_id;
            $this->sign_document_number = $employeeCompany->sign_document_number;
            $this->sign_document_date = $employeeCompany->sign_document_date;
            $this->signature_id = $employeeCompany->signature_id;
        }
    }


    // RELATIONS

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentAuthor()
    {
        return $this->hasOne(Employee::className(), ['id' => 'document_author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentStatus()
    {
        return new \yii\db\ActiveQuery;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployeeSignature()
    {
        return $this->hasOne(EmployeeSignature::className(), ['id' => 'signature_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    abstract public function getFile();

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFiles()
    {
        return $this->hasMany(File::className(), ['owner_id' => 'id'])
            ->onCondition(['owner_model' => static::className()]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScanDocuments()
    {
        return $this->hasMany(ScanDocument::className(), ['owner_id' => 'id'])
            ->onCondition(['owner_model' => static::className()]);
    }

    /**
     * @inheritdoc
     */
    public function updateHasFile()
    {
        $owner_table = static::tableName();

        if ($this->hasAttribute('has_file')) {
            Yii::$app->db->createCommand("
                UPDATE {{{$owner_table}}}
                SET
                    {{{$owner_table}}}.[[has_file]] = IF(
                        EXISTS(SELECT {{f}}.[[id]] FROM {{file}} {{f}} WHERE {{f}}.[[owner_id]] = {{{$owner_table}}}.id AND {{f}}.[[owner_model]] = :owner_class), 1,
                        EXISTS(SELECT {{s}}.[[id]] FROM {{scan_document}} {{s}} WHERE {{s}}.[[owner_id]] = {{{$owner_table}}}.id AND {{s}}.[[owner_model]] = :owner_class)
                    )
                WHERE {{{$owner_table}}}.[[id]] = :owner_id
            ", [
                ':owner_class' => static::className(),
                ':owner_id' => $this->id,
            ])->execute();
        }
    }

    // PRINTABLE

    /**
     * @return string
     */
    public function getPdfFileName()
    {
        return $this->getPrintTitle() . '.pdf';
    }

    /**
     * @param int $page
     * @param int $totalPages
     * @return string
     */
    public function getPngFileName($page = 1, $totalPages = 1)
    {
        return $this->getPrintTitle() . ($totalPages > 1 ? "-стр.{$page}.png" : ".png");
    }

    /**
     * @return string
     */
    public function getPrintTitle()
    {
        return $this->printablePrefix . '_' . $this->getFullNumber();
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        $prefix = isset($this->shortPrefix) ? $this->shortPrefix : $this->printablePrefix;
        $date = date_format(date_create($this->document_date), 'd.m.Y');

        return "{$prefix} № {$this->getFullNumber()} от {$date}";
    }

    /**
     * @return null|string
     */
    public function getBasisName()
    {
        if (!empty($this->basis_document_name) &&
            !empty($this->basis_document_number) &&
            !empty($this->basis_document_date)
        ) {
            if ($this->basis_document_name == 'Счет' && isset($this->invoices) && count($this->invoices) > 1) {
                return $this->getBasisInvoicesNames();
            }
            return $this->basis_document_name .
                ' № ' . Html::encode($this->basis_document_number) . ' от ' .
                DateHelper::format(
                    $this->basis_document_date,
                    DateHelper::FORMAT_USER_DATE,
                    DateHelper::FORMAT_DATE
                );
        } elseif (isset($this->invoice)) {
            if (isset($this->invoices) && count($this->invoices) > 1) {
                return $this->getBasisInvoicesNames();
            }
            return $this->invoice->getBasisName() ? : 'Счет № '.$this->invoice->fullNumber.' от '.DateHelper::format(
                $this->invoice->document_date,
                DateHelper::FORMAT_USER_DATE,
                DateHelper::FORMAT_DATE
            );
        }

        return null;
    }

    public function getBasisInvoicesNames()
    {
        if (!isset($this->invoices))
            return '';

        $invoiceTexts = [];
        foreach ((array)$this->invoices as $invoice) {
            $invoiceTexts[] = 'Счет № '.$invoice->fullNumber.' от '.DateHelper::format(
                    $invoice->document_date,
                    DateHelper::FORMAT_USER_DATE,
                    DateHelper::FORMAT_DATE
                );
        }

        return implode(',  ', $invoiceTexts);
    }
    /**
     * @return string
     */
    public function getViewUrl()
    {
        return \yii\helpers\Url::to([
            "/documents/{$this->urlPart}/view",
            'type' => $this->type,
            'id' => $this->id,
        ]);
    }

    /**
     * Full document number
     *
     * @return string
     */
    abstract public function getFullNumber();


    // ILOGMESSAGE

    /**
     * @inheritdoc
     */
    public function getLogIcon(Log $log)
    {
        return Documents::getLogIcon($log);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        if (!$this instanceof Invoice && (isset($this->invoice) && $this->invoice)) {
            return $this->invoice->getCompany();
        }
    }

    /**
     * @param string $name
     * @return DetailsFile|null
     */
    public function getImageModel($name)
    {
        if ($attribute = ArrayHelper::getValue(DetailsFileType::$fieldNameList, DetailsFileType::findTypeId($name))) {
            return DetailsFile::findOne($this->$attribute);
        }

        return null;
    }

    /**
     * @param string $name
     * @return string|null
     */
    public function getImage($name)
    {
        return ArrayHelper::getValue($this->getImageModel($name), 'filePath');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->getDocumentAuthor();
    }

    /**
     * @return string
     */
    public function getUploadDirectory()
    {
        $filePath = DIRECTORY_SEPARATOR . static::$uploadDirectory
            . DIRECTORY_SEPARATOR . $this->id;
        $fullPath = Yii::getAlias('@common') . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . 'documents' . $filePath;
        if (!file_exists($fullPath)) {
            $parentDir = substr($fullPath, 0, strrpos($fullPath, DIRECTORY_SEPARATOR));
            if (!file_exists($parentDir)) {
                mkdir($parentDir);
            }
            mkdir($fullPath);
        }

        return $fullPath;
    }

    /**
     * @return mixed
     */
    public function getDocumentNumber()
    {
        return $this->document_number ? $this->document_number : $this->ordinal_document_number;
    }

    /**
     * @return mixed
     */
    public function getMaxDocumentNumber()
    {
        if (!isset($this->invoice)) {
            $company = null;
        } else {
            $company = $this->invoice->company;
        }

        $year = date('Y', strtotime($this->document_date));
        $t0 = Invoice::tableName();
        $t1 = Act::tableName();
        $t2 = PackingList::tableName();
        $t3 = InvoiceFacture::tableName();
        $t4 = Upd::tableName();
        $t5 = Waybill::tableName();
        $query1 = Act::find()->byCompany($company->id)
            ->select(new Expression("{{{$t1}}}.[[document_number]], YEAR({{{$t1}}}.[[document_date]]) [[document_year]]"))
            ->andWhere(["$t0.is_deleted" => false, "$t0.type" => $this->type])
            ->having(['document_year' => $year])->andHaving('CHAR_LENGTH([[document_number]]) <= ' . self::MAX_INCREMENTED_DOCUMENT_NUMBER_LENGTH);
        $query2 = PackingList::find()->byCompany($company->id)
            ->select(new Expression("{{{$t2}}}.[[document_number]], YEAR({{{$t2}}}.[[document_date]]) [[document_year]]"))
            ->andWhere(["$t0.is_deleted" => false, "$t0.type" => $this->type])
            ->having(['document_year' => $year])->andHaving('CHAR_LENGTH([[document_number]]) <= ' . self::MAX_INCREMENTED_DOCUMENT_NUMBER_LENGTH);
        $query3 = InvoiceFacture::find()->byCompany($company->id)
            ->select(new Expression("{{{$t3}}}.[[document_number]], YEAR({{{$t3}}}.[[document_date]]) [[document_year]]"))
            ->andWhere(["$t0.is_deleted" => false, "$t0.type" => $this->type])
            ->having(['document_year' => $year])->andHaving('CHAR_LENGTH([[document_number]]) <= ' . self::MAX_INCREMENTED_DOCUMENT_NUMBER_LENGTH);
        $query4 = Upd::find()->byCompany($company->id)
            ->select(new Expression("{{{$t4}}}.[[document_number]], YEAR({{{$t4}}}.[[document_date]]) [[document_year]]"))
            ->andWhere(["$t0.is_deleted" => false, "$t0.type" => $this->type])
            ->having(['document_year' => $year])->andHaving('CHAR_LENGTH([[document_number]]) <= ' . self::MAX_INCREMENTED_DOCUMENT_NUMBER_LENGTH);
        $query5 = Waybill::find()->byCompany($company->id)
            ->select(new Expression("{{{$t5}}}.[[document_number]], YEAR({{{$t5}}}.[[document_date]]) [[document_year]]"))
            ->andWhere(["$t0.is_deleted" => false, "$t0.type" => $this->type])
            ->having(['document_year' => $year])->andHaving('CHAR_LENGTH([[document_number]]) <= ' . self::MAX_INCREMENTED_DOCUMENT_NUMBER_LENGTH);


        $query = (new Query)->from(['t' => $query1->union($query2)->union($query3)->union($query4)->union($query5)]);

        return (integer)$query->max('[[document_number]] * 1');
    }

    /**
     * @param integer
     */
    public function setFileCount($value)
    {
        $this->_fileCount = ctype_digit($value) ? intval($value) : $value;
    }

    /**
     * @return integer
     */
    public function getFileCount()
    {
        if (!isset($this->_fileCount)) {
            $this->_fileCount = (int) $this->getFiles()->count();
        }

        return $this->_fileCount;
    }

    /**
     * @return string
     */
    public function invoiceTableStatusIcon()
    {
        if (!empty($this->invoice)) {
            $content = '';
            if ($this->invoice->invoice_status_id == InvoiceStatus::STATUS_PAYED) {
                $content = '<i class="icon icon-check"></i>';
            } elseif ($this->type == Documents::IO_TYPE_OUT) {
                $content = '<i class="icon ' . $this->statusOut->getIcon() . '"></i>';
            } else {
                if ($this->is_original) {
                    $content = '<i class="icon icon-original-document"></i>';
                } else {
                    $content = '<i class="icon fa fa-copy"></i>';
                }
            }

            return Html::tag('span', $content, ['class' => 'pull-right', 'style' => 'color: #5b9bd1; padding-right: 5px;']);
        }

        return '';
    }

    /**
     * @return string
     */
    public function getDocumentNumberOneC()
    {
        if (isset($this->invoice) && $this->invoice instanceof \common\models\document\Invoice)
            $invoice = $this->invoice;
        elseif ($this instanceof \common\models\document\Invoice)
            $invoice = $this;
        else
            $invoice = null;

        return ($invoice && $invoice->is_additional_number_before) ?
            $this->document_additional_number . $this->document_number :
            $this->document_number . $this->document_additional_number;
    }

    /**
     * @param  EmployeeCompany $sender
     * @param  string|array $toEmail
     * @param  string $emailText
     * @return Message
     */
    public function getMailerMessage(EmployeeCompany $sender, $toEmail, $emailText = null, $subject = null, $addHtml = null)
    {
        $emailSubject = $subject ?: $this->emailSubject;
        $params = [
            'model' => $this,
            'employeeCompany' => $sender,
            'employee' => $sender->employee,
            'subject' => $emailSubject,
            'toEmail' => $toEmail,
            'emailText' => $emailText,
            'addHtml' => $addHtml,
            'pixel' => [
                'company_id' => $this->company->id,
                'email' => is_array($toEmail) ? implode(',', $toEmail) : $toEmail,
            ],
        ];

        Yii::$app->mailer->htmlLayout = 'layouts/document-html';

        $message = Yii::$app->mailer->compose([
            'html' => "system/documents/{$this->emailTemplate}/html",
            'text' => "system/documents/{$this->emailTemplate}/text",
        ], $params)
            ->setFrom([\Yii::$app->params['emailList']['docs'] => $sender->getFio()])
            ->setReplyTo([$sender->employee->email => $sender->getFio(true)])
            ->setSubject($emailSubject)
            ->setTo($toEmail);

        return $message;
    }

    /**
     * @return string
     */
    public function getEmailSubject()
    {
        return '';
    }

    /**
     * @param  EmployeeCompany $sender
     * @param  string|array $toEmail
     * @param  string $emailText
     * @return boolean
     */
    public function sendAsEmail(EmployeeCompany $sender, $toEmail, $emailText = null, $subject = null)
    {
        return false;
    }

    /**
     *
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert) {
            $event = null;
            switch (true) {
                case $this instanceof InvoiceAuto:
                    $event = $this->type == Documents::IO_TYPE_IN ? null : ($this->type == Documents::IO_TYPE_OUT ? 52 : null);
                    break;
                case $this instanceof Invoice:
                    if ($this->invoice_status_id == InvoiceStatus::STATUS_AUTOINVOICE) {
                        $event = $this->type == Documents::IO_TYPE_IN ? null : ($this->type == Documents::IO_TYPE_OUT ? 52 : null);
                    } else {
                        $event = $this->type == Documents::IO_TYPE_IN ? 38 : ($this->type == Documents::IO_TYPE_OUT ? 7 : null);
                    }
                    break;
                case $this instanceof Act:
                    $event = $this->type == Documents::IO_TYPE_IN ? 40 : ($this->type == Documents::IO_TYPE_OUT ? 18 : null);
                    break;
                case $this instanceof PackingList:
                    $event = $this->type == Documents::IO_TYPE_IN ? 42 : ($this->type == Documents::IO_TYPE_OUT ? 23 : null);
                    break;
                case $this instanceof InvoiceFacture:
                    $event = $this->type == Documents::IO_TYPE_IN ? 44 : ($this->type == Documents::IO_TYPE_OUT ? 28 : null);
                    break;
                case $this instanceof Upd:
                    $event = $this->type == Documents::IO_TYPE_IN ? null : ($this->type == Documents::IO_TYPE_OUT ? 33 : null);
                    break;
                case $this instanceof Agreement:
                    $event = null;
                    break;
            }

            if ($event) {
                \common\models\company\CompanyFirstEvent::checkEvent($this->company, $event);
            }
        }

        // Scan Preview Mode
        if (is_a(Yii::$app,'yii\web\Application')) {
            if (!$insert || Yii::$app->request->get('permanentAttachFiles')) {
                if (Yii::$app->request->get('mode') == 'previewScan') {
                    UploadManagerHelper::attachFiles($this);
                    $this->updateHasFile();
                    UploadManagerHelper::setAttachFilesMessage($this);
                }
            }
        }
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $this->deletedCompany = $this->company ?? $this->invoice->company;

            /** @var File $file */
            foreach ($this->files as $file) {
                $file->delete();
            }
            /** @var ScanDocument $scan */
            foreach ($this->scanDocuments as $scan) {
                $scan->owner_model = null;
                $scan->owner_id = null;
                $scan->owner_table = null;
                $scan->save(false);
            }

            return true;
        }

        return false;
    }

    /**
     *
     */
    public function afterDelete()
    {
        parent::afterDelete();

        $event = null;
        switch (true) {
            case $this instanceof Agreement:
                break;
            case $this instanceof InvoiceAuto:
                break;
            case $this instanceof Invoice:
                $event = $this->type == Documents::IO_TYPE_IN ? null : ($this->type == Documents::IO_TYPE_OUT ? 14 : null);
                break;
            case $this instanceof Act:
                $event = $this->type == Documents::IO_TYPE_IN ? null : ($this->type == Documents::IO_TYPE_OUT ? 22 : null);
                break;
            case $this instanceof PackingList:
                $event = $this->type == Documents::IO_TYPE_IN ? null : ($this->type == Documents::IO_TYPE_OUT ? 27 : null);
                break;
            case $this instanceof InvoiceFacture:
                $event = $this->type == Documents::IO_TYPE_IN ? null : ($this->type == Documents::IO_TYPE_OUT ? 32 : null);
                break;
            case $this instanceof Upd:
                $event = $this->type == Documents::IO_TYPE_IN ? null : ($this->type == Documents::IO_TYPE_OUT ? 37 : null);
                break;
        }
        if ($event) {
            \common\models\company\CompanyFirstEvent::checkEvent($this->deletedCompany, $event);
        }
    }

    /**
     * @return string
     */
    public function getPdfContent()
    {
        if (method_exists($this, 'getRenderer')) {
            return static::getRenderer(null, $this, PdfRenderer::DESTINATION_STRING)->output(false);
        }

        return null;
    }


    protected $_printAmount;
    protected $_orderAmountArray;

    /**
     * @return integer
     */
    public function getPrintAmountWithNds()
    {
        if (!isset($this->_printAmount)) {
            $this->_printAmount = $this->totalAmountWithNds;
            if (isset($this->invoice) && $this->invoice->currency_name != Currency::DEFAULT_NAME) {
                $method = "getOwnOrders";
                if (method_exists($this, $method)) {
                    $count1 = $this->$method()->sum('quantity');
                    $count2 = $this->invoice->getOrders()->sum('quantity');
                    if ($count1 == $count2) {
                        $this->_printAmount = $this->invoice->total_amount_with_nds;
                    }
                }
            }
        }

        return $this->_printAmount;
    }

    /**
     * @return integer
     */
    public function getPrintAmountNoNds()
    {
        return $this->getPrintAmountWithNds() - $this->getTotalNds();
    }

    /**
     * @return integer
     */
    public function getPrintOrderAmount($orderId, $ndsExclude = false)
    {
        if (!isset($this->_orderAmountArray)) {
            $data = [];
            if (isset($this->ownOrders)) {
                foreach ($this->ownOrders as $key => $item) {
                    $itemId = $item->order_id;
                    $data[$itemId] = [
                        0 => $item->amountNoNds,
                        1 => $item->amountWithNds,
                    ];
                    $data[$itemId]['nds'] = $data[$itemId][1] - $data[$itemId][0];
                }
            }

            $printAmount = $this->getPrintAmountWithNds();
            $totalAmount = $this->totalAmountWithNds;

            if (isset($this->invoice) &&
                $this->invoice->currency_name != Currency::DEFAULT_NAME &&
                $totalAmount != $printAmount
            ) {
                $maxId = null;
                $maxVal = 0;
                foreach ($data as $key => $val) {
                    if ($val[1] > $maxVal) {
                        $maxId = $key;
                        $maxVal = $val[1];
                    }
                }
                $d = $totalAmount - $printAmount;
                $maxVal = $maxVal - $d;
                $data[$maxId][1] = $maxVal;
                $data[$maxId][0] = $maxVal - $data[$maxId]['nds'];
            }

            $this->_orderAmountArray = $data;
        }

        $nds = $ndsExclude ? 0 : 1;

        return ArrayHelper::getValue($this->_orderAmountArray, [$orderId, $nds]);
    }

    /**
     * @return bool
     */
    public function getIsWasSent()
    {
        switch (true) {
            case ($this instanceof Act):
                $attribute = 'status_out_id';
                $statusId = status\ActStatus::STATUS_SEND;
                break;

            case ($this instanceof Agreement):
                $attribute = 'status_id';
                $statusId = status\AgreementStatus::STATUS_SEND;
                break;

            case ($this instanceof Invoice):
                $attribute = 'invoice_status_id';
                $statusId = status\InvoiceStatus::STATUS_SEND;
                break;

            case ($this instanceof InvoiceFacture):
                $attribute = 'status_out_id';
                $statusId = status\InvoiceFactureStatus::STATUS_DELIVERED;
                break;

            case ($this instanceof PackingList):
                $attribute = 'status_out_id';
                $statusId = status\PackingListStatus::STATUS_SEND;
                break;

            case ($this instanceof Upd):
                $attribute = 'status_out_id';
                $statusId = status\UpdStatus::STATUS_SEND;
                break;
        }

        return (isset($attribute, $statusId)) ? Log::find()->where([
            'and',
            ['model_name' => get_called_class()],
            ['model_id' => $this->id],
            ['log_entity_type_id' => LogEntityType::TYPE_DOCUMENT],
            ['log_event_id' => LogEvent::LOG_EVENT_UPDATE_STATUS],
            ['like', 'attributes_new', "\"{$attribute}\":{$statusId},"],
        ])->exists() : false;
    }

    public function groupOwnOrdersByProduct($orders = [])
    {
        if (count($orders) <= 1)
            return $orders;

        $includedPositions = [];
        $mergedOrders = [];

        for ($i = 0; $i < count($orders); $i++) {
            if (!in_array($i, $includedPositions)) {

                $mergedOrders[$i] = $orders[$i];
                $includedPositions[] = $i;
            }

            for ($j = $i + 1; $j < count($orders); $j++) {
                if (!in_array($j, $includedPositions)) {
                    if ($orders[$i]->order->product_id == $orders[$j]->order->product_id
                        && $orders[$i]->order->view_price_one == $orders[$j]->order->view_price_one
                        && trim($orders[$i]->order->product_title) == trim($orders[$j]->order->product_title)) {

                        $mergedOrders[$i]->quantity += $orders[$j]->quantity;
                        $includedPositions[] = $j;
                    }
                }
            }
        }

        return $mergedOrders;
    }
}
