<?php

namespace common\models\document;

use Yii;
use common\models\Contractor;

/**
 * This is the model class for table "agent_report_order".
 *
 * @property integer $id
 * @property integer $agent_report_id
 * @property integer $contractor_id
 * @property integer $payments_sum
 * @property string $agent_percent
 * @property integer $agent_sum
 * @property integer $is_exclude
 *
 * @property AgentReport $agentReport
 * @property Contractor $contractor
 */
class AgentReportOrder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'agent_report_order';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['agent_report_id', 'contractor_id'], 'required'],
            [['agent_report_id', 'contractor_id', 'payments_sum', 'agent_sum', 'is_exclude'], 'integer'],
            [['agent_percent'], 'number', 'min' => 0, 'max' => 100],
            [['agent_report_id'], 'exist', 'skipOnError' => true, 'targetClass' => AgentReport::className(), 'targetAttribute' => ['agent_report_id' => 'id']],
            [['contractor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contractor::className(), 'targetAttribute' => ['contractor_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'agent_report_id' => 'Agent Report ID',
            'contractor_id' => 'Contractor ID',
            'payments_sum' => 'Сумма оплат за месяц',
            'agent_percent' => 'Комиссионное вознаграждение',
            'agent_sum' => 'Комиссионное вознаграждение %',
            'is_exclude' => 'Не включать в отчет',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgentReport()
    {
        return $this->hasOne(AgentReport::className(), ['id' => 'agent_report_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContractor()
    {
        return $this->hasOne(Contractor::className(), ['id' => 'contractor_id']);
    }
}
