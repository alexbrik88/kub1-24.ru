<?php

namespace common\models\document;

use common\models\Company;
use Yii;

/**
 * This is the model class for table "autoact".
 *
 * @property integer $company_id
 * @property integer $rule
 * @property integer $status
 * @property integer $send_together
 * @property integer $send_auto
 * @property integer $no_pay_partial
 * @property integer $no_pay_order
 * @property integer $no_pay_emoney
 *
 * @property Company $company
 */
class Autoact extends \yii\db\ActiveRecord
{
    const MANUAL = 1;
    const BY_INVOICE = 2;
    const BY_PAYMENT = 3;
    const LAST_WORK_DAY = 4;
    const LAST_MONTH_DAY = 5;
    const LAST_WORK_DAY_2 = 6;
    const LAST_MONTH_DAY_2 = 7;

    const STATUS_PAY = 1;
    const STATUS_ALL = 2;

    public static $rules = [
        self::MANUAL,
        self::BY_INVOICE,
        self::BY_PAYMENT,
        self::LAST_WORK_DAY,
        self::LAST_MONTH_DAY,
        self::LAST_WORK_DAY_2,
        self::LAST_MONTH_DAY_2,
    ];

    public static $ruleLabels = [
        self::MANUAL => 'Вручную',
        self::BY_INVOICE => 'Дата Акта = Дата Счета',
        self::BY_PAYMENT => 'Дата Акта = Дате Оплаты',
        self::LAST_WORK_DAY => 'Дата Акта = Последний РАБОЧИЙ день месяца',
        self::LAST_MONTH_DAY => 'Дата Акта = Последний день месяца',
        self::LAST_WORK_DAY_2 => 'Дата Акта = Последний РАБОЧИЙ день месяца',
        self::LAST_MONTH_DAY_2 => 'Дата Акта = Последний день месяца',
    ];

    public static $ruleComments = [
        self::MANUAL => null,
        self::BY_INVOICE => 'Акт создается той же датой, что и Счет. Вручную можно изменить дату Акта на дату отличную от даты счета',
        self::BY_PAYMENT => 'Выставление Акта к счету после оплаты',
        self::LAST_WORK_DAY => 'Выставление Актов в последний РАБОЧИЙ день месяца <span class="underline">к счетам, выставленным в течение ЭТОГО месяца</span>',
        self::LAST_MONTH_DAY => 'Выставление Актов в последний день месяца <span class="underline">к счетам, выставленным в течение ЭТОГО месяца</span>',
        self::LAST_WORK_DAY_2 => 'Выставление Актов в последний РАБОЧИЙ день месяца <span class="underline">к счетам, выставленным в течение ПРЕДЫДУЩЕГО месяца</span>',
        self::LAST_MONTH_DAY_2 => 'Выставление Актов в последний день месяца <span class="underline">к счетам, выставленным в течение ПРЕДЫДУЩЕГО месяца</span>',
    ];

    public static $statusArray = [
        self::STATUS_PAY => 'Выставлять только по оплаченным счетам',
        self::STATUS_ALL => 'Выставлять по всем счетам за месяц',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'autoact';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $status1 = self::LAST_WORK_DAY;
        $status2 = self::LAST_MONTH_DAY;
        return [
            [
                ['rule'], 'required',
                'message' => 'Необходимо выбрать правило.',
            ],
            [
                ['rule'], 'in',
                'range' => self::$rules,
            ],
            [
                ['status'], 'required',
                'when' => function ($model) {
                    return in_array($model->rule, [
                        self::LAST_WORK_DAY,
                        self::LAST_MONTH_DAY,
                        self::LAST_WORK_DAY_2,
                        self::LAST_MONTH_DAY_2,
                    ]);
                },
                'message' => 'Необходимо выбрать, по каким счетам выставлять Акт.',
            ],
            [
                ['status'], 'in', 'range' => array_keys(self::$statusArray),
                'when' => function ($model) {
                    return in_array($model->rule, [
                        self::LAST_WORK_DAY,
                        self::LAST_MONTH_DAY,
                        self::LAST_WORK_DAY_2,
                        self::LAST_MONTH_DAY_2,
                    ]);
                },
            ],
            [['send_together', 'send_auto'], 'boolean'],
            [
                ['no_pay_partial', 'no_pay_order', 'no_pay_emoney'], 'boolean',
                'when' => function ($model) {
                    return $model->rule == self::BY_PAYMENT || $model->status == self::STATUS_PAY;
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'company_id' => 'Company ID',
            'rule' => 'Правило выставления',
            'status' => 'Выстчвлять по всем или только оплаченным',
            'send_together' => 'Отправлять клиенту Счет и Акт в одном письме',
            'send_auto' => 'Автоматическая отправка Акта клиенту, после выставления',
            'no_pay_partial' => 'К частично оплаченным счетам акты не выставлять',
            'no_pay_order' => 'Акты не выставлять к счетам, оплаченным по кассе',
            'no_pay_emoney' => 'Акты не выставлять к счетам, оплаченным по e-money',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if (!$insert && $changedAttributes) {
            \common\models\company\CompanyFirstEvent::checkEvent($this->company, 53);
        }
    }
}
