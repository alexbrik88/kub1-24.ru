<?php

namespace common\models\document;

use Yii;

/**
 * This is the model class for table "invoice_act".
 *
 * @property integer $invoice_id
 * @property integer $act_id
 *
 * @property Act $act
 * @property Invoice $invoice
 */
class InvoiceAct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invoice_act';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['invoice_id', 'act_id'], 'required'],
            [['invoice_id', 'act_id'], 'integer'],
            [['act_id'], 'exist', 'skipOnError' => true, 'targetClass' => Act::className(), 'targetAttribute' => ['act_id' => 'id']],
            [['invoice_id'], 'exist', 'skipOnError' => true, 'targetClass' => Invoice::className(), 'targetAttribute' => ['invoice_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'invoice_id' => 'Invoice ID',
            'act_id' => 'Act ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAct()
    {
        return $this->hasOne(Act::className(), ['id' => 'act_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(Invoice::className(), ['id' => 'invoice_id']);
    }
}
