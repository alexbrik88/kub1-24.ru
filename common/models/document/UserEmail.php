<?php

namespace common\models\document;

use common\models\Contractor;
use Yii;
use common\models\Company;

/**
 * This is the model class for table "user_email".
 *
 * @property integer $id
 * @property integer $company_id
 * @property string $fio
 * @property string $email
 * @property integer $contractor_id
 *
 * @property Company $company
 * @property Contractor $contractor
 */
class UserEmail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user_email';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'email', 'contractor_id'], 'required'],
            [['company_id', 'contractor_id'], 'integer'],
            [['email'], 'email'],
            [['fio', 'email'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['contractor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contractor::className(), 'targetAttribute' => ['contractor_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'fio' => 'Fio',
            'email' => 'Email',
            'contractor_id' => 'Contractor ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContractor()
    {
        return $this->hasOne(Contractor::className(), ['id' => 'contractor_id']);
    }
}
