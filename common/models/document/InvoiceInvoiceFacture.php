<?php

namespace common\models\document;

use Yii;

/**
 * This is the model class for table "invoice_invoice_facture".
 *
 * @property integer $invoice_id
 * @property integer $invoice_facture_id
 *
 * @property Invoice $invoice
 * @property InvoiceFacture $invoiceFacture
 */
class InvoiceInvoiceFacture extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invoice_invoice_facture';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['invoice_id', 'invoice_facture_id'], 'required'],
            [['invoice_id', 'invoice_facture_id'], 'integer'],
            [['invoice_id'], 'exist', 'skipOnError' => true, 'targetClass' => Invoice::className(), 'targetAttribute' => ['invoice_id' => 'id']],
            [['invoice_facture_id'], 'exist', 'skipOnError' => true, 'targetClass' => InvoiceFacture::className(), 'targetAttribute' => ['invoice_facture_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'invoice_id' => 'Invoice ID',
            'invoice_facture_id' => 'Invoice Facture ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(Invoice::className(), ['id' => 'invoice_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceFacture()
    {
        return $this->hasOne(InvoiceFacture::className(), ['id' => 'invoice_facture_id']);
    }
}
