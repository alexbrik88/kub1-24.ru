<?php

namespace common\models\document;

use common\components\calculator\CalculatorHelper;
use common\models\product\Product;
use frontend\models\Documents;
use Yii;

/**
 * This is the model class for table "order_waybill".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $waybill_id
 * @property integer $product_id
 * @property integer $quantity
 * @property int     $with_packing_list
 * @property int     $with_invoice
 * @property int     $with_invoice_facture
 * @property string  $consignment_packaging_type
 * @property string  $consignment_places_count
 * @property int     $weight_determining_method
 * @property string  $consignment_code
 * @property string  $consignment_container_number
 * @property string  $consignment_classification
 * @property float   $consignment_gross_weight
 *
 * @property Order $order
 * @property Waybill $waybill
 * @property Product $product
 */
class OrderWaybill extends AbstractOrder
{
    public static $WEIGHT_DETERMINING_METHOD = [
        0 => '---',
        1 => 'взвешиванием',
        2 => 'по стандарту мест',
        3 => 'расчетным путем'
    ];

    public static $SUM_TYPE = [
        '2' => 'sales',
        '1' => 'purchase',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_waybill';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['with_packing_list', 'with_invoice', 'with_invoice_facture', 'weight_determining_method'], 'integer'],
            [['consignment_packaging_type', 'consignment_places_count',
                'consignment_code', 'consignment_container_number', 'consignment_classification', 'consignment_gross_weight'], 'string'],
            [['weight_determining_method'], 'in', 'range' => array_keys(self::$WEIGHT_DETERMINING_METHOD)],
            [['quantity'], 'filter', 'filter' => function ($value) {
                return str_replace(",", ".", $value);
            },],
            [['quantity'], 'number', 'numberPattern' => '/^\d+(\.\d*)?$/'],
            [['quantity'], function ($attribute, $params) {
                if ($this->hasErrors($attribute)) return;
                $title = 'для «' . $this->order->product_title . '»';
                $max = self::availableQuantity($this->order, $this->waybill_id);
                if ($this->$attribute <= 0) {
                    $this->addError($attribute, "Количество {$title} должно быть больше 0.");
                } elseif (bccomp($this->$attribute, $max, 4) > 0) {
                    $this->addError($attribute, "Максимальное возможное количество {$title}: {$max}.");
                }
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'waybill_id' => 'Waybill ID',
            'product_id' => 'Product ID',
            'quantity' => 'Количество',
            'with_packing_list' => 'Товарная накладная',
            'with_invoice' => 'Счет',
            'with_invoice_facture' => 'Счет-фактура',
            'consignment_packaging_type' => 'Вид упаковки',
            'consignment_places_count' => 'Количество мест',
            'weight_determining_method' => 'Способ определения массы',
            'consignment_code' => 'Код груза',
            'consignment_container_number' => 'Номер контейнера',
            'consignment_classification' => 'Класс груза',
            'consignment_gross_weight' => 'Масса брутто, т',
        ];
    }

    /**
     * @param $waybill_id
     * @param Order $order
     * @return OrderWaybill
     */
    public static function createFromOrder($waybill_id, Order $order)
    {
        if (OrderWaybill::compareWithOrderByQuantity($order) != 0 &&
            $order->product->production_type == Product::PRODUCTION_TYPE_GOODS
        ) {
            $order_waybill = new OrderWaybill();
            $order_waybill->order_id = $order->id;
            $order_waybill->waybill_id = $waybill_id;
            $order_waybill->product_id = $order->product_id;
            $order_waybill->quantity = OrderWaybill::compareWithOrderByQuantity($order);

            return $order_waybill;
        }

        return null;
    }

    /**
     * @param $pacingListId
     * @return mixed
     */
    public static function countByWaybill($pacingListId)
    {
        return count(OrderWaybill::findAll(['waybill_id' => $pacingListId]));
    }

    /**
     * @param $order_id
     * @param $waybill_id
     * @param $quantity
     */
    public static function setQuantity($order_id, $waybill_id, $quantity)
    {
        $order_waybill = OrderWaybill::findOne(['order_id' => $order_id, 'waybill_id' => $waybill_id]);
        $order = Order::findOne(['id' => $order_id]);
        if ($order_waybill != null) {
            $order_waybill->quantity = $quantity;
            $order_waybill->save();
        }
    }

    /**
     * @param $order_id
     * @param $waybill_id
     * @return bool
     */
    public static function ifExist($order_id, $waybill_id)
    {
        return OrderWaybill::findOne(['order_id' => $order_id, 'waybill_id' => $waybill_id]) ? true : false;
    }

    /**
     * returns residual of order and sum of lists, if haven't returns null
     * @param Order $order
     * @return int
     */
    public static function compareWithOrderByQuantity(Order $order)
    {
        $query = OrderWaybill::find()->where(['order_id' => $order->id]);
        if ($query != null) {
            $waybills_quantity = $query->sum('quantity');
        } else {
            $waybills_quantity = 0;
        }
        if ($waybills_quantity == $order->quantity) {

            return 0;
        } elseif ($waybills_quantity < $order->quantity) {

            return $order->quantity - $waybills_quantity;
        } else {

            return null;
        }
    }

    /**
     * @param Invoice $invoice
     * @return int|mixed
     */
    public static function compareWithInvoiceBySum(Invoice $invoice)
    {
        $opl = 0;
        $ordersSum = 0;
        if ($invoice->orders != null) {
            foreach ($invoice->orders as $order) {
                if ($order->product->production_type == Product::PRODUCTION_TYPE_GOODS) {
                    $ordersSum += $order->quantity;
                    $opl += OrderWaybill::find()->where(['order_id' => $order->id])->sum('quantity');
                }
            }
        }
        if ($ordersSum > $opl) {
            return $ordersSum - $opl;
        } elseif ($ordersSum == $opl) {
            return 0;
        } else {
            return 0;
        }
    }

    /**
     * @param $waybill_id
     * @return mixed
     */
    public static function allQuantitySum($waybill_id)
    {
        return OrderWaybill::find()->where(['waybill_id' => $waybill_id])->sum('quantity');
    }

    public static function allWaybillSum($waybill_id, $sum_type)
    {
        return OrderWaybill::find()->where(['waybill_id' => $waybill_id])->sum('amount_' . $sum_type . '_with_vat');
    }

    /**
     * @param $waybill_id
     * @return array
     */
    public static function getAvailable($waybill_id)
    {
        $waybill = Waybill::findOne(['id' => $waybill_id]);
        $invoice = Invoice::findOne(['id' => $waybill->invoice_id]);
        $available = [];
        if ($invoice->orders != null) {
            foreach ($invoice->orders as $order) {
                if ($order->product->production_type == Product::PRODUCTION_TYPE_GOODS) {
                    $order_waybill = OrderWaybill::findOne(['order_id' => $order->id, 'waybill_id' => $waybill_id]);
                    if ($order_waybill == null && OrderWaybill::compareWithOrderByQuantity($order) != 0) {
                        $available[$order->product_title] = OrderWaybill::createFromOrder($waybill_id, $order)->quantity;
                    }
                }
            }
        }

        return $available;
    }

    /**
     * @param Order $order
     * @param integer $waybill_id
     * @return numder
     */
    public static function availableQuantity(Order $order, $waybill_id = null)
    {
        $used_quantity = self::find()
            ->andWhere(['order_id' => $order->id])
            ->andWhere(['not', ['waybill_id' => $waybill_id]])
            ->sum('quantity');

        $available = $order->quantity - $used_quantity;

        return max(0, $available);
    }

    /**
     * @return int
     */
    public function getAvailableQuantity()
    {
        $order_quantity = $this->order->quantity;

        $waybills_quantity = OrderWaybill::find()->where(['order_id' => $this->order_id])->andWhere(['!=','id',$this->id])->sum('quantity');

        $result = $order_quantity - $waybills_quantity;

        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getWaybill()
    {
        return $this->hasOne(Waybill::className(), ['id' => 'waybill_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocument()
    {
        return $this->getWaybill();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                if ($this->waybill->type == Documents::IO_TYPE_IN) {
                    $this->product->setCountOrderBuy($this->quantity);
                } else {
                    $this->product->setCountOrderSale($this->quantity);
                    $this->order->updateAttributes([
                        'reserve' => max(0, $this->order->reserve - $this->quantity),
                    ]);
                }
            } else {
                $quantityDiff = $this->quantity - $this->getOldAttribute('quantity');
                if ($quantityDiff !== 0) {
                    if ($this->waybill->type == Documents::IO_TYPE_IN) {
                        $this->product->setCountOrderBuy($this->quantity - $this->getOldAttribute('quantity'));
                    } else {
                        $this->product->setCountOrderSale($this->quantity - $this->getOldAttribute('quantity'));
                        $this->order->updateAttributes([
                            'reserve' => max(0, $this->order->reserve + $this->getOldAttribute('quantity') - $this->quantity),
                        ]);
                    }
                }
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            if ($this->waybill->status_out_id != status\WaybillStatus::STATUS_REJECTED) {
                $this->resetProductCount();
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     *
     */
    public function resetProductCount()
    {
        if ($this->waybill->type == Documents::IO_TYPE_IN) {
            $this->product->setCountOrderSale($this->quantity);
        } else {
            $this->product->setCountOrderBuy($this->quantity);
            $this->order->updateAttributes([
                'reserve' => $this->order->reserve + $this->quantity,
            ]);
        }
    }
}
