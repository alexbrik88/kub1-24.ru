<?php

namespace common\models\document;

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\Company;
use common\models\Contractor;
use common\models\currency\Currency;
use common\models\employee\Employee;
use common\models\file\File;
use frontend\models\Documents;
use frontend\models\log\ILogMessage;
use frontend\models\log\Log;
use frontend\models\log\LogEvent;
use php_rutils\RUtils;
use DateTime;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\bootstrap\Html;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "autoinvoice".
 *
 * @property integer $id
 * @property integer $period
 * @property integer $month
 * @property integer $day
 * @property integer $date_from
 * @property integer $date_to
 * @property integer $add_month_and_year
 * @property integer $type
 * @property integer $company_id
 * @property integer $document_author_id
 * @property string $production_type
 * @property integer $contractor_id
 * @property integer $invoice_expenditure_item_id
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $document_number
 * @property string $all_sum
 * @property integer $last_invoice_date
 * @property integer $next_invoice_date
 * @property integer $status
 * @property integer $payment_delay
 * @property integer $date_stop
 * @property integer $send_to_director
 * @property integer $send_to_chief_accountant
 * @property integer $send_to_contact
 *
 * @property Company $company
 * @property Contractor $contractor
 * @property InvoiceExpenditureItem $invoiceExpenditureItem
 * @property Autoorder[] $autoorders
 */
class Autoinvoice extends \yii\db\ActiveRecord implements ILogMessage
{
    /**
     * period
     */
    const MONTHLY = 1;
    const QUARTERLY = 2;
    const HALF_YEAR = 3;
    const ANNUALLY = 4;

    /**
     * status
     */
    const DELETED = 0;
    const ACTIVE = 1;
    const CANCELED = 2;

    /**
     * add to title
     */
    const ADD_NO = 0;
    const ADD_CURRENT = 1;
    const ADD_NEXT = 2;
    const ADD_PREV = 3;

    /**
     * @var array
     */
    public static $STATUS = [
        self::DELETED => 'Удален',
        self::ACTIVE => 'Активный',
        self::CANCELED => 'Остановлен',
    ];

    /**
     * @var array
     */
    public static $PERIOD = [
        self::MONTHLY => 'Каждый месяц',
        self::QUARTERLY => 'Каждый квартал',
        self::HALF_YEAR => 'Каждое полугодие',
        self::ANNUALLY => 'Каждый год',
    ];

    /**
     * @var array
     */
    public static $addToTitle1 = [
        self::ADD_NO => '---',
        self::ADD_CURRENT => 'Месяц и год',
        self::ADD_NEXT => 'Следующий месяц и год',
        self::ADD_PREV => 'Предыдущий месяц и год',
    ];

    /**
     * @var array
     */
    public static $addToTitle2 = [
        self::ADD_NO => '---',
        self::ADD_CURRENT => 'Квартал и год',
        self::ADD_NEXT => 'Следующий квартал и год',
        self::ADD_PREV => 'Предыдущий квартал и год',
    ];

    /**
     * @var array
     */
    public static $addToTitle3 = [
        self::ADD_NO => '---',
        self::ADD_CURRENT => 'Полугодие и год',
        self::ADD_NEXT => 'Следующее полугодие и год',
        self::ADD_PREV => 'Предыдущее полугодие и год',
    ];

    /**
     * @var array
     */
    public static $addToTitle4 = [
        self::ADD_NO => '---',
        self::ADD_CURRENT => 'Год',
        self::ADD_NEXT => 'Следующий год',
        self::ADD_PREV => 'Предыдущий год',
    ];

    /**
     * @var array
     */
    public static $DAY = ['1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5',
        '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11', '12' => '12',
        '13' => '13', '14' => '14', '15' => '15', '16' => '16', '17' => '17', '18' => '18',
        '19' => '19', '20' => '20', '21' => '21', '22' => '22', '23' => '23', '24' => '24',
        '25' => '25', '26' => '26', '27' => '27', '28' => '28', '29' => '29', '30' => '30', '31' => '31'];

    public $printablePrefix = 'Шаблон';

    /**
     * @var string
     */
    public static $uploadDirectory = 'autoinvoice';

    public $type = Documents::IO_TYPE_OUT;

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            'documentCreatedAtBehavior' => [
                'class' => TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return 'autoinvoice';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['period', 'day', 'document_number', 'payment_delay', 'dateFrom', 'dateTo'], 'required'],
            [['period', 'day', 'document_number', 'month', 'status', 'payment_delay'], 'integer'],
            [['month'], 'required', 'when' => function ($model) {
                return in_array($model->period, [
                    self::QUARTERLY,
                    self::HALF_YEAR,
                    self::ANNUALLY,
                ]);
            }],
            [
                ['month'], 'in',
                'range' => self::quarterMonths(),
                'when' => function ($model) {
                    return $model->period == self::QUARTERLY;
                },
            ],
            [
                ['month'], 'in',
                'range' => self::halfyearMonths(),
                'when' => function ($model) {
                    return $model->period == self::HALF_YEAR;
                },
            ],
            [
                ['month'], 'in',
                'range' => self::annuallyMonths(),
                'when' => function ($model) {
                    return $model->period == self::ANNUALLY;
                },
            ],
            [
                ['document_number'], 'unique', 'targetAttribute' => ['document_number', 'company_id'],
                'filter' => ['not', ['status' => self::DELETED]],
                'message' => 'Шаблон с таким номером уже существует, одинаковые номера не допустимы',
            ],
            [['dateFrom', 'dateTo'], 'date', 'format' => 'php:d.m.Y'],
            [['period'], 'in', 'range' => [self::MONTHLY, self::QUARTERLY, self::HALF_YEAR, self::ANNUALLY]],
            [['add_month_and_year'], 'in', 'range' => [
                self::ADD_NO,
                self::ADD_CURRENT,
                self::ADD_NEXT,
                self::ADD_PREV,
            ]],
            [['dateTo'], function ($attribute, $params) {
                $dateFrom = DateTime::createFromFormat('d.m.Y', $this->dateFrom);
                $dateTo = DateTime::createFromFormat('d.m.Y', $this->$attribute);
                if ($dateFrom && $dateTo && $dateTo < $dateFrom) {
                    $this->addError($attribute, 'Значение «Окончание» не может быть меньше значения «Начало».');
                }
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'period' => 'Периодичность',
            'day' => 'Число',
            'date_from' => 'Начало',
            'dateFrom' => 'Начало',
            'date_to' => 'Окончание',
            'dateTo' => 'Окончание',
            'add_month_and_year' => 'В названии услуг, автоматически добавлять',
            'type' => 'Type',
            'company_id' => 'Компания',
            'document_author_id' => 'Автор',
            'production_type' => 'Production Type',
            'contractor_id' => 'Покупатель',
            'invoice_expenditure_item_id' => 'Invoice Expenditure Item ID',
            'created_at' => 'Created At',
            'document_number' => 'Номер документа',
            'last_invoice_date' => 'Последний счет',
            'next_invoice_date' => 'Следующий счет',
            'status' => 'Статус',
            'payment_delay' => 'Отсрочка оплаты',
            'date_stop' => 'Date Stop',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContractor()
    {
        return $this->hasOne(Contractor::className(), ['id' => 'contractor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceExpenditureItem()
    {
        return $this->hasOne(InvoiceExpenditureItem::className(), ['id' => 'invoice_expenditure_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuthor()
    {
        return $this->hasOne(Employee::className(), ['id' => 'document_author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutoorders()
    {
        return $this->hasMany(Autoorder::className(), ['autoinvoice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoices()
    {
        return $this->hasMany(Invoice::className(), ['autoinvoice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(InvoiceAuto::className(), ['id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function getFile()
    {
        return $this->hasOne(File::className(), ['owner_id' => 'id'])
            ->onCondition(['owner_model' => Autoinvoice::className(),]);
    }

    /**
     * @return int
     */
    public function getFullNumber()
    {
        return $this->document_number;
    }

    /**
     * @param  Autoinvoice $model
     * @param  DateTime|string $dateFrom format d.m.Y|Y-m-d
     * @return string
     */
    public function setNextDate($dateFrom = null, $save = false)
    {
        if ($dateFrom instanceof \DateTime) {
            $date = clone $dateFrom;
            $date->modify('today');
        } else {
            $date = $dateFrom ? (
                date_create_from_format('d.m.Y|', $dateFrom) ? : date_create_from_format('Y-m-d|', $dateFrom)
            ) : false;
        }

        if (!$date) {
            $date = date_create('today');
        }
        $day = $this->day;
        $result = null;

        switch ($this->period) {
            case self::MONTHLY:
                $Y = $date->format('Y');
                $m = $date->format('n');
                $d = min($date->format('t'), $day);
                $nextDate = date_create_from_format('Y-n-j|', "{$Y}-{$m}-{$d}");

                if ($nextDate < $date || $nextDate->format('Y-m-d') == $this->last_invoice_date) {
                    $nextDate->modify('+1 month');
                    $Y = $nextDate->format('Y');
                    $m = $nextDate->format('n');
                    $d = min($nextDate->format('t'), $day);
                    $nextDate = date_create_from_format('Y-n-j|', "{$Y}-{$m}-{$d}");
                }
                $result = $nextDate->format('Y-m-d');
                break;

            case self::QUARTERLY:
                $Y = (int) $date->format('Y');
                $month = $this->month ? : 1;
                $period = ceil($date->format('n') / 3);
                $m = ($period - 1) * 3 + $month;
                $d = min(cal_days_in_month(CAL_GREGORIAN, $m, $Y), $day);
                $nextDate = date_create_from_format('Y-n-j|', "{$Y}-{$m}-{$d}");

                if ($nextDate < $date || $nextDate->format('Y-m-d') == $this->last_invoice_date) {
                    $nextDate->modify('first day of +3 month');
                    $Y = $nextDate->format('Y');
                    $m = $nextDate->format('n');
                    $d = min($nextDate->format('t'), $day);
                    $nextDate = date_create_from_format('Y-n-j|', "{$Y}-{$m}-{$d}");
                }
                $result = $nextDate->format('Y-m-d');
                break;

            case self::HALF_YEAR:
                $Y = (int) $date->format('Y');
                $month = $this->month ? : 1;
                $period = ceil($date->format('n') / 6);
                $m = ($period - 1) * 6 + $month;
                $d = min(cal_days_in_month(CAL_GREGORIAN, $m, $Y), $day);
                $nextDate = date_create_from_format('Y-n-j|', "{$Y}-{$m}-{$d}");

                if ($nextDate < $date || $nextDate->format('Y-m-d') == $this->last_invoice_date) {
                    $nextDate->modify('first day of +6 month');
                    $Y = $nextDate->format('Y');
                    $m = $nextDate->format('n');
                    $d = min($nextDate->format('t'), $day);
                    $nextDate = date_create_from_format('Y-n-j|', "{$Y}-{$m}-{$d}");
                }
                $result = $nextDate->format('Y-m-d');
                break;

            case self::ANNUALLY:
                $Y = (int) $date->format('Y');
                $m = $this->month ? : 1;
                $d = min(cal_days_in_month(CAL_GREGORIAN, $m, $Y), $day);
                $nextDate = date_create_from_format('Y-n-j|', "{$Y}-{$m}-{$d}");

                if ($nextDate < $date || $nextDate->format('Y-m-d') == $this->last_invoice_date) {
                    $nextDate->modify('first day of +1 year');
                    $Y = $nextDate->format('Y');
                    $m = $nextDate->format('n');
                    $d = min($nextDate->format('t'), $day);
                    $nextDate = date_create_from_format('Y-n-j|', "{$Y}-{$m}-{$d}");
                }
                $result = $nextDate->format('Y-m-d');
                break;
        }
        if ($save) {
            $this->updateAttributes([
                'next_invoice_date' => $result,
            ]);
        }
        $this->next_invoice_date = $result;

        return $result;
    }

    /**
     * @inheritdoc
     */
    public function setNextNumber($company_id = null)
    {
        $this->document_number = self::find()->select("IFNULL(MAX({{autoinvoice}}.[[document_number]]), 0) + 1 AS [[doc_number]]")
            ->joinWith('invoice')
            ->andWhere(['not', ['autoinvoice.status' => self::DELETED]])
            ->andWhere([
                'invoice.is_deleted' => false,
                'invoice.company_id' => $company_id,
            ])->scalar();
    }

    /**
     * @param Log $log
     * @return string
     */
    public function getLogMessage(Log $log)
    {
        $text = 'Шаблон №' . join(' ', array_filter([
                $log->getModelAttributeNew('document_number'),
            ]));

        $link = Html::a($text, [
            '/documents/autoinvoice/view',
            'id' => $log->getModelAttributeNew('id'),
        ]);

        $currencyName = $log->getModelAttributeNew('currency_name');
        $currency = $currencyName == Currency::DEFAULT_NAME ? 'руб' : $currencyName;

        $amount = TextHelper::invoiceMoneyFormat($log->getModelAttributeNew('view_total_with_nds'), 2) . " {$currency}.";
        $contractor = Contractor::findOne($log->getModelAttributeNew('contractor_id'));
        $contractorName = $contractor ? ', ' . $contractor->getTitle(true) : '';
        $invoiceText = ', ' . $amount . $contractorName . ',';

        switch ($log->log_event_id) {
            case LogEvent::LOG_EVENT_CREATE:
                return $link . $invoiceText . ' был создан.';
            case LogEvent::LOG_EVENT_DELETE:
                return $text . $invoiceText . ' был удалён.';
            case LogEvent::LOG_EVENT_UPDATE:
                return $link . $invoiceText . ' был изменен.';
            case LogEvent::LOG_EVENT_UPDATE_STATUS:
                $status = static::$STATUS[$log->getModelAttributeNew('status')];

                return $status !== null
                    ? $link . $invoiceText . ' статус "' . $status . '"'
                    : '';
            default:
                return $log->message;
        }
    }

    /**
     * @return bool
     */
    public function beforeSave($insert)
    {
        $this->checkOverdue();

        $dateFrom = max(\DateTime::createFromFormat('d.m.Y', $this->dateFrom), date_create());
        $dateLast = \DateTime::createFromFormat('Y-m-d', $this->last_invoice_date);
        $dateNextFrom = $dateLast ? max($dateLast, $dateFrom) : $dateFrom;

        $this->setNextDate($dateNextFrom);

        return parent::beforeSave($insert);
    }

    /**
     * @param string $value
     */
    public function setDateFrom($value)
    {
        if ($date = \DateTime::createFromFormat('d.m.Y', $value)) {
            $this->date_from = $date->format('Y-m-d');
        } else {
            $this->date_from = null;
        }
    }

    /**
     * @param string $value
     */
    public function setDateTo($value)
    {
        if ($date = \DateTime::createFromFormat('d.m.Y', $value)) {
            $this->date_to = $date->format('Y-m-d');
        } else {
            $this->date_to = null;
        }
    }

    /**
     * @return string
     */
    public function getDateFrom()
    {
        return $this->date_from ? \DateTime::createFromFormat('Y-m-d', $this->date_from)->format('d.m.Y') : null;
    }

    /**
     * @return string
     */
    public function getDateTo()
    {
        return $this->date_to ? \DateTime::createFromFormat('Y-m-d', $this->date_to)->format('d.m.Y') : null;
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();

        $this->checkOverdue();
    }

    /**
     * @inheritdoc
     */
    public function getStatusName()
    {
        return isset(static::$STATUS[$this->status]) ? static::$STATUS[$this->status] : null;
    }

    /**
     *
     */
    public function checkOverdue()
    {
        if ($this->status == self::ACTIVE && date_create($this->date_to) < date_create(date('Y-m-d'))) {
            $this->status = Autoinvoice::CANCELED;
            $this->save(false, ['status']);
        }
    }

    /**
     * @inheritdoc
     */
    public function getAddToTitle()
    {
        $varName = 'addToTitle' . $this->period;

        return isset(static::$$varName[$this->add_month_and_year]) ?
            static::$$varName[$this->add_month_and_year] : '';
    }

    /**
     * @inheritdoc
     */
    public static function monthDays()
    {
        $days = range(1, 31);

        return array_combine($days, $days);
    }

    /**
     * @inheritdoc
     */
    public static function quarterDays()
    {
        $days = range(1, 92);

        return array_combine($days, $days);
    }

    /**
     * @inheritdoc
     */
    public static function quarterMonths()
    {
        $range = range(1, 3);

        return array_combine($range, $range);
    }

    /**
     * @inheritdoc
     */
    public static function halfyearMonths()
    {
        $range = range(1, 6);

        return array_combine($range, $range);
    }

    /**
     * @inheritdoc
     */
    public static function annuallyMonths()
    {
        $range = range(1, 12);

        return array_combine($range, $range);
    }

    /**
     * @inheritdoc
     */
    public function getLogIcon(Log $log)
    {
        return Documents::getLogIcon($log);
    }

    /**
     * @inheritdoc
     */
    public function getPeriodText()
    {
        $period = Autoinvoice::$PERIOD[$this->period] . ' ';

        switch ($this->period) {
            case self::MONTHLY:
                $period .= "{$this->day} день";
                break;

            case self::QUARTERLY:
            case self::HALF_YEAR:
            case self::ANNUALLY:
                $period .= "{$this->month} месяц {$this->day} день";
                break;
        }

        return $period;
    }
}
