<?php

namespace common\models\document;

trait BasisDocumentTrait
{
    /**
     * @inheritDoc
     */
    public function getBasisDocumentDate(): string
    {
        return (string) $this->getAttribute('basis_document_date');
    }

    /**
     * @inheritDoc
     */
    public function getBasisDocumentName(): string
    {
        if ($this->hasAttribute('basis_name')) {
            return (string) $this->getAttribute('basis_name');
        }

        return (string) $this->getAttribute('basis_document_name');
    }

    /**
     * @inheritDoc
     */
    public function getBasisDocumentNumber(): string
    {
        return (string) $this->getAttribute('basis_document_number');
    }

    /**
     * @inheritDoc
     */
    public function hasBasisDocumentParams(): bool
    {
        return ($this->getBasisDocumentDate() && $this->getBasisDocumentName() && $this->getBasisDocumentNumber());
    }
}
