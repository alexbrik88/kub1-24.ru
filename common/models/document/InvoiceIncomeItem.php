<?php

namespace common\models\document;

use Yii;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashOrderFlows;
use common\models\Company;
use common\models\Contractor;
use common\models\IncomeItemFlowOfFunds;
use frontend\modules\reports\models\PlanCashFlows;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * This is the model class for table "invoice_income_item".
 *
 * @property int $id
 * @property int $parent_id
 * @property string $name
 * @property int $company_id
 * @property int $sort
 * @property bool $is_visible
 * @property bool $is_prepayment
 * @property bool $can_be_controlled
 *
 * @property IncomeItemFlowOfFunds[] $incomeItemsFlowOfFunds
 * @property InvoiceIncomeItem $parent
 * @property InvoiceIncomeItem[] $children
 * @property string $fullName
 */
class InvoiceIncomeItem extends ActiveRecord implements InvoiceReasonInterface
{
    const NAME_MAX_LENGTH = 45;

    const ITEM_PAYMENT_FROM_BUYER = 1;
    const ITEM_CASH_PAYMENT = 2;
    const ITEM_RETURN = 3;
    const ITEM_LOAN = 4;
    const ITEM_CREDIT = 5;
    const ITEM_FROM_FOUNDER = 6;
    const ITEM_OTHER = 7;
    const ITEM_COMMISSION = 8;
    const ITEM_OWN_FOUNDS = 9;
    const ITEM_BORROWING_REDEMPTION = 10;
    const ITEM_RECEIVED_PERCENTS = 11;
    const ITEM_CARD_REPLENISHMENT = 12;
    const ITEM_BUDGET_RETURN = 13;
    const ITEM_ENSURE_PAYMENT = 14;
    const ITEM_STARTING_BALANCE = 15;
    const ITEM_TRADE_RECEIPTS = 16;
    const ITEM_TRADE_ACQUIRING = 17;
    const ITEM_AGENT_FEE = 18;
    const ITEM_BORROWING_DEPOSIT = 19;
    const ITEM_SALE_FIEXD_ASSETS = 20;
    const ITEM_RECEIVED_PERCENTS_BY_DEPOSITS = 21;
    const ITEM_INPUT_MONEY = 22;
    const ITEM_CURRENCY_CONVERSION = 23;
    const ITEM_PRODUCT = 24;

    public static $incomesPattern = [
        '/внесени(е|я) через кассу/ui' => InvoiceIncomeItem::ITEM_CASH_PAYMENT,
        '/пополнение счета/ui' => InvoiceIncomeItem::ITEM_CASH_PAYMENT,
        '/(кредит|овердрафт)/ui' => InvoiceIncomeItem::ITEM_CREDIT,
        '/(возврат|погашение).+\bза(е|ё|й)м(а|у)?/ui' => InvoiceIncomeItem::ITEM_BORROWING_REDEMPTION,
        '/ошибочно перечисленные/ui' => InvoiceIncomeItem::ITEM_RETURN,
        '/возврат/ui' => InvoiceIncomeItem::ITEM_RETURN,
        '/\bза(е|ё|й)м(а|у)?/ui' => InvoiceIncomeItem::ITEM_LOAN,
        '/\b(собствен(н)?ы|личны(х|й|е|ми))/ui' => InvoiceIncomeItem::ITEM_OWN_FOUNDS,
        '/перевод между счетами/ui' => InvoiceIncomeItem::ITEM_OWN_FOUNDS,
        '/пополнени(е|я) карт/ui' => InvoiceIncomeItem::ITEM_OWN_FOUNDS,
        '/обеспечительный плат(ё|е)ж/ui' => InvoiceIncomeItem::ITEM_ENSURE_PAYMENT,
    ];

    public static $accountsPattern = [
        '/^20202810/' => InvoiceIncomeItem::ITEM_TRADE_RECEIPTS,
        '/^(30232810|30233810)/' => InvoiceIncomeItem::ITEM_TRADE_ACQUIRING,
    ];

    public static $internalItems = [
        self::ITEM_OWN_FOUNDS,
        self::ITEM_CURRENCY_CONVERSION,
    ];

    protected static $_data = [];

    /**
     * @param string $subject
     * @param integer $default
     * @return integer
     */
    public static function itemIdByReturn($subject, $default = null)
    {
        if (mb_stripos($subject, 'возврат') !== false) {
            if (mb_stripos($subject, 'займ') !== false) {
                return self::ITEM_BORROWING_REDEMPTION;
            } else {
                return self::ITEM_RETURN;
            }
        }

        return $default;
    }

    /**
     * @param string $subject
     * @param integer $default
     * @return integer
     */
    public static function itemIdByPurpose($subject, $default = null)
    {
        foreach (self::$incomesPattern as $pattern => $id) {
            if (preg_match($pattern, $subject) == 1) {
                return $id;
            }
        }

        return $default;
    }

    /**
     * @param string $subject
     * @param integer $default
     * @return integer
     */
    public static function itemIdByAccount($subject, $default = null)
    {
        foreach (self::$accountsPattern as $pattern => $id) {
            if (preg_match($pattern, $subject) == 1) {
                return $id;
            }
        }

        return $default;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invoice_income_item';
    }

    /**
     * @inheritDoc
     */
    public function getReasonName(): string
    {
        return $this->name;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'trim'],
            [['name'], 'filter', 'filter' => function ($value) {
                return mb_strtoupper(mb_substr($value, 0, 1)) . mb_substr($value, 1);
            }],
            [['name'], 'required'],
            [['is_visible', 'can_be_controlled', 'is_prepayment'], 'boolean'],
            [['name'], 'string', 'max' => self::NAME_MAX_LENGTH],
            [['name'], 'nameValidator'],
            [['parent_id'], 'exist', 'skipOnError' => true, 'skipOnEmpty' => true, 'targetClass' => self::class, 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function nameValidator($attribute, $params)
    {
        $query = self::find()->where([
            'and',
            ['name' => $this->name],
            ['not', ['id' => $this->id]],
            [
                'or',
                ['company_id' => null],
                ['company_id' => $this->company_id],
            ],
        ]);

        if ($query->exists()) {
            $this->addError($attribute, 'Такой тип прихода уже существует');
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBankFlows()
    {
        return $this->hasMany(CashBankFlows::className(), ['income_item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmoneyFlows()
    {
        return $this->hasMany(CashEmoneyFlows::className(), ['income_item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlanCashFlows()
    {
        return $this->hasMany(PlanCashFlows::className(), ['income_item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderFlows()
    {
        return $this->hasMany(CashOrderFlows::className(), ['income_item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoices()
    {
        return $this->hasMany(Invoice::className(), ['invoice_income_item_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                if (self::find()->max('id') < 100) {
                    $this->id = 100;
                }

                if ($this->company_id !== null) {
                    $this->is_visible = true;
                    $this->is_prepayment = true;
                    $this->can_be_controlled = true;
                }

                $this->sort = 100;
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    public function getContractors()
    {
        return $this->hasMany(Contractor::className(), ['invoice_income_item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIncomeItemsFlowOfFunds()
    {
        return $this->hasMany(IncomeItemFlowOfFunds::className(), ['income_item_id' => 'id']);
    }

    public function getFlowOfFundsItemByCompany($companyId)
    {
        return $this->hasOne(IncomeItemFlowOfFunds::className(), ['income_item_id' => 'id'])
            ->onCondition(['company_id' => $companyId]);
    }

    public function getParent()
    {
        return $this->hasOne(InvoiceIncomeItem::class, ['id' => 'parent_id']);
    }

    public function getChildren($companyId = null)
    {
        if (empty($companyId))
            $companyId = \Yii::$app->user->identity->currentEmployeeCompany->company_id;

        return $this->hasMany(InvoiceIncomeItem::class, ['parent_id' => 'id'])
            ->onCondition(['company_id' => $companyId]);
    }

    public function getFullName()
    {
        return ($this->parent) ? ($this->parent->name . ' - ' . $this->name) : $this->name;
    }

    /**
     * @return array
     */
    public static function getList($company_id, $exclude = [])
    {
        $key = Json::encode([$company_id, $exclude]);
        if (!isset(self::$_data['getList'][$key])) {
            $query = self::find()
                ->alias('i')
                ->leftJoin([
                    'ff' => 'income_item_flow_of_funds',
                ], '{{ff}}.[[income_item_id]] = {{i}}.[[id]] AND {{ff}}.[[company_id]] = :company', [
                    ':company' => $company_id
                ])
                ->andWhere([
                    'AND',
                    [
                        'OR',
                        ['ff.is_visible' => null],
                        ['ff.is_visible' => 1],
                    ],
                    [
                        'OR',
                        ['i.company_id' => null],
                        ['i.company_id' => $company_id],
                    ],
                ]);

            if ($items = ArrayHelper::getValue($exclude, 'items')) {
                $query->andWhere(['not', ['i.id' => $items]]);
            }

            self::$_data['getList'][$key] = $query->orderBy(['i.sort' => SORT_ASC, 'i.name' => SORT_ASC])->indexBy('id')->with('parent')->all();
        }

        return self::$_data['getList'][$key];
    }

    /**
     * @inheritdoc
     */
    public static function getSelect2Data($company_id, $exclude = [], $can_add = false)
    {
        $key = Json::encode([$company_id, $exclude]);
        if (!isset(self::$_data['getSelect2Data'][$key])) {
            $list = [];
            $options = [];
            $_tmpParent = $_tmpChildren = [];

            if ($can_add) {
                $list['add-modal-income-item'] = \frontend\themes\kub\helpers\Icon::get('add-icon', ['class' => 'add-button-icon']) . ' Добавить статью прихода';
                $options['add-modal-income-item'] = [
                    'data-editable' => false,
                    'data-can-delete' => false
                ];
            }

            /** @var InvoiceIncomeItem $item */
            foreach (static::getList($company_id, $exclude) as $item) {

                if (!$item->parent_id) {
                    $_tmpParent[$item->id] = $item->name;
                } else {
                    $_tmpChildren[$item->parent->id][$item->id] =
                        $item->parent->name .chr(hexdec("1f")). $item->name;
                }

                //$list[$item->id] = $item->name;
                $options[$item['id']] = [
                    'data-child' => $item->parent_id ? 1 : 0,
                    'data-editable' => $item->company_id == $company_id ? 1 : 0,
                    'data-can-delete' => ($item->company_id == $company_id && $item->getCanDelete()) ? 1 : 0,
                ];
            }

            foreach ($_tmpParent as $parentId => $parentName) {
                $list[$parentId] = $parentName;
                if (isset($_tmpChildren[$parentId])) {
                    foreach ($_tmpChildren[$parentId] as $childId => $childName)
                        $list[$childId] = $childName;
                }
            }

            self::$_data['getSelect2Data'][$key] = ['list' => $list, 'options' => $options];
        }

        if ($can_add) {
            self::$_data['getSelect2Data'][$key]['list'] = [
                'add-modal-income-item' => \frontend\components\Icon::get('add-icon', ['class' => 'add-button-icon']) . ' Добавить статью прихода',
            ] + self::$_data['getSelect2Data'][$key]['list'];
            self::$_data['getSelect2Data'][$key]['options']['add-modal-income-item'] = [
                'data-editable' => false,
                'data-can-delete' => false
            ];
        }

        return self::$_data['getSelect2Data'][$key];
    }

    public function isUsed()
    {
        return PlanCashFlows::find()->andWhere(['income_item_id' => $this->id])->exists()
            || CashBankFlows::find()->andWhere(['income_item_id' => $this->id])->exists()
            || CashEmoneyFlows::find()->andWhere(['income_item_id' => $this->id])->exists()
            || CashOrderFlows::find()->andWhere(['income_item_id' => $this->id])->exists();
    }

    /**
     * @inheritdoc
     */
    public function getCanDelete()
    {
        $q1 = $this->getBankFlows()->select('id');
        $q2 = $this->getEmoneyFlows()->select('id');
        $q3 = $this->getOrderFlows()->select('id');
        $q4 = $this->getPlanCashFlows()->select('id');
        $q5 = $this->getInvoices()->select('id');
        $q6 = $this->getContractors()->select('id');

        return !$q1->union($q2)->union($q3)->union($q4)->union($q5)->union($q6)->exists();
    }

    /**
     * @inheritdoc
     */
    public function getCanToggleVisibility()
    {
        $company = Yii::$app->user->identity->currentEmployeeCompany->company;

        return (
            !$this->getBankFlows()->andWhere(['company_id' => $company->id])->exists()
            && !$this->getEmoneyFlows()->andWhere(['company_id' => $company->id])->exists()
            && !$this->getOrderFlows()->andWhere(['company_id' => $company->id])->exists()
            && !$this->getPlanCashFlows()->andWhere(['company_id' => $company->id])->exists()
            //&& !$this->getContractors()->andWhere(['company_id' => $company->id])->exists()
            //&& !$this->getInvoices()->andWhere(['company_id' => $company->id, 'is_deleted' => 0])->exists()
        );
    }
}
