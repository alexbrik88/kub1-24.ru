<?php

namespace common\models\document;

use backend\models\Bank;
use common\components\date\DateHelper;
use common\components\date\DatePickerFormatBehavior;
use common\components\excel\Excel;
use common\components\helpers\ModelHelper;
use common\components\pdf\PdfRenderer;
use common\components\TextHelper;
use common\models\Agreement;
use common\models\AgreementType;
use common\models\Company;
use common\models\Contractor;
use common\models\cash\CashBankForeignCurrencyFlows;
use common\models\cash\CashBankForeignCurrencyFlowsToInvoice;
use common\models\cash\CashOrderForeignCurrencyFlows;
use common\models\cash\CashOrderForeignCurrencyFlowsToInvoice;
use common\models\cash\CashEmoneyForeignCurrencyFlows;
use common\models\cash\CashEmoneyForeignCurrencyFlowsToInvoice;
use common\models\cash\CashFactory;
use common\models\company\CheckingAccountant;
use common\models\currency\Currency;
use common\models\currency\CurrencyRate;
use common\models\document\DocumentType;
use common\models\document\status\InvoiceStatus;
use common\models\EmployeeCompany;
use common\models\employee\Employee;
use common\models\employee\EmployeeSignature;
use common\models\file\File;
use common\models\out\OutInvoice;
use common\models\paymentReminder\PaymentReminderMessage;
use common\models\paymentReminder\PaymentReminderMessageContractor;
use common\models\paymentReminder\Settings;
use common\models\product\Product;
use common\models\product\ProductUnit;
use common\models\product\Store;
use common\models\service\Payment;
use common\models\service\Subscribe;
use common\models\TaxRate;
use console\components\PaymentReminder;
use Faker\Provider\DateTime;
use frontend\models\Documents;
use frontend\models\log\Log;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\modules\api\components\ApiController;
use frontend\modules\cash\models\CashContractorType;
use frontend\modules\documents\assets\InvoicePrintAsset;
use frontend\modules\documents\components\Message;
use frontend\modules\export\models\export\Export;
use frontend\modules\export\models\one_c\OneCExport;
use frontend\rbac\permissions\document\Document as DocumentPermissions;
use Yii;
use yii\base\Exception;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\BadRequestHttpException;

/**
 * This is the model class for table "foreign_currency_invoice".
 *
 * @property integer $id
 * @property string $uid
 * @property integer $type_id
 * @property integer $company_id
 * @property integer $contractor_id
 * @property integer $document_author_id
 * @property string $currency_name
 * @property dateTime $document_date
 * @property dateTime $payment_limit_date
 * @property dateTime $basis_document_date
 * @property string $document_number
 * @property string $document_additional_number
 * @property integer $invoice_status_id
 * @property integer $basis_document_type_id
 * @property string $basis_document_name
 * @property string $basis_document_number
 * @property string $comment
 * @property integer $created_at
 * @property integer $updated_at
 *
 * Relations
 * @property Company $company
 * @property Contractor $contractor
 * @property Employee $documentAuthor
 *
 *
 * @mixin TimestampBehavior
 */
class ForeignCurrencyInvoice extends AbstractDocument implements PayableInvoiceInterface
{
    const SCENARIO_USER_CREATE = 'user_create';
    const SCENARIO_USER_UPDATE = 'user_update';

    /**
     * Delete status
     */
    const IS_DELETED = 1;
    const NOT_IS_DELETED = 0;

    /**
     * @var string
     */
    public static $uploadDirectory = 'foreign_currency_invoice';

    /**
     * @var string
     */
    public $printablePrefix = 'Invoice';

    /**
     * @var string
     */
    public $emailTemplate = 'foreign-currency-invoice';

    /**
     * @var string
     */
    public $ordersLoadParamName = 'orderArray';

    private $_data;

    protected $_weightPrecision;
    protected $_volumePrecision;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'foreign_currency_invoice';
    }

    /**
     * @return query\ForeignCurrencyInvoiceQuery
     */
    public static function find($config = [])
    {
        return new query\ForeignCurrencyInvoiceQuery(get_called_class(), $config);
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
            ],
        ];
    }

    /**
     * @return string
     */
    public function formName()
    {
        return 'Invoice';
    }

    /**
     * @inheritdoc
     */
    private function getData()
    {
        if ($this->_data === null) {
            $data = $this->getForeignCurrencyInvoiceData()->select([
                'foreign_currency_invoice_id',
                'key',
                'value',
            ])->indexBy('key')->asArray()->all();
            $this->_data = [];
            $id = $this->id;
            foreach (ForeignCurrencyInvoiceData::$dataFields as $key) {
                $this->_data[$key] = $data[$key] ?? [
                    'foreign_currency_invoice_id' => $id,
                    'key' => $key,
                    'value' => null,
                ];
            }
        }

        return $this->_data;
    }

    /**
     * @inheritdoc
     */
    private function setData($value)
    {
        $this->_data = $value;
    }

    /**
     * @return bool
     */
    private function getIsUserScenario()
    {
        return in_array($this->getScenario(), [
            self::SCENARIO_USER_CREATE,
            self::SCENARIO_USER_UPDATE,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function __get($name)
    {
        if (in_array($name, ForeignCurrencyInvoiceData::$dataFields)) {
            return $this->getData()[$name]['value'];
        } else {
            return parent::__get($name);
        }
    }

    /**
     * @inheritdoc
     */
    public function __set($name, $value)
    {
        if (in_array($name, ForeignCurrencyInvoiceData::$dataFields)) {
            if ($this->_data === null) {
                $this->getData();
            }
            $this->_data[$name]['value'] = $value;
        } else {
            parent::__set($name, $value);
        }
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_DEFAULT => [],
            self::SCENARIO_USER_CREATE => [
                'type',
                'document_number',
                'document_additional_number',
                'document_date_input',
                'checking_accountant_id',
                'contractor_id',
                'payment_limit_date_input',
                'agreement_id',
                'has_discount',
                'has_markup',
                'has_weight',
                'has_volume',
                'nds_view_type_id',
                'invoice_expenditure_item_id',
                'comment',
                'discount_type',
                'is_hidden_discount',
                'show_article',
                'comment_internal',
            ],
            self::SCENARIO_USER_UPDATE => [
                'document_number',
                'document_additional_number',
                'document_date_input',
                'checking_accountant_id',
                'contractor_id',
                'payment_limit_date_input',
                'agreement_id',
                'has_discount',
                'has_markup',
                'has_weight',
                'has_volume',
                'nds_view_type_id',
                'invoice_expenditure_item_id',
                'comment',
                'discount_type',
                'is_hidden_discount',
                'show_article',
                'comment_internal',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $outClosure = function ($model) {
            return $model->type == Documents::IO_TYPE_OUT;
        };
        $inClosure = function ($model) {
            return $model->type == Documents::IO_TYPE_IN;
        };

        return [
            [
                ['invoice_expenditure_item_id'], 'filter',
                'when' => $inClosure,
                'filter' => function ($value) {
                    return intval($value) ?: null;
                },
            ],
            [['comment', 'comment_internal'], 'trim'],
            [['show_article'], 'default', 'value' => 0],
            [
                [
                    'type',
                    'document_number',
                    'document_date_input',
                    'checking_accountant_id',
                    'contractor_id',
                    'payment_limit_date_input',
                    'nds_view_type_id',
                ],
                'required',
            ],
            ['type', 'in', 'range' => [
                Documents::IO_TYPE_IN,
                Documents::IO_TYPE_OUT,
            ]],
            [
                ['orders'], 'required',
                'message' => 'Не найдено ни одного товара/услуги.',
            ],
            [
                ['invoice_expenditure_item_id'], 'required',
                'when' => $inClosure,
            ],
            [
                [
                    'document_date_input',
                    'payment_limit_date_input',
                ], 'date',
                'format' => 'php:d.m.Y',
            ],
            [
                ['contractor_id',], 'exist',
                'targetClass' => Contractor::className(),
                'targetAttribute' => 'id',
                'filter' => [
                    'and',
                    ['type' => $this->type],
                    ['company_id' => $this->company_id],
                ],
            ],
            [
                [
                    'discount_type',
                ], 'integer',
            ],
            [
                ['payment_limit_date_input'],
                function ($attribute, $params) {
                    if ((new \DateTime($this->$attribute)) < (new \DateTime($this->document_date))) {
                        $this->addError($attribute, 'Значение «Оплатить до» не должно быть меньше даты счета.');
                    }
                },
            ],
            [
                [
                    'is_hidden_discount',
                    'remind_contractor',
                    'has_discount',
                    'has_markup',
                    'has_weight',
                    'has_volume',
                    'show_article',
                ], 'boolean',
            ],
            [
                ['checking_accountant_id'], 'exist',
                'skipOnEmpty' => true,
                'targetClass' => CheckingAccountant::class,
                'targetAttribute' => 'id',
                'filter' => [
                    'and',
                    ['company_id' => $this->company_id],
                    ['not', ['currency_id' => Currency::DEFAULT_ID]],
                ],
            ],
            [['document_additional_number',], 'string', 'max' => 255,],

            // type depending
            // out
            [['document_number',], 'integer', 'when' => $outClosure,],
            // in
            [['document_number'], 'string',
                'max' => 45,
                'when' => $inClosure,
            ],
            [
                ['invoice_expenditure_item_id'], 'exist',
                'targetClass' => InvoiceExpenditureItem::class,
                'targetAttribute' => 'id',
                'when' => $inClosure,
            ],
            [
                ['nds_view_type_id'], 'exist',
                'targetClass' => NdsViewType::class,
                'targetAttribute' => 'id',
            ],
            [
                ['document_number'], 'unique',
                'targetAttribute' => ['company_id', 'document_number', 'document_additional_number'],
                'when' => function ($model) {
                    return $model->type == Documents::IO_TYPE_OUT &&
                        $model->invoice_status_id != InvoiceStatus::STATUS_AUTOINVOICE;
                },
                'filter' => function ($query) {
                    $year = date('Y', strtotime($this->document_date));
                    $query->andWhere([
                        'type' => Documents::IO_TYPE_OUT,
                        'is_deleted' => self::NOT_IS_DELETED,
                    ])
                        ->andWhere(['not', ['invoice_status_id' => InvoiceStatus::STATUS_AUTOINVOICE]])
                        ->andWhere([
                            'between',
                            'document_date',
                            $year.'-01-01',
                            $year.'-12-31',
                        ]);

                    return $query;
                },
                'message' => 'Исходящий счет с таким номером уже существует',
            ],
            [['comment', 'comment_internal'], 'string'],
            [['price_precision'], 'in', 'range' => Invoice::$precisions],
        ];
    }

    /**
     * @inheritdoc
     */
    public function setDocument_date_input($value)
    {
        $this->document_date = ($d = date_create_from_format('d.m.Y', $value)) ? $d->format('Y-m-d') : $value;
    }

    /**
     * @inheritdoc
     */
    public function getDocument_date_input()
    {
        return ($d = date_create_from_format('Y-m-d', $this->document_date)) ? $d->format('d.m.Y') : $this->document_date;
    }

    /**
     * @inheritdoc
     */
    public function setPayment_limit_date_input($value)
    {
        $this->payment_limit_date = ($d = date_create_from_format('d.m.Y', $value)) ? $d->format('Y-m-d') : $value;
    }

    /**
     * @inheritdoc
     */
    public function getPayment_limit_date_input()
    {
        return ($d = date_create_from_format('Y-m-d', $this->payment_limit_date)) ? $d->format('d.m.Y') : $this->payment_limit_date;
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Тип (входящий/исходящий)',
            'invoice_status_id' => 'Статус',
            'invoice_status_updated_at' => 'Дата обнволения статуса',
            'invoice_status_author_id' => 'Автор статуса',
            'created_at' => 'Дата создания',
            'document_author_id' => 'Автор документа',
            'document_date' => 'Дата документа',
            'document_date_input' => 'Дата документа',
            'document_number' => 'Номер документа',
            'document_additional_number' => 'Дополнительный номер документа',
            'payment_limit_date' => 'Оплатить до',
            'payment_limit_date_input' => 'Оплатить до',
            'production_type' => 'Тип продукции',
            'invoice_expenditure_item_id' => 'Статья расходов',
            'company_id' => 'Компания',
            'company_name_full' => 'Полное название компании',
            'company_name_short' => 'Краткое название компании',
            'company_bank_name' => 'Название банка компании',
            'company_bank_city' => 'Город банка компании',
            'company_inn' => 'ИНН компании',
            'company_kpp' => 'КПП компании',
            'company_egrip' => 'ЕГРИП компании',
            'company_okpo' => 'ОКПО компании',
            'company_bik' => 'БИК компании',
            'company_ks' => 'КС компании',
            'company_rs' => 'Мой рас.счет',
            'company_phone' => 'Телефон компании',
            'company_address_legal_full' => 'Юридический адрес компании',
            'company_chief_post_name' => 'Должность руководителя компании',
            'company_chief_lastname' => 'Фамилия руководителя компании',
            'company_chief_firstname_initials' => 'Company Chief Firstname Initials',
            'company_chief_patronymic_initials' => 'Company Chief Patronymic Initials',
            'company_print_filename' => 'Печать компании',
            'company_chief_signature_filename' => 'Company Chief Signature Filename',
            'company_chief_accountant_lastname' => 'Company Chief Accountant Lastname',
            'company_chief_accountant_firstname_initials' => 'Company Chief Accountant Firstname Initials',
            'company_chief_accountant_patronymic_initials' => 'Company Chief Accountant Patronymic Initials',
            'contractor_id' => $this->getContractorLabel(),
            'contractor_name_full' => 'Contractor Name Full',
            'contractor_name_short' => 'Contractor Name Short',
            'contractor_bank_name' => 'Contractor Bank Name',
            'contractor_bank_city' => 'Contractor Bank City',
            'contractor_address_legal_full' => 'Contractor Address Legal Full',
            'contractor_inn' => 'Contractor Inn',
            'contractor_kpp' => 'Contractor Kpp',
            'contractor_bik' => 'Contractor Bik',
            'contractor_ks' => 'Contractor Ks',
            'contractor_rs' => 'Contractor Rs',
            'remind_contractor' => 'Отправить напоминание',
            'total_place_count' => 'Total Place Count',
            'total_mass_gross' => 'Total Mass Gross',
            'total_amount_no_nds' => 'Всего без НДС',
            'total_amount_with_nds' => 'Всего с НДС',
            'total_amount_has_nds' => 'Total Amount Has Nds',
            'total_amount_nds' => 'В том числе НДС',
            'total_order_count' => 'Количество наименований',
            'payment_form_id' => 'Payment Form ID',
            'paid_amount' => 'Payment Partial Amount',
            'is_deleted' => 'Удалён',
            'email_messages' => 'Колличество отправленных писем',
            'company_checking_accountant_id' => 'Расчетный счет',
            'basis_document_name' => 'Основание',
            'basis_document_number' => 'Номер основания',
            'basis_document_date' => 'Дата основания',
            'remaining_amount' => 'Оставшаяся для оплаты сумма',
            'comment' => 'Комментарий в счете',
            'comment_internal' => 'Комментарий',
            'price_precision' => 'Количество знаков после запятой',
            'has_discount' => 'Указать скидку',
            'has_markup' => 'Указать наценку',
            'has_weight' => 'Указать вес',
            'has_volume' => 'Указать объем',
            'currency_name' => 'Валюта счета',
            'show_paylimit_info' => 'Другой обменный курс',
            'contract_essence' => 'Предмет договора',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreement()
    {
        return $this->hasOne(Agreement::className(), ['id' => 'agreement_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBankPayments()
    {
        return $this->hasMany(CashBankForeignCurrencyFlowsToInvoice::className(), ['invoice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashBankFlows()
    {
        return $this->hasMany(CashBankForeignCurrencyFlows::className(), ['id' => 'flow_id'])
            ->viaTable(CashBankForeignCurrencyFlowsToInvoice::tableName(), ['invoice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderPayments()
    {
        return $this->hasMany(CashOrderForeignCurrencyFlowsToInvoice::className(), ['invoice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashOrderFlows()
    {
        return $this->hasMany(CashOrderForeignCurrencyFlows::className(), ['id' => 'flow_id'])
            ->viaTable(CashOrderForeignCurrencyFlowsToInvoice::tableName(), ['invoice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmoneyPayments()
    {
        return $this->hasMany(CashEmoneyForeignCurrencyFlowsToInvoice::className(), ['invoice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashEmoneyFlows()
    {
        return $this->hasMany(CashEmoneyForeignCurrencyFlows::className(), ['id' => 'flow_id'])
            ->viaTable(CashEmoneyForeignCurrencyFlowsToInvoice::tableName(), ['invoice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContractor()
    {
        return $this->hasOne(Contractor::className(), ['id' => 'contractor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['name' => 'currency_name']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentAuthor()
    {
        return $this->hasOne(Employee::className(), ['id' => 'document_author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentStatus()
    {
        return $this->getInvoiceStatus();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCheckingAccountant()
    {
        return $this->hasOne(CheckingAccountant::className(), ['id' => 'checking_accountant_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForeignCurrencyAccount()
    {
        return $this->getCheckingAccountant();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getForeignCurrencyInvoiceData()
    {
        return $this->hasMany(ForeignCurrencyInvoiceData::className(), ['foreign_currency_invoice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceStatus()
    {
        return $this->hasOne(status\InvoiceStatus::className(), ['id' => 'invoice_status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceStatusAuthor()
    {
        return $this->hasOne(Employee::className(), ['id' => 'invoice_status_author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceExpenditureItem()
    {
        return $this->hasOne(InvoiceExpenditureItem::className(), ['id' => 'invoice_expenditure_item_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNdsViewType()
    {
        return $this->hasOne(NdsViewType::className(), ['id' => 'nds_view_type_id']);
    }

    /**
     * @return query\OrderQuery
     */
    public function getOrders()
    {
        return $this->hasMany(ForeignCurrencyInvoiceOrder::className(), ['foreign_currency_invoice_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentForm()
    {
        return $this->hasOne(PaymentForm::className(), ['id' => 'payment_form_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getResponsible()
    {
        return $this->hasOne(EmployeeCompany::className(), [
            'company_id' => 'company_id',
            'employee_id' => 'responsible_employee_id',
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSignBasisDocument()
    {
        return $this->hasOne(DocumentType::className(), ['id' => 'sign_document_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSignEmployeeCompany()
    {
        return $this->hasOne(EmployeeCompany::className(), ['employee_id' => 'signed_by_employee_id'])
            ->andWhere(['company_id' => $this->company_id]);
    }

    /**
     * @inheritdoc
     */
    public function getFile()
    {
        return $this->hasOne(File::className(), ['owner_id' => 'id'])
            ->onCondition(['owner_model' => self::className(),]);
    }

    /**
     * @return bool
     */
    public function updateAllowed()
    {
        return (!in_array($this->invoice_status_id, status\InvoiceStatus::$hasPayment));
    }

    /**
     * Full invoice number
     *
     * @return string
     */
    public function getFullNumber()
    {
        if ($this->is_additional_number_before) {
            return $this->document_additional_number . $this->document_number;
        } else {
            return $this->document_number . $this->document_additional_number;
        }
    }

    /**
     * @return string
     */
    public function getEmailSubject()
    {
        return $this->printablePrefix . ' № ' . $this->fullNumber
            . ' от ' . date_format(date_create($this->document_date), 'd.m.Y')
            . ' от ' . $this->company_name_short;
    }

    /**
     * @return string
     */
    public function getEmailText()
    {
        $text = <<<EMAIL_TEXT
Здравствуйте!
EMAIL_TEXT;

        return $text;
    }

    public function getEmailPermanentText()
    {
        $document_date = DateHelper::format($this->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
        $payment_limit_date = DateHelper::format($this->payment_limit_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
        $sum = TextHelper::invoiceMoneyFormat($this->total_amount_with_nds, 2);

        $currency = $this->currency_name == Currency::DEFAULT_NAME ? 'руб' : $this->currency_name;

        return "Счёт № {$this->fullNumber} от {$document_date} на сумму {$sum} {$currency}.<br>
        Оплатить до {$payment_limit_date}г.";
    }

    /**
     * @throws \MpdfException
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function loadEmailFiles()
    {
        $files = [];

        $emailFile = new EmailFile();
        $emailFile->company_id = $this->company_id;
        $emailFile->file_name = str_replace(['\\', '/'], '_', $this->pdfFileName);
        $emailFile->ext = 'pdf';
        $emailFile->mime = 'application/pdf';
        $emailFile->size = 0;
        $emailFile->type = EmailFile::TYPE_FOREIGN_CURRENCY_INVOICE;
        if ($emailFile->save()) {
            $path = $emailFile->getFilePath() . DIRECTORY_SEPARATOR . $emailFile->file_name;
            $size = file_put_contents($path, self::getRenderer(null, $this, PdfRenderer::DESTINATION_STRING, null)->output(false));
            $emailFile->size = ceil($size / 1024);
            if ($emailFile->save(true, ['size'])) {
                $files[] = [
                    'id' => $emailFile->id,
                    'name' => $emailFile->file_name,
                    'documentPdf' => true,
                    'previewImg' => '<img src="/img/email/emailPdf.png" class="preview-img">',
                    'size' => $emailFile->size,
                    'deleteUrl' => Url::to(['/email/delete-email-file', 'id' => $emailFile->id]),
                    'downloadUrl' => Url::to(['/email/download-file', 'id' => $emailFile->id]),
                ];
            }
        }
        if (false && $this->contractor_bank_swift && $this->contractor_rs) {
            $name = ($this->is_invoice_contract && $this->type == Documents::IO_TYPE_OUT) ? 'счет-договора' : 'счета';
            $emailFile = new EmailFile();
            $emailFile->company_id = $this->company_id;
            $emailFile->file_name = 'Платежное_поручение_для_' . $name . '_№' .
                mb_ereg_replace("([^\w\s\d\-_])", '', $this->fullNumber) . '.txt';
            $emailFile->ext = 'txt';
            $emailFile->mime = 'text/plain';
            $emailFile->size = 0;
            $emailFile->type = EmailFile::TYPE_INVOICE_PAYMENT_ORDER;
            if ($emailFile->save()) {
                $path = $emailFile->getFilePath() . DIRECTORY_SEPARATOR . $emailFile->file_name;
                $size = file_put_contents($path, Yii::$app->view->render('@frontend/modules/documents/views/invoice/1C_payment_order.php', [
                    'model' => $this,
                ]));
                if (file_exists($path)) {
                    unlink($path);
                }
                $emailFile->size = ceil($size / 1024);
                if ($emailFile->save(true, ['size'])) {
                    $files[] = [
                        'id' => $emailFile->id,
                        'name' => $emailFile->file_name,
                        'previewImg' => '<img src="/img/email/emailTxt.png" class="preview-img">',
                        'size' => $emailFile->size,
                        'deleteUrl' => Url::to(['/email/delete-email-file', 'id' => $emailFile->id]),
                    ];
                }
            }
        }

        return $files;
    }

    /**
     * @param EmployeeCompany $sender
     * @param array|string $toEmail
     * @param null $emailText
     * @param null $subject
     * @param null $addStamp
     * @param null $addHtml
     * @param null $attach
     * @param bool $onlyMyAttach
     * @return bool
     * @throws Exception
     * @throws \MpdfException
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function sendAsEmail(EmployeeCompany $sender, $toEmail, $emailText = null, $subject = null, $addStamp = null, $addHtml = null, $attach = null, $onlyMyAttach = false, $sendWithDocuments = false)
    {
        if ($this->type == Documents::IO_TYPE_OUT) {
            if ($this->uid == null) {
                $this->updateAttributes([
                    'uid' => self::generateUid(),
                ]);
            }
            $message = $this->getMailerMessage($sender, $toEmail, $emailText, $subject, $addHtml);
            if (!$onlyMyAttach) {
                $message->attachContent(self::getRenderer(null, $this, PdfRenderer::DESTINATION_STRING, $addStamp)->output(false), [
                    'fileName' => str_replace(['\\', '/'], '_', $this->pdfFileName),
                    'contentType' => 'application/pdf',
                ]);
            }

            if ($attach) {
                foreach ($attach as $file) {
                    if ($file['is_file']) {
                        if (exif_imagetype($file['content']) === false) {
                            $message->attach($file['content'], [
                                'fileName' => $file['fileName'],
                                'contentType' => $file['contentType'],
                            ]);
                        } else {
                            $message->embed($file['content'], [
                                'fileName' => $file['fileName'],
                                'contentType' => $file['contentType'],
                            ]);
                        }
                    } else {
                        $message->attachContent($file['content'], [
                            'fileName' => $file['fileName'],
                            'contentType' => $file['contentType'],
                        ]);
                    }
                }
            }

            if ($message->send()) {
                $this->updateSendStatus($sender->employee_id);

                return true;
            }
        }

        return false;
    }

    /**
     * @param $employeeId
     * @throws Exception
     */
    public function updateSendStatus($employeeId)
    {
        if (in_array($this->invoice_status_id, [InvoiceStatus::STATUS_CREATED, InvoiceStatus::STATUS_SEND])) {
            $this->setAttributes([
                'invoice_status_id' => InvoiceStatus::STATUS_SEND,
                'invoice_status_updated_at' => time(),
                'invoice_status_author_id' => $employeeId,
                'email_messages' => $this->email_messages + 1,
            ]);
            LogHelper::save($this, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function ($model) {
                return $model->save(false, [
                    'invoice_status_id',
                    'invoice_status_updated_at',
                    'invoice_status_author_id',
                    'email_messages',
                ]);
            });
        } else {
            $currentStatus = $this->invoice_status_id;
            $this->invoice_status_id = InvoiceStatus::STATUS_SEND;
            LogHelper::log($this, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS);
            $this->invoice_status_id = $currentStatus;
            $this->updateAttributes([
                'email_messages' => $this->email_messages + 1,
            ]);
        }

    }

    /**
     * @param $fileName
     * @param $outInvoice
     * @param string $destination
     * @param null $addStamp
     * @return PdfRenderer
     *
     */
    public static function getRenderer($fileName, $outInvoice, $destination = PdfRenderer::DESTINATION_FILE, $addStamp = null)
    {
        $renderer = new PdfRenderer([
            'view' => '@frontend/modules/documents/views/foreign-currency-invoice/pdf-view',
            'params' => array_merge([
                'model' => $outInvoice,
                'message' => new Message(Documents::DOCUMENT_INVOICE, $outInvoice->type),
                'ioType' => $outInvoice->type,
                'addStamp' => isset($addStamp) ? $addStamp : $outInvoice->company->pdf_send_signed,
            ]),
            'destination' => $destination,
            'filename' => $fileName,
            'displayMode' => PdfRenderer::DISPLAY_MODE_FULLPAGE,
            'format' => 'A4-P',
        ]);
        \Yii::$app->controller->view->params['asset'] = InvoicePrintAsset::className();

        return $renderer;
    }

    /**
     * @return string
     */
    public function getNdsViewTypeName()
    {
        return 'TAX (VAT)';
        switch ($this->nds_view_type_id) {
            case NdsViewType::NDS_VIEW_IN:
                return 'VAT inside';
                break;

            case NdsViewType::NDS_VIEW_OUT:
                return 'VAT on top';
                break;

            case NdsViewType::NDS_VIEW_WITHOUT:
            default:
                return 'Without VAT';
                break;
        }
    }

    /**
     * @return string
     */
    public function getExecutorFullRequisites()
    {
        return $this->type == Documents::IO_TYPE_IN ? $this->getContractorFullRequisites() : $this->getCompanyFullRequisites();
    }

    /**
     * @return string
     */
    public function getCustomerFullRequisites()
    {
        return $this->type == Documents::IO_TYPE_IN ? $this->getCompanyFullRequisites() : $this->getContractorFullRequisites();
    }

    /**
     * @return string
     */
    public function getContractorFullRequisites()
    {
        $isLegal = ($this->contractor->face_type == Contractor::TYPE_LEGAL_PERSON);
        $result = strpos($this->contractor_name_short, 'ИП ') === 0 ? $this->contractor_name_full : $this->contractor_name_short;
        $result .= ($isLegal && $this->contractor_inn) ? ', ИНН ' . $this->contractor_inn : '';
        $result .= ($isLegal && $this->contractor_kpp) ? ', КПП ' . $this->contractor_kpp : '';
        $result .= ($this->contractor_address_legal_full) ? ', ' . $this->contractor_address_legal_full : '';
        $result .= ($isLegal && $this->contractor_rs) ? ', р/с ' . $this->contractor_rs : '';
        $result .= ($isLegal && $this->contractor_bank_name) ? ' в банке ' . $this->contractor_bank_name : '';
        $result .= ($isLegal && $this->contractor_bik) ? ', БИК ' . $this->contractor_bik : '';
        $result .= ($isLegal && $this->contractor_ks) ? ', к/с ' . $this->contractor_ks : '';

        return $result;
    }

    /**
     * @return string
     */
    public function getCompanyFullRequisites()
    {
        $result = $this->company->getIsLikeIP() ? $this->company_name_full : $this->company_name_short;
        $result .= $this->company_inn ? ', ИНН ' . $this->company_inn : '';
        $result .= $this->company_kpp ? ', КПП ' . $this->company_kpp : '';
        $result .= $this->company_address_legal_full ? ', ' . $this->company_address_legal_full : '';
        $result .= $this->company_rs ? ', р/с ' . $this->company_rs : '';
        $result .= $this->company_bank_name ? ' в банке ' . $this->company_bank_name : '';
        $result .= $this->company_bik ? ', БИК ' . $this->company_bik : '';
        $result .= $this->company_ks ? ', к/с ' . $this->company_ks : '';

        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPossibleBankFlows()
    {
        $existsFlowIdArray = $this->getBankPayments()->select('flow_id')->column();
        $query = CashBankForeignCurrencyFlows::find()
            ->alias('flow')
            ->leftJoin([
                'paid' => CashBankForeignCurrencyFlowsToInvoice::find()
                    ->select(['flow_id', 'invoices_amount' => new Expression("SUM(amount)")])
                    ->groupBy('flow_id')
            ], 'flow.id = paid.flow_id')
            ->andWhere([
                'flow.flow_type' => CashFactory::$documentToFlowType[$this->type],
                'flow.company_id' => $this->company_id,
                'flow.contractor_id' => $this->contractor_id,
                'flow.currency_id' => $this->currency->id,
            ])
            ->andWhere(['>', 'flow.amount', new Expression("IFNULL(invoices_amount, 0)")]);

        if ($existsFlowIdArray) {
            $query->andWhere(['not', ['flow.id' => $existsFlowIdArray]]);
        }

        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPossibleOrderFlows()
    {
        $existsFlowIdArray = $this->getOrderPayments()->select('flow_id')->column();
        $query = CashOrderForeignCurrencyFlows::find()
            ->alias('flow')
            ->leftJoin([
                'paid' => CashOrderForeignCurrencyFlowsToInvoice::find()
                    ->select(['flow_id', 'invoices_amount' => new Expression("SUM(amount)")])
                    ->groupBy('flow_id')
            ], 'flow.id = paid.flow_id')
            ->andWhere([
                'flow.flow_type' => CashFactory::$documentToFlowType[$this->type],
                'flow.company_id' => $this->company_id,
                'flow.contractor_id' => $this->contractor_id,
                'flow.currency_id' => $this->currency->id,
            ])
            ->andWhere(['>', 'flow.amount', new Expression("IFNULL(invoices_amount, 0)")]);

        if ($existsFlowIdArray) {
            $query->andWhere(['not', ['flow.id' => $existsFlowIdArray]]);
        }

        return $query;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPossibleEmoneyFlows()
    {
        $existsFlowIdArray = $this->getEmoneyPayments()->select('flow_id')->column();
        $query = CashEmoneyForeignCurrencyFlows::find()
            ->alias('flow')
            ->leftJoin([
                'paid' => CashEmoneyForeignCurrencyFlowsToInvoice::find()
                    ->select(['flow_id', 'invoices_amount' => new Expression("SUM(amount)")])
                    ->groupBy('flow_id')
            ], 'flow.id = paid.flow_id')
            ->andWhere([
                'flow.flow_type' => CashFactory::$documentToFlowType[$this->type],
                'flow.company_id' => $this->company_id,
                'flow.contractor_id' => $this->contractor_id,
                'flow.currency_id' => $this->currency->id,
            ])
            ->andWhere(['>', 'flow.amount', new Expression("IFNULL(invoices_amount, 0)")]);

        if ($existsFlowIdArray) {
            $query->andWhere(['not', ['flow.id' => $existsFlowIdArray]]);
        }

        return $query;
    }

    /**
     * @return boolean
     */
    public function getHasFlows()
    {
        return $this->getCashBankFlows()->exists() || $this->getCashEmoneyFlows()->exists() || $this->getCashOrderFlows()->exists();
    }

    /**
     * @return array common\models\cash\CashFlowsBase
     */
    public function getAllFlows()
    {
        return $this->getCashBankFlows()->all() + $this->getCashEmoneyFlows()->all() + $this->getCashOrderFlows()->all();
    }

    /**
     * @param $class
     * @return static
     */
    public function getPaidAmount()
    {
        return (int)$this->getBankPayments()->select('amount')
            ->union($this->getEmoneyPayments()->select('amount'), true)
            ->union($this->getOrderPayments()->select('amount'), true)
            ->sum('amount');
    }

    /**
     * @return int
     */
    public function getAvailablePaymentAmount()
    {
        return max(0, $this->total_amount_with_nds - $this->getPaidAmount());
    }

    /**
     * @param Invoice $invoice
     * @return int
     */
    public function getIsFullyPaid()
    {
        return $this->getAvailablePaymentAmount() == 0;
    }

    /**
     * @param Invoice $invoice
     * @return int
     */
    public function getLastPaymentDate()
    {
        $date = $this->getCashBankFlows()->max('date');

        return $date ? strtotime($date . ' ' . date('H:i')) : 0;
    }

    /**
     * @param null $cashFlow
     * @return int
     * @throws Exception
     * @throws \yii\db\Exception
     * @throws ForbiddenHttpException
     * @throws NotFoundHttpException
     * @throws RangeNotSatisfiableHttpException
     */
    public function checkPaymentStatus($cashFlow = null)
    {
        $paid = min($this->total_amount_with_nds, $this->getPaidAmount());
        $remaining = $this->total_amount_with_nds - $paid;
        if ($paid == 0) {
            if (in_array($this->invoice_status_id, [InvoiceStatus::STATUS_PAYED, InvoiceStatus::STATUS_PAYED_PARTIAL])) {
                $this->invoice_status_id = $this->type == Documents::IO_TYPE_OUT ?
                    InvoiceStatus::STATUS_SEND :
                    InvoiceStatus::STATUS_CREATED;
            }
        } else {
            $lastPaymentTimestamp = $this->getLastPaymentDate();
            $this->invoice_status_id = ($remaining == 0) ? InvoiceStatus::STATUS_PAYED : InvoiceStatus::STATUS_PAYED_PARTIAL;
            $this->invoice_status_updated_at = $lastPaymentTimestamp ?: time();
        }

        if ($paid != $this->paid_amount || $remaining != $this->remaining_amount) {
            $this->updateAttributes([
                'paid_amount' => $paid,
                'remaining_amount' => $remaining,
            ]);
        }

        if ($this->isAttributeChanged('invoice_status_id')) {
            if (Yii::$app->id != 'app-console') {
                LogHelper::save($this, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function ($model) {
                    return $model->save(true, ['invoice_status_id', 'invoice_status_updated_at', 'invoice_status_author_id']);
                });
            } else {
                $this->save(true, ['invoice_status_id', 'invoice_status_updated_at', 'invoice_status_author_id']);
            }
        }

        return true;
    }

    /**
     * Whether invoice overdue or not.
     * @return bool
     */
    public function isOverdue()
    {
        return ($this->payment_limit_date < date(DateHelper::FORMAT_DATE) && !$this->getIsFullyPaid())
            ||
            ($this->payment_limit_date < date(DateHelper::FORMAT_DATE, $this->invoice_status_updated_at) && $this->getIsFullyPaid());
    }

    /**
     * @param bool $short
     *
     * @param bool $reverse
     * @return string
     */
    public function getCompanyChiefFio($short = false, $reverse = false)
    {
        return $this->getFio('company_chief', $short, $reverse);
    }

    /**
     * @param bool|false $short
     * @param bool|false $reverse
     * @return string
     */
    public function getCompanyChiefAccountantFio($short = false, $reverse = false)
    {
        return $this->getFio($this->company->chief_is_chief_accountant ? 'company_chief' : 'company_chief_accountant', $short, $reverse);
    }

    /**
     * @param $prefix
     * @param $short
     * @param $reverse
     * @return string
     */
    public function getFio($prefix, $short, $reverse)
    {
        $f = $short ? mb_substr($this->{$prefix . '_firstname'}, 0, 1, "UTF-8") : $this->{$prefix . '_firstname'};
        $p = $short ? mb_substr($this->{$prefix . '_patronymic'}, 0, 1, "UTF-8") : $this->{$prefix . '_patronymic'};

        $fioArray = array_filter([
            $this->{$prefix . '_lastname'},
            $f ? ($short ? $f.'.' : $f) : null,
            $p ? ($short ? $p.'.' : $p) : null,
        ]);

        if ($reverse) {
            $fioArray[] = array_shift($fioArray);
        }

        return join(' ', $fioArray);
    }

    /**
     * Get formatted log message from Log model
     * @param Log $log
     * @return string
     */
    public function getLogMessage(Log $log)
    {
        $message = [];

        $text = $this->printablePrefix.' №'.$this->getFullNumber();

        if ($this->is_deleted) {
            $message[] = $text;
        } else {
            $message[] = Html::a($text, [
                '/documents/foreign-currency-invoice/view',
                'type' => $log->getModelAttributeNew('type'),
                'id' => $log->getModelAttributeNew('id'),
            ]);
        }

        $currency = $log->getModelAttributeNew('currency_name');
        $message[] = TextHelper::invoiceMoneyFormat($log->getModelAttributeNew('view_total_with_nds'), 2) . " {$currency}";
        $message[] = Contractor::findOne($log->getModelAttributeNew('contractor_name_short'));

        switch ($log->log_event_id) {
            case LogEvent::LOG_EVENT_CREATE:
                return implode(', ', $message) . ' был создан.';
            case LogEvent::LOG_EVENT_DELETE:
                return implode(', ', $message) . ' был удалён.';
            case LogEvent::LOG_EVENT_UPDATE:
                return implode(', ', $message) . ' был изменен.';
            case LogEvent::LOG_EVENT_UPDATE_STATUS:
                /* @var InvoiceStatus $status */
                $status = InvoiceStatus::findOne($log->getModelAttributeNew('invoice_status_id'));

                return $status !== null
                    ? implode(', ', $message) . ' статус "' . $status->name . '"'
                    : '';
            default:
                return $log->message;
        }
    }

    /**
     * @param Company|integer $company
     * @param integer $ioType
     * @param string $date
     * @return int
     */
    public static function getNextDocumentNumber($company, $ioType, $date = null)
    {
        $companyId = ($company instanceof Company) ? $company->id : $company;
        $year = ($d = date_create($date)) ? $d->format('Y') : date('Y');
        $query = self::find()->andWhere([
            'and',
            ['company_id' => $companyId],
            ['between', 'document_date', "{$year}-01-01", "{$year}-12-31"],
            ['type' => $ioType],
            ['is_deleted' => false],
        ]);

        $n = ((int) $query->max('([[document_number]] * 1)')) + 1;

        return max(1, $n);
    }

    /**
     * @return boolean
     */
    public function getIsContractorEditable() {

        if ($this->getIsNewRecord()) {
            return true;
        }

        if ($this->company->getIsFreeTariff()) {
            return false;
        }

        return true;
    }

    /**
     * @return boolean
     */
    public function getHasNds()
    {
        return $this->nds_view_type_id != NdsViewType::NDS_VIEW_WITHOUT;
    }

    /**
     * @return string
     */
    public function getContractorLabel()
    {
        switch ($this->type) {
            case Documents::IO_TYPE_IN:
                return 'Поставщик';
                break;

            case Documents::IO_TYPE_OUT:
                return 'Покупатель';
                break;

            default:
                return 'Контрагент';
                break;
        }
    }

    /**
     * @inheritdoc
     */
    public function getTotalWeight()
    {
        $total = 0;
        foreach ($this->orders as $order) {
            $total += floatval($order->quantity) * floatval($order->weight);
        }

        return $total;
    }

    /**
     * @inheritdoc
     */
    public function getTotalVolume()
    {
        $total = 0;
        foreach ($this->orders as $order) {
            $total += floatval($order->quantity) * floatval($order->volume);
        }

        return $total;
    }

    /**
     * @return int
     */
    public function getWeightPrecision() {
        if (!$this->_weightPrecision) {
            $this->_weightPrecision = 2;
            foreach ($this->orders as $order) {
                if (strpos($order->weight, '.') !== false) {
                    $wArr = explode('.', $order->weight);
                    if (!empty($wArr[1]))
                        $this->_weightPrecision = max($this->_weightPrecision, strlen($wArr[1]));
                }
            }
        }

        return $this->_weightPrecision;
    }

    /**
     * @return int
     */
    public function getVolumePrecision() {
        if (!$this->_volumePrecision) {
            $this->_volumePrecision = 2;
            foreach ($this->orders as $order) {
                if (strpos($order->volume, '.') !== false) {
                    $vArr = explode('.', $order->volume);
                    if (!empty($vArr[1]))
                        $this->_volumePrecision = max($this->_volumePrecision, strlen($vArr[1]));
                }
            }
        }

        return $this->_volumePrecision;
    }

    /**
     * @param array $data the data array to load, typically `$_POST` or `$_GET`.
     * @param string $formName the form name to use to load the data into the model.
     * If not set, [[formName()]] is used.
     * @return bool whether `load()` found the expected form in `$data`.
     */
    public function load($data, $formName = null)
    {
        if (parent::load($data, $formName)) {
            $this->ordersLoad(ArrayHelper::getValue($data, $this->ordersLoadParamName, []));

            return true;
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public function ordersLoad($ordersData)
    {
        $orderArray = [];
        $orderDataArray = [];
        $noNds = true;
        $this->removedOrders = $this->getOrders()->indexBy('id')->all();
        if (is_array($ordersData)) {
            foreach ($ordersData as $data) {
                $productId = ArrayHelper::getValue($data, 'product_id');
                $productQuery = Product::find()->andWhere([
                    'id' => $productId,
                    'company_id' => $this->company_id,
                    'not_for_sale' => $this->type == Documents::IO_TYPE_IN ? [0, 1] : 0,
                ]);

                $orderQuery = ForeignCurrencyInvoiceOrder::find()->andWhere([
                    'id' => ArrayHelper::getValue($data, 'id'),
                    'foreign_currency_invoice_id' => $this->id,
                    'product_id' => $productId,
                ]);

                if ($order = $orderQuery->one()) {
                    $order->populateRelation('invoice', $this);
                } elseif ($product = $productQuery->one()) {
                    if (isset($data['tax_rate_id'])) {
                        if ($this->type == Documents::IO_TYPE_OUT) {
                            $product->price_for_sell_nds_id = $data['tax_rate_id'];
                        } elseif ($this->type == Documents::IO_TYPE_IN) {
                            $product->price_for_buy_nds_id = $data['tax_rate_id'];
                        }
                    }
                    $order = ForeignCurrencyInvoiceOrder::createFromProduct($product, $this, null);
                } else {
                    $order = null;
                }

                if ($order !== null) {
                    $orderArray[] = $order;
                    $orderDataArray[] = $data;
                    if (isset($this->removedOrders[$order->id])) {
                        unset($this->removedOrders[$order->id]);
                    }
                }
            }

            array_walk($orderArray, function ($order, $key) use ($orderDataArray) {
                $order->load($orderDataArray[$key], '');
                $order->calculateOrder();
                $order->number = $key + 1;
            });
        }

        $this->populateRelation('orders', $orderArray);
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            if ($this->getIsUserScenario()) {
                $this->calculateTotalAmount();
            }

            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function afterValidate()
    {
        parent::afterValidate();
        if ($this->getIsUserScenario()) {
            foreach ($this->orders as $order) {
                if (!$order->validate()) {
                    $errors = $order->getFirstErrors();
                    $this->addError('orders', reset($errors));
                }
            }
        }
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->paid_amount = 0;
                $this->remaining_amount = $this->total_amount_with_nds;
                $this->setCompany();
            }
            if ($this->isAttributeChanged('checking_accountant_id')) {
                $this->setCheckingAccountant();
            }
            if ($this->isAttributeChanged('contractor_id')) {
                $this->setContractor();
            }
            if ($this->isAttributeChanged('agreement_id')) {
                $this->setAgreement();
            }
            if ($this->isAttributeChanged('document_author_id')) {
                $this->setDocumentAuthor();
            }
            if (!isset(
                $this->company_id,
                $this->contractor_id,
                $this->invoice_status_id,
                $this->document_author_id,
                $this->invoice_status_author_id,
                $this->currency_name
            )) {
                return false;
            }

            if (empty($this->responsible_employee_id)) {
                $this->responsible_employee_id = $this->document_author_id;
            }

            return true;
        }

        return false;
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if (!empty($this->_data)) {
            if ($insert) {
                array_walk($this->_data, function (&$item, $key) {
                    $item['foreign_currency_invoice_id'] = $this->id;
                });
            }
            $db = $this->db;
            $sql = $db->queryBuilder->batchInsert(ForeignCurrencyInvoiceData::tableName(), [
                'foreign_currency_invoice_id',
                'key',
                'value',
            ], $this->_data);
            $db->createCommand($sql . ' ON DUPLICATE KEY UPDATE [[value]]=VALUES([[value]])')->execute();
        }
        if ($this->getIsUserScenario()) {
            foreach ($this->orders as $order) {
                $order->foreign_currency_invoice_id = $this->id;
                $order->save(false);
            }

            foreach ($this->removedOrders as $removedOrder) {
                $removedOrder->delete();
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function delete()
    {
        if (!$this->beforeDelete()) {
            return false;
        }

        $this->updateAttributes([
            'is_deleted' => true,
        ]);

        $this->afterDelete();

        return 1;
    }

    /**
     * @inheritdoc
     */
    public function calculateTotalAmount()
    {
        $hasNds = $this->nds_view_type_id != NdsViewType::NDS_VIEW_WITHOUT;
        $ndsOut = $this->nds_view_type_id == NdsViewType::NDS_VIEW_OUT;
        $this->total_discount =
        $this->total_amount =
        $this->total_nds =
        $this->total_amount_with_nds =
        $this->total_mass_gross =
        $this->total_place_count = 0;

        $this->total_order_count = count($this->orders);

        foreach ($this->orders as $order) {
            $this->total_amount += $order->amount;
            $this->total_mass_gross += floatval($order->mass_gross);
            $this->total_place_count += floatval($order->place_count);
            if ($hasNds) {
                $this->total_nds += $order->nds_amount;
            }
            if ($this->has_discount) {
                $this->total_discount += ($order->base_amount - $order->amount);
            }
        }

        $this->total_amount = $this->total_amount_with_nds = round($this->total_amount);
        $this->total_nds = round($this->total_nds);
        $this->total_discount = round($this->total_discount);

        if ($ndsOut) {
            $this->total_amount_with_nds += $this->total_nds;
        }
    }

    /**
     * @param Company $company
     *
     * @throws Exception
     */
    public function setCompany(Company $company = null)
    {
        $company = $company ?? $this->company;

        if ($company === null) {
            throw new Exception('Компания не установлена');
        }

        $this->company_id = $company->id;
        $this->company_inn = $company->inn;
        $this->company_kpp = $company->kpp;
        $this->company_egrip = $company->egrip;
        $this->company_okpo = $company->okpo;
        $this->company_name_full = $company->getTitleEn();
        $this->company_name_short = $company->getShortTitleEn();
        $this->company_address_legal_full = $company->address_legal_en;
        $this->company_phone = $company->phone;
        $this->company_chief_post_name = $company->chief_post_name_en;
        $this->company_chief_lastname = $company->lastname_en;
        $this->company_chief_firstname = $company->firstname_en;
        $this->company_chief_patronymic = $company->patronymic_en;
        $this->company_print_filename = $company->print_link;
        $this->company_chief_signature_filename = $company->chief_signature_link;
    }

    /**
     * @return bool
     */
    public function setDocumentAuthor()
    {
        if ($employee = $this->documentAuthor) {
            $this->document_author_email = $employee->email;
            $this->document_author_phone = EmployeeCompany::findOne([
                'employee_id' => $employee->id,
                'company_id' => $this->company_id,
            ])->phone ?? $employee->phone;
        } else {
            $this->document_author_email =
            $this->document_author_phone = null;
        }
    }

    /**
     * @return bool
     */
    public function setAgreement()
    {
        //
    }

    /**
     * @param Company $company
     * @return bool
     */
    public function setCheckingAccountant(CheckingAccountant $account = null)
    {
        $account = $account ?: $this->foreignCurrencyAccount;
        if ($account) {
            $this->company_rs = $account->rs;
            $this->company_iban = $account->iban;
            $this->company_bank_name = $account->bank_name_en;
            $this->company_bank_address = $account->bank_address_en;
            $this->company_bank_swift = $account->swift;
            $this->company_corr_rs = $account->corr_rs;
            $this->company_corr_bank_name = $account->corr_bank_name;
            $this->company_corr_bank_address = $account->corr_bank_address;
            $this->company_corr_bank_swift = $account->corr_swift;
            $this->currency_name = $account->currency->name ?? null;
        } else {
            $this->company_rs =
            $this->company_iban =
            $this->company_bank_name =
            $this->company_bank_address =
            $this->company_bank_swift =
            $this->company_corr_rs =
            $this->company_bank_name =
            $this->company_bank_address =
            $this->company_bank_swift =
            $this->currency_name = null;
        }
    }

    /**
     * @param Contractor $contractor
     */
    public function setContractor()
    {
        if ($contractor = $this->contractor) {
            $this->contractor_name_short = $contractor->getTitle(true);
            $this->contractor_name_full = $contractor->getTitle(false);
            $this->contractor_director_name = $contractor->director_name;
            $this->contractor_director_post_name = $contractor->director_post_name;
            $this->contractor_director_email = $contractor->director_email;
            $this->contractor_director_phone = $contractor->director_phone;
            $this->contractor_address_legal_full = $contractor->legal_address;
            $this->contractor_inn = $contractor->ITN;
            $this->contractor_kpp = $contractor->PPC;
            $this->contractor_rs = $contractor->current_account;
            $this->contractor_bank_name = $contractor->bank_name;
            $this->contractor_bank_address = $contractor->bank_address;
            $this->contractor_bank_swift = $contractor->bic;
            $this->contractor_corr_rs = $contractor->corresp_account;
            $this->contractor_corr_bank_name = $contractor->corr_bank_name;
            $this->contractor_corr_bank_address = $contractor->corr_bank_address;
            $this->contractor_corr_bank_swift = $contractor->corr_bank_swift;
        } else {
            $this->contractor_name_short =
            $this->contractor_name_full =
            $this->contractor_director_name =
            $this->contractor_director_post_name =
            $this->contractor_address_legal_full =
            $this->contractor_inn =
            $this->contractor_kpp =
            $this->contractor_rs =
            $this->contractor_bank_name =
            $this->contractor_bank_address =
            $this->contractor_bank_swift =
            $this->contractor_corr_rs =
            $this->contractor_corr_bank_name =
            $this->contractor_corr_bank_address =
            $this->contractor_corr_bank_swift = null;
        }
    }

    /**
     * @return bool
     */
    public function checkContractor()
    {
        $contractor = $this->contractor;
        $contractorNameShort = $contractor->getTitle(true);
        $contractorNameFull = $contractor->getTitle(false);
        $contractorAddressLegalFull = $contractor->legal_address;

        $contractorFields = [
            'contractor_name_short' => $contractorNameShort,
            'contractor_name_full' => $contractorNameFull,
            'contractor_director_name' => 'director_name',
            'contractor_director_post_name' => 'director_post_name',
            'contractor_address_legal_full' => $contractorAddressLegalFull,
            'contractor_inn' => 'ITN',
            'contractor_kpp' => 'PPC',
            'contractor_rs' => 'current_account',
            'contractor_bank_swift' => 'BIC',
            'contractor_bank_name' => 'bank_name',
            'contractor_bank_address' => 'bank_address',
            'contractor_corr_rs' => 'corresp_account',
            'contractor_corr_bank_swift' => 'corr_bank_swift',
            'contractor_corr_bank_name' => 'corr_bank_name',
            'contractor_corr_bank_address' => 'corr_bank_address',
        ];
        $defaultContractorFields = $contractor->fields();
        foreach ($contractorFields as $invoiceContractorField => $contractorField) {
            if (in_array($contractorField, $defaultContractorFields)) {
                if ($this->$invoiceContractorField !== $contractor->$contractorField) {
                    if ($this->$invoiceContractorField === "" && $contractor->$contractorField === null)
                        continue;

                    return true;
                }
            } elseif ($this->$invoiceContractorField !== $contractorField) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param $class
     * @return static
     */
    public function getPaymentType()
    {
        if ($this->getCashBankFlows()->exists()) {
            return 1;
        }
        if ($this->getCashOrderFlows()->exists()) {
            return 2;
        }
        if ($this->getCashEmoneyFlows()->exists()) {
            return 3;
        }

        return 0;
    }

    /**
     * @return string
     */
    public function getPaymentIcon()
    {
        $type = $this->getPaymentType();

        if (isset(Invoice::$invoicePaymentIcon[$type])) {
            return Invoice::$invoicePaymentIcon[$type];
        }

        return '';
    }

    /**
     * @return string
     */
    public function getPaymentLabel()
    {
        $type = $this->getPaymentType();

        if (isset(Invoice::$invoicePaymentLabel[$type])) {
            return Invoice::$invoicePaymentLabel[$type];
        }

        return '';
    }

    /**
     * @param $employeeCompany
     */
    public function setResponsible(EmployeeCompany $employeeCompany) : bool
    {
        if ($this->company_id == $employeeCompany->company_id) {
            if ($this->updateAttributes(['responsible_employee_id' => $employeeCompany->employee_id])) {
                $this->populateRelation('responsible', $employeeCompany);
                LogHelper::log($this, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_RESPONSIBLE);

                return true;
            }
        }

        return false;
    }
}
