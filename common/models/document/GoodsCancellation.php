<?php

namespace common\models\document;

use Yii;
use common\models\Company;
use common\models\Contractor;
use common\models\TaxRate;
use common\models\product\Product;
use common\models\product\Store;
use common\models\project\Project;
use common\models\employee\Employee;
use common\models\company\DetailsFile;
use common\models\project\ProjectEstimate;
use common\models\employee\EmployeeSignature;
use common\models\currency\Currency;
use frontend\models\log\LogEvent;
use frontend\models\Documents;
use common\models\file\File;
use frontend\models\log\Log;
use frontend\models\log\LogHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use common\components\date\DateHelper;
use common\components\pdf\PdfRenderer;
use common\components\TextHelper;
use common\models\EmployeeCompany;
use common\models\company\CompanyIndustry;
use common\models\companyStructure\SalePoint;
use frontend\modules\documents\assets\DocumentPrintAsset;
use frontend\modules\documents\components\Message;
use frontend\modules\documents\components\GoodsCancellationHelper;

/**
 * This is the model class for table "goods_cancellation".
 *
 * @property int $id
 * @property int $company_id
 * @property int $store_id
 * @property int $profit_loss_type
 * @property int|null $contractor_id
 * @property int|null $project_id
 * @property int|null $sale_point_id
 * @property int|null $industry_id
 * @property int|null $project_estimate_id
 * @property int|null $expenditure_item_id
 * @property string $document_date
 * @property int|null $document_number
 * @property string|null $document_additional_number
 * @property int $status_id
 * @property int $nds_view_type_id
 * @property int|null $signed_by_employee_id
 * @property string|null $signed_by_name
 * @property int|null $sign_document_type_id
 * @property string|null $sign_document_number
 * @property string|null $sign_document_date
 * @property int|null $signature_id
 * @property int|null $print_id
 * @property int|null $chief_signature_id
 * @property int|null $chief_accountant_signature_id
 * @property int $document_author_id
 * @property int $responsible_employee_id
 * @property int $created_at
 * @property string|null $comment
 * @property int $price_precision
 *
 * @property DetailsFile $chiefAccountantSignature
 * @property DetailsFile $chiefSignature
 * @property Contractor $contractor
 * @property Employee $documentAuthor
 * @property OrderGoodsCancellation[] $orders
 * @property DetailsFile $print
 * @property Project $project
 * @property ProjectEstimate $projectEstimate
 * @property CompanyIndustry $industry
 * @property SalePoint $salePoint
 * @property DocumentType $signDocumentType
 * @property EmployeeSignature $signature
 * @property Employee $signedByEmployee
 * @property GoodsCancellationStatus $status
 * @property Store $store
 * @property Company $company
 * @property InvoiceExpenditureItem $expenditureItem
 * @property integer $totalAmountWithNds
 */
class GoodsCancellation extends AbstractDocument
{
    const VARIABLE_EXPENSE_TYPE   = 1; // ProfitAndLossCompanyItem::VARIABLE_EXPENSE_TYPE
    const CONSTANT_EXPENSE_TYPE   = 2; // ProfitAndLossCompanyItem::CONSTANT_EXPENSE_TYPE
    const OPERATING_EXPENSE_TYPE  = 3; // not used
    const OTHER_EXPENSE_TYPE      = 4; // ProfitAndLossCompanyItem::OTHER_EXPENSE_TYPE
    const PRIME_COST_EXPENSE_TYPE = 5; // ProfitAndLossCompanyItem::PRIME_COST_EXPENSE_TYPE
    const UNSET_EXPENSE_TYPE     = 0;

    /**
     * @var array
     */
    public static $precisions = ['2', '4'];

    public $printablePrefix = 'Списание товара';
    public static $uploadDirectory = 'goods_cancellation';
    public $urlPart = 'goods-cancellation';

    public static $expenditureItemList = [
        InvoiceExpenditureItem::ITEM_GOODS_CANCELLATION => 'Списание товара'
    ];

    public static $profitLossTypeItemList = [
        self::PRIME_COST_EXPENSE_TYPE => 'Себестоимость',
        self::VARIABLE_EXPENSE_TYPE => 'Переменные расходы',
        self::CONSTANT_EXPENSE_TYPE => 'Постоянные расходы',
        //self::OPERATING_EXPENSE_TYPE => 'Операционные расходы',
        self::OTHER_EXPENSE_TYPE => 'Другие расходы',
        self::UNSET_EXPENSE_TYPE => 'Не указан'
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'goods_cancellation';
    }

    /**
     * @param null $ioType
     * @return GoodsCancellation
     * @throws Exception
     */
    public static function newModel(EmployeeCompany $employeeCompany, Store $store = null, array $attributes = [])
    {
        /** @var Employee $employee */
        $employee = $employeeCompany->employee;
        /** @var Company $company */
        $company = $employeeCompany->company;
        /** @var GoodsCancellation $model */
        $model = new GoodsCancellation();

        $model->company_id = $company->id;
        $model->document_date = date(DateHelper::FORMAT_DATE);
        $model->nds_view_type_id = $company->nds_view_type_id;
        $model->document_number = (string)GoodsCancellation::getNextDocumentNumber($company->id, null, $model->document_date);
        $model->document_author_id = $employee->id;
        $model->responsible_employee_id = $employee->id;
        $model->profit_loss_type = GoodsCancellation::PRIME_COST_EXPENSE_TYPE;
        $model->price_precision = 2;
        $model->created_at = time();
        $model->expenditure_item_id = InvoiceExpenditureItem::ITEM_GOODS_CANCELLATION;

        $model->populateRelation('company', $company);

        // Set user store
        if ($store === null) {
            $store = $employee->getStores()->orderBy(['is_main' => SORT_DESC])->limit(1)->one();
        }
        if ($store !== null) {
            $model->store_id = $store->id;
        } else {
            throw new Exception('Нет доступных складов');
        }
        $model->load($attributes, '');

        return $model;
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'store_id', 'profit_loss_type', 'document_date', 'document_author_id', 'expenditure_item_id', 'created_at'], 'required'],
            [['company_id', 'store_id', 'profit_loss_type', 'contractor_id', 'expenditure_item_id', 'document_number', 'status_id', 'nds_view_type_id', 'document_author_id', 'responsible_employee_id'], 'integer'],
            [['project_id', 'project_estimate_id', 'sale_point_id', 'industry_id'], 'integer'],
            [['project_id', 'sale_point_id', 'industry_id'], 'filter', 'filter' => function ($value) {
                return intval($value) ?: null;
            }],
            [['document_date', 'sign_document_date', 'created_at'], 'safe'],
            [['document_additional_number'], 'string', 'max' => 45],
            [['signed_by_name'], 'string', 'max' => 255],
            [['sign_document_number'], 'string', 'max' => 50],
            [['comment'], 'string', 'max' => 1000],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => GoodsCancellationStatus::class, 'targetAttribute' => ['status_id' => 'id']],
            [['contractor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contractor::class, 'targetAttribute' => ['contractor_id' => 'id']],
            [['document_author_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::class, 'targetAttribute' => ['document_author_id' => 'id']],
            [['responsible_employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::class, 'targetAttribute' => ['responsible_employee_id' => 'id']],
            [['project_id'], 'exist', 'skipOnError' => true, 'targetClass' => Project::class, 'targetAttribute' => ['project_id' => 'id']],
            [['sale_point_id'], 'exist', 'skipOnError' => true, 'targetClass' => SalePoint::class, 'targetAttribute' => ['sale_point_id' => 'id']],
            [['industry_id'], 'exist', 'skipOnError' => true, 'targetClass' => CompanyIndustry::class, 'targetAttribute' => ['industry_id' => 'id']],
            [['project_estimate_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProjectEstimate::class, 'targetAttribute' => ['project_estimate_id' => 'id']],
            [['store_id'], 'exist', 'skipOnError' => true, 'targetClass' => Store::class, 'targetAttribute' => ['store_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['company_id' => 'id']],
            [['expenditure_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => InvoiceExpenditureItem::class, 'targetAttribute' => ['expenditure_item_id' => 'id']],
            [['price_precision'], 'in', 'range' => self::$precisions],
            [
                ['orders'], 'required',
                'message' => 'Не найдено ни одного товара.',
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Компания',
            'store_id' => 'Со склада',
            'profit_loss_type' => 'Раздел ОПиУ',
            'contractor_id' => 'Под покупателя',
            'project_id' => 'Проект',
            'project_estimate_id' => 'Смета',
            'expenditure_item_id' => 'Статья расходов',
            'document_date' => 'Дата',
            'document_number' => 'Номер',
            'document_additional_number' => 'Доп. номер',
            'status_id' => 'Статус',
            'nds_view_type_id' => 'Вид НДС',
            'document_author_id' => 'Автор',
            'responsible_employee_id' => 'Ответственный',
            'comment' => 'Комментарий',
            'price_precision' => 'Количество знаков после запятой',

        ];
    }

    /**
     * @var $config
     * @return query\GoodsCancellationQuery
     */
    public static function find($config = [])
    {
        return new query\GoodsCancellationQuery(get_called_class(), $config);
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        // calculate total amount
        $this->calculateTotalAmount();

        return parent::beforeValidate();
    }

    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            if ($this->getHasRelated()) {
                return false;
            }
            foreach ($this->orders as $order) {
                if (!$order->delete()) {
                    return false;
                }
            }

            return true;
        }

        return false;
    }

    /**
     * Gets query for [[ChiefAccountantSignature]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getChiefAccountantSignature()
    {
        return $this->hasOne(DetailsFile::class, ['id' => 'chief_accountant_signature_id']);
    }

    /**
     * Gets query for [[ChiefSignature]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getChiefSignature()
    {
        return $this->hasOne(DetailsFile::class, ['id' => 'chief_signature_id']);
    }

    /**
     * Gets query for [[Contractor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getContractor()
    {
        return $this->hasOne(Contractor::class, ['id' => 'contractor_id']);
    }

    /**
     * Gets query for [[DocumentAuthor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentAuthor()
    {
        return $this->hasOne(Employee::class, ['id' => 'document_author_id']);
    }

    /**
     * Gets query for [[Responsible]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getResponsible()
    {
        return $this->hasOne(Employee::class, ['id' => 'responsible_employee_id']);
    }

    /**
     * Gets query for [[Orders]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(OrderGoodsCancellation::class, ['goods_cancellation_id' => 'id']);
    }

    /**
     * Gets query for [[Print]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getPrint()
    {
        return $this->hasOne(DetailsFile::class, ['id' => 'print_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProject()
    {
        return $this->hasOne(Project::class, ['id' => 'project_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProjectEstimate()
    {
        return $this->hasOne(ProjectEstimate::class, ['id' => 'project_estimate_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getIndustry()
    {
        return $this->hasOne(CompanyIndustry::class, ['id' => 'industry_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalePoint()
    {
        return $this->hasOne(SalePoint::class, ['id' => 'sale_point_id']);
    }

    /**
     * Gets query for [[SignDocumentType]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSignDocumentType()
    {
        return $this->hasOne(DocumentType::class, ['id' => 'sign_document_type_id']);
    }

    /**
     * Gets query for [[Signature]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSignature()
    {
        return $this->hasOne(EmployeeSignature::class, ['id' => 'signature_id']);
    }

    /**
     * Gets query for [[SignedByEmployee]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSignedByEmployee()
    {
        return $this->hasOne(Employee::class, ['id' => 'signed_by_employee_id']);
    }

    /**
     * Gets query for [[Status]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(GoodsCancellationStatus::class, ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpenditureItem()
    {
        return $this->hasOne(InvoiceExpenditureItem::className(), ['id' => 'expenditure_item_id']);
    }

    /**
     * Gets query for [[Store]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStore()
    {
        return $this->hasOne(Store::class, ['id' => 'store_id']);
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSignBasisDocument()
    {
        return $this->hasOne(DocumentType::className(), ['id' => 'sign_document_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSignEmployeeCompany()
    {
        return $this->hasOne(EmployeeCompany::className(), ['employee_id' => 'signed_by_employee_id'])
            ->andWhere(['company_id' => $this->company_id]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActGoodsCancellations()
    {
        return $this->hasMany(ActGoodsCancellation::className(), ['cancellation_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPackingListGoodsCancellations()
    {
        return $this->hasMany(PackingListGoodsCancellation::className(), ['cancellation_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdGoodsCancellations()
    {
        return $this->hasMany(UpdGoodsCancellation::className(), ['cancellation_id' => 'id']);
    }

    /**
     * @return bool
     */
    public function getHasRelated()
    {
        return $this->getActGoodsCancellations()->union($this->getUpdGoodsCancellations())->exists();
    }

    /**
     * @return int
     */
    public function getType()
    {
        return null;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentStatus()
    {
        return $this->getStatus();
    }

    /**
     * @return integer
     */
    public function getTotalAmountWithNds()
    {
        return $this->total_amount_with_nds;
    }

    /**
     * @param $company
     * @param null $number
     * @param null $date
     * @return int|null
     */
    public static function getNextDocumentNumber($company, $number = null, $date = null)
    {
        $companyId = ($company instanceof Company) ? $company->id : $company;

        $numQuery = self::find()->where(['company_id' => $companyId]);
        $year = $date ? date('Y', strtotime($date)) : null;
        if ($year) {
            $numQuery->andWhere([
                'between',
                'document_date',
                $year.'-01-01',
                $year.'-12-31',
            ]);
        }

        $numQuery->having('CHAR_LENGTH(`document_number`) <= ' . AbstractDocument::MAX_INCREMENTED_DOCUMENT_NUMBER_LENGTH);

        $lastNumber = $number ? $number : (int)$numQuery->max('(document_number * 1)');
        $nextNumber = 1 + $lastNumber;

        $existNumQuery = static::find()
            ->select('document_number')
            ->where(['company_id' => $companyId]);

        if ($year) {
            $existNumQuery->andWhere([
                'between',
                'document_date',
                $year.'-01-01',
                $year.'-12-31',
            ]);
        }

        $numArray = $existNumQuery->column();

        if (in_array($nextNumber, $numArray)) {
            return static::getNextDocumentNumber($company, $nextNumber, $date);
        } else {
            return $nextNumber;
        }
    }

    /**
     * @inheritdoc
     */
    public function getFullNumber()
    {
        if ($this->document_number !== null) {
            if ($this->company->is_additional_number_before) {
                $documentNumber = $this->document_additional_number . $this->document_number;
            } else {
                $documentNumber = $this->document_number . $this->document_additional_number;
            }
        } else {
            $documentNumber = null;
        }

        return $documentNumber;
    }

    /**
     * @inheritdoc
     */
    public function getFile()
    {
        return $this->hasOne(File::class, ['owner_id' => 'id'])
            ->onCondition(['owner_model' => GoodsCancellation::class]);
    }

    /**
     * Get formatted log message from Log model
     * @param Log $log
     * @return string
     */
    public function getLogMessage(Log $log)
    {
        $title = 'Списание товара №';

        $ioType = $log->getModelAttributeNew('type');
        if ($ioType == Documents::IO_TYPE_IN) {
            $title .= $log->getModelAttributeNew('document_number');
        } else {
            $title .= $log->getModelAttributeNew('document_number') . ' ' . $log->getModelAttributeNew('document_additional_number');
        }

        $link = Html::a($title, [
            '/documents/goods-cancellation/view',
            'type' => $log->getModelAttributeNew('type'),
            'id' => $log->getModelAttributeNew('id'),
        ]);

        switch ($log->log_event_id) {
            case LogEvent::LOG_EVENT_CREATE:
                return $link . ' был создан.';
            case LogEvent::LOG_EVENT_DELETE:
                return $title . ' был удален.';
            case LogEvent::LOG_EVENT_UPDATE:
                return $link . ' был изменен.';
            case LogEvent::LOG_EVENT_UPDATE_STATUS:
                return $link . ' статус "' . GoodsCancellationStatus::findOne($log->getModelAttributeNew('status_id'))->name . '"';
            default:
                return $log->message;
        }
    }

    /**
     * @return boolean
     */
    public function getHasNds()
    {
        return $this->nds_view_type_id != NdsViewType::NDS_VIEW_WITHOUT;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNdsViewType()
    {
        return $this->hasOne(NdsViewType::class, ['id' => 'nds_view_type_id']);
    }

    /**
     * @inheritdoc
     */
    public function calculateTotalAmount()
    {
        $this->view_total_base =
        $this->view_total_amount =
        $this->view_total_no_nds =
        $this->view_total_nds =
        $this->view_total_with_nds = 0;

        $this->total_amount_with_nds =
        $this->total_amount_nds =
        $this->total_amount_no_nds =
        $this->total_amount_has_nds = 0;


        $this->total_order_count = count($this->orders);

        foreach ($this->orders as $order) {
            $this->view_total_base += $order->view_total_base;
            $this->view_total_amount += $order->view_total_amount;
            $this->view_total_no_nds += $order->view_total_no_nds;
            $this->view_total_nds += $order->view_total_nds;
            $this->view_total_with_nds += $order->view_total_with_nds;
            $this->total_amount_with_nds += $order->amount_purchase_with_vat;
            $this->total_amount_nds += $order->purchase_tax;
            $this->total_amount_no_nds += $order->amount_purchase_no_vat;
        }

        $this->view_total_base = round($this->view_total_base);
        $this->view_total_amount = round($this->view_total_amount);
        $this->view_total_no_nds = round($this->view_total_no_nds);
        $this->view_total_nds = round($this->view_total_nds);
        $this->view_total_with_nds = round($this->view_total_with_nds);

        $this->total_amount_with_nds = round($this->total_amount_with_nds);
        $this->total_amount_nds = round($this->total_amount_nds);
        $this->total_amount_no_nds = round($this->total_amount_no_nds);
        $this->total_amount_has_nds = $this->total_amount_nds > 0;
    }

    /**
     * @return string
     */
    public function getEmailSubject()
    {
        return 'Списание товара № ' . $this->fullNumber
            . ' от ' . DateHelper::format($this->document_date, 'd.m.y', DateHelper::FORMAT_DATE)
            . ' от ' . $this->company->getShortName();
    }

    /**
     * @return string
     */
    public function getEmailText()
    {
        $document_date = DateHelper::format($this->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
        $sum = TextHelper::invoiceMoneyFormat($this->view_total_with_nds, 2);

        $text = <<<EMAIL_TEXT
Здравствуйте!

Списание товара № {$this->fullNumber} от {$document_date} на сумму {$sum} р.
EMAIL_TEXT;

        return $text;
    }

    /**
     * @return array
     * @throws \Mpdf\MpdfException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function loadEmailFiles()
    {
        $files = [];

        $emailFile = new EmailFile();
        $emailFile->company_id = $this->company_id;
        $emailFile->file_name = str_replace(['\\', '/'], '_', $this->pdfFileName);
        $emailFile->ext = 'pdf';
        $emailFile->mime = 'application/pdf';
        $emailFile->size = 0;
        $emailFile->type = EmailFile::TYPE_GOODS_CANCELLATION;
        if ($emailFile->save()) {
            $path = $emailFile->getFilePath() . DIRECTORY_SEPARATOR . $emailFile->file_name;
            $size = file_put_contents($path, self::getRenderer(null, $this, PdfRenderer::DESTINATION_STRING)->output(false));
            $emailFile->size = ceil($size / 1024);
            if ($emailFile->save(true, ['size'])) {
                $files[] = [
                    'id' => $emailFile->id,
                    'name' => $emailFile->file_name,
                    'previewImg' => '<img src="/img/email/emailPdf.png" class="preview-img">',
                    'size' => $emailFile->size,
                    'deleteUrl' => Url::to(['/email/delete-email-file', 'id' => $emailFile->id]),
                    'downloadUrl' => Url::to(['/email/download-file', 'id' => $emailFile->id]),
                ];
            }
        }

        return $files;
    }

    public static function cancellationByAct(EmployeeCompany $employeeCompany, Act $document)
    {
        if ($document->type == Documents::IO_TYPE_OUT) {
            self::cancellationByDocument($employeeCompany, $document, $document->orderActs);
        }
    }

    public static function cancellationByUpd(EmployeeCompany $employeeCompany, Upd $document)
    {
        if ($document->type == Documents::IO_TYPE_OUT) {
            self::cancellationByDocument($employeeCompany, $document, $document->orderUpds);
        }
    }

    public static function ordersDataFromPackingList(PackingList $document)
    {
        $ordersData = [];
        foreach ($document->orderPackingLists as $orderPL) {
            $product = $orderPL->product;
            $ordersData[] = [
                'title' => $product->title,
                'id' => '',
                'product_id' => $product->id,
                'max_product_count' => '1',
                'count' => $orderPL->quantity,
                'unit_id' => $product->product_unit_id,
                'purchase_tax_rate_id' => $product->price_for_buy_nds_id ?: TaxRate::RATE_WITHOUT,
                'price' => bcdiv($product->price_for_buy_with_nds, 100, 2),
                'discount' => '0',
                'markup' => '0',
                'weight' => '',
                'volume' => '',
            ];
        }

        return $ordersData;
    }

    protected static function cancellationByDocument(EmployeeCompany $employeeCompany, AbstractDocument $document, array $orderArray)
    {
        $cancellations = [];
        foreach ($orderArray as $order) {
            $product = $order->product;
            if ($product && $product->unit_type == Product::UNIT_TYPE_COMPOSITE) {
                $compositeMaps = $product->compositeMaps;
                foreach ($compositeMaps as $map) {
                    $simpleProduct = $map->simpleProduct;
                    if ($simpleProduct->production_type == Product::PRODUCTION_TYPE_GOODS) {
                        $id = $simpleProduct->id;
                        if (!isset($cancellations[$id])) {
                            $cancellations[$id] = [
                                'product' => $simpleProduct,
                                'count' => 0,
                            ];
                        }
                        $cancellations[$id]['count'] += $map->quantity * $order->quantity;
                    }
                }
            }
        }
        if ($cancellations) {
            $ordersData = [];
            foreach ($cancellations as $key => $value) {
                $product = $value['product'];
                $ordersData[] = [
                    'title' => $product->title,
                    'id' => '',
                    'product_id' => $product->id,
                    'max_product_count' => '1',
                    'count' => $value['count'],
                    'unit_id' => $product->product_unit_id,
                    'purchase_tax_rate_id' => $product->price_for_buy_nds_id ?: TaxRate::RATE_WITHOUT,
                    'price' => bcdiv($product->price_for_buy_with_nds, 100, 2),
                    'discount' => '0',
                    'markup' => '0',
                    'weight' => '',
                    'volume' => '',
                ];
            }
            $model = $document->goodsCancellations[0] ?? self::newModel($employeeCompany, $employeeCompany->company->store);
            $model->contractor_id = $document->invoice->contractor_id;
            GoodsCancellationHelper::ordersLoad($model, $ordersData);
            $useTransaction = LogHelper::$useTransaction;
            LogHelper::$useTransaction = false;
            $isNewRecord = $model->isNewRecord;
            if (GoodsCancellationHelper::save($model)) {
                if ($isNewRecord) {
                    $document->link('goodsCancellations', $model);
                }
            } else {
                \common\components\helpers\ModelHelper::logErrors($model, __METHOD__);
            }
            LogHelper::$useTransaction = $useTransaction;
        }
    }

    /**
     * @param $fileName
     * @param $model
     * @return PdfRenderer
     */
    public static function getRenderer($fileName, GoodsCancellation $model, $destination = PdfRenderer::DESTINATION_FILE)
    {
        $renderer = new PdfRenderer([
            'view' => '@documents/views/goods-cancellation/pdf-view',
            'params' => array_merge([
                'model' => $model,
                'message' => new Message(Documents::DOCUMENT_GOODS_CANCELLATION, null),
                'ioType' => null,
                'addStamp' => true,
            ]),

            'destination' => $destination,
            'filename' => $fileName,
            'displayMode' => PdfRenderer::DISPLAY_MODE_FULLPAGE,
            'format' => 'A4-L',
        ]);
        \Yii::$app->controller->view->params['asset'] = DocumentPrintAsset::class;

        return $renderer;
    }

    // todo
    public function getHas_discount() { return false; }
    public function getHas_markup() { return false; }
    public function getHas_weight() { return false; }
    public function getHas_volume() { return false; }
    public function getDiscount_type() { return false; }
    public function getTotalWeight() { return 0; }
    public function getTotalVolume() { return 0; }
    public function getWeightPrecision() { return 3; }
    public function getVolumePrecision() { return 3; }
    public function getCurrency_name() { return Currency::DEFAULT_NAME; }
    public function getView_total_discount() { return 0; }
}
