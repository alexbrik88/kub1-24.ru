<?php

namespace common\models\document;

use Yii;

/**
 * This is the model class for table "foreign_currency_invoice_data".
 *
 * @property int $foreign_currency_invoice_id
 * @property string $key
 * @property string|null $value
 */
class ForeignCurrencyInvoiceData extends \yii\db\ActiveRecord
{
    public static $dataFields = [
        'comment',
        'comment_internal',
        'signed_by_name',
        'sign_document_type_id',
        'sign_document_number',
        'sign_document_date',
        'signature_id',
        //company
        'company_inn',
        'company_kpp',
        'company_name_full',
        'company_name_short',
        'company_rs',
        'company_iban',
        'company_bank_name',
        'company_bank_address',
        'company_bank_swift',
        'company_corr_rs',
        'company_corr_bank_name',
        'company_corr_bank_address',
        'company_corr_bank_swift',
        'company_egrip',
        'company_okpo',
        'company_phone',
        'company_address_legal_full',
        'company_chief_post_name',
        'company_chief_lastname',
        'company_chief_firstname',
        'company_chief_patronymic',
        'company_print_filename',
        'company_chief_signature_filename',
        'document_author_email',
        'document_author_phone',
        //contractor
        'contractor_inn',
        'contractor_kpp',
        'contractor_name_full',
        'contractor_name_short',
        'contractor_director_post_name',
        'contractor_director_name',
        'contractor_director_email',
        'contractor_director_phone',
        'contractor_rs',
        'contractor_bank_name',
        'contractor_bank_address',
        'contractor_bank_swift',
        'contractor_corr_rs',
        'contractor_corr_bank_name',
        'contractor_corr_bank_address',
        'contractor_corr_bank_swift',
        'contractor_address_legal_full',
        'contractor_passport',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'foreign_currency_invoice_data';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['value'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'foreign_currency_invoice_id' => 'Foreign Currency Invoice ID',
            'key' => 'Key',
            'value' => 'Value',
        ];
    }
}
