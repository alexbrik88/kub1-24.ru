<?php

namespace common\models\document;

use Yii;

/**
 * This is the model class for table "payment_type".
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 *
 * @property PaymentOrder[] $paymentOrders
 */
class PaymentType extends \yii\db\ActiveRecord
{
    const TYPE_0 = 1;
    const TYPE_PE = 2;
    const TYPE_PC = 3;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string'],
            [['code'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentOrders()
    {
        return $this->hasMany(PaymentOrder::className(), ['payment_type_id' => 'id']);
    }
}
