<?php

namespace common\models\document;

use common\models\address\Country;
use common\models\product\Product;
use common\models\product\ProductUnit;
use frontend\models\Documents;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "order_upd".
 *
 * @property integer $upd_id
 * @property integer $order_id
 * @property integer $quantity
 * @property integer $country_id
 * @property string $custom_declaration_number
 *
 * @property Order $order
 * @property Upd $upd
 */
class OrderUpd extends AbstractOrder
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_upd';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['quantity'], 'filter', 'filter' => function ($value) {
                return str_replace(",", ".", $value);
            },],
            [['quantity'], 'number', 'numberPattern' => '/^\d+(\.\d*)?$/', 'min' => 0.0000000001, 'max' => 9999999999],
            [['custom_declaration_number'], 'string', 'max' => 255],
            [['country_id'], 'required', 'when' => function ($model) {
                return !$model->isNewRecord;
            },],
            [['country_id'], 'exist', 'targetClass' => Country::className(), 'targetAttribute' => 'id'],
            //[['quantity'], function ($attribute, $params) {
            //    if ($this->hasErrors($attribute)) return;
            //    $title = 'для «' . $this->order->product_title . '»';
            //    $max = self::availableQuantity($this->order, $this->upd_id);
            //    if ($this->$attribute <= 0) {
            //        $this->addError($attribute, "Количество {$title} должно быть больше 0.");
            //    } elseif ($this->$attribute > $max) {
            //        $this->addError($attribute, "Максимальное возможное количество {$title}: {$max}.");
            //    }
            //}],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'upd_id' => 'Upd ID',
            'order_id' => 'Order ID',
            'quantity' => 'Количество',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id'])->via('order');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpd()
    {
        return $this->hasOne(Upd::className(), ['id' => 'upd_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocument()
    {
        return $this->getUpd();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @param int $upd_id
     * @param Order $order
     * @return OrderUpd|null
     */
    public static function createFromOrder($upd_id, Order $order)
    {
        if ($quantity = self::availableQuantity($order, $upd_id)) {
            $model = new OrderUpd([
                'upd_id' => $upd_id,
                'order_id' => $order->id,
                'product_id' => $order->product_id,
                'quantity' => $quantity,
                'country_id' => $order->country_id,
                'custom_declaration_number' => $order->custom_declaration_number,
            ]);

            return $model;
        }

        return null;
    }

    /**
     * @param Order $order
     * @param integer $upd_id
     * @return int|null
     */
    public static function availableQuantity(Order $order, $upd_id = null)
    {
        $ordersIds = Invoice::find()
            ->joinWith(['invoiceUpds', 'orders'])
            ->where(['order.product_id' => $order->product_id])
            ->andWhere(['invoice_upd.upd_id' => ArrayHelper::getColumn($order->invoice->upds, 'id')])
            ->select('order.id')
            ->distinct()
            ->column() ?: [$order->id];

        $orders_quantity = Order::find()->where(['id' => $ordersIds])->sum('quantity');

        $used_quantity = OrderUpd::find()
            ->andWhere(['order_id' => $ordersIds])
            ->andWhere(['not', ['upd_id' => $upd_id]])
            ->sum('quantity');

        $available = $orders_quantity - $used_quantity;

        return max(0, $available);
    }

    /**
     * @return integer
     */
    public function getPrecision()
    {
        return ($this->upd && $this->upd->invoice && $this->upd->invoice->price_precision == 4) ? 2: 0;
    }

    /**
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $order = $this->order;
                if (empty($this->country_id)) {
                    $this->country_id = $order->country_id;
                }
                if (empty($this->custom_declaration_number)) {
                    $this->custom_declaration_number = $order->custom_declaration_number;
                }
            }
            if ($this->product->production_type == Product::PRODUCTION_TYPE_GOODS) {
                if ($insert) {
                    if ($this->upd->type == Documents::IO_TYPE_IN) {
                        $this->product->setCountOrderBuy($this->quantity, $this->order->invoice->store_id);
                    } else {
                        $this->product->setCountOrderSale($this->quantity, $this->order->invoice->store_id);
                        $this->order->updateAttributes([
                            'reserve' => max(0, $this->order->reserve - $this->quantity),
                        ]);
                    }
                } else {
                    $quantityDiff = $this->quantity - $this->getOldAttribute('quantity');
                    if ($quantityDiff !== 0) {
                        if ($this->upd->type == Documents::IO_TYPE_IN) {
                            $this->product->setCountOrderBuy($quantityDiff, $this->order->invoice->store_id);
                        } else {
                            $this->product->setCountOrderSale($quantityDiff, $this->order->invoice->store_id);
                            $this->order->updateAttributes([
                                'reserve' => max(0, $this->order->reserve + $this->getOldAttribute('quantity') - $this->quantity),
                            ]);
                        }
                    }
                }
            }
            if (empty($this->custom_declaration_number)) {
                $this->custom_declaration_number = Product::DEFAULT_VALUE;
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            if ($this->upd->status_out_id != status\UpdStatus::STATUS_REJECTED) {
                $this->resetProductCount();
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        Order::setProductReserve($this->order_id);
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();

        Order::setProductReserve($this->order_id);
    }

    /**
     *
     */
    public function resetProductCount()
    {
        if ($this->upd->type == Documents::IO_TYPE_IN) {
            $this->product->setCountOrderSale($this->quantity, $this->order->invoice->store_id);
        } else {
            $this->product->setCountOrderBuy($this->quantity, $this->order->invoice->store_id);
        }
    }
}
