<?php

namespace common\models\document;

use Yii;

/**
 * This is the model class for table "upd_payment_document".
 *
 * @property integer $upd_id
 * @property string $payment_document_number
 * @property string $payment_document_date
 *
 * @property Upd $upd
 */
class UpdPaymentDocument extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'upd_payment_document';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['upd_id', 'payment_document_number', 'payment_document_date'], 'required'],
            [['upd_id'], 'integer'],
            [['payment_document_date'], 'safe'],
            [['payment_document_number'], 'string', 'max' => 255],
            [['upd_id'], 'exist', 'skipOnError' => true, 'targetClass' => Upd::className(), 'targetAttribute' => ['upd_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'upd_id' => 'Upd ID',
            'payment_document_number' => 'Payment Document Number',
            'payment_document_date' => 'Payment Document Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpd()
    {
        return $this->hasOne(Upd::className(), ['id' => 'upd_id']);
    }
}
