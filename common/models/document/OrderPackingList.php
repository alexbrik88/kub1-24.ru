<?php

namespace common\models\document;

use common\components\calculator\CalculatorHelper;
use common\models\product\Product;
use frontend\models\Documents;
use Yii;

/**
 * This is the model class for table "order_packing_list".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $packing_list_id
 * @property integer $product_id
 * @property integer $quantity
 *
 * @property Order $order
 * @property PackingList $packingList
 * @property Product $product
 */
class OrderPackingList extends AbstractOrder
{

    public static $SUM_TYPE = [
        '2' => 'sales',
        '1' => 'purchase',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_packing_list';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['quantity'], 'filter', 'filter' => function ($value) {
                return str_replace(",", ".", $value);
            },],
            [['quantity'], 'number', 'numberPattern' => '/^\d+(\.\d*)?$/'],
            [['quantity'], function ($attribute, $params) {
                if ($this->hasErrors($attribute)) return;
                $title = 'для «' . $this->order->product_title . '»';
                $max = self::availableQuantity($this->order, $this->packing_list_id);
                if ($this->$attribute <= 0) {
                    $this->addError($attribute, "Количество {$title} должно быть больше 0.");
                } elseif (bccomp($this->$attribute, $max, 4) > 0) {
                    $this->addError($attribute, "Максимальное возможное количество {$title}: {$max}.");
                }
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'packing_list_id' => 'Packing List ID',
            'product_id' => 'Product ID',
            'quantity' => 'Количество',
        ];
    }

    /**
     * @param $packing_list_id
     * @param Order $order
     * @return OrderPackingList
     */
    public static function createFromOrder($packing_list_id, Order $order)
    {
        if (OrderPackingList::compareWithOrderByQuantity($order) != 0 &&
            $order->product->production_type == Product::PRODUCTION_TYPE_GOODS
        ) {
            $order_packing_list = new OrderPackingList();
            $order_packing_list->order_id = $order->id;
            $order_packing_list->packing_list_id = $packing_list_id;
            $order_packing_list->product_id = $order->product_id;
            $order_packing_list->quantity = OrderPackingList::compareWithOrderByQuantity($order);

            return $order_packing_list;
        }

        return null;
    }

    /**
     * @param $pacingListId
     * @return mixed
     */
    public static function countByPackingList($pacingListId)
    {
        return count(OrderPackingList::findAll(['packing_list_id' => $pacingListId]));
    }

    /**
     * @param $order_id
     * @param $packing_list_id
     * @param $quantity
     */
    public static function setQuantity($order_id, $packing_list_id, $quantity)
    {
        $order_packing_list = OrderPackingList::findOne(['order_id' => $order_id, 'packing_list_id' => $packing_list_id]);
        $order = Order::findOne(['id' => $order_id]);
        if ($order_packing_list != null) {
            $order_packing_list->quantity = $quantity;
            $order_packing_list->save();
        }
    }

    /**
     * @param $order_id
     * @param $packing_list_id
     * @return bool
     */
    public static function ifExist($order_id, $packing_list_id)
    {
        return OrderPackingList::findOne(['order_id' => $order_id, 'packing_list_id' => $packing_list_id]) ? true : false;
    }

    /**
     * returns residual of order and sum of packing lists, if haven't returns null
     * @param Order $order
     * @return int
     */
    public static function compareWithOrderByQuantity(Order $order)
    {
        $query = OrderPackingList::find()->where(['order_id' => $order->id]);
        if ($query != null) {
            $packing_lists_quantity = $query->sum('quantity');
        } else {
            $packing_lists_quantity = 0;
        }
        if ($packing_lists_quantity == $order->quantity) {

            return 0;
        } elseif ($packing_lists_quantity < $order->quantity) {

            return $order->quantity - $packing_lists_quantity;
        } else {

            return null;
        }
    }

    /**
     * @param Invoice $invoice
     * @return int|mixed
     */
    public static function compareWithInvoiceBySum(Invoice $invoice)
    {
        $opl = 0;
        $ordersSum = 0;
        if ($invoice->orders != null) {
            foreach ($invoice->orders as $order) {
                if ($order->product->production_type == Product::PRODUCTION_TYPE_GOODS) {
                    $ordersSum += $order->quantity;
                    $opl += OrderPackingList::find()->where(['order_id' => $order->id])->sum('quantity');
                }
            }
        }
        if ($ordersSum > $opl) {
            return $ordersSum - $opl;
        } elseif ($ordersSum == $opl) {
            return 0;
        } else {
            return 0;
        }
    }

    /**
     * @param $packing_list_id
     * @return mixed
     */
    public static function allQuantitySum($packing_list_id)
    {
        return OrderPackingList::find()->where(['packing_list_id' => $packing_list_id])->sum('quantity');
    }

    public static function allPackingListSum($packing_list_id, $sum_type)
    {
        return OrderPackingList::find()->where(['packing_list_id' => $packing_list_id])->sum('amount_' . $sum_type . '_with_vat');
    }

    /**
     * @param $packing_list_id
     * @return array
     */
    public static function getAvailable($packing_list_id)
    {
        $packing_list = PackingList::findOne(['id' => $packing_list_id]);
        $invoice = Invoice::findOne(['id' => $packing_list->invoice_id]);
        $available = [];
        if ($invoice->orders != null) {
            foreach ($invoice->orders as $order) {
                if ($order->product->production_type == Product::PRODUCTION_TYPE_GOODS) {
                    $order_packing_list = OrderPackingList::findOne(['order_id' => $order->id, 'packing_list_id' => $packing_list_id]);
                    if ($order_packing_list == null && OrderPackingList::compareWithOrderByQuantity($order) != 0) {
                        $available[$order->product_title] = OrderPackingList::createFromOrder($packing_list_id, $order)->quantity;
                    }
                }
            }
        }

        return $available;
    }

    /**
     * @param Order $order
     * @param integer $packing_list_id
     * @return numder
     */
    public static function availableQuantity(Order $order, $packing_list_id = null)
    {
        $used_quantity = self::find()
            ->andWhere(['order_id' => $order->id])
            ->andWhere(['not', ['packing_list_id' => $packing_list_id]])
            ->sum('quantity');

        $available = $order->quantity - $used_quantity;

        return max(0, $available);
    }

    /**
     * @return int
     */
    public function getAvailableQuantity()
    {
        $order_quantity = $this->order->quantity;

        $packing_lists_quantity = OrderPackingList::find()->where(['order_id' => $this->order_id])->andWhere(['!=','id',$this->id])->sum('quantity');

        $result = $order_quantity - $packing_lists_quantity;

        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPackingList()
    {
        return $this->hasOne(PackingList::className(), ['id' => 'packing_list_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocument()
    {
        return $this->getPackingList();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                if ($this->packingList->type == Documents::IO_TYPE_IN) {
                    $this->product->setCountOrderBuy($this->quantity, $this->order->invoice->store_id);
                } else {
                    $this->product->setCountOrderSale($this->quantity, $this->order->invoice->store_id);
                }
            } else {
                $quantityDiff = $this->quantity - $this->getOldAttribute('quantity');
                if ($quantityDiff !== 0) {
                    if ($this->packingList->type == Documents::IO_TYPE_IN) {
                        $this->product->setCountOrderBuy($this->quantity - $this->getOldAttribute('quantity'), $this->order->invoice->store_id);
                    } else {
                        $this->product->setCountOrderSale($this->quantity - $this->getOldAttribute('quantity'), $this->order->invoice->store_id);
                    }
                }
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            if ($this->packingList->status_out_id != status\PackingListStatus::STATUS_REJECTED) {
                $this->resetProductCount();
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        Order::setProductReserve($this->order_id);
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();

        Order::setProductReserve($this->order_id);
    }

    /**
     *
     */
    public function resetProductCount()
    {
        if ($this->packingList->type == Documents::IO_TYPE_IN) {
            $this->product->setCountOrderSale($this->quantity, $this->order->invoice->store_id);
        } else {
            $this->product->setCountOrderBuy($this->quantity, $this->order->invoice->store_id);
        }
    }
}
