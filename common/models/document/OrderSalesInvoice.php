<?php

namespace common\models\document;

use common\components\calculator\CalculatorHelper;
use common\models\product\Product;
use frontend\models\Documents;
use Yii;

/**
 * This is the model class for table "order_sales_invoice".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $sales_invoice_id
 * @property integer $product_id
 * @property integer $quantity
 *
 * @property Order $order
 * @property SalesInvoice $salesInvoice
 * @property Product $product
 */
class OrderSalesInvoice extends AbstractOrder
{

    public static $SUM_TYPE = [
        '2' => 'sales',
        '1' => 'purchase',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_sales_invoice';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['quantity'], 'filter', 'filter' => function ($value) {
                return str_replace(",", ".", $value);
            },],
            [['quantity'], 'number', 'numberPattern' => '/^\d+(\.\d*)?$/'],
            [['quantity'], function ($attribute, $params) {
                if ($this->hasErrors($attribute)) return;
                $title = 'для «' . $this->order->product_title . '»';
                $max = self::availableQuantity($this->order, $this->sales_invoice_id);
                if ($this->$attribute <= 0) {
                    $this->addError($attribute, "Количество {$title} должно быть больше 0.");
                } elseif (bccomp($this->$attribute, $max, 4) > 0) {
                    $this->addError($attribute, "Максимальное возможное количество {$title}: {$max}.");
                }
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'sales_invoice_id' => 'Sales Invoice ID',
            'product_id' => 'Product ID',
            'quantity' => 'Количество',
        ];
    }

    /**
     * @inheritDoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                if ($this->salesInvoice->type == Documents::IO_TYPE_IN) {
                    $this->product->setCountOrderBuy($this->quantity, $this->order->invoice->store_id);
                } else {
                    $this->product->setCountOrderSale($this->quantity, $this->order->invoice->store_id);
                }
            } else {
                $quantityDiff = $this->quantity - $this->getOldAttribute('quantity');
                if ($quantityDiff !== 0) {
                    if ($this->salesInvoice->type == Documents::IO_TYPE_IN) {
                        $this->product->setCountOrderBuy($this->quantity - $this->getOldAttribute('quantity'), $this->order->invoice->store_id);
                    } else {
                        $this->product->setCountOrderSale($this->quantity - $this->getOldAttribute('quantity'), $this->order->invoice->store_id);
                    }
                }
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            if ($this->salesInvoice->status_out_id != status\SalesInvoiceStatus::STATUS_REJECTED) {
                $this->resetProductCount();
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        Order::setProductReserve($this->order_id);
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();

        Order::setProductReserve($this->order_id);
    }


    /**
     * @param $sales_invoice_id
     * @param Order $order
     * @return OrderSalesInvoice
     */
    public static function createFromOrder($sales_invoice_id, Order $order)
    {
        if (OrderSalesInvoice::compareWithOrderByQuantity($order) != 0 &&
            $order->product->production_type == Product::PRODUCTION_TYPE_GOODS
        ) {
            $order_sales_invoice = new OrderSalesInvoice();
            $order_sales_invoice->order_id = $order->id;
            $order_sales_invoice->sales_invoice_id = $sales_invoice_id;
            $order_sales_invoice->product_id = $order->product_id;
            $order_sales_invoice->quantity = OrderSalesInvoice::compareWithOrderByQuantity($order);

            return $order_sales_invoice;
        }

        return null;
    }

    /**
     * @param $pacingListId
     * @return mixed
     */
    public static function countBySalesInvoice($pacingListId)
    {
        return count(OrderSalesInvoice::findAll(['sales_invoice_id' => $pacingListId]));
    }

    /**
     * @param $order_id
     * @param $sales_invoice_id
     * @param $quantity
     */
    public static function setQuantity($order_id, $sales_invoice_id, $quantity)
    {
        $order_sales_invoice = OrderSalesInvoice::findOne(['order_id' => $order_id, 'sales_invoice_id' => $sales_invoice_id]);
        $order = Order::findOne(['id' => $order_id]);
        if ($order_sales_invoice != null) {
            $order_sales_invoice->quantity = $quantity;
            $order_sales_invoice->save();
        }
    }

    /**
     * @param $order_id
     * @param $sales_invoice_id
     * @return bool
     */
    public static function ifExist($order_id, $sales_invoice_id)
    {
        return OrderSalesInvoice::findOne(['order_id' => $order_id, 'sales_invoice_id' => $sales_invoice_id]) ? true : false;
    }

    /**
     * returns residual of order and sum of sales invoice, if haven't returns null
     * @param Order $order
     * @return int
     */
    public static function compareWithOrderByQuantity(Order $order)
    {
        $query = OrderSalesInvoice::find()->where(['order_id' => $order->id]);

        if ($query != null) {
            $sales_invoices_quantity = $query->sum('quantity');
        } else {
            $sales_invoices_quantity = 0;
        }

        if ($sales_invoices_quantity == $order->quantity) {
            return 0;
        } elseif ($sales_invoices_quantity < $order->quantity) {
            return $order->quantity - $sales_invoices_quantity;
        } else {
            return null;
        }

    }

    /**
     * @param Invoice $invoice
     * @return int|mixed
     */
    public static function compareWithInvoiceBySum(Invoice $invoice)
    {
        $opl = 0;
        $ordersSum = 0;
        if ($invoice->orders != null) {
            foreach ($invoice->orders as $order) {
                if ($order->product->production_type == Product::PRODUCTION_TYPE_GOODS) {
                    $ordersSum += $order->quantity;
                    $opl += OrderSalesInvoice::find()->where(['order_id' => $order->id])->sum('quantity');
                }
            }
        }
        if ($ordersSum > $opl) {
            return $ordersSum - $opl;
        } elseif ($ordersSum == $opl) {
            return 0;
        } else {
            return 0;
        }
    }

    /**
     * @param $sales_invoice_id
     * @return mixed
     */
    public static function allQuantitySum($sales_invoice_id)
    {
        return OrderSalesInvoice::find()->where(['sales_invoice_id' => $sales_invoice_id])->sum('quantity');
    }

    public static function allSalesInvoiceSum($sales_invoice_id, $sum_type)
    {
        return OrderSalesInvoice::find()->where(['sales_invoice_id' => $sales_invoice_id])->sum('amount_' . $sum_type . '_with_vat');
    }

    /**
     * @param $sales_invoice_id
     * @return array
     */
    public static function getAvailable($sales_invoice_id)
    {
        $sales_invoice = SalesInvoice::findOne(['id' => $sales_invoice_id]);
        $invoice = Invoice::findOne(['id' => $sales_invoice->invoice_id]);
        $available = [];
        if ($invoice->orders != null) {
            foreach ($invoice->orders as $order) {
                if ($order->product->production_type == Product::PRODUCTION_TYPE_GOODS) {
                    $order_sales_invoice = OrderSalesInvoice::findOne(['order_id' => $order->id, 'sales_invoice_id' => $sales_invoice_id]);
                    if ($order_sales_invoice == null && OrderSalesInvoice::compareWithOrderByQuantity($order) != 0) {
                        $available[$order->product_title] = OrderSalesInvoice::createFromOrder($sales_invoice_id, $order)->quantity;
                    }
                }
            }
        }

        return $available;
    }

    /**
     * @param Order $order
     * @param null $sales_invoice_id
     * @return mixed
     */
    public static function availableQuantity(Order $order, $sales_invoice_id = null)
    {
        $used_quantity = self::find()
            ->andWhere(['order_id' => $order->id])
            ->andWhere(['not', ['sales_invoice_id' => $sales_invoice_id]])
            ->sum('quantity');

        $available = $order->quantity - $used_quantity;

        return max(0, $available);
    }

    /**
     * @return int
     */
    public function getAvailableQuantity()
    {
        $order_quantity = $this->order->quantity;

        $sales_invoices_quantity = OrderSalesInvoice::find()->where(['order_id' => $this->order_id])->andWhere(['!=','id',$this->id])->sum('quantity');

        $result = $order_quantity - $sales_invoices_quantity;

        return $result;
    }

    public function resetProductCount()
    {
        if ($this->salesInvoice->type == Documents::IO_TYPE_IN) {
            $this->product->setCountOrderSale($this->quantity, $this->order->invoice->store_id);
        } else {
            $this->product->setCountOrderBuy($this->quantity, $this->order->invoice->store_id);
        }
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSalesInvoice()
    {
        return $this->hasOne(SalesInvoice::className(), ['id' => 'sales_invoice_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocument()
    {
        return $this->getSalesInvoice();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }
}
