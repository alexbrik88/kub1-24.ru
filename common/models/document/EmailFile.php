<?php

namespace common\models\document;

use common\components\ImageHelper;
use Yii;
use common\models\Company;
use yii\helpers\FileHelper;
use yii\helpers\Url;

/**
 * This is the model class for table "email_file".
 *
 * @property integer $id
 * @property integer $company_id
 * @property string $file_name
 * @property string $ext
 * @property string $mime
 * @property string $size
 * @property integer $type
 * @property integer $act_id
 *
 * @property Company $company
 * @property Act $act
 */
class EmailFile extends \yii\db\ActiveRecord
{
    const TYPE_INVOICE_DOCUMENT = 1;
    const TYPE_ACT_DOCUMENT = 2;
    const TYPE_PACKING_LIST_DOCUMENT = 3;
    const TYPE_INVOICE_FACTURE_DOCUMENT = 4;
    const TYPE_UPD_DOCUMENT = 5;
    const TYPE_INVOICE_PAYMENT_ORDER = 6;
    const TYPE_ONE_C_FILE = 7;
    const TYPE_AGREEMENT_DOCUMENT = 11;
    const TYPE_WAYBILL_DOCUMENT = 12;
    const TYPE_PROXY = 13;
    const TYPE_AGENT_REPORT = 14;
    const TYPE_ORDER_DOCUMENT = 15;
    const TYPE_COMPANIES_REPORT = 16;
    const TYPE_FOREIGN_CURRENCY_INVOICE = 17;
    const TYPE_SALES_INVOICE = 18;
    const TYPE_PROJECT_ESTIMATE = 19;
    const TYPE_GOODS_CANCELLATION = 20;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'email_file';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'file_name',], 'required'],
            [['company_id', 'size', 'type', 'act_id'], 'integer'],
            [['file_name', 'mime', 'ext',], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['act_id'], 'exist', 'skipOnError' => true, 'targetClass' => Act::className(), 'targetAttribute' => ['act_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'file_name' => 'File Name',
            'mime' => 'Mime',
            'ext' => 'Ext',
            'size' => 'Size',
            'type' => 'Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAct()
    {
        return $this->hasOne(Act::className(), ['id' => 'act_id']);
    }

    /**
     * @return string
     */
    public function getFilePath()
    {
        $basePath = Yii::getAlias(Yii::$app->params['uploadDir']) . DIRECTORY_SEPARATOR . self::tableName() . DIRECTORY_SEPARATOR . $this->id;
        if (!file_exists($basePath)) {
            mkdir($basePath);
        }

        return $basePath;
    }

    /**
     * @return array
     */
    public function loadFile()
    {
        $size = $_FILES['emailFile']['size'];
        if ($size <= 1 * 1024 * 1024) {
            $fileName = $_FILES['emailFile']['name'];
            $ext = mb_strtolower(pathinfo($fileName, PATHINFO_EXTENSION));
            $this->file_name = $fileName;
            $this->ext = $ext;
            $this->size = ceil($size / 1024);
            if ($this->save()) {
                $downloadUrl = null;
                $preloadPath = $this->getFilePath();
                if (move_uploaded_file($_FILES['emailFile']['tmp_name'], $preloadPath . DIRECTORY_SEPARATOR . $fileName)) {
                    $this->mime = mime_content_type($preloadPath . DIRECTORY_SEPARATOR . $fileName);
                    if ($this->save(true, ['mime'])) {
                        switch ($ext) {
                            case 'xls':
                                $imgSrc = '/img/email/emailXls.png';
                                break;
                            case 'xlsx':
                                $imgSrc = '/img/email/emailXls.png';
                                break;
                            case 'pdf':
                                $downloadUrl = Url::to(['/email/download-file', 'id' => $this->id]);
                                $imgSrc = '/img/email/emailPdf.png';
                                break;
                            case 'txt':
                                $imgSrc = '/img/email/emailTxt.png';
                                break;
                            case 'doc':
                                $imgSrc = '/img/email/emailDoc.png';
                                break;
                            case 'docx':
                                $imgSrc = '/img/email/emailDoc.png';
                                break;
                            case 'zip':
                                $imgSrc = '/img/email/emailZip.png';
                                break;
                            default:
                                $imgSrc = '/img/email/emailUnknown.png';
                                break;
                        }
                        $previewImg = '<img src="' . $imgSrc . '" class="preview-img">';

                        return [
                            'result' => true,
                            'id' => $this->id,
                            'name' => $this->file_name,
                            'previewImg' => $previewImg,
                            'size' => $this->size,
                            'deleteUrl' => Url::to(['/email/delete-email-file', 'id' => $this->id]),
                            'downloadUrl' => $downloadUrl,
                        ];
                    }
                }
            }

            return [
                'result' => false,
                'msg' => 'Произошла ошибка при сохранении файла.',
            ];
        }

        return [
            'result' => false,
            'msg' => 'Размер загружаемого файла не должен превышать 1МБ.',
        ];
    }

    /**
     * @return false|int
     * @throws \yii\db\StaleObjectException
     */
    public function _delete()
    {
        if ($this->file_name) {
            $filePath = FileHelper::normalizePath($this->getFilePath() . DIRECTORY_SEPARATOR . $this->file_name);
            if (is_file($filePath)) {
                try {
                    FileHelper::unlink($filePath);
                } catch (\yii\base\ErrorException $e) {

                }
            }
        }

        return $this->delete();
    }
}
