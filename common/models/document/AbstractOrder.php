<?php

namespace common\models\document;

use frontend\models\Documents;

/**
 * This is the model class for documents Order.
 *
 * @property integer $precision
 * @property integer $docType
 * @property float $priceNoNds
 * @property float $priceWithNds
 * @property float $amountWithNds
 * @property float $amountNoNds
 * @property float $amountNds
 * @property float $quantity
 *
 * @property Order $order
 */
class AbstractOrder extends \yii\db\ActiveRecord
{
    protected $_docType;
    protected $_precision;
    protected $_data = [];

    /**
     *
     */
    public function afterFind()
    {
        parent::afterFind();

        $this->quantity = $this->quantity * 1;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrder()
    {
        return $this->hasOne(Order::className(), ['id' => 'order_id']);
    }

    /**
     * @return integer
     */
    public function getPrecision()
    {
        if (!isset($this->_data['getPrecision'])) {
            $this->_data['getPrecision'] = $this->order->invoice->price_precision == 4 ? 2: 0;
        }

        return $this->_data['getPrecision'];
    }

    /**
     * @return integer
     */
    public function getDocType()
    {
        if (!isset($this->_data['getDocType'])) {
            $this->_data['getDocType'] = $this->order->invoice->type;
        }

        return $this->_data['getDocType'];
    }

    /**
     * @return integer
     */
    public function getNdsViewType()
    {
        if (!isset($this->_data['getNdsViewType'])) {
            $this->_data['getNdsViewType'] = $this->order->invoice->nds_view_type_id;
        }

        return $this->_data['getNdsViewType'];
    }

    /**
     * @return integer
     */
    public function getNdsRate()
    {
        if (!isset($this->_data['getNdsRate'])) {
            $type = $this->getDocType() == Documents::IO_TYPE_OUT ? 'sale' : 'purchase';
            $this->_data['getNdsRate'] = (float) \common\models\TaxRate::rateById($this->order->{$type . '_tax_rate_id'});
        }

        return $this->_data['getNdsRate'];
    }

    /**
     * @return integer
     */
    public function getOrderPriceOne()
    {
        if (!isset($this->_data['getOrderBasePrice'])) {
            $this->_data['getOrderBasePrice'] = (float) $this->order->getPriceOne();
        }

        return $this->_data['getOrderBasePrice'];
    }

    /**
     * @return integer
     */
    public function getPriceDetails()
    {
        if (!isset($this->_data['getPriceDetails'])) {
            $this->_data['getPriceDetails'] = Order::calculateOrderPriceDetails(
                $this->getOrderPriceOne(),
                $this->quantity,
                $this->getNdsViewType(),
                $this->getNdsRate(),
                $this->getPrecision()
            );
        }

        return $this->_data['getPriceDetails'];
    }

    /**
     * @return integer
     */
    public function getPriceNoNds()
    {
        return $this->getPriceDetails()['priceOneNoNds'];
    }

    /**
     * @return integer
     */
    public function getPriceWithNds()
    {
        return $this->getPriceDetails()['priceOneWithNds'];
    }

    /**
     * @return integer
     */
    public function getAmountWithNds()
    {
        return $this->getPriceDetails()['totalWithNds'];
    }

    /**
     * @return integer
     */
    public function getAmountNoNds()
    {
        return $this->getPriceDetails()['totalNoNds'];
    }

    /**
     * @return integer
     */
    public function getAmountNds()
    {
        return $this->getPriceDetails()['totalNds'];
    }
}
