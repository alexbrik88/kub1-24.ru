<?php

namespace common\models\document;

use Yii;

/**
 * This is the model class for table "payment_order_invoice".
 *
 * @property integer $payment_order_id
 * @property integer $invoice_id
 *
 * @property Invoice $invoice
 * @property PaymentOrder $paymentOrder
 */
class PaymentOrderInvoice extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_order_invoice';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['payment_order_id', 'invoice_id'], 'required'],
            [['payment_order_id', 'invoice_id'], 'integer'],
            [['invoice_id'], 'exist', 'skipOnError' => true, 'targetClass' => Invoice::className(), 'targetAttribute' => ['invoice_id' => 'id']],
            [['payment_order_id'], 'exist', 'skipOnError' => true, 'targetClass' => PaymentOrder::className(), 'targetAttribute' => ['payment_order_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'payment_order_id' => 'Payment Order ID',
            'invoice_id' => 'Invoice ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(Invoice::className(), ['id' => 'invoice_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentOrder()
    {
        return $this->hasOne(PaymentOrder::className(), ['id' => 'payment_order_id']);
    }
}
