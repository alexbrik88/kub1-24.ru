<?php

namespace common\models\document;

use common\components\date\DateHelper;
use common\components\helpers\ArrayHelper;
use common\components\TextHelper;
use common\models\Contractor;
use common\models\EmployeeCompany;
use frontend\models\Documents;
use common\models\Company;
use yii\helpers\Html;
use yii\helpers\Url;
use Yii;

/**
 * This is the model class for table "invoice_in_to_invoice_out".
 *
 * @property int $company_id
 * @property int $invoice_id
 * @property int $number
 *
 * @property Company $company
 * @property Invoice $invoice
 */
class InvoiceInToInvoiceOut extends \yii\db\ActiveRecord
{
    public static $outInvoices = [];
    public static $inInvoices = [];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'invoice_in_to_invoice_out';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'invoice_id', 'number'], 'required'],
            [['company_id', 'invoice_id', 'number'], 'integer'],
            [['company_id', 'invoice_id'], 'unique', 'targetAttribute' => ['company_id', 'invoice_id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['company_id' => 'id']],
            [['invoice_id'], 'exist', 'skipOnError' => true, 'targetClass' => Invoice::class, 'targetAttribute' => ['invoice_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'company_id' => 'Company ID',
            'invoice_id' => 'Invoice ID',
            'number' => 'Number',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(Invoice::class, ['id' => 'invoice_id']);
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if ($this->number === null)
            $this->number = $this->getNextNumber();

        return parent::beforeValidate();
    }

    /**
     * @return int|null
     */
    public function getNextNumber()
    {
        return ($this->company_id !== null)
            ? (1 + self::find()->where(['company_id' => $this->company_id])->max('number'))
            : null;
    }

    public static function linkInvoices($companyId, $outInvoiceIds, $inInvoiceIds)
    {
        $outIds = (array)$outInvoiceIds;
        $inIds = (array)$inInvoiceIds;
        $number = (new InvoiceInToInvoiceOut(['company_id' => $companyId]))->getNextNumber();

        // validate
        $isValid = true;
        foreach (array_merge($outIds, $inIds) as $invoiceId) {
            $isValid = $isValid && (new InvoiceInToInvoiceOut(['company_id' => $companyId, 'invoice_id' => $invoiceId, 'number' => $number]))->validate();
        }

        // save
        if ($isValid) {
            foreach (array_merge($outIds, $inIds) as $invoiceId) {
                (new InvoiceInToInvoiceOut(['company_id' => $companyId, 'invoice_id' => $invoiceId, 'number' => $number]))->save();
            }

            return true;
        }

        return false;
    }

    // HELPERS
    public static function getOutInfo($companyId, $linkNumber, $attribute, $glue = '<br/>')
    {
        self::preloadInvoices($companyId, $linkNumber);

        $ret = [];
        foreach (self::$outInvoices[$linkNumber] as $invoice) {
            $ret[] = self::getInvoiceAttributeValue($invoice, $attribute);
        }

        return implode($glue, $ret);
    }

    public static function getInInfo($companyId, $linkNumber, $attribute, $glue = '<br/>')
    {
        self::preloadInvoices($companyId, $linkNumber);

        $ret = [];
        foreach (self::$inInvoices[$linkNumber] as $invoice) {
            $ret[] = self::getInvoiceAttributeValue($invoice, $attribute);
        }

        return implode($glue, $ret);
    }

    public static function getResponsibleEmployee($companyId, $linkNumber)
    {
        self::preloadInvoices($companyId, $linkNumber);

        $employeeId = null;

        foreach (self::$inInvoices[$linkNumber] as $invoice) {
            $employeeId = $invoice->responsible_employee_id;
            break;
        }
        if (!$employeeId)
            foreach (self::$outInvoices[$linkNumber] as $invoice) {
                $employeeId = $invoice->responsible_employee_id;
                break;
            }

        if (!$employeeId)
            return '';

        $employeeCompany = EmployeeCompany::findOne([
            'employee_id' => $employeeId,
            'company_id' => $companyId,
        ]);

        return $employeeCompany ? $employeeCompany->getFio(true) : '';
    }

    private static function preloadInvoices($companyId, $linkNumber)
    {
        if (isset(self::$outInvoices[$linkNumber]) && isset(self::$inInvoices[$linkNumber])) {
            return true;
        }

        self::$outInvoices[$linkNumber] = [];
        self::$inInvoices[$linkNumber] = [];

        $sort = Yii::$app->request->get('sort');

        if (substr($sort, 0, 1) === '-') {
            $sortKey = substr($sort, 1);
            $sortValue = SORT_DESC;
        } else {
            $sortKey = $sort;
            $sortValue = SORT_ASC;
        }

        switch ($sortKey) {
            case 'in_document_number':
            case 'out_document_number':
                $orderBy = ['document_number' => $sortValue];
                break;
            case 'in_document_date':
            case 'out_document_date':
                $orderBy = ['document_date' => $sortValue];
                break;
            case 'in_total_amount_with_nds':
            case 'out_total_amount_with_nds':
                $orderBy = ['total_amount_with_nds' => $sortValue];
                break;
            case 'in_payment_date':
            case 'out_payment_date':
            case 'margin_amount':
            case 'margin_percent':
            default:
                $orderBy = ['document_number' => SORT_ASC];
                break;
        }

        $_ids = self::find()
            ->where(['company_id' => $companyId, 'number' => $linkNumber])
            ->select(['invoice_id'])
            ->asArray()
            ->column();

        $invoices = Invoice::find()
            ->where(['company_id' => $companyId, 'id' => $_ids])
            ->orderBy($orderBy)
            ->all();

        foreach ($invoices as $invoice)
            if ($invoice->type == Documents::IO_TYPE_OUT) {
                self::$outInvoices[$linkNumber][] = $invoice;
            } else {
                self::$inInvoices[$linkNumber][] = $invoice;
            }

        return true;
    }

    /**
     * @param Invoice $invoice
     * @param $attribute
     * @return string
     */
    private static function getInvoiceAttributeValue(Invoice $invoice, $attribute)
    {
        switch ($attribute) {
            case 'id':
                return $invoice->id;
            case 'payment_date':
                $lastPaymentDate = $invoice->getLastPaymentDate();
                return ($lastPaymentDate) ? date('d.m.Y', $lastPaymentDate) : '---';
            case 'document_date':
                return DateHelper::format(ArrayHelper::getValue($invoice, $attribute), 'd.m.Y', 'Y-m-d');
            case 'document_number':
                return Html::a($invoice->getFullNumber(), Url::to(['/documents/invoice/view', 'type' => $invoice->type, 'id' => $invoice->id]));
            case 'total_amount_with_nds':
                return TextHelper::invoiceMoneyFormat(ArrayHelper::getValue($invoice, $attribute), 2);
            case 'contractor.name':
                if ($invoice->contractor instanceof Contractor) {
                    return Html::a($invoice->contractor->name, Url::to(['/contractor/view', 'type' => $invoice->contractor->type, 'id' => $invoice->contractor->id]));
                } else {
                    return $invoice->contractor_id;
                }
            case 'invoiceStatus.name':
                return ArrayHelper::getValue($invoice, $attribute);
        }

        return '---';
    }
}
