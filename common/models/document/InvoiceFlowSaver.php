<?php
/**
 * Created by konstantin.
 * Date: 27.7.15
 * Time: 15.13
 */

namespace common\models\document;


use common\models\cash\CashFlowsBase;
use common\models\document\status\InvoiceStatus;
use frontend\models\Documents;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;

/**
 * @class InvoiceFlowSaver
 * @package common\models\document
 */
class InvoiceFlowSaver
{

    /**
     * Saves invoice after adding/delete/update flow.To`
     * @param CashFlowsBase $flow
     * @param Invoice $invoice
     * @param InvoiceFlowCalculator $invoiceFlowCalc
     * @return bool
     * @throws \yii\base\Exception
     */
    public static function save(CashFlowsBase $flow, Invoice $invoice, InvoiceFlowCalculator $invoiceFlowCalc)
    {
        if ($invoiceFlowCalc->payed == 0) {
            $invoice->invoice_status_id = $invoice->type == Documents::IO_TYPE_OUT
                ? InvoiceStatus::STATUS_SEND
                : InvoiceStatus::STATUS_CREATED;
        } else {
            $invoice->invoice_status_id = $invoiceFlowCalc->isPayed()
                ? InvoiceStatus::STATUS_PAYED
                : InvoiceStatus::STATUS_PAYED_PARTIAL;
            $invoice->invoice_status_updated_at = strtotime($flow->date);
        }

        if ($invoice->isAttributeChanged('invoice_status_id')) {
            return LogHelper::save($invoice, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function (Invoice $model) {
                return $model->save(true, ['invoice_status_id', 'invoice_status_updated_at', 'invoice_status_author_id']);
            });
        }

        return true;
    }

}