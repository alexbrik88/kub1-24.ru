<?php

namespace common\models\document;

use common\components\calculator\CalculatorHelper;
use common\models\NdsOsno;
use common\models\product\Product;
use common\models\TaxRate;
use frontend\models\Documents;

/**
 * This is the model class for table "autoorder".
 *
 * @property integer $id
 * @property integer $autoinvoice_id
 * @property integer $number
 * @property integer $product_id
 * @property string $quantity
 * @property string $product_title
 * @property string $product_code
 * @property integer $unit_id
 * @property string $count_in_place
 * @property string $place_count
 * @property string $mass_gross
 * @property string $purchase_price_no_vat
 * @property string $purchase_price_with_vat
 * @property string $selling_price_no_vat
 * @property string $selling_price_with_vat
 * @property string $amount_purchase_no_vat
 * @property string $amount_purchase_with_vat
 * @property string $amount_sales_no_vat
 * @property string $amount_sales_with_vat
 * @property integer $excise
 * @property string $excise_price
 * @property string $sale_tax
 * @property string $purchase_tax
 * @property integer $purchase_tax_rate_id
 * @property integer $sale_tax_rate_id
 * @property integer $country_id
 * @property string $custom_declaration_number
 * @property string $box_type
 *
 * @property Autoinvoice $autoinvoice
 * @property Product $product
 */
class Autoorder extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'autoorder';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['autoinvoice_id', 'number', 'product_id', 'unit_id', 'purchase_price_no_vat', 'purchase_price_with_vat', 'selling_price_no_vat', 'selling_price_with_vat', 'amount_purchase_no_vat', 'amount_purchase_with_vat', 'amount_sales_no_vat', 'amount_sales_with_vat', 'excise', 'sale_tax', 'purchase_tax', 'purchase_tax_rate_id', 'sale_tax_rate_id', 'country_id'], 'integer'],
            [['quantity'], 'number'],
            [['product_title', 'product_code', 'count_in_place', 'excise_price'], 'string', 'max' => 255],
            [['place_count', 'mass_gross', 'custom_declaration_number', 'box_type'], 'string', 'max' => 45],
            [['autoinvoice_id'], 'exist', 'skipOnError' => true, 'targetClass' => Autoinvoice::className(), 'targetAttribute' => ['autoinvoice_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'autoinvoice_id' => 'Autoinvoice ID',
            'number' => 'Number',
            'product_id' => 'Product ID',
            'quantity' => 'Quantity',
            'product_title' => 'Product Title',
            'product_code' => 'Product Code',
            'unit_id' => 'Unit ID',
            'count_in_place' => 'Count In Place',
            'place_count' => 'Place Count',
            'mass_gross' => 'Mass Gross',
            'purchase_price_no_vat' => 'Purchase Price No Vat',
            'purchase_price_with_vat' => 'Purchase Price With Vat',
            'selling_price_no_vat' => 'Selling Price No Vat',
            'selling_price_with_vat' => 'Selling Price With Vat',
            'amount_purchase_no_vat' => 'Amount Purchase No Vat',
            'amount_purchase_with_vat' => 'Amount Purchase With Vat',
            'amount_sales_no_vat' => 'Amount Sales No Vat',
            'amount_sales_with_vat' => 'Amount Sales With Vat',
            'excise' => 'Excise',
            'excise_price' => 'Excise Price',
            'sale_tax' => 'Sale Tax',
            'purchase_tax' => 'Purchase Tax',
            'purchase_tax_rate_id' => 'Purchase Tax Rate ID',
            'sale_tax_rate_id' => 'Sale Tax Rate ID',
            'country_id' => 'Country ID',
            'custom_declaration_number' => 'Custom Declaration Number',
            'box_type' => 'Box Type',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAutoinvoice()
    {
        return $this->hasOne(Autoinvoice::className(), ['id' => 'autoinvoice_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxRate()
    {
        return $this->hasOne(TaxRate::className(), ['id' => 'sale_tax_rate_id']);
    }


    /**
     * @param $priceOneWithVat
     * @param $documentType
     * @param $taxRateId
     * @param null $ndsViewType
     */
    public function calculateOrderPrice($priceOneWithVat,  $taxRateId)
    {
        $priceOneWithVat = floatval($priceOneWithVat) * 100;
        $company = (\Yii::$app->params['paymentUser'] !== null) ?
            \Yii::$app->params['paymentUser']->company :
            \Yii::$app->user->identity->company;
        if ($company->nds == NdsOsno::WITHOUT_NDS) {
            $this->selling_price_no_vat = $priceOneWithVat;
            $this->selling_price_with_vat = $priceOneWithVat;
            $this->amount_sales_no_vat = $this->selling_price_no_vat * $this->quantity;
            $this->amount_sales_with_vat = $this->selling_price_with_vat * $this->quantity;
            $this->sale_tax_rate_id = $taxRateId;
        } else {
            $this->selling_price_no_vat = $priceOneWithVat;
            $this->selling_price_with_vat =  CalculatorHelper::getPriceWithNds($priceOneWithVat, $taxRateId);
            $this->amount_sales_no_vat = $this->selling_price_no_vat * $this->quantity;
            $this->amount_sales_with_vat = $this->selling_price_with_vat * $this->quantity;
            $this->sale_tax_rate_id = $taxRateId;
        }
    }


    /**
     * @param $orderArray
     * @param $autoinvoice
     */
    public static function createFromArray($orderArray, $autoinvoice_id)
    {
        /**
         * @var $autoinvoice Autoinvoice
         */
        foreach ($orderArray as $item => $order) {
            $autoorder = new Autoorder();
            $product = Product::findOne(['id' => $order['id']]);
            $autoorder->product_id = $product->id;
            $autoorder->product_title = $order['title'];
            $autoorder->product_code = $product->code;
            $autoorder->autoinvoice_id = $autoinvoice_id;
            $autoorder->quantity = $order['count'];
            $autoorder->number = $item;
            $autoorder->box_type = $product->box_type;
            $autoorder->count_in_place = $product->count_in_place;
            $autoorder->place_count = $product->place_count;
            $autoorder->country_id = $product->country_origin_id;
            $autoorder->mass_gross = $product->mass_gross;
            $autoorder->custom_declaration_number = $product->customs_declaration_number;
            $autoorder->excise = $product->has_excise;
            $autoorder->excise_price = $product->excise;
            $autoorder->unit_id = $product->product_unit_id;
            $autoorder->calculateOrderPrice(
                $order['priceOneWithVat'],
                $autoorder->product->priceForSellNds->id);
            $autoorder->save();
        }
    }
}
