<?php

namespace common\models\document;

use common\components\calculator\CalculatorHelper;
use common\components\helpers\ModelHelper;
use common\models\address;
use common\models\Company;
use common\models\currency\Currency;
use common\models\document\Invoice;
use common\models\document\query\OrderQuery;
use common\models\NdsOsno;
use common\models\product\Product;
use common\models\product\ProductUnit;
use common\models\TaxRate;
use frontend\models\Documents;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * This is the model class for table "order".
 *
 * @property integer $id
 * @property integer $invoice_id
 * @property integer $number
 * @property integer $product_id
 * @property integer $quantity
 * @property string $product_title
 * @property string $product_code
 * @property integer $unit_id
 * @property string $count_in_place
 * @property string $place_count
 * @property string $box_type
 * @property string $mass_gross
 * @property float $discount
 * @property float $markup
 * @property float $weight
 * @property float $volume
 * @property integer $purchase_price_no_vat // TODO: rename.
 * @property integer $purchase_price_with_vat // TODO: rename.
 * @property integer $selling_price_no_vat // TODO: rename.
 * @property integer $selling_price_with_vat // TODO: rename.
 * @property integer $amount_purchase_no_vat // TODO: rename.
 * @property integer $amount_purchase_with_vat // TODO: rename.
 * @property integer $amount_sales_no_vat // TODO: rename.
 * @property integer $amount_sales_with_vat // TODO: rename.
 * @property integer $excise // TODO: rename to has_excise.
 * @property string $excise_price
 * @property integer $sale_tax // TODO: rename (налог в рублях).
 * @property integer $purchase_tax // TODO: rename.
 * @property integer $purchase_tax_rate_id // TODO: rename.
 * @property integer $sale_tax_rate_id // TODO: rename.
 * @property integer $country_id
 * @property integer $custom_declaration_number
 * @property string $productTitle
 *
 * @property address\Country $country
 * @property Invoice $invoice
 * @property Product $product
 * @property ProductUnit $unit
 * @property OrderInvoiceFacture[] $orderInvoiceFactures
 * @property TaxRate $purchaseTaxRate
 * @property TaxRate $saleTaxRate
 * @property boolean $isDeleteAllowed
 * @property OrderUpd[] $orderUpds
 * @property Upd[] $upds
 */
class Order extends \yii\db\ActiveRecord
{
    const MAX_PRICE = 999999999999999999.9999;
    const MIN_PRICE = 0;
    const MAX_QUANTITY = 9999999999.9999999999;
    const MIN_QUANTITY = 0.0000000001;
    const MAX_DISCOUNT = 100;
    const MAX_MARKUP = 9999.9999;

    public $max_product_count;

    /**
     * @param $invoiceId
     * @param $orderId
     * @return Order
     */
    public static function findById($invoiceId, $orderId)
    {
        return static::find()
            ->byInvoice($invoiceId)
            ->andWhere([
                'id' => $orderId,
            ])->one();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order';
    }

    /**
     * @return OrderQuery
     */
    public static function find()
    {
        return new OrderQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function fields()
    {
        $fields = parent::fields();
        $fields['production_type'] = 'production_type';

        return $fields;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['number', 'price', 'quantity', 'count', 'title'], 'required',],
            [['discount', 'markup', 'weight', 'volume'], 'default', 'value' => 0],
            [['number'], 'integer'],
            [['max_product_count'], 'safe'],
            [['price', 'quantity', 'discount', 'markup', 'volume', 'weight'], 'filter', 'filter' => function ($value) {
                return str_replace(",", ".", $value);
            },],
            [['max_product_count'], 'double'],
            //[['count'], function($attribute, $params) {
            //    $title = 'Для "' . $this->product_title . '"';
            //    if (!empty($this->max_product_count) && $this->{$attribute} > (float)$this->max_product_count) {
            //            $this->addError($attribute, "{$title} количество не должно превышать " . $this->max_product_count);
            //        }
            //}],
            [
                ['price'], 'number',
                'numberPattern' => '/^\d+(\.\d*)?$/',
                'min' => self::MIN_PRICE,
                'max' => self::MAX_PRICE,
            ],
            [
                ['quantity'], 'number',
                'numberPattern' => '/^\d+(\.\d*)?$/',
                'min' => self::MIN_QUANTITY,
                'max' => self::MAX_QUANTITY,
            ],
            [['quantity'], function ($attribute, $params) {
                if ($this->hasErrors($attribute)) {
                    return;
                }
                $title = 'для «' . $this->product_title . '»';
                $min = $this->isNewRecord ? 0 : $this->getDependentMaxQuantity() * 1;
                if ($min && $this->$attribute < $min) {
                    $this->addError(
                        $attribute,
                        "Количество {$title} не должно быть меньше, чем в сопутствующих документах: {$min}."
                    );
                }
            }],
            [['discount'], 'number', 'numberPattern' => '/^\d+(\.\d*)?$/', 'min' => 0, 'max' => self::MAX_DISCOUNT,
                'when' => function (Order $model) {
                    return $model->invoice->discount_type == Invoice::DISCOUNT_TYPE_PERCENT;
                }],
            [['discount'], 'number', 'numberPattern' => '/^\d+(\.\d*)?$/', 'min' => 0, 'max' => self::MAX_PRICE,
                'when' => function (Order $model) {
                    return $model->invoice->discount_type == Invoice::DISCOUNT_TYPE_RUBLE;
                }],
            [['markup'], 'number', 'numberPattern' => '/^\d+(\.\d*)?$/', 'min' => 0, 'max' => self::MAX_MARKUP],
            [['weight', 'volume'], 'number', 'numberPattern' => '/^\d+(\.\d*)?$/', 'min' => 0],
            [['discount', 'markup', 'weight', 'volume'], 'filter', 'filter' => function ($value) {
                return round($value, 6);
            },],
            [['unit_id'], 'exist', 'skipOnEmpty' => true, 'skipOnError' => true, 'targetClass' => ProductUnit::className(), 'targetAttribute' => ['unit_id' => 'id']],
            [['sale_tax_rate_id'], 'exist', 'skipOnEmpty' => true, 'skipOnError' => true, 'targetClass' => TaxRate::className(), 'targetAttribute' => ['sale_tax_rate_id' => 'id']],
            [['purchase_tax_rate_id'], 'exist', 'skipOnEmpty' => true, 'skipOnError' => true, 'targetClass' => TaxRate::className(), 'targetAttribute' => ['purchase_tax_rate_id' => 'id']],
            [['unit_id', 'sale_tax_rate_id', 'purchase_tax_rate_id'], 'filter', 'filter' => function ($value) {
                return intval($value) ?: null;
            }]
        ];
    }

    /**
     * @return array
     */
    public function attributeLabels()
    {
        return [
            'quantity' => 'Количество',
            'count' => 'Количество',
            'amount_purchase_no_vat' => 'Сумма покупки без НДС',
            'amount_purchase_with_vat' => 'Сумма продажи с НДС',
            'amount_sales_no_vat' => 'Сумма продажи без НДС',
            'amount_sales_with_vat' => 'Сумма продажи с НДС',
            'purchase_tax' => 'НДС покупки',
            'sale_tax' => 'НДС продажи',
            'discount' => 'Скидка',
            'markup' => 'Наценка',
            'weight' => 'Вес',
            'volume' => 'Объем',
            'price' => 'Цена',
            'unit_id' => 'Единица измерения'
        ];
    }

    /**
     *
     */
    public function validateProduct()
    {
        if (!$this->isNewRecord) {
            return;
        }

        if ($this->product === null) {
            $this->addError('product_id', 'Товар/услуга не найдены.');

            return;
        }
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            $this->calculateOrder();

            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            ModelHelper::HtmlEntitiesModelAttributes($this);

            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        static::setProductReserve($this->id);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(address\Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(Invoice::className(), ['id' => 'invoice_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceAuto()
    {
        return $this->hasOne(InvoiceAuto::className(), ['id' => 'invoice_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnit()
    {
        return $this->hasOne(ProductUnit::className(), ['id' => 'unit_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchaseTaxRate()
    {
        return $this->hasOne(TaxRate::className(), ['id' => 'purchase_tax_rate_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderActs()
    {
        return $this->hasMany(OrderAct::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSaleTaxRate()
    {
        return $this->hasOne(TaxRate::className(), ['id' => 'sale_tax_rate_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getActOrders()
    {
        return $this->hasMany(OrderAct::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPackingListOrders()
    {
        return $this->hasMany(OrderPackingList::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderInvoiceFactures()
    {
        return $this->hasMany(OrderInvoiceFacture::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceFactureOrders()
    {
        return $this->hasMany(OrderInvoiceFacture::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdOrders()
    {
        return $this->hasMany(OrderUpd::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderUpds()
    {
        return $this->hasMany(OrderUpd::className(), ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceFactures()
    {
        return $this->hasMany(InvoiceFacture::className(), ['id' => 'invoice_facture_id'])
            ->viaTable('order_invoice_facture', ['order_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpds()
    {
        return $this->hasMany(Upd::className(), ['id' => 'upd_id'])->viaTable('order_upd', ['order_id' => 'id']);
    }

    /**
     * @return OrderInvoiceFacture | null
     */
    public function getOrderInvoiceFacture($id = null)
    {
        return $this->getOrderInvoiceFactures()->andWhere([
            'invoice_facture_id' => $id ? $id : $this->getInvoiceFacture()->select('id')->scalar(),
        ])->one();
    }

    /**
     * @return OrderUpd | null
     */
    public function getOrderUpd($id = null)
    {
        return $this->getOrderUpds()->andWhere([
            'upd_id' => $id ? $id : $this->getUpd()->select('id')->scalar(),
        ])->one();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceFacture()
    {
        return $this->invoice->getInvoiceFacture();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpd()
    {
        return $this->invoice->getUpd();
    }

    /**
     * @param $id
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getOrderToInvoice($id)
    {
        return $this->find()->where(['invoice_id' => $id])->all();
    }

    /**
     * @param $value
     */
    public function getNdsRate()
    {
        return ($this->invoice->type == Documents::IO_TYPE_IN) ? $this->purchaseTaxRate->rate : $this->saleTaxRate->rate;
    }

    /**
     * @param $value
     */
    public function setPrice($value)
    {
        $invoice = $this->invoice;
        $ndsViewType = $invoice->nds_view_type_id;
        $hasNds = $ndsViewType != Invoice::NDS_VIEW_WITHOUT;
        $ndsOut = $ndsViewType == Invoice::NDS_VIEW_OUT;
        $ndsRate = floatval($this->getNdsRate());

        $value = strtr($value, [',' => '.']);
        if (!is_numeric($value)) {
            $value = 0;
        }

        $this->view_price_base = max(0, $value * 100);

        $price = $invoice->toRub($this->view_price_base);

        $this->base_price_no_vat = round($ndsOut ? $price : ($hasNds ? $price / (1 + $ndsRate) : $price), 2);
        $this->base_price_with_vat = round(!$ndsOut ? $price : ($hasNds ? $price * (1 + $ndsRate) : $price), 2);
    }

    /**
     * @return string
     */
    public function getPrice()
    {
        return $this->view_price_base / 100;
    }

    /**
     * @return string
     */
    public function getBasePrice()
    {
        if ($this->invoice->nds_view_type_id == Invoice::NDS_VIEW_OUT) {
            return $this->base_price_no_vat;
        } else {
            return $this->base_price_with_vat;
        }
    }

    /**
     * @return integer
     */
    public function getPriceOne()
    {
        $invoice = $this->invoice;
        if ($invoice->type == Documents::IO_TYPE_IN) {
            if ($invoice->nds_view_type_id == Invoice::NDS_VIEW_OUT) {
                return $this->purchase_price_no_vat;
            } else {
                return $this->purchase_price_with_vat;
            }
        } else {
            if ($invoice->nds_view_type_id == Invoice::NDS_VIEW_OUT) {
                return $this->selling_price_no_vat;
            } else {
                return $this->selling_price_with_vat;
            }
        }
    }

    /**
     * @param $value
     */
    public function setTitle($value)
    {
        $this->product_title = $value;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->product_title;
    }

    /**
     * @param $value
     */
    public function setCount($value)
    {
        $this->quantity = $value;
    }

    /**
     * @return float
     */
    public function getCount()
    {
        return $this->quantity;
    }

    /**
     * @param $price
     * @param $discount
     * @param $markup
     * @param int $discountType
     * @return float|int
     */
    public static function priceOne($price, $discount, $markup, $discountType = Invoice::DISCOUNT_TYPE_PERCENT)
    {
        if ($discount > 0) {
            if ($discountType == Invoice::DISCOUNT_TYPE_PERCENT) {
                return $price * (100 - $discount) / 100;
            } else {
                return $price - ($discount * 100);
            }
        } elseif ($markup > 0) {
            return $price * (100 + $markup) / 100;
        }

        return $price;
    }

    /**
     *
     */
    public function calculateOrder()
    {
        $ndsViewType = $this->invoice->nds_view_type_id;
        $hasNds = $ndsViewType != Invoice::NDS_VIEW_WITHOUT;
        $ndsOut = $ndsViewType == Invoice::NDS_VIEW_OUT;
        $rate = $this->getNdsRate();
        $precision = $this->invoice->price_precision == 4 ? 2 : 0;

        $price = $this->calculatePrice($this->view_price_base, $precision);
        $this->view_price_one = $price['priceOne'];
        $this->view_total_base = $price['totalBase'];
        $this->view_total_amount = $price['totalAmount'];
        $this->view_total_no_nds = $price['totalNoNds'];
        $this->view_total_nds = $price['totalNds'];
        $this->view_total_with_nds = $price['totalWithNds'];

        if ($this->invoice->has_markup) {
            $this->markup = max(0, min(self::MAX_MARKUP, $this->markup));
            $this->discount = 0;
        } else {
            $this->markup = 0;
            if ($this->invoice->has_discount) {
                if ($this->invoice->discount_type == Invoice::DISCOUNT_TYPE_PERCENT) {
                    $discount = $this->adjustDiscount($this->getBasePrice(), $this->discount);
                    $this->discount = max(0, min(self::MAX_DISCOUNT, $discount));
                }
            } else {
                $this->discount = 0;
            }
        }
        $discount = $this->discount;
        $markup = $this->markup;
        $priceOne = self::priceOne($this->getBasePrice(), $discount, $markup, $this->invoice->discount_type);

        if ($this->invoice->currency_name === Currency::DEFAULT_NAME) {
            $priceOne = round($priceOne, $precision);
        }

        $priceDetails = Order::calculateOrderPriceDetails(
            $priceOne,
            $this->quantity,
            $this->invoice->nds_view_type_id,
            $this->getNdsRate(),
            $precision
        );

        if ($this->invoice->type == Documents::IO_TYPE_IN) {
            $this->purchase_price_no_vat = $priceDetails['priceOneNoNds'];
            $this->purchase_price_with_vat = $priceDetails['priceOneWithNds'];
            $this->amount_purchase_no_vat = $priceDetails['totalNoNds'];
            $this->purchase_tax = $priceDetails['totalNds'];
            $this->amount_purchase_with_vat = $priceDetails['totalWithNds'];
        } else {
            $this->selling_price_no_vat = $priceDetails['priceOneNoNds'];
            $this->selling_price_with_vat = $priceDetails['priceOneWithNds'];
            $this->amount_sales_no_vat = $priceDetails['totalNoNds'];
            $this->sale_tax = $priceDetails['totalNds'];
            $this->amount_sales_with_vat = $priceDetails['totalWithNds'];
        }
    }

    public function adjustDiscount($price, $discount)
    {
        if (!isset($this->invoice->price_round_rule, $this->invoice->price_round_precision) ||
            $this->invoice->price_round_rule == Invoice::ROUND_RULE_NO ||
            $price == 0
        ) return $discount;

        $k = $this->invoice->getPriceRoundPrecision() * 100;
        switch ($this->invoice->price_round_rule) {
            case Invoice::ROUND_RULE_UP:
                $f = 'ceil';
                break;
            case Invoice::ROUND_RULE_DOWN:
                $f = 'floor';
                break;
            case Invoice::ROUND_RULE_MATH:
            default:
                $f = 'round';
                break;
        }

        $sum = $f($price * (100 - $discount) / 100 / $k);

        return round(100 - $sum * $k * 100 / $price, 6) * 1;
    }

    /**
     * @param $packing_list_id
     */
    public function getAllWithoutCurrentPackingList($packing_list_id)
    {
    }

    /**
     * Calculate Product reserve by UPD and Packing List
     */
    public static function setProductReserve($orderId)
    {
        $command = Yii::$app->db->createCommand("
            UPDATE {{order}}
            LEFT JOIN {{invoice}} ON {{invoice}}.[[id]] = {{order}}.[[invoice_id]]
            LEFT JOIN {{product}} ON {{product}}.[[id]] = {{order}}.[[product_id]]
            SET {{order}}.[[reserve]] = IF(
                {{product}}.[[production_type]] = :goods
                AND {{invoice}}.[[type]] = :type
                AND {{invoice}}.[[invoice_status_id]] <> :auto
                AND {{invoice}}.[[is_deleted]] = 0,

                GREATEST(
                    0,
                    {{order}}.[[quantity]] - (
                        SELECT COALESCE(SUM({{sold}}.[[quantity]]), 0)
                        FROM (
                            SELECT {{order_packing_list}}.[[quantity]] FROM {{order_packing_list}}
                                LEFT JOIN {{packing_list}} ON {{packing_list}}.[[id]] = {{order_packing_list}}.[[packing_list_id]]
                                WHERE {{order_packing_list}}.[[order_id]] = :orderId
                                AND {{packing_list}}.[[status_out_id]] <> :rejectedPL
                            UNION ALL
                            SELECT {{order_upd}}.[[quantity]] FROM {{order_upd}}
                                LEFT JOIN {{upd}} ON {{upd}}.[[id]] = {{order_upd}}.[[upd_id]]
                                WHERE {{order_upd}}.[[order_id]] = :orderId
                                AND {{upd}}.[[status_out_id]] <> :rejectedUPD
                            UNION ALL
                            SELECT {{order_sales_invoice}}.[[quantity]] FROM {{order_sales_invoice}}
                                LEFT JOIN {{sales_invoice}} ON {{sales_invoice}}.[[id]] = {{order_sales_invoice}}.[[sales_invoice_id]]
                                WHERE {{order_sales_invoice}}.[[order_id]] = :orderId
                                AND {{sales_invoice}}.[[status_out_id]] <> :rejectedSI
                        ) {{sold}}
                    )
                ),

                0
            )
            WHERE {{order}}.[[id]] = :orderId
        ", [
            ':orderId' => $orderId,
            ':goods' => Product::PRODUCTION_TYPE_GOODS,
            ':type' => Documents::IO_TYPE_OUT,
            ':auto' => status\InvoiceStatus::STATUS_AUTOINVOICE,
            ':rejectedPL' => status\PackingListStatus::STATUS_REJECTED,
            ':rejectedUPD' => status\UpdStatus::STATUS_REJECTED,
            ':rejectedSI' => status\SalesInvoiceStatus::STATUS_REJECTED,

        ]);

        return $command->execute();
    }

    /**
     * @param \common\models\document\Invoice|null $invoice
     * @return int
     */
    public function getTaxRateByInvoice(Invoice $invoice = null)
    {
        $invoice = $invoice === null ? $this->invoice : $invoice;

        return $invoice->type == Documents::IO_TYPE_OUT
            ? $this->sale_tax_rate_id
            : $this->purchase_tax_rate_id;
    }

    /**
     *
     */
    public function afterFind()
    {
        parent::afterFind();
        $this->quantity = $this->quantity * 1;
        $this->discount = $this->discount * 1;
        $this->markup = $this->markup * 1;
        $this->weight = $this->weight * 1;
        $this->volume = $this->volume * 1;
    }

    /**
     * @return integer
     */
    public function getProductionType()
    {
        return $this->product->production_type;
    }

    /**
     * The number of dependent documents where the position exist
     * @return integer
     */
    public function getDependentOrdersMethod()
    {
        return $this->productionType == Product::PRODUCTION_TYPE_GOODS ? 'getPackingListOrders' : 'getActOrders';
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDependentOrders()
    {
        return array_merge($this->{$this->dependentOrdersMethod}()->all(), $this->getUpdOrders()->all());
    }

    /**
     * @return boolean
     */
    public function getIsDependentExists()
    {
        return $this->{$this->dependentOrdersMethod}()->exists() || $this->getUpdOrders()->exists();
    }

    /**
     * @return boolean
     */
    public function getIsDeleteAllowed()
    {
        return !$this->isDependentExists;
    }

    /**
     * The number of dependent documents where the position exist
     * @return integer
     */
    public function getDependentNumber()
    {
        return $this->{$this->dependentOrdersMethod}()->count() + $this->getUpdOrders()->count();
    }

    /**
     * Quantity in a dependent documents
     * @return integer|float
     */
    public function getDependentQuantity()
    {
        return $this->{$this->dependentOrdersMethod}()->sum('quantity') * 1 + $this->getUpdOrders()->sum('quantity') * 1;
    }

    /**
     * Quantity in a dependent documents
     * @return integer|float
     */
    public function getDependentMaxQuantity()
    {
        return 0;

        // todo: not working for many invoices-to-one document relations
        //return max(
        //    0,
        //    $this->getActOrders()->sum('quantity'),
        //    $this->getPackingListOrders()->sum('quantity'),
        //    $this->getInvoiceFactureOrders()->sum('quantity'),
        //    $this->getUpdOrders()->sum('quantity')
        //);
    }

    /**
     * @return string
     */
    public function getProduction_type()
    {
        return ArrayHelper::getValue($this->product, 'production_type');
    }

    /**
     * @return string
     */
    public function getProductTitle()
    {
        return Html::encode($this->product_title);
    }

    /**
     * @return boolean
     */
    public function getIsHour()
    {
        return $this->unit_id == ProductUnit::UNIT_HOUR;
    }

    /**
     * @return boolean
     */
    public function getDiscountViewValue()
    {
        $parts = explode('.', strval($this->discount * 1));

        return ArrayHelper::getValue($parts, 0, '0') .','.str_pad(ArrayHelper::getValue($parts, 1, ''), 2, '0');
    }

    /**
     * @return array
     */
    public function calculatePrice($priceBase, $precision)
    {
        $discount = 0;
        $markup = 0;
        $discountType = $this->invoice->discount_type;
        $quantity = floatval($this->quantity) == intval($this->quantity) ? intval($this->quantity) : floatval($this->quantity);
        if ($this->invoice->has_markup) {
            $markup = max(0, min(self::MAX_MARKUP, $this->markup));
        } else {
            if ($this->invoice->has_discount) {
                if ($discountType == Invoice::DISCOUNT_TYPE_PERCENT) {
                    $discount = max(0, min(self::MAX_DISCOUNT, $this->discount));
                } else {
                    $discount = $this->discount;
                }
            } else {
                $discount = 0;
            }
        }

        $priceOne = self::priceOne($priceBase, $discount, $markup, $this->invoice->discount_type);

        if ($this->invoice->currency_name === Currency::DEFAULT_NAME) {
            $priceOne = round($priceOne, $precision);
        }

        $priceDetails = self::calculateOrderPriceDetails(
            $priceOne,
            $quantity,
            $this->invoice->nds_view_type_id,
            $this->getNdsRate(),
            $precision
        );

        return [
            'priceBase' => $priceBase,
            'totalBase' => round($priceBase * $quantity, $precision),
            'priceOne' => $priceDetails['priceOne'],
            'totalAmount' => $priceDetails['totalAmount'],
            'totalNoNds' => $priceDetails['totalNoNds'],
            'totalNds' => $priceDetails['totalNds'],
            'totalWithNds' => $priceDetails['totalWithNds'],
        ];
    }

    /**
     * @return array
     */
    public static function calculateOrderPriceDetails($price, $quantity, $ndsViewType, $ndsRate, $precision = 2)
    {
        if ($ndsRate && $ndsViewType == Invoice::NDS_VIEW_WITHOUT) {
            $ndsRate = 0;
        }
        $quantity = floatval($quantity) == intval($quantity) ? intval($quantity) : floatval($quantity);
        $ndsRate = floatval($ndsRate);
        $hasNds = $ndsViewType != Invoice::NDS_VIEW_WITHOUT;
        $ndsOut = $ndsViewType == Invoice::NDS_VIEW_OUT;
        $totalAmount = round($price * $quantity, (int) $precision);
        if ($ndsOut) {
            $totalNoNds = $totalAmount;
            $totalNds = $hasNds ? round($totalNoNds * $ndsRate, $precision) : 0;
            $totalWithNds = round($totalNoNds + $totalNds, $precision);
        } else {
            $totalWithNds = $totalAmount;
            $totalNds = $hasNds ? round($totalWithNds * $ndsRate / (1 + $ndsRate), $precision) : 0;
            $totalNoNds = round($totalWithNds - $totalNds, $precision);
        }

        return [
            'priceOne' => $price,
            'priceOneNoNds' => round($ndsOut ? $price : ($hasNds ? $price / (1 + $ndsRate) : $price), $precision),
            'priceOneWithNds' => round(!$ndsOut ? $price : ($hasNds ? $price * (1 + $ndsRate) : $price), $precision),
            'totalAmount' => $totalAmount,
            'totalNoNds' => $totalNoNds,
            'totalNds' => $totalNds,
            'totalWithNds' => $totalWithNds,
        ];
    }

    public function getProduct_title_chunked($maxStrLen = 70)
    {
        $arr = explode(' ', (string)$this->product_title);
        foreach ($arr as $k => $str) {
            if (mb_strlen($str) > $maxStrLen) {
                $arr[$k] = implode(' ', mb_str_split($str, $maxStrLen));
            }
        }

        return implode(' ', $arr);
    }

    public function getProductTitlePrintable($maxStrLen = 35)
    {
        return implode('<br/>', mb_str_split($this->product_title, $maxStrLen));
    }

    public function getProductTitlePrintableRowsCount($maxStrLen = 35)
    {
        return count(explode('<br/>', $this->getProductTitlePrintable($maxStrLen)));
    }
}
