<?php

namespace common\models\document;

use Yii;

/**
 * This is the model class for table "invoice_facture_payment_document".
 *
 * @property integer $invoice_facture_id
 * @property string $payment_document_number
 * @property string $payment_document_date
 *
 * @property InvoiceFacture $invoiceFacture
 */
class InvoiceFacturePaymentDocument extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invoice_facture_payment_document';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['invoice_facture_id', 'payment_document_number', 'payment_document_date'], 'required'],
            [['invoice_facture_id'], 'integer'],
            [['payment_document_date'], 'safe'],
            [['payment_document_number'], 'string', 'max' => 255],
            [['invoice_facture_id'], 'exist', 'skipOnError' => true, 'targetClass' => InvoiceFacture::className(), 'targetAttribute' => ['invoice_facture_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'invoice_facture_id' => 'Invoice Facture ID',
            'payment_document_number' => 'Payment Document Number',
            'payment_document_date' => 'Payment Document Date',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceFacture()
    {
        return $this->hasOne(InvoiceFacture::className(), ['id' => 'invoice_facture_id']);
    }
}
