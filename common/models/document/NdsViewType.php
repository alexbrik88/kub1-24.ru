<?php

namespace common\models\document;

use Yii;

/**
 * This is the model class for table "nds_view_type".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Invoice[] $invoices
 */
class NdsViewType extends \yii\db\ActiveRecord
{
    const NDS_VIEW_IN = 0;
    const NDS_VIEW_OUT = 1;
    const NDS_VIEW_WITHOUT = 2;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'nds_view_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoices()
    {
        return $this->hasMany(Invoice::className(), ['nds_view_type_id' => 'id']);
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        return false;
    }
}
