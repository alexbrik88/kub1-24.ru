<?php

namespace common\models\document;

use common\components\date\DateHelper;
use frontend\modules\documents\components\InvoiceHelper;
use php_rutils\RUtils;

/**
 * This is the model class for table "invoice".
 */
class InvoiceAuto extends Invoice
{
    /**
     * @return query\InvoiceAutoQuery
     */
    public static function find($config = [])
    {
        return new query\InvoiceAutoQuery(get_called_class(), $config);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAuto()
    {
        return $this->hasOne(Autoinvoice::className(), ['id' => 'id']);
    }

    /**
     * @return query\InvoiceAutoQuery
     */
    public function createFromAuto($targetDate, $period = null)
    {
        $model = clone $this;
        unset($model->id);
        $model->scenario = 'insert';
        $model->invoice_status_id = status\InvoiceStatus::STATUS_CREATED;
        $model->isNewRecord = true;
        $model->isAutoinvoice = 0;
        $model->autoinvoice_id = $this->id;
        $model->document_date = $targetDate->format('Y-m-d');
        $model->document_number = (string) $model->getNextDocumentNumber($this->company_id, $this->type, null, $model->document_date);
        $model->document_additional_number = $model->document_additional_number;
        $model->payment_limit_date = date(DateHelper::FORMAT_DATE, strtotime("+{$this->auto->payment_delay} day"));

        if ($this->contractor->responsible_employee_id) {
            $model->document_author_id = $this->contractor->responsible_employee_id;
        }

        $titleAppend = '';
        if ($this->auto->period == Autoinvoice::MONTHLY) {
            switch ($this->auto->add_month_and_year) {
                case Autoinvoice::ADD_CURRENT:
                    $date = $targetDate;
                    break;
                case Autoinvoice::ADD_NEXT:
                    $date = $targetDate->modify('first day of next month');
                    break;
                case Autoinvoice::ADD_PREV:
                    $date = $targetDate->modify('first day of last month');
                    break;

                default:
                    $date = null;
                    break;
            }
            if ($date) {
                $titleAppend = ' за ' . RUtils::dt()->ruStrFTime([
                    'format' => 'F Y г.',
                    'date' => $date,
                ]);
            }
        } elseif ($this->auto->period == Autoinvoice::QUARTERLY && $period) {
            $year = $targetDate->format('Y');
            switch ($this->auto->add_month_and_year) {
                case Autoinvoice::ADD_CURRENT:
                    $p = $period;
                    $y = $year;
                    break;
                case Autoinvoice::ADD_NEXT:
                    if ($period == 4) {
                        $p = 1;
                        $y = $year + 1;
                    } else {
                        $p = $period + 1;
                        $y = $year;
                    }
                    break;
                case Autoinvoice::ADD_PREV:
                    if ($period == 1) {
                        $p = 4;
                        $y = $year - 1;
                    } else {
                        $p = $period - 1;
                        $y = $year;
                    }
                    break;

                default:
                    $p = $y = null;
                    break;
            }
            if ($p && $y) {
                $titleAppend = " за {$p}-й квартал {$y} г.";
            }
        } elseif ($this->auto->period == Autoinvoice::HALF_YEAR && $period) {
            $year = $targetDate->format('Y');
            switch ($this->auto->add_month_and_year) {
                case Autoinvoice::ADD_CURRENT:
                    $p = $period;
                    $y = $year;
                    break;
                case Autoinvoice::ADD_NEXT:
                    if ($period == 2) {
                        $p = 1;
                        $y = $year + 1;
                    } else {
                        $p = 2;
                        $y = $year;
                    }
                    break;
                case Autoinvoice::ADD_PREV:
                    if ($period == 1) {
                        $p = 2;
                        $y = $year - 1;
                    } else {
                        $p = 1;
                        $y = $year;
                    }
                    break;

                default:
                    $p = $y = null;
                    break;
            }
            if ($p && $y) {
                $titleAppend = " за {$p}-е полугодие {$y} г.";
            }
        } elseif ($this->auto->period == Autoinvoice::ANNUALLY) {
            $year = $targetDate->format('Y');
            switch ($this->auto->add_month_and_year) {
                case Autoinvoice::ADD_CURRENT:
                    $y = $year;
                    break;
                case Autoinvoice::ADD_NEXT:
                    $y = $year + 1;
                    break;
                case Autoinvoice::ADD_PREV:
                    $y = $year - 1;
                    break;

                default:
                    $y = null;
                    break;
            }
            if ($y) {
                $titleAppend = " за {$y} год.";
            }
        }

        $orderArray = [];
        foreach ($this->orders as $order) {
            $cloneOrder = clone $order;
            unset($cloneOrder->id);
            $cloneOrder->isNewRecord = true;
            $cloneOrder->invoice_id = null;
            $cloneOrder->populateRelation('invoice', $model);

            if ($cloneOrder->product->production_type == 0 && $titleAppend) {
                $cloneOrder->product_title .= $titleAppend;
            }
            $orderArray[] = $cloneOrder;
        }

        $model->populateRelation('orders', $orderArray);

        if (InvoiceHelper::save($model)) {
            return $model;
        }

        \common\components\helpers\ModelHelper::logErrors($model, __METHOD__);

        return null;
    }

    /**
     * @param Order $order
     * @return Order
     */
    public function cloneOrder(Order $order, Invoice $model)
    {
        $cloneOrder = clone $order;
        unset($cloneOrder->id);
        $cloneOrder->isNewRecord = true;
        $cloneOrder->invoice_id = null;
        $cloneOrder->populateRelation('invoice', $model);

        return $cloneOrder;
    }

    /**
     * @return boolean
     */
    public function getIsChangeContractorAllowed()
    {
        return true;
    }
}
