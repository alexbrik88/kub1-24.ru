<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 10/15/15
 * Time: 2:54 PM
 * Email: t.kanstantsin@gmail.com
 */

namespace common\models\document;


use common\models\file\File;

class DocumentFile extends File
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            'file' => [['file'], 'file',], /** @todo set limitation of file types. */
        ]);
    }
}
