<?php

namespace common\models\document;

use common\components\calculator\CalculatorHelper;
use common\models\product\Product;
use frontend\models\Documents;
use Yii;

/**
 * This is the model class for table "order_proxy".
 *
 * @property integer $id
 * @property integer $order_id
 * @property integer $proxy_id
 * @property integer $product_id
 * @property integer $quantity
 *
 * @property Order $order
 * @property Proxy $proxy
 * @property Product $product
 */
class OrderProxy extends AbstractOrder
{

    public static $SUM_TYPE = [
        '2' => 'sales',
        '1' => 'purchase',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_proxy';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['quantity'], 'filter', 'filter' => function ($value) {
                return str_replace(",", ".", $value);
            },],
            [['quantity'], 'number', 'numberPattern' => '/^\d+(\.\d*)?$/'],
            [['quantity'], function ($attribute, $params) {
                if ($this->hasErrors($attribute)) return;
                $title = 'для «' . $this->order->product_title . '»';
                $max = self::availableQuantity($this->order, $this->proxy_id);
                if ($this->$attribute <= 0) {
                    $this->addError($attribute, "Количество {$title} должно быть больше 0.");
                } elseif (bccomp($this->$attribute, $max, 4) > 0) {
                    $this->addError($attribute, "Максимальное возможное количество {$title}: {$max}.");
                }
            }],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_id' => 'Order ID',
            'proxy_id' => 'Proxy ID',
            'product_id' => 'Product ID',
            'quantity' => 'Количество',
        ];
    }

    /**
     * @param $proxy_id
     * @param Order $order
     * @return OrderProxy
     */
    public static function createFromOrder($proxy_id, Order $order)
    {
        if (OrderProxy::compareWithOrderByQuantity($order) != 0 &&
            $order->product->production_type == Product::PRODUCTION_TYPE_GOODS
        ) {
            $order_proxy = new OrderProxy();
            $order_proxy->order_id = $order->id;
            $order_proxy->proxy_id = $proxy_id;
            $order_proxy->product_id = $order->product_id;
            $order_proxy->quantity = OrderProxy::compareWithOrderByQuantity($order);

            return $order_proxy;
        }

        return null;
    }

    /**
     * @param $pacingListId
     * @return mixed
     */
    public static function countByProxy($pacingListId)
    {
        return count(OrderProxy::findAll(['proxy_id' => $pacingListId]));
    }

    /**
     * @param $order_id
     * @param $proxy_id
     * @param $quantity
     */
    public static function setQuantity($order_id, $proxy_id, $quantity)
    {
        $order_proxy = OrderProxy::findOne(['order_id' => $order_id, 'proxy_id' => $proxy_id]);
        $order = Order::findOne(['id' => $order_id]);
        if ($order_proxy != null) {
            $order_proxy->quantity = $quantity;
            $order_proxy->save();
        }
    }

    /**
     * @param $order_id
     * @param $proxy_id
     * @return bool
     */
    public static function ifExist($order_id, $proxy_id)
    {
        return OrderProxy::findOne(['order_id' => $order_id, 'proxy_id' => $proxy_id]) ? true : false;
    }

    /**
     * returns residual of order and sum of proxies, if haven't returns null
     * @param Order $order
     * @return int
     */
    public static function compareWithOrderByQuantity(Order $order)
    {
        $query = OrderProxy::find()->where(['order_id' => $order->id]);
        if ($query != null) {
            $proxies_quantity = $query->sum('quantity');
        } else {
            $proxies_quantity = 0;
        }
        if ($proxies_quantity == $order->quantity) {

            return 0;
        } elseif ($proxies_quantity < $order->quantity) {

            return $order->quantity - $proxies_quantity;
        } else {

            return null;
        }
    }

    /**
     * @param Invoice $invoice
     * @return int|mixed
     */
    public static function compareWithInvoiceBySum(Invoice $invoice)
    {
        $opl = 0;
        $ordersSum = 0;
        if ($invoice->orders != null) {
            foreach ($invoice->orders as $order) {
                if ($order->product->production_type == Product::PRODUCTION_TYPE_GOODS) {
                    $ordersSum += $order->quantity;
                    $opl += OrderProxy::find()->where(['order_id' => $order->id])->sum('quantity');
                }
            }
        }
        if ($ordersSum > $opl) {
            return $ordersSum - $opl;
        } elseif ($ordersSum == $opl) {
            return 0;
        } else {
            return 0;
        }
    }

    /**
     * @param $proxy_id
     * @return mixed
     */
    public static function allQuantitySum($proxy_id)
    {
        return OrderProxy::find()->where(['proxy_id' => $proxy_id])->sum('quantity');
    }

    public static function allProxySum($proxy_id, $sum_type)
    {
        return OrderProxy::find()->where(['proxy_id' => $proxy_id])->sum('amount_' . $sum_type . '_with_vat');
    }

    /**
     * @param $proxy_id
     * @return array
     */
    public static function getAvailable($proxy_id)
    {
        $proxy = Proxy::findOne(['id' => $proxy_id]);
        $invoice = Invoice::findOne(['id' => $proxy->invoice_id]);
        $available = [];
        if ($invoice->orders != null) {
            foreach ($invoice->orders as $order) {
                if ($order->product->production_type == Product::PRODUCTION_TYPE_GOODS) {
                    $order_proxy = OrderProxy::findOne(['order_id' => $order->id, 'proxy_id' => $proxy_id]);
                    if ($order_proxy == null && OrderProxy::compareWithOrderByQuantity($order) != 0) {
                        $available[$order->product_title] = OrderProxy::createFromOrder($proxy_id, $order)->quantity;
                    }
                }
            }
        }

        return $available;
    }

    /**
     * @param Order $order
     * @param integer $proxy_id
     * @return numder
     */
    public static function availableQuantity(Order $order, $proxy_id = null)
    {
        $used_quantity = self::find()
            ->andWhere(['order_id' => $order->id])
            ->andWhere(['not', ['proxy_id' => $proxy_id]])
            ->sum('quantity');

        $available = $order->quantity - $used_quantity;

        return max(0, $available);
    }

    /**
     * @return int
     */
    public function getAvailableQuantity()
    {
        $order_quantity = $this->order->quantity;

        $proxies_quantity = OrderProxy::find()->where(['order_id' => $this->order_id])->andWhere(['!=','id',$this->id])->sum('quantity');

        $result = $order_quantity - $proxies_quantity;

        return $result;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProxy()
    {
        return $this->hasOne(Proxy::className(), ['id' => 'proxy_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocument()
    {
        return $this->getProxy();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                if ($this->proxy->type == Documents::IO_TYPE_IN) {
                    $this->product->setCountOrderBuy($this->quantity, $this->order->invoice->store_id);
                } else {
                    $this->product->setCountOrderSale($this->quantity, $this->order->invoice->store_id);
                    $this->order->updateAttributes([
                        'reserve' => max(0, $this->order->reserve - $this->quantity),
                    ]);
                }
            } else {
                $quantityDiff = $this->quantity - $this->getOldAttribute('quantity');
                if ($quantityDiff !== 0) {
                    if ($this->proxy->type == Documents::IO_TYPE_IN) {
                        $this->product->setCountOrderBuy($this->quantity - $this->getOldAttribute('quantity'), $this->order->invoice->store_id);
                    } else {
                        $this->product->setCountOrderSale($this->quantity - $this->getOldAttribute('quantity'), $this->order->invoice->store_id);
                        $this->order->updateAttributes([
                            'reserve' => max(0, $this->order->reserve + $this->getOldAttribute('quantity') - $this->quantity),
                        ]);
                    }
                }
            }

            return true;
        } else {
            return false;
        }
    }
}
