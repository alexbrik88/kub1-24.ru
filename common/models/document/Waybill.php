<?php

namespace common\models\document;

use common\components\excel\Excel;
use common\components\helpers\ModelHelper;
use common\components\pdf\PdfRenderer;
use common\components\TextHelper;
use common\components\date\DateHelper;
use common\components\date\DatePickerFormatBehavior;
use common\models\Agreement;
use common\models\AgreementType;
use common\models\Contractor;
use common\models\document\DocumentType;
use common\models\document\query\WaybillQuery;
use common\models\document\status\WaybillStatus;
use common\models\employee\Employee;
use common\models\file\File;
use common\models\product\Product;
use common\models\product\ProductUnit;
use frontend\models\Documents;
use frontend\models\log\Log;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\modules\documents\assets\DocumentPrintAsset;
use frontend\modules\documents\components\InvoiceHelper;
use frontend\modules\documents\components\Message;
use frontend\modules\export\models\export\Export;
use frontend\modules\export\models\one_c\OneCExport;
use Yii;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This is the model class for table "waybill".
 *
 * @property string $created_at
 * @property integer $invoice_id
 * @property integer $status_out_id
 * @property string $status_out_updated_at
 * @property integer $status_out_author_id
 * @property string $document_number
 * @property string $document_additional_number
 * @property string $waybill_number
 * @property string $waybill_date
 * @property string $basis_name
 * @property string $basis_document_number
 * @property string $basis_document_date
 * @property integer $consignor_id
 * @property integer $consignee_id
 * @property integer $orders_sum
 * @property integer $proxy_number
 * @property integer $proxy_date
 * @property boolean $contractor_address
 * @property string $given_out_position
 * @property string $given_out_fio
 *
 * @property string $release_allowed_position
 * @property string $release_allowed_fullname
 * @property string $release_produced_position
 * @property string $release_produced_fullname
 * @property string $cargo_accepted_position
 * @property string $cargo_accepted_fullname
 * @property string $driver_license
 * @property string $delivery_time
 * @property string $organization
 * @property string $car
 * @property string $car_number
 * @property string $license_card
 * @property string $transportation_type
 * @property string $transportation_route
 * @property string $trailer1_brand
 * @property string $trailer1_number
 * @property string $trailer1_garage_number
 * @property string $trailer2_brand
 * @property string $trailer2_number
 * @property string $trailer2_garage_number
 *
 * @property string $loading_operations
 * @property string $unloading_operations
 * @property string $loading_method_name
 * @property string $unloading_method_name
 * @property string $loading_method_code
 * @property string $unloading_method_code
 * @property string $loading_time_come
 * @property string $loading_time_gone
 * @property string $loading_time_operations
 * @property string $unloading_time_come
 * @property string $unloading_time_gone
 * @property string $unloading_time_operations
 * @property string $transportation_downtime_loading
 * @property string $transportation_downtime_unloading
 * @property string $taxation
 * @property string $taximaster
 * @property integer $transportation_distance
 * @property integer $transportation_in_city
 * @property integer $transportation_group_1
 * @property integer $transportation_group_2
 * @property integer $transportation_group_3
 * @property integer $transportation_cost
 * @property string $loading_contractor
 * @property string $unloading_contractor
 * @property string $completed_per_ton
 * @property string $completed_per_ton_km
 * @property string $completed_loading_and_unloading_works
 * @property string $completed_underload
 * @property string $completed_forwarding
 * @property string $completed_excess_simple_loading
 * @property string $completed_excess_simple_unloading
 * @property string $completed_urgency
 * @property string $completed_special_transport
 * @property string $completed_another_surcharge
 * @property string $completed_total
 * @property string $rate_per_ton
 * @property string $rate_per_ton_km
 * @property string $rate_loading_and_unloading_works
 * @property string $rate_underload
 * @property string $rate_forwarding
 * @property string $rate_excess_simple_loading
 * @property string $rate_excess_simple_unloading
 * @property string $rate_urgency
 * @property string $rate_special_transport
 * @property string $rate_another_surcharge
 * @property string $rate_total
 * @property string $to_pay_per_ton
 * @property string $to_pay_per_ton_km
 * @property string $to_pay_loading_and_unloading_works
 * @property string $to_pay_underload
 * @property string $to_pay_forwarding
 * @property string $to_pay_excess_simple_loading
 * @property string $to_pay_excess_simple_unloading
 * @property string $to_pay_urgency
 * @property string $to_pay_special_transport
 * @property string $to_pay_another_surcharge
 * @property string $to_pay_total
 *
 * @property Invoice $invoice
 * @property status\WaybillStatus $statusOut
 * @property Employee $statusOutAuthor
 * @property OrderWaybill[] $orderWaybills
 * @property Contractor $consignor
 * @property Contractor $consignee
 * @property Order[] $orders
 *
 * @property string $fullNumber
 */
class Waybill extends AbstractDocument
{
    /**
     * @var string
     */
    public static $uploadDirectory = 'waybill';

    /**
     * @var string
     */
    public $printablePrefix = 'Товарно-транспортная накладная';
    public $shortPrefix = 'ТТН';

    /**
     * @var string
     */
    public $urlPart = 'waybill';

    /**
     * @var string
     */
    public $agreement;

    public $updatedOrders;

    const CONTRACTOR_ADDRESS_LEGAL = 0;
    const CONTRACTOR_ADDRESS_ACTUAL = 1;

    const LICENSE_STANDART = 0;
    const LICENSE_LIMITED = 1;

    public static $LICENSE_CARD_TYPES = [
        0 => 'стандартная',
        1 => 'ограниченная'
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'waybill';
    }

    /**
     * @return WaybillQuery
     */
    public static function find()
    {
        return new WaybillQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'documentDateBehavior' => [
                'class' => DatePickerFormatBehavior::className(),
                'attributes' => [
                    'waybill_date' => [
                        'message' => 'Дата транспортной накладной указана неверно.',
                    ],
                    'basis_document_date' => [
                        'message' => 'Дата основания указана неверно.',
                    ],
                    'proxy_date' => [
                        'message' => 'Дата доверенности указана неверно.',
                    ],
                ],
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $outClosure = function (Waybill $model) {
            return $model->type == Documents::IO_TYPE_OUT;
        };
        $inClosure = function (Waybill $model) {
            return $model->type == Documents::IO_TYPE_IN;
        };

        return ArrayHelper::merge(parent::rules(), [
            [
                ['orderWaybills'], 'required',
                'message' => 'ТН не может быть пустой.',
            ],

            /** COMMON */
            [['basis_document_number', 'document_additional_number', 'agreement',], 'string'],

            // type depending
            // out
            [
                ['consignor_id', 'consignee_id'], 'exist', 'when' => $outClosure,
                'targetClass' => Contractor::class,
                'targetAttribute' => 'id',
                'filter' => [
                    'or',
                    ['company_id' => null],
                    ['company_id' => $this->invoice->company_id],
                ]
            ],
            [['document_additional_number'], 'string', 'max' => 45, 'when' => $outClosure,],
            [['waybill_number'], 'string', 'max' => 45, 'when' => $outClosure,],
            [['waybill_date', 'basis_document_date', 'proxy_date'], 'safe'],
            [['proxy_number'], 'string', 'max' => 50, 'when' => $outClosure],
            [['given_out_position', 'given_out_fio'], 'string', 'max' => 255, 'when' => $outClosure],
            [['document_number',], 'integer', 'when' => $outClosure],
            [['document_number'], 'outDocumentNumberValidator', 'when' => $outClosure],
            [['contractor_address'], 'integer'],
            // in
            [['basis_name', 'driver_license', 'loading_contractor', 'unloading_contractor',
                'completed_per_ton', 'completed_per_ton_km', 'completed_loading_and_unloading_works',
                'completed_underload', 'completed_forwarding', 'completed_excess_simple_loading',
                'completed_excess_simple_unloading', 'completed_urgency', 'completed_special_transport',
                'completed_another_surcharge', 'completed_total', 'rate_per_ton', 'rate_per_ton_km',
                'rate_loading_and_unloading_works', 'rate_underload', 'rate_forwarding', 'rate_excess_simple_loading',
                'rate_excess_simple_unloading', 'rate_urgency', 'rate_special_transport', 'rate_another_surcharge',
                'rate_total', 'to_pay_per_ton', 'to_pay_per_ton_km', 'to_pay_loading_and_unloading_works',
                'to_pay_underload', 'to_pay_forwarding', 'to_pay_excess_simple_loading',
                'to_pay_excess_simple_unloading', 'to_pay_urgency', 'to_pay_special_transport',
                'to_pay_another_surcharge', 'to_pay_total'], 'string', 'max' => 255],
            [['document_number'], 'inDocumentNumberValidator', 'when' => $inClosure],
            [['transportation_distance', 'transportation_in_city', 'transportation_group_1', 'transportation_group_2',
                'transportation_group_3', 'transportation_cost'], 'integer'],
            [['loading_time_come', 'loading_time_gone', 'loading_time_operations', 'unloading_time_come', 'unloading_time_gone',
                'unloading_time_operations', 'transportation_downtime_loading', 'transportation_downtime_unloading'], 'timeValidator'],
            [['release_allowed_position', 'release_allowed_fullname', 'release_produced_position', 'release_produced_fullname',
                'cargo_accepted_position', 'cargo_accepted_fullname', 'delivery_time', 'organization',
                'car', 'car_number', 'license_card', 'transportation_type', 'transportation_route', 'trailer1_brand',
                'trailer1_number', 'trailer1_garage_number', 'trailer2_brand', 'trailer2_number', 'trailer2_garage_number',
                'loading_operations', 'unloading_operations', 'loading_method_name', 'unloading_method_name',
                'loading_method_code', 'unloading_method_code', 'taxation', 'taximaster'], 'safe'],
            [['updatedOrders'], 'safe'],
        ]);
    }

    public function timeValidator($attribute, $params) {
        // "HH:MM" and "H:MM"
        if (!preg_match("/^(?(?=\d{2})(?:2[0-3]|[01][0-9])|[0-9]):[0-5][0-9]$/", $this->{$attribute})) {
            $this->addError($attribute, 'Неверный формат времени: '.$this->attributeLabels()[$attribute]);
        }
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function inDocumentNumberValidator($attribute, $params)
    {
        $query = $this->invoice->getWaybills()
            ->andWhere(['!=', 'id', $this->id])
            ->andWhere([
                'document_number' => $this->$attribute,
            ]);

        if (!empty($this->document_date)) {
            $year = date('Y', strtotime($this->document_date));
            $query->andWhere(['between', 'doc.document_date', $year.'-01-01', $year.'-12-31']);
        }

        if ($this->document_additional_number) {
            $query->andWhere(['document_additional_number' => $this->document_additional_number]);
        } else {
            $query->andWhere(['or',
                ['document_additional_number' => ''],
                ['document_additional_number' => null],
            ]);
        }

        if ($query->exists()) {
            $this->addError($attribute, 'Такой номер уже существует, одинаковые номера не допустимы');
        }
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function outDocumentNumberValidator($attribute, $params)
    {
        $isNumberExists = function ($number) {
            $query1 = self::find()->alias('doc')->joinWith(['invoice'])
                ->andWhere(['not', ['doc.id' => $this->id]])
                ->andWhere([
                    'invoice.is_deleted' => false,
                    'invoice.company_id' => $this->invoice->company_id,
                    'doc.type' => $this->type,
                    'doc.document_number' => $number,
                ]);
            $query2 = Act::find()->alias('doc')->joinWith(['invoice'])
                ->andWhere(['!=', 'doc.invoice_id', $this->invoice->id])
                ->andWhere([
                    'invoice.is_deleted' => false,
                    'invoice.company_id' => $this->invoice->company_id,
                    'doc.type' => $this->type,
                    'doc.document_number' => $number,
                ]);
            $query3 = InvoiceFacture::find()->alias('doc')->joinWith(['invoice'])
                ->andWhere(['!=', 'doc.invoice_id', $this->invoice->id])
                ->andWhere([
                    'invoice.is_deleted' => false,
                    'invoice.company_id' => $this->invoice->company_id,
                    'doc.type' => $this->type,
                    'doc.document_number' => $number,
                ]);

            if ($this->document_date) {
                $year = date('Y', strtotime($this->document_date));
                $query1->andWhere(['between', 'doc.document_date', $year.'-01-01', $year.'-12-31']);
                $query2->andWhere(['between', 'doc.document_date', $year.'-01-01', $year.'-12-31']);
                $query3->andWhere(['between', 'doc.document_date', $year.'-01-01', $year.'-12-31']);
            }
            if ($this->document_additional_number) {
                $query1->andWhere(['doc.document_additional_number' => $this->document_additional_number]);
                $query2->andWhere(['doc.document_additional_number' => $this->document_additional_number]);
                $query3->andWhere(['doc.document_additional_number' => $this->document_additional_number]);
            } else {
                $query1->andWhere(['or',
                    ['doc.document_additional_number' => ''],
                    ['doc.document_additional_number' => null],
                ]);
                $query2->andWhere(['or',
                    ['doc.document_additional_number' => ''],
                    ['doc.document_additional_number' => null],
                ]);
                $query3->andWhere(['or',
                    ['doc.document_additional_number' => ''],
                    ['doc.document_additional_number' => null],
                ]);
            }

            return $query1->exists() || $query2->exists() || $query3->exists();
        };

        if ($isNumberExists($this->$attribute)) {
            if ($this->findFreeNumber) {
                $nextNumber = $this->$attribute;
                do {
                    $nextNumber++;
                } while ($isNumberExists($nextNumber));
            } else {
                $nextNumber = null;
            }
            $this->addError($attribute, "Номер {$this->$attribute} занят. Укажите свободный номер {$nextNumber}");
        }
    }

    /**
     * Generate document number
     */
    public function newDocumentNumber()
    {
        $this->document_number = $this->document_additional_number = null;

        $query1 = $this->invoice->getActs()
            ->select(new Expression('[[document_number]], IF([[document_additional_number]] = "", NULL, [[document_additional_number]]) [[additional_number]]'));
        $query2 = $this->invoice->getInvoiceFactures()
            ->select(new Expression('[[document_number]], IF([[document_additional_number]] = "", NULL, [[document_additional_number]]) [[additional_number]]'));
        $query = (new Query)
            ->select(['document_number', 'additional_number'])
            ->from(['t' => $query1->union($query2)])
            ->orderBy([
                'document_number' => SORT_ASC,
                'additional_number' => SORT_ASC,
            ]);

        if ($rowArray = $query->all()) {
            foreach ($rowArray as $row) {
                $this->document_number = $row['document_number'];
                $this->document_additional_number = $row['additional_number'];
                if ($this->validate(['document_number'])) {
                    break;
                } else {
                    $this->document_number = $this->document_additional_number = null;
                }
            }
        }

        if (!$this->document_number) {
            $this->document_number = $this->getMaxDocumentNumber();
            do {
                $this->document_number++;
                $this->document_number .= '';
            } while (!$this->validate(['document_number']));
        }
    }

    /**
     * @return boolean
     */
    public function getIsEditableNumber()
    {
        if ($this->isNewRecord || $this->type == Documents::IO_TYPE_IN) {
            return true;
        } else {
            $query1 = $this->invoice->getActs()->andWhere(['document_number' => $this->document_number]);
            $query2 = $this->invoice->getInvoiceFactures()->andWhere(['document_number' => $this->document_number]);
            if ($this->document_additional_number) {
                $query1->andWhere(['document_additional_number' => $this->document_additional_number]);
                $query2->andWhere(['document_additional_number' => $this->document_additional_number]);
            } else {
                $query1->andWhere(['or',
                    ['document_additional_number' => ''],
                    ['document_additional_number' => null],
                ]);
                $query2->andWhere(['or',
                    ['document_additional_number' => ''],
                    ['document_additional_number' => null],
                ]);
            }

            return (!$query1->exists() && !$query2->exists());
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Creation Date',
            'document_author_id' => 'Author',
            'invoice_id' => 'Invoice ID',
            'status_out_id' => 'Waybill Status ID',
            'status_out_updated_at' => 'Status Date',
            'status_out_author_id' => 'Status Author',
            'document_date' => 'Update Date',
            'document_additional_number' => 'Additional Number',
            'waybill_number' => 'Номер транспортной накладной',
            'waybill_date' => 'Дата транспортной накладной',
            'basis_name' => 'Основание',
            'basis_document_number' => 'Номер основания',
            'basis_document_date' => 'Дата основания',
            'consignor_id' => 'Грузоотправитель',
            'consignee_id' => 'Грузополучатель',
            'document_number' => 'Номер документа',
            'given_out_position' => 'Должность',
            'given_out_fio' => 'ФИО',
            'release_allowed_position' => 'Отпуск груза разрешил (должность)',
            'release_allowed_fullname' => 'Отпуск груза разрешил (ФИО)',
            'release_produced_position' => 'Отпуск груза произвел (должность)',
            'release_produced_fullname' => 'Отпуск груза (ФИО)',
            'cargo_accepted_position' => 'Груз к перевозке принял (должность)',
            'cargo_accepted_fullname' => 'Груз к перевозке принял (ФИО)',
            'driver_license' => 'Удостоверение',
            'delivery_time' => 'Срок доставки груза',
            'organization' => 'Организация',
            'car' => 'Автомобиль',
            'car_number' => 'Государственный номерной знак',
            'license_card' => 'Лицензионная карточка',
            'transportation_type' => 'Вид перевозки',
            'transportation_route' => 'Маршрут',
            'trailer1_brand' => 'Прицеп 1 (марка)',
            'trailer1_number' => 'Прицеп 1 (гос. номер)',
            'trailer1_garage_number' => 'Прицеп 1 (гаражный номер)',
            'trailer2_brand' => 'Прицеп 2 (марка)',
            'trailer2_number' => 'Прицеп 2 (гос. номер)',
            'trailer2_garage_number' => 'Прицеп 2 (гаражный номер)',
            'loading_operations' => 'Погрузка - дополнительные операции',
            'unloading_operations' => 'Разгрузка - дополнительные операции',
            'loading_method_name' => 'Погрузка - способ',
            'unloading_method_name' => 'Разгрузка - способ',
            'loading_method_code' => 'Погрузка - код',
            'unloading_method_code' => 'Разгрузка - код',
            'loading_time_come' => 'Погрузка - время прибытия',
            'loading_time_gone' => 'Погрузка - время убытия',
            'loading_time_operations' => 'Погрузка - время дополнительных операций',
            'unloading_time_come' => 'Разгрузка - время прибытия',
            'unloading_time_gone' => 'Разгрузка - время убытия',
            'unloading_time_operations' => 'Разгрузка - время дополнительных операций',
            'transportation_distance' => 'Расстояние перевозки - всего',
            'transportation_in_city' => 'Расстояние перевозки - в городе',
            'transportation_group_1' => 'Расстояние перевозки - группа I',
            'transportation_group_2' => 'Расстояние перевозки - группа II',
            'transportation_group_3' => 'Расстояние перевозки - группа III',
            'transportation_cost' => 'Сумма за транспортные услуги (с клиента)',
            'transportation_downtime_loading' => 'Время простоя - погрузка',
            'transportation_downtime_unloading' => 'Время простоя - разгрузка',
            'taxation' => 'Таксировка',
            'taximaster' => 'Таксировщик',
            'loading_contractor' => 'Исполнитель погрузки',
            'unloading_contractor' => 'Исполнитель разгрузки',
        ];
    }


    /**
     * @return OrderWaybill
     */
    public function createOrderWaybill()
    {
        $model = new OrderWaybill();
        $model->waybill_id = $this->id;


        return $model;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusOut()
    {
        return $this->hasOne(status\WaybillStatus::className(), ['id' => 'status_out_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentStatus()
    {
        return $this->getStatusOut();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(Invoice::className(), ['id' => 'invoice_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(status\WaybillStatus::className(), ['id' => 'status_out_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusOutAuthor()
    {
        return $this->hasOne(Employee::className(), ['id' => 'status_out_author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreementType()
    {
        return $this->hasOne(AgreementType::className(), ['id' => 'basis_document_type_id']);
    }

    /**
     * @inheritdoc
     */
    public function getFile()
    {
        return $this->hasOne(File::className(), ['owner_id' => 'id'])
            ->onCondition(['owner_model' => Waybill::className(),]);
    }

    /**
     * @inheritdoc
     */
    public function getFiles()
    {
        return $this->hasMany(File::className(), ['owner_id' => 'id'])
            ->onCondition([File::tableName() . '.owner_model' => Waybill::className(),]);
    }

    /**
     * @inheritdoc
     */
    public function getScanDocuments()
    {
        return $this->hasMany(ScanDocument::className(), ['owner_id' => 'id'])
            ->onCondition([ScanDocument::tableName() . '.owner_model' => Waybill::className(),]);
    }

    /**
     * @inheritdoc
     */
    public function ordersLoad($ordersData)
    {
        $orderArray = [];
        $this->removedOrders = ArrayHelper::index($this->orderWaybills, 'order_id');
        if (is_array($ordersData)) {
            foreach ($ordersData as $data) {
                $orderId = ArrayHelper::remove($data, 'order_id');
                $order = $orderId ? Order::findOne(['id' => $orderId, 'invoice_id' => $this->invoice_id]) : null;
                if ($order !== null) {
                    $orderWaybill = OrderWaybill::findOne([
                        'order_id' => $order->id,
                        'waybill_id' => $this->id,
                    ]);
                    if ($orderWaybill === null) {
                        $orderWaybill = new OrderWaybill([
                            'order_id' => $order->id,
                            'waybill_id' => $this->id,
                            'product_id' => $order->product_id,
                        ]);
                    } else {
                        unset($this->removedOrders[$orderId]);
                    }
                    $orderWaybill->load($data, '');

                    $orderArray[$orderId] = $orderWaybill;
                }
            }
        }

        $this->populateRelation('orderWaybills', $orderArray);
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        $agreement = explode('&', $this->agreement);
        if (is_array($agreement)) {
            $this->basis_name = isset($agreement[0]) ? $agreement[0] : null;
            $this->basis_document_number = isset($agreement[1]) ? $agreement[1] : null;
            $this->basis_document_date = isset($agreement[2]) ? $agreement[2] : null;
            $this->basis_document_type_id = isset($agreement[3]) ? $agreement[3] : null;
        } else {
            $this->basis_name = null;
            $this->basis_document_number = null;
            $this->basis_document_date = null;
            $this->basis_document_type_id = null;
        }

        return parent::beforeValidate();
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                if ($this->document_date === null) {
                    $this->document_date = date(DateHelper::FORMAT_DATE);
                }
                if (empty($this->document_number)) {
                    $this->newDocumentNumber();
                    $this->ordinal_document_number = $this->document_number;
                }
            }

            if ($this->type == Documents::IO_TYPE_OUT) {
                if ($insert) {
                    $this->status_out_id = status\WaybillStatus::STATUS_CREATED;
                }

                if ($insert || $this->isAttributeChanged('status_out_id')) {
                    $this->status_out_updated_at = time();
                    $this->status_out_author_id = Yii::$app->user->id;
                }
            }

            $this->object_guid = OneCExport::generateGUID();

            if ($this->isAttributeChanged('status_out_id') && $this->status_out_id == status\WaybillStatus::STATUS_REJECTED) {
                foreach ($this->orderWaybills as $order) {
                    $order->resetProductCount();
                }
            }

            ModelHelper::HtmlEntitiesModelAttributes($this);

            if ($this->delivery_time !== null && (empty($this->delivery_time) || date_create_from_format('Y-m-d', $this->delivery_time) === false)) {
                $this->delivery_time = null;
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            foreach ($this->getOrderWaybills()->all() as $order) {
                if (!$order->delete()) {
                    return false;
                }
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if (!$insert) {
            InvoiceHelper::checkForWaybill($this->invoice_id);
        }
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();

        InvoiceHelper::checkForWaybill($this->invoice_id);
    }

    /**
     *
     */
    public function generateDocumentNumber()
    {
        $year = date('Y', strtotime($this->document_date));
        $productionTypeArray = explode(', ', $this->invoice->production_type);
        $lastWaybillId = self::find()
            ->byCompany(Yii::$app->user->identity->company->id)
            ->byIOType($this->type)
            ->andWhere(['between', self::tableName().'.document_date', $year.'-01-01', $year.'-12-31'])
            ->max('ordinal_document_number');
        $lastActId = Act::find()
            ->byCompany(Yii::$app->user->identity->company->id)
            ->byIOType($this->type)
            ->andWhere(['between', Act::tableName().'.document_date', $year.'-01-01', $year.'-12-31'])
            ->max('ordinal_document_number');
        if (in_array(Product::PRODUCTION_TYPE_GOODS, $productionTypeArray)) {
            if ($this->invoice->act !== null) {
                $this->ordinal_document_number = $lastWaybillId + 1;
                $this->document_number = $lastWaybillId + 1;
                $this->document_date = $this->invoice->act->document_date;
                $this->document_additional_number = $this->invoice->act->document_additional_number;
            } else {
                if ($lastActId > $lastWaybillId) {
                    $documentNumber = $lastActId + 1;
                } else {
                    $documentNumber = $lastWaybillId + 1;
                }
                $number = $this->checkOnEmptyDocumentNumber($documentNumber);
                $this->document_number = $number;
                $this->ordinal_document_number = $number;
            }
        } else {
            $number = $this->checkOnEmptyDocumentNumber($lastActId + 1);
            $this->document_number = $number;
            $this->ordinal_document_number = $number;
        }
    }

    /**
     * @param $number
     * @return mixed
     */
    public function checkOnEmptyDocumentNumber($number)
    {
        $forbiddenId = [];
        $query = Act::find()
            ->byCompany(Yii::$app->user->identity->company->id)
            ->byIOType($this->type);
        $subQuery = new Query();
        $subQuery->select([
            Invoice::tableName() . '.id',
            Invoice::tableName() . '.production_type',
        ]);
        $subQuery->from(Invoice::tableName());
        $query->leftJoin(['u' => $subQuery], 'u.id = ' . Act::tableName() . '.invoice_id')->andWhere(['u.production_type' => Product::PRODUCTION_TYPE_SERVICE . ', ' . Product::PRODUCTION_TYPE_GOODS]);
        $actArray = $query->all();
        foreach ($actArray as $act) {
            /* @var Waybill $act */
            if ($act->invoice->waybill == null) {
                $forbiddenId[] = $act->ordinal_document_number;
            }
        }
        if (in_array($number, $forbiddenId)) {
            $number = $this->checkOnEmptyDocumentNumber($number + 1);
        }

        return $number;
    }

    /**
     * @inheritdoc
     */
    public function getFullNumber()
    {
        if ($this->document_number !== null) {
            $documentNumber = $this->document_number;
        } else {
            $documentNumber = $this->invoice->ordinal_document_number;
        }

        return $this->type == Documents::IO_TYPE_OUT
            ? $documentNumber . $this->document_additional_number
            : $documentNumber;
    }

    /**
     * @return string
     */
    public function getPrintTitle()
    {
        return str_replace([' ', '"'], ['_'],
            $this->printablePrefix . '_№' . $this->getFullNumber() . '_' .
            date_format(date_create($this->document_date), 'd.m.Y') . '_' .
            $this->invoice->company_name_short
        );
    }

    /**
     * Get formatted log message from Log model
     * @param Log $log
     * @return string
     */
    public function getLogMessage(Log $log)
    {
        $title = 'Товарно-транспортная накладная №';
        $invoiceText = '';
        $invoice = Invoice::findOne($log->getModelAttributeNew('invoice_id'));

        $ioType = $log->getModelAttributeNew('type');
        if ($ioType == Documents::IO_TYPE_IN) {
            $title .= $log->getModelAttributeNew('document_number');
        } else {
            $title .= $log->getModelAttributeNew('document_number') . ' ' . $log->getModelAttributeNew('document_additional_number');
        }

        $link = Html::a($title, [
            '/documents/waybill/view',
            'type' => $log->getModelAttributeNew('type'),
            'id' => $log->getModelAttributeNew('id'),
        ]);

        if ($invoice) {
            $amount = TextHelper::invoiceMoneyFormat($invoice->total_amount_with_nds, 2) . ' руб.';
            $contractorName = $invoice->contractor ? ', ' . $invoice->contractor->getTitle(true) : '';
            $invoiceText = ', ' . $amount . $contractorName . ',';
        }

        switch ($log->log_event_id) {
            case LogEvent::LOG_EVENT_CREATE:
                return $link . $invoiceText . ' была создана.';
            case LogEvent::LOG_EVENT_DELETE:
                return $title . $invoiceText . ' была удалена.';
            case LogEvent::LOG_EVENT_UPDATE:
                return $link . $invoiceText . ' была изменена.';
            case LogEvent::LOG_EVENT_UPDATE_STATUS:
                return $link . $invoiceText . ' статус "' . WaybillStatus::findOne($log->getModelAttributeNew('status_out_id'))->name . '"';
            default:
                return $log->message;
        }
    }

    /**
     * @param bool|false $withNds
     * @return int
     */
    public function getTotalAmount($withNds = false)
    {
        return $withNds ? $this->getTotalAmountWithNds() : $this->getTotalAmountNoNds();
    }

    /**
     * @return int
     */
    public function getTotalNds()
    {
        return $this->getTotalPLNds();
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        $orders = $this->invoice->getOrdersByProductType(Product::PRODUCTION_TYPE_GOODS);
        $quantity = 0;

        foreach ($orders as $order) {
            $quantity += $order->quantity;
        }

        return $quantity;
    }

    /**
     * @return array
     */
    public function getConsignorArray()
    {
        $query = Contractor::getSorted()
            ->byCompany($this->invoice->company_id)
            ->byDeleted()
            ->andWhere(['or', [Contractor::tableName() . '.is_seller' => 1], [Contractor::tableName() . '.is_customer' => 1]])
            ->andWhere(['!=', Contractor::tableName() . '.id', $this->invoice->contractor_id])
            ->all();

        return ArrayHelper::merge(['' => 'Он же'], ArrayHelper::map($query, 'id', 'shortName'));
    }

    /**
     * @return null|static
     */
    public function getConsignor()
    {
        return Contractor::findOne([$this->consignor_id]);
    }

    /**
     * @return null|static
     */
    public function getConsignee()
    {
        return Contractor::findOne([$this->consignee_id]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['id' => 'order_id'])->viaTable('order_waybill', ['waybill_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderWaybills()
    {
        return $this->hasMany(OrderWaybill::className(), ['waybill_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSignBasisDocument()
    {
        return $this->hasOne(DocumentType::className(), ['id' => 'sign_document_type_id']);
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();

        $this->agreement = "{$this->basis_name}&{$this->basis_document_number}&{$this->basis_document_date}&{$this->basis_document_type_id}";
    }

    /**
     * @return string
     */
    public static function generateUid()
    {
        do {
            $randString = TextHelper::randString(5);
        } while (static::find()->byUid($randString)->count() > 0);

        return $randString;
    }

    /**
     * @param $fileName
     * @param $outWaybill
     * @return PdfRenderer
     */
    public static function getRenderer($fileName, $outWaybill, $destination = PdfRenderer::DESTINATION_FILE)
    {
        $renderer = new PdfRenderer([
            'view' => '@documents/views/waybill/pdf-view',
            'params' => array_merge([
                'model' => $outWaybill,
                'message' => new Message(Documents::DOCUMENT_WAYBILL, $outWaybill->type),
                'ioType' => $outWaybill->type,
                'addStamp' => true,
            ]),

            'destination' => $destination,
            'filename' => $fileName,
            'displayMode' => PdfRenderer::DISPLAY_MODE_FULLPAGE,
            'format' => 'A4-L',
        ]);
        \Yii::$app->controller->view->params['asset'] = DocumentPrintAsset::className();

        return $renderer;
    }

    /**
     * @inheritdoc
     */
    public function setStatusPrinted()
    {
        if ($this && in_array($this->status_out_id, [WaybillStatus::STATUS_CREATED, WaybillStatus::STATUS_PRINTED])) {
            LogHelper::save($this, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function ($model) {
                $model->status_out_id = WaybillStatus::STATUS_PRINTED;
                $model->status_out_updated_at = time();
                $model->status_out_author_id = \Yii::$app->user->id;

                return $model->save(true, ['status_out_id', 'status_out_updated_at', 'status_out_author_id']);
            });
        }
    }

    /**
     * @return integer
     */
    public function getTotalAmountWithNds()
    {
        $amount = 0;
        foreach ($this->orderWaybills as $key => $value) {
            $amount += $value->amountWithNds;
        }

        return $amount;
    }

    /**
     * @return integer
     */
    public function getTotalAmountNoNds()
    {
        $amount = 0;
        foreach ($this->orderWaybills as $key => $value) {
            $amount += $value->amountNoNds;
        }

        return $amount;
    }

    /**
     * @return integer
     */
    public function getTotalPLNds()
    {
        return $this->totalAmountWithNds - $this->totalAmountNoNds;
    }

    /**
     * @param $period
     * @param $waybills
     */
    public static function generateXlsTable($period, $waybills)
    {
        $excel = new Excel();

        $fileName = 'Товарные_накладные_' . DateHelper::format($period['from'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) .
            '-' . DateHelper::format($period['to'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $waybills,
            'title' => 'Товарные накладные',
            'columns' => [
                [
                    'attribute' => 'document_date',
                    'value' => function (Waybill $data) {
                        return DateHelper::format($data->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                    },
                ],
                [
                    'attribute' => 'document_number',
                    'value' => function (Waybill $data) {
                        return $data->fullNumber;
                    },
                ],
                [
                    'attribute' => 'invoice.total_amount_with_nds',
                    'value' => function ($data) {
                        return TextHelper::invoiceMoneyFormat($data->totalAmountWithNds, 2);
                    },
                ],
                [
                    'attribute' => 'contractor_id',
                    'value' => function (Waybill $data) {
                        return $data->invoice->contractor_name_short;
                    },
                ],
                [
                    'attribute' => 'status_out_id',
                    'value' => function (Waybill $data) {
                        return $data->statusOut->name;
                    },
                ],
                [
                    'attribute' => 'invoice_document_number',
                    'value' => function (Waybill $data) {
                        return $data->invoice->fullNumber;
                    },
                ],
            ],
            'headers' => [
                'document_date' => 'Дата т/н',
                'document_number' => '№ т/н',
                'invoice.total_amount_with_nds' => 'Сумма',
                'contractor_id' => 'Контрагент',
                'status_out_id' => 'Статус',
                'invoice_document_number' => 'Счёт №',
            ],
            'format' => 'Xlsx',
            'fileName' => $fileName,
        ]);
    }

    /**
     * @return bool|string
     * @throws \yii\db\Exception
     */
    public function generateOneCFile()
    {
        $exportModel = new Export(['company_id' => Yii::$app->user->identity->company->id]);

        $oneCExport = new OneCExport($exportModel);
        if ($oneCExport->findDataObjectWaybill($this->id)) {
            $path = Yii::getAlias('@frontend/runtime/export') . DIRECTORY_SEPARATOR . $oneCExport->createExportFile();
            Yii::$app->db->createCommand("DELETE FROM `export` WHERE `export`.`id` = {$exportModel->id}")->execute();

            return $path;
        }
        return false;
    }

    /**
     * @return string
     */
    public function getOneCFileName()
    {
        return 'Товарно-транспортная накладная №' . $this->getFullNumber() . '.xml';
    }

    /**
     * @return string
     */
    public function getEmailSubject()
    {
        return 'Товарно-транспортная накладная № ' . $this->fullNumber
            . ' от ' . DateHelper::format($this->document_date, 'd.m.y', DateHelper::FORMAT_DATE)
            . ' от ' . $this->invoice->company_name_short;
    }

    /**
     * @return string
     */
    public function getEmailText()
    {
        $document_date = DateHelper::format($this->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
        $sum = TextHelper::invoiceMoneyFormat($this->totalAmountWithNds, 2);

        $text = <<<EMAIL_TEXT
Здравствуйте!

Товарно-транспортная накладная № {$this->fullNumber} от {$document_date} на сумму {$sum} р.
EMAIL_TEXT;

        return $text;
    }

    /**
     * @return array
     * @throws \MpdfException
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function loadEmailFiles()
    {
        $files = [];

        $emailFile = new EmailFile();
        $emailFile->company_id = $this->invoice->company_id;
        $emailFile->file_name = str_replace(['\\', '/'], '_', $this->pdfFileName);
        $emailFile->ext = 'pdf';
        $emailFile->mime = 'application/pdf';
        $emailFile->size = 0;
        $emailFile->type = EmailFile::TYPE_WAYBILL_DOCUMENT;
        if ($emailFile->save()) {
            $path = $emailFile->getFilePath() . DIRECTORY_SEPARATOR . $emailFile->file_name;
            $size = file_put_contents($path, self::getRenderer(null, $this, PdfRenderer::DESTINATION_STRING)->output(false));
            $emailFile->size = ceil($size / 1024);
            if ($emailFile->save(true, ['size'])) {
                $files[] = [
                    'id' => $emailFile->id,
                    'name' => $emailFile->file_name,
                    'previewImg' => '<img src="/img/email/emailPdf.png" class="preview-img">',
                    'size' => $emailFile->size,
                    'deleteUrl' => Url::to(['/email/delete-email-file', 'id' => $emailFile->id]),
                    'downloadUrl' => Url::to(['/email/download-file', 'id' => $emailFile->id]),
                ];
            }
        }

        return $files;
    }

    /**
     * @param OrderWaybill[] $orders
     * @return float|int
     */
    public function getRealMassGross($orders)
    {
        $countedOrders = array_filter($orders, function (OrderWaybill $order) {
            return isset(ProductUnit::$countableUnits[$order->order->product->product_unit_id]) && $order->order->mass_gross > 0;
        });

        if (count($countedOrders) === count($orders)) {
            return array_sum(ArrayHelper::getColumn($orders, function (OrderWaybill $order) {
                    return bcmul($order->order->quantity * $order->order->mass_gross, ProductUnit::$countableUnits[$order->order->product->product_unit_id], 5);
                })
            );
        }

        return 0;
    }

    /**
     * @param OrderWaybill[] $orders
     * @return float|int
     */
    public function getRealMassNet($orders)
    {
        $countedOrders = array_filter($orders, function (OrderWaybill $order) {
            return isset(ProductUnit::$countableUnits[$order->order->product->product_unit_id]) && $order->order->product->weight > 0;
        });

        if (count($countedOrders) === count($orders)) {
            return array_sum(ArrayHelper::getColumn($orders, function (OrderWaybill $order) {
                return bcmul($order->order->quantity * $order->order->product->weight, ProductUnit::$countableUnits[$order->order->product->product_unit_id], 5);
            })
            );
        }

        return 0;
    }

    /**
     * @param OrderWaybill[] $orders
     * @return int
     */
    public function getRealPlaceCount($orders)
    {
        $countedOrders = array_filter($orders, function(OrderWaybill $order) {
            return ($order->order->place_count > 0);
        });

        if (count($countedOrders) == count($orders)) {
            return array_sum(ArrayHelper::getColumn($orders, function(OrderWaybill $order) {
                return floor($order->order->quantity * $order->order->place_count);
            }));
        }

        return 0;
    }

    /**
     * @param array $orderParams
     * @param array $paymentParams
     * @return mixed
     * @throws \Throwable
     */
    public function updateAndSave($orderParams = [], $paymentParams = [])
    {
        return Yii::$app->db->transaction(function ($db) use ($orderParams, $paymentParams) {
            if (!$this->save()) {
                Yii::$app->session->setFlash('error', Html::errorSummary($this));
                $db->transaction->rollBack();

                return false;
            }

            $this->unlinkAll('orders', true);
            $ownOrderArray = [];
            if (is_array($orderParams)) {
                foreach ($orderParams as $orderId => $params) {
                    /* @var $order Order */
                    $order = $orderId ? Order::findOne(['id' => $orderId, 'invoice_id' => $this->invoice->id]) : null;
                    $quantity = ArrayHelper::getValue($params, 'quantity');
                    if (!empty($quantity) && $order) {
                        $ownOrderArray[] = new OrderWaybill([
                            'waybill_id' => $this->id,
                            'order_id' => $order->id,
                            'product_id' => $order->product_id,
                            'quantity' => min(OrderWaybill::availableQuantity($order), max(0, $quantity)),
                        ]);
                    }
                }
            }
            if ($ownOrderArray) {
                foreach ($ownOrderArray as $ownOrder) {
                    $ownOrder->quantity = number_format($ownOrder->quantity, 10, '.', '');
                    if (!$ownOrder->save()) {
                        Yii::$app->session->setFlash('error', Html::errorSummary($ownOrder));
                        $db->transaction->rollBack();

                        return false;
                    }
                }
            } else {
                Yii::$app->session->setFlash('error', 'ТТН не может быть пустой');
                $db->transaction->rollBack();

                return false;
            }

            InvoiceHelper::checkForWaybill($this->invoice->id);

            return true;
        });
    }
}
