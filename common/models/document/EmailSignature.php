<?php

namespace common\models\document;

use common\models\employee\Employee;
use Yii;
use common\models\Company;

/**
 * This is the model class for table "email_signature".
 *
 * @property integer $id
 * @property integer $company_id
 * @property string $text
 * @property integer $employee_id
 *
 * @property Company $company
 */
class EmailSignature extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'email_signature';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['text'], 'trim'],
            [['text'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'text' => 'Text',
            'employee_id' => 'Employee ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'employee_id']);
    }
}
