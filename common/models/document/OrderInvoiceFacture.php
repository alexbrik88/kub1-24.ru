<?php

namespace common\models\document;

use common\models\address\Country;
use common\models\product\Product;
use common\models\product\ProductUnit;
use frontend\models\Documents;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "order_invoice_facture".
 *
 * @property integer $invoice_facture_id
 * @property integer $order_id
 * @property integer $quantity
 * @property integer $country_id
 * @property string $custom_declaration_number
 *
 * @property Order $order
 * @property InvoiceFacture $invoiceFacture
 */
class OrderInvoiceFacture extends AbstractOrder
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_invoice_facture';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['quantity'], 'filter', 'filter' => function ($value) {
                return str_replace(",", ".", $value);
            },],
            [['quantity'], 'required', 'isEmpty' => function ($value) {
                return empty($value);
            },],
            [['quantity'], 'number', 'numberPattern' => '/^\d+(\.\d*)?$/', 'min' => 0.0000000001, 'max' => 9999999999],
            [['custom_declaration_number'], 'string', 'max' => 255],
            [['country_id'], 'required', 'when' => function ($model) {
                return !$model->isNewRecord;
            },],
            [['country_id'], 'exist', 'targetClass' => Country::className(), 'targetAttribute' => 'id'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'invoice_facture_id' => 'Invoice Facture ID',
            'order_id' => 'Order ID',
            'quantity' => 'Количество',
            'empty_unit_code' => 'Empty Unit Code',
            'empty_unit_name' => 'Empty Unit Name',
            'empty_quantity' => 'Empty Quantity',
            'empty_price' => 'Empty Price',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceFacture()
    {
        return $this->hasOne(InvoiceFacture::className(), ['id' => 'invoice_facture_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocument()
    {
        return $this->getInvoiceFacture();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @param int $invoice_facture_id
     * @param Order $order
     * @return OrderInvoiceFacture|null
     */
    public static function createFromOrder($invoice_facture_id, Order $order)
    {
        if ($quantity = self::availableQuantity($order, $invoice_facture_id)) {
            $model = new self([
                'order_id' => $order->id,
                'invoice_facture_id' => $invoice_facture_id,
                'quantity' => $quantity,
                'country_id' => $order->country_id,
                'custom_declaration_number' => $order->custom_declaration_number ?? '',
            ]);

            return $model;
        }

        return null;
    }

    /**
     * @param Order $order
     * @param integer $invoice_facture_id
     * @return int|null
     */
    public static function availableQuantity(Order $order, $invoice_facture_id = null)
    {
        $ordersIds = Invoice::find()
            ->joinWith(['invoiceFactures', 'orders'])
            ->where(['order.product_id' => $order->product_id])
            ->andWhere(['invoice_invoice_facture.invoice_facture_id' => ArrayHelper::getColumn($order->invoice->invoiceFactures, 'id')])
            ->select('order.id')
            ->distinct()
            ->column() ?: [$order->id];

        $orders_quantity = Order::find()->where(['id' => $ordersIds])->sum('quantity');

        $used_quantity = OrderInvoiceFacture::find()
            ->andWhere(['order_id' => $ordersIds])
            ->andWhere(['not', ['invoice_facture_id' => $invoice_facture_id]])
            ->sum('quantity');

        $available = $orders_quantity - $used_quantity;

        return max(0, $available);
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $order = $this->order;
                if (empty($this->country_id)) {
                    $this->country_id = $order->country_id;
                }
                if (empty($this->custom_declaration_number)) {
                    $this->custom_declaration_number = $order->custom_declaration_number ?? '';
                }
            }

            return true;
        }

        return false;
    }
}
