<?php

namespace common\models\document;

use common\models\Company;
use Yii;

/**
 * This is the model class for table "autoinvoicefacture".
 *
 * @property integer $company_id
 * @property integer $rule
 * @property integer $send_auto
 *
 * @property Company $company
 */
class Autoinvoicefacture extends \yii\db\ActiveRecord
{
    const MANUAL = 1;
    const BY_MANUAL_DOC = 2;
    const BY_AUTO_DOC = 3;

    public static $rules = [
        self::MANUAL,
        self::BY_MANUAL_DOC,
        self::BY_AUTO_DOC,
    ];

    public static $ruleLabels = [
        self::MANUAL => 'Вручную',
        self::BY_MANUAL_DOC => 'Дата Счет-фактуры = Дата Акта / Товарной Накладной',
        self::BY_AUTO_DOC => 'Дата Счет-фактуры = Дата АвтоАкта',
    ];

    public static $ruleComments = [
        self::MANUAL => null,
        self::BY_MANUAL_DOC => '(При выставление Акта /ТН вручную, Счет-фактура сразу создается автоматически той же датой, что и Акт / ТН)',
        self::BY_AUTO_DOC => '(Счет-фактура создается той же датой, что Акт, который создается согласно настройкам АвтоАктов) ',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'autoinvoicefacture';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['rule'], 'required'],
            [['rule'], 'in', 'range' => self::$rules],
            [['send_auto'], 'boolean'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'company_id' => 'Company ID',
            'rule' => 'Правило выставления',
            'send_auto' => 'Автоматическая отправка СФ клиенту, после выставления',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        parent::beforeDelete();

        return false;
    }
}
