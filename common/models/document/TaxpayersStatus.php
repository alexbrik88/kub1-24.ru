<?php

namespace common\models\document;

use Yii;

/**
 * This is the model class for table "taxpayers_status".
 *
 * @property integer $id
 * @property string $name
 * @property string $code
 *
 * @property PaymentOrder[] $paymentOrders
 */
class TaxpayersStatus extends \yii\db\ActiveRecord
{
    const DEFAULT_OOO = 1;
    const DEFAULT_IP = 13;
    const NO_VALUE = 25;

    public static $deprecated = [9, 10, 11, 12];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'taxpayers_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string'],
            [['code'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'code' => 'Code',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentOrders()
    {
        return $this->hasMany(PaymentOrder::className(), ['taxpayers_status_id' => 'id']);
    }
}
