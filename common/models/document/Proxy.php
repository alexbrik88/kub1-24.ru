<?php

namespace common\models\document;

use common\components\excel\Excel;
use common\components\helpers\ModelHelper;
use common\components\pdf\PdfRenderer;
use common\components\TextHelper;
use common\components\date\DateHelper;
use common\components\date\DatePickerFormatBehavior;
use common\models\Agreement;
use common\models\AgreementType;
use common\models\Contractor;
use common\models\document\DocumentType;
use common\models\document\query\ProxyQuery;
use common\models\document\status\ProxyStatus;
use common\models\employee\Employee;
use common\models\file\File;
use common\models\product\Product;
use frontend\models\Documents;
use frontend\models\log\Log;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\modules\documents\assets\DocumentPrintAsset;
use frontend\modules\documents\components\InvoiceHelper;
use frontend\modules\documents\components\Message;
use frontend\modules\export\models\export\Export;
use frontend\modules\export\models\one_c\OneCExport;
use Yii;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * This is the model class for table "proxy".
 *
 * @property string $created_at
 * @property integer $invoice_id
 * @property integer $status_out_id
 * @property string $status_out_updated_at
 * @property integer $status_out_author_id
 * @property string $document_number
 * @property string $document_additional_number
 * @property string $basis_name
 * @property string $basis_document_number
 * @property string $basis_document_date
 * @property integer $consignor_id
 * @property integer $consignee_id
 * @property integer $orders_sum
 *
 * @property integer $proxy_person_id
 * @property string $limit_date
 *
 * @property Invoice $invoice
 * @property status\ProxyStatus $statusOut
 * @property Employee $statusOutAuthor
 * @property OrderProxy[] $orderProxies
 * @property Contractor $consignee
 * @property Order[] $orders
 *
 * @property string $fullNumber
 */
class Proxy extends AbstractDocument
{
    /**
     * @var string
     */
    public static $uploadDirectory = 'proxy';

    /**
     * @var string
     */
    public $printablePrefix = 'Доверенность';
    public $shortPrefix = 'Доверенность';

    /**
     * @var string
     */
    public $urlPart = 'proxy';

    /**
     * @var string
     */
    public $agreement;

    const CONTRACTOR_ADDRESS_LEGAL = 0;
    const CONTRACTOR_ADDRESS_ACTUAL = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'proxy';
    }

    /**
     * @return ProxyQuery
     */
    public static function find()
    {
        return new ProxyQuery(get_called_class());
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'documentDateBehavior' => [
                'class' => DatePickerFormatBehavior::className(),
                'attributes' => [
                    'basis_document_date' => [
                        'message' => 'Дата основания указана неверно.',
                    ],
                    'limit_date' => [
                        'message' => 'Дата окончания срока действия доверенности указана неверно.',
                    ],
                ],
            ],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        $outClosure = function (Proxy $model) {
            return $model->type == Documents::IO_TYPE_OUT;
        };
        $inClosure = function (Proxy $model) {
            return $model->type == Documents::IO_TYPE_IN;
        };

        return ArrayHelper::merge(parent::rules(), [
            [
                ['orderProxies'], 'required',
                'message' => 'Доверенность не может быть пустой.',
            ],
            [['type', 'invoice_id'], 'integer'],

            /** COMMON */
            [['basis_document_number', 'document_additional_number', 'agreement',], 'string'],

            // type depending
            // in
            [['basis_name'], 'string', 'max' => 255, 'when' => $inClosure,],
            [['document_number'], 'string',
                'max' => 45,
                'when' => $inClosure,
            ],
            [['document_number'], 'inDocumentNumberValidator', 'when' => $inClosure],
            //[
            //    ['proxy_person_id'], 'exist', 'when' => $inClosure,
            //    'targetClass' => Employee::class,
            //    'targetAttribute' => 'id',
            //    'filter' => [
            //        'or',
            //        ['company_id' => null],
            //        ['company_id' => $this->invoice->company_id],
            //    ]
            //],
            [['proxy_person_id'], 'integer'],
            [['limit_date_input', 'document_date_input'], 'date', 'format' => 'php:d.m.Y', 'message' => 'Дата указана неверно.'],
            [['limit_date'], 'date', 'format' => 'php:Y-m-d', 'message' => 'Дата указана неверно.'],
            [['document_date'], 'date', 'format' => 'php:Y-m-d', 'message' => 'Дата указана неверно.'],
            [['proxy_person_id', 'limit_date_input'], 'required', 'when' => $inClosure]
        ]);
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function inDocumentNumberValidator($attribute, $params)
    {
        $query = $this->invoice->getProxies()
            ->andWhere(['!=', 'id', $this->id])
            ->andWhere([
                'document_number' => $this->$attribute,
            ]);

        if (!empty($this->document_date)) {
            $year = date('Y', strtotime($this->document_date));
            $query->andWhere(['between', 'document_date', $year.'-01-01', $year.'-12-31']);
        }

        if ($this->document_additional_number) {
            $query->andWhere(['document_additional_number' => $this->document_additional_number]);
        } else {
            $query->andWhere(['or',
                ['document_additional_number' => ''],
                ['document_additional_number' => null],
            ]);
        }

        if ($query->exists()) {
            $this->addError($attribute, 'Такой номер уже существует, одинаковые номера не допустимы');
        }
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function outDocumentNumberValidator($attribute, $params)
    {
        $isNumberExists = function ($number) {
            $query1 = self::find()->alias('doc')->joinWith(['invoice'])
                ->andWhere(['not', ['doc.id' => $this->id]])
                ->andWhere([
                    'invoice.is_deleted' => false,
                    'invoice.company_id' => $this->invoice->company_id,
                    'doc.type' => $this->type,
                    'doc.document_number' => $number,
                ]);
            $query2 = Act::find()->alias('doc')->joinWith(['invoice'])
                ->andWhere(['!=', 'doc.invoice_id', $this->invoice->id])
                ->andWhere([
                    'invoice.is_deleted' => false,
                    'invoice.company_id' => $this->invoice->company_id,
                    'doc.type' => $this->type,
                    'doc.document_number' => $number,
                ]);
            $query3 = InvoiceFacture::find()->alias('doc')->joinWith(['invoice'])
                ->andWhere(['!=', 'doc.invoice_id', $this->invoice->id])
                ->andWhere([
                    'invoice.is_deleted' => false,
                    'invoice.company_id' => $this->invoice->company_id,
                    'doc.type' => $this->type,
                    'doc.document_number' => $number,
                ]);

            if ($this->document_date) {
                $year = date('Y', strtotime($this->document_date));
                $query1->andWhere(['between', 'doc.document_date', $year.'-01-01', $year.'-12-31']);
                $query2->andWhere(['between', 'doc.document_date', $year.'-01-01', $year.'-12-31']);
                $query3->andWhere(['between', 'doc.document_date', $year.'-01-01', $year.'-12-31']);
            }
            if ($this->document_additional_number) {
                $query1->andWhere(['doc.document_additional_number' => $this->document_additional_number]);
                $query2->andWhere(['doc.document_additional_number' => $this->document_additional_number]);
                $query3->andWhere(['doc.document_additional_number' => $this->document_additional_number]);
            } else {
                $query1->andWhere(['or',
                    ['doc.document_additional_number' => ''],
                    ['doc.document_additional_number' => null],
                ]);
                $query2->andWhere(['or',
                    ['doc.document_additional_number' => ''],
                    ['doc.document_additional_number' => null],
                ]);
                $query3->andWhere(['or',
                    ['doc.document_additional_number' => ''],
                    ['doc.document_additional_number' => null],
                ]);
            }

            return $query1->exists() || $query2->exists() || $query3->exists();
        };

        if ($isNumberExists($this->$attribute)) {
            if ($this->findFreeNumber) {
                $nextNumber = $this->$attribute;
                do {
                    $nextNumber++;
                } while ($isNumberExists($nextNumber));
            } else {
                $nextNumber = null;
            }
            $this->addError($attribute, "Номер {$this->$attribute} занят. Укажите свободный номер {$nextNumber}");
        }
    }

    /**
     * Generate document number
     */
    public function newDocumentNumber()
    {
        $this->document_number = $this->document_additional_number = null;

        $query3 = $this->invoice->getProxies()
            ->select(new Expression('[[document_number]], IF([[document_additional_number]] = "", NULL, [[document_additional_number]]) [[additional_number]]'));
        $query = (new Query)
            ->select(['document_number', 'additional_number'])
            ->from(['t' => $query3])
            ->orderBy([
                'document_number' => SORT_ASC,
                'additional_number' => SORT_ASC,
            ]);

        if ($rowArray = $query->all()) {
            foreach ($rowArray as $row) {
                $this->document_number = $row['document_number'];
                $this->document_additional_number = $row['additional_number'];
                if ($this->validate(['document_number'])) {
                    break;
                } else {
                    $this->document_number = $this->document_additional_number = null;
                }
            }
        }

        if (!$this->document_number) {
            $this->document_number = $this->getMaxDocumentNumber();
            do {
                $this->document_number++;
                $this->document_number .= '';

            } while (!$this->validate(['document_number']));
        }
    }

    /**
     * @return boolean
     */
    public function getIsEditableNumber()
    {
        if ($this->isNewRecord || $this->type == Documents::IO_TYPE_IN) {
            return true;
        } else {
            $query1 = $this->invoice->getActs()->andWhere(['document_number' => $this->document_number]);
            $query2 = $this->invoice->getInvoiceFactures()->andWhere(['document_number' => $this->document_number]);
            if ($this->document_additional_number) {
                $query1->andWhere(['document_additional_number' => $this->document_additional_number]);
                $query2->andWhere(['document_additional_number' => $this->document_additional_number]);
            } else {
                $query1->andWhere(['or',
                    ['document_additional_number' => ''],
                    ['document_additional_number' => null],
                ]);
                $query2->andWhere(['or',
                    ['document_additional_number' => ''],
                    ['document_additional_number' => null],
                ]);
            }

            return (!$query1->exists() && !$query2->exists());
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'created_at' => 'Creation Date',
            'document_author_id' => 'Author',
            'invoice_id' => 'Invoice ID',
            'status_out_id' => 'Proxy Status ID',
            'status_out_updated_at' => 'Status Date',
            'status_out_author_id' => 'Status Author',
            'document_date' => 'Update Date',
            'document_additional_number' => 'Additional Number',
            'basis_name' => 'Основание',
            'basis_document_number' => 'Номер основания',
            'basis_document_date' => 'Дата основания',
            'consignor_id' => 'Грузоотправитель',
            'consignee_id' => 'Грузополучатель',
            'document_number' => 'Номер документа',
            'limit_date' => 'Действительна по',
            'limit_date_input' => 'Действительна по',
            'proxy_person_id' => 'Доверенное лицо'
        ];
    }


    /**
     * @return OrderProxy
     */
    public function createOrderProxy()
    {
        $model = new OrderProxy();
        $model->proxy_id = $this->id;


        return $model;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusOut()
    {
        return $this->hasOne(status\ProxyStatus::className(), ['id' => 'status_out_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentStatus()
    {
        return $this->getStatusOut();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(Invoice::className(), ['id' => 'invoice_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(status\ProxyStatus::className(), ['id' => 'status_out_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusOutAuthor()
    {
        return $this->hasOne(Employee::className(), ['id' => 'status_out_author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreementType()
    {
        return $this->hasOne(AgreementType::className(), ['id' => 'basis_document_type_id']);
    }

    /**
     * @inheritdoc
     */
    public function getFile()
    {
        return $this->hasOne(File::className(), ['owner_id' => 'id'])
            ->onCondition(['owner_model' => Proxy::className(),]);
    }

    /**
     * @inheritdoc
     */
    public function getFiles()
    {
        return $this->hasMany(File::className(), ['owner_id' => 'id'])
            ->onCondition([File::tableName() . '.owner_model' => Proxy::className(),]);
    }

    /**
     * @inheritdoc
     */
    public function getScanDocuments()
    {
        return $this->hasMany(ScanDocument::className(), ['owner_id' => 'id'])
            ->onCondition([ScanDocument::tableName() . '.owner_model' => Proxy::className(),]);
    }

    /**
     * @inheritdoc
     */
    public function ordersLoad($ordersData)
    {
        $orderArray = [];
        $this->removedOrders = ArrayHelper::index($this->orderProxies, 'order_id');
        if (is_array($ordersData)) {
            foreach ($ordersData as $data) {
                $orderId = ArrayHelper::remove($data, 'order_id');
                $order = $orderId ? Order::findOne(['id' => $orderId, 'invoice_id' => $this->invoice_id]) : null;
                if ($order !== null) {
                    $orderProxy = OrderProxy::findOne([
                        'order_id' => $order->id,
                        'proxy_id' => $this->id,
                    ]);
                    if ($orderProxy === null) {
                        $orderProxy = new OrderProxy([
                            'order_id' => $order->id,
                            'proxy_id' => $this->id,
                            'product_id' => $order->product_id,
                        ]);
                    } else {
                        unset($this->removedOrders[$orderId]);
                    }
                    $orderProxy->load($data, '');

                    $orderArray[$orderId] = $orderProxy;
                }
            }
        }

        $this->populateRelation('orderProxies', $orderArray);
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        $agreement = explode('&', $this->agreement);
        if (is_array($agreement)) {
            $this->basis_name = isset($agreement[0]) ? $agreement[0] : null;
            $this->basis_document_number = isset($agreement[1]) ? $agreement[1] : null;
            $this->basis_document_date = isset($agreement[2]) ? $agreement[2] : null;
            $this->basis_document_type_id = isset($agreement[3]) ? $agreement[3] : null;
        } else {
            $this->basis_name = null;
            $this->basis_document_number = null;
            $this->basis_document_date = null;
            $this->basis_document_type_id = null;
        }

        return parent::beforeValidate();
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                if ($this->document_date === null) {
                    $this->document_date = date(DateHelper::FORMAT_DATE);
                }
                if (empty($this->document_number)) {
                    $this->newDocumentNumber();
                    $this->ordinal_document_number = $this->document_number;
                }
            }

            if ($this->type == Documents::IO_TYPE_IN) {
                if ($insert) {
                    $this->status_out_id = status\ProxyStatus::STATUS_CREATED;
                }

                if ($insert || $this->isAttributeChanged('status_out_id')) {
                    $this->status_out_updated_at = time();
                    $this->status_out_author_id = Yii::$app->user->id;
                }
            }

            $this->object_guid = OneCExport::generateGUID();

            ModelHelper::HtmlEntitiesModelAttributes($this);

            return true;
        } else {
            return false;
        }
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            foreach ($this->getOrderProxies()->all() as $order) {
                if (!$order->delete()) {
                    return false;
                }
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if (!$insert) {
            InvoiceHelper::checkForProxy($this->invoice_id);
        }
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();

        InvoiceHelper::checkForProxy($this->invoice_id);
    }

    /**
     *
     */
    public function generateDocumentNumber()
    {
        $year = date('Y', strtotime($this->document_date));
        $productionTypeArray = explode(', ', $this->invoice->production_type);
        $lastProxyId = self::find()
            ->byCompany(Yii::$app->user->identity->company->id)
            ->byIOType($this->type)
            ->andWhere(['between', self::tableName().'.document_date', $year.'-01-01', $year.'-12-31'])
            ->max('ordinal_document_number');
        $lastActId = Act::find()
            ->byCompany(Yii::$app->user->identity->company->id)
            ->byIOType($this->type)
            ->andWhere(['between', Act::tableName().'.document_date', $year.'-01-01', $year.'-12-31'])
            ->max('ordinal_document_number');
        if (in_array(Product::PRODUCTION_TYPE_GOODS, $productionTypeArray)) {
            if ($this->invoice->act !== null) {
                $this->ordinal_document_number = $lastProxyId + 1;
                $this->document_number = $lastProxyId + 1;
                $this->document_date = $this->invoice->act->document_date;
                $this->document_additional_number = $this->invoice->act->document_additional_number;
            } else {
                if ($lastActId > $lastProxyId) {
                    $documentNumber = $lastActId + 1;
                } else {
                    $documentNumber = $lastProxyId + 1;
                }
                $number = $this->checkOnEmptyDocumentNumber($documentNumber);
                $this->document_number = $number;
                $this->ordinal_document_number = $number;
            }
        } else {
            $number = $this->checkOnEmptyDocumentNumber($lastActId + 1);
            $this->document_number = $number;
            $this->ordinal_document_number = $number;
        }
    }

    /**
     * @param $number
     * @return mixed
     */
    public function checkOnEmptyDocumentNumber($number)
    {
        $forbiddenId = [];
        $query = Act::find()
            ->byCompany(Yii::$app->user->identity->company->id)
            ->byIOType($this->type);
        $subQuery = new Query();
        $subQuery->select([
            Invoice::tableName() . '.id',
            Invoice::tableName() . '.production_type',
        ]);
        $subQuery->from(Invoice::tableName());
        $query->leftJoin(['u' => $subQuery], 'u.id = ' . Act::tableName() . '.invoice_id')->andWhere(['u.production_type' => Product::PRODUCTION_TYPE_SERVICE . ', ' . Product::PRODUCTION_TYPE_GOODS]);
        $actArray = $query->all();
        foreach ($actArray as $act) {
            /* @var Proxy $act */
            if ($act->invoice->proxy == null) {
                $forbiddenId[] = $act->ordinal_document_number;
            }
        }
        if (in_array($number, $forbiddenId)) {
            $number = $this->checkOnEmptyDocumentNumber($number + 1);
        }

        return $number;
    }

    /**
     * @inheritdoc
     */
    public function getFullNumber()
    {
        if ($this->document_number !== null) {
            $documentNumber = $this->document_number;
        } else {
            $documentNumber = $this->invoice->ordinal_document_number;
        }

        return $this->type == Documents::IO_TYPE_OUT
            ? $documentNumber . $this->document_additional_number
            : $documentNumber;
    }

    /**
     * @return string
     */
    public function getPrintTitle()
    {
        return str_replace([' ', '"'], ['_'],
            $this->printablePrefix . '_№' . $this->getFullNumber() . '_' .
            date_format(date_create($this->document_date), 'd.m.Y') . '_' .
            $this->invoice->company_name_short
        );
    }

    /**
     * Get formatted log message from Log model
     * @param Log $log
     * @return string
     */
    public function getLogMessage(Log $log)
    {
        $title = 'Доверенность №';
        $invoiceText = '';
        $invoice = Invoice::findOne($log->getModelAttributeNew('invoice_id'));

        $ioType = $log->getModelAttributeNew('type');
        if ($ioType == Documents::IO_TYPE_IN) {
            $title .= $log->getModelAttributeNew('document_number');
        } else {
            $title .= $log->getModelAttributeNew('document_number') . ' ' . $log->getModelAttributeNew('document_additional_number');
        }

        $link = Html::a($title, [
            '/documents/proxy/view',
            'type' => $log->getModelAttributeNew('type'),
            'id' => $log->getModelAttributeNew('id'),
        ]);

        if ($invoice) {
            $amount = TextHelper::invoiceMoneyFormat($invoice->total_amount_with_nds, 2) . ' руб.';
            $contractorName = $invoice->contractor ? ', ' . $invoice->contractor->getTitle(true) : '';
            $invoiceText = ', ' . $amount . $contractorName . ',';
        }

        switch ($log->log_event_id) {
            case LogEvent::LOG_EVENT_CREATE:
                return $link . $invoiceText . ' была создана.';
            case LogEvent::LOG_EVENT_DELETE:
                return $title . $invoiceText . ' была удалена.';
            case LogEvent::LOG_EVENT_UPDATE:
                return $link . $invoiceText . ' была изменена.';
            case LogEvent::LOG_EVENT_UPDATE_STATUS:
                return $link . $invoiceText . ' статус "' . ProxyStatus::findOne($log->getModelAttributeNew('status_out_id'))->name . '"';
            default:
                return $log->message;
        }
    }

    /**
     * @param bool|false $withNds
     * @return int
     */
    public function getTotalAmount($withNds = false)
    {
        return $withNds ? $this->getTotalAmountWithNds() : $this->getTotalAmountNoNds();
    }

    /**
     * @return int
     */
    public function getTotalNds()
    {
        return $this->getTotalPLNds();
    }

    /**
     * @return int
     */
    public function getQuantity()
    {
        $orders = $this->invoice->getOrdersByProductType(Product::PRODUCTION_TYPE_GOODS);
        $quantity = 0;

        foreach ($orders as $order) {
            $quantity += $order->quantity;
        }

        return $quantity;
    }

    /**
     * @return array
     */
    public function getConsignorArray()
    {
        $query = Contractor::getSorted()
            ->byCompany($this->invoice->company_id)
            ->byDeleted()
            ->andWhere(['or', [Contractor::tableName() . '.is_seller' => 1], [Contractor::tableName() . '.is_customer' => 1]])
            ->andWhere(['!=', Contractor::tableName() . '.id', $this->invoice->contractor_id])
            ->all();

        return ArrayHelper::merge(['' => 'Он же'], ArrayHelper::map($query, 'id', 'shortName'));
    }

    /**
     * @return null|static
     */
    public function getConsignor()
    {
        return Contractor::findOne([$this->consignor_id]);
    }

    /**
     * @return null|static
     */
    public function getConsignee()
    {
        return Contractor::findOne([$this->consignee_id]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['id' => 'order_id'])->viaTable('order_proxy', ['proxy_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderProxies()
    {
        return $this->hasMany(OrderProxy::className(), ['proxy_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSignBasisDocument()
    {
        return $this->hasOne(DocumentType::className(), ['id' => 'sign_document_type_id']);
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        parent::afterFind();

        $this->agreement = "{$this->basis_name}&{$this->basis_document_number}&{$this->basis_document_date}&{$this->basis_document_type_id}";
    }

    /**
     * @param $fileName
     * @param $outProxy
     * @return PdfRenderer
     */
    public static function getRenderer($fileName, $outProxy, $destination = PdfRenderer::DESTINATION_FILE)
    {
        $renderer = new PdfRenderer([
            'view' => '@documents/views/proxy/pdf-view',
            'params' => array_merge([
                'model' => $outProxy,
                'message' => new Message(Documents::DOCUMENT_PROXY, $outProxy->type),
                'ioType' => $outProxy->type,
                'addStamp' => true,
            ]),

            'destination' => $destination,
            'filename' => $fileName,
            'displayMode' => PdfRenderer::DISPLAY_MODE_FULLPAGE,
            'format' => 'A4-P',
        ]);
        \Yii::$app->controller->view->params['asset'] = DocumentPrintAsset::className();

        return $renderer;
    }

    /**
     * @inheritdoc
     */
    public function setStatusPrinted()
    {
        if ($this && in_array($this->status_out_id, [ProxyStatus::STATUS_CREATED, ProxyStatus::STATUS_PRINTED])) {
            LogHelper::save($this, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function ($model) {
                $model->status_out_id = ProxyStatus::STATUS_PRINTED;
                $model->status_out_updated_at = time();
                $model->status_out_author_id = \Yii::$app->user->id;

                return $model->save(true, ['status_out_id', 'status_out_updated_at', 'status_out_author_id']);
            });
        }
    }

    /**
     * @return integer
     */
    public function getTotalAmountWithNds()
    {
        $amount = 0;
        foreach ($this->orderProxies as $key => $value) {
            $amount += $value->amountWithNds;
        }

        return $amount;
    }

    /**
     * @return integer
     */
    public function getTotalAmountNoNds()
    {
        $amount = 0;
        foreach ($this->orderProxies as $key => $value) {
            $amount += $value->amountNoNds;
        }

        return $amount;
    }

    /**
     * @return integer
     */
    public function getTotalPLNds()
    {
        return $this->totalAmountWithNds - $this->totalAmountNoNds;
    }

    /**
     * @param $period
     * @param $proxies
     */
    public static function generateXlsTable($period, $proxies)
    {
        $excel = new Excel();

        $fileName = 'Доверенности_' . DateHelper::format($period['from'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) .
            '-' . DateHelper::format($period['to'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $proxies,
            'title' => 'Доверенности',
            'columns' => [
                [
                    'attribute' => 'document_date',
                    'value' => function (Proxy $data) {
                        return DateHelper::format($data->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                    },
                ],
                [
                    'attribute' => 'document_number',
                    'value' => function (Proxy $data) {
                        return $data->fullNumber;
                    },
                ],
                [
                    'attribute' => 'invoice.total_amount_with_nds',
                    'styleArray' => [
                        'numberformat' => [
                            'code' => '# ##0.00',
                        ]
                    ],
                    'dataType' => 'n',
                    'value' => function ($model) {
                        return bcdiv($model->totalAmountWithNds, 100, 2);
                    },
                ],
                [
                    'attribute' => 'contractor_id',
                    'value' => function (Proxy $data) {
                        return $data->invoice->contractor_name_short;
                    },
                ],
                [
                    'attribute' => 'status_out_id',
                    'value' => function (Proxy $data) {
                        return $data->statusOut->name;
                    },
                ],
                [
                    'attribute' => 'invoice_document_number',
                    'value' => function (Proxy $data) {
                        return $data->invoice->fullNumber;
                    },
                ],
            ],
            'headers' => [
                'document_date' => 'Дата',
                'document_number' => '№',
                'invoice.total_amount_with_nds' => 'Сумма',
                'contractor_id' => 'Контрагент',
                'status_out_id' => 'Статус',
                'invoice_document_number' => 'Счёт №',
            ],
            'format' => 'Excel2007',
            'fileName' => $fileName . '.xlsx',
        ]);
    }

    /**
     * @return bool|string
     * @throws \yii\base\Exception
     * @throws \yii\db\Exception
     */
    public function generateOneCFile()
    {
        return false;
    }

    /**
     * @return string
     */
    public function getOneCFileName()
    {
        return 'Доверенность №' . $this->getFullNumber() . '.xml';
    }

    /**
     * @return string
     */
    public function getEmailSubject()
    {
        return 'Доверенность № ' . $this->fullNumber
            . ' от ' . DateHelper::format($this->document_date, 'd.m.y', DateHelper::FORMAT_DATE)
            . ' от ' . $this->invoice->company_name_short;
    }

    /**
     * @return string
     */
    public function getEmailText()
    {
        $document_date = DateHelper::format($this->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
        $sum = TextHelper::invoiceMoneyFormat($this->totalAmountWithNds, 2);

        $text = <<<EMAIL_TEXT
Здравствуйте!

Доверенность № {$this->fullNumber} от {$document_date} на сумму {$sum} р.
EMAIL_TEXT;

        return $text;
    }

    /**
     * @return array
     * @throws \MpdfException
     * @throws \yii\db\StaleObjectException
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function loadEmailFiles()
    {
        $files = [];

        $emailFile = new EmailFile();
        $emailFile->company_id = $this->invoice->company_id;
        $emailFile->file_name = str_replace(['\\', '/'], '_', $this->pdfFileName);
        $emailFile->ext = 'pdf';
        $emailFile->mime = 'application/pdf';
        $emailFile->size = 0;
        $emailFile->type = EmailFile::TYPE_PROXY;
        if ($emailFile->save()) {
            $path = $emailFile->getFilePath() . DIRECTORY_SEPARATOR . $emailFile->file_name;
            $size = file_put_contents($path, self::getRenderer(null, $this, PdfRenderer::DESTINATION_STRING)->output(false));
            $emailFile->size = ceil($size / 1024);
            if ($emailFile->save(true, ['size'])) {
                $files[] = [
                    'id' => $emailFile->id,
                    'name' => $emailFile->file_name,
                    'previewImg' => '<img src="/img/email/emailPdf.png" class="preview-img">',
                    'size' => $emailFile->size,
                    'deleteUrl' => Url::to(['/email/delete-email-file', 'id' => $emailFile->id]),
                    'downloadUrl' => Url::to(['/email/download-file', 'id' => $emailFile->id]),
                ];
            }
        }

        return $files;
    }

    /**
     * @inheritdoc
     */
    public function setLimit_date_input($value)
    {
        $this->limit_date = is_string($value) ? implode('-', array_reverse(explode('.', $value))) : null;
    }

    /**
     * @inheritdoc
     */
    public function getLimit_date_input()
    {
        return $this->limit_date ? implode('.', array_reverse(explode('-', $this->limit_date))) : null;
    }

    /**
     * @inheritdoc
     */
    public function setDocument_date_input($value)
    {
        $this->document_date = is_string($value) ? implode('-', array_reverse(explode('.', $value))) : null;
    }

    /**
     * @inheritdoc
     */
    public function getDocument_date_input()
    {
        return $this->document_date ? implode('.', array_reverse(explode('-', $this->document_date))) : null;
    }
}
