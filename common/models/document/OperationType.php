<?php
namespace common\models\document;

use Yii;

/**
 * This is the model class for table "operation_type".
 *
 * @property integer $id
 * @property string $code
 * @property string $name
 *
 * @property PaymentOrder[] $paymentOrders
 */
class OperationType extends \yii\db\ActiveRecord
{
    const TYPE_PAYMENT_ORDER = 1;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'operation_type';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'string'],
            [['code'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'code' => 'Code',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentOrders()
    {
        return $this->hasMany(PaymentOrder::className(), ['operation_type_id' => 'id']);
    }
}
