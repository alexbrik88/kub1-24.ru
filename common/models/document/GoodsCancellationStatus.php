<?php

namespace common\models\document;

use Yii;

/**
 * This is the model class for table "goods_cancellation_status".
 *
 * @property int $id
 * @property string $name
 *
 * @property GoodsCancellation[] $goodsCancellations
 */
class GoodsCancellationStatus extends \yii\db\ActiveRecord
{
    const STATUS_CREATED = 1;
    const STATUS_PRINTED = 2;
    const STATUS_SEND = 3;

    /**
     * @var array
     */
    public static $statusIcon = [
        self::STATUS_CREATED => 'new-doc',
        self::STATUS_PRINTED => 'print',
        self::STATUS_SEND => 'envelope',
    ];

    /**
     * @var array
     */
    public static $statusStyleClass = [
        self::STATUS_CREATED => '#26CD58',
        self::STATUS_PRINTED => '#FAC031',
        self::STATUS_SEND => '#FAC031',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'goods_cancellation_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
        ];
    }

    /**
     * Gets query for [[GoodsCancellations]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGoodsCancellations()
    {
        return $this->hasMany(GoodsCancellation::class, ['status_id' => 'id']);
    }
}
