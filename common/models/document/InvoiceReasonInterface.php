<?php

namespace common\models\document;

interface InvoiceReasonInterface
{
    /**
     * @return string
     */
    public function getReasonName(): string;
}
