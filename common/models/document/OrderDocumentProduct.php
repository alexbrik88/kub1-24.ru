<?php

namespace common\models\document;

use common\components\calculator\CalculatorHelper;
use common\models\Company;
use common\models\document\status\InvoiceStatus;
use common\models\document\status\OrderDocumentStatus;
use common\models\NdsOsno;
use common\models\TaxRate;
use frontend\models\Documents;
use Yii;
use common\models\address\Country;
use common\models\product\Product;
use common\models\product\ProductUnit;
use yii\helpers\Html;
use yii\db\Query;

/**
 * This is the model class for table "order_document_product".
 *
 * @property integer $id
 * @property integer $order_document_id
 * @property integer $product_id
 * @property string $quantity
 * @property string $product_title
 * @property string $product_code
 * @property integer $unit_id
 * @property string $count_in_place
 * @property string $place_count
 * @property string $mass_gross
 * @property integer $base_price_no_vat
 * @property integer $base_price_with_vat
 * @property string $discount
 * @property string $markup
 * @property integer $purchase_price_no_vat
 * @property integer $purchase_price_with_vat
 * @property integer $selling_price_no_vat
 * @property integer $selling_price_with_vat
 * @property integer $amount_purchase_no_vat
 * @property integer $amount_purchase_with_vat
 * @property integer $amount_sales_no_vat
 * @property integer $amount_sales_with_vat
 * @property integer $excise
 * @property string $excise_price
 * @property integer $sale_tax
 * @property integer $purchase_tax
 * @property integer $purchase_tax_rate_id
 * @property integer $sale_tax_rate_id
 * @property integer $country_id
 * @property string $custom_declaration_number
 * @property string $box_type
 * @property integer $number
 *
 * @property Country $country
 * @property OrderDocument $orderDocument
 * @property Product $product
 * @property ProductUnit $unit
 * @property string $productionType
 * @property string $productTitle
 * @property TaxRate $saleTaxRate
 * @property TaxRate $purchaseTaxRate
 * @property integer $productReserve
 * @property integer $productAvailable
 */
class OrderDocumentProduct extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_document_product';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order_document_id', 'number', 'product_id', 'product_title', 'base_price_no_vat', 'base_price_with_vat',
                'amount_purchase_no_vat', 'amount_purchase_with_vat', 'amount_sales_no_vat', 'amount_sales_with_vat',
                'sale_tax', 'purchase_tax', 'purchase_tax_rate_id', 'sale_tax_rate_id', 'country_id'], 'required'],
            [['order_document_id', 'product_id', 'unit_id', 'base_price_no_vat', 'base_price_with_vat',
                'purchase_price_no_vat', 'purchase_price_with_vat', 'selling_price_no_vat', 'selling_price_with_vat',
                'amount_purchase_no_vat', 'amount_purchase_with_vat', 'amount_sales_no_vat', 'amount_sales_with_vat',
                'excise', 'sale_tax', 'purchase_tax', 'purchase_tax_rate_id', 'sale_tax_rate_id', 'country_id',
                'number'], 'integer'],
            [['quantity'], 'required', 'isEmpty' => function ($value) {
                return empty($value);
            }],
            [
                ['quantity'], 'number', 'numberPattern' => '/^\d+(\.\d*)?$/', 'min' => 0.000000001, 'max' => 9999999999,
            ],
            [['discount', 'markup'], 'number', 'numberPattern' => '/^\d+(\.\d*)?$/', 'min' => 0, 'max' => 99.9999],
            [['discount', 'markup'], 'filter', 'filter' => function ($value) {
                return round($value, 4);
            },],
            [['product_title'], 'string'],
            [['product_code', 'count_in_place', 'excise_price'], 'string', 'max' => 255],
            [['place_count', 'mass_gross', 'custom_declaration_number', 'box_type'], 'string', 'max' => 45],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Country::className(), 'targetAttribute' => ['country_id' => 'id']],
            [['order_document_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderDocument::className(), 'targetAttribute' => ['order_document_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            [['unit_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductUnit::className(), 'targetAttribute' => ['unit_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'order_document_id' => 'Order Document ID',
            'product_id' => 'Product ID',
            'quantity' => 'Количество',
            'product_title' => 'Product Title',
            'product_code' => 'Product Code',
            'unit_id' => 'Unit ID',
            'count_in_place' => 'Count In Place',
            'place_count' => 'Place Count',
            'mass_gross' => 'Mass Gross',
            'base_price_no_vat' => 'Base Price No Vat',
            'base_price_with_vat' => 'Base Price With Vat',
            'discount' => 'Discount',
            'markup' => 'Markup',
            'purchase_price_no_vat' => 'Purchase Price No Vat',
            'purchase_price_with_vat' => 'Purchase Price With Vat',
            'selling_price_no_vat' => 'Selling Price No Vat',
            'selling_price_with_vat' => 'Selling Price With Vat',
            'amount_purchase_no_vat' => 'Amount Purchase No Vat',
            'amount_purchase_with_vat' => 'Amount Purchase With Vat',
            'amount_sales_no_vat' => 'Amount Sales No Vat',
            'amount_sales_with_vat' => 'Amount Sales With Vat',
            'excise' => 'Excise',
            'excise_price' => 'Excise Price',
            'sale_tax' => 'Sale Tax',
            'purchase_tax' => 'Purchase Tax',
            'purchase_tax_rate_id' => 'Purchase Tax Rate ID',
            'sale_tax_rate_id' => 'Sale Tax Rate ID',
            'country_id' => 'Country ID',
            'custom_declaration_number' => 'Custom Declaration Number',
            'box_type' => 'Box Type',
        ];
    }

    /**
     * @param $orderDocumentId
     * @param $orderId
     * @return OrderDocumentProduct
     */
    public static function findById($orderDocumentId, $orderId)
    {
        return static::find()
            ->andWhere(['and',
                ['id' => $orderId],
                ['order_document_id' => $orderDocumentId],
            ])->one();
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        $this->calculateAmount();

        return parent::beforeValidate();
    }

    /**
     *
     */
    public function afterFind()
    {
        parent::afterFind();
        $this->quantity = $this->quantity * 1;
        $this->discount = $this->discount * 1;
        $this->markup = $this->markup * 1;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Country::className(), ['id' => 'country_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderDocument()
    {
        return $this->hasOne(OrderDocument::className(), ['id' => 'order_document_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::className(), ['id' => 'product_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUnit()
    {
        return $this->hasOne(ProductUnit::className(), ['id' => 'unit_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSaleTaxRate()
    {
        return $this->hasOne(TaxRate::className(), ['id' => 'sale_tax_rate_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPurchaseTaxRate()
    {
        return $this->hasOne(TaxRate::className(), ['id' => 'purchase_tax_rate_id']);
    }

    /**
     * @return integer
     */
    public function getProductionType()
    {
        return $this->product->production_type;
    }

    /**
     * @return string
     */
    public function getProductTitle()
    {
        return Html::encode($this->product_title);
    }

    public function getShippedCount()
    {
        return $this->orderDocument->invoice ?
            $this->orderDocument->invoice->getPackingLists()
                ->leftJoin(['packingListOrder' => OrderPackingList::tableName()], 'packingListOrder.product_id = ' . $this->product_id . ' AND packingListOrder.packing_list_id = ' . PackingList::tableName() . '.id')
                ->sum('packingListOrder.quantity') * 1 : 0;
    }

    /**
     * @param $priceOneWithVat
     * @param $taxRateId
     */
    public function calculateOrderPrice($priceOneWithVat, $taxRateId)
    {
        $priceOneWithVat *= 1;
        $company = $this->orderDocument->company;
        $ioType = $this->orderDocument->type;
        $discount = min(100, $this->discount);
        $markup   = min(100, $this->markup);
        $ndsViewTypeId = $this->orderDocument->nds_view_type_id;

        if ($ioType == Documents::IO_TYPE_OUT) {
            // nds
            if ($ndsViewTypeId == OrderDocument::NDS_VIEW_WITHOUT) {
                $this->selling_price_no_vat = $priceOneWithVat;
                $this->selling_price_with_vat = $priceOneWithVat;
            }
            if ($ndsViewTypeId == OrderDocument::NDS_VIEW_OUT) {
                $this->selling_price_no_vat = $priceOneWithVat;
                $this->selling_price_with_vat = CalculatorHelper::getPriceWithNds($priceOneWithVat, $taxRateId);
            } else {
                $this->selling_price_no_vat = CalculatorHelper::getPriceWithoutNds($priceOneWithVat, $taxRateId);
                $this->selling_price_with_vat = $priceOneWithVat;
            }
            // discount/markup
            if ($discount > 0) {
                $this->base_price_no_vat = round($this->selling_price_no_vat * 100 / (100 - $discount));
                $this->base_price_with_vat = round($this->selling_price_with_vat * 100 / (100 - $discount));
            } elseif ($markup > 0) {
                $this->base_price_no_vat = round($this->selling_price_no_vat * 100 / (100 + $markup));
                $this->base_price_with_vat = round($this->selling_price_with_vat * 100 / (100 + $markup));
            } else {
                $this->base_price_no_vat = $this->selling_price_no_vat;
                $this->base_price_with_vat = $this->selling_price_with_vat;
            }
        } else {
            // nds
            if ($ndsViewTypeId == OrderDocument::NDS_VIEW_WITHOUT) {
                $this->purchase_price_no_vat = $priceOneWithVat;
                $this->purchase_price_with_vat = $priceOneWithVat;
            } elseif ($ndsViewTypeId == OrderDocument::NDS_VIEW_OUT) {
                $this->purchase_price_no_vat = $priceOneWithVat;
                $this->purchase_price_with_vat = CalculatorHelper::getPriceWithNds($priceOneWithVat, $taxRateId);
            } else {
                $this->purchase_price_no_vat = CalculatorHelper::getPriceWithoutNds($priceOneWithVat, $taxRateId);
                $this->purchase_price_with_vat = $priceOneWithVat;
            }
            // discount/markup
            if ($discount > 0) {
                $this->base_price_no_vat = round($this->purchase_price_no_vat * 100 / (100 - $discount));
                $this->base_price_with_vat = round($this->purchase_price_with_vat * 100 / (100 - $discount));
            } elseif ($markup > 0) {
                $this->base_price_no_vat = round($this->purchase_price_no_vat * 100 / (100 + $markup));
                $this->base_price_with_vat = round($this->purchase_price_with_vat * 100 / (100 + $markup));
            } else {
                $this->base_price_no_vat = $this->purchase_price_no_vat;
                $this->base_price_with_vat = $this->purchase_price_with_vat;
            }
        }

        //var_dump($this->purchase_price_no_vat);
        //var_dump($this->purchase_price_with_vat);
    }

    /**
     *
     */
    public function calculateAmount()
    {
        if (!is_numeric($this->quantity)) {
            $this->quantity = 0;
        }
        if (!is_numeric($this->purchase_price_with_vat)) {
            $this->purchase_price_with_vat = 0;
        }
        if (!is_numeric($this->selling_price_with_vat)) {
            $this->selling_price_with_vat = 0;
        }
        $this->amount_purchase_with_vat = round($this->purchase_price_with_vat * $this->quantity);
        $this->amount_purchase_no_vat = (int)CalculatorHelper::getPriceWithoutNds(
            $this->amount_purchase_with_vat,
            $this->purchase_tax_rate_id
        );

        $this->amount_sales_with_vat = round($this->selling_price_with_vat * $this->quantity);
        $this->amount_sales_no_vat = (int)CalculatorHelper::getPriceWithoutNds(
            $this->amount_sales_with_vat,
            $this->sale_tax_rate_id
        );

        $this->purchase_tax = $this->amount_purchase_with_vat - $this->amount_purchase_no_vat;
        $this->sale_tax = $this->amount_sales_with_vat - $this->amount_sales_no_vat;
    }

    /**
     * @return mixed
     */
    public function getProductReserve($odId = null)
    {
        $query = new Query;
        $subQuery1 = OrderDocumentProduct::find()->alias('odp')
            ->leftJoin(['od' => OrderDocument::tableName()], '{{odp}}.[[order_document_id]] = {{od}}.[[id]]')
            ->leftJoin(['invoice' => Invoice::tableName()], '{{od}}.[[invoice_id]] = {{invoice}}.[[id]]')
            ->select(['count' => 'odp.quantity'])
            ->andWhere(['odp.product_id' => $this->product_id])
            ->andWhere(['od.is_deleted' => false])
            ->andWhere(['not', ['od.status_id' => [OrderDocumentStatus::STATUS_RETURN, OrderDocumentStatus::STATUS_CANCELED]]])
            ->andFilterWhere(['not', ['od.id' => $odId]])
            ->andWhere([
                'or',
                ['invoice.id' => null],
                ['invoice.is_deleted' => true],
                ['invoice.invoice_status_id' => InvoiceStatus::STATUS_REJECTED],
            ]);
        $subQuery2 = Order::find()
            ->select('[[reserve]] AS [[count]]')
            ->andWhere(['product_id' => $this->product_id]);

        return 1 * $query->from(['t' => $subQuery1->union($subQuery2, true)])->sum('count');
    }

    /**
     * @return int
     */
    public function getProductAvailable($odId = null)
    {
        return $this->product->totalQuantity - $this->getProductReserve($odId);
    }

    public function getTotal_amount() { return null; }
}
