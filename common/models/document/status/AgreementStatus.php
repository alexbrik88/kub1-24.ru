<?php

namespace common\models\document\status;

use common\models\document\Agreement;
use Yii;

/**
 * This is the model class for table "agreement_status".
 *
 * @property integer $id
 * @property string $name
 */
class AgreementStatus extends DocumentStatusBase
{

    /**
     * Статус - "создан"
     */
    const STATUS_CREATED = 1;
    /**
     * Статус - "распечатан"
     */
    const STATUS_PRINTED = 2;
    /**
     * Статус - "отправлен"
     */
    const STATUS_SEND = 3;
    /**
     * Статус - "получен"
     */
    const STATUS_RECEIVED = 4;
    /**
     * Статус - "отменен"
     */
    const STATUS_REJECTED = 5;
    /**
     * Статус - "окончен"
     */
    const STATUS_FINISHED = 6;


    /**
     * @var array
     */
    public static $statusIcon = [
        self::STATUS_CREATED => 'icon-doc',
        self::STATUS_PRINTED => 'icon-printer',
        self::STATUS_SEND => 'icon-paper-plane',
        self::STATUS_RECEIVED => 'icon-check',
        self::STATUS_REJECTED => 'icon-ban',
        self::STATUS_FINISHED => 'icon-check',
    ];

    /**
     * @var array
     */
    public static $statusStyleClass = [
        self::STATUS_CREATED => 'darkblue',
        self::STATUS_PRINTED => 'darkblue',
        self::STATUS_SEND => 'darkblue',
        self::STATUS_RECEIVED => 'green',
        self::STATUS_REJECTED => 'green',
        self::STATUS_FINISHED => 'green',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'agreement_status';
    }
}
