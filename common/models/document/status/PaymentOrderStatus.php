<?php

namespace common\models\document\status;

use Yii;
use common\models\document\PaymentOrder;

/**
 * This is the model class for table "payment_order_status".
 *
 * @property integer $id
 * @property string $name
 *
 * @property PaymentOrder[] $paymentOrders
 */
class PaymentOrderStatus extends \yii\db\ActiveRecord
{
    const STATUS_CREATED = 1;
    const STATUS_IMPORT_IN_BANK = 2;
    const STATUS_PRINTED = 3;
    const STATUS_SENT_TO_BANK = 4;
    const STATUS_PAID = 5;

    public static $statusArray = [
        PaymentOrderStatus::STATUS_CREATED,
        PaymentOrderStatus::STATUS_IMPORT_IN_BANK,
        PaymentOrderStatus::STATUS_PRINTED,
        PaymentOrderStatus::STATUS_SENT_TO_BANK,
        PaymentOrderStatus::STATUS_PAID,
    ];

    public static $statusUpdateDanyAllow = [
        PaymentOrderStatus::STATUS_PAID => [],
        PaymentOrderStatus::STATUS_SENT_TO_BANK => [
            PaymentOrderStatus::STATUS_PAID,
        ],
        PaymentOrderStatus::STATUS_IMPORT_IN_BANK => [
            PaymentOrderStatus::STATUS_PAID,
            PaymentOrderStatus::STATUS_SENT_TO_BANK,
        ],
        PaymentOrderStatus::STATUS_PRINTED => [
            PaymentOrderStatus::STATUS_PAID,
            PaymentOrderStatus::STATUS_SENT_TO_BANK,
            PaymentOrderStatus::STATUS_IMPORT_IN_BANK,
        ],
        PaymentOrderStatus::STATUS_CREATED => [
            PaymentOrderStatus::STATUS_PAID,
            PaymentOrderStatus::STATUS_SENT_TO_BANK,
            PaymentOrderStatus::STATUS_IMPORT_IN_BANK,
            PaymentOrderStatus::STATUS_PRINTED,
        ],
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'payment_order_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentOrders()
    {
        return $this->hasMany(PaymentOrder::className(), ['payment_order_status_id' => 'id']);
    }
}
