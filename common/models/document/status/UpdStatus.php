<?php

namespace common\models\document\status;

use common\models\document\Upd;
use Yii;

/**
 * This is the model class for table "upd_status".
 *
 * @property integer $id
 * @property string $name
 *
 * @property Upd[] $upds
 */
class UpdStatus extends DocumentStatusBase
{
    /**
     * Статус - "создан"
     */
    const STATUS_CREATED = 1;
    /**
     * Статус - "распечатан"
     */
    const STATUS_PRINTED = 2;
    /**
     * Статус - "отправлен"
     */
    const STATUS_SEND = 3;
    /**
     * Статус - "получен"
     */
    const STATUS_RECEIVED = 4;
    /**
     * Статус - "отменен"
     */
    const STATUS_REJECTED = 5;

    /**
     * @var array
     */
    public static $statusIcon = [
        self::STATUS_CREATED => 'icon-doc',
        self::STATUS_PRINTED => 'icon-printer',
        self::STATUS_SEND => 'icon-paper-plane',
        self::STATUS_RECEIVED => 'icon-check',
        self::STATUS_REJECTED => 'icon-ban',
    ];

    /**
     * @var array
     */
    public static $statusStyleClass = [
        self::STATUS_CREATED => 'darkblue',
        self::STATUS_PRINTED => 'darkblue',
        self::STATUS_SEND => 'darkblue',
        self::STATUS_RECEIVED => 'green',
        self::STATUS_REJECTED => 'green',
    ];

    /**
     * @var array
     */
    public static $turmoverStatus = [
        self::STATUS_CREATED,
        self::STATUS_PRINTED,
        self::STATUS_SEND,
        self::STATUS_RECEIVED,
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'upd_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpds()
    {
        return $this->hasMany(Upd::className(), ['status_out_id' => 'id']);
    }
}
