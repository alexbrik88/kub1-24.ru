<?php

namespace common\models\document\status;


use yii\db\ActiveRecord;

/**
 * Class DocumentStatusBase
 *
 * @property int id
 * @property string name
 *
 * @package common\models\document\status
 */
abstract class DocumentStatusBase extends ActiveRecord
{
    /**
     * @var array
     */
    public static $statusIcon = [];

    /**
     * @var array
     */
    public static $statusStyleClass = [];

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return static::getStatusIcon($this->id);
    }

    /**
     * @return string
     */
    public function getStyleClass()
    {
        return static::getStatusStyleClass($this->id);
    }

    /**
     * @param $statusId
     * @return string
     */
    public static function getStatusIcon($statusId)
    {
        return isset(static::$statusIcon[$statusId]) ? static::$statusIcon[$statusId] : '';
    }

    /**
     * @param $statusId
     * @return string
     */
    public function getStatusStyleClass($statusId)
    {
        return isset(static::$statusStyleClass[$statusId]) ? static::$statusStyleClass[$statusId] : '';
    }

}
