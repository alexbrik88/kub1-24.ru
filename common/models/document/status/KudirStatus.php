<?php

namespace common\models\document\status;

use Yii;

/**
 * This is the model class for table "kudir_status".
 *
 * @property integer $id
 * @property string $name
 */
class KudirStatus extends DocumentStatusBase
{
    /**
     * Статус - "Создана"
     */
    const STATUS_CREATED = 1;
    /**
     * Статус - "Распечатана"
     */
    const STATUS_PRINTED = 2;
    /**
     * Статус - "Передана"
     */
    const STATUS_SEND = 3;
    /**
     * Статус - "Принята"
     */
    const STATUS_ACCEPTED = 4;
    /**
     * Статус - "Подписана"
     */
    const STATUS_SIGNED = 5;

    /**
     * @var array
     */
    public static $statusIcon = [
        self::STATUS_CREATED => 'icon-doc',
        self::STATUS_PRINTED => 'icon-printer',
        self::STATUS_SEND => 'icon-paper-plane',
        self::STATUS_ACCEPTED => 'icon-check',
        self::STATUS_SIGNED => 'icon-doc',
    ];

    /**
     * @var array
     */
    public static $statusStyleClass = [
        self::STATUS_CREATED => 'darkblue',
        self::STATUS_PRINTED => 'darkblue',
        self::STATUS_SEND => 'darkblue',
        self::STATUS_ACCEPTED => 'green',
        self::STATUS_SIGNED => 'darkblue',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kudir_status';
    }
}
