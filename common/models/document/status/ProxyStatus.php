<?php

namespace common\models\document\status;

use Yii;

/**
 * This is the model class for table "proxy_status".
 *
 * @property integer $id
 * @property string $name
 */
class ProxyStatus extends DocumentStatusBase
{
    /**
     * Статус - "Создана"
     */
    const STATUS_CREATED = 1;
    /**
     * Статус - "Распечатана"
     */
    const STATUS_PRINTED = 2;
    /**
     * Статус - "Передана"
     */
    const STATUS_SEND = 3;
    /**
     * Статус - "Получена-подписана"
     */
    const STATUS_RECEIVED = 4;
    /**
     * Статус - "Отменена"
     */
    const STATUS_REJECTED = 5;

    /**
     * @var array
     */
    public static $statusIcon = [
        self::STATUS_CREATED => 'icon-doc',
        self::STATUS_PRINTED => 'icon-printer',
        self::STATUS_SEND => 'icon-paper-plane',
        self::STATUS_RECEIVED => 'icon-check',
        self::STATUS_REJECTED => 'icon-ban',
    ];

    /**
     * @var array
     */
    public static $statusStyleClass = [
        self::STATUS_CREATED => 'darkblue',
        self::STATUS_PRINTED => 'darkblue',
        self::STATUS_SEND => 'darkblue',
        self::STATUS_RECEIVED => 'green',
        self::STATUS_REJECTED => 'green',
    ];

    /**
     * @var array
     */
    public static $turmoverStatus = [
        self::STATUS_CREATED,
        self::STATUS_PRINTED,
        self::STATUS_SEND,
        self::STATUS_RECEIVED,
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'proxy_status';
    }
}
