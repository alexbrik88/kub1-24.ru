<?php

namespace common\models\document\status;

use Yii;
use common\models\document\OrderDocument;

/**
 * This is the model class for table "order_document_status".
 *
 * @property integer $id
 * @property string $name
 * @property string $color
 *
 * @property OrderDocument[] $orderDocuments
 */
class OrderDocumentStatus extends \yii\db\ActiveRecord
{
    const STATUS_NEW = 1;
    const STATUS_RECEIVED = 2;
    const STATUS_SEND = 3;
    const STATUS_CONFIRMED = 4;
    const STATUS_IN_PROCESSING = 5;
    const STATUS_ASSEMBLED = 6;
    const STATUS_SHIPPED = 7;
    const STATUS_DELIVERED = 8;
    const STATUS_PAID = 9;
    const STATUS_RETURN = 10;
    const STATUS_CANCELED = 11;

    /**
     * @var array
     */
    public static $statusName = [
        self::STATUS_NEW => 'Новые',
        self::STATUS_DELIVERED => 'Доставленные',
        self::STATUS_PAID => 'Оплаченные',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'order_document_status';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name', 'color'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'color' => 'Color',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderDocuments()
    {
        return $this->hasMany(OrderDocument::className(), ['status_id' => 'id']);
    }
}
