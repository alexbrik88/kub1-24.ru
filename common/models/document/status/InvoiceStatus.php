<?php

namespace common\models\document\status;

use frontend\models\Documents;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "out_invoice_status".
 *
 * TODO: drop in_invoice_status table, rename out_invoice_status to invoice_status, add relation from invoice to invoice_status.
 *
 * In: created, payed partial, payed, overdue
 * Out: created, send, viewed, approved, rejected, payed partial, payed, overdue
 *
 * @property integer $id
 * @property string $name
 */
class InvoiceStatus extends DocumentStatusBase
{
    /**
     *
     */
    const STATUS_CREATED = 1;
    /**
     *
     */
    const STATUS_SEND = 2;
    /**
     * STATUS_VIEWED deleted
     */
    const STATUS_VIEWED = 3;
    /**
     *
     */
    const STATUS_APPROVED = 4;
    /**
     *
     */
    const STATUS_REJECTED = 5;
    /**
     *
     */
    const STATUS_PAYED_PARTIAL = 6;
    /**
     *
     */
    const STATUS_PAYED = 7;
    /**
     *
     */
    const STATUS_OVERDUE = 8; // 4
    /**
     *
     */
    const STATUS_AUTOINVOICE = 9;

    public static $statusByType = [
        Documents::IO_TYPE_IN => [
            self::STATUS_CREATED,
            self::STATUS_PAYED_PARTIAL,
            self::STATUS_PAYED,
            self::STATUS_OVERDUE,
        ],
        Documents::IO_TYPE_OUT => [
            self::STATUS_CREATED,
            self::STATUS_SEND,
            self::STATUS_VIEWED,
            //self::STATUS_APPROVED, // deleted
            self::STATUS_REJECTED,
            self::STATUS_PAYED_PARTIAL,
            self::STATUS_PAYED,
            self::STATUS_OVERDUE,
        ],
    ];

    public static $oneCStatuses = [
        self::STATUS_CREATED => 'НеОплачен',
        self::STATUS_PAYED_PARTIAL => 'ОплаченЧастично',
        self::STATUS_PAYED => 'Оплачен',
        self::STATUS_OVERDUE => 'НеОплачен',
        self::STATUS_SEND => 'НеОплачен',
        self::STATUS_REJECTED => 'Отменен',
        self::STATUS_VIEWED => 'НеОплачен',
    ];

    /**
     * @var array
     */
    public static $statusIcon = [
        self::STATUS_CREATED => 'icon-doc',
        self::STATUS_SEND => 'icon-envelope',
        self::STATUS_VIEWED => 'icon-eye',
        self::STATUS_APPROVED => 'icon-like',
        self::STATUS_REJECTED => 'icon-ban',
        self::STATUS_PAYED_PARTIAL => 'icon-pie-chart',
        self::STATUS_PAYED => 'icon-check',
        self::STATUS_OVERDUE => 'icon-calendar',
    ];

    /**
     * @var array
     */
    public static $statusStyleClass = [
        self::STATUS_CREATED => 'darkblue',
        self::STATUS_SEND => 'darkblue',
        self::STATUS_VIEWED => 'green',
        self::STATUS_APPROVED => 'green',
        self::STATUS_REJECTED => 'green',
        self::STATUS_PAYED_PARTIAL => 'green',
        self::STATUS_PAYED => 'green',
        self::STATUS_OVERDUE => 'red',
    ];

    /**
     * @var array
     */
    protected $rejectAllowed = [
        self::STATUS_CREATED,
        self::STATUS_SEND,
        self::STATUS_VIEWED,
        self::STATUS_APPROVED,
        self::STATUS_PAYED_PARTIAL,
        self::STATUS_OVERDUE,
    ];
    /**
     * @var array
     */
    protected $sendAllowed = [
        self::STATUS_CREATED,
        self::STATUS_SEND,
        self::STATUS_PAYED,
        self::STATUS_VIEWED,
        self::STATUS_APPROVED,
        self::STATUS_PAYED_PARTIAL,
        self::STATUS_OVERDUE,
    ];
    /**
     * @var array
     */
    public static $payAllowed = [
        self::STATUS_CREATED,
        self::STATUS_SEND,
        self::STATUS_VIEWED,
        self::STATUS_PAYED_PARTIAL,
        self::STATUS_OVERDUE,
    ];
    /**
     * @var array
     */
    public static $hasPayment = [
        self::STATUS_PAYED,
        self::STATUS_PAYED_PARTIAL,
    ];
    /**
     * @var array
     */
    protected $paymentAllowed = [
        Documents::IO_TYPE_OUT => [
            self::STATUS_CREATED,
            self::STATUS_SEND,
            self::STATUS_PAYED_PARTIAL,
            self::STATUS_OVERDUE,
        ],
        Documents::IO_TYPE_IN => [
            self::STATUS_CREATED,
            self::STATUS_PAYED_PARTIAL,
            self::STATUS_OVERDUE,
        ],
    ];
    /**
     * @var array
     */
    public static $deleteAllowed = [
        self::STATUS_CREATED,
        self::STATUS_SEND,
        self::STATUS_VIEWED,
        self::STATUS_APPROVED,
        self::STATUS_REJECTED,
        self::STATUS_OVERDUE,
        self::STATUS_AUTOINVOICE,
    ];

    /**
     * @var array
     */
    public static $validInvoices = [
        self::STATUS_CREATED,
        self::STATUS_SEND,
        self::STATUS_PAYED,
        self::STATUS_VIEWED,
        self::STATUS_APPROVED,
        self::STATUS_PAYED_PARTIAL,
        self::STATUS_OVERDUE,
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'out_invoice_status';
    }

    /**
     * @return bool
     */
    public function rejectAllowed()
    {
        return in_array($this->id, $this->rejectAllowed);
    }

    /**
     * @return bool
     */
    public function sendAllowed()
    {
        return in_array($this->id, $this->sendAllowed);
    }

    /**
     * @param $type
     * @return bool
     */
    public function paymentAllowed($type)
    {
        $statusArray = ArrayHelper::getValue($this->paymentAllowed, $type, []);

        return in_array($this->id, $statusArray);
    }

    /**
     * @param int $type
     * @return int[]
     */
    public static function getPaymentAllowedTypes($type)
    {
        $model = new static();

        return ArrayHelper::getValue($model->paymentAllowed, $type, []);
    }
}
