<?php

namespace common\models\document\status;

use common\models\document\AgentReport;
use Yii;

/**
 * This is the model class for table "agent_report_status".
 *
 * @property integer $id
 * @property string $name
 */
class AgentReportStatus extends DocumentStatusBase
{
    const STATUS_CREATED = 1;
    const STATUS_SEND = 2;
    const STATUS_PRINTED = 3;
    const STATUS_PAYED_PARTIAL = 4;
    const STATUS_PAYED = 5;
    const STATUS_OVERDUE = 6;
    /**
     * @var array
     */
    public static $statusIcon = [
        self::STATUS_CREATED => 'icon-doc',
        self::STATUS_SEND => 'icon-envelope',
        self::STATUS_PRINTED => 'icon-printer',
        self::STATUS_PAYED_PARTIAL => 'icon-pie-chart',
        self::STATUS_PAYED => 'icon-check',
        self::STATUS_OVERDUE => 'icon-calendar',
    ];

    /**
     * @var array
     */
    public static $statusStyleClass = [
        self::STATUS_CREATED => 'darkblue',
        self::STATUS_SEND => 'darkblue',
        self::STATUS_PRINTED => 'darkblue',
        self::STATUS_PAYED_PARTIAL => 'green',
        self::STATUS_PAYED => 'green',
        self::STATUS_OVERDUE => 'red',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'agent_report_status';
    }
}
