<?php

namespace common\models\document\status;

use Yii;

/**
 * This is the model class for table "invoice_facture_status".
 *
 * @property integer $id
 * @property string $name
 */
class InvoiceFactureStatus extends DocumentStatusBase
{
    /**
     *
     */
    const STATUS_CREATED = 1;
    /**
     *
     */
    const STATUS_PRINTED = 2;
    /**
     *
     */
    const STATUS_DELIVERED = 3;
    /**
     *
     */
    const STATUS_REJECTED = 4;

    /**
     * @var array
     */
    public static $statusIcon = [
        self::STATUS_CREATED => 'icon-doc',
        self::STATUS_PRINTED => 'icon-printer',
        self::STATUS_DELIVERED => 'icon-paper-plane',
        self::STATUS_REJECTED => 'icon-ban',
    ];


    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invoice_facture_status';
    }
}
