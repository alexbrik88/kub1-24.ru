<?php

namespace common\models\document;

use Yii;
use common\models\Company;

/**
 * This is the model class for table "act_essence".
 *
 * @property integer $id
 * @property integer $company_id
 * @property string $text
 * @property integer $is_checked
 *
 * @property Company $company
 */
class ActEssence extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'act_essence';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id'], 'required'],
            [['company_id', 'is_checked'], 'integer'],
            [['text'], 'string'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'text' => 'Text',
            'is_checked' => 'Is Checked',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }
}
