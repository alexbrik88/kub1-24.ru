<?php

namespace common\models\document;

use common\components\excel\Excel;
use common\components\pdf\PdfRenderer;
use common\components\TextHelper;
use common\components\date\DateHelper;
use common\components\helpers\ArrayHelper;
use common\models\AgreementType;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\currency\Currency;
use common\models\document\query\UpdQuery;
use common\models\document\status\UpdStatus;
use common\models\employee\Employee;
use common\models\employee\EmployeeSignature;
use common\models\EmployeeCompany;
use common\models\file\File;
use common\models\product\Product;
use frontend\models\Documents;
use frontend\models\log\Log;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\modules\analytics\models\PlanCashContractor;
use frontend\modules\documents\assets\DocumentPrintAsset;
use frontend\modules\documents\components\InvoiceHelper;
use frontend\modules\documents\components\Message;
use frontend\modules\documents\forms\InvoiceSendForm;
use frontend\modules\export\models\one_c\OneCExport;
use Yii;
use yii\base\Exception;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\db\Query;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\TaxRate;

/**
 * This is the model class for table "upd".
 *
 * @property integer $id
 * @property string $uid
 * @property integer $type
 * @property integer $company_id
 * @property integer $contractor_id
 * @property integer $invoice_id
 * @property integer $document_author_id
 * @property integer $status_out_id
 * @property integer $status_out_author_id
 * @property string $document_date
 * @property string $document_number
 * @property string $document_additional_number
 * @property integer $consignor_id
 * @property integer $consignee_id
 * @property string $waybill_number
 * @property string $waybill_date
 * @property string $object_guid
 * @property string $object_invoice_facture_guid
 * @property integer $ordinal_document_number
 * @property integer $is_original
 * @property integer $is_original_updated_at
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $status_out_updated_at
 * @property integer $signed_by_employee_id
 * @property string $signed_by_name
 * @property integer $sign_document_type_id
 * @property string $sign_document_number
 * @property string $sign_document_date
 * @property integer $signature_id
 * @property integer $state_contract
 * @property integer $show_service_units
 * @property boolean $contractor_address
 * @property boolean $add_stamp
 * @property integer $remaining_amount
 *
 * @property OrderUpd[] $orderUpds
 * @property Order[] $orders
 * @property AgreementType $basisDocumentType
 * @property DocumentType $signDocumentType
 * @property EmployeeSignature $signature
 * @property Employee $documentAuthor
 * @property Employee $signedByEmployee
 * @property Employee $statusOutAuthor
 * @property Invoice $invoice
 * @property UpdStatus $statusOut
 * @property UpdPaymentDocument[] $paymentDocuments
 * @property Contractor $consignee
 * @property Contractor $consignor
 * @property InvoiceUpd[] $invoiceUpds
 * @property Invoice[] $invoices
 * @property $orders_sum int
 * @property $order_nds int
 */
class Upd extends AbstractDocument implements BasisDocumentInterface
{
    use BasisDocumentTrait;

    const PAYMENT_DOCUMENT_DEFAULT_VALUE = '---';

    /**
     * @var string
     */
    public static $uploadDirectory = 'upd';

    public $agreement;

    /**
     * @var string
     */
    public $printablePrefix = 'Универсальный передаточный документ';
    public $shortPrefix = 'УПД';

    const CONTRACTOR_ADDRESS_LEGAL = 0;
    const CONTRACTOR_ADDRESS_ACTUAL = 1;

    /**
     * @var string
     */
    public $urlPart = 'upd';

    private $beforeDeleteInvoices = [];

    public $isCreateForSeveralInvoices;

    public $_isGroupOwnOrdersByProduct;

    public function behaviors()
    {
        return [
            'timestamp' => TimestampBehavior::className(),
        ];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'upd';
    }

    /**
     * @return UpdQuery
     */
    public static function find()
    {
        return new UpdQuery(get_called_class());
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return ArrayHelper::merge(parent::rules(), [
            [['document_number', 'documentDate'], 'required'],
            [['document_number', 'waybill_number'], 'string', 'max' => 255],
            [['documentDate', 'waybillDate'], 'date', 'format' => 'php:d.m.Y'],
            [['document_number',], 'integer',
                'when' => function ($model) {
                    return $model->type == Documents::IO_TYPE_OUT;
                },
            ],
            [['document_number',], 'outDocumentNumberValidator',
                'when' => function ($model) {
                    return $model->type == Documents::IO_TYPE_OUT;
                },
            ],
            [['document_additional_number'], 'string', 'max' => 45,
                'when' => function ($model) {
                    return $model->type == Documents::IO_TYPE_OUT;
                },
            ],
            [['agreement'], 'safe'],
            [['consignor_id', 'consignee_id'], 'exist', 'targetClass' => Contractor::className(), 'targetAttribute' => 'id'],
            [['state_contract'], 'string', 'max' => 25],
            [['show_service_units'], 'boolean',
                'when' => function ($model) {
                    return $model->type == Documents::IO_TYPE_OUT;
                },
            ],
            [['contractor_address'], 'integer'],
            [['orders_sum', 'order_nds'], 'filter', 'filter' => function ($value) {
                return round($value);
            }],
            [['orders_sum', 'order_nds'], 'integer'],
            // invoice fields
            [['company_id', 'contractor_id'], 'safe'],
            [['remaining_amount'], 'integer']
        ]);
    }

    /**
     * @param $attribute
     * @param $params
     */
    public function outDocumentNumberValidator($attribute, $params)
    {
        if ($this->isNumberExists($this->$attribute)) {
            if ($this->findFreeNumber) {
                $nextNumber = $this->$attribute;
                do {
                    $nextNumber++;
                } while ($this->isNumberExists($nextNumber));
            } else {
                $nextNumber = null;
            }
            $this->addError($attribute, "Номер {$this->$attribute} занят. Укажите свободный номер {$nextNumber}");
        }
    }

    /**
     * @param $number
     * @return bool
     */
    public function isNumberExists($number)
    {
        $query1 = self::find()->alias('doc')->joinWith(['invoice'])
            ->andWhere(['not', ['doc.id' => $this->id]])
            ->andWhere([
                'invoice.is_deleted' => false,
                'invoice.company_id' => $this->invoice->company_id,
                'doc.type' => $this->type,
                'doc.document_number' => $number,
            ]);
        $query2 = Act::find()->alias('doc')->joinWith(['invoice'])
            ->andWhere(['!=', 'doc.invoice_id', $this->invoice->id])
            ->andWhere([
                'invoice.is_deleted' => false,
                'invoice.company_id' => $this->invoice->company_id,
                'doc.type' => $this->type,
                'doc.document_number' => $number,
            ]);
        $query3 = PackingList::find()->alias('doc')->joinWith(['invoice'])
            ->andWhere(['!=', 'doc.invoice_id', $this->invoice->id])
            ->andWhere([
                'invoice.is_deleted' => false,
                'invoice.company_id' => $this->invoice->company_id,
                'doc.type' => $this->type,
                'doc.document_number' => $number,
            ]);
        $query4 = InvoiceFacture::find()->alias('doc')->joinWith(['invoice'])
            ->andWhere(['!=', 'doc.invoice_id', $this->invoice->id])
            ->andWhere([
                'invoice.is_deleted' => false,
                'invoice.company_id' => $this->invoice->company_id,
                'doc.type' => $this->type,
                'doc.document_number' => $number,
            ]);

        if ($this->document_date) {
            $year = date('Y', strtotime($this->document_date));
            $query1->andWhere(['between', 'doc.document_date', $year.'-01-01', $year.'-12-31']);
            $query2->andWhere(['between', 'doc.document_date', $year.'-01-01', $year.'-12-31']);
            $query3->andWhere(['between', 'doc.document_date', $year.'-01-01', $year.'-12-31']);
            $query4->andWhere(['between', 'doc.document_date', $year.'-01-01', $year.'-12-31']);
        }
        if ($this->document_additional_number) {
            $query1->andWhere(['doc.document_additional_number' => $this->document_additional_number]);
            $query2->andWhere(['doc.document_additional_number' => $this->document_additional_number]);
            $query3->andWhere(['doc.document_additional_number' => $this->document_additional_number]);
            $query4->andWhere(['doc.document_additional_number' => $this->document_additional_number]);
        } else {
            $query1->andWhere(['or',
                ['doc.document_additional_number' => ''],
                ['doc.document_additional_number' => null],
            ]);
            $query2->andWhere(['or',
                ['doc.document_additional_number' => ''],
                ['doc.document_additional_number' => null],
            ]);
            $query3->andWhere(['or',
                ['doc.document_additional_number' => ''],
                ['doc.document_additional_number' => null],
            ]);
            $query4->andWhere(['or',
                ['doc.document_additional_number' => ''],
                ['doc.document_additional_number' => null],
            ]);
        }

        return $query1->exists() || $query2->exists() || $query3->exists() || $query4->exists();
    }

    /**
     * Generate document number
     */
    public function newDocumentNumber()
    {
        $this->document_number = $this->document_additional_number = null;

        $query1 = $this->invoice->getActs()
            ->select(new Expression('[[document_number]], IF([[document_additional_number]] = "", NULL, [[document_additional_number]]) [[additional_number]]'));
        $query2 = $this->invoice->getPackingLists()
            ->select(new Expression('[[document_number]], IF([[document_additional_number]] = "", NULL, [[document_additional_number]]) [[additional_number]]'));
        $query3 = $this->invoice->getInvoiceFactures()
            ->select(new Expression('[[document_number]], IF([[document_additional_number]] = "", NULL, [[document_additional_number]]) [[additional_number]]'));
        $query = (new Query)
            ->select(['document_number', 'additional_number'])
            ->from(['t' => $query1->union($query2)->union($query3)])
            ->orderBy([
                'document_number' => SORT_ASC,
                'additional_number' => SORT_ASC,
            ]);
        if ($rowArray = $query->all()) {
            foreach ($rowArray as $row) {
                $this->document_number = $row['document_number'];
                $this->document_additional_number = $row['additional_number'];
                if ($this->validate(['document_number'])) {
                    break;
                } else {
                    $this->document_number = $this->document_additional_number = null;
                }
            }
        }
        if (!$this->document_number || $this->isNumberExists($this->document_number)) {
            $this->document_number = $this->getMaxDocumentNumber();
            do {
                $this->document_number++;
                $this->document_number .= '';
            } while (!$this->validate(['document_number']) || $this->isNumberExists($this->document_number));
        }
    }

    /**
     * @return boolean
     */
    public function getIsEditableNumber()
    {
        if ($this->isNewRecord || $this->type == Documents::IO_TYPE_IN) {
            return true;
        } else {
            $query1 = $this->invoice->getActs()->andWhere(['document_number' => $this->document_number]);
            $query2 = $this->invoice->getPackingLists()->andWhere(['document_number' => $this->document_number]);
            $query3 = $this->invoice->getInvoiceFacture()->andWhere(['document_number' => $this->document_number]);
            if ($this->document_additional_number) {
                $query1->andWhere(['document_additional_number' => $this->document_additional_number]);
                $query2->andWhere(['document_additional_number' => $this->document_additional_number]);
                $query3->andWhere(['document_additional_number' => $this->document_additional_number]);
            } else {
                $query1->andWhere(['or',
                    ['document_additional_number' => ''],
                    ['document_additional_number' => null],
                ]);
                $query2->andWhere(['or',
                    ['document_additional_number' => ''],
                    ['document_additional_number' => null],
                ]);
                $query3->andWhere(['or',
                    ['document_additional_number' => ''],
                    ['document_additional_number' => null],
                ]);
            }

            return (!$query1->exists() && !$query2->exists() && !$query3->exists());
        }
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'uid' => 'Uid',
            'type' => 'Type',
            'invoice_id' => 'Invoice ID',
            'document_author_id' => 'Document Author ID',
            'status_out_id' => 'Status Out ID',
            'status_out_author_id' => 'Status Out Author ID',
            'document_date' => 'Document Date',
            'document_number' => 'Document Number',
            'document_additional_number' => 'Document Additional Number',
            'object_guid' => 'Object Guid',
            'object_invoice_facture_guid' => 'Object Invoice Facture Guid',
            'ordinal_document_number' => 'Ordinal Document Number',
            'is_original' => 'Is Original',
            'is_original_updated_at' => 'Is Original Updated At',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'status_out_updated_at' => 'Status Out Updated At',
            'signed_by_employee_id' => 'Signed By Employee ID',
            'signed_by_name' => 'Signed By Name',
            'sign_document_type_id' => 'Sign Document Type ID',
            'sign_document_number' => 'Sign Document Number',
            'sign_document_date' => 'Sign Document Date',
            'signature_id' => 'Signature ID',
            'state_contract' => 'Гос.контракт, договор (соглашение)',
            'consignor_id' => 'Грузоотправитель',
            'consignee_id' => 'Грузополучатель',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderUpds()
    {
        return $this->hasMany(OrderUpd::className(), ['upd_id' => 'id']);
    }

    /**
     * @return array
     */
    public function getGroupedOrderUpds()
    {
        $orders = $this->getOrderUpds()->with('order')->all();

        return $this->groupOwnOrdersByProduct($orders);
    }

    /**
     * @return \yii\db\ActiveQuery|array
     */
    public function getOwnOrders()
    {
        // don't group foreign currency invoices
        if (isset($this->invoice) && $this->invoice->currency_name != Currency::DEFAULT_NAME)
            return $this->getOrderUpds();

        return ($this->isGroupOwnOrdersByProduct) ? $this->getGroupedOrderUpds() : $this->getOrderUpds();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrders()
    {
        return $this->hasMany(Order::className(), ['id' => 'order_id'])->viaTable('order_upd', ['upd_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSignature()
    {
        return $this->hasOne(EmployeeSignature::className(), ['id' => 'signature_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSignDocumentType()
    {
        return $this->hasOne(DocumentType::className(), ['id' => 'sign_document_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentAuthor()
    {
        return $this->hasOne(Employee::className(), ['id' => 'document_author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSignedByEmployee()
    {
        return $this->hasOne(Employee::className(), ['id' => 'signed_by_employee_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusOutAuthor()
    {
        return $this->hasOne(Employee::className(), ['id' => 'status_out_author_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(Invoice::className(), ['id' => 'invoice_id'])->viaTable('invoice_upd', ['upd_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceUpds()
    {
        return $this->hasMany(InvoiceUpd::className(), ['upd_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoices()
    {
        return $this->hasMany(Invoice::className(), ['id' => 'invoice_id'])->viaTable('invoice_upd', ['upd_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatusOut()
    {
        return $this->hasOne(UpdStatus::className(), ['id' => 'status_out_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentStatus()
    {
        return $this->getStatusOut();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPaymentDocuments()
    {
        return $this->hasMany(UpdPaymentDocument::className(), ['upd_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAgreementType()
    {
        return $this->hasOne(AgreementType::className(), ['id' => 'basis_document_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSignBasisDocument()
    {
        return $this->hasOne(DocumentType::className(), ['id' => 'sign_document_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdGoodsCancellations()
    {
        return $this->hasMany(UpdGoodsCancellation::className(), ['document_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGoodsCancellations()
    {
        return $this->hasMany(GoodsCancellation::className(), ['id' => 'cancellation_id'])->via('updGoodsCancellations');
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        $agreement = explode('&', $this->agreement);
        if (is_array($agreement)) {
            $this->basis_document_name = isset($agreement[0]) ? $agreement[0] : null;
            $this->basis_document_number = isset($agreement[1]) ? $agreement[1] : null;
            $this->basis_document_date = isset($agreement[2]) ? $agreement[2] : null;
            $this->basis_document_type_id = isset($agreement[3]) ? $agreement[3] : null;
        } else {
            $this->basis_document_name = null;
            $this->basis_document_number = null;
            $this->basis_document_date = null;
            $this->basis_document_type_id = null;
        }

        return parent::beforeValidate();
    }

    /**
     * @inheritdoc
     */
    public function setDocumentDate($value)
    {
        if ($value) {
            $this->document_date = ($d = date_create_from_format('d.m.Y', $value)) ? $d->format('Y-m-d') : $value;
        } else {
            $this->document_date = null;
        }
    }

    /**
     * @inheritdoc
     */
    public function getDocumentDate()
    {
        if (isset($this->document_date)) {
            return ($d = date_create_from_format('Y-m-d', $this->document_date)) ? $d->format('d.m.Y') : $this->document_date;
        } else {
            return null;
        }
    }

    /**
     * @inheritdoc
     */
    public function setWaybillDate($value)
    {
        if ($value) {
            $this->waybill_date = ($d = date_create_from_format('d.m.Y', $value)) ? $d->format('Y-m-d') : $value;
        } else {
            $this->waybill_date = null;
        }
    }

    /**
     * @inheritdoc
     */
    public function getWaybillDate()
    {
        if (isset($this->waybill_date)) {
            return ($d = date_create_from_format('Y-m-d', $this->waybill_date)) ? $d->format('d.m.Y') : $this->waybill_date;
        } else {
            return null;
        }
    }

    /**
     * @param bool $insert
     *
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->status_out_id = status\UpdStatus::STATUS_CREATED;
                if ($this->document_date === null) {
                    $this->document_date = date(DateHelper::FORMAT_DATE);
                }
                if (empty($this->document_number)) {
                    $this->newDocumentNumber();
                    $this->ordinal_document_number = $this->document_number;
                }
                if (empty($this->ordinal_document_number)) {
                    $this->ordinal_document_number = $this->document_number;
                }
            }

            if ($this->isAttributeChanged('status_out_id') && !Yii::$app->request->isConsoleRequest) {
                $this->status_out_updated_at = time();
                $this->status_out_author_id = Yii::$app->user->id;
                if ($this->status_out_id == UpdStatus::STATUS_REJECTED) {
                    foreach ($this->orderUpds as $order) {
                        $order->resetProductCount();
                    }
                }
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $this->beforeDeleteInvoices = $this->invoices;
            foreach ($this->getOrderUpds()->all() as $order) {
                if (!$order->delete()) {
                    return false;
                }
            }
            foreach ($this->goodsCancellations as $item) {
                $this->unlink('goodsCancellations', $item, true);
                if (!LogHelper::delete($item, LogEntityType::TYPE_DOCUMENT)) {
                    return false;
                }
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @inheritdoc
     */
    public function afterFind()
    {
        $this->agreement = "{$this->basis_document_name}&{$this->basis_document_number}&{$this->basis_document_date}&{$this->basis_document_type_id}";

        parent::afterFind();
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        if ($insert) {
            if ($this->invoice_id && !$this->isCreateForSeveralInvoices) {
                $invoiceUpd = new InvoiceUpd();
                $invoiceUpd->invoice_id = $this->invoice_id;
                $invoiceUpd->upd_id = $this->id;
                $invoiceUpd->save();
            }
        }

        if (!$insert) {
            foreach ($this->invoices as $invoice) {
                InvoiceHelper::checkForUpd($invoice->id, $this->invoices);
            }
        }
    }

    /**
     * @inheritdoc
     */
    public function afterDelete()
    {
        parent::afterDelete();

        foreach ($this->beforeDeleteInvoices as $invoice) {
            InvoiceHelper::checkForUpd($invoice->id, $this->beforeDeleteInvoices);

            // remove plan flow
            if (in_array($invoice->payment_limit_rule, [Invoice::PAYMENT_LIMIT_RULE_MIXED, Invoice::PAYMENT_LIMIT_RULE_DOCUMENT])) {
                if ($invoiceWithoutThisDoc = Invoice::findOne($invoice->id)) {
                    PlanCashContractor::updateFlowByInvoice($invoiceWithoutThisDoc, Documents::DOCUMENT_UPD);
                }
            }
        }
    }

    /**
     * @return array
     */
    public function getConsignorArray()
    {
        $tContractor = Contractor::tableName();
        $tType = CompanyType::tableName();

        $query = Contractor::getSorted()
            ->select([
                'contractor_name' => new Expression("IF({{{$tType}}}.[[id]] IS NULL,
                    {{{$tContractor}}}.[[name]],
                    CONCAT({{{$tType}}}.[[name_short]], ' ', {{{$tContractor}}}.[[name]]))"),
                Contractor::tableName() . '.id',
            ])
            ->byCompany(Yii::$app->user->identity->company->id)
            ->byDeleted()
            ->andWhere(['or', [Contractor::tableName() . '.is_seller' => 1], [Contractor::tableName() . '.is_customer' => 1]])
            ->andWhere(['!=', Contractor::tableName() . '.id', $this->invoice->contractor_id])
            ->indexBy('id');

        return ArrayHelper::merge(['' => 'Он же'], $query->column());
    }

    /**
     * @return null|Contractor
     */
    public function getConsignor()
    {
        return Contractor::findOne([$this->consignor_id]);
    }

    /**
     * @return null|Contractor
     */
    public function getConsignee()
    {
        return Contractor::findOne([$this->consignee_id]);
    }

    /**
     * @inheritdoc
     */
    public function getFile()
    {
        return $this->hasOne(File::className(), ['owner_id' => 'id'])
            ->onCondition(['owner_model' => Upd::className(),]);
    }

    /**
     * @inheritdoc
     */
    public function getFiles()
    {
        return $this->hasMany(File::className(), ['owner_id' => 'id'])
            ->onCondition(['owner_model' => Upd::className(),]);
    }

    /**
     * @inheritdoc
     */
    public function getScanDocuments()
    {
        return $this->hasMany(ScanDocument::className(), ['owner_id' => 'id'])
            ->onCondition(['owner_model' => Upd::className(),]);
    }

    /**
     * Full invoice number
     *
     * @return string
     * @throws Exception
     */
    public function getFullNumber()
    {
        if ($this->document_number !== null) {
            $documentNumber = $this->document_number;
        } else {
            $documentNumber = $this->invoice->ordinal_document_number;
        }

        if ($this->type == Documents::IO_TYPE_OUT) {

            if ($this->invoice && $this->invoice->is_additional_number_before) {
                return $this->document_additional_number . $documentNumber;
            } else {
                return $documentNumber . $this->document_additional_number;
            }

        }

        return $documentNumber;
    }

    /**
     * @return string
     */
    public function getPrintTitle()
    {
        return str_replace([' ', '"'], ['_'],
            $this->printablePrefix . '_№' . $this->getFullNumber() . '_' .
            date_format(date_create($this->document_date), 'd.m.Y') . '_' .
            $this->invoice->company_name_short
        );
    }

    /**
     * Get formatted log message from Log model
     *
     * @param Log $log
     *
     * @return string
     */
    public function getLogMessage(Log $log)
    {
        $title = 'УПД №';
        $invoiceText = '';
        /* @var Invoice $invoice */
        $invoice = Invoice::findOne($log->getModelAttributeNew('invoice_id'));

        $ioType = $log->getModelAttributeNew('type');
        if ($ioType == Documents::IO_TYPE_IN) {
            $title .= $log->getModelAttributeNew('document_number');
        } else {
            $title .= $log->getModelAttributeNew('document_number') . ' ' . $log->getModelAttributeNew('document_additional_number');
        }

        $link = Html::a($title, [
            '/documents/upd/view',
            'type' => $log->getModelAttributeNew('type'),
            'id' => $log->getModelAttributeNew('id'),
        ]);

        if ($log->log_event_id == LogEvent::LOG_EVENT_RESPONSIBLE) {
            $message = $link . ', изменен ответственный';
            if ($this->invoice->responsible) {
                $message .= ' на '.$this->invoice->responsible->getFio(true).'.';
            } else {
                $message .= '.';
            }

            return $message;
        }

        if ($invoice) {
            $amount = TextHelper::invoiceMoneyFormat($invoice->total_amount_with_nds, 2) . ' руб.';
            $contractorName = $invoice->contractor ? ', ' . $invoice->contractor->getTitle(true) : '';
            $invoiceText = ', ' . $amount . $contractorName . ',';
        }

        switch ($log->log_event_id) {
            case LogEvent::LOG_EVENT_CREATE:
                return $link . $invoiceText . ' был создан.';
            case LogEvent::LOG_EVENT_DELETE:
                return $title . $invoiceText . ' был удален.';
            case LogEvent::LOG_EVENT_UPDATE:
                return $link . $invoiceText . ' был изменен.';
            case LogEvent::LOG_EVENT_UPDATE_STATUS:
                return $link . $invoiceText . ' статус "' . UpdStatus::findOne($log->getModelAttributeNew('status_out_id'))->name . '"';
            default:
                return $log->message;
        }
    }

    /**
     * @param $fileName
     * @param $upd
     * @return PdfRenderer
     */
    public static function getRenderer($fileName, $upd, $destination = PdfRenderer::DESTINATION_FILE)
    {
        $renderer = new PdfRenderer([
            'view' => '@documents/views/upd/pdf-view',
            'layout' => '@frontend/modules/documents/views/layouts/upd',
            'params' => array_merge([
                'model' => $upd,
                'message' => new Message(Documents::DOCUMENT_INVOICE_FACTURE, $upd->type),
                'ioType' => $upd->type,
                'addStamp' => true,
            ]),

            'destination' => $destination,
            'filename' => $fileName,
            'displayMode' => PdfRenderer::DISPLAY_MODE_FULLPAGE,
            'format' => 'A4-L',
        ]);
        \Yii::$app->controller->view->params['asset'] = DocumentPrintAsset::className();

        return $renderer;
    }

    /**
     * @inheritdoc
     */
    public function setStatusPrinted()
    {
        if (in_array($this->status_out_id, [UpdStatus::STATUS_CREATED, UpdStatus::STATUS_PRINTED])) {
            LogHelper::save($this, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_UPDATE_STATUS, function ($model) {
                $model->status_out_id = UpdStatus::STATUS_PRINTED;
                $model->status_out_updated_at = time();
                $model->status_out_author_id = \Yii::$app->user->id;

                return $model->save(true, ['status_out_id', 'status_out_updated_at', 'status_out_author_id']);
            });
        }
    }

    /**
     * @param $period
     * @param $upds
     */
    public static function generateXlsTable($period, $upds)
    {
        $excel = new Excel();

        $fileName = 'УПД_' . DateHelper::format($period['from'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) .
            '-' . DateHelper::format($period['to'], DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);

        $excel->export([
            'mode' => 'export',
            'isMultipleSheet' => false,
            'asAttachment' => true,
            'models' => $upds,
            'title' => 'УПД',
            'columns' => [
                [
                    'attribute' => 'document_date',
                    'value' => function (Upd $data) {
                        return DateHelper::format($data->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
                    },
                ],
                [
                    'attribute' => 'document_number',
                    'value' => function (Upd $data) {
                        return $data->fullNumber;
                    },
                ],
                [
                    'attribute' => 'invoice.total_amount_with_nds',
                    'styleArray' => [
                        'numberformat' => [
                            'code' => '# ##0.00',
                        ]
                    ],
                    'dataType' => 'n',
                    'value' => function ($model) {
                        return bcdiv($model->totalAmountWithNds, 100, 2);
                    },
                ],
                [
                    'attribute' => 'contractor_id',
                    'value' => function (Upd $data) {
                        return $data->invoice->contractor_name_short;
                    },
                ],
                [
                    'attribute' => 'status_out_id',
                    'value' => function (Upd $data) {
                        return $data->statusOut->name;
                    },
                ],
                [
                    'attribute' => 'invoice.document_number',
                    'value' => function (Upd $data) {
                        return $data->invoice->fullNumber;
                    },
                ],
                [
                    'attribute' => 'nomenclature',
                    'wrap' => true,
                    'value' => function (Upd $model) {

                        $list = [];
                        foreach ($model->orders as $order) {
                            $unitName = $order->unit ? $order->unit->name : Product::DEFAULT_VALUE;
                            $list[] = $order->product_title . ' ' .
                                TextHelper::numberFormat($order->quantity, 2) . ' ' .
                                $unitName . ' x ' .
                                TextHelper::invoiceMoneyFormat($order->view_price_one, 2) . 'р. = ' .
                                TextHelper::invoiceMoneyFormat($order->view_total_amount, 2) . 'р.';
                        }

                        return $list ? implode("\r", $list) : 'НЕТ';
                    },
                ]
            ],
            'headers' => [
                'document_date' => 'Дата УПД',
                'document_number' => '№ УПД',
                'invoice.total_amount_with_nds' => 'Сумма',
                'contractor_id' => 'Контрагент',
                'status_out_id' => 'Статус',
                'invoice.document_number' => 'Счёт №',
                'nomenclature' => 'Номенклатурная часть'
            ],
            'format' => 'Xlsx',
            'fileName' => $fileName,
        ]);
    }

    /**
     * @return integer
     */
    public function getTotalAmountWithNds()
    {
        $amount = 0;
        foreach ($this->ownOrders as $key => $value) {
            $amount += $value->amountWithNds;
        }

        return $amount;
    }

    /**
     * @return integer
     */
    public function getOrders_sum()
    {
        return $this->getTotalAmountWithNds();
    }

    /**
     * @return integer
     */
    public function getTotalAmountNoNds()
    {
        $amount = 0;
        foreach ($this->ownOrders as $key => $value) {
            $amount += $value->amountNoNds;
        }

        return $amount;
    }

    /**
     * @return integer
     */
    public function getTotalNds()
    {
        return $this->totalAmountWithNds - $this->totalAmountNoNds;
    }

    /**
     * @return query\OrderQuery
     */
    public function getPosibleOrders()
    {
        $invoices = ArrayHelper::getColumn($this->invoices, 'id');
        return Order::find()
            ->andWhere(['in', 'order.invoice_id', $invoices])
            ->leftJoin(
                '{{order_upd}}',
                '{{order_upd}}.[[order_id]] = {{order}}.[[id]] AND {{order_upd}}.[[upd_id]] != :id',
                [':id' => $this->id]
            )
            ->having(['>', 'order.quantity', new Expression('SUM(IFNULL({{order_upd}}.[[quantity]], 0))')])
            ->groupBy('order.id');
    }

    /**
     * @param array $orderParams
     * @param array $paymentsData
     * @param array $surchargeProducts
     * @return mixed
     * @throws \Throwable
     */
    public function updateAndSave($orderParams = [], $paymentsData = [], $surchargeProducts = [])
    {
        $isSaved = Yii::$app->db->transaction(function ($db) use ($orderParams, $paymentsData, $surchargeProducts) {

            if (!$this->save()) {
                Yii::$app->session->setFlash('error', Html::errorSummary($this));
                $db->transaction->rollBack();

                return false;
            }

            $availableQuantity = [];
            foreach ($this->orderUpds as $orderUpd) {
                $availableQuantity[$orderUpd->order_id] = OrderUpd::availableQuantity($orderUpd->order, $this->id);
            }

            $ownOrderArray = [];
            $ownOrderUpdated = 0;
            $invoices = ArrayHelper::getColumn($this->invoices, 'id');
            if (is_array($orderParams)) {

                $newOrderIds = array_keys($orderParams);
                $oldOrderIds = \yii\helpers\ArrayHelper::getColumn($this->orderUpds, 'order_id', []);

                // delete
                foreach ($this->orderUpds as $orderUpd) {
                    if (!in_array($orderUpd->order_id, $newOrderIds))
                        $orderUpd->delete();
                }

                foreach ($orderParams as $orderId => $params) {
                    /* @var $order Order */
                    $order = $orderId ? Order::findOne(['id' => $orderId, 'invoice_id' => $invoices]) : null;
                    $quantity = ArrayHelper::getValue($params, 'quantity');
                    if (!empty($quantity) && $order) {

                        // update
                        if (in_array($orderId, $oldOrderIds)) {
                            foreach ($this->orderUpds as $orderUpd) {
                                if ($orderId == $orderUpd->order_id) {
                                    $orderUpd->quantity = max(0, $quantity);
                                    $orderUpd->country_id = ArrayHelper::getValue($params, 'country_id');
                                    $orderUpd->custom_declaration_number = ArrayHelper::getValue($params, 'custom_declaration_number');
                                    if (!$orderUpd->save()) {
                                        Yii::$app->session->setFlash('error', Html::errorSummary($orderUpd));
                                        $db->transaction->rollBack();
                                        return false;
                                    }
                                    $ownOrderUpdated++;
                                }
                            }

                            if ($quantity > $availableQuantity[$orderId]) {
                                $this->surchargeInvoiceOrders[$orderId] = $quantity - $availableQuantity[$orderId];
                            }
                        }
                        // insert
                        else {
                            $ownOrderArray[] = new OrderUpd([
                                'upd_id' => $this->id,
                                'order_id' => $order->id,
                                'product_id' => $order->product_id,
                                'quantity' => max(0, $quantity),
                                'country_id' => ArrayHelper::getValue($params, 'country_id'),
                                'custom_declaration_number' => ArrayHelper::getValue($params, 'custom_declaration_number'),
                            ]);
                        }
                    }
                }
            }
            if ($ownOrderArray) {
                foreach ($ownOrderArray as $ownOrder) {
                    $ownOrder->quantity = number_format($ownOrder->quantity, 10, '.', '');
                    if (!$ownOrder->save()) {
                        Yii::$app->session->setFlash('error', Html::errorSummary($ownOrder));
                        $db->transaction->rollBack();
                        return false;
                    }
                }
            } elseif (!$ownOrderUpdated) {
                Yii::$app->session->setFlash('error', 'УПД не может быть пустой');
                $db->transaction->rollBack();

                return false;
            }

            if ($this->surchargeInvoiceOrders || $surchargeProducts) {
                if (!$this->createSurchargeInvoice($surchargeProducts)) {
                    Yii::$app->session->setFlash('error', 'Не удалось создать счет на доплату');
                    $db->transaction->rollBack();
                    return false;
                }
            }

            foreach ($this->invoices as $invoice) {
                InvoiceHelper::checkForUpd($invoice->id, $this->invoices);
            }

            UpdPaymentDocument::deleteAll(['upd_id' => $this->id]);
            if (isset($paymentsData['number']) && is_array($paymentsData['number'])) {
                foreach ($paymentsData['number'] as $key => $value) {
                    if (($value = trim($value)) && ($date = \DateTime::createFromFormat('d.m.Y', $paymentsData['date'][$key]))) {
                        $payment = new UpdPaymentDocument([
                            'upd_id' => $this->id,
                            'payment_document_number' => $value,
                            'payment_document_date' => $date->format('Y-m-d'),
                        ]);
                        if (!$payment->save()) {
                            Yii::$app->session->setFlash('error', Html::errorSummary($payment));
                            $db->transaction->rollBack();

                            return false;
                        }
                    }
                }
            }

            return true;
        });

        // trigger olap_documents_update_order_upd
        if ($isSaved) {
            OrderUpd::findOne(['upd_id' => $this->id])->save(false);

            // refresh orders sum
            $orders_sum = 0;
            $order_nds = 0;
            foreach ($this->getOwnOrders()->all() as $key => $value) {
                $orders_sum += $value->amountWithNds;
                $order_nds += $value->amountNds;
            }
            $this->updateAttributes([
                'orders_sum' => round($orders_sum),
                'order_nds' => round($order_nds)
            ]);
            $this->invoice->updateDocumentsPaid();
            PlanCashContractor::updateFlowByInvoice($this->invoice, Documents::DOCUMENT_UPD);
        }

        return $isSaved;
    }

    /**
     * @return string
     */
    public function getEmailSubject()
    {
        return 'УПД № ' . $this->fullNumber
            . ' от ' . DateHelper::format($this->document_date, 'd.m.y', DateHelper::FORMAT_DATE)
            . ' от ' . $this->invoice->company_name_short;
    }

    /**
     * @return string
     */
    public function getEmailText()
    {
        $document_date = DateHelper::format($this->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
        $sum = TextHelper::invoiceMoneyFormat($this->totalAmountWithNds, 2);

        $text = <<<EMAIL_TEXT
Здравствуйте!

Универсальный передаточный документ № {$this->fullNumber} от {$document_date} на сумму {$sum} р.
EMAIL_TEXT;

        return $text;
    }

    /**
     * @return array
     * @throws \MpdfException
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function loadEmailFiles()
    {
        $files = [];

        $emailFile = new EmailFile();
        $emailFile->company_id = $this->invoice->company_id;
        $emailFile->file_name = str_replace(['\\', '/'], '_', $this->pdfFileName);
        $emailFile->ext = 'pdf';
        $emailFile->mime = 'application/pdf';
        $emailFile->size = 0;
        $emailFile->type = EmailFile::TYPE_UPD_DOCUMENT;
        if ($emailFile->save()) {
            $path = $emailFile->getFilePath() . DIRECTORY_SEPARATOR . $emailFile->file_name;
            $size = file_put_contents($path, self::getRenderer(null, $this, PdfRenderer::DESTINATION_STRING)->output(false));
            $emailFile->size = ceil($size / 1024);
            if ($emailFile->save(true, ['size'])) {
                $files[] = [
                    'id' => $emailFile->id,
                    'name' => $emailFile->file_name,
                    'previewImg' => '<img src="/img/email/emailPdf.png" class="preview-img">',
                    'size' => $emailFile->size,
                    'deleteUrl' => Url::to(['/email/delete-email-file', 'id' => $emailFile->id]),
                    'downloadUrl' => Url::to(['/email/download-file', 'id' => $emailFile->id]),
                ];
            }
        }

        return $files;
    }

    /**
     * @param EmployeeCompany $sender
     * @param $toEmail
     * @param $type
     * @param $updIDs
     * @return bool
     * @throws \MpdfException
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     * @throws \Exception
     */
    public static function manySend(EmployeeCompany $sender, $toEmail, $type, $updIDs)
    {
        /* @var $models Upd[] */
        $models = [];
        $emailSubject = [];
        $companyID = null;
        $emailTemplate = null;
        $companyNameShort = null;
        if ($type == Documents::IO_TYPE_OUT) {
            foreach ($updIDs as $updID) {
                $upd = is_object($updID) ? $updID : self::findOne($updID);
                if ($upd->uid == null) {
                    $upd->updateAttributes([
                        'uid' => self::generateUid(),
                    ]);
                }
                $models[] = $upd;
                $emailSubject[] = 'УПД № ' . $upd->fullNumber
                    . ' от ' . DateHelper::format($upd->document_date, 'd.m.y', DateHelper::FORMAT_DATE);
                $companyID = $upd->invoice->company_id;
                $companyNameShort = $upd->invoice->company_name_short;
            }
            $emailSubject = implode(', ', $emailSubject);
            $emailSubject .= (' от ' . $companyNameShort);
            $params = [
                'model' => $models,
                'employee' => $sender->employee,
                'employeeCompany' => $sender,
                'subject' => $emailSubject,
                'pixel' => [
                    'company_id' => $companyID,
                    'email' => is_array($toEmail) ? implode(',', $toEmail) : $toEmail,
                ],
            ];
            Yii::$app->mailer->htmlLayout = 'layouts/document-html';
            $message = Yii::$app->mailer->compose([
                'html' => 'system/documents/upd-out/html',
                'text' => 'system/documents/upd-out/text',
            ], $params)
                ->setFrom([Yii::$app->params['emailList']['docs'] => $sender->getFio()])
                ->setReplyTo([$sender->email => $sender->getFio(true)])
                ->setSubject($emailSubject)
                ->setTo($toEmail);
            foreach ($models as $model) {
                $message->attachContent(Upd::getRenderer(null, $model, PdfRenderer::DESTINATION_STRING)->output(false), [
                    'fileName' => $model->pdfFileName,
                    'contentType' => 'application/pdf',
                ]);
            }
            if ($message->send()) {
                foreach ($models as $model) {
                    InvoiceSendForm::setUpdSendStatus($model);
                }

                return true;
            }
        }

        return false;
    }

    /**
     * @return bool
     */
    public function createSurchargeInvoice($newProducts = [])
    {
        $company = Yii::$app->user->identity->company;
        if (!$company->createInvoiceAllowed($this->invoice->type)) {
            return false;
        }
        $cloneInvoice = new Invoice($this->invoice->getAttributes([
            'type',
            'production_type',
            'document_date',
            'payment_limit_date',
            'has_markup',
            'has_discount',
            'nds_view_type_id',
            'price_precision',
            'currency_name',
            'currency_amount',
            'currency_rate',
            'currency_rate_type',
            'currency_rate_date',
            'currency_rate_amount',
            'currency_rate_value',
            'invoice_expenditure_item_id',
        ]));
        $cloneInvoice->is_surcharge = 1; // disable change quantity!
        $cloneInvoice->document_date = date(DateHelper::FORMAT_DATE);
        $cloneInvoice->document_number = ($this->type == Documents::IO_TYPE_OUT) ?
            (string)$cloneInvoice->getNextDocumentNumber($company->id, $this->type, null, $cloneInvoice->document_date) :
            (string)$this->invoice->document_number;
        $cloneInvoice->document_additional_number = $this->invoice->document_additional_number;
        $cloneInvoice->payment_limit_date = date(DateHelper::FORMAT_DATE, strtotime('+10 day'));
        $cloneInvoice->company_id = $this->invoice->company->id;
        $cloneInvoice->contractor_id = $this->invoice->contractor->id;
        $cloneInvoice->object_guid = OneCExport::generateGUID();
        $cloneInvoice->populateRelation('company', $this->invoice->company);
        $cloneInvoice->populateRelation('contractor', $this->invoice->contractor);

        $cloneOrderArray = [];
        $newOrderUpdArray = [];
        foreach ((array)$this->surchargeInvoiceOrders as $orderId => $quantity) {
            /** @var Order $order */
            $order = Order::findOne($orderId);
            if ($order) {
                $cloneOrder = OrderHelper::createOrderByProduct(
                    $order->product,
                    $cloneInvoice,
                    $quantity,
                    $order->price * 100,
                    $order->discount
                );
                $cloneOrder->product_title = $order->product_title;

                $cloneOrderArray[] = $cloneOrder;
            }
        }
        foreach ((array)$newProducts as $productID => $data) {
            /** @var Product $product */
            $product = Product::find()->where(['id' => $productID, 'company_id' => $this->invoice->company_id])->one();
            $customPrice = (100 * (float)str_replace(',' ,'.', $data['price']));
            if ($customPrice && $this->invoice->nds_view_type_id == Invoice::NDS_VIEW_IN) {
                $customPrice = round($customPrice + (($product->priceForSellNds instanceof TaxRate) ?
                    ($this->invoice->type == 2 ? $customPrice * $product->priceForSellNds->rate : $customPrice * $product->priceForBuyNds->rate) : 0));
            }
            if ($product) {
                // invoice order
                $cloneOrder = OrderHelper::createOrderByProduct(
                    $product,
                    $cloneInvoice,
                    $data['quantity'],
                    $customPrice ?: null
                );
                $cloneOrder->country_id = $data['country_id'];
                $cloneOrder->custom_declaration_number = str_replace('---', '', $data['custom_declaration_number']);
                $cloneOrderArray[] = $cloneOrder;
                // upd order
                $orderUpd = new OrderUpd([
                    'upd_id' => $this->id,
                    'order_id' => null,
                    'product_id' => $productID,
                    'quantity' => $cloneOrder->quantity,
                    'country_id' => $cloneOrder->country_id,
                    'custom_declaration_number' => $cloneOrder->custom_declaration_number
                ]);

                $newOrderUpdArray[$productID] = $orderUpd;
            }
        }

        $cloneInvoice->populateRelation('orders', $cloneOrderArray);
        // save invoice
        if (!$cloneInvoice->save()) {

            return false;
        }
        // save orders
        foreach ($cloneInvoice->orders as $cloneOrder) {
            $cloneOrder->invoice_id = $cloneInvoice->id;
            if (!$cloneOrder->save(false)) {

                return false;
            }
            if (isset($newOrderUpdArray[$cloneOrder->product_id])) {
                /** @var OrderUpd $orderUpd */
                $orderUpd = $newOrderUpdArray[$cloneOrder->product_id];
                $orderUpd->order_id = $cloneOrder->id;
                if (!$orderUpd->save(false)) {

                    return false;
                }
            }
        }
        // save relation
        $invoice2Upd = new InvoiceUpd();
        $invoice2Upd->invoice_id = $cloneInvoice->id;
        $invoice2Upd->upd_id = $this->id;
        if (!$invoice2Upd->save(false)) {
            return false;
        }

        // update flags
        $cloneInvoice->updateAttributes([
            //
            'can_add_act' => 0,
            'can_add_packing_list' => 0,
            'can_add_sales_invoice' => 0,
            'can_add_proxy' => 0,
            'can_add_invoice_facture' => 0,
            'can_add_upd' => 0,
            'has_act' => 0,
            'has_packing_list' => 0,
            'has_sales_invoice' => 0,
            'has_proxy' => 0,
            'has_invoice_facture' => 0,
            'has_upd' => 1,
            'has_file' => 0
        ]);

        return true;
    }

    public function setIsGroupOwnOrdersByProduct($value)
    {
        $this->_isGroupOwnOrdersByProduct = $value;
    }

    public function getIsGroupOwnOrdersByProduct()
    {
        return ($this->document_date < '2021-07-01')
            ? $this->_isGroupOwnOrdersByProduct
            : false; // disable grouped products since 1 jule 2021
    }

    public function getPrintablePaymentDocuments()
    {
        $paymentDocuments = [];
        foreach ($this->paymentDocuments as $doc) {
            $date = date('d.m.Y', strtotime($doc->payment_document_date));
            $paymentDocuments[] = "№ {$doc->payment_document_number} от {$date}";
        }

        return $paymentDocuments ? join('; ', $paymentDocuments) : '№ — от —';
    }

    public function getPrintableShippingDocuments()
    {
        $shippingDocuments = [];
        $num = 1;
        foreach ([$this] as $doc) {
            $date = date('d.m.Y', strtotime($doc->document_date));
            $positionNumber = "{$num}-" . ($num += count($doc->ownOrders) - 1);
            $shippingDocuments[] = "№ п/п {$positionNumber} № {$doc->fullNumber} от {$date}";
            $num++;
        }

        return $shippingDocuments ? join('; ', $shippingDocuments) : '№ — от —';
    }

    /**
     * @param $byProducts
     * @return int
     */
    public function getTotalPurchaseAmountOnDate($byProducts)
    {
        $id = $this->id;
        $table = 'upd';
        $byProducts = ($byProducts) ? 1 : 0;

        try {
            return (int)Yii::$app->db->createCommand("
                SELECT SUM(IFNULL(`purchase_amount`, 0)) 
                FROM `product_turnover` 
                WHERE `document_id` = {$id} AND `document_table` = '{$table}' AND `production_type` = '{$byProducts}'
                GROUP BY `document_id`
            ")->queryScalar();
        } catch (\Throwable $e) {}

        return 0;
    }
}
