<?php

namespace common\models\document;

use Yii;

/**
 * This is the model class for table "order_document_in_to_out".
 *
 * @property int $out_order_document
 * @property int $out_order_document_product
 * @property int $in_order_document
 * @property int $in_order_document_product
 *
 * @property OrderDocument $inOrderDocument
 * @property OrderDocumentProduct $inOrderDocumentProduct
 * @property OrderDocument $outOrderDocument
 * @property OrderDocumentProduct $outOrderDocumentProduct
 */
class OrderDocumentInToOut extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'order_document_in_to_out';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['out_order_document', 'out_order_document_product', 'in_order_document', 'in_order_document_product'], 'required'],
            [['out_order_document', 'out_order_document_product', 'in_order_document', 'in_order_document_product'], 'integer'],
            [['in_order_document'], 'exist', 'skipOnError' => true, 'targetClass' => OrderDocument::class, 'targetAttribute' => ['in_order_document' => 'id']],
            [['in_order_document_product'], 'exist', 'skipOnError' => true, 'targetClass' => OrderDocumentProduct::class, 'targetAttribute' => ['in_order_document_product' => 'id']],
            [['out_order_document'], 'exist', 'skipOnError' => true, 'targetClass' => OrderDocument::class, 'targetAttribute' => ['out_order_document' => 'id']],
            [['out_order_document_product'], 'exist', 'skipOnError' => true, 'targetClass' => OrderDocumentProduct::class, 'targetAttribute' => ['out_order_document_product' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'out_order_document' => 'Out Order Document',
            'out_order_document_product' => 'Out Order Document Product',
            'in_order_document' => 'In Order Document',
            'in_order_document_product' => 'In Order Document Product',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInOrderDocument()
    {
        return $this->hasOne(OrderDocument::class, ['id' => 'in_order_document']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInOrderDocumentProduct()
    {
        return $this->hasOne(OrderDocumentProduct::class, ['id' => 'in_order_document_product']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutOrderDocument()
    {
        return $this->hasOne(OrderDocument::class, ['id' => 'out_order_document']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOutOrderDocumentProduct()
    {
        return $this->hasOne(OrderDocumentProduct::class, ['id' => 'out_order_document_product']);
    }
}
