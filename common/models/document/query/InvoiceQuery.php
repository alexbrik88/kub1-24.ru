<?php

namespace common\models\document\query;


use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;

/**
 * Class InvoiceQuery
 *
 * @package common\models\document
 */
class InvoiceQuery extends DocumentActiveQuery
{
    use \common\components\TableAliasTrait;

    protected $autoinvoice = false;

    /**
     * Constructor.
     * @param string|ActiveRecord $modelClass the model class associated with this query
     * @param array $config configurations to be applied to the newly created query object
     */
    public function __construct($modelClass, $config = [])
    {
        parent::__construct($modelClass, $config);

        $this->andWhere([
            'or',
            ['not', [$this->getTableAlias() . '.invoice_status_id' => InvoiceStatus::STATUS_AUTOINVOICE]],
            [$this->getTableAlias() . '.invoice_status_id' => null],
        ]);
    }

    /**
     *
     * @param $companyId
     * @return InvoiceQuery
     */
    public function byCompany($companyId)
    {
        return $this->andWhere([
            $this->getTableAlias() . '.company_id' => $companyId,
        ]);
    }

    /**
     *
     * @param int $contractorId
     * @return InvoiceQuery
     */
    public function byContractorId($contractorId)
    {
        return $this->andWhere([
            $this->getTableAlias() . '.contractor_id' => $contractorId,
        ]);
    }

    /**
     * @param int|int[] $status
     * @return $this
     */
    public function byStatus($status)
    {
        return $this->andWhere(['in', $this->getTableAlias() . '.invoice_status_id', $status]);
    }

    /**
     * @param bool|int $isDeleted
     * @return static
     */
    public function byDeleted($isDeleted = false)
    {
        return $this->andWhere([
            $this->getTableAlias() . '.is_deleted' => (int)$isDeleted,
        ]);
    }

    /**
     * @param bool|int $isDeleted
     * @return static
     */
    public function byDemo($isDemo = false)
    {
        return $this->andWhere([
            $this->getTableAlias() . '.from_demo_out_invoice' => (int)$isDemo,
        ]);
    }

    /**
     * @param int $ioType
     *
     * @return $this
     */
    public function byIOType($ioType)
    {
        return $this->andWhere([
            $this->getTableAlias() . '.type' => $ioType,
        ]);
    }
}
