<?php

namespace common\models\document\query;


use yii\db\Query;

interface IIOStrictQuery
{
    /**
     * @param int $ioType
     *
     * @return Query
     */
    public function byIOType($ioType);

}
