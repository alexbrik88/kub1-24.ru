<?php

namespace common\models\document\query;


use common\models\document\ForeignCurrencyInvoice;
use common\models\document\status\InvoiceStatus;

/**
 * Class ForeignCurrencyInvoiceQuery
 *
 * @package common\models\document
 */
class ForeignCurrencyInvoiceQuery extends DocumentActiveQuery
{
    use \common\components\TableAliasTrait;

    protected $autoinvoice = false;

    /**
     *
     * @param $companyId
     * @return ForeignCurrencyInvoiceQuery
     */
    public function byCompany($companyId)
    {
        return $this->andWhere([
            $this->getTableAlias() . '.company_id' => $companyId,
        ]);
    }

    /**
     *
     * @param int $contractorId
     * @return ForeignCurrencyInvoiceQuery
     */
    public function byContractorId($contractorId)
    {
        return $this->andWhere([
            $this->getTableAlias() . '.contractor_id' => $contractorId,
        ]);
    }

    /**
     * @param int|int[] $status
     * @return $this
     */
    public function byStatus($status)
    {
        return $this->andWhere(['in', $this->getTableAlias() . '.invoice_status_id', $status]);
    }

    /**
     * @param bool|int $isDeleted
     * @return static
     */
    public function byDeleted($isDeleted = false)
    {
        return $this->andWhere([
            $this->getTableAlias() . '.is_deleted' => (int)$isDeleted,
        ]);
    }

    /**
     * @param bool|int $isDeleted
     * @return static
     */
    public function byDemo($isDemo = false)
    {
        return $this->andWhere([
            $this->getTableAlias() . '.from_demo_out_invoice' => (int)$isDemo,
        ]);
    }

    /**
     * @param int $ioType
     *
     * @return $this
     */
    public function byIOType($ioType)
    {
        return $this->andWhere([
            $this->getTableAlias() . '.type' => $ioType,
        ]);
    }

    public function getTableAlias()
    {
        return $this->getTableNameAndAlias()[1];
    }
}
