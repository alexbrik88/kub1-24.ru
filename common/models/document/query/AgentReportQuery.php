<?php

namespace common\models\document\query;

/**
 * Class ActQuery
 *
 * @package common\models\document
 */
class AgentReportQuery extends DocumentActiveQuery
{
    /**
     * @param $companyId
     * @return $this
     */
    public function byCompany($companyId)
    {
        return $this->andWhere([
            'company_id' => $companyId,
        ]);
    }

    /**
     * @param int $type
     * @return $this
     */
    public function byType($type)
    {
        return $this->andWhere([
            'type' => $type,
        ]);
    }
}
