<?php

namespace common\models\document\query;

use common\models\document\GoodsCancellation;
use common\models\ICompanyStrictQuery;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * Class ProxyQuery
 *
 * @package common\models\document
 */
class GoodsCancellationQuery extends ActiveQuery implements ICompanyStrictQuery
{
    /**
     * @var string
     */
    public $tableName;

    /**
     * Constructor.
     * @param string|ActiveRecord $modelClass the model class associated with this query
     * @param array $config configurations to be applied to the newly created query object
     */
    public function __construct($modelClass, $config = [])
    {
        parent::__construct($modelClass, $config);

        $this->tableName = $modelClass::tableName();
    }

    /**
     * @param $companyId
     *
     * @return $this
     */
    public function byCompany($companyId)
    {
        return $this->andWhere([
            GoodsCancellation::tableName() . '.company_id' => $companyId,
        ]);
    }

    /**
     * @param int $ioType
     *
     * @return $this
     */
    public function byIOType($ioType)
    {
        return $this;
    }

    /**
     * @param $from
     * @param $to
     * @return $this
     */
    public function byDocumentDateRange($from, $to)
    {
        return $this->andWhere(['between', $this->tableName . '.document_date', $from, $to]);
    }

    /**
     * @param $uid
     * @return $this
     */
    public function byUid($uid)
    {
        return $this->andWhere([
            'uid' => $uid,
        ]);
    }
}
