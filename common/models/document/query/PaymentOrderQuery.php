<?php

namespace common\models\document\query;

use common\models\document\PaymentOrder;
use common\models\ICompanyStrictQuery;
use yii\db\ActiveQuery;
use yii\db\Query;

/**
 * Class PaymentOrderQuery
 *
 * @package common\models\document
 */
class PaymentOrderQuery extends ActiveQuery implements ICompanyStrictQuery
{
    /**
     * @param int $companyId
     * @return Query
     */
    public function byCompany($companyId)
    {
        return $this->andWhere([
            'company_id' => $companyId,
        ]);
    }

    /**
     * @param $from
     * @param $to
     * @return $this
     */
    public function byDocumentDateRange($from, $to)
    {
        return $this->andWhere(['between', PaymentOrder::tableName() . '.document_date', $from, $to]);
    }
}
