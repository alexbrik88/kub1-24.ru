<?php

namespace common\models\document\query;


use common\models\document\Order;
use yii\db\ActiveQuery;

/**
 * Class OrderQuery
 *
 * @package common\models\document
 */
class OrderQuery extends ActiveQuery
{
    /**
     * @param $invoiceId
     *
     * @return OrderQuery
     */
    public function byInvoice($invoiceId)
    {
        return $this->andWhere([
            Order::tableName() . '.invoice_id' => $invoiceId,
        ]);
    }

    /**
     * @return OrderQuery
     */
    public function orderNumber()
    {
        return $this->orderBy(Order::tableName() . '.number ASC');
    }
}
