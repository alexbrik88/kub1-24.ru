<?php

namespace common\models\document\query;


use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;

/**
 * Class InvoiceQuery
 *
 * @package common\models\document
 */
class InvoiceAutoQuery extends DocumentActiveQuery
{
    protected $autoinvoice = false;

    /**
     * Constructor.
     * @param string|ActiveRecord $modelClass the model class associated with this query
     * @param array $config configurations to be applied to the newly created query object
     */
    public function __construct($modelClass, $config = [])
    {
        parent::__construct($modelClass, $config);

        $this->andWhere([
            $this->tableName . '.invoice_status_id' => InvoiceStatus::STATUS_AUTOINVOICE,
        ]);
    }

    /**
     *
     * @param $companyId
     * @return InvoiceQuery
     */
    public function byCompany($companyId)
    {
        return $this->andWhere([
            Invoice::tableName() . '.company_id' => $companyId,
        ]);
    }

    /**
     *
     * @param int $contractorId
     * @return InvoiceQuery
     */
    public function byContractorId($contractorId)
    {
        return $this->andWhere([
            Invoice::tableName() . '.contractor_id' => $contractorId,
        ]);
    }

    /**
     * @param bool|int $isDeleted
     * @return static
     */
    public function byDeleted($isDeleted = false)
    {
        return $this->andWhere([
            Invoice::tableName() . '.is_deleted' => (int) $isDeleted,
        ]);
    }
}
