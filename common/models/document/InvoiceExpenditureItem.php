<?php

namespace common\models\document;

use common\models\Company;
use common\models\cash\CashBankFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashOrderFlows;
use common\models\Contractor;
use common\models\ExpenseItemFlowOfFunds;
use common\models\TaxKbk;
use frontend\models\Documents;
use frontend\modules\reports\models\PlanCashFlows;
use frontend\modules\reports\models\ProfitAndLossCompanyItem;
use Yii;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * This is the model class for table "invoice_expenditure_item".
 *
 * @property int $id
 * @property int $parent_id
 * @property int $group_id
 * @property string $name
 * @property int $company_id
 * @property int $sort
 * @property bool $is_visible
 * @property bool $is_prepayment
 * @property bool $can_be_controlled
 *
 * @property ExpenseItemFlowOfFunds[] $expenseItemsFlowOfFunds
 * @property InvoiceExpenditureGroup[] $itemGroup
 * @property ProfitAndLossCompanyItem[] $profitAndLossCompanyItems
 * @property ProfitAndLossCompanyItem $profitAndLossCompanyItem
 * @property InvoiceExpenditureItem $parent
 * @property InvoiceExpenditureItem[] $children
 * @property string $fullName
 */
class InvoiceExpenditureItem extends ActiveRecord implements InvoiceReasonInterface
{
    const NAME_MAX_LENGTH = 45;

    const ITEM_PRODUCT = 1;
    const ITEM_LEASE = 2;
    const ITEM_SALARY = 3;
    const ITEM_SALARY_TAX = 4;
    const ITEM_TAX = 5;
    const ITEM_INTERNET = 6;
    const ITEM_TELEPHONY = 7;
    const ITEM_HOUSEHOLD_EXPENSES = 8;
    const ITEM_ADVERTISING = 9;
    const ITEM_FURNITURE = 10;
    const ITEM_OFFICE_EQUIPMENT = 11;

    const ITEM_OTHER = 12;
    const ITEM_CONTRACTOR_PAYMENT = 13;
    const ITEM_RETURN = 14;
    const ITEM_DIVIDEND_PAYMENT = 15;
    const ITEM_PAYMENT_ACCOUNTABLE_PERSONS = 16;
    const ITEM_BANK_COMMISSION = 17;

    const ITEM_LOAN_REPAYMENT = 18;

    const ITEM_SERVICE = 19;
    const ITEM_NDS = 20;
    const ITEM_TAX_AND_PROFIT = 21;
    const ITEM_IT = 22;
    const ITEM_PROGRAMS = 23;
    const ITEM_DELIVERY = 24;
    const ITEM_BOOKKEEPING = 25;
    const ITEM_LAWYER_SERVICE = 26;
    const ITEM_USN_TAX = 27;
    const ITEM_INSURANCE_PAYMENT_IP = 28;
    const ITEM_NDFL_DIVIDENDS = 29;
    const ITEM_PROJECT_1 = 30;
    const ITEM_PROJECT_2 = 31;
    const ITEM_PROJECT_3 = 32;

    const ITEM_REPAYMENT_CREDIT = 37;
    const ITEM_PAYMENT_PERCENT = 38;
    const ITEM_RETURN_FOUNDER = 39;
    const ITEM_OWN_FOUNDS = 44;
    const ITEM_BORROWING_EXTRADITION = 45;
    const ITEM_OMS_FIXED_PAYMENT = 46;

    const ITEM_TAX_USN_15 = 47;
    const ITEM_TAX_USN_6 = 48;
    const ITEM_TAX_PATENT = 49;
    const ITEM_TAX_TRADING_FEE = 50;
    const ITEM_TAX_TRANSPORT = 51;
    const ITEM_ENSURE_PAYMENT = 52;
    const ITEM_PFR_FIXED_PAYMENT = 53;
    const ITEM_AGENT_FEE = 54;
    const ITEM_COMMUNAL_EXPENSES = 59;

    const ITEM_ELECTRIC_POWER = 60;
    const ITEM_DEPOSIT = 62;
    const ITEM_STATIONERY = 63;
    const ITEM_LEASING = 64;

    const ITEM_BOUNTY_CONTRACTORS = 69;
    const ITEM_TRAVEL = 72;
    const ITEM_ADVERTISING_PRODUCTION = 73;
    const ITEM_ADVERTISING_PR = 74;
    const ITEM_REPAIR = 75;
    const ITEM_REPAIR_VEHICLE = 76;
    const ITEM_REPAIR_OFFICE = 77;
    const ITEM_RECRUITMENT_AGENCY_SERVICE = 78;
    const ITEM_EDUCATION = 79;
    const ITEM_BOUNTY_EMPLOYERS = 81;

    const ITEM_INSURANCE = 84;
    const ITEM_INSURANCE_VEHICLE = 85;
    const ITEM_INSURANCE_HEALTH = 86;
    const ITEM_CHOKY_SERVICE = 87;
    const ITEM_FUEL = 88;

    const ITEM_OUTPUT_MONEY = 89;
    const ITEM_OUTPUT_PROFIT = 90;
    const ITEM_CURRENCY_CONVERSION = 91;

    const ITEM_GOODS_CANCELLATION = 92;

    /** @var int Лизинг */
    public const ITEM_LEASING_PAYMENT = 93;

    /** @var int Авансовый платеж */
    public const ITEM_LEASING_PREPAID = 94;

    /** @var int Выкупной платеж */
    public const ITEM_LEASING_REDEMPTION = 95;

    /** @var int Лизинг комиссия */
    public const ITEM_LEASING_COMMISSION = 96;

    /** @var int Страховая премия */
    public const ITEM_LEASING_INSURANCE = 97;

    /** @var int Себестоимость товара */
    public const ITEM_PRIME_COST_PRODUCT = 98;

    /** @var int Себестоимость услуг */
    public const ITEM_PRIME_COST_SERVICE = 99;

    /** @var int Приобретение Основных средств */
    public const ITEM_BALANCE_ARTICLE_FIXED = 71;

    /** @var int Приобретение НМА */
    public const ITEM_BALANCE_ARTICLE_INTANGIBLE = 101;

    /** @var int[] */
    public const LEASING_ITEMS = [
        self::ITEM_LEASING_PAYMENT,
        self::ITEM_LEASING_PREPAID,
        self::ITEM_LEASING_REDEMPTION,
        self::ITEM_BANK_COMMISSION,
        self::ITEM_LEASING_INSURANCE,
    ];

    public static $hiddenAlwaysArticles = [
        self::ITEM_GOODS_CANCELLATION
    ];

    public $sort_index;

    public static $expencesPattern = [
        '/(возврат|погашение).+\bза(е|ё|й)м(а|у)?/ui' => self::ITEM_LOAN_REPAYMENT,
        '/возврат/ui' => self::ITEM_RETURN,
    ];

    public static $accountsPattern = [
        '/^(70601810|47423810|47422810)/' => InvoiceExpenditureItem::ITEM_BANK_COMMISSION,
    ];

    public static $internalItems = [
        self::ITEM_OWN_FOUNDS,
        self::ITEM_CURRENCY_CONVERSION,
    ];

    protected static $_taxIds;
    protected static $_data = [];

    /**
     * @return array
     */
    public static function taxIds()
    {
        if (!isset(self::$_taxIds)) {
            self::$_taxIds = self::find()->select('id')->where([
                'group_id' => InvoiceExpenditureGroup::TAXES,
            ])->column();
        }

        return self::$_taxIds;
    }

    /**
     * @param string $subject
     * @param integer $default
     * @return integer
     */
    public static function itemIdByReturn($subject, $default = null)
    {
        if (mb_stripos($subject, 'возврат') !== false) {
            if (mb_stripos($subject, 'займ') !== false) {
                return self::ITEM_LOAN_REPAYMENT;
            } else {
                return self::ITEM_RETURN;
            }
        }

        return $default;
    }

    /**
     * @param string $subject
     * @param integer $default
     * @return integer
     */
    public static function itemIdByPurpose($subject, $default = null)
    {
        foreach (self::$expencesPattern as $pattern => $id) {
            if (preg_match($pattern, $subject) == 1) {
                return $id;
            }
        }

        return $default;
    }

    /**
     * @param string $subject
     * @param integer $default
     * @return integer
     */
    public static function itemIdByAccount($subject, $default = null)
    {
        foreach (self::$accountsPattern as $pattern => $id) {
            if (preg_match($pattern, $subject) == 1) {
                return $id;
            }
        }

        return $default;
    }

    /**
     * @inheritDoc
     */
    public function getReasonName(): string
    {
        return $this->name;
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'invoice_expenditure_item';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'trim'],
            [['name'], 'filter', 'filter' => function ($value) {
                return mb_strtoupper(mb_substr($value, 0, 1)) . mb_substr($value, 1);
            }],
            [['is_visible', 'can_be_controlled', 'is_prepayment'], 'boolean'],
            [['name'], 'required'],
            [['name'], 'string', 'max' => self::NAME_MAX_LENGTH],
            [['name'], 'nameValidator'],
            [['parent_id'], 'exist', 'skipOnError' => true, 'skipOnEmpty' => true, 'targetClass' => self::class, 'targetAttribute' => ['parent_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function nameValidator($attribute, $params)
    {
        $query = self::find()->where([
            'and',
            ['name' => $this->name],
            ['not', ['id' => $this->id]],
            [
                'or',
                ['company_id' => null],
                ['company_id' => $this->company_id],
            ],
        ]);

        if ($query->exists()) {
            $this->addError($attribute, 'Такая статья расхода уже существует');
        }
    }

    /**
     * @inheritdoc
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                if (self::find()->max('id') < 100) {
                    $this->id = 100;
                }

                if ($this->company_id !== null) {
                    $this->is_visible = true;
                    $this->is_prepayment = true;
                    $this->can_be_controlled = true;
                }

                $this->sort = 100;
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    public function getContractors()
    {
        return $this->hasMany(Contractor::className(), ['invoice_expenditure_item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBankFlows()
    {
        return $this->hasMany(CashBankFlows::className(), ['expenditure_item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmoneyFlows()
    {
        return $this->hasMany(CashEmoneyFlows::className(), ['expenditure_item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlanCashFlows()
    {
        return $this->hasMany(PlanCashFlows::className(), ['expenditure_item_id' => 'id']);
    }

    public function getProfitAndLossCompanyItems()
    {
        return $this->hasMany(ProfitAndLossCompanyItem::className(), ['item_id' => 'id']);
    }

    public function getProfitAndLossCompanyItem()
    {
        return $this->hasOne(ProfitAndLossCompanyItem::className(), ['item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderFlows()
    {
        return $this->hasMany(CashOrderFlows::className(), ['expenditure_item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoices()
    {
        return $this->hasMany(Invoice::className(), ['invoice_expenditure_item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItemGroup()
    {
        return $this->hasOne(InvoiceExpenditureGroup::className(), ['id' => 'group_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getExpenseItemsFlowOfFunds()
    {
        return $this->hasMany(ExpenseItemFlowOfFunds::className(), ['expense_item_id' => 'id']);
    }

    public function getFlowOfFundsItemByCompany($companyId)
    {
        return $this->hasOne(ExpenseItemFlowOfFunds::className(), ['expense_item_id' => 'id'])
            ->onCondition(['company_id' => $companyId]);
    }

    public function getParent()
    {
        return $this->hasOne(InvoiceExpenditureItem::class, ['id' => 'parent_id']);
    }

    public function getChildren($companyId = null)
    {
        if (empty($companyId))
            $companyId = \Yii::$app->user->identity->currentEmployeeCompany->company_id;

        return $this->hasMany(InvoiceExpenditureItem::class, ['parent_id' => 'id'])
            ->onCondition(['company_id' => $companyId]);
    }

    public function getFullName()
    {
        return ($this->parent) ? ($this->parent->name . ' - ' . $this->name) : $this->name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTaxKbk()
    {
        return $this->hasMany(TaxKbk::className(), ['expenditure_item_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function findTaxItems()
    {
        return self::find()->andWhere([
            'group_id' => InvoiceExpenditureGroup::TAXES,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function getListGroup()
    {
        switch ($this->group_id) {
            case InvoiceExpenditureGroup::TAXES:
                return 'tax';
            case InvoiceExpenditureGroup::RENT:
                return 'rent';
            case InvoiceExpenditureGroup::ADVERTISING:
                return 'advertising';
            case InvoiceExpenditureGroup::EMPLOYEE:
                return 'employee';
            default:
                return $this->sort_index < 0 ? 'often' : 'rest';
        }
    }

    /**
     * @inheritdoc
     */
    public function getCanDelete()
    {
        $q1 = $this->getBankFlows()->select('id');
        $q2 = $this->getEmoneyFlows()->select('id');
        $q3 = $this->getOrderFlows()->select('id');
        $q4 = $this->getPlanCashFlows()->select('id');
        $q5 = $this->getInvoices()->select('id');
        $q6 = $this->getContractors()->select('id');

        return !$q1->union($q2)->union($q3)->union($q4)->union($q5)->union($q6)->exists();
    }

    /**
     * @inheritdoc
     */
    public function getCanToggleVisibility()
    {
        $company = Yii::$app->user->identity->currentEmployeeCompany->company;

        return (
            !$this->getBankFlows()->andWhere(['company_id' => $company->id])->exists()
            && !$this->getEmoneyFlows()->andWhere(['company_id' => $company->id])->exists()
            && !$this->getOrderFlows()->andWhere(['company_id' => $company->id])->exists()
            && !$this->getPlanCashFlows()->andWhere(['company_id' => $company->id])->exists()
            //&& !$this->getContractors()->andWhere(['company_id' => $company->id])->exists()
            //&& !$this->getInvoices()->andWhere(['company_id' => $company->id, 'is_deleted' => 0])->exists()
        );
    }

    /**
     * @inheritdoc
     */
    public static function getSorted($company_id, $exclude = [])
    {
        $key = Json::encode([$company_id, $exclude]);
        if (!isset(self::$_data['getSorted'][$key])) {
            $companyId = $company_id ?? ArrayHelper::getValue(Yii::$app, ['user', 'identity', 'company', 'id']);
            $subQuery = Invoice::find()->select([
                'item_id' => 'invoice_expenditure_item_id',
                'use_count' => '-COUNT([[invoice_expenditure_item_id]])',
            ])
                ->where([
                    'type' => Documents::IO_TYPE_IN,
                    'company_id' => $companyId,
                ])
                ->andWhere(['>=', 'created_at', time() - (90 * 24 * 60 * 60),])
                ->groupBy('[[item_id]]')
                ->having('[[use_count]] < -1')
                ->orderBy(['use_count' => SORT_ASC])
                ->limit(3);

            $query = self::find()->alias('items')->addSelect([
                'items.*',
                'sort_index' => 'IF({{sort}}.[[use_count]] is null, IFNULL({{items}}.[[sort]], 100), {{sort}}.[[use_count]])',
            ])
                ->leftJoin(['sort' => $subQuery], '{{sort}}.[[item_id]] = {{items}}.[[id]]')
                ->leftJoin([
                    'ff' => 'expense_item_flow_of_funds',
                ], '{{ff}}.[[expense_item_id]] = {{items}}.[[id]] AND {{ff}}.[[company_id]] = :company', [
                    ':company' => $company_id,
                ])
                ->andWhere([
                    'OR',
                    ['ff.is_visible' => null],
                    ['ff.is_visible' => 1],
                ],)
                ->andWhere([
                    'or',
                    ['items.company_id' => null],
                    ['items.company_id' => $companyId],
                ])
                ->orderBy([
                    'sort_index' => SORT_ASC,
                    'items.name' => SORT_ASC,
                ])
                ->indexBy('id');

            if ($group = ArrayHelper::getValue($exclude, 'group')) {
                $query->andWhere([
                    'or',
                    ['items.group_id' => null],
                    ['not', ['items.group_id' => $group]]
                ]);
            }
            if ($items = ArrayHelper::getValue($exclude, 'items')) {
                $query->andWhere(['not', ['items.id' => $items]]);
            }

            $query->andFilterWhere(['not', ['items.id' => self::$hiddenAlwaysArticles]]);

            self::$_data['getSorted'][$key] = $query->with('parent')->all();
        }

        return self::$_data['getSorted'][$key];
    }

    /**
     * @inheritdoc
     */
    public static function getList($company_id, $exclude = [])
    {
        $key = Json::encode([$company_id, $exclude]);
        if (!isset(self::$_data['getList'][$key])) {
            $list = [];
            $_tmpParent = $_tmpChildren = [];

            /** @var InvoiceExpenditureItem $item */
            foreach (self::getSorted($company_id, $exclude) as $item) {
                if (!$item->parent_id) {
                    $_tmpParent[$item->listGroup][$item->id] = [
                        'id' => $item->id,
                        'parentId' => $item->parent_id,
                        'name' => $item->name,
                        'editable' => $item->company_id == $company_id,
                        'canDelete' => ($item->company_id == $company_id && $item->getCanDelete()) ? 1 : 0,
                    ];
                } else {
                    $_tmpChildren[$item->parent->id][$item->id] = [
                        'id' => $item->id,
                        'parentId' => $item->parent_id,
                        'name' => $item->parent->name .chr(hexdec("1f")). $item->name,
                        'editable' => $item->company_id == $company_id,
                        'canDelete' => ($item->company_id == $company_id && $item->getCanDelete()) ? 1 : 0,
                    ];
                }
            }

            foreach ($_tmpParent as $listGroup => $parentsList) {
                foreach ($parentsList as $parentId => $parent) {
                    $list[$listGroup][] = $parent;
                    if (isset($_tmpChildren[$parentId])) {
                        foreach ($_tmpChildren[$parentId] as $child)
                            $list[$listGroup][] = $child;
                    }
                }
            }

            self::$_data['getList'][$key] = $list;
        }

        return self::$_data['getList'][$key];
    }

    /**
     * @inheritdoc
     */
    public static function getSelect2Data($company_id, $exclude = [], $can_add = false)
    {
        $data = static::getList($company_id, $exclude);
        $items = [];
        $options = [];


        foreach ($data as $group => $itemList) {
            foreach ((array)$itemList as $item) {
                $items[$group][$item['id']] = $item['name'];
                $options[$item['id']] = [
                    'data-editable' => $item['editable'] ? 1 : 0,
                    'data-can-delete' => $item['canDelete'] ? 1 : 0,
                    'data-child' => $item['parentId'] ? 1 : 0
                ];
            }
        }
        $list = array_flip(ArrayHelper::remove($items, 'often', []));
        if ($list) {
            $list['separator'] = '';
        }

        $rest = ArrayHelper::remove($items, 'rest', []);
        $tax = ArrayHelper::remove($items, 'tax');
        $itemsByGroup = [
            InvoiceExpenditureGroup::RENT_NAME => ArrayHelper::remove($items, 'rent', []),
            InvoiceExpenditureGroup::ADVERTISING_NAME => ArrayHelper::remove($items, 'advertising', []),
            InvoiceExpenditureGroup::EMPLOYEE_NAME => ArrayHelper::remove($items, 'employee', []),
        ];
        foreach ($rest as $key => $value) {
            if ($key != self::ITEM_CONTRACTOR_PAYMENT &&
                $tax !== null &&
                strnatcasecmp(InvoiceExpenditureGroup::TAXES_NAME, $value) < 0
            ) {
                $list[InvoiceExpenditureGroup::TAXES_NAME] = $tax;
                $tax = null;
            }

            $list[$value] = $key;
        }

        foreach ($itemsByGroup as $groupName => $items) {
            if (count($items) === 0) {
                continue;
            }

            foreach ($items as $id => $item) {
                if (count($items) === 1) {
                    $list[$item] = $id;
                    break;
                }

                $list[$groupName][$id] = $item;
            }
        }

        ksort($list);

        $formattedList = [];
        foreach ($list as $key => $item) {
            if (is_array($item)) {
                $formattedList[$key] = $item;
                continue;
            }

            $formattedList[$item] = $key;
        }

        if ($can_add) {
            $formattedList =
                ['add-modal-expenditure-item' => \frontend\themes\kub\helpers\Icon::get('add-icon', ['class' => 'add-button-icon']) . ' Добавить статью расхода'] +
                $formattedList;
            $options['add-modal-expenditure-item'] = [
                'data-editable' => false,
                'data-can-delete' => false
            ];
        }

        if (array_key_exists('separator', $formattedList)) {
            $options['separator'] = [
                'disabled' => true,
                'data-separator' => 'separator',
            ];
        }

        return ['list' => $formattedList, 'options' => $options];
    }
}
