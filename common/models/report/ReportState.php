<?php

namespace common\models\report;

use common\models\company\CompanyType;
use common\models\service\ServiceInvoiceTariff;
use common\models\service\SubscribeTariff;
use Yii;
use common\models\Company;
use common\models\service\Subscribe;

/**
 * This is the model class for table "report_state".
 *
 * @property integer $id
 * @property integer $company_id
 * @property integer $subscribe_id
 * @property integer $tariff_id
 * @property integer $invoice_tariff_id
 * @property string $pay_date
 * @property string $pay_sum
 * @property integer $invoice_count
 * @property integer $act_count
 * @property integer $packing_list_count
 * @property integer $invoice_facture_count
 * @property integer $upd_count
 * @property integer $has_logo
 * @property integer $has_print
 * @property integer $has_signature
 * @property integer $product_count
 * @property integer $service_count
 * @property integer $download_statement_count
 * @property integer $download_1c_count
 * @property integer $days_without_payment
 * @property integer $sum_company_images
 * @property integer $employees_count
 * @property integer $company_type_id
 * @property integer $company_taxation_type_name
 * @property integer $import_xls_product_count
 * @property integer $import_xls_service_count
 * @property string $sum
 *
 * @property Company $company
 * @property Subscribe $subscribe
 * @property CompanyType $companyType
 */
class ReportState extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report_state';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['company_id', 'pay_date', 'pay_sum'], 'required'],
            [['company_id', 'subscribe_id', 'invoice_count', 'act_count', 'packing_list_count',
                'invoice_facture_count', 'upd_count', 'product_count', 'service_count',
                'download_statement_count', 'download_1c_count', 'pay_date', 'days_without_payment',
                'sum_company_images', 'employees_count', 'company_type_id', 'import_xls_product_count',
                'import_xls_service_count'], 'integer'],
            [['pay_sum', 'company_taxation_type_name', 'sum'], 'string', 'max' => 255],
            [['has_logo', 'has_print', 'has_signature',], 'boolean'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['subscribe_id'], 'exist', 'skipOnError' => true, 'targetClass' => Subscribe::className(), 'targetAttribute' => ['subscribe_id' => 'id']],
            [['company_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => CompanyType::className(), 'targetAttribute' => ['company_type_id' => 'id']],
            [['tariff_id'], 'exist', 'skipOnError' => true, 'targetClass' => SubscribeTariff::className(), 'targetAttribute' => ['tariff_id' => 'id']],
            [['invoice_tariff_id'], 'exist', 'skipOnError' => true, 'targetClass' => ServiceInvoiceTariff::className(), 'targetAttribute' => ['invoice_tariff_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'subscribe_id' => 'Subscribe ID',
            'tariff_id' => 'Tariff ID',
            'invoice_tariff_id' => 'Invoice tariff ID',
            'pay_date' => 'Дата оплаты',
            'pay_sum' => 'Сумма оплаты',
            'invoice_count' => 'Счет',
            'act_count' => 'Акт',
            'packing_list_count' => 'ТН',
            'invoice_facture_count' => 'СФ',
            'upd_count' => 'УПД',
            'has_logo' => 'Логотип',
            'has_print' => 'Печать',
            'has_signature' => 'Подпись',
            'product_count' => 'Товар',
            'service_count' => 'Услуги',
            'download_statement_count' => 'Загрузка выписки',
            'download_1c_count' => 'Выгрузка 1С',
            'import_xls_product_count' => 'Загружено товаров из Excel',
            'import_xls_service_count' => 'Загружено услуг из Excel',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompanyType()
    {
        return $this->hasOne(CompanyType::className(), ['id' => 'company_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSubscribe()
    {
        return $this->hasOne(Subscribe::className(), ['id' => 'subscribe_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTariff()
    {
        return $this->hasOne(SubscribeTariff::className(), ['id' => 'tariff_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceTariff()
    {
        return $this->hasOne(ServiceInvoiceTariff::className(), ['id' => 'invoice_tariff_id']);
    }
}
