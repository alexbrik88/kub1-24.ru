<?php

namespace common\models\report;

use common\models\Company;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\helpers\FileHelper;
use yii\web\UploadedFile;

/**
 * This is the model class for table "report".
 *
 * @property integer $id
 * @property integer $company_id
 * @property string $created_at
 * @property string $author_id
 * @property string $title
 * @property string $description
 *
 * @property Company $company
 * @property ReportFile[] $reportFiles
 */
class Report extends \yii\db\ActiveRecord
{
    const SCENARIO_CREATE = 'create';

    public $maxFiles = 3;
    public $uploadedFile;
    public $has_file;

    public static $extensions = 'jpeg, jpg, png, gif, bmp, pdf, doc, docx, xlsx, txt, xls';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report';
    }

    /**
     * @return ReportQuery
     */
    public static function find()
    {
        return new ReportQuery(get_called_class());
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'updatedAtAttribute' => false,
            ],
        ];
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        return array_merge(parent::scenarios(), [
            self::SCENARIO_CREATE => [],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [
                ['has_file'], 'required',
                'when' => function ($model) {
                    return !$model->getReportFiles()->exists();
                },
                'whenClient' => "function () {
                    return !$('#report-file-list-{$this->id} .file').length;
                }",
                'message' => 'Необходимо загрузить хотя бы один документ',
            ],
            [['description'], 'string'],
            [['title'], 'string', 'max' => 255],
            [
                ['uploadedFile'], 'file',
                'skipOnEmpty' => true,
                'extensions' => self::$extensions,
                'maxSize' => 3*1024*1024,
                'maxFiles' => $this->maxFiles,
                'tooMany' => 'Вы не можете загружать более ' . $this->maxFiles . ' файлов.'
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Компания',
            'created_at' => 'Дата создания',
            'author_id' => 'Автор',
            'title' => 'Название',
            'description' => 'Описание',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::className(), ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReportFiles()
    {
        return $this->hasMany(ReportFile::className(), ['report_id' => 'id']);
    }

    /**
     * @param bool $insert
     *
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($insert) {
                $this->company_id = Yii::$app->user->identity->company->id;
                $this->author_id = Yii::$app->user->id;
            }

            if ($this->scenario !== self::SCENARIO_CREATE && $this->is_created != 1) {
                $this->is_created = 1;
                $this->created_at = time();
            }

            return true;
        }

        return false;
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            foreach ($this->getReportFiles()->all() as $file) {
                $file->delete();
            }
            $uploadPath = $this->getUploadPath();
            if (file_exists($uploadPath)) {
                FileHelper::removeDirectory($uploadPath);
            }

            return true;
        } else {
            return false;
        }
    }

    /**
     * @param bool $validate
     *
     * @return bool
     */
    public function upload($validate = true)
    {
        $saved = true;
        if (!empty($this->uploadedFile) && is_array($this->uploadedFile) && (!$validate || $this->validate('uploadedFile'))) {
            foreach ($this->uploadedFile as $file) {
                if ($file instanceof UploadedFile) {
                    $reportFile = new ReportFile();
                    $reportFile->report_id = $this->id;
                    $reportFile->file_name = $file->name;
                    $reportFile->save();
                    $uploadPath = $reportFile->getUploadPath(true);
                    if (!file_exists($uploadPath)) {
                        FileHelper::createDirectory($uploadPath);
                    }
                    $saved = ($file->saveAs($uploadPath . DIRECTORY_SEPARATOR . $reportFile->id) && $saved);
                } else {
                    return false;
                }
            }

            return $saved;
        }
        return false;
    }

    /**
     * @param bool $absolutePath
     * @return string
     * @throws Exception
     */
    public function getUploadPath($absolutePath = true)
    {
        if ($absolutePath) {
            $path = Company::fileUploadPath($this->company_id);
        } else {
            $path = DIRECTORY_SEPARATOR . Company::fileUploadDir($this->company_id);
        }
        $path .= DIRECTORY_SEPARATOR . $this->tableName() . DIRECTORY_SEPARATOR . $this->id;

        return $path;
    }
}
