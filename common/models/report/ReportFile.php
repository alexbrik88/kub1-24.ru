<?php

namespace common\models\report;

use common\models\Company;
use Yii;
use yii\base\Exception;
use yii\helpers\FileHelper;

/**
 * This is the model class for table "report_file".
 *
 * @property integer $id
 * @property string $file_name
 * @property integer $report_id
 *
 * @property Report $report
 */
class ReportFile extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'report_file';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['report_id'], 'integer'],
            [['file_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'file_name' => 'File Name',
            'report_id' => 'Report ID',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public static function getUploadDir()
    {
        return Yii::getAlias('@common') . DIRECTORY_SEPARATOR . 'uploads' . DIRECTORY_SEPARATOR . self::tableName();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getReport()
    {
        return $this->hasOne(Report::className(), ['id' => 'report_id']);
    }

    /**
     * @param bool $absolutePath
     * @return string
     * @throws Exception
     */
    public function getUploadPath($absolutePath = true)
    {
        return $this->report->getUploadPath($absolutePath);
    }

    /**
     * @param bool $absolutePath
     * @return string
     * @throws Exception
     */
    public function getFilePath($absolutePath = true)
    {
        return $this->getUploadPath($absolutePath) . DIRECTORY_SEPARATOR . $this->id;
    }

    /**
     * @return bool
     */
    public function beforeDelete()
    {
        if (parent::beforeDelete()) {
            $file = $this->getFilePath();
            if (is_file($file)) {
                unlink($file);
            }

            return true;
        } else {
            return false;
        }
    }
}
