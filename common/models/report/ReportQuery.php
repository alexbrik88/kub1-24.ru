<?php

namespace common\models\report;


use common\models\ICompanyStrictQuery;
use yii\db\ActiveQuery;

/**
 * Class ReportQuery
 *
 * @package common\models\document
 */
class ReportQuery extends ActiveQuery implements ICompanyStrictQuery
{
    /**
     * @param $companyId
     *
     * @return ReportQuery
     */
    public function byCompany($companyId)
    {
        return $this->andWhere([
            Report::tableName() . '.company_id' => $companyId,
        ]);
    }
}
