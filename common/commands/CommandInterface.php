<?php

namespace common\commands;

use yii\base\Configurable;

interface CommandInterface extends Configurable
{
    /**
     * @return void
     */
    public function run(): void;
}
