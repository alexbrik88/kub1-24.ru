<?php

namespace common\behaviors;

use yii\base\Behavior;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;

/**
 * @property-read ActiveRecord $owner
 */
class JsonAttributesBehavior extends Behavior
{
    /**
     * @var string[]
     */
    public $attributes = [];

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (empty($this->attributes)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @inheritDoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => [$this, 'encodeAttributes'],
            ActiveRecord::EVENT_BEFORE_UPDATE => [$this, 'encodeAttributes'],
            ActiveRecord::EVENT_AFTER_FIND => [$this, 'decodeAttributes'],
            // ActiveRecord::EVENT_AFTER_REFRESH => [$this, 'decodeAttributes'],
            ActiveRecord::EVENT_AFTER_INSERT => [$this, 'decodeAttributes'],
            ActiveRecord::EVENT_AFTER_UPDATE => [$this, 'decodeAttributes'],
        ];
    }

    /**
     * @return void
     */
    public function encodeAttributes(): void
    {
        array_walk($this->attributes, function (string $attribute): void {
            $data = $this->owner->getAttribute($attribute);

            if ($data !== null) {
                $value = json_encode($data, JSON_UNESCAPED_UNICODE);
                $this->owner->setAttribute($attribute, $value);
            }
        });
    }

    /**
     * @return void
     */
    public function decodeAttributes(): void
    {
        array_walk($this->attributes, function (string $attribute): void {
            $value = $this->owner->getAttribute($attribute);

            if ($value !== null) {
                $data = json_decode($value, true);
                $this->owner->setAttribute($attribute, $data);
                $this->owner->setOldAttribute($attribute, $data);
            }
        });
    }
}
