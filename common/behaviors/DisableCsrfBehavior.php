<?php

namespace common\behaviors;

use yii\base\ActionEvent;
use yii\base\Behavior;
use yii\base\InvalidConfigException;
use yii\web\Controller;

/**
 * @property Controller $owner
 */
class DisableCsrfBehavior extends Behavior
{
    /**
     * @var string[]
     */
    public $actions = [];

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (empty($this->actions)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @inheritDoc
     */
    public function events()
    {
        return [
            Controller::EVENT_BEFORE_ACTION => [$this, 'beforeAction'],
        ];
    }

    /**
     * @param ActionEvent $event
     * @return void
     */
    public function beforeAction(ActionEvent $event): void
    {
        if (in_array($event->name, $this->actions)) {
            $this->owner->enableCsrfValidation = false;
        }
    }
}
