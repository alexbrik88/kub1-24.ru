<?php

namespace common\behaviors;

use Yii;
use yii\base\Behavior;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;

/**
 * @property-read ActiveRecord $owner
 */
class PasswordAttributeBehavior extends Behavior
{
    /**
     * @var string
     */
    public $attribute;

    /**
     * @var string
     */
    public $encryptPassword;

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (empty($this->attribute) || empty($this->encryptPassword)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @inheritDoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => [$this, 'encryptAttribute'],
            ActiveRecord::EVENT_BEFORE_UPDATE => [$this, 'encryptAttribute'],
            ActiveRecord::EVENT_AFTER_FIND => [$this, 'decryptAttribute'],
            ActiveRecord::EVENT_AFTER_INSERT => [$this, 'decryptAttribute'],
            ActiveRecord::EVENT_AFTER_UPDATE => [$this, 'decryptAttribute'],
        ];
    }

    /**
     * @return void
     */
    public function encryptAttribute(): void
    {
        $data = $this->owner->getAttribute($this->attribute);

        if ($data !== null) {
            $value = base64_encode(Yii::$app->security->encryptByPassword($data, $this->encryptPassword));
            $this->owner->setAttribute($this->attribute, $value);
        }
    }

    /**
     * @return void
     */
    public function decryptAttribute(): void
    {
        $value = $this->owner->getAttribute($this->attribute);

        if ($value !== null) {
            $data = Yii::$app->security->decryptByPassword(base64_decode($value), $this->encryptPassword);
            $this->owner->setAttribute($this->attribute, $data);
            $this->owner->setOldAttribute($this->attribute, $data);
        }
    }
}
