<?php

/* @var $this yii\web\View */
/* @var $model common\models\retail\RetailPoint */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'systemniye';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
?>

<table cellpadding="20" cellspacing="0" style="width:600px;">
    <tr>
        <td>
            <p style="font-size: 24px; line-height: 21px; color: #000; margin-bottom: 20px;">
                Добро пожаловать в КУБ.РозничнаяТочка!
            </p>
            <p style="margin: 0; font-size: 12pt;  margin-bottom: 20px;">
                Ваши данные для входа в КУБ.РозничнаяТочка:
            </p>
            <p style="margin: 0; margin-bottom: 10px; font-size: 12pt;">
                <?= $model->getTitle() ?>
            </p>
            <p style="margin: 0; font-size: 12pt;">
                Логин: <?= $model->login ?>
            </p>
            <p style="margin: 0; font-size: 12pt;">
                Пароль: <?= $model->new_password ?>
            </p>
            <?php if (!empty($pin)) : ?>
                <p style="margin: 0; font-size: 12pt;">
                    ПИН: <?= $pin ?>
                </p>
            <?php endif ?>
        </td>
    </tr>
</table>

<?= $this->render('@common/mail/layouts/parts/_alexey', ['message' => $message]) ?>

<?= $this->render('@common/mail/layouts/parts/_links') ?>