<?php

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\company\CompanyType;
use yii\helpers\Html;

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'systemniye';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');

?>

<table cellpadding="20" cellspacing="0" style="width:600px;">
    <tr>
        <td>
            <?php if (!empty($emailText)) : ?>
                <p style="margin: 0; font-size: 14px; margin-bottom: 25px;">
                    <?= nl2br($emailText) ?>
                </p>
            <?php else : ?>
                <p style="font-size: 24px; line-height: 21px; color: #000; margin-bottom: 20px;">
                    Здравствуйте!
                </p>
                <?php if (!empty($model) && is_array($model)): ?>
                    <?php foreach ($model as $m): ?>
                        <p style="margin: 0; font-size: 14px; margin-bottom: 5px;">Смета
                            No <?= $m->fullNumber; ?>
                            от <?= DateHelper::format($m->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                        </p>
                    <?php endforeach; ?>
                    <p style="margin: 0;margin-bottom: 40px;"></p>
                <?php elseif (!empty($model)): ?>
                    <p style="margin: 0; font-size: 14px; margin-bottom: 25px;">Смета
                        No <?= $model->fullNumber; ?>
                        от <?= DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                    </p>
                <?php else: ?>
                    <p style="margin: 0; font-size: 14px; margin-bottom: 25px;"><?= $subject ?></p>
                <?php endif; ?>
            <?php endif; ?>
            <?php /* if (!is_array($model)): ?>
                <p style="margin: 0; font-size: 14px; margin-bottom: 25px;">
                    <?= Html::a('Открыть смету', Yii::$app->urlManager->createAbsoluteUrl([
                        '/bill/packing-list/' . $model->uid,
                        'utm_source' => $this->params['utm_source'],
                        'utm_medium' => $this->params['utm_medium'],
                        'utm_campaign' => $this->params['utm_campaign'],
                        'utm_term' => $this->params['utm_term'],
                        'utm_content' => 'packing-list_link',
                    ]), [
                        'style' => 'text-decoration: none;color: #ffffff;padding-top: 8px;background-color: #0077A7; padding-bottom: 8px;padding-left: 16px;padding-right: 16px;',
                    ]); ?>
                </p>
            <?php endif; */ ?>
            <p style="margin: 0; font-size: 14px; margin-bottom: 25px;">
                <?php if (isset($emailSignature) && $emailSignature->text): ?>
                    <?= nl2br($emailSignature->text); ?>
                <?php else: ?>
                    С уважением,<br>
                    <?= empty($employeeCompany) ? $employee->getFio(true) : $employeeCompany->getFio(true); ?>
                    <?php if ($company->company_type_id != CompanyType::TYPE_IP) : ?>
                        <br>
                        <?= empty($employeeCompany) ? $employee->position : $employeeCompany->position; ?>
                    <?php endif; ?>
                <?php endif; ?>
            </p>
        </td>
    </tr>
</table>