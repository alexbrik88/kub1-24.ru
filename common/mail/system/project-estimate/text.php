<?php
use yii\helpers\Html;

?>

<?php

    if (isset($userMessage)):
        echo Html::encode($userMessage);
    endif;
?>

По всем вопросам обращайтесь в службу поддержки <?= $supportEmail ?>

С уважением, Команда КУБ.