<?php

use yii\helpers\Html;

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'systemniye';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$this->params['img_src'] = $img_src ?? 'https://lk.kub-24.ru/img/email/tpl/2.png';

$passwordUrl = 'https://lk.kub-24.ru/profile/index';
?>

<table cellpadding="20" cellspacing="0" style="width:600px;">
    <tr>
        <td>
            <table>
                <tbody>
                    <tr>
                        <td style="vertical-align: top;">1)</td>
                        <td style="padding-bottom: 20px;">
                            <strong>
                                Ваши данные для входа в КУБ24:
                            </strong>
                            <div>
                                <div>
                                    Логин: <?= $login ?>
                                </div>
                                <div>
                                    Пароль: <?= $password ?>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td style="vertical-align: top;">2)</td>
                        <td style="padding-bottom: 20px;">
                            <strong>
                                Чтобы изменить пароль на тот, который вам будет легче запомнить, перейдите по ссылке
                            </strong>
                            <div>
                                <a href="<?= $passwordUrl ?>"><?= $passwordUrl ?></a>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <div style="margin: 0; font-size: 12pt;">
                Меня зовут Алексей, я и наша команда поможем вам разобраться в "ФинДиректору" и расскажем,
                как он будет полезен именно вам.
                <br>
                Вы получили бесплатный доступ на 14 дней ко всему функционалу сервиса.
            </div>
        </td>
    </tr>
</table>

<?= $this->render('@common/mail/layouts/parts/_alexey', ['message' => $message]) ?>
