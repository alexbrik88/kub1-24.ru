<?php
/**
 * @var $message \common\components\sender\unisender\UniSender
 */
$baseUrl = \Yii::$app->params['uniSender']['baseUrl'];
$feedback = \Yii::$app->params['uniSender']['feedback'];
$callUs = \Yii::$app->params['uniSender']['phone'];
$supportEmail = \Yii::$app->params['emailList']['support'];
$passwordUrl = 'https://lk.kub-24.ru/profile/index';
?>
Добро пожаловать в КУБ24.ФинДиректор!

1) Ваши данные для входа в КУБ24:
   Логин: <?= $login ?>
   Пароль: <?= $password ?>

2) Чтобы изменить пароль на тот, который вам будет легче запомнить, перейдите по ссылке:
   <?= $passwordUrl ?>

С уважением,
Алексей Кущенко,
Руководитель КУБ24

ВОЙТИ В КУБ24 <?= $baseUrl ?>/login

Нужна помощь?

Позвоните нам <?= $callUs ?>

Напишите нам <?= $supportEmail ?>

Оставить сообщение на сайте <?= $feedback ?>
