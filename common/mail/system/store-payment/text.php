<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 11/18/15
 * Time: 1:17 PM
 * Email: t.kanstantsin@gmail.com
 */

use php_rutils\RUtils;
use common\models\service\Payment;

/* @var \yii\web\View $this */
/* @var \common\models\document\Invoice $invoice */
/* @var $payment Payment */

$urlManager = clone \Yii::$app->urlManager;
$urlManager->baseUrl = '/';
$period = (int)$payment->sum == (int)$payment->storeTariff->total_amount ?
    'год' : 'месяц';
?>
Здравствуйте <?= $company->chief_firstname; ?>!

Счет на оплату "Кабинета для покупателя" в кол-ве <?= $tariff->cabinets_count; ?> шт., срок 1 <?= $period; ?>.


Ссылка на счет <?= $urlManager->createAbsoluteUrl([
    '/bill/invoice/' . $invoice->uid,
]); ?>

С уважением,
Команда КУБ


Вы получили это письмо, потому что подписывались на новости сервиса КУБ на сайте kub-24.ru.
У Вас есть возможность отписаться от рассылки, для этого необходимо перейти в личный кабинет, в раздел
настройки и убрать галочки в блоке «Получение уведомлений».
