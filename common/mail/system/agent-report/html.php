<?php

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\company\CompanyType;
use common\models\document\OrderWaybill;
use yii\helpers\Html;

/* @var \yii\web\View $this */
/* @var \yii\mail\BaseMessage $message */
/* @var \common\models\document\AgentReport $model */
/* @var \common\models\employee\Employee $employee */
/* @var \common\models\EmployeeCompany $employeeCompany */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'systemniye';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$this->params['companyName'] = $employeeCompany->company->getShortName();
$this->params['pixel'] = !empty($pixel) ? $pixel : null;

$emailSignature = $employeeCompany->company->getEmailSignature(empty($employeeCompany) ? $employee->id : $employeeCompany->employee_id);
?>

<table cellpadding="20" cellspacing="0" style="width:600px;">
    <tr>
        <td>
            <?php if (!empty($emailText)) : ?>
                <p style="margin: 0; font-size: 14px; margin-bottom: 25px;">
                    <?= nl2br($emailText) ?>
                </p>
            <?php else : ?>
                <p style="font-size: 24px; line-height: 21px; color: #000; margin-bottom: 20px;">
                    Здравствуйте!
                </p>
                <?php if (is_array($model)): ?>
                    <?php foreach($model as $m): ?>
                        <p style="margin: 0; font-size: 14px; margin-bottom: 25px;">Агентский отчет за <?= $m->getReadableDate() ?><br/>
                            <?= $m->agent->getShortName() ?>
                        </p>
                    <?php endforeach; ?>
                <?php else: ?>
                    <p style="margin: 0; font-size: 14px; margin-bottom: 25px;">Агентский отчет за <?= $model->getReadableDate() ?><br/>
                        <?= $model->agent->getShortName() ?>
                    </p>
                <?php endif; ?>
            <?php endif; ?>
            <p style="margin: 0; font-size: 14px; margin-bottom: 25px;">
                <?php if (isset($emailSignature) && $emailSignature->text): ?>
                    <?= nl2br($emailSignature->text); ?>
                <?php else: ?>
                    С уважением,<br>
                    <?= empty($employeeCompany) ? $employee->getFio(true) : $employeeCompany->getFio(true); ?>
                    <?php if ($employeeCompany->company->company_type_id != CompanyType::TYPE_IP) : ?>
                        <br>
                        <?= empty($employeeCompany) ? $employee->position : $employeeCompany->position; ?>
                    <?php endif; ?>
                <?php endif; ?>
            </p>
        </td>
    </tr>
</table>