<?php
use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\document\OrderWaybill;

/* @var \yii\web\View $this */
/* @var \common\models\document\AgentReport $model */
/* @var \common\models\employee\Employee $employee */
/* @var \common\models\EmployeeCompany $employeeCompany */

$emailSignature = $employeeCompany->company->getEmailSignature(empty($employeeCompany) ? $employee->id : $employeeCompany->employee_id);
?>
<?=$employeeCompany->company->getShortName(); ?>
<?= str_repeat('=', mb_strlen($employeeCompany->company->getShortName())); ?>


<?php if (!empty($emailText)) : ?>
    <?= $emailText; ?>
<?php else : ?>
    Здравствуйте!

    <?php if (is_array($model)): ?>
        <?php foreach($model as $m): ?>
            Агетнский отчет за <?= $m->getReadableDate() ?>
            <?= $m->agent->getShortName() ?>
            --
        <?php endforeach; ?>
    <?php else: ?>
        Агетнский отчет за <?= $model->getReadableDate() ?>
        <?= $model->agent->getShortName() ?>
    <?php endif; ?>
<?php endif; ?>

--
<?php if (isset($emailSignature) && $emailSignature->text): ?>
    <?= $emailSignature->text; ?>
<?php else: ?>
    С уважением,
    <?= empty($employeeCompany) ? $employee->getFio(true) : $employeeCompany->getFio(true); ?>
<?php endif; ?>

------------------------------------
Это письмо было отправлено через он-лайн сервис для предпринимателей КУБ
Попробовать бесплатно: <?= Yii::$app->params['serviceSite'] . '?utm_source=letter&utm_medium=email&utm_campaign=schet'; ?>