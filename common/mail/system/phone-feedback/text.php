<?php

use common\models\Company;
use common\models\employee\Employee;

/**
 * @var $message \common\components\sender\unisender\UniSender
 * @var $user Employee
 * @var $company Company
 * @var $subject string
 * @var $type integer
 * @var $phone string
 * @var $question string
 */

$baseUrl = \Yii::$app->params['uniSender']['baseUrl'];
$feedback = \Yii::$app->params['uniSender']['feedback'];
$callUs = \Yii::$app->params['uniSender']['phone'];
$supportEmail = \Yii::$app->params['emailList']['support'];
?>
Обратная связь!

Компания: <?= $company->getTitle(true); ?>
ФИО: <?= $user->getFio(true); ?>
E-mail: <?= $user->email; ?>
С какого попапа отправили: <?= Company::$afterRegistrationBlock[$type]; ?>
Телефон: <?= $phone; ?>
Вопрос: <?= $question; ?>
