<?php

use common\models\employee\Employee;
use common\models\Company;
use yii\helpers\Inflector;

/* @var $user Employee
 * @var $company Company
 * @var $subject string
 * @var $type integer
 * @var $phone string
 * @var $question string
 */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'systemniye';
$this->params['utm_term'] = Inflector::slug($subject, '_');
?>
<table cellpadding="20" cellspacing="0" style="width:600px;">
    <tr>
        <td>
            <p style="font-size: 24px; line-height: 21px; color: #000; margin-bottom: 20px;">
                Нужна помощь!
            </p>
            <p style="margin: 0; font-size: 12pt;  margin-bottom: 20px;">
                Компания:
                <span><?= $company->getTitle(true); ?></span>
            </p>
            <p style="margin: 0; font-size: 12pt;">
                ФИО:
                <span><?= $user->getFio(); ?></span>
            </p>
            <p style="margin: 0; font-size: 12pt;">
                E-mail:
                <span><?= $user->email; ?></span>
            </p>
            <p style="margin: 0; font-size: 12pt;">
                С какого попапа отправили:
                <span><?= Company::$afterRegistrationBlock[$type]; ?></span>
            </p>
            <p style="margin: 0; font-size: 12pt;">
                Телефон:
                <span><?= $phone; ?></span>
            </p>
            <p style="margin: 0; font-size: 12pt;">
                Вопрос:
                <span><?= $question; ?></span>
            </p>
        </td>
    </tr>
</table>