<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 10.8.15
 * Time: 16.02
 * Email: t.kanstantsin@gmail.com
 */

use common\components\date\DateHelper;
use common\models\currency\Currency;
use common\models\document\Invoice;
use frontend\models\Documents;
use common\models\product\Product;

/* @var \yii\web\View $this */
/* @var \common\models\product\PriceList $model */
/* @var \common\models\employee\Employee $employee */

$emailSignature = $model->company->getEmailSignature(empty($employeeCompany) ? $employee->id : $employeeCompany->employee_id);
?>
<?= $model->company->getTitle(true); ?>
<?= str_repeat('=', mb_strlen($model->company->getTitle(true))); ?>

<?php if (!empty($emailText)) : ?>
<?= nl2br($emailText) ?>
<?php else : ?>
Здравствуйте!

Высылаю прайс-лист.
<?php endif; ?>

Открыть прайс-лист: <?= Yii::$app->urlManager->createAbsoluteUrl(['/price-list/out-view/' . $model->uid . (isset($sendTo) ? ('?nuid=' . $model->getNotificationUid($sendTo)) : '')]); ?>

--
<?php if (isset($emailSignature) && $emailSignature->text): ?>
    <?= $emailSignature->text; ?>
<?php else: ?>
    С уважением,
    <?= empty($employeeCompany) ? $employee->getFio(true) : $employeeCompany->getFio(true); ?>
<?php endif; ?>


------------------------------------
Это письмо было отправлено через он-лайн сервис для предпринимателей КУБ
Попробовать бесплатно: <?= Yii::$app->params['serviceSite'] . '?utm_source=lk_server&utm_medium=email&utm_campaign=schet'; ?>