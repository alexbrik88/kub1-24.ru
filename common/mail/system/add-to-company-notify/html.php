<?php

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'systemniye';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');

/* @var $user \common\models\employee\Employee */
/* @var $company \common\models\Company */
?>

<table cellpadding="20" cellspacing="0" style="width:100%;">
    <tr>
        <td>
            <p style="font-size: 24px; line-height: 21px; color: #000; margin-bottom: 20px;">
                Новый сотрудник в Вашей компании.
            </p>
            <p style="margin: 0; font-size: 12pt; margin-bottom: 20px;">
                В компанию <?= $company->getTitle(true) ?> добавлен новый сотрудник:
                <br>
                <?= $user->getFio(); ?>
                <?php if ($user->phone) : ?>
                    <br>
                    Тел.: <?= $user->phone; ?>
                <?php endif ?>
                <br>
                Email: <?= $user->email; ?>
            </p>
        </td>
    </tr>
</table>
<?= $this->render('@common/mail/layouts/parts/_contacts') ?>