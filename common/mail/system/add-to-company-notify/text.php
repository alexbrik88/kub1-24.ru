<?php
/* @var $user \common\models\employee\Employee */
/* @var $company \common\models\Company */
?>

Новый сотрудник в Вашей компании.

В компанию <?= $company->getTitle(true) ?> добавлен новый сотрудник:

<?= $user->getFio(); ?>

<?php if ($user->phone) : ?>
Тел.: <?= $user->phone; ?>
<?php endif ?>

Email: <?= $user->email; ?>
