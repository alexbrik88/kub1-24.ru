<?php

/* @var \yii\web\View $this */
/* @var common\models\employee\Employee $user */
/* @var string $confirmUrl */

$greeting = 'Здравствуйте, ' . $user->firstname . '!';
?>
<?= $greeting; ?>
<?= str_repeat('=', mb_strlen($greeting)); ?>


Подтвердите ваш e-mail и Ваши покупатели будут видеть его при получении счетов из сервиса КУБ.

Для подтверждения перейдите по ссылке: <?= $confirmUrl; ?>

