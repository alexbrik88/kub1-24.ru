<?php

use yii\helpers\Html;

/* @var common\models\employee\Employee $user */
/* @var string $confirmUrl */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'systemniye';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
?>

<table cellpadding="20" cellspacing="0" style="width:100%;">
    <tr>
        <td>
            <p style="font-size: 24px; line-height: 21px; color: #000; margin-bottom: 20px;">
                Вам назначен промокод
            </p>
            <p style="margin: 0; font-size: 12pt; margin-bottom: 20px;">
                Здравствуйте, <?= Html::encode($user->firstname) ?>!
            </p>
            <p style="margin: 0; font-size: 12pt; margin-bottom: 20px;">
                Подтвердите ваш e-mail <br/>
                и Ваши покупатели будут видеть его<br/>
                при получении счетов из сервиса КУБ.
            </p>
            <p style="margin: 0; font-size: 12pt;">
                <a href="<?= $confirmUrl ?>" target="_blank" style="color: #2E41BA">
                    Подтвердить e-mail
                </a>
            </p>
        </td>
    </tr>
</table>
<?= $this->render('@common/mail/layouts/parts/_contacts') ?>