<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 13.10.15
 * Time: 12.27
 * Email: t.kanstantsin@gmail.com
 */
use common\models\company\CheckingAccountant;

/* @var \yii\web\View $this */
/* @var \common\models\Company $company */
/* @var common\models\employee\Employee $model */

$checkingAccountant = new CheckingAccountant;
$mainCheckingAccountant = $company->mainCheckingAccountant;
?>
Данная страница создана с помощью сервиса <?= \Yii::$app->name; ?>

<?php echo $company->getTitle(true, true); ?>
<?= str_repeat('=', mb_strlen($company->getTitle(true, true))); ?>


<?= $company->getAttributeLabel('taxation_type_id'); ?>: <?= $company->companyTaxationType->name ?>


Юридический адрес: <?= $company->getAddressLegalFull(); ?>
Фактический адрес: <?= $company->getAddressActualFull(); ?>

<?php if ($company->company_type_id == \common\models\company\CompanyType::TYPE_IP): ?>
<?= $company->getAttributeLabel('egrip'); ?>: <?= $company->egrip; ?>
<?= $company->getAttributeLabel('inn'); ?>: <?= $company->inn; ?>
<?= $company->getAttributeLabel('okved'); ?>: <?= $company->okved; ?>
<?php else: ?>
<?= $company->getAttributeLabel('ogrn'); ?>: <?= $company->ogrn; ?>
<?= $company->getAttributeLabel('inn'); ?>: <?= $company->inn; ?>
<?= $company->getAttributeLabel('kpp'); ?>: <?= $company->kpp; ?>
<?= $company->getAttributeLabel('okved'); ?>: <?= $company->okved; ?>
<?php endif; ?>

<?= $checkingAccountant->getAttributeLabel('rs'); ?>: <?= $mainCheckingAccountant? $mainCheckingAccountant->rs: ''; ?>
<?= $checkingAccountant->getAttributeLabel('bank_name'); ?>: <?= $mainCheckingAccountant? $mainCheckingAccountant->bank_name: ''; ?>
<?= $checkingAccountant->getAttributeLabel('ks'); ?>: <?= $mainCheckingAccountant? $mainCheckingAccountant->ks: ''; ?>
<?= $checkingAccountant->getAttributeLabel('bik'); ?>: <?= $mainCheckingAccountant? $mainCheckingAccountant->bik: ''; ?>

<?php if ($company->company_type_id != \common\models\company\CompanyType::TYPE_IP): ?>
<?= $company->chief_post_name; ?>: <?= $company->getChiefFio(); ?>
Главный бухгалтер: <?= $company->getChiefAccountantFio(); ?>
<?php endif; ?>

<?= $company->getAttributeLabel('phone'); ?>: <?= $company->phone; ?>
