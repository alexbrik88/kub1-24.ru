<?php

use yii\helpers\Html;
use frontend\models\AffiliateProgramForm;

/* @var $subject string
 * @var $affiliateProgramForm AffiliateProgramForm
 * @var $userFIO string
 * @var $email string
 * @var $phone string
 */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'systemniye';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
?>
    <table cellpadding="20" cellspacing="0" style="width:600px;">
        <tr>
            <td>
                <p style="font-size: 12pt; line-height: 21px; color: #000; margin-bottom: 20px;">
                    Заявка на вывод вознаграждения в размере
                    <b><?= $affiliateProgramForm->withdrawal_amount; ?> руб.
                        <?php if ($affiliateProgramForm->type == AffiliateProgramForm::RS_TYPE): ?>
                            на расчетный счет компании
                        <?php else: ?>
                            на счет физического лица
                        <?php endif; ?>
                    </b>
                </p>

                <p style="margin: 0; font-size: 12pt;">
                    Контактное лицо: <?= $userFIO; ?>
                </p>

                <p style="margin: 0; font-size: 12pt;">
                    Email: <?= $email; ?>
                </p>

                <p style="margin: 0; font-size: 12pt;">
                    Тел.: <?= $phone; ?>
                </p>
            </td>
        </tr>
    </table>

<?= $this->render('@common/mail/layouts/parts/_links') ?>