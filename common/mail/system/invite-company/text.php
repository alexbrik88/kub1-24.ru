<?php
use yii\helpers\Html;

?>

<?php

    if (isset($userMessage)):
        echo Html::encode($userMessage);
    else:
?>
    Здравствуйте, <?= Html::encode($user->firstname) ?>!

    Приглашаем вас зарегистрироваться в системе Куб.
<?php
    endif;
?>

Чтобы зарегестрироваться или познкомиться с Куб скопируйте и вставьте URL <?= $inviteUrl; ?> в адресную строку в новом окне браузера

По всем вопросам обращайтесь в службу поддержки <?= $supportEmail ?>

С уважением, Команда КУБ.