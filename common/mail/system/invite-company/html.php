<?php
use yii\helpers\Html;

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'systemniye';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');

?>

<table cellpadding="20" cellspacing="0" style="width:600px;">
    <tr>
        <td>
            <?php if (isset($userMessage)):
            ?>
                <p style="margin: 0; font-size: 12pt; margin-bottom: 20px;">
                    <?= $userMessage; ?>
                </p>
            <?php
                else:
            ?>
            <p style="margin: 0; font-size: 12pt; margin-bottom: 20px;">
                Здравствуйте, <?= Html::encode($user->firstname) ?>!
            </p>
            <p style="margin: 0; font-size: 12pt; margin-bottom: 20px;">
                Приглашаем вас зарегестрироваться в системе Куб.
            </p>
            <?php
                endif;
            ?>
            <p style="margin: 0; font-size: 12pt; margin-bottom: 20px;">
                Для регистрации или чтобы познакомиться с "КУБ" пройдите по ссылке:<br>
                <a href="<?= $inviteUrl ?>"
                    target="_blank"
                    style="color: #2E41BA">
                    <?= $inviteUrl ?></a>
            </p>
            <p style="margin: 0; font-size: 12pt; margin-bottom: 20px;">
                Если ссылка не работает, скопируйте и
                вставьте URL в адресную строку в новом
                окне браузера.
            </p>
            <p style="margin: 0; font-size: 12pt;">
                По всем вопросам обращайтесь в службу
                поддержки <?= Html::a($supportEmail, 'mailto:' . $supportEmail) ?>
            </p>
        </td>
    </tr>
    <tr>
        <td>
            <p style="margin: 0; font-size: 14px; font-weight: bold;">
                С уважением,<br> Команда КУБ
            </p>
        </td>
    </tr>
</table>
