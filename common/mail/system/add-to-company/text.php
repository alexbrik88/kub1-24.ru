<?php
/**
 * @var $message \common\components\sender\unisender\UniSender
 * @var $data array
 * @var $user \common\models\employee\Employee
 */
?>
Регистрация в сервисе КУБ

Вы зарегистрированы в качестве нового сотрудника компании <?= $company->getShortTitle(); ?>".

Ваши данные для входа в систему стались прежними:
Логин: <?= $user->email; ?>

Подключенные компании в вашем профиле:
<?php foreach ($user->companies as $key => $model): ?>
    <?= ++$key . '. ' . $model->getShortTitle(); ?>
<?php endforeach; ?>