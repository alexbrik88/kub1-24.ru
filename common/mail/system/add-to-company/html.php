<?php
/**
 * @var $user \common\models\employee\Employee
 */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'systemniye';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
?>

<table cellpadding="20" cellspacing="0" style="width:100%;">
    <tr>
        <td>
            <p style="font-size: 24px; line-height: 21px; color: #000; margin-bottom: 20px;">
                Регистрация в сервисе КУБ
            </p>
            <p style="margin: 0; font-size: 12pt; margin-bottom: 20px;">
                Вы зарегистрированы в качестве нового сотрудника компании
                <br>
                <?= $company->getShortTitle(); ?>".
            </p>
            <p style="margin: 0; font-size: 12pt;">
                Ваши данные для входа в систему остались прежними:
            </p>

            <p style="margin: 0; font-size: 12pt; margin-bottom: 30px;">
                Логин: <span><?= $user->email; ?></span>
            </p>
            <p style="margin: 0; font-size: 12pt;">
                Подключенные компании в вашем профиле:
            </p>
            <?php foreach ($user->companies as $key => $model) : ?>
                <p style="margin: 0; font-size: 12pt;">
                    <?= ++$key . '. ' . $model->getShortTitle(); ?>
                </p>
            <?php endforeach ?>
        </td>
    </tr>
</table>
<?= $this->render('@common/mail/layouts/parts/_contacts') ?>