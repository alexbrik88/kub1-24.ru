<?php

use yii\helpers\Html;

/* @var \yii\web\View $this */
/* @var string $subject */
/* @var string $code */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'systemniye';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$this->params['img_src'] = $img_src ?? 'https://lk.kub-24.ru/img/email/tpl/3.png';
?>

<table cellpadding="20" cellspacing="0" style="width:100%;">
    <tr>
        <td>
            <p style="font-size: 24px; line-height: 21px; color: #000; margin-bottom: 20px;">
                Подтвердите ваш e-mail.
            </p>
            <p style="margin: 0; font-size: 12pt; margin-bottom: 20px;">
                Код подтверждения: <?= $code ?>
            </p>
        </td>
    </tr>
</table>
