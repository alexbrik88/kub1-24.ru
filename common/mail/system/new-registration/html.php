<?php

use yii\helpers\Html;

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'systemniye';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$this->params['img_src'] = $img_src ?? 'https://lk.kub-24.ru/img/email/tpl/1.png';
?>

<table cellpadding="20" cellspacing="0" style="width:600px;">
    <tr>
        <td>
            <p style="font-size: 24px; line-height: 21px; color: #000; margin-bottom: 20px;">
                Добро пожаловать в КУБ24!
            </p>
            <p style="margin: 0; font-size: 12pt;  margin-bottom: 20px;">
                Ваши данные для входа в КУБ24:
            </p>
            <p style="margin: 0; font-size: 12pt;">
                Логин: <?= $login ?>
            </p>

            <p style="margin: 0; font-size: 12pt;">
                Пароль: <?= $password ?>
            </p>
        </td>
    </tr>
    <tr>
        <td>
            <p style="margin: 0; margin-bottom: 20px; font-size: 12pt;">
                Меня зовут Алексей, я и наша команда поможем вам разобраться в КУБе и расскажем, как он будет полезен именно вам.
                Вы получили бесплатный доступ на 14 дней ко всему функционалу блока "Выставление счетов".
            </p>
            <p style="margin: 0; font-size: 12pt;">
                Вы, как любой предприниматель, должны фокусироваться на оказании качественных услуг,
                производстве замечательных товаров и продаже их клиентам,
                а КУБ24 поможет упростить рутинную работу со счетами, актами и другими документами и финансовыми отчетами.
            </p>
        </td>
    </tr>
</table>

<?php /*
<table cellpadding="20" cellspacing="0" style="width:600px; border-top:1px solid #cccccc; border-bottom:1px solid #cccccc;">
    <tr>
        <td>
            <p style="margin: 0; font-size: 12pt; text-align: center;">
                <a href="https://vimeo.com/217134783" style="color: #4276a5;">
                    Краткое видео с обзором КУБ24
                    <br>
                    <br>
                    <?= Html::img($message->embed(Yii::getAlias('@common/mail/assets/img/video_play.png'))); ?>
                </a>
            </p>
        </td>
    </tr>
</table>
*/ ?>

<?= $this->render('@common/mail/layouts/parts/_alexey', ['message' => $message]) ?>
