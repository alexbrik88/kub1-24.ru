<?php
/**
 * @var $message \common\components\sender\unisender\UniSender
 */
$baseUrl = \Yii::$app->params['uniSender']['baseUrl'];
$feedback = \Yii::$app->params['uniSender']['feedback'];
$callUs = \Yii::$app->params['uniSender']['phone'];
$supportEmail = \Yii::$app->params['emailList']['support'];
?>
Добро пожаловать в КУБ24!

Ваши данные для входа в КУБ24:

Логин: <?= $login ?>
Пароль: <?= $password ?>

ВОЙТИ В КУБ24 <?= $baseUrl ?>/login

Нужна помощь?

Позвоните нам <?= $callUs ?>

Напишите нам <?= $supportEmail ?>

Оставить сообщение на сайте <?= $feedback ?>


Вы получили это письмо, потому что подписывались на новости сервиса КУБ24 на сайте <?= $baseUrl ?>.
У Вас есть возможность отписаться от рассылки, для этого необходимо перейти в личный кабинет <?= $baseUrl ?>, в раздел настройки и убрать галочки в блоке «Получение уведомлений».