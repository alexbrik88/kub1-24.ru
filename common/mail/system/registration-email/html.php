<?php
/**
 * @var $data array
 */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'systemniye';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
?>

<table cellpadding="20" cellspacing="0" style="width:100%;">
    <tr>
        <td>
            <p style="font-size: 24pt; line-height: 21px; color: #000; margin-bottom: 20px;">
                Регистрация в сервисе КУБ
            </p>
            <p style="margin: 0; font-size: 12pt; margin-bottom: 20px;">
                Вы зарегистрированы в качестве нового сотрудника компании
                <?= $data['companyTypeShort'] ?>&nbsp;"<?= $data['companyName'] ?>".
            </p>
            <p style="margin: 0; font-size: 12pt;">
                Ваши данные для входа в систему:</p>

            <p style="margin: 0; font-size: 12pt;">
                Логин: <span><?= $data['login'] ?></span>
            </p>

            <p style="margin: 0; font-size: 12pt;">
                Пароль: <span><?= $data['password'] ?></span>
            </p>
        </td>
    </tr>
    <tr>
        <td>
            <p style="margin: 0; font-size: 14px; font-weight: bold;">
                С уважением,<br> Команда КУБ
            </p>
        </td>
    </tr>
</table>
<?= $this->render('@common/mail/layouts/parts/_contacts') ?>