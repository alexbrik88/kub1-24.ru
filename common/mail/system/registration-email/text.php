<?php
/**
 * @var $message \common\components\sender\unisender\UniSender
 * @var $data array
 */
?>
Регистрация в сервисе КУБ

Вы зарегистрированы в качестве нового сотрудника компании <?php echo $data['companyTypeShort']; ?> "<?php echo $data['companyName']; ?>".

Ваши данные для входа в систему:

Логин: <?php echo $data['login']; ?>
Пароль: <?php echo $data['password']; ?>


С уважением, Команда КУБ