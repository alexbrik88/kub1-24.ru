<?php

use common\components\helpers\Html;
use common\models\service\StoreOutInvoiceTariff;

/* @var \yii\web\View $this */
/* @var \common\models\document\Invoice $invoice */
/* @var \yii\mail\BaseMessage $message */
/* @var string $subject */
/* @var StoreOutInvoiceTariff $tariff */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'systemniye';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
    "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}";
$urlManager = clone \Yii::$app->urlManager;
$urlManager->baseUrl = '/';

$pdfUrl = $urlManager->createAbsoluteUrl([
    '/bill/invoice/' . $invoice->uid . '?' . $linkParams,
]);
?>
<table border="0" cellspacing="0" cellpadding="20" style="width: 100%">
    <tr>
        <td>
            <p style="margin: 0; font-size: 24pt; line-height: 21px; color: #000;">
                Здравствуйте, <?= $company->chief_firstname ?>!
            </p>
        </td>
    </tr>
    <tr>
        <td>
            <p style="margin: 0; font-size: 12pt;">
                Счет на оплату предоставления модуля онлайн выставления счетов для покупателя на 1 год.
                Ссылок <?= $tariff->links_count; ?>
            </p>
        </td>
    </tr>
    <tr>
        <td>
            <?= Html::a('Ссылка на счет', $pdfUrl, [
                'style' => 'text-decoration: none;color: #ffffff;padding-top: 8px;background-color: #0077A7;padding-bottom: 8px;padding-left: 16px;padding-right: 16px;',
            ]); ?>
        </td>
    </tr>
    <tr>
        <td>
            <p style="margin: 0; font-size: 12pt; font-weight: bold;">
                С уважением,<br> Команда КУБ
            </p>
        </td>
    </tr>
</table>