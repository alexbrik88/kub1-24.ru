<?php
/**
 * @var $data array
 */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'systemniye';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
?>

<table cellpadding="20" cellspacing="0" style="width:100%;">
    <tr>
        <td>
            <p style="font-size: 24pt; line-height: 21px; color: #000; margin-bottom: 20px;">
                Добрый день!
            </p>
            <p style="margin: 0; font-size: 12pt;">
                Робот-Бухгалтер может считать бухгалтерию только для <b>ИП на УСН 6%</b>.
                <br>
                Мы сообщим вам, когда робот сможет считать бухгалтерию для вашей компании.
            </p>
        </td>
    </tr>
    <tr>
        <td>
            <p style="margin: 0; font-size: 14px; font-weight: bold;">
                С уважением,<br> Команда КУБ
            </p>
        </td>
    </tr>
</table>
<?= $this->render('@common/mail/layouts/parts/_contacts') ?>