<?php

use common\models\service\RewardRequest;

/**
 * @var $message \common\components\sender\unisender\UniSender
 * @var $subject string
 * @var $subject string
 * @var $type integer
 * @var $sum integer
 */
$baseUrl = \Yii::$app->params['uniSender']['baseUrl'];
$feedback = \Yii::$app->params['uniSender']['feedback'];
$callUs = \Yii::$app->params['uniSender']['phone'];
$supportEmail = \Yii::$app->params['emailList']['support'];
?>

Добрый день!

Ваше вознаграждение в сумме <?= $sum; ?> руб. оплачено
на <?= $type == RewardRequest::RS_TYPE ? 'р/с компании' : 'карту'; ?>

Срок зачисления от нескольких минут до 5 дней.


Нужна помощь?

Позвоните нам <?= $callUs ?>

Напишите нам <?= $supportEmail ?>

Оставить сообщение на сайте <?= $feedback ?>


Вы получили это письмо, потому что подписывались на новости сервиса КУБ на сайте <?= $baseUrl ?>.
У Вас есть возможность отписаться от рассылки, для этого необходимо перейти в личный кабинет <?= $baseUrl ?>, в раздел настройки и убрать галочки в блоке «Получение уведомлений».