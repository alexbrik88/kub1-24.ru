<?php

use common\models\service\RewardRequest;

/* @var $subject string
 * @var $type integer
 * @var $sum integer
 */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'systemniye';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
?>
    <table cellpadding="20" cellspacing="0" style="width:600px;">
        <tr>
            <td>
                <p style="margin: 0; font-size: 24pt;line-height: 21px;margin-bottom: 20px;">
                    Добрый день!
                </p>

                <p style="font-size: 12pt; line-height: 21px; color: #000; margin-bottom: 20px;">
                    Ваше вознаграждение в сумме <b><?= $sum; ?> руб. оплачено</b>
                    на <?= $type == RewardRequest::RS_TYPE ? 'р/с компании' : 'карту'; ?>
                </p>

                <p style="margin: 0; font-size: 12pt;">
                    Срок зачисления от нескольких минут до 5 дней.
                </p>
            </td>
        </tr>
    </table>

<?= $this->render('@common/mail/layouts/parts/_links') ?>