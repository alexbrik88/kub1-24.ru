<?php

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\document\SalesInvoice;
use common\models\employee\Employee;
use yii\web\View;

/** @var View $this */
/** @var SalesInvoice $model */
/** @var Employee $employee */

if (is_array($model)) {
    $randomSalesInvoice = current($model);
    $companyNameShort = $randomSalesInvoice->invoice->company_name_short;
    $company = $randomSalesInvoice->invoice->company;
} else {
    $companyNameShort = $model->invoice->company_name_short;
    $company = $model->invoice->company;
}

$emailSignature = $company->getEmailSignature(empty($employeeCompany) ? $employee->id : $employeeCompany->employee_id);

?>

<?= $companyNameShort; ?>
<?= str_repeat('=', mb_strlen($companyNameShort)); ?>

<?php if (!empty($emailText)) : ?>
    <?= $emailText; ?>
<?php else : ?>
    Здравствуйте!

    <?php if (is_array($model)): ?>
        <?php foreach ($model as $salesInvoice): ?>
            Расходная накладная № <?= $salesInvoice->fullNumber; ?> от <?= DateHelper::format($salesInvoice->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?> на сумму <?php echo TextHelper::invoiceMoneyFormat($salesInvoice->totalAmountWithNds, 2); ?> р.
        <?php endforeach; ?>
    <?php else: ?>
        Расходная накладная № <?= $model->fullNumber; ?> от <?= DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?> на сумму <?php echo TextHelper::invoiceMoneyFormat($model->totalAmountWithNds, 2); ?> р.
    <?php endif; ?>
<?php endif; ?>
<?php if (!is_array($model)): ?>
    Открыть расходную накладную: <?= Yii::$app->urlManager->createAbsoluteUrl(['documents/sales-invoice/view', 'id' => $model->id]); ?>
<?php endif; ?>

--
<?php if (isset($emailSignature) && $emailSignature->text): ?>
    <?= $emailSignature->text; ?>
<?php else: ?>
    С уважением,
    <?= empty($employeeCompany) ? $employee->getFio(true) : $employeeCompany->getFio(true); ?>
<?php endif; ?>

------------------------------------
Это письмо было отправлено через он-лайн сервис для предпринимателей КУБ
Попробовать бесплатно: <?= Yii::$app->params['serviceSite'] . '?utm_source=letter&utm_medium=email&utm_campaign=schet'; ?>
