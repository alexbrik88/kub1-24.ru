<?php

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\company\CompanyType;
use common\models\document\OrderSalesInvoice;
use common\models\document\SalesInvoice;
use common\models\employee\Employee;
use yii\helpers\Html;
use yii\mail\BaseMessage;
use yii\web\View;

/** @var View $this */
/** @var BaseMessage $message */
/** @var SalesInvoice|SalesInvoice[] $model */
/** @var Employee $employee */

if (is_array($model)) {
    $randomSalesInvoice = current($model);
    $companyNameShort = $randomSalesInvoice->invoice->company_name_short;
    $company = $randomSalesInvoice->invoice->company;
} else {
    $companyNameShort = $model->invoice->company_name_short;
    $company = $model->invoice->company;
}

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'systemniye';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$this->params['companyName'] = $companyNameShort;
$this->params['pixel'] = !empty($pixel) ? $pixel : null;

$emailSignature = $company->getEmailSignature(empty($employeeCompany) ? $employee->id : $employeeCompany->employee_id);
?>

<table cellpadding="20" cellspacing="0" style="width:600px;">
    <tr>
        <td>
            <?php if (!empty($emailText)) : ?>
                <p style="margin: 0; font-size: 14px; margin-bottom: 25px;">
                    <?= nl2br($emailText) ?>
                </p>
            <?php else : ?>
                <p style="font-size: 24px; line-height: 21px; color: #000; margin-bottom: 20px;">
                    Здравствуйте!
                </p>
                <?php if (is_array($model)): ?>
                    <?php foreach ($model as $salesInvoice): ?>
                        <p style="margin: 0; font-size: 14px; margin-bottom: 5px;">Расходная накладная
                            No <?= $salesInvoice->fullNumber; ?>
                            от <?= DateHelper::format($salesInvoice->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                            на сумму <?= TextHelper::invoiceMoneyFormat($salesInvoice->totalAmountWithNds, 2); ?> р.
                        </p>
                    <?php endforeach; ?>
                    <p style="margin: 0;margin-bottom: 40px;"></p>
                <?php else: ?>
                    <p style="margin: 0; font-size: 14px; margin-bottom: 25px;">Расходная накладная
                        No <?= $model->fullNumber; ?>
                        от <?= DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                        на сумму <?= TextHelper::invoiceMoneyFormat($model->totalAmountWithNds, 2); ?> р.
                    </p>
                <?php endif; ?>
            <?php endif; ?>
            <?php if (!is_array($model)): ?>
                <p style="margin: 0; font-size: 14px; margin-bottom: 25px;">
                    <?= Html::a('Открыть расходную накладную', Yii::$app->urlManager->createAbsoluteUrl([
                        '/documents/sales-invoice/view',
                        'id' => $model->id,
                        'utm_source' => $this->params['utm_source'],
                        'utm_medium' => $this->params['utm_medium'],
                        'utm_campaign' => $this->params['utm_campaign'],
                        'utm_term' => $this->params['utm_term'],
                        'utm_content' => 'sales-invoice_link',
                    ]), [
                        'style' => 'text-decoration: none;color: #ffffff;padding-top: 8px;background-color: #0077A7; padding-bottom: 8px;padding-left: 16px;padding-right: 16px;',
                    ]); ?>
                </p>
            <?php endif; ?>
            <p style="margin: 0; font-size: 14px; margin-bottom: 25px;">
                <?php if (isset($emailSignature) && $emailSignature->text): ?>
                    <?= nl2br($emailSignature->text); ?>
                <?php else: ?>
                    С уважением,<br>
                    <?= empty($employeeCompany) ? $employee->getFio(true) : $employeeCompany->getFio(true); ?>
                    <?php if ($company->company_type_id != CompanyType::TYPE_IP) : ?>
                        <br>
                        <?= empty($employeeCompany) ? $employee->position : $employeeCompany->position; ?>
                    <?php endif; ?>
                <?php endif; ?>
            </p>
        </td>
    </tr>
</table>