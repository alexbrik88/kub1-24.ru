<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 10.8.15
 * Time: 16.02
 * Email: t.kanstantsin@gmail.com
 */

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\document\OrderPackingList;

/* @var \yii\web\View $this */
/* @var \common\models\document\PackingList $model */
/* @var \common\models\employee\Employee $employee */

if (is_array($model)) {
    $randomPackingList = current($model);
    $companyNameShort = $randomPackingList->invoice->company_name_short;
    $company = $randomPackingList->invoice->company;
} else {
    $companyNameShort = $model->invoice->company_name_short;
    $company = $model->invoice->company;
}
$emailSignature = $company->getEmailSignature(empty($employeeCompany) ? $employee->id : $employeeCompany->employee_id);
?>
<?= $companyNameShort; ?>
<?= str_repeat('=', mb_strlen($companyNameShort)); ?>


<?php if (!empty($emailText)) : ?>
    <?= $emailText; ?>
<?php else : ?>
    Здравствуйте!

    <?php if (is_array($model)): ?>
        <?php foreach ($model as $packingList): ?>
            Товарная накладная № <?= $packingList->fullNumber; ?> от <?= DateHelper::format($packingList->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?> на сумму <?php echo TextHelper::invoiceMoneyFormat($packingList->totalAmountWithNds, 2); ?> р.
        <?php endforeach; ?>
    <?php else: ?>
        Товарная накладная № <?= $model->fullNumber; ?> от <?= DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?> на сумму <?php echo TextHelper::invoiceMoneyFormat($model->totalAmountWithNds, 2); ?> р.
    <?php endif; ?>
<?php endif; ?>
<?php if (!is_array($model)): ?>
    Открыть товарную накладную: <?= Yii::$app->urlManager->createAbsoluteUrl(['bill/packing-list/' . $model->uid]); ?>
<?php endif; ?>


--
<?php if (isset($emailSignature) && $emailSignature->text): ?>
    <?= $emailSignature->text; ?>
<?php else: ?>
    С уважением,
    <?= empty($employeeCompany) ? $employee->getFio(true) : $employeeCompany->getFio(true); ?>
<?php endif; ?>

------------------------------------
Это письмо было отправлено через он-лайн сервис для предпринимателей КУБ
Попробовать бесплатно: <?= Yii::$app->params['serviceSite'] . '?utm_source=letter&utm_medium=email&utm_campaign=schet'; ?>
