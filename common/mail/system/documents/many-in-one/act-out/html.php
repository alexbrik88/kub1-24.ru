<?php

use common\components\date\DateHelper;
use common\models\company\CompanyType;
use common\models\document\Invoice;
use yii\helpers\Html;
use common\components\TextHelper;

/* @var \yii\web\View $this */
/* @var \yii\mail\BaseMessage $message */
/* @var \common\models\document\Act|\common\models\document\Act[] $models */
/* @var \common\models\employee\Employee $employee */

if (is_array($models)) {
    $randomAct = current($models);
    $companyNameShort = $randomAct->invoice->company_name_short;
    $company = $randomAct->invoice->company;
} else {
    $companyNameShort = $models->invoice->company_name_short;
    $company = $models->invoice->company;
}

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'systemniye';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$this->params['companyName'] = $companyNameShort;
$this->params['pixel'] = !empty($pixel) ? $pixel : null;

$emailSignature = $company->getEmailSignature(empty($employeeCompany) ? $employee->id : $employeeCompany->employee_id);
?>
<table cellpadding="20" cellspacing="0" style="width:600px;">
    <tr>
        <td>
            <?php if (!empty($emailText)) : ?>
                <p style="margin: 0; font-size: 14px; margin-bottom: 25px;">
                    <?= nl2br($emailText) ?>
                </p>
            <?php else : ?>
                <p style="font-size: 24px; line-height: 21px; color: #000; margin-bottom: 20px;">
                    Здравствуйте!
                </p>
                <?php if (is_array($models)): ?>
                    <?php foreach ($models as $act): ?>
                        <p style="margin: 0; font-size: 14px; margin-bottom: 5px;">
                            Акт No <?= $act->fullNumber; ?>
                            от <?= DateHelper::format($act->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                            на сумму <?= $act->invoice->nds_view_type_id == Invoice::NDS_VIEW_OUT ?
                                TextHelper::invoiceMoneyFormat($act->totalAmountNoNds, 2) :
                                TextHelper::invoiceMoneyFormat($act->totalAmountWithNds, 2); ?> р.
                        </p>
                    <?php endforeach; ?>
                    <p style="margin: 0;margin-bottom: 40px;"></p>
                <?php else: ?>
                    <p style="margin: 0; font-size: 14px; margin-bottom: 25px;">
                        Акт No <?= $models->fullNumber; ?>
                        от <?= DateHelper::format($models->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                        на сумму <?= $models->invoice->nds_view_type_id == Invoice::NDS_VIEW_OUT ?
                            TextHelper::invoiceMoneyFormat($models->totalAmountNoNds, 2) :
                            TextHelper::invoiceMoneyFormat($models->totalAmountWithNds, 2); ?> р.
                    </p>
                <?php endif; ?>
            <?php endif; ?>
            <?php if (!is_array($models)): ?>
                <p style="margin: 0; font-size: 14px; margin-bottom: 25px;">
                    <?= Html::a('Открыть акт', Yii::$app->urlManager->createAbsoluteUrl([
                        '/bill/act/' . $models->uid,
                        'utm_source' => $this->params['utm_source'],
                        'utm_medium' => $this->params['utm_medium'],
                        'utm_campaign' => $this->params['utm_campaign'],
                        'utm_term' => $this->params['utm_term'],
                        'utm_content' => 'act_link',
                    ]), [
                        'style' => 'text-decoration: none;color: #ffffff;padding-top: 8px;background-color: #0077A7; padding-bottom: 8px;padding-left: 16px;padding-right: 16px;',
                    ]); ?>
                </p>
            <?php endif; ?>
            <p style="margin: 0; font-size: 14px; margin-bottom: 25px;">
                <?php if (isset($emailSignature) && $emailSignature->text): ?>
                    <?= nl2br($emailSignature->text); ?>
                <?php else: ?>
                    С уважением,<br>
                    <?= empty($employeeCompany) ? $employee->getFio(true) : $employeeCompany->getFio(true); ?>
                    <?php if ($company->company_type_id != CompanyType::TYPE_IP) : ?>
                        <br>
                        <?= empty($employeeCompany) ? $employee->position : $employeeCompany->position; ?>
                    <?php endif; ?>
                <?php endif; ?>
            </p>
        </td>
    </tr>
</table>
