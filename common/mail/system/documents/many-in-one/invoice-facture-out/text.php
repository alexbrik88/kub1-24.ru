<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 10.8.15
 * Time: 16.02
 * Email: t.kanstantsin@gmail.com
 */

use common\components\date\DateHelper;
use common\models\document\Invoice;
use common\components\TextHelper;

/* @var \yii\web\View $this */
/* @var \common\models\document\InvoiceFacture $models */
/* @var \common\models\employee\Employee $employee */

if (is_array($models)) {
    $randomInvoiceFacture = current($models);
    $companyNameShort = $randomInvoiceFacture->invoice->company_name_short;
    $company = $randomInvoiceFacture->invoice->company;
} else {
    $companyNameShort = $models->invoice->company_name_short;
    $company = $models->invoice->company;
}
$emailSignature = $company->getEmailSignature(empty($employeeCompany) ? $employee->id : $employeeCompany->employee_id);
?>
<?= $companyNameShort; ?>
<?= str_repeat('=', mb_strlen($companyNameShort)); ?>

<?php if (!empty($emailText)) : ?>
    <?= $emailText; ?>
<?php else : ?>
    Здравствуйте!

    <?php if (is_array($models)): ?>
        <?php foreach ($models as $packingList): ?>
            Счет фактура № <?= $packingList->fullNumber; ?> от <?= DateHelper::format($packingList->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?> на сумму <?= $packingList->invoice->nds_view_type_id == Invoice::NDS_VIEW_OUT ?
                TextHelper::invoiceMoneyFormat($packingList->totalAmountNoNds, 2) :
                TextHelper::invoiceMoneyFormat($packingList->totalAmountWithNds, 2); ?> р.
        <?php endforeach; ?>
    <?php else: ?>
        Счет фактура № <?= $models->fullNumber; ?> от <?= DateHelper::format($models->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?> на сумму <?= $models->invoice->nds_view_type_id == Invoice::NDS_VIEW_OUT ?
            TextHelper::invoiceMoneyFormat($models->totalAmountNoNds, 2) :
            TextHelper::invoiceMoneyFormat($models->totalAmountWithNds, 2); ?> р.
    <?php endif; ?>
<?php endif; ?>
<?php if (!is_array($models)): ?>
    Открыть счет фактуру: <?= Yii::$app->urlManager->createAbsoluteUrl(['bill/invoice-facture/' . $models->uid]); ?>
<?php endif; ?>


--
<?php if (isset($emailSignature) && $emailSignature->text): ?>
    <?= $emailSignature->text; ?>
<?php else: ?>
    С уважением,
    <?= empty($employeeCompany) ? $employee->getFio(true) : $employeeCompany->getFio(true); ?>
<?php endif; ?>

------------------------------------
Это письмо было отправлено через он-лайн сервис для предпринимателей КУБ
Попробовать бесплатно: <?= Yii::$app->params['serviceSite'] . '?utm_source=lk_server&utm_medium=email&utm_campaign=InvoiceFacture'; ?>
