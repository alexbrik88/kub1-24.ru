<?php

use common\components\TextHelper;
use common\components\date\DateHelper;
use common\models\currency\Currency;
use common\models\document\Invoice;
use frontend\models\Documents;

/* @var \yii\web\View $this */
/* @var \common\models\document\Invoice[] $models */
/* @var \common\models\employee\Employee $employee */

$model = $models[0];

$emailSignature = $model->company->getEmailSignature(empty($employeeCompany) ? $employee->id : $employeeCompany->employee_id);
$urlManager = Yii::$app->urlManager;

$text = '';
foreach ($models as $m) {
    if ($m instanceof Invoice) {
        $viewUrl = $urlManager->createAbsoluteUrl([
            '/documents/invoice/out-view',
            'uid' => $m->uid,
        ]);
        $label = ($m->is_invoice_contract && $m->type == Documents::IO_TYPE_OUT ? 'Счет-договор' : 'Счет');
        $text .= $label.' No '.$m->fullNumber.' от '.date_create($m->document_date)->format('d.m.Y');
        $text .= ' на сумму '.TextHelper::invoiceMoneyFormat($m->view_total_with_nds, 2).' ';
        $text .= ($m->currency_name == Currency::DEFAULT_NAME ? 'руб' : $m->currency_name)."\n";
        $text .= 'Оплатить до '.date_create($m->payment_limit_date)->format('d.m.Y').'г.'."\n\n";
        $text .= 'Открыть: '.$viewUrl."\n\n";
        if ($m->outInvoice && ($comment = $m->outInvoice->add_comment)) {
            $text .= $model->outInvoice->add_comment."\n\n";
        }
    } else {
        $text .= str_replace("Здравствуйте!", "", $m->emailText)."\n\n";
    }
}
?>
<?= $model->company_name_short; ?>
<?= str_repeat('=', mb_strlen($model->company_name_short)); ?>

<?php if (!empty($emailText)) : ?>
<?= $emailText ?>
<?php else : ?>
Здравствуйте!
<?php endif ?>

<?= $text ?>

--
<?php if (isset($emailSignature) && $emailSignature->text): ?>
    <?= $emailSignature->text; ?>
<?php else: ?>
    С уважением,
    <?= empty($employeeCompany) ? $employee->getFio(true) : $employeeCompany->getFio(true); ?>
<?php endif; ?>


------------------------------------
Это письмо было отправлено через он-лайн сервис для предпринимателей КУБ
Попробовать бесплатно: <?= Yii::$app->params['serviceSite'] . '?utm_source=lk_server&utm_medium=email&utm_campaign=schet'; ?>
