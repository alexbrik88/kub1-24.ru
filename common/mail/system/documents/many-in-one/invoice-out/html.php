<?php

use common\components\TextHelper;
use common\components\date\DateHelper;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\currency\Currency;
use common\models\document\Invoice;
use frontend\models\Documents;
use frontend\modules\cash\modules\banking\components\Banking;
use yii\helpers\Html;

/* @var \yii\web\View $this */
/* @var \yii\mail\BaseMessage $message */
/* @var \common\models\document\Invoice[] $models */
/* @var \common\models\employee\Employee $employee */

$model = $models[0];

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'systemniye';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$this->params['companyName'] = $model->company_name_short;
$this->params['pixel'] = !empty($pixel) ? $pixel : null;

$emailSignature = $model->company->getEmailSignature(empty($employeeCompany) ? $employee->id : $employeeCompany->employee_id);
$urlManager = Yii::$app->urlManager;

$text = '';
foreach ($models as $m) {
    if ($m instanceof Invoice) {
        $viewUrl = $urlManager->createAbsoluteUrl([
            '/documents/invoice/out-view',
            'uid' => $m->uid,
            'utm_source' => $this->params['utm_source'],
            'utm_medium' => $this->params['utm_medium'],
            'utm_campaign' => $this->params['utm_campaign'],
            'utm_term' => $this->params['utm_term'],
            'utm_content' => 'invoice_link',
        ]);
        $label = ($m->is_invoice_contract && $m->type == Documents::IO_TYPE_OUT ? 'Счет-договор' : 'Счет');
        $text .= Html::a($label.' No '.$m->fullNumber.' от '.date_create($m->document_date)->format('d.m.Y'), $viewUrl);
        $text .= ' на сумму '.TextHelper::invoiceMoneyFormat($m->view_total_with_nds, 2).' ';
        $text .= $m->currency_name == Currency::DEFAULT_NAME ? 'руб' : $m->currency_name;
        $text .= '<br>Оплатить до '.date_create($m->payment_limit_date)->format('d.m.Y').'г.<br/><br/>';
        if ($m->outInvoice && ($comment = $m->outInvoice->add_comment)) {
            $text .= nl2br($m->outInvoice->add_comment).'<br/><br/>';
        }
    } else {
        $text .= str_replace("Здравствуйте!", "", $m->emailText).'<br/><br/>';
    }
}
?>

<table cellpadding="0" cellspacing="0" style="width:600px; padding: 20px;">
    <tr>
        <td style="font-size: 14px;">
            <div style="margin: 0; margin-bottom: 25px;">
                <?php if (!empty($emailText)) : ?>
                    <?= nl2br($emailText); ?><br><br>
                <?php else : ?>
                    <div style="font-size: 24px; line-height: 21px; color: #000; margin-bottom: 20px;">
                        Здравствуйте!
                    </div>
                <?php endif ?>
            <?= $text ?>
            <?php if (!empty($addHtml)) : ?>
                <div style="margin: 0; margin-bottom: 15px; max-width: 100%; overflow: hidden;">
                    <?= $addHtml; ?>
                </div>
            <?php endif ?>
            <div style="margin: 0;">
                <?php if (isset($emailSignature) && $emailSignature->text) : ?>
                    <?= nl2br($emailSignature->text); ?>
                <?php else : ?>
                    С уважением,<br>
                    <?= empty($employeeCompany) ? $employee->getFio(true) : $employeeCompany->getFio(true); ?>
                    <?php if ($model->company->company_type_id != CompanyType::TYPE_IP) : ?>
                        <br>
                        <?= empty($employeeCompany) ? $employee->position : $employeeCompany->position; ?>
                    <?php endif ?>
                <?php endif; ?>
            </div>
        </td>
    </tr>
</table>

