<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 10.8.15
 * Time: 16.02
 * Email: t.kanstantsin@gmail.com
 */

use common\components\TextHelper;
use common\components\date\DateHelper;

/* @var \yii\web\View $this */
/* @var \common\models\document\InvoiceFacture $models */
/* @var \common\models\employee\Employee $employee */
/* @var \common\models\EmployeeCompany $employeeCompany */

if (is_array($models)) {
    $randomUpd = current($models);
    $companyNameShort = $randomUpd->invoice->company_name_short;
    $company = $randomUpd->invoice->company;
} else {
    $companyNameShort = $models->invoice->company_name_short;
    $company = $models->invoice->company;
}
$emailSignature = $company->getEmailSignature(empty($employeeCompany) ? $employee->id : $employeeCompany->employee_id);
?>
<?= $companyNameShort; ?>
<?= str_repeat('=', mb_strlen($companyNameShort)); ?>


<?php if (!empty($emailText)) : ?>
    <?= $emailText; ?>
<?php else : ?>
    Здравствуйте!

    <?php if (is_array($models)): ?>
        <?php foreach ($models as $upd): ?>
            Универсальный передаточный документ № <?= $upd->fullNumber; ?> от <?= DateHelper::format($upd->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?> на сумму <?= TextHelper::invoiceMoneyFormat($upd->totalAmountWithNds, 2) ?> р.
        <?php endforeach; ?>
    <?php else: ?>
        Универсальный передаточный документ № <?= $models->fullNumber; ?> от <?= DateHelper::format($models->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?> на сумму <?= TextHelper::invoiceMoneyFormat($models->totalAmountWithNds, 2) ?> р.
    <?php endif; ?>
<?php endif; ?>
<?php if (!is_array($models)): ?>
    Открыть УПД: <?= Yii::$app->urlManager->createAbsoluteUrl(['bill/upd/' . $models->uid]); ?>
<?php endif; ?>


--
<?php if (isset($emailSignature) && $emailSignature->text): ?>
    <?= $emailSignature->text; ?>
<?php else: ?>
    С уважением,
    <?= empty($employeeCompany) ? $employee->getFio(true) : $employeeCompany->getFio(true); ?>
<?php endif; ?>


------------------------------------
Это письмо было отправлено через он-лайн сервис для предпринимателей КУБ
Попробовать бесплатно: <?= Yii::$app->params['serviceSite'] . '?utm_source=lk_server&utm_medium=email&utm_campaign=InvoiceFacture'; ?>
