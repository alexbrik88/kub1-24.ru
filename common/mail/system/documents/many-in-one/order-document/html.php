<?php

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\company\CompanyType;
use common\models\document\OrderDocument;
use yii\helpers\Html;

/* @var \yii\web\View $this */
/* @var \yii\mail\BaseMessage $message */
/* @var OrderDocument[] $models */
/* @var \common\models\employee\Employee $employee */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'systemniye';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$this->params['companyName'] = $models[0]->company->getTitle(true);
$this->params['pixel'] = !empty($pixel) ? $pixel : null;
?>

<table cellpadding="20" cellspacing="0" style="width:600px;">
    <?php foreach ($models as $k=>$model): ?>
    <?php $orderUrl = Yii::$app->urlManager->createAbsoluteUrl([
            '/bill/order-document/' . $model->uid,
            'utm_source' => $this->params['utm_source'],
            'utm_medium' => $this->params['utm_medium'],
            'utm_campaign' => $this->params['utm_campaign'],
            'utm_term' => $this->params['utm_term'],
            'utm_content' => 'order_document_link',
        ]); ?>
    <tr>
        <td>
        <?php if (!empty($emailText)) : ?>
            <?php if ($k == 0): ?>
                <p style="margin: 0; font-size: 14px; margin-bottom: 25px;">
                    <?= nl2br($emailText) ?>
                </p>
            <?php endif; ?>
        <?php else : ?>
            <?php if ($k == 0): ?>
                <p style="font-size: 24px; line-height: 21px; color: #000; margin-bottom: 20px;">
                    Здравствуйте!
                </p>
            <?php endif; ?>
            <p style="margin: 0; font-size: 14px; margin-bottom: 25px;">
                <a href="<?= $orderUrl ?>">Заказ No <?= $model->fullNumber; ?>
                    от <?= DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?></a>
                на сумму <?= TextHelper::invoiceMoneyFormat($model->total_amount, 2); ?> р.
                <br>
                Отгрузить до <?= DateHelper::format($model->ship_up_to_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE)?>г.
            </p>
        <?php endif ?>
            <?php if ($k == count($models) - 1): ?>
                <p style="margin: 0; font-size: 14px; margin-bottom: 25px;">
                    С уважением,<br>
                    <?= empty($employeeCompany) ? $employee->getFio(true) : $employeeCompany->getFio(true); ?>
                    <?php if ($model->company->company_type_id != CompanyType::TYPE_IP) : ?>
                        <br>
                        <?= empty($employeeCompany) ? $employee->position : $employeeCompany->position; ?>
                    <?php endif ?>
                </p>
            <?php endif; ?>
        </td>
    </tr>
    <?php endforeach; ?>
</table>