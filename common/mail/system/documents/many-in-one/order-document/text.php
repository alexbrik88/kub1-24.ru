<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 10.8.15
 * Time: 16.02
 * Email: t.kanstantsin@gmail.com
 */

use common\components\date\DateHelper;
use common\models\document\OrderDocument;

/* @var \yii\web\View $this */
/* @var OrderDocument[] $models */
/* @var \common\models\employee\Employee $employee */
?>
<?= $models[0]->company->getTitle(true); ?>
<?= str_repeat('=', mb_strlen($models[0]->company->getTitle(true))); ?>

<?php foreach ($models as $k=>$model): ?>

    <?php if (!empty($emailText)) : ?>
    <?= ($k==0) ? nl2br($emailText) : '' ?>
    <?php else : ?>
    <?php if ($k==0): ?>
    Здравствуйте!

    <?php endif; ?>
    Заказ № <?= $model->fullNumber; ?> от <?= DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?> на сумму <?= \common\components\TextHelper::invoiceMoneyFormat($model->total_amount, 2); ?> р.
    Отгрузить до <?=DateHelper::format($model->ship_up_to_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE)?>г.
    <?php endif ?>

    Ссылка на заказ: <?= Yii::$app->urlManager->createAbsoluteUrl(['bill/order-document/' . $model->uid]); ?>

<?php endforeach; ?>

--
С уважением,
<?= empty($employeeCompany) ? $employee->getFio(true) : $employeeCompany->getFio(true); ?>


------------------------------------
Это письмо было отправлено через он-лайн сервис для предпринимателей КУБ
Попробовать бесплатно: <?= Yii::$app->params['serviceSite'] . '?utm_source=lk_server&utm_medium=email&utm_campaign=schet'; ?>
