<?php

use common\components\date\DateHelper;
use common\models\company\CompanyType;
use common\models\document\Invoice;
use yii\helpers\Html;
use common\components\TextHelper;

/* @var \yii\web\View $this */
/* @var \yii\mail\BaseMessage $message */
/* @var \common\models\document\AbstractDocument[] $models */
/* @var \common\models\EmployeeCompany $sender */
/* @var string $subject */

$company = $sender->company;
$companyNameShort = $company->getTitle(true);

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'systemniye';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$this->params['companyName'] = $companyNameShort;
$this->params['pixel'] = !empty($pixel) ? $pixel : null;

$emailSignature = $company->getEmailSignature($sender->employee_id);
?>
<table cellpadding="20" cellspacing="0" style="width:600px;">
    <tr>
        <td>
            <p style="font-size: 24px; line-height: 21px; color: #000; margin-bottom: 20px;">
                Здравствуйте!
            </p>
            <?php foreach ($models as $model): ?>
                <p style="margin: 0; font-size: 14px; margin-bottom: 5px;">
                    <?= $model->getTitle() ?>
                </p>
            <?php endforeach; ?>
            <p style="margin: 10px 0; font-size: 14px;">
                От <?= $sender->company->getTitle(true) ?>
            </p>

            <p style="margin: 25px 0; font-size: 14px;">
                <?php if (isset($emailSignature) && $emailSignature->text): ?>
                    <?= nl2br($emailSignature->text); ?>
                <?php else: ?>
                    С уважением,<br>
                    <?= $sender->getFio(true); ?>
                    <?php if ($company->company_type_id != CompanyType::TYPE_IP) : ?>
                        <br>
                        <?= $sender->position; ?>
                    <?php endif; ?>
                <?php endif; ?>
            </p>
        </td>
    </tr>
</table>
