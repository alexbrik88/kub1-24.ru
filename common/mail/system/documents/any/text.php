<?php

use common\components\date\DateHelper;
use common\models\company\CompanyType;
use common\models\document\Invoice;
use yii\helpers\Html;
use common\components\TextHelper;

/* @var \yii\web\View $this */
/* @var \yii\mail\BaseMessage $message */
/* @var \common\models\document\AbstractDocument[] $models */
/* @var \common\models\EmployeeCompany $sender */
/* @var string $subject */

$company = $sender->company;
$companyNameShort = $company->getTitle(true);
$emailSignature = $company->getEmailSignature($sender->employee_id);
?>

Здравствуйте!

<?php foreach ($models as $model): ?>
<?= $model->getTitle() ?>.
<?php endforeach; ?>

<?php if (isset($emailSignature) && $emailSignature->text): ?>
<?= $emailSignature->text; ?>
<?php else: ?>
С уважением,
<?= $sender->getFio(true); ?>
<?php if ($company->company_type_id != CompanyType::TYPE_IP) : ?>
    <?= $sender->position; ?>
<?php endif; ?>
<?php endif; ?>

