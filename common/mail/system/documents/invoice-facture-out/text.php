<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 10.8.15
 * Time: 16.02
 * Email: t.kanstantsin@gmail.com
 */

use common\components\date\DateHelper;
use common\models\document\Invoice;
use common\components\TextHelper;

/* @var \yii\web\View $this */
/* @var \common\models\document\InvoiceFacture $model */
/* @var \common\models\employee\Employee $employee */

if (is_array($model)) {
    $randomInvoiceFacture = current($model);
    $companyNameShort = $randomInvoiceFacture->invoice->company_name_short;
    $company = $randomInvoiceFacture->invoice->company;
} else {
    $companyNameShort = $model->invoice->company_name_short;
    $company = $model->invoice->company;
}
$emailSignature = $company->getEmailSignature(empty($employeeCompany) ? $employee->id : $employeeCompany->employee_id);
?>
<?= $companyNameShort; ?>
<?= str_repeat('=', mb_strlen($companyNameShort)); ?>

<?php if (!empty($emailText)) : ?>
    <?= $emailText; ?>
<?php else : ?>
    Здравствуйте!

    <?php if (is_array($model)): ?>
        <?php foreach ($model as $packingList): ?>
            Счет фактура № <?= $packingList->fullNumber; ?> от <?= DateHelper::format($packingList->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?> на сумму <?= $packingList->invoice->nds_view_type_id == Invoice::NDS_VIEW_OUT ?
                TextHelper::invoiceMoneyFormat($packingList->totalAmountNoNds, 2) :
                TextHelper::invoiceMoneyFormat($packingList->totalAmountWithNds, 2); ?> р.
        <?php endforeach; ?>
    <?php else: ?>
        Счет фактура № <?= $model->fullNumber; ?> от <?= DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?> на сумму <?= $model->invoice->nds_view_type_id == Invoice::NDS_VIEW_OUT ?
            TextHelper::invoiceMoneyFormat($model->totalAmountNoNds, 2) :
            TextHelper::invoiceMoneyFormat($model->totalAmountWithNds, 2); ?> р.
    <?php endif; ?>
<?php endif; ?>
<?php if (!is_array($model)): ?>
    Открыть счет фактуру: <?= Yii::$app->urlManager->createAbsoluteUrl(['bill/invoice-facture/' . $model->uid]); ?>
<?php endif; ?>


--
<?php if (isset($emailSignature) && $emailSignature->text): ?>
    <?= $emailSignature->text; ?>
<?php else: ?>
    С уважением,
    <?= empty($employeeCompany) ? $employee->getFio(true) : $employeeCompany->getFio(true); ?>
<?php endif; ?>

------------------------------------
Это письмо было отправлено через он-лайн сервис для предпринимателей КУБ
Попробовать бесплатно: <?= Yii::$app->params['serviceSite'] . '?utm_source=lk_server&utm_medium=email&utm_campaign=InvoiceFacture'; ?>
