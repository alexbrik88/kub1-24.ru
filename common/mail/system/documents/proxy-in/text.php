<?php
use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\document\OrderWaybill;

/* @var \yii\web\View $this */
/* @var \common\models\document\Waybill $model */
/* @var \common\models\employee\Employee $employee */

$emailSignature = $model->invoice->company->getEmailSignature(empty($employeeCompany) ? $employee->id : $employeeCompany->employee_id);
?>
<?= $model->invoice->company_name_short; ?>
<?= str_repeat('=', mb_strlen($model->invoice->company_name_short)); ?>


<?php if (!empty($emailText)) : ?>
    <?= $emailText; ?>
<?php else : ?>
    Здравствуйте!

    Доверенность № <?= $model->fullNumber; ?> от <?= DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?> на сумму <?php echo TextHelper::invoiceMoneyFormat($model->totalAmountWithNds, 2); ?> р.
<?php endif; ?>
Открыть доверенность: <?= Yii::$app->urlManager->createAbsoluteUrl([
    '/documents/proxy/out-view',
    'uid' => $model->uid,
]); ?>


--
<?php if (isset($emailSignature) && $emailSignature->text): ?>
    <?= $emailSignature->text; ?>
<?php else: ?>
    С уважением,
    <?= empty($employeeCompany) ? $employee->getFio(true) : $employeeCompany->getFio(true); ?>
<?php endif; ?>

------------------------------------
Это письмо было отправлено через он-лайн сервис для предпринимателей КУБ
Попробовать бесплатно: <?= Yii::$app->params['serviceSite'] . '?utm_source=letter&utm_medium=email&utm_campaign=schet'; ?>
