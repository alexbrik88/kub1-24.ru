<?php

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\company\CompanyType;
use yii\helpers\Html;

/* @var \yii\web\View $this */
/* @var \yii\mail\BaseMessage $message */
/* @var \common\models\Agreement $model */
/* @var \common\models\employee\Employee $employee */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'systemniye';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$this->params['companyName'] = $model->company->name_short;
$this->params['pixel'] = !empty($pixel) ? $pixel : null;

$emailSignature = $model->company->getEmailSignature(empty($employeeCompany) ? $employee->id : $employeeCompany->employee_id);
?>

<table cellpadding="20" cellspacing="0" style="width:600px;">
    <tr>
        <td>
            <?php if (!empty($emailText)) : ?>
                <p style="margin: 0; font-size: 14px; margin-bottom: 25px;">
                    <?= nl2br($emailText) ?>
                </p>
            <?php else : ?>
                <p style="font-size: 24px; line-height: 21px; color: #000; margin-bottom: 20px;">
                    Здравствуйте!
                </p>
                <p style="margin: 0; font-size: 14px; margin-bottom: 25px;">
                    Договор № <?= $model->fullNumber; ?>
                    от <?= DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                </p>
            <?php endif; ?>
            <?php /*
            <p style="margin: 0; font-size: 14px; margin-bottom: 25px;">
                <?= Html::a('Открыть документ', Yii::$app->urlManager->createAbsoluteUrl([
                    '/bill/upd/' . $model->getPdfFileName(),
                    'utm_source' => $this->params['utm_source'],
                    'utm_medium' => $this->params['utm_medium'],
                    'utm_campaign' => $this->params['utm_campaign'],
                    'utm_term' => $this->params['utm_term'],
                    'utm_content' => 'invoice-facture_link',
                ]), [
                    'style' => 'text-decoration: none;color: #ffffff;padding-top: 8px;background-color: #0077A7; padding-bottom: 8px;padding-left: 16px;padding-right: 16px;',
                ]); ?>
            </p>
            */ ?>
            <p style="margin: 0; font-size: 14px;">
                <?php if (isset($emailSignature) && $emailSignature->text): ?>
                    <?= nl2br($emailSignature->text); ?>
                <?php else: ?>
                    С уважением,<br>
                    <?= empty($employeeCompany) ? $employee->getFio(true) : $employeeCompany->getFio(true); ?>
                    <?php if ($model->company->company_type_id != CompanyType::TYPE_IP) : ?>
                        <br>
                        <?= empty($employeeCompany) ? $employee->position : $employeeCompany->position; ?>
                    <?php endif; ?>
                <?php endif; ?>
            </p>
        </td>
    </tr>
</table>