<?php

use common\components\TextHelper;
use common\components\date\DateHelper;

/* @var \yii\web\View $this */
/* @var \common\models\Agreement $model */
/* @var \common\models\employee\Employee $employee */
/* @var \common\models\EmployeeCompany $employeeCompany */

$emailSignature = $model->company->getEmailSignature(empty($employeeCompany) ? $employee->id : $employeeCompany->employee_id);
?>
<?= $model->company->name_short; ?>
<?= str_repeat('=', mb_strlen($model->company->name_short)); ?>


<?php if (!empty($emailText)) : ?>
    <?= $emailText; ?>
<?php else : ?>
    Здравствуйте!

    Документ № <?= $model->fullNumber; ?> от <?= DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
<?php endif; ?>
<?php /*
Открыть документ: <?= Yii::$app->urlManager->createAbsoluteUrl(['bill/upd/' . $model->uid]); ?>
*/ ?>


--
<?php if (isset($emailSignature) && $emailSignature->text): ?>
    <?= $emailSignature->text; ?>
<?php else: ?>
    С уважением,
    <?= empty($employeeCompany) ? $employee->getFio(true) : $employeeCompany->getFio(true); ?>
<?php endif; ?>


------------------------------------
Это письмо было отправлено через он-лайн сервис для предпринимателей КУБ
Попробовать бесплатно: <?= Yii::$app->params['serviceSite'] . '?utm_source=lk_server&utm_medium=email&utm_campaign=Agreement'; ?>
