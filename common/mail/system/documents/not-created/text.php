<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $company \common\models\Company */
/* @var $employee \common\models\employee\Employee */
/* @var $month string */

?>

Добрый день<?= $employee->firstname ? ', ' . $employee->firstname : '' ?>!

Закончился <?= $month ?>, чтобы подготовиться к сдаче отчетности и оплате налогов за квартал, мы рекомендуем:
1. Если вы оказываете услуги, то подготовить акты и подписать их с клиентами.
2. Если вы продаете товар, то подготовить Товарные Накладные и подписать их с клиентами.
3. Если ваше ООО или ИП на ОСНО (вы работаете с НДС), то обязательно подготовить Счета-Фактуры и предоставить их клиентам.

У вас в КУБе есть Счета в количестве 12 шт, по которым нужно сформировать:
<?php if ($company->notCreatedAct) : ?>
Акты в кол-ве - <?= $company->notCreatedAct ?> шт.
<?php endif ?>
<?php if ($company->notCreatedPL) : ?>
Товарные накладные в кол-ве - <?= $company->notCreatedPL ?> шт.
<?php endif ?>
<?php if ($company->notCreatedIF) : ?>
Счета-фактуры в кол-ве - <?= $company->notCreatedIF ?> шт.
<?php endif ?>

Для этого вам нужно в меню "Продажи" войти в "Счета". Там в таблице счетов, напротив каждого счета вы увидете кнопку "ДОБАВИТЬ" - это значит, что к этому счету нет Акта, Товарной накладной или Счета-фактуры.
Нажмите кнопку "ДОБАВИТЬ" и нужный документ автоматически добавится.
ВАЖНО: проверьте дату документа и, если нужно, измените ее!