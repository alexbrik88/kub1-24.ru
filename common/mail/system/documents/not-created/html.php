<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $company \common\models\Company */
/* @var $employee \common\models\employee\Employee */
/* @var $month string */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'systemniye';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$this->params['companyName'] = $company->getTitle(true);
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
        "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}";
?>

<table cellpadding="0" cellspacing="0" style="width:600px;">
    <tbody>
        <tr>
            <td align="center">
                <?= Html::img('https://lk.kub-24.ru/img/unisender/12-2.jpg', [
                    'width' => 600,
                    'alt' => '',
                ]); ?>
            </td>
        </tr>
    </tbody>
</table>
<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody>
        <tr>
            <td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
                <h1 style="margin: 0 0 20px; font-size:28px !important; line-height:100% !important; font-weight:600; color:#333333">
                    Добрый день<?= $employee->firstname ? ', ' . $employee->firstname : '' ?>!
                </h1>
                <b>Закончился <?= $month ?></b>, чтобы подготовиться к сдаче отчетности и оплате налогов за квартал, мы рекомендуем:
                <br>
                1. Если вы оказываете услуги, то подготовить акты и подписать их с клиентами.
                <br>
                2. Если вы продаете товар, то подготовить Товарные Накладные и подписать их с клиентами.
                <br>
                3. Если ваше ООО или ИП на ОСНО (вы работаете с НДС), то обязательно подготовить Счета-Фактуры и предоставить их клиентам.
                <br>
                <br>
                У вас в КУБе, по <b><?= $company->getTitle(true) ?></b> есть <b>Счета в количестве <?= $company->notCreatedInvoice ?> шт</b>, по которым нужно сформировать:
                <br>
                <?php if ($company->notCreatedAct) : ?>
                    <b>Акты</b> в кол-ве - <?= $company->notCreatedAct ?> шт.
                    <br>
                <?php endif ?>
                <?php if ($company->notCreatedPL) : ?>
                    <b>Товарные накладные</b> в кол-ве - <?= $company->notCreatedPL ?> шт.
                    <br>
                <?php endif ?>
                <?php if ($company->notCreatedIF) : ?>
                    <b>Счета-фактуры</b> в кол-ве - <?= $company->notCreatedIF ?> шт.
                    <br>
                <?php endif ?>
                <br>
                Для этого вам нужно в меню "Продажи" войти в "Счета". Там в таблице счетов, напротив каждого счета вы увидете кнопку "ДОБАВИТЬ" - это значит, что к этому счету нет Акта, Товарной накладной или Счета-фактуры.
                <br>
                Нажмите кнопку "ДОБАВИТЬ" и нужный документ автоматически добавится.
                <br>
                <b>ВАЖНО</b>: проверьте дату документа и, если нужно, измените ее!
            </td>
        </tr>
    </tbody>
</table>
<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody>
        <tr>
            <td align="center" style="background-color:#ffffff; font-family:Arial,Helvetica,sans-serif; line-height:100%; font-size:16px; ">
                <a href="https://kub-24.ru/login/?<?= $linkParams ?>&amp;utm_content=button" style="color:#ffffff; font-weight:400; text-decoration:none; padding:10px 20px 10px 20px; background-color:#0077a7">
                    Войти в КУБ
                </a>
            </td>
        </tr>
    </tbody>
</table>

<?= $this->render('@common/mail/layouts/parts/_arslan', ['message' => $message]) ?>

<?= $this->render('@common/mail/layouts/parts/_links') ?>

