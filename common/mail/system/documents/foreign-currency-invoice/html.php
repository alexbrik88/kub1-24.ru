<?php

use common\components\date\DateHelper;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\currency\Currency;
use common\models\document\Invoice;
use frontend\models\Documents;
use frontend\modules\cash\modules\banking\components\Banking;
use yii\helpers\Html;
use common\components\TextHelper;

/* @var \yii\web\View $this */
/* @var \yii\mail\BaseMessage $message */
/* @var \common\models\document\Invoice $model */
/* @var \common\models\employee\Employee $employee */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'systemniye';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$this->params['companyName'] = $model->company_name_short;
$this->params['pixel'] = !empty($pixel) ? $pixel : null;

$emailSignature = $model->company->getEmailSignature(empty($employeeCompany) ? $employee->id : $employeeCompany->employee_id);
$uid = $model->uid;
$urlManager = Yii::$app->urlManager;
$viewUrl = $urlManager->createAbsoluteUrl([
    '/documents/foreign-currency-invoice/out-view',
    'uid' => $uid,
    'email' => $toEmail,
    'utm_source' => $this->params['utm_source'],
    'utm_medium' => $this->params['utm_medium'],
    'utm_campaign' => $this->params['utm_campaign'],
    'utm_term' => $this->params['utm_term'],
    'utm_content' => 'invoice_link',
]);
$pdfUrl = $urlManager->createAbsoluteUrl([
    '/documents/foreign-currency-invoice/download',
    'type' => 'pdf',
    'uid' => $uid,
    'utm_source' => $this->params['utm_source'],
    'utm_medium' => $this->params['utm_medium'],
    'utm_campaign' => $this->params['utm_campaign'],
    'utm_term' => $this->params['utm_term'],
    'utm_content' => 'invoice_link',
]);
$wordUrl = $urlManager->createAbsoluteUrl([
    '/documents/foreign-currency-invoice/download',
    'type' => 'docx',
    'uid' => $uid,
    'utm_source' => $this->params['utm_source'],
    'utm_medium' => $this->params['utm_medium'],
    'utm_campaign' => $this->params['utm_campaign'],
    'utm_term' => $this->params['utm_term'],
    'utm_content' => 'invoice_link',
]);
?>

<table cellpadding="20" cellspacing="0" style="width:600px;">
    <tr>
        <td style="font-size: 14px;">
            <?php if (!empty($emailText)) : ?>
                <div style="margin: 0; margin-bottom: 25px;">
                    <?= nl2br($emailText); ?><br>
                    Invoice No <?= $model->fullNumber; ?>
                    от <?= DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                    на сумму <?= TextHelper::invoiceMoneyFormat($model->total_amount_with_nds, 2); ?>
                    <?= $model->currency_name ?>.
                    <br>
                    Оплатить до <?=DateHelper::format($model->payment_limit_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE)?>г.
                </div>
            <?php else : ?>
                <div style="font-size: 24px; line-height: 21px; color: #000; margin-bottom: 20px;">
                    Здравствуйте!
                </div>
                <div style="margin: 0; margin-bottom: 25px;">
                    Invoice No <?= $model->fullNumber; ?>
                    от <?= DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                    на сумму <?= TextHelper::invoiceMoneyFormat($model->total_amount_with_nds, 2); ?>
                        <?= $model->currency_name ?>.
                    <br>
                    Оплатить до <?=DateHelper::format($model->payment_limit_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE)?>г.
                </div>
            <?php endif ?>
            <div style="margin-bottom: 25px; position: relative;">
                <a href="<?= $viewUrl ?>"
                   style="display: block; padding: 15px; text-align: center; text-decoration: none; color: #ffffff; background-color: #3a8bf7;">
                    VIEW INVOICE
                </a>
            </div>
            <div style="margin: 0; margin-bottom: 15px;">
                <table cellpadding="15" cellspacing="0" style="width: 100%">
                    <tr>
                        <td style="width: 25%; text-align: center; vertical-align: top;">
                            <a href="<?= $viewUrl ?>" style="color: #122c49; text-decoration: none;">
                                <img alt="" src="<?= $message->embed(Yii::getAlias('@frontend/web/img/email/invoice-out/pdf.png')); ?>">
                                <br>
                                Скачать
                                <br>
                                в PDF
                            </a>
                        </td>
                        <td style="width: 25%; text-align: center; vertical-align: top; border-left: 1px solid #ccc;">
                            <a href="<?= $viewUrl ?>" style="color: #122c49; text-decoration: none;">
                                <img alt="" src="<?= $message->embed(Yii::getAlias('@frontend/web/img/email/invoice-out/doc.png')); ?>">
                                <br>
                                Скачать
                                <br>
                                в WORD
                            </a>
                        </td>
                        <td style="width: 25%; text-align: center; vertical-align: top; border-left: 1px solid #ccc;">
                            <a href="<?= $viewUrl ?>" style="color: #122c49; text-decoration: none;">
                                <img alt="" src="<?= $message->embed(Yii::getAlias('@frontend/web/img/email/invoice-out/save.png')); ?>">
                                <br>
                                Сохранить
                                <br>
                                в личном
                                <br>
                                кабинете
                            </a>
                        </td>
                        <td style="width: 25%; text-align: center; vertical-align: top; border-left: 1px solid #ccc;">
                            <a href="<?= $viewUrl ?>" style="color: #122c49; text-decoration: none;">
                                <img alt="" src="<?= $message->embed(Yii::getAlias('@frontend/web/img/email/invoice-out/pay.png')); ?>">
                                <br>
                                Оплатить
                                <br>
                                счет
                            </a>
                        </td>
                    </tr>
                </table>
            </div>
            <?php if (!empty($addHtml)) : ?>
                <div style="margin: 0; margin-bottom: 15px; max-width: 100%; overflow: hidden;">
                    <?= $addHtml; ?>
                </div>
            <?php endif ?>
            <div style="margin: 0;">
                <?php if (isset($emailSignature) && $emailSignature->text) : ?>
                    <?= nl2br($emailSignature->text); ?>
                <?php else : ?>
                    С уважением,<br>
                    <?= empty($employeeCompany) ? $employee->getFio(true) : $employeeCompany->getFio(true); ?>
                    <?php if ($model->company->company_type_id != CompanyType::TYPE_IP) : ?>
                        <br>
                        <?= empty($employeeCompany) ? $employee->position : $employeeCompany->position; ?>
                    <?php endif ?>
                <?php endif; ?>
            </div>
        </td>
    </tr>
</table>
