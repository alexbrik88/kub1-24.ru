<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 10.8.15
 * Time: 16.02
 * Email: t.kanstantsin@gmail.com
 */

use common\components\date\DateHelper;
use common\models\document\Invoice;
use common\components\TextHelper;

/* @var \yii\web\View $this */
/* @var \common\models\document\Act $model */
/* @var \common\models\employee\Employee $employee */

if (is_array($model)) {
    $randomAct = current($model);
    $companyNameShort = $randomAct->invoice->company_name_short;
    $company = $randomAct->invoice->company;
} else {
    $companyNameShort = $model->invoice->company_name_short;
    $company = $model->invoice->company;
}
$emailSignature = $company->getEmailSignature(empty($employeeCompany) ? $employee->id : $employeeCompany->employee_id);
?>
<?= $companyNameShort; ?>
<?= str_repeat('=', mb_strlen($companyNameShort)); ?>



<?php if (!empty($emailText)) : ?>
    <?= $emailText; ?>
<?php else: ?>
    Здравствуйте!
    <?php if (is_array($model)): ?>
        <?php foreach ($model as $act): ?>
            Акт № <?= $act->fullNumber ?> от <?= DateHelper::format($act->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) ?> на сумму <?= $act->invoice->nds_view_type_id == Invoice::NDS_VIEW_OUT ?
                TextHelper::invoiceMoneyFormat($act->totalAmountNoNds, 2) :
                TextHelper::invoiceMoneyFormat($act->totalAmountWithNds, 2); ?> р.
        <?php endforeach; ?>
    <?php else: ?>
        Акт № <?= $model->fullNumber ?> от <?= DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) ?> на сумму <?= $model->invoice->nds_view_type_id == Invoice::NDS_VIEW_OUT ?
            TextHelper::invoiceMoneyFormat($model->totalAmountNoNds, 2) :
            TextHelper::invoiceMoneyFormat($model->totalAmountWithNds, 2); ?> р.
    <?php endif; ?>
<?php endif; ?>
<?php if (!is_array($model)): ?>
    Открыть акт: <?= Yii::$app->urlManager->createAbsoluteUrl(['bill/act/' . $model->uid]) ?>
<?php endif; ?>


--
<?php if (isset($emailSignature) && $emailSignature->text): ?>
    <?= $emailSignature->text; ?>
<?php else: ?>
    С уважением,
    <?= empty($employeeCompany) ? $employee->getFio(true) : $employeeCompany->getFio(true); ?>
<?php endif; ?>


------------------------------------
Это письмо было отправлено через он-лайн сервис для предпринимателей КУБ
Попробовать бесплатно: <?= Yii::$app->params['serviceSite'] . '?utm_source=lk_server&utm_medium=email&utm_campaign=act'; ?>
