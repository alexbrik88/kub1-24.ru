<?php

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\document\GoodsCancellation;
use common\models\employee\Employee;

/* @var \yii\web\View $this */
/* @var GoodsCancellation $model */
/* @var Employee $employee */

if (is_array($model)) {
    $randomGoodsCancellation = current($model);
    $companyNameShort = $randomGoodsCancellation->company->getShortName();
    $company = $randomGoodsCancellation->company;
} else {
    $companyNameShort = $model->company->getShortName();
    $company = $model->company;
}

$emailSignature = $company->getEmailSignature(empty($employeeCompany) ? $employee->id : $employeeCompany->employee_id);
?>
<?= $companyNameShort; ?>
<?= str_repeat('=', mb_strlen($companyNameShort)); ?>


<?php if (!empty($emailText)) : ?>
    <?= $emailText; ?>
<?php else : ?>
    Здравствуйте!

    <?php if (is_array($model)): ?>
        <?php foreach ($model as $m): ?>
            Списание товара № <?= $m->fullNumber; ?> от <?= DateHelper::format($m->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?> на сумму <?php echo TextHelper::invoiceMoneyFormat($m->totalAmountWithNds, 2); ?> р.
        <?php endforeach; ?>
    <?php else: ?>
        Списание товара № <?= $model->fullNumber; ?> от <?= DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?> на сумму <?php echo TextHelper::invoiceMoneyFormat($model->totalAmountWithNds, 2); ?> р.
    <?php endif; ?>
<?php endif; ?>

--
<?php if (isset($emailSignature) && $emailSignature->text): ?>
    <?= $emailSignature->text; ?>
<?php else: ?>
    С уважением,
    <?= empty($employeeCompany) ? $employee->getFio(true) : $employeeCompany->getFio(true); ?>
<?php endif; ?>

------------------------------------
Это письмо было отправлено через он-лайн сервис для предпринимателей КУБ
Попробовать бесплатно: <?= Yii::$app->params['serviceSite'] . '?utm_source=letter&utm_medium=email&utm_campaign=schet'; ?>
