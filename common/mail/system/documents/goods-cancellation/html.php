<?php

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\company\CompanyType;
use common\models\document\GoodsCancellation;
use common\models\employee\Employee;

/* @var \yii\web\View $this */
/* @var \yii\mail\BaseMessage $message */
/* @var GoodsCancellation|GoodsCancellation[] $model */
/* @var Employee $employee */

if (is_array($model)) {
    $randomGoodsCancellation = current($model);
    $companyNameShort = $randomGoodsCancellation->company->getShortName();
    $company = $randomGoodsCancellation->company;
} else {
    $companyNameShort = $model->company->getShortName();
    $company = $model->company;
}

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'systemniye';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$this->params['companyName'] = $companyNameShort;
$this->params['pixel'] = !empty($pixel) ? $pixel : null;

$emailSignature = $company->getEmailSignature(empty($employeeCompany) ? $employee->id : $employeeCompany->employee_id);
?>

<table cellpadding="20" cellspacing="0" style="width:600px;">
    <tr>
        <td>
            <?php if (!empty($emailText)) : ?>
                <p style="margin: 0; font-size: 14px; margin-bottom: 25px;">
                    <?= nl2br($emailText) ?>
                </p>
            <?php else : ?>
                <p style="font-size: 24px; line-height: 21px; color: #000; margin-bottom: 20px;">
                    Здравствуйте!
                </p>
                <?php if (is_array($model)): ?>
                    <?php foreach ($model as $m): ?>
                        <p style="margin: 0; font-size: 14px; margin-bottom: 5px;">Списание товара
                            No <?= $m->fullNumber; ?>
                            от <?= DateHelper::format($m->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                            на сумму <?= TextHelper::invoiceMoneyFormat($m->view_total_with_nds, 2); ?> р.
                        </p>
                    <?php endforeach; ?>
                    <p style="margin: 0;margin-bottom: 40px;"></p>
                <?php else: ?>
                    <p style="margin: 0; font-size: 14px; margin-bottom: 25px;">Списание товара
                        No <?= $model->fullNumber; ?>
                        от <?= DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                        на сумму <?= TextHelper::invoiceMoneyFormat($model->view_total_with_nds, 2); ?> р.
                    </p>
                <?php endif; ?>
            <?php endif; ?>
            <p style="margin: 0; font-size: 14px; margin-bottom: 25px;">
                <?php if (isset($emailSignature) && $emailSignature->text): ?>
                    <?= nl2br($emailSignature->text); ?>
                <?php else: ?>
                    С уважением,<br>
                    <?= empty($employeeCompany) ? $employee->getFio(true) : $employeeCompany->getFio(true); ?>
                    <?php if ($company->company_type_id != CompanyType::TYPE_IP) : ?>
                        <br>
                        <?= empty($employeeCompany) ? $employee->position : $employeeCompany->position; ?>
                    <?php endif; ?>
                <?php endif; ?>
            </p>
        </td>
    </tr>
</table>