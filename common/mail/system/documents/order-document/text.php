<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 10.8.15
 * Time: 16.02
 * Email: t.kanstantsin@gmail.com
 */

use common\components\date\DateHelper;
use common\models\document\OrderDocument;

/* @var \yii\web\View $this */
/* @var OrderDocument $model */
/* @var \common\models\employee\Employee $employee */
?>
<?= $model->company->getTitle(true); ?>
<?= str_repeat('=', mb_strlen($model->company->getTitle(true))); ?>

<?php if (!empty($emailText)) : ?>
<?= nl2br($emailText) ?>
<?php else : ?>
Здравствуйте!

Заказ № <?= $model->fullNumber; ?> от <?= DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?> на сумму <?= \common\components\TextHelper::invoiceMoneyFormat($model->total_amount, 2); ?> р.
<?php /* Отгрузить до <?=DateHelper::format($model->ship_up_to_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE)?>г. */ ?>
<?php endif ?>

Ссылка на заказ: <?= Yii::$app->urlManager->createAbsoluteUrl(['bill/order-document/' . $model->uid]); ?>


--
С уважением,
<?= empty($employeeCompany) ? $employee->getFio(true) : $employeeCompany->getFio(true); ?>


------------------------------------
Это письмо было отправлено через он-лайн сервис для предпринимателей КУБ
Попробовать бесплатно: <?= Yii::$app->params['serviceSite'] . '?utm_source=lk_server&utm_medium=email&utm_campaign=schet'; ?>
