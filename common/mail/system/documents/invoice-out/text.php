<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 10.8.15
 * Time: 16.02
 * Email: t.kanstantsin@gmail.com
 */
use common\components\date\DateHelper;
use common\models\currency\Currency;
use common\models\document\Invoice;
use frontend\models\Documents;

/* @var \yii\web\View $this */
/* @var \common\models\document\Invoice $model */
/* @var \common\models\employee\Employee $employee */

$totalSum = \common\components\TextHelper::invoiceMoneyFormat($model->view_total_with_nds, 2);
$currency = $model->currency_name == Currency::DEFAULT_NAME ? 'руб' : $model->currency_name;
$emailSignature = $model->company->getEmailSignature(empty($employeeCompany) ? $employee->id : $employeeCompany->employee_id);
?>
<?= $model->company_name_short; ?>
<?= str_repeat('=', mb_strlen($model->company_name_short)); ?>

<?php if (!empty($emailText)) : ?>
<?= nl2br($emailText) ?>
<?php else : ?>
Здравствуйте!

<?= ($model->is_invoice_contract && $model->type == Documents::IO_TYPE_OUT) ? 'Счет-договор' : 'Счет'; ?> № <?= $model->fullNumber; ?> от <?= DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?> на сумму <?= $totalSum; ?> <?=$currency?>.
Оплатить до <?=DateHelper::format($model->payment_limit_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE)?>г.
<?php endif ?>

Открыть <?= ($model->is_invoice_contract ? 'счет-договор' : 'счет') ?>: <?= Yii::$app->urlManager->createAbsoluteUrl(['bill/invoice/' . $model->uid]); ?>

<?php if ($model->outInvoice && $model->outInvoice->add_comment) : ?>
    <?= $model->outInvoice->add_comment; ?>
<?php endif ?>

--
<?php if (isset($emailSignature) && $emailSignature->text): ?>
    <?= $emailSignature->text; ?>
<?php else: ?>
    С уважением,
    <?= empty($employeeCompany) ? $employee->getFio(true) : $employeeCompany->getFio(true); ?>
<?php endif; ?>


------------------------------------
Это письмо было отправлено через он-лайн сервис для предпринимателей КУБ
Попробовать бесплатно: <?= Yii::$app->params['serviceSite'] . '?utm_source=lk_server&utm_medium=email&utm_campaign=schet'; ?>
