<?php

use common\components\date\DateHelper;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\currency\Currency;
use common\models\document\Invoice;
use frontend\models\Documents;
use frontend\modules\cash\modules\banking\components\Banking;
use yii\helpers\Html;
use common\components\TextHelper;

/* @var \yii\web\View $this */
/* @var \yii\mail\BaseMessage $message */
/* @var \common\models\document\Invoice $model */
/* @var \common\models\employee\Employee $employee */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'systemniye';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$this->params['companyName'] = $model->company_name_short;
$this->params['pixel'] = !empty($pixel) ? $pixel : null;

$banText = false; // ($model->contractor_bik && $model->contractor_rs);
$hasLogo = (($bank = $model->contractorBank) !== null && is_file($logoFile = $bank->uploadDirectory . $bank->logo_link)) ? true : false;
$emailSignature = $model->company->getEmailSignature(empty($employeeCompany) ? $employee->id : $employeeCompany->employee_id);
$uid = $model->uid;
$urlManager = Yii::$app->urlManager;
$viewUrl = $urlManager->createAbsoluteUrl([
    '/documents/invoice/out-view',
    'uid' => $uid,
    'email' => $toEmail,
    'utm_source' => $this->params['utm_source'],
    'utm_medium' => $this->params['utm_medium'],
    'utm_campaign' => $this->params['utm_campaign'],
    'utm_term' => $this->params['utm_term'],
    'utm_content' => 'invoice_link',
]);
$pdfUrl = $urlManager->createAbsoluteUrl([
    '/documents/invoice/download',
    'type' => 'pdf',
    'uid' => $uid,
    'utm_source' => $this->params['utm_source'],
    'utm_medium' => $this->params['utm_medium'],
    'utm_campaign' => $this->params['utm_campaign'],
    'utm_term' => $this->params['utm_term'],
    'utm_content' => 'invoice_link',
]);
$wordUrl = $urlManager->createAbsoluteUrl([
    '/documents/invoice/download',
    'type' => 'docx',
    'uid' => $uid,
    'utm_source' => $this->params['utm_source'],
    'utm_medium' => $this->params['utm_medium'],
    'utm_campaign' => $this->params['utm_campaign'],
    'utm_term' => $this->params['utm_term'],
    'utm_content' => 'invoice_link',
]);
$payUrl = null;

if ($model->contractor->face_type == Contractor::TYPE_PHYSICAL_PERSON) {
    if ($model->company->qiwi_public_key) {
        $payUrl = $urlManager->createAbsoluteUrl([
            '/qiwi/pay-form',
            'uid' => $uid,
            'email' => $toEmail,
            'utm_source' => $this->params['utm_source'],
            'utm_medium' => $this->params['utm_medium'],
            'utm_campaign' => $this->params['utm_campaign'],
            'utm_term' => $this->params['utm_term'],
            'utm_content' => 'invoice_link',
        ]);
    }
} else {
    if ($model->contractor_bik && ($alias = Banking::aliasByBik($model->contractor_bik)) !== null) {
        $payUrl = $urlManager->createAbsoluteUrl([
            "/cash/banking/{$alias}/default/pay-bill",
            'uid' => $uid,
            'utm_source' => $this->params['utm_source'],
            'utm_medium' => $this->params['utm_medium'],
            'utm_campaign' => $this->params['utm_campaign'],
            'utm_term' => $this->params['utm_term'],
            'utm_content' => 'invoice_link',
        ]);
    } else {
        $payUrl = $urlManager->createAbsoluteUrl([
            "/documents/invoice/out-view",
            'uid' => $uid,
            'payment' => 1,
            'utm_source' => $this->params['utm_source'],
            'utm_medium' => $this->params['utm_medium'],
            'utm_campaign' => $this->params['utm_campaign'],
            'utm_term' => $this->params['utm_term'],
            'utm_content' => 'invoice_link'
        ]);
    }
}
?>

<table cellpadding="20" cellspacing="0" style="width:600px;">
    <tr>
        <td style="font-size: 14px;">
            <?php if (!empty($emailText)) : ?>
                <div style="margin: 0; margin-bottom: 25px;">
                    <?= nl2br($emailText); ?><br>
                    <?= ($model->is_invoice_contract && $model->type == Documents::IO_TYPE_OUT) ? 'Счет-договор' : 'Счет'; ?> No <?= $model->fullNumber; ?>
                    от <?= DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                    на сумму <?= TextHelper::invoiceMoneyFormat($model->view_total_with_nds, 2); ?>
                    <?= $model->currency_name == Currency::DEFAULT_NAME ? 'руб' : $model->currency_name ?>.
                    <br>
                    Оплатить до <?=DateHelper::format($model->payment_limit_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE)?>г.
                </div>
            <?php else : ?>
                <div style="font-size: 24px; line-height: 21px; color: #000; margin-bottom: 20px;">
                    Здравствуйте!
                </div>
                <div style="margin: 0; margin-bottom: 25px;">
                    <?= ($model->is_invoice_contract && $model->type == Documents::IO_TYPE_OUT) ? 'Счет-договор' : 'Счет'; ?> No <?= $model->fullNumber; ?>
                    от <?= DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                    на сумму <?= TextHelper::invoiceMoneyFormat($model->view_total_with_nds, 2); ?>
                        <?= $model->currency_name == Currency::DEFAULT_NAME ? 'руб' : $model->currency_name ?>.
                    <br>
                    Оплатить до <?=DateHelper::format($model->payment_limit_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE)?>г.
                </div>
            <?php endif ?>
            <div style="margin-bottom: 25px; position: relative;">
                <a href="<?= $viewUrl ?>"
                   style="display: block; padding: 15px; text-align: center; text-decoration: none; color: #ffffff; background-color: #3a8bf7;">
                    ОТКРЫТЬ <?= $model->is_invoice_contract ? 'СЧЕТ-ДОГОВОР' : 'СЧЕТ'; ?>
                </a>
            </div>
            <div style="margin: 0; margin-bottom: 15px;">
                <table cellpadding="15" cellspacing="0" style="width: 100%">
                    <tr>
                        <td style="width: 25%; text-align: center; vertical-align: top;">
                            <a href="<?= $viewUrl ?>" style="color: #122c49; text-decoration: none;">
                                <img alt="" src="<?= $message->embed(Yii::getAlias('@frontend/web/img/email/invoice-out/pdf.png')); ?>">
                                <br>
                                Скачать
                                <br>
                                в PDF
                            </a>
                        </td>
                        <td style="width: 25%; text-align: center; vertical-align: top; border-left: 1px solid #ccc;">
                            <a href="<?= $viewUrl ?>" style="color: #122c49; text-decoration: none;">
                                <img alt="" src="<?= $message->embed(Yii::getAlias('@frontend/web/img/email/invoice-out/doc.png')); ?>">
                                <br>
                                Скачать
                                <br>
                                в WORD
                            </a>
                        </td>
                        <td style="width: 25%; text-align: center; vertical-align: top; border-left: 1px solid #ccc;">
                            <a href="<?= $viewUrl ?>" style="color: #122c49; text-decoration: none;">
                                <img alt="" src="<?= $message->embed(Yii::getAlias('@frontend/web/img/email/invoice-out/save.png')); ?>">
                                <br>
                                Сохранить
                                <br>
                                в личном
                                <br>
                                кабинете
                            </a>
                        </td>
                        <td style="width: 25%; text-align: center; vertical-align: top; border-left: 1px solid #ccc;">
                            <a href="<?= $viewUrl ?>" style="color: #122c49; text-decoration: none;">
                                <img alt="" src="<?= $message->embed(Yii::getAlias('@frontend/web/img/email/invoice-out/pay.png')); ?>">
                                <br>
                                Оплатить
                                <br>
                                счет
                            </a>
                        </td>
                    </tr>
                </table>
            </div>
            <?php if ($model->outInvoice && $model->outInvoice->add_comment) : ?>
                <div style="margin: 0; margin-bottom: 15px; max-width: 100%; overflow: hidden;">
                    <?= nl2br($model->outInvoice->add_comment); ?>
                </div>
            <?php endif ?>
            <?php if (!empty($addHtml)) : ?>
                <div style="margin: 0; margin-bottom: 15px; max-width: 100%; overflow: hidden;">
                    <?= $addHtml; ?>
                </div>
            <?php endif ?>
            <div style="margin: 0;">
                <?php if (isset($emailSignature) && $emailSignature->text) : ?>
                    <?= nl2br($emailSignature->text); ?>
                <?php else : ?>
                    С уважением,<br>
                    <?= empty($employeeCompany) ? $employee->getFio(true) : $employeeCompany->getFio(true); ?>
                    <?php if ($model->company->company_type_id != CompanyType::TYPE_IP) : ?>
                        <br>
                        <?= empty($employeeCompany) ? $employee->position : $employeeCompany->position; ?>
                    <?php endif ?>
                <?php endif; ?>
            </div>
        </td>
    </tr>
</table>
<?php if ($banText) : ?>
<table cellpadding="20" cellspacing="0" style="width:600px; border-top: 1px dashed #cccccc;">
    <tr>
        <td style="text-align: left; color: rgb(80, 80, 80); font-family: Arial, Helvetica, sans-serif; font-size: 14px; line-height: 150%;
        <?= $hasLogo ? ' width: 370px;' : ''; ?>">
            Сервис КУБ позаботился о том, что бы вы не набивали платежку по данному счету и подготовил платежное поручение для загрузки в клиент-банк.
            <br>
            Перейдите
            <?php if ($bank && $bank->url) : ?>
                <a title="Перейти в клиент-банк" href="<?= $bank->url ?>" style="color:#0077a7;">на страницу вашего банка</a>,
            <?php else : ?>
                на страницу вашего банка,
            <?php endif ?>
            войдите в клиент-банк и загрузите платежку, как документ из 1С.
            Поменяйте номер платежного поручения с №1 на тот, который у вас по порядку.
        </td>
        <?php if ($hasLogo) : ?>
            <td style="width: 150px;">
                <a align="center" href="<?= $bank->url ?>" style="text-decoration:none; " title="Перейти в клиент-банк">
                    <img alt="<?= $bank->bank_name ?>" src="<?= $message->embed($logoFile); ?>" style="width:150px">
                </a>
            </td>
        <?php endif ?>
    </tr>
</table>
<table cellpadding="20" cellspacing="0" style="width:600px; border-top: 1px dashed #cccccc;">
    <tr>
        <td style=" font-family:Arial,Helvetica,sans-serif; line-height:100%; font-size:14px; "><strong>Процветания вам и вашему бизнесу!</strong></td>
    </tr>
</table>
<?php endif ?>