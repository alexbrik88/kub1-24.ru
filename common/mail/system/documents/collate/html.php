<?php

use common\components\date\DateHelper;
use yii\helpers\Html;

/* @var \yii\web\View $this */
/* @var \yii\mail\BaseMessage $message */
/* @var \frontend\models\CollateForm $model */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'systemniye';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$this->params['companyName'] = $model->company->getTitle(true);
$this->params['pixel'] = !empty($pixel) ? $pixel : null;
?>
<table cellpadding="20" cellspacing="0" style="width:600px;">
    <tr>
        <td>
            <?= nl2br($model->emailText) ?>
        </td>
    </tr>
</table>