<?php

use common\components\date\DateHelper;

/* @var \yii\web\View $this */
/* @var \yii\mail\BaseMessage $message */
/* @var \frontend\models\CollateForm $model */

$companyName = $model->company->getTitle(true);
?>
<?= $companyName ?>
<?= str_repeat('=', mb_strlen($companyName)); ?>

<?= $model->emailText ?>

------------------------------------
Это письмо было отправлено через он-лайн сервис для предпринимателей КУБ
Попробовать бесплатно: <?= Yii::$app->params['serviceSite'] . '?utm_source=lk_server&utm_medium=email&utm_campaign=act'; ?>
