<?php

use common\models\companyStructure\NewSalePointTypeForm;

/**
 * @var $message \common\components\sender\unisender\UniSender
 * @var $subject string
 * @var $form NewSalePointTypeForm
 * @var $userFIO string
 * @var $email string
 * @var $phone string
 */
$baseUrl = \Yii::$app->params['uniSender']['baseUrl'];
$feedback = \Yii::$app->params['uniSender']['feedback'];
$callUs = \Yii::$app->params['uniSender']['phone'];
$supportEmail = \Yii::$app->params['emailList']['support'];
?>

Запрос на подключение CRM

<?= $form->description ?>

Контактное лицо: <?= $userFIO; ?>

Email: <?= $email; ?>

Тел.: <?= $phone; ?>

Нужна помощь?

Позвоните нам <?= $callUs ?>

Напишите нам <?= $supportEmail ?>

Оставить сообщение на сайте <?= $feedback ?>


Вы получили это письмо, потому что подписывались на новости сервиса КУБ на сайте <?= $baseUrl ?>.
У Вас есть возможность отписаться от рассылки, для этого необходимо перейти в личный кабинет <?= $baseUrl ?>, в раздел настройки и убрать галочки в блоке «Получение уведомлений».