<?php

use frontend\modules\analytics\models\ProfitAndLossContactUs;

/**
 * @var $message \common\components\sender\unisender\UniSender
 * @var $subject string
 * @var $form ProfitAndLossContactUs
 */
$baseUrl = \Yii::$app->params['uniSender']['baseUrl'];
$feedback = \Yii::$app->params['uniSender']['feedback'];
$callUs = \Yii::$app->params['uniSender']['phone'];
$supportEmail = \Yii::$app->params['emailList']['support'];
?>

<?= $subject . "\r\n" ?>

Имя: <?= $form->name . "\r\n"; ?>
Тел.: <?= $form->phone . "\r\n"; ?>
Email: <?= $form->email . "\r\n"; ?>

<?= (isset($footerText)) ? $footerText : '' ?>

