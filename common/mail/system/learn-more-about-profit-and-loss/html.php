<?php
use frontend\modules\analytics\models\ProfitAndLossContactUs;

/* @var $subject string
 * @var $form ProfitAndLossContactUs
 */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'systemniye';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
?>
    <table cellpadding="20" cellspacing="0" style="width:600px;">
        <tr>
            <td>
                <p style="font-size: 12pt; line-height: 21px; color: #000; margin-bottom: 20px;">
                    <b><?= $subject ?></b>
                </p>

                <p style="margin: 0; font-size: 12pt;">
                    Имя: <?= $form->name; ?>
                </p>
                <p style="margin: 0; font-size: 12pt;">
                    Тел.: <?= $form->phone; ?>
                </p>
                <p style="margin: 0; font-size: 12pt;">
                    Email: <?= $form->email; ?>
                </p>

                <?php if (isset($footerText)): ?>
                    <br/>
                    <p style="margin: 0; font-size: 12pt;">
                        <?= $footerText ?>
                    </p>
                <?php endif; ?>
            </td>
        </tr>
    </table>

