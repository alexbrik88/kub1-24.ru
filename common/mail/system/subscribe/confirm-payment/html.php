<?php

use common\components\helpers\Html;

/* @var \yii\web\View $this */
/* @var \common\models\service\Subscribe $subscribe */
/* @var \common\models\document\Invoice $invoice */
/* @var \yii\mail\BaseMessage $message */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'systemniye';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
        "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}";
?>
<table border="0" cellspacing="0" cellpadding="20" style="width: 100%">
    <tr>
        <td>
            <p style="margin: 0; font-size: 20pt; line-height: 30px; color: #000;">
                Платеж на сумму <?= $data['sum'] ?> руб. успешно зачислен!
            </p>
        </td>
    </tr>
    <tr>
        <td>
            <p style="margin: 0; font-size: 12pt;;">
                Ваша подписка активирована.<br>
                Для использования сервиса КУБ, пройдите в
                <a href="<?=trim(\Yii::$app->params['serviceSite'], '/')?>/login/?<?= $linkParams ?>&amp;utm_content=login-content"
                target="_blank">личный кабинет</a>.
            </p>
        </td>
    </tr>
    <tr>
        <td>
            <p style="margin: 0; font-size: 12pt;;">
                Если вам нужны оригиналы закрывающих документов с подписью и печатью,
                распечатайте акт в двух экземплярах, и отправьте нам обычным письмом по адресу:
                <br>
                Кому: ООО "КУБ"
                <br>
                Куда: 119180, Москва, ул.Большая Полянка, 51А/9
                <br>
                <br>
                В ответ мы вышлем вам подписанный экземпляр документов.
            </p>
        </td>
    </tr>
    <tr>
        <td>
            <p style="margin: 0; font-size: 12pt; font-weight: bold;">
                С уважением,<br> Команда КУБ
            </p>
        </td>
    </tr>
</table>