<?php

use common\components\helpers\Html;

/* @var \yii\web\View $this */
/* @var \common\models\service\Subscribe $subscribe */
/* @var \common\models\document\Invoice $invoice */
/* @var \yii\mail\BaseMessage $message */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'systemniye';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
        "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}";
$urlManager = clone \Yii::$app->urlManager;
$urlManager->baseUrl = '/';

$viewUrl = $urlManager->createAbsoluteUrl([
    '/bill/invoice/' . $invoice->uid . '?' . $linkParams,
]);
?>
<table border="0" cellspacing="0" cellpadding="20" style="width: 100%">
    <tr>
        <td>
            <p style="margin: 0; font-size: 24pt; line-height: 21px; color: #000;">
                Здравствуйте, <?= $company->chief_firstname ?>!
            </p>
        </td>
    </tr>
    <tr>
        <td>
            <p style="margin: 0; font-size: 12pt;">
                Счет на оплату сервиса kub-24.ru
            </p>
        </td>
    </tr>
    <tr>
        <td>
            <div style="margin-bottom: 25px; position: relative;">
                <a href="<?= $viewUrl ?>"
                   style="display: block; padding: 15px; text-align: center; text-decoration: none; color: #ffffff; background-color: #3a8bf7;">
                    ОТКРЫТЬ СЧЕТ
                </a>
            </div>
            <div style="margin: 0; margin-bottom: 15px;">
                <table cellpadding="15" cellspacing="0" style="width: 100%">
                    <tr>
                        <td style="width: 25%; text-align: center; vertical-align: top;">
                            <a href="<?= $viewUrl ?>" style="color: #122c49; text-decoration: none;">
                                <img alt="" src="<?= $message->embed(Yii::getAlias('@frontend/web/img/email/invoice-out/pdf.png')); ?>">
                                <br>
                                Скачать
                                <br>
                                в PDF
                            </a>
                        </td>
                        <td style="width: 25%; text-align: center; vertical-align: top; border-left: 1px solid #ccc;">
                            <a href="<?= $viewUrl ?>" style="color: #122c49; text-decoration: none;">
                                <img alt="" src="<?= $message->embed(Yii::getAlias('@frontend/web/img/email/invoice-out/doc.png')); ?>">
                                <br>
                                Скачать
                                <br>
                                в WORD
                            </a>
                        </td>
                        <td style="width: 25%; text-align: center; vertical-align: top; border-left: 1px solid #ccc;">
                            <a href="<?= $viewUrl ?>" style="color: #122c49; text-decoration: none;">
                                <img alt="" src="<?= $message->embed(Yii::getAlias('@frontend/web/img/email/invoice-out/save.png')); ?>">
                                <br>
                                Сохранить
                                <br>
                                в личном
                                <br>
                                кабинете
                            </a>
                        </td>
                        <td style="width: 25%; text-align: center; vertical-align: top; border-left: 1px solid #ccc;">
                            <a href="<?= $viewUrl ?>" style="color: #122c49; text-decoration: none;">
                                <img alt="" src="<?= $message->embed(Yii::getAlias('@frontend/web/img/email/invoice-out/pay.png')); ?>">
                                <br>
                                Оплатить
                                <br>
                                счет
                            </a>
                        </td>
                    </tr>
                </table>
            </div>
        </td>
    </tr>
    <tr>
        <td>
            <p style="margin: 0; font-size: 12pt; font-weight: bold;">
                С уважением,<br> Команда КУБ
            </p>
        </td>
    </tr>
</table>