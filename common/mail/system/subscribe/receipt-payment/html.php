<?php

/* @var \yii\web\View $this */
/* @var \common\models\service\Payment $payment */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'systemniye';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
?>
<table border="0" cellspacing="0" cellpadding="20" style="width: 100%">
    <tr>
        <td>
            <p style="margin: 0; font-size: 24pt; line-height: 21px; color: #000;">
                Квитанция Сбербанка
            </p>
        </td>
    </tr>
    <tr>
        <td>
            <p style="margin: 0; font-size: 12pt;">
                Квитанция Сбербанка на оплату подписки сервиса kub-24.ru
            </p>
        </td>
    </tr>
    <tr>
        <td>
            <p style="margin: 0; font-size: 12pt; font-weight: bold;">
                С уважением,<br> Команда КУБ
            </p>
        </td>
    </tr>
</table>
