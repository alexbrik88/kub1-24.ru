<?php

namespace app\common\mail\crm\message;

use common\models\employee\Employee;
use common\models\EmployeeCompany;
use frontend\modules\crm\models\Client;
use Yii;
use yii\mail\BaseMessage;
use yii\web\View;

/**
 * @var View $this
 * @var BaseMessage $message
 * @var Employee $employee
 * @var EmployeeCompany $employeeCompany
 * @var Client $model
 * @var string $emailText
 * @var string $subject
 * @var mixed[] $pixel
 */

$emailSignature = $model->company->getEmailSignature(empty($employeeCompany) ? $employee->id : $employeeCompany->employee_id);
?>
<?= $model->company->name_short; ?>
<?= str_repeat('=', mb_strlen($model->company->name_short)) ?>

<?= nl2br($emailText) ?>

--
<?php if ($emailSignature && $emailSignature->text): ?>
    <?= $emailSignature->text; ?>
<?php else: ?>
    С уважением,
    <?= empty($employeeCompany) ? $employee->getFio(true) : $employeeCompany->getFio(true); ?>
<?php endif; ?>


------------------------------------
Это письмо было отправлено через он-лайн сервис для предпринимателей КУБ
Попробовать бесплатно: <?= (Yii::$app->params['serviceSite'] . '?utm_source=lk_server&utm_medium=email&utm_campaign=schet') ?>
