<?php

namespace app\common\mail\crm\message;

use common\models\company\CompanyType;
use common\models\employee\Employee;
use common\models\EmployeeCompany;
use frontend\modules\crm\models\Client;
use yii\mail\BaseMessage;
use yii\helpers\Inflector;
use yii\web\View;

/**
 * @var View $this
 * @var BaseMessage $message
 * @var Employee $employee
 * @var EmployeeCompany $employeeCompany
 * @var Client $model
 * @var string $emailText
 * @var string $subject
 * @var mixed[] $pixel
 */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'systemniye';
$this->params['utm_term'] = Inflector::slug($subject, '_');
$this->params['companyName'] = $model->company->name_short;
$this->params['pixel'] = $pixel ?? null;

$emailSignature = $model->company->getEmailSignature(empty($employeeCompany) ? $employee->id : $employeeCompany->employee_id);

?>

<table cellpadding="20" cellspacing="0" style="width:600px;">
    <tr>
        <td style="font-size: 14px;">
            <div style="margin: 0; margin-bottom: 25px;">
                <?= nl2br($emailText) ?><br>
            </div>

            <div style="margin: 0;">
                <?php if (isset($emailSignature) && $emailSignature->text): ?>
                    <?= nl2br($emailSignature->text) ?>
                <?php else : ?>
                    С уважением,<br>
                    <?= empty($employeeCompany) ? $employee->getFio(true) : $employeeCompany->getFio(true); ?>
                    <?php if ($model->company->company_type_id != CompanyType::TYPE_IP): ?>
                        <br>
                        <?= empty($employeeCompany) ? $employee->position : $employeeCompany->position; ?>
                    <?php endif ?>
                <?php endif; ?>
            </div>
        </td>
    </tr>
</table>
