<?php

use yii\helpers\Html;

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'systemniye';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
?>

<table cellpadding="20" cellspacing="0" style="width:600px;">
    <tr>
        <td>
            <p style="font-size: 24px; line-height: 21px; color: #000; margin-bottom: 20px;">
                Здравствуйте, <?= $user->firstname ?>!
            </p>
            <p style="margin: 0; font-size: 12pt;  margin-bottom: 20px;">
                В ваш аккаунт на store.kub-24.ru добавлен продавец <?= $company->getTitle(true) ?>.
            </p>
            <p style="margin: 0; font-size: 12pt;">
                Пройдите по ссылке в свой кабинет:
            </p>
            <p style="margin: 0; font-size: 12pt;">
                <a href="https://store.kub-24.ru/">https://store.kub-24.ru/</a>
            </p>
        </td>
    </tr>
</table>

<?= $this->render('@common/mail/layouts/parts/_contacts') ?>
