<?php

/* @var $message \common\components\sender\unisender\UniSender */

$feedback = Yii::$app->params['uniSender']['feedback'];
$callUs = Yii::$app->params['uniSender']['phone'];
$supportEmail = Yii::$app->params['emailList']['support'];
?>
Здравствуйте, <?= $user->firstname ?>!

В ваш аккаунт на store.kub-24.ru добавлен продавец <?= $company->getTitle(true) ?>

Пройдите по ссылке в свой кабинет:

https://store.kub-24.ru/site/login

Нужна помощь?

Позвоните нам <?= $callUs ?>

Напишите нам <?= $supportEmail ?>
