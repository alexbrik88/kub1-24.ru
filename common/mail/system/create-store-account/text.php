<?php

/* @var $message \common\components\sender\unisender\UniSender */

$feedback = Yii::$app->params['uniSender']['feedback'];
$callUs = Yii::$app->params['uniSender']['phone'];
$supportEmail = Yii::$app->params['emailList']['support'];
?>
Добро пожаловать в КУБ!

Ваши данные для входа в КУБ:

Логин: <?= $user->email ?>
Пароль: <?= $password ?>

ВОЙТИ В КУБ https://store.kub-24.ru/site/login

Нужна помощь?

Позвоните нам <?= $callUs ?>

Напишите нам <?= $supportEmail ?>
