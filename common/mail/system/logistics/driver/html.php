<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 24.11.2018
 * Time: 19:17
 */

use common\components\date\DateHelper;
use common\models\company\CompanyType;

/* @var \yii\web\View $this */
/* @var \yii\mail\BaseMessage $message */
/* @var \common\models\driver\Driver $model */
/* @var string $subject */
/* @var $employeeCompany */
/* @var $emailText */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'systemniye';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');

$emailSignature = $model->company->getEmailSignature($employeeCompany->employee_id);
?>
<table cellpadding="20" cellspacing="0" style="width:600px;">
    <tr>
        <td>
            <?php if (!empty($emailText)) : ?>
                <p style="margin: 0; font-size: 14px; margin-bottom: 25px;">
                    <?= nl2br($emailText) ?>
                </p>
            <?php else : ?>
                <p style="font-size: 24px; line-height: 21px; color: #000; margin-bottom: 20px;">
                    Здравствуйте!
                </p>
                <p style="margin: 0; font-size: 12pt;">
                    Водитель: <?= $model->getFio(); ?>
                </p>
                <p style="margin: 0; font-size: 12pt;">
                    Удостоверение личности: <?= $model->identificationType ? $model->identificationType->name : null; ?>
                </p>
                <p style="margin: 0; font-size: 12pt;">
                    Серия: <?= $model->identification_series; ?>
                </p>
                <p style="margin: 0; font-size: 12pt;">
                    Номер: <?= $model->identification_number; ?>
                </p>
                <p style="margin: 0; font-size: 12pt;">
                    Дата выдачи: <?= DateHelper::format($model->identification_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                </p>
                <p style="margin: 0; font-size: 12pt;margin-bottom: 20px;">
                    Кем выдан: <?= $model->identification_issued_by; ?>
                </p>
                <p style="margin: 0; font-size: 12pt;">
                    Водительское удостоверение:
                </p>
                <p style="margin: 0; font-size: 12pt;">
                    Серия: <?= $model->driver_license_series; ?>
                </p>
                <p style="margin: 0; font-size: 12pt;">
                    Номер: <?= $model->driver_license_number; ?>
                </p>
                <p style="margin: 0; font-size: 12pt;">
                    Дата выдачи: <?= DateHelper::format($model->driver_license_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                </p>
                <p style="margin: 0; font-size: 12pt;margin-bottom: 20px;">
                    Кем выдан: <?= $model->driver_license_issued_by; ?>
                </p>
                <?php if ($model->vehicle): ?>
                    <p style="margin: 0; font-size: 12pt;">
                        Транспортное средство: <?= $model->vehicle->getVehicleTitle(); ?>
                    </p>
                <?php endif; ?>
            <?php endif ?>
            <p style="margin: 0; font-size: 14px; margin-bottom: 25px;">
                <?php if (isset($emailSignature) && $emailSignature->text) : ?>
                    <?= nl2br($emailSignature->text); ?>
                <?php else : ?>
                    С уважением,<br>
                    <?= $employeeCompany->getFio(true); ?>
                    <?php if ($model->company->company_type_id != CompanyType::TYPE_IP) : ?>
                        <br>
                        <?= $employeeCompany->position; ?>
                    <?php endif ?>
                <?php endif; ?>
            </p>
        </td>
    </tr>
</table>

