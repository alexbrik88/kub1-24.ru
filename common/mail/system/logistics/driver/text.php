<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 24.11.2018
 * Time: 19:17
 */

use common\components\date\DateHelper;

/* @var \yii\mail\BaseMessage $message */
/* @var \common\models\driver\Driver $model */
/* @var string $subject */

$baseUrl = \Yii::$app->params['uniSender']['baseUrl'];
$feedback = \Yii::$app->params['uniSender']['feedback'];
$callUs = \Yii::$app->params['uniSender']['phone'];
$supportEmail = \Yii::$app->params['emailList']['support'];

$emailSignature = $model->company->getEmailSignature($employeeCompany->employee_id);
?>
<?php if (!empty($emailText)) : ?>
    <?= nl2br($emailText) ?>
<?php else : ?>
    Здравствуйте!

    Водитель: <?= $model->getFio(); ?>

    Удостоверение личности: <?= $model->identificationType ? $model->identificationType->name : null; ?>

    Серия: <?= $model->identification_series; ?>

    Номер: <?= $model->identification_number; ?>

    Дата выдачи: <?= DateHelper::format($model->identification_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>

    Кем выдан: <?= $model->identification_issued_by; ?>

    Водительское удостоверение:

    Серия: <?= $model->driver_license_series; ?>

    Номер: <?= $model->driver_license_number; ?>

    Дата выдачи: <?= DateHelper::format($model->driver_license_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>

    Кем выдан: <?= $model->driver_license_issued_by; ?>

    <?php if ($model->vehicle): ?>
        Транспортное средство: <?= $model->vehicle->getVehicleTitle(); ?>
    <?php endif; ?>
<?php endif; ?>
<?php if (isset($emailSignature) && $emailSignature->text): ?>
    <?= $emailSignature->text; ?>
<?php else: ?>
    С уважением,
    <?= $employeeCompany->getFio(true); ?>
<?php endif; ?>
