<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 10.8.15
 * Time: 16.02
 * Email: t.kanstantsin@gmail.com
 */

use common\components\date\DateHelper;
use common\models\document\Invoice;

/* @var \yii\web\View $this */
/* @var \common\models\logisticsRequest\LogisticsRequest $model */
/* @var \common\models\employee\Employee $employee */

$emailSignature = $model->company->getEmailSignature($employeeCompany->employee_id);
?>

Здравствуйте!

<?php if (!empty($emailText)) : ?>
    <?= $emailText; ?>
<?php else: ?>
    <?= $model->getEmailSubject($requestType); ?>
<?php endif; ?>
Открыть акт: <?= Yii::$app->urlManager->createAbsoluteUrl(['/logistics/bill/request/' . $model->id . '/' . $requestType]) ?>


--
<?php if (isset($emailSignature) && $emailSignature->text): ?>
    <?= $emailSignature->text; ?>
<?php else: ?>
    С уважением,
    <?= empty($employeeCompany) ? $employee->getFio(true) : $employeeCompany->getFio(true); ?>
<?php endif; ?>


------------------------------------
Это письмо было отправлено через он-лайн сервис для предпринимателей КУБ
Попробовать бесплатно: <?= Yii::$app->params['serviceSite'] . '?utm_source=lk_server&utm_medium=email&utm_campaign=request'; ?>
