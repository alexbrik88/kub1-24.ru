<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 24.11.2018
 * Time: 19:17
 */

use common\components\date\DateHelper;
use common\models\company\CompanyType;
use common\models\vehicle\VehicleType;

/* @var \yii\web\View $this */
/* @var \yii\mail\BaseMessage $message */
/* @var \common\models\vehicle\Vehicle $model */
/* @var string $subject */
/* @var $employeeCompany */
/* @var $emailText */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'systemniye';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');

$emailSignature = $model->company->getEmailSignature($employeeCompany->employee_id);
?>
<table cellpadding="20" cellspacing="0" style="width:600px;">
    <tr>
        <td>
            <?php if (!empty($emailText)) : ?>
                <p style="margin: 0; font-size: 14px; margin-bottom: 25px;">
                    <?= nl2br($emailText) ?>
                </p>
            <?php else : ?>
                <p style="font-size: 24px; line-height: 21px; color: #000; margin-bottom: 20px;">
                    Здравствуйте!
                </p>
                <p style="margin: 0; font-size: 12pt;">
                    Водитель: <?= $model->driver ? $model->driver->getFio() : null; ?>
                </p>
                <p style="margin: 0; font-size: 12pt;">
                    Марка: <?= $model->model; ?>
                </p>
                <p style="margin: 0; font-size: 12pt;">
                    Номер ТС: <?= $model->state_number; ?>
                </p>
                <p style="margin: 0; font-size: 12pt;">
                    Тип кузова: <?= $model->bodyType ? $model->bodyType->name : null; ?>
                </p>
                <p style="margin: 0; font-size: 12pt;">
                    Цвет ТС: <?= $model->color; ?>
                </p>
                <?php if ($model->vehicle_type_id == VehicleType::TYPE_TRACTOR && $model->semitrailerType): ?>
                    <p style="margin: 0; font-size: 12pt;">
                        Полуприцеп: <?= ($model->semitrailerType->model ? ($model->semitrailerType->model . ' ') : null) . ' ' . $model->semitrailerType->state_number; ?>
                    </p>
                <?php elseif ($model->vehicle_type_id == VehicleType::TYPE_WAGON && $model->trailerType): ?>
                    <p style="margin: 0; font-size: 12pt;">
                        Прицеп: <?= ($model->trailerType->model ? ($model->trailerType->model . ' ') : null) . ' ' . $model->trailerType->state_number; ?>
                    </p>
                <?php endif; ?>
            <?php endif ?>
            <p style="margin: 0; font-size: 14px; margin-bottom: 25px;">
                <?php if (isset($emailSignature) && $emailSignature->text) : ?>
                    <?= nl2br($emailSignature->text); ?>
                <?php else : ?>
                    С уважением,<br>
                    <?= $employeeCompany->getFio(true); ?>
                    <?php if ($model->company->company_type_id != CompanyType::TYPE_IP) : ?>
                        <br>
                        <?= $employeeCompany->position; ?>
                    <?php endif ?>
                <?php endif; ?>
            </p>
        </td>
    </tr>
</table>

