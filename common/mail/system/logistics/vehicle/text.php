<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 24.11.2018
 * Time: 19:17
 */

use common\components\date\DateHelper;
use common\models\vehicle\VehicleType;

/* @var \yii\mail\BaseMessage $message */
/* @var \common\models\vehicle\Vehicle $model */
/* @var string $subject */

$baseUrl = \Yii::$app->params['uniSender']['baseUrl'];
$feedback = \Yii::$app->params['uniSender']['feedback'];
$callUs = \Yii::$app->params['uniSender']['phone'];
$supportEmail = \Yii::$app->params['emailList']['support'];

$emailSignature = $model->company->getEmailSignature($employeeCompany->employee_id);
?>
<?php if (!empty($emailText)) : ?>
    <?= nl2br($emailText) ?>
<?php else : ?>
    Здравствуйте!

    Водитель: <?= $model->driver ? $model->driver->getFio() : null; ?>

    Марка: <?= $model->model; ?>

    Номер ТС: <?= $model->state_number; ?>

    Тип кузова: <?= $model->bodyType ? $model->bodyType->name : null; ?>

    Цвет ТС: <?= $model->color; ?>

    <?php if ($model->vehicle_type_id == VehicleType::TYPE_TRACTOR && $model->semitrailerType): ?>
        Полуприцеп: <?= ($model->semitrailerType->model ? ($model->semitrailerType->model . ' ') : null) . ' ' . $model->semitrailerType->state_number; ?>
    <?php elseif ($model->vehicle_type_id == VehicleType::TYPE_WAGON && $model->trailerType): ?>
        Прицеп: <?= ($model->trailerType->model ? ($model->trailerType->model . ' ') : null) . ' ' . $model->trailerType->state_number; ?>
    <?php endif; ?>
<?php endif; ?>
<?php if (isset($emailSignature) && $emailSignature->text): ?>
    <?= $emailSignature->text; ?>
<?php else: ?>
    С уважением,
    <?= $employeeCompany->getFio(true); ?>
<?php endif; ?>
