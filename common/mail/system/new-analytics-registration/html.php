<?php

use common\models\employee\Employee;
use common\models\Company;
use yii\helpers\Inflector;

/* @var $user Employee
 * @var $company Company
 * @var $subject string
 * @var $type integer
 * @var $phone string
 * @var $question string
 */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'systemniye';
$this->params['utm_term'] = Inflector::slug($subject, '_');
?>
<table cellpadding="20" cellspacing="0" style="width:600px;">
    <tr>
        <td>
            <p style="font-size: 24px; line-height: 21px; color: #000; margin-bottom: 20px;">
                <?= $subject; ?>
            </p>
            <p style="margin: 0; font-size: 12pt;">
                Имя:
                <span><?= $name; ?></span>
            </p>
            <p style="margin: 0; font-size: 12pt;">
                E-mail:
                <span><?= $email; ?></span>
            </p>
            <p style="margin: 0; font-size: 12pt;">
                Телефон:
                <span><?= $phone; ?></span>
            </p>
            <p style="margin: 0; font-size: 12pt;">
                Дата и время регистрации:
                <span><?= Yii::$app->formatter->asDateTime($time) ?></span>
            </p>
            <p style="margin: 0; font-size: 12pt;">
                Источник:
                <span><?= $source ?? ''; ?></span>
            </p>
            <p style="margin: 0; font-size: 12pt;">
                Метка:
                <span><?= $regPage ?? ''; ?></span>
            </p>
            <p style="margin: 0; font-size: 12pt;">
                Страна местонахождения бизнеса:
                <span><?= $place ?? ''; ?></span>
            </p>
            <p style="margin: 0; font-size: 12pt;">
                Роль в компании:
                <span><?= $role ?? ''; ?></span>
            </p>
            <p style="margin: 0; font-size: 12pt;">
                Тип бизнеса:
                <span><?= $business ?? ''; ?></span>
            </p>
            <p style="margin: 0; font-size: 12pt;">
                Ответсвенный:
                <span><?= $responsible ?? ''; ?></span>
            </p>
        </td>
    </tr>
</table>