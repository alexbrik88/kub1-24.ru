<?php

use common\models\Company;
use common\models\employee\Employee;

/**
 * @var $message \common\components\sender\unisender\UniSender
 * @var $user Employee
 * @var $company Company
 * @var $subject string
 * @var $type integer
 * @var $phone string
 * @var $question string
 */

?>
<?= $subject; ?>

Имя: <?= $name; ?>
E-mail: <?= $email; ?>
Телефон: <?= $phone; ?>
Дата и время регистрации: <?= Yii::$app->formatter->asDateTime($time); ?>
Источник: <?= $source ?? ''; ?>
Метка: <?= $regPage ?? ''; ?>
Страна местонахождения бизнеса: <?= $place ?? ''; ?>
Роль в компании: <?= $role ?? ''; ?>
Тип бизнеса: <?= $business ?? ''; ?>
Ответсвенный: <?= $responsible ?? ''; ?>