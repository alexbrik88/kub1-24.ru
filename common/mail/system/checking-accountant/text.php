<?php
/* @var \yii\web\View $this */
/* @var $application ApplicationToBank */
?>
<?= $application->company_name; ?>
Заявка на открытие расчетного счета

Название компании: <?= $application->company_name; ?>

ИНН: <?= $application->inn; ?>

Юр.Адрес: <?= $application->legal_address; ?>

ФИО: <?= $application->fio; ?>

Контактный телефон: <?= $application->contact_phone; ?>

Банк: <?= $application->bank->bank_name; ?>



С уважением,
Команда КУБ