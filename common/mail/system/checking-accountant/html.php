<?php
use yii\helpers\Html;
use common\models\company\ApplicationToBank;

/* @var \yii\web\View $this */
/* @var \yii\mail\BaseMessage $message */
/* @var $application ApplicationToBank */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'systemniye';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
?>
<table cellpadding="20" cellspacing="0" style="width:100%;">
    <tr>
        <td>
            <p style="font-size: 24px; line-height: 21px; color: #000; margin-bottom: 20px;">
                Заявка на открытие расчетного счета
            </p>
            <table cellpadding="0" cellspacing="0" style="width:100%;">
                <tr>
                    <td align="left" width="50%" style="background-color: #ffffff; border: 0;">
                        <p style="margin: 0; font-size: 14px; margin-bottom: 20px; margin-left: 0.6cm;">Название компании:</p>
                    </td>
                    <td align="left" width="50%" style="border-right: 20px solid #ffffff; background-color: #ffffff;">
                        <p style="margin: 0; margin-left: 3px; font-size: 14px; margin-bottom: 20px; "><b><?= $application->company_name; ?></b></p>
                    </td>
                </tr>
                <tr>
                    <td align="left" width="50%" style="background-color: #ffffff; border: 0;">
                        <p style="margin: 0; font-size: 14px; margin-bottom: 20px; margin-left: 0.6cm;">ИНН:</p>
                    </td>
                    <td align="left" width="50%" style="border-right: 20px solid #ffffff; background-color: #ffffff;">
                        <p style="margin: 0; margin-left: 3px; font-size: 14px; margin-bottom: 20px; "><b><?= $application->inn; ?></b></p>
                    </td>
                </tr>
                <tr>
                    <td align="left" width="50%" style="background-color: #ffffff; border: 0;">
                        <p style="margin: 0; font-size: 14px; margin-bottom: 20px; margin-left: 0.6cm;">Юр.Адрес:</p>
                    </td>
                    <td align="left" width="50%" style="border-right: 20px solid #ffffff; background-color: #ffffff;">
                        <p style="margin: 0; margin-left: 3px; font-size: 14px; margin-bottom: 20px; "><b><?= $application->legal_address; ?></b></p>
                    </td>
                </tr>
                <tr>
                    <td align="left" width="50%" style="background-color: #ffffff; border: 0;">
                        <p style="margin: 0; font-size: 14px; margin-bottom: 20px; margin-left: 0.6cm;">ФИО:</p>
                    </td>
                    <td align="left" width="50%" style="border-right: 20px solid #ffffff; background-color: #ffffff;">
                        <p style="margin: 0; margin-left: 3px; font-size: 14px; margin-bottom: 20px; "><b><?= $application->fio; ?></b></p>
                    </td>
                </tr>
                <tr>
                    <td align="left" width="50%" style="background-color: #ffffff; border: 0;">
                        <p style="margin: 0; font-size: 14px; margin-bottom: 20px; margin-left: 0.6cm;">Контактный телефон:</p>
                    </td>
                    <td align="left" width="50%" style="border-right: 20px solid #ffffff; background-color: #ffffff;">
                        <p style="margin: 0; margin-left: 3px; font-size: 14px; margin-bottom: 20px; "><b><?= $application->contact_phone; ?></b></p>
                    </td>
                </tr>
                <tr>
                    <td align="left" width="50%" style="background-color: #ffffff; border: 0;">
                        <p style="margin: 0; font-size: 14px; margin-bottom: 20px; margin-left: 0.6cm;">Email:</p>
                    </td>
                    <td align="left" width="50%" style="border-right: 20px solid #ffffff; background-color: #ffffff;">
                        <p style="margin: 0; margin-left: 3px; font-size: 14px; margin-bottom: 20px; "><b><?= $application->contact_email; ?></b></p>
                    </td>
                </tr>
                <tr>
                    <td align="left" width="50%" style="background-color: #ffffff; border: 0;">
                        <p style="margin: 0; font-size: 14px; margin-bottom: 20px; margin-left: 0.6cm;">Банк:</p>
                    </td>
                    <td align="left" width="50%" style="border-right: 20px solid #ffffff; background-color: #ffffff;">
                        <p style="margin: 0; margin-left: 3px; font-size: 14px; margin-bottom: 20px; "><b><?= $application->bank->bank_name; ?></b></p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <p style="margin: 0; font-size: 14px; font-weight: bold;">
                С уважением,<br> Команда КУБ
            </p>
        </td>
    </tr>
</table>
<?= $this->render('@common/mail/layouts/parts/_contacts') ?>