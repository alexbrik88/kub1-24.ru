<?php
use yii\helpers\Html;

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'systemniye';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
        "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}";
?>

<table cellpadding="20" cellspacing="0" style="width:600px;">
    <tr>
        <td>
            <p style="font-size: 24px; line-height: 21px; color: #000; margin-bottom: 20px;">
                Добрый день!
            </p>
            <p style="margin: 0 0 10px; font-size: 12pt; ">
                Аккаунт
                <?php if (!empty($companyName)) : ?>
                    для <?= $companyName ?>
                <?php endif ?>
                переведен на тариф БЕСПЛАТНО.
            </p>
            <p style="margin: 0 0 10px; font-size: 12pt; ">
                Ограничения на тарифе БЕСПЛАТНО:
                <ul>
                    <li>
                        Не более 5 счетов в месяц
                    </li>
                    <li>
                        Только 1 организация. Т.е. если вы владелец более 1-й компании, то вы не сможете добавить еще компании.
                    </li>
                    <li>
                        Не больше 3-х сотрудников
                    </li>
                    <li>
                        Место на диске - 1 ГБ
                    </li>
                </ul>
            </p>
            <p style="margin: 0; font-size: 12pt; ">
                Что бы работать без ограничений нужно перейти на
                <?= Html::a('платный тариф', \Yii::$app->params['uniSender']['lkBaseUrl'] . '/subscribe?' . $linkParams) ?>
            </p>
    </tr>
    <tr>
        <td>
            <p style="margin: 0; font-size: 12pt; font-weight: bold;">
                С уважением,<br> Команда КУБ
            </p>
        </td>
    </tr>
</table>
<?= $this->render('@common/mail/layouts/parts/_contacts') ?>