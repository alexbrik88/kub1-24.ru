<?php

use common\models\company\CompanyType;

/**
 * @var $message \common\components\sender\unisender\UniSender
 * @var $author \common\models\EmployeeCompany
 */
?>
<?= $content; ?>


С уважением,
<?= $author->getFio(true); ?>
<?php if ($author->company->company_type_id != CompanyType::TYPE_IP) : ?>
    <br>
    <?= $author->position; ?>
<?php endif ?>

------------------------------------
Это письмо было отправлено через он-лайн сервис для предпринимателей КУБ
Попробовать бесплатно: <?= Yii::$app->params['serviceSite'] . '?utm_source=lk_server&utm_medium=email&utm_campaign=schet'; ?>