<?php

use common\models\company\CompanyType;
use common\components\helpers\Html;

/**
 * @var $message \common\components\sender\unisender\UniSender
 * @var $subject string
 * @var $content string
 * @var $author \common\models\EmployeeCompany
 */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'progrev';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
    "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}";
?>
<?php Yii::$app->view->beginContent('@common/mail/layouts/document-html.php'); ?>
<table border="0" cellpadding="20" cellspacing="0" style="width: 100%;">
    <tr>
        <td style="font-size: 12pt;">
            <?= $content; ?>
        </td>
    </tr>
</table>
<?php Yii::$app->view->endContent(); ?>
