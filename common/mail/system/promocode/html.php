<?php
use yii\helpers\Html;

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'systemniye';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$lkBaseUrl = \Yii::$app->params['uniSender']['lkBaseUrl'];
?>

<table cellpadding="20" cellspacing="0" style="width:600px;">
    <tr>
        <td>
            <p style="font-size: 24pt; ine-height: 21px; color: #000; margin-bottom: 20px;">
                Вам назначен промокод
            </p>
            <p style="margin: 0; font-size: 12pt; margin-bottom: 20px;">
                Здравствуйте, <?= Html::encode($user->firstname) ?>!
            </p>
            <p style="margin: 0; font-size: 12pt; margin-bottom: 20px;">
                Вам назначен промокод:
                <span style="font-weight: bold;"><?= $promoCode->code; ?></span>
            </p>
            <p style="margin: 0; font-size: 12pt; margin-bottom: 20px;">
                Пройдите по ссылке, что бы ввести
                промокод:<br>
                <a href="<?= $lkBaseUrl ?>/subscribe"
                    target="_blank"
                    style="color: #2E41BA">
                    <?= $lkBaseUrl ?>/subscribe</a>
            </p>
            <p style="margin: 0; font-size: 12pt; margin-bottom: 20px;">
                Если ссылка не работает, скопируйте и
                вставьте URL в адресную строку в новом
                окне браузера.
            </p>
            <p style="margin: 0; font-size: 12pt;">
                По всем вопросам обращайтесь в службу
                поддержки <?= $supportEmail; ?>
            </p>
        </td>
    </tr>
    <tr>
        <td>
            <p style="margin: 0; font-size: 14px; font-weight: bold;">
                С уважением,<br> Команда КУБ
            </p>
        </td>
    </tr>
</table>
