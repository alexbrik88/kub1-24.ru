<?php
use yii\helpers\Html;

$baseUrl = \Yii::$app->params['uniSender']['baseUrl'];
$lkBaseUrl = \Yii::$app->params['uniSender']['lkBaseUrl'];
?>
Здравствуйте, <?= Html::encode($user->firstname) ?>!

Вам назначен промокод: <?= $promoCode->code; ?>

Пройдите по ссылке, что бы ввести промокод: <?= $lkBaseUrl ?>/subscribe

По всем вопросам обращайтесь в службу поддержки <?= $supportEmail ?>

С уважением, Команда КУБ

Вы получили это письмо, потому что подписывались на новости сервиса КУБ на сайте <?= $baseUrl ?>.
У Вас есть возможность отписаться от рассылки, для этого необходимо перейти в личный кабинет <?= $baseUrl ?>/login, в раздел настройки и убрать галочки в блоке «Получение уведомлений».