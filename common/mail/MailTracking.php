<?php

namespace common\mail;

use Yii;
use yii\base\BaseObject;
use yii\helpers\Html;

class MailTracking extends BaseObject
{
    public static function getTag($options = null)
    {
        return Html::img(static::getUrl($options));
    }

    public static function getCid()
    {
        $ga = !empty(Yii::$app->request->cookies)? Yii::$app->request->cookies->getValue('_ga'): null;

        if (!empty($ga)) {
            list($version,$domainDepth, $cid1, $cid2) = split('[\.]', $_COOKIE["_ga"],4);
            return $cid1.'.'.$cid2;
        } else {
            return uniqid();
        }
    }

    public static function getOptions()
    {
        return [
            'v' => 1,
            'tid' => \Yii::$app->params['GATID'],
            'cid' => static::getCid(),
            'uid' => !empty(Yii::$app->user)? Yii::$app->user->id: null,
            't' => 'event',
            'ec' => 'email',
            'ea' => 'open',
        ];
    }

    public static function getUrl($options = null)
    {
        $params = is_array($options)? array_merge(static::getOptions(), $options): static::getOptions();
        return 'https://www.google-analytics.com/collect?' . http_build_query($params);
    }
}