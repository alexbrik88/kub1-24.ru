<?php
use yii\helpers\Html;
use yii\helpers\Inflector;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */

$utm_source = !empty($this->params['utm_source']) ? $this->params['utm_source'] : 'unisender';
$utm_medium = !empty($this->params['utm_medium']) ? $this->params['utm_medium'] : 'email';
$utm_campaign = !empty($this->params['utm_campaign']) ? $this->params['utm_campaign'] :
    ((!empty($message) && $message instanceof \yii\swiftmailer\Message) ? 'systemniye' : 'progrev');
$utm_term = !empty($this->params['utm_term']) ? $this->params['utm_term'] :
    (!empty($message) ? Inflector::slug($message->subject, '_') : '');
$linkParams = "utm_source={$utm_source}&amp;utm_medium={$utm_medium}&amp;utm_campaign={$utm_campaign}&amp;utm_term={$utm_term}";
$imgSrc = $this->params['img_src'] ?? null;
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body style="margin: 0px auto; padding: 0px; background-color: rgb(255, 255, 255); cursor: auto;">
<?php $this->beginBody() ?>
<table align="center" cellpadding="0" cellspacing="0" style="border-radius: 10px 10px 10px 10px; min-width:320px; max-width:600px; background-color:#ffffff; margin-bottom: 20px">
    <tbody>
        <?php if ($imgSrc) : ?>
        <!-- IMG -->
        <tr>
            <td>
            <table align="center" cellpadding="0" cellspacing="0" style="min-width:320px; max-width:600px">
                <tbody>
                    <tr>
                        <td style="text-align: center;">
                            <?= Html::img($imgSrc, [
                                'style' => 'min-width:320px; max-width:600px; max-height:240px;',
                                'alt' => 'Картинка',
                            ]) ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            </td>
        </tr>
        <!-- END IMG -->
        <?php endif ?>
        <!-- CONTENT PART1-->
        <tr>
            <td>
            <?= $content ?>
            </td>
        </tr>
        <!-- END CONTENT PART1 --><!-- FOOTER -->
        <tr>
            <td>
            <table align="center" cellpadding="0" cellspacing="0" style="color:#000000; font-family:Arial, sans-serif; min-width:320px; max-width:600px; border-top:1px dashed #cccccc; padding-top:20px">
                <tbody>
                    <tr align="center" style="marin-top:20px; margin-bottom:20px">
                        <td>
                            <a href="https://kub-24.ru/login?<?= $linkParams ?>" style="color:#ffffff; text-decoration:none; font-family:Arial,Helvetica,sans-serif; padding: 5px 10px 5px 10px; background-color: #4679ae; border-radius:4px;">Войти в КУБ24</a>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="padding:20px 0px 10px 0px;">
                            <a href="https://www.facebook.com/kub24ru/" style="text-decoration: none;" title="Перейти в Фейсбук">
                                <img alt="Мы в Фейсбук" src="https://lk.kub-24.ru/img/ico-sotial/facebook.png" />
                            </a>
                            <a href="https://vk.com/kub24ru" style="text-decoration: none; margin-left:10px;" title="Перейти в Вконтакте">
                                <img alt="Мы в Вконтакте" src="https://lk.kub-24.ru/img/ico-sotial/vkontakte.png" />
                            </a>
                            <a href="https://www.instagram.com/kub24ru/" style="text-decoration: none; margin-left:10px;" title="Перейти в Инстаграм">
                                <img alt="Мы в Инстаграм" src="https://lk.kub-24.ru/img/ico-sotial/instagram.png" />
                            </a>
                            <a href="https://play.google.com/store/apps/details?id=com.kub.android" style="text-decoration: none; margin-left:10px;" title="Перейти в Google Play">
                                <img alt="Мы в Google Play" src="https://lk.kub-24.ru/img/ico-sotial/google-play.png" />
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" style="font-size:10px; padding:10px 20px 10px 20px; ">
                            Вы получили это письмо, потому что подписывались на новости сервиса КУБ24 на сайте kub-24.ru.
                            <br />
                            У Вас есть возможность отписаться от рассылки, для этого необходимо перейти в
                            <a href="https://kub-24.ru/login/?<?= $linkParams ?>" style="color:#0044cc; font-weight:normal; text-decoration:underline;" title="Перейти в личный кабинет">личный кабинет</a>,
                            в раздел настройки и убрать галочки в блоке &laquo;Получение уведомлений&raquo;.
                        </td>
                    </tr>
                </tbody>
            </table>
            </td>
        </tr>
        <!-- END FOOTER -->
    </tbody>
</table>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
