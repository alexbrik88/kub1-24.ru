<?php
use yii\helpers\Html;
use yii\helpers\Inflector;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */

$utm_source = !empty($this->params['utm_source']) ? $this->params['utm_source'] : 'unisender';
$utm_medium = !empty($this->params['utm_medium']) ? $this->params['utm_medium'] : 'email';
$utm_campaign = !empty($this->params['utm_campaign']) ? $this->params['utm_campaign'] :
    ((!empty($message) && $message instanceof \yii\swiftmailer\Message) ? 'systemniye' : 'progrev');
$utm_term = !empty($this->params['utm_term']) ? $this->params['utm_term'] :
    (!empty($message) ? Inflector::slug($message->subject, '_') : '');
$linkParams = "utm_source={$utm_source}&amp;utm_medium={$utm_medium}&amp;utm_campaign={$utm_campaign}&amp;utm_term={$utm_term}";
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body style="margin: 0px auto; padding: 0px; background-color: rgb(222, 224, 226); cursor: auto;">
<style type="text/css">
table {
    table-layout: fixed;
}
td {
    word-wrap:break-word;
}
</style>
<?php $this->beginBody() ?>
<table align="center" cellpadding="0" cellspacing="0" style="border:1px solid #cccccc; width:600px; background-color:#ffffff; font-family:Arial,Helvetica,sans-serif;">
    <tbody>
        <?php if (!empty($this->params['companyName'])) : ?>
            <tr><!-- HEADER -->
                <td>
                <table cellpadding="20" cellspacing="0" style="border-bottom:4px solid #4276a5; width:100%;">
                    <tbody>
                        <tr>
                            <td style="text-align:left;">
                                <div style="font-size: 24px; line-height: 24px; color: #000;"><?= $this->params['companyName']; ?></div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                </td>
            </tr>
            <!-- END HEADER -->
        <?php endif ?>
        <tr>
            <td>
            <?= $content ?>
            </td>
        </tr>
        <!-- FOOTER -->
        <tr>
            <td>
            <table cellpadding="0" cellspacing="0" style="width: 100%;">
                <tbody>
                    <tr>
                        <td style="font-size:10px; padding:10px 20px 10px 20px; border-top:1px solid #cccccc">
                            <table cellpadding="0" cellspacing="0" style="width: 100%;">
                                <tbody>
                                    <tr>
                                        <td width="80%" align="left" valign="top" style="color:#505050; line-height:15px; font-size:10px;">
                                            <p style="margin: 0;">
                                                Это письмо было отправлено через сервис выставления счетов
                                                <br>
                                                <?= Html::a('Попробовать бесплатно', Yii::$app->params['serviceSite'] .
                                                    "?" . $linkParams . "&utm_content=try_free", [
                                                    'target' => '_blank',
                                                    'style' => 'color: #055EC3;',
                                                ]); ?>
                                                <?php if (!empty($this->params['pixel'])) : ?>

                                                <?php endif ?>
                                            </p>
                                        </td>
                                        <td width="20%" align="right" valign="middle">
                                            <?= Html::a(Html::img('https://lk.kub-24.ru/img/fav.svg', [
                                                'style' => 'style="border-bottom-width:0px; border-left-width: 0px; border-right-width: 0px; border-top-width: 0px;',
                                                'width' => '24px',
                                                'height' => '24px',
                                            ]), Yii::$app->params['serviceSite'] . "?" . $linkParams . "&utm_content=logo", [
                                                'target' => '_blank',
                                            ]); ?>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </td>
                    </tr>
                </tbody>
            </table>
            </td>
        </tr>
        <!-- END FOOTER -->
    </tbody>
</table>

<?php if (empty($this->params['pixel'])) : ?>
    <?= \common\mail\MailTracking::getTag() ?>
<?php else : ?>
    <?= $this->render('parts/_pixel', $this->params['pixel']) ?>
<?php endif ?>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
