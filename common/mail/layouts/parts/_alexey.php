<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
?>

<table cellpadding="0" cellspacing="0" style="width: 100%; border-bottom:1px dashed #cccccc; background-color:#ffffff;">
    <tbody>
        <tr style="color: #333333; font-family:Arial,Helvetica,sans-serif; font-size:16px !important; font-weight: 600;">
            <td style="width: 180px;">
                <div style="display: inline-block; padding: 20px 20px 20px 40px;">
                    <?= Html::img('https://lk.kub-24.ru/img/unisender/kushchenko-a.png', [
                        'width' => 120,
                        'height' => 120,
                    ]); ?>
                </div>
            </td>
            <td style="line-height:100%; text-align:left; vertical-align:center; padding-left: 20px;">
                <div style="margin: 16px 0;">С уважением,</div>
                <div style="margin: 16px 0;">Алексей Кущенко,</div>
                <div style="margin: 16px 0;">Руководитель КУБ24</div>
            </td>
        </tr>
    </tbody>
</table>