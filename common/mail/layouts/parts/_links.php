<?php

use yii\helpers\ArrayHelper;

$baseUrl = trim(ArrayHelper::getValue(Yii::$app->params, 'serviceSiteLk'), '/');

?>

<table cellpadding="0" cellspacing="0" style="color:#989896; font-family:Arial,Helvetica,sans-serif; width: 100%; margin: 10px 0;">
    <tbody>
        <tr>
            <td align="center" style="padding: 5px 10px 5px 0;">
                <a href="tel:+78005005436" style="color:#989896; text-decoration: none;">+7 (800) 500-5436</a>
            </td>
            <td align="center" style="padding: 5px 10px;">
                Наше приложение:
            </td>
            <td align="center" style="padding: 5px 10px;">
                Добавляйтесь в друзья:
            </td>
        </tr>
        <tr>
            <td align="center" valign="top" style="padding: 5px 10px 5px 0;">
                <a href="mailto:support@kub-24.ru" style="color:#989896; text-decoration: none;">support@kub-24.ru</a>
            </td>
            <td align="center" style="padding: 5px 10px;">
                <a href="https://play.google.com/store/apps/details?id=com.kub.android" style="padding: 6px 8px; border: 1px solid #989896; border-radius: 5px; line-height: 22px;">
                    <img alt="android" src="<?= $baseUrl ?>/img/android/google-play.png" style="max-width: 100px; vertical-align: top;"/>
                </a>
            </td>
            <td align="center" style="padding: 5px 10px;">
                <table  cellpadding="0" cellspacing="0" style="width: 100%;">
                    <col width="1*" />
                    <col width="1*" />
                    <col width="1*" />
                    <tbody>
                        <tr>
                            <td align="center">
                                <a href="https://vk.com/kub24ru" style="text-decoration:none;" title="Перейти в Вконтакте">
                                    <img alt="Мы в Вконтакте" src="<?= $baseUrl ?>/img/unisender/vk.png" />
                                </a>
                            </td>
                            <td align="center">
                                <a href="https://www.facebook.com/kub24ru/" style="text-decoration:none;" title="Перейти в Фейсбук">
                                    <img alt="Мы в Фейсбук" src="<?= $baseUrl ?>/img/unisender/facebook.png" />
                                </a>
                            </td>
                            <td align="center">
                                <a href="https://www.instagram.com/kub24ru/" style="text-decoration:none;" title="Перейти в Инстаграм">
                                    <img alt="Мы в Инстаграм" src="<?= $baseUrl ?>/img/unisender/insta.jpg" />
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
    </tbody>
</table>