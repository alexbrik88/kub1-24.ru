<?php

use common\models\reference\Pixel;
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */

$pixel = new Pixel([
    'company_id' => $company_id,
    'email' => $email,
]);

if ($pixel->save(false)) {
    $data = [
        'v=1',
        't=event',
        'tid=' . Yii::$app->params['GATID'],
        'cid=' . $pixel->id,
        'ec=email',
        'ea=open',
        'cm=email',
        'cs=newsletter',
        'dp=%2Femail%2Finvoice',
        'el=' . $pixel->id,
    ];

    echo Html::img('https://www.google-analytics.com/collect?' . implode('&', $data), [
        'alt' => '',
    ]);
}
