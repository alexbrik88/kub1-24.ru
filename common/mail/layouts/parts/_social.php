
<table cellpadding="0" cellspacing="0" style="color:#505050; font-family:Arial,Helvetica,sans-serif; width: 100%;">
    <tbody>
        <tr>
            <td align="center" style="padding:20px 0px 10px 0px;">
                <a href="https://www.facebook.com/kub24ru/" style="text-decoration:none;" title="Перейти в Фейсбук">
                    <img alt="Мы в Фейсбук" src="https://lk.kub-24.ru/img/unisender/facebook.png" />
                </a>
                <a href="https://vk.com/kub24ru" style="margin-left:10px; text-decoration:none;" title="Перейти в Вконтакте">
                    <img alt="Мы в Вконтакте" src="https://lk.kub-24.ru/img/unisender/vk.png" />
                </a>
                <a href="https://telegram.me/joinchat/EFV9EAnpdIDfe4dkWmmTvQ" style="margin-left:10px; text-decoration:none;" title="Перейти в Телеграм">
                    <img alt="Мы в Телеграм" src="https://lk.kub-24.ru/img/unisender/tgram.jpg" />
                </a>
                <a href="https://www.instagram.com/kub24ru/" style="margin-left:10px; text-decoration:none;" title="Перейти в Инстаграм">
                    <img alt="Мы в Инстаграм" src="https://lk.kub-24.ru/img/unisender/insta.jpg" />
                </a>
                <a href="https://twitter.com/kub24ru" style="margin-left:10px; text-decoration:none;" title="Перейти в Твитер">
                    <img alt="Мы в Твитер" src="https://lk.kub-24.ru/img/unisender/twitter.png" />
                </a>
            </td>
        </tr>
    </tbody>
</table>