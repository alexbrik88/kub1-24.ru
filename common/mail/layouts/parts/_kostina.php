<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
?>
<table cellpadding="20" cellspacing="0" style="width: 100%; border-bottom:1px dashed #cccccc;">
    <tbody><!-- line 1 -->
        <tr>
            <td style="background-color:#ffffff;">
            <!-- Colum 1 -->
            <table align="left" cellpadding="20" cellspacing="0" style="display:inline; width:130px;">
                <tbody>
                    <tr>
                        <td style="color:#333333; font-family:Arial,Helvetica,sans-serif; line-height:150%; padding-top:0; padding-right:0; padding-bottom:0px; padding-left:10; text-align:left; width:130px; background-color:#ffffff; ">
                            <?= Html::img('https://lk.kub-24.ru/img/unisender/kostina-n.png', [
                                'width' => 120,
                                'height' => 120,
                            ]); ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <!-- END Colum 1 -->

            <!-- Colum 2 -->
            <table align="right" cellpadding="20" cellspacing="0" style="display:inline; width:390px; height:120;">
                <tbody>
                    <tr>
                        <td style="color:#333333; font-family:Arial,Helvetica,sans-serif; line-height:150%; padding-top:0; padding-right:0; padding-bottom:0px; padding-left:0; text-align:left; width:390px; height:120; background-color:#ffffff; vertical-align:center;">
                        <h2 style="font-size:16px !important;line-height:100% !important; font-weight:600">Руководитель отдела бухгалтерии,</h2>

                        <h2 style="font-size:16px !important;line-height:100% !important; font-weight:600">Наталья Костина,</h2>

                        <h2 style="font-size:16px !important;line-height:100% !important; font-weight:600">Бухгалтерская компания «СМАРТ+»</h2>
                        </td>
                    </tr>
                </tbody>
            </table>
            <!-- END Colum 2 -->
            </td>
        </tr>
    </tbody>
</table>