<table cellpadding="0" cellspacing="0" style="color:#505050; font-family:Arial,Helvetica,sans-serif; width: 100%;">
    <tbody>
        <tr>
            <td align="center" style="padding:20px 0px 10px 0px; border-top: 1px solid #cccccc;">
                <h3 style="font-size:22px !important;line-height:100% !important; font-weight:400">
                    Нужна помощь?<br />
                    <span style="font-size:12px; line-height:100% ">
                        Cвяжитесь с нами через социальные сети, по почте <u>support@kub-24.ru</u>
                        <br />или звоните на бесплатный номер.
                    </span>
                </h3>
                <a href="https://www.facebook.com/kub24ru/" style="text-decoration:none;" title="Перейти в Фейсбук">
                    <img alt="Мы в Фейсбук" src="https://lk.kub-24.ru/img/unisender/facebook.png" />
                </a>
                <a href="https://vk.com/kub24ru" style="margin-left:10px;text-decoration:none;" title="Перейти в Вконтакте">
                    <img alt="Мы в Вконтакте" src="https://lk.kub-24.ru/img/unisender/vk.png" />
                </a>
                <a href="https://telegram.me/joinchat/EFV9EAnpdIDfe4dkWmmTvQ" style="margin-left:10px;text-decoration:none;" title="Перейти в Телеграм">
                    <img alt="Мы в Телеграм" src="https://lk.kub-24.ru/img/unisender/tgram.jpg" />
                </a>
                <a href="https://www.instagram.com/kub24ru/" style="margin-left:10px;text-decoration:none;" title="Перейти в Инстаграм">
                    <img alt="Мы в Инстаграм" src="https://lk.kub-24.ru/img/unisender/insta.jpg" />
                </a>
                <a href="https://twitter.com/kub24ru" style="margin-left:10px;text-decoration:none;" title="Перейти в Твитер">
                    <img alt="Мы в Твитер" src="https://lk.kub-24.ru/img/unisender/twitter.png" />
                </a>
                <h3 style="padding-bottom:0; font-size:24px !important;line-height:100% !important; font-weight:300">
                    8 800 500-54-36<br />
                    <span style="font-size:16px; ">Техническая поддержка</span>
                </h3>
            </td>
        </tr>
    </tbody>
</table>
