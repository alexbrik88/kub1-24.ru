<?php
use yii\helpers\Html;
use yii\helpers\Inflector;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $content string main view render result */

$utm_source = !empty($this->params['utm_source']) ? $this->params['utm_source'] : 'unisender';
$utm_medium = !empty($this->params['utm_medium']) ? $this->params['utm_medium'] : 'email';
$utm_campaign = !empty($this->params['utm_campaign']) ? $this->params['utm_campaign'] :
    ((!empty($message) && $message instanceof \yii\swiftmailer\Message) ? 'systemniye' : 'progrev');
$utm_term = !empty($this->params['utm_term']) ? $this->params['utm_term'] :
    (!empty($message) ? Inflector::slug($message->subject, '_') : '');
$linkParams = "utm_source={$utm_source}&amp;utm_medium={$utm_medium}&amp;utm_campaign={$utm_campaign}&amp;utm_term={$utm_term}";
?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=<?= Yii::$app->charset ?>" />
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body style="margin: 0px auto; padding: 0px; background-color: rgb(222, 224, 226); cursor: auto;">
<style type="text/css">
table {
    table-layout: fixed;
}
td {
    word-wrap:break-word;
}
</style>
<?php $this->beginBody() ?>
<table align="center" cellpadding="0" cellspacing="0" style="border:1px solid #cccccc; width:600px; background-color:#ffffff; font-family:Arial,Helvetica,sans-serif;">
    <tbody>
        <tr><!-- HEADER -->
            <td>
            <table cellpadding="20" cellspacing="0" style="border-bottom:4px solid #4276a5; width: 100%;">
                <tbody>
                    <tr>
                        <td style="text-align:left;">
                            <a href="<?=\Yii::$app->params['serviceSite']?>?<?= $linkParams ?>&amp;utm_content=logo" title="КУБ">
                                <?= $this->render('parts/_logo') ?>
                            </a>
                        </td>
                        <td style="text-align:right; width:150px">
                            <a href="<?=trim(\Yii::$app->params['serviceSite'], '/')?>/login/?<?= $linkParams ?>&amp;utm_content=login_top"
                                style="color:#4276a5; font-weight:bold; text-decoration:underline; font-family:Arial,Helvetica,sans-serif;" title="Войти">
                                Войти в КУБ
                            </a>
                        </td>
                    </tr>
                </tbody>
            </table>
            </td>
        </tr>
        <!-- END HEADER -->
        <tr>
            <td>
            <?= $content ?>
            </td>
        </tr>
        <!-- FOOTER -->
        <tr>
            <td>
            <table cellpadding="0" cellspacing="0" style="color:#505050; font-family:Arial,Helvetica,sans-serif; width: 100%;">
                <tbody>
                    <tr>
                        <td style="font-size:10px; padding:10px 20px 10px 20px; border-top:1px solid #cccccc">
                            Вы получили это письмо, потому что подписывались на новости сервиса КУБ на сайте kub-24.ru.<br />
                            У Вас есть возможность отписаться от рассылки, для этого необходимо перейти в
                            <a href="<?=trim(\Yii::$app->params['serviceSite'], '/')?>/login/?<?= $linkParams ?>&amp;utm_content=login_bottom"
                                style="color:#4276a5; font-weight:normal; text-decoration:underline;" title="Перейти в личный кабинет">личный кабинет</a>,
                            в раздел настройки и убрать галочки в блоке &laquo;Получение уведомлений&raquo;.
                        </td>
                    </tr>
                </tbody>
            </table>
            </td>
        </tr>
        <!-- END FOOTER -->
    </tbody>
</table>
<?php //echo \common\mail\MailTracking::getTag() ?>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
