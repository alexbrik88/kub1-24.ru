<?php
/**
 * @var $message \common\components\sender\unisender\UniSender
 */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'progrev';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
        "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}";
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/html.php'); ?>

<table cellpadding="20" cellspacing="0" style="width:600px;">
    <tr>
        <td>
            <p style="font-size: 24pt; line-height: 21px; color: #000;">
                Чек-лист по изучению КУБа.
            </p>
        </td>
    </tr>
    <tr>
        <td style="font-size: 12pt;">
            <p style="margin: 0 0 20px; font-size: 12pt; padding-left: 80px;">
                Шаг 1: Простая навигация по сервису
            </p>
            <p style="margin: 20px 0; font-size: 12pt; text-align: center;">
                <a href="https://vimeo.com/217134783" target="_blank">
                    <img src="<?php echo $message->getDataURI('video_play.png'); ?>"
                        width="195" height="110"
                        style="border-bottom-width: 0; border-left-width: 0px; border-right-width: 0px; border-top-width: 0px;">
                </a>
            </p>
            <p style="margin: 20px 0; color: #6B6B6B; padding-left: 80px;">
                    После просмотра видео вы сможете<br>
                    - Найти все, что вам нужно в сервисе КУБ.<br>
                    - Просто вводить информацию.<br>
                    - Изменять КУБ, что бы он отвечал вашим потребностям.
            </p>
            <p style="margin: 20px 0; padding-left: 80px;">
                Шаг 2: Выставляйте счета на ходу.
            </p>
            <p style="margin: 20px 0; padding-left: 80px;">
                Шаг 3: Получайте оплату ваших счетов быстрее.
            </p>
        </td>
    </tr>
    <tr>
        <td style="text-align: center;">
            <a href="http://lk.kub-24.ru/site/login/?<?= $linkParams ?>&amp;utm_content=login-bottom"
                target="_blank" style="color: #000;font-weight: bold;font-size: 14pt;"
                rel="noopener" data-snippet-id="2df5426d-d48c-ef22-23db-ed6cd8d5ef84">ВОЙТИ В КУБ</a>
        </td>
    </tr>
</table>
<?= $this->render('@common/mail/layouts/parts/_contacts') ?>

<?php \Yii::$app->view->endContent(); ?>
