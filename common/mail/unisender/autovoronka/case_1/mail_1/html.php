<?php

use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $subject string */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'progrev';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
        "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}";

$url = 'http://robot.kub-24.ru/?utm_source=unisender&utm_medium=email&utm_campaign=letter_4_rukovodstvo_po zapolneniyu_deklarazii';
$link = 'https://kub-24.ru/2019/12/02/2020-god-stanet-osobennym-dlya-vseh-ip/?utm_source=unisender&utm_medium=email&utm_campaign=letter_1_4';
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/taxrobot-html.php'); ?>

<?= $this->render('../../_image', [
    'src' => 'https://lk.kub-24.ru/img/unisender/delivery/ip_usn-6/letter-3_2.png',
]); ?>

<table align="center" cellpadding="0" cellspacing="0" style="width: 100%; background-color:#ffffff">
<tbody><!-- CONTENT PART1-->
<tr>
<td>
<table cellpadding="20" cellspacing="0" style="width: 100%; border-bottom:1px solid #cccccc;">
<tbody>
<tr>
<td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
<h1 style="margin-top:0; font-size:28px !important; line-height:150% !important; font-weight:600; color:#333333">
    Привет{{Name?, }}{{Name}}!
</h1>
Есть несколько способов <strong>рассчитать налоги, подготовить платежку и налоговую декларацию без ошибок</strong>.
Вот эти способы:
<ul style="list-style-type: decimal;">
<li>Найти <a href="<?= $link ?>">бухгалтера</a>, чтобы он всё сделал за вас.</li>
<li>Воспользоваться <a href="<?= $link ?>">онлайн бухгалтерией</a>, разобраться в ней и сделать всё самому.</li>
<li>Воспользоваться <a href="<?= $link ?>">программой по заполнению налоговой декларации</a>, которая так же сама рассчитает налоги.</li>
<li>Рассчитать налоги самостоятельно, <a href="<?= $link ?>">скачать бланк налоговой декларации</a> и заполнить его самостоятельно.</li>
</ul>
Все плюсы и минусы варианта, который вы используете или собираетесь использовать, вы можете узнать, кликнув на него.
<br>
<br>
Если говорить про стоимость, то самый дешевый – это заполнить налоговую декларацию самостоятельно. НО этот же вариант самый рискованный с точки зрения ошибок и самый затратный по времени.
<br>
<strong>В случае самостоятельного заполнения декларации, не забудьте:</strong>
<ul>
<li>
Форма декларация может измениться, обязательно отслеживайте ее актуальность.
</li>
<li>
Перед оплатой налогов, нужно проверить, не изменились ли реквизиты вашей налоговой.
</li>
<li>
Проверьте, не изменился ли КБК
</li>
<li>
Штраф за несвоевременную подачу декларации в размере 5% от суммы налога за каждый месяц или неполный месяц просрочки и до 30% от суммы налога, если вы просрочили подачу декларации больше чем на полгода.
</li>
<li>
Блокировка расчетного счета через 10 дней после крайнего срока подачи декларации.
</li>
<li>
ИП несет ответственность своим имуществом .
</li>
<li>
Если хотите <strong>заплатить меньше налогов</strong>, то нужно в этом разобраться.
</li>
</ul>

Для тех, кто готов заполнить бланк налоговой декларации самостоятельно, то скачайте в приложении
<strong>руководство по заполнению налоговой декларации для ИП на УСН 6%</strong>
<br>
<br>
Если вы <strong>НЕ ХОТИТЕ тратить время на заполнение</strong>
и/или <strong>хотите быть уверенными</strong> в отсутствии ошибок в налоговой декларации
и правильности расчета налогов, то для вас:
<a href="<?= $url ?>">
    Программа по заполнению налоговой декларации и расчету налогов
</a>
<br>
<br>
В следующем письме расскажу, про ИП, которые могут законно платить меньше налогов
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<!-- END CONTENT PART1 -->
</tbody>
</table>

<?= $this->render('../../_sender', [
    'url' => $url,
]); ?>

<?= $this->render('@common/mail/layouts/parts/_links') ?>

<?php \Yii::$app->view->endContent(); ?>
