КУБ - Онлайн сервис для предпринимателей

(https://kub-24.ru/?utm_source=unisender&utm_medium=email&utm_campaign=news&utm_content=ip-usn-6-letter-2&utm_term=logo)
Войти в КУБ (https://kub-24.ru/login/?utm_source=unisender&utm_medium=email&utm_content=ip-usn-6-letter-2&utm_term=login_top)

Привет{{Name?, }}{{Name}}! Вы знаете, что налоговая получила право принудительно закрывать ИП ?
Одна из причин, по которой это происходит не вовремя сданная отчетность и уплаченные налоги! Подробнее об этом расскажу в следующем письме.
Что бы не забывать про сроки уплаты налогов и подачи налоговой декларации.
В приложении налоговый календарь для ИП на УСН 6%
Так же предлагаем вам записать себе в календарь основные даты налогового календаря для ИП на УСН 6%:

Добавить в Календарь Google (Вы должны быть авторизованы в системе Google Calendar)
https://calendar.google.com/calendar/embed?src=smart-pls.ru_hcpoq4apjfjhmd2ldauqrsae9s%40group.calendar.google.com&ctz=Europe%2FMoscow

Добавить в календарь (*.ics)
https://calendar.google.com/calendar/ical/smart-pls.ru_hcpoq4apjfjhmd2ldauqrsae9s%40group.calendar.google.com/public/basic.ics

В следующем письме расскажу, на каких основаниях налоговая сможет закрыть Ваше ИП.

Алексей Кущенко,КУБ-24 Программа для ИП по заполнению налоговой декларации и расчету налогов
http://robot.kub-24.ru/?utm_source=unisender&utm_medium=email&utm_campaign=letter_3_calendar_ip+google

(https://www.facebook.com/kub24ru/) (https://vk.com/kub24ru) (https://telegram.me/joinchat/EFV9EAnpdIDfe4dkWmmTvQ) (https://www.instagram.com/kub24ru/) (https://twitter.com/kub24ru)

Вы получили это письмо, потому что подписывались на новости сервиса КУБ на сайте kub-24.ru.
У Вас есть возможность отписаться от рассылки, для этого необходимо перейти в личный кабинет (https://kub-24.ru/login/?utm_source=unisender&utm_medium=email&utm_content=ip-usn-6-letter-3&utm_term=login_bottom), в раздел настройки и убрать галочки в блоке «Получение уведомлений».