<?php

use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $subject string */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'progrev';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
        "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}";

$url = 'http://robot.kub-24.ru/?utm_source=unisender&utm_medium=email&utm_campaign=letter_3_calendar_ip+google';
$url1 = 'https://calendar.google.com/calendar/embed?src=smart-pls.ru_hcpoq4apjfjhmd2ldauqrsae9s%40group.calendar.google.com&ctz=Europe%2FMoscow';
$url2 = 'https://calendar.google.com/calendar/ical/smart-pls.ru_hcpoq4apjfjhmd2ldauqrsae9s%40group.calendar.google.com/public/basic.ics';
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/taxrobot-html.php'); ?>

<?= $this->render('../../_image', [
    'src' => 'https://lk.kub-24.ru/img/unisender/delivery/ip_usn-6/letter-kalendar.png',
]); ?>

<table align="center" cellpadding="0" cellspacing="0" style="width:100%; background-color:#ffffff">
<tbody>
<!-- CONTENT PART1-->
<tr>
<td>
<table cellpadding="20" cellspacing="0" style="width:100%; border-bottom:1px solid #cccccc;">
<tbody>
<tr>
<td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
<h1 style="margin-top:0; font-size:28px !important; line-height:150% !important; font-weight:600; color:#333333">Привет{{Name?, }}{{Name}}!</h1>
Вы знаете, что <u><strong>налоговая получила право принудительно закрывать ИП</strong></u> ?
<br>
<br>
Одна из причин, по которой это происходит не вовремя сданная отчетность и уплаченные налоги!
Подробнее об этом расскажу в следующем письме.
<br>
<br>
Что бы <strong>не забывать про сроки уплаты налогов и подачи налоговой декларации</strong>.
<br>
В приложении налоговый календарь для ИП на УСН 6%
<br>
<br>
Так же предлагаем вам записать себе в календарь основные даты налогового календаря для ИП на УСН 6%:
<br>
<a href="<?= $url1 ?>">Добавить в Календарь Google</a> (Вы должны быть авторизованы в системе Google Calendar)
<br>
<a href="<?= $url2 ?>">Добавить в календарь</a> (*.ics)
<br>
<br>
<u>В следующем письме</u> для тех, кто готов заполнить налоговую декларацию самостоятельно, пришлю пошаговую инструкцию. Даже если вы не хотите это делать, сохраните, такого в интернете не найдете.</td>
</tr>
</tbody>
</table>
</td>
</tr>
<!-- END CONTENT PART1 -->
</tbody>
</table>

<?= $this->render('../../_sender', [
    'url' => $url,
]); ?>

<?= $this->render('@common/mail/layouts/parts/_links') ?>

<?php \Yii::$app->view->endContent(); ?>
