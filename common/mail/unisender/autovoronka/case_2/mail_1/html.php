<?php

use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $subject string */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'progrev';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
        "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}";
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/taxrobot-html.php'); ?>

<?= $this->render('../../_image', [
    'src' => 'https://lk.kub-24.ru/img/unisender/delivery/ip_usn-6/letter-1_2.png',
]); ?>

<table align="center" cellpadding="0" cellspacing="0" style="width: 100%; background-color:#ffffff">
<tbody><!-- CONTENT PART1-->
<tr>
<td>
<table cellpadding="20" cellspacing="0" style="width: 100%; border-bottom:1px solid #cccccc;">
<tbody>
<tr>
<td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
<h1 style="margin-top:0; font-size:28px !important; line-height:150% !important; font-weight:600; color:#333333">Привет{{Name?, }}{{Name}}!</h1>
Меня зовут Алексей!
<br>
Мы поговорили с банками и предпринимателями и собрали все причины <strong>блокировки</strong> банком расчетного счета Индивидуального Предпринимателя.<br>
<br>
Сохраните чек-лист, что бы он напоминал вам, что нужно учитывать при работе с банками.
<br>
Блокировка счета вашего ИП:
<ul>
<li>Это <strong>куча потраченного времени</strong> на объяснения с банком и предоставление документов.</li>
<li>Это <strong>зависшие деньги на счете</strong>, которыми вы не можете ни чего оплатить.</li>
<li>Это <strong>срочное открытие расчетного счета в другом банке</strong>, чтобы бизнес не был парализован.</li>
</ul>
Чек-лист нужно регулярно просматривать, что бы вы на подсознательном уровне помнили, какие действия нельзя совершать.<br>
<br>
<strong>P.S.</strong> Вы уже слышали, что <strong>ФНС получила право принудительно исключать ИП из ЕГРИП?</strong><br>
Нет? В следующем письме расскажу, как такое возможно и за что Ваше ИП закроют.<br>
<br>
Успехов вам и вашему бизнесу!</td>
</tr>
</tbody>
</table>
</td>
</tr>
<!-- END CONTENT PART1 -->
</tbody>
</table>

<?= $this->render('../../_sender', [
    'url' => 'http://robot.kub-24.ru/?utm_source=unisender&utm_medium=email&utm_campaign=letter_1_10_prichin',
]); ?>

<?= $this->render('@common/mail/layouts/parts/_links') ?>

<?php \Yii::$app->view->endContent(); ?>
