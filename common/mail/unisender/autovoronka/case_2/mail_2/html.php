<?php

use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $subject string */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'progrev';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
        "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}";
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/taxrobot-html.php'); ?>

<?= $this->render('../../_image', [
    'src' => 'https://lk.kub-24.ru/img/unisender/delivery/ip_usn-6/letter-2_2.png',
]); ?>

<table align="center" cellpadding="0" cellspacing="0" style="width:100%; background-color:#ffffff">
<tbody>
<!-- CONTENT PART1-->
<tr>
<td>
<table cellpadding="20" cellspacing="0" style="width:100%; border-bottom:1px solid #cccccc;">
<tbody>
<tr>
<td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
<h1 style="margin-top:0; font-size:28px !important; line-height:150% !important; font-weight:600; color:#333333">Привет{{Name?, }}{{Name}}!</h1>
Обычно новости от государства для предпринимателей не радостные… эта, к сожалению, такая же :(<br>
<br>
С 1 сентября 2020 года в силу вступят поправки к закону «О государственной регистрации юридических лиц и индивидуальных предпринимателей»<br>
Согласно этому закону, теперь для ИП вводится новая форма наказания – принудительное исключение ИП из ЕГРИП.<br>
<br>
Т.е. кроме штрафов за просрочку подачи налоговой декларации в размере от 5% до 30% от суммы налога за каждый месяц или неполный месяц просрочки, блокировки расчетного счета через 10 дней после крайнего срока подачи декларации, <u><strong>налоговая получила право принудительно закрывать ИП</strong></u>! Для исключения вашего ИП из ЕГРИП, должны одновременно соблюдаться оба условия:
<ol>
<li>Истекло пятнадцать месяцев с даты окончания действия патента или индивидуальный предприниматель в течение последних пятнадцати месяцев не представлял документы отчетности, сведения о расчетах, предусмотренные законодательством Российской Федерации о налогах и сборах;</li>
<li>ИП имеет недоимку и задолженность в соответствии с законодательством Российской Федерации о налогах и сборах.</li>
</ol>
Так же важно знать, что <strong>после принудительного исключения из ЕГРИП граждане, которых исключили, не смогут регистрироваться в качестве ИП в течение 3-х лет.</strong><br>
<br>
Получается что, если Вы недоплатили в казну хотя бы 1 рубль, плюс не сдали отчетность, то Вы можете потерять право заниматься коммерческой деятельностью на 3 года.<br>
<br>
С одной стороны все следят за уплатой налогов и подачей декларации в налоговую, НО всегда случаются разные ситуации.<br>
<br>
<u><strong>Самые распространенные ошибки ИП, связанные с налоговой</strong></u>:
<ol>
<li><strong>Уплата налогов по неверным реквизитам налоговой.</strong><br>
Вы уверены, что налоги уплатили, а на самом деле реквизиты налоговой поменялись и деньги уплачены не туда! Налоговая своё не получила – соответственно у вас долг.</li>
<li><strong>Уплата не всей суммы налогов.</strong><br>
Тут причины разные – от не правильно рассчитанных налогов, до просто забыли оплатить.</li>
<li><strong>Сдали налоговую декларацию, НО она не сдана.</strong><br>
- Вы использовали устаревшую форму декларации – вы её отправили, а налоговая не приняла.<br>
- Форма декларации та правильная, но в ней вы что-то заполнили не правильно – декларация не принята.<br>
- Отправили не по тому адресу налоговой.</li>
<li><strong>Не сдали налоговую декларацию.</strong><br>
- Забыли отправить :(</li>
</ol>
<u>В следующем письме</u> – советы как избежать этих ошибок, что бы вашим ИП не заинтересовалась налоговая<br>
<br>
Успехов вам и вашему бизнесу!</td>
</tr>
</tbody>
</table>
</td>
</tr>
<!-- END CONTENT PART1 -->
</tbody>
</table>

<?= $this->render('../../_sender', [
    'url' => 'http://robot.kub-24.ru/?utm_source=unisender&utm_medium=email&utm_campaign=letter_2',
]); ?>

<?= $this->render('@common/mail/layouts/parts/_links') ?>

<?php \Yii::$app->view->endContent(); ?>
