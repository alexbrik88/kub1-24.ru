<?php

use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $subject string */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'progrev';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
        "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}";

$url = 'http://robot.kub-24.ru/?utm_source=unisender&utm_medium=email&utm_campaign=letter_5';
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/taxrobot-html.php'); ?>

<?= $this->render('../../_image', [
    'src' => 'https://lk.kub-24.ru/img/unisender/delivery/ip_usn-6/letter-5_2.png',
]); ?>

<table align="center" cellpadding="0" cellspacing="0" style="border:1px solid #cccccc; width:100%; background-color:#ffffff">
<tbody><!-- CONTENT PART1-->
<tr>
<td>
<table cellpadding="20" cellspacing="0" style="width:100%; border-bottom:1px solid #cccccc;">
<tbody>
<tr>
<td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
<h1 style="margin-top:0; font-size:28px !important; line-height:150% !important; font-weight:600; color:#333333">Привет{{Name?, }}{{Name}}!</h1>
Конечно, НЕ ПЛАТИТЬ налоги <strong>НЕЛЬЗЯ!</strong><br>
<strong>НО, закон позволяет</strong> ИП на УСН 6% <strong>уменьшить налог</strong> к уплате!<br>
<br>
Рекомендации на эту тему вы найдете в нашей программе
<a href="<?= $url ?>">КУБ-24</a>
по заполнению налоговой декларации.<br>
<br>
Программа, которая поможет <strong>просто и за 10 минут сделает всё нужное для ИП на УСН 6%</strong>:
<ol>
<li>Расчет <strong>налогов по УСН 6%</strong></li>
<li>Законное <strong style="color:red">уменьшение налога</strong> УСН 6%</li>
<li>Расчет фиксированных <strong>страховых платежей</strong></li>
<li>Расчет страховые взносы <strong>1%</strong> с дохода свыше <strong>300 000 ₽</strong></li>
<li>Подготовит <strong>платежки</strong> в налоговую</li>
<li>Заполнит <strong>налоговую декларацию</strong></li>
<li>Сформирует <strong>КУДиР</strong>, если нужно</li>
</ol>
Посмотреть программу КУБ-24 можно <a href="<?= $url ?>" style="color:#4276a5;" target="_self">тут</a><br>
&nbsp;
<div style="text-align:center"><strong>Выгоды для ВАС</strong></div>
<table style="width:560px;">
<tbody>
<tr>
<td style="text-align:center; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; border:1px solid #cccccc; width:49%">Дешевле бухгалтера<br>
и онлайн бухгалтерий</td>
<td style="text-align:center; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; border:1px solid #cccccc;">Платите меньше налогов</td>
</tr>
<tr>
<td style="text-align:center; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; border:1px solid #cccccc;">Все просто<br>
и нет лишнего функционала</td>
<td style="text-align:center; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; border:1px solid #cccccc;">Актуальный бланк налоговой декларации.<br>
Актуальные реквизиты Вашей налоговой.<br>
Правильные КБК и ОКТМО</td>
</tr>
</tbody>
</table>
<div style="text-align:center"><a href="<?= $url ?>" style="color:#4276a5;" target="_self">Программа по автоматическому заполнению налоговой декларации<br>
и расчету налогов</a></div>
</td>
</tr>
</tbody>
</table>
</td>
</tr>
<!-- END CONTENT PART1 -->
</tbody>
</table>

<?= $this->render('../../_sender', [
    'url' => $url,
]); ?>

<?= $this->render('@common/mail/layouts/parts/_links') ?>

<?php \Yii::$app->view->endContent(); ?>
