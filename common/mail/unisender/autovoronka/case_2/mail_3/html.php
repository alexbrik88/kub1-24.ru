<?php

use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $subject string */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'progrev';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
        "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}";

$url = 'http://robot.kub-24.ru/?utm_source=unisender&utm_medium=email&utm_campaign=letter_3_calendar_ip+google';
$blankUrl = 'https://kub-24.ru/nalogovaya-deklaratsiya-dlya-individualnogo-predprinimatelya-na-uproshhennoj-sisteme-nalogooblozheniya-deklaratsiya-ip-usn-2017-2018-goda/';
$google = 'https://calendar.google.com/calendar/embed?src=smart-pls.ru_hcpoq4apjfjhmd2ldauqrsae9s%40group.calendar.google.com&ctz=Europe%2FMoscow';
$ics = 'https://calendar.google.com/calendar/ical/smart-pls.ru_hcpoq4apjfjhmd2ldauqrsae9s%40group.calendar.google.com/public/basic.ics';
$link = 'https://kub-24.ru/2019/12/02/2020-god-stanet-osobennym-dlya-vseh-ip/?utm_source=unisender&utm_medium=email&utm_campaign=letter_1_4';
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/taxrobot-html.php'); ?>

<?= $this->render('../../_image', [
    'src' => 'https://lk.kub-24.ru/img/unisender/delivery/ip_usn-6/letter-3_2.png',
]); ?>

<table align="center" cellpadding="0" cellspacing="0" style="width:100%; background-color:#ffffff">
<tbody>
<!-- CONTENT PART1-->
<tr>
<td>
<table cellpadding="20" cellspacing="0" style="width:100%; border-bottom:1px solid #cccccc;">
<tbody>
<tr>
<td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
<h1 style="margin-top:0; font-size:28px !important; line-height:150% !important; font-weight:600; color:#333333">Привет{{Name?, }}{{Name}}!</h1>
В предыдущем письме я говорил о том, что <u><strong>налоговая получила право принудительно закрывать ИП.</strong></u><br>
Также я написал основные косяки предпринимателей по налогам и отчетности, из-за которых ваше ИП на УСН 6% может попасть в список на закрытие.<br>
<br>
Что сделать, чтобы не попасть в список на закрытие?
<ol>
<li>Конечно, <strong>не забывать про сроки уплаты налогов и подачи налоговой декларации</strong>. В приложении налоговый календарь для ИП на УСН 6%<br>
Так же предлагаем вам записать себе в календарь основные даты налогового календаря для ИП на УСН 6%:<br>
<a href="<?= $google ?>" style="color:#4276a5;" target="_self">Добавить в Календарь Google</a> (Вы должны быть авторизованы в системе Google Calendar)<br>
<a href="<?= $ics ?>" style="color:#4276a5;" target="_self">Добавить в календарь (*.ics)</a></li>
<li>Нужно <strong>правильно рассчитать налоги, подготовить платежку и налоговую декларацию без ошибок.</strong><br>
Варианты как это сделать, расположил их по убыванию стоимости:<br>
1) Найти <a href="<?= $link ?>">бухгалтера</a>, чтобы он всё сделал за вас<br>
2) Воспользоваться <a href="<?= $link ?>">онлайн бухгалтерией</a>, разобраться в ней и сделать всё самому.<br>
3) Воспользоваться <a href="<?= $link ?>">программой по заполнению налоговой декларации</a>, которая проверит каждое поле декларации, а так же сама рассчитает налоги.<br>
4) Рассчитать налоги самостоятельно, скачать <a href="<?= $link ?>">бланк налоговой декларации</a> и заполнить его самостоятельно.</li>
</ol>
Нажмите на тот вариант, которым вы уже пользуетесь или собираетесь воспользоваться и узнайте все плюсы и минусы вашего решения.<br>
<br>
Скачать бланк налоговой декларации можно <a href="<?= $blankUrl ?>" style="color:#4276a5;" target="_self">тут</a><br>
Заполнить налоговую декларацию и рассчитать налоги с помощью программы можно <a href="<?=$url ?>" style="color:#4276a5;" target="_self">тут</a>.<br>
<br>
<strong><i>В следующем письме</i></strong> для тех, кто готов заполнить налоговую декларацию самостоятельно, пришлю пошаговую инструкцию. Даже если вы не хотите это делать, сохраните, такого в интернете не найдете.</td>
</tr>
</tbody>
</table>
</td>
</tr>
<!-- END CONTENT PART1 -->
</tbody>
</table>

<?= $this->render('../../_sender', [
    'url' => $url,
]); ?>

<?= $this->render('@common/mail/layouts/parts/_links') ?>

<?php \Yii::$app->view->endContent(); ?>
