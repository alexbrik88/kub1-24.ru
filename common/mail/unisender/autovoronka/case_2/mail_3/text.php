КУБ - Онлайн сервис для предпринимателей

(https://kub-24.ru/?utm_source=unisender&utm_medium=email&utm_campaign=news&utm_content=ip-usn-6-letter-3&utm_term=logo)
Войти в КУБ (https://kub-24.ru/login/?utm_source=unisender&utm_medium=email&utm_content=ip-usn-6-letter-3&utm_term=login_top)

Привет{{Name?, }}{{Name}}!В предыдущем письме я говорил о том, что налоговая получила право принудительно закрывать ИП.
Также я написал основные косяки предпринимателей по налогам и отчетности, из-за которых ваше ИП на УСН 6% может попасть в список на закрытие.

Что сделать, чтобы не попасть в список на закрытие?
Конечно, не забывать про сроки уплаты налогов и подачи налоговой декларации. В приложении налоговый календарь для ИП на УСН 6%
Так же предлагаем вам записать себе в календарь основные даты налогового календаря для ИП на УСН 6%:
Добавить в Календарь Google  (Вы должны быть авторизованы в системе Google Calendar)
https://calendar.google.com/calendar/embed?src=smart-pls.ru_hcpoq4apjfjhmd2ldauqrsae9s%40group.calendar.google.com&ctz=Europe%2FMoscow

Добавить в календарь (*.ics)
https://calendar.google.com/calendar/ical/smart-pls.ru_hcpoq4apjfjhmd2ldauqrsae9s%40group.calendar.google.com/public/basic.ics

Нужно правильно рассчитать налоги, подготовить платежку и налоговую декларацию без ошибок.
Варианты как это сделать, расположил их по убыванию стоимости:
1) Найти бухгалтера, чтобы он всё сделал за вас
2) Воспользоваться онлайн бухгалтерией, разобраться в ней и сделать всё самому.
3) Воспользоваться программой по заполнению налоговой декларации, которая проверит каждое поле декларации, а так же сама рассчитает налоги.
4) Рассчитать налоги самостоятельно, скачать бланк налоговой декларации и заполнить его самостоятельно.Нажмите на тот вариант, которым вы уже пользуетесь или собираетесь воспользоваться и узнайте все плюсы и минусы вашего решения.

Скачать бланк налоговой декларации можно тут
https://kub-24.ru/nalogovaya-deklaratsiya-dlya-individualnogo-predprinimatelya-na-uproshhennoj-sisteme-nalogooblozheniya-deklaratsiya-ip-usn-2017-2018-goda/

Заполнить налоговую декларацию и рассчитать налоги с помощью программы можно тут
http://robot.kub-24.ru/?utm_source=unisender&utm_medium=email&utm_campaign=letter_3_calendar_ip+google

В следующем письме для тех, кто готов заполнить налоговую декларацию самостоятельно, пришлю пошаговую инструкцию. Даже если вы не хотите это делать, сохраните, такого в интернете не найдете.

Алексей Кущенко,КУБ-24 Программа для ИП по заполнению налоговой декларации и расчету налогов
http://robot.kub-24.ru/?utm_source=unisender&utm_medium=email&utm_campaign=letter_3_calendar_ip+google

(https://www.facebook.com/kub24ru/) (https://vk.com/kub24ru) (https://telegram.me/joinchat/EFV9EAnpdIDfe4dkWmmTvQ) (https://www.instagram.com/kub24ru/) (https://twitter.com/kub24ru)

Вы получили это письмо, потому что подписывались на новости сервиса КУБ на сайте kub-24.ru.
У Вас есть возможность отписаться от рассылки, для этого необходимо перейти в личный кабинет (https://kub-24.ru/login/?utm_source=unisender&utm_medium=email&utm_content=ip-usn-6-letter-3&utm_term=login_bottom), в раздел настройки и убрать галочки в блоке «Получение уведомлений».