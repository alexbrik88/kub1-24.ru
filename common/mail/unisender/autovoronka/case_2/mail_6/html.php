<?php

use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $subject string */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'progrev';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
        "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}";

$url = 'http://robot.kub-24.ru/?utm_source=unisender&utm_medium=email&utm_campaign=letter_6';
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/taxrobot-html.php'); ?>

<?= $this->render('../../_image', [
    'src' => 'https://lk.kub-24.ru/img/unisender/delivery/ip_usn-6/letter-6_2.png',
]); ?>

<table align="center" cellpadding="0" cellspacing="0" style="border:1px solid #cccccc; width:600px; background-color:#ffffff">
<tbody><!-- CONTENT PART1-->
<tr>
<td>
<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
<tbody>
<tr>
<td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
<h1 style="margin-top:0; font-size:28px !important; line-height:150% !important; font-weight:600; color:#333333">Привет{{Name?, }}{{Name}}!</h1>
Среди наших партнеров есть банки и для клиентов наших партнеров мы предлагаем особые условия:<br>
<br>
<strong>Для ИП на УСН 6%, у которых есть счет в одном из этих банков:</strong>
<table style="width:560px;">
<tbody>
<tr>
<td style="text-align:center; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; border:1px solid #cccccc; width:32%">
    <img src="https://lk.kub-24.ru/assets/gallery_thumbnails/0e/0eaee3ed78a28cb5c5068646e3d29852.png" style="width: 160px;">
    <br>
    Сбербанк
</td>
<td style="text-align:center; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; border:1px solid #cccccc;">
    <img src="https://lk.kub-24.ru/assets/gallery_thumbnails/95/9500b98af6972c585c357965f6d0da32.jpg" style="width: 160px;">
    <br>
    Точка банк
</td>
<td style="text-align:center; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; border:1px solid #cccccc;">
    <img src="https://lk.kub-24.ru/assets/gallery_thumbnails/5f/5f5b85e8e8ccdfd7cb8262f804d8dc67.png" style="width: 160px;">
    <br>
    Модульбанк
</td>
</tr>
<tr>
<td style="text-align:center; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; border:1px solid #cccccc;">
    <img src="https://lk.kub-24.ru/assets/gallery_thumbnails/9e/9e80aaf98ad9f7e19c1b067a0b1fbae6.png" style="width: 160px;">
    <br>
    Тинькофф банк
</td>
<td callspan="2" style="text-align:center; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; border:1px solid #cccccc;">
    <img src="https://lk.kub-24.ru/assets/gallery_thumbnails/5e/5e87de128d9684539f6b1fe6c55ba5ba.png" style="width: 160px;">
    <br>
    Альфабанк
</td>
</tr>
</tbody>
</table>
&nbsp;
<div style="border-left:4px solid #4276a5; padding-left:10px">Счетов у вас может быть 2, 3 или даже больше, но хотя бы один счет в одном из этих банков. Даже если счет в одном из этих банков уже не действующий, но в этом году была хотя бы одна операция.</div>
<br>
<strong>Скидка 25% при оплате за год</strong>. Экономия 750 рублей!<br>
Стоимость за год всего 2250 рублей.<br>
<br>
<strong>Как получить:</strong>
<ol>
<li>Зарегистрируетесь в КУБ-24 по этой <a href="<?= $url ?>" style="color:#4276a5;" target="_self">ссылке</a></li>
<li>Укажите свои счета</li>
<li>Подгрузите выписку хотя бы из одного банка указанного выше</li>
<li>Автоматически получите скидку</li>
</ol>
<strong>ВАЖНО</strong>: Регистрироваться для получения скидки нужно только по этой <a href="<?= $url ?>" style="color:#4276a5;" target="_self">ссылке КУБ-24</a></td>
</tr>
</tbody>
</table>
</td>
</tr>
<!-- END CONTENT PART1 -->
</tbody>
</table>

<?= $this->render('../../_sender', [
    'url' => $url,
]); ?>

<?= $this->render('@common/mail/layouts/parts/_links') ?>

<?php \Yii::$app->view->endContent(); ?>
