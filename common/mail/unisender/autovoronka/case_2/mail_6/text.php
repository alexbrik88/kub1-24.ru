КУБ - Онлайн сервис для предпринимателей

(https://kub-24.ru/?utm_source=unisender&utm_medium=email&utm_campaign=news&utm_content=ip-usn-6-letter-6&utm_term=logo)
Войти в КУБ (https://kub-24.ru/login/?utm_source=unisender&utm_medium=email&utm_content=ip-usn-6-letter-6&utm_term=login_top)

Привет{{Name?, }}{{Name}}!Среди наших партнеров есть банки и для клиентов наших партнеров мы предлагаем особые условия:

Для ИП на УСН 6%, у которых есть счет в одном из этих банков:

Сбербанк
Точка банк
Модульбанк

Тинькофф банк
Альфабанк
Счетов у вас может быть 2, 3 или даже больше, но хотя бы один счет в одном из этих банков. Даже если счет в одном из этих банков уже не действующий, но в этом году была хотя бы одна операция.
Скидка 25% при оплате за год. Экономия 750 рублей!
Стоимость за год всего 2250 рублей.

Как получить:
Зарегистрируетесь в КУБ-24 по этой ссылке
Укажите свои счета
Подгрузите выписку хотя бы из одного банка указанного выше
Автоматически получите скидкуВАЖНО: Регистрироваться для получения скидки нужно только по этой ссылке КУБ-24
http://robot.kub-24.ru/?utm_source=unisender&utm_medium=email&utm_campaign=letter_6

Алексей Кущенко,КУБ-24 Программа для ИП по заполнению налоговой декларации и расчету налогов
http://robot.kub-24.ru/?utm_source=unisender&utm_medium=email&utm_campaign=letter_6

(https://www.facebook.com/kub24ru/) (https://vk.com/kub24ru) (https://telegram.me/joinchat/EFV9EAnpdIDfe4dkWmmTvQ) (https://www.instagram.com/kub24ru/) (https://twitter.com/kub24ru)

Вы получили это письмо, потому что подписывались на новости сервиса КУБ на сайте kub-24.ru.
У Вас есть возможность отписаться от рассылки, для этого необходимо перейти в личный кабинет (https://kub-24.ru/login/?utm_source=unisender&utm_medium=email&utm_content=ip-usn-6-letter-6&utm_term=login_bottom), в раздел настройки и убрать галочки в блоке «Получение уведомлений».