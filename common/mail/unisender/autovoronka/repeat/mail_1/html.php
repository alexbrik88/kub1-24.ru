<?php

use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $subject string */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'progrev';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
        "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}";

$url = 'http://robot.kub-24.ru/?utm_source=unisender&utm_medium=email&utm_campaign=letter_3_calendar_ip+google';
$link = 'https://kub-24.ru/2019/12/02/2020-god-stanet-osobennym-dlya-vseh-ip/?utm_source=unisender&utm_medium=email&utm_campaign=letter_1_4';
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/taxrobot-html.php'); ?>

<?= $this->render('../../_image', [
    'src' => 'https://lk.kub-24.ru/img/unisender/delivery/ip_usn-6/letter-3_2.png',
]); ?>

<table cellpadding="20" cellspacing="0" style="width:100%; border-bottom:1px solid #cccccc;">
<tbody>
<tr>
<td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
<h1 style="margin-top:0; font-size:28px !important; line-height:150% !important; font-weight:600; color:#333333">Привет{{Name?, }}{{Name}}!</h1>
ИП может не сдавать отчетность, если готов заплатить штрафы.
Во всех остальных случаях СДАВАТЬ налоговую декларацию ОБЯЗАТЕЛЬНО!
<br>
<br>
Варианты, как подготовить декларацию:
<ol>
<li>Найти <a href="<?= $link ?>">бухгалтера</a>, чтобы он всё сделал за вас</li>
<li>Воспользоваться <a href="<?= $link ?>">онлайн бухгалтерией</a>, разобраться в ней и сделать всё самому.</li>
<li>Воспользоваться <a href="<?= $link ?>">программой по заполнению налоговой декларации</a>, которая так же сама рассчитает налоги.</li>
<li>Рассчитать налоги самостоятельно, скачать <a href="<?= $link ?>">бланк налоговой декларации</a> и заполнить его самостоятельно.</li>
</ol>
Нажмите на тот вариант, которым вы уже пользуетесь или собираетесь воспользоваться и узнайте все плюсы и минусы вашего решения.<br>
<br>
<br>
Скачать бланк налоговой декларации можно <a href="<?= $url ?>">тут</a>.
<br>
Заполнить налоговую декларацию и рассчитать налоги с помощью программы можно  <a href="<?= $url ?>">тут</a>.
</td>
</tr>
</tbody>
</table>

<?= $this->render('../../_sender', [
    'url' => $url,
]); ?>

<?= $this->render('@common/mail/layouts/parts/_links') ?>

<?php \Yii::$app->view->endContent(); ?>
