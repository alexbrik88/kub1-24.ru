КУБ - Онлайн сервис для предпринимателей

(https://kub-24.ru/?utm_source=unisender&utm_medium=email&utm_campaign=news&utm_content=ip-usn-6-letter-4&utm_term=logo)
Войти в КУБ (https://kub-24.ru/login/?utm_source=unisender&utm_medium=email&utm_content=ip-usn-6-letter-4&utm_term=login_top)

Привет{{Name?, }}{{Name}}! ИП может не сдавать отчетность, если готов заплатить штрафы.
Во всех остальных случаях СДАВАТЬ налоговую декларацию ОБЯЗАТЕЛЬНО!

Варианты, как подготовить декларацию
1) Найти бухгалтера, чтобы он всё сделал за вас
2) Воспользоваться онлайн бухгалтерией, разобраться в ней и сделать всё самому.
3) Воспользоваться программой по заполнению налоговой декларации, которая так же сама рассчитает налоги.
4) Рассчитать налоги самостоятельно, скачать бланк налоговой декларации и заполнить его самостоятельно.

Нажмите на тот вариант, которым вы уже пользуетесь или собираетесь воспользоваться и узнайте все плюсы и минусы вашего решения.

Скачать бланк налоговой декларации можно тут
http://robot.kub-24.ru/?utm_source=unisender&utm_medium=email&utm_campaign=letter_3_calendar_ip+google

Заполнить налоговую декларацию и рассчитать налоги с помощью программы можно тут
http://robot.kub-24.ru/?utm_source=unisender&utm_medium=email&utm_campaign=letter_3_calendar_ip+google

Алексей Кущенко,КУБ-24 Программа для ИП по заполнению налоговой декларации и расчету налогов
http://robot.kub-24.ru/?utm_source=unisender&utm_medium=email&utm_campaign=letter_3_calendar_ip+google

(https://www.facebook.com/kub24ru/) (https://vk.com/kub24ru) (https://telegram.me/joinchat/EFV9EAnpdIDfe4dkWmmTvQ) (https://www.instagram.com/kub24ru/) (https://twitter.com/kub24ru)

Вы получили это письмо, потому что подписывались на новости сервиса КУБ на сайте kub-24.ru.
У Вас есть возможность отписаться от рассылки, для этого необходимо перейти в личный кабинет (https://kub-24.ru/login/?utm_source=unisender&utm_medium=email&utm_content=ip-usn-6-letter-4&utm_term=login_bottom), в раздел настройки и убрать галочки в блоке «Получение уведомлений».