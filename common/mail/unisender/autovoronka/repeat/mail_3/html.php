<?php

use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */
/* @var $subject string */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'progrev';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
        "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}";

$url = 'http://robot.kub-24.ru/?utm_source=unisender&utm_medium=email&utm_campaign=letter_5';
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/taxrobot-html.php'); ?>

<?= $this->render('../../_image', [
    'src' => 'https://lk.kub-24.ru/img/unisender/delivery/ip_usn-6/letter-deklaraciya-10-minut.png',
]); ?>

<table cellpadding="20" cellspacing="0" style="width:100%; border-bottom:1px solid #cccccc;">
<tbody>
<tr>
<td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
<h1 style="margin-top:0; font-size:28px !important; line-height:150% !important; font-weight:600; color:#333333">Привет{{Name?, }}{{Name}}!</h1>
Программа, которая позволяет <strong>просто и за 10 минут сделать всё нужное для ИП на УСН 6%</strong>,
самостоятельно сделает за Вас:
<ol>
<li>Расчет налогов по УСН 6%</li>
<li>Законное уменьшение налога УСН 6%</li>
<li>Расчет фиксированных страховых платежей</li>
<li>Расчет страховые взносы 1%  с дохода свыше 300 000 ₽</li>
<li>Подготовит платежки в налоговую</li>
<li>Заполнит налоговую декларацию</li>
<li>Сформирует КУДиР, если нужно</li>
</ol>
Посмотреть программу КУБ-24 можно <a href="<?= $url ?>" style="color:#4276a5;" target="_self">тут</a><br>
&nbsp;
<div style="text-align:center"><strong>Выгоды для ВАС</strong></div>
<table style="width:560px;">
<tbody>
<tr>
<td style="text-align:center; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; border:1px solid #cccccc; width:49%">Дешевле бухгалтера<br>
и онлайн бухгалтерий</td>
<td style="text-align:center; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; border:1px solid #cccccc;">Платите меньше налогов</td>
</tr>
<tr>
<td style="text-align:center; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; border:1px solid #cccccc;">Все просто<br>
и нет лишнего функционала</td>
<td style="text-align:center; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; border:1px solid #cccccc;">Актуальный бланк налоговой декларации.<br>
Актуальные реквизиты Вашей налоговой.<br>
Правильные КБК и ОКТМО</td>
</tr>
</tbody>
</table>
<br>
<a href="https://robot.kub-24.ru/?utm_source=unisender&utm_medium=email&utm_campaign=letter_5">
Программа по автоматическому заполнению налоговой декларации и расчету налогов
</a>
</td>
</tr>
</tbody>
</table>

<?= $this->render('../../_sender', [
    'url' => $url,
]); ?>

<?= $this->render('@common/mail/layouts/parts/_links') ?>

<?php \Yii::$app->view->endContent(); ?>
