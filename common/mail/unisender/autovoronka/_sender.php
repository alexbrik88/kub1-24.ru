<?php

if (!isset($url)) {
    $url = 'http://robot.kub-24.ru/?utm_source=unisender&utm_medium=email&utm_campaign=letter';
}

?>

<table cellpadding="20" cellspacing="0" style="width: 100%; border-bottom:1px dashed #cccccc;">
<tbody><!-- line 1 -->
<tr>
<td style="background-color:#ffffff;"><!-- Colum 1 -->
<table align="left" cellpadding="20" cellspacing="0" style="display:inline; width: 25%;">
<tbody>
<tr>
<td style="color:#333333; font-family:Arial,Helvetica,sans-serif; line-height:150%; padding-top:0; padding-right:0; padding-bottom:0px; padding-left:10; text-align:left; width: 100%; background-color:#ffffff;"><img height="120" src="https://lk.kub-24.ru/img/unisender/delivery/people/kushchenko-a.png" width="120"></td>
</tr>
</tbody>
</table>
<!-- END Colum 1 --><!-- Colum 2 -->
<table align="right" cellpadding="20" cellspacing="0" style="display:inline; width: 70%; height:120;">
<tbody>
<tr>
<td style="color:#333333; font-family:Arial,Helvetica,sans-serif; line-height:150%; padding-top:0; padding-right:0; padding-bottom:0px; padding-left:0; text-align:left; width: 100%; height:120; background-color:#ffffff; vertical-align:center;">
<div style="font-size:16px !important;line-height:100% !important; font-weight:600">
    Алексей Кущенко
</div>
<div style="font-size:16px !important;line-height:100% !important; font-weight:600; margin-top:20px;">
    <a href="<?= $url ?>" style="color:#4276a5;" target="_self">КУБ-24</a>
</div>
<div style="font-size:16px !important;line-height:100% !important; font-weight:400; margin-top:20px;">
    Программа для ИП по заполнению налоговой декларации и расчету налогов
</div>
</td>
</tr>
</tbody>
</table>
<!-- END Colum 2 --></td>
</tr>
</tbody>
</table>
