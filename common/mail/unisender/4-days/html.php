<?php
/**
 * @var $message \common\components\sender\unisender\UniSender
 */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'progrev';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
        "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}";
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/html.php'); ?>

<table border="0" cellpadding="20" cellspacing="0" style="width:600px;">
    <tr>
        <td>
            <p style="font-size: 24pt; line-height: 21px; color: #000;">
                Привет{{Name?, }}{{Name}} </p>
        </td>
    </tr>
    <tr>
        <td>
            <p style="margin: 0; font-size: 12pt;">
                Деятельность любой  компании может быть успешной и прибыльной только если вы до минимума сведете финансовые риски, в том числе связанные с бухгалтерской отчетностью и уплатой налогов. Изменения в порядке исчисления и уплаты налогов, как оформить и сроки подачи документов, штрафы за не сданную отчетность, как сэкономить на налогах и многие другие вопросы вы можете задать нашим налоговым консультантам.
            </p>
        </td>
    </tr>
    <tr>
        <td>
            <p style="margin: 0; font-size: 12pt;">
                <i>Консультации профессионалов по налогам и бухгалтерии—</i> это производственная необходимость для бизнеса, который хочет сохранить и укрепить свое положение. Поэтому мы собрали профессиональную команду, которая ответит вам на все ваши вопросы абсолютно бесплатно. 
            </p>
        </td>
    </tr>
</table>
<?= $this->render('@common/mail/layouts/parts/_contacts') ?>

<?php \Yii::$app->view->endContent(); ?>
