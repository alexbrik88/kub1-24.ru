<?php

use common\models\Discount;
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'work_with_debtors';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
        "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}";

$host = Yii::$app->params['serviceSiteLk'];
$serviceSite = Yii::$app->params['serviceSite'];
$serviceUrl = trim($serviceSite, '/')."/?".$linkParams."&amp;utm_content=service";
$registerUrl = trim($serviceSite, '/')."/registration/?".$linkParams."&amp;utm_content=registration";
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/html.php'); ?>

<table cellpadding="0" cellspacing="0" style="width:600px;">
    <tbody>
        <tr>
            <td align="center">
                <?= Html::img($host.'img/unisender/debtors/mail_1.jpg', [
                    'style' => 'max-width: 100%;',
                    'alt' => '',
                ]); ?>
            </td>
        </tr>
    </tbody>
</table>
<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody>
        <tr>
            <td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
                <h1 style="margin-top:0; font-size:28px !important; line-height:100% !important; font-weight:600; color:#333333">
                    Привет{{Name?, }}{{Name}}!
                </h1>
                <br>
                <div>
                    Сейчас время кризиса и неопределенности, а в такие периоды, некоторые компании из-за ограниченности в финансах специально затягивают платежи. Ваша же задача – собрать долги как можно быстрее, так как размер долга влияет на эффективность бизнеса и ваш личный карман.
                </div>
                <br>
                <div style="font-size: 16px;">
                    <strong>
                        ПРИЧИНЫ ВОЗНИКНОВЕНИЯ дебиторской задолженности
                    </strong>
                </div>
                <br>
                <div>
                    <strong>
                        Дебиторская задолженность в бизнесе – нормальное явление.
                    </strong>
                    <br>
                    Важно контролировать размер общей дебиторской задолженности и задолженности по каждому клиенту,
                    что бы она ни превышала приемлемый для компании уровень.
                </div>
                <br>
                <div>
                    <strong>
                        Основные причины возникновения дебиторской задолженности:
                    </strong>
                    <table>
                        <body>
                            <tr>
                                <td style="vertical-align: top; padding-right: 5px;">1)</td>
                                <td>
                                    У клиента нет денег, чтобы рассчитаться с вами.
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top; padding-right: 5px;">2)</td>
                                <td>
                                    Клиент сознательно не оплачивает долг, используя деньги на более приоритетные для него цели.
                                    Фактически клиент кредитует свой бизнес за счет вас.
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top; padding-right: 5px;">3)</td>
                                <td>
                                    Недобросовестный клиент.
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top; padding-right: 5px;">4)</td>
                                <td>
                                    Клиент забыл, а ваши сотрудники не напоминают.
                                </td>
                            </tr>
                        </body>
                    </table>
                </div>
                <br>
                <div style="font-size: 16px;">
                    <strong>
                        ПРАВИЛЬНАЯ РАБОТА с должниками
                    </strong>
                </div>
                <div>
                    Работа по возврату дебиторской задолженности начинается еще до заключения договора с клиентом. Основные этапы:
                </div>
                <table>
                    <body>
                        <tr>
                            <td style="vertical-align: top; padding-right: 5px;">1)</td>
                            <td>
                                Разработка политики работы с клиентами – от проверки и анализа клиента
                                до определения размеров сумм и сроков по отсрочке платежей.
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top; padding-right: 5px;">2)</td>
                            <td>
                                Разработка шаблонов договоров, проработка основных условий,
                                которые защитят вас в случае просрочки платежей.
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top; padding-right: 5px;">3)</td>
                            <td>
                                Осуществление контроля над своевременностью выполнения оплаты счетов клиентами.
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top; padding-right: 5px;">4)</td>
                            <td>
                                Контроль сумм, которые нам должны.
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top; padding-right: 5px;">5)</td>
                            <td>
                                Работа с дебиторами, нарушившими сроки оплаты.
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top; padding-right: 5px;">6)</td>
                            <td>
                                Активные действия по взысканию задолженности в судебном порядке.
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top; padding-right: 5px;">7)</td>
                            <td>
                                Списание безнадежной дебиторской задолженности.
                            </td>
                        </tr>
                    </body>
                </table>
                <br>
                <div>
                    Работа с должниками должна проводиться регулярно и комплексно.
                    Необходимо какие-то этапы формализовать, что бы ваши сотрудники знали что делать,
                    а в некоторых случаях, что бы процесс был автоматизирован и не зависел от сотрудников.
                    В тоже время, в данной работе много индивидуальной работы с должником.
                </div>
                <br>
                <div>
                    <strong>
                        <i>
                            В приложении ЧЕК-ЛИСТ по АКТИВНОМУ ВОЗВРАТУ ДОЛГОВ
                        </i>
                    </strong>
                </div>
                <br>
                <div>
                    В следующем письме расскажем про опыт наших клиентов по работе с должниками,
                    и чем может помочь сервис <a href="<?= $registerUrl ?>">kub-24.ru</a>
                </div>
            </td>
        </tr>
    </tbody>
</table>

<?= $this->render('@common/mail/layouts/parts/_alexey', ['message' => $message]) ?>

<?= $this->render('@common/mail/layouts/parts/_links') ?>

<?php \Yii::$app->view->endContent(); ?>
