<?php

use common\models\Discount;
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'work_with_debtors';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
        "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}";

$host = Yii::$app->params['serviceSiteLk'];
$serviceSite = Yii::$app->params['serviceSite'];
$serviceUrl = trim($serviceSite, '/')."/?".$linkParams."&amp;utm_content=service";
$registerUrl = trim($serviceSite, '/')."/registration/?".$linkParams."&amp;utm_content=registration";
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/html.php'); ?>

<table cellpadding="0" cellspacing="0" style="width:600px;">
    <tbody>
        <tr>
            <td align="center">
                <?= Html::img($host.'img/unisender/debtors/mail_2.jpg', [
                    'style' => 'max-width: 100%;',
                    'alt' => '',
                ]); ?>
            </td>
        </tr>
    </tbody>
</table>
<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody>
        <tr>
            <td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
                <h1 style="margin-top:0; font-size:28px !important; line-height:100% !important; font-weight:600; color:#333333">
                    Привет{{Name?, }}{{Name}}!
                </h1>
                <br>
                <div>
                    <strong>
                        Лучший метод борьбы с долгами – профилактика их возникновения.
                    </strong>
                    Лучший способ профилактики – дать сотрудникам инструменты,
                    которые будут им помогать, и в тоже время не будут отнимать время.
                </div>
                <br>
                <div>
                    Мы сами, а так же более 5 000 компаний, используем в своей работе возможности сервиса
                    <a href="<?= $serviceUrl ?>">kub-24.ru</a>
                    и вам рекомендуем использовать этот сервис, который помогает:
                    <ul>
                        <li>
                            <strong>
                                Проверить новых и имеющихся клиентов.
                            </strong>
                            <br>
                            Проверка осуществляется в самом сервисе.
                            Данные автоматически собираются из различных источников.
                            Собранная информация всегда под рукой в закладке «Досье» клиента.
                        </li>
                        <li>
                            <strong>
                                Создать шаблон договора, который защитит ваши интересы.
                            </strong>
                            <br>
                            В КУБе есть конструктор договоров, в котором вы создаете шаблон договора.
                            При формировании договора с клиентом, в шаблон автоматически подставляются ваши реквизиты и вашего клиента.
                            Сотрудники не смогут случайно изменить текст договора.
                        </li>
                        <li>
                            <strong>
                                Определить критические суммы задолженности.
                            </strong>
                            <br>
                            В разделе АвтоСбор Долгов указывает по всем клиентами или по каждому в отдельности допустимые суммы задолженности.
                            При превышении этой суммы клиенту отправится письмо.
                        </li>
                        <li>
                            <strong>
                                Определить критические суммы задолженности.
                            </strong>
                            <br>
                            В разделе АвтоСбор Долгов указывает по всем клиентами или по каждому в отдельности допустимые суммы задолженности.
                            При превышении этой суммы клиенту отправится письмо.
                        </li>
                        <li>
                            <strong>
                                Настроить автоматические цепочки писем по должникам.
                            </strong>
                            <br>
                            Шаблоны писем есть в разделе АвтоСбор Долгов. Шаблоны можно изменить.
                            Письма отправляются автоматически с напоминанием о сроке оплаты,
                            при просрочке оплаты счета и/или превышении допустимой суммы задолженности.
                        </li>
                        <li>
                            <strong>
                                Предотвращать появление новых должников.
                            </strong>
                            <br>
                            В отчетах есть АВС анализ клиентов, а так же отчет по Платежной дисциплине клиентов.
                            Эти отчеты позволяют мониторить клиентов и заранее принимать решения по клиентам,
                            которые вот-вот перейдут в разряд должников.
                        </li>
                        <li>
                            <strong>
                                Следить за ростом дебиторской задолженности и ухудшением её качества.
                            </strong>
                            <br>
                            В КУБе есть графики, которые строятся автоматически
                            и показывают текущую ситуацию по всем долгам и по каждому клиенту в отдельности.
                        </li>
                        <li>
                            <strong>
                                Предотвращать кассовые разрывы.
                            </strong>
                            <br>
                            КУБ покажет, что у вас не хватает оборотных средств на предстоящие платежи
                            и нужно усилить работу по сбору долгов.
                        </li>
                    </ul>
                </div>
                <br>
                <div style="text-align: center;">
                    <a href="<?= $registerUrl ?>" style="
                        padding: 8px 16px;
                        text-decoration: none;
                        color: #ffffff;
                        background-color: #3a8bf7;
                    ">
                        Настроить работу с должниками
                    </a>
                </div>
            </td>
        </tr>
    </tbody>
</table>

<?= $this->render('@common/mail/layouts/parts/_alexey', ['message' => $message]) ?>

<?= $this->render('@common/mail/layouts/parts/_links') ?>

<?php \Yii::$app->view->endContent(); ?>
