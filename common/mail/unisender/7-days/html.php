<?php
/**
 * @var $message \common\components\sender\unisender\UniSender
 */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'progrev';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
        "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}";
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/html.php'); ?>

<table border="0" cellpadding="20" cellspacing="0" style="width:600px;">
    <tr>
        <td>
            <p style="font-size: 24pt;line-height: 21px; color: #000;">
                Привет{{Name?, }}{{Name}} </p>
        </td>
    </tr>
    <tr>
        <td>
            <p style="margin: 0; font-size: 12pt;">
                Вы регистрировались в сервисе КУБ и, к нашему сожалению  больше не заходили в него. Что бы сделать сервис лучше, нам очень важно знать ваше мнение. Уделите пару минут - ответьте на это письмо указав:
            </p>
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <p style="margin: 0; font-size: 12pt; width: 280px;">
                            <span style="font-weight: bold;">1.   Почему вы не используете КУБ?</span>
                        </p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellspacing="0" cellpadding="0" style="width: 100%;">
                <tr>
                    <td style="width: 100%; border-bottom: 1px dashed #DEDEDE;">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <p style="margin: 0; font-size: 12pt; width: 280px;">
                            <span style="font-weight: bold;">2.   Что нам нужно поменять или что дополнить, что бы вы стали использовать КУБ?</span>
                        </p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellspacing="0" cellpadding="0" style="width: 100%;">
                <tr>
                    <td style="width: 100%; border-bottom: 1px dashed #DEDEDE;">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <p style="margin: 0; font-size: 12pt; width: 280px;">
                            <span style="font-weight: bold;">3.   Как вы сейчас выставляете счета?</span>
                        </p>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellspacing="0" cellpadding="0" style="width: 100%;">
                <tr>
                    <td style="width: 100%; border-bottom: 1px dashed #DEDEDE;">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <p style="margin: 0; font-size: 12pt;">
                Будем ОЧЕНЬ благодарны за Ваши ответы.
            </p>
        </td>
    </tr>
</table>
<?= $this->render('@common/mail/layouts/parts/_contacts') ?>

<?php \Yii::$app->view->endContent(); ?>
