<?php

use common\models\Discount;
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'progrev';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
        "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}";
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/html.php'); ?>

<table cellpadding="0" cellspacing="0" style="width:600px;">
    <tbody>
        <tr>
            <td align="center">
                <?= Html::img('https://lk.kub-24.ru/img/unisender/11-3.jpg', [
                    'width' => 600,
                    'alt' => '',
                ]); ?>
            </td>
        </tr>
    </tbody>
</table>
<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody>
        <tr>
            <td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
                <h1 style="margin-top:0; font-size:28px !important; line-height:100% !important; font-weight:600; color:#333333">
                    Добрый день{{Name?, }}{{Name}}!
                </h1>
                <div>
                    Сегодня последний день, когда вы можете воспользоваться бонусами.
                    Вы можете оплатить счет на подписку с банковского счета вашей компании или оплатить банковской картой.
                    Напоминаю, что вы можете использовать один из следующих бонусов
                    <br>
                    1) скидка <?= Discount::TYPE_2; ?>% при подписке на 1 месяц;
                    <br>
                    2) скидка <?= Discount::TYPE_2; ?>% при подписке 4 месяца;
                    <br>
                    3) скидка <?= Discount::TYPE_2; ?>% при подписке 12 месяцев.
                </div>
            </td>
        </tr>
    </tbody>
</table>

{{html_content}}

<?= $this->render('@common/mail/layouts/parts/_arslan', ['message' => $message]) ?>

<?= $this->render('@common/mail/layouts/parts/_links') ?>

<?php \Yii::$app->view->endContent(); ?>
