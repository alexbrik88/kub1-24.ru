<?php
/**
 * @var $message \common\components\sender\unisender\UniSender
 */
$startWeekTime = strtotime('-7 day');
$startWeek = date('d.m.Y', $startWeekTime);
$endWeek = date('d.m.Y', time());
$count = (int) $message->getParam('count');
?>
Ваши недельные рузльтаты!
Привет{{Name?, }}{{Name}}!
Итоги вашего бизнеса
_________________________________________________________________
<?php for ($i=0; $i < $count; $i++) : ?>
<?php $key = ($i === 0) ? '' : (string) $i; ?>

{{company<?=$i?>}}
за прошедшую неделю: <?=$startWeek?> - <?=$endWeek?>

Клиенты вам оплатили: {{clientInSum<?=$i?>}} ₽.

Клиенты вам должны: {{clientOutSum<?=$i?>}} ₽.

Вы выставили счетов на: {{exhibitInvoiceSum<?=$i?>}} ₽.

Ваши расходы: {{expenses<?=$i?>}} ₽.
_________________________________________________________________
<?php endfor ?>

По всем вопросам обращайтесь в службу поддержки support@kub-24.ru



Вы получили это письмо, потому что подписывались на новости сервиса КУБ на сайте <?php echo $message->getParam('baseUrl'); ?>.
У Вас есть возможность отписаться от рассылки, для этого необходимо перейти в личный кабинет <?php echo $message->getParam('lkBaseUrl'); ?>, в раздел настройки и убрать галочки в блоке «Получение уведомлений».
