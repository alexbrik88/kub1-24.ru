<?php
/**
 * @var $message \common\components\sender\unisender\UniSender
 */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'progrev';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
        "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}";
$startWeekTime = strtotime('-7 day');
$startWeek = date('d.m.Y', $startWeekTime);
$endWeek = date('d.m.Y', strtotime('-1 day'));
$count = (int) $message->getParam('count');
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/html.php'); ?>

<table cellpadding="20" cellspacing="0" style="width:600px;">
    <tbody>
        <tr>
            <td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
                <h1 style="margin-top:0; font-size:28px !important; line-height:100% !important; font-weight:500; color:#333333">
                    Ваши недельные результаты!
                </h1>
                Привет{{Name?, }}{{Name}}!
                <br>
                Итоги вашего бизнеса
            </td>
        </tr>
    </tbody>
</table>

<?php for ($i=0; $i < $count; $i++) : ?>

<?php $key = ($i === 0) ? '' : (string) $i; ?>
<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px dashed #cccccc; border-top:1px solid #cccccc">
    <tbody>
        <tr>
            <td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
                <h2 style="margin-top:0; margin-bottom:0; font-size:18px !important; line-height:150% !important; font-weight:500; color:#333333">
                    <strong>{{company<?=$key?>}}</strong>
                    <br>
                    за прошедшую неделю: <?=$startWeek?> - <?=$endWeek?>
                </h2>
            </td>
        </tr>
    </tbody>
</table>

<table cellpadding="20" cellspacing="0" style="width:600px;">
    <tbody>
        <!-- LINE 1 -->
        <tr>
            <td style="background-color:#ffffff;">
                <!-- Colum 1 -->
                <table align="left" cellpadding="20" cellspacing="0" style="display:inline; width:260px;" width="600">
                    <tbody>
                        <tr>
                            <td style="color:#333333; font-family:Arial,Helvetica,sans-serif; line-height:150%; padding-top:0; padding-right:0; padding-bottom:0px; padding-left:10; text-align:left; width:260px; background-color:#e0f3fe; border-bottom:1px solid #c3e7fa">
                                <h2 style="font-size:14px !important;line-height:100% !important; font-weight:500"><i>Клиенты вам оплатили:</i></h2>
                            </td>
                        </tr>
                        <tr>
                            <td style="color:#333333; font-family:Arial,Helvetica,sans-serif; font-size:20px; line-height:150%; padding-top:0; padding-right:0; padding-bottom:20px; padding-left:0; text-align:center; width:260px; background-color:#e0f3fe;">
                                <h3 style="font-size:28px !important;line-height:100% !important; font-weight:500">{{clientInSum<?=$key?>}} ₽</h3>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <!-- END Colum 1 --><!-- Colum 2 -->
                <table align="right" cellpadding="20" cellspacing="0" style="display:inline; width:260px;" width="600">
                    <tbody>
                        <tr>
                            <td style="color:#333333; font-family:Arial,Helvetica,sans-serif; line-height:150%; padding-top:0; padding-right:0; padding-bottom:0px; padding-left:10; text-align:left; width:260px; background-color:#e0f3fe; border-bottom:1px solid #c3e7fa">
                                <h2 style="font-size:14px !important;line-height:100% !important; font-weight:500"><i>Клиенты&nbsp;вам должны:</i></h2>
                            </td>
                        </tr>
                        <tr>
                            <td style="color:#333333; font-family:Arial,Helvetica,sans-serif; font-size:20px; line-height:150%; padding-top:0; padding-right:0; padding-bottom:20px; padding-left:0; text-align:center; width:260px; background-color:#e0f3fe;">
                                <h3 style="font-size:28px !important;line-height:100% !important; font-weight:500">{{clientOutSum<?=$key?>}} ₽</h3>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <!-- END Colum 2 -->
            </td>
        </tr>
        <!-- END LINE 1 --><!-- LINE 2 -->
        <tr>
            <td style="background-color:#ffffff; border-top:1px dashed #cccccc">
                <!-- Colum 1 -->
                <table align="left" cellpadding="20" cellspacing="0" style="display:inline; width:260px;" width="600">
                    <tbody>
                        <tr>
                            <td style="color:#333333; font-family:Arial,Helvetica,sans-serif; line-height:150%; padding-top:0; padding-right:0; padding-bottom:0px; padding-left:10; text-align:left; width:260px; background-color:#e0f3fe;">
                                <h2 style="font-size:14px !important;line-height:150% !important; font-weight:500"><i>Вы выставили счетов на:</i></h2>
                            </td>
                        </tr>
                        <tr>
                            <td style="color:#333333; font-family:Arial,Helvetica,sans-serif; font-size:20px; line-height:150%; padding-top:0; padding-right:0; padding-bottom:20px; padding-left:0; text-align:center; width:260px; background-color:#ffffff;">
                                <h3 style="font-size:28px !important;line-height:100% !important; font-weight:500">{{exhibitInvoiceSum<?=$key?>}} ₽</h3>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <!-- END Colum 1 --><!-- Colum 2 -->
                <table align="right" cellpadding="20" cellspacing="0" style="display:inline; width:260px;" width="600">
                    <tbody>
                        <tr>
                            <td style="color:#333333; font-family:Arial,Helvetica,sans-serif; line-height:150%; padding-top:0; padding-right:0; padding-bottom:0px; padding-left:10; text-align:left; width:260px; background-color:#ffffff;">
                                <h2 style="font-size:14px !important;line-height:100% !important; font-weight:500"><i>Откладывали выставления счетов? Это легко исправить. Начните выставлять счета, быстрая отправка счетов - это самый быстрый способ получить деньги.</i></h2>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="background-color:#ffffff; font-family:Arial,Helvetica,sans-serif; line-height:100%; font-size:12px; padding-top:12px; padding-left:20px">
                                <a href="<?= $message->getParam('lkBaseUrl') . Yii::$app->params['uniSender']['utm']['billInvoice']; ?>"
                                style="color:#ffffff; font-weight:400; text-decoration:none; padding:10px 20px 10px 20px; background-color:#0077a7">
                                    Выставить счет
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <!-- END Colum 2 -->
            </td>
        </tr>
        <!-- END line 2 --><!-- LINE 3 -->
        <tr>
            <td style="background-color:#ffffff; border-top:1px dashed #cccccc"><!-- Colum 1 -->
                <table align="left" cellpadding="20" cellspacing="0" style="display:inline; width:260px;" width="600">
                    <tbody>
                        <tr>
                            <td style="color:#333333; font-family:Arial,Helvetica,sans-serif; line-height:150%; padding-top:0; padding-right:0; padding-bottom:0px; padding-left:10; text-align:left; width:260px; background-color:#e0f3fe;">
                                <h2 style="font-size:14px !important;line-height:150% !important; font-weight:500"><i>Ваши расходы:</i></h2>
                            </td>
                        </tr>
                        <tr>
                            <td style="color:#333333; font-family:Arial,Helvetica,sans-serif; font-size:20px; line-height:150%; padding-top:0; padding-right:0; padding-bottom:20px; padding-left:0; text-align:center; width:260px; background-color:#ffffff;">
                                <h3 style="font-size:28px !important;line-height:100% !important; font-weight:500">{{expenses<?=$key?>}} ₽</h3>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <!-- END Colum 1 --><!-- Colum 2 -->
                <table align="right" cellpadding="20" cellspacing="0" style="display:inline; width:260px;" width="600">
                    <tbody>
                        <tr>
                            <td style="color:#333333; font-family:Arial,Helvetica,sans-serif; line-height:150%; padding-top:0; padding-right:0; padding-bottom:0px; padding-left:10; text-align:left; width:260px; background-color:#ffffff;">
                                <h2 style="font-size:14px !important;line-height:100% !important; font-weight:500">
                                    <i>
                                        Вы уверены, что ничего не покупали на этой неделе?
                                        Если вы пропустили и не внесли расходные документы, то сделайте это сейчас.
                                        Когда нужно будет сдавать отчетность, они вам обязательно понадобятся, что бы уменьшить налоги.
                                    </i>
                                </h2>
                            </td>
                        </tr>
                        <tr>
                            <td align="left" style="background-color:#ffffff; font-family:Arial,Helvetica,sans-serif; line-height:100%; font-size:12px; padding-top:12px; padding-left:20px">
                                <a href="<?= $message->getParam('lkBaseUrl') . Yii::$app->params['uniSender']['utm']['addCosts']; ?>"
                                style="color:#ffffff; font-weight:400; text-decoration:none; padding:10px 20px 10px 20px; background-color:#0077a7">
                                    Внести расходы
                                </a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
            <!-- END Colum 2 -->
        </tr>
        <!-- END Line 3 -->
    </tbody>
</table>

<?php endfor ?>

<?= $this->render('@common/mail/layouts/parts/_contacts') ?>

<?php \Yii::$app->view->endContent(); ?>
