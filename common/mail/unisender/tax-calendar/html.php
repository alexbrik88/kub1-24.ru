<?php
use yii\helpers\Html;
use yii\helpers\Inflector;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */

$subject = $message->getParam('subject');
$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'progrev';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
        "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}";
$year = date('Y');
$y = date('y');
$m = date('m');
$imgFile = Yii::getAlias("@frontend/web/img/unisender/tax-calendar/{$y}/{$m}.jpg");
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/html.php'); ?>

<?php if (is_file($imgFile)) : ?>
    <table cellpadding="0" cellspacing="0" style="width:600px;">
        <tbody>
            <tr>
                <td align="center">
                    <?= Html::img("https://lk.kub-24.ru/img/unisender/tax-calendar/{$y}/{$m}.jpg", [
                        'width' => 600,
                        'alt' => '',
                    ]); ?>
                </td>
            </tr>
        </tbody>
    </table>
<?php endif; ?>

{{html_content}}

<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody>
        <tr>
            <td align="center" style="background-color:#ffffff; font-family:Arial,Helvetica,sans-serif; line-height:100%; font-size:16px; ">
                <a href="https://lk.kub-24.ru/notification/index?month=<?= $m ?>&amp;year=<?= $year ?>&amp;<?= $linkParams ?>&amp;utm_content=button"
                    style="color:#ffffff; font-weight:400; text-decoration:none; padding:10px 20px 10px 20px; background-color:#0077a7">Налоговый календарь на <?= $year ?> год
                </a>
            </td>
        </tr>
    </tbody>
</table>

<?= $this->render('@common/mail/layouts/parts/_alexey', ['message' => $message]) ?>

<?= $this->render('@common/mail/layouts/parts/_links') ?>

<?php \Yii::$app->view->endContent(); ?>
