<?php

use php_rutils\RUtils;

/* @var $this yii\web\View */
/* @var $model common\models\notification\Notification */

?>

<div style="margin-top: 5px; margin-bottom: 10px; border-top: 1px solid #d4d4d4;"></div>
<div style="font-size: 15px; color: #4276a4; font-weight: bold;">
    <?= RUtils::dt()->ruStrFTime([
        'date' => $model->event_date,
        'format' => 'd F Y',
        'monthInflected' => true,
    ]); ?>
</div>
<div style="font-size: 17px; font-weight: bold;">
    <?= $model->title; ?>
</div>
<div style="font-size: 14px;">
    <div>
        <b><i>Срок:</i></b> до <?= RUtils::dt()->ruStrFTime([
            'date' => $model->event_date,
            'format' => 'd F Y',
            'monthInflected' => true,
        ]); ?>
    </div>
    <div>
        <b><i>Кто:</i></b> <?= $model->text; ?>
    </div>
    <div>
        <b><i>Штраф:</i></b> <?= $model->fine; ?>
    </div>
</div>
