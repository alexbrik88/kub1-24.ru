<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'progrev';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
        "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}";
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/html.php'); ?>

<table cellpadding="0" cellspacing="0" style="width:600px;">
    <tbody>
        <tr>
            <td align="center">
                <?= Html::img('https://lk.kub-24.ru/img/unisender/tax-robot/4.png', [
                    'width' => 600,
                    'alt' => '',
                ]); ?>
            </td>
        </tr>
    </tbody>
</table>
<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody>
        <tr>
            <td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
                <h1 style="margin-top:0; font-size:28px !important; line-height:100% !important; font-weight:600; color:#333333">
                    Добрый день{{Name?, }}{{Name}}!
                </h1>
                <div style="margin-top: 20px;">
                    КУДиР – это Книга Учета Доходов и Расходов, которую обязаны вести:
                    ИП на УСН, ОСН, ПСН, ЕСХН (т.е. все кроме ЕНВД).
                </div>
                <div style="margin-top: 20px;">
                    КУДиР можно вести самостоятельно, т.е. вручную.
                    Но при большом количестве доходно-расходных операций,
                    вести КУДиР вручную неудобно и затратно по времени,
                    при этом  еще возможны ошибки.
                </div>
                <div style="margin-top: 20px;">
                    В целях экономии времени и во избежание ошибок КУБ позволяет автоматически формировать КУДиР для ИП.
                </div>
                <div style="margin-top: 20px;">
                    Сдавать КУДиР в налоговую инспекцию не нужно.
                    Однако, прошитая и пронумерованная КУДиР должна быть обязательно,
                    т.к. налоговая может затребовать её для проверки.
                    При отсутствии КУДиР вам выпишут штраф и могут назначить более тщательную проверку.
                </div>
            </td>
        </tr>
    </tbody>
</table>
<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody>
        <tr>
            <td align="center" style="background-color:#ffffff; font-family:Arial,Helvetica,sans-serif; line-height:100%; font-size:16px; ">
                <a href="https://lk.kub-24.ru/tax/robot/bank?<?= $linkParams ?>&amp;utm_content=button" style="color:#ffffff; font-weight:400; text-decoration:none; padding:10px 20px 10px 20px; background-color:#0077a7">
                    Сформировать КУДиР
                </a>
            </td>
        </tr>
    </tbody>
</table>

<?= $this->render('@common/mail/layouts/parts/_arslan', ['message' => $message]) ?>

<?= $this->render('@common/mail/layouts/parts/_links') ?>

<?php \Yii::$app->view->endContent(); ?>
