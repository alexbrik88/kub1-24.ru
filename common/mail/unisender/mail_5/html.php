<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'progrev';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
        "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}";
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/html.php'); ?>

<table cellpadding="0" cellspacing="0" style="width:600px;">
    <tbody>
        <tr>
            <td align="center">
                <?= Html::img('https://lk.kub-24.ru/img/unisender/11.jpg', [
                    'width' => 600,
                    'alt' => '',
                ]); ?>
            </td>
        </tr>
    </tbody>
</table>
<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody>
        <tr>
            <td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
                <h1 style="margin-top:0; font-size:28px !important; line-height:100% !important; font-weight:600; color:#333333">
                    Добрый день{{Name?, }}{{Name}}!
                </h1>
                Всегда приятно, когда ваши счета оплачены. Значит, клиент оценил вашу работу или товар.
                Отследить оплату ваших счетов можно в Клиент-Банке, но чем больше счетов,
                тем сложнее держать все в голове – оплачен счет или нет, полностью оплачен или частично, сколько еще должны доплатить.
                <br>
                <br>
                КУБ – упрощает вашу работу. Что бы не держать в голове всю информацию по выставленным счетам, вы можете:
                <ul style="list-style-position: inside;">
                    <li>
                        Отметить счет, как оплаченный нажав на кнопку «Оплачен».
                    </li>
                    <li>
                        Выгрузить выписку из Клиент-Банка, а потом загрузить её в КУБ.
                        Далее, КУБ автоматически отметит все счета, оплаченные полностью или частично.
                    </li>
                    <li>
                        Из КУБа подключится к вашему Клиент-Банку и автоматически получить выписку.
                        Это можно сделать, если у вас счет в банке, с которым мы уже интегрировались.
                    </li>
                </ul>
                <br>
                <b>
                    Фокусируйтесь на продажах, а КУБ отследит оплаты всех счетов.
                </b>
            </td>
        </tr>
    </tbody>
</table>

<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody>
        <tr>
            <td align="center" style="background-color:#ffffff; font-family:Arial,Helvetica,sans-serif; line-height:100%; font-size:16px; ">
                <a href="https://kub-24.ru/login/?<?= $linkParams ?>&amp;utm_content=button" style="color:#ffffff; font-weight:400; text-decoration:none; padding:10px 20px 10px 20px; background-color:#0077a7">
                    Загрузить выписку из банка
                </a>
            </td>
        </tr>
        <tr>
            <td style="text-align:center; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%; padding-top:0px">
                <a href="https://kub-24.ru/2017/05/22/kak-izbezhat-oshibok-privodyashhih-k-snizheniyu-vyruchki/?<?= $linkParams ?>&amp;utm_content=button" style="color:#0077a7; font-weight:400; text-decoration:underline;">
                    узнайте, какие ошибки уменьшают вашу выручку
                </a>
            </td>
        </tr>
    </tbody>
</table>

<?= $this->render('@common/mail/layouts/parts/_alexey', ['message' => $message]) ?>

<?= $this->render('@common/mail/layouts/parts/_links') ?>

<?php \Yii::$app->view->endContent(); ?>
