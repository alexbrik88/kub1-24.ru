<?php
/**
 * @var $message \common\components\sender\unisender\UniSender
 */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'progrev';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
        "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}";
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/html.php'); ?>

<table border="0" cellpadding="20" cellspacing="0" style="width:600px;">
    <tr>
        <td>
            <p style="font-size: 24pt; line-height: 21px; color: #000;">
                Привет{{Name?, }}{{Name}} </p>
        </td>
    </tr>
    <tr>
        <td>
            <p style="margin: 0; font-size: 12pt;">
                Если у вас не было времени изучить все возможности КУБа, воспользуйтесь промокодом «СЧЕТА-В-КУБЕ» и получите еще 14 бесплатных дней для начала работы в КУБе.
            </p>
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <p style="margin: 0; font-size: 12pt; padding-bottom: 20px;">
                            <span style="font-weight: bold;">Видео по выставлению счета в КУБе</span>
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p style="margin: 0; font-size: 12pt; width: 280px;">
                            <img src="video.png" alt="">
                        </p></td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellspacing="0" cellpadding="0" style="width: 100%;">
                <tr>
                    <td style="width: 100%; border-bottom: 1px dashed #DEDEDE;">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <p style="margin: 0; font-size: 12pt; width: 480px; padding-bottom: 20px;">
                            <span
                                style="font-weight: bold; display: block;">После просмотра видео вы сможете:

                            </span>
                            <br>
                            <span style="padding-left: 40px;">-   Просто и быстро выставить счет.</span>
                            <br>
                            <span style="padding-left: 40px;"">
                            -   Просто вводить информацию.</span>
                            <br>
                            <span style="padding-left: 40px;"">
                                
                            -   Отправлять счет сразу покупателю на e-mail.
                            </span>
                        </p></td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<?= $this->render('@common/mail/layouts/parts/_contacts') ?>

<?php \Yii::$app->view->endContent(); ?>
