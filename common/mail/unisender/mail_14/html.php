<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'progrev';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
        "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}";
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/html.php'); ?>

<table cellpadding="0" cellspacing="0" style="width:600px;">
    <tbody>
        <tr>
            <td align="center">
                <?= Html::img('https://lk.kub-24.ru/img/unisender/7-4-1.jpg', [
                    'width' => 600,
                    'alt' => '',
                ]); ?>
            </td>
        </tr>
    </tbody>
</table>

<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody>
        <tr>
            <td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
                <h1 style="margin-top:0; font-size:28px !important; line-height:100% !important; font-weight:600; color:#333333">
                    {{Name|Д}}{{Name?, д}}обрый день!
                </h1>
                <div style="margin: 20px 0;">
                    В прошлом письме мы рассказали, как в КУБе просто настроить контроль оплаченных и не оплаченных счетов
                    и тем самым видеть долги по каждому клиенту.
                    Сегодня мы расскажем, что нужно проанализировать.
                </div>
                <div style="margin: 20px 0;">
                    <div style="font-weight: bold;">
                        Шаг № 1. Проверить сроки просрочки.
                    </div>
                    Это нужно для того, чтобы понимать, какова вероятность добровольного возврата долга.
                    <br>
                    Чем дольше клиент не платит, тем меньше вероятность того, что вы вообще получите оплату.
                </div>
                <div style="margin: 20px 0;">
                    <div style="font-weight: bold;">
                        Шаг № 2. Рассчитать долю просроченной задолженности в общем объеме задолженности.
                    </div>
                    Это необходимо для определения критичности просроченных долгов.
                    Планируя будущие поступления, вы, помимо всего прочего, смотрите и на дебиторскую задолженность,
                    то есть на суммы, которые, вероятнее всего, пополнят ваш расчетный счет.
                    Однако, если доля просроченных долгов в общем объеме 50 процентов и более,
                    то это не безопасно для платежеспособности вашего предприятия!
                    <br>
                    В КУБе, на главной странице вашего аккаунта есть график с долей просроченной задолженности по вашим клиентам.
                </div>
                <div style="margin: 20px 0;">
                    <div style="font-weight: bold;">
                        Шаг № 3. Принять решение по каждому должнику:
                    </div>
                    <ul style="list-style-position: inside;">
                        <li>
                            дальнейшее ожидание оплаты;
                        </li>
                        <li>
                            проведение дополнительных переговоров;
                        </li>
                        <li>
                            переход к шагу № 4.
                        </li>
                    </ul>
                </div>
                <div style="margin: 20px 0;">
                    <div style="font-weight: bold;">
                        Шаг № 4. Подготовить и отправить претензию должнику.
                    </div>
                    В большинстве случаев этого достаточно, что бы должник оплатил вам деньги.
                    <br>
                    Если вы не знаете, как это сделать и у вас нет юриста, который это сделает,
                    то юристы КУБа помогут вам подготовить претензию.
                    <br>
                    Подготовка претензия силами юристов КУБа стоит 3000 рублей.
                    <br>
                    Нужна помощь, пишите!
                </div>
                <div style="margin: 20px 0;">
                    <div style="font-weight: bold;">
                        Шаг № 5. Начать судебное разбирательство.
                    </div>
                    Если отправка претензии не помогла, то необходимо идти в суд.
                    <br>
                    Данный шаг требует индивидуального рассмотрения и необходимо привлекать юристов.
                    КУБ готов вам помочь и в этом.
                </div>
                <div style="margin: 20px 0 0;">
                    Секрет эффективности сбора долгов в системном подходе.
                    КУБ – ваш электронный финансовый помощник, позволяющий систематизировать и автоматизировать информацию по долгам.
                </div>
            </td>
        </tr>
    </tbody>
</table>

<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody>
        <tr>
            <td align="center" style="background-color:#ffffff; font-family:Arial,Helvetica,sans-serif; line-height:100%; font-size:16px; ">
                <a href="https://kub-24.ru/login/?<?= $linkParams ?>&amp;utm_content=button" style="color:#ffffff; font-weight:400; text-decoration:none; padding:10px 20px 10px 20px; background-color:#0077a7">
                    Войти в КУБ
                </a>
            </td>
        </tr>
    </tbody>
</table>

<?= $this->render('@common/mail/layouts/parts/_alexey', ['message' => $message]) ?>

<?= $this->render('@common/mail/layouts/parts/_links') ?>

<?php \Yii::$app->view->endContent(); ?>
