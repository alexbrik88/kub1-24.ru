<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'progrev';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
        "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}";
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/html.php'); ?>

<table align="center" cellpadding="0" cellspacing="0" style="width:600px;">
    <tbody>
        <!-- IMG -->
        <tr>
            <td>
                <table cellpadding="0" cellspacing="0" width="600">
                    <tbody>
                        <tr>
                            <td align="center">
                                <img src="https://lk.kub-24.ru/img/unisender/delivery/%D0%BA%D0%B0%D1%80%D1%82%D0%B8%D0%BD%D0%BA%D0%B0-%D0%B4%D0%BB%D1%8F-%D0%BF%D0%B8%D1%81%D0%B5%D0%BC-%D1%80%D0%BE%D0%B1%D0%BE%D1%82-%D0%B1%D1%83%D1%85-2.jpg" width="600">
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <!-- END IMG -->
        <tr>
            <td>
                <table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px dashed #cccccc;">
                    <tbody><!-- CONTENT PART1-->
                        <tr>
                            <td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
                                <h1 style="margin-top:0; font-size:28px !important; line-height:100% !important; font-weight:500; color:#333333">
                                    Добрый день{{Name?, }}{{Name}}!
                                </h1>
                                Всегда приятно, когда вашими товарами и услугами интересуются.
                                Это означает, что клиент рассматривает возможность купить у вас товары или заказать услугу.
                                <br>
                                Важно отослать Прайс-лист быстро!
                                Но иногда нужно немного «поколдовать» - убрать товар,
                                которого нет в наличие или одному клиенту отправить прайс-лист с остатками,
                                а другому отправить без остатков на складе.
                                <br>
                                <br>
                                Об этом часто забывают, но <strong>прайс-лист — это маркетинговый инструмент</strong>.
                                Поэтому важно, чтобы он был хорошо сверстан, не содержал ошибок и, самое главное, провоцировал на покупку.
                                Для этого в нем обязательно должны быть указаны название и контакты вашей компании.
                                <br>
                                <br>
                                <strong>КУБ – упростил создание красивых и удобных прайс-листов!</strong>
                                <br>
                                Мы добавили в разделах Товар и Услуги кнопку ПРАЙС-ЛИСТ.
                                <br>
                                <img src="https://lk.kub-24.ru/img/unisender/delivery/bee8c5c596-2-%D0%BF%D0%B8%D1%81%D1%8C%D0%BC%D0%BE.jpg" width="550">
                                <br>
                                Теперь вы можете в пару кликов подготавливать нужный вам вид прайс-листа и сразу же из КУБа отсылаете его клиенту.
                                Клиент получит красивый прайс-лист с вашим логотипом и вашими контактами.
                                <br>
                                <img src="https://lk.kub-24.ru/img/unisender/delivery/bee8c5c596-1-%D0%BF%D0%B8%D1%81%D1%8C%D0%BC%D0%BE.jpg" width="550">
                                <br>
                                <strong>Важно!</strong>
                                При ответе клиентом на письмо с прайс-листом из КУБа, ответ придет <strong>на ваш e-mail</strong>.
                                Так же в прайс-листе указаны ваши контакты.
                                <br>
                                <br>
                                Какой бы товар вы не продавали, или какие бы услуги вы не оказывали,
                                красивый и удобный прайс-лист сделает ваш бизнес привлекательней в глаз клиентов.
                                <br>
                                <br>
                                <strong>
                                    P.S. Напишите мне, какие доработки в прайс-листе, вы считайте полезными для вашего бизнеса.
                                    Тем, кто напишет, от меня подарок
                                </strong>
                            </td>
                        </tr>
                        <!-- END CONTENT PART1 -->
                    </tbody>
                </table>
            </td>
        </tr>
        <!-- BUTTON -->
        <tr>
            <td>
                <table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
                    <tbody>
                        <tr>
                            <td align="center" style="background-color:#ffffff; font-family:Arial,Helvetica,sans-serif; line-height:100%; font-size:16px;">
                                <a href="https://lk.kub-24.ru/product/index?productionType=1&amp;<?= $linkParams ?>&amp;utm_content=button"
                                    style="color:#ffffff; font-weight:400; text-decoration:none; padding:10px 20px 10px 20px; background-color:#0077a7" target="_self">Подготовить ПРАЙС-ЛИСТ</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <!-- END BUTTON -->
    </tbody>
</table>

<?= $this->render('@common/mail/layouts/parts/_alexey', ['message' => $message]) ?>

<?= $this->render('@common/mail/layouts/parts/_links') ?>

<?php \Yii::$app->view->endContent(); ?>
