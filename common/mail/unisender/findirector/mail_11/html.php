<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'FinDir';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$this->params['utm_content'] = 'text-button';
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
    "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}&amp;utm_content={$this->params['utm_content']}";
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/html2.php'); ?>

<table cellpadding="0" cellspacing="0" style="width:600px;">
    <tbody>
        <tr>
            <td align="center">
                <?= Html::img('https://lk.kub-24.ru/img/unisender/findirector/letter-2021-11.png', [
                    'width' => 600,
                    'alt' => '',
                ]); ?>
            </td>
        </tr>
    </tbody>
</table>

<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody>
        <tr>
            <td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
                <div>
                    Как так, почему? Чтобы разобраться, предлагаю немного теории.
                    Разберемся с двумя базовыми понятиями: <strong>АКТИВЫ</strong> и <strong>ЗАТРАТЫ</strong>.
                </div>
                <div style="margin-top: 20px;">
                    Активы – раздел баланса, содержащий статистическую информацию о материальном имуществе и нематериальных ценностях предприятия. Или иными словами, станки, транспорт, товар на складе, сайт, дебиторы (так сказать люди, которые нам должны), а также ДЕНЬГИ.
                </div>
                <div style="margin-top: 20px;">
                    Идем дальше.
                    <strong>Когда я покупаю себе товар для дальней перепродажи, я обмениваю актив ДЕНЬГИ на актив ТОВАР.</strong>
                    Денежные ЗАТРАТЫ понес (потратил деньги)? – ДА. Активов стало меньше? – Нет.
                    Теперь у меня вместо денег товар, который я потом продам с наценкой.
                </div>
                <div style="margin-top: 20px;">
                    Следующая ситуация. Оплатил аренду в конце месяца. Денежные ЗАТРАТЫ понес (потратил деньги)? – ДА.
                    Активов стало меньше? – ДА, так как актив ДЕНЬГИ потрачен на возможность еще один месяц хранить свой товар.
                    Собственно, это и есть РАСХОД, так как произошло выбытие актива без возможности его повторного использования для получения прибыли.
                </div>
                <div style="margin-top: 20px;">
                    <strong>
                        Теперь ПРИМЕР.
                    </strong>
                </div>
                <table>
                    <tbody>
                        <tr>
                            <td style="vertical-align: top;">1)</td>
                            <td>
                                У Сергея интернет-магазин по продаже смартфонов Apple.
                                Сергей с хорошей скидкой закупил в июне 100 смартфонов по 50 000 рублей, итого ЗАТРАТИЛ 5 000 000 рублей.
                                То есть обменял актив ДЕНЬГИ на актив смартфоны.
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;">2)</td>
                            <td>
                                Летом продажи не очень, и Сергей в июне продал только 20 смартфонов по 100 000 (!) рублей за каждый,
                                итого выручил 2 000 000 рублей.
                                Затратил 5 миллионов, выручил только 2 миллиона, неужели Сергей, торгуя Apple, находится в убытке?
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;">3)</td>
                            <td>
                                Но Сергей доволен. У него на начало июня было 5 000 000 рублей активов в виде ДЕНЕГ.
                                В конце июня наблюдаем в активах 2 000 000 рублей в виде вырученных денег и 80 оставшихся смартфонов
                                на сумму 4 000 000 рублей (80*50 000 рублей).
                                Итого активы на конец июня составили 6 миллионов рублей, что на 1 миллион больше чем в начале месяца.
                                То есть прибыль у Сергея 1 000 000 рублей. Неплохо.
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;">4)</td>
                            <td>
                                Ну, или более простой способ рассчитать прибыль методом начисления:
                                <br>
                                [100 000 -50 000]*20 = 1 000 000 рублей.
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div style="margin-top: 20px;">
                    Если у вас более 20 продаж в месяц, то время на размышления и учет – что ЗАТРАТЫ, а что  РАСХОДЫ не будет.
                </div>
                <div style="margin-top: 20px;">
                    <strong>
                        Пусть баланс и отчеты корректно формирует КУБ24.ФинДиректор, а свое бесценное время потратьте на бизнес.
                    </strong>
                    <div>
                        «КУБ24.ФинДиректор» – ваш навигатор для бизнеса.
                    </div>
                </div>
            </td>
        </tr>
    </tbody>
</table>

<?= $this->render('@common/mail/layouts/parts/_alexey', ['message' => $message]) ?>

<?php \Yii::$app->view->endContent(); ?>
