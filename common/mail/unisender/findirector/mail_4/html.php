<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'FinDir';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$this->params['utm_content'] = 'text-button';
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
    "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}&amp;utm_content={$this->params['utm_content']}";
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/html2.php'); ?>

<table cellpadding="0" cellspacing="0" style="width:600px;">
    <tbody>
        <tr>
            <td align="center">
                <?= Html::img('https://lk.kub-24.ru/img/unisender/findirector/letter-2021-4.png', [
                    'width' => 600,
                    'alt' => '',
                ]); ?>
            </td>
        </tr>
    </tbody>
</table>

<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody>
        <tr>
            <td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
                <div style="">
                    Ситуация встречается часто, когда на бумаге прибыль есть, а по факту денег на счетах нет.
                    Как результат у собственника нет возможности получить дивиденды.
                </div>
                <div style="margin-top: 20px;">
                    <strong>
                        Почему такая ситуация случается?
                    </strong>
                    <div>
                        Всё просто – прибыль заморозилась в каком-то активе.
                        Чаще всего это дебиторская задолженность и/или запасы на складе, но это могут быть и основные средства,
                        например купленное оборудование или транспортное средство.
                    </div>
                </div>
                <div style="margin-top: 20px;">
                    <strong>
                        Что бы увидеть, где прячется прибыль, вам нужно 2 отчета:
                    </strong>
                    <div>
                        <span style="text-decoration: underline;">Отчет о прибылях и убытках</span>
                        – покажет, сколько вы заработали.
                        <br>
                        <span style="text-decoration: underline;">Баланс</span>
                        – покажет, где заморожены деньги.
                    </div>
                </div>
                <div style="margin-top: 20px;">
                    <strong>
                        Что бы спланировать, когда вы сможете забрать свои дивиденды, вам понадобятся другие два отчета:
                    </strong>
                    <div>
                        <span style="text-decoration: underline;">Отчет о движение денежных средств</span>
                        и
                        <span style="text-decoration: underline;">Платежный календарь</span>
                        – вместе они покажут, когда вы сможете вынуть дивиденды без создания кассового разрыва в бизнесе.
                    </div>
                </div>
                <div style="margin-top: 20px;">
                    <strong>
                        ПРИБЫЛЬ отличается от ДЕНЕГ! Прибыль считается по обязательствам, а деньги — по факту.
                    </strong>
                    <div>
                        «КУБ24.ФинДиректор» поможет и подскажет.
                    </div>
                </div>
                <div style="margin-top: 20px; text-align: center;">
                    <?= Html::a('Записаться на демонстрацию сервиса', 'https://kub-24.ru/webinar/?consigliori=true&amp;'.$linkParams, [
                        'style' => 'color:#ffffff; text-decoration:none; font-family:Arial,Helvetica,sans-serif; font-size: 16px; padding: 10px 20px 10px 20px; background-color: #4679ae; border-radius:4px;',
                        'target' => '_blank',
                    ]) ?>
                </div>
            </td>
        </tr>
    </tbody>
</table>

<?= $this->render('@common/mail/layouts/parts/_alexey', ['message' => $message]) ?>

<?php \Yii::$app->view->endContent(); ?>
