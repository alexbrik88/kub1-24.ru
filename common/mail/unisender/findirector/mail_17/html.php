<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'FinDir';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$this->params['utm_content'] = 'text-button';
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
    "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}&amp;utm_content={$this->params['utm_content']}";
$youtube = Html::img('https://lk.kub-24.ru/img/ico-sotial/youtube.png', [
    'style' => 'vertical-align: middle; margin-left: -10px; margin-right: 5px; width: 28px;',
    'alt' => '',
]);
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/html2.php'); ?>

<table cellpadding="0" cellspacing="0" style="width:600px;">
    <tbody>
        <tr>
            <td align="center">
                <?= Html::img('https://lk.kub-24.ru/img/unisender/findirector/letter-2021-17.png', [
                    'width' => 600,
                    'alt' => '',
                ]); ?>
            </td>
        </tr>
    </tbody>
</table>

<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody>
        <tr>
            <td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
                <div>
                    <strong>Дебиторская задолженность – это когда нам должны. </strong>
                    <div>
                        Две основных ситуации возникновения дебиторки:
                    </div>
                    <ul style="margin: 0; padding: 0; padding-left: 20px;">
                        <li>
                            отгрузили товар (оказали услугу), а деньги через неделю;
                        </li>
                        <li>
                            сделали предоплату за услугу (товар), которая будет выполнена позже.
                        </li>
                    </ul>
                </div>
                <div style="margin-top: 20px;">
                    Конечно, было бы идеально и здорово, когда тебе платят день в день, и в рознице это возможно.
                    Но если ты оптовик или у тебя производство, дебиторка неизбежна.
                    Бывают ситуации, когда продажа без отсрочки в принципе невозможна, например, продажи в торговые сети.
                </div>
                <div style="margin-top: 20px;">
                    Очень важный момент! <strong>С появлением дебиторки в балансе, ты СТАНОВИШЬСЯ БАНКИРОМ,</strong>
                    так как должен теперь контролировать, чтобы клиенты вернули деньги в полном объеме и в указанный срок.
                    Если клиенты вовремя не возвращают деньги, то дебиторка превращается в плохую или «токсичные активы»
                    (термин со времен кризиса 2008).
                </div>
                <div style="margin-top: 20px;">
                    Пожалуй, единственным способом не допускать просроченную дебиторку,
                    это постоянно мониторить платежи клиентов в разрезе дат, договоров, конкретных сделок
                    и всегда иметь на руках актуальную информацию по должникам.
                    Заблаговременно оповещать клиентов об оплате, напоминать о платеже в день оплаты,
                    и уж тем более не «слезать» с должника, если возникла просрочка.
                </div>
                <div style="margin-top: 20px;">
                    Хранить и обновлять всю информацию о дебиторах в Excel или даже в 1С достаточно сложно, у себя в голове – невозможно.
                    В тех же банках применяют очень дорогие специализированные программы.
                    Применение сервиса КУБ24.ФинДиректор позволит Вам качественно и эффективно работать с дебиторской задолженностью
                    и предотвращать просрочку.
                </div>
                <div style="margin-top: 20px; text-align: center;">
                    <?= Html::a($youtube.' Работа с должниками', 'https://kub-24.ru/video?'.$linkParams, [
                        'style' => 'color:#ffffff; text-decoration:none; font-family:Arial,Helvetica,sans-serif; font-size: 16px; padding: 10px 20px 10px 20px; background-color: #4679ae; border-radius:4px;',
                        'target' => '_blank',
                    ]) ?>
                </div>
                <div style="margin-top: 20px;">
                    <strong>
                        Эффективная работа с должникам!
                    </strong>
                    <div>
                        «КУБ24.ФинДиректор» - ваш навигатор для бизнеса.
                    </div>
                </div>
            </td>
        </tr>
    </tbody>
</table>

<?= $this->render('@common/mail/layouts/parts/_alexey', ['message' => $message]) ?>

<?php \Yii::$app->view->endContent(); ?>
