<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'FinDir';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$this->params['utm_content'] = 'text-button';
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
    "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}&amp;utm_content={$this->params['utm_content']}";
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/html2.php'); ?>

<table cellpadding="0" cellspacing="0" style="width:600px;">
    <tbody>
        <tr>
            <td align="center">
                <?= Html::img('https://lk.kub-24.ru/img/unisender/findirector/letter-2021-16.png', [
                    'width' => 600,
                    'alt' => '',
                ]); ?>
            </td>
        </tr>
    </tbody>
</table>

<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody>
        <tr>
            <td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
                <div>
                    <strong>Прибыль есть, а денег нет.</strong>
                    Кажется, что это абсурд. Уверяю, с подобной ситуацией сталкиваются многие предпринимателей.
                    Подобное расхождение связано с различными методами учета прибыли и денег, метод начисления и кассовый метод соответственно.
                </div>
                <div style="margin-top: 20px;">
                    Прибыль считается в момент отгрузки товара, готовой продукции или оказания услуг.
                    Из выручки вычитаем все расходы и получаем прибыль.
                    Но, если сделка с отсрочкой платежа и деньги от продажи придут спустя неделю,
                    то и монетизация (превращение прибыли в деньги) произойдет спустя 7 дней.
                </div>
                <div style="margin-top: 20px;">
                    Бизнес нельзя поставить на паузу и дождаться пока с тобой рассчитаются по каждой сделке.
                    Как правило, деньги от предыдущих продаж уже в обороте, а от текущих сделок еще не поступили на счет.
                </div>
                <div style="margin-top: 20px;">
                    Выход есть. Необходимо формировать фонд дивидендов, можно назвать <strong>«Фонд покупки авто (квартиры)».</strong>
                    То есть установить норму отчисления (например, 1%) от всех поступивших на расчетный счет денег и аккумулировать их на отдельном счете.
                    И если прибыль составила 1 000 000 рублей, то максимальная сумма дивидендов может составить не более миллиона, и главное
                    Вы видите и чувствуете свою прибыль. Можно автомобиль поменять, съездить отдохнуть.
                </div>
                <div style="margin-top: 20px;">
                    Конечно, учет можно вести в Excel, но управление нормами накопления и сопоставление отчетов по прибыли и денежных средств операции достаточно сложные и математические.
                </div>
                <div style="margin-top: 20px;">
                    <strong>
                        Что бы ваши личные планы выполнялись, формируйте фонд дивидендов в бизнесе.
                    </strong>
                    <div>
                        «КУБ24.ФинДиректор» - ваш навигатор для бизнеса.
                    </div>
                </div>
            </td>
        </tr>
    </tbody>
</table>

<?= $this->render('@common/mail/layouts/parts/_alexey', ['message' => $message]) ?>

<?php \Yii::$app->view->endContent(); ?>
