<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'FinDir';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$this->params['utm_content'] = 'text-button';
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
    "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}&amp;utm_content={$this->params['utm_content']}";
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/html2.php'); ?>

<table cellpadding="0" cellspacing="0" style="width:600px;">
    <tbody>
        <tr>
            <td align="center">
                <?= Html::img('https://lk.kub-24.ru/img/unisender/findirector/letter-2021-9.png', [
                    'width' => 600,
                    'alt' => '',
                ]); ?>
            </td>
        </tr>
    </tbody>
</table>

<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody>
        <tr>
            <td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
                <div>
                    Бесплатный период заканчивается уже завтра!
                </div>
                <div style="margin-top: 20px;">
                    <strong>Мы предлагаем:</strong>
                </div>
                <table>
                    <tbody>
                        <tr>
                            <td style="vertical-align: top; font-weight: bold;">1)</td>
                            <td>
                                <div>
                                    Если вы еще не полностью ознакомились со всеми возможностями, то
                                    <strong>запишитесь на 30 минутную демонстрацию сервиса с нашим консультантом.</strong>
                                    Сможете задать вопросы и узнать максимум о возможностях КУБ24.
                                </div>
                                <div style="margin-top: 20px; margin-bottom: 20px; text-align: center;">
                                    <?= Html::a('Записаться на демонстрацию сервиса', 'https://kub-24.ru/webinar/?consigliori=true&amp;'.$linkParams, [
                                        'style' => 'color:#ffffff; text-decoration:none; font-family:Arial,Helvetica,sans-serif; font-size: 16px; padding: 10px 20px 10px 20px; background-color: #4679ae; border-radius:4px;',
                                        'target' => '_blank',
                                    ]) ?>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top; font-weight: bold;">2)</td>
                            <td>
                                <strong>Выберете способ оплаты</strong> КУБ24. ФинДиректор – со скидкой или без:
                                <ul style="margin: 0; padding: 0; padding-left: 20px;">
                                    <li>
                                        <strong>Без скидки</strong> – оплата за один месяц <strong>2500 руб.</strong>
                                    </li>
                                    <li>
                                        <strong>Со скидкой 10%</strong> – оплата за 4 месяца 9000 руб., т.е. <strong>2250 руб./месяц.</strong>
                                    </li>
                                    <li>
                                        <strong>Со скидкой 20%</strong> – оплата за 12 месяцев 24 000 руб., т.е. <strong>2000 руб./месяц.</strong>
                                    </li>
                                </ul>
                                <div style="margin-top: 10px;">
                                    Посмотреть подробнее можно тут
                                </div>
                                <div style="margin-top: 10px; margin-bottom: 20px;">
                                    <?= Html::img('https://lk.kub-24.ru/img/unisender/findirector/letter-2021-9-d.png', [
                                        'style' => 'max-width: 300px;',
                                        'alt' => '',
                                    ]); ?>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top; font-weight: bold;">3)</td>
                            <td>
                                Напишите ответным письмом, что вам нужно, но нет или не нашли в «КУБ24.ФинДиректор».
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div style="margin-top: 20px;">
                    <strong>
                        Мы сделаем доработку под вас!
                    </strong>
                    <div>
                        «КУБ24.ФинДиректор» - ваш навигатор для бизнеса.
                    </div>
                </div>
            </td>
        </tr>
    </tbody>
</table>

<?= $this->render('@common/mail/layouts/parts/_arslan', ['message' => $message]) ?>

<?php \Yii::$app->view->endContent(); ?>
