<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'FinDir';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$this->params['utm_content'] = 'text-button';
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
    "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}&amp;utm_content={$this->params['utm_content']}";
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/html2.php'); ?>

<table cellpadding="0" cellspacing="0" style="width:600px;">
    <tbody>
        <tr>
            <td align="center">
                <?= Html::img('https://lk.kub-24.ru/img/unisender/findirector/letter-2021-7.png', [
                    'width' => 600,
                    'alt' => '',
                ]); ?>
            </td>
        </tr>
    </tbody>
</table>

<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody>
        <tr>
            <td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
                <div>
                    Есть собственники, которые забирают дивиденды из бизнеса редко, например 1 раз в год.
                </div>
                <div style="margin-top: 20px;">
                    <strong>
                        Почему это плохо?
                    </strong>
                    <div>
                        Прибыль, которую не забрал собственник, расходится на закупку товаров и потом оседает в складских запасах, растворяется в оборотке, которая оседает в дебиторке или уходит в закупку машин и оборудования. На бумаге у вас прибыль есть, а по факту достать её из бизнеса уже сложно, а иногда это губительно действует на компанию.
                    </div>
                </div>
                <div style="margin-top: 20px;">
                    <strong>
                        Интересный парадокс
                    </strong>
                    – если забирать дивиденды чаще, например, ежеквартально или ежемесячно, то бизнес становиться более эффективным!
                </div>
                <div style="margin-top: 20px;">
                    <strong>
                        Почему бизнес будет более эффективным?
                    </strong>
                    <div>
                        Все просто - чтобы регулярно платить дивиденды, компания должна:
                    </div>
                </div>
                <ul style="margin: 0; padding: 0; padding-left: 20px;">
                    <li>
                        выстроить все бизнес-процессы так, что бы лучше собиралась просроченная дебиторка,
                    </li>
                    <li>
                        улучшить условия от поставщиков,
                    </li>
                    <li>
                        оптимизировать закупку товара, что бы на складе было меньше неликвида,
                    </li>
                    <li>
                        измерять эффективность каждого сотрудника.
                    </li>
                </ul>
                <div>
                    …и многое другое. Это всё позволяет бизнесу быть более эффективным и высвобождать деньги для выплаты дивидендов.
                </div>
                <div style="margin-top: 20px;">
                    <strong>
                        Как это сделать?
                    </strong>
                    <div>
                        Начните всё измерять и сравнивать: динамику по дебиторке, эффективность каждого проекта,
                        кол-во запасов на складе и их оборачиваемость, эффективность рекламных компаний и эффективность сотрудников.
                    </div>
                </div>
                <div style="margin-top: 20px;">
                    <strong>
                        Регулярные дивиденды – это эффективные управленческие решения!
                    </strong>
                    <div>
                        «КУБ24.ФинДиректор» поможет и подскажет.
                    </div>
                </div>
            </td>
        </tr>
    </tbody>
</table>

<?= $this->render('@common/mail/layouts/parts/_alexey', ['message' => $message]) ?>

<?php \Yii::$app->view->endContent(); ?>
