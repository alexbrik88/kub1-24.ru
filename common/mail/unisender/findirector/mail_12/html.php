<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'FinDir';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$this->params['utm_content'] = 'text-button';
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
    "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}&amp;utm_content={$this->params['utm_content']}";
$youtube = Html::img('https://lk.kub-24.ru/img/ico-sotial/youtube.png', [
    'style' => 'vertical-align: middle; margin-left: -10px; margin-right: 5px; width: 28px;',
    'alt' => '',
]);
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/html2.php'); ?>

<table cellpadding="0" cellspacing="0" style="width:600px;">
    <tbody>
        <tr>
            <td align="center">
                <?= Html::img('https://lk.kub-24.ru/img/unisender/findirector/letter-2021-12.png', [
                    'width' => 600,
                    'alt' => '',
                ]); ?>
            </td>
        </tr>
    </tbody>
</table>

<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody>
        <tr>
            <td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
                <div>
                    Налоги, аренда, зарплата, поставщики, - всем нужны твои деньги. Кассовый разрыв день, через день.
                    Сегодня закрыли налоги, а завтра день зарплаты, что делать с арендой вообще не понятно.
                    <strong>Это и есть немедицинское определение стресса.</strong>
                </div>
                <div style="margin-top: 20px;">
                    А что, если взять и прикинуть все поступления и расходы денежных средств. Сначала на неделю вперед, затем на месяц.
                    Все планируемые операции сведем в один отчет, и каждый день фиксируем фактические платежи.
                    Анализируем отклонения, «беспокоим» дебиторов, аккумулируем деньги, готовимся к пиковым выплатам.
                    Это и есть <strong>Платежный календарь, простой и эффективный инструмент контроля за бизнесом,</strong> такой же, как аспирин.
                </div>
                <div style="margin-top: 20px;">
                    Но, как всегда, не все так просто. Вести Платежный календарь необходимо ежедневно и скрупулезно, иначе будет только видимость контроля. Можно вести самому, например в Excel, или поручить бухгалтеру. Результат очевиден, через месяц все сведется, скажем так, к формализму. Очевидно, что нужна автоматизация. Именно для решения таких задач мы создали в КУБ24.ФинДиректор – автоматический Платежный календарь, который строит плановые платежи либо на основе документов, с учетом отсрочек оплат либо на основе платежей в прошедших периодах. Ну и конечно можно настроить одновременно и по документам и на основе предыдущих периодов.
                </div>
                <div style="margin-top: 20px; text-align: center;">
                    <?= Html::a($youtube.' Умный платежный календарь', 'https://kub-24.ru/video?'.$linkParams, [
                        'style' => 'color:#ffffff; text-decoration:none; font-family:Arial,Helvetica,sans-serif; font-size: 16px; padding: 10px 20px 10px 20px; background-color: #4679ae; border-radius:4px;',
                        'target' => '_blank',
                    ]) ?>
                </div>
                <div style="margin-top: 20px;">
                    <strong>
                        Зачем вводить данные каждый день, если за вас все делают роботы
                    </strong>
                    <div>
                        «КУБ24.ФинДиректор» – ваш навигатор для бизнеса.
                    </div>
                </div>
            </td>
        </tr>
    </tbody>
</table>

<?= $this->render('@common/mail/layouts/parts/_alexey', ['message' => $message]) ?>

<?php \Yii::$app->view->endContent(); ?>
