<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'FinDir';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$this->params['utm_content'] = 'text-button';
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
    "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}&amp;utm_content={$this->params['utm_content']}";
$videoLink = Html::a('Обучающее видео', 'https://kub-24.ru/video/');
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/html2.php'); ?>

<table cellpadding="0" cellspacing="0" style="width:600px;">
    <tbody>
        <tr>
            <td align="center">
                <?= Html::img('https://lk.kub-24.ru/img/unisender/findirector/letter-2021-2.png', [
                    'width' => 600,
                    'alt' => '',
                ]); ?>
            </td>
        </tr>
    </tbody>
</table>

<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody>
        <tr>
            <td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
                <div>
                    Что бы настроить КУБ24 под Вас, выполните пункты ниже:
                </div>
                <table>
                    <tbody>
                        <tr>
                            <td style="vertical-align: top;">1.</td>
                            <td>
                                <strong>
                                    Внесите данные по вашей компании и добавьте банковские счета и кассы –
                                    <span style="color: #1e91cf;">3 минуты</span>
                                </strong>
                                <div>
                                    Наличие реквизитов по компании, позволит импортировать данные без ошибок, а вам будет удобнее проводить операции по счетам и кассам.
                                </div>
                                <div>
                                    <?= $videoLink ?>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;">2.</td>
                            <td>
                                <strong>
                                    Подключите импорт данных из банка –
                                    <span style="color: #1e91cf;">3 минуты</span>
                                </strong>
                                <div>
                                    Для получения операций из банка за предыдущий период и каждое утро за предыдущий день.
                                    Если с вашим банком нет интеграции, то нужно из Клиент-Банка выгрузить выписку в формате «для 1С», у файла будет расширение «.txt»
                                </div>
                                <div>
                                    <?= $videoLink ?>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;">3.</td>
                            <td>
                                <strong>
                                    Загрузите данные по кассе из Excel –
                                    <span style="color: #1e91cf;">15 минут</span>
                                </strong>
                                <div>
                                    Для отражения операций по кассе.
                                </div>
                                <div>
                                    <?= $videoLink ?>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;">4.</td>
                            <td>
                                <strong>
                                    Разнести операции по категориям –
                                    <span style="color: #1e91cf;">30 минут</span>
                                </strong>
                                <div>
                                    Один раз закрепляете статьи прихода и расхода за контрагентами и далее они будут проставляться автоматически.
                                </div>
                                <div>
                                    <?= $videoLink ?>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;">5.</td>
                            <td>
                                <strong>
                                    Синхронизируйтесь с вашей с 1С –
                                    <span style="color: #1e91cf;">30 минут</span>
                                </strong>
                                <div>
                                    Для загрузки списка товаров, услуг, актов, товарных накладных и УПД.
                                    Это позволит точно считать и анализировать дебиторку и кредиторку, а также анализировать продажи в разрезе товаров, услуг, покупателей и поставщиков.
                                </div>
                                <div>
                                    <?= $videoLink ?>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div style="margin-top: 20px;">
                    На сегодня это всё.
                    <br>
                    В следующие несколько дней расскажем про учет финансов по проектам, про причины кассовых разрывов, про дивиденды, а также про помощников и как они будут вам полезны.
                </div>
                <div style="margin-top: 20px;">
                    <strong>
                        ПОРЯДОК в ФИНАНСАХ – Вы сможете принимать решения по бизнесу на основе цифр.
                    </strong>
                    <div>
                        «КУБ24.ФинДиректор» - ваш навигатор для бизнеса.
                    </div>
                </div>
            </td>
        </tr>
    </tbody>
</table>

<?= $this->render('@common/mail/layouts/parts/_alexey', ['message' => $message]) ?>

<?php \Yii::$app->view->endContent(); ?>
