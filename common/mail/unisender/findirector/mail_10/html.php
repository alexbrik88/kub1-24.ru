<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'FinDir';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$this->params['utm_content'] = 'text-button';
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
    "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}&amp;utm_content={$this->params['utm_content']}";
$youtube = Html::img('https://lk.kub-24.ru/img/ico-sotial/youtube.png', [
    'style' => 'vertical-align: middle; margin-left: -10px; margin-right: 5px; width: 28px;',
    'alt' => '',
]);
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/html2.php'); ?>

<table cellpadding="0" cellspacing="0" style="width:600px;">
    <tbody>
        <tr>
            <td align="center">
                <?= Html::img('https://lk.kub-24.ru/img/unisender/findirector/letter-2021-10-1.png', [
                    'width' => 600,
                    'alt' => '',
                ]); ?>
            </td>
        </tr>
    </tbody>
</table>

<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody>
        <tr>
            <td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
                <div>
                    Финансовая модель – дает понимание, сколько вы заработаете через три месяца, полгода или год. Она ответит на вопрос – стоит ли открывать новое направление или ввязываться в новый проект.
                </div>
                <div style="margin-top: 20px;">
                    <strong>
                        Финансовая модель ответит на эти и многие другие вопросы:
                    </strong>
                    <ul style="margin: 0; padding: 0; padding-left: 20px;">
                        <li>
                            Сколько нужно денег, что бы запустить новое направление бизнеса?
                        </li>
                        <li>
                            Сколько нужно денег, что бы открыть новую точку продаж?
                        </li>
                        <li>
                            Когда направление или точка продаж выйдет в ноль?
                        </li>
                        <li>
                            Как проверить идею по изменению чего-то в бизнесе?
                        </li>
                        <li>
                            Какая минимальная наценка на товар/услугу должны быть, чтобы бизнес работал в плюс?
                        </li>
                        <li>
                            Какой показатель дает максимальную прибавку в прибыли?
                        </li>
                        <li>
                            Сколько денег нужно вложить в рекламу, что бы получить желаемое кол-во продаж?
                        </li>
                        <li>
                            Какие планы должны быть у отдела продаж, что бы выйти на желаемую прибыль?
                        </li>
                        <li>
                            Какая должна быть система мотивации?
                        </li>
                        <li>
                            Сколько дивидендов можно вывести из бизнеса без риска?
                        </li>
                        <li>
                            Как оценить эффективность инвестиций в бизнес?
                        </li>
                    </ul>
                </div>
                <div style="margin-top: 20px;">
                    Если вы сталкиваетесь с подобными вопросами, и у вас нет однозначного ответа, то самое время построить финмодель, что бы получить решения.
                </div>
                <div style="margin-top: 20px; text-align: center;">
                    <?= Html::a($youtube.' Построение финмодели', 'https://kub-24.ru/video?'.$linkParams, [
                        'style' => 'color:#ffffff; text-decoration:none; font-family:Arial,Helvetica,sans-serif; font-size: 16px; padding: 10px 20px 10px 20px; background-color: #4679ae; border-radius:4px;',
                        'target' => '_blank',
                    ]) ?>
                </div>
                <div style="margin-top: 20px;">
                    <strong>
                        Эффективные решения не возможны без прогнозирования с помощью финансовых моделей!
                    </strong>
                    <div>
                        «КУБ24.ФинДиректор» поможет и подскажет.
                    </div>
                </div>
            </td>
        </tr>
    </tbody>
</table>

<?= $this->render('@common/mail/layouts/parts/_alexey', ['message' => $message]) ?>

<?php \Yii::$app->view->endContent(); ?>
