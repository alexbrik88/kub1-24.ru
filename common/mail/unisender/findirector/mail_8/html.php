<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'FinDir';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$this->params['utm_content'] = 'text-button';
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
    "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}&amp;utm_content={$this->params['utm_content']}";
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/html2.php'); ?>

<table cellpadding="0" cellspacing="0" style="width:600px;">
    <tbody>
        <tr>
            <td align="center">
                <?= Html::img('https://lk.kub-24.ru/img/unisender/findirector/letter-2021-8.png', [
                    'width' => 600,
                    'alt' => '',
                ]); ?>
            </td>
        </tr>
    </tbody>
</table>

<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody>
        <tr>
            <td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
                <div>
                    В обычной жизни мы уже привыкли к разному роду помощников. Все начиналось с помощников по хозяйству – «Муж на час».
                    Пришел, повесил полку на стену, починил замок и еще пару дел сделал, которые вы уже месяца три собираетесь сделать.
                    В итоге вы довольны, супруга тоже.
                    <br>
                    Сегодня таких помощников на любой вкус, самые популярные  – курьеры, которые доставляют нам еду из ресторанов,
                    покупки из супермаркетов, покупки из интернет-магазинов.
                    Они экономят нам кучу времени, а мы тратим это время с пользой для себя.
                </div>
                <div style="margin-top: 20px;">
                    В бизнесе точно так же – руководитель должен делегировать.
                    Но некоторые бояться этого – сотрудник сделает не так как «я привык» или наделает ошибок.
                    В итоге придется все перепроверять и переделывать.
                    У многих предпринимателей принцип «Если хочешь сделать хорошо, то сделай сам».
                    Увы, с такими принципами – вы много сделать не успеете.
                </div>
                <div style="margin-top: 20px;">
                    <strong>Мы предлагаем вам снять с себя рутинную работу</strong>
                    и передать её нашим проверенным и опытным Ассистентам.
                </div>
                <div>
                    <strong>
                        Первый вариант заберет с вас всю рутину по вводу данных – «АССИСТЕНТ по вводу данных».
                    </strong>
                </div>
                <div>
                    Он будет ежемесячно:
                </div>
                <div style="padding-left: 20px;">
                    <ul style="margin: 0; padding: 0; padding-left: 20px;">
                        <li>
                            Загружать банковские выписки 1 раз в неделю
                        </li>
                        <li>
                            Загружать данные из 1С 1 раз в неделю
                        </li>
                        <li>
                            Загружать операций по кассе 1 раз в неделю
                        </li>
                        <li>
                            Назначать статьи расходов и доходов по новым операциям
                        </li>
                    </ul>
                    <div>
                        <strong>Стоимость:</strong> 10 000 рублей в месяц
                    </div>
                    <div>
                        <?= Html::a('Прайс-лист', 'https://lk.kub-24.ru/price-list/out-view?uid=a9e2fb823a22eb6c&amp;'.$linkParams, [
                            'target' => '_blank',
                        ]) ?>
                    </div>
                </div>
                <div style="margin-top: 20px;">
                    <strong>
                        Второй вариант помимо рутины еще будет анализировать финансовое положение вашего бизнеса и давать рекомендации.
                    </strong>
                </div>
                <div>
                    <strong>
                        «АССИСТЕНТ по финмодели»
                    </strong>
                    - это тот человек, с кем вам можно и нужно советоваться по вашим финансам.
                </div>
                <div>
                    Он будет ежемесячно:
                </div>
                <div style="padding-left: 20px;">
                    <ul style="margin: 0; padding: 0; padding-left: 20px;">
                        <li>
                            Первоначальное построение финмодели
                        </li>
                        <li>
                            Сопровождать финмодель
                        </li>
                        <li>
                            Созваниваться 2 раза в месяц для проведения анализа финансового положения и предоставления вам рекомендаций.
                        </li>
                    </ul>
                    <div>
                        <strong>Стоимость:</strong> 25 000 рублей в месяц
                    </div>
                    <div>
                        <?= Html::a('Прайс-лист', 'https://lk.kub-24.ru/price-list/out-view?uid=a9e2fb823a22eb6c&amp;'.$linkParams, [
                            'target' => '_blank',
                        ]) ?>
                    </div>
                </div>
                <div style="margin-top: 20px;">
                    <strong>
                        Освободите время для принятия решений, а рутиной займется АССИСТЕНТ
                    </strong>
                    <div>
                        «КУБ24.ФинДиректор» поможет.
                    </div>
                </div>
            </td>
        </tr>
    </tbody>
</table>

<?= $this->render('@common/mail/layouts/parts/_arslan', ['message' => $message]) ?>

<?php \Yii::$app->view->endContent(); ?>
