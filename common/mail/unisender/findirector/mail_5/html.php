<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'FinDir';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$this->params['utm_content'] = 'text-button';
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
    "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}&amp;utm_content={$this->params['utm_content']}";
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/html2.php'); ?>

<table cellpadding="0" cellspacing="0" style="width:600px;">
    <tbody>
        <tr>
            <td align="center">
                <?= Html::img('https://lk.kub-24.ru/img/unisender/findirector/letter-2021-5.png', [
                    'width' => 600,
                    'alt' => '',
                ]); ?>
            </td>
        </tr>
    </tbody>
</table>

<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody>
        <tr>
            <td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
                <div>
                    Мы знаем, что вносить операции, загружать выписки и эксель файлы в систему управленческого учета занимает время.
                    Поэтому у нас есть специально обученные сотрудники для этой работы.
                    Опция Ассистента подойдет для тех, у кого не хватает времени на самостоятельное ведение учета и тем,
                    кто хочет быстрый и правильный результат с самого начала.
                </div>
                <div style="margin-top: 20px;">
                    <strong style="text-decoration: underline;">
                        Варианты Ассистентов и их стоимость
                    </strong>
                </div>
                <table>
                    <tbody>
                        <tr>
                            <td style="vertical-align: top; font-weight: bold;">1)</td>
                            <td>
                                <strong>
                                    АССИСТЕНТ по первоначальной настройке сервиса под Вас:
                                </strong>
                                <ul style="margin: 0; padding: 0; padding-left: 20px;">
                                    <li>
                                        Согласно вашим Эксель таблицам настройка статей расхода и дохода
                                    </li>
                                    <li>
                                        Ввод контрагентов
                                    </li>
                                    <li>
                                        Добавление товарных позиций
                                    </li>
                                    <li>
                                        Настройка интеграции с банком
                                    </li>
                                    <li>
                                        Настройка интеграции с 1С
                                    </li>
                                    <li>
                                        Проведение обучения по сервису
                                    </li>
                                    <li>
                                        В течении месяца будет отвечать на все вопросы по сервису и по управленческому учету.
                                    </li>
                                </ul>
                                <div>
                                    <strong>Стоимость:</strong> 12 000 рублей
                                </div>
                                <div>
                                    <strong>Что сделать, что бы ни платить за настройку?</strong>
                                </div>
                                <div>
                                    Оплачиваете тариф «ФинДиректор +» за год и получаете первоначальную настройку БЕСПЛАТНО.
                                    Или оплачиваете тариф «ФинДиректор» и получаете скидку 50%
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top; font-weight: bold;">2)</td>
                            <td>
                                <strong>
                                    АССИСТЕНТ по вводу данных, будет ежемесячно:
                                </strong>
                                <ul style="margin: 0; padding: 0; padding-left: 20px;">
                                    <li>
                                        Загружать банковские выписки 1 раз в неделю
                                    </li>
                                    <li>
                                        Загружать данные из 1С 1 раз в неделю
                                    </li>
                                    <li>
                                        Загружать операций по кассе 1 раз в неделю
                                    </li>
                                    <li>
                                        Назначать статьи расходов и доходов по новым операциям
                                    </li>
                                </ul>
                                <div>
                                    <strong>Стоимость:</strong> 10 000 рублей в месяц
                                </div>
                                <div>
                                    <strong>Как получить скидку 50%?</strong>
                                </div>
                                <div>
                                    Оплачиваете тариф «ФинДиректор +» за год и Ассистента по вводу данных тоже за год со скидкой 50%.
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top; font-weight: bold;">3)</td>
                            <td>
                                <strong>
                                    АССИСТЕНТ по финмодели, будет ежемесячно:
                                </strong>
                                <ul style="margin: 0; padding: 0; padding-left: 20px;">
                                    <li>
                                        Первоначальное построение финмодели
                                    </li>
                                    <li>
                                        Сопровождать финмодель
                                    </li>
                                    <li>
                                        Созваниваться 2 раза в месяц для проведения анализа финансового положения и предоставления вам рекомендаций.
                                    </li>
                                </ul>
                                <div>
                                    <strong>Стоимость:</strong> 25 000 рублей в месяц
                                </div>
                                <div>
                                    <strong>Как получить скидку 20%?</strong>
                                </div>
                                <div>
                                    Оплачиваете тариф «ФинДиректор +» за год и Ассистента по финмодели тоже за год со скидкой 20%.
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div style="margin-top: 20px; text-align: center;">
                    <?= Html::a('Стоимость Ассистентов + скидки', 'https://lk.kub-24.ru/price-list/out-view?uid=a9e2fb823a22eb6c&amp;'.$linkParams, [
                        'style' => 'color:#ffffff; text-decoration:none; font-family:Arial,Helvetica,sans-serif; font-size: 16px; padding: 10px 20px 10px 20px; background-color: #4679ae; border-radius:4px;',
                        'target' => '_blank',
                    ]) ?>
                </div>
                <div style="margin-top: 20px;">
                    <strong>
                        Освободите время для принятия решений, а рутиной займется АССИСТЕНТ
                    </strong>
                    <br>
                    «КУБ24.ФинДиректор» поможет.
                </div>
            </td>
        </tr>
    </tbody>
</table>

<?= $this->render('@common/mail/layouts/parts/_arslan', ['message' => $message]) ?>

<?php \Yii::$app->view->endContent(); ?>
