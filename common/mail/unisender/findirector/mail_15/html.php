<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'FinDir';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$this->params['utm_content'] = 'text-button';
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
    "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}&amp;utm_content={$this->params['utm_content']}";
$youtube = Html::img('https://lk.kub-24.ru/img/ico-sotial/youtube.png', [
    'style' => 'vertical-align: middle; margin-left: -10px; margin-right: 5px; width: 28px;',
    'alt' => '',
]);
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/html2.php'); ?>

<table cellpadding="0" cellspacing="0" style="width:600px;">
    <tbody>
        <tr>
            <td align="center">
                <?= Html::img('https://lk.kub-24.ru/img/unisender/findirector/letter-2021-15.png', [
                    'width' => 600,
                    'alt' => '',
                ]); ?>
            </td>
        </tr>
    </tbody>
</table>

<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody>
        <tr>
            <td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
                <div>
                    Как говорил Наполеон: «Война план покажет». Мы, конечно, не историки, но аналогии напрашиваются сами собой.
                    Жизнь бизнеса без плана будет яркой, но не долгой.
                    Поэтому если возникает альтернатива между планировать или не планировать, однозначно выбираем ПЛАН.
                </div>
                <div style="margin-top: 20px;">
                    В системе управления бизнесом <strong>ПЛАН представляет собой траекторию движения к ЦЕЛИ.</strong>
                    Как узнать, что мы не сбились с пути?
                    Очень просто, фиксировать фактические показатели (ФАКТ) и сравнивать с ПЛАНОМ.
                    Для этого применяется <strong>отчет План-Факт</strong> – рабочий инструмент, позволяющий держать руку на пульсе бизнеса.
                </div>
                <div style="margin-top: 20px;">
                    Как правило, таким образом, контролируются продажи.
                    Продвинутые предприниматели при помощи план-фактного анализа следят за дебиторкой, прибылью или остатками товаров на складе.
                    План-фактный отчет обладает волшебной магией, если вести его добросовестно, то планы всегда сбудутся.
                </div>
                <div style="margin-top: 20px;">
                    Выбрав основные показатели бизнеса для план-фактного анализа, необходимо организовать онлайн отчеты,
                    контролирующие изменяющиеся параметры, то есть сформировать панель управления бизнесом.
                    Воспользовавшись разделом Финмодель в КУБ24.ФинДиректор,
                    Вы сможете запланировать маркетинговые показатели, показатели по продажам и конечно финансовые.
                    А далее всё автоматически сравниться с фактическими данными.
                </div>
                <div style="margin-top: 20px; text-align: center;">
                    <?= Html::a($youtube.' Планирование и финмоделирование', 'https://kub-24.ru/video?'.$linkParams, [
                        'style' => 'color:#ffffff; text-decoration:none; font-family:Arial,Helvetica,sans-serif; font-size: 16px; padding: 10px 20px 10px 20px; background-color: #4679ae; border-radius:4px;',
                        'target' => '_blank',
                    ]) ?>
                </div>
                <div style="margin-top: 20px;">
                    <strong>
                        Содержательные и визуально красивые графики позволят быстро считывать информацию и принимать управленческие решения на основе цифр, а не интуиции.
                    </strong>
                    <div>
                        «КУБ24.ФинДиректор» - ваш навигатор для бизнеса.
                    </div>
                </div>
            </td>
        </tr>
    </tbody>
</table>

<?= $this->render('@common/mail/layouts/parts/_alexey', ['message' => $message]) ?>

<?php \Yii::$app->view->endContent(); ?>
