<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'FinDir';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$this->params['utm_content'] = 'text-button';
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
    "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}&amp;utm_content={$this->params['utm_content']}";
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/html2.php'); ?>

<table cellpadding="0" cellspacing="0" style="width:600px;">
    <tbody>
        <tr>
            <td align="center">
                <?= Html::img('https://lk.kub-24.ru/img/unisender/findirector/letter-2021-3.png', [
                    'width' => 600,
                    'alt' => '',
                ]); ?>
            </td>
        </tr>
    </tbody>
</table>

<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody>
        <tr>
            <td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
                <div>
                    В проектном бизнесе с клиентами то густо, то пусто.
                    Сначала все работают в цейтноте и запаре, денег достаточно, а потом скатываемся в ожидание новых заказов и провал по деньгам.
                    И так по кругу, только на каждом круге мы прирастаем фиксированными затратами и новыми сотрудниками, и нам уже нужно больше проектов, что бы закрыть накопившиеся долги и выйти в плюс.
                </div>
                <div style="margin-top: 20px;">
                    <strong style="text-decoration: underline;">
                        Частые спутники проектного бизнеса:
                    </strong>
                </div>
                <table>
                    <tbody>
                        <tr>
                            <td style="vertical-align: top; font-weight: bold;">1.</td>
                            <td>
                                <strong>
                                    Работаете в минус по части проектов.
                                </strong>
                                <span>Причины:</span>
                                <ul style="margin: 0; padding: 0; padding-left: 20px;">
                                    <li>
                                        Берете любые проекты, что бы было чем людей загрузить и зарплату выплатить.
                                    </li>
                                    <li>
                                        Готовы по несколько раз делать переделки по просьбе клиента и конечно это делаете за свой счет.
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top; font-weight: bold;">2.</td>
                            <td>
                                <strong>
                                    Кассовые разрывы.
                                </strong>
                                <span>Причины:</span>
                                <ul style="margin: 0; padding: 0; padding-left: 20px;">
                                    <li>
                                        Соглашаетесь на рассрочку оплаты клиентом, т.к нужно удержать клиента.
                                    </li>
                                    <li>
                                        Зарплата сотрудников плохо привязана к результату – платите большой фикс и маленький бонус.
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top; font-weight: bold;">3.</td>
                            <td>
                                <strong>
                                    Большая дебиторка.
                                </strong>
                                <span>Причины:</span>
                                <ul style="margin: 0; padding: 0; padding-left: 20px;">
                                    <li>
                                        Дали большие отсрочки клиентам.
                                    </li>
                                    <li>
                                        Плохо подписываете акты и «токсичные» клиенты уходят в отказ по оплате.
                                    </li>
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top; font-weight: bold;">4.</td>
                            <td>
                                <strong>
                                    Собственник сам много работает и времени ни на что не хватает.
                                </strong>
                                <span>Причины:</span>
                                <ul style="margin: 0; padding: 0; padding-left: 20px;">
                                    <li>
                                        Если хочешь сделать, хорошо, то сделай сам!?
                                    </li>
                                    <li>
                                        Сотрудники не замотивированны, т.к. получают фиксированную зарплату
                                    </li>
                                </ul>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div style="margin-top: 20px;">
                    <strong style="text-decoration: underline;">
                        Что делать?
                    </strong>
                </div>
                <table>
                    <tbody>
                        <tr>
                            <td style="vertical-align: top; font-weight: bold;">&#10004;</td>
                            <td>
                                Считать рентабельность каждого проекта – и большого и маленького.
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top; font-weight: bold;">&#10004;</td>
                            <td>
                                Сравнивать проекты и направления по эффективности и отказаться от менее рентабельных, что бы больше сил и средств вкладывать в более эффективные.
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top; font-weight: bold;">&#10004;</td>
                            <td>
                                Сделать проекты рентабельными и отсечь клиентов халявщиков.
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top; font-weight: bold;">&#10004;</td>
                            <td>
                                Контролировать внутренние сметы и работу с должниками.
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div style="margin-top: 20px;">
                    <strong style="text-decoration: underline;">
                        «КУБ24. ФинДиректор» поможет:
                    </strong>
                </div>
                <table>
                    <tbody>
                        <tr>
                            <td style="vertical-align: top; font-weight: bold;">1)</td>
                            <td>
                                <strong>
                                    Вести правильный учет по проектам – делим расходы на прямые и косвенные.
                                </strong>
                                <div>
                                    Прямые расходы –  расходы на конкретный проект. Больше проектов –  больше прямых расходов.
                                    Косвенные расходы –  расходы на компанию в целом. Плохо, когда косвенных расходов много.
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top; font-weight: bold;">2)</td>
                            <td>
                                <strong>
                                    Считать прибыль каждого проекта и сравнивать с остальными проектами.
                                </strong>
                                <div>
                                    Используйте возможности «ФинДиректора» и заносите все расходы и доходы.
                                    В итоге получите результат по каждому проекту – рентабельность, кассовый разрыв по проекту, эффективность каждого сотрудника в проекте и т.д.
                                    Все проекты сравнивайте в общем отчете.
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top; font-weight: bold;">3)</td>
                            <td>
                                <strong>
                                    Составлять сметы под каждый проект
                                </strong>
                                <div>
                                    Еще до заключения договора составьте смету, в которой пропишете необходимые пункты и их стоимость, а также сроки оплаты.
                                    Смета для клиента должна создаваться на основе внутренней сметы, где у вас согласованы кол-во часов по каждому сотруднику, стоимость покупки сторонних услуг, а также стоимость необходимых материалов и товаров.
                                    <br>
                                    Посчитайте рентабельность и постройте платежный календарь по проекту.
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top; font-weight: bold;">4)</td>
                            <td>
                                <strong>
                                    Работать с должниками
                                </strong>
                                <div>
                                    Активно используйте автонапоминания о неоплаченных счетах.
                                    <br>
                                    Используйте цепочки писем по работе с должниками.
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top; font-weight: bold;">5)</td>
                            <td>
                                <strong>
                                    Создавать акты выполненных работ и акты сверки с клиентами.
                                </strong>
                                <div>
                                    Подписанные акты с клиентом – это ваша гарантия, что клиент не решит отказаться от уже сделанного вами и передумает платить.
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div style="margin-top: 20px;">
                    <strong>
                        Начните вести учет по проектному бизнесу правильно!
                    </strong>
                    <div>
                        «КУБ24.ФинДиректор» поможет и подскажет.
                    </div>
                </div>
            </td>
        </tr>
    </tbody>
</table>

<?= $this->render('@common/mail/layouts/parts/_alexey', ['message' => $message]) ?>

<?php \Yii::$app->view->endContent(); ?>
