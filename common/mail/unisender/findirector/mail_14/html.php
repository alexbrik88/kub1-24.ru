<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'FinDir';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$this->params['utm_content'] = 'text-button';
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
    "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}&amp;utm_content={$this->params['utm_content']}";
$youtube = Html::img('https://lk.kub-24.ru/img/ico-sotial/youtube.png', [
    'style' => 'vertical-align: middle; margin-left: -10px; margin-right: 5px; width: 28px;',
    'alt' => '',
]);
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/html2.php'); ?>

<table cellpadding="0" cellspacing="0" style="width:600px;">
    <tbody>
        <tr>
            <td align="center">
                <?= Html::img('https://lk.kub-24.ru/img/unisender/findirector/letter-2021-14.png', [
                    'width' => 600,
                    'alt' => '',
                ]); ?>
            </td>
        </tr>
    </tbody>
</table>

<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody>
        <tr>
            <td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
                <div>
                    Лучший способ бороться с зимней простудой и гриппом – это профилактика и ЗОЖ (уж, простите за очевидность), то есть подготовиться заблаговременно. Болезни бизнеса также можно и нужно предотвращать до появления «первых симптомов». Пожалуй, самая распространенная болезнь – это невозможность организации вовремя платить по счетам, то есть кассовый разрыв.
                </div>
                <div style="margin-top: 20px;">
                    Получайте «рецепет» (лайфхак):
                </div>
                <table>
                    <tbody>
                        <tr>
                            <td style="vertical-align: top;">1.</td>
                            <td>
                                Анализировать платежи за прошедшие периоды. Для этого необходимо вести
                                <strong>Отчет о Движении Денежных Средств.</strong>
                                Для Вас мы предлагаем уже готовые решения по автоматизированному учету и анализу денежного потока.
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;">2.</td>
                            <td>
                                Планировать платежи как минимум на месяц вперед. Речь идет об управленческом инструменте –
                                <strong>Платежный календарь.</strong>
                                Данный инструмент также реализован в системе КУБ24.ФинДиретор.
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;">3.</td>
                            <td>
                                Разбить получателей (поставщиков) денежных средств по приоритетным группам.
                                Например, группа с безусловным исполнением по срокам и суммам: налоги, банки, принципиальные поставщики;
                                вторая группа с фиксированной суммой и плавающей датой: зарплаты, арендодатели и лояльные поставщики, не применяющие штрафы;
                                и группа необязательных платежей: расходы на офис, необязательные услуги и тому подобное.
                                Расставив всех
                                <strong>получателей по приоритетам,</strong>
                                вам будет проще составить график платежей.
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top;">4.</td>
                            <td>
                                Разбить клиентов по значимости, исходя из объема оборота и доходности, которую они приносят.
                                Например, используя отчет <strong>АВС-анализ.</strong>
                                Это достаточно сложная процедура, но она также реализована в системе КУБ24.ФинДиретор.
                                Дальше выяснить при помощи отчета <strong>Платежная дисциплина,</strong> кто как платит.
                                И после этого сразу станет очевидно кто полезен для компании, а кто паразитирует за счет вашего бизнеса.
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div style="margin-top: 20px; text-align: center;">
                    <?= Html::a($youtube.' Все о платежной дисциплине', 'https://kub-24.ru/video?'.$linkParams, [
                        'style' => 'color:#ffffff; text-decoration:none; font-family:Arial,Helvetica,sans-serif; font-size: 16px; padding: 10px 20px 10px 20px; background-color: #4679ae; border-radius:4px;',
                        'target' => '_blank',
                    ]) ?>
                </div>
                <div style="margin-top: 20px;">
                    <strong>
                        Избавьтесь от кассовых разрывов, настроив правильный учет и анализ данных.
                    </strong>
                    <div>
                        «КУБ24.ФинДиректор» - ваш навигатор для бизнеса.
                    </div>
                </div>
            </td>
        </tr>
    </tbody>
</table>

<?= $this->render('@common/mail/layouts/parts/_alexey', ['message' => $message]) ?>

<?php \Yii::$app->view->endContent(); ?>
