<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'FinDir';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$this->params['utm_content'] = 'text-button';
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
    "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}&amp;utm_content={$this->params['utm_content']}";
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/html2.php'); ?>

<table cellpadding="0" cellspacing="0" style="width:600px;">
    <tbody>
        <tr>
            <td align="center">
                <?= Html::img('https://lk.kub-24.ru/img/unisender/findirector/letter-2021-9_2.png', [
                    'width' => 600,
                    'alt' => '',
                ]); ?>
            </td>
        </tr>
    </tbody>
</table>

<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody>
        <tr>
            <td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
                <div>
                    Мы знаем, что сложно найти время на изучение новых сервисов, поэтому подготовили несколько видео,
                    которые познакомят вас с возможностями «ФинДиректора».
                </div>
                <table style="margin-top: 10px;">
                    <tbody>
                        <tr>
                            <td style="vertical-align: top; font-weight: bold; color: #1e91cf;">1.</td>
                            <td>
                                <strong>
                                    Автоматическая загрузка данных –
                                    <span style="color: #1e91cf;">3 минуты</span>
                                </strong>
                                <div>
                                    Импортируйте данные из банка и 1С в автоматическом режиме без ошибок и с проверкой импортированных данных.
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top; font-weight: bold; color: #1e91cf;">2.</td>
                            <td>
                                <strong>
                                    Автоматическое разнесение операций по статьям (категориям) –
                                    <span style="color: #1e91cf;">3 минуты</span>
                                </strong>
                                <div>
                                    Один раз закрепляете за контрагентами и далее, статьи приходов и расходов будут проставляться автоматически.
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top; font-weight: bold; color: #1e91cf;">3.</td>
                            <td>
                                <strong>
                                    Платежный календарь на автомате –
                                    <span style="color: #1e91cf;">15 минут</span>
                                </strong>
                                <div>
                                    Для получения операций из банка за предыдущий период и каждое утро за предыдущий день.
                                    Если с вашим банком нет интеграции, то нужно из Клиент-Банка выгрузить выписку в формате «для 1С»,
                                    у файла будет расширение «.txt»
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top; font-weight: bold; color: #1e91cf;">4.</td>
                            <td>
                                <strong>
                                    Прогноз кассовых разрывов –
                                    <span style="color: #1e91cf;">30 минут</span>
                                </strong>
                                <div>
                                    Прогнозируйте и предотвращайте возникновение кассовых разрывов.
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="vertical-align: top; font-weight: bold; color: #1e91cf;">5.</td>
                            <td>
                                <strong>
                                    Сравнивайте прибыль по проектам –
                                    <span style="color: #1e91cf;">30 минут</span>
                                </strong>
                                <div>
                                    Ведите учет расходов и доходов по проектам. Выявляйте прибыльные проекты и те, которые мало эффективны.
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
                <div style="margin-top: 20px;">
                    Если не нашли, что-то, что вам важно иметь в программе управленческого учета, напишите нам. Мы либо покажем, в каком разделе это уже есть, или доработаем необходимый вам функционал для вас.
                </div>
                <div style="margin-top: 20px;">
                    <strong>
                        Управляйте бизнесом на основе цифр
                    </strong>
                    <div>
                        «КУБ24.ФинДиректор» - ваш навигатор для бизнеса.
                    </div>
                </div>
            </td>
        </tr>
    </tbody>
</table>

<?= $this->render('@common/mail/layouts/parts/_arslan', ['message' => $message]) ?>

<?php \Yii::$app->view->endContent(); ?>
