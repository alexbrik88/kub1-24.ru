<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'FinDir';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$this->params['utm_content'] = 'text-button';
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
    "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}&amp;utm_content={$this->params['utm_content']}";
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/html2.php'); ?>

<table cellpadding="0" cellspacing="0" style="width:600px;">
    <tbody>
        <tr>
            <td align="center">
                <?= Html::img('https://lk.kub-24.ru/img/unisender/findirector/letter-2021-13.png', [
                    'width' => 600,
                    'alt' => '',
                ]); ?>
            </td>
        </tr>
    </tbody>
</table>

<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody>
        <tr>
            <td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
                <div>
                    Дивиденды – главная награда собственника за трудный труд и нервы, за общение с налоговой и банками,
                    за то, что главный продажник свалил в пик сезона, поставщик подвел по срокам, а клиенты не платят в срок, и тому подобное.
                    <strong>Дивиденды – это именно награда, а не зарплата или бонусы.</strong>
                </div>
                <div style="margin-top: 20px;">
                    Теперь главный вопрос, <strong>сколько денег вынимать из бизнеса без ущерба для него.</strong>
                    Для этого нам потребуется Отчет о Прибылях и Убытках (ОПиУ), который и ответит на вопрос собственника — сколько бизнес заработал.
                </div>
                <div style="margin-top: 20px;">
                    Максимальная сумма дивидендов соответствует прибыли за соответствующий период. Но если Вы планируете развивать бизнес, дополнительно поощрить сотрудников, сформировать резервный фонд («кубышку» на форс-мажорный случай), то деньги на дивиденды нужно извлекать за вычетом расходов на Ваши светлые идеи.
                </div>
                <div style="margin-top: 20px;">
                    Что бы спрогнозировать траты на развитие бизнеса и понять, сколько же можно вынуть денег из бизнеса, используйте Платежный календарь, в котором вы сможете запланировать свои расходы и будет понятно, сколько можно забрать денег на себя, без попадания в кассовый разрыв.
                </div>
                <div style="margin-top: 20px;">
                    <strong>
                        Получать актуальные данные по прибыли и эффективности бизнеса в режиме онлайн.
                    </strong>
                    <div>
                        «КУБ24.ФинДиректор» - ваш навигатор для бизнеса.
                    </div>
                </div>
            </td>
        </tr>
    </tbody>
</table>

<?= $this->render('@common/mail/layouts/parts/_alexey', ['message' => $message]) ?>

<?php \Yii::$app->view->endContent(); ?>
