<?php

use common\models\Discount;
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'progrev';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
        "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}";
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/html.php'); ?>

<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody style="color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
        <tr>
            <td style="text-align: center;">
                <img src="https://lk.kub-24.ru/img/unisender/inactive/7/0.jpg" style="max-width: 100%" alt="">
            </td>
        </tr>
        <tr>
            <td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
                <div style="margin:0 0 20px; font-size:28px !important; line-height:100% !important; font-weight:600; color:#333333">
                    Добрый день{{Name?, }}{{Name}}!
                </div>
                <div style="margin-bottom: 15px;">
                    Каждый счет за услуги или работы должен быть закрыт актом.
                    <span style="font-weight: bold;">
                        Акт выполненных работ
                    </span>
                    – документ, в котором сказано,
                    что назначенные работы (услуги) выполнены в полном объеме или частично.
                    Если же у вас вообще нет акта, то официально вами работа не сделана!
                </div>
                <div style="margin-bottom: 15px;">
                    Поэтому готовить акты нужно, это обязательно и неизбежно.
                    Но если у вас много счетов и соответственно много актов, то вы регулярно тратите на это время.
                    Такая рутина отнимает время от бизнеса, поэтому этим не хочется заниматься.
                </div>
                <div style="margin-bottom: 15px;">
                    <span style="text-decoration: underline;">Мы решили эту проблему! </span>
                </div>
                <div style="margin-bottom: 15px;">
                    <span style="font-weight: bold;">
                        КУБ за вас автоматически выставит АКТ в нужный момент и даже отправит его клиенту с вашей подписью и печатью.
                    </span>
                </div>
                <div style="margin-bottom: 15px;">
                    Теперь у вас больше времени на бизнес!
                </div>
                <div style="margin-bottom: 15px;">
                    <span style="text-decoration: underline;">
                        Автоматизировав выставление актов, вы:
                    </span>
                    <ul style="margin: 0; padding: 0; padding-left: 30px; list-style: none;">
                        <li>
                            &#10003;
                            <span style="font-weight: bold;">
                                Экономите ваше время на рутинных операциях.
                            </span>
                        </li>
                        <li>
                            &#10003;
                            Если у вас этим занимался бухгалтер, то
                            <span style="font-weight: bold;">
                                экономите деньги на бухгалтере – он меньше делает, вы меньше платите.
                            </span>
                        </li>
                    </ul>
                </div>
                <div style="margin-bottom: 15px;">
                    <span style="font-weight: bold;">
                        Как настроить автоматизацию актов?
                    </span>
                </div>
                <div style="margin-bottom: 15px;">
                    В меню
                    «<span style="font-weight: bold;">ПРОДАЖИ</span>»
                    выбираете закладку
                    «<span style="font-weight: bold;">АКТЫ</span>»
                    и там нажимаете на кнопку
                    «<span style="font-weight: bold;">ПРАВИЛА ВЫСТАВЛЕНИЯ</span>».
                </div>
                <div style="margin-bottom: 15px;">
                    <img src="https://lk.kub-24.ru/img/unisender/inactive/7/1.jpg" style="max-width: 100%" alt="">
                </div>
                <div style="margin-bottom: 15px;">
                    Выберите правила выставления актов, подходящие под ваш бизнес.
                </div>
                <div style="margin-bottom: 15px;">
                    Например, если вы выставляете акт в день оплаты счета, то выбирайте этот вариант и т.д.
                </div>
                <div style="margin-bottom: 15px;">
                    <span style="font-weight: bold; text-decoration: underline;">Правила выставления актов:</span>
                </div>
                <div style="margin-bottom: 15px;">
                    <img src="https://lk.kub-24.ru/img/unisender/inactive/7/2.jpg" style="max-width: 100%" alt="">
                </div>
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold; color: #505050;">
                <div style="text-align: center;">
                    Онлайн сервис КУБ
                    <br>
                    простая программа автоматизации
                    <br>
                    для малого и среднего бизнеса
                </div>
            </td>
        </tr>
        <tr>
            <td style="text-align: center;">
                <a href="https://kub-24.ru/login/?<?= $linkParams ?>&amp;utm_content=button"
                   style="color:#ffffff; font-weight:400; text-decoration:none; padding:10px 20px 10px 20px; background-color:#0077a7">
                    ВОЙТИ в КУБ
                </a>
            </td>
        </tr>
    </tbody>
</table>

<?= $this->render('@common/mail/layouts/parts/_alexey', ['message' => $message]) ?>

<?php \Yii::$app->view->endContent(); ?>
