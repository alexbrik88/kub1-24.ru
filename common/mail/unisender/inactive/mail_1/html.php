<?php

use common\models\Discount;
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'progrev';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
        "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}";
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/html.php'); ?>

<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody style="color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
        <tr>
            <td style="text-align: center;">
                <img src="https://lk.kub-24.ru/img/unisender/inactive/1/0.jpg" style="max-width: 100%" alt="">
            </td>
        </tr>
        <tr>
            <td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
                <div style="margin:0 0 20px; font-size:28px !important; line-height:100% !important; font-weight:600; color:#333333">
                    Добрый день{{Name?, }}{{Name}}!
                </div>
                <div style="margin-bottom: 15px;">
                    Ваш клиент, готов оплатить товар или услугу и просит прислать ему счет.
                    Дело за малым – выставить счет. Однако иногда это оказывается небольшой, но проблемой….
                </div>
                <div style="margin-bottom: 15px;">
                    …вы звоните своему бухгалтеру и просите сделать счет.
                    Бухгалтер сейчас не у компьютера и будет через 3-4 часа.
                    Когда он добирается до компьютера, ему нужно уточнить у вас:
                    <ul style="margin: 0; padding: 0; padding-left: 30px; list-style: none;">
                        <li>
                            - Точно ли покупатель ООО «ЗАКУПАЮ всё» или всё таки это ООО «ВСЁ закупаю»?
                        </li>
                        <li>
                            - Что продаем в этот раз?
                        </li>
                        <li>
                            - Цену ставить как всегда или со скидкой?
                        </li>
                    </ul>
                </div>
                <div style="margin-bottom: 15px;">
                    Но вы сейчас на встрече! и вам не удобно говорить и … откладываете разговор на час.
                    После того, как вы наконец-то смогли поговорить с бухгалтером и всё согласовали, он вам присылает счет.
                    Вроде бы всё ОК,…НО срочно набираете бухгалтеру с вопросом:
                    Почему в счёте покупатель ООО «Ни чего не ЗАКУПАЮ»?
                    Бухгалтер извиняется и через полчаса высылает правильный счет.
                </div>
                <div style="margin-bottom: 15px;">
                    Конечно, такое случается не всегда.
                    Но, уверен, что
                    <span style="font-weight: bold;">
                        объясняя бухгалтеру по телефону или в письме, что нужно указать в счете, вы иногда думали, что
                        <span style="text-decoration: underline;">проще и быстрее выставить счет самостоятельно</span>
                    </span>.
                </div>
                <div style="margin-bottom: 15px;">
                    И это правда!
                    Так происходит у большинства: от маленького ИП, до крупной корпорации – выставляют счета НЕ бухгалтера.
                    В крупных компаниях, хотя у них не один десяток бухгалтеров в штате,
                    счета выставляют менеджеры по продажам, т.е. те, кто общается с клиентом и знает все договоренности.
                </div>
                <div style="margin-bottom: 15px;">
                    <span style="font-weight: bold;">
                        Почему вам
                        <span style="text-decoration: underline;">не нужно дожидаться бухгалтера</span>
                        для выставления счета? Все просто:
                    </span>
                    <ul style="margin: 0; padding: 0; padding-left: 30px; list-style: none;">
                        <li>
                            &#10003; Вы теряете свое время
                        </li>
                        <li>
                            &#10003; Вы платите деньги за время бухгалтера.
                        </li>
                        <li>
                            &#10003; Вы можете потерять клиента, если опоздаете со счетом.
                        </li>
                    </ul>
                </div>
                <div style="margin-bottom: 15px;">
                    Есть много решений, как выставить счет самостоятельно.
                    Но нужно выбрать то, которое будет реально помогать и поверьте это не Эксель.
                    В нем нужно уметь прописать все формулы, а если вы не умеете это делать,
                    то придется считать на калькуляторе и вбивать цифры…..
                    это не удобно, долго и большая вероятность ошибок.
                </div>
                <div style="margin-bottom: 15px;">
                    Мы рекомендуем выставлять счета в КУБе, хотя вы можете найти и другие решения.
                </div>
                <div style="margin-bottom: 15px;">
                    <span style="font-weight: bold;">
                        Почему нужно выставлять счета в КУБе:
                    </span>
                    <ul style="margin: 0; padding: 0; padding-left: 30px; list-style: square inside;">
                        <li>
                            Онлайн, 24/7. Можно выставить счет из любой точки мира
                        </li>
                        <li>
                            Отправка счета на e-mail клиента из КУБа
                        </li>
                        <li>
                            ПРОФЕССИОНАЛЬНЫЙ ВИД счета с логотипом, печатью и подписью
                        </li>
                        <li>
                            ФОРМУЛЫ БЕЗ ОШИБОК, в том числе по расчету НДС и «Итого счета»
                        </li>
                        <li>
                            АВТОМАТИЧЕСКОЕ формирование суммы прописью по цифрам в «Итого счета»
                        </li>
                        <li>
                            АВТОМАТИЧЕСКОЕ проставление нумерации счета
                        </li>
                        <li>
                            АВТОЗАПОЛНЕНИЕ реквизитов покупателя по ИНН
                        </li>
                        <li>
                            В 1 КЛИК подготовка к счету акта, товарной накладной, счета-фактуры
                        </li>
                        <li>
                            ИНТЕГРАЦИЯ с 1С для выгрузки счетов  бухгалтеру
                        </li>
                    </ul>
                </div>
                <div style="margin-bottom: 15px;">
                    И еще один важный момент –
                    <span style="color: red;">УСКОРИТЕЛЬ ОПЛАТЫ СЧЕТОВ</span>.
                </div>
                <div style="margin-bottom: 15px;">
                    Вместе со счетом ваш покупатель получает электронную платежку,
                    которая в 2 клика загружается в его клиент-банк и вы получаете оплату счета в тот же день.
                </div>
                <div style="margin-bottom: 15px;">
                    <span style="font-weight: bold;">Как в КУБе выставить счет:</span>
                    <br>
                    В меню
                    «<span style="font-weight: bold;">ПРОДАЖИ</span>»
                    выбираете закладку
                    «<span style="font-weight: bold;">СЧЕТА</span>»
                    и там нажимаете на кнопку
                    «<span style="font-weight: bold;">ДОБАВИТЬ</span>».
                </div>
                <div style="margin-bottom: 15px;">
                    <img src="https://lk.kub-24.ru/img/unisender/inactive/1/1.jpg" style="max-width: 100%" alt="">
                </div>
                <div style="margin-bottom: 15px;">
                    Далее выбираете покупателя и наименования товаров или услуг, которые продаете
                </div>
                <div style="margin-bottom: 15px;">
                    <img src="https://lk.kub-24.ru/img/unisender/inactive/1/2.jpg" style="max-width: 100%" alt="">
                </div>
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold; color: #505050;">
                <div style="text-align: center;">
                    Онлайн сервис КУБ
                    <br>
                    простая программа автоматизации
                    <br>
                    для малого и среднего бизнеса
                </div>
            </td>
        </tr>
        <tr>
            <td style="text-align: center;">
                <a href="https://kub-24.ru/login/?<?= $linkParams ?>&amp;utm_content=button"
                   style="color:#ffffff; font-weight:400; text-decoration:none; padding:10px 20px 10px 20px; background-color:#0077a7">
                    ВОЙТИ в КУБ
                </a>
            </td>
        </tr>
    </tbody>
</table>

<?= $this->render('@common/mail/layouts/parts/_alexey', ['message' => $message]) ?>

<?php \Yii::$app->view->endContent(); ?>
