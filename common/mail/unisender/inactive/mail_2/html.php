<?php

use common\models\Discount;
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'progrev';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
        "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}";
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/html.php'); ?>

<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody style="color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
        <tr>
            <td style="text-align: center;">
                <img src="https://lk.kub-24.ru/img/unisender/inactive/2/0.jpg" style="max-width: 100%" alt="">
            </td>
        </tr>
        <tr>
            <td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
                <div style="margin:0 0 20px; font-size:28px !important; line-height:100% !important; font-weight:600; color:#333333">
                    Добрый день{{Name?, }}{{Name}}!
                </div>
                <div style="margin-bottom: 15px;">
                    Есть предприниматели, которые не заморачиваются с выставлением Актов и подписанием их у клиентов.
                    Считают, что это не нужная бумажка, т.к. деньги они уже получили или клиент всегда платит с задержкой, но платит.
                    Часто бухгалтера требуют акты для бухгалтерии, но даже они бессильны, если у предпринимателя ИП на УСН ДОХОДы
                    – тут можно не вести бухгалтерию, а значит, зачем время тратить на Акты!
                </div>
                <div style="margin-bottom: 15px;">
                    <span style="font-weight: bold;">Но не всё так просто</span>
                </div>
                <div style="margin-bottom: 15px;">
                    <span style="font-weight: bold;">Что такое Акт:</span>
                    <div style="margin-left: 20px; position: relative;">
                        <div style="padding-left: 10px; border-left: 1px solid #505050;">
                            <span style="font-weight: bold;">АКТ ВЫПОЛНЕННЫХ РАБОТ</span>
                            считается первичным документом бухгалтерского учета, который оформляется после окончания (выполнения) работ.
                            Оформление документа выполняется на основании ранее заключенного договора.
                            Если акт отсутствует, затраты не будут учтены в налоге на прибыль.
                            Акт составляется в двух экземплярах: первый остается у организации, оказавшей услуги, второй у заказчика услуги.
                        </div>
                    </div>
                </div>
                <div style="margin-bottom: 15px;">
                    <span style="font-weight: bold;">Зачем нужен Акт:</span>
                </div>
                <div style="margin-bottom: 15px;">
                    Акт выполненных работ – документ, в котором сказано,
                    что назначенные работы (услуги) выполнены в полном объеме или частично.
                    Если вы продаете товар, то Акт вам не нужен!
                    Даты, указанные в акте свидетельствуют о том, что работы проводились в сроки, оговоренные в договоре.
                </div>
                <div style="margin-bottom: 15px;">
                    По датам в акте можно судить, нарушены ли сроки выполнения договора.
                    Если же нарушения присутствуют, организация может принять меры согласно условиям договора.
                    Перечень работ в акте подтверждает,
                    что условия договора выполнены в рамках договоренности или присутствуют нарушения со стороны исполнителя.
                </div>
                <div style="margin-bottom: 15px;">
                    <span style="font-weight: bold; font-style: oblique;">
                        Если же у вас вообще нет акта, то формально вами работа не сделана!
                    </span>
                </div>
                <div style="margin-bottom: 15px;">
                    Какими бы не были хорошими у вас отношения с клиентом, но как говориться «дружба дружбой, а денежки врозь».
                    Может произойти любая нелепость или даже злой умысел, когда вам не заплатят или заплатят, но потребуют вернуть деньги.
                    Вы смогли бы быстро доказать, что работа сделана, предъявив подписанный клиентом акт выполненных работ.
                    НО вы не сделали этот акт!
                    Теперь будите доказывать свою правоту в суде и возможно, с хорошим юристом докажите и получите деньги.
                    НО вам нужен этот геморрой?
                </div>
                <div style="margin-bottom: 15px;">
                    <span style="font-weight: bold;">
                        Если нет подписанного акта с клиентом, то вы:
                    </span>
                    <ul style="margin: 0; padding: 0; padding-left: 30px; list-style: none;">
                        <li>
                            &#10003; Потеряете нервы – разбирательство и выяснения отношений с клиентом.
                        </li>
                        <li>
                            &#10003; Потеряете деньги – вам их не оплатят или по суду вам придется их вернуть.
                        </li>
                        <li>
                            &#10003; Потеряете ваше время – потратили время на оказание услуг, за которые не заплатили.
                        </li>
                        <li>
                            &#10003; Потеряете еще денег на юриста и на оплату госпошлины в суде.
                        </li>
                        <li>
                            &#10003; Потеряете клиента – как результат всего перечисленного выше.
                        </li>
                    </ul>
                </div>
                <div style="margin-bottom: 15px;">
                    <span style="font-weight: bold;">
                        Альтернатива этим проблемам - несколько нажатий на кнопки:
                    </span>
                    <ul style="margin: 0; padding: 0; padding-left: 30px; list-style: none;">
                        <li>
                            1. Нажать в счете на ОДНУ кнопку для создания акта.
                        </li>
                        <li>
                            2. Нажать на ЕЩЕ ОДНУ кнопку и отправить его клиенту.
                        </li>
                        <li>
                            3. Можно нажать на ДРУГУЮ кнопку и распечатать акт.
                        </li>
                        <li>
                            4. Подписать акт и отправить подписанный акт клиенту.
                        </li>
                    </ul>
                </div>
                <div style="margin-bottom: 15px;">
                    Даже скан подписанного акта поможет не ввязаться в проблемы, НО лучше это прописать в договоре.
                </div>
                <div style="margin-bottom: 20px;">
                    <span style="font-weight: bold;">
                        Поэтому
                        <span style="text-decoration: underline;">хочешь мира, готовься к войне</span>
                        – СДЕЛАЙ АКТ И СПИ СПОКОЙНО.
                    </span>
                </div>
                <div style="margin-bottom: 15px;">
                    <img src="https://lk.kub-24.ru/img/unisender/inactive/2/4.jpg" style="max-width: 100%" alt="">
                </div>
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold; color: #505050;">
                <div style="text-align: center;">
                    Онлайн сервис КУБ
                    <br>
                    простая программа автоматизации
                    <br>
                    для малого и среднего бизнеса
                </div>
            </td>
        </tr>
        <tr>
            <td style="text-align: center;">
                <a href="https://kub-24.ru/login/?<?= $linkParams ?>&amp;utm_content=button"
                   style="color:#ffffff; font-weight:400; text-decoration:none; padding:10px 20px 10px 20px; background-color:#0077a7">
                    ВОЙТИ в КУБ
                </a>
            </td>
        </tr>
    </tbody>
</table>

<?= $this->render('@common/mail/layouts/parts/_alexey', ['message' => $message]) ?>

<?php \Yii::$app->view->endContent(); ?>
