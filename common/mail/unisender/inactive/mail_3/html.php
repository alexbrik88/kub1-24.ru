<?php

use common\models\Discount;
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'progrev';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
        "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}";
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/html.php'); ?>

<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody style="color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
        <tr>
            <td style="text-align: center;">
                <img src="https://lk.kub-24.ru/img/unisender/inactive/3/0.jpg" style="max-width: 100%" alt="">
            </td>
        </tr>
        <tr>
            <td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
                <div style="margin:0 0 20px; font-size:28px !important; line-height:100% !important; font-weight:600; color:#333333">
                    Добрый день{{Name?, }}{{Name}}!
                </div>
                <div style="margin-bottom: 15px;">
                    Торгуя товаром, вы должны понимать, что передавая его покупателю,
                    вам необходимо  подтвердить факт перехода собственности и ответственности за этот товар от вас к покупателю.
                </div>
                <div style="margin-bottom: 15px;">
                    <span style="font-weight: bold;">Что такое товарная накладная:</span>
                    <div style="margin-left: 20px; position: relative;">
                        <div style="padding-left: 10px; border-left: 1px solid #505050;">
                            <span style="font-weight: bold;">ТОВАРНАЯ НАКЛАДНАЯ</span>
                            — первичный документ,
                            который применяется для оформления продажи (отпуска) товарно-материальных ценностей сторонней организации.
                            На основании товарной накладной фирма-продавец списывает стоимость товаров в бухгалтерском учете,
                            а покупатель оприходует полученные ценности.
                            <br>
                            Унифицированная форма товарной накладной, применяющаяся в Российской Федерации — «ТОРГ-12».
                            <br>
                            Товарная накладная составляется в двух экземплярах:
                            первый остается у организации, реализующей товарно-материальные ценности,
                            второй передается организации-покупателю.
                        </div>
                    </div>
                </div>
                <div style="margin-bottom: 15px;">
                    <span style="font-weight: bold;">Зачем нужна товарная накладная:</span>
                </div>
                <div style="margin-bottom: 15px;">
                    Накладная ТОРГ-12 используется для оформления перехода прав собственности на товарно-материальные ценности от одной стороны к другой.
                    Это один из самых важных первичных документов, который используется при продаже товаров.
                    Продавец выписывает накладную, а покупатель, проверив качество товара, ее подписывает.
                </div>
                <div style="margin-bottom: 15px;">
                    Если у вас нет подписанной товарной накладной, то может так случится, что вы «подарите» товар.
                    Данная ситуация может произойти как не предумышленно, так и специально.
                    Тем более вы договариваетесь о продаже товара с одним сотрудником в компании,
                    а отдаете другому (например, кладовщику или водителю).
                    Этот «другой» может случайно испортить его (уронить, разбить или сломать),
                    а так же ваш товар может ему понравиться.
                    И что бы потом не платить из своего кармана, он может сказать,
                    что не принимал такой товар или принимал, но в меньшем количестве…
                </div>
                <div style="margin-bottom: 15px;">
                    Доказать свою правоту вы сможете только предоставив доказательства в виде подписанной товарной накладной!
                    Так же рекомендую вам удостоверится,
                    что сотрудник принимающий товар имеет на это право и вообще работает в данной организации.
                    Для этого попросите у него доверенность.
                </div>
                <div style="margin-bottom: 15px;">
                    <span style="font-weight: bold;">
                        Если нет подписанной товарной накладной, то вы:
                    </span>
                    <ul style="margin: 0; padding: 0; padding-left: 30px; list-style: none;">
                        <li>
                            &#10003; Потеряете время и нервы на разбирательство и выяснения отношений с покупателем.
                        </li>
                        <li>
                            &#10003; Потеряете деньги – вам могут не оплатить за товар, если у вас нет документа, подтверждающего его передачу.
                        </li>
                        <li>
                            &#10003; Потеряете еще денег на юриста и на оплату госпошлины в суде.
                        </li>
                        <li>
                            &#10003; Потеряете клиента – как результат всего перечисленного выше.
                        </li>
                    </ul>
                </div>
                <div style="margin-bottom: 15px;">
                    <span style="font-weight: bold;">
                        Как избежать потерь? Для этого нужно нажать пару кнопок:
                    </span>
                    <ul style="margin: 0; padding: 0; padding-left: 30px; list-style: none;">
                        <li>
                            1. Нажать в счете на ОДНУ кнопку для создания товарной накладной.
                        </li>
                        <li>
                            2. Нажать на ЕЩЕ ОДНУ кнопку и распечатать её.
                        </li>
                        <li>
                            3. Подписать товарную накладную со своей стороны
                        </li>
                        <li>
                            4. При передаче товара, подписать у покупателя.
                        </li>
                    </ul>
                </div>
                <div style="margin-bottom: 15px;">
                    <img src="https://lk.kub-24.ru/img/unisender/inactive/3/3.jpg" style="max-width: 100%" alt="">
                </div>
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold; color: #505050;">
                <div style="text-align: center;">
                    Онлайн сервис КУБ
                    <br>
                    простая программа автоматизации
                    <br>
                    для малого и среднего бизнеса
                </div>
            </td>
        </tr>
        <tr>
            <td style="text-align: center;">
                <a href="https://kub-24.ru/login/?<?= $linkParams ?>&amp;utm_content=button"
                   style="color:#ffffff; font-weight:400; text-decoration:none; padding:10px 20px 10px 20px; background-color:#0077a7">
                    ВОЙТИ в КУБ
                </a>
            </td>
        </tr>
    </tbody>
</table>

<?= $this->render('@common/mail/layouts/parts/_alexey', ['message' => $message]) ?>

<?php \Yii::$app->view->endContent(); ?>
