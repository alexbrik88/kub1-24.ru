<?php

use common\models\Discount;
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'progrev';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
        "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}";
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/html.php'); ?>

<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody style="color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
        <tr>
            <td style="text-align: center;">
                <img src="https://lk.kub-24.ru/img/unisender/inactive/5/0.jpg" style="max-width: 100%" alt="">
            </td>
        </tr>
        <tr>
            <td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
                <div style="margin:0 0 20px; font-size:28px !important; line-height:100% !important; font-weight:600; color:#333333">
                    Добрый день{{Name?, }}{{Name}}!
                </div>
                <div style="margin-bottom: 15px;">
                    Как же утомляет делать одно и то же….. Особенно если это касается документов
                </div>
                <div style="margin-bottom: 15px;">
                    Нужно привлекать новых клиентов, договариваться с поставщиками и вообще дел у предпринимателя всегда много,
                    а приходится каждый месяц тратить время на выставление и рассылку счетов.
                    А если не успел выставить счета вовремя, то клиенты заплатят позже, а это может привести к проблемам в бизнесе.
                </div>
                <div style="margin-bottom: 15px;">
                    Что бы ни тратить время и не терять деньги, нужно автоматизировать повторяющиеся действия!
                </div>
                <div style="margin-bottom: 15px;">
                    КУБ поможет
                    <span style="font-weight: bold;">
                        сэкономить ваше время на выставлении и отправке счетов
                    </span>
                    клиентам.
                </div>
                <div style="margin-bottom: 15px;">
                    Если у вас этим занимается бухгалтер, то
                    <span style="font-weight: bold;">
                        сэкономьте деньги на бухгалтере – он меньше делает, вы меньше платите.
                    </span>
                </div>
                <div style="margin-bottom: 15px;">
                    Счета отправляют менеджеры? КУБ сделает это за них, а они в это время привлекут вам новых клиентов!
                </div>
                <div style="margin-bottom: 15px;">
                    <span style="font-weight: bold;">
                        Автоматизация выставления счетов и отправки их на e-mail клиентов идеально подходит для:
                    </span>
                    <ul style="margin: 0; padding: 0; padding-left: 30px; list-style: none;">
                        <li>
                            &#10003;
                            Компаний сдающих недвижимость в аренду
                        </li>
                        <li>
                            &#10003;
                            Компаний  оказывающих услуги по предоставлению телефонии и/или интернета
                        </li>
                        <li>
                            &#10003;
                            АйТи компаний
                        </li>
                        <li>
                            &#10003;
                            Бухгалтерских компаний
                        </li>
                        <li>
                            &#10003;
                            Компаний оказывающих услуги по уборке
                        </li>
                        <li>
                            &#10003;
                            Компаний предоставляющих программное обеспечение за абонентскую плату
                        </li>
                        <li>
                            &#10003;
                            Многим другим
                        </li>
                    </ul>
                </div>
                <div style="margin-bottom: 15px;">
                    <span style="font-weight: bold;">
                        Как работает автоматизация?
                    </span>
                    <ul style="margin: 0; padding: 0; padding-left: 30px; list-style: none;">
                        <li>
                            &#10003;
                            В заданный день месяца или квартала формируются счета и отправляются на e-mail клиентов.
                        </li>
                        <li>
                            &#10003;
                            В назначении счета к вашей услуге автоматически дописывается нужный  месяц (квартал) и год.
                        </li>
                        <li>
                            &#10003;
                            Например «Аренда недвижимости за апрель 2018 г.»
                        </li>
                        <li>
                            &#10003;
                            Счета могут быть как за текущий месяц, так и за прошедший или за будущий.
                        </li>
                        <li>
                            &#10003;
                            Вместе со счетом клиент получает электронную платежку, которая загружается в его клиент-банк для ускорения оплаты.
                        </li>
                        <li>
                            &#10003;
                            Автоматическую отправку счетов всегда можно отключить.
                        </li>
                    </ul>
                </div>

                <div style="margin-bottom: 15px;">
                    Что бы включить автоматическое выставление и отправку счета, нужно на странице выставления счета,
                    нажать на кнопку
                    «<span style="font-weight: bold; text-decoration: underline;">АвтоСчет</span>»
                    и далее настроить шаблон «АвтоСчета»
                </div>
                <div style="margin-bottom: 15px;">
                    <img src="https://lk.kub-24.ru/img/unisender/inactive/5/3.jpg" style="max-width: 100%" alt="">
                </div>
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold; color: #505050;">
                <div style="text-align: center;">
                    Онлайн сервис КУБ
                    <br>
                    простая программа автоматизации
                    <br>
                    для малого и среднего бизнеса
                </div>
            </td>
        </tr>
        <tr>
            <td style="text-align: center;">
                <a href="https://kub-24.ru/login/?<?= $linkParams ?>&amp;utm_content=button"
                   style="color:#ffffff; font-weight:400; text-decoration:none; padding:10px 20px 10px 20px; background-color:#0077a7">
                    ВОЙТИ в КУБ
                </a>
            </td>
        </tr>
    </tbody>
</table>

<?= $this->render('@common/mail/layouts/parts/_alexey', ['message' => $message]) ?>

<?php \Yii::$app->view->endContent(); ?>
