<?php

use common\models\Discount;
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'progrev';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
        "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}";
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/html.php'); ?>

<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody style="color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
        <tr>
            <td style="text-align: center;">
                <img src="https://lk.kub-24.ru/img/unisender/inactive/6/0.jpg" style="max-width: 100%" alt="">
            </td>
        </tr>
        <tr>
            <td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
                <div style="margin:0 0 20px; font-size:28px !important; line-height:100% !important; font-weight:600; color:#333333">
                    Добрый день{{Name?, }}{{Name}}!
                </div>
                <div style="margin-bottom: 15px;">
                    Всегда найдется клиент, который уверен, что вам уже всё оплатил и даже переплатил, но вы с этим не согласны. Как быстро разобраться кто кому должен?
                    <br>
                    Возможные варианты:
                    <ul style="margin: 0; padding: 0; padding-left: 30px; list-style: none;">
                        <li>
                            &#10003;
                            Самостоятельно выверять оплаты по банковской выписке,
                            занося их в Эксель и туда же занося выставленные счета или товарные накладные.
                            НО это долго и возможны ошибки.
                        </li>
                        <li>
                            &#10003;
                            Попросить бухгалтера, что бы сформировал Акт сверки в 1С.
                            НО бухгалтера обычно не быстрые и акт сверки получите через 1-2 часа.
                        </li>
                    </ul>
                </div>
                <div style="margin-bottom: 15px;">
                    <span style="text-decoration: underline;">Самый быстрый способ</span>,
                    позволяющий не тратить свое время и не ждать бухгалтера – в КУБе
                    <span style="text-decoration: underline;">нажать на кнопку «Акт Сверки».</span>
                </div>

                <div style="margin-bottom: 15px;">
                    В карточке покупателя нажимаете на кнопку «Акт сверки»:
                </div>
                <div style="margin-bottom: 15px;">
                    <img src="https://lk.kub-24.ru/img/unisender/inactive/6/1.jpg" style="max-width: 100%" alt="">
                </div>
                <div style="margin-bottom: 15px;">
                    Далее выбираете необходимые вам параметры:
                    <ul style="margin: 0; padding: 0; padding-left: 30px; list-style: none;">
                        <li>
                            1. Какой АКТ СВЕРКИ вам нужен: по счетам, по актам и накладным или по счетам-фактурам
                        </li>
                        <li>
                            2. Указываете период сверки.
                        </li>
                        <li>
                            3. Выбираете дату подписания акта сверки.
                        </li>
                    </ul>
                </div>
                <div style="margin-bottom: 15px;">
                    <img src="https://lk.kub-24.ru/img/unisender/inactive/6/2.jpg" style="max-width: 100%" alt="">
                </div>
                <div style="margin-bottom: 15px;">
                    После того, как АКТ СВЕРКИ будет сформирован, его можно будет оправить по e-mail,
                    распечатать или скачать в PDF или Excel файле
                </div>
                <div style="margin-bottom: 15px;">
                    <img src="https://lk.kub-24.ru/img/unisender/inactive/6/3.jpg" style="max-width: 100%" alt="">
                </div>
                <div style="margin-bottom: 15px;">
                    Теперь у вас есть документ, который точно определит, кто, кому  и сколько должен.
                </div>
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold; color: #505050;">
                <div style="text-align: center;">
                    Онлайн сервис КУБ
                    <br>
                    простая программа автоматизации
                    <br>
                    для малого и среднего бизнеса
                </div>
            </td>
        </tr>
        <tr>
            <td style="text-align: center;">
                <a href="https://kub-24.ru/login/?<?= $linkParams ?>&amp;utm_content=button"
                   style="color:#ffffff; font-weight:400; text-decoration:none; padding:10px 20px 10px 20px; background-color:#0077a7">
                    ВОЙТИ в КУБ
                </a>
            </td>
        </tr>
    </tbody>
</table>

<?= $this->render('@common/mail/layouts/parts/_arslan', ['message' => $message]) ?>

<?php \Yii::$app->view->endContent(); ?>
