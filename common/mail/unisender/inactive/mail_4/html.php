<?php

use common\models\Discount;
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'progrev';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
        "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}";
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/html.php'); ?>

<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody style="color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
        <tr>
            <td style="text-align: center;">
                <img src="https://lk.kub-24.ru/img/unisender/inactive/4/0.jpg" style="max-width: 100%" alt="">
            </td>
        </tr>
        <tr>
            <td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
                <div style="margin:0 0 20px; font-size:28px !important; line-height:100% !important; font-weight:600; color:#333333">
                    Добрый день{{Name?, }}{{Name}}!
                </div>
                <div style="margin-bottom: 15px;">
                    Если вы работаете на Общей Системе НалогоОбложения (ОСНО),
                    то для учета НДС вы обязаны выписывать счета-фактуры вашим клиентам.
                    Ну и конечно не забывать про себя и получать счета-фактуры от ваших подрядчиков, которые тоже на ОСНО.
                </div>
                <div style="margin-bottom: 15px;">
                    <span style="font-weight: bold;">Что такое счет-фактура:</span>
                    <div style="margin-left: 20px; position: relative;">
                        <div style="padding-left: 10px; border-left: 1px solid #505050;">
                            <span style="font-weight: bold;">Счёт-фактура</span>
                            — документ, служащий основанием для принятия покупателем предъявленных продавцом сумм НДС к вычету.
                            Счёт-фактура содержит в себе информацию о наименовании и реквизитах продавца и покупателя,
                            перечне товаров или услуг, их цене, стоимости, ставке и сумме НДС, прочих показателях.
                            На основании полученных счётов-фактур налогоплательщиком НДС формируется «Книга покупок»,
                            а на основании выданных счётов-фактур — «Книга продаж».
                            Эти книги отправляются в налоговую каждый квартал.
                        </div>
                    </div>
                </div>
                <div style="margin-bottom: 15px;">
                    <span style="font-weight: bold;">Зачем нужна счет-фактура:</span>
                </div>
                <div style="margin-bottom: 15px;">
                    Счет-фактура – это документ, который выписывает поставщик (исполнитель)
                    на каждую поставку товара (оказанную услугу, выполненную работу).
                    <span style="font-weight: bold;">
                        Именно на основании этого документа производится вычет по НДС.
                    </span>
                    Т.е. если вы не предоставите вашему клиенту счет-фуктуры, то он уплатит в бюджет большую сумму НДС!
                    За это он вам не скажет спасибо – первое он с вас её вытрясет, вплоть до суда, во вторых больше с вами работать не будет.
                </div>
                <div style="margin-bottom: 15px;">
                    Если ваш бухгалтер вдруг не отразит счет-фактуру, которую вы должны выдать покупателю, в вашей отчетности,
                    то это уже налоговое нарушение, за которое вы заплатите штраф.
                    Поэтому важно передавать бухгалтеру счета-фактуры, если вы их готовите самостоятельно.
                </div>
                <div style="margin-bottom: 15px;">
                    <span style="font-weight: bold;">
                        Если вы работаете с НДС и не передадите счет-фактуру клиенту, то:
                    </span>
                    <ul style="margin: 0; padding: 0; padding-left: 30px; list-style: none;">
                        <li>
                            &#10003;
                            Подставите клиента, т.к. он заплатит в бюджет больше НДС
                        </li>
                        <li>
                            &#10003;
                            Подставите клиента, т.к. ему придется объясняться с налоговой
                        </li>
                        <li>
                            &#10003;
                            Налоговая затребует уточнения по вашей отчетности
                        </li>
                        <li>
                            &#10003;
                            Потеряете время и нервы – разборки с клиентом и налоговой
                        </li>
                        <li>
                            &#10003;
                            Потеряете репутацию – вас будут считать не надежным поставщиком
                        </li>
                        <li>
                            &#10003;
                            Потеряете деньги, т.к. ваш клиент сможет через суд взыскать с вас налоговые штрафы, которые получит по вашей вине
                        </li>
                        <li>
                            &#10003;
                            Потеряете клиента – как результат всего перечисленного выше.
                        </li>
                    </ul>
                </div>
                <div style="margin-bottom: 15px;">
                    <span style="font-weight: bold;">
                        Как избежать потерь? Для этого нужно нажать пару кнопок:
                    </span>
                    <ul style="margin: 0; padding: 0; padding-left: 30px; list-style: none;">
                        <li>
                            1. Нажать в счете на ОДНУ кнопку для создания счет-фактуры.
                        </li>
                        <li>
                            2. Нажать на ЕЩЕ ОДНУ кнопку и распечатать её.
                        </li>
                        <li>
                            3. Подписать счет-фактуру и передать клиенту
                        </li>
                    </ul>
                </div>
                <div style="margin-bottom: 15px;">
                    <img src="https://lk.kub-24.ru/img/unisender/inactive/4/4.jpg" style="max-width: 100%" alt="">
                </div>
            </td>
        </tr>
        <tr>
            <td style="font-weight: bold; color: #505050;">
                <div style="text-align: center;">
                    Онлайн сервис КУБ
                    <br>
                    простая программа автоматизации
                    <br>
                    для малого и среднего бизнеса
                </div>
            </td>
        </tr>
        <tr>
            <td style="text-align: center;">
                <a href="https://kub-24.ru/login/?<?= $linkParams ?>&amp;utm_content=button"
                   style="color:#ffffff; font-weight:400; text-decoration:none; padding:10px 20px 10px 20px; background-color:#0077a7">
                    ВОЙТИ в КУБ
                </a>
            </td>
        </tr>
    </tbody>
</table>

<?= $this->render('@common/mail/layouts/parts/_arslan', ['message' => $message]) ?>

<?php \Yii::$app->view->endContent(); ?>
