<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'progrev';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
        "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}";
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/html.php'); ?>

<table cellpadding="0" cellspacing="0" style="width:600px;">
    <tbody>
        <tr>
            <td align="center">
                <?= Html::img('https://lk.kub-24.ru/img/unisender/9-2.jpg', [
                    'width' => 600,
                    'alt' => '',
                ]); ?>
            </td>
        </tr>
    </tbody>
</table>
<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody>
        <tr>
            <td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
                <h1 style="margin-top:0; font-size:28px !important; line-height:100% !important; font-weight:600; color:#333333">
                    Добрый день{{Name?, }}{{Name}}!
                </h1>
                <b>Ваш бесплатный период в 14 дней заканчивается и ваш аккаунт будет переведен на тариф БЕСПЛАТНО.</b>
                <br>
                Что бы использовать весь функционал КУБа, нужно выбрать платный тариф.
                Для этого зайдите в раздел «Оплатить сервис» и выберете удобный для вас вариант оплаты.
                <br>
                При оплате за один месяц - <span style="font-weight: bold;">600 руб. в месяц</span>.
                <br>
                При оплате 4-х месяцев - стоимость <span style="font-weight: bold;">540 руб. в месяц</span>.
                <br>
                При оплате 12-ти месяцев - стоимость <span style="font-weight: bold;">480 руб. в месяц</span>.
                <br>
                <br>
                <b>Если у вас менее 5-ти счетов в месяц, то используйте тариф БЕСЛАТНО.</b>
                <br>
                Данный тариф имеет ограничения:
                <ul style="list-style-position: inside;">
                    <li>
                        Не более 5 счетов в месяц
                    </li>
                    <li>
                        Только 1 организация. Т.е. если вы владелец более 1-й компанией, то вы не сможете добавить еще компании.
                    </li>
                    <li>
                        Не больше 3-х сотрудников
                    </li>
                    <li>
                        Место на диске - 1 ГБ
                    </li>
                </ul>
            </td>
        </tr>
    </tbody>
</table>

<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody>
        <tr>
            <td align="center" style="background-color:#ffffff; font-family:Arial,Helvetica,sans-serif; line-height:100%; font-size:16px; ">
                <a href="https://kub-24.ru/login/?<?= $linkParams ?>&amp;utm_content=button" style="color:#ffffff; font-weight:400; text-decoration:none; padding:10px 20px 10px 20px; background-color:#0077a7">
                    Оплатить КУБ
                </a>
            </td>
        </tr>
    </tbody>
</table>

<?= $this->render('@common/mail/layouts/parts/_arslan', ['message' => $message]) ?>

<?= $this->render('@common/mail/layouts/parts/_links') ?>

<?php \Yii::$app->view->endContent(); ?>
