<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'progrev';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
        "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}";
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/html.php'); ?>

<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody>
        <tr>
            <td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
                <h1 style="margin-top:0; font-size:28px !important; line-height:100% !important; font-weight:600; color:#333333">
                    Добрый день{{Name?, }}{{Name}}!
                </h1>
                <div style="margin: 20px 0;">
                    Благодарим Вас за регистрацию в сервисе КУБ.
                    <br>
                    Мы обратили внимание, что Вы не используете сервис.
                </div>
                <div style="margin: 20px 0;">
                    Возможно, на то есть причины?
                </div>
                <div style="margin: 20px 0 0;">
                    Ответьте на один вопрос, нам важно знать Ваше мнение о сервисе КУБ:
                </div>
            </td>
        </tr>
    </tbody>
</table>

<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px dotted #cccccc;">
    <tbody>
        <tr>
            <td colspan="2" style="text-align:center; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%; padding-bottom:5">
                <h2 style="margin-top:0; font-size:18px !important; line-height:100% !important; font-weight:600; color:#333333">
                    Оцените наш сервис по шкале от 1 до 10
                </h2>
                <br />
                <a href="http://kub-24.ru/otvet-na-opros-prinyat/?utm_source=unisender&utm_medium=opros&utm_campaign=1&utm_content={{Email}}"
                    style="margin-right:13px; text-decoration:none;" target="_self" title="Поставить оценку">
                    <img src="https://lk.kub-24.ru/img/unisender/delivery/opros/1.jpg" width="40" alt="1" />
                </a>
                <a href="http://kub-24.ru/otvet-na-opros-prinyat/?utm_source=unisender&utm_medium=opros&utm_campaign=2&utm_content={{Email}}"
                    style="margin-right:13px; text-decoration:none;" target="_self" title="Поставить оценку">
                    <img src="https://lk.kub-24.ru/img/unisender/delivery/opros/2.jpg" width="40" alt="2" />
                </a>
                <a href="http://kub-24.ru/otvet-na-opros-prinyat/?utm_source=unisender&utm_medium=opros&utm_campaign=3&utm_content={{Email}}"
                    style="margin-right:13px; text-decoration:none;" target="_self" title="Поставить оценку">
                    <img src="https://lk.kub-24.ru/img/unisender/delivery/opros/3.jpg" width="40" alt="3" />
                </a>
                <a href="http://kub-24.ru/otvet-na-opros-prinyat/?utm_source=unisender&utm_medium=opros&utm_campaign=4&utm_content={{Email}}"
                    style="margin-right:13px; text-decoration:none;" target="_self" title="Поставить оценку">
                    <img src="https://lk.kub-24.ru/img/unisender/delivery/opros/4.jpg" width="40" alt="4" />
                </a>
                <a href="http://kub-24.ru/otvet-na-opros-prinyat/?utm_source=unisender&utm_medium=opros&utm_campaign=5&utm_content={{Email}}"
                    style="margin-right:13px; text-decoration:none;" target="_self" title="Поставить оценку">
                    <img src="https://lk.kub-24.ru/img/unisender/delivery/opros/5.jpg" width="40" alt="5" />
                </a>
                <a href="http://kub-24.ru/otvet-na-opros-prinyat/?utm_source=unisender&utm_medium=opros&utm_campaign=6&utm_content={{Email}}"
                    style="margin-right:13px; text-decoration:none;" target="_self" title="Поставить оценку">
                    <img src="https://lk.kub-24.ru/img/unisender/delivery/opros/6.jpg" width="40" alt="6" />
                </a>
                <a href="http://kub-24.ru/otvet-na-opros-prinyat/?utm_source=unisender&utm_medium=opros&utm_campaign=7&utm_content={{Email}}"
                    style="margin-right:13px; text-decoration:none;" target="_self" title="Поставить оценку">
                    <img src="https://lk.kub-24.ru/img/unisender/delivery/opros/7.jpg" width="40" alt="7" />
                </a>
                <a href="http://kub-24.ru/otvet-na-opros-prinyat/?utm_source=unisender&utm_medium=opros&utm_campaign=8&utm_content={{Email}}"
                    style="margin-right:13px; text-decoration:none;" target="_self" title="Поставить оценку">
                    <img src="https://lk.kub-24.ru/img/unisender/delivery/opros/8.jpg" width="40" alt="8" />
                </a>
                <a href="http://kub-24.ru/otvet-na-opros-prinyat/?utm_source=unisender&utm_medium=opros&utm_campaign=9&utm_content={{Email}}"
                    style="margin-right:13px; text-decoration:none;" target="_self" title="Поставить оценку">
                    <img src="https://lk.kub-24.ru/img/unisender/delivery/opros/9.jpg" width="40" alt="9" />
                </a>
                <a href="http://kub-24.ru/otvet-na-opros-prinyat/?utm_source=unisender&utm_medium=opros&utm_campaign=10&utm_content={{Email}}"
                    style="text-decoration:none;" target="_self">
                    <img src="https://lk.kub-24.ru/img/unisender/delivery/opros/10.jpg" width="40" alt="10" />
                </a>
            </td>
        </tr>
        <tr>
            <td style="margin-top:0px; padding-top:0px; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%; font-weight:600;">
                Очень плохо
            </td>
            <td style="text-align:right; margin-top:0px; padding-top:0px; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%; font-weight:600;">
                Очень хорошо
            </td>
        </tr>
    </tbody>
</table>

<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody>
        <tr>
            <td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
                Спасибо за ваш ответ.
                Если у вас возникнут вопросы по сервису КУБ,
                звоните нам на бесплатный номер <span style="color:#4276a5;">8-800-500-54-36</span>
                или пишите на почту <span style="color:#4276a5;">support@kub-24.ru</span>
            </td>
        </tr>
    </tbody>
</table>

<?= $this->render('@common/mail/layouts/parts/_arslan', ['message' => $message]) ?>

<?= $this->render('@common/mail/layouts/parts/_links') ?>

<?php \Yii::$app->view->endContent(); ?>
