<?php

use common\models\Discount;
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'work_with_debtors';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
        "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}";

$host = Yii::$app->params['serviceSiteLk'];
$serviceSite = Yii::$app->params['serviceSite'];
$serviceUrl = trim($serviceSite, '/')."/?".$linkParams."&amp;utm_content=service";
$registerUrl = trim($serviceSite, '/')."/registration/?".$linkParams."&amp;utm_content=registration";
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/html.php'); ?>

<table cellpadding="0" cellspacing="0" style="width:600px;">
    <tbody>
        <tr>
            <td align="center">
                <?= Html::img($host.'img/unisender/price-list/img1.jpg', [
                    'style' => 'max-width: 100%;',
                    'alt' => '',
                ]); ?>
            </td>
        </tr>
    </tbody>
</table>
<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody>
        <tr>
            <td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
                <h1 style="margin-top:0; font-size:28px !important; line-height:100% !important; font-weight:600; color:#333333">
                    Привет{{Name?, }}{{Name}}!
                </h1>
                <br>
                <div>
                    Если Вы регистрировались в КУБе, что бы сделать прайс-лист,
                    но не разобрались с этим, или вам не понравилось, то это письмо для вас.
                </div>
                <br>
                <div>
                    Мы исправились – мы сделали создание, редактирование и работу с прайс-листами проще и удобнее.
                    <br>
                    Что мы сделали:
                    <br>
                    1) Разбили создание прайс-листа по шагам.
                    <br>
                    2) Для каждого шага сделали отдельную страницу.
                    <br>
                    3) Наполнили крутым функционалом возможности прайс-листа.
                    <br>
                    4) Прайс-лист можно скачать как PDF и Excel или отправить как ссылку.
                </div>
                <br>
                <div>
                    <strong>
                        ВЫГОДЫ для  ВАС
                    </strong>
                    <ul style="margin: 0;">
                        <li><strong>Меньше времени на создание и редактирование прайс-листов</strong></li>
                        <li><strong>Довольные клиенты</strong></li>
                        <li><strong>Увеличение продаж</strong></li>
                    </ul>
                </div>
                <br>
                <div>
                    Порсмотрите образец Прайс-листа
                    <?= Html::a('тут', 'https://lk.kub-24.ru/price-list/out-view?uid=1f3afc6af9e761d9') ?>
                </div>
                <br>
                <div>
                    <strong>
                        Ваши  ПРЕИМУЩЕСТВА
                        <br>
                        1) Онлайн прайс-лист
                    </strong>
                    <ul style="margin: 0;">
                        <li>Клиент может пользоваться им постоянно, пока вы его не отключите.</li>
                        <li>Нужно поменять цену, кол-во, описание? Меняйте всё онлайн – данные в отправленном прайс-листе обновятся автоматически.</li>
                        <li>Теперь не нужно после любого исправления рассылать новые прайс-листы!</li>
                    </ul>
                </div>
                <br>
                <div>
                    <strong>
                        2) Продающий прайс-лист
                    </strong>
                    <ul style="margin: 0;">
                        <li>Вам не нужно оформлять заказ и выставлять счета! Теперь заказ товара и счет на его оплату формируется из прайс-листа. Клиент это сможет сделать самостоятельно в любое время дня и ночи.</li>
                        <li>Потенциальные клиенты обязательно оценят такой подход.</li>
                    </ul>
                </div>
                <br>
                <div>
                    <strong>
                        3) Как мини интернет-магазин
                    </strong>
                    <ul style="margin: 0;">
                        <li>Добавьте фото товара и описание товара. Тогда при клике на строку прайс-листа открывается карточка товара, в которой фото и описание товара.</li>
                        <li>Прайс-лист по ссылке открывается как на компьютере, так и на телефоне. На телефоне он  будет аккуратным и читаемым.</li>
                        <li>Ссылку на прайс-лист можно отправлять не только по e-mail, но и в месседжере или в соцсети.</li>
                    </ul>
                </div>
                <br>
                <div>
                    <strong>
                        4) Индивидуальные настройки под каждого клиента
                    </strong>
                    <ul style="margin: 0;">
                        <li>Указывайте индивидуальные скидки.</li>
                        <li>Указывайте индивидуальные наценки.</li>
                        <li>Добавляйте нужные столбцы и убирайте лишние.</li>
                        <li>Указывайте только тот товар, который есть на складе или весь список возможного товара.</li>
                    </ul>
                </div>
                <br>
                <div>
                    <strong>
                        5) Вишенка на торте – Уведомление о прочтении
                    </strong>
                    <ul style="margin: 0;">
                        Как только клиент откроет ваш прайс-лист, вам придет уведомление!
                        Позвоните клиенту в тот момент, когда он изучает ваш прайс-лист.
                        Вы его приятно удивите и, сразу сможете ответить на все его вопросы и проконсультировать по товару.
                        <br>
                        <strong>
                            Вы опередите ваших конкурентов с умным прайс-листом!
                        </strong>
                    </ul>
                </div>
                <br>
                <div>
                    Создание прайс-листов находится в левом меню в разделе «Товары», называется «Прайс-листы». В шапке переключитесь на новый дизайн!
                    <br>
                    Порсмотрите образец Прайс-листа
                    <?= Html::a('тут', 'https://lk.kub-24.ru/price-list/out-view?uid=1f3afc6af9e761d9') ?>
                </div>
                <br>
                <div>
                    Успехов вам и вашему бизнесу!
                </div>
            </td>
        </tr>
    </tbody>
</table>

<?= $this->render('@common/mail/layouts/parts/_alexey', ['message' => $message]) ?>

<?= $this->render('@common/mail/layouts/parts/_links') ?>

<?php \Yii::$app->view->endContent(); ?>
