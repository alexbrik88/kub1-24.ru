<?php

use common\models\Discount;
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'progrev';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
        "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}";

$host = Yii::$app->params['serviceSiteLk'];
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/html.php'); ?>

<table cellpadding="0" cellspacing="0" style="width:600px;">
    <tbody>
        <tr>
            <td align="center">
                <?= Html::img($host.'img/unisender/new_design/img_0.png', [
                    'style' => 'max-width: 100%;',
                    'alt' => '',
                ]); ?>
            </td>
        </tr>
    </tbody>
</table>
<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody>
        <tr>
            <td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
                <h1 style="margin-top:0; font-size:28px !important; line-height:100% !important; font-weight:600; color:#333333">
                    Добрый день{{Name?, }}{{Name}}!
                </h1>
                <p>
                    У нас отличная новость - мы обновили дизайн сервиса!
                    <br>
                    Наш сервис постоянно развивается, и мы становимся более технологичными,
                    за счет добавления нового функционала и новых интеграций с партнерами.
                    В старом дизайне стало сложнее добавлять новый функционал,
                    и даже новые кнопки иногда вмещаются с большим трудом.
                    Поэтому в середине прошлого года мы решили изменить дизайн и немного поменять структуру сервиса.
                </p>
                <p>
                    За основу мы взяли <strong>бизнес стиль</strong> в онлайне.
                    Изучив кучу всего по дизайну, мы постарались использовать современные тенденции в онлайн дизайне
                    и этот самый «<strong>бизнес стиль</strong>».
                    В итоге дизайн получился более легким, простым, понятным и удобным в использовании.
                </p>
                <p>
                    Мы знаем, что иногда тяжело переключаться на что-то новое. Вы привыкли к одному, а тут что-то новое…… Поэтому мы сделали переключатель между сегодняшним и новым дизайном, что бы вы постепенно изучали новый дизайн и полностью перешли на него.
                </p>
                <p>
                    Помимо нового дизайна, у нас еще новости – мы добавили 2 новых сервиса:
                    <br>
                    <strong>«Досье»</strong> - проверка контрагентов.
                    <br>
                    <strong>«Документы»</strong> - распознавание сканов документов и ввод таким образом документов в КУБ
                    <br>
                    Подробно о них расскажем в следующих письмах.
                </p>
                <br>
                <div>
                    <strong>
                        1) Новые иконки, которых раньше не было в верхнем меню.
                    </strong>
                    <div>
                        <?= Html::img($host.'img/unisender/new_design/img_1_1.jpg', [
                            'style' => 'max-width: 100%; border: 1px solid #ccc;',
                            'alt' => '',
                        ]); ?>
                    </div>
                    Тут переключатель между версиями.
                </div>
                <br>
                <div>
                    <strong>Далее слева на право:</strong>
                    <br>
                    <strong>Иконка «Папка» (на картинке с цифрой 6)</strong>
                    – это иконка, которая ведет в новый раздел Документы.
                    <div>
                        <?= Html::img($host.'img/unisender/new_design/img_1_2.jpg', [
                            'style' => 'max-width: 100%; border: 1px solid #ccc;',
                            'alt' => '',
                        ]); ?>
                    </div>
                    Там можно автоматически собирать сканы от ваших контрагентов,
                    раскладывать сканы по папочкам и распознавать сканы, превращая их в документы.
                    Об этом разделе подробнее в следующем письме.
                </div>
                <br>
                <div>
                    <strong>Иконка «Портфель»</strong>
                    – это информация по вашим тарифам.
                    <div>
                        <?= Html::img($host.'img/unisender/new_design/img_1_3.jpg', [
                            'style' => 'max-width: 100%; border: 1px solid #ccc;',
                            'alt' => '',
                        ]); ?>
                    </div>
                    Сколько счетов у вас осталось. Сколько дней до окончания тарифа.
                    Остальные три – это к новому функционалу, расскажу в следующих письмах.
                </div>
                <br>
                <div>
                    <strong>Иконка «9 маленьких квадратиков»</strong>
                    – это удобный доступ и переключение между сервисами КУБа.
                    <div>
                        <?= Html::img($host.'img/unisender/new_design/img_1_4.jpg', [
                            'style' => 'max-width: 100%; border: 1px solid #ccc;',
                            'alt' => '',
                        ]); ?>
                    </div>
                </div>
                <br>
                <div>
                    <strong>Иконка «Колокольчик»</strong>
                    – тут сообщения по налоговому календарю с ближайшими событиями.
                    <div>
                        <?= Html::img($host.'img/unisender/new_design/img_1_5.jpg', [
                            'style' => 'max-width: 100%; border: 1px solid #ccc;',
                            'alt' => '',
                        ]); ?>
                    </div>
                </div>
                <br>
                <div>
                    <strong>Иконка «Кружочек с человечком»</strong>
                    – это меню пользователя с доступами к настройкам и оплате.
                    <div>
                        <?= Html::img($host.'img/unisender/new_design/img_1_6.jpg', [
                            'style' => 'max-width: 100%; border: 1px solid #ccc;',
                            'alt' => '',
                        ]); ?>
                    </div>
                </div>
                <br>
                <div>
                    <strong>2) У списков документов есть кнопка, которая делает таблицу более компактной.</strong>
                    <div>
                        <?= Html::img($host.'img/unisender/new_design/img_2_1.jpg', [
                            'style' => 'max-width: 100%; border: 1px solid #ccc;',
                            'alt' => '',
                        ]); ?>
                    </div>
                </div>
                <br>
                <div>
                    <strong>3) «Досье», т.е. проверка контрагентов</strong>
                    (Более подробно в следующем письме)
                    <div>
                        <?= Html::img($host.'img/unisender/new_design/img_3_1.jpg', [
                            'style' => 'max-width: 100%; border: 1px solid #ccc;',
                            'alt' => '',
                        ]); ?>
                    </div>
                </div>
                <br>
                <div>
                    <strong>4) Доработали карточку товара.</strong>
                    <br>
                    Дополнили закладки  «Цены», «Оборот», «Управление запасами» дополнительным функционалом.
                    <div>
                        <?= Html::img($host.'img/unisender/new_design/img_4_1.jpg', [
                            'style' => 'max-width: 100%; border: 1px solid #ccc;',
                            'alt' => '',
                        ]); ?>
                    </div>
                </div>
                <br>
                <p>
                    Новый функционал будем добавлять как в старом, так и в новом дизайне.
                    Но после 01.06.2020 обновления будут только в новом дизайне.
                </p>
                <p>
                    Ждем ваши оценки нового дизайна, а так же пожелания и замечания.
                </p>
            </td>
        </tr>
    </tbody>
</table>

<?= $this->render('@common/mail/layouts/parts/_alexey', ['message' => $message]) ?>

<?= $this->render('@common/mail/layouts/parts/_links') ?>

<?php \Yii::$app->view->endContent(); ?>
