<?php

use common\models\Discount;
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'progrev';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
        "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}";

$host = Yii::$app->params['serviceSiteLk'];
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/html.php'); ?>
<style type="text/css">
ul#info-list {
  list-style: none;
}
ul#info-list li:before {
  content: '✓ ';
}
</style>
<table cellpadding="0" cellspacing="0" style="width:600px;">
    <tbody>
        <tr>
            <td align="center">
                <?= Html::img($host.'img/unisender/dossier/img_0.jpg', [
                    'style' => 'max-width: 100%;',
                    'alt' => '',
                ]); ?>
            </td>
        </tr>
    </tbody>
</table>
<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody>
        <tr>
            <td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
                <h1 style="margin-top:0; font-size:28px !important; line-height:100% !important; font-weight:600; color:#333333">
                    Добрый день{{Name?, }}{{Name}}!
                </h1>
                <p>
                    Часто мы соглашаемся практически на любые условия, только что бы потенциальный клиент купил именно у нас.
                    На первом месте среди этих «любых условий» – отсрочка платежа.
                    Скорее всего, вы уже знакомы с сервисами по проверке контрагентов.
                    Мы хотим обезопасить вас от недобросовестных клиентов и поставщиков,
                    поэтому добавили в КУБ раздел проверки контрагентов «КУБ.Досье».
                </p>
                <p>
                    <strong>Чем наш сервис лучше других? </strong>
                    <br>
                    Наш сервис позволяет иметь под рукой «Досье» на ваших контрагентов.
                    Вы делаете отгрузку покупателю или выставляете ему счет и можете сразу посмотреть,
                    какая ситуация по покупателю, более того эта информация будет всегда в карточке клиента.
                    Вы можете узнать, в каких судебных делах ваш клиент отвечает ответчиком, и на какие суммы.
                    Есть ли уже в отношении него дела у приставов, по которым будут списаны деньги или изъяты товары со склада.
                    Так же можно посмотреть финансовое положение данной компании и всех компаний связанных с ней.
                    <br>
                    Т.е. вы сможете оценить риски – стоит ли отгружать товар или оказывать услуги с отсрочкой платежа
                    или необходимо договориться о предоплате.
                </p>
                <p>
                    <strong>Мы собрали для вас актуальную информацию по контрагентам из:</strong>
                    <ul id="info-list">
                        <li>Налоговой</li>
                        <li>Арбитражных судов</li>
                        <li>ФССП (приставы)</li>
                        <li>Росстата</li>
                    </ul>
                </p>
                <p>
                    Раздел «Досье» находится в карточке контрагента и включает в себя сводную информацию и 8 разделов:
                    <div>
                        <?= Html::img($host.'img/unisender/dossier/img_1.jpg', [
                            'style' => 'max-width: 100%; border: 1px solid #ccc;',
                            'alt' => '',
                        ]); ?>
                    </div>
                </p>
                <div>
                    <strong>
                        Что бы бизнес был стабилен и прибыльным,
                    </strong>
                    необходимо проверять, как новых клиентов, так и текущих, что бы ваша прибыль ни застревала в долгах, которые вам не смогут оплатить ваши клиенты.
                </div>
            </td>
        </tr>
    </tbody>
</table>

<?= $this->render('@common/mail/layouts/parts/_arslan', ['message' => $message]) ?>

<?= $this->render('@common/mail/layouts/parts/_links') ?>

<?php \Yii::$app->view->endContent(); ?>
