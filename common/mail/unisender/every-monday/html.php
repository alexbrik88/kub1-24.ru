<?php
/**
 * @var $message \common\components\sender\unisender\UniSender
 */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'progrev';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
        "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}";
$startWeekTime = strtotime('-7 day');
$startWeek = date('d.m.Y', $startWeekTime);
$endWeek = date('d.m.Y', strtotime('-1 day'));
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/html.php'); ?>

<table cellpadding="20" cellspacing="0" style="width:600px;">
    <tr>
        <td>
            <p style="font-size: 24px; margin-top: 20px; line-height: 21px; margin-bottom: 20px; color: #000;">
                Ваши недельные результаты!
            </p>
        </td>
    </tr>
    <tr>
        <td>
            <p style="margin: 0; font-size: 12pt; margin-bottom: 20px;">
                Привет{{Name?, }}{{Name}}!
            </p>
        </td>
    </tr>
    <tr>
        <td>
            <p style="margin: 0; font-size: 12pt;">
                Итоги вашего бизнеса ({{company}}) за прошедшую неделю:
                <i><?= $startWeek . ' - ' . $endWeek; ?></i>
            </p>
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="width: 45%;background-color: #E0F3FE;">
                        <table style="width: 100%;">
                            <tbody>
                            <tr>
                                <td style="border-bottom: 1px;padding-top: 5px;padding-bottom: 5px;font-size: 14px;padding-left: 10px;border-style: solid;border-top: 0;border-left: 0;border-right: 0;border-color: #C3E7FA;">
                                    <i>Клиенты вам
                                        оплатили:</i>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table style="width: 100%;">
                            <tbody>
                            <tr>
                                <td style="border-bottom: 1px;padding-top: 15px;padding-bottom: 25px;font-size: 30px;padding-left: 10px;text-align: center;">
                                    {{clientInSum}} ₽
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                    <td style="width: 10%;">
                    </td>
                    <td style="width: 45%;background-color: #E0F3FE;">
                        <table style="width: 100%;">
                            <tbody>
                            <tr>
                                <td style="border-bottom: 1px;padding-top: 5px;padding-bottom: 5px;font-size: 14px;padding-left: 10px;border-style: solid;border-top: 0;border-left: 0;border-right: 0;border-color: #C3E7FA;">
                                    <i>Клиенты вам
                                        должны:</i></td>
                            </tr>
                            </tbody>
                        </table>
                        <table style="width: 100%;">
                            <tbody>
                            <tr>
                                <td style="border-bottom: 1px;padding-top: 15px;padding-bottom: 25px;font-size: 30px;padding-left: 10px;text-align: center;">
                                    {{clientOutSum}} ₽
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellspacing="0" cellpadding="0" style="width: 100%;">
                <tr>
                    <td style="width: 100%; border-bottom: 1px dashed #DEDEDE;">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="width: 45%;">
                        <table
                            style="width: 100%; background-color: #C7E9FF;">
                            <tbody>
                            <tr>
                                <td style="border-bottom: 0;padding-top: 3px;padding-bottom: 3px;font-size: 14px;padding-left: 10px;border-style: solid;border-top: 0;border-left: 0;border-right: 0;border-color: #ffffff;">
                                    <i>Вы выставили
                                        счетов на:</i>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table style="width: 100%;">
                            <tbody>
                            <tr>
                                <td style="border-bottom: 1px;padding-top: 15px;padding-bottom: 30px;font-size: 30px;padding-left: 10px;text-align: center;">
                                    {{exhibitInvoiceSum}}
                                    ₽
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                    <td style="width: 10%;">
                    </td>
                    <td style="width: 45%;">
                        <table style="width: 100%;">
                            <tbody>
                            <tr>
                                <td style="border-bottom: 1px;padding-top: 0;padding-bottom: 13px;font-size: 14px;padding-left: 10px;">
                                    <i>Откладывали
                                        выставления
                                        счетов? Это
                                        легко исправить.
                                        Начните
                                        выставлять
                                        счета, быстрая
                                        отправка счетов
                                        - это самый
                                        быстрый способ
                                        получить
                                        деньги.</i></td>
                            </tr>
                            </tbody>
                        </table>
                        <table style="width: 100%;">
                            <tbody>
                            <tr>
                                <td style="border-bottom: 1px;padding-top: 0;padding-bottom: 0;font-size: 13px;padding-left: 10px;text-align: left;">
                                    <a href="<?= $message->getParam('lkBaseUrl') . '/documents/invoice/create?type=2&' .$linkParams .'&utm_content=create-invoice'; ?>"
                                       style="text-decoration: none;color: #ffffff;padding-top: 8px;background-color: #0077A7;padding-bottom: 8px;padding-left: 16px;padding-right: 16px;">Выставить
                                        счет</a></td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellspacing="0" cellpadding="0" style="width: 100%;">
                <tr>
                    <td style="width: 100%; border-bottom: 1px dashed #DEDEDE;">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellspacing="0" cellpadding="0" style="margin-bottom: 30px;">
                <tr>
                    <td style="width: 45%;">
                        <table style="width: 100%; background-color: #C7E9FF;">
                            <tbody>
                            <tr>
                                <td style="border-bottom: 0;padding-top: 3px;padding-bottom: 3px;font-size: 14px;padding-left: 10px;border-style: solid;border-top: 0;border-left: 0;border-right: 0;border-color: #ffffff;">
                                    <i>Ваши расходы:</i>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                        <table style="width: 100%;">
                            <tbody>
                            <tr>
                                <td style="border-bottom: 1px;padding-top: 35px;padding-bottom: 25px;font-size: 30px;padding-left: 10px;text-align: center;">
                                    {{expenses}} ₽
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                    <td style="width: 10%;">
                    </td>
                    <td style="width: 45%;">
                        <table style="width: 100%;">
                            <tbody>
                            <tr>
                                <td style="border-bottom: 1px;padding-top: 0;padding-bottom: 13px;font-size: 14px;padding-left: 10px;">
                                    <i>Вы уверены, что
                                        ничего не
                                        покупали на этой
                                        неделе? Если вы
                                        пропустили и не
                                        внесли расходные
                                        документы, то
                                        сделайте это
                                        сейчас. Когда
                                        нужно будет
                                        сдавать
                                        отчетность, они
                                        вам обязательно
                                        понадобятся, что
                                        бы уменьшить
                                        налоги.</i></td>
                            </tr>
                            </tbody>
                        </table>
                        <table style="width: 100%;">
                            <tbody>
                            <tr>
                                <td style="border-bottom: 1px;padding-top: 0;padding-bottom: 0;font-size: 13px;padding-left: 10px;text-align: left;">
                                    <a href="<?= $message->getParam('lkBaseUrl') . Yii::$app->params['uniSender']['utm']['addCosts']; ?>"
                                       style="text-decoration: none;color: #ffffff;padding-top: 8px;background-color: #0077A7;padding-bottom: 8px;padding-left: 16px;padding-right: 16px;">Внести
                                        расходы</a></td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
<?= $this->render('@common/mail/layouts/parts/_contacts') ?>

<?php \Yii::$app->view->endContent(); ?>
