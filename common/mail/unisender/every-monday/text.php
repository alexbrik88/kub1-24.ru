<?php
/**
 * @var $message \common\components\sender\unisender\UniSender
 */
$startWeekTime = strtotime('-7 day');
$startWeek = date('d.m.Y', $startWeekTime);
$endWeek = date('d.m.Y', time());
?>
Ваши недельные рузльтаты!
Привет{{Name?, }}{{Name}}!
Итоги вашего бизнеса за прошедшую неделю: <?= $startWeek . ' - ' . $endWeek; ?>

Клиенты вам оплатили: {{clientInSum}} ₽.

Клиенты вам должны: {{clientOutSum}} ₽.

Вы выставили счетов на: {{exhibitInvoiceSum}} ₽.

Ваши расходы: {{expenses}} ₽.

По всем вопросам обращайтесь в службу поддержки support@kub-24.ru



Вы получили это письмо, потому что подписывались на новости сервиса КУБ на сайте <?php echo $message->getParam('baseUrl'); ?>.
У Вас есть возможность отписаться от рассылки, для этого необходимо перейти в личный кабинет <?php echo $message->getParam('lkBaseUrl'); ?>, в раздел настройки и убрать галочки в блоке «Получение уведомлений».
