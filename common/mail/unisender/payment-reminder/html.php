<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 14.05.2018
 * Time: 5:24
 */

use common\models\company\CompanyType;
use common\components\helpers\Html;

/**
 * @var $message \common\components\sender\unisender\UniSender
 * @var $subject string
 * @var $content string
 * @var $author \common\models\EmployeeCompany
 */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'progrev';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
    "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}";
?>
<?php Yii::$app->view->beginContent('@common/mail/layouts/html.php'); ?>

<table border="0" cellpadding="20" cellspacing="0" style="width:600px;">
    <tr>
        <td>
            <p style="margin: 0; font-size: 12pt;">
                <?= $content; ?>
            </p>
            <div style="margin-top: 40px">
                С уважением,<br>
                <?= $author->getFio(true); ?>
                <?php if ($author->company->company_type_id != CompanyType::TYPE_IP) : ?>
                    <br>
                    <?= $author->position; ?>
                <?php endif ?>
            </div>
        </td>
    </tr>
    <tr>
        <td style="padding: 0;">
            <table cellpadding="0" cellspacing="0" style="width: 100%;">
                <tbody>
                <tr>
                    <td style="font-size:10px; padding:10px 20px 10px 20px; border-top:1px solid #cccccc">
                        <table cellpadding="0" cellspacing="0" style="width: 100%;">
                            <tbody>
                            <tr>
                                <td width="80%" align="left" valign="top" style="color:#505050; line-height:15px; font-size:10px;">
                                    <p style="margin: 0;">
                                        Это письмо было отправлено через сервис выставления счетов
                                        <br>
                                        <?= Html::a('Попробовать бесплатно', Yii::$app->params['serviceSite'] .
                                            "?" . $linkParams . "&utm_content=try_free", [
                                            'target' => '_blank',
                                            'style' => 'color: #055EC3;',
                                        ]); ?>
                                    </p>
                                </td>
                                <td width="20%" align="right" valign="middle">
                                    <?= Html::a(Html::img($message->embed(Yii::getAlias('@common/mail/assets/img/logo.png')), [
                                        'style' => 'style="border-bottom-width:0px; border-left-width: 0px; border-right-width: 0px; border-top-width: 0px;',
                                    ]), Yii::$app->params['serviceSite'] . "?" . $linkParams . "&utm_content=logo", [
                                        'target' => '_blank',
                                    ]); ?>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
                </tbody>
            </table>
        </td>
    </tr>
</table>
<?php Yii::$app->view->endContent(); ?>
