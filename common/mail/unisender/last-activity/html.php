<?php
/**
 * @var $message \common\components\sender\unisender\UniSender
 */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'progrev';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
        "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}";
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/html.php'); ?>

<table cellpadding="20" cellspacing="0" style="width:600px;">
    <tr>
        <td>
            <p style="font-size: 24pt; line-height: 21px;">
                Привет{{Name?, }}{{Name}}!
            </p>
            <p style="margin: 0; font-size: 12pt; margin-bottom: 20px;">
                Меня зовут Алексей.<br>
                Вы зарегистрировались в сервисе КУБ и больше не заходили в него. Я знаю, что сложно найти время, что бы изучить, что-то новое, но поверьте мне -  это стоит того. Автоматизировав выставление счетов и других документов, вы упростите свою работу и будете уверены, что в документах нет ошибок. У вас будет больше свободного времени, что бы заниматься вашим любимым делом.
            </p>
            <p style="margin: 0; font-size: 12pt;">
                Потратьте несколько минут, что бы посмотреть, как КУБ упростит вашу работу.
            </p>
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="width: 60%; padding-left: 20px">
                        <p style="margin: 0; font-size: 12pt;">
                            <span style="font-weight: bold;">1. Заполнение данных по Вашей компании.</span>
                        </p>
                        <p style="margin: 0; font-size: 12pt;">
                           Вводите ваш ИНН, добавляете логотип, скан печати и подписи.
                       </p>
                   </td>
                    <td style="width: 40%; padding-left: 20px">
                        <img src="<?php echo $message->getDataURI('video_play.png'); ?>">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellspacing="0" cellpadding="0" style="width: 100%;">
                <tr>
                    <td style="width: 100%; border-bottom: 1px dashed #DEDEDE;">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="width: 60%; padding-left: 20px">
                        <p style="margin: 0; font-size: 12pt;">
                            <span style="font-weight: bold;">2. Добавление покупателя.</span>
                        </p>
                        <p style="margin: 0; font-size: 12pt;">
                            Вводите ИНН покупателя, БИК его банк и расчетный счет.
                        </p>
                    </td>
                    <td style="width: 40%; padding-left: 20px">
                        <img src="<?php echo $message->getDataURI('macbook_ipad.png'); ?>">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellspacing="0" cellpadding="0" style="width: 100%;">
                <tr>
                    <td style="width: 100%; border-bottom: 1px dashed #DEDEDE;">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="width: 60%; padding-left: 20px">
                        <p style="margin: 0; font-size: 12pt;">
                            <span style="font-weight: bold;">3. Выставление счета. </span>
                        </p>
                        <p style="margin: 0; font-size: 12pt;">
                            Выставляйте счета в несколько кликов и отправляйте их на e-mail покупателя.
                        </p>
                    </td>
                    <td style="width: 40%; padding-left: 20px">
                        <img src="<?php echo $message->getDataURI('video_play.png'); ?>">
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="text-align: center;">
            <a href="<?=trim(\Yii::$app->params['serviceSite'], '/')?>/login/?utm_source=<?=$this->params['utm_source']?>&amp;utm_medium=email&amp;utm_campaign=<?=$this->params['utm_campaign']?>&amp;utm_content=login-content"
                target="_blank" style="color: #000;font-weight: bold;font-size: 14pt;"
                rel="noopener" data-snippet-id="2df5426d-d48c-ef22-23db-ed6cd8d5ef84">ВОЙТИ В КУБ</a>
        </td>
    </tr>
</table>
<?= $this->render('@common/mail/layouts/parts/_contacts') ?>

<?php \Yii::$app->view->endContent(); ?>
