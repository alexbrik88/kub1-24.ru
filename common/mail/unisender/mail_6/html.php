<?php
use yii\helpers\Html;

/* @var $this \yii\web\View view component instance */
/* @var $message \yii\mail\MessageInterface the message being composed */

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'progrev';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');
$linkParams = "utm_source={$this->params['utm_source']}&amp;utm_medium={$this->params['utm_medium']}" .
        "&amp;utm_campaign={$this->params['utm_campaign']}&amp;utm_term={$this->params['utm_term']}";
?>

<?php \Yii::$app->view->beginContent('@common/mail/layouts/html.php'); ?>

<table cellpadding="0" cellspacing="0" style="width:600px;">
    <tbody>
        <tr>
            <td align="center">
                <?= Html::img('https://lk.kub-24.ru/img/unisender/7-4-1.jpg', [
                    'width' => 600,
                    'alt' => '',
                ]); ?>
            </td>
        </tr>
    </tbody>
</table>
<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody>
        <tr>
            <td style="text-align:left; color:#505050; font-family:Arial,Helvetica,sans-serif; font-size:14px; line-height:150%;">
                <h1 style="margin-top:0; font-size:28px !important; line-height:100% !important; font-weight:600; color:#333333">
                    Добрый день{{Name?, }}{{Name}}!
                </h1>
                Независимо от того, насколько хорошо оказаны вами услуги и насколько замечателен ваш товар,
                Вы периодически будете сталкиваться с клиентами, которые не платят вовремя или вообще не платят.
                Поэтому важно отслеживать оплаченные счета и соответственно контролировать те, которые просрочены.
                Если счета не оплачиваются, у вас не будет денежных средств для ведения бизнеса.
                <br>
                <br>
                <b>
                    КУБ ваш электронный финансовый помощник, который:
                </b>
                <ul style="list-style-position: inside;">
                    <li>
                        Покажет задолженность по всей вашей компании.
                    </li>
                    <li>
                        Покажет долги по каждому вашему клиенту.
                    </li>
                    <li>
                        Поможет правильно составить претензию должнику.
                    </li>
                    <li>
                        Предоставит юриста для представления ваших интересов в суде.
                    </li>
                </ul>
            </td>
        </tr>
    </tbody>
</table>

<table cellpadding="20" cellspacing="0" style="width:600px; border-bottom:1px solid #cccccc;">
    <tbody>
        <tr>
            <td align="center" style="background-color:#ffffff; font-family:Arial,Helvetica,sans-serif; line-height:100%; font-size:16px; ">
                <a href="https://kub-24.ru/login/?<?= $linkParams ?>&amp;utm_content=button" style="color:#ffffff; font-weight:400; text-decoration:none; padding:10px 20px 10px 20px; background-color:#0077a7">
                    Посмотреть моих должников
                </a>
            </td>
        </tr>
    </tbody>
</table>

<?= $this->render('@common/mail/layouts/parts/_alexey', ['message' => $message]) ?>

<?= $this->render('@common/mail/layouts/parts/_links') ?>

<?php \Yii::$app->view->endContent(); ?>
