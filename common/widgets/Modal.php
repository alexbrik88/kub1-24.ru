<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 3.11.15
 * Time: 15.49
 * Email: t.kanstantsin@gmail.com
 */

namespace common\widgets;


class Modal extends \yii\bootstrap\Modal
{
    /**
     * @var string
     */
    public $content;

    /**
     * @inheritdoc
     */
    public function run()
    {
        if (!empty($this->content)) {
            echo "\n" . $this->content;
        }

        parent::run();
    }
}