<?php


namespace common\widgets;


use yii\bootstrap\Widget;

class SummarySelectProjectEstimateWidget extends Widget
{
    public $buttons = [];

    public function run()
    {
        return $this->render('summary_select_project_estimate', ['widget' => $this]);
    }
}