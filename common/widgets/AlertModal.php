<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 9/14/15
 * Time: 3:06 AM
 * Email: t.kanstantsin@gmail.com
 */

namespace common\widgets;


use yii\bootstrap\Modal;
use yii\helpers\Html;

class AlertModal extends Modal
{
    /**
     * Modal window body.
     * @var string
     */
    public $content;

    /**
     * Initializes the widget.
     */
    public function init()
    {
        if ($this->footer === null) {
            $this->footer = Html::button('Ок', [
                'class' => 'btn btn-primary',
                'data' => [
                    'dismiss' => 'modal',
                ],
            ]);
        }

        parent::init();
    }

    /**
     * Renders the widget.
     */
    public function run() {
        if ($this->content !== null) {
            echo "\n" . $this->content;
        }

        parent::run();
    }
}
