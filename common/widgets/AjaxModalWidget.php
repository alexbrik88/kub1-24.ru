<?php

namespace common\widgets;

use Yii;
use yii\bootstrap\Modal;
use yii\bootstrap\Widget;
use yii\helpers\Html;

/**
 * Class AjaxModalWidget
 * @package common\widgets

 */
class AjaxModalWidget extends Widget
{
    /**
     * @var string
     */
    public $id = 'ajax-modal-box';
    /**
     * @var string
     */
    public $linkSelector = '.ajax-modal-btn';
    /**
     * @var
     */
    public $options;
    /**
     * @var
     */
    public $closeButton = [];

    /**
     *
     */
    public function init()
    {
        Modal::begin([
            'id' => $this->id,
            'header' => '<h3 id="ajax-modal-header"></h3>',
            'options' => $this->options,
            'closeButton' => $this->closeButton,
            'toggleButton' => false,
        ]); ?>

            <div id="ajax-modal-content"></div>

        <?php Modal::end();

        $js = <<<JS
        $(function(){
            $(document).on('click', '{$this->linkSelector}', function(e){
                e.preventDefault();
                var ajaxModal = $('#{$this->id}');
                if (!ajaxModal.hasClass('in')) {
                    ajaxModal.modal('show');
                }
                $('#ajax-modal-content', ajaxModal).load($(this).attr('href') || $(this).data('url'));
                $('#ajax-modal-header', ajaxModal).html($(this).data('title') || $(this).attr('title'));
            });
            $(document).on('hidden.bs.modal', '#{$this->id}', function() {
                $('#ajax-modal-header', this).html('');
                $('#ajax-modal-content', this).html('');
            });
        });
JS;

        $view = $this->getView();
        $view->registerJs($js);
    }
}
