<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<div class="form-actions">
    <div class="row action-buttons" id="buttons-fixed">
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            <?= Html::submitButton('Сохранить', [
                'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-md hidden-sm hidden-xs',
            ]); ?>
            <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
                'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-lg',
                'title' => 'Сохранить',
            ]); ?>
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            <a href="<?= Url::to(['index']) ?>">
                <button type="button" class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs">Отменить</button>
                <button type="button" class="btn darkblue widthe-100 hidden-lg" title="Отменить"><i class="fa fa-reply fa-2x"></i></button>
            </a>
        </div>
    </div>
</div>