<?php

use common\widgets\SummarySelectProjectEstimateWidget;
use yii\helpers\Html;
use yii\web\View;

/** @var $this View */
/** @var $widget SummarySelectProjectEstimateWidget */

?>

<div id="summary-container" class="wrap wrap_btns check-condition">
    <div class="row align-items-center justify-content-end">
        <div class="column flex-shrink-0 mr-3">
            <input class="joint-operation-main-checkbox" type="checkbox" name="count-list-table">
        </div>
        <div class="column flex-shrink-0 mr-3">
            <span class="checkbox-txt total-txt-foot">
                Выбрано: <strong class="total-count-select ml-1 pl-1">0</strong>
            </span>
        </div>
        <div class="column total-txt-foot mr-3">
            Приход: <strong class="total-amount-select ml-1">0</strong>
        </div>
        <div class="column ml-auto"></div>
        <?php
        foreach (array_filter($widget->buttons[1]) as $key => $button) : ?>
            <?= Html::tag('div', $button, [
                'class' => 'column',
            ]) ?>
        <?php endforeach; ?>
    </div>
</div>

<div id="summary-container-2" class="wrap wrap_btns mb-0" style="position: sticky; z-index: 998; bottom:0;">
    <div class="row align-items-start">
        <?php foreach ($widget->buttons[0] as $key => $button) : ?>
            <div class="column mr-1">
            <?= Html::tag('td', $button, [
                'style' => 'width: 80px; height: 34px; color: #4276a4;',
            ]) ?>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="clearfix"></div>
</div>

<script>
    $(function () {
        $('.joint-operation-checkbox').on('change', function () {
            var countChecked = 0;
            var summary = 0;
            $('.joint-operation-checkbox:checked').each(function () {
                var sum = $(this).data('sum');
                summary += sum === undefined ? 0 : sum / 100;
                countChecked++;
            });
            if (countChecked > 0) {
                $('#summary-container').removeClass('hidden');
                $('#summary-container-2').addClass('hidden');

                $('#summary-container .total-count-select').text(countChecked);
                $('#summary-container .total-amount-select').text(number_format(summary, 2, ',', ' '));
            } else {
                $('#summary-container').addClass('hidden');
                $('#summary-container-2').removeClass('hidden');
            }
        });
    });
</script>