<?php

namespace common\widgets;

use yii\base\Widget;

class FormButtonsWidget extends Widget
{
    public function run()
    {
        return $this->render('formButtonsWidget');
    }
}