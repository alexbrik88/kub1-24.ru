<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 10.08.2018
 * Time: 21:50
 */

namespace common\components\helpers;



/**
 * Class Month
 * @package common\components\helpers
 */
/**
 * Class Month
 * @package common\components\helpers
 */
class Month
{
    /**
     *
     */
    const JANUARY = 1;

    /**
     *
     */
    const FEBRUARY = 2;

    /**
     *
     */
    const MARCH = 3;

    /**
     *
     */
    const APRIL = 4;

    /**
     *
     */
    const MAY = 5;

    /**
     *
     */
    const JUNE = 6;

    /**
     *
     */
    const JULY = 7;

    /**
     *
     */
    const AUGUST = 8;

    /**
     *
     */
    const SEPTEMBER = 9;

    /**
     *
     */
    const OCTOBER = 10;

    /**
     *
     */
    const NOVEMBER = 11;

    /**
     *
     */
    const DECEMBER = 12;

    /**
     *
     */
    const JANUARY_FULL = '01';

    /**
     *
     */
    const FEBRUARY_FULL = '02';

    /**
     *
     */
    const MARCH_FULL = '03';

    /**
     *
     */
    const APRIL_FULL = '04';

    /**
     *
     */
    const MAY_FULL = '05';

    /**
     *
     */
    const JUNE_FULL = '06';

    /**
     *
     */
    const JULY_FULL = '07';

    /**
     *
     */
    const AUGUST_FULL = '08';

    /**
     *
     */
    const SEPTEMBER_FULL = '09';

    /**
     *
     */
    const OCTOBER_FULL = 10;

    /**
     *
     */
    const NOVEMBER_FULL = 11;

    /**
     *
     */
    const DECEMBER_FULL = 12;

    /**
     * @var array
     */
    public static $monthShort = [
        self::JANUARY => 'янв.',
        self::FEBRUARY => 'февр.',
        self::MARCH => 'март',
        self::APRIL => 'апр.',
        self::MAY => 'май',
        self::JUNE => 'июнь',
        self::JULY => 'июль',
        self::AUGUST => 'авг.',
        self::SEPTEMBER => 'сент.',
        self::OCTOBER => 'окт.',
        self::NOVEMBER => 'нояб.',
        self::DECEMBER => 'дек.',
    ];

    /**
     * @var array
     */
    public static $monthShortByFull = [
        self::JANUARY_FULL => 'янв.',
        self::FEBRUARY_FULL => 'февр.',
        self::MARCH_FULL => 'март',
        self::APRIL_FULL => 'апр.',
        self::MAY_FULL => 'май',
        self::JUNE_FULL => 'июнь',
        self::JULY_FULL => 'июль',
        self::AUGUST_FULL => 'авг.',
        self::SEPTEMBER_FULL => 'сент.',
        self::OCTOBER_FULL => 'окт.',
        self::NOVEMBER_FULL => 'нояб.',
        self::DECEMBER_FULL => 'дек.',
    ];

    /**
     * @var array
     */
    public static $monthFullRU = [
        self::JANUARY => 'Январь',
        self::FEBRUARY => 'Февраль',
        self::MARCH => 'Март',
        self::APRIL => 'Апрель',
        self::MAY => 'Май',
        self::JUNE => 'Июнь',
        self::JULY => 'Июль',
        self::AUGUST => 'Август',
        self::SEPTEMBER => 'Сентябрь',
        self::OCTOBER => 'Октябрь',
        self::NOVEMBER => 'Ноябрь',
        self::DECEMBER => 'Декабрь',
    ];

    /**
     * @var array
     */
    public static $monthGenitiveRU = [
        self::JANUARY_FULL => 'Января',
        self::FEBRUARY_FULL => 'Февраля',
        self::MARCH_FULL => 'Марта',
        self::APRIL_FULL => 'Апреля',
        self::MAY_FULL => 'Мая',
        self::JUNE_FULL => 'Июня',
        self::JULY_FULL => 'Июля',
        self::AUGUST_FULL => 'Августа',
        self::SEPTEMBER_FULL => 'Сентября',
        self::OCTOBER_FULL => 'Октября',
        self::NOVEMBER_FULL => 'Ноября',
        self::DECEMBER_FULL => 'Декабря',
    ];

    /**
     * @return array
     */
    public static function getLastTwelveMonth()
    {
        $currentMonth = date('n');
        $currentYear = date('Y');
        $data[] = [
            'month' => $currentMonth,
            'year' => $currentYear,
        ];
        for ($i = 1; $i < 12; $i++) {
            $month = $currentMonth -= 1;
            $year = $currentMonth > 0 ? $currentYear : ($currentYear - 1);
            if ($currentMonth < 1) {
                $month = 12 + $currentMonth;
            }
            $data[] = [
                'month' => $month,
                'year' => $year,
            ];
        }
        krsort($data);

        return $data;
    }

    /**
     * Calculate working days in the month of the date
     * @param  DateTime|string|integer $date
     * @return integer
     */
    public static function workDays($date) {
        $count = 0;
        $d = null;
        if ($date instanceof \DateTime) {
            $d = clone $date;
        } elseif (is_string($date)) {
            $d = date_create($date);
        } elseif (is_integer($date)) {
            $d = new DateTime('@' . $date);
        }
        if ($d instanceof \DateTime) {
            $d->modify('first day of this month');
            $m = $d->format('n');
            $w = [6, 7];
            while ($d->format('n') == $m) {
                if (in_array($d->format('N'), $w) == false) {
                    $count++;
                }
                $d->modify('+1 day');
            }
        }

        return $count;
    }
}
