<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 02.10.2018
 * Time: 1:24
 */

namespace common\components\helpers;

use yii\base\Model;
use yii\helpers\ArrayHelper;

/**
 * Class ModelHelper
 * @package common\components\helpers
 */
class ModelHelper
{
    /**
     * @param $model
     */
    public static function HtmlEntitiesModelAttributes($model)
    {
        foreach ($model->attributes as $attributeName => $value) {
            if (!empty($model->$attributeName) &&
                is_string($model->$attributeName) &&
                strip_tags($model->$attributeName) !== $model->$attributeName) {
                $model->$attributeName = htmlspecialchars($value);
            }
        }
    }

    /**
     * example:
     * \common\components\helpers\ModelHelper::logErrors($model, __METHOD__);
     *
     * @param $model
     */
    public static function logErrors(Model $model = null, $method = null, $message = null)
    {
        $logData = ($message !== null ? ['message' => $message] : []) + [
            'method' => $method,
            'model' => get_class($model),
            'attributes' => ArrayHelper::getValue($model, 'attributes'),
            'errors' => ArrayHelper::getValue($model, 'errors'),
        ];

        \Yii::error(\yii\helpers\VarDumper::dumpAsString($logData), 'validation');
    }
}
