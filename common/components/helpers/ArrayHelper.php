<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 6/12/15
 * Time: 5:49 PM
 * Email: t.kanstantsin@gmail.com
 */

namespace common\components\helpers;


/**
 * Class ArrayHelper
 * @package common\components\helpers
 */
class ArrayHelper extends \yii\helpers\ArrayHelper
{

    /**
     * @param array $array
     * @param $key
     * @param null $group
     * @return array
     */
    public static function getAssoc($array, $key, $group = null)
    {
        return static::map($array, $key, function ($model) {
            return $model;
        }, $group);
    }

}
