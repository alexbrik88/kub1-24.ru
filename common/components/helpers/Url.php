<?php

namespace common\components\helpers;

use Yii;

/**
 * Url provides a set of static methods for managing URLs.
 */
class Url extends \yii\helpers\BaseUrl
{
    public static function currentArray(array $params = [])
    {
        $currentParams = Yii::$app->getRequest()->getQueryParams();
        $currentParams[0] = '/' . Yii::$app->controller->getRoute();

        return array_replace_recursive($currentParams, $params);
    }
}
