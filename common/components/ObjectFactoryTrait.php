<?php

namespace common\components;

use Yii;

trait ObjectFactoryTrait
{
    /**
     * @return static
     * @throws
     */
    public static function getInstance(): object
    {
        return Yii::createObject(static::class);
    }
}
