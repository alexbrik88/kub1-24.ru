<?php
namespace common\components\log\behaviors;

use common\models\log\Log;
use Yii;
use yii\base\Behavior;
use yii\base\ErrorException;
use yii\helpers\Json;

/**
 * AR that implements this behavior, must implement `ILogMessage` interface
 */
class LogBehavior extends Behavior
{
    /**
     *
     */
    const EVENT_BEFORE_SAVE = 'beforeSave';
    /**
     *
     */
    const EVENT_BEFORE_DELETE = 'beforeDelete';


    /**
     * Model columns, that would be saved into `data` column.
     * Pairs `'data' => 'value'`
     * @var array
     */
    public $columns = [];
    /**
     * Model class name (with namespace)
     * @var
     */
    public $modelName;
    /**
     * Model unique attribute
     * @var string
     */
    public $modelId = 'id';
    /**
     * @var array
     */
    public $events = [];

    /**
     * @param array $config
     * @throws ErrorException
     */
    public function __construct($config = [])
    {
        parent::__construct($config);

        if (!class_exists($this->modelName) || !$this->owner->hasProperty($this->modelId)) {
            throw new ErrorException('Class `' . $this->modelName . '`` not exists or do not hav property `' . $this->modelId . '`.`');
        }

        foreach ($this->events as $key => $event) {
            if (!isset($event['attribute'], $event['event'], $event['callback'])
                || !($event['callback'] instanceof \Closure)
            ) {
                unset($this->events[$key]);
            }
        }
    }

    /**
     * @param $insert
     */
    public function beforeSave($insert)
    {
        $this->checkEvent(self::EVENT_BEFORE_SAVE, $insert);
    }

    /**
     *
     */
    public function beforeDelete()
    {
        $this->checkEvent(self::EVENT_BEFORE_DELETE);
    }

    /**
     * @param string $eventType
     * @param bool $insert
     */
    protected function checkEvent($eventType, $insert = false)
    {
        foreach ($this->events as $event) {
            if ($event['event'] == $eventType
                && $this->owner->hasProperty($event['attribute'])
            ) {
                $event['attribute']($this->owner, $event['attribute']);
            }
        }
    }

    /**
     *
     */
    protected function createLog()
    {
        $log = new Log();
        $log->author_id = Yii::$app->user->id;
        $log->company_id = Yii::$app->user->identity->company->id;
        $log->model_name = $this->modelName;
        $log->model_id = $this->owner->{$this->modelId};
        $log->data = Json::encode($this->columns);

        $log->save();
    }
}
