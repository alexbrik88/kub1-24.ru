<?php
/**
 * Created by konstantin.
 * Date: 28.7.15
 * Time: 12.46
 */

namespace common\components\filters;


use yii\web\ForbiddenHttpException;
use yii\web\User;

/**
 * Class AccessControl
 * @package common\components\filters
 */
class AccessControl extends \yii\filters\AccessControl
{

    /**
     * Denies the access of the user.
     * The default implementation will redirect the user to the login page if he is a guest;
     * if the user is already logged, a 403 HTTP exception will be thrown.
     * @param User $user the current user
     * @throws ForbiddenHttpException if the user is already logged in.
     */
    protected function denyAccess($user)
    {
        if ($user !== false && $user->getIsGuest()) {
            $user->loginRequired();
        } else {
            throw new ForbiddenHttpException('У Вас нет прав на данное действие.');
        }
    }

}