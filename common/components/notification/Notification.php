<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 24.11.2018
 * Time: 1:42
 */

namespace common\components\notification;


use common\components\helpers\Html;
use lavrentiev\widgets\toastr\NotificationBase;

/**
 * Class Notification
 * @package common\components\notification
 */
class Notification extends NotificationBase
{
    /**
     * @inheritdoc
     */
    public function run()
    {
        $this->message = Html::decode($this->message);
        if (in_array($this->type, $this->types)){
            return $this->view->registerJs("toastr.{$this->type}(\"{$this->message}\", \"{$this->title}\", {$this->options});");
        }

        return $this->view->registerJs("toastr.{$this->typeDefault}(\"{$this->message}\", \"{$this->title}\", {$this->options});");
    }
}