<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 10.08.2018
 * Time: 17:21
 */

namespace common\components\notification;


use lavrentiev\widgets\toastr\NotificationBase;
use yii\helpers\Html;
use yii\helpers\Json;
use common\components\notification\Notification;

/**
 * Class NotificationFlash
 * @package common\components\notification
 */
class NotificationFlash extends NotificationBase
{
    /** @var object $session */
    private $session;

    /**
     * @return string|void
     * @throws \Exception
     */
    public function run()
    {
        $this->session = \Yii::$app->session;

        $flashes = $this->session->getAllFlashes();

        foreach ($flashes as $type => $data) {
            $data = (array) $data;

            foreach ($data as $i => $message) {
                $message = str_replace('"', '\"', $message);
                Notification::widget([
                    'type' => Html::encode($type),
                    'message' => Html::decode($message),
                    'options' => Json::decode((string) $this->options),
                ]);
            }

            $this->session->removeFlash($type);
        }
    }
}