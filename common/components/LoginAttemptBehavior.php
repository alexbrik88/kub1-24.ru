<?php

namespace common\components;

use common\models\LoginAttempt;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Model;


class LoginAttemptBehavior extends \yii\base\Behavior
{
    public $useKey = true;
    public $useIp = true;
    public $attempts = 3;
    public $duration = 300;
    public $disableDuration = 900;
    public $usernameAttribute = 'username';
    public $passwordAttribute = 'password';
    public $message = 'You have exceeded the maximum number of password attempts.';

    private $_attempt;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        if (!$this->useKey && !$this->useIp) {
            throw new InvalidConfigException('The "useKey" or "useIp" property must be set as "true".');
        }
    }

    public function events()
    {
        return [
            Model::EVENT_BEFORE_VALIDATE => 'beforeValidate',
            Model::EVENT_AFTER_VALIDATE => 'afterValidate',
        ];
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        $this->_attempt = LoginAttempt::find()->where($this->condition)->andWhere(['>', 'reset_at', time()])->one();

        if ($this->_attempt !== null && $this->_attempt->amount >= $this->attempts) {
            $disableDuration = ceil($this->disableDuration / 60) * 60;
            $message = Yii::$app->getI18n()->format($this->message, [
                'disableDuration' => Yii::$app->formatter->asDuration($disableDuration),
            ], Yii::$app->language);
            $this->owner->addError($this->usernameAttribute, $message);
        }
    }

    /**
     * @inheritdoc
     */
    public function afterValidate()
    {
        if ($this->owner->hasErrors($this->passwordAttribute)) {
            if (!$this->_attempt) {
                $this->_attempt = new LoginAttempt($this->condition);
            }
            $this->_attempt->amount += 1;
            if ($this->_attempt->amount >= $this->attempts) {
                $this->_attempt->reset_at = time() + $this->disableDuration;
            } else {
                $this->_attempt->reset_at = time() + $this->duration;
            }
            $this->_attempt->save();
        }
    }

    /**
     * @return string
     */
    public function getCondition()
    {
        $condition = [
            'key' => (string) ($this->useKey ? $this->key : ''),
            'ip' => (string) ($this->useIp ? $this->ip : ''),
        ];

        return $condition;
    }

    /**
     * @return string
     */
    public function getKey()
    {
        return (string) sha1($this->owner->{$this->usernameAttribute});
    }

    /**
     * @return string|null
     */
    public function getIp()
    {
        return (string) Yii::$app->request->userIP;
    }
}
