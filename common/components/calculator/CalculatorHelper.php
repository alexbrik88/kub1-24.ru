<?php

namespace common\components\calculator;

use common\models\TaxRate;

class CalculatorHelper
{

    /**
     * @param $price
     * @param $taxRateId
     *
     * @return string
     */
    public static function getPriceWithoutNds($price, $taxRateId, $precision = 0)
    {
        $priceWithoutNds = $price;
        if ($taxRateId != TaxRate::RATE_WITHOUT) {
            /** @var TaxRate $taxRate */
            $taxRate = TaxRate::get($taxRateId);
            $priceWithoutNds = round($priceWithoutNds / (1 + round($taxRate->rate, 2)), $precision);
        }

        return $priceWithoutNds;
    }

    public static function getPriceWithNds($price, $taxRateId, $precision = 0)
    {
        $priceWithoutNds = $price;
        if ($taxRateId != TaxRate::RATE_WITHOUT) {
            /** @var TaxRate $taxRate */
            $taxRate = TaxRate::get($taxRateId);
            $priceWithoutNds = round($priceWithoutNds * (1 + round($taxRate->rate, 2)), $precision);
        }

        return $priceWithoutNds;
    }
}