<?php

namespace common\components;

use common\models\EmployeeCompany;

/**
 * Interface SendDocument
 */
interface SendDocument
{

    /**
     * Returns email subject.
     * @return string
     */
    public function getEmailSubject();

    /**
     * @param  EmployeeCompany $sender
     * @param  string|array    $toEmail
     * @param  string          $emailText
     * @param  string          $subject
     * @return yii\swiftmailer\Message
     */
    public function getMailerMessage(EmployeeCompany $sender, $toEmail, $emailText = null, $subject = null);

    /**
     * Returns email send status.
     * @param  EmployeeCompany $sender
     * @param  string|array    $toEmail
     * @param  string          $emailText
     * @param  string          $subject
     * @return boolean
     */
    public function sendAsEmail(EmployeeCompany $sender, $toEmail, $emailText = null, $subject = null);
}
