<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 1/4/16
 * Time: 11:46 AM
 * Email: t.kanstantsin@gmail.com
 */

namespace common\components\grid;

use common\components\helpers\Html;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQueryInterface;
use yii\helpers\Inflector;

/**
 * Class DataColumn
 *
 * NOTE: that attribute `attr_name` MUST be exists in the `Search` class.
 *      If there are relation - you must create additional column to handle
 *      attribute values from request.
 *
 * @package common\components\grid
 */
class DataColumn extends \yii\grid\DataColumn
{
    /**
     * Header cell options for sorting column.
     * @var array
     */
    public $sortHeaderCellOptions = [
        'class' => 'sorting',
    ];

    /**
     * Header cell options for filter column.
     * @var array
     */
    public $filterHeaderCellOptions = [
        'class' => 'dropdown-filter',
    ];

    /**
     * @inheritdoc
     */
    public function renderHeaderCell()
    {
        if ($this->attribute !== null && $this->enableSorting
            && ($sort = $this->grid->dataProvider->getSort()) !== false
            && $sort->hasAttribute($this->attribute)
        ) { // sorting enabled
            Html::addCssClass($this->headerOptions, $this->sortHeaderCellOptions['class']);
        }
        return parent::renderHeaderCell();
    }

    /**
     * @inheritdoc
     */
    public function renderFilterCell()
    {
        Html::addCssClass($this->headerOptions, $this->filterHeaderCellOptions['class']);

        return Html::tag('th', $this->renderFilterCellContent(), $this->headerOptions);
    }

    /**
     * @inheritdoc
     */
    public function renderFilterCellContent()
    {
        $activeValue = $this->grid->filterModel->{$this->attribute};

        $items = [];
        foreach ($this->filter as $key => $name) {
            $a = Html::a($name, '#', [
                'role' => 'menuitem',
                'tabindex' => -1,
                'data-value' => $key,
            ]);

            $items[] = Html::tag('li', $a, [
                'class' => $activeValue == $key ? 'active' : '',
            ]);
        }

        $dropDownUL = Html::tag('ul', join('', $items), [
            'class' => 'dropdown-menu',
            'rowl' => 'menu',
        ]);
        // Hidden input to store value of current input
        $input = Html::activeHiddenInput($this->grid->filterModel, $this->attribute);
        $toggle = Html::tag('div', $this->getHeaderCellLabel(), [
            'class' => 'dropdown-toggle',
            'data' => [
                'toggle' => 'dropdown',
            ],
        ]);
        $dropDown = Html::tag('div', $toggle . $dropDownUL . $input, ['class' => 'dropdown',]);

        $filterIcon = Html::tag('i', '', [
            'class' => 'fa fa-filter pull-right' . (!empty($activeValue) ? ' active' : ''),
        ]);

        $this->grid->getView()->registerJs(<<<JS
            $('#{$this->grid->id} th.dropdown-filter li a').click(function() {
                var hiddenInput = $(this).closest('.dropdown').find('input');
                hiddenInput.val($(this).data('value'));
                $('#{$this->grid->id}').yiiGridView('applyFilter');
                return false;
            });
JS
        );

        return $filterIcon . $dropDown;
    }

    /**
     * Generates label for table header.
     * @todo: write pull request.
     * @return string
     */
    public function getHeaderCellLabel()
    {
        $provider = $this->grid->dataProvider;

        if ($this->label === null) {
            if ($provider instanceof ActiveDataProvider && $provider->query instanceof ActiveQueryInterface) {
                /* @var $model Model */
                $model = new $provider->query->modelClass;
                $label = $model->getAttributeLabel($this->attribute);
            } else {
                $models = $provider->getModels();
                if (($model = reset($models)) instanceof Model) {
                    /* @var $model Model */
                    $label = $model->getAttributeLabel($this->attribute);
                } else {
                    $label = Inflector::camel2words($this->attribute);
                }
            }
        } else {
            $label = $this->label;
        }

        return $label;
    }

    private $_total = 0;

    public function getDataCellValue($model, $key, $index)
    {
        $value = parent::getDataCellValue($model, $key, $index);
        if (is_integer($value) || is_float($value)) {
            $this->_total += $value;
        }
        return $value;
    }

    protected function renderFooterCellContent()
    {
    	if($this->footer) { $this->_total = $this->footer; }
        return $this->grid->formatter->format($this->_total, $this->format);;
    }
}
