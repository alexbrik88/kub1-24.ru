<?php

namespace common\components\grid;

use yii\base\InvalidConfigException;
use yii\bootstrap4\Html;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;

/**
 * KubLinkPager
 */
class KubLinkPager extends \yii\bootstrap4\LinkPager
{
    /**
     * @var array HTML attributes for the pager list tag.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $listOptions = ['class' => ['nav-pagination list-clr']];
    /**
     * @var array HTML attributes which will be applied to all link containers
     */
    public $linkContainerOptions = [];
    /**
     * @var array HTML attributes for the link in a pager container tag.
     * @see \yii\helpers\Html::renderTagAttributes() for details on how attributes are being rendered.
     */
    public $linkOptions = [];
    /**
     * @var int maximum number of page buttons that can be displayed. Defaults to 10.
     */
    public $maxButtonCount = 4;


    /**
     * Renders the page buttons.
     * @return string the rendering result
     */
    protected function renderPageButtons()
    {
        $pageCount = $this->pagination->getPageCount();
        if ($pageCount < 2 && $this->hideOnSinglePage) {
            return '';
        }

        $buttons = [];
        $currentPage = $this->pagination->getPage();
        $lastPage = $pageCount - 1;
        list($beginPage, $endPage) = $this->getPageRange();

        $this->firstPageLabel = $beginPage > 0;
        $this->prevPageLabel = $beginPage > 1 ? '...' : false;

        $this->lastPageLabel = $endPage < $lastPage;
        $this->nextPageLabel = ($lastPage - $endPage) > 1 ? '...' : false;

        // first page
        $firstPageLabel = $this->firstPageLabel === true ? '1' : $this->firstPageLabel;
        if ($firstPageLabel !== false) {
            $buttons[] = $this->renderPageButton(
                $firstPageLabel,
                0,
                $this->firstPageCssClass,
                $currentPage <= 0,
                false
            );
        }

        // prev page
        if ($this->prevPageLabel !== false) {
            if (($page = $beginPage - 1) < 0) {
                $page = 0;
            }
            $buttons[] = $this->renderPageButton(
                $this->prevPageLabel,
                $page,
                $this->prevPageCssClass,
                $beginPage <= 0,
                false
            );
        }

        // internal pages
        for ($i = $beginPage; $i <= $endPage; ++$i) {
            $buttons[] = $this->renderPageButton(
                $i + 1,
                $i,
                null,
                $this->disableCurrentPageButton && $i == $currentPage,
                $i == $currentPage
            );
        }

        // next page
        if ($this->nextPageLabel !== false) {
            if (($page = $endPage + 1) >= $pageCount - 1) {
                $page = $pageCount - 1;
            }
            $buttons[] = $this->renderPageButton(
                $this->nextPageLabel,
                $page,
                $this->nextPageCssClass,
                $endPage >= $pageCount - 1,
                false
            );
        }

        // last page
        $lastPageLabel = $this->lastPageLabel === true ? $pageCount : $this->lastPageLabel;
        if ($lastPageLabel !== false) {
            $buttons[] = $this->renderPageButton(
                $lastPageLabel,
                $pageCount - 1,
                $this->lastPageCssClass,
                $currentPage >= $pageCount - 1,
                false
            );
        }

        $options = $this->listOptions;
        $tag = ArrayHelper::remove($options, 'tag', 'ul');
        return Html::tag($tag, implode("\n", $buttons), $options);
    }

    /**
     * @return array the begin and end pages that need to be displayed.
     */
    protected function getPageRange()
    {
        $maxCount = (int) $this->maxButtonCount;
        $lastPage = max(0, $this->pagination->getPageCount() - 1);
        if ($lastPage <= $maxCount) {
            return [0, $lastPage];
        }

        $currentPage = $this->pagination->getPage();

        if ($currentPage <= 0) {
            return [0, $maxCount];
        }

        $prev = ($currentPage - 1) % $maxCount;
        $next = $maxCount - $prev - 1;

        $beginPage = max(0, $currentPage - $prev);
        $endPage = min($lastPage, $currentPage + $next);

        if ($endPage == $lastPage && ($endPage - $beginPage) < $maxCount) {
            $beginPage = max(0, $endPage - $maxCount);
        }

        return [$beginPage, $endPage];
    }
}
