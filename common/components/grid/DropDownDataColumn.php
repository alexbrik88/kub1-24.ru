<?php

namespace common\components\grid;

use Yii;
use yii\helpers\Html;

/**
 * Class DataColumn
 *
 * @todo: replace all usages of this class for new DataColumn class.
 *
 * NOTE: that attribute `attr_name` MUST be exists in the `Search` class.
 *      If there are relation - you must create additional column to handle
 *      attribute values from request.
 *
 * @package frontend\components
 */
class DropDownDataColumn extends \yii\grid\DataColumn
{
    public $dropdownOptions = [];
    public $enableSorting = false;

    /**
     * @inheritdoc
     */
    public function renderFilterCell()
    {
        // Whether current filter is active
        $activeValue = $this->grid->filterModel->{$this->attribute};
        $filterIsActive = !empty($activeValue);

        $items = [];
        foreach ($this->filter as $key => $name) {
            $a = Html::a($name, '#', [
                'role' => 'menuitem',
                'tabindex' => -1,
                'data-value' => $key,
                'title' => $name,
            ]);

            $items[] = Html::tag('li', $a, [
                'class' => $activeValue == $key ? 'active' : '',
            ]);
        }

        Html::addCssClass($this->dropdownOptions, 'dropdown-menu dropdown-menu-bill dropdown-list');
        $dropDownUL = Html::tag('ul', join('', $items), $this->dropdownOptions);

        // Hidden input to store value of current input
        $input = Html::activeHiddenInput($this->grid->filterModel, $this->attribute);

        $icon = Html::tag('i', '', [
            'class' => 'fa fa-filter' . ($filterIsActive ? ' active' : ''),
        ]);
        $iconCell = Html::tag('div', $icon, [
            'class' => 'data-filter-icon-cell',
        ]);
        $toggle = Html::tag('div', $this->renderHeaderCellContent() . $iconCell, [
            'class' => 'dropdown-toggle',
            'data' => [
                'toggle' => 'dropdown',
            ],
        ]);

        $dropDown = '<div class="dropdown">' . $toggle . $dropDownUL . $input . '</div>';


        Html::addCssClass($this->headerOptions, 'dropdown-filter');
        $filterCell = Html::tag('th', $dropDown, $this->headerOptions);

        // Ставим singleton для того, чтобы не регистрировать один и тот же скрипт несколько раз,
        // но при этом не регистрировать его вовсе, если данный столбец не используется
        static $isAddedJs = false;
        if (!$isAddedJs) {
            $gridId = $this->grid->options['id'];
            $this->grid->getView()->registerJs('
                $("#' . $gridId . ' th.dropdown-filter li a").click(function() {
                    $hiddenInput = $(this).closest(".dropdown").find("input");
                    $hiddenInput.val($(this).data("value"));
                    $("#' . $gridId . '").yiiGridView("applyFilter");
                    return false;
                });
            ');

            $isAddedJs = true;
        }

        return $filterCell;
    }
}
