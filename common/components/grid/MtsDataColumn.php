<?php

namespace common\components\grid;

use frontend\themes\mts\components\Icon;
use kartik\select2\Select2;
use yii\base\Model;
use yii\helpers\Html;
use yii\web\JsExpression;

/**
 * Class MtsDataColumn
 *
 * NOTE: that attribute `attr_name` MUST be exists in the `Search` class.
 *      If there are relation - you must create additional column to handle
 *      attribute values from request.
 *
 * @package common\components\grid
 */
class MtsDataColumn extends \yii\grid\DataColumn
{
    public $selectPluginOptions = [];
    public $selectWidgetOptions = [];
    public $selectIdSuffix = '--filter';
    public $selectId;
    public $hideSearch = true;
    public $s2containerCssClass;
    public $s2width = 'auto'; // Dropdown width

    /**
     * {@inheritdoc}
     */
    protected function renderHeaderCellContent()
    {
        if ($this->header !== null || $this->label === null && $this->attribute === null) {
            return parent::renderHeaderCellContent();
        }

        $label = $this->getHeaderCellLabel();
        if ($this->encodeLabel) {
            $label = Html::encode($label);
        }
        if ($this->filter) {
            $this->enableSorting = false;
        }

        if ($this->attribute !== null &&
            $this->enableSorting &&
            ($sort = $this->grid->dataProvider->getSort()) !== false &&
            $sort->hasAttribute($this->attribute)
        ) {
            Html::addCssClass($this->sortLinkOptions, 'th-title-name');
            $link = $sort->link($this->attribute, array_merge($this->sortLinkOptions, ['label' => $label]));
            $btns = Html::beginTag('span', [
                'class' => 'th-title-btns',
            ]);
            $btns .= Html::a(Icon::get('count-arrow', [
                'class' => 'th-title-icon th-title-icon_reverse',
            ]), $sort->createUrl($this->attribute), [
                'class' => 'th-title-btn icon_asc button-clr',
            ]);
            $btns .= Html::a(Icon::get('count-arrow', [
                'class' => 'th-title-icon',
            ]), $sort->createUrl($this->attribute), [
                'class' => 'th-title-btn icon_desc button-clr',
            ]);
            $btns .= Html::endTag('span');
            $class = 'th-title';
            if (($direction = $sort->getAttributeOrder($this->attribute)) !== null) {
                $class .= $direction === SORT_DESC ? ' sort_desc' : ' sort_asc';
            }
            return Html::tag('div', $link . $btns, [
                'class' => $class,
            ]);
        }

        return $label;
    }

    /**
     * @inheritdoc
     */
    public function renderFilterCell()
    {
        Html::addCssClass($this->headerOptions, 'dropdown-filter');

        return Html::tag('th', $this->renderFilterCellContent(), $this->headerOptions);
    }

    /**
     * @inheritdoc
     */
    public function renderFilterCellContent()
    {
        // Whether current filter is active
        $activeValue = $this->grid->filterModel->{$this->attribute};
        $isActive = !empty($activeValue);

        $inputId = Html::getInputId($this->grid->filterModel, $this->attribute);
        $selectId = $this->selectId ? : $inputId . $this->selectIdSuffix;
        $toggleId = $inputId . '-toggle';

        $pluginOptions = array_merge([
            'width' => $this->s2width,
            'dropdownCssClass' => 'dropdown-search-data',
            'templateResult' => new JsExpression('function(data, container) {
                return data.text;
            }'),
        ], $this->selectPluginOptions);
        $containerCssClass = array_merge(['select2-container--grid-filter'], (array) $this->s2containerCssClass);

        if (empty($this->selectWidgetOptions['id'])) {
            $this->selectWidgetOptions['id'] = $selectId;
        }

        $filterSearch = Select2::widget([
            'model' => $this->grid->filterModel,
            'attribute' => $this->attribute,
            'data' => $this->filter,
            'hideSearch' => $this->hideSearch,
            'options' => $this->selectWidgetOptions,
            'pluginOptions' => $pluginOptions,
            'theme' => Select2::THEME_KRAJEE_BS4 . ' ' . implode(' ', $containerCssClass),
        ]);

        $btns = Html::beginTag('span', [
            'class' => 'th-title-btns',
        ]);
        $btns .= Html::button(Icon::get('filter', [
            'class' => 'th-title-icon-filter',
        ]), [
            'class' => 'th-title-btn button-clr',
        ]);
        $btns .= Html::tag('div', $filterSearch, [
            'class' => 'filter-select2-select-container',
        ]);
        $btns .= Html::endTag('span');
        $label = Html::tag('span', $this->renderHeaderCellContent(), [
            'class' => 'th-title-name',
        ]);
        $toggleButton = Html::tag('div', $label . $btns, [
            'id' => $toggleId,
            'class' => 'th-title filter filter-open' . ($isActive ? ' active' : ''),
        ]);

        $this->grid->getView()->registerJs('
            $(document).on("click", "#' . $toggleId . '", function (e) {
                e.preventDefault();
                if (!$("#' . $selectId . '").data("select2").isOpen()) {
                    $("#' . $selectId . '").select2("open");
                }
                return false;
            });
        ');

        return $toggleButton;
    }
}
