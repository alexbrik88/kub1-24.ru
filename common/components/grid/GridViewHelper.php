<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 29.08.2016
 * Time: 7:44
 */

namespace common\components\grid;


use common\models\company\CompanySearch;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * Class GridViewHelper
 * @package frontend\components\helpers
 */
class GridViewHelper
{
    /**
     * @param $className
     * @param $selectedPageSize
     * @param array $url
     * @return string
     */
    public static function getPagination($className, $selectedPageSize, $url = ['index'])
    {
        $pagination = '';
        foreach ([10, 20, 50, 100,] as $value) {
            $class = $selectedPageSize == $value ? 'active' : '';
            $pagination .= '<li class="' . $class . '">' . Html::a($value, Url::to(ArrayHelper::merge($url, [
                    $className => [
                        'pageSize' => $value,
                    ]
                ]))) . '</li>';
        }

        return $pagination;
    }

    /**
     * @param array $buttonOptions
     * @return string
     */
    public static function getAddButton($buttonOptions)
    {
        $addButton = Html::beginTag('table', [
                'class' => 'add-square-btn-table',
            ]) .
            Html::beginTag('tr', [
                'class' => 'button-add-line',
                'role' => 'row',
            ]) .
            Html::beginTag('td', [
                'class' => 'bord-right',
            ]) .
            Html::beginTag('div', [
                'class' => 'portlet pull-left control-panel button-width-table',
            ]) .
            Html::beginTag('div', [
                'class' => 'btn-group pull-right',
            ]) .
            Html::beginTag('span', $buttonOptions) .
            Html::beginTag('i', [
                'class' => 'pull-left fa icon fa-plus-circle',
            ]) .
            Html::endTag('i') . Html::endTag('span') . Html::endTag('div') . Html::endTag('div') . Html::endTag('td') .
            Html::beginTag('td', [
                'colspan' => 10,
            ]) . Html::endTag('td') . Html::endTag('tr') . Html::endTag('table');

        return $addButton;
    }
}