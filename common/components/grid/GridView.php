<?php

namespace common\components\grid;

use common\models\employee\Employee;
use Yii;
use yii\grid\GridViewAsset;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\helpers\Url;

/**
 * Class GridView
 * @package common\components\grid
 */
class GridView extends \yii\grid\GridView
{
    /** @var string[] */
    private const DATA_COLUMN_TYPES = [
        'mts' => MtsDataColumn::class,
        'kub' => KubDataColumn::class,
        'mobile' => KubDataColumn::class,
    ];

    /** @var string[] */
    private const LINK_PAGER_TYPES = [
        'mts' => MtsLinkPager::class,
        'kub' => KubLinkPager::class,
        'mobile' => KubLinkPager::class,
    ];

    /**
     * @var array
     */
    public $perPageButtons = [

        [
            'title' => '10',
            'value' => 10,
        ],
        [
            'title' => '20',
            'value' => 20,
        ],
        [
            'title' => '50',
            'value' => 50,
        ],
        [
            'title' => 'Все',
            'value' => -1,
        ],

    ];

    /**
     * @var int
     */
    public $per_page = 10;

    /**
     * @var string|null
     */
    public $configAttribute;

    /**
     * Runs the widget.
     */
    public function run()
    {
        $this->applyConfigAttribute();
        $id = $this->options['id'];
        $options = Json::htmlEncode($this->getClientOptions());
        $view = $this->getView();
        GridViewAsset::register($view);
        $view->registerJs("jQuery('#$id').yiiGridView($options);");
        parent::run();
    }

    /**
     * @inheritDoc
     */
    public function init()
    {
        $theme = basename(ArrayHelper::getValue(Yii::$app, 'view.theme.baseUrl'));

        if (!isset($this->dataColumnClass) && isset(self::DATA_COLUMN_TYPES[$theme])) {
            $this->dataColumnClass = self::DATA_COLUMN_TYPES[$theme];
        }

        if (!isset($this->pager['class']) && isset(self::LINK_PAGER_TYPES[$theme])) {
            $this->pager['class'] = self::LINK_PAGER_TYPES[$theme];
        }

        parent::init();
    }

    /**
     * Renders the table header.
     * @return string the rendering result.
     */
    public function renderTableHeader()
    {
        $cells = [];
        foreach ($this->columns as $key => $column) {
            /* @var $column DataColumn */
            if (empty($column->filter) || is_string($column->filter)) {
                if (isset($column->headerOptions['type']) && $column->headerOptions['type'] == 'header') {
                    $j = 0;
                    if (isset($column->headerOptions['colspan'])) {
                        $j = $column->headerOptions['colspan'] + 1;
                    }
                    $cells[] = Html::tag('th', $column->label, $column->headerOptions);
                    unset($this->columns[$key]);
                    for ($i = $key + 1; $i < $key + $j; $i++) {
                        $newTr[$i] = $this->columns[$i];
                    }
                } else {
                    if (isset($newTr)) {
                        if (!array_key_exists($key, $newTr)) {
                            $cells[] = $column->renderHeaderCell();
                        }
                    } else {
                        $cells[] = $column->renderHeaderCell();
                    }
                }
            } else {
                $cells[] = $column->renderFilterCell();
            }
        }

        $content = Html::tag('tr', implode('', $cells), array_merge($this->filterRowOptions, $this->headerRowOptions));
        if (isset($newTr)) {
            $cells = [];
            foreach ($newTr as $column) {
                if (empty($column->filter) || is_string($column->filter)) {
                    $cells[] = $column->renderHeaderCell();
                } else {
                    $cells[] = $column->renderFilterCell();
                }
            }
            $content .= Html::tag('tr', implode('', $cells), array_merge($this->filterRowOptions, $this->headerRowOptions));
        }

        return "<thead>\n" . $content . "\n</thead>";
    }

    /**
     * @param string $name
     * @return bool|string
     */
    public function renderSection($name)
    {
        switch ($name) {
            case '{sizer}':
                return $this->renderSizer();
            default:
                return parent::renderSection($name);
        }
    }

    /**
     * Render the page sizer
     * @return string the rendering result
     */
    public function renderSizer()
    {
        $header = '<span class="pagination-label">Выводить по количеству строк:</span> <ul class="pagination ">';
        $content = '';
        foreach ($this->perPageButtons as $item) {
            if ($item['value'] == $this->per_page) {
                $class = 'active';
            } else {
                $class = '';
            }

            $content .= "<li class='$class'><a href=\"?per-page=$item[value]\">$item[title]</a></li>";
        }
        $footer = '</ul>';

        return Html::tag('div', $header . $content . $footer);
    }

    /**
     * Returns the options for the grid view JS widget.
     * @return array the options
     */
    protected function getClientOptions()
    {
        $filterUrl = isset($this->filterUrl) ? $this->filterUrl : Yii::$app->request->url;
        $id = $this->filterRowOptions['id'];
        $filterSelector = "#$id input:hidden, #$id select";
        if (isset($this->filterSelector)) {
            $filterSelector .= ', ' . $this->filterSelector;
        }

        return [
            'filterUrl' => Url::to($filterUrl),
            'filterSelector' => $filterSelector,
        ];
    }

    /**
     * @return void
     */
    private function applyConfigAttribute(): void
    {
        /** @var Employee $employee */
        $employee = Yii::$app->user->identity;

        if ($this->configAttribute && $employee) {
            $config = $employee->config;

            if ($config->hasAttribute($this->configAttribute) && $config->getAttribute($this->configAttribute)) {
                $class = $config->getTableViewClass($this->configAttribute);

                Html::addCssClass($this->tableOptions, $class);
            }
        }
    }
}
