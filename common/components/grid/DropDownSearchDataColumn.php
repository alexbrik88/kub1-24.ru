<?php

namespace common\components\grid;

use kartik\select2\Select2;
use Yii;
use yii\helpers\Html;
use yii\web\JsExpression;

/**
 * Class DataColumn
 *
 * @todo: replace all usages of this class for new DataColumn class.
 *
 * NOTE: that attribute `attr_name` MUST be exists in the `Search` class.
 *      If there are relation - you must create additional column to handle
 *      attribute values from request.
 *
 * @package frontend\components
 */
class DropDownSearchDataColumn extends \yii\grid\DataColumn
{
    public $selectPluginOptions = [];
    public $selectWidgetOptions = [];
    public $enableSorting = false;
    public $hideSearch = false;

    /**
     * @inheritdoc
     */
    public function renderFilterCell()
    {
        // Whether current filter is active
        $activeValue = $this->grid->filterModel->{$this->attribute};
        $filterIsActive = !in_array($activeValue, ['', null], true);

        $inputId = Html::getInputId($this->grid->filterModel, $this->attribute);
        $toggleId = $inputId . '-toggle';

        $pluginOptions = array_merge([
            'dropdownCssClass' => 'dropdown-search-data',
            'templateResult' => new JsExpression('function(data, container) {
                return data.text;
            }'),
        ], $this->selectPluginOptions);

        $filterSearch = Select2::widget([
            'model' => $this->grid->filterModel,
            'attribute' => $this->attribute,
            'data' => $this->filter,
            'hideSearch' => $this->hideSearch,
            'options' => $this->selectWidgetOptions,
            'pluginOptions' => $pluginOptions,
        ]);

        $filterSearchContainer = Html::tag('div', $filterSearch, [
            'class' => 'filter-select2',
            'style' => 'margin: 0; padding: 0; height: 0; overflow: hidden;'
        ]);

        $icon = Html::tag('i', '', [
            'class' => 'fa fa-filter' . ($filterIsActive ? ' active' : ''),
        ]);
        $iconCell = Html::tag('div', $icon, [
            'class' => 'data-filter-icon-cell',
        ]);
        $toggleButton = Html::tag('div', $this->renderHeaderCellContent() . $iconCell, [
            'id' => $toggleId,
            'class' => 'dropdown-search-toggle',
        ]);

        $dropDown = '<div class="dropdown-search open">' . $toggleButton . $filterSearchContainer . '</div>';

        Html::addCssClass($this->headerOptions, 'dropdown-filter');

        $filterCell = Html::tag('th', $dropDown, $this->headerOptions);

        $this->grid->getView()->registerJs('
            $(document).on("click", "#' . $toggleId . '", function (e) {
                e.preventDefault();
                if (!$("#' . $inputId . '").data("select2").isOpen()) {
                    $("#' . $inputId . '").select2("open");
                }
            });
            //$(document).on("select2:open", "#' . $inputId . '", function (e) {
            //    $("span.select2-dropdown")[0].style.setProperty("border", "1px solid #4276a4", "important");
            //});
        ');

        return $filterCell;
    }
}
