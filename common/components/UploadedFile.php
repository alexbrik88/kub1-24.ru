<?php

namespace common\components;

class UploadedFile extends \yii\web\UploadedFile
{
    public function saveAs($file, $deleteTempFile = true)
    {
        if ($this->error == UPLOAD_ERR_OK) {
            if (is_uploaded_file($this->tempName) && $deleteTempFile) {
                return rename($this->tempName, $file);
            } else {
                return copy($this->tempName, $file);
            }
        }

        return false;
    }
}
