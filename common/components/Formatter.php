<?php
namespace common\components;

use Yii;

/**
 * Class Formatter
 */
class Formatter extends \yii\i18n\Formatter
{
    public function asMoney($value)
    {
        if ($value === null) {
            return $this->nullDisplay;
        }
        $value = $this->normalizeNumericValue($value);

        return $this->asDecimal($value / 100, 2);
    }

    public function asNumber($value, int $decimals = null)
    {
        if ($value === null) {
            return $this->nullDisplay;
        }
        $value = $this->normalizeNumericValue($value);
        if ($decimals === null) {
            $decimals = strlen(substr(strrchr($value, "."), 1));
        }

        return $this->asDecimal($value, $decimals);
    }
}
