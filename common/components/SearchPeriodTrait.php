<?php

namespace common\components;

use frontend\components\StatisticPeriod;

trait SearchPeriodTrait
{
    protected $_sessionPeriod;
    protected $_periodFrom;
    protected $_periodTo;

    /**
     * @param string $value
     */
    public function setPeriodFrom($value)
    {
        $this->_periodFrom = $value;
    }

    /**
     * @param string $value
     */
    public function setPeriodTo($value)
    {
        $this->_periodTo = $value;
    }

    /**
     * @return string
     */
    public function getSessionPeriod()
    {
        if ($this->_sessionPeriod === null) {
            $this->_sessionPeriod = StatisticPeriod::getSessionPeriod();
        }

        return $this->_sessionPeriod;
    }

    /**
     * @return string
     */
    public function getPeriodFrom()
    {
        if ($this->_periodFrom === null) {
            $period = $this->getSessionPeriod();
            $this->_periodFrom = $period['from'];
        }

        return $this->_periodFrom;
    }

    /**
     * @return string
     */
    public function getPeriodTo()
    {
        if ($this->_periodTo === null) {
            $period = $this->getSessionPeriod();
            $this->_periodTo = $period['to'];
        }

        return $this->_periodTo;
    }
}
