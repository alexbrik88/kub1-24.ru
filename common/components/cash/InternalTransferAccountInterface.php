<?php

namespace common\components\cash;

/**
 * Интерфейс перевода между своими счетами
 */
interface InternalTransferAccountInterface
{
    public function getCashContractorTypeText() : string;
    public function getInternalTransferId() : string;
    public function getInternalTransferName() : ?string;
    public function getCurrencyId() : ?string;
    public function getCurrencyName() : ?string;
    public function getCashFlowClass() : string;
    public function getCashFlowTable() : string;
    public function getCashFlowNewModel() : InternalTransferFlowInterface;
}
