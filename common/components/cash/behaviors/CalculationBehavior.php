<?php
namespace common\components\cash\behaviors;

use common\components\date\DateHelper;
use common\models\cash\CashFlowsBase;
use common\models\cash\CashBankFlows;
use common\models\cash\CashOrderFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\query\CashFlowBase;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\employee\EmployeeRole;
use frontend\components\StatisticPeriod;
use DateInterval;
use DateTime;
use common\modules\acquiring\models\AcquiringOperation;
use Yii;
use yii\base\Behavior;
use yii\base\ErrorException;
use yii\db\Query;

/**
 * @author ErickSkrauch <erickskrauch@yandex.ru>
 *
 * Поведение CalculationBehavior обеспечивает функции получения баланса,
 * суммы прихода и расхода на начало и конец указанного отчётного периода.
 *
 * @property CashFlowsBase $owner Поведение должно цепляться к модели операций с деньгами
 *
 * Геттеры:
 * @property integer $balanceAtBegin
 * @property integer $balanceAtEnd
 * @property integer $periodIncome
 * @property integer $periodExpense
 */
class CalculationBehavior extends Behavior
{
    /**
     * @var CashFlowsBase the owner of this behavior
     */
    public $owner;

    /**
     * @var DateTime с которого (и включая которое) числа нужно считать период
     */
    public $beginAt = NULL;

    /**
     * @var DateTime по какое (и включая которое) число нужно считать период
     */
    public $endAt = NULL;

    /**
     * CalculationBehavior constructor.
     * @param array $config
     */
    public function __construct($config = [])
    {
        parent::__construct($config);
        $a = 10;
    }

    /**
     * @param Company|NULL $company
     * @return CashFlowBase
     */
    public function searchQuery(Company $company = NULL)
    {
        if ($query = $this->owner->getStatisticQuery()) {
            return $query;
        }

        $t1 = $this->owner->tableName();
        $ec = !empty($this->owner->employeeCompany) ? $this->owner->employeeCompany : null;
        $query = $this->owner->find();
        $query->andWhere([
            "$t1.company_id" => $company ? $company->id : Yii::$app->user->identity->company->id
        ]);

        if ($this->owner instanceof CashBankFlows) {
            $bik = $this->owner->bik && $this->owner->bik != 'all' ? $this->owner->bik : null;
            $rs = $this->owner->rs && $this->owner->rs != 'all' ? $this->owner->rs : null;
            $account_id = $this->owner->account_id ? $this->owner->account_id : null;
            if ($bik || $rs || $account_id) {
                $t2 = CheckingAccountant::tableName();
                $query
                    ->leftJoin($t2, "{{{$t1}}}.[[company_id]] = {{{$t2}}}.[[company_id]] AND {{{$t1}}}.[[rs]] = {{{$t2}}}.[[rs]]")
                    ->andFilterWhere([
                        "$t2.bik" => $bik,
                        "$t2.rs" => $rs,
                        "$t2.id" => $account_id
                    ]);
            }
        } elseif ($this->owner instanceof CashOrderFlows) {
            if ($this->owner->cashbox_id && $this->owner->cashbox_id != 'all') {
                $query->andWhere(["$t1.cashbox_id" => $this->owner->cashbox_id]);
            }

            // бухгалтер видит все по своей кассе или только для учета в бухгалтерии
            if ($ec && $ec->employee_role_id == EmployeeRole::ROLE_ACCOUNTANT) {
                $query
                    ->joinWith(['cashbox'], false)
                    ->andWhere([
                        'or',
                        [$t1 . '.is_accounting' => true],
                        ['cashbox.responsible_employee_id' =>  $ec->employee_id],
                    ]);
            }
        } elseif ($this->owner instanceof CashEmoneyFlows) {
            if ($this->owner->emoney_id && $this->owner->emoney_id != 'all') {
                $query->andWhere(["$t1.emoney_id" => $this->owner->emoney_id]);
            }

            // бухгалтер видит только для учета в бухгалтерии
            if ($ec && $ec->employee_role_id == EmployeeRole::ROLE_ACCOUNTANT) {
                $query->andWhere([$t1 . '.is_accounting' => true]);
            }
        }

        $this->setConditions($query);

        return $query;
    }

    /**
     * Пытается понять, какой именно формат даты был передан в этот метод
     *
     * @param $date
     * @return DateTime|null
     */
    protected function processPassedDate($date = NULL)
    {
        if ($date === NULL) {
            return NULL;
        }

        if ($date instanceof DateTime) {
            return $date;
        }

        if (is_string($date)) {
            return DateTime::createFromFormat(DateHelper::FORMAT_USER_DATE, $date);
        }

        return NULL;
    }

    /**
     * Вычисляет баланс от момента начала времён и до указанного $date
     * для счётов компании текущего пользователя
     *
     * @param DateTime $date
     * @param Company|null $company
     * @return false|null|string
     */
    protected function getBalanceAtDate(DateTime $date, Company $company = NULL)
    {
        /* @var CashFlowBase $query */
        $query = $this->searchQuery();
        $t = $query->getTableAlias();
        $i = CashFlowsBase::FLOW_TYPE_INCOME;
        $select = "IF({{{$t}}}.[[flow_type]] = {$i}, {{{$t}}}.[[amount]], - {{{$t}}}.[[amount]])";
        $query->select(['flow_sum' => $select])
            ->andWhere(['<=', $t.'.date', $date->format('Y-m-d')]);

        return (int) (new Query)->from(['flow' => $query])->sum('flow_sum');
    }

    /**
     * @param DateTime|NULL $date
     * @param Company|NULL $company
     * @return int
     * @throws ErrorException
     */
    public function getBalanceAtBegin(DateTime $date = NULL, Company $company = NULL)
    {
        //return 0;
        //  Был этот код, но получается, что в начале периода нам всегда нужен 0
        $date = $this->processPassedDate($date);
        if (!$date && !$date = clone $this->beginAt) {
            throw new ErrorException('Необходимо задать дату!');
        }
        $atDate = clone $date;
        $atDate->sub(new DateInterval('P1D')); // Исключаем текущий день
        return $this->getBalanceAtDate($atDate, $company);
    }

    /**
     * @param null $date
     * @param Company|NULL $company
     * @return false|null|string
     * @throws ErrorException
     */
    public function getBalanceAtEnd($date = NULL, Company $company = NULL)
    {
        $date = $this->processPassedDate($date);
        if (!$date && !$date = clone $this->endAt) {
            throw new ErrorException('Необходимо задать дату!');
        }

        return $this->getBalanceAtDate($date, $company);
    }

    /**
     * Вычисляет сумму [[type]] типа операций для указаного промежутка дат компании
     * текущего пользователя
     *
     * @param integer $type тип влияния на счёт. @see CashFlowsBase::getFlowTypes
     * @param DateTime $beginAtDate
     * @param DateTime $endAtDate
     * @param Company $company
     * @return integer
     */
    protected function calculateFlow($type, DateTime $beginAtDate, DateTime $endAtDate, Company $company = NULL)
    {
        $query = $this->searchQuery($company);
        $t = $query->getTableAlias();
        $query->select($t.'.amount')->byFlowType($type)->andWhere([
            'between',
            $t.'.date',
            $beginAtDate->format(DateHelper::FORMAT_DATE),
            $endAtDate->format(DateHelper::FORMAT_DATE),
        ]);

        return (int) (new Query)->from(['flow' => $query])->sum('amount');
    }

    /**
     * @param $type
     * @param DateTime $beginAtDate
     * @param DateTime $endAtDate
     * @return int|string
     */
    protected function calculateFlowCount($type, DateTime $beginAtDate, DateTime $endAtDate)
    {
        $query = $this->searchQuery();
        $t = $query->getTableAlias();
        $query->byFlowType($type)->andWhere([
            'between',
            $t.'.date',
            $beginAtDate->format(DateHelper::FORMAT_DATE),
            $endAtDate->format(DateHelper::FORMAT_DATE),
        ]);

        return $query->count();
    }

    /**
     * @param null $beginAtDate
     * @param null $endAtDate
     * @param Company|NULL $company
     * @return int
     * @throws ErrorException
     */
    public function getPeriodIncome($beginAtDate = NULL, $endAtDate = NULL, Company $company = NULL)
    {
        $beginAtDate = $this->processPassedDate($beginAtDate);
        $endAtDate = $this->processPassedDate($endAtDate);

        if (!$beginAtDate && !($beginAtDate = clone $this->beginAt)) {
            throw new ErrorException('Неверно задана начальная дата!');
        }

        if (!$endAtDate && !($endAtDate = clone $this->endAt)) {
            throw new ErrorException('Неверно задана конечная дата!');
        }

        return $this->calculateFlow(CashFlowsBase::FLOW_TYPE_INCOME, $beginAtDate, $endAtDate, $company);
    }

    /**
     * @param null $beginAtDate
     * @param null $endAtDate
     * @return int|string
     * @throws ErrorException
     */
    public function getCountIncome($beginAtDate = NULL, $endAtDate = NULL)
    {
        $beginAtDate = $this->processPassedDate($beginAtDate);
        $endAtDate = $this->processPassedDate($endAtDate);

        if (!$beginAtDate && !($beginAtDate = clone $this->beginAt)) {
            throw new ErrorException('Неверно задана начальная дата!');
        }

        if (!$endAtDate && !($endAtDate = clone $this->endAt)) {
            throw new ErrorException('Неверно задана конечная дата!');
        }

        return $this->calculateFlowCount(CashFlowsBase::FLOW_TYPE_INCOME, $beginAtDate, $endAtDate);
    }

    /**
     * @param null $beginAtDate
     * @param null $endAtDate
     * @param Company|NULL $company
     * @return int
     * @throws ErrorException
     */
    public function getPeriodExpense($beginAtDate = NULL, $endAtDate = NULL, Company $company = NULL)
    {
        $beginAtDate = $this->processPassedDate($beginAtDate);
        $endAtDate = $this->processPassedDate($endAtDate);

        if (!$beginAtDate && !$beginAtDate = clone $this->beginAt) {
            throw new ErrorException('Неверно задана начальная дата!');
        }

        if (!$endAtDate && !$endAtDate = clone $this->endAt) {
            throw new ErrorException('Неверно задана конечная дата!');
        }

        return $this->calculateFlow(CashFlowsBase::FLOW_TYPE_EXPENSE, $beginAtDate, $endAtDate, $company);
    }

    /**
     * @param null $beginAtDate
     * @param null $endAtDate
     * @return int|string
     * @throws ErrorException
     */
    public function getCountExpense($beginAtDate = NULL, $endAtDate = NULL)
    {
        $beginAtDate = $this->processPassedDate($beginAtDate);
        $endAtDate = $this->processPassedDate($endAtDate);

        if (!$beginAtDate && !($beginAtDate = clone $this->beginAt)) {
            throw new ErrorException('Неверно задана начальная дата!');
        }

        if (!$endAtDate && !($endAtDate = clone $this->endAt)) {
            throw new ErrorException('Неверно задана конечная дата!');
        }

        return $this->calculateFlowCount(CashFlowsBase::FLOW_TYPE_EXPENSE, $beginAtDate, $endAtDate);
    }

    /**
     * Get start period date for widget. Format (d.m.y)
     * @return string
     */
    public function getPeriodStartDate()
    {
        return $this->beginAt->format('d.m.Y');
    }

    /**
     * Get end period date for widget. Format (d.m.y)
     * @return string
     */
    public function getPeriodEndDate()
    {
        return $this->endAt->format('d.m.Y');
    }

    /**
     * @param Query $query
     */
    private function setConditions(Query $query)
    {
        $table = $this->owner->tableName();
        if ($this->owner->hasAttribute('is_accounting')
            && $this->owner->is_accounting !== null
            && ($this->owner->is_accounting == 1 || $this->owner->is_accounting == 0)
        ) {
            $query->andWhere([
                $table . '.is_accounting' => $this->owner->is_accounting,
            ]);
        }

        if ($this->owner->hasAttribute('income_item_id')) {
            $query->andFilterWhere([
                $table . '.income_item_id' => $this->owner->income_item_id,
            ]);
        }

        $query->andFilterWhere([
            $table . '.contractor_id' => $this->owner->contractor_id,
            $table . '.expenditure_item_id' => $this->owner->expenditure_item_id,
            $table . '.flow_type' => ($this->owner->flow_type == '-1')? null: $this->owner->flow_type,
        ]);
    }
}
