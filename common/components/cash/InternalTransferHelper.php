<?php

namespace common\components\cash;

use common\models\cash\CashBankFlows;
use common\models\cash\CashBankForeignCurrencyFlows;
use common\models\cash\CashEmoneyFlows;
use common\models\cash\CashEmoneyForeignCurrencyFlows;
use common\models\cash\CashOrderFlows;
use common\models\cash\CashOrderForeignCurrencyFlows;
use common\models\cash\Cashbox;
use common\models\cash\Emoney;
use common\models\company\CheckingAccountant;

/**
 *
 */
class InternalTransferHelper
{
    public static function accountClassByTable($table)
    {
        $items = [
            CheckingAccountant::tableName() => CheckingAccountant::class,
            Cashbox::tableName() => Cashbox::class,
            Emoney::tableName() => Emoney::class,
        ];

        return $items[$table] ?? null;
    }
    public static function flowClassByTable($table)
    {
        $items = [
            CashBankFlows::tableName() => CashBankFlows::class,
            CashBankForeignCurrencyFlows::tableName() => CashBankForeignCurrencyFlows::class,
            CashOrderFlows::tableName() => CashOrderFlows::class,
            CashOrderForeignCurrencyFlows::tableName() => CashOrderForeignCurrencyFlows::class,
            CashEmoneyFlows::tableName() => CashEmoneyFlows::class,
            CashEmoneyForeignCurrencyFlows::tableName() => CashEmoneyForeignCurrencyFlows::class,
        ];

        return $items[$table] ?? null;
    }
}
