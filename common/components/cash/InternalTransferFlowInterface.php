<?php

namespace common\components\cash;

use common\models\cash\CashFlowsBase;
use yii\db\ActiveQuery;

/**
 * Интерфейс перевода между своими счетами для операций
 */
interface InternalTransferFlowInterface
{
    public function setInternalTransferAttributes(CashFlowsBase $flow) : void;
    public function getCashContractorTypeText() : string;
    public function getInternalTransferAccountClass() : ?string;
    public function getInternalTransferFlowClass() : ?string;
    public function getSelfAccount() : ?ActiveQuery;
    public function getInternalTransferAccount() : ?ActiveQuery;
    public function getInternalTransferFlow() : ?ActiveQuery;
}
