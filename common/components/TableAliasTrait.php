<?php

namespace common\components;

trait TableAliasTrait
{
    /**
     * @return string
     */
    public function getTableAlias()
    {
        list(, $alias) = $this->getTableNameAndAlias();

        return $alias;
    }
}
