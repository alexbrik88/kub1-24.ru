<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 07.07.2017
 * Time: 6:05
 */

namespace common\components\excel;

use common\models\product\Product;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;


/**
 * Class Excel
 * @package common\components\excel
 */
class Excel extends \moonland\phpexcel\Excel
{
    public $title = '';

    public $rangeHeader = null;

    /**
     * (non-PHPdoc)
     * @see \yii\base\Widget::run()
     */
    public function run()
    {
        if ($this->mode == 'export') {
            $sheet = new Spreadsheet();

            if (!isset($this->models))
                throw new InvalidConfigException('Config models must be set');

            if (isset($this->properties)) {
                $this->properties($sheet, $this->properties);
            }

            $this->rangeHeader = $this->rangeHeader ? $this->rangeHeader : range('A', 'K');
            foreach ($this->rangeHeader as $key => $columnID) {
                $cell = $sheet->getActiveSheet()->getCellByColumnAndRow($key, 1);
                //if ($cell->getValue() !== null) {
                $sheet->getActiveSheet()->getColumnDimension($columnID)->setAutoSize(true);
                $sheet->getActiveSheet()->getStyle($columnID . 1)->getFont()->setBold(true);
                $sheet->getActiveSheet()->getStyleByColumnAndRow($key, 1)->applyFromArray([
                    'alignment' => [
                        'horizontal' => \PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
                    ],
                    'fill' => [
                        'type' => \PHPExcel_Style_Fill::FILL_SOLID,
                        'color' => [
                            'rgb' => '4276a4',
                        ],
                    ],
                ]);
                //}
            }

            if ($this->isMultipleSheet) {
                $index = 0;
                $worksheet = [];
                foreach ($this->models as $title => $models) {
                    $sheet->createSheet($index);
                    $sheet->getSheet($index)->setTitle($title);
                    $worksheet[$index] = $sheet->getSheet($index);
                    $columns = isset($this->columns[$title]) ? $this->columns[$title] : [];
                    $headers = isset($this->headers[$title]) ? $this->headers[$title] : [];
                    $this->executeColumns($worksheet[$index], $models, $this->populateColumns($columns), $headers);
                    $index++;
                }
            } else {
                $worksheet = $sheet->getActiveSheet();
                $this->executeColumns($worksheet, $this->models, isset($this->columns) ? $this->populateColumns($this->columns) : [], isset($this->headers) ? $this->headers : []);
            }
            if ($this->asAttachment) {
                $this->setHeaders();
            }
            $sheet->getActiveSheet()->setTitle($this->title);

            $this->writeFile($sheet);
        } elseif ($this->mode == 'import') {
            if (is_array($this->fileName)) {
                $datas = [];
                foreach ($this->fileName as $key => $filename) {
                    $datas[$key] = $this->readFile($filename);
                }

                return $datas;
            } else {
                return $this->readFile($this->fileName);
            }
        }
    }

    /**
     * Setting data from models
     */
    public function executeColumns(&$activeSheet = null, $models, $columns = [], $headers = [])
    {
        if ($activeSheet == null) {
            $activeSheet = $this->activeSheet;
        }
        $hasHeader = false;
        $row = 1;
        $char = 26;
        foreach ($models as $model) {
            if (empty($columns)) {
                $columns = $model->attributes();
            }
            if ($this->setFirstTitle && !$hasHeader) {
                $isPlus = false;
                $colplus = 0;
                $colnum = 1;
                foreach ($columns as $key => $column) {
                    $col = '';
                    if ($colnum > $char) {
                        $colplus += 1;
                        $colnum = 1;
                        $isPlus = true;
                    }
                    if ($isPlus) {
                        $col .= chr(64 + $colplus);
                    }
                    $col .= chr(64 + $colnum);
                    $header = '';
                    if (is_array($column)) {
                        if (isset($column['header'])) {
                            $header = $column['header'];
                        } elseif (isset($column['attribute']) && isset($headers[$column['attribute']])) {
                            $header = $headers[$column['attribute']];
                        } elseif (isset($column['attribute'])) {
                            $header = $model->getAttributeLabel($column['attribute']);
                        }
                    } else {
                        $header = $model->getAttributeLabel($column);
                    }
                    $activeSheet->setCellValue($col . $row, $header);
                    $colnum++;
                }
                $hasHeader = true;
                $row++;
            }
            $isPlus = false;
            $colplus = 0;
            $colnum = 1;
            foreach ($columns as $key => $column) {
                $col = '';
                if ($colnum > $char) {
                    $colplus++;
                    $colnum = 1;
                    $isPlus = true;
                }
                if ($isPlus) {
                    $col .= chr(64 + $colplus);
                }
                $col .= chr(64 + $colnum);
                if (is_array($column)) {
                    $column_value = $this->executeGetColumnData($model, $column);
                } else {
                    $column_value = $this->executeGetColumnData($model, ['attribute' => $column]);
                }
                if (((isset($column['image']) && $column['image']) ||
                        (!is_array($column) && $model instanceof Product && $column == 'image')) &&
                    file_exists($column_value)) {
                    $objDrawing = new \PhpOffice\PhpSpreadsheet\Worksheet\Drawing();
                    $objDrawing->setName('PHPExcel image');
                    $objDrawing->setDescription('PHPExcel image');
                    $objDrawing->setPath($column_value);
                    $objDrawing->setResizeProportional(false);
                    $objDrawing->setHeight(99);
                    $objDrawing->setWidth(97);
                    $objDrawing->setOffsetY(1);
                    $objDrawing->setOffsetX(1);
                    $objDrawing->setCoordinates($col . $row);
                    $objDrawing->setWorksheet($activeSheet);

                    $activeSheet->getRowDimension($row)->setRowHeight(75);
                    $activeSheet->getColumnDimension($col)->setAutoSize(false);
                    $activeSheet->getColumnDimension($col)->setWidth(14);
                } else {
                    if (isset($column['type'])) {
                        $activeSheet->getStyle($col . $row)->getNumberFormat()->setFormatCode($column['type']);
                    }
                    if (isset($column['wrap'])) {
                        $activeSheet->getStyle($col . $row)->getAlignment()->setWrapText(true);
                    }
                    $activeSheet->getStyle($col . $row)->getAlignment()->setVertical('top');

                    if (isset($column['styleArray'])) {
                        $activeSheet->getStyle($col . $row)->applyFromArray($column['styleArray']);
                    }
                    if (isset($column['dataType'])) {
                        $activeSheet->setCellValueExplicit($col . $row, $column_value, $column['dataType']);
                    } else {
                        $activeSheet->setCellValue($col . $row, $column_value);
                    }
                }
                $colnum++;
            }
            $row++;
        }
    }
}