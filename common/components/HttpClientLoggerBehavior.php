<?php

namespace common\components;

use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\httpclient\Client;

class HttpClientLoggerBehavior extends Behavior
{
    /**
     * @var string $logName
     */
    public $logName;

    public function events()
    {
        return [
            Client::EVENT_AFTER_SEND => 'afterSend',
        ];
    }

    public function afterSend($event)
    {
        if (!empty($this->logName)) {
            $logFile = \Yii::getAlias("@runtime/logs/{$this->logName}.log");
            $data = [
                'responseTime' => $event->request->responseTime(),
                'request' => $event->request,
                'response' => $event->response,
            ];

            file_put_contents($logFile, date('c')." ".\yii\helpers\VarDumper::dumpAsString($data)."\n\n", FILE_APPEND);
        }
    }
}
