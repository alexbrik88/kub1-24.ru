<?php

namespace common\components\rest;

use common\components\DbTimeZone;
use common\models\Company;
use common\models\company\Attendance;
use common\models\company\CompanyVisit;
use Yii;
use yii\base\ActionEvent;
use yii\base\Behavior;
use yii\base\Module;
use yii\web\Controller;
use yii\web\ForbiddenHttpException;
use yii\web\UnauthorizedHttpException;

/**
 * ApiPreprocess
 *
 * ```php
 * public function behaviors()
 * {
 *     return [
 *         'preprocess' => [
 *             'class' => \frontend\modules\api\components\ApiPreprocess::className(),
 *         ],
 *     ];
 * }
 * ```
 */
class ApiPreprocess extends Behavior
{
    /**
     * @var array list of action IDs that this filter should apply to. If this property is not set,
     * then the filter applies to all actions, unless they are listed in [[except]].
     * If an action ID appears in both [[only]] and [[except]], this filter will NOT apply to it.
     *
     * @see except
     */
    public $only;
    /**
     * @var array list of action IDs that this filter should not apply to.
     * @see only
     */
    public $except = [];

    /**
     * Declares event handlers for the [[owner]]'s events.
     * @return array events (array keys) and the corresponding event handler methods (array values).
     */
    public function events()
    {
        return [Controller::EVENT_BEFORE_ACTION => 'beforeAction'];
    }

    /**
     * @param ActionEvent $event
     * @return bool
     * @throws ForbiddenHttpException when the request is not allowed.
     */
    public function beforeAction($event)
    {
        $action = $event->action->id;
        $identity = Yii::$app->getUser()->getIdentity();

        if ($identity !== null && $identity->timeZone) {
            date_default_timezone_set($identity->timeZone->time_zone);
        } else {
            date_default_timezone_set('Europe/Moscow');
        }

        DbTimeZone::sync();

        if ($this->isContinue($event->action)) {
            if ($identity === null) {
                throw new UnauthorizedHttpException('Your request was made with invalid credentials.');
            }
            $company = $identity->getCompany();
            if ($company === null) {
                throw new ForbiddenHttpException('User\'s company not found.');
            }
            if ($company->blocked == Company::BLOCKED) {
                throw new ForbiddenHttpException('User\'s company is locked.');
            }

            if (!Attendance::find()->andWhere(['and',
                    ['company_id' => $company->id],
                    ['date' => date('Y-m-d')],
                ])->exists()
            ) {
                $attendance = new Attendance();
                $attendance->company_id = $company->id;
                $attendance->date = date('Y-m-d');
                $attendance->activity_status = $company->activation_type;
                $attendance->is_first_visit = !Attendance::find()->andWhere([
                    'company_id' => $company->id,
                ])->exists();
                $attendance->save(false);
            }
            CompanyVisit::checkVisit($company);
            \common\models\company\CompanyLastVisit::create($company, $identity, Yii::$app->getRequest()->getUserIP());
        }

        return $event->isValid;
    }

    /**
     * Returns a value indicating whether the filter is active for the given action.
     * @param Action $action the action being filtered
     * @return bool whether the filter is active for the given action.
     */
    protected function isContinue($action)
    {
        $id = $this->getActionId($action);

        if (empty($this->only)) {
            $onlyMatch = true;
        } else {
            $onlyMatch = false;
            foreach ($this->only as $pattern) {
                if (fnmatch($pattern, $id)) {
                    $onlyMatch = true;
                    break;
                }
            }
        }

        $exceptMatch = false;
        foreach ($this->except as $pattern) {
            if (fnmatch($pattern, $id)) {
                $exceptMatch = true;
                break;
            }
        }

        return !$exceptMatch && $onlyMatch;
    }

    /**
     * Returns an action ID by converting [[Action::$uniqueId]] into an ID relative to the module
     * @param Action $action
     * @return string
     */
    protected function getActionId($action)
    {
        if ($this->owner instanceof Module) {
            $mid = $this->owner->getUniqueId();
            $id = $action->getUniqueId();
            if ($mid !== '' && strpos($id, $mid) === 0) {
                $id = substr($id, strlen($mid) + 1);
            }
        } else {
            $id = $action->id;
        }

        return $id;
    }
}
