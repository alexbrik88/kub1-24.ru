<?php

namespace common\components\rest;

use Yii;
use yii\base\Model;
use yii\helpers\Url;
use yii\web\ServerErrorHttpException;

/**
 * CreateAction implements the API endpoint for creating a new model from the given data.
 *
 * For more details and usage information on CreateAction, see the [guide article on rest controllers](guide:rest-controllers).
 */
class CreateAction extends \yii\rest\CreateAction
{
    /**
     * @var callable a PHP callable that will be called when running an action to create a new model object
     * The signature of the callable should be:
     *
     * ```php
     * function ($action, $scenario) {
     *     // $action is the action object currently running
     * }
     * ```
     *
     * The callable should return an instance of [[\yii\db\ActiveRecord]].
     */
    public $newModel;

    /**
     * @var callable a PHP callable that will be called if the action was successful
     * The signature of the callable should be:
     *
     * ```php
     * function ($model, $action) {
     *     // $action is the action object currently running
     * }
     * ```
     */
    public $success;

    /**
     * Creates a new model.
     * @return \yii\db\ActiveRecordInterface the model newly created
     * @throws ServerErrorHttpException if there is any error when creating the model
     */
    public function run()
    {
        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id);
        }

        /* @var $model \yii\db\ActiveRecord */
        if ($this->newModel) {
            $model = call_user_func($this->newModel, $this->scenario, $this);
        } else {
            $model = new $this->modelClass([
                'scenario' => $this->scenario,
            ]);
        }

        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        if ($model->save()) {
            if ($this->success) {
                call_user_func($this->success, $model, $this);
            }
            $response = Yii::$app->getResponse();
            $response->setStatusCode(201);
            $id = implode(',', array_values($model->getPrimaryKey(true)));
            $response->getHeaders()->set('Location', Url::toRoute([$this->viewAction, 'id' => $id], true));
        } elseif (!$model->hasErrors()) {
            throw new ServerErrorHttpException('Failed to create the object for unknown reason.');
        }

        return $model;
    }
}
