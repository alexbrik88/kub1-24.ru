<?php

namespace common\components\rest;

use Yii;
use yii\web\ServerErrorHttpException;

/**
 * DeleteAction implements the API endpoint for deleting a model.
 *
 * For more details and usage information on DeleteAction, see the [guide article on rest controllers](guide:rest-controllers).
 */
class DeleteAction extends \yii\rest\DeleteAction
{
    /**
     * @var callable a PHP callable that will be called if the action was successful
     * The signature of the callable should be:
     *
     * ```php
     * function ($model, $action) {
     *     // $action is the action object currently running
     * }
     * ```
     */
    public $success;

    /**
     * Deletes a model.
     * @param mixed $id id of the model to be deleted.
     * @throws ServerErrorHttpException on failure.
     */
    public function run($id)
    {
        $model = $this->findModel($id);

        if ($this->checkAccess) {
            call_user_func($this->checkAccess, $this->id, $model);
        }

        if ($model->delete() === false) {
            if ($model->hasErrors()) {
                return $model;
            }
            throw new ServerErrorHttpException('Failed to delete the object for unknown reason.');
        }

        if ($this->success) {
            call_user_func($this->success, $model, $this);
        }

        Yii::$app->getResponse()->setStatusCode(204);
    }
}

