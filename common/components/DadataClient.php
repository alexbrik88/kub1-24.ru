<?php
namespace common\components;

use common\components\curl;
use common\models\Company;
use common\models\company\CompanyType;
use common\models\Contractor;
use Yii;
use yii\base\Component;
use yii\helpers\ArrayHelper;

/**
 * Class DadataClient
 *
 * Dadata API PHP Client
 * https://dadata.ru/api/
 */
class DadataClient extends Component
{
    /**
     * Dadata API Url
     * @var string
     */
    public static $url = 'https://suggestions.dadata.ru/suggestions/api/4_1/rs/';

    /**
     * Curl request
     * @param  string $uri  URL part
     * @param  array  $data request data
     * @return array|null
     */
    public function request($uri, $data)
    {
        if ($token = ArrayHelper::getValue(Yii::$app->params, 'dadata_token')) {
            $requestHeader = [
                'Authorization: Token ' . $token,
                'Content-Type: application/json',
                'Accept: application/json',
            ];

            $curl = new curl\Curl();
            $response = $curl->setOptions([
                CURLOPT_HTTPHEADER => $requestHeader,
                CURLOPT_POSTFIELDS => json_encode($data),
            ])->post(static::$url . $uri);

            if ($curl->responseCode == 200) {
                return json_decode($response, true);
            }
        }

        return [];
    }

    /**
     * Get Bank data by bank ID
     * @param  string  $query
     * @return array
     */
    public function getBank($query)
    {
        $data = [
            'query' => $query,
        ];

        return (array) $this->request('findById/bank', $data);
    }

    /**
     * Get FNS data by IFNS
     * @param  string  $ifns_ga
     * @return array
     */
    public function getFns($ifns_ga) {

        if (!$ifns_ga)
            return [];

        $data = [
            'query' => $ifns_ga,
            'branch_type' => 'MAIN'
        ];

        return (array) $this->request('suggest/fns_unit', $data);
    }

    /**
     * Get Company data by INN
     * @param  string  $query
     * @return array
     */
    public function getCompany($query)
    {
        $data = [
            'query' => $query,
            'branch_type' => 'MAIN'
        ];

        return (array) $this->request('findById/party', $data);
    }

    /**
     * Find Companies by INN, name
     * @param  string  $query
     * @return array
     */
    public function getCompanies($query)
    {
        $data = [
            'query' => $query,
        ];

        return (array) $this->request('suggest/party', $data);
    }

    /**
     * Company data by INN
     * @param  string $inn
     * @return array
     */
    public static function getBankData($id)
    {
        $dadata = new DadataClient;
        $suggestions = (array) ArrayHelper::getValue($dadata->getCompany($inn), 'suggestions', []);
        foreach ($suggestions as $item) {
            $data = ArrayHelper::getValue($item, 'data');
            $status = ArrayHelper::getValue($data, 'state.status');
            if ($data && $status == 'ACTIVE') {
                if (empty($kpp) || (isset($data['kpp']) && $kpp == $data['kpp'])) {
                    return $data;
                }
            }
        }

        return null;
    }

    /**
     * Company data by INN
     * @param  string $inn
     * @return array
     */
    public static function getCompanyData($inn, $kpp = null, $allSatatus = false)
    {
        $dadata = new DadataClient;
        $suggestions = (array) ArrayHelper::getValue($dadata->getCompany($inn), 'suggestions', []);
        foreach ($suggestions as $item) {
            $data = ArrayHelper::getValue($item, 'data');
            $status = ArrayHelper::getValue($data, 'state.status');
            if ($data && ($allSatatus || $status == 'ACTIVE')) {
                if (empty($kpp) || (isset($data['kpp']) && $kpp == $data['kpp'])) {
                    return $data;
                }
            }
        }

        return null;
    }

    /**
     * Company data by INN and KPP
     * @param  string $inn
     * @param  string $kpp
     * @return array
     */
    public static function getCompanyAttributes($inn, $kpp = null)
    {
        $data = self::getCompanyData($inn, $kpp);

        if ($data !== null) {
            $name = (array) ArrayHelper::getValue($data, 'name');
            if ($typeName = ArrayHelper::getValue($data, 'opf.short', '')) {
                $nameWithOpf = ArrayHelper::getValue($name, 'short_with_opf');
                if (mb_strstr($nameWithOpf, $typeName)) {
                    $typeId = CompanyType::find()->select('id')->where([
                        'name_short' => $typeName,
                    ])->scalar() ? : null;
                } else {
                    list($typeId,) = \frontend\modules\cash\modules\banking\components\vidimus\VidimusUploader::getTypeName($nameWithOpf);
                }
            } else {
                $typeId = null;
            }
            if ($typeId === null) {
                switch (strlen((string) $inn)) {
                    case 10:
                        $typeId = CompanyType::TYPE_OOO;
                        break;
                    case 12:
                        $typeId = CompanyType::TYPE_IP;
                        break;

                    default:
                        $typeId = CompanyType::TYPE_EMPTY;
                        break;
                }
            }
            $isIp = $typeId == CompanyType::TYPE_IP;

            $chiefName = $isIp ? ArrayHelper::getValue($name, 'full') : ArrayHelper::getValue($data, 'management.name');
            $nameItems = is_string($chiefName) ? preg_split("/\s+/", trim($chiefName), 3) : [];

            $address = (array) ArrayHelper::getValue($data, 'address');

            $addresString = (string) ArrayHelper::getValue($address, 'unrestricted_value');

            $ogrn = (string) ArrayHelper::getValue($data, 'ogrn');
            $regDate = round(ArrayHelper::getValue($data, 'state.registration_date')/1000);
            return [
                'company_type_id' => $typeId,
                'inn' => ArrayHelper::getValue($data, 'inn'),
                'kpp' => ArrayHelper::getValue($data, 'kpp'),
                'ogrn' => strlen($ogrn) == 13 ? $ogrn : null,
                'egrip' => strlen($ogrn) == 15 ? $ogrn : null,
                'okpo' => ArrayHelper::getValue($data, 'okpo'),
                'okved' => ArrayHelper::getValue($data, 'okved'),
                'okato' => (string) ArrayHelper::getValue($address, 'data.okato'),
                'oktmo' => (string) ArrayHelper::getValue($address, 'data.oktmo'),
                'ifns_ga' => (string) ArrayHelper::getValue($address, 'data.tax_office_legal'),
                'name_full' => ArrayHelper::getValue($name, 'full'),
                'name_short' => $isIp ? TextHelper::nameShort($chiefName) : ArrayHelper::getValue($name, 'short'),
                'address_legal' => $addresString,
                'address_actual' => $addresString,
                'chief_post_name' => $isIp ? '' : ArrayHelper::getValue($data, 'management.post'),
                'chief_lastname' => ArrayHelper::getValue($nameItems, 0),
                'chief_firstname' => ArrayHelper::getValue($nameItems, 1),
                'chief_patronymic' => ArrayHelper::getValue($nameItems, 2),
                'ip_lastname' => $isIp ? ArrayHelper::getValue($nameItems, 0) : null,
                'ip_firstname' => $isIp ? ArrayHelper::getValue($nameItems, 1) : null,
                'ip_patronymic' => $isIp ? ArrayHelper::getValue($nameItems, 2) : null,
                'nds' => $isIp ? null : \common\models\NdsOsno::WITH_NDS,
                'tax_authority_registration_date' => $regDate ? date('Y-m-d', $regDate) : null,
                'taxRegistrationDate' => $regDate ? date('d.m.Y', $regDate) : null,
            ];
        }

        return [];
    }

    /**
     * Contractor data by INN and KPP
     * @param  string $inn
     * @param  string $kpp
     * @return array
     */
    public static function getContractorAttributes($inn, $kpp = null)
    {
        $data = self::getCompanyData($inn, $kpp);
        if ($data !== null) {
            $management = (array) ArrayHelper::getValue($data, 'management');
            $name = (array) ArrayHelper::getValue($data, 'name');
            if ($typeName = ArrayHelper::getValue($data, 'opf.short', '')) {
                $nameWithOpf = ArrayHelper::getValue($name, 'short_with_opf');
                if (mb_strstr($nameWithOpf, $typeName)) {
                    $typeId = CompanyType::find()->select('id')->where([
                        'name_short' => $typeName,
                    ])->scalar() ? : null;
                } else {
                    list($typeId,) = \frontend\modules\cash\modules\banking\components\vidimus\VidimusUploader::getTypeName($nameWithOpf);
                }
            } else {
                $typeId = null;
            }
            $isIp = $typeId == CompanyType::TYPE_IP;

            $chiefName = $isIp ? ArrayHelper::getValue($name, 'full') : ArrayHelper::getValue($management, 'name');

            $address = (array) ArrayHelper::getValue($data, 'address', []);

            $postCode = (string) ArrayHelper::getValue(ArrayHelper::getValue($address, 'data'), 'postal_code');
            $addresString = ArrayHelper::getValue($address, 'value');
            if ($addresString && $postCode && strpos($addresString, $postCode) === false) {
                $addresString = $postCode . ', ' . $addresString;
            }

            return [
                'face_type' => Contractor::TYPE_LEGAL_PERSON,
                'company_type_id' => $typeId,
                'ITN' => (string) ArrayHelper::getValue($data, 'inn'),
                'BIN' => (string) ArrayHelper::getValue($data, 'ogrn'),
                'name' => ArrayHelper::getValue($name, 'full'),
                'PPC' => ArrayHelper::getValue($data, 'kpp'),
                'okpo' => ArrayHelper::getValue($data, 'okpo'),
                'director_name' => $chiefName ? : Contractor::DEFAULT_DIRECTOR_NAME,
                'director_post_name' => (string) ($isIp ? '' : ArrayHelper::getValue($management, 'post')),
                'legal_address' => $addresString,
                'actual_address' => $addresString,
                'postal_address' => $addresString,
            ];
        }

        return [];
    }
}
