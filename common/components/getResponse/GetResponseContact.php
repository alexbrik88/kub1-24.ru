<?php

namespace common\components\getResponse;

use Yii;
use common\models\Company;
use common\models\employee\Employee;

/**
 * This is the model class for table "get_response_contact".
 *
 * @property int $id
 * @property int $employee_id
 * @property int $company_id
 * @property int $created_at
 * @property int $updated_at
 * @property int $update_attempts_count
 *
 * @property Company $company
 * @property Employee $employee
 */
class GetResponseContact extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'get_response_contact';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['employee_id', 'company_id', 'created_at', 'updated_at'], 'required'],
            [['employee_id', 'company_id', 'created_at', 'updated_at', 'update_attempts_count'], 'integer'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['company_id' => 'id']],
            [['employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::class, 'targetAttribute' => ['employee_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'employee_id' => 'Employee ID',
            'company_id' => 'Company ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'update_attempts_count' => 'Update Attempts Count',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::class, ['id' => 'employee_id']);
    }
}
