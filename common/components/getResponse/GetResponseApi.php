<?php

namespace common\components\getResponse;

use common\models\Company;
use common\models\company\CompanyInfoIndustry;
use common\models\service\SubscribeHelper;
use common\models\service\SubscribeTariff;
use common\models\service\SubscribeTariffGroup;
use frontend\models\FinanceRegistrationForm;
use Yii;
use yii\helpers\VarDumper;
use yii\helpers\ArrayHelper;
use common\components\curl\Curl;
use common\models\employee\Employee;

class GetResponseApi {

    const TIMEOUT_SEC = 5;
    const TIMEOUT_CONNECT_SEC = 5;

    const STATUS_EMPTY_PROFILE = 'не заполнен профиль';
    const STATUS_FILLED_PROFILE = 'профиль заполнен';

    const PRODUCT_FINDIR = 'findir';
    const PRODUCT_BILLING = 'billing';

    const PRODUCT_TYPE_PRODUCT = 'tovari';
    const PRODUCT_TYPE_SERVICE = 'uslugi';
    const PRODUCT_TYPE_OTHER = 'drugoe';

    const REG_USER_TYPE_DIRECTOR = 'director';
    const REG_USER_TYPE_FIN_CONSULTANT = 'fin_consultant';
    const REG_USER_TYPE_OTHER = 'drugoe';

    const TRUE = 'true';
    const FALSE = 'false';

    const FINDIR_TARIFF_GROUPS = [
        SubscribeTariffGroup::BI_FULL_CONSTRUCTION,
        SubscribeTariffGroup::BI_ALL_INCLUSIVE,
        SubscribeTariffGroup::BI_FINANCE_PLUS,
        SubscribeTariffGroup::BI_FINANCE,
    ];

    // see table `company_info_industry`
    const PRODUCT_TYPES = [
        1 => self::PRODUCT_TYPE_SERVICE,
        2 => self::PRODUCT_TYPE_SERVICE,
        3 => self::PRODUCT_TYPE_PRODUCT,
        4 => self::PRODUCT_TYPE_PRODUCT,
        5 => self::PRODUCT_TYPE_PRODUCT,
        6 => self::PRODUCT_TYPE_PRODUCT,
        7 => self::PRODUCT_TYPE_PRODUCT,
        8 => self::PRODUCT_TYPE_OTHER,
        9 => self::PRODUCT_TYPE_SERVICE,
        10 => self::PRODUCT_TYPE_PRODUCT,
        11 => self::PRODUCT_TYPE_SERVICE,
        12 => self::PRODUCT_TYPE_SERVICE,
    ];

    // see table `employee_info_role`
    const REG_USER_TYPES = [
        1 => self::REG_USER_TYPE_DIRECTOR,
        2 => self::REG_USER_TYPE_DIRECTOR,
        9 => self::REG_USER_TYPE_DIRECTOR,
        4 => self::REG_USER_TYPE_FIN_CONSULTANT,
        8 => self::REG_USER_TYPE_OTHER
    ];
    
    //
    private $apiKey;

    public $_debugMode = true;

    public $campaignId = [
        'kub24_findirektor' => 'ocM7c'
    ];
    public $customFieldId = [
        'country' => 'Vgiq1A',
        'phone' => 'Vlok9t', // code from reg_phone field to prevent phone validation
        'comment' => 'VgiMag',
        'user_email_owner' => 'Vwnnnt',
        'user_pass_owner' => 'Vwnn4Q',
        'subscribtion_settings' => 'Vwnnpp',
        // new
        'status' => 'VgGZ5g', // text
        'product' => 'VgGZqs', // findir|billing
        'trial_14_days' => 'VgGZZB', // true|false
        'trial_count' => 'VgGZiU', // number
        'tarif_count' => 'VgGZfu', // number
        'reg_url' => 'VgGZvk', // text
        'last_sign_in' => 'VgGijG', // datetime
        'company_type' => 'VwyBBM', // text
        'product_type' => 'VwyQB2', // tovari|uslugi|drugoe
        'reg_user_type' => 'VwyQzM' // text
    ];

    public $protocol = 'https://';
    public $host = 'api.getresponse.com';
    public $path = '/v3/contacts';

    public function __construct()
    {
        $this->apiKey = ArrayHelper::getValue(Yii::$app->params['get_response'], 'api_key');
    }

    public function createGetResponseContact(Employee $user, FinanceRegistrationForm $form)
    {
        $company = $user->currentEmployeeCompany->company;

        $subscribeInfo = self::getCompanySubscribeInfo($company);

        $registrationData = [
            'email' => $form->email,
            'name' => $form->name,
            'phone' => $form->phone,
            'password' => $form->password,
            'notify' => [
                'newFeatures' => $user->notify_new_features,
                'nearlyReport' => $user->notify_nearly_report
            ],
            // new
            'status' => self::STATUS_EMPTY_PROFILE,
            'product' => self::PRODUCT_FINDIR,
            'trial_14_days' => ($subscribeInfo['isTrial']) ? self::TRUE : self::FALSE,
            'trial_count' => $subscribeInfo['trialCountDays'],
            'tarif_count' => $subscribeInfo['tariffCountDays'],
            'reg_url' => Yii::$app->request->referrer,
            'last_sign_in' => date('Y-m-d H:i:s'),
            // update later
            //'company_type' => null,
            //'product_type' => null,
            //'reg_user_type' => null
        ];

        if ($this->_createContact($registrationData)) {

            if ($this->_addEmployeeContact($form->user, $form->company)) {

                return true;

            } else {

                // todo: add2log("can't add contact to db (email)")
            }
        } else {

            // todo: add2log("can't create get response contact (email)")
        }

        return false;
    }

    public function updateGetResponseContact(Employee $user, array $data)
    {
        if ($contactId = $this->_getContactId($user->email)) {
            return $this->_updateContact($contactId, $data);
        } else {

            // todo: add2log("get response contact id not found (email)")
        }

        return false;
    }

    public static function getCompanySubscribeInfo(Company $company)
    {
        $actualSubscribe = null;
        foreach (self::FINDIR_TARIFF_GROUPS as $tariffGroupId) {
            if ($actualSubscribe = $company->getActualSubscription($tariffGroupId))
                break;
        }

        $isTrial = ($actualSubscribe) ? $actualSubscribe->isTrial : false;
        $isPaid = ($actualSubscribe) ? !$actualSubscribe->isTrial : false;

        if ($isTrial) {
            $trialCountDays = SubscribeHelper::getExpireLeftDays($actualSubscribe->expired_at);
        }

        if ($isPaid) {
            $tariffCountDays = SubscribeHelper::getExpireLeftDays($actualSubscribe->expired_at);
        }

        return [
            'isTrial' => $isTrial,
            'isPaid' => $isPaid,
            'trialCountDays' => $trialCountDays ?? 0,
            'tariffCountDays' => $tariffCountDays ?? 0,
        ];
    }

    private function _getContactId($email)
    {
        $requestHeader = [
            "Content-Type: application/json",
            "X-Auth-Token: api-key {$this->apiKey}",
        ];

        $curl = new Curl();

        $query = '/?' . http_build_query([
            'query[campaignId]' => $this->campaignId['kub24_findirektor'],
            'query[email]' => $email
        ]);

        $response = $curl->setOptions([
            CURLOPT_HTTPHEADER => $requestHeader,
            CURLOPT_TIMEOUT => self::TIMEOUT_SEC,
            CURLOPT_CONNECTTIMEOUT => self::TIMEOUT_CONNECT_SEC,
        ])->get($this->protocol . $this->host . $this->path . $query);

        $this->debugLog($curl, __METHOD__);

        $contactsArr = json_decode($response);
        if (is_array($contactsArr) && count($contactsArr) === 1) {
            $contact = reset($contactsArr);
            return $contact->contactId ?? null;
        }

        return null;
    }

    private function _createContact($data)
    {
        $postData = [
            'name' => $data['name'] ?? null,
            'campaign' => [
                'campaignId' => $this->campaignId['kub24_findirektor']
            ],
            'email' => $data['email'] ?? null,
            'customFieldValues' => [
                [
                    'customFieldId' => $this->customFieldId['phone'],
                    'value' => array(preg_replace('/[^0-9+]/', '', $data['phone'] ?? null))
                ],
                [
                    'customFieldId' => $this->customFieldId['country'],
                    'value' => array('Russian Federation')
                ],
                [
                    'customFieldId' => $this->customFieldId['user_email_owner'],
                    'value' => array($data['email'] ?? null)
                ],
                [
                    'customFieldId' => $this->customFieldId['user_pass_owner'],
                    'value' => array($data['password'] ?? null)
                ],
                // new
                [
                    'customFieldId' => $this->customFieldId['status'],
                    'value' => array($data['status'] ?? null)
                ],
                [
                    'customFieldId' => $this->customFieldId['product'],
                    'value' => array($data['product'] ?? null)
                ],
                [
                    'customFieldId' => $this->customFieldId['trial_14_days'],
                    'value' => array($data['trial_14_days'] ?? null)
                ],
                [
                    'customFieldId' => $this->customFieldId['trial_count'],
                    'value' => array($data['trial_count'] ?? null)
                ],
                [
                    'customFieldId' => $this->customFieldId['tarif_count'],
                    'value' => array($data['tarif_count'] ?? null)
                ],
                [
                    'customFieldId' => $this->customFieldId['reg_url'],
                    'value' => array($data['reg_url'] ?? null)
                ],
                [
                    'customFieldId' => $this->customFieldId['last_sign_in'],
                    'value' => array($data['last_sign_in'] ?? null)
                ],
                // update later
                //[
                //    'customFieldId' => $this->customFieldId['company_type'],
                //    'value' => array($data['company_type'] ?? null)
                //],
                //[
                //    'customFieldId' => $this->customFieldId['product_type'],
                //    'value' => array($data['product_type'] ?? null)
                //],
                //[
                //    'customFieldId' => $this->customFieldId['reg_user_type'],
                //    'value' => array($data['reg_user_type'] ?? null)
                //],
            ]
        ];

        if (!empty($registrationData['notify'])) {
            $value = [];
            if (!empty($registrationData['notify']['newFeatures']))
                $value[] = 'new_features';
            if (!empty($registrationData['notify']['nearlyReport']))
                $value[] = 'nearly_reports_and_taxes';

            $postData['customFieldValues'][] = [
                'customFieldId' => $this->customFieldId['subscribtion_settings'],
                'value' => $value ?: ['-']
            ];
        }

        $requestHeader = [
            "Content-Type: application/json",
            "X-Auth-Token: api-key {$this->apiKey}",
        ];

        $curl = new Curl();

        $response = $curl->setOptions([
            CURLOPT_HTTPHEADER => $requestHeader,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => json_encode($postData),
            CURLOPT_TIMEOUT => self::TIMEOUT_SEC,
            CURLOPT_CONNECTTIMEOUT => self::TIMEOUT_CONNECT_SEC,
        ])->post($this->protocol . $this->host . $this->path);

        $this->debugLog($curl, __METHOD__);

        return (!$curl->errorCode);
    }

    private function _updateContact($contactId, $data)
    {
        $postData = [
            'customFieldValues' => []
        ];

        if (!empty($data['notify'])) {
            $value = array();
            if (!empty($data['notify']['newFeatures']))
                $value[] = 'new_features';
            if (!empty($data['notify']['nearlyReport']))
                $value[] = 'nearly_reports_and_taxes';

            $postData['customFieldValues'][] = [
                'customFieldId' => $this->customFieldId['subscribtion_settings'],
                'value' => $value ?: array('-')
            ];
        }

        if (isset($data['company_type'])) {
            $postData['customFieldValues'][] = [
                'customFieldId' => $this->customFieldId['company_type'],
                'value' => array($data['company_type'])
            ];
        }
        if (isset($data['product_type'])) {
            $postData['customFieldValues'][] = [
                'customFieldId' => $this->customFieldId['product_type'],
                'value' => array($data['product_type'])
            ];
        }
        if (isset($data['reg_user_type'])) {
            $postData['customFieldValues'][] = [
                'customFieldId' => $this->customFieldId['reg_user_type'],
                'value' => array($data['reg_user_type'])
            ];
        }
        if (isset($data['status'])) {
            $postData['customFieldValues'][] = [
                'customFieldId' => $this->customFieldId['status'],
                'value' => array($data['status'])
            ];
        }
        if (isset($data['last_sign_in'])) {
            $postData['customFieldValues'][] = [
                'customFieldId' => $this->customFieldId['last_sign_in'],
                'value' => array($data['last_sign_in'])
            ];
        }
        if (isset($data['trial_14_days'])) {
            $postData['customFieldValues'][] = [
                'customFieldId' => $this->customFieldId['trial_14_days'],
                'value' => array($data['trial_14_days'])
            ];
        }
        if (isset($data['trial_count'])) {
            $postData['customFieldValues'][] = [
                'customFieldId' => $this->customFieldId['trial_count'],
                'value' => array($data['trial_count'])
            ];
        }
        if (isset($data['tarif_count'])) {
            $postData['customFieldValues'][] = [
                'customFieldId' => $this->customFieldId['tarif_count'],
                'value' => array($data['tarif_count'])
            ];
        }
        if (isset($data['reg_url'])) {
            $postData['customFieldValues'][] = [
                'customFieldId' => $this->customFieldId['reg_url'],
                'value' => array($data['reg_url'])
            ];
        }

        if ($postData['customFieldValues']) {

            $requestHeader = [
                "Content-Type: application/json",
                "X-Auth-Token: api-key {$this->apiKey}",
            ];

            $curl = new Curl();
            $response = $curl->setOptions([
                CURLOPT_HTTPHEADER => $requestHeader,
                CURLOPT_POST => true,
                CURLOPT_POSTFIELDS => json_encode($postData),
                CURLOPT_TIMEOUT => self::TIMEOUT_SEC,
                CURLOPT_CONNECTTIMEOUT => self::TIMEOUT_CONNECT_SEC,
            ])->post($this->protocol . $this->host . $this->path . "/{$contactId}/custom-fields");

            $this->debugLog($curl, __METHOD__);

            return (!$curl->errorCode);
        }

        return false;
    }

    private function _addEmployeeContact(Employee $user, Company $company)
    {
        $grContact = new GetResponseContact();
        $grContact->employee_id = $user->id;
        $grContact->company_id = $company->id;
        $grContact->created_at = time();
        $grContact->updated_at = time();

        return $grContact->save();
    }

    // LOGS

    /**
     * @inheritdoc
     */
    public function debugLog(Curl $curl, $method)
    {
        if ($this->_debugMode) {
            $logFile = Yii::getAlias('@runtime/logs/debug_get_response.log');
            file_put_contents($logFile, $method . "\n" . self::curlLogMsg($curl) . "\n\n", FILE_APPEND);
        }
    }

    /**
     * @inheritdoc
     */
    public static function curlLogMsg(Curl $curl)
    {
        if (method_exists($curl, 'getDump')) {
            return $curl->getDump();
        } else {
            $data = [
                'curl_info' => curl_getinfo($curl->curl),
                'curl_postfields' => ArrayHelper::getValue($curl->getOptions(), CURLOPT_POSTFIELDS),
                'response_headers' => $curl->responseHeaders,
                'response' => $curl->response,
            ];
            if ($curl->errorCode || $curl->errorText) {
                $data['error_code'] = $curl->errorCode;
                $data['error_text'] = $curl->errorText;
            }

            return VarDumper::dumpAsString($data);
        }
    }
}