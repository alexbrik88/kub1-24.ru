<?php
namespace common\components;

use Yii;
use yii\db\Query;

/**
 * Class AddressHelper
 */
class AddressDictionaryHelper extends \yii\base\Component
{
    /**
     * table name
     * @var string
     */
    public static $table = 'address_dictionary';

    /**
     * `address_dictionary` row
     * @return array
     */
    public static function searchData($city, $street)
    {
        $table = static::$table;
        return (new Query)
            ->select('t2.ifnsul, t2.oktmo')
            ->from("{{{$table}}} {{t1}}")
            ->innerJoin("{{{$table}}} {{t2}}", '{{t2}}.[[PLAINCODE]] LIKE CONCAT({{t1}}.[[PLAINCODE]], "%")')
            ->where([
                't1.aolevel' => 4,
                't1.actstatus' => 1,
                't2.aolevel' => 7,
                't2.actstatus' => 1,
            ])
            ->andWhere([
                'or',
                ['t1.fullname' => $city],
                ['t1.formalname' => $city],
            ])
            ->andWhere([
                'or',
                ['t2.fullname' => $street],
                ['t2.formalname' => $street],
            ])
            ->one();
    }
}