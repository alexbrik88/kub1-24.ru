<?php
namespace common\components;

use common\models\Company;
use Yii;

/**
 * Class KubCompany
 */
class KubCompany extends Company
{
    public function init()
    {
        parent::init();

        $attributes = static::find()->where(['id' => Yii::$app->params['service']['company_id']])->asArray()->one();

        if ($attributes) {
            static::populateRecord($this, $attributes);
        } else {
            throw new \Exception('Service company not found.');
        }

        parent::afterFind();
    }
}