<?php

namespace common\components\floatLabelField;

use Yii;
use yii\base\Component;
use yii\base\ErrorHandler;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\base\InvalidArgumentException;
use yii\base\Model;
use yii\web\JsExpression;

/**
 * FloatLabelActiveField represents a material design input field within an [[ActiveForm]].
 */
class FloatLabelActiveField extends \yii\widgets\ActiveField
{
    public $inputCssClass = ['input-sm'];
    public $labelOptions = ['class' => ''];
    public $template = "{input}\n{label}\n{hint}\n{error}";

    protected $_attribute;

    public function init()
    {
        FloatLabelAsset::register(Yii::$app->getView());
        if (!preg_match(Html::$attributeRegex, $this->attribute, $matches)) {
            throw new InvalidArgumentException('Attribute name must contain word characters only.');
        }
        $this->_attribute = $matches[2];
        if ($this->model->{$this->_attribute}) {
            Html::addCssClass($this->inputOptions, 'edited');
        }

        parent::init();
    }

    /**
     * {@inheritdoc}
     */
    public function textInput($options = [])
    {
        Html::addCssClass($this->options, ['form-md-line-input', 'form-md-floating-label']);
        Html::addCssClass($this->inputOptions, $this->inputCssClass);
        if (strlen($this->model->{$this->_attribute}) > 0) {
            Html::addCssClass($this->inputOptions, 'edited');
        }

        return parent::textInput($options);
    }

    /**
     * {@inheritdoc}
     */
    public function passwordInput($options = [])
    {
        Html::addCssClass($this->options, ['form-md-line-input', 'form-md-floating-label']);
        Html::addCssClass($this->inputOptions, $this->inputCssClass);
        if (strlen($this->model->{$this->_attribute}) > 0) {
            Html::addCssClass($this->inputOptions, 'edited');
        }

        return parent::passwordInput($options);
    }

    /**
     * {@inheritdoc}
     */
    public function textarea($options = [])
    {
        Html::addCssClass($this->options, ['form-md-line-input', 'form-md-floating-label']);
        Html::addCssClass($this->inputOptions, $this->inputCssClass);
        if (strlen($this->model->{$this->_attribute}) > 0) {
            Html::addCssClass($this->inputOptions, 'edited');
        }

        return parent::textarea($options);
    }

    /**
     * {@inheritdoc}
     */
    public function dropDownList($items, $options = [])
    {
        Html::addCssClass($this->options, ['form-md-line-input', 'form-md-floating-label']);
        Html::addCssClass($this->inputOptions, $this->inputCssClass);

        return parent::dropDownList($items, $options);
    }

    /**
     * {@inheritdoc}
     */
    public function checkbox($options = [], $enclosedByLabel = true)
    {
        if ($enclosedByLabel) {
            $this->parts['{label}'] = '';
        } else {
            if (isset($options['label']) && !isset($this->parts['{label}'])) {
                $this->parts['{label}'] = $options['label'];
                if (!empty($options['labelOptions'])) {
                    $this->labelOptions = $options['labelOptions'];
                }
            }
            unset($options['labelOptions']);
            $options['label'] = null;
        }
        $this->parts['{input}'] = $this->renderCheckbox($options);
        $this->addAriaAttributes($options);
        $this->adjustLabelFor($options);

        return $this;
    }

    /**
     * {@inheritdoc}
     */
    public function renderCheckbox($options = [])
    {
        Html::removeCssClass($options, ['form-control', 'input-sm']);
        Html::addCssClass($options, ['md-check']);
        $labelContent = "<span class=\"inc\"></span>\n<span class=\"check\"></span>\n<span class=\"box\"></span>";
        if (!array_key_exists('label', $options)) {
            $labelText = Html::encode($this->model->getAttributeLabel(Html::getAttributeName($this->attribute)));
            $labelContent .= "\n$labelText";
        }
        $label = Html::label($labelContent, $this->getInputId());
        $options['label'] = null;
        $input = Html::activeCheckbox($this->model, $this->attribute, $options);

        return Html::tag('span', "$input\n$label", ['class' => 'md-checkbox']);
    }
}
