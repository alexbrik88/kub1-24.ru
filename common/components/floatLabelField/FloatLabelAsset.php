<?php

namespace common\components\floatLabelField;

use yii\web\AssetBundle;

/**
 * Class FloatLabelAsset
 * @package common\components\floatLabelField
 */
class FloatLabelAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@common/components/floatLabelField/assets';

    /**
     * @var array
     */
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
    ];

    /**
     * Registers the CSS and JS files with the given view.
     * @param \yii\web\View $view the view that the asset files are to be registered with.
     */
    public function registerAssetFiles($view)
    {
        $this->css[] = 'css/md' . (!YII_DEBUG ? '.min' : '') . '.css';
        $this->js[] = 'js/md' . (!YII_DEBUG ? '.min' : '') . '.js';
        parent::registerAssetFiles($view);
    }
}
