<?php
namespace common\components\contractor\behaviors;

use common\models\Contractor;
use frontend\modules\documents\components\InvoiceStatistic;
use yii\base\Behavior;

/**
 * Class CalculationBehavior
 * @package common\components\document\behaviors
 *
 * @property Contractor $owner
 *
 * Поведение CalculationBehavior обеспечивает функции получения колличесва счетов,
 * сумму для определённых статусов на начало и конец указанного отчётного периода.
 *
 * @property Array $notPaidStatisticInfo
 */
class CalculationBehavior extends Behavior
{
    /**
     * @var string с которого (и включая которое) числа нужно считать период
     */
    public $beginAt = null;

    /**
     * @var string по какое (и включая которое) число нужно считать период
     */
    public $endAt = null;

    /**
     * @return integer
     */
    public function getNotPaidStatisticSum()
    {
        return $this->getNotPaidStatisticInfo()['sum'];
    }

    /**
     * @return integer
     */
    public function getNotPaidStatisticCount()
    {
        return $this->getNotPaidStatisticInfo()['count'];
    }

    /**
     * @return integer
     */
    public function getPaidStatisticSum()
    {
        return $this->getPaidStatisticInfo()['sum'];
    }

    /**
     * @return integer
     */
    public function getPaidStatisticCount()
    {
        return $this->getPaidStatisticInfo()['count'];
    }

    /**
     * @return array|bool
     */
    public function getNotPaidStatisticInfo()
    {
        return $this->statisticInfo(InvoiceStatistic::NOT_PAID);
    }

    /**
     * @return array|bool
     */
    public function getPaidStatisticInfo()
    {
        return $this->statisticInfo(InvoiceStatistic::PAID);
    }

    /**
     * @param $statusesGroup
     *
     * @return array|bool
     */
    protected function statisticInfo($statusesGroup)
    {
        return InvoiceStatistic::getStatisticInfo(
            $this->owner->type,
            $this->owner->company_id,
            array_merge(InvoiceStatistic::$invoiceStatuses[$statusesGroup][$this->owner->type],
                InvoiceStatistic::$invoiceStatuses[InvoiceStatistic::NOT_PAID_IN_TIME][$this->owner->type]),
            [
                'from' => $this->beginAt,
                'to' => $this->endAt,
            ],
            $this->owner->id
        );
    }
}