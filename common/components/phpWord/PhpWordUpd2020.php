<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 01.04.2019
 * Time: 19:44
 */

namespace common\components\phpWord;


use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\document\Upd;
use common\models\product\Product;
use yii\base\Component;
use \PhpOffice\PhpWord\PhpWord;
use \PhpOffice\PhpWord\IOFactory;
use php_rutils\RUtils;
use PhpOffice\PhpWord\Settings;

/**
 * Class PhpWordUpd
 * @package common\components\phpWord
 */
class PhpWordUpd2020 extends Component
{
    /**
     * @var Upd
     */
    protected $model;
    /**
     * @var PhpWord
     */
    protected $phpWord;

    /**
     * @param Upd $model
     * @param array $config
     */
    public function __construct(Upd $model, $config = [])
    {
        $this->model = $model;
        $this->phpWord = new PhpWord();

        parent::__construct($config);
    }

    /**
     *
     */
    public function init()
    {
        parent::init();

        Settings::setOutputEscapingEnabled(true);

        $title = 'Универсальный передаточный документ';
        $model = $this->model;
        $model->isGroupOwnOrdersByProduct = true;

        $centerParStyle = [
            'spaceBefore' => 0,
            'spaceAfter' => 0,
            'spacing' => 0,
            'align' => 'center',
        ];
        $leftParStyle = [
            'spaceBefore' => 0,
            'spaceAfter' => 0,
            'spacing' => 0,
            'align' => 'left',
        ];
        $rightParStyle = [
            'spaceBefore' => 0,
            'spaceAfter' => 0,
            'spacing' => 0,
            'align' => 'right',
        ];
        $borderNoBottom = [
            'borderBottomSize' => 0,
            'borderBottomColor' => 'FFFFFF',
        ];
        $borderNoRight = [
            'borderRightSize' => 0,
            'borderRightColor' => 'FFFFFF',
        ];
        $borderBottom = [
            'borderBottomSize' => 6,
        ];
        $cellBottom = [
            'valign' => 'bottom'
        ];
        $cellCenter = [
            'valign' => 'center'
        ];
        $cellRowSpan = ['vMerge' => 'restart'];
        $cellRowContinue = ['vMerge' => 'continue'];
        $gridSpan2 = ['gridSpan' => 2];

        $documentDate = RUtils::dt()->ruStrFTime([
            'date' => $model->document_date,
            'format' => 'd F Y г.',
            'monthInflected' => true,
        ]);
        $invoice = $model->invoice;
        $hasNds = $invoice->hasNds;
        $consignor = $invoice->production_type ? (
        $model->consignor ?
            $model->consignor->shortName . ', ' . $model->consignor->legal_address :
            'он же'
        ) : Product::DEFAULT_VALUE;
        if ($model->contractor_address == Upd::CONTRACTOR_ADDRESS_LEGAL) {
            $address = $model->consignee ? $model->consignee->legal_address : $model->invoice->contractor->legal_address;
        } else {
            $address = $model->consignee ? $model->consignee->actual_address : $model->invoice->contractor->actual_address;
        }
        $consignee = $invoice->production_type ? ($model->consignee ?
            $model->consignee->shortName . ', ' . $address :
            $invoice->contractor_name_short . ', ' . $address
        ) : Product::DEFAULT_VALUE;
        $paymentDocuments = [];
        foreach ($model->paymentDocuments as $doc) {
            $date = date('d.m.Y', strtotime($doc->payment_document_date));
            $paymentDocuments[] = "№ {$doc->payment_document_number} от {$date}";
        }
        $paymentDocumentsText = $paymentDocuments ? implode(', ', $paymentDocuments) : Product::DEFAULT_VALUE;
        $contractorINN = $invoice->contractor->face_type == Contractor::TYPE_LEGAL_PERSON ?
            ($invoice->contractor_inn . '/' . $invoice->contractor_kpp) : '';
        $precision = $model->invoice->price_precision;
        $totalAmountNoNds = $hasNds ? $model->getPrintAmountNoNds() : $model->getPrintAmountWithNds();
        $totalNds = $hasNds ? $model->totalNds : 0;
        $base = null;
        if ($model->basis_document_name && $model->basis_document_number && $model->basis_document_date) {
            $base = $model->basis_document_name;
            $base .= " № {$model->basis_document_number}";
            $date = DateHelper::format($model->basis_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
            $base .= " от {$date}";
        } elseif ($invoice->basis_document_name && $invoice->basis_document_number && $invoice->basis_document_date) {
            $base = $invoice->basis_document_name;
            $base .= " № {$invoice->basis_document_number}";
            $date = DateHelper::format($invoice->basis_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
            $base .= " от {$date}";
        } else {
            $base = "Счет № {$invoice->fullNumber}";
            $date = DateHelper::format($invoice->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
            $base .= " от {$date}";
        }

        //Creating PhpWord ===============================================================================================================
        $phpWord = $this->phpWord;
        $phpWord->setDefaultFontName('Arial');
        $phpWord->setDefaultFontSize(6);

        $properties = $phpWord->getDocInfo();

        $properties->setTitle($title);
        $properties->setCreated(time());
        $properties->setModified(time());

        $section = $phpWord->addSection([
            'marginTop' => 567 / 2,
            'marginLeft' => 2 * 567,
            'marginRight' => 567,
            'marginBottom' => 567 / 2,
        ]);
        $sectionStyle = $section->getStyle();
        $sectionStyle->setOrientation($sectionStyle::ORIENTATION_LANDSCAPE);

        $subheadTable = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
            'cellMargin' => 6,
        ]);
        $subheadTable->addRow();
        $subheadTable
            ->addCell(1636, [
                'borderRightSize' => 12,
            ])
            ->addText("Универсальный передаточный документ", ['size' => 8], $leftParStyle);
        $subheadTable->addCell(500);
        $subheadCell = $subheadTable->addCell(3500);
        $subheadCell->addText("Счет-фактура № {$model->fullNumber} от {$documentDate} (1)", ['size' => 8], $leftParStyle);
        $subheadCell->addText("Исправление №________от_________(1а)", ['size' => 8], $leftParStyle);
        $subheadCell = $subheadTable->addCell(9500);
        $subheadCell->addText("Приложение №1", ['size' => 6], $rightParStyle);
        $subheadCell->addText("к постановлению Правительства Российской Федерации от 26 декабря 2011 г. № 1137", ['size' => 6], $rightParStyle);
        $subheadCell->addText("(в редакции постановления Правительства Российской Федерации от 19 августа 2017 г. № 981)", ['size' => 6], $rightParStyle);

        $headTable = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
            'cellMargin' => 0,
        ]);

        $headTable->addRow();
        $headTable
            ->addCell(1000, $cellCenter)
            ->addText("Статус:", ['size' => 8], $leftParStyle);
        $headTable->addCell(150, $cellBottom);
        $headTable
            ->addCell(336, [
                'borderTopSize' => 12,
                'borderBottomSize' => 12,
                'borderLeftSize' => 12,
                'borderRightSize' => 12,
                'valign' => 'center',
            ])
            ->addText($hasNds ? 1 : 2, ['size' => 8], $centerParStyle);
        $headTable->addCell(150, [
            'borderRightSize' => 12,
        ]);
        $headTable->addCell(500);
        $headTable
            ->addCell(3500, $cellBottom)
            ->addText('Продавец', ['size' => 8, 'bold' => true], $leftParStyle);
        $headTable
            ->addCell(9200, $cellBottom + $borderBottom)
            ->addText($invoice->company_name_short, ['size' => 8,], $leftParStyle);
        $headTable
            ->addCell(300, $cellBottom)
            ->addText('(2)', ['size' => 8,], $leftParStyle);

        $headTable->addRow();
        $headTableCell = $headTable->addCell(1636, [
            'gridSpan' => 4,
            'borderRightSize' => 12,
            'vMerge' => 'restart',
        ]);
        $headTableCell->addText("1 - счет-фактура и", ['size' => 6], $leftParStyle);
        $headTableCell->addText("передаточный", ['size' => 6], $leftParStyle);
        $headTableCell->addText("документ (акт)", ['size' => 6], $leftParStyle);
        $headTableCell->addText("2 - передаточный", ['size' => 6], $leftParStyle);
        $headTableCell->addText("документ (акт)", ['size' => 6], $leftParStyle);
        $headTable->addCell(500);
        $headTable
            ->addCell(3500, $cellBottom)
            ->addText('Адрес', ['size' => 8,], $leftParStyle);
        $headTable
            ->addCell(9200, $cellBottom + $borderBottom)
            ->addText($invoice->company_address_legal_full, ['size' => 8,], $leftParStyle);
        $headTable
            ->addCell(300, $cellBottom)
            ->addText('(2a)', ['size' => 8,], $leftParStyle);

        $headTable->addRow();
        $headTable->addCell(1636, [
            'gridSpan' => 4,
            'borderRightSize' => 12,
            'vMerge' => 'continue',
        ]);
        $headTable->addCell(500);
        $headTable
            ->addCell(3500, $cellBottom)
            ->addText('ИНН/КПП продавца', ['size' => 8,], $leftParStyle);
        $headTable
            ->addCell(9200, $cellBottom + $borderBottom)
            ->addText("{$invoice->company_inn}/{$invoice->company_kpp}", ['size' => 8,], $leftParStyle);
        $headTable
            ->addCell(300, $cellBottom)
            ->addText('(2б)', ['size' => 8,], $leftParStyle);

        $headTable->addRow();
        $headTable->addCell(1636, [
            'gridSpan' => 4,
            'borderRightSize' => 12,
        ]);
        $headTable->addCell(500);
        $headTable
            ->addCell(3500, $cellBottom)
            ->addText('Грузоотправитель и его адрес', ['size' => 8,], $leftParStyle);
        $headTable
            ->addCell(9200, $cellBottom + $borderBottom)
            ->addText($consignor, ['size' => 8,], $leftParStyle);
        $headTable
            ->addCell(300, $cellBottom)
            ->addText('(3)', ['size' => 8,], $leftParStyle);

        $headTable->addRow();
        $headTable->addCell(1636, [
            'gridSpan' => 4,
            'borderRightSize' => 12,
        ]);
        $headTable->addCell(500);
        $headTable
            ->addCell(3500, $cellBottom)
            ->addText('Грузополучатель и его адрес', ['size' => 8,], $leftParStyle);
        $headTable
            ->addCell(9200, $cellBottom + $borderBottom)
            ->addText($consignee, ['size' => 8,], $leftParStyle);
        $headTable
            ->addCell(300, $cellBottom)
            ->addText('(4)', ['size' => 8,], $leftParStyle);

        $headTable->addRow();
        $headTable->addCell(1636, [
            'gridSpan' => 4,
            'borderRightSize' => 12,
        ]);
        $headTable->addCell(500);
        $headTable
            ->addCell(3500, $cellBottom)
            ->addText('К платежно-расчетному документу', ['size' => 8,], $leftParStyle);
        $headTable
            ->addCell(9200, $cellBottom + $borderBottom)
            ->addText($paymentDocumentsText, ['size' => 8,], $leftParStyle);
        $headTable
            ->addCell(300, $cellBottom)
            ->addText('(5)', ['size' => 8,], $leftParStyle);

        $headTable->addRow();
        $headTable->addCell(1636, [
            'gridSpan' => 4,
            'borderRightSize' => 12,
        ]);
        $headTable->addCell(500);
        $headTable
            ->addCell(3500, $cellBottom)
            ->addText('Покупатель', ['size' => 8, 'bold' => true,], $leftParStyle);
        $headTable
            ->addCell(9200, $cellBottom + $borderBottom)
            ->addText($invoice->contractor_name_short, ['size' => 8,], $leftParStyle);
        $headTable
            ->addCell(300, $cellBottom)
            ->addText('(6)', ['size' => 8,], $leftParStyle);

        $headTable->addRow();
        $headTable->addCell(1636, [
            'gridSpan' => 4,
            'borderRightSize' => 12,
        ]);
        $headTable->addCell(500);
        $headTable
            ->addCell(3500, $cellBottom)
            ->addText('Адрес', ['size' => 8,], $leftParStyle);
        $headTable
            ->addCell(9200, $cellBottom + $borderBottom)
            ->addText($invoice->contractor_address_legal_full, ['size' => 8,], $leftParStyle);
        $headTable
            ->addCell(300, $cellBottom)
            ->addText('(6a)', ['size' => 8,], $leftParStyle);

        $headTable->addRow();
        $headTable->addCell(1636, [
            'gridSpan' => 4,
            'borderRightSize' => 12,
        ]);
        $headTable->addCell(500);
        $headTable
            ->addCell(3500, $cellBottom)
            ->addText('ИНН/КПП покупателя', ['size' => 8,], $leftParStyle);
        $headTable
            ->addCell(9200, $cellBottom + $borderBottom)
            ->addText($contractorINN, ['size' => 8,], $leftParStyle);
        $headTable
            ->addCell(300, $cellBottom)
            ->addText('(6б)', ['size' => 8,], $leftParStyle);

        $headTable->addRow();
        $headTable->addCell(1636, [
            'gridSpan' => 4,
            'borderRightSize' => 12,
        ]);
        $headTable->addCell(500);
        $headTable
            ->addCell(3500, $cellBottom)
            ->addText('Валюта: наименование, код', ['size' => 8,], $leftParStyle);
        $headTable
            ->addCell(9200, $cellBottom + $borderBottom)
            ->addText('Российский рубль, 643', ['size' => 8,], $leftParStyle);
        $headTable
            ->addCell(300, $cellBottom)
            ->addText('(7)', ['size' => 8,], $leftParStyle);

        $headTable->addRow();
        $headTable->addCell(1636, [
            'gridSpan' => 4,
            'borderRightSize' => 12,
        ]);
        $headTable->addCell(500);
        $headTable
            ->addCell(3500, $cellBottom)
            ->addText('Идентификатор государственного контракта, договора (соглашения)(при наличии)', ['size' => 8,], $leftParStyle);
        $headTable
            ->addCell(9200, $cellBottom + $borderBottom)
            ->addText($model->state_contract ? $model->state_contract : Product::DEFAULT_VALUE, ['size' => 8,], $leftParStyle);
        $headTable
            ->addCell(300, $cellBottom)
            ->addText('(8)', ['size' => 8,], $leftParStyle);

        $headTable->addRow(200);
        $headTable->addCell(1636, [
            'gridSpan' => 4,
            'borderRightSize' => 12,
        ]);
        $headTable->addCell(13500, [
            'gridSpan' => 4,
        ]);

        $productTable = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => '000000',
            'borderSize' => 12,
            'cellMargin' => 20,
        ]);
        $productColWidth = [
            1 => 818,
            2 => 818,
            3 => 1850,
            4 => 1064,
            5 => 700,
            6 => 1500,
            7 => 1186,
            8 => 600,
            9 => 650,
            10 => 1000,
            11 => 800,
            12 => 1100,
            13 => 700,
            14 => 800,
            15 => 750,
            16 => 750,
        ];
        $productTable->addRow();
        $productTable
            ->addCell($productColWidth[1], $cellCenter + $cellRowSpan)
            ->addText('N п/п', ['size' => 8], $centerParStyle);
        $productTable
            ->addCell($productColWidth[2], $cellCenter + $cellRowSpan)
            ->addText('Код товара/ работ, услуг', ['size' => 8], $centerParStyle);
        $productTable
            ->addCell($productColWidth[3], $cellCenter + $cellRowSpan)
            ->addText('Наименование товара (описание выполненных работ, оказанных услуг), имущественного права', ['size' => 8], $centerParStyle);
        $productTable
            ->addCell($productColWidth[4], $cellCenter + $cellRowSpan)
            ->addText('Код вида товара', ['size' => 8], $centerParStyle);
        $productTable
            ->addCell($productColWidth[5] + $productColWidth[6], $cellCenter + $gridSpan2)
            ->addText('Единица измерения', ['size' => 8], $centerParStyle);
        $productTable
            ->addCell($productColWidth[7], $cellCenter + $cellRowSpan)
            ->addText('Коли-чество (объем)', ['size' => 8], $centerParStyle);
        $productTable
            ->addCell($productColWidth[8], $cellCenter + $cellRowSpan)
            ->addText('Цена (тариф) за единицу измерения', ['size' => 8], $centerParStyle);
        $productTable
            ->addCell($productColWidth[9], $cellCenter + $cellRowSpan)
            ->addText('Стоимость товаров (работ, услуг), имущественных прав без налога - всего', ['size' => 8], $centerParStyle);
        $productTable
            ->addCell($productColWidth[10], $cellCenter + $cellRowSpan)
            ->addText('В том числе сумма акциза', ['size' => 8], $centerParStyle);
        $productTable
            ->addCell($productColWidth[11], $cellCenter + $cellRowSpan)
            ->addText('Налоговая ставка', ['size' => 8], $centerParStyle);
        $productTable
            ->addCell($productColWidth[12], $cellCenter + $cellRowSpan)
            ->addText('Сумма налога, предъявляемая покупателю', ['size' => 8], $centerParStyle);
        $productTable
            ->addCell($productColWidth[13], $cellCenter + $cellRowSpan)
            ->addText('Стоимость товаров(работ, услуг), имущественных прав с налогом - всего', ['size' => 8], $centerParStyle);
        $productTable
            ->addCell($productColWidth[14] + $productColWidth[15], $cellCenter + $gridSpan2)
            ->addText('Стоимость товаров(работ, услуг), имущественных прав с налогом - всего', ['size' => 8], $centerParStyle);
        $productTable
            ->addCell($productColWidth[16], $cellCenter + $cellRowSpan)
            ->addText('Регистрационный номер таможенной декларации', ['size' => 8], $centerParStyle);

        $productTable->addRow();
        $productTable->addCell($productColWidth[1], $cellRowContinue);
        $productTable->addCell($productColWidth[2], $cellRowContinue);
        $productTable->addCell($productColWidth[3], $cellRowContinue);
        $productTable->addCell($productColWidth[4], $cellRowContinue);
        $productTable
            ->addCell($productColWidth[5], $cellCenter)
            ->addText('код', ['size' => 8], $centerParStyle);
        $productTable
            ->addCell($productColWidth[6], $cellCenter)
            ->addText('условное обозначение (национальное)', ['size' => 8], $centerParStyle);
        $productTable->addCell($productColWidth[7], $cellRowContinue);
        $productTable->addCell($productColWidth[8], $cellRowContinue);
        $productTable->addCell($productColWidth[9], $cellRowContinue);
        $productTable->addCell($productColWidth[10], $cellRowContinue);
        $productTable->addCell($productColWidth[11], $cellRowContinue);
        $productTable->addCell($productColWidth[12], $cellRowContinue);
        $productTable->addCell($productColWidth[13], $cellRowContinue);
        $productTable
            ->addCell($productColWidth[14], $cellCenter)
            ->addText('Цифро-вой код', ['size' => 8], $centerParStyle);
        $productTable
            ->addCell($productColWidth[15], $cellCenter)
            ->addText('Краткое наименование', ['size' => 8], $centerParStyle);
        $productTable->addCell($productColWidth[16], $cellRowContinue);

        $productTable->addRow();
        $productTable->addCell($productColWidth[1], $cellBottom)->addText('A', ['size' => 7], $centerParStyle);
        $productTable->addCell($productColWidth[2], $cellBottom)->addText('Б', ['size' => 7], $centerParStyle);
        $productTable->addCell($productColWidth[3], $cellBottom)->addText(1, ['size' => 7], $centerParStyle);
        $productTable->addCell($productColWidth[4], $cellBottom)->addText('1a', ['size' => 7], $centerParStyle);
        $productTable->addCell($productColWidth[5], $cellBottom)->addText(2, ['size' => 7], $centerParStyle);
        $productTable->addCell($productColWidth[6], $cellBottom)->addText('2a', ['size' => 7], $centerParStyle);
        for ($i = 7; $i <= 14; $i++) {
            $productTable->addCell($productColWidth[$i], $cellBottom)->addText($i - 4, ['size' => 7], $centerParStyle);
        }
        $productTable->addCell($productColWidth[15], $cellBottom)->addText('10a', ['size' => 7], $centerParStyle);
        $productTable->addCell($productColWidth[16], $cellBottom)->addText('11', ['size' => 7], $centerParStyle);

        foreach ($model->ownOrders as $key => $orderUpd) {
            $order = $orderUpd->order;
            $product = $order->product;
            $hideUnits = ($product->production_type == Product::PRODUCTION_TYPE_SERVICE && !$model->show_service_units);

            $productColValue[1] = ++$key;
            $productColValue[2] = $product->code ?: Product::DEFAULT_VALUE;
            $productColValue[3] = htmlspecialchars($order->product_title);
            $productColValue[4] = $product->item_type_code ?: Product::DEFAULT_VALUE;
            $productColValue[5] = (!$hideUnits && $order->unit) ?
                $order->unit->code_okei :
                Product::DEFAULT_VALUE;
            $productColValue[6] = (!$hideUnits && $order->unit) ?
                $order->unit->name :
                Product::DEFAULT_VALUE;
            $productColValue[7] = ($hideUnits && $orderUpd->quantity == 1) ?
                Product::DEFAULT_VALUE :
                strtr($orderUpd->quantity, ['.' => ',']);
            $productColValue[8] = $hideUnits ?
                Product::DEFAULT_VALUE :
                TextHelper::invoiceMoneyFormat($orderUpd->priceNoNds, $precision);
            $productColValue[9] = TextHelper::invoiceMoneyFormat($model->getPrintOrderAmount($orderUpd->order_id, true), $precision);
            $productColValue[10] = $order->excise ?
                TextHelper::invoiceMoneyFormat($order->excise_price, 2) :
                'без акциза';
            $productColValue[11] = $hasNds ? $order->saleTaxRate->name : Product::DEFAULT_VALUE;
            $productColValue[12] = $hasNds ? TextHelper::invoiceMoneyFormat($orderUpd->amountNds, $precision) : Product::DEFAULT_VALUE;
            $productColValue[13] = TextHelper::invoiceMoneyFormat($model->getPrintOrderAmount($orderUpd->order_id, false), $precision);
            $productColValue[14] = $orderUpd->country->code ? : Product::DEFAULT_VALUE;
            $productColValue[15] = $orderUpd->country->name_short ? : Product::DEFAULT_VALUE;
            $productColValue[16] = $orderUpd->custom_declaration_number ? : Product::DEFAULT_VALUE;

            $productTable->addRow();
            for ($i = 1; $i <= 16; $i++) {
                $productTable
                    ->addCell($productColWidth[$i], $cellCenter)
                    ->addText($productColValue[$i], ['size' => 8], $i == 3 ? $leftParStyle : $centerParStyle);
            }
        }
        $productTable->addRow();
        $productTable->addCell($productColWidth[1], $cellCenter);
        $productTable->addCell($productColWidth[2], $cellCenter);
        $productTable
            ->addCell($productColWidth[3] + $productColWidth[4] + $productColWidth[5] + $productColWidth[6] + $productColWidth[7] + $productColWidth[8], $cellCenter + ['gridSpan' => 6])
            ->addText('Всего к оплате', ['size' => 8, 'bold' => true], $leftParStyle);
        $productTable
            ->addCell($productColWidth[9], $cellCenter)
            ->addText(TextHelper::invoiceMoneyFormat($totalAmountNoNds, 2), ['size' => 8,], $centerParStyle);
        $productTable
            ->addCell($productColWidth[10] + $productColWidth[11], $cellCenter + $gridSpan2)
            ->addText('X', ['size' => 8,], $centerParStyle);
        $productTable
            ->addCell($productColWidth[12], $cellCenter)
            ->addText(TextHelper::invoiceMoneyFormat($totalNds, 2), ['size' => 8,], $centerParStyle);
        $productTable
            ->addCell($productColWidth[13], $cellCenter)
            ->addText(TextHelper::invoiceMoneyFormat($model->getPrintAmountWithNds(), 2), ['size' => 8,], $centerParStyle);
        $productTable
            ->addCell($productColWidth[14] + $productColWidth[15] + $productColWidth[16], $cellCenter + ['gridSpan' => 3]);

        $preFooterTable = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
            'cellMargin' => 0,
        ]);
        $preFooterTable->addRow(300);
        $preFooterTable
            ->addCell(1636, [
                'borderRightSize' => 12,
            ]);
        $preFooterTable->addCell(13500, [
            'gridSpan' => 10,
        ]);

        $preFooterTable->addRow();
        $preFooterTable
            ->addCell(1636, [
                'borderRightSize' => 12,
            ])
            ->addText('Документ составлен на 1 листах', ['size' => 8], $leftParStyle);
        $preFooterTable->addCell(500);
        $preFooterCell = $preFooterTable->addCell(3000, $cellBottom);
        $preFooterCell->addText('Руководитель организации или', ['size' => 8,], $leftParStyle);
        $preFooterCell->addText('иное уполномоченное лицо', ['size' => 8,], $leftParStyle);
        $preFooterTable
            ->addCell(1500, $cellBottom + $borderBottom)
            ->addText('', ['size' => 8], $leftParStyle);
        $preFooterTable->addCell(100);
        $preFooterCell = $preFooterTable->addCell(2200, $cellBottom + $borderBottom);
        if ($invoice->company->company_type_id != CompanyType::TYPE_IP) {
            $preFooterCell->addText($model->signed_by_name ?
                $model->signed_by_name :
                $invoice->getCompanyChiefFio(true), ['size' => 8,], $leftParStyle);
            if ($model->signed_by_employee_id) {
                $document = 'по ' . ($model->signBasisDocument ? mb_strtolower($model->signBasisDocument->name2) : 'доверенности')
                    . ' №' . $model->sign_document_number;
                $date = 'от ' . DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . 'г.';
                $preFooterCell->addText($document, ['size' => 8], $leftParStyle);
                $preFooterCell->addText($date, ['size' => 8], $leftParStyle);
            }
        }
        $preFooterTable->addCell(100);
        $preFooterCell = $preFooterTable->addCell(2600, $cellBottom);
        $preFooterCell->addText('Главный бухгалтер или', ['size' => 8,], $leftParStyle);
        $preFooterCell->addText('иное уполномоченное лицо', ['size' => 8,], $leftParStyle);
        $preFooterTable
            ->addCell(1500, $cellBottom + $borderBottom)
            ->addText('', ['size' => 8], $leftParStyle);
        $preFooterTable->addCell(100);
        $preFooterCell = $preFooterTable->addCell(2200, $cellBottom + $borderBottom);
        if ($invoice->company->company_type_id != CompanyType::TYPE_IP) {
            $preFooterCell->addText($model->signed_by_name ?
                $model->signed_by_name :
                $invoice->getCompanyChiefAccountantFio(true), ['size' => 8,], $leftParStyle);
            if ($model->signed_by_employee_id) {
                $document = 'по ' . mb_strtolower($model->signBasisDocument->name2) . ' №' . $model->sign_document_number;
                $date = 'от ' . DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . 'г.';
                $preFooterCell->addText($document, ['size' => 8], $leftParStyle);
                $preFooterCell->addText($date, ['size' => 8], $leftParStyle);
            }
        }
        $preFooterTable->addRow();
        $preFooterTable->addCell(1636, [
            'borderRightSize' => 12,
        ]);
        $preFooterTable->addCell(500, $cellCenter);
        $preFooterTable->addCell(3000, $cellCenter);
        $preFooterTable
            ->addCell(1500, $cellCenter)
            ->addText('(подпись)', ['size' => 6], $centerParStyle);
        $preFooterTable->addCell(100, $cellCenter);
        $preFooterTable
            ->addCell(2200, $cellCenter)
            ->addText('(ф.и.о.)', ['size' => 6], $centerParStyle);
        $preFooterTable->addCell(100, $cellCenter);
        $preFooterTable->addCell(2600, $cellCenter);
        $preFooterTable
            ->addCell(1500, $cellCenter)
            ->addText('(подпись)', ['size' => 6], $centerParStyle);
        $preFooterTable->addCell(100, $cellCenter);
        $preFooterTable
            ->addCell(2200, $cellCenter)
            ->addText('(ф.и.о.)', ['size' => 6], $centerParStyle);

        $preFooterTable->addRow();
        $preFooterTable->addCell(1636, [
            'borderRightSize' => 12,
        ]);
        $preFooterTable->addCell(500);
        $preFooterCell = $preFooterTable->addCell(3000, $cellBottom);
        $preFooterCell->addText('Индивидуальный предприниматель или', ['size' => 8,], $leftParStyle);
        $preFooterCell->addText('иное уполномоченное лицо', ['size' => 8,], $leftParStyle);
        $preFooterTable
            ->addCell(1500, $cellBottom + $borderBottom)
            ->addText('', ['size' => 8], $leftParStyle);
        $preFooterTable->addCell(100);
        $preFooterTable
            ->addCell(2200, $cellBottom + $borderBottom)
            ->addText($model->invoice->company->company_type_id == CompanyType::TYPE_IP ?
                $model->invoice->getCompanyChiefFio(true) : null, ['size' => 8], $leftParStyle);
        $preFooterTable->addCell(100);
        $preFooterTable
            ->addCell(6500, $cellBottom + $borderBottom + ['gridSpan' => 4])
            ->addText($model->invoice->company->certificate, ['size' => 8], $leftParStyle);

        $preFooterTable->addRow();
        $preFooterTable->addCell(1636, [
            'borderRightSize' => 12,
        ]);
        $preFooterTable->addCell(500, $cellCenter);
        $preFooterTable->addCell(3000, $cellCenter);
        $preFooterTable
            ->addCell(1500, $cellCenter)
            ->addText('(подпись)', ['size' => 6], $centerParStyle);
        $preFooterTable->addCell(100, $cellCenter);
        $preFooterTable
            ->addCell(2200, $cellCenter)
            ->addText('(ф.и.о.)', ['size' => 6], $centerParStyle);
        $preFooterTable->addCell(100, $cellCenter);
        $preFooterTable
            ->addCell(6500, $cellCenter + ['gridSpan' => 4])
            ->addText('(реквизиты свидетельства о государственной регистрации индивидуального предпринимателя)', ['size' => 6], $centerParStyle);

        $preFooterTable->addRow(300);
        $preFooterTable
            ->addCell(1636, [
                'borderRightSize' => 12,
            ]);
        $preFooterTable->addCell(13500, [
            'gridSpan' => 10,
            'borderBottomSize' => 12,
        ]);

        $section->addTextBreak(1, [], $leftParStyle);

        $footerTable = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
            'cellMargin' => 0,
        ]);

        $footerTable->addRow();
        $footerTable
            ->addCell(4000, $cellBottom)
            ->addText('Основание передачи (сдачи) / получения (приемки)', ['size' => 8,], $leftParStyle);
        $footerTable
            ->addCell(10700, $cellBottom + $borderBottom)
            ->addText($base, ['size' => 8,], $leftParStyle);
        $footerTable->addCell(136, $cellBottom);
        $footerTable
            ->addCell(200, $cellBottom)
            ->addText('[8]', ['size' => 8], $leftParStyle);

        $footerTable->addRow();
        $footerTable->addCell(4000, $cellCenter);
        $footerTable
            ->addCell(10700, $cellCenter)
            ->addText('(договор; доверенность и др.)', ['size' => 6,], $centerParStyle);
        $footerTable->addCell(136, $cellBottom);
        $footerTable->addCell(200, $cellBottom);

        $footerTable->addRow();
        $footerTable
            ->addCell(4000, $cellBottom)
            ->addText('Данные о транспортировке и грузе', ['size' => 8,], $leftParStyle);
        $footerTable
            ->addCell(10700, $cellBottom + $borderBottom)
            ->addText(($model->waybill_date && $model->waybill_number) ?
                ('транспортная накладная № ' . $model->waybill_number . ' от ' .
                    date_create_from_format('Y-m-d', $model->waybill_date)->format('d.m.Y')) : '', ['size' => 8,], $leftParStyle);
        $footerTable->addCell(136, $cellBottom);
        $footerTable
            ->addCell(200, $cellBottom)
            ->addText('[9]', ['size' => 8], $leftParStyle);

        $footerTable->addRow();
        $footerTable->addCell(4000, $cellCenter);
        $footerTable
            ->addCell(10700, $cellCenter)
            ->addText('(транспортная накладная, поручение экспедитору, экспедиторская / складская расписка и др. / масса нетто/брутто груза, если не приведены ссылки на транспортные документы, содержащие эти сведения)', ['size' => 6,], $centerParStyle);
        $footerTable->addCell(136, $cellBottom);
        $footerTable->addCell(200, $cellBottom);

        $section->addTextBreak(1, [], $leftParStyle);

        $footerTable = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
            'cellMargin' => 0,
        ]);
        $footerTable->addRow();
        $footerTable
            ->addCell(7500, [
                'borderRightSize' => 12,
            ])
            ->addText('Товар (груз) передал / услуги, результаты работ, права сдал', ['size' => 8,], $leftParStyle);
        $footerTable->addCell(136);
        $footerTable
            ->addCell(7500)
            ->addText('Товар (груз) получил / услуги, результаты работ, права принял', ['size' => 8,], $leftParStyle);

        $footerTable = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
            'cellMargin' => 0,
        ]);
        $footerTable->addRow();
        $footerTable
            ->addCell(3050, $cellBottom + $borderBottom)
            ->addText(($invoice->signEmployeeCompany) ? $invoice->signEmployeeCompany->position : $invoice->company_chief_post_name, ['size' => 8,], $leftParStyle);
        $footerTable->addCell(100, $cellBottom);
        $footerTable->addCell(1600, $cellBottom + $borderBottom);
        $footerTable->addCell(100, $cellBottom);
        $footerTable
            ->addCell(2150, $cellBottom + $borderBottom)
            ->addText($invoice->getCompanyChiefFio(true), ['size' => 8,], $leftParStyle);
        $footerTable->addCell(100, $cellBottom);
        $footerTable
            ->addCell(400, $cellBottom + ['borderRightSize' => 12,])
            ->addText('[10]', ['size' => 8,], $centerParStyle);

        $footerTable->addCell(136);
        $footerTable->addCell(3050, $cellBottom + $borderBottom);
        $footerTable->addCell(100, $cellBottom);
        $footerTable->addCell(1600, $cellBottom + $borderBottom);
        $footerTable->addCell(100, $cellBottom);
        $footerTable->addCell(2150, $cellBottom + $borderBottom);
        $footerTable->addCell(100, $cellBottom);
        $footerTable
            ->addCell(264, $cellBottom)
            ->addText('[15]', ['size' => 8,], $centerParStyle);

        $footerTable->addRow();
        $footerTable
            ->addCell(3050, $cellCenter)
            ->addText('(должность)', ['size' => 6,], $centerParStyle);
        $footerTable->addCell(100, $cellCenter);
        $footerTable
            ->addCell(1600, $cellCenter)
            ->addText('подпись', ['size' => 6,], $centerParStyle);
        $footerTable->addCell(100, $cellCenter);
        $footerTable
            ->addCell(2150, $cellCenter)
            ->addText('(ф.и.о)', ['size' => 6,], $centerParStyle);
        $footerTable->addCell(100, $cellCenter);
        $footerTable->addCell(400, $cellCenter + ['borderRightSize' => 12,]);

        $footerTable->addCell(136);
        $footerTable
            ->addCell(3050, $cellCenter)
            ->addText('(должность)', ['size' => 6,], $centerParStyle);
        $footerTable->addCell(100, $cellCenter);
        $footerTable
            ->addCell(1600, $cellCenter)
            ->addText('подпись', ['size' => 6,], $centerParStyle);
        $footerTable->addCell(100, $cellCenter);
        $footerTable
            ->addCell(2150, $cellCenter)
            ->addText('(ф.и.о)', ['size' => 6,], $centerParStyle);
        $footerTable->addCell(100, $cellCenter);
        $footerTable->addCell(264, $cellCenter);

        $footerTable->addRow();
        $footerTable
            ->addCell(3050, $cellBottom)
            ->addText('Дата отгрузки, передачи (сдачи)', ['size' => 8,], $leftParStyle);
        $footerTable->addCell(100, $cellBottom);
        $footerTable
            ->addCell(3850, $cellBottom + $borderBottom + ['gridSpan' => 3])
            ->addText($documentDate, ['size' => 8,], $leftParStyle);
        $footerTable->addCell(100, $cellBottom);
        $footerTable
            ->addCell(400, $cellBottom + ['borderRightSize' => 12,])
            ->addText('[11]', ['size' => 8,], $centerParStyle);

        $footerTable->addCell(136);
        $footerTable
            ->addCell(3050, $cellBottom)
            ->addText('Дата получения (приемки)', ['size' => 8,], $leftParStyle);
        $footerTable->addCell(100, $cellBottom);
        $footerTable->addCell(3850, $cellBottom + $borderBottom + ['gridSpan' => 3]);
        $footerTable->addCell(100, $cellBottom);
        $footerTable
            ->addCell(264, $cellBottom)
            ->addText('[16]', ['size' => 8,], $centerParStyle);

        $footerTable->addRow(200);
        $footerTable
            ->addCell(7500, [
                'borderRightSize' => 12,
                'gridSpan' => 7,
            ]);
        $footerTable->addCell(136);
        $footerTable
            ->addCell(7500, [
                'gridSpan' => 7,
            ]);

        $footerTable->addRow();
        $footerTable
            ->addCell(7500, [
                'borderRightSize' => 12,
                'gridSpan' => 7,
            ])
            ->addText('Иные сведения об отгрузке, передаче', ['size' => 8,], $leftParStyle);
        $footerTable->addCell(136);
        $footerTable
            ->addCell(7500, [
                'gridSpan' => 7,
            ])
            ->addText('Иные сведения об отгрузке, передаче', ['size' => 8,], $leftParStyle);

        $footerTable->addRow();
        $footerTable->addCell(7000, $cellBottom + $borderBottom + [
                'gridSpan' => 5,
            ]);
        $footerTable->addCell(100, $cellBottom);
        $footerTable
            ->addCell(400, $cellBottom + ['borderRightSize' => 12,])
            ->addText('[12]', ['size' => 8,], $centerParStyle);

        $footerTable->addCell(136);
        $footerTable->addCell(7000, $cellBottom + $borderBottom + [
                'gridSpan' => 5,
            ]);
        $footerTable->addCell(100, $cellBottom);
        $footerTable
            ->addCell(264, $cellBottom)
            ->addText('[17]', ['size' => 8,], $centerParStyle);

        $footerTable->addRow();
        $footerTable
            ->addCell(7000, $cellCenter + [
                    'gridSpan' => 5,
                ])
            ->addText('(ссылки на неотъемлемые приложения, сопутствующие документы, иные документы и т.п.)', ['size' => 6], $centerParStyle);
        $footerTable->addCell(100, $cellCenter);
        $footerTable->addCell(400, $cellCenter + ['borderRightSize' => 12,]);

        $footerTable->addCell(136);
        $footerTable
            ->addCell(7000, $cellCenter + [
                    'gridSpan' => 5,
                ])
            ->addText('(ссылки на неотъемлемые приложения, сопутствующие документы, иные документы и т.п.)', ['size' => 6], $centerParStyle);
        $footerTable->addCell(100, $cellBottom);
        $footerTable->addCell(264, $cellBottom);

        $footerTable->addRow();
        $footerTable
            ->addCell(7500, [
                'borderRightSize' => 12,
                'gridSpan' => 7,
            ])
            ->addText('Ответственный за правильность оформления факта хозяйственной жизни', ['size' => 8,], $leftParStyle);
        $footerTable->addCell(136);
        $footerTable
            ->addCell(7500, [
                'gridSpan' => 7,
            ])
            ->addText('Ответственный за правильность оформления факта хозяйственной жизни', ['size' => 8,], $leftParStyle);

        $footerTable->addRow();
        $footerTable
            ->addCell(3050, $cellBottom + $borderBottom)
            ->addText(($invoice->signEmployeeCompany) ? $invoice->signEmployeeCompany->position : $invoice->company_chief_post_name, ['size' => 8,], $leftParStyle);
        $footerTable->addCell(100, $cellBottom);
        $footerTable->addCell(1600, $cellBottom + $borderBottom);
        $footerTable->addCell(100, $cellBottom);
        $footerTable
            ->addCell(2150, $cellBottom + $borderBottom)
            ->addText($invoice->getCompanyChiefFio(true), ['size' => 8,], $leftParStyle);
        $footerTable->addCell(100, $cellBottom);
        $footerTable
            ->addCell(400, $cellBottom + ['borderRightSize' => 12,])
            ->addText('[13]', ['size' => 8,], $centerParStyle);

        $footerTable->addCell(136);
        $footerTable->addCell(3050, $cellBottom + $borderBottom);
        $footerTable->addCell(100, $cellBottom);
        $footerTable->addCell(1600, $cellBottom + $borderBottom);
        $footerTable->addCell(100, $cellBottom);
        $footerTable->addCell(2150, $cellBottom + $borderBottom);
        $footerTable->addCell(100, $cellBottom);
        $footerTable
            ->addCell(264, $cellBottom)
            ->addText('[18]', ['size' => 8,], $centerParStyle);

        $footerTable->addRow();
        $footerTable
            ->addCell(3050, $cellCenter)
            ->addText('(должность)', ['size' => 6,], $centerParStyle);
        $footerTable->addCell(100, $cellCenter);
        $footerTable
            ->addCell(1600, $cellCenter)
            ->addText('подпись', ['size' => 6,], $centerParStyle);
        $footerTable->addCell(100, $cellCenter);
        $footerTable
            ->addCell(2150, $cellCenter)
            ->addText('(ф.и.о)', ['size' => 6,], $centerParStyle);
        $footerTable->addCell(100, $cellCenter);
        $footerTable->addCell(400, $cellCenter + ['borderRightSize' => 12,]);

        $footerTable->addCell(136);
        $footerTable
            ->addCell(3050, $cellCenter)
            ->addText('(должность)', ['size' => 6,], $centerParStyle);
        $footerTable->addCell(100, $cellCenter);
        $footerTable
            ->addCell(1600, $cellCenter)
            ->addText('подпись', ['size' => 6,], $centerParStyle);
        $footerTable->addCell(100, $cellCenter);
        $footerTable
            ->addCell(2150, $cellCenter)
            ->addText('(ф.и.о)', ['size' => 6,], $centerParStyle);
        $footerTable->addCell(100, $cellCenter);
        $footerTable->addCell(264, $cellCenter);

        $footerTable->addRow();
        $footerTable
            ->addCell(7500, [
                'borderRightSize' => 12,
                'gridSpan' => 7,
            ])
            ->addText('Наименование экономического субъекта – составителя документа (в т.ч. комиссионера / агента)', ['size' => 8,], $leftParStyle);
        $footerTable->addCell(136);
        $footerTable
            ->addCell(7500, [
                'gridSpan' => 7,
            ])
            ->addText('Наименование экономического субъекта – составителя документа (в т.ч. комиссионера / агента)', ['size' => 8,], $leftParStyle);

        $footerTable->addRow();
        $footerTable
            ->addCell(7000, $cellBottom + $borderBottom + [
                    'gridSpan' => 5,
                ])
            ->addText("$invoice->company_name_short, ИНН/КПП $invoice->company_inn/$invoice->company_kpp", ['size' => 8,], $leftParStyle);
        $footerTable->addCell(100, $cellBottom);
        $footerTable
            ->addCell(400, $cellBottom + ['borderRightSize' => 12,])
            ->addText('[14]', ['size' => 8,], $centerParStyle);

        $footerTable->addCell(136);
        $economicEntity = $invoice->contractor_name_short .
            (($invoice->contractor->face_type == Contractor::TYPE_LEGAL_PERSON) ?
                ', ИНН/КПП ' . $invoice->contractor_inn . '/' . $invoice->contractor_kpp : '');
        $footerTable
            ->addCell(7000, $cellBottom + $borderBottom + [
                'gridSpan' => 5,
            ])
            ->addText($economicEntity, ['size' => 8,], $leftParStyle);
        $footerTable->addCell(100, $cellBottom);
        $footerTable
            ->addCell(264, $cellBottom)
            ->addText('[19]', ['size' => 8,], $centerParStyle);

        $footerTable->addRow();
        $footerTable
            ->addCell(7000, $cellCenter + [
                    'gridSpan' => 5,
                ])
            ->addText('(может не заполняться при проставлении печати в М.П., может быть указан ИНН / КПП)', ['size' => 6], $centerParStyle);
        $footerTable->addCell(100, $cellCenter);
        $footerTable->addCell(400, $cellCenter + ['borderRightSize' => 12,]);

        $footerTable->addCell(136);
        $footerTable
            ->addCell(7000, $cellCenter + [
                    'gridSpan' => 5,
                ])
            ->addText('(может не заполняться при проставлении печати в М.П., может быть указан ИНН / КПП)', ['size' => 6], $centerParStyle);
        $footerTable->addCell(100, $cellBottom);
        $footerTable->addCell(264, $cellBottom);

        $footerTable->addRow();
        $footerTable
            ->addCell(7500, [
                'borderRightSize' => 12,
                'gridSpan' => 7,
            ])
            ->addText('M.П.', ['size' => 8,], $centerParStyle);
        $footerTable->addCell(136);
        $footerTable
            ->addCell(7500, [
                'gridSpan' => 7,
            ])
            ->addText('M.П.', ['size' => 8,], $centerParStyle);

    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->model->getPrintTitle() . '.docx';
    }

    /**
     * @return mixed
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    public function getFile()
    {
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=\"{$this->fileName}\"");
        header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Expires: 0');

        $objWriter = IOFactory::createWriter($this->phpWord, 'Word2007');

        $objWriter->save("php://output");

        exit;
    }

    /**
     * @param $path
     * @return mixed
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    public function saveFile($path)
    {
        $objWriter = IOFactory::createWriter($this->phpWord, 'Word2007');

        return $objWriter->save($path);
    }
}