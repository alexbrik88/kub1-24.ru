<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 12.12.2018
 * Time: 1:56
 */

namespace common\components\phpWord;


use common\components\date\DateHelper;
use common\components\image\EasyThumbnailImage;
use common\components\TextHelper;
use common\models\vehicle\VehicleType;
use PhpOffice\PhpWord\IOFactory;
use yii\base\Component;
use common\models\logisticsRequest\LogisticsRequest;
use PhpOffice\PhpWord\PhpWord;

/**
 * Class PhpWordLogisticsRequest
 * @package common\components\phpWord
 */
class PhpWordLogisticsRequest extends Component
{
    /* @var $model LogisticsRequest */
    protected $model;

    /* @var $phpWord PhpWord */
    protected $phpWord;

    /* @var int */
    protected $requestType;

    /**
     * PhpWordLogisticsRequest constructor.
     * @param LogisticsRequest $model
     * @param int $requestType
     * @param array $config
     */
    public function __construct(LogisticsRequest $model, $requestType = LogisticsRequest::TYPE_CUSTOMER, $config = [])
    {
        $this->model = $model;
        $this->phpWord = new PhpWord();
        $this->requestType = $requestType;

        parent::__construct($config);
    }

    /**
     * @throws \himiklab\thumbnail\FileNotFoundException
     * @throws \yii\web\NotFoundHttpException
     */
    public function init()
    {
        parent::init();

        $model = $this->model;
        $title = $model->getTitle($this->requestType);
        $headerCompanyName = $this->requestType == LogisticsRequest::TYPE_CARRIER ?
            $model->company->getTitle(true) : $model->customer->getNameWithType();
        $logo = !($logoImage = $model->company->getImage('logoImage')) ? null :
            EasyThumbnailImage::thumbnailFile($logoImage, 150, 50, EasyThumbnailImage::THUMBNAIL_INSET);
        if ($this->requestType == LogisticsRequest::TYPE_CARRIER) {
            $contractor = $model->carrier;
            $agreement = 'carrierAgreement';
            $prefix = 'carrier';
        } else {
            $contractor = $model->customer;
            $agreement = 'customerAgreement';
            $prefix = 'customer';
        }
        $contactPersonAttribute = $prefix . '_contact_person';
        $formID = $prefix . '_form_id';
        $form = $prefix . 'Form';
        $conditionID = $prefix . '_condition_id';
        $condition = $prefix . 'Condition';
        $delay = $prefix . '_delay';
        $rate = $prefix . '_rate';
        $essence = $prefix . '_request_essence';
        $directorName = null;
        if (!empty($contractor->director_name) && $contractor->director_name != 'ФИО Руководителя') {
            $directorName = TextHelper::nameShort($contractor->director_name);
        }

        $phpWord = $this->phpWord;
        $phpWord->setDefaultFontName('Arial');
        $phpWord->setDefaultFontSize(10);

        $properties = $phpWord->getDocInfo();

        $properties->setTitle($title);
        $properties->setCreated(time());
        $properties->setModified(time());

        $section = $phpWord->addSection([
            'marginTop' => 600,
            'marginLeft' => 900,
            'marginRight' => 900,
        ]);

        // paragraph styles with align and empty spaces
        $centerParStyle = [
            'spaceBefore' => 0,
            'spaceAfter' => 0,
            'spacing' => 0,
            'align' => 'center',
        ];
        $leftParStyle = [
            'spaceBefore' => 0,
            'spaceAfter' => 0,
            'spacing' => 0,
            'align' => 'left',
        ];
        $rightParStyle = [
            'spaceBefore' => 0,
            'spaceAfter' => 0,
            'spacing' => 0,
            'align' => 'right',
        ];
        $tableStyle = [
            'bidiVisual' => 0,
            'cellMarginTop' => 30,
            'cellMarginBottom' => 30,
            'cellMarginLeft' => 50,
            'cellMarginRight' => 50,
            'borderColor' => '000000',
            'borderTopSize' => 12,
            'borderBottomSize' => 12,
            'borderLeftSize' => 12,
            'borderRightSize' => 12,
            'borderInsideHSize' => 6,
            'borderInsideVSize' => 6,
        ];

        $headTable = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
            'cellMargin' => 50,
        ]);
        $headTable->addRow(850);
        $headTable->addCell(7500)
            ->addText($headerCompanyName, ['size' => 12, 'bold' => true], ['align' => 'left', 'spaceBefore' => 100, 'spaceAfter' => 0]);
        $logoCell = $headTable->addCell(2800);
        if ($this->requestType == LogisticsRequest::TYPE_CARRIER && $logo && is_file($logo)) {
            $logoCell->addImage($logo, ['align' => 'right']);
        }

        $section->addTextBreak(1, [], $leftParStyle);

        $requestTitleTable = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
        ]);

        $requestTitleTable->addRow(100);
        $requestTitleTable->addCell(10300)
            ->addText($model->getTitle($this->requestType), ['size' => 11, 'bold' => true], $centerParStyle);

        $section->addTextBreak(1, [], $leftParStyle);

        $topTable = $section->addTable($tableStyle);

        $topTable->addRow();
        $topTable->addCell(5150, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF'])
            ->addText('Заказчик', ['bold' => true], $leftParStyle);
        $topTable->addCell(5150, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF'])
            ->addText('Исполнитель', ['bold' => true], $leftParStyle);

        $topTable->addRow();
        $topTable->addCell(5150, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF', 'borderTopSize' => 0, 'borderTopColor' => 'FFFFFF'])
            ->addText($this->requestType == LogisticsRequest::TYPE_CARRIER ?
                $model->company->getTitle(true) : $contractor->getNameWithType(), [], $leftParStyle);
        $topTable->addCell(5150, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF', 'borderTopSize' => 0, 'borderTopColor' => 'FFFFFF'])
            ->addText($this->requestType == LogisticsRequest::TYPE_CARRIER ?
                $contractor->getNameWithType() : $model->company->getTitle(true), [], $leftParStyle);

        $topTable->addRow();
        $topTable->addCell(5150, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF', 'borderTopSize' => 0, 'borderTopColor' => 'FFFFFF'])
            ->addText('тел: ' . ($this->requestType == LogisticsRequest::TYPE_CARRIER ?
                    $model->company->phone : $contractor->getRealContactPhone()), [], $leftParStyle);
        $topTable->addCell(5150, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF', 'borderTopSize' => 0, 'borderTopColor' => 'FFFFFF'])
            ->addText('тел: ' . ($this->requestType == LogisticsRequest::TYPE_CARRIER ?
                    $contractor->getRealContactPhone() : $model->company->phone), [], $leftParStyle);

        $topTable->addRow();
        $topTable->addCell(5150, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF', 'borderTopSize' => 0, 'borderTopColor' => 'FFFFFF'])
            ->addText('ФИО: ' . ($this->requestType == LogisticsRequest::TYPE_CARRIER ?
                    $model->company->getChiefFio(true) : $contractor->getDirectorFio()), [], $leftParStyle);
        $topTable->addCell(5150, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF', 'borderTopSize' => 0, 'borderTopColor' => 'FFFFFF'])
            ->addText('ФИО: ' . ($this->requestType == LogisticsRequest::TYPE_CARRIER ?
                    $contractor->getDirectorFio() : $model->company->getChiefFio(true)), [], $leftParStyle);

        $topTable = $section->addTable($tableStyle);

        $topTable->addRow();
        $topTable->addCell(2575, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',])
            ->addText('Маршрут', ['bold' => true], $leftParStyle);
        $topTable->addCell(7725, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',])
            ->addText($model->loading->city . ' - ' . $model->unloading->city, [], $leftParStyle);

        $topTable->addRow();
        $topTable->addCell(2575, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',])
            ->addText('Погрузка', ['bold' => true], $leftParStyle);
        $topTable->addCell(7725, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',])
            ->addText(($model->loading->date ?
                    DateHelper::format($model->loading->date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) : null) . ' ' .
                ($model->loading->time ?
                    DateHelper::format($model->loading->time, 'H:i', 'H:i:s') : null), [], $leftParStyle);

        $topTable->addRow();
        $topTable->addCell(2575, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',])
            ->addText('Адрес', [], $leftParStyle);
        $topTable->addCell(7725, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',])
            ->addText($model->loading->city . ' ' . $model->loading->address, [], $leftParStyle);

        $topTable = $section->addTable($tableStyle);

        $topTable->addRow();
        $topTable->addCell(2575, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',])
            ->addText('Контактное лицо', [], $leftParStyle);
        $topTable->addCell(2575, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',])
            ->addText($model->loading->contact_person, [], $leftParStyle);
        $topTable->addCell(2575, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',])
            ->addText('Телефон', [], $leftParStyle);
        $topTable->addCell(2575, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',])
            ->addText($model->loading->contact_person_phone, [], $leftParStyle);

        $topTable = $section->addTable($tableStyle);

        $topTable->addRow();
        $topTable->addCell(2575, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',])
            ->addText('Разгрузка', ['bold' => true], $leftParStyle);
        $topTable->addCell(7725, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',])
            ->addText(($model->unloading->date ?
                    DateHelper::format($model->unloading->date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) : null) . ' ' .
                ($model->unloading->time ?
                    DateHelper::format($model->unloading->time, 'H:i', 'H:i:s') : null), [], $leftParStyle);

        $topTable->addRow();
        $topTable->addCell(2575, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',])
            ->addText('Адрес', [], $leftParStyle);
        $topTable->addCell(7725, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',])
            ->addText($model->unloading->city . ' ' . $model->unloading->address, [], $leftParStyle);

        $topTable = $section->addTable($tableStyle);

        $topTable->addRow();
        $topTable->addCell(2575, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',])
            ->addText('Контактное лицо', [], $leftParStyle);
        $topTable->addCell(2575, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',])
            ->addText($model->unloading->contact_person, [], $leftParStyle);
        $topTable->addCell(2575, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',])
            ->addText('Телефон', [], $leftParStyle);
        $topTable->addCell(2575, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',])
            ->addText($model->unloading->contact_person_phone, [], $leftParStyle);

        $topTable = $section->addTable($tableStyle);

        $topTable->addRow();
        $topTable->addCell(2575, ['vMerge' => 'restart', 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',])
            ->addText('Груз', ['bold' => true], $leftParStyle);
        $topTable->addCell(7725, ['borderBottomSize' => 0, 'gridSpan' => 3, 'borderBottomColor' => 'FFFFFF',])
            ->addText($model->goods_name, [], $leftParStyle);

        $topTable->addRow();
        $topTable->addCell(2575, ['vMerge' => 'continue', 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',]);
        $cell = $topTable->addCell(2575, ['vMerge' => 'restart', 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',]);
        $cellRun = $cell->addTextRun($leftParStyle);
        $cellRun->addText('Вес (тонн): ', ['bold' => true]);
        $cellRun->addText($model->goods_weight);
        $cell = $topTable->addCell(2575, ['vMerge' => 'restart', 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',]);
        $cellRun = $cell->addTextRun($leftParStyle);
        $cellRun->addText('Габариты: ', ['bold' => true]);
        $cellRun->addText("{$model->goods_length}x{$model->goods_width}x{$model->goods_height}=" .
            $model->goods_length * $model->goods_width * $model->goods_height . " м3");
        $cell = $topTable->addCell(2575, ['vMerge' => 'restart', 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',]);
        $cellRun = $cell->addTextRun($leftParStyle);
        $cellRun->addText('Кол-во мест: ', ['bold' => true]);
        $cellRun->addText($model->goods_count);

        $topTable = $section->addTable($tableStyle);

        $topTable->addRow();
        $topTable->addCell(2575, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',])
            ->addText('Способ погрузки', [], $leftParStyle);
        $topTable->addCell(2575, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',])
            ->addText($model->loading->method, [], $leftParStyle);
        $topTable->addCell(2575, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',])
            ->addText('Способ разгрузки', [], $leftParStyle);
        $topTable->addCell(2575, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',])
            ->addText($model->unloading->method, [], $leftParStyle);

        $topTable = $section->addTable($tableStyle);

        $topTable->addRow();
        $topTable->addCell(2575, ['vMerge' => 'restart', 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',])
            ->addText('Стоимость услуги', ['bold' => true], $leftParStyle);
        $topTable->addCell(7725, ['borderBottomSize' => 0, 'gridSpan' => 3, 'borderBottomColor' => 'FFFFFF',])
            ->addText($model->$rate . ' руб.', ['bold' => true], $leftParStyle);

        $delayText = null;
        if ($model->$delay) {
            $delayText = 'Через ' . $model->$delay;
            if ($model->$delay == 1) {
                $delayText .= ' банковский день';
            } elseif ($model->$delay > 1 && $model->$delay < 5) {
                $delayText .= ' банковских дня';
            } else {
                $delayText .= ' банковских дней';
            }
        }

        $topTable->addRow();
        $topTable->addCell(2575, ['vMerge' => 'continue', 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',]);
        $cell = $topTable->addCell(2575, ['vMerge' => 'restart', 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',]);
        $cellRun = $cell->addTextRun($leftParStyle);
        $cellRun->addText('Форма оплаты: ', ['bold' => true]);
        $cellRun->addText($model->$formID ? $model->$form->name : null);
        $cell = $topTable->addCell(2575, ['vMerge' => 'restart', 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',]);
        $cellRun = $cell->addTextRun($leftParStyle);
        $cellRun->addText('Условие оплаты: ', ['bold' => true]);
        $cellRun->addText($model->$conditionID ? $model->$condition->name : null);
        $cell = $topTable->addCell(2575, ['vMerge' => 'restart', 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',]);
        $cellRun = $cell->addTextRun($leftParStyle);
        $cellRun->addText('Оплата: ', ['bold' => true]);
        $cellRun->addText($delayText);

        $topTable = $section->addTable($tableStyle);

        $topTable->addRow();
        $topTable->addCell(2575, ['vMerge' => 'restart', 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',])
            ->addText('Транспортное средство', ['bold' => true], $leftParStyle);
        $topTable->addCell(2575, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',])
            ->addText($model->vehicle->vehicleType->name, [], $leftParStyle);
        $topTable->addCell(2575, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',])
            ->addText($model->vehicle->model, [], $leftParStyle);
        $topTable->addCell(2575, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',])
            ->addText($model->vehicle->state_number, [], $leftParStyle);

        if ($model->vehicle->vehicle_type_id == VehicleType::TYPE_TRACTOR && $model->vehicle->semitrailerType) {
            $topTable->addRow();
            $topTable->addCell(2575, ['vMerge' => 'continue', 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',]);
            $topTable->addCell(2575, ['vMerge' => 'restart', 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',])
                ->addText($model->vehicle->semitrailerType->vehicleType->name, [], $leftParStyle);
            $topTable->addCell(2575, ['vMerge' => 'restart', 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',])
                ->addText($model->vehicle->semitrailerType->model, [], $leftParStyle);
            $topTable->addCell(2575, ['vMerge' => 'restart', 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',])
                ->addText($model->vehicle->semitrailerType->state_number, [], $leftParStyle);
        } elseif ($model->vehicle->vehicle_type_id == VehicleType::TYPE_WAGON && $model->vehicle->trailerType) {
            $topTable->addRow();
            $topTable->addCell(2575, ['vMerge' => 'continue', 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',]);
            $topTable->addCell(2575, ['vMerge' => 'restart', 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',])
                ->addText($model->vehicle->trailerType->vehicleType->name, [], $leftParStyle);
            $topTable->addCell(2575, ['vMerge' => 'restart', 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',])
                ->addText($model->vehicle->trailerType->model, [], $leftParStyle);
            $topTable->addCell(2575, ['vMerge' => 'restart', 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',])
                ->addText($model->vehicle->trailerType->state_number, [], $leftParStyle);
        }

        $topTable->addRow();
        $topTable->addCell(2575, ['vMerge' => 'continue', 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',]);
        $cell = $topTable->addCell(2575, ['vMerge' => 'restart', 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',]);
        $cellRun = $cell->addTextRun($leftParStyle);
        $cellRun->addText('Тип кузова: ', ['bold' => true]);
        $cellRun->addText($model->vehicle->bodyType ? $model->vehicle->bodyType->name : null);
        $cell = $topTable->addCell(2575, ['vMerge' => 'restart', 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',]);
        $cellRun = $cell->addTextRun($leftParStyle);
        $cellRun->addText('Грузоподъемность: ', ['bold' => true]);
        $cellRun->addText($model->vehicle->getTonnageFull() . ' т.');
        $cell = $topTable->addCell(2575, ['vMerge' => 'restart', 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',]);
        $cellRun = $cell->addTextRun($leftParStyle);
        $cellRun->addText('Объем: ', ['bold' => true]);
        $cellRun->addText($model->vehicle->getVolumeFull() . ' м3');

        $topTable = $section->addTable($tableStyle);

        $topTable->addRow();
        $topTable->addCell(2575, ['vMerge' => 'restart', 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',])
            ->addText('Водитель', ['bold' => true], $leftParStyle);
        $topTable->addCell(5150, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',])
            ->addText($model->driver->getFio(), [], $leftParStyle);
        $topTable->addCell(2575, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',])
            ->addText($model->driver->mainDriverPhone ? $model->driver->mainDriverPhone->phone : null, [], $leftParStyle);

        $identificationDate = DateHelper::format($model->driver->identification_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
        $identificationText = "{$model->driver->identification_series}, {$model->driver->identification_number}, {$model->driver->identification_issued_by}, {$identificationDate}";
        $topTable->addRow();
        $topTable->addCell(2575, ['vMerge' => 'continue', 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',]);
        $cell = $topTable->addCell(7725, ['vMerge' => 'restart', 'gridSpan' => 2, 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',]);
        $cellRun = $cell->addTextRun($leftParStyle);
        $cellRun->addText($model->driver->identificationType->name . ': ', ['bold' => true]);
        $cellRun->addText($identificationText);

        $topTable = $section->addTable($tableStyle);

        $topTable->addRow();
        $topTable->addCell(10300, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',])
            ->addText($model->$essence, [], $leftParStyle);

        $topTable = $section->addTable($tableStyle);

        $topTable->addRow();
        $topTable->addCell(5150, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF'])
            ->addText('Заказчик', ['bold' => true], $leftParStyle);
        $topTable->addCell(5150, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF'])
            ->addText('Исполнитель', ['bold' => true], $leftParStyle);

        $topTable->addRow();
        $topTable->addCell(5150, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF', 'borderTopSize' => 0, 'borderTopColor' => 'FFFFFF'])
            ->addText($this->requestType == LogisticsRequest::TYPE_CARRIER ?
                $model->company->getTitle(true) : $contractor->getNameWithType(), ['bold' => true], $leftParStyle);
        $topTable->addCell(5150, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF', 'borderTopSize' => 0, 'borderTopColor' => 'FFFFFF'])
            ->addText($this->requestType == LogisticsRequest::TYPE_CARRIER ?
                $contractor->getNameWithType() : $model->company->getTitle(true), ['bold' => true], $leftParStyle);

        $topTable->addRow();
        $cell = $topTable->addCell(5150, ['borderTopSize' => 0, 'borderTopColor' => 'FFFFFF', 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',]);
        $cellRun = $cell->addTextRun($leftParStyle);
        $cellRun->addText('Юр. адрес: ', ['bold' => true]);
        $cellRun->addText($this->requestType == LogisticsRequest::TYPE_CARRIER ?
            $model->company->getAddressActualFull() : $contractor->legal_address);
        $cell = $topTable->addCell(5150, ['borderTopSize' => 0, 'borderTopColor' => 'FFFFFF', 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',]);
        $cellRun = $cell->addTextRun($leftParStyle);
        $cellRun->addText('Юр. адрес: ', ['bold' => true]);
        $cellRun->addText($this->requestType == LogisticsRequest::TYPE_CARRIER ?
            $contractor->legal_address : $model->company->getAddressActualFull());

        $topTable->addRow();
        $cell = $topTable->addCell(5150, ['borderTopSize' => 0, 'borderTopColor' => 'FFFFFF', 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',]);
        $cellRun = $cell->addTextRun($leftParStyle);
        $cellRun->addText('Почт. адрес: ', ['bold' => true]);
        $cellRun->addText($this->requestType == LogisticsRequest::TYPE_CARRIER ?
            $model->company->getAddressLegalFull() : $contractor->actual_address);
        $cell = $topTable->addCell(5150, ['borderTopSize' => 0, 'borderTopColor' => 'FFFFFF', 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',]);
        $cellRun = $cell->addTextRun($leftParStyle);
        $cellRun->addText('Почт. адрес: ', ['bold' => true]);
        $cellRun->addText($this->requestType == LogisticsRequest::TYPE_CARRIER ?
            $contractor->actual_address : $model->company->getAddressLegalFull());

        $topTable->addRow();
        $cell = $topTable->addCell(5150, ['borderTopSize' => 0, 'borderTopColor' => 'FFFFFF', 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',]);
        $cellRun = $cell->addTextRun($leftParStyle);
        $cellRun->addText('ИНН / КПП: ', ['bold' => true]);
        $cellRun->addText($this->requestType == LogisticsRequest::TYPE_CARRIER ?
            ($model->company->inn . ' / ' . $model->company->kpp) : ($contractor->ITN . ' / ' . $contractor->PPC));
        $cell = $topTable->addCell(5150, ['borderTopSize' => 0, 'borderTopColor' => 'FFFFFF', 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',]);
        $cellRun = $cell->addTextRun($leftParStyle);
        $cellRun->addText('ИНН / КПП: ', ['bold' => true]);
        $cellRun->addText($this->requestType == LogisticsRequest::TYPE_CARRIER ?
            ($contractor->ITN . ' / ' . $contractor->PPC) : ($model->company->inn . ' / ' . $model->company->kpp));

        $topTable->addRow();
        $cell = $topTable->addCell(5150, ['borderTopSize' => 0, 'borderTopColor' => 'FFFFFF', 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',]);
        $cellRun = $cell->addTextRun($leftParStyle);
        $cellRun->addText('ОГРН: ', ['bold' => true]);
        $cellRun->addText($this->requestType == LogisticsRequest::TYPE_CARRIER ?
            $model->company->ogrn : $contractor->BIN);
        $cell = $topTable->addCell(5150, ['borderTopSize' => 0, 'borderTopColor' => 'FFFFFF', 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',]);
        $cellRun = $cell->addTextRun($leftParStyle);
        $cellRun->addText('ОГРН: ', ['bold' => true]);
        $cellRun->addText($this->requestType == LogisticsRequest::TYPE_CARRIER ?
            $contractor->BIN : $model->company->ogrn);

        $topTable->addRow();
        $cell = $topTable->addCell(5150, ['borderTopSize' => 0, 'borderTopColor' => 'FFFFFF', 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',]);
        $cellRun = $cell->addTextRun($leftParStyle);
        $cellRun->addText('Р/с: ', ['bold' => true]);
        $cellRun->addText($this->requestType == LogisticsRequest::TYPE_CARRIER ?
            ($model->company->mainCheckingAccountant ?
                ($model->company->mainCheckingAccountant->rs . ' в ' . $model->company->mainCheckingAccountant->bank_name) : null) :
            ($contractor->current_account ?
                ($contractor->current_account . ' в ' . $contractor->bank_name) : null));
        $cell = $topTable->addCell(5150, ['borderTopSize' => 0, 'borderTopColor' => 'FFFFFF', 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',]);
        $cellRun = $cell->addTextRun($leftParStyle);
        $cellRun->addText('Р/с: ', ['bold' => true]);
        $cellRun->addText($this->requestType == LogisticsRequest::TYPE_CARRIER ?
            ($contractor->current_account ?
                ($contractor->current_account . ' в ' . $contractor->bank_name) : null) :
            ($model->company->mainCheckingAccountant ?
                ($model->company->mainCheckingAccountant->rs . ' в ' . $model->company->mainCheckingAccountant->bank_name) : null));

        $topTable->addRow();
        $cell = $topTable->addCell(5150, ['borderTopSize' => 0, 'borderTopColor' => 'FFFFFF', 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',]);
        $cellRun = $cell->addTextRun($leftParStyle);
        $cellRun->addText('Кор/с: ', ['bold' => true]);
        $cellRun->addText($this->requestType == LogisticsRequest::TYPE_CARRIER ?
            $model->company->mainCheckingAccountant->ks : $contractor->corresp_account);
        $cell = $topTable->addCell(5150, ['borderTopSize' => 0, 'borderTopColor' => 'FFFFFF', 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',]);
        $cellRun = $cell->addTextRun($leftParStyle);
        $cellRun->addText('Кор/с: ', ['bold' => true]);
        $cellRun->addText($this->requestType == LogisticsRequest::TYPE_CARRIER ?
            $contractor->corresp_account : $model->company->mainCheckingAccountant->ks);

        $topTable->addRow();
        $cell = $topTable->addCell(5150, ['borderTopSize' => 0, 'borderTopColor' => 'FFFFFF', 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',]);
        $cellRun = $cell->addTextRun($leftParStyle);
        $cellRun->addText('БИК: ', ['bold' => true]);
        $cellRun->addText($this->requestType == LogisticsRequest::TYPE_CARRIER ?
            $model->company->mainCheckingAccountant->bik : $contractor->BIC);
        $cell = $topTable->addCell(5150, ['borderTopSize' => 0, 'borderTopColor' => 'FFFFFF', 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',]);
        $cellRun = $cell->addTextRun($leftParStyle);
        $cellRun->addText('БИК: ', ['bold' => true]);
        $cellRun->addText($this->requestType == LogisticsRequest::TYPE_CARRIER ?
            $contractor->BIC : $model->company->mainCheckingAccountant->bik);

        $topTable->addRow();
        $cell = $topTable->addCell(5150, ['borderTopSize' => 0, 'borderTopColor' => 'FFFFFF', 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',]);
        $cellRun = $cell->addTextRun($leftParStyle);
        $cellRun->addText('Тел: ', ['bold' => true]);
        $cellRun->addText($this->requestType == LogisticsRequest::TYPE_CARRIER ?
            $model->company->phone : $contractor->getRealContactPhone());
        $cell = $topTable->addCell(5150, ['borderTopSize' => 0, 'borderTopColor' => 'FFFFFF', 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',]);
        $cellRun = $cell->addTextRun($leftParStyle);
        $cellRun->addText('Тел: ', ['bold' => true]);
        $cellRun->addText($this->requestType == LogisticsRequest::TYPE_CARRIER ?
            $contractor->getRealContactPhone() : $model->company->phone);

        $topTable->addRow();
        $cell = $topTable->addCell(5150, ['borderTopSize' => 0, 'borderTopColor' => 'FFFFFF', 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',]);
        $cellRun = $cell->addTextRun($leftParStyle);
        $cellRun->addText('Email: ', ['bold' => true]);
        $cellRun->addText($this->requestType == LogisticsRequest::TYPE_CARRIER ?
            $model->company->email : $contractor->getRealContactEmail());
        $cell = $topTable->addCell(5150, ['borderTopSize' => 0, 'borderTopColor' => 'FFFFFF', 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF',]);
        $cellRun = $cell->addTextRun($leftParStyle);
        $cellRun->addText('Email: ', ['bold' => true]);
        $cellRun->addText($this->requestType == LogisticsRequest::TYPE_CARRIER ?
            $contractor->getRealContactEmail() : $model->company->email);

        $topTable = $section->addTable($tableStyle);

        $topTable->addRow();
        $topTable->addCell(5150, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF'])
            ->addText('Генеральный директор', ['bold' => true], $leftParStyle);
        $topTable->addCell(5150, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF'])
            ->addText('Генеральный директор', ['bold' => true], $leftParStyle);

        $topTable = $section->addTable($tableStyle);
        $signCellStyle = [
            'valign' => 'bottom',
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
            'borderBottomColor' => '000000',
            'borderBottomSize' => 6,
        ];
        $noBorderStyle = [
            'valign' => 'top',
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
        ];
        $topTable->addRow(600);
        $topTable->addCell(150, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF', 'borderTopSize' => 0, 'borderTopColor' => 'FFFFFF', 'borderRightSize' => 0, 'borderRightColor' => 'FFFFFF',]);
        $topTable->addCell(4850, $signCellStyle)->addText($this->requestType == LogisticsRequest::TYPE_CARRIER ?
            ('/ ' . $model->company->getChiefFio(true) . ' /') :
            (!empty($directorName) ? ('/ ' . $directorName . ' /') : ''), [], ['align' => 'right']);
        $topTable->addCell(150, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF', 'borderTopSize' => 0, 'borderTopColor' => 'FFFFFF', 'borderLeftSize' => 0, 'borderLeftColor' => 'FFFFFF',]);
        $topTable->addCell(150, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF', 'borderTopSize' => 0, 'borderTopColor' => 'FFFFFF', 'borderLeftSize' => 0, 'borderLeftColor' => 'FFFFFF', 'borderRightSize' => 0, 'borderRightColor' => 'FFFFFF',]);
        $topTable->addCell(4850, $signCellStyle)->addText($this->requestType == LogisticsRequest::TYPE_CARRIER ?
            (!empty($directorName) ? ('/ ' . $directorName . ' /') : '') :
            ('/ ' . $model->company->getChiefFio(true) . ' /'), [], ['align' => 'right']);
        $topTable->addCell(150, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF', 'borderTopSize' => 0, 'borderTopColor' => 'FFFFFF', 'borderLeftSize' => 0, 'borderLeftColor' => 'FFFFFF',]);

        $topTable = $section->addTable($tableStyle);
        $topTable->addRow(300);
        $topTable->addCell(5150, ['borderTopSize' => 0, 'borderTopColor' => 'FFFFFF']);
        $topTable->addCell(5150, ['borderTopSize' => 0, 'borderTopColor' => 'FFFFFF']);
    }

    /**
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function getFileName()
    {
        return $this->model->getPrintTitle() . '.docx';
    }

    /**
     * @return mixed
     * @throws \PhpOffice\PhpWord\Exception\Exception
     * @throws \yii\web\NotFoundHttpException
     */
    public function getFile()
    {
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=\"{$this->getFileName()}\"");
        header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Expires: 0');

        $objWriter = IOFactory::createWriter($this->phpWord, 'Word2007');

        $objWriter->save("php://output");

        exit;
    }

    /**
     * @param $path
     * @return mixed
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    public function saveFile($path)
    {
        $objWriter = IOFactory::createWriter($this->phpWord, 'Word2007');

        return $objWriter->save($path);
    }
}