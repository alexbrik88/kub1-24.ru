<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 19.02.2018
 * Time: 6:02
 */

namespace common\components\phpWord;


use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\product\Product;
use frontend\models\Documents;
use himiklab\thumbnail\EasyThumbnailImage;
use PhpOffice\PhpWord\IOFactory;
use yii\base\Component;
use common\models\document\OrderDocument;
use PhpOffice\PhpWord\PhpWord;
use php_rutils\RUtils;

/**
 * Class PhpWordOrderDocument
 * @package common\components\phpWord
 */
class PhpWordOrderDocument extends Component
{
    /* @var $model OrderDocument */
    protected $model;

    /* @var $phpWord PhpWord */
    protected $phpWord;

    /**
     * @param OrderDocument $model
     * @param array $config
     */
    public function __construct(OrderDocument $model, $config = [])
    {
        $this->model = $model;
        $this->phpWord = new PhpWord();

        parent::__construct($config);
    }

    /**
     * @throws \himiklab\thumbnail\FileNotFoundException
     */
    public function init()
    {
        parent::init();

        $model = $this->model;
        $invoice = $model;
        $company = $model->company;
        $contractor = $model->contractor;
        $orderArray = $model->orderDocumentProducts;
        $orderCount = count($orderArray);
        $hasNds = $model->has_nds;
        $dateFormatted = RUtils::dt()->ruStrFTime([
            'date' => $model->document_date,
            'format' => 'd F Y г.',
            'monthInflected' => true,
        ]);
        $title = "Заказ № {$model->fullNumber} от {$dateFormatted}";

        $executorText = "{$company->getTitle(true)}, ";
        $executorText .= "ИНН {$company->inn}, ";
        $executorText .= "{$company->getAddressLegalFull()}, ";
        $executorText .= "р/с {$company->mainCheckingAccountant->rs}, ";
        $executorText .= "в банке {$company->mainCheckingAccountant->bank_name}, ";
        $executorText .= "БИК {$company->mainCheckingAccountant->bik}, ";
        $executorText .= "к/с {$company->mainCheckingAccountant->ks}";

        $customerText = $invoice->contractor_name_short;
        $customerText .= ($contractor->face_type == Contractor::TYPE_LEGAL_PERSON) ? ', ИНН ' . $invoice->contractor_inn : '';
        $customerText .= ', ' . ($contractor->face_type == Contractor::TYPE_LEGAL_PERSON) ?
            $invoice->contractor_address_legal_full : $invoice->contractor_address_legal_full;
        $customerText .= ($contractor->face_type == Contractor::TYPE_LEGAL_PERSON && !empty($invoice->contractor_rs)) ?
            ', р/с ' . $invoice->contractor_rs : '';
        $customerText .= ($contractor->face_type == Contractor::TYPE_LEGAL_PERSON && !empty($invoice->contractor_bank_name)) ?
            ', в банке ' . $invoice->contractor_bank_name : '';
        $customerText .= ($contractor->face_type == Contractor::TYPE_LEGAL_PERSON && !empty($invoice->contractor_bik)) ?
            ', БИК ' . $invoice->contractor_bik : '';
        $customerText .= ($contractor->face_type == Contractor::TYPE_LEGAL_PERSON && !empty($invoice->contractor_ks)) ?
            ', к/с ' . $invoice->contractor_ks : '';

        if ($model->type !== Documents::IO_TYPE_OUT) {
            $exClone = $executorText;
            $executorText = $customerText;
            $customerText = $exClone;
        }

        $ndsName = (isset($model->ndsViewType->name)) ? $model->ndsViewType->name : '-';
        $ndsValue = ($model->has_nds) ? TextHelper::invoiceMoneyFormat($model->view_total_nds, 2) : '-';

        $reasonText = '';
        if ($model->basis_document_name && $model->basis_document_number && $model->basis_document_date) {
            $reasonText .= $model->basis_document_name;
            $reasonText .= ' № ' . $model->basis_document_number;
            $reasonText .= ' от ' . DateHelper::format($model->basis_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
        }
        $logo = !($logoImage = $company->getImage('logoImage')) ? null :
            EasyThumbnailImage::thumbnailFile($logoImage, 150, 50, EasyThumbnailImage::THUMBNAIL_INSET);

        //Creating PhpWord ===============================================================================================================
        $phpWord = $this->phpWord;
        $phpWord->setDefaultFontName('Arial');
        $phpWord->setDefaultFontSize(10);

        $properties = $phpWord->getDocInfo();

        $properties->setTitle($title);
        $properties->setCreated(time());
        $properties->setModified(time());

        $section = $phpWord->addSection([
            'marginTop' => 600,
            'marginLeft' => 900,
            'marginRight' => 900,
        ]);

        // paragraph styles with align and empty spaces
        $centerParStyle = [
            'spaceBefore' => 0,
            'spaceAfter' => 0,
            'spacing' => 0,
            'align' => 'center',
        ];
        $leftParStyle = [
            'spaceBefore' => 0,
            'spaceAfter' => 0,
            'spacing' => 0,
            'align' => 'left',
        ];
        $rightParStyle = [
            'spaceBefore' => 0,
            'spaceAfter' => 0,
            'spacing' => 0,
            'align' => 'right',
        ];


        $headTable = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
            'cellMargin' => 50,
        ]);
        $headTable->addRow(850);
        $headTable->addCell(7500)
            ->addText($model->contractor_name_short, ['size' => 12, 'bold' => true], ['align' => 'left', 'spaceBefore' => 100, 'spaceAfter' => 0]);
        $logoCell = $headTable->addCell(2800);
        if ($logo && is_file($logo)) {
            $logoCell->addImage($logo, ['align' => 'right']);
        }

        $section->addTextBreak(1, [], $leftParStyle);

        $topTable = $section->addTable([
            'bidiVisual' => 0,
            'cellMarginTop' => 30,
            'cellMarginBottom' => 30,
            'cellMarginLeft' => 50,
            'cellMarginRight' => 50,
            'borderColor' => '000000',
            'borderTopSize' => 12,
            'borderBottomSize' => 12,
            'borderLeftSize' => 12,
            'borderRightSize' => 12,
            'borderInsideHSize' => 6,
            'borderInsideVSize' => 6,
        ]);
        $topTable->addRow(240);
        $topTable->addCell(5000, ['vMerge' => 'restart', 'gridSpan' => 2, 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF'])
            ->addText(($model->type == 2) ? $company->mainCheckingAccountant->bank_name : $contractor->bank_name, [], $leftParStyle);
        $topTable->addCell(1000)
            ->addText('БИК', [], $leftParStyle);
        $topTable->addCell(4300, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF'])
            ->addText(($model->type == 2) ? $company->mainCheckingAccountant->bik : $contractor->BIC, [], $leftParStyle);

        $topTable->addRow(240);
        $topTable->addCell(5000, ['vMerge' => 'continue', 'gridSpan' => 2]);
        $topTable->addCell(1000, ['vMerge' => 'restart'])
            ->addText('Сч. № ', [], $leftParStyle);
        $topTable->addCell(4300, ['vMerge' => 'restart', 'borderTopSize' => 0, 'borderTopColor' => 'FFFFFF'])
            ->addText(($model->type == 2) ? $company->mainCheckingAccountant->ks : $contractor->corresp_account, [], $leftParStyle);

        $topTable->addRow(240);
        $topTable->addCell(5000, ['gridSpan' => 2, 'borderTopSize' => 0, 'borderTopColor' => 'FFFFFF', 'borderBottomSize' => 12])
            ->addText('Банк получателя', [], $leftParStyle);
        $topTable->addCell(1000, ['vMerge' => 'continue', 'borderBottomSize' => 12]);
        $topTable->addCell(4300, ['vMerge' => 'continue', 'borderBottomSize' => 12]);

        $topTable->addRow(240);
        $topTable->addCell(2500, ['borderTopSize' => 12])
            ->addText('ИНН ' . ($model->type == 2) ? $company->inn : $contractor->ITN, [], $leftParStyle);
        $topTable->addCell(2500, ['borderTopSize' => 12])
            ->addText('КПП ' . ($model->type == 2) ? $company->kpp : $contractor->PPC, [], $leftParStyle);
        $topTable->addCell(1000, ['borderTopSize' => 12, 'borderBottomSize' => 12, 'vMerge' => 'restart'])
            ->addText('Сч. № ', [], $leftParStyle);
        $topTable->addCell(4300, ['borderTopSize' => 12, 'borderBottomSize' => 12, 'vMerge' => 'restart'])
            ->addText(($model->type == 2) ? $company->mainCheckingAccountant->rs : $contractor->current_account, [], $leftParStyle);

        $topTable->addRow(240);
        $topTable->addCell(5000, ['vMerge' => 'restart', 'gridSpan' => 2, 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF'])
            ->addText(($model->type == 2) ? $model->company->getTitle(true) : $model->contractor->getTitle(true), [], $leftParStyle);
        $topTable->addCell(1000, ['vMerge' => 'continue']);
        $topTable->addCell(4300, ['vMerge' => 'continue']);

        $topTable->addRow(240);
        $topTable->addCell(5000, ['vMerge' => 'continue', 'gridSpan' => 2]);
        $topTable->addCell(1000, ['vMerge' => 'continue']);
        $topTable->addCell(4300, ['vMerge' => 'continue']);

        $topTable->addRow(240);
        $topTable->addCell(5000, ['gridSpan' => 2, 'borderBottomSize' => 12, 'borderTopSize' => 0, 'borderTopColor' => 'FFFFFF'])
            ->addText('Получатель', [], $leftParStyle);
        $topTable->addCell(1000, ['vMerge' => 'continue']);
        $topTable->addCell(4300, ['vMerge' => 'continue']);

        $section->addTextBreak(1, ['size' => 1], $leftParStyle);

        $titleTable = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
        ]);
        $titleTable->addRow(700);
        $titleTable->addCell(10300, ['valign' => 'center', 'borderBottomSize' => 12, 'borderBottomColor' => '000000'])
            ->addText($title, ['size' => 14, 'bold' => true], $centerParStyle);

        $section->addTextBreak(1, [], $leftParStyle);

        $topTable2 = $section->addTable([
            'bidiVisual' => 0,
            'cellMargin' => 0,
            'cellMarginTop' => 100,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
        ]);
        $topTable2->addRow();
        $topTable2->addCell(1500)->addText('Поставщик (Исполнитель):', [], $leftParStyle);
        $topTable2->addCell(8800)->addText($executorText, ['bold' => true], $leftParStyle);
        $topTable2->addRow();
        $topTable2->addCell(1500)->addText('Покупатель (Заказчик):', [], $leftParStyle);
        $topTable2->addCell(8800)->addText($customerText, ['bold' => true], $leftParStyle);
        $topTable2->addRow();
        $topTable2->addCell(1500)->addText('Основание:', [], $leftParStyle);
        $topTable2->addCell(8800)->addText($reasonText, ['bold' => true], $leftParStyle);

        $section->addTextBreak(1, [], $leftParStyle);

        $productTable = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => '000000',
            'borderTopSize' => 12,
            'borderBottomSize' => 12,
            'borderLeftSize' => 12,
            'borderRightSize' => 12,
            'borderInsideHSize' => 6,
            'borderInsideVSize' => 6,
            'cellMargin' => 50
        ]);
        $dsc = $model->has_discount || $model->has_markup;
        $dscPriceText = $model->has_markup ? 'Цена с наценкой' : 'Цена со скидкой';
        $dscTotalSumText = $model->has_markup ? 'Сумма наценки' : 'Сумма скидки';
        $productTable->addRow(400);
        $productTable->addCell($dsc ? 400 : 500, ['valign' => 'center'])->addText('№', ['bold' => true], $centerParStyle);
        $productTable->addCell($dsc ? 4000 : 5200, ['valign' => 'center'])->addText('Наименование работ, услуг', ['bold' => true], $centerParStyle);
        $productTable->addCell($dsc ? 900 : 1000, ['valign' => 'center'])->addText('Кол-вo', ['bold' => true], $centerParStyle);
        $productTable->addCell($dsc ? 600 : 800, ['valign' => 'center'])->addText('Ед', ['bold' => true], $centerParStyle);
        $productTable->addCell($dsc ? 1200 : 1400, ['valign' => 'center'])->addText('Цена', ['bold' => true], $centerParStyle);
        if ($dsc) {
            $productTable->addCell(800, ['valign' => 'center'])->addText('Скидка %', ['bold' => true, 'size' => 9], $centerParStyle);
            $productTable->addCell(1200, ['valign' => 'center'])->addText($dscPriceText, ['bold' => true], $centerParStyle);
        }
        $productTable->addCell($dsc ? 1200 : 1400, ['valign' => 'center'])->addText('Сумма', ['bold' => true], $centerParStyle);

        foreach ($orderArray as $key => $order) {
            $unitName = $order->unit ? $order->unit->name : Product::DEFAULT_VALUE;
            $orderQuantity = $unitName == Product::DEFAULT_VALUE ? $unitName : $order->quantity;
            if ($hasNds && $company->isNdsExclude) {
                $basePrice = TextHelper::invoiceMoneyFormat($order->base_price_no_vat, 2);
                $price = TextHelper::invoiceMoneyFormat($order->selling_price_no_vat, 2);
                $sum = TextHelper::invoiceMoneyFormat($order->amount_sales_no_vat, 2);
            } else {
                $basePrice = TextHelper::invoiceMoneyFormat($order->base_price_with_vat, 2);
                $price = TextHelper::invoiceMoneyFormat($order->selling_price_with_vat, 2);
                $sum = TextHelper::invoiceMoneyFormat($order->amount_sales_with_vat, 2);
            }
            $productTable->addRow();
            $productTable->addCell($dsc ? 400 : 500)->addText(++$key, [], $centerParStyle);
            $productTable->addCell($dsc ? 4000 : 5200)->addText(htmlspecialchars($order->product_title), [], $leftParStyle);
            $productTable->addCell($dsc ? 900 : 1000)->addText($orderQuantity, [], $rightParStyle);
            $productTable->addCell($dsc ? 600 : 800)->addText($unitName, [], $rightParStyle);
            if ($dsc) {
                $productTable->addCell(1200)->addText($basePrice, [], $rightParStyle);
                $productTable->addCell(800)->addText($order->discount, [], $centerParStyle);
            }
            $productTable->addCell($dsc ? 1200 : 1400)->addText($price, [], $rightParStyle);
            $productTable->addCell($dsc ? 1200 : 1400)->addText($sum, [], $rightParStyle);
        }

        $section->addTextBreak(1, ['size' => 1], $leftParStyle);

        $amount = TextHelper::invoiceMoneyFormat($model->total_amount, 2);
        $preAmount = TextHelper::invoiceMoneyFormat($model->view_total_amount, 2);
        $discountSum = TextHelper::invoiceMoneyFormat(abs($model->getDiscountSum()), 2);

        $totalTable = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
            'borderBottomSize' => 12,
            'borderBottomColor' => '000000',
            'cellMarginTop' => 0,
            'cellMarginRight' => 50,
            'cellMarginBottom' => 10,
            'cellMarginLeft' => 50,
        ]);
        if ($dsc) {
            $totalTable->addRow(200);
            $totalTable->addCell(8900)->addText($dscTotalSumText.':', ['bold' => true], $rightParStyle);
            $totalTable->addCell(1400)->addText($discountSum, [], $rightParStyle);
        }

        $totalTable->addRow(200);
        $totalTable->addCell(8900)->addText('Итого:', ['bold' => true], $rightParStyle);
        $totalTable->addCell(1400)->addText($preAmount, [], $rightParStyle);

        $totalTable->addRow(200);
        $totalTable->addCell(8900)->addText($ndsName, ['bold' => true], $rightParStyle);
        $totalTable->addCell(1400)->addText($ndsValue, [], $rightParStyle);

        $totalTable->addRow(200);
        $totalTable->addCell(8900)->addText('Всего к оплате:', ['bold' => true], $rightParStyle);
        $totalTable->addCell(1400)->addText($amount, [], $rightParStyle);

        $totalTable->addRow(200);
        $totalTable->addCell(8900)->addText('Вес:', ['bold' => true], $rightParStyle);
        $totalTable->addCell(1400)->addText($model->getTotalWeight(), [], $rightParStyle);
        $totalTable->addRow(200);
        $totalTable->addCell(10300, ['gridSpan' => 2])
            ->addText("Всего наименований {$orderCount}, на сумму {$amount} руб.", [], $leftParStyle);
        $totalTable->addRow(200);
        $totalTable->addCell(10300, ['gridSpan' => 2])
            ->addText(TextHelper::mb_ucfirst(TextHelper::amountToWords($model->total_amount / 100)), ['bold' => true], $leftParStyle);

        $section->addTextBreak(1, [], $leftParStyle);

        $signTable = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
            'cellMarginTop' => 0,
            'cellMarginRight' => 50,
            'cellMarginBottom' => 0,
            'cellMarginLeft' => 50,
        ]);

        $signTable->addRow(1000);
        $signTable->addCell(1900, ['valign' => 'bottom'])
            ->addText('Руководитель', [], $leftParStyle);
        $signTable->addCell(100);
        $signTable->addCell(2700, ['valign' => 'bottom', 'borderBottomSize' => 6, 'borderBottomColor' => '000000'])
            ->addText(($model->type != Documents::IO_TYPE_OUT) ? $model->contractor_director_post_name : $company->chief_post_name, [], $centerParStyle);
        $signTable->addCell(100);
        $signTable->addCell(2700, ['valign' => 'bottom', 'borderBottomSize' => 6, 'borderBottomColor' => '000000']);
        $signTable->addCell(100);
        $signCell = $signTable->addCell(2700, ['valign' => 'bottom', 'borderBottomSize' => 6, 'borderBottomColor' => '000000']);
        $signCell->addText(($model->type != Documents::IO_TYPE_OUT) ? $model->contractor_director_name : $company->getChiefFio(true), [], $centerParStyle);

        $signTable->addRow(300);
        $signTable->addCell(1900);
        $signTable->addCell(100);
        $signTable->addCell(2700, ['valign' => 'top'])->addText('должность', ['size' => 8], $centerParStyle);
        $signTable->addCell(100);
        $signTable->addCell(2700, ['valign' => 'top'])->addText('подпись', ['size' => 8], $centerParStyle);
        $signTable->addCell(100);
        $signTable->addCell(2700, ['valign' => 'top'])->addText('расшифровка подписи', ['size' => 8], $centerParStyle);

        $section->addTextBreak(1);
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->model->getPrintTitle() . '.docx';
    }

    /**
     * @return mixed
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    public function getFile()
    {
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=\"{$this->fileName}\"");
        header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Expires: 0');

        $objWriter = IOFactory::createWriter($this->phpWord, 'Word2007');

        $objWriter->save("php://output");

        exit;
    }

    /**
     * @param $path
     * @return mixed
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    public function saveFile($path)
    {
        $objWriter = IOFactory::createWriter($this->phpWord, 'Word2007');

        return $objWriter->save($path);
    }
}