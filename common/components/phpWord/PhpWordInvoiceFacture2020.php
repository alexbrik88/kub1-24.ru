<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 01.04.2019
 * Time: 13:59
 */

namespace common\components\phpWord;


use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\document\InvoiceFacture;
use common\models\product\Product;
use frontend\models\Documents;
use yii\base\Component;
use \PhpOffice\PhpWord\PhpWord;
use \PhpOffice\PhpWord\IOFactory;

/**
 * Class PhpWordInvoiceFacture
 * @package common\components\phpWord
 */
class PhpWordInvoiceFacture2020 extends Component
{
    /**
     * @var InvoiceFacture
     */
    protected $model;
    /**
     * @var PhpWord
     */
    protected $phpWord;

    /**
     * @param InvoiceFacture $model
     * @param array $config
     */
    public function __construct(InvoiceFacture $model, $config = [])
    {
        $this->model = $model;
        $this->phpWord = new PhpWord();

        parent::__construct($config);
    }

    public function init()
    {
        parent::init();

        $title = 'Счет-фактура';
        $model = $this->model;
        $model->isGroupOwnOrdersByProduct = true;

        $centerParStyle = [
            'spaceBefore' => 0,
            'spaceAfter' => 0,
            'spacing' => 0,
            'align' => 'center',
        ];
        $leftParStyle = [
            'spaceBefore' => 0,
            'spaceAfter' => 0,
            'spacing' => 0,
            'align' => 'left',
        ];
        $rightParStyle = [
            'spaceBefore' => 0,
            'spaceAfter' => 0,
            'spacing' => 0,
            'align' => 'right',
        ];
        $borderNoBottom = [
            'borderBottomSize' => 0,
            'borderBottomColor' => 'FFFFFF',
        ];
        $borderNoRight = [
            'borderRightSize' => 0,
            'borderRightColor' => 'FFFFFF',
        ];
        $borderBottom = [
            'borderBottomSize' => 6,
        ];
        $cellBottom = [
            'valign' => 'bottom'
        ];
        $cellCenter = [
            'valign' => 'center'
        ];
        $cellRowSpan = ['vMerge' => 'restart'];
        $cellRowContinue = ['vMerge' => 'continue'];
        $gridSpan2 = ['gridSpan' => 2];
        $FULL_WIDTH = 15136;

        $consignor = $model->invoice->production_type ? (
        $model->consignor ?
            $model->consignor->getRequisitesFull(null, false) :
            'он же'
        ) : Product::DEFAULT_VALUE;
        if ($model->contractor_address == InvoiceFacture::CONTRACTOR_ADDRESS_LEGAL) {
            $consigneeAddress = $model->consignee ? $model->consignee->legal_address : $model->invoice->contractor->legal_address;
        } else {
            $consigneeAddress = $model->consignee ? $model->consignee->actual_address : $model->invoice->contractor->actual_address;
        }

        $consignee = $model->invoice->production_type ? (
        $model->consignee ?
            $model->consignee->getRequisitesFull($consigneeAddress, false) :
            $model->invoice->contractor->getRequisitesFull($consigneeAddress, false)
        ) : Product::DEFAULT_VALUE;

        $paymentDocuments = [];
        foreach ($model->paymentDocuments as $doc) {
            $date = date('d.m.Y', strtotime($doc->payment_document_date));
            $paymentDocuments[] = "№ {$doc->payment_document_number} от {$date}";
        }
        $paymentDocumentsText = $paymentDocuments ? join(', ', $paymentDocuments) : '№ --- от ---';

        $customerInn = ($model->invoice->contractor->face_type == Contractor::TYPE_LEGAL_PERSON) ?
            ($model->invoice->contractor_inn . '/' . $model->invoice->contractor_kpp) : null;

        $stateContract = $model->state_contract ? '№ ' . $model->state_contract : Product::DEFAULT_VALUE;

        $precision = $model->invoice->price_precision;

        $dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
            'date' => $model->document_date,
            'format' => 'd F Y г.',
            'monthInflected' => true,
        ]);

        //Creating PhpWord ===============================================================================================================
        $phpWord = $this->phpWord;
        $phpWord->setDefaultFontName('Arial');
        $phpWord->setDefaultFontSize(6);

        $properties = $phpWord->getDocInfo();

        $properties->setTitle($title);
        $properties->setCreated(time());
        $properties->setModified(time());

        $section = $phpWord->addSection([
            'marginTop' => 567 / 2,
            'marginLeft' => 2 * 567,
            'marginRight' => 567,
            'marginBottom' => 567 / 2,
        ]);
        $sectionStyle = $section->getStyle();
        $sectionStyle->setOrientation($sectionStyle::ORIENTATION_LANDSCAPE);

        $subheadTable = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
            'cellMargin' => 6,
        ]);
        $subheadTable->addRow()->addCell($FULL_WIDTH)->addText("Приложение №1", ['size' => 6], $rightParStyle);
        $subheadTable->addRow()->addCell($FULL_WIDTH)->addText("к постановлению Правительства Российской Федерации от 26 декабря 2011 г. № 1137", ['size' => 6], $rightParStyle);
        $subheadTable->addRow()->addCell($FULL_WIDTH)->addText("(в редакции постановления Правительства Российской Федерации от 19 августа 2017 г. № 981)", ['size' => 6], $rightParStyle);

        $headTable = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
            'cellMargin' => 0,
        ]);
        $headTable
            ->addRow()
            ->addCell($FULL_WIDTH)
            ->addText("Счет-фактура № {$model->fullNumber} от {$dateFormatted}", ['size' => 14, 'bold'=>true], $leftParStyle);
        $headTable
            ->addRow()
            ->addCell($FULL_WIDTH)
            ->addText("Исправление № — от —", ['size' => 14, 'bold'=>true], $leftParStyle);
        $headTable
            ->addRow()
            ->addCell($FULL_WIDTH)
            ->addText("Продавец: {$model->invoice->company_name_short}", ['size' => 8], $leftParStyle);
        $headTable
            ->addRow()
            ->addCell($FULL_WIDTH)
            ->addText("Адрес: {$model->invoice->company_address_legal_full}", ['size' => 8], $leftParStyle);
        $headTable
            ->addRow()
            ->addCell($FULL_WIDTH)
            ->addText("ИНН/КПП продавца: {$model->invoice->company_inn}/{$model->invoice->company_kpp}", ['size' => 8], $leftParStyle);
        $headTable
            ->addRow()
            ->addCell($FULL_WIDTH)
            ->addText("Грузоотправитель и его адрес: {$consignor}", ['size' => 8], $leftParStyle);
        $headTable
            ->addRow()
            ->addCell($FULL_WIDTH)
            ->addText("Грузополучатель и его адрес: {$consignee}", ['size' => 8], $leftParStyle);
        $headTable
            ->addRow()
            ->addCell($FULL_WIDTH)
            ->addText("К платежно-расчетному документу: {$paymentDocumentsText}", ['size' => 8], $leftParStyle);
        $headTable
            ->addRow()
            ->addCell($FULL_WIDTH)
            ->addText("Покупатель: {$model->invoice->contractor_name_short}", ['size' => 8], $leftParStyle);
        $headTable
            ->addRow()
            ->addCell($FULL_WIDTH)
            ->addText("Адрес: {$model->invoice->contractor_address_legal_full}", ['size' => 8], $leftParStyle);
        $headTable
            ->addRow()
            ->addCell($FULL_WIDTH)
            ->addText("ИНН/КПП покупателя: {$customerInn}", ['size' => 8], $leftParStyle);
        $headTable
            ->addRow()
            ->addCell($FULL_WIDTH)
            ->addText("Валюта: наименование, код Российский рубль, 643", ['size' => 8], $leftParStyle);
        $headTable
            ->addRow()
            ->addCell($FULL_WIDTH)
            ->addText("Идентификатор государственного контракта, договора (соглашения)(при наличии): {$stateContract}", ['size' => 8], $leftParStyle);

        $section->addTextBreak(1, [], $leftParStyle);

        $productTable = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => '000000',
            'borderSize' => 6,
            'cellMargin' => 20,
        ]);
        $productColWidth = [
            1 => 3000,
            2 => 800,
            3 => 800,
            4 => 1500,
            5 => 700,
            6 => 1500,
            7 => 1186,
            8 => 600,
            9 => 650,
            10 => 1000,
            11 => 800,
            12 => 1100,
            13 => 700,
            14 => 800,
        ];
        $productTable->addRow();
        $productTable
            ->addCell($productColWidth[1], $cellCenter + $cellRowSpan)
            ->addText('Наименование товара (описание выполненных работ, оказанных услуг), имущественного права', ['size' => 8], $centerParStyle);
        $productTable
            ->addCell($productColWidth[2], $cellCenter + $cellRowSpan)
            ->addText('Код вида товара', ['size' => 8], $centerParStyle);
        $productTable
            ->addCell($productColWidth[3] + $productColWidth[4], $cellCenter + $gridSpan2)
            ->addText('Единица измерения', ['size' => 8], $centerParStyle);
        $productTable
            ->addCell($productColWidth[5], $cellCenter + $cellRowSpan)
            ->addText('Коли-чество (объем)', ['size' => 8], $centerParStyle);
        $productTable
            ->addCell($productColWidth[6], $cellCenter + $cellRowSpan)
            ->addText('Цена (тариф) за единицу измерения', ['size' => 8], $centerParStyle);
        $productTable
            ->addCell($productColWidth[7], $cellCenter + $cellRowSpan)
            ->addText('Стоимость товаров (работ, услуг), имущественных прав без налога - всего', ['size' => 8], $centerParStyle);
        $productTable
            ->addCell($productColWidth[8], $cellCenter + $cellRowSpan)
            ->addText('В том числе сумма акциза', ['size' => 8], $centerParStyle);
        $productTable
            ->addCell($productColWidth[9], $cellCenter + $cellRowSpan)
            ->addText('Налоговая ставка', ['size' => 8], $centerParStyle);
        $productTable
            ->addCell($productColWidth[10], $cellCenter + $cellRowSpan)
            ->addText('Сумма налога, предъявляемая покупателю', ['size' => 8], $centerParStyle);
        $productTable
            ->addCell($productColWidth[11], $cellCenter + $cellRowSpan)
            ->addText('Стоимость товаров (работ, услуг), имущественных прав с налогом - всего', ['size' => 8], $centerParStyle);
        $productTable
            ->addCell($productColWidth[12] + $productColWidth[13], $cellCenter + $gridSpan2)
            ->addText('Страна происхождения товара', ['size' => 8], $centerParStyle);
        $productTable
            ->addCell($productColWidth[14], $cellCenter + $cellRowSpan)
            ->addText('Регистрационный номер таможенной декларации', ['size' => 8], $centerParStyle);

        $productTable->addRow();
        $productTable->addCell($productColWidth[1], $cellRowContinue);
        $productTable->addCell($productColWidth[2], $cellRowContinue);
        $productTable
            ->addCell($productColWidth[3], $cellCenter)
            ->addText('код', ['size' => 8], $centerParStyle);
        $productTable
            ->addCell($productColWidth[4], $cellCenter)
            ->addText('условное обозначение (национальной)', ['size' => 8], $centerParStyle);
        $productTable->addCell($productColWidth[5], $cellRowContinue);
        $productTable->addCell($productColWidth[6], $cellRowContinue);
        $productTable->addCell($productColWidth[7], $cellRowContinue);
        $productTable->addCell($productColWidth[8], $cellRowContinue);
        $productTable->addCell($productColWidth[9], $cellRowContinue);
        $productTable->addCell($productColWidth[10], $cellRowContinue);
        $productTable->addCell($productColWidth[11], $cellRowContinue);
        $productTable
            ->addCell($productColWidth[12], $cellCenter)
            ->addText('цифровой код', ['size' => 8], $centerParStyle);
        $productTable
            ->addCell($productColWidth[13], $cellCenter)
            ->addText('краткое наименование', ['size' => 8], $centerParStyle);
        $productTable->addCell($productColWidth[14], $cellRowContinue);

        $productTable->addRow();
        $productTable->addCell($productColWidth[1], $cellBottom)->addText(1, ['size' => 7], $centerParStyle);
        $productTable->addCell($productColWidth[2], $cellBottom)->addText('1a', ['size' => 7], $centerParStyle);
        $productTable->addCell($productColWidth[3], $cellBottom)->addText(2, ['size' => 7], $centerParStyle);
        $productTable->addCell($productColWidth[4], $cellBottom)->addText('2a', ['size' => 7], $centerParStyle);
        for ($i = 5; $i <= 12; $i++) {
            $productTable->addCell($productColWidth[$i], $cellBottom)->addText($i - 2, ['size' => 7], $centerParStyle);
        }
        $productTable->addCell($productColWidth[13], $cellBottom)->addText('10a', ['size' => 7], $centerParStyle);
        $productTable->addCell($productColWidth[14], $cellBottom)->addText(11, ['size' => 7], $centerParStyle);

        foreach ($model->ownOrders as $ownOrder) {
            $invoiceOrder = $ownOrder->order;
            $product = $invoiceOrder->product;
            $hideUnits = ($product->production_type == Product::PRODUCTION_TYPE_SERVICE && !$model->show_service_units);
            if ($ownOrder->quantity != intval($ownOrder->quantity)) {
                $ownOrder->quantity = rtrim(number_format($ownOrder->quantity, 10, '.', ''), 0);
            }
            $productColValue[1] = htmlspecialchars($invoiceOrder->product_title);
            $productColValue[2] = $product->item_type_code ?: Product::DEFAULT_VALUE;
            $productColValue[3] = $hideUnits ?
                Product::DEFAULT_VALUE :
                ($invoiceOrder->unit ? $invoiceOrder->unit->code_okei : Product::DEFAULT_VALUE);
            $productColValue[4] = $hideUnits ?
                Product::DEFAULT_VALUE :
                ($invoiceOrder->unit ? $invoiceOrder->unit->name : Product::DEFAULT_VALUE);
            $productColValue[5] = ($hideUnits && $ownOrder->quantity == 1) ?
                Product::DEFAULT_VALUE :
                strtr($ownOrder->quantity, ['.' => ',']);
            $productColValue[6] = $hideUnits ?
                Product::DEFAULT_VALUE :
                ($invoiceOrder->selling_price_no_vat ?
                    TextHelper::invoiceMoneyFormat($invoiceOrder->selling_price_no_vat, $precision) :
                    Product::DEFAULT_VALUE);
            $productColValue[7] = TextHelper::invoiceMoneyFormat($model->getPrintOrderAmount($ownOrder->order_id, true), $precision);
            $productColValue[8] = $invoiceOrder->excise ?
                TextHelper::invoiceMoneyFormat($invoiceOrder->excise_price, 2) :
                'без акциза';
            $productColValue[9] = $invoiceOrder->saleTaxRate->name;
            $productColValue[10] = TextHelper::invoiceMoneyFormat($ownOrder->amountNds, $precision);
            $productColValue[11] = TextHelper::invoiceMoneyFormat($model->getPrintOrderAmount($ownOrder->order_id, false), $precision);
            $productColValue[12] = $ownOrder->country->code == '--' ?
                Product::DEFAULT_VALUE :
                $ownOrder->country->code;
            $productColValue[13] = $ownOrder->country->name_short == '--' ?
                Product::DEFAULT_VALUE :
                $ownOrder->country->name_short;
            $productColValue[14] = $ownOrder->custom_declaration_number;

            $productTable->addRow();
            for ($i = 1; $i <= 14; $i++) {
                $productTable
                    ->addCell($productColWidth[$i], $cellCenter)
                    ->addText($productColValue[$i], ['size' => 8], $i == 1 ? $leftParStyle : $centerParStyle);
            }
        }

        $productTable->addRow();
        $productTable
            ->addCell($productColWidth[1] + $productColWidth[2] + $productColWidth[3] + $productColWidth[4] + $productColWidth[5] + $productColWidth[6], $cellCenter + ['gridSpan' => 6])
            ->addText('Всего к оплате', ['size' => 8, 'bold' => true], $leftParStyle);
        $productTable
            ->addCell($productColWidth[7], $cellCenter)
            ->addText(TextHelper::invoiceMoneyFormat($model->getPrintAmountNoNds(), 2), ['size' => 8,], $centerParStyle);
        $productTable
            ->addCell($productColWidth[8] + $productColWidth[9], $cellCenter + $gridSpan2)
            ->addText('X', ['size' => 8,], $centerParStyle);
        $productTable
            ->addCell($productColWidth[10], $cellCenter)
            ->addText(TextHelper::invoiceMoneyFormat($model->totalNds, 2), ['size' => 8,], $centerParStyle);
        $productTable
            ->addCell($productColWidth[11], $cellCenter)
            ->addText(TextHelper::invoiceMoneyFormat($model->getPrintAmountWithNds(), 2), ['size' => 8,], $centerParStyle);
        $productTable
            ->addCell(array_sum(array_slice($productColWidth, 12, 14)), $borderNoBottom + $borderNoRight + $cellCenter + ['gridSpan' => 3]);

        $section->addTextBreak(1, [], $leftParStyle);

        $footerTable = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
            'cellMargin' => 0,
        ]);
        $footerTable->addRow();
        $footerCell = $footerTable->addCell(3200, $cellBottom);
        $footerCell->addText('Руководитель организации', ['size' => 8], $leftParStyle);
        $footerCell->addText('или иное уполномоченное лицо', ['size' => 8], $leftParStyle);
        $footerTable
            ->addCell(1700, $cellBottom + $borderBottom)
            ->addText('', ['size' => 8], $leftParStyle);
        $footerTable->addCell(100);
        $footerCell = $footerTable->addCell(2568, $cellBottom + $borderBottom);
        if ($model->invoice->company->company_type_id != CompanyType::TYPE_IP) {
            if ($model->type == Documents::IO_TYPE_OUT) {
                $fio = $model->signed_by_name ? $model->signed_by_name : $model->invoice->getCompanyChiefFio(true);
                $footerCell->addText($fio, ['size' => 8], $centerParStyle);
                if ($model->signed_by_employee_id) {
                    $number = mb_strtolower($model->signBasisDocument->name2) . ' №' . $model->sign_document_number;
                    $date = 'от ' . DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . 'г.';
                    $footerCell->addText($number, ['size' => 8], $centerParStyle);
                    $footerCell->addText($date, ['size' => 8], $centerParStyle);
                }
            } else {
                $footerCell->addText($model->invoice->contractor->director_name, ['size' => 8], $centerParStyle);
            }
        }
        $footerTable->addCell(100);
        $footerCell = $footerTable->addCell(3200, $cellBottom);
        $footerCell->addText('Главный бухгалтер', ['size' => 8], $leftParStyle);
        $footerCell->addText('или иное уполномоченное лицо', ['size' => 8], $leftParStyle);
        $footerTable
            ->addCell(1700, $cellBottom + $borderBottom)
            ->addText('', ['size' => 8], $leftParStyle);
        $footerTable->addCell(100);
        $footerCell = $footerTable->addCell(2568, $cellBottom + $borderBottom);
        if ($model->invoice->company->company_type_id != CompanyType::TYPE_IP) {
            if ($model->type == Documents::IO_TYPE_OUT) {
                $fio = $model->signed_by_name ? $model->signed_by_name : $model->invoice->getCompanyChiefAccountantFio(true);
                $footerCell->addText($fio, ['size' => 8], $centerParStyle);
                if ($model->signed_by_employee_id) {
                    $number = mb_strtolower($model->signBasisDocument->name2) . ' №' . $model->sign_document_number;
                    $date = 'от ' . DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . 'г.';
                    $footerCell->addText($number, ['size' => 8], $centerParStyle);
                    $footerCell->addText($date, ['size' => 8], $centerParStyle);
                }
            } else {
                if ($model->invoice->contractor->chief_accountant_is_director) {
                    $footerCell->addText($model->invoice->contractor->director_name, ['size' => 8], $centerParStyle);
                } else {
                    $footerCell->addText($model->invoice->contractor->chief_accountant_name, ['size' => 8], $centerParStyle);
                }
            }
        }
        $footerTable->addRow();
        $footerTable->addCell(3200, $cellCenter);
        $footerTable
            ->addCell(1700, $cellCenter)
            ->addText('(подпись)', ['size' => 6], $centerParStyle);
        $footerTable->addCell(100, $cellCenter);
        $footerTable
            ->addCell(2568, $cellCenter)
            ->addText('(ф.и.о.)', ['size' => 6], $centerParStyle);
        $footerTable->addCell(100, $cellCenter);
        $footerTable->addCell(3200, $cellCenter);
        $footerTable
            ->addCell(1700, $cellCenter)
            ->addText('(подпись)', ['size' => 6], $centerParStyle);
        $footerTable->addCell(100, $cellCenter);
        $footerTable
            ->addCell(2568, $cellCenter)
            ->addText('(ф.и.о.)', ['size' => 6], $centerParStyle);

        $section->addTextBreak(1, [], $leftParStyle);

        $footerTable = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
            'cellMargin' => 0,
        ]);
        $footerTable->addRow();
        $footerCell = $footerTable->addCell(3200, $cellBottom);
        $footerCell->addText('Индивидуальный предприниматель', ['size' => 8], $leftParStyle);
        $footerCell->addText('или иное уполномоченное лицо', ['size' => 8], $leftParStyle);
        $footerTable
            ->addCell(1700, $cellBottom + $borderBottom)
            ->addText('', ['size' => 8], $leftParStyle);
        $footerTable->addCell(100);
        $footerTable
            ->addCell(2568, $cellBottom + $borderBottom)
            ->addText($model->invoice->company->company_type_id == CompanyType::TYPE_IP ?
                $model->invoice->getCompanyChiefFio(true) : null, ['size' => 8], $centerParStyle);
        $footerTable->addCell(500);
        $footerTable
            ->addCell(7068, $cellBottom + $borderBottom)
            ->addText($model->invoice->company->company_type_id == CompanyType::TYPE_IP ?
                $model->invoice->company->getCertificate(false) : null, ['size' => 8], $centerParStyle);

        $footerTable->addRow();
        $footerTable->addCell(3200, $cellCenter);
        $footerTable
            ->addCell(1700, $cellCenter)
            ->addText('(подпись)', ['size' => 6], $centerParStyle);
        $footerTable->addCell(100, $cellCenter);
        $footerTable
            ->addCell(2568, $cellCenter)
            ->addText('(ф.и.о.)', ['size' => 6], $centerParStyle);
        $footerTable->addCell(500, $cellCenter);
        $footerCell = $footerTable->addCell(7068, $cellCenter);
        $footerCell->addText('(реквизиты свидетельства о государственной,', ['size' => 6], $centerParStyle);
        $footerCell->addText('регистрации индивидуального предпринимателя)', ['size' => 6], $centerParStyle);
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->model->getPrintTitle() . '.docx';
    }

    /**
     * @return mixed
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    public function getFile()
    {
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=\"{$this->fileName}\"");
        header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Expires: 0');

        $objWriter = IOFactory::createWriter($this->phpWord, 'Word2007');

        $objWriter->save("php://output");

        exit;
    }

    /**
     * @param $path
     * @return mixed
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    public function saveFile($path)
    {
        $objWriter = IOFactory::createWriter($this->phpWord, 'Word2007');

        return $objWriter->save($path);
    }
}