<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 01.04.2019
 * Time: 13:59
 */

namespace common\components\phpWord;


use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\document\InvoiceFacture;
use common\models\product\Product;
use frontend\models\Documents;
use yii\base\Component;
use \PhpOffice\PhpWord\PhpWord;
use \PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\Element\Section;

/**
 * Class PhpWordInvoiceFacture
 * @package common\components\phpWord
 */
class PhpWordInvoiceFacture2021 extends Component
{
    /**
     * @var InvoiceFacture
     */
    protected $model;
    /**
     * @var PhpWord
     */
    protected $phpWord;

    /**
     * @param InvoiceFacture $model
     * @param array $config
     */
    public function __construct(InvoiceFacture $model, $config = [])
    {
        $this->model = $model;
        $this->phpWord = new PhpWord();

        parent::__construct($config);
    }

    public function init()
    {
        parent::init();

        $title = 'Счет-фактура';
        $model = $this->model;
        $company = $model->invoice->company;
        $model->isGroupOwnOrdersByProduct = true;

        $centerParStyle = [
            'spaceBefore' => 0,
            'spaceAfter' => 0,
            'spacing' => 0,
            'align' => 'center',
        ];
        $leftParStyle = [
            'spaceBefore' => 0,
            'spaceAfter' => 0,
            'spacing' => 0,
            'align' => 'left',
        ];
        $rightParStyle = [
            'spaceBefore' => 0,
            'spaceAfter' => 0,
            'spacing' => 0,
            'align' => 'right',
        ];
        $borderNoBottom = [
            'borderBottomSize' => 0,
            'borderBottomColor' => 'FFFFFF',
        ];
        $borderNoRight = [
            'borderRightSize' => 0,
            'borderRightColor' => 'FFFFFF',
        ];
        $borderBottom = [
            'borderBottomSize' => 6,
        ];
        $cellBottom = [
            'valign' => 'bottom'
        ];
        $cellCenter = [
            'valign' => 'center'
        ];
        $cellRowSpan = ['vMerge' => 'restart'];
        $cellRowContinue = ['vMerge' => 'continue'];
        $gridSpan2 = ['gridSpan' => 2];
        $FULL_WIDTH = 20836;

        $consignor = $model->invoice->production_type ? (
        $model->consignor ?
            $model->consignor->getRequisitesFull(null, false) :
            'он же'
        ) : Product::DEFAULT_VALUE;
        if ($model->contractor_address == InvoiceFacture::CONTRACTOR_ADDRESS_LEGAL) {
            $consigneeAddress = $model->consignee ? $model->consignee->legal_address : $model->invoice->contractor->legal_address;
        } else {
            $consigneeAddress = $model->consignee ? $model->consignee->actual_address : $model->invoice->contractor->actual_address;
        }

        $consignee = $model->invoice->production_type ? (
        $model->consignee ?
            $model->consignee->getRequisitesFull($consigneeAddress, false) :
            $model->invoice->contractor->getRequisitesFull($consigneeAddress, false)
        ) : Product::DEFAULT_VALUE;

        $paymentDocumentsText = $model->printablePaymentDocuments;
        $shippingDocumentsText = $model->printableShippingDocuments;

        $customerInn = ($model->invoice->contractor->face_type == Contractor::TYPE_LEGAL_PERSON) ?
            ($model->invoice->contractor_inn . '/' . $model->invoice->contractor_kpp) : null;

        $stateContract = $model->state_contract ? '№ ' . $model->state_contract : Product::DEFAULT_VALUE;

        $precision = $model->invoice->price_precision;

        //Creating PhpWord ===============================================================================================================
        $phpWord = $this->phpWord;
        $phpWord->setDefaultFontName('Arial');
        $phpWord->setDefaultFontSize(6);

        $properties = $phpWord->getDocInfo();

        $properties->setTitle($title);
        $properties->setCreated(time());
        $properties->setModified(time());

        $section = $phpWord->addSection([
            'marginTop' => 567 / 2,
            'marginLeft' => 2 * 567,
            'marginRight' => 567,
            'marginBottom' => 567 / 2,
        ]);
        $sectionStyle = $section->getStyle();
        $sectionStyle->setOrientation($sectionStyle::ORIENTATION_LANDSCAPE);

        $headParams = [
            'FULL_WIDTH' => $FULL_WIDTH,
            'centerParStyle' => $centerParStyle,
            'rightParStyle' => $rightParStyle,
            'leftParStyle' => $leftParStyle,
            'cellBottom' => $cellBottom,
            'borderBottom' => $borderBottom,
            'consignor' => $consignor,
            'consignee' => $consignee,
            'paymentDocumentsText' => $paymentDocumentsText,
            'shippingDocumentsText' => $shippingDocumentsText,
            'customerInn' => $customerInn,
            'stateContract' => $stateContract,
        ];

        if ($company->invoice_facture_print_template == 2) {
            $this->_headOneColumn($model, $section, $headParams);
        } else {
            $this->_headTwoColumns($model, $section, $headParams);
        }

        $productTable = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => '000000',
            'borderSize' => 6,
            'cellMargin' => 20,
        ]);
        $productColWidth = [
            0 => 300,
            1 => 3000 * 2,
            2 => 800,
            3 => 800,
            4 => 1500,
            5 => 700,
            6 => 1500,
            7 => 1186,
            8 => 600,
            9 => 650,
            10 => 1000,
            11 => 800,
            12 => 1100,
            13 => 700,
            14 => 800,
            15 => 800,
            16 => 800,
            17 => 800,
        ];

        // HEADER

        $productTable->addRow();
        $productTable
            ->addCell($productColWidth[0], $cellCenter + $cellRowSpan)
            ->addText('№ п/п', ['size' => 8], $centerParStyle);
        $productTable
            ->addCell($productColWidth[1], $cellCenter + $cellRowSpan)
            ->addText('Наименование товара (описание выполненных работ, оказанных услуг), имущественного права', ['size' => 8], $centerParStyle);
        $productTable
            ->addCell($productColWidth[2], $cellCenter + $cellRowSpan)
            ->addText('Код вида товара', ['size' => 8], $centerParStyle);
        $productTable
            ->addCell($productColWidth[3] + $productColWidth[4], $cellCenter + $gridSpan2)
            ->addText('Единица измерения', ['size' => 8], $centerParStyle);
        $productTable
            ->addCell($productColWidth[5], $cellCenter + $cellRowSpan)
            ->addText('Коли-чество (объем)', ['size' => 8], $centerParStyle);
        $productTable
            ->addCell($productColWidth[6], $cellCenter + $cellRowSpan)
            ->addText('Цена (тариф) за единицу измерения', ['size' => 8], $centerParStyle);
        $productTable
            ->addCell($productColWidth[7], $cellCenter + $cellRowSpan)
            ->addText('Стоимость товаров (работ, услуг), имущественных прав без налога - всего', ['size' => 8], $centerParStyle);
        $productTable
            ->addCell($productColWidth[8], $cellCenter + $cellRowSpan)
            ->addText('В том числе сумма акциза', ['size' => 8], $centerParStyle);
        $productTable
            ->addCell($productColWidth[9], $cellCenter + $cellRowSpan)
            ->addText('Налоговая ставка', ['size' => 8], $centerParStyle);
        $productTable
            ->addCell($productColWidth[10], $cellCenter + $cellRowSpan)
            ->addText('Сумма налога, предъявляемая покупателю', ['size' => 8], $centerParStyle);
        $productTable
            ->addCell($productColWidth[11], $cellCenter + $cellRowSpan)
            ->addText('Стоимость товаров (работ, услуг), имущественных прав с налогом - всего', ['size' => 8], $centerParStyle);
        $productTable
            ->addCell($productColWidth[12] + $productColWidth[13], $cellCenter + $gridSpan2)
            ->addText('Страна происхождения товара', ['size' => 8], $centerParStyle);
        $productTable
            ->addCell($productColWidth[14], $cellCenter + $cellRowSpan)
            ->addText('Регистрационный номер таможенной декларации', ['size' => 8], $centerParStyle);
        $productTable
            ->addCell($productColWidth[15] + $productColWidth[16], $cellCenter + $gridSpan2)
            ->addText('Количественная единица измерения товара, используемая в целях осуществления прослеживаемости', ['size' => 8], $centerParStyle);
        $productTable
            ->addCell($productColWidth[17], $cellCenter + $cellRowSpan)
            ->addText('Количество товара, подлежащего прослеживаемости, в количественной единице измерения товара, используемой в целях осуществления прослеживаемости', ['size' => 8], $centerParStyle);

        $productTable->addRow();
        $productTable->addCell($productColWidth[0], $cellRowContinue);
        $productTable->addCell($productColWidth[1], $cellRowContinue);
        $productTable->addCell($productColWidth[2], $cellRowContinue);
        $productTable
            ->addCell($productColWidth[3], $cellCenter)
            ->addText('код', ['size' => 8], $centerParStyle);
        $productTable
            ->addCell($productColWidth[4], $cellCenter)
            ->addText('условное обозначение (национальной)', ['size' => 8], $centerParStyle);
        $productTable->addCell($productColWidth[5], $cellRowContinue);
        $productTable->addCell($productColWidth[6], $cellRowContinue);
        $productTable->addCell($productColWidth[7], $cellRowContinue);
        $productTable->addCell($productColWidth[8], $cellRowContinue);
        $productTable->addCell($productColWidth[9], $cellRowContinue);
        $productTable->addCell($productColWidth[10], $cellRowContinue);
        $productTable->addCell($productColWidth[11], $cellRowContinue);
        $productTable
            ->addCell($productColWidth[12], $cellCenter)
            ->addText('цифровой код', ['size' => 8], $centerParStyle);
        $productTable
            ->addCell($productColWidth[13], $cellCenter)
            ->addText('краткое наименование', ['size' => 8], $centerParStyle);
        $productTable->addCell($productColWidth[14], $cellRowContinue);
        $productTable
            ->addCell($productColWidth[15], $cellCenter)
            ->addText('код', ['size' => 8], $centerParStyle);
        $productTable
            ->addCell($productColWidth[16], $cellCenter)
            ->addText('условное обозначение', ['size' => 8], $centerParStyle);
        $productTable->addCell($productColWidth[17], $cellRowContinue);

        $productTable->addRow();
        foreach (['1', '1а', '1б', '2', '2а', '3', '4', '5', '6', '7', '8', '9', '10', '10а', '11', '12', '12а', '13'] as $colNum)
            $productTable->addCell($productColWidth[0], $cellBottom)->addText($colNum, ['size' => 7], $centerParStyle);

        // TABLE

        // bind PackingList orders position numbers with InvoiceFacture orders position numbers
        $ownOrders = $model->getOwnOrdersByShippingDocuments();

        foreach ($ownOrders as $orderPos => $ownOrder) {
            $invoiceOrder = $ownOrder->order;
            $product = $invoiceOrder->product;
            $hideUnits = ($product->production_type == Product::PRODUCTION_TYPE_SERVICE && !$model->show_service_units);
            if ($ownOrder->quantity != intval($ownOrder->quantity)) {
                $ownOrder->quantity = rtrim(number_format($ownOrder->quantity, 10, '.', ''), 0);
            }
            $productColValue[0] = $orderPos;
            $productColValue[1] = htmlspecialchars($invoiceOrder->product_title);
            $productColValue[2] = $product->item_type_code ?: Product::DEFAULT_VALUE;
            $productColValue[3] = $hideUnits ? Product::DEFAULT_VALUE : ($invoiceOrder->unit ? $invoiceOrder->unit->code_okei : Product::DEFAULT_VALUE);
            $productColValue[4] = $hideUnits ? Product::DEFAULT_VALUE : ($invoiceOrder->unit ? $invoiceOrder->unit->name : Product::DEFAULT_VALUE);
            $productColValue[5] = ($hideUnits && $ownOrder->quantity == 1) ? Product::DEFAULT_VALUE : strtr($ownOrder->quantity, ['.' => ',']);
            $productColValue[6] = $hideUnits ? Product::DEFAULT_VALUE : ($invoiceOrder->selling_price_no_vat ? TextHelper::invoiceMoneyFormat($invoiceOrder->selling_price_no_vat, $precision) : Product::DEFAULT_VALUE);
            $productColValue[7] = TextHelper::invoiceMoneyFormat($model->getPrintOrderAmount($ownOrder->order_id, true), $precision);
            $productColValue[8] = $invoiceOrder->excise ? TextHelper::invoiceMoneyFormat($invoiceOrder->excise_price, 2) : 'без акциза';
            $productColValue[9] = $invoiceOrder->saleTaxRate->name;
            $productColValue[10] = TextHelper::invoiceMoneyFormat($ownOrder->amountNds, $precision);
            $productColValue[11] = TextHelper::invoiceMoneyFormat($model->getPrintOrderAmount($ownOrder->order_id, false), $precision);
            $productColValue[12] = $ownOrder->country->code == '--' ? Product::DEFAULT_VALUE : $ownOrder->country->code;
            $productColValue[13] = $ownOrder->country->name_short == '--' ? Product::DEFAULT_VALUE : $ownOrder->country->name_short;
            $productColValue[14] = ($ownOrder->custom_declaration_number) ? $ownOrder->custom_declaration_number : Product::DEFAULT_VALUE;
            $productColValue[15] = ($invoiceOrder->product->is_traceable) ? $productColValue[3] : Product::DEFAULT_VALUE;
            $productColValue[16] = ($invoiceOrder->product->is_traceable) ? $productColValue[4] : Product::DEFAULT_VALUE;
            $productColValue[17] = ($invoiceOrder->product->is_traceable) ? $productColValue[5] : Product::DEFAULT_VALUE;

            $productTable->addRow();
            for ($i = 0; $i <= 17; $i++) {
                $productTable
                    ->addCell($productColWidth[$i], $cellCenter)
                    ->addText($productColValue[$i], ['size' => 8], $i == 1 ? $leftParStyle : $centerParStyle);
            }
        }

        // FOOTER

        $productTable->addRow();
        $productTable
            ->addCell($productColWidth[0] + $productColWidth[1] + $productColWidth[2] + $productColWidth[3] + $productColWidth[4] + $productColWidth[5] + $productColWidth[6], $cellCenter + ['gridSpan' => 7])
            ->addText('Всего к оплате', ['size' => 8, 'bold' => true], $leftParStyle);
        $productTable
            ->addCell($productColWidth[7], $cellCenter)
            ->addText(TextHelper::invoiceMoneyFormat($model->getPrintAmountNoNds(), 2), ['size' => 8,], $centerParStyle);
        $productTable
            ->addCell($productColWidth[8] + $productColWidth[9], $cellCenter + ['gridSpan' => 2])
            ->addText('X', ['size' => 8,], $centerParStyle);
        $productTable
            ->addCell($productColWidth[10], $cellCenter)
            ->addText(TextHelper::invoiceMoneyFormat($model->totalNds, 2), ['size' => 8,], $centerParStyle);
        $productTable
            ->addCell($productColWidth[11], $cellCenter)
            ->addText(TextHelper::invoiceMoneyFormat($model->getPrintAmountWithNds(), 2), ['size' => 8,], $centerParStyle);
        $productTable
            ->addCell(array_sum(array_slice($productColWidth, 12, 17)), $borderNoBottom + $borderNoRight + $cellCenter + ['gridSpan' => 3]);

        $section->addTextBreak(1, [], $leftParStyle);

        $footerTable = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
            'cellMargin' => 0,
        ]);
        $footerTable->addRow();
        $footerCell = $footerTable->addCell(3200, $cellBottom);
        $footerCell->addText('Руководитель организации', ['size' => 8], $leftParStyle);
        $footerCell->addText('или иное уполномоченное лицо', ['size' => 8], $leftParStyle);
        $footerTable
            ->addCell(1700, $cellBottom + $borderBottom)
            ->addText('', ['size' => 8], $leftParStyle);
        $footerTable->addCell(100);
        $footerCell = $footerTable->addCell(2568, $cellBottom + $borderBottom);
        if ($model->invoice->company->company_type_id != CompanyType::TYPE_IP) {
            if ($model->type == Documents::IO_TYPE_OUT) {
                $fio = $model->signed_by_name ? $model->signed_by_name : $model->invoice->getCompanyChiefFio(true);
                $footerCell->addText($fio, ['size' => 8], $centerParStyle);
                if ($model->signed_by_employee_id) {
                    $number = mb_strtolower($model->signBasisDocument->name2) . ' №' . $model->sign_document_number;
                    $date = 'от ' . DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . 'г.';
                    $footerCell->addText($number, ['size' => 8], $centerParStyle);
                    $footerCell->addText($date, ['size' => 8], $centerParStyle);
                }
            } else {
                $footerCell->addText($model->invoice->contractor->director_name, ['size' => 8], $centerParStyle);
            }
        }
        $footerTable->addCell(100);
        $footerCell = $footerTable->addCell(3200, $cellBottom);
        $footerCell->addText('Главный бухгалтер', ['size' => 8], $leftParStyle);
        $footerCell->addText('или иное уполномоченное лицо', ['size' => 8], $leftParStyle);
        $footerTable
            ->addCell(1700, $cellBottom + $borderBottom)
            ->addText('', ['size' => 8], $leftParStyle);
        $footerTable->addCell(100);
        $footerCell = $footerTable->addCell(2568, $cellBottom + $borderBottom);
        if ($model->invoice->company->company_type_id != CompanyType::TYPE_IP) {
            if ($model->type == Documents::IO_TYPE_OUT) {
                $fio = $model->signed_by_name ? $model->signed_by_name : $model->invoice->getCompanyChiefAccountantFio(true);
                $footerCell->addText($fio, ['size' => 8], $centerParStyle);
                if ($model->signed_by_employee_id) {
                    $number = mb_strtolower($model->signBasisDocument->name2) . ' №' . $model->sign_document_number;
                    $date = 'от ' . DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . 'г.';
                    $footerCell->addText($number, ['size' => 8], $centerParStyle);
                    $footerCell->addText($date, ['size' => 8], $centerParStyle);
                }
            } else {
                if ($model->invoice->contractor->chief_accountant_is_director) {
                    $footerCell->addText($model->invoice->contractor->director_name, ['size' => 8], $centerParStyle);
                } else {
                    $footerCell->addText($model->invoice->contractor->chief_accountant_name, ['size' => 8], $centerParStyle);
                }
            }
        }
        $footerTable->addRow();
        $footerTable->addCell(3200, $cellCenter);
        $footerTable
            ->addCell(1700, $cellCenter)
            ->addText('(подпись)', ['size' => 6], $centerParStyle);
        $footerTable->addCell(100, $cellCenter);
        $footerTable
            ->addCell(2568, $cellCenter)
            ->addText('(ф.и.о.)', ['size' => 6], $centerParStyle);
        $footerTable->addCell(100, $cellCenter);
        $footerTable->addCell(3200, $cellCenter);
        $footerTable
            ->addCell(1700, $cellCenter)
            ->addText('(подпись)', ['size' => 6], $centerParStyle);
        $footerTable->addCell(100, $cellCenter);
        $footerTable
            ->addCell(2568, $cellCenter)
            ->addText('(ф.и.о.)', ['size' => 6], $centerParStyle);

        $section->addTextBreak(1, [], $leftParStyle);

        $footerTable = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
            'cellMargin' => 0,
        ]);
        $footerTable->addRow();
        $footerCell = $footerTable->addCell(3200, $cellBottom);
        $footerCell->addText('Индивидуальный предприниматель', ['size' => 8], $leftParStyle);
        $footerCell->addText('или иное уполномоченное лицо', ['size' => 8], $leftParStyle);
        $footerTable
            ->addCell(1700, $cellBottom + $borderBottom)
            ->addText('', ['size' => 8], $leftParStyle);
        $footerTable->addCell(100);
        $footerTable
            ->addCell(2568, $cellBottom + $borderBottom)
            ->addText($model->invoice->company->company_type_id == CompanyType::TYPE_IP ?
                $model->invoice->getCompanyChiefFio(true) : null, ['size' => 8], $centerParStyle);
        $footerTable->addCell(500);
        $footerTable
            ->addCell(7068, $cellBottom + $borderBottom)
            ->addText($model->invoice->company->company_type_id == CompanyType::TYPE_IP ?
                $model->invoice->company->getCertificate(false) : null, ['size' => 8], $centerParStyle);

        $footerTable->addRow();
        $footerTable->addCell(3200, $cellCenter);
        $footerTable
            ->addCell(1700, $cellCenter)
            ->addText('(подпись)', ['size' => 6], $centerParStyle);
        $footerTable->addCell(100, $cellCenter);
        $footerTable
            ->addCell(2568, $cellCenter)
            ->addText('(ф.и.о.)', ['size' => 6], $centerParStyle);
        $footerTable->addCell(500, $cellCenter);
        $footerCell = $footerTable->addCell(7068, $cellCenter);
        $footerCell->addText('(реквизиты свидетельства о государственной,', ['size' => 6], $centerParStyle);
        $footerCell->addText('регистрации индивидуального предпринимателя)', ['size' => 6], $centerParStyle);
    }

    private function _headOneColumn(InvoiceFacture $model, Section $section, array $params): void
    {
        $FULL_WIDTH = $params['FULL_WIDTH'];
        $rightParStyle = $params['rightParStyle'];
        $leftParStyle = $params['leftParStyle'];
        $cellBottom = $params['cellBottom'];
        $borderBottom = $params['borderBottom'];
        $consignor = $params['consignor'];
        $consignee = $params['consignee'];
        $paymentDocumentsText = $params['paymentDocumentsText'];
        $shippingDocumentsText = $params['shippingDocumentsText'];
        $customerInn = $params['customerInn'];
        $stateContract = $params['stateContract'];
        $dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
            'date' => $model->document_date,
            'format' => 'd F Y г.',
            'monthInflected' => true,
        ]);
        
        $subheadTable = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
            'cellMargin' => 6,
        ]);
        $subheadTable->addRow()->addCell($FULL_WIDTH)->addText("Приложение №1", ['size' => 6], $rightParStyle);
        $subheadTable->addRow()->addCell($FULL_WIDTH)->addText("к постановлению Правительства Российской Федерации от 26 декабря 2011 г. № 1137", ['size' => 6], $rightParStyle);
        $subheadTable->addRow()->addCell($FULL_WIDTH)->addText("(в ред. Постановления Правительства РФ от 02.04.2021 № 534)", ['size' => 6], $rightParStyle);

        $headTable = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
            'cellMargin' => 0,
        ]);
        $headTable
            ->addRow()
            ->addCell(3000)
            ->addText("", ['size' => 8, 'bold'=>true], $leftParStyle);
        $headTable
            ->addCell(9200, $cellBottom)
            ->addText("Счет-фактура № {$model->fullNumber} от {$dateFormatted}", ['size' => 8, 'bold'=>true], $leftParStyle);
        $headTable
            ->addCell(300, $cellBottom)
            ->addText('(1)', ['size' => 8,], $leftParStyle);

        $headTable
            ->addRow()
            ->addCell(3000)
            ->addText("", ['size' => 8, 'bold'=>true], $leftParStyle);
        $headTable
            ->addCell(9200, $cellBottom)
            ->addText("Исправление № — от —", ['size' => 8, 'bold'=>true], $leftParStyle);
        $headTable
            ->addCell(300, $cellBottom)
            ->addText('(1а)', ['size' => 8,], $leftParStyle);

        $headTable
            ->addRow()
            ->addCell(3000)
            ->addText("Продавец:", ['size' => 8], $leftParStyle);
        $headTable
            ->addCell(9200, $cellBottom + $borderBottom)
            ->addText("{$model->invoice->company_name_short}", ['size' => 8,], $leftParStyle);
        $headTable
            ->addCell(300, $cellBottom)
            ->addText('(2)', ['size' => 8,], $leftParStyle);

        $headTable
            ->addRow()
            ->addCell(3000)
            ->addText("Адрес:", ['size' => 8], $leftParStyle);
        $headTable
            ->addCell(9200, $cellBottom + $borderBottom)
            ->addText("{$model->invoice->company_address_legal_full}", ['size' => 8,], $leftParStyle);
        $headTable
            ->addCell(300, $cellBottom)
            ->addText('(2а)', ['size' => 8,], $leftParStyle);

        $headTable
            ->addRow()
            ->addCell(3000)
            ->addText("ИНН/КПП продавца:", ['size' => 8], $leftParStyle);
        $headTable
            ->addCell(9200, $cellBottom + $borderBottom)
            ->addText("{$model->invoice->company_inn}/{$model->invoice->company_kpp}", ['size' => 8,], $leftParStyle);
        $headTable
            ->addCell(300, $cellBottom)
            ->addText('(2б)', ['size' => 8,], $leftParStyle);

        $headTable
            ->addRow()
            ->addCell(3000)
            ->addText("Грузоотправитель и его адрес:", ['size' => 8], $leftParStyle);
        $headTable
            ->addCell(9200, $cellBottom + $borderBottom)
            ->addText($model->type == Documents::IO_TYPE_OUT ? $consignor : $consignee, ['size' => 8,], $leftParStyle);
        $headTable
            ->addCell(300, $cellBottom)
            ->addText('(3)', ['size' => 8,], $leftParStyle);

        $headTable
            ->addRow()
            ->addCell(3000)
            ->addText("Грузополучатель и его адрес:", ['size' => 8], $leftParStyle);
        $headTable
            ->addCell(9200, $cellBottom + $borderBottom)
            ->addText($model->type == Documents::IO_TYPE_OUT ? $consignee : $consignor, ['size' => 8,], $leftParStyle);
        $headTable
            ->addCell(300, $cellBottom)
            ->addText('(4)', ['size' => 8,], $leftParStyle);

        $headTable
            ->addRow()
            ->addCell(3000)
            ->addText("К платежно-расчетному документу:", ['size' => 8], $leftParStyle);
        $headTable
            ->addCell(9200, $cellBottom + $borderBottom)
            ->addText("{$paymentDocumentsText}", ['size' => 8,], $leftParStyle);
        $headTable
            ->addCell(300, $cellBottom)
            ->addText('(5)', ['size' => 8,], $leftParStyle);

        $headTable
            ->addRow()
            ->addCell(3000)
            ->addText("Документ об отгрузке:", ['size' => 8], $leftParStyle);
        $headTable
            ->addCell(9200, $cellBottom + $borderBottom)
            ->addText("{$shippingDocumentsText}", ['size' => 8,], $leftParStyle);
        $headTable
            ->addCell(300, $cellBottom)
            ->addText('(5а)', ['size' => 8,], $leftParStyle);

        $headTable
            ->addRow()
            ->addCell(3000)
            ->addText("Покупатель:", ['size' => 8], $leftParStyle);
        $headTable
            ->addCell(9200, $cellBottom + $borderBottom)
            ->addText("{$model->invoice->contractor_name_short}", ['size' => 8,], $leftParStyle);
        $headTable
            ->addCell(300, $cellBottom)
            ->addText('(6)', ['size' => 8,], $leftParStyle);

        $headTable
            ->addRow()
            ->addCell(3000)
            ->addText("Адрес:", ['size' => 8], $leftParStyle);
        $headTable
            ->addCell(9200, $cellBottom + $borderBottom)
            ->addText("{$model->invoice->contractor_address_legal_full}", ['size' => 8,], $leftParStyle);
        $headTable
            ->addCell(300, $cellBottom)
            ->addText('(6а)', ['size' => 8,], $leftParStyle);

        $headTable
            ->addRow()
            ->addCell(3000)
            ->addText("ИНН/КПП покупателя:", ['size' => 8], $leftParStyle);
        $headTable
            ->addCell(9200, $cellBottom + $borderBottom)
            ->addText("{$customerInn}", ['size' => 8,], $leftParStyle);
        $headTable
            ->addCell(300, $cellBottom)
            ->addText('(6б)', ['size' => 8,], $leftParStyle);

        $headTable
            ->addRow()
            ->addCell(3000)
            ->addText("Валюта: наименование, код", ['size' => 8], $leftParStyle);
        $headTable
            ->addCell(9200, $cellBottom + $borderBottom)
            ->addText("Российский рубль, 643", ['size' => 8,], $leftParStyle);
        $headTable
            ->addCell(300, $cellBottom)
            ->addText('(7)', ['size' => 8,], $leftParStyle);

        $headTable
            ->addRow()
            ->addCell(3000)
            ->addText("Идентификатор государственного контракта, договора (соглашения)(при наличии):", ['size' => 8], $leftParStyle);
        $headTable
            ->addCell(9200, $cellBottom + $borderBottom)
            ->addText("{$stateContract}", ['size' => 8,], $leftParStyle);
        $headTable
            ->addCell(300, $cellBottom)
            ->addText('(8)', ['size' => 8,], $leftParStyle);

        $section->addTextBreak(1, [], $leftParStyle);
    }

    private function _headTwoColumns(InvoiceFacture $model, Section $section, array $params): void
    {
        $centerParStyle = $params['centerParStyle'];
        $rightParStyle = $params['rightParStyle'];
        $leftParStyle = $params['leftParStyle'];
        $cellBottom = $params['cellBottom'];
        $borderBottom = $params['borderBottom'];
        $consignor = $params['consignor'];
        $consignee = $params['consignee'];
        $paymentDocumentsText = $params['paymentDocumentsText'];
        $shippingDocumentsText = $params['shippingDocumentsText'];
        $customerInn = $params['customerInn'];
        $stateContract = $params['stateContract'];
        $dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
            'date' => $model->document_date,
            'format' => 'd F Y г.',
            'monthInflected' => true,
        ]);

        $cellTop = ['valign' => 'top'];
        $cellRowSpan = ['vMerge' => 'restart'];
        $cellRowContinue = ['vMerge' => 'continue'];

        $FULL_WIDTH = 20836;

        $col = [
            1 => $FULL_WIDTH * 1/100 * 20,
            2 => $FULL_WIDTH * 1/100 * 12,
            3 => $FULL_WIDTH * 1/100 * 4,
            4 => $FULL_WIDTH * 1/100 * 12,
            5 => $FULL_WIDTH * 1/100 * 3,
            6 => $FULL_WIDTH * 1/100 * 16,
            7 => $FULL_WIDTH * 1/100 * 12,
            8 => $FULL_WIDTH * 1/100 * 18,
            9 => $FULL_WIDTH * 1/100 * 3,
        ];

        $headTable = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
            'cellMargin' => 0,
        ]);

        $headTable->addRow();
        $headTable->addCell($col[1]);
        $headTable->addCell($col[2]);
        $headTable->addCell($col[3]);
        $headTable->addCell($col[4]);
        $headTable->addCell($col[5]);
        $headTable->addCell($col[6]);
        $headTable->addCell($col[7]);
        $headTable->addCell($col[8]);
        $headTable->addCell($col[9]);

        $headTable
            ->addRow()
            ->addCell($col[1], $cellBottom)
            ->addText("Счет-фактура №", ['size' => 10, 'bold' => false], $leftParStyle);
        $headTable
            ->addCell($col[2], $cellBottom + $borderBottom)
            ->addText($model->fullNumber, ['size' => 8], $centerParStyle);
        $headTable
            ->addCell($col[3], $cellBottom)
            ->addText('от', ['size' => 8], $centerParStyle);
        $headTable
            ->addCell($col[4], $cellBottom + $borderBottom)
            ->addText($dateFormatted, ['size' => 8], $centerParStyle);
        $headTable
            ->addCell($col[5], $cellBottom)
            ->addText('(1)', ['size' => 8], $centerParStyle);
        $topCell = $headTable
            ->addCell($col[6] + $col[7] + $col[8] + $col[9], $cellTop + $cellRowSpan + ['gridSpan' => 4]);
        $topCell
            ->addText("Приложение №1", ['size' => 6], $rightParStyle);
        $topCell
            ->addText("к постановлению Правительства Российской Федерации от 26 декабря 2011 г. № 1137", ['size' => 6], $rightParStyle);
        $topCell
            ->addText("(в ред. Постановления Правительства РФ от 02.04.2021 № 534)", ['size' => 6], $rightParStyle);

        $headTable
            ->addRow()
            ->addCell($col[1], $cellBottom)
            ->addText("Исправление №", ['size' => 10, 'bold' => false], $leftParStyle);
        $headTable
            ->addCell($col[2], $cellBottom + $borderBottom)
            ->addText('—', ['size' => 8], $centerParStyle);
        $headTable
            ->addCell($col[3], $cellBottom)
            ->addText('от', ['size' => 8], $centerParStyle);
        $headTable
            ->addCell($col[4], $cellBottom + $borderBottom)
            ->addText('—', ['size' => 8], $centerParStyle);
        $headTable
            ->addCell($col[5], $cellBottom)
            ->addText('(1а)', ['size' => 8], $centerParStyle);
        $headTable->addCell($col[6] + $col[7] + $col[8] + $col[9], $cellTop + $cellRowContinue + ['gridSpan' => 4]);

        $headTable
            ->addRow()
            ->addCell($col[1], $cellBottom)
            ->addText("Продавец:", ['size' => 8, 'bold' => true], $leftParStyle);
        $headTable
            ->addCell($col[2] + $col[3] + $col[4], $cellBottom + $borderBottom + ['gridSpan' => 3])
            ->addText(($model->type == Documents::IO_TYPE_OUT) ? $model->invoice->company_name_short : $model->invoice->contractor_name_short, ['size' => 8], $leftParStyle);
        $headTable
            ->addCell($col[5], $cellBottom)
            ->addText('(2)', ['size' => 8], $centerParStyle);
        $headTable
            ->addCell($col[6], $cellBottom)
            ->addText("Покупатель:", ['size' => 8, 'bold' => true], $leftParStyle);
        $headTable
            ->addCell($col[7] + $col[8], $cellBottom + $borderBottom + ['gridSpan' => 2])
            ->addText(($model->type == Documents::IO_TYPE_IN) ? $model->invoice->company_name_short : $model->invoice->contractor_name_short, ['size' => 8], $leftParStyle);
        $headTable
            ->addCell($col[9], $cellBottom)
            ->addText('(6)', ['size' => 8], $centerParStyle);

        $headTable
            ->addRow()
            ->addCell($col[1], $cellBottom)
            ->addText("Адрес:", ['size' => 8, 'bold' => false], $leftParStyle);
        $headTable
            ->addCell($col[2] + $col[3] + $col[4], $cellBottom + $borderBottom + ['gridSpan' => 3])
            ->addText(($model->type == Documents::IO_TYPE_OUT) ? $model->invoice->company_address_legal_full : $model->invoice->contractor_address_legal_full, ['size' => 8], $leftParStyle);
        $headTable
            ->addCell($col[5], $cellBottom)
            ->addText('(2а)', ['size' => 8], $centerParStyle);
        $headTable
            ->addCell($col[6], $cellBottom)
            ->addText("Адрес:", ['size' => 8, 'bold' => false], $leftParStyle);
        $headTable
            ->addCell($col[7] + $col[8], $cellBottom + $borderBottom + ['gridSpan' => 2])
            ->addText(($model->type == Documents::IO_TYPE_IN) ? $model->invoice->company_address_legal_full : $model->invoice->contractor_address_legal_full, ['size' => 8], $leftParStyle);
        $headTable
            ->addCell($col[9], $cellBottom)
            ->addText('(6а)', ['size' => 8], $centerParStyle);

        $headTable
            ->addRow()
            ->addCell($col[1], $cellBottom)
            ->addText("ИНН/КПП продавца:", ['size' => 8, 'bold' => false], $leftParStyle);
        $headTable
            ->addCell($col[2] + $col[3] + $col[4], $cellBottom + $borderBottom + ['gridSpan' => 3])
            ->addText(($model->type == Documents::IO_TYPE_OUT) ? ($model->invoice->company_inn.' / '.$model->invoice->company_kpp) : ($model->invoice->contractor_inn.' / '.$model->invoice->contractor_kpp), ['size' => 8], $leftParStyle);
        $headTable
            ->addCell($col[5], $cellBottom)
            ->addText('(2б)', ['size' => 8], $centerParStyle);
        $headTable
            ->addCell($col[6], $cellBottom)
            ->addText("ИНН/КПП покупателя:", ['size' => 8, 'bold' => false], $leftParStyle);
        $headTable
            ->addCell($col[7] + $col[8], $cellBottom + $borderBottom + ['gridSpan' => 2])
            ->addText(($model->type == Documents::IO_TYPE_IN) ? ($model->invoice->company_inn.' / '.$model->invoice->company_kpp) : ($model->invoice->contractor_inn.' / '.$model->invoice->contractor_kpp), ['size' => 8], $leftParStyle);
        $headTable
            ->addCell($col[9], $cellBottom)
            ->addText('(6б)', ['size' => 8], $centerParStyle);

        $headTable
            ->addRow()
            ->addCell($col[1], $cellBottom)
            ->addText("Грузоотправитель и его адрес:", ['size' => 8, 'bold' => false], $leftParStyle);
        $headTable
            ->addCell($col[2] + $col[3] + $col[4], $cellBottom + $borderBottom + ['gridSpan' => 3])
            ->addText(($model->type == Documents::IO_TYPE_OUT) ? $consignor : $consignee, ['size' => 8], $leftParStyle);
        $headTable
            ->addCell($col[5], $cellBottom)
            ->addText('(3)', ['size' => 8], $centerParStyle);
        $headTable
            ->addCell($col[6], $cellBottom)
            ->addText("Валюта: наименование, код", ['size' => 8, 'bold' => false], $leftParStyle);
        $headTable
            ->addCell($col[7] + $col[8], $cellBottom + $borderBottom + ['gridSpan' => 2])
            ->addText('Российский рубль, 643', ['size' => 8], $leftParStyle);
        $headTable
            ->addCell($col[9], $cellBottom)
            ->addText('(7)', ['size' => 8], $centerParStyle);

        $headTable
            ->addRow()
            ->addCell($col[1], $cellBottom)
            ->addText("Грузополучатель и его адрес:", ['size' => 8, 'bold' => false], $leftParStyle);
        $headTable
            ->addCell($col[2] + $col[3] + $col[4], $cellBottom + $borderBottom + ['gridSpan' => 3])
            ->addText(($model->type == Documents::IO_TYPE_IN) ? $consignor : $consignee, ['size' => 8], $leftParStyle);
        $headTable
            ->addCell($col[5], $cellBottom)
            ->addText('(4)', ['size' => 8], $centerParStyle);
        $headTable
            ->addCell($col[6] + $col[7], $cellBottom + $cellRowSpan + ['gridSpan' => 2])
            ->addText("Идентификатор государственного контракта, договора (соглашения) (при наличии)", ['size' => 8, 'bold' => false], $leftParStyle);
        $headTable
            ->addCell($col[8], $cellBottom + $borderBottom + $cellRowSpan)
            ->addText($stateContract, ['size' => 8], $leftParStyle);
        $headTable
            ->addCell($col[9], $cellBottom + $cellRowSpan)
            ->addText('(8)', ['size' => 8], $centerParStyle);

        $headTable
            ->addRow()
            ->addCell($col[1], $cellBottom)
            ->addText("К платежно-расчетному документу №:", ['size' => 8, 'bold' => false], $leftParStyle);
        $headTable
            ->addCell($col[2] + $col[3] + $col[4], $cellBottom + $borderBottom + ['gridSpan' => 3])
            ->addText($paymentDocumentsText, ['size' => 8], $leftParStyle);
        $headTable
            ->addCell($col[5], $cellBottom)
            ->addText('(5)', ['size' => 8], $centerParStyle);
        $headTable
            ->addCell($col[6] + $col[7], $cellBottom + $cellRowContinue + ['gridSpan' => 2]);
        $headTable
            ->addCell($col[8], $cellBottom + $borderBottom + $cellRowContinue);
        $headTable
            ->addCell($col[9], $cellBottom + $cellRowContinue);

        $headTable
            ->addRow()
            ->addCell($col[1], $cellBottom)
            ->addText("Документ об отгрузке:", ['size' => 8, 'bold' => false], $leftParStyle);
        $headTable
            ->addCell($col[2] + $col[3] + $col[4], $cellBottom + $borderBottom + ['gridSpan' => 3])
            ->addText($shippingDocumentsText, ['size' => 8], $leftParStyle);
        $headTable
            ->addCell($col[5], $cellBottom)
            ->addText('(5а)', ['size' => 8], $centerParStyle);
        $headTable
            ->addCell($col[6] + $col[7] + $col[8] + $col[9], $cellBottom + ['gridSpan' => 4]);

        $section->addTextBreak(1, [], $leftParStyle);
    }

    /**
     * @return string
     */
    public function getFileName()
    {
        return $this->model->getPrintTitle() . '.docx';
    }

    /**
     * @return mixed
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    public function getFile()
    {
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=\"{$this->fileName}\"");
        header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Expires: 0');

        $objWriter = IOFactory::createWriter($this->phpWord, 'Word2007');

        $objWriter->save("php://output");

        exit;
    }

    /**
     * @param $path
     * @return mixed
     * @throws \PhpOffice\PhpWord\Exception\Exception
     */
    public function saveFile($path)
    {
        $objWriter = IOFactory::createWriter($this->phpWord, 'Word2007');

        return $objWriter->save($path);
    }
}