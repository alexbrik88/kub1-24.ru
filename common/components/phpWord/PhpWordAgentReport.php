<?php

namespace common\components\phpWord;

use common\components\date\DateHelper;
use common\components\image\EasyThumbnailImage;
use common\components\TextHelper;
use common\models\Contractor;
use common\models\company\CompanyType;
use common\models\currency\Currency;
use common\models\document\AgentReport;
use common\models\document\Invoice;
use common\models\document\InvoiceContractorSignature;
use common\models\product\Product;
use frontend\models\Documents;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Shared\Converter;
use yii\base\Component;
use yii\helpers\Html;

/**
 * Class PdfRenderer
 * @package common\components\pdf
 */
class PhpWordAgentReport extends Component
{
    protected $model;
    protected $phpWord;

    /**
     * @param Invoice $model
     * @param array $config
     */
    public function __construct(AgentReport $model, $config = [])
    {
        $this->model = $model;
        $this->phpWord = new \PhpOffice\PhpWord\PhpWord();

        parent::__construct($config);
    }

    public function init()
    {
        parent::init();

        $model = $this->model;
        $company = $model->company;
        $agent = $model->agent;

        $isCompanyIP = $company->company_type_id == \common\models\company\CompanyType::TYPE_IP;
        $isAgentIP = $agent->company_type_id == \common\models\company\CompanyType::TYPE_IP;
        $agentAgreementTitle = $model->agreement ?
            '№ '.$model->agreement->fullNumber.' от '.DateHelper::format($model->agreement->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) : '';
        $startDate = DateHelper::format($model->document_date, '01.m.Y', DateHelper::FORMAT_DATE);
        $endDate = DateHelper::format($model->document_date, 't.m.Y', DateHelper::FORMAT_DATE);
        $dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
            'date' => $model->document_date,
            'format' => 'd F Y г.',
            'monthInflected' => true,
        ]);
        $director_name = $agent->getDirectorFio();
        $hasNds = $agent->taxation_system == Contractor::WITH_NDS;
        $isNdsExclude = false;
        $ndsName = ($hasNds) ? 'В том числе НДС' : 'Без налога (НДС)';
        $ndsValue = $hasNds ? TextHelper::invoiceMoneyFormat($model->total_nds, 2) : '-';
        $precision = 2;

        $agent_director_name = '';
        if (!empty($agent->director_name) && $agent->director_name != 'ФИО Руководителя') {
            $agent_director_name = TextHelper::nameShort($agent->director_name);
        }
        $title = "Отчет агента № {$model->fullNumber} от {$dateFormatted}";

        $headerText = '';
        $headerText .= $company->getShortName() . ', именуемое далее «Принципал», в лице ' . $company->getChiefFio();
        $headerText .= (!$isCompanyIP ? ' действующего на основании Устава' : '') . ', с одной стороны и ' . $agent->getShortName();
        $headerText .= ', именуемое далее «Агент», в лице ' . $agent->director_post_name .' '. $agent_director_name;
        $headerText .= (!$isAgentIP ? ' действующего на основании Устава' : '') . ', с другой стороны, в дальнейшем именуемые Стороны, настоящим Отчетом удостоверяют, что';
        $headerText .= ($agentAgreementTitle ? " в соответствии с условиями Договора {$agentAgreementTitle}, " : '');
        $headerText .= ' в период с ' . $startDate . ' по ' . $endDate . ', были получены оплаты от клиентов Агента:';

        $bottomText_1  = 'Общая сумма вознаграждения по данному отчету ' . TextHelper::amountToWords($model->total_sum / 100);
        $bottomText_1 .= ($hasNds) ? ', в том числе НДС 20%.' : ', НДС не облагается, в связи с применением Упрощенной Системы Налогообложения.';
        $bottomText_2 = 'Услуги оказаны полностью. Стороны не имеют претензий по оказанию услуг.';

        // paragraph styles with align and empty spaces
        $centerParStyle = [
            'spaceBefore' => 0,
            'spaceAfter' => 0,
            'spacing' => 0,
            'align' => 'center',
        ];
        $leftParStyle = [
            'spaceBefore' => 0,
            'spaceAfter' => 0,
            'spacing' => 0,
            'align' => 'left',
        ];
        $rightParStyle = [
            'spaceBefore' => 0,
            'spaceAfter' => 0,
            'spacing' => 0,
            'align' => 'right',
        ];

        //Creating PhpWord
        $phpWord = $this->phpWord;
        $phpWord->setDefaultFontName('Arial');
        $phpWord->setDefaultFontSize(10);

        $properties = $phpWord->getDocInfo();

        $properties->setTitle($title);
        $properties->setCreated(time());
        $properties->setModified(time());

        $section = $phpWord->addSection([
            'marginTop' => 600,
            'marginLeft' => 900,
            'marginRight' => 900,
            'marginBottom' => 900,
        ]);

        // Title
        $section->addText($title, ['size' => 14, 'bold' => true], [
            'align' => 'center',
            'spaceBefore' => 0,
            'spaceAfter' => 100,
        ]);
        if ($agentAgreementTitle) {
            $section->addText('По агентскому договору ' . $agentAgreementTitle, ['size' => 11, 'bold' => true], [
                'align' => 'center',
                'spaceBefore' => 0,
                'spaceAfter' => 100,
            ]);
        }
        // end Title

        $section->addText((DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE)), [], [
            'align' => 'right',
            'spaceBefore' => 0,
            'spaceAfter' => 100,
        ]);
        $titleLine = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
        ]);
        $titleLine->addRow(7);
        $titleLine->addCell(10300, ['valign' => 'center', 'borderTopSize' => 12, 'borderTopColor' => '000000']);

        $section->addText($headerText, [], [
            'align' => 'left',
            'spaceBefore' => 0,
            'spaceAfter' => 100,
        ]);

        $section->addTextBreak(1, ['size' => 9], $leftParStyle);

        // Product Table
        $productTable = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => '000000',
            'borderTopSize' => 12,
            'borderBottomSize' => 12,
            'borderLeftSize' => 12,
            'borderRightSize' => 12,
            'borderInsideHSize' => 6,
            'borderInsideVSize' => 6,
            'cellMargin' => 50
        ]);
        $headCellStyle = [
            'valign' => 'center',
            'cellMargin' => 50,

        ];
        $prodCellStyle = [
            'valign' => 'top',
            'cellMargin' => 50,
        ];

        $productTable->addRow(600);
        $productTable->addCell(515, $headCellStyle)
            ->addText('№', ['bold' => true], ['align' => 'center']);
        $productTable->addCell(4520, $headCellStyle)
            ->addText('Покупатель', ['bold' => true], ['align' => 'center']);
        $productTable->addCell(2175, $headCellStyle)
            ->addText('Сумма оплат за месяц (руб.)', ['bold' => true], ['align' => 'center']);
        $productTable->addCell(1545, $headCellStyle)
            ->addText('Комиссионное вознаграждение %', ['bold' => true], ['align' => 'center']);
        $productTable->addCell(1545, $headCellStyle)
            ->addText('Комиссионное вознаграждение (руб.)', ['bold' => true], ['align' => 'center']);

        foreach ($model->agentReportOrders as $key => $order) {

            if ($order->is_exclude) continue;

            $productTable->addRow(200);
            $productTable->addCell(515, $prodCellStyle)->addText(++$key, [], $centerParStyle);
            $productTable->addCell(4520, $prodCellStyle)->addText(htmlspecialchars($order->contractor->getShortName()), [], $leftParStyle);
            $productTable->addCell(2175, $prodCellStyle)->addText(TextHelper::invoiceMoneyFormat($order->payments_sum, $precision), [], $rightParStyle);
            $productTable->addCell(1545, $prodCellStyle)->addText(TextHelper::numberFormat($order->agent_percent, $precision), [], $rightParStyle);
            $productTable->addCell(1545, $prodCellStyle)->addText(TextHelper::invoiceMoneyFormat($order->agent_sum, $precision), [], $rightParStyle);
        }

        $section->addTextBreak(1, ['size' => 3], $leftParStyle);

        // Totals
        $totalTable = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
            'cellMarginTop' => 0,
            'cellMarginRight' => 50,
            'cellMarginBottom' => 10,
            'cellMarginLeft' => 50,
        ]);
        $totalTable->addRow(275);
        $totalTable->addCell(8700)
            ->addText("Итого:", ['bold' => true], $rightParStyle);
        $totalTable->addCell(1600)
            ->addText(TextHelper::invoiceMoneyFormat($model->total_sum, 2), [], $rightParStyle);
        $totalTable->addRow(275);
        $totalTable->addCell(8700)
            ->addText($ndsName, ['bold' => true], $rightParStyle);
        $totalTable->addCell(1600)
            ->addText(TextHelper::invoiceMoneyFormat($ndsValue, 2), [], $rightParStyle);
        $totalTable->addRow(275);
        $totalTable->addCell(8700)
            ->addText("Всего к оплате:", ['bold' => true], $rightParStyle);
        $totalTable->addCell(1600)
            ->addText(TextHelper::invoiceMoneyFormat($model->total_sum, 2), [], $rightParStyle);

        $section->addTextBreak(1, ['size' => 9], $leftParStyle);
        $section->addText($bottomText_1, [], $leftParStyle);
        $section->addTextBreak(1, ['size' => 5], $leftParStyle);
        $section->addText($bottomText_2, [], $leftParStyle);

        $section->addTextBreak(1, ['size' => 5], $leftParStyle);

        $footerLine = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
        ]);
        $footerLine->addRow(7);
        $footerLine->addCell(10300, ['valign' => 'center', 'borderTopSize' => 12, 'borderTopColor' => '000000']);

        $section->addTextBreak(2, ['size' => 9], $leftParStyle);

        // Sign
        $noBorderStyle = [
            'valign' => 'top',
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
        ];
        $signCellStyle = [
            'valign' => 'bottom',
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
            'borderBottomColor' => '000000',
            'borderBottomSize' => 6,
        ];
        $signTable = $section->addTable();
        $signTable->addRow(400);
        $signTable->addCell(5000, $noBorderStyle)->addText('АГЕНТ', ['bold' => true]);
        $signTable->addCell(200, $noBorderStyle);
        $signTable->addCell(5000, $noBorderStyle)->addText('ПРИНЦИПАЛ', ['bold' => true]);
        $signTable->addRow(400);
        $signTable->addCell(5000, $noBorderStyle)->addText($agent->getShortName());
        $signTable->addCell(200, $noBorderStyle);
        $signTable->addCell(5000, $noBorderStyle)->addText($company->getShortName());
        $signTable->addRow(400);
        $signTable->addCell(5000, $signCellStyle)->addText(!empty($agent_director_name) ? ('/ ' . $agent_director_name . ' /'): '', [], $rightParStyle);
        $signTable->addCell(200, $noBorderStyle);
        $signTable->addCell(5000, $signCellStyle)->addText('/ ' . $agent->company->getChiefFio(true) . ' /', [], $rightParStyle);
        // end Sign Table
    }

    public function getFileName()
    {
        return $this->model->getPrintTitle($escape = true) . '.docx';
    }

    public function getFile()
    {
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=\"{$this->fileName}\"");
        header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Expires: 0');

        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($this->phpWord, 'Word2007');

        $objWriter->save("php://output");

        exit;
    }

    public function saveFile($path)
    {
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($this->phpWord, 'Word2007');

        return $objWriter->save($path);
    }
}
