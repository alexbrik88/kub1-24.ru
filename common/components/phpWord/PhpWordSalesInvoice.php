<?php

namespace common\components\phpWord;

use common\components\date\DateHelper;
use common\components\helpers\ArrayHelper;
use common\components\image\EasyThumbnailImage;
use common\components\TextHelper;
use common\models\Contractor;
use common\models\company\CompanyType;
use common\models\currency\Currency;
use common\models\document\Invoice;
use common\models\document\SalesInvoice;
use common\models\document\InvoiceContractorSignature;
use common\models\product\Product;
use frontend\models\Documents;
use php_rutils\RUtils;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Shared\Converter;
use yii\base\Component;
use yii\helpers\Html;
use common\models\document\OrderSalesInvoice;

/**
 * Class PhpWordSalesInvoice
 * @package common\components\phpWord
 */
class PhpWordSalesInvoice extends Component
{
    protected $model;
    protected $phpWord;

    /**
     * @param SalesInvoice $model
     * @param array $config
     */
    public function __construct(SalesInvoice $model, $config = [])
    {
        $this->model = $model;
        $this->phpWord = new PhpWord();

        parent::__construct($config);
    }

    public function init()
    {
        parent::init();

        /** @var SalesInvoice $model */
        /** @var Invoice $invoice */

        $model = $this->model;
        $invoice = $model->invoice;
        $precision = $invoice->price_precision;
        $company = $invoice->company;
        $contractor = $invoice->contractor;
        $orderArray = $invoice->getOrders()->all();
        $orderCount = count($orderArray);
        $hasNds = $invoice->hasNds;
        $dateFormated = \php_rutils\RUtils::dt()->ruStrFTime([
            'date' => $model->document_date,
            'format' => 'd F Y г.',
            'monthInflected' => true,
        ]);
        $name = 'Товарная накладная';
        $title = "{$name}";
        $invoiceContractorSignature = $invoice->company->invoiceContractorSignature;
        if (!$invoiceContractorSignature) {
            $invoiceContractorSignature = new InvoiceContractorSignature();
            $invoiceContractorSignature->company_id = $invoice->company_id;
        }
        $realMassNet = $model->invoice->getRealMassNet();
        $realMassGross = $model->invoice->getRealMassGross();

        // Грузоотправитель
        $CONSIGNOR = '';
        if ($model->consignor) {
            $CONSIGNOR = $model->consignor->getRequisitesFull();
        } elseif ($model->type == Documents::IO_TYPE_OUT) {
            $CONSIGNOR = $this->getCompanyRequisitesFull($model);
        } else {
            $CONSIGNOR = $this->getContractorRequisitesFull($model);
        }

        // Грузополучатель
        if ($model->consignee) {
            if ($model->contractor_address == SalesInvoice::CONTRACTOR_ADDRESS_LEGAL) {
                $address = $model->consignee->legal_address;
            } else {
                $address = $model->consignee->actual_address;
            }
            $CONSIGNEE = $model->consignee->getRequisitesFull($address);
        } elseif ($model->type == Documents::IO_TYPE_OUT) {
            $CONSIGNEE = $this->getContractorRequisitesFull($model);
        } else {
            $CONSIGNEE = $this->getCompanyRequisitesFull($model);
        }

        // Поставщик
        if ($model->type == Documents::IO_TYPE_OUT) {
            $CONTRACTOR = $this->getCompanyRequisitesFull($model);
        } else {
            $CONTRACTOR = $this->getContractorRequisitesFull($model);
        }

        // Плательщик
        if ($model->type == Documents::IO_TYPE_OUT) {
            $PAYER = $this->getContractorRequisitesFull($model);
        } else {
            $PAYER = $this->getCompanyRequisitesFull($model);
        }


        //Creating PhpWord ===============================================================================================================
        $phpWord = $this->phpWord;
        $phpWord->setDefaultFontName('Arial');
        $phpWord->setDefaultFontSize(6);

        $properties = $phpWord->getDocInfo();

        $properties->setTitle($title);
        $properties->setCreated(time());
        $properties->setModified(time());

        $section = $phpWord->addSection([
            'marginTop' => 567 / 2,
            'marginLeft' => 2 * 567,
            'marginRight' => 567,
            'marginBottom' => 567 / 2,
        ]);
        $sectionStyle = $section->getStyle();
        $sectionStyle->setOrientation($sectionStyle::ORIENTATION_LANDSCAPE);

        // cell style
        $cellBottom = [
            'valign' => 'bottom'
        ];
        $cellCenter = [
            'valign' => 'center'
        ];
        // paragraph styles with align and empty spaces
        $centerParStyle = [
            'spaceBefore' => 0,
            'spaceAfter' => 0,
            'spacing' => 0,
            'align' => 'center',
        ];
        $leftParStyle = [
            'spaceBefore' => 0,
            'spaceAfter' => 0,
            'spacing' => 0,
            'align' => 'left',
        ];
        $rightParStyle = [
            'spaceBefore' => 0,
            'spaceAfter' => 0,
            'spacing' => 0,
            'align' => 'right',
        ];
        $borderStyle = [
            'borderTopSize' => 6,
            'borderLeftSize' => 6,
            'borderRightSize' => 6,
        ];
        $borderStyleNoTop = [
            'borderLeftSize' => 6,
            'borderRightSize' => 6,
        ];
        $borderBottom = [
            'borderBottomSize' => 6,
        ];
        $borderLeft = [
            'borderLeftSize' => 6,
        ];
        $borderStyleB = [
            'borderTopSize' => 12,
            'borderLeftSize' => 12,
            'borderRightSize' => 12,
        ];
        $borderBottomB = [
            'borderBottomSize' => 12,
        ];
        $borderNoBottom = [
            'borderBottomSize' => 0,
            'borderBottomColor' => 'FFFFFF',
        ];
        $borderNoLeft = [
            'borderLeftSize' => 0,
            'borderLeftColor' => 'FFFFFF',
        ];
        $borderNoTop = [
            'borderTopSize' => 0,
            'borderTopColor' => 'FFFFFF',
        ];
        $borderNetto = [
            'borderTopSize' => 12,
            'borderLeftSize' => 12,
            'borderRightSize' => 12,
            'borderBottomSize' => 0,
        ];
        $borderBrutto = [
            'borderTopSize' => 0,
            'borderLeftSize' => 12,
            'borderRightSize' => 12,
            'borderBottomSize' => 12,
        ];
        $borderB_RL = [
            'borderLeftSize' => 12,
            'borderRightSize' => 12,
        ];

        $rightIndent = ['indentation' => ['left' => 0, 'right' => 120]];
        $leftIndent = ['indentation' => ['left' => 120, 'right' => 0]];

        $gridSpan2 = ['gridSpan' => 2];
        $gridSpan3 = ['gridSpan' => 3];
        $gridSpan4 = ['gridSpan' => 4];
        $gridSpan5 = ['gridSpan' => 5];
        $cellRowSpan = array('vMerge' => 'restart');
        $cellRowContinue = array('vMerge' => 'continue');

        if ($model->type == Documents::IO_TYPE_OUT) {
            $sellerName = strpos($invoice->company_name_short, 'ИП ') === 0 ?
                $invoice->company_name_full :
                $invoice->company_name_short;
        } else {
            $sellerName = strpos($invoice->contractor_name_short, 'ИП ') === 0 ?
                $invoice->contractor_name_full :
                $invoice->contractor_name_short;
        }

        $FULL_WIDTH = 15136;

        $subheadTable = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
            'cellMargin' => 6,
        ]);

        $headTable = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
            'cellMargin' => 0,
        ]);

        $HW_L = 1016;
        $HW_C = 12000;

        $headTable->addRow();
        $headTable->addCell($HW_L + $HW_C, $gridSpan2)->addText('Расходная накладная №' . $model->document_number . ' от ' . $model->document_date, ['size' => 13, 'bold' => true,], $leftParStyle);
        $headTable->addRow(500);
        $headTable->addCell(100)->addText('', ['size' => 13], $centerParStyle);

        $headTable2 = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
            'cellMargin' => 0,
        ]);

        $HW_L = 1016;
        $HW_C = 12000;
        $HW_R = 1000;


        // Поставщик
        $headTable2->addRow();
        $headTable2->addCell($HW_L)->addText('Поставщик', ['size' => 8], $leftParStyle);
        $headTable2->addCell($HW_C)->addText($CONTRACTOR, ['size' => 8], $leftParStyle + $borderBottom);
        $headTable2->addCell(120)->addText('', ['size' => 8], $leftParStyle);
        $headTable2->addRow();
        $headTable2->addCell($HW_L)->addText('', ['size' => 7], $leftParStyle);
        $headTable2->addCell($HW_C)->addText('организация, адрес, телефон, факс, банковские реквизиты', ['size' => 7], $centerParStyle);
        $headTable2->addCell(120)->addText('', ['size' => 7], $leftParStyle);
        $headTable2->addRow();
        $headTable2->addCell(120)->addText('', ['size' => 7], $leftParStyle);
        // Плательщик
        $headTable2->addRow();
        $headTable2->addCell($HW_L)->addText('Плательщик', ['size' => 8], $leftParStyle);
        $headTable2->addCell($HW_C)->addText($PAYER, ['size' => 8], $leftParStyle + $borderBottom);
        $headTable2->addCell(120)->addText('', ['size' => 8], $leftParStyle);
        $headTable2->addRow();
        $headTable2->addCell($HW_L)->addText('', ['size' => 7], $leftParStyle);
        $headTable2->addCell($HW_C)->addText('организация, адрес, телефон, факс, банковские реквизиты', ['size' => 7], $centerParStyle);
        $headTable2->addCell(120)->addText('', ['size' => 7], $leftParStyle);
        $headTable2->addRow(700);
        $headTable2->addCell(120)->addText('', ['size' => 7], $leftParStyle);

        // TABLE

        $table = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => '000000',
            'borderSize' => 6,
            'cellMargin' => 20,
        ]);

        $colWidth = [
            1 => 300,
            2 => 4186,
            3 => 800,
            4 => 800,
            5 => 700,
            6 => 800,
            7 => 700,
            8 => 600,
            9 => 650,
            10 => 1000,
            11 => 800,
            12 => 1100,
            13 => 700,
            14 => 800,
            15 => 1200
        ];

        $table->addRow();
        $table->addCell($colWidth[1],  $cellCenter + $cellRowSpan)->addText('Номер по порядку', ['size' => 8], $centerParStyle);
        $table->addCell($colWidth[2] + $colWidth[3],  $cellCenter + $gridSpan2)->addText('Товар', ['size' => 8], $centerParStyle);
        $table->addCell($colWidth[3] + $colWidth[4],  $cellCenter + $gridSpan2)->addText('Единица измерения', ['size' => 8], $centerParStyle);
        $table->addCell($colWidth[6],  $cellCenter + $cellRowSpan)->addText('Вид упаковки', ['size' => 8], $centerParStyle);
        $table->addCell($colWidth[7] + $colWidth[8],  $cellCenter + $gridSpan2)->addText('Количество', ['size' => 8], $centerParStyle);
        $table->addCell($colWidth[9],  $cellCenter + $cellRowSpan)->addText('Масса брутто', ['size' => 8], $centerParStyle);
        $table->addCell($colWidth[10], $cellCenter + $cellRowSpan)->addText('Количество (масса нетто)', ['size' => 8], $centerParStyle);
        $table->addCell($colWidth[11], $cellCenter + $cellRowSpan)->addText('Цена, руб. коп.', ['size' => 8], $centerParStyle);
        $table->addCell($colWidth[12], $cellCenter + $cellRowSpan)->addText('Сумма без учета НДС, руб. коп.', ['size' => 8], $centerParStyle);
        $table->addCell($colWidth[13] + $colWidth[14], $cellCenter + $gridSpan2)->addText('НДС', ['size' => 8], $centerParStyle);
        $table->addCell($colWidth[15], $cellCenter + $cellRowSpan)->addText('Сумма с учетом НДС, руб. коп.', ['size' => 8], $centerParStyle);

        $table->addRow();
        $table->addCell($colWidth[1],  $cellRowContinue);
        $table->addCell($colWidth[2],  $cellCenter)->addText('наименование, характеристика, сорт, артикул', ['size' => 8], $centerParStyle);
        $table->addCell($colWidth[3],  $cellCenter)->addText('код', ['size' => 8], $centerParStyle);
        $table->addCell($colWidth[4],  $cellCenter)->addText('наимено- вание', ['size' => 8], $centerParStyle);
        $table->addCell($colWidth[5],  $cellCenter)->addText('код по ОКЕИ', ['size' => 8], $centerParStyle);
        $table->addCell($colWidth[6],  $cellRowContinue);
        $table->addCell($colWidth[7],  $cellCenter)->addText('в одном месте', ['size' => 8], $centerParStyle);
        $table->addCell($colWidth[8],  $cellCenter)->addText('мест штук', ['size' => 8], $centerParStyle);
        $table->addCell($colWidth[9],  $cellRowContinue);
        $table->addCell($colWidth[10], $cellRowContinue);
        $table->addCell($colWidth[11], $cellRowContinue);
        $table->addCell($colWidth[12], $cellRowContinue);
        $table->addCell($colWidth[13], $cellCenter)->addText('ставка, %', ['size' => 8], $centerParStyle);
        $table->addCell($colWidth[14], $cellCenter)->addText('сумма, руб. коп.', ['size' => 8], $centerParStyle);
        $table->addCell($colWidth[15], $cellRowContinue);

        $table->addRow();
        for ($i=1; $i<=15; $i++) {
            $table->addCell($colWidth[$i], $cellBottom)->addText($i, ['size' => 7], $centerParStyle);
        }

        $orderQuery = OrderSalesInvoice::find()->where(['sales_invoice_id'=>$model->id]);
        $orderArray = $orderQuery->all();
        foreach ($orderArray as $key => $order) {
            /** @var OrderSalesInvoice $order */
            if ($model->invoice->hasNds) {
                $priceNoNds = $order->priceNoNds;
                $amountNoNds = $model->getPrintOrderAmount($order->order_id, true);
                $amountWithNds = $model->getPrintOrderAmount($order->order_id, false);
                $TaxRateName = $order->order->saleTaxRate->name;
                $ndsAmount = TextHelper::invoiceMoneyFormat(($amountWithNds - $amountNoNds), $precision);
            } else {
                $priceNoNds = $order->priceWithNds;
                $amountNoNds = $model->getPrintOrderAmount($order->order_id, false);
                $amountWithNds = $model->getPrintOrderAmount($order->order_id, false);
                $TaxRateName = 'Без НДС';
                $ndsAmount = Product::DEFAULT_VALUE;
            }
            if ($order->quantity != intval($order->quantity)) {
                $order->quantity = rtrim(number_format($order->quantity, 10, '.', ''), 0);
            }

            $colValue[1]  = $key + 1;
            $colValue[2]  = htmlspecialchars($order->order->product_title);
            $colValue[3]  = $order->order->article;
            $colValue[4]  = $order->order->unit ? $order->order->unit->name : Product::DEFAULT_VALUE;;
            $colValue[5]  = $order->order->unit ? $order->order->unit->code_okei : Product::DEFAULT_VALUE;;
            $colValue[6]  = $order->order->box_type;
            $colValue[7]  = $order->order->count_in_place;
            $colValue[8]  = $order->order->place_count;
            $colValue[9]  = $order->order->mass_gross;
            $colValue[10] = $order->quantity;
            $colValue[11] = TextHelper::invoiceMoneyFormat($priceNoNds, $precision);
            $colValue[12] = TextHelper::invoiceMoneyFormat($amountNoNds, $precision);
            $colValue[13] = $TaxRateName;
            $colValue[14] = $ndsAmount;
            $colValue[15] = TextHelper::invoiceMoneyFormat($amountWithNds, $precision);

            $table->addRow();
            for ($i=1; $i<=15; $i++) {
                $table->addCell($colWidth[$i], $cellCenter)->addText($colValue[$i], ['size' => 8], ($i == 2) ? ($leftParStyle) : $centerParStyle);
            }
        }

        $totalQuantity = $orderQuery->sum('quantity') * 1;
        if ($totalQuantity != intval($totalQuantity)) {
            $totalQuantity = rtrim(number_format($totalQuantity, 10, '.', ''), 0);
        }

        $colValue = [];
        $colValue[8] = $model->invoice->total_place_count;
        $colValue[9] = $model->invoice->total_mass_gross;
        $colValue[10] = $totalQuantity;
        $colValue[11] = 'X';
        $colValue[12] = TextHelper::invoiceMoneyFormat($model->invoice->hasNds ? $model->getPrintAmountNoNds() : $model->getPrintAmountWithNds(), 2);
        $colValue[13] = 'X';
        $colValue[14] = ($model->invoice->hasNds) ? TextHelper::invoiceMoneyFormat($model->totalPLNds, 2) : '';
        $colValue[15] = TextHelper::invoiceMoneyFormat($model->getPrintAmountWithNds(), 2);

        $table->addRow();
        $table->addCell(array_sum(array_slice($colWidth, 0, 7)), $borderNoBottom + $borderNoLeft + $cellCenter + ['gridSpan' => 7])->addText('Итого', ['size' => 8], $rightParStyle + $rightIndent);
        for ($i=8; $i<=15; $i++) {
            $table->addCell($colWidth[$i], $cellCenter)->addText($colValue[$i], ['size' => 8], $centerParStyle);
        }

        $colValue = [];
        $colValue[8] = $model->invoice->total_place_count;
        $colValue[9] = $model->invoice->total_mass_gross;
        $colValue[10] = $totalQuantity;
        $colValue[11] = 'X';
        $colValue[12] = TextHelper::invoiceMoneyFormat($model->invoice->hasNds ? $model->getPrintAmountNoNds() : $model->getPrintAmountWithNds(), 2);
        $colValue[13] = 'X';
        $colValue[14] = ($model->invoice->hasNds) ? TextHelper::invoiceMoneyFormat($model->totalPLNds, 2) : '';
        $colValue[15] = TextHelper::invoiceMoneyFormat($model->getPrintAmountWithNds(), 2);

        $table->addRow();
        $table->addCell(array_sum(array_slice($colWidth, 0, 7)), $borderNoTop + $borderNoBottom + $borderNoLeft + $cellCenter + ['gridSpan' => 7])->addText('Всего по накладной', ['size' => 8], $rightParStyle + $rightIndent);
        for ($i=8; $i<=15; $i++) {
            $table->addCell($colWidth[$i], $cellCenter)->addText($colValue[$i], ['size' => 8], $centerParStyle);
        }

        $footerTotalSumInWords = \common\components\TextHelper::amountToWords($model->getPrintAmountWithNds() / 100);

        $footer = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
            'cellMargin' => 0,
        ]);

        $HR = [
            1 => 3448,
            2 => 3000,
            3=> 800,
            4 => 120,
            5 => 120,
            6 => 2048,
            7 => 5600
        ];

        $footer->addRow(700);
        $footer->addCell(120)->addText('', ['size' => 6], $centerParStyle);
        $footer->addRow();
        $footer->addCell($HR[1] + $HR[2] + $HR[3] + $HR[4], $gridSpan4)
            ->addText('Всего наименований '.count($orderArray).', на сумму ' . TextHelper::invoiceMoneyFormat($model->getTotalAmountWithNds(), 2), ['size' => 8, 'bold' => true], $leftParStyle);
        $footer->addRow();
        $footer->addCell($HR[1] + $HR[2] + $HR[3], $gridSpan3 + $borderBottom)->addText($footerTotalSumInWords, ['size' => 8, 'bold' => true], $leftParStyle);
        $footer->addRow();
        $footer->addCell($HR[1] + $HR[2] + $HR[3], $gridSpan3 + $cellCenter)->addText('(прописью)', ['size' => 6], $centerParStyle);

        $HR_L = [
            1 => 2048,
            2 => 500,
            3 => 2500,
            4 => 120,
            5 => 120,
            6 => 1960,
            7 => 120
        ];
        $HR_R = [
            1 => 2048,
            2 => 500,
            3 => 2500,
            4 => 120,
            5 => 120,
            6 => 1960,
            7 => 120
        ];

        $footer2 = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
            'cellMargin' => 0,
        ]);

        // Отпуск груза произвел
        $footer2->addRow(1200);
        $footer2->addCell($HR_L[1], $cellBottom)->addText('Отпустил', ['size' => 8], $centerParStyle);
        $footer2->addCell($HR_L[2], $cellBottom)->addText('', ['size' => 8], $leftParStyle);
        $footer2->addCell($HR_L[3], $cellBottom + $borderBottom)->addText('', ['size' => 8], $centerParStyle);
        $footer2->addCell($HR_L[4], $cellBottom)->addText('', ['size' => 8], $leftParStyle);
        $footer2->addCell($HR_L[5], $cellBottom)->addText('', ['size' => 8], $leftParStyle);
        $footer2->addCell($HR_L[6], $cellBottom)->addText('', ['size' => 8], $leftParStyle);
        $footer2->addCell($HR_L[7], $cellBottom)->addText('', ['size' => 8], $leftParStyle);
        $footer2->addCell($HR_R[1], $cellBottom)->addText('Получил', ['size' => 8], $centerParStyle);
        $footer2->addCell($HR_R[2], $cellBottom)->addText('', ['size' => 8], $leftParStyle);
        $footer2->addCell($HR_R[3], $cellBottom + $borderBottom)->addText('', ['size' => 8], $centerParStyle);
        $footer2->addCell($HR_R[4], $cellBottom)->addText('', ['size' => 8], $leftParStyle);
        $footer2->addCell($HR_R[5], $cellBottom)->addText('', ['size' => 8], $leftParStyle);
        $footer2->addCell($HR_R[6], $cellBottom)->addText('', ['size' => 8], $leftParStyle);
        $footer2->addCell($HR_R[7], $cellBottom)->addText('', ['size' => 8], $leftParStyle);
        $footer2->addRow();
        $footer2->addCell($HR_L[1], $cellBottom)->addText('', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_L[2], $cellBottom)->addText('', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_L[3], $cellBottom)->addText('(подпись)', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_L[4], $cellBottom)->addText('', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_L[5], $cellBottom)->addText('', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_L[6], $cellBottom)->addText('', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_L[7], $cellBottom)->addText('', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_R[1], $cellBottom + $borderLeft)->addText('', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_R[2], $cellBottom)->addText('', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_R[3], $cellBottom)->addText('(подпись)', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_R[4], $cellBottom)->addText('', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_R[5], $cellBottom)->addText('', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_R[6], $cellBottom)->addText('', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_R[7], $cellBottom)->addText('', ['size' => 6], $centerParStyle);
        // М.П.

        //$section->addTextBreak(1, [], $leftParStyle);

    }

    public function getFileName()
    {
        return $this->model->getPrintTitle() . '.docx';
    }

    public function getFile()
    {
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=\"{$this->fileName}\"");
        header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Expires: 0');

        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($this->phpWord, 'Word2007');

        $objWriter->save("php://output");

        exit;
    }

    public function saveFile($path)
    {
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($this->phpWord, 'Word2007');

        return $objWriter->save($path);
    }

    private function getContractorRequisitesFull($model)
    {
        $ret = $model->invoice->contractor_name_short;
        $ret .= $model->invoice->contractor_inn ? ", ИНН {$model->invoice->contractor_inn}" : '';
        $ret .= $model->invoice->contractor_kpp ? ", КПП {$model->invoice->contractor_kpp}" : '';
        $ret .= $model->invoice->contractor_address_legal_full ? ", {$model->invoice->contractor_address_legal_full}" : '';
        $ret .= $model->invoice->contractor_rs ? ", р/с {$model->invoice->contractor_rs}" : '';
        $ret .= $model->invoice->contractor_bank_name ? ", в банке {$model->invoice->contractor_bank_name}" : '';
        $ret .= $model->invoice->contractor_bik ? ", БИК {$model->invoice->contractor_bik}" : '';
        $ret .= $model->invoice->contractor_ks ? ", к/с {$model->invoice->contractor_ks}" : '';

        return $ret;
    }

    private function getCompanyRequisitesFull($model)
    {
        $ret = $model->invoice->company_name_short . ', ИНН ' . $model->invoice->company_inn;
        $ret .= $model->invoice->company_kpp ? (", КПП {$model->invoice->company_kpp}") : '';
        $ret .= ", {$model->invoice->company_address_legal_full}";
        $ret .= ", р/с {$model->invoice->company_rs}";
        $ret .= ", в банке {$model->invoice->company_bank_name}";
        $ret .= ", БИК {$model->invoice->company_bik}";
        $ret .= $model->invoice->company_ks ? ", к/с {$model->invoice->company_ks}" : '';

        return $ret;
    }
}
