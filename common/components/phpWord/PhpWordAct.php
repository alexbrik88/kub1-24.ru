<?php

namespace common\components\phpWord;

use common\components\date\DateHelper;
use common\components\TextHelper;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\document\Act;
use common\models\product\Product;
use frontend\models\Documents;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Shared\Converter;
use yii\base\Component;
use yii\helpers\Html;

/**
 * Class PdfRenderer
 * @package common\components\pdf
 */
class PhpWordAct extends Component
{
    protected $model;
    protected $phpWord;

    /**
     * @param Act $model
     * @param array $config
     */
    public function __construct(Act $model, $config = [])
    {
        $this->model = $model;
        $this->phpWord = new \PhpOffice\PhpWord\PhpWord();

        parent::__construct($config);
    }

    public function init()
    {
        parent::init();

        $model = $this->model;
        $model->isGroupOwnOrdersByProduct = true;
        $invoice = $model->invoice;
        $precision = $invoice->price_precision;
        $company = $invoice->company;
        $contractor = $invoice->contractor;
        $orderArray = $model->ownOrders;
        $orderCount = count($orderArray);
        $hasNds = $model->invoice->hasNds;
        $dateFormated = \php_rutils\RUtils::dt()->ruStrFTime([
            'date' => $model->document_date,
            'format' => 'd F Y г.',
            'monthInflected' => true,
        ]);
        $title = "Акт № {$model->fullNumber} от {$dateFormated}";

        $director_name = '';
        if ($contractor->director_in_act) {
            if (!empty($invoice->contractor_director_name) && $invoice->contractor_director_name != 'ФИО Руководителя') {
                $director_name = TextHelper::nameShort($invoice->contractor_director_name);
            } elseif (!empty($contractor->director_name) && $contractor->director_name != 'ФИО Руководителя') {
                $director_name = TextHelper::nameShort($contractor->director_name);
            }
        } elseif ($contractor->chief_accountant_in_act) {
            if (!empty($contractor->chief_accountant_name)) {
                $director_name = TextHelper::nameShort($contractor->chief_accountant_name);
            }
        } elseif ($contractor->contact_in_act) {
            if (!empty($contractor->contact_name)) {
                $director_name = TextHelper::nameShort($contractor->contact_name);
            }
        }

        $reasonText = $model->getBasisName();

        //Creating PhpWord
        $phpWord = $this->phpWord;
        $phpWord->setDefaultFontName('Arial');
        $phpWord->setDefaultFontSize(9);

        $properties = $phpWord->getDocInfo();

        $properties->setTitle($title);
        $properties->setCreated(time());
        $properties->setModified(time());

        $section = $phpWord->addSection([
            'marginTop' => 600,
            'marginLeft' => 900,
            'marginRight' => 900,
            'marginBottom' => 900,
        ]);

        // Title
        $section->addText($title, ['size' => 14, 'bold' => true], [
            'align' => 'center',
            'spaceBefore' => 0,
            'spaceAfter' => 100,
        ]);
        // end Title

        $section->addTextBreak(1);

        // Top Table
        $topTable = $section->addTable([
            'bidiVisual' => 0,
            'cellMarginTop' => 100,
            'cellMarginBottom' => 0,
            'cellMarginRight' => 0,
            'cellMarginLeft' => 0,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
        ]);
        $topTableCellStyle = [
            'valign' => 'top',
            'cellMarginTop' => 100,
            'cellMarginBottom' => 0,
            'cellMarginRight' => 0,
            'cellMarginLeft' => 0,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
        ];
        $topTable->addRow();
        $topTable->addCell(1300, $topTableCellStyle)->addText('Исполнитель:');
        $topTable->addCell(9000, $topTableCellStyle)->addText($model->getExecutorFullRequisites(), ['bold' => true]);
        $topTable->addRow();
        $topTable->addCell(1300, $topTableCellStyle)->addText('Заказчик:');
        $topTable->addCell(9000, $topTableCellStyle)->addText($model->getCustomerFullRequisites(), ['bold' => true]);
        $topTable->addRow();
        $topTable->addCell(1300, $topTableCellStyle)->addText('Основание:');
        $topTable->addCell(9000, $topTableCellStyle)->addText($reasonText, ['bold' => true]);
        // end Top Table

        $section->addTextBreak(1);

        // Product Table
        $productTable = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => '000000',
            'borderSize' => 6,
            'cellMargin' => 50,
        ]);
        $headCellStyle = [
            'valign' => 'center',
            'cellMargin' => 50,
            'borderColor' => '000000',
            'borderTopSize' => 12,
            'borderRightSize' => 6,
            'borderBottomSize' => 6,
            'borderLeftSize' => 6,
        ];
        $prodCellStyle = [
            'valign' => 'top',
            'cellMargin' => 50,
            'borderColor' => '000000',
            'borderTopSize' => 6,
            'borderRightSize' => 6,
            'borderBottomSize' => 6,
            'borderLeftSize' => 6,
        ];
        $leftCellStyle = [
            'borderLeftSize' => 12,
        ];
        $rightCellStyle = [
            'borderRightSize' => 12,
        ];
        $bottomCellStyle = [
            'borderBottomSize' => 12,
        ];
        $productTable->addRow(600);
        $productTable->addCell(600, array_merge($headCellStyle, $leftCellStyle))
            ->addText('№', ['bold' => true], ['align' => 'center']);
        $productTable->addCell(5500, $headCellStyle)
            ->addText('Наименование работ, услуг', ['bold' => true], ['align' => 'center']);
        $productTable->addCell(1000, $headCellStyle)
            ->addText('Кол-вo', ['bold' => true], ['align' => 'center']);
        $productTable->addCell(800, $headCellStyle)
            ->addText('Ед', ['bold' => true], ['align' => 'center']);
        $productTable->addCell(1200, $headCellStyle)
            ->addText('Цена', ['bold' => true], ['align' => 'center']);
        $productTable->addCell(1200, array_merge($headCellStyle, $rightCellStyle))
            ->addText('Сумма', ['bold' => true], ['align' => 'center']);

        foreach ($orderArray as $key => $order) {
            $unitName = $order->order->unit ? $order->order->unit->name : Product::DEFAULT_VALUE;
            $orderQuantity = $unitName == Product::DEFAULT_VALUE ? $unitName : $order->quantity;
            if ($hasNds && $company->isNdsExclude) {
                $price = TextHelper::invoiceMoneyFormat($order->priceNoNds, $precision);
                $sum = TextHelper::invoiceMoneyFormat($model->getPrintOrderAmount($order->order_id, true), $precision);
            } else {
                $price = TextHelper::invoiceMoneyFormat($order->priceWithNds, $precision);
                $sum = TextHelper::invoiceMoneyFormat($model->getPrintOrderAmount($order->order_id, false), $precision);
            }
            $currentCellStyle = (($key + 1) == $orderCount) ? array_merge($prodCellStyle, $bottomCellStyle) : $prodCellStyle;

            $productTable->addRow(200);
            $productTable->addCell(600, array_merge($currentCellStyle, $leftCellStyle))->addText(++$key, [], ['align' => 'center']);
            $productTable->addCell(5600, $currentCellStyle)->addText(htmlspecialchars($order->order->product_title), [], ['align' => 'left']);
            $productTable->addCell(1000, $currentCellStyle)->addText($orderQuantity, [], ['align' => 'right']);
            $productTable->addCell(1000, $currentCellStyle)->addText($unitName, [], ['align' => 'right']);
            $productTable->addCell(1000, $currentCellStyle)->addText($price, [], ['align' => 'right']);
            $productTable->addCell(1000, array_merge($currentCellStyle, $rightCellStyle))->addText($sum, [], ['align' => 'right']);
        }
        // end Product Table

        //$section->addTextBreak(1);

        // Total Table
        if ($hasNds && $company->isNdsExclude) {
            $amount = TextHelper::invoiceMoneyFormat($model->getPrintAmountNoNds(), 2);
        } else {
            $amount = TextHelper::invoiceMoneyFormat($model->getPrintAmountWithNds(), 2);
        }

        $totalTable = $section->addTable([
            'bidiVisual' => 0,
            'cellMargin' => 50,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
        ]);
        $totalTableCellStyle = [
            'cellMargin' => 50,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
        ];
        $totalTable->addRow(200);
        $totalTable->addCell(9000, $totalTableCellStyle)->addText('Итого:', ['bold' => true], ['align' => 'right']);
        $totalTable->addCell(1300, $totalTableCellStyle)->addText($amount, [], ['align' => 'right']);
        $totalTable->addRow(200);
        $totalTable->addCell(9000, $totalTableCellStyle)->addText(
            $hasNds ? ($company->isNdsExclude ? '' : 'В том числе ') . 'НДС:' : 'Без налога (НДС):',
            ['bold' => true],
            ['align' => 'right']
        );
        $totalTable->addCell(1300, $totalTableCellStyle)->addText(
            $hasNds ? TextHelper::invoiceMoneyFormat($model->totalActNds, 2) : '-',
            [],
            ['align' => 'right']
        );
        // end Total Table

        // Total Text
        $actSum = TextHelper::invoiceMoneyFormat($model->getPrintAmountWithNds(), 2);
        $text1 = "Всего оказано услуг {$orderCount}, на сумму {$actSum} руб.";
        $text2 = TextHelper::mb_ucfirst(TextHelper::amountToWords($model->getPrintAmountWithNds() / 100));
        $text3 = $model->comment;

        $section->addText($text1, null, [
            'spaceBefore' => 0,
            'spaceAfter' => 0,
        ]);
        $section->addText($text2, ['bold' => true], [
            'spaceBefore' => 0,
            'spaceAfter' => 0,
        ]);
        $section->addTextBreak();
        $section->addText($text3);
        // end Total Text

        $section->addTextBreak(2);

        // Sign Table
        $signedByName = $model->signed_by_name ? $model->signed_by_name : $model->invoice->getCompanyChiefFio(true);
        if ($model->signed_by_employee_id) {
            $signedByText = "/ {$signedByName}\nпо" . mb_strtolower($model->signBasisDocument->name2) . ' №' . $model->sign_document_number .
                "\nот " . DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . "г. ";
        } else {
            $signedByText = "/ {$signedByName} /";
        }

        $noBorderStyle = [
            'valign' => 'top',
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
        ];
        $signCellStyle = [
            'valign' => 'bottom',
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
            'borderBottomColor' => '000000',
            'borderBottomSize' => 6,
        ];
        $signTable = $section->addTable();
        $signTable->addRow();
        $signTable->addCell(5000, $noBorderStyle)->addText('ИСПОЛНИТЕЛЬ', ['bold' => true]);
        $signTable->addCell(200, $noBorderStyle);
        $signTable->addCell(5000, $noBorderStyle)->addText('ЗАКАЗЧИК', ['bold' => true]);
        $signTable->addRow();
        $signTable->addCell(5000, $noBorderStyle)->addText($model->type == Documents::IO_TYPE_OUT ?
            $invoice->company_name_short : $invoice->contractor_name_short);
        $signTable->addCell(200, $noBorderStyle);
        $signTable->addCell(5000, $noBorderStyle)->addText($model->type == Documents::IO_TYPE_OUT ?
            $invoice->contractor_name_short : $invoice->company_name_short);
        $signTable->addRow();
        $signTable->addCell(5000, $noBorderStyle)->addText($model->type == Documents::IO_TYPE_OUT ?
            ($company->company_type_id == CompanyType::TYPE_IP ? '' : ($company->chief_post_name .' ')) :
            ($invoice->contractor->company_type_id == CompanyType::TYPE_IP || $invoice->contractor->face_type == Contractor::TYPE_PHYSICAL_PERSON ? '' : ($invoice->contractor_director_post_name .' ')));
        $signTable->addCell(200, $noBorderStyle);
        $signTable->addCell(5000, $noBorderStyle)->addText($model->type == Documents::IO_TYPE_OUT ?
            ($invoice->contractor->company_type_id == CompanyType::TYPE_IP || $invoice->contractor->face_type == Contractor::TYPE_PHYSICAL_PERSON ? '' : ($invoice->contractor_director_post_name .' ')) :
            ($company->company_type_id == CompanyType::TYPE_IP ? '' : ($company->chief_post_name .' ')));
        $signTable->addRow(600);
        $signTable->addCell(5000, $signCellStyle)->addText($model->type == Documents::IO_TYPE_OUT ?
            $signedByText :
            ($director_name ? '/ ' . $director_name . ' /' : ''), [], ['align' => 'right']);
        $signTable->addCell(200, $noBorderStyle);
        $signTable->addCell(5000, $signCellStyle)->addText($model->type == Documents::IO_TYPE_OUT ?
            ($director_name ? '/ ' . $director_name . ' /' : '') :
            $signedByText, [], ['align' => 'right']);
        // end Sign Table
    }

    public function getFileName()
    {
        return $this->model->getPrintTitle() . '.docx';
    }

    public function getFile()
    {
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=\"{$this->fileName}\"");
        header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Expires: 0');

        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($this->phpWord, 'Word2007');

        $objWriter->save("php://output");

        exit;
    }

    public function saveFile($path)
    {
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($this->phpWord, 'Word2007');

        return $objWriter->save($path);
    }
}