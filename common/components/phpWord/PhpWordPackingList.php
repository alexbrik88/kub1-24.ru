<?php

namespace common\components\phpWord;

use common\components\date\DateHelper;
use common\components\image\EasyThumbnailImage;
use common\components\TextHelper;
use common\models\Contractor;
use common\models\company\CompanyType;
use common\models\currency\Currency;
use common\models\document\Invoice;
use common\models\document\PackingList;
use common\models\document\InvoiceContractorSignature;
use common\models\product\Product;
use frontend\models\Documents;
use php_rutils\RUtils;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Shared\Converter;
use yii\base\Component;
use yii\helpers\Html;
use common\models\document\OrderPackingList;

/**
 * Class PdfRenderer
 * @package common\components\pdf
 */
class PhpWordPackingList extends Component
{
    protected $model;
    protected $phpWord;

    /**
     * @param PackingList $model
     * @param array $config
     */
    public function __construct(PackingList $model, $config = [])
    {
        $this->model = $model;
        $this->phpWord = new \PhpOffice\PhpWord\PhpWord();

        parent::__construct($config);
    }

    public function init()
    {
        parent::init();

        /** @var PackingList $model */
        /** @var Invoice $invoice */

        $model = $this->model;
        $invoice = $model->invoice;
        $precision = $invoice->price_precision;
        $company = $invoice->company;
        $contractor = $invoice->contractor;
        $orderArray = $invoice->getOrders()->all();
        $orderCount = count($orderArray);
        $hasNds = $invoice->hasNds;
        $dateFormated = \php_rutils\RUtils::dt()->ruStrFTime([
            'date' => $model->document_date,
            'format' => 'd F Y г.',
            'monthInflected' => true,
        ]);
        $name = 'Товарная накладная';
        $title = "{$name}";
        $invoiceContractorSignature = $invoice->company->invoiceContractorSignature;
        if (!$invoiceContractorSignature) {
            $invoiceContractorSignature = new InvoiceContractorSignature();
            $invoiceContractorSignature->company_id = $invoice->company_id;
        }
        $realMassNet = $model->invoice->getRealMassNet();
        $realMassGross = $model->invoice->getRealMassGross();

        // Грузоотправитель
        $CONSIGNOR = '';
        if ($model->consignor) {
            $CONSIGNOR = $model->consignor->getRequisitesFull();
        } elseif ($model->type == Documents::IO_TYPE_OUT) {
            $CONSIGNOR = $this->getCompanyRequisitesFull($model);
        } else {
            $CONSIGNOR = $this->getContractorRequisitesFull($model);
        }

        // Грузополучатель
        if ($model->consignee) {
            if ($model->contractor_address == PackingList::CONTRACTOR_ADDRESS_LEGAL) {
                $address = $model->consignee->legal_address;
            } else {
                $address = $model->consignee->actual_address;
            }
            $CONSIGNEE = $model->consignee->getRequisitesFull($address);
        } elseif ($model->type == Documents::IO_TYPE_OUT) {
            $CONSIGNEE = $this->getContractorRequisitesFull($model);
        } else {
            $CONSIGNEE = $this->getCompanyRequisitesFull($model);
        }

        // Поставщик
        if ($model->type == Documents::IO_TYPE_OUT) {
            $CONTRACTOR = $this->getCompanyRequisitesFull($model);
        } else {
            $CONTRACTOR = $this->getContractorRequisitesFull($model);
        }

        // Плательщик
        if ($model->type == Documents::IO_TYPE_OUT) {
            $PAYER = $this->getContractorRequisitesFull($model);
        } else {
            $PAYER = $this->getCompanyRequisitesFull($model);
        }


        //Creating PhpWord ===============================================================================================================
        $phpWord = $this->phpWord;
        $phpWord->setDefaultFontName('Arial');
        $phpWord->setDefaultFontSize(6);

        $properties = $phpWord->getDocInfo();

        $properties->setTitle($title);
        $properties->setCreated(time());
        $properties->setModified(time());

        $section = $phpWord->addSection([
            'marginTop' => 567 / 2,
            'marginLeft' => 2 * 567,
            'marginRight' => 567,
            'marginBottom' => 567 / 2,
        ]);
        $sectionStyle = $section->getStyle();
        $sectionStyle->setOrientation($sectionStyle::ORIENTATION_LANDSCAPE);

        // cell style
        $cellBottom = [
            'valign' => 'bottom'
        ];
        $cellCenter = [
            'valign' => 'center'
        ];
        // paragraph styles with align and empty spaces
        $centerParStyle = [
            'spaceBefore' => 0,
            'spaceAfter' => 0,
            'spacing' => 0,
            'align' => 'center',
        ];
        $leftParStyle = [
            'spaceBefore' => 0,
            'spaceAfter' => 0,
            'spacing' => 0,
            'align' => 'left',
        ];
        $rightParStyle = [
            'spaceBefore' => 0,
            'spaceAfter' => 0,
            'spacing' => 0,
            'align' => 'right',
        ];
        $borderStyle = [
            'borderTopSize' => 6,
            'borderLeftSize' => 6,
            'borderRightSize' => 6,
        ];
        $borderStyleNoTop = [
            'borderLeftSize' => 6,
            'borderRightSize' => 6,
        ];
        $borderBottom = [
            'borderBottomSize' => 6,
        ];
        $borderLeft = [
            'borderLeftSize' => 6,
        ];
        $borderStyleB = [
            'borderTopSize' => 12,
            'borderLeftSize' => 12,
            'borderRightSize' => 12,
        ];
        $borderBottomB = [
            'borderBottomSize' => 12,
        ];
        $borderNoBottom = [
            'borderBottomSize' => 0,
            'borderBottomColor' => 'FFFFFF',
        ];
        $borderNoLeft = [
            'borderLeftSize' => 0,
            'borderLeftColor' => 'FFFFFF',
        ];
        $borderNoTop = [
            'borderTopSize' => 0,
            'borderTopColor' => 'FFFFFF',
        ];
        $borderNetto = [
            'borderTopSize' => 12,
            'borderLeftSize' => 12,
            'borderRightSize' => 12,
            'borderBottomSize' => 0,
        ];
        $borderBrutto = [
            'borderTopSize' => 0,
            'borderLeftSize' => 12,
            'borderRightSize' => 12,
            'borderBottomSize' => 12,
        ];
        $borderB_RL = [
            'borderLeftSize' => 12,
            'borderRightSize' => 12,
        ];

        $rightIndent = ['indentation' => ['left' => 0, 'right' => 120]];
        $leftIndent = ['indentation' => ['left' => 120, 'right' => 0]];

        $gridSpan2 = ['gridSpan' => 2];
        $gridSpan3 = ['gridSpan' => 3];
        $gridSpan4 = ['gridSpan' => 4];
        $gridSpan5 = ['gridSpan' => 5];
        $cellRowSpan = array('vMerge' => 'restart');
        $cellRowContinue = array('vMerge' => 'continue');

        if ($model->type == Documents::IO_TYPE_OUT) {
            $sellerName = strpos($invoice->company_name_short, 'ИП ') === 0 ?
                $invoice->company_name_full :
                $invoice->company_name_short;
        } else {
            $sellerName = strpos($invoice->contractor_name_short, 'ИП ') === 0 ?
                $invoice->contractor_name_full :
                $invoice->contractor_name_short;
        }

        $FULL_WIDTH = 15136;

        $subheadTable = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
            'cellMargin' => 6,
        ]);

        $subheadTable->addRow()->addCell($FULL_WIDTH)->addText("Унифицированая форма № ТОРГ-12", ['size' => 6], $rightParStyle);
        $subheadTable->addRow()->addCell($FULL_WIDTH)->addText("Утверждена постановлением Госкомстата России от 25.12.98 № 132", ['size' => 6], $rightParStyle);

        $headTable = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
            'cellMargin' => 0,
        ]);

        // LOGO
        $HW_L = 13136;
        $HW_R = 1000;

        $headTable->addRow();
        $headTable->addCell($HW_L + $HW_R, $gridSpan2)->addText('', ['size' => 8], $rightParStyle);
        $headTable->addCell($HW_R, $borderStyle  + $cellCenter)->addText('Коды', ['size' => 8], $centerParStyle);

        // ROWS 1

        $headTable->addRow();
        $headTable->addCell($HW_L + $HW_R, $gridSpan2 + $cellCenter)->addText('Форма по ОКУД', ['size' => 8], $rightParStyle + $rightIndent);
        $headTable->addCell($HW_R, $borderStyle + $cellBottom)->addText('330212', ['size' => 9, 'bold' => true], $centerParStyle);

        $headTable->addRow();
        $headTable->addCell($HW_L)->addText($CONSIGNOR, ['size' => 8], $leftParStyle + $borderBottom);
        $headTable->addCell($HW_R, $cellBottom)->addText('по ОКПО', ['size' => 8], $rightParStyle + $rightIndent);
        $headTable->addCell($HW_R, $borderStyle + $cellBottom)->addText($model->consignor ? '' : $model->invoice->company_okpo, ['size' => 8, 'bold' => true], $centerParStyle);
        $headTable->addRow();
        $headTable->addCell($HW_L)->addText('организация-грузоотправитель, адрес, телефон, факс, банковские реквизиты', ['size' => 7], $centerParStyle);
        $headTable->addCell($HW_R)->addText('', ['size' => 7], $centerParStyle);
        $headTable->addCell($HW_R, $borderStyle)->addText('', ['size' => 7], $centerParStyle);

        $headTable->addRow();
        $headTable->addCell($HW_L)->addText('', ['size' => 8], $leftParStyle + $borderBottom);
        $headTable->addCell($HW_R)->addText('', ['size' => 8], $rightParStyle + $rightIndent);
        $headTable->addCell($HW_R, $borderStyleNoTop)->addText('', ['size' => 8], $centerParStyle);
        $headTable->addRow();
        $headTable->addCell($HW_L)->addText('структурное подразделение', ['size' => 7], $centerParStyle);
        $headTable->addCell($HW_R)->addText('', ['size' => 7], $rightParStyle + $rightIndent);
        $headTable->addCell($HW_R, $borderStyle)->addText('', ['size' => 7], $centerParStyle);

        $headTable->addRow();
        $headTable->addCell($HW_L + $HW_R, $gridSpan2)->addText('Вид деятельности по ОКДП', ['size' => 8], $rightParStyle + $rightIndent);
        $headTable->addCell($HW_R, $borderStyleNoTop + ['borderBottomSize' => 7])->addText('', ['size' => 8, 'bold' => true], $centerParStyle);

        // ROWS 2
        $basisDocumentName = $model->basis_name . ($model->invoice->agreementFullName ? (' "'.$model->invoice->agreementFullName.'"') : '');
        $basisDocumentNumber = $model->basis_document_number;
        $basisDocumentDate = ($model->basis_document_date) ? DateHelper::format($model->basis_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) : '';
        $waybillNumber = $model->waybill_number;
        $waybillDate = $model->waybill_date ? DateHelper::format($model->waybill_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) : '';

        $headTable2 = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
            'cellMargin' => 0,
        ]);

        $HW_L = 1016;
        $HW_C = 12000;
        $HW_R = 1000;

        // Грузополучатель
        $headTable2->addRow();
        $headTable2->addCell($HW_L, $cellBottom)->addText('Грузополучатель', ['size' => 8], $rightParStyle + $rightIndent);
        $headTable2->addCell($HW_C)->addText($CONSIGNEE, ['size' => 8], $leftParStyle + $borderBottom);
        $headTable2->addCell(120)->addText('', ['size' => 8], $rightParStyle);
        $headTable2->addCell($HW_R, $cellBottom)->addText('по ОКПО', ['size' => 8], $rightParStyle + $rightIndent);
        $headTable2->addCell($HW_R, $borderStyle)->addText($model->invoice->contractor->okpo, ['size' => 8, 'bold' => true], $centerParStyle);
        $headTable2->addRow();
        $headTable2->addCell($HW_L)->addText('', ['size' => 7], $rightParStyle);
        $headTable2->addCell($HW_C)->addText('организация, адрес, телефон, факс, банковские реквизиты', ['size' => 7], $centerParStyle);
        $headTable2->addCell(120)->addText('', ['size' => 7], $rightParStyle);
        $headTable2->addCell($HW_R)->addText('', ['size' => 7], $rightParStyle + $rightIndent);
        $headTable2->addCell($HW_R, $borderStyle)->addText('', ['size' => 7], $centerParStyle);
        // Поставщик
        $headTable2->addRow();
        $headTable2->addCell($HW_L, $cellBottom)->addText('Поставщик', ['size' => 8], $rightParStyle + $rightIndent);
        $headTable2->addCell($HW_C)->addText($CONTRACTOR, ['size' => 8], $leftParStyle + $borderBottom);
        $headTable2->addCell(120)->addText('', ['size' => 8], $rightParStyle);
        $headTable2->addCell($HW_R, $cellBottom)->addText('по ОКПО', ['size' => 8], $rightParStyle + $rightIndent);
        $headTable2->addCell($HW_R, $borderStyleNoTop)->addText($model->invoice->company_okpo, ['size' => 8, 'bold' => true], $centerParStyle);
        $headTable2->addRow();
        $headTable2->addCell($HW_L)->addText('', ['size' => 7], $rightParStyle);
        $headTable2->addCell($HW_C)->addText('организация, адрес, телефон, факс, банковские реквизиты', ['size' => 7], $centerParStyle);
        $headTable2->addCell(120)->addText('', ['size' => 7], $rightParStyle);
        $headTable2->addCell($HW_R)->addText('', ['size' => 7], $rightParStyle + $rightIndent);
        $headTable2->addCell($HW_R, $borderStyle)->addText('', ['size' => 7], $centerParStyle);
        // Плательщик
        $headTable2->addRow();
        $headTable2->addCell($HW_L, $cellBottom)->addText('Плательщик', ['size' => 8], $rightParStyle + $rightIndent);
        $headTable2->addCell($HW_C)->addText($PAYER, ['size' => 8], $leftParStyle + $borderBottom);
        $headTable2->addCell(120)->addText('', ['size' => 8], $rightParStyle);
        $headTable2->addCell($HW_R, $cellBottom)->addText('по ОКПО', ['size' => 8], $rightParStyle + $rightIndent);
        $headTable2->addCell($HW_R, $borderStyleNoTop)->addText($model->invoice->contractor->okpo, ['size' => 8, 'bold' => true], $centerParStyle);
        $headTable2->addRow();
        $headTable2->addCell($HW_L)->addText('', ['size' => 7], $rightParStyle);
        $headTable2->addCell($HW_C)->addText('организация, адрес, телефон, факс, банковские реквизиты', ['size' => 7], $centerParStyle);
        $headTable2->addCell(120)->addText('', ['size' => 7], $rightParStyle);
        $headTable2->addCell($HW_R, $borderStyle)->addText('', ['size' => 7], $rightParStyle + $rightIndent);
        $headTable2->addCell($HW_R, $borderStyle)->addText('', ['size' => 7], $centerParStyle);
        // Основание
        $headTable2->addRow();
        $headTable2->addCell($HW_L, $cellBottom)->addText('Основание', ['size' => 8], $rightParStyle + $rightIndent);
        $headTable2->addCell($HW_C)->addText($basisDocumentName, ['size' => 8], $leftParStyle + $borderBottom);
        $headTable2->addCell(120)->addText('', ['size' => 8], $rightParStyle);
        $headTable2->addCell($HW_R, $borderStyleNoTop + $cellBottom)->addText('номер', ['size' => 8], $rightParStyle + $rightIndent);
        $headTable2->addCell($HW_R, $borderStyleNoTop)->addText($basisDocumentNumber, ['size' => 8, 'bold' => true], $centerParStyle);
        $headTable2->addRow();
        $headTable2->addCell($HW_L)->addText('', ['size' => 7], $rightParStyle);
        $headTable2->addCell($HW_C)->addText('договор, заказ-наряд', ['size' => 7], $centerParStyle);
        $headTable2->addCell(120)->addText('', ['size' => 7], $rightParStyle);
        $headTable2->addCell($HW_R, $cellBottom + $borderStyle)->addText('дата', ['size' => 8], $rightParStyle + $rightIndent);
        $headTable2->addCell($HW_R, $borderStyle + $cellBottom)->addText($basisDocumentDate, ['size' => 8, 'bold' => true], $centerParStyle);
        // Транспортная накладная
        $headTable2->addRow();
        $headTable2->addCell($HW_L + $HW_C, $gridSpan2)->addText('Транспортная накладная', ['size' => 8], $rightParStyle + $rightIndent);
        $headTable2->addCell(120)->addText('', ['size' => 8], $rightParStyle);
        $headTable2->addCell($HW_R, $cellBottom + $borderStyle + $borderBottom)->addText('номер', ['size' => 8], $rightParStyle + $rightIndent);
        $headTable2->addCell($HW_R, $borderStyle + $borderBottom + $cellBottom)->addText($waybillNumber, ['size' => 8, 'bold' => true], $centerParStyle);

        // ROWS 3

        $headTable3 = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
            'cellMargin' => 0,
        ]);

        $HW_LL = 4136;
        $HW_L = 2500;
        $HW_R = 4000;
        $HW_RR = 1000;
        $headTable3->addRow();
        $headTable3->addCell($HW_LL)->addText('', ['size' => 8], $rightParStyle);
        $headTable3->addCell($HW_L, $cellCenter + $borderStyle + $borderBottom)->addText('Номер документа', ['size' => 8], $centerParStyle);
        $headTable3->addCell($HW_L, $cellCenter + $borderStyle + $borderBottom)->addText('Дата составления', ['size' => 8], $centerParStyle);
        $headTable3->addCell($HW_R)->addText('', ['size' => 8], $rightParStyle);
        $headTable3->addCell($HW_RR, $cellBottom + $borderStyle + $borderBottom)->addText('дата', ['size' => 8], $rightParStyle + $rightIndent);
        $headTable3->addCell($HW_RR, $borderStyle + $cellBottom)->addText($waybillDate, ['size' => 8, 'bold' => true], $centerParStyle);
        $headTable3->addRow();
        $headTable3->addCell($HW_LL, $cellBottom)->addText('ТОВАРНАЯ НАКЛАДНАЯ', ['size' => 8, 'bold' => true], $rightParStyle + ['indentation' => ['left' => 0, 'right' => 600]]);
        $headTable3->addCell($HW_L, $cellBottom + $borderStyleB + $borderBottomB)->addText($model->getFullNumber(), ['size' => 8], $centerParStyle);
        $headTable3->addCell($HW_L, $cellBottom + $borderStyleB + $borderBottomB)->addText($dateFormated, ['size' => 8], $centerParStyle);
        $headTable3->addCell($HW_R + $HW_RR, $cellBottom + $gridSpan2)->addText('Вид операции', ['size' => 8], $rightParStyle + $rightIndent);
        $headTable3->addCell($HW_RR, $borderStyle + $borderBottom)->addText('', ['size' => 7], $centerParStyle);
        $headTable3->addRow();
        $headTable3->addCell($HW_R + $HW_RR, $cellBottom + ['gridSpan' => 6])->addText('Страница №1', ['size' => 8, 'italic' => true], $rightParStyle);

        // TABLE

        $table = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => '000000',
            'borderSize' => 6,
            'cellMargin' => 20,
        ]);

        $colWidth = [
            1 => 300,
            2 => 4186,
            3 => 800,
            4 => 800,
            5 => 700,
            6 => 800,
            7 => 700,
            8 => 600,
            9 => 650,
            10 => 1000,
            11 => 800,
            12 => 1100,
            13 => 700,
            14 => 800,
            15 => 1200
        ];

        $table->addRow();
        $table->addCell($colWidth[1],  $cellCenter + $cellRowSpan)->addText('Номер по порядку', ['size' => 8], $centerParStyle);
        $table->addCell($colWidth[2] + $colWidth[3],  $cellCenter + $gridSpan2)->addText('Товар', ['size' => 8], $centerParStyle);
        $table->addCell($colWidth[3] + $colWidth[4],  $cellCenter + $gridSpan2)->addText('Единица измерения', ['size' => 8], $centerParStyle);
        $table->addCell($colWidth[6],  $cellCenter + $cellRowSpan)->addText('Вид упаковки', ['size' => 8], $centerParStyle);
        $table->addCell($colWidth[7] + $colWidth[8],  $cellCenter + $gridSpan2)->addText('Количество', ['size' => 8], $centerParStyle);
        $table->addCell($colWidth[9],  $cellCenter + $cellRowSpan)->addText('Масса брутто', ['size' => 8], $centerParStyle);
        $table->addCell($colWidth[10], $cellCenter + $cellRowSpan)->addText('Количество (масса нетто)', ['size' => 8], $centerParStyle);
        $table->addCell($colWidth[11], $cellCenter + $cellRowSpan)->addText('Цена, руб. коп.', ['size' => 8], $centerParStyle);
        $table->addCell($colWidth[12], $cellCenter + $cellRowSpan)->addText('Сумма без учета НДС, руб. коп.', ['size' => 8], $centerParStyle);
        $table->addCell($colWidth[13] + $colWidth[14], $cellCenter + $gridSpan2)->addText('НДС', ['size' => 8], $centerParStyle);
        $table->addCell($colWidth[15], $cellCenter + $cellRowSpan)->addText('Сумма с учетом НДС, руб. коп.', ['size' => 8], $centerParStyle);

        $table->addRow();
        $table->addCell($colWidth[1],  $cellRowContinue);
        $table->addCell($colWidth[2],  $cellCenter)->addText('наименование, характеристика, сорт, артикул', ['size' => 8], $centerParStyle);
        $table->addCell($colWidth[3],  $cellCenter)->addText('код', ['size' => 8], $centerParStyle);
        $table->addCell($colWidth[4],  $cellCenter)->addText('наимено- вание', ['size' => 8], $centerParStyle);
        $table->addCell($colWidth[5],  $cellCenter)->addText('код по ОКЕИ', ['size' => 8], $centerParStyle);
        $table->addCell($colWidth[6],  $cellRowContinue);
        $table->addCell($colWidth[7],  $cellCenter)->addText('в одном месте', ['size' => 8], $centerParStyle);
        $table->addCell($colWidth[8],  $cellCenter)->addText('мест штук', ['size' => 8], $centerParStyle);
        $table->addCell($colWidth[9],  $cellRowContinue);
        $table->addCell($colWidth[10], $cellRowContinue);
        $table->addCell($colWidth[11], $cellRowContinue);
        $table->addCell($colWidth[12], $cellRowContinue);
        $table->addCell($colWidth[13], $cellCenter)->addText('ставка, %', ['size' => 8], $centerParStyle);
        $table->addCell($colWidth[14], $cellCenter)->addText('сумма, руб. коп.', ['size' => 8], $centerParStyle);
        $table->addCell($colWidth[15], $cellRowContinue);

        $table->addRow();
        for ($i=1; $i<=15; $i++) {
            $table->addCell($colWidth[$i], $cellBottom)->addText($i, ['size' => 7], $centerParStyle);
        }

        $orderQuery = OrderPackingList::find()->where(['packing_list_id'=>$model->id]);
        $orderArray = $orderQuery->all();
        foreach ($orderArray as $key => $order) {
            /** @var OrderPackingList $order */
            if ($model->invoice->hasNds) {
                $priceNoNds = $order->priceNoNds;
                $amountNoNds = $model->getPrintOrderAmount($order->order_id, true);
                $amountWithNds = $model->getPrintOrderAmount($order->order_id, false);
                $TaxRateName = $order->order->saleTaxRate->name;
                $ndsAmount = TextHelper::invoiceMoneyFormat(($amountWithNds - $amountNoNds), $precision);
            } else {
                $priceNoNds = $order->priceWithNds;
                $amountNoNds = $model->getPrintOrderAmount($order->order_id, false);
                $amountWithNds = $model->getPrintOrderAmount($order->order_id, false);
                $TaxRateName = 'Без НДС';
                $ndsAmount = 'Без НДС';
            }
            if ($order->quantity != intval($order->quantity)) {
                $order->quantity = rtrim(number_format($order->quantity, 10, '.', ''), 0);
            }

            $colValue[1]  = $key + 1;
            $colValue[2]  = htmlspecialchars($order->order->product_title);
            $colValue[3]  = $order->order->article;
            $colValue[4]  = $order->order->unit ? $order->order->unit->name : Product::DEFAULT_VALUE;;
            $colValue[5]  = $order->order->unit ? $order->order->unit->code_okei : Product::DEFAULT_VALUE;;
            $colValue[6]  = $order->order->box_type;
            $colValue[7]  = $order->order->count_in_place;
            $colValue[8]  = $order->order->place_count;
            $colValue[9]  = $order->order->mass_gross;
            $colValue[10] = $order->quantity;
            $colValue[11] = TextHelper::invoiceMoneyFormat($priceNoNds, $precision);
            $colValue[12] = TextHelper::invoiceMoneyFormat($amountNoNds, $precision);
            $colValue[13] = $TaxRateName;
            $colValue[14] = $ndsAmount;
            $colValue[15] = TextHelper::invoiceMoneyFormat($amountWithNds, $precision);

            $table->addRow();
            for ($i=1; $i<=15; $i++) {
                $table->addCell($colWidth[$i], $cellCenter)->addText($colValue[$i], ['size' => 8], ($i == 2) ? ($leftParStyle) : $centerParStyle);
            }
        }

        $totalQuantity = $orderQuery->sum('quantity') * 1;
        if ($totalQuantity != intval($totalQuantity)) {
            $totalQuantity = rtrim(number_format($totalQuantity, 10, '.', ''), 0);
        }

        $colValue = [];
        $colValue[8] = $model->invoice->total_place_count;
        $colValue[9] = $model->invoice->total_mass_gross;
        $colValue[10] = $totalQuantity;
        $colValue[11] = 'X';
        $colValue[12] = TextHelper::invoiceMoneyFormat($model->invoice->hasNds ? $model->getPrintAmountNoNds() : $model->getPrintAmountWithNds(), 2);
        $colValue[13] = 'X';
        $colValue[14] = ($model->invoice->hasNds) ? TextHelper::invoiceMoneyFormat($model->totalPLNds, 2) : '';
        $colValue[15] = TextHelper::invoiceMoneyFormat($model->getPrintAmountWithNds(), 2);

        $table->addRow();
        $table->addCell(array_sum(array_slice($colWidth, 0, 7)), $borderNoBottom + $borderNoLeft + $cellCenter + ['gridSpan' => 7])->addText('Итого', ['size' => 8], $rightParStyle + $rightIndent);
        for ($i=8; $i<=15; $i++) {
            $table->addCell($colWidth[$i], $cellCenter)->addText($colValue[$i], ['size' => 8], $centerParStyle);
        }

        $colValue = [];
        $colValue[8] = $model->invoice->total_place_count;
        $colValue[9] = $model->invoice->total_mass_gross;
        $colValue[10] = $totalQuantity;
        $colValue[11] = 'X';
        $colValue[12] = TextHelper::invoiceMoneyFormat($model->invoice->hasNds ? $model->getPrintAmountNoNds() : $model->getPrintAmountWithNds(), 2);
        $colValue[13] = 'X';
        $colValue[14] = ($model->invoice->hasNds) ? TextHelper::invoiceMoneyFormat($model->totalPLNds, 2) : '';
        $colValue[15] = TextHelper::invoiceMoneyFormat($model->getPrintAmountWithNds(), 2);

        $table->addRow();
        $table->addCell(array_sum(array_slice($colWidth, 0, 7)), $borderNoTop + $borderNoBottom + $borderNoLeft + $cellCenter + ['gridSpan' => 7])->addText('Всего по накладной', ['size' => 8], $rightParStyle + $rightIndent);
        for ($i=8; $i<=15; $i++) {
            $table->addCell($colWidth[$i], $cellCenter)->addText($colValue[$i], ['size' => 8], $centerParStyle);
        }

        // PRE-FOOTER

        $HW_LL = 500;
        $HW_L = 3636;
        $HW_C = 6000;
        $HW_R = 5000;

        $preFooterNumbersCount = RUtils::numeral()->getInWords(count($orderArray));
        $preFooterTotalPlaces = ($model->invoice->total_place_count > 0) ?
            iconv("UTF-8", "UTF-8//IGNORE", TextHelper::mb_ucfirst(RUtils::numeral()->getInWords($model->invoice->total_place_count, RUtils::NEUTER))) :
            '';
        $preFooterMassNetto = ($realMassNet > 0) ? (TextHelper::moneyFormat($realMassNet, 2) . ' КГ') : '';
        $preFooterMassGross = ($realMassGross > 0) ? (TextHelper::moneyFormat($realMassGross, 2) . ' КГ') : '';
        $preFooterMassNettoWords = ($realMassNet > 0) ? (TextHelper::mb_ucfirst(RUtils::numeral()->getInWords($realMassNet))  . ' КГ') : '';
        $preFooterMassGrossWords = ($realMassGross > 0) ? (TextHelper::mb_ucfirst(RUtils::numeral()->getInWords($realMassGross)) . ' КГ') : '';

        $preFooter = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
            'cellMargin' => 0,
        ]);

        $preFooter->addRow();
        $preFooter->addCell($HW_LL + $HW_L, $cellBottom + $gridSpan2)->addText('Товарная накладная имеет приложение на', ['size' => 8], $rightParStyle + $rightIndent);
        $preFooter->addCell($HW_C, $cellBottom + $borderBottom)->addText('', ['size' => 8], $leftParStyle);
        $preFooter->addCell($HW_R, $cellBottom)->addText('листах', ['size' => 8], $leftParStyle + $leftIndent);

        $preFooter->addRow();
        $preFooter->addCell($HW_LL, $cellBottom)->addText('', ['size' => 8], $rightParStyle);
        $preFooter->addCell($HW_L, $cellBottom)->addText('и содержит', ['size' => 8], $rightParStyle + $rightIndent);
        $preFooter->addCell($HW_C, $cellBottom + $borderBottom)->addText($preFooterNumbersCount, ['size' => 8], $leftParStyle + $leftIndent);
        $preFooter->addCell($HW_R, $cellBottom)->addText('порядковых номеров записей', ['size' => 8], $leftParStyle + $leftIndent);
        $preFooter->addRow();
        $preFooter->addCell($HW_LL + $HW_L, $cellCenter + $gridSpan2)->addText('', ['size' => 6], $rightParStyle + $rightIndent);
        $preFooter->addCell($HW_C, $cellCenter)->addText('(прописью)', ['size' => 6], $centerParStyle);
        $preFooter->addCell($HW_R, $cellCenter)->addText('', ['size' => 6], $leftParStyle);

        // PRE-FOOTER-2

        $preFooter2 = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
            'cellMargin' => 0,
        ]);

        $HW_LL = 1420;
        $HW_L = 3580;
        $HW_C = 2000;
        $HW_R = 5000;
        $HW_RR = 3136;

        // Нетто
        $preFooter2->addRow(250);
        $preFooter2->addCell($HW_LL, $cellBottom)->addText('', ['size' => 8], $rightParStyle);
        $preFooter2->addCell($HW_L, $cellBottom)->addText('', ['size' => 8], $rightParStyle);
        $preFooter2->addCell($HW_C, $cellBottom)->addText('Масса груза (нетто)', ['size' => 8], $rightParStyle + $rightIndent);
        $preFooter2->addCell($HW_R, $cellBottom + $borderBottom)->addText($preFooterMassNettoWords, ['size' => 8], $centerParStyle + $rightIndent);
        $preFooter2->addCell($HW_RR, $cellBottom + $borderNetto)->addText($preFooterMassNetto, ['size' => 8], $centerParStyle + $leftIndent);
        $preFooter2->addRow();
        $preFooter2->addCell($HW_LL + $HW_L + $HW_C, $cellBottom + ['gridSpan' => 3])->addText('', ['size' => 6], $rightParStyle);
        $preFooter2->addCell($HW_R, $cellCenter)->addText('(прописью)', ['size' => 6], $centerParStyle);
        $preFooter2->addCell($HW_RR, $cellBottom + $borderB_RL + $borderNoBottom)->addText('', ['size' => 6], $centerParStyle + $leftIndent);
        // Брутто
        $preFooter2->addRow();
        $preFooter2->addCell($HW_LL, $cellBottom)->addText('Всего мест', ['size' => 8], $rightParStyle + $rightIndent);
        $preFooter2->addCell($HW_L, $cellBottom + $borderBottom)->addText($preFooterTotalPlaces, ['size' => 8], $leftParStyle + $leftIndent);
        $preFooter2->addCell($HW_C, $cellBottom)->addText('Масса груза (брутто)', ['size' => 8], $rightParStyle + $rightIndent);
        $preFooter2->addCell($HW_R, $cellBottom + $borderBottom)->addText($preFooterMassGrossWords, ['size' => 8], $centerParStyle + $rightIndent);
        $preFooter2->addCell($HW_RR, $cellBottom + $borderBrutto + $borderNoTop)->addText($preFooterMassGross, ['size' => 8], $centerParStyle + $leftIndent);
        $preFooter2->addRow();
        $preFooter2->addCell($HW_LL, $cellBottom)->addText('', ['size' => 6], $rightParStyle);
        $preFooter2->addCell($HW_L, $cellBottom)->addText('(прописью)', ['size' => 6], $centerParStyle);
        $preFooter2->addCell($HW_C, $cellBottom)->addText('', ['size' => 6], $rightParStyle);
        $preFooter2->addCell($HW_R, $cellCenter)->addText('(прописью)', ['size' => 6], $centerParStyle);
        $preFooter2->addCell($HW_RR, $cellBottom)->addText('', ['size' => 6], $centerParStyle + $leftIndent);

        // $FULL_WIDTH LEFT = 7368;
        // $FULL_WIDTH RIGHT = 7768;

        $footerLicenseNumber = ($model->proxy_number && $model->proxy_date) ?
            ($model->proxy_number . ' от ' . \Yii::$app->formatter->asDate($model->proxy_date, 'long')) : '---';
        if ($model->consignee) {
            $footerLicense = $model->consignee->getTitle(true);
        } else {
            $footerLicense = $model->invoice->contractor_name_short;
            //$footerLicense .= ($model->given_out_position ? ", {$model->given_out_position}" : null);
            //$footerLicense .= ($model->given_out_fio ? ", {$model->given_out_fio}" : null);
        }

        $footerTotalSumInWords = \common\components\TextHelper::amountToWords($model->getPrintAmountWithNds() / 100);

        $footer = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
            'cellMargin' => 0,
        ]);

        $HR = [
            1 => 3448,
            2 => 3000,
            3=> 800,
            4 => 120,
            5 => 120,
            6 => 2048,
            7 => 5600
        ];

        $footer->addRow();
        $footer->addCell($HR[1], $cellBottom)->addText('Приложение (паспорт, сертификаты и т.п.) на', ['size' => 8], $leftParStyle + $rightIndent);
        $footer->addCell($HR[2], $cellBottom + $borderBottom)->addText('', ['size' => 8], $centerParStyle);
        $footer->addCell($HR[3], $cellBottom)->addText('листах', ['size' => 8], $leftParStyle + $leftIndent);
        $footer->addCell($HR[4], $cellBottom)->addText('', ['size' => 8], $centerParStyle);
        $footer->addCell($HR[5], $cellBottom + $borderLeft)->addText('', ['size' => 8], $centerParStyle);
        $footer->addCell($HR[6], $cellBottom)->addText('По доверенности №', ['size' => 8], $leftParStyle + $rightIndent);
        $footer->addCell($HR[7], $cellBottom + $borderBottom)->addText($footerLicenseNumber, ['size' => 8], $leftParStyle + $leftIndent);
        $footer->addRow();
        $footer->addCell($HR[1], $cellBottom)->addText('', ['size' => 6], $centerParStyle);
        $footer->addCell($HR[2], $cellBottom)->addText('(прописью)', ['size' => 6], $centerParStyle);
        $footer->addCell($HR[3], $cellBottom)->addText('', ['size' => 6], $centerParStyle);
        $footer->addCell($HR[4], $cellBottom)->addText('', ['size' => 6], $centerParStyle);
        $footer->addCell($HR[5], $cellBottom + $borderLeft)->addText('', ['size' => 6], $centerParStyle);
        $footer->addCell($HR[6], $cellBottom)->addText('', ['size' => 6], $centerParStyle);
        $footer->addCell($HR[7], $cellBottom)->addText('(номер, дата)', ['size' => 6], $centerParStyle);
        $footer->addRow();
        $footer->addCell($HR[1] + $HR[2] + $HR[3] + $HR[4], $gridSpan4)->addText('Всего отпущено на сумму', ['size' => 8, 'bold' => true], $leftParStyle);
        $footer->addCell($HR[5], $cellBottom + $borderLeft)->addText('', ['size' => 8], $centerParStyle);
        $footer->addCell($HR[6], $cellBottom)->addText('выданной', ['size' => 8], $leftParStyle + $rightIndent);
        $footer->addCell($HR[7], $cellBottom + $borderBottom)->addText($footerLicense, ['size' => 8], $leftParStyle + $leftIndent);
        $footer->addRow();
        $footer->addCell($HR[1] + $HR[2] + $HR[3], $gridSpan3 + $borderBottom)->addText($footerTotalSumInWords, ['size' => 8, 'bold' => true], $leftParStyle);
        $footer->addCell($HR[4], $cellCenter)->addText('', ['size' => 8], $centerParStyle);
        $footer->addCell($HR[5], $cellCenter + $borderLeft)->addText('', ['size' => 8], $centerParStyle);
        $footer->addCell($HR[6], $cellCenter)->addText('', ['size' => 8], $centerParStyle);
        $footer->addCell($HR[7], $cellCenter)->addText('кем, кому (организация, должность, фамилия, и.о.)', ['size' => 6], $centerParStyle);
        $footer->addRow();
        $footer->addCell($HR[1] + $HR[2] + $HR[3], $gridSpan3 + $cellCenter)->addText('(прописью)', ['size' => 6], $centerParStyle);
        $footer->addCell($HR[4], $cellCenter)->addText('', ['size' => 6], $centerParStyle);
        $footer->addCell($HR[5], $cellCenter + $borderLeft)->addText('', ['size' => 6], $centerParStyle);
        $footer->addCell($HR[6], $cellCenter)->addText('', ['size' => 6], $centerParStyle);
        $footer->addCell($HR[7], $cellCenter + $borderBottom)->addText('', ['size' => 6], $centerParStyle);

        $HR_L = [
            1 => 2048,
            2 => 1800,
            3 => 120,
            4 => 1200,
            5 => 120,
            6 => 1960,
            7 => 120
        ];
        $HR_R = [
            1 => 120,
            2 => 2048,
            3 => 1800,
            4 => 120,
            5 => 1200,
            6 => 120,
            7 => 2360,
        ];

        $footer2 = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
            'cellMargin' => 0,
        ]);

        // Отпуск груза разрешил (должность)
        $cargoAcceptedPosition = '';
        if ($model->type == Documents::IO_TYPE_OUT):
            if (!$company->getIsLikeIp()):
                $cargoAcceptedPosition = ($model->invoice->signEmployeeCompany) ?
                    $model->invoice->signEmployeeCompany->position : $model->invoice->company_chief_post_name;
            endif;
        else:
            if ($model->invoice->contractor->chief_accountant_is_director):
                $cargoAcceptedPosition = $model->invoice->contractor->director_name;
            else:
                $cargoAcceptedPosition = 'Главный бухгалтер';
            endif;
        endif;
        // Отпуск груза разрешил (ФИО)
        $cargoAcceptedFio = '';
        if ($model->type == Documents::IO_TYPE_OUT):
            $cargoAcceptedFio = $model->signed_by_name ? $model->signed_by_name : $model->invoice->getCompanyChiefFio(true);
            if ($model->signed_by_employee_id):
                $cargoAcceptedFio .= ' по ' .  mb_strtolower($model->signBasisDocument->name2) . ' №' . $model->sign_document_number;
                $cargoAcceptedFio .= ' от ' . DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . 'г.';
            endif;
        else:
            if ($model->invoice->contractor->chief_accountant_is_director):
                $cargoAcceptedFio = $model->invoice->contractor->director_name;
            else:
                $cargoAcceptedFio = $model->invoice->contractor->chief_accountant_name;
            endif;
        endif;
        // Глав. бух.
        $mainAccountant = '';
        if ($model->type == Documents::IO_TYPE_OUT):
            if (!$company->getIsLikeIP()):
                $mainAccountant = $model->signed_by_name ? $model->signed_by_name : $model->invoice->getCompanyChiefAccountantFio(true);
                if ($model->signed_by_employee_id) :
                    $mainAccountant .= ' по ' . mb_strtolower($model->signBasisDocument->name2) . ' №' . $model->sign_document_number;
                    $mainAccountant .= ' от ' . DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . 'г.';
                endif;
            endif;
        else:
            if ($model->invoice->contractor->chief_accountant_is_director):
                $mainAccountant = $model->invoice->contractor->director_name;
            else:
                $mainAccountant = $model->invoice->contractor->chief_accountant_name;
            endif;
        endif;

        $dateDay = DateHelper::format($model->document_date, 'd', DateHelper::FORMAT_DATE);
        $dateMonth = \php_rutils\RUtils::dt()->ruStrFTime([
            'date' => $model->document_date,
            'format' => 'F', // month in word
            'monthInflected' => true,
        ]);
        $dateYear = DateHelper::format($model->document_date, 'Y', DateHelper::FORMAT_DATE);

        $footer2->addRow(300);
        $footer2->addCell($HR_L[1], $cellBottom)->addText('Отпуск груза разрешил', ['size' => 8], $leftParStyle);
        $footer2->addCell($HR_L[2], $cellBottom + $borderBottom)->addText($cargoAcceptedPosition, ['size' => 8], $centerParStyle);
        $footer2->addCell($HR_L[3], $cellBottom)->addText('', ['size' => 8], $centerParStyle);
        $footer2->addCell($HR_L[4], $cellBottom + $borderBottom)->addText('', ['size' => 8], $centerParStyle);
        $footer2->addCell($HR_L[5], $cellBottom)->addText('', ['size' => 8], $centerParStyle);
        $footer2->addCell($HR_L[6], $cellBottom + $borderBottom)->addText($cargoAcceptedFio, ['size' => 8], $centerParStyle);
        $footer2->addCell($HR_L[7], $cellBottom)->addText('', ['size' => 8], $centerParStyle);
        $footer2->addCell($HR_R[1], $cellBottom + $borderLeft)->addText('', ['size' => 8], $centerParStyle);
        $footer2->addCell($HR_R[2], $cellBottom)->addText('', ['size' => 8], $leftParStyle);
        $footer2->addCell($HR_R[3] + $HR_R[4] + $HR_R[5] + $HR_R[6] + $HR_R[7], $cellBottom + $borderBottom + $gridSpan5)->addText('', ['size' => 8], $leftParStyle);
        $footer2->addRow();
        $footer2->addCell($HR_L[1], $cellBottom)->addText('', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_L[2], $cellBottom)->addText('(должность)', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_L[3], $cellBottom)->addText('', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_L[4], $cellBottom)->addText('(подпись)', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_L[5], $cellBottom)->addText('', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_L[6], $cellBottom)->addText('(расшифровка подписи)', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_L[7], $cellBottom)->addText('', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_R[1], $cellBottom + $borderLeft)->addText('', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_R[2], $cellBottom)->addText('', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_R[3] + $HR_R[4] + $HR_R[5] + $HR_R[6] + $HR_R[7], $cellBottom + $gridSpan5)->addText('', ['size' => 6], $centerParStyle);
        // Главный (старший) бухгалтер
        $footer2->addRow();
        $footer2->addCell($HR_L[1], $cellBottom)->addText('Главный (старший) бухгалтер', ['size' => 8], $leftParStyle);
        $footer2->addCell($HR_L[2], $cellBottom)->addText('', ['size' => 8], $leftParStyle);
        $footer2->addCell($HR_L[3], $cellBottom)->addText('', ['size' => 8], $centerParStyle);
        $footer2->addCell($HR_L[4], $cellBottom + $borderBottom)->addText('', ['size' => 8], $leftParStyle);
        $footer2->addCell($HR_L[5], $cellBottom)->addText('', ['size' => 8], $centerParStyle);
        $footer2->addCell($HR_L[6], $cellBottom + $borderBottom)->addText($mainAccountant, ['size' => 8], $centerParStyle);
        $footer2->addCell($HR_L[7], $cellBottom)->addText('', ['size' => 8], $centerParStyle);
        $footer2->addCell($HR_R[1], $cellBottom + $borderLeft)->addText('', ['size' => 8], $centerParStyle);
        $footer2->addCell($HR_R[2], $cellBottom)->addText('Груз принял', ['size' => 8], $leftParStyle);
        $footer2->addCell($HR_R[3], $cellBottom + $borderBottom)->addText($model->cargo_accepted_position, ['size' => 8], $leftParStyle);
        $footer2->addCell($HR_R[4], $cellBottom)->addText('', ['size' => 8], $centerParStyle);
        $footer2->addCell($HR_R[5], $cellBottom + $borderBottom)->addText('', ['size' => 8], $leftParStyle);
        $footer2->addCell($HR_R[6], $cellBottom)->addText('', ['size' => 8], $centerParStyle);
        $footer2->addCell($HR_R[7], $cellBottom + $borderBottom)->addText($model->cargo_accepted_fullname, ['size' => 8], $leftParStyle);
        $footer2->addRow();
        $footer2->addCell($HR_L[1], $cellBottom)->addText('', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_L[2], $cellBottom)->addText('', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_L[3], $cellBottom)->addText('', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_L[4], $cellBottom)->addText('(подпись)', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_L[5], $cellBottom)->addText('', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_L[6], $cellBottom)->addText('(расшифровка подписи)', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_L[7], $cellBottom)->addText('', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_R[1], $cellBottom + $borderLeft)->addText('', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_R[2], $cellBottom)->addText('', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_R[3], $cellBottom)->addText('(должность)', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_R[4], $cellBottom)->addText('', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_R[5], $cellBottom)->addText('(подпись)', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_R[6], $cellBottom)->addText('', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_R[7], $cellBottom)->addText('(расшифровка подписи)', ['size' => 6], $centerParStyle);
        // Отпуск груза произвел
        $footer2->addRow();
        $footer2->addCell($HR_L[1], $cellBottom)->addText('Отпуск груза произвел', ['size' => 8], $leftParStyle);
        $footer2->addCell($HR_L[2], $cellBottom + $borderBottom)->addText($cargoAcceptedPosition, ['size' => 8], $centerParStyle);
        $footer2->addCell($HR_L[3], $cellBottom)->addText('', ['size' => 8], $centerParStyle);
        $footer2->addCell($HR_L[4], $cellBottom + $borderBottom)->addText('', ['size' => 8], $leftParStyle);
        $footer2->addCell($HR_L[5], $cellBottom)->addText('', ['size' => 8], $centerParStyle);
        $footer2->addCell($HR_L[6], $cellBottom + $borderBottom)->addText($cargoAcceptedFio, ['size' => 8], $centerParStyle);
        $footer2->addCell($HR_L[7], $cellBottom)->addText('', ['size' => 8], $centerParStyle);
        $footer2->addCell($HR_R[1], $cellBottom + $borderLeft)->addText('', ['size' => 8], $centerParStyle);
        $footer2->addCell($HR_R[2], $cellBottom)->addText('Груз получил', ['size' => 8], $leftParStyle);
        $footer2->addCell($HR_R[3], $cellBottom + $borderBottom)->addText($model->cargo_received_position, ['size' => 8], $leftParStyle);
        $footer2->addCell($HR_R[4], $cellBottom)->addText('', ['size' => 8], $centerParStyle);
        $footer2->addCell($HR_R[5], $cellBottom + $borderBottom)->addText('', ['size' => 8], $leftParStyle);
        $footer2->addCell($HR_R[6], $cellBottom)->addText('', ['size' => 8], $centerParStyle);
        $footer2->addCell($HR_R[7], $cellBottom + $borderBottom)->addText($model->cargo_received_fullname, ['size' => 8], $leftParStyle);
        $footer2->addRow();
        $footer2->addCell($HR_L[1], $cellBottom)->addText('', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_L[2], $cellBottom)->addText('(должность)', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_L[3], $cellBottom)->addText('', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_L[4], $cellBottom)->addText('(подпись)', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_L[5], $cellBottom)->addText('', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_L[6], $cellBottom)->addText('(расшифровка подписи)', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_L[7], $cellBottom)->addText('', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_R[1], $cellBottom + $borderLeft)->addText('', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_R[2], $cellBottom)->addText('', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_R[3], $cellBottom)->addText('(должность)', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_R[4], $cellBottom)->addText('', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_R[5], $cellBottom)->addText('(подпись)', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_R[6], $cellBottom)->addText('', ['size' => 6], $centerParStyle);
        $footer2->addCell($HR_R[7], $cellBottom)->addText('(расшифровка подписи)', ['size' => 6], $centerParStyle);
        // М.П.
        $footer2->addRow(250);
        $footer2->addCell($HR_L[1], $cellBottom)->addText('М.П.', ['size' => 8], $rightParStyle);
        $footer2->addCell($HR_L[2], $cellBottom)->addText('" '.$dateDay.' "', ['size' => 8], $rightParStyle);
        $footer2->addCell($HR_L[3], $cellBottom)->addText('', ['size' => 8], $centerParStyle);
        $footer2->addCell($HR_L[4], $cellBottom + $borderBottom)->addText($dateMonth, ['size' => 8], $centerParStyle);
        $footer2->addCell($HR_L[5], $cellBottom)->addText('', ['size' => 8], $centerParStyle);
        $footer2->addCell($HR_L[6], $cellBottom)->addText($dateYear.' года', ['size' => 8], $leftParStyle);
        $footer2->addCell($HR_L[7], $cellBottom)->addText('', ['size' => 8], $centerParStyle);
        $footer2->addCell($HR_R[1], $cellBottom + $borderLeft)->addText('', ['size' => 8], $centerParStyle);
        $footer2->addCell($HR_R[2], $cellBottom)->addText('М.П.', ['size' => 8], $rightParStyle);
        $footer2->addCell($HR_R[3], $cellBottom )->addText('"       "', ['size' => 8], $rightParStyle);
        $footer2->addCell($HR_R[4], $cellBottom)->addText('', ['size' => 8], $centerParStyle);
        $footer2->addCell($HR_R[5], $cellBottom + $borderBottom)->addText('', ['size' => 8], $leftParStyle);
        $footer2->addCell($HR_R[6], $cellBottom)->addText('', ['size' => 8], $centerParStyle);
        $footer2->addCell($HR_R[7], $cellBottom)->addText('20        года', ['size' => 8], $leftParStyle);

        //$section->addTextBreak(1, [], $leftParStyle);

    }

    public function getFileName()
    {
        return $this->model->getPrintTitle() . '.docx';
    }

    public function getFile()
    {
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=\"{$this->fileName}\"");
        header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Expires: 0');

        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($this->phpWord, 'Word2007');

        $objWriter->save("php://output");

        exit;
    }

    public function saveFile($path)
    {
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($this->phpWord, 'Word2007');

        return $objWriter->save($path);
    }

    private function getContractorRequisitesFull($model)
    {
        $ret = $model->invoice->contractor_name_short;
        $ret .= $model->invoice->contractor_inn ? ", ИНН {$model->invoice->contractor_inn}" : '';
        $ret .= $model->invoice->contractor_kpp ? ", КПП {$model->invoice->contractor_kpp}" : '';
        $ret .= $model->invoice->contractor_address_legal_full ? ", {$model->invoice->contractor_address_legal_full}" : '';
        $ret .= $model->invoice->contractor_rs ? ", р/с {$model->invoice->contractor_rs}" : '';
        $ret .= $model->invoice->contractor_bank_name ? ", в банке {$model->invoice->contractor_bank_name}" : '';
        $ret .= $model->invoice->contractor_bik ? ", БИК {$model->invoice->contractor_bik}" : '';
        $ret .= $model->invoice->contractor_ks ? ", к/с {$model->invoice->contractor_ks}" : '';

        return $ret;
    }

    private function getCompanyRequisitesFull($model)
    {
        $ret = $model->invoice->company_name_short . ', ИНН ' . $model->invoice->company_inn;
        $ret .= $model->invoice->company_kpp ? (", КПП {$model->invoice->company_kpp}") : '';
        $ret .= ", {$model->invoice->company_address_legal_full}";
        $ret .= ", р/с {$model->invoice->company_rs}";
        $ret .= ", в банке {$model->invoice->company_bank_name}";
        $ret .= ", БИК {$model->invoice->company_bik}";
        $ret .= $model->invoice->company_ks ? ", к/с {$model->invoice->company_ks}" : '';

        return $ret;
    }
}
