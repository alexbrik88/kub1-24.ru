<?php

namespace common\components\phpWord;

use common\components\date\DateHelper;
use common\components\image\EasyThumbnailImage;
use common\components\TextHelper;
use common\models\Contractor;
use common\models\company\CompanyType;
use common\models\currency\Currency;
use common\models\document\Invoice;
use common\models\document\InvoiceContractorSignature;
use common\models\product\Product;
use frontend\models\Documents;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\Shared\Converter;
use yii\base\Component;
use yii\helpers\Html;

/**
 * Class PdfRenderer
 * @package common\components\pdf
 */
class PhpWordInvoice extends Component
{
    protected $model;
    protected $phpWord;

    /**
     * @param Invoice $model
     * @param array $config
     */
    public function __construct(Invoice $model, $config = [])
    {
        $this->model = $model;
        $this->phpWord = new \PhpOffice\PhpWord\PhpWord();

        parent::__construct($config);
    }

    public function init()
    {
        parent::init();

        $model = $this->model;
        $precision = $model->price_precision;
        $invoice = $model;
        $company = $model->company;
        $contractor = $model->contractor;
        $orderArray = $model->getOrders()->all();
        $orderCount = count($orderArray);
        $hasNds = $model->hasNds;
        $dateFormated = \php_rutils\RUtils::dt()->ruStrFTime([
            'date' => $model->document_date,
            'format' => 'd F Y г.',
            'monthInflected' => true,
        ]);
        $name = $model->is_invoice_contract && $model->type == Documents::IO_TYPE_OUT ? 'Счет-договор' : 'Счет';
        $title = "{$name} № {$model->fullNumber} от {$dateFormated}";
        $invoiceContractorSignature = $model->company->invoiceContractorSignature;
        if (!$invoiceContractorSignature) {
            $invoiceContractorSignature = new InvoiceContractorSignature();
            $invoiceContractorSignature->company_id = $model->company_id;
        }

        $executorText = $invoice->getExecutorFullRequisites();
        $customerText = $model->getCustomerFullRequisites();

        $reasonText = '';
        if ($model->basis_document_name && $model->basis_document_number && $model->basis_document_date) {
            $reasonText .= $model->basis_document_name;
            $reasonText .= ' № ' . $model->basis_document_number;
            $reasonText .= ' от ' . DateHelper::format($model->basis_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
        }
        $logo = !(($logoImage = @$company->getImage('logoImage')) && is_file($logoImage)) ? null :
            EasyThumbnailImage::thumbnailFile($logoImage, 150, 50, EasyThumbnailImage::THUMBNAIL_INSET);

        //Creating PhpWord ===============================================================================================================
        $phpWord = $this->phpWord;
        $phpWord->setDefaultFontName('Arial');
        $phpWord->setDefaultFontSize(10);

        $properties = $phpWord->getDocInfo();

        $properties->setTitle($title);
        $properties->setCreated(time());
        $properties->setModified(time());

        $section = $phpWord->addSection([
            'marginTop' => 600,
            'marginLeft' => 900,
            'marginRight' => 900,
        ]);

        // paragraph styles with align and empty spaces
        $centerParStyle = [
            'spaceBefore' => 0,
            'spaceAfter' => 0,
            'spacing' => 0,
            'align' => 'center',
        ];
        $leftParStyle = [
            'spaceBefore' => 0,
            'spaceAfter' => 0,
            'spacing' => 0,
            'align' => 'left',
        ];
        $rightParStyle = [
            'spaceBefore' => 0,
            'spaceAfter' => 0,
            'spacing' => 0,
            'align' => 'right',
        ];


        $headTable = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
            'cellMargin' => 50,
        ]);

        if ($model->type == Documents::IO_TYPE_OUT) {
            $sellerName = strpos($model->company_name_short, 'ИП ') === 0 ?
                $model->company_name_full :
                $model->company_name_short;
        } else {
            $sellerName = strpos($model->contractor_name_short, 'ИП ') === 0 ?
                $model->contractor_name_full :
                $model->contractor_name_short;
        }
        $headTable->addRow(850);
        $headTable->addCell(7500)
            ->addText($sellerName, ['size' => 12, 'bold' => true], ['align' => 'left', 'spaceBefore' => 100, 'spaceAfter' => 0]);
        $logoCell = $headTable->addCell(2800);
        if ($logo && is_file($logo)) {
            $logoCell->addImage($logo, ['align' => 'right']);
        }

        $section->addTextBreak(1, [], $leftParStyle);

        $topTable = $section->addTable([
            'bidiVisual' => 0,
            'cellMarginTop' => 30,
            'cellMarginBottom' => 30,
            'cellMarginLeft' => 50,
            'cellMarginRight' => 50,
            'borderColor' => '000000',
            'borderTopSize' => 12,
            'borderBottomSize' => 12,
            'borderLeftSize' => 12,
            'borderRightSize' => 12,
            'borderInsideHSize' => 6,
            'borderInsideVSize' => 6,
        ]);
        $topTable->addRow(240);
        $topTable->addCell(5000, ['vMerge' => 'restart', 'gridSpan' => 2, 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF'])
            ->addText($model->type == Documents::IO_TYPE_OUT ?
                $model->company_bank_name :
                $model->contractor->bank_name, [], $leftParStyle);
        $topTable->addCell(1000)
            ->addText('БИК', [], $leftParStyle);
        $topTable->addCell(4300, ['borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF'])
            ->addText($model->type == Documents::IO_TYPE_OUT ?
                $model->company_bik :
                $model->contractor->BIC, [], $leftParStyle);

        $topTable->addRow(240);
        $topTable->addCell(5000, ['vMerge' => 'continue', 'gridSpan' => 2]);
        $topTable->addCell(1000, ['vMerge' => 'restart'])
            ->addText('Сч. № ', [], $leftParStyle);
        $topTable->addCell(4300, ['vMerge' => 'restart', 'borderTopSize' => 0, 'borderTopColor' => 'FFFFFF'])
            ->addText($model->type == Documents::IO_TYPE_OUT ?
                $model->company_ks :
                $model->contractor->corresp_account, [], $leftParStyle);

        $topTable->addRow(240);
        $topTable->addCell(5000, ['gridSpan' => 2, 'borderTopSize' => 0, 'borderTopColor' => 'FFFFFF', 'borderBottomSize' => 12])
            ->addText('Банк получателя', [], $leftParStyle);
        $topTable->addCell(1000, ['vMerge' => 'continue', 'borderBottomSize' => 12]);
        $topTable->addCell(4300, ['vMerge' => 'continue', 'borderBottomSize' => 12]);

        $topTable->addRow(240);
        $topTable->addCell(2500, ['borderTopSize' => 12])
            ->addText('ИНН ' . ($model->type == Documents::IO_TYPE_OUT ?
                    $model->company_inn :
                    $model->contractor->ITN), [], $leftParStyle);
        $topTable->addCell(2500, ['borderTopSize' => 12])
            ->addText('КПП ' . ($model->type == Documents::IO_TYPE_OUT ?
                    $model->company_kpp :
                    $model->contractor->PPC), [], $leftParStyle);
        $topTable->addCell(1000, ['borderTopSize' => 12, 'borderBottomSize' => 12, 'vMerge' => 'restart'])
            ->addText('Сч. № ', [], $leftParStyle);
        $topTable->addCell(4300, ['borderTopSize' => 12, 'borderBottomSize' => 12, 'vMerge' => 'restart'])
            ->addText($model->type == Documents::IO_TYPE_OUT ?
                $model->company_rs :
                $model->contractor->current_account, [], $leftParStyle);

        $topTable->addRow(240);
        $topTable->addCell(5000, ['vMerge' => 'restart', 'gridSpan' => 2, 'borderBottomSize' => 0, 'borderBottomColor' => 'FFFFFF'])
            ->addText($sellerName, [], $leftParStyle);
        $topTable->addCell(1000, ['vMerge' => 'continue']);
        $topTable->addCell(4300, ['vMerge' => 'continue']);

        $topTable->addRow(240);
        $topTable->addCell(5000, ['vMerge' => 'continue', 'gridSpan' => 2]);
        $topTable->addCell(1000, ['vMerge' => 'continue']);
        $topTable->addCell(4300, ['vMerge' => 'continue']);

        $topTable->addRow(240);
        $topTable->addCell(5000, ['gridSpan' => 2, 'borderBottomSize' => 12, 'borderTopSize' => 0, 'borderTopColor' => 'FFFFFF'])
            ->addText('Получатель', [], $leftParStyle);
        $topTable->addCell(1000, ['vMerge' => 'continue']);
        $topTable->addCell(4300, ['vMerge' => 'continue']);

        $section->addTextBreak(1, ['size' => 1], $leftParStyle);

        $titleTable = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
        ]);
        $titleTable->addRow(700);
        $titleTable->addCell(10300, ['valign' => 'center', 'borderBottomSize' => 12, 'borderBottomColor' => '000000'])
            ->addText($title, ['size' => 14, 'bold' => true], $centerParStyle);

        $section->addTextBreak(1, [], $leftParStyle);

        $topTable2 = $section->addTable([
            'bidiVisual' => 0,
            'cellMargin' => 0,
            'cellMarginTop' => 100,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
        ]);
        $topTable2->addRow();
        $topTable2->addCell(1500)->addText('Поставщик (Исполнитель):', [], $leftParStyle);
        $topTable2->addCell(8800)->addText($executorText, ['bold' => true], $leftParStyle);
        $topTable2->addRow();
        $topTable2->addCell(1500)->addText('Покупатель (Заказчик):', [], $leftParStyle);
        $topTable2->addCell(8800)->addText($customerText, ['bold' => true], $leftParStyle);
        $topTable2->addRow();
        $topTable2->addCell(1500)->addText('Основание:', [], $leftParStyle);
        $topTable2->addCell(8800)->addText($reasonText, ['bold' => true], $leftParStyle);

        $section->addTextBreak(1, [], $leftParStyle);

        $productTable = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => '000000',
            'borderTopSize' => 12,
            'borderBottomSize' => 12,
            'borderLeftSize' => 12,
            'borderRightSize' => 12,
            'borderInsideHSize' => 6,
            'borderInsideVSize' => 6,
            'cellMargin' => 50,
        ]);
        $dsc = $model->has_discount && !$model->is_hidden_discount;
        $art = $model->show_article;
        $productTable->addRow(400);
        $productTable->addCell($dsc ? 400 : 500, ['valign' => 'center'])->addText('№', ['bold' => true], $centerParStyle);
        if ($art) {
            $productTable->addCell(1400, ['valign' => 'center'])->addText('Артикул', ['bold' => true], $centerParStyle);
        }
        $productTable->addCell(($dsc ? 4000 : 5200) - ($art ? 1400 : 0), ['valign' => 'center'])
            ->addText('Товары (работы, услуги)', ['bold' => true], $centerParStyle);
        $productTable->addCell($dsc ? 900 : 1000, ['valign' => 'center'])->addText('Кол-вo', ['bold' => true], $centerParStyle);
        $productTable->addCell($dsc ? 600 : 800, ['valign' => 'center'])->addText('Ед', ['bold' => true], $centerParStyle);
        $productTable->addCell($dsc ? 1200 : 1400, ['valign' => 'center'])->addText('Цена', ['bold' => true], $centerParStyle);
        if ($dsc) {
            $discountText = ((YII_ENV_PROD && $model->company_id == 4 ? 'Агентское вознаграждение' : 'Скидка') .
                ($model->discount_type == Invoice::DISCOUNT_TYPE_RUBLE ? ' (руб.)' : ' %'));
            $discountWithPriceText = YII_ENV_PROD && $model->company_id == 4 ? 'Цена с агентским вознаграждением' : 'Цена со скидкой';
            $productTable->addCell(800, ['valign' => 'center'])->addText($discountText, ['bold' => true, 'size' => 9], $centerParStyle);
            $productTable->addCell(1200, ['valign' => 'center'])->addText($discountWithPriceText, ['bold' => true], $centerParStyle);
        }
        $productTable->addCell($dsc ? 1200 : 1400, ['valign' => 'center'])->addText('Сумма', ['bold' => true], $centerParStyle);

        foreach ($orderArray as $key => $order) {
            $unitName = $order->unit ? $order->unit->name : Product::DEFAULT_VALUE;
            $orderQuantity = $unitName == Product::DEFAULT_VALUE ? $unitName : $order->quantity;
            if ($unitName !== Product::DEFAULT_VALUE && $orderQuantity != intval($orderQuantity)) {
                $orderQuantity = rtrim(number_format($orderQuantity, 10, '.', ''), 0);
            }
            $productTable->addRow();
            $productTable->addCell($dsc ? 400 : 500)->addText(++$key, [], $centerParStyle);
            if ($art) {
                $productTable->addCell(1400, ['valign' => 'center'])->addText($order->article, [], $leftParStyle);
            }
            $productTable->addCell(($dsc ? 4000 : 5200) - ($art ? 1400 : 0))->addText(htmlspecialchars($order->product_title), [], $leftParStyle);
            $productTable->addCell($dsc ? 900 : 1000)->addText($orderQuantity, [], $rightParStyle);
            $productTable->addCell($dsc ? 600 : 800)
                ->addText($unitName, [], $rightParStyle);
            if ($dsc) {
                $discountAmount = $order->getDiscountViewValue();
                $productTable->addCell(1200)
                    ->addText(TextHelper::invoiceMoneyFormat($order->view_price_base, $precision), [], $rightParStyle);
                $productTable->addCell(800)
                    ->addText($discountAmount, [], $centerParStyle);
            }
            $productTable->addCell($dsc ? 1200 : 1400)
                ->addText(TextHelper::invoiceMoneyFormat($order->view_price_one, $precision), [], $rightParStyle);
            $productTable->addCell($dsc ? 1200 : 1400)
                ->addText(TextHelper::invoiceMoneyFormat($order->view_total_amount, $precision), [], $rightParStyle);
        }

        $section->addTextBreak(1, ['size' => 1], $leftParStyle);

        $totalAmount = TextHelper::invoiceMoneyFormat($model->view_total_with_nds, 2);
        $currCode = $model->currency_name == Currency::DEFAULT_NAME ? '' : " ({$model->currency_name})";

        $totalTable = $section->addTable([
            'bidiVisual' => 0,
            'borderColor' => 'FFFFFF',
            'borderSize' => 0,
            'borderBottomSize' => 12,
            'borderBottomColor' => '000000',
            'cellMarginTop' => 0,
            'cellMarginRight' => 50,
            'cellMarginBottom' => 10,
            'cellMarginLeft' => 50,
        ]);
        if ($dsc) {
            $totalTable->addRow(200);
            $totalTable->addCell(8900)
                ->addText("Сумма скидки{$currCode}:", ['bold' => true], $rightParStyle);
            $totalTable->addCell(1400)
                ->addText(TextHelper::invoiceMoneyFormat($model->view_total_discount, 2), [], $rightParStyle);
        }
        $totalTable->addRow(200);
        $totalTable->addCell(8900)
            ->addText("Итого{$currCode}:", ['bold' => true], $rightParStyle);
        $totalTable->addCell(1400)
            ->addText(TextHelper::invoiceMoneyFormat($model->view_total_amount, 2), [], $rightParStyle);
        $totalTable->addRow(200);
        $totalTable->addCell(8900)
            ->addText(
                $hasNds ? ($company->isNdsExclude ? '' : 'В том числе ') . "НДС:" : 'Без налога (НДС):',
                ['bold' => true],
                $rightParStyle
            );
        $totalTable->addCell(1400)
            ->addText($hasNds ? TextHelper::invoiceMoneyFormat($model->view_total_nds, 2) : '-', [], $rightParStyle);
        $totalTable->addRow(200);
        $totalTable->addCell(8900)
            ->addText("Всего к оплате{$currCode}:", ['bold' => true], $rightParStyle);
        $totalTable->addCell(1400)
            ->addText($totalAmount, [], $rightParStyle);

        $money = $model->currency_name == Currency::DEFAULT_NAME ? 'руб' : $model->currency_name;
        $totalTable->addRow(200);
        $totalTable->addCell(10300, ['gridSpan' => 2])
            ->addText("Всего наименований {$orderCount}, на сумму {$totalAmount} {$money}.", [], $leftParStyle);
        $totalTable->addRow(200);
        $totalTable->addCell(10300, ['gridSpan' => 2])
            ->addText(TextHelper::mb_ucfirst(Currency::textPrice($model->view_total_with_nds / 100, $model->currency_name)), ['bold' => true], $leftParStyle);
        if ($model->is_invoice_contract && $model->type == Documents::IO_TYPE_OUT && $model->contract_essence) {
            $contractEssence = explode("\n", $model->contract_essence);
            $totalTable->addRow(200);
            $totalTable->addCell(10300, ['gridSpan' => 2])
                ->addText(null, [], $leftParStyle);

            $totalTable->addRow(200);
            $totalTable->addCell(10300, ['gridSpan' => 2])
                ->addText('Предмет договора:', ['bold' => true], $leftParStyle);

            $totalTable->addRow(200);
            $contractEssenceCell = $totalTable->addCell(10300, ['gridSpan' => 2]);
            foreach ($contractEssence as $key => $line) {
                $contractEssenceCell->addText($line, [], $leftParStyle);
            }
        } elseif ($model->comment) {
            $comment = explode("\n", $model->comment);
            $totalTable->addRow(200);
            $commentCell = $totalTable->addCell(10300, ['gridSpan' => 2]);
            foreach ($comment as $key => $line) {
                $commentCell->addText($line, [], $leftParStyle);
            }
        }

        $section->addTextBreak(1, [], $leftParStyle);

        if ($model->is_invoice_contract && $model->type == Documents::IO_TYPE_OUT && $invoiceContractorSignature->is_active) {
            $titleInvoiceContractTable = $section->addTable([
                'bidiVisual' => 0,
                'borderColor' => 'FFFFFF',
                'borderSize' => 0,
            ]);
            $titleInvoiceContractTable->addRow(700);
            $titleInvoiceContractTable->addCell(10300, ['valign' => 'center',])
                ->addText('Адреса и реквизиты сторон:', ['size' => 12, 'bold' => true], $centerParStyle);

            $section->addTextBreak(1, [], $leftParStyle);

            $invoiceContractRequisitesTable = $section->addTable([
                'bidiVisual' => 0,
                'cellMargin' => 0,
                'cellMarginTop' => 100,
                'borderColor' => 'FFFFFF',
                'borderSize' => 0,
            ]);

            $invoiceContractRequisitesTable->addRow();
            $invoiceContractRequisitesTable->addCell(5100)->addText('Исполнитель', ['size' => 11, 'bold' => true], $leftParStyle);
            $invoiceContractRequisitesTable->addCell(100)->addText(null, [], $leftParStyle);
            $invoiceContractRequisitesTable->addCell(5100)->addText('Заказчик', ['size' => 11, 'bold' => true], $leftParStyle);

            $invoiceContractRequisitesTable->addRow();
            $invoiceContractRequisitesTable->addCell(5100)->addText(strpos($model->company_name_short, 'ИП ') === 0 ?
                $model->company_name_full : $model->company_name_short, [], $leftParStyle);
            $invoiceContractRequisitesTable->addCell(100)->addText(null, [], $leftParStyle);
            $invoiceContractRequisitesTable->addCell(5100)->addText($model->contractor_name_short, [], $leftParStyle);

            $invoiceContractRequisitesTable->addRow();
            $invoiceContractRequisitesCell = $invoiceContractRequisitesTable->addCell(5100);
            $invoiceContractRequisitesTextRun = $invoiceContractRequisitesCell->addTextRun($leftParStyle);
            $invoiceContractRequisitesTextRun->addText('ИНН ', ['bold' => true,], $leftParStyle);
            $invoiceContractRequisitesTextRun->addText($model->company_inn, [], $leftParStyle);
            $invoiceContractRequisitesTable->addCell(100)->addText(null, [], $leftParStyle);
            $invoiceContractRequisitesCell = $invoiceContractRequisitesTable->addCell(5100);
            $invoiceContractRequisitesTextRun = $invoiceContractRequisitesCell->addTextRun($leftParStyle);
            $invoiceContractRequisitesTextRun->addText('ИНН ', ['bold' => true,], $leftParStyle);
            $invoiceContractRequisitesTextRun->addText($model->contractor_inn, [], $leftParStyle);

            $invoiceContractRequisitesTable->addRow();
            $invoiceContractRequisitesCell = $invoiceContractRequisitesTable->addCell(5100);
            $invoiceContractRequisitesTextRun = $invoiceContractRequisitesCell->addTextRun($leftParStyle);
            $invoiceContractRequisitesTextRun->addText('КПП ', ['bold' => true,], $leftParStyle);
            $invoiceContractRequisitesTextRun->addText($model->company_kpp, [], $leftParStyle);
            $invoiceContractRequisitesTable->addCell(100)->addText(null, [], $leftParStyle);
            $invoiceContractRequisitesCell = $invoiceContractRequisitesTable->addCell(5100);
            $invoiceContractRequisitesTextRun = $invoiceContractRequisitesCell->addTextRun($leftParStyle);
            $invoiceContractRequisitesTextRun->addText('КПП ', ['bold' => true,], $leftParStyle);
            $invoiceContractRequisitesTextRun->addText($model->contractor_kpp, [], $leftParStyle);

            $invoiceContractRequisitesTable->addRow();
            $invoiceContractRequisitesCell = $invoiceContractRequisitesTable->addCell(5100);
            $invoiceContractRequisitesTextRun = $invoiceContractRequisitesCell->addTextRun($leftParStyle);
            $invoiceContractRequisitesTextRun->addText($model->company->company_type_id == CompanyType::TYPE_IP ? 'ОГРНИП ' : 'ОГРН ', ['bold' => true,], $leftParStyle);
            $invoiceContractRequisitesTextRun->addText($model->company->company_type_id == CompanyType::TYPE_IP ? $model->company_egrip : $model->company->ogrn, [], $leftParStyle);
            $invoiceContractRequisitesTable->addCell(100)->addText(null, [], $leftParStyle);
            $invoiceContractRequisitesCell = $invoiceContractRequisitesTable->addCell(5100);
            $invoiceContractRequisitesTextRun = $invoiceContractRequisitesCell->addTextRun($leftParStyle);
            $invoiceContractRequisitesTextRun->addText($model->company->company_type_id == CompanyType::TYPE_IP ? 'ОГРНИП ' : 'ОГРН ', ['bold' => true,], $leftParStyle);
            $invoiceContractRequisitesTextRun->addText($model->contractor->BIN, [], $leftParStyle);

            $invoiceContractRequisitesTable->addRow();
            $invoiceContractRequisitesCell = $invoiceContractRequisitesTable->addCell(5100);
            $invoiceContractRequisitesTextRun = $invoiceContractRequisitesCell->addTextRun($leftParStyle);
            $invoiceContractRequisitesTextRun->addText('Юридический адрес ', ['bold' => true,], $leftParStyle);
            $invoiceContractRequisitesTextRun->addText($model->company_address_legal_full, [], $leftParStyle);
            $invoiceContractRequisitesTable->addCell(100)->addText(null, [], $leftParStyle);
            $invoiceContractRequisitesCell = $invoiceContractRequisitesTable->addCell(5100);
            $invoiceContractRequisitesTextRun = $invoiceContractRequisitesCell->addTextRun($leftParStyle);
            $invoiceContractRequisitesTextRun->addText('Юридический адрес ', ['bold' => true,], $leftParStyle);
            $invoiceContractRequisitesTextRun->addText($model->contractor_address_legal_full, [], $leftParStyle);

            $invoiceContractRequisitesTable->addRow();
            $invoiceContractRequisitesCell = $invoiceContractRequisitesTable->addCell(5100);
            $invoiceContractRequisitesTextRun = $invoiceContractRequisitesCell->addTextRun($leftParStyle);
            $invoiceContractRequisitesTextRun->addText('Р/сч ', ['bold' => true,], $leftParStyle);
            $invoiceContractRequisitesTextRun->addText($model->company_rs, [], $leftParStyle);
            $invoiceContractRequisitesTable->addCell(100)->addText(null, [], $leftParStyle);
            $invoiceContractRequisitesCell = $invoiceContractRequisitesTable->addCell(5100);
            $invoiceContractRequisitesTextRun = $invoiceContractRequisitesCell->addTextRun($leftParStyle);
            $invoiceContractRequisitesTextRun->addText('Р/сч ', ['bold' => true,], $leftParStyle);
            $invoiceContractRequisitesTextRun->addText($model->contractor_rs, [], $leftParStyle);

            $invoiceContractRequisitesTable->addRow();
            $invoiceContractRequisitesTable->addCell(5100)->addText($model->company_bank_name ? ('в ' . $model->company_bank_name) : null, [], $leftParStyle);
            $invoiceContractRequisitesTable->addCell(100)->addText(null, [], $leftParStyle);
            $invoiceContractRequisitesTable->addCell(5100)->addText($model->contractor_bank_name ? ('в ' . $model->contractor_bank_name) : null, [], $leftParStyle);

            $invoiceContractRequisitesTable->addRow();
            $invoiceContractRequisitesCell = $invoiceContractRequisitesTable->addCell(5100);
            $invoiceContractRequisitesTextRun = $invoiceContractRequisitesCell->addTextRun($leftParStyle);
            $invoiceContractRequisitesTextRun->addText('Кор.счет ', ['bold' => true,], $leftParStyle);
            $invoiceContractRequisitesTextRun->addText($model->company_ks, [], $leftParStyle);
            $invoiceContractRequisitesTable->addCell(100)->addText(null, [], $leftParStyle);
            $invoiceContractRequisitesCell = $invoiceContractRequisitesTable->addCell(5100);
            $invoiceContractRequisitesTextRun = $invoiceContractRequisitesCell->addTextRun($leftParStyle);
            $invoiceContractRequisitesTextRun->addText('Кор.счет ', ['bold' => true,], $leftParStyle);
            $invoiceContractRequisitesTextRun->addText($model->contractor_ks, [], $leftParStyle);

            $invoiceContractRequisitesTable->addRow();
            $invoiceContractRequisitesCell = $invoiceContractRequisitesTable->addCell(5100);
            $invoiceContractRequisitesTextRun = $invoiceContractRequisitesCell->addTextRun($leftParStyle);
            $invoiceContractRequisitesTextRun->addText('БИК ', ['bold' => true,], $leftParStyle);
            $invoiceContractRequisitesTextRun->addText($model->company_bik, [], $leftParStyle);
            $invoiceContractRequisitesTable->addCell(100)->addText(null, [], $leftParStyle);
            $invoiceContractRequisitesCell = $invoiceContractRequisitesTable->addCell(5100);
            $invoiceContractRequisitesTextRun = $invoiceContractRequisitesCell->addTextRun($leftParStyle);
            $invoiceContractRequisitesTextRun->addText('БИК ', ['bold' => true,], $leftParStyle);
            $invoiceContractRequisitesTextRun->addText($model->contractor_bik, [], $leftParStyle);

            $invoiceContractRequisitesTable->addRow();
            $invoiceContractRequisitesTable->addCell(5100)->addText(null, [], $leftParStyle);
            $invoiceContractRequisitesTable->addCell(100)->addText(null, [], $leftParStyle);
            $invoiceContractRequisitesTable->addCell(5100)->addText(null, [], $leftParStyle);

            $invoiceContractRequisitesTable->addRow();
            $invoiceContractRequisitesTable->addCell(5100)->addText($company->self_employed ? 'Самозанятый' : (
                $company->company_type_id == CompanyType::TYPE_IP ? 'Предприниматель' : 'Руководитель'
            ), [], $leftParStyle);
            $invoiceContractRequisitesTable->addCell(100)->addText(null, [], $leftParStyle);
            $invoiceContractRequisitesTable->addCell(5100)->addText($model->contractor->company_type_id == CompanyType::TYPE_IP ? 'Предприниматель' : 'Руководитель', [], $leftParStyle);

            $section->addTextBreak(1, [], $leftParStyle);

            $signTable = $section->addTable([
                'bidiVisual' => 0,
                'borderColor' => 'FFFFFF',
                'borderSize' => 0,
                'cellMarginTop' => 0,
                'cellMarginRight' => 50,
                'cellMarginBottom' => 0,
                'cellMarginLeft' => 50,
            ]);

            $signTable->addRow(1000);
            $signTable->addCell(2500, ['valign' => 'bottom', 'borderBottomSize' => 6, 'borderBottomColor' => '000000']);
            $signTable->addCell(100);
            $signCell = $signTable->addCell(2500, ['valign' => 'bottom', 'borderBottomSize' => 6, 'borderBottomColor' => '000000']);
            $signCell->addText($model->type == Documents::IO_TYPE_OUT ?
                ($model->signed_by_name ? $model->signed_by_name : $model->getCompanyChiefFio(true)) :
                $model->contractor->director_name, [], $centerParStyle);
            if ($model->signed_by_employee_id && $model->type == Documents::IO_TYPE_OUT) {
                $document = 'по ' . mb_strtolower($model->signBasisDocument->name2) . ' №' . $model->sign_document_number;
                $date = 'от ' . DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . 'г.';
                $signCell->addText($document, [], $centerParStyle);
                $signCell->addText($date, [], $centerParStyle);
            }
            $signTable->addCell(100);
            $signTable->addCell(2500, ['valign' => 'bottom', 'borderBottomSize' => 6, 'borderBottomColor' => '000000']);
            $signTable->addCell(100);
            $signCell = $signTable->addCell(2500, ['valign' => 'bottom', 'borderBottomSize' => 6, 'borderBottomColor' => '000000']);
            $signCell->addText($model->contractor->getDirectorFio(), [], $centerParStyle);

            $signTable->addRow(300);
            $signTable->addCell(2500, ['valign' => 'top'])->addText('(подпись)', ['size' => 8], $centerParStyle);
            $signTable->addCell(100);
            $signTable->addCell(2500, ['valign' => 'top'])->addText('(расшифровка подписи)', ['size' => 8], $centerParStyle);
            $signTable->addCell(100);
            $signTable->addCell(2500, ['valign' => 'top'])->addText('(подпись)', ['size' => 8], $centerParStyle);
            $signTable->addCell(100);
            $signTable->addCell(2500, ['valign' => 'top'])->addText('(расшифровка подписи)', ['size' => 8], $centerParStyle);

        } else {
            $signTable = $section->addTable([
                'bidiVisual' => 0,
                'borderColor' => 'FFFFFF',
                'borderSize' => 0,
                'cellMarginTop' => 0,
                'cellMarginRight' => 50,
                'cellMarginBottom' => 0,
                'cellMarginLeft' => 50,
            ]);
            if (($model->type == Documents::IO_TYPE_OUT && $model->company->getIsLikeIp()) ||
                ($model->type == Documents::IO_TYPE_IN && $model->contractor->companyTypeId == CompanyType::TYPE_IP)) {
                $sellerText = $model->type == Documents::IO_TYPE_OUT && $company->self_employed ?
                    'Самозанятый' : 'Предприниматель';
                $signTable->addRow(1000);
                $signTable->addCell(3000, ['valign' => 'bottom'])->addText($sellerText, [], $leftParStyle);
                $signTable->addCell(150);
                $signTable->addCell(3500, ['borderBottomSize' => 6, 'borderBottomColor' => '000000']);
                $signTable->addCell(150);
                $signTable->addCell(3500, ['valign' => 'bottom', 'borderBottomSize' => 6, 'borderBottomColor' => '000000'])
                    ->addText($model->type == Documents::IO_TYPE_OUT ?
                        $model->getCompanyChiefFio(true) :
                        $model->contractor->director_name, [], $centerParStyle);

                $signTable->addRow(300);
                $signTable->addCell(3000);
                $signTable->addCell(150);
                $signTable->addCell(3500, ['valign' => 'top'])->addText('подпись', ['size' => 8], $centerParStyle);
                $signTable->addCell(150);
                $signTable->addCell(3500, ['valign' => 'top'])->addText('расшифровка подписи', ['size' => 8], $centerParStyle);
            } else {
                $signTable->addRow(1000);
                $signTable->addCell(1900, ['valign' => 'bottom'])
                    ->addText('Руководитель', [], $leftParStyle);
                $signTable->addCell(100);
                $signTable->addCell(2700, ['valign' => 'bottom', 'borderBottomSize' => 6, 'borderBottomColor' => '000000'])
                    ->addText($model->type == Documents::IO_TYPE_OUT ?
                        ($model->signEmployeeCompany ? $model->signEmployeeCompany->position : $model->company_chief_post_name) :
                        $model->contractor->director_post_name, [], $centerParStyle);
                $signTable->addCell(100);
                $signTable->addCell(2700, ['valign' => 'bottom', 'borderBottomSize' => 6, 'borderBottomColor' => '000000']);
                $signTable->addCell(100);
                $signCell = $signTable->addCell(2700, ['valign' => 'bottom', 'borderBottomSize' => 6, 'borderBottomColor' => '000000']);
                $signCell->addText($model->type == Documents::IO_TYPE_OUT ?
                    ($model->signed_by_name ? $model->signed_by_name : $model->getCompanyChiefFio(true)) :
                    $model->contractor->director_name, [], $centerParStyle);
                if ($model->signed_by_employee_id && $model->type == Documents::IO_TYPE_OUT) {
                    $document = 'по ' . mb_strtolower($model->signBasisDocument->name2) . ' №' . $model->sign_document_number;
                    $date = 'от ' . DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . 'г.';
                    $signCell->addText($document, [], $centerParStyle);
                    $signCell->addText($date, [], $centerParStyle);
                }

                $signTable->addRow(300);
                $signTable->addCell(1900);
                $signTable->addCell(100);
                $signTable->addCell(2700, ['valign' => 'top'])->addText('должность', ['size' => 8], $centerParStyle);
                $signTable->addCell(100);
                $signTable->addCell(2700, ['valign' => 'top'])->addText('подпись', ['size' => 8], $centerParStyle);
                $signTable->addCell(100);
                $signTable->addCell(2700, ['valign' => 'top'])->addText('расшифровка подписи', ['size' => 8], $centerParStyle);

                $section->addTextBreak(1, [], $leftParStyle);

                $signTable2 = $section->addTable([
                    'bidiVisual' => 0,
                    'borderColor' => 'FFFFFF',
                    'borderSize' => 0,
                    'cellMarginTop' => 0,
                    'cellMarginRight' => 50,
                    'cellMarginBottom' => 0,
                    'cellMarginLeft' => 50,
                ]);

                $accountant = '';
                if ($model->type == Documents::IO_TYPE_OUT) {
                    $accountant .= $model->signed_by_name ? $model->signed_by_name : $model->getCompanyChiefAccountantFio(true);
                    if ($model->signed_by_employee_id) {
                        $accountant .= ' по ' . mb_strtolower($model->signBasisDocument->name2);
                        $accountant .= ' №' . $model->sign_document_number;
                        $accountant .= ' от ' . DateHelper::format(
                                $model->sign_document_date,
                                DateHelper::FORMAT_USER_DATE,
                                DateHelper::FORMAT_DATE
                            ) . 'г.';
                    }
                } else {
                    if ($model->contractor->chief_accountant_is_director) {
                        $accountant .= $model->contractor->director_name;
                    } else {
                        $accountant .= $model->contractor->chief_accountant_name;
                    }
                }

                $signTable2->addRow(600);
                $signTable2->addCell(4000, ['valign' => 'bottom'])->addText('Главный (старший) бухгалтер:', [], $leftParStyle);
                $signTable2->addCell(150);
                $signTable2->addCell(3000, ['valign' => 'bottom', 'borderBottomSize' => 6, 'borderBottomColor' => '000000']);
                $signTable2->addCell(150);
                $signTable2->addCell(3000, ['valign' => 'bottom', 'borderBottomSize' => 6, 'borderBottomColor' => '000000'])
                    ->addText($accountant, [], $centerParStyle);

                $signTable2->addRow(300);
                $signTable2->addCell(4000);
                $signTable2->addCell(150);
                $signTable2->addCell(3000, ['valign' => 'top'])->addText('подпись', ['size' => 8], $centerParStyle);
                $signTable2->addCell(150);
                $signTable2->addCell(3000, ['valign' => 'top'])->addText('расшифровка подписи', ['size' => 8], $centerParStyle);
            }
        }

        $section->addTextBreak(1);
    }

    public function getFileName()
    {
        return $this->model->getPrintTitle() . '.docx';
    }

    public function getFile()
    {
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=\"{$this->fileName}\"");
        header('Content-Type: application/vnd.openxmlformats-officedocument.wordprocessingml.document');
        header('Content-Transfer-Encoding: binary');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Expires: 0');

        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($this->phpWord, 'Word2007');

        $objWriter->save("php://output");

        exit;
    }

    public function saveFile($path)
    {
        $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($this->phpWord, 'Word2007');

        return $objWriter->save($path);
    }
}
