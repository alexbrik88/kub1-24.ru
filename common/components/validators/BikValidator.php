<?php
/**
 * Created by konstantin.
 * Date: 23.7.15
 * Time: 10.38
 */

namespace common\components\validators;


use common\models\dictionary\bik\BikDictionary;
use yii\base\InvalidConfigException;
use yii\validators\Validator;

/**
 * Class BikValidator
 * @package common\components\validators
 */
class BikValidator extends Validator
{

    /**
     * @var string
     */
    public $notFound;
    /**
     * @var string
     */
    public $wrongLength;

    /**
     * Key-value pairs to fill up model if bik was found.
     * Representation: [key => value,], where key - attribute of current model, value - attribute of `bik` model.
     * Used only in `validateAttribute``.
     * @var array
     */
    public $related;

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();

        if ($this->notFound === null) {
            $this->notFound = '{attribute} {value} не найден.';
        }
        if ($this->wrongLength === null) {
            $this->wrongLength = \Yii::t('yii', '{attribute} should contain {length, number} {length, plural, one{character} other{characters}}.');
        }


        if ($this->related !== null && !is_array($this->related)) {
            throw new InvalidConfigException(\Yii::t('', '{attribute} must be an array.', [
                'attribute' => 'related',
            ]));
        }
    }

    /**
     * @inheritdoc
     */
    public function validateAttribute($model, $attribute)
    {
        $value = $model->$attribute;

        if (is_array($value)) {
            $this->addError($model, $attribute, $this->message);

            return;
        }



        if (($bik = $this->_getBik($value)) !== null) {
            if (!empty($this->related)) {
                // set related attributes
                foreach ($this->related as $key => $value) {
                    $model->$key = $bik->$value;
                }
            }
        } else {
            //$this->addError($model, $attribute, $this->notFound);
        }
    }

    /**
     * @inheritdoc
     */
    protected function validateValue($value)
    {
        if (is_array($value)) {
            return [\Yii::t('yii', '{attribute} is invalid.'), []];
        }

        if ($this->_getBik($value) === null) {
            return [$this->notFound, []];
        }

        return null;
    }

    /**
     * @param $value
     * @return array|null|\yii\db\ActiveRecord
     */
    private function _getBik($value)
    {
        return BikDictionary::find()->byBik($value)->byActive()->one();
    }
}