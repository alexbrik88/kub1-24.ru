<?php
namespace common\components\validators;

use Yii;
use yii\helpers\ArrayHelper;
use yii\validators\Validator;

class PasswordValidator extends Validator
{
    public $minLength = 8;
    public $minConditions = 3;
    public $minLengthMsg = 'Длина пароля - минимум {length} символов';
    public $minConditionsMsg = 'Должно выполняться {conditions} из 4х условий: буквы нижнего регистра, буквы верхнего регистра, наличие цифр, наличие знаков.';

    private $patterns = [
        '/\p{Lu}/u',
        '/\p{Ll}/u',
        '/\d/u',
        '/[_\W]/u',
    ];

    public function validateAttribute($model, $attribute)
    {
        $config = ArrayHelper::getValue(Yii::$app->params, 'passwordValidator');
        $minLength = ArrayHelper::getValue($config, 'minLength', $this->minLength);
        $minConditions = ArrayHelper::getValue($config, 'minConditions', $this->minConditions);

        $value = $model->$attribute;
        if (strlen($value) < $minLength) {
            $this->addError($model, $attribute, $this->minLengthMsg, [
                'length' => $minLength,
            ]);
        }

        $conditions = 0;
        foreach ($this->patterns as $pattern) {
            if (preg_match($pattern, $value)) {
                $conditions++;
            }
        }
        if ($conditions < $minConditions) {
            $this->addError($model, $attribute, $this->minConditionsMsg, [
                'conditions' => $minConditions,
            ]);
        }
    }
}
