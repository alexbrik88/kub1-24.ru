<?php
/**
 * Created by konstantin.
 * Date: 23.7.15
 * Time: 13.04
 */

namespace common\components\validators;


use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use Yii;
use yii\db\ActiveRecord;
use yii\validators\EmailValidator;
use yii\validators\Validator;

/**
 * Class EmployeeEmailValidator
 * @package common\components\validators
 */
class EmployeeEmailValidator extends Validator
{
    /**
     * @var int|\Closure
     */
    public $userId;

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->message = Yii::t('yii', '{attribute} "{value}" has already been taken.');
    }

    /**
     * @inheritdoc
     */
    public function validateAttribute($model, $attribute)
    {
        if ($this->userId !== null && $this->userId instanceof \Closure) {
            $this->userId = call_user_func($this->userId);
        } elseif ($model instanceof ActiveRecord && $model->hasAttribute('id')) {
            $this->userId = $model->id;
        }

        parent::validateAttribute($model, $attribute);
    }

    /**
     * @inheritdoc
     */
    protected function validateValue($value)
    {
        if (!$this->_validate($value)) {
            return [$this->message, []];
        }

        return null;
    }

    /**
     * @param $email
     * @return bool
     */
    private function _validate($email)
    {
        if (\Yii::$app->user->identity instanceof Employee) {
            /* @var $currentUser Employee */
            $currentUser = Yii::$app->user->identity;
            $query = Employee::find()
                ->byIsDeleted(Employee::NOT_DELETED)
                ->byEmail($email);
            if ($currentUser !== null) {
                $query->andWhere(['company_id' => $currentUser->company->id]);
            }

            if ($this->userId !== null) {
                $query->andWhere(['<>', 'id', $this->userId]);
            }

            return ((int) $query->count()) === 0;
        } else {
            return true;
        }
    }

}