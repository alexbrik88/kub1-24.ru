<?php
/**
 * Created by konstantin.
 * Date: 23.7.15
 * Time: 10.38
 */

namespace common\components\validators;

use Yii;
use yii\validators\RegularExpressionValidator;
use yii\validators\Validator;

/**
 * Class PhoneValidator
 *
 * @todo: create CompositeValidator and mote methods `validateValue` and `clientValidateAttribute` into it.
 * @todo: create asset for setting mask on input.
 *
 * @package common\components\validators
 */
class PhoneValidator extends Validator
{

    const FORMAT_INTERNATIONAL = 'international';

    /**
     * @var array
     */
    public static $patternArray = [
        self::FORMAT_INTERNATIONAL => '/\+7\([\d]{3}\) [\d]{3}\-[\d]{2}-[\d]{2}/',
    ];

    /**
     * @var string
     */
    public $pattern = self::FORMAT_INTERNATIONAL;

    /**
     * @var string
     */
    public $message = '{attribute} должен быть в формате +7(XXX) XXX-XX-XX';

    public function init()
    {
        if (!isset(static::$patternArray[$this->pattern])) {
            throw new \Exception('Формат {attribute} задан неверно.');
        }
    }

    /**
     * @inheritdoc
     */
    protected function validateValue($value)
    {
        $errorMessage = null;
        foreach ($this->getValidators() as $validator) {
            $validator->validate($value, $errorMessage);

            if ($errorMessage !== null) {
                return [$errorMessage, []];
            }
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function clientValidateAttribute($model, $attribute, $view)
    {
        $script = [];

        foreach ($this->getValidators() as $validator) {
            $script[] = $validator->clientValidateAttribute($model, $attribute, $view);
        }

        return join('', $script);
    }

    /**
     * @return Validator[]
     */
    private function getValidators()
    {
        return [
            new RegularExpressionValidator([
                'pattern' => static::$patternArray[$this->pattern],
                'message' => $this->message,
            ]),
        ];
    }

}