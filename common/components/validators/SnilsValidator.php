<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 07.06.2019
 * Time: 12:44
 */

namespace common\components\validators;

use yii\validators\RegularExpressionValidator;
use yii\validators\Validator;

/**
 * Class SnilsValidator
 * @package common\components\validators
 */
class SnilsValidator extends Validator
{
    /**
     * @var string
     */
    public $message = '{attribute} должен быть в формате ХХХ-ХХХ-ХХХ ХХ';

    /**
     * @inheritdoc
     */
    protected function validateValue($value)
    {
        $errorMessage = null;
        foreach ($this->getValidators() as $validator) {
            $validator->validate($value, $errorMessage);

            if ($errorMessage !== null) {
                return [$errorMessage, []];
            }
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function clientValidateAttribute($model, $attribute, $view)
    {
        $script = [];

        foreach ($this->getValidators() as $validator) {
            $script[] = $validator->clientValidateAttribute($model, $attribute, $view);
        }

        return join('', $script);
    }

    /**
     * @return Validator[]
     */
    private function getValidators()
    {
        return [
            new RegularExpressionValidator([
                'pattern' => '/[\d]{3}\-[\d]{3}\-[\d]{3} [\d]{2}/',
                'message' => $this->message,
            ]),
        ];
    }
}