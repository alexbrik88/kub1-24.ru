<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 23.11.2018
 * Time: 19:43
 */

namespace common\components\validators;


use yii\validators\RegularExpressionValidator;
use yii\validators\Validator;

/**
 * Class CarStateNumberValidator
 * @package common\components\validators
 */
class CarStateNumberValidator extends Validator
{
    /**
     *
     */
    const FORMAT_RUSSIA = 'russia';

    /**
     * @var array
     */
    public static $patternArray = [
        self::FORMAT_RUSSIA => '/^[а-яА-ЯЁё]{1} [0-9]{3} [а-яА-ЯЁё]{2} [0-9]{2,3}$/u',
    ];

    /**
     * @var string
     */
    public $pattern = self::FORMAT_RUSSIA;

    /**
     * @var string
     */
    public $message = '«Номер ТС» должен быть в формате X XXX XX (XXX)';

    /**
     * @throws \Exception
     */
    public function init()
    {
        if (!isset(static::$patternArray[$this->pattern])) {
            throw new \Exception('Формат {attribute} задан неверно.');
        }
    }

    /**
     * @inheritdoc
     */
    protected function validateValue($value)
    {
        $errorMessage = null;
        foreach ($this->getValidators() as $validator) {
            $validator->validate($value, $errorMessage);

            if ($errorMessage !== null) {
                return [$errorMessage, []];
            }
        }

        return null;
    }

    /**
     * @inheritdoc
     */
    public function clientValidateAttribute($model, $attribute, $view)
    {
        $script = [];

        foreach ($this->getValidators() as $validator) {
            $script[] = $validator->clientValidateAttribute($model, $attribute, $view);
        }

        return join('', $script);
    }

    /**
     * @return Validator[]
     */
    private function getValidators()
    {
        return [
            new RegularExpressionValidator([
                'pattern' => static::$patternArray[$this->pattern],
                'message' => $this->message,
            ]),
        ];
    }
}