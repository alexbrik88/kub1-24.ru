<?php

namespace common\components;

use DateTime;
use Yii;
use yii\helpers\ArrayHelper;

/**
 * Class SyncTimeZone
 */
class SyncTimeZone extends \yii\base\Component
{
    private static $synchronized = false;

    /**
     * sync php and db session timezone with user timezone
     */
    public static function sync() : void
    {
        if (self::$synchronized) return;

        if ($tz = ArrayHelper::getValue(Yii::$app, ['user', 'identity', 'timeZone', 'time_zone'])) {
            date_default_timezone_set($tz);
        } else {
            date_default_timezone_set('Europe/Moscow');
        }

        $date = new DateTime();

        Yii::$app->db->createCommand("
            SET time_zone='{$date->format('P')}';
        ")->execute();

        self::$synchronized = true;
    }
}
