<?php

namespace common\components;

use common\models\Company;

class Integrations
{
    private ?\DateTimeInterface $dateFrom;
    private ?\DateTimeInterface $dateTo;
    private ?Company $company;

    public function __construct(\DateTimeInterface $dateFrom = null, \DateTimeInterface $dateTo = null, ?Company $company = null)
    {
        $this->dateFrom = $dateFrom ? date_create($dateFrom->format('Y-m-d 00:00:00')) : null;
        $this->dateTo = $dateTo ? date_create($dateTo->format('Y-m-d 23:59:59')) : null;
        $this->company = $company;
    }

    /**
     * @return array
     */
    public function getIntegrationsTimeAll()
    {
        return [
            $this->getIntegrationAmocrmTime(),
            $this->getIntegrationBitrix24Time(),
            $this->getIntegrationPlatformaOfdTime(),
            $this->getIntegrationTaxcomOfdTime(),
            $this->getIntegrationTaxcomTime(),
            $this->getIntegrationMonetaRuTime(),
            $this->getIntegrationUKassaTime(),
            $this->getIntegrationZenmoneyTime(),
        ];
    }

    /**
     * @return int|string
     */
    public function getIntegrationPlatformaOfdTime()
    {
        return [
            'name' => 'Платформа ОФД',
            'time' => $this->getIntegrationPlatformaOfdQuery()->select('FROM_UNIXTIME([[created_at]])')->scalar(),
        ];
    }

    /**
     * @return int|string
     */
    public function getIntegrationTaxcomOfdTime()
    {
        return [
            'name' => 'Такском ОФД',
            'time' => $this->getIntegrationTaxcomOfdQuery()->select('FROM_UNIXTIME([[created_at]])')->scalar(),
        ];
    }

    /**
     * @return int|string
     */
    public function getIntegrationTaxcomTime()
    {
        return [
            'name' => 'Такском',
            'time' => $this->getIntegrationTaxcomQuery()->select('FROM_UNIXTIME([[created_at]])')->scalar(),
        ];
    }

    /**
     * @return int|string
     */
    public function getIntegrationAmocrmTime()
    {
        return [
            'name' => 'AmoCRM',
            'time' => $this->getIntegrationAmocrmQuery()->select('FROM_UNIXTIME([[created_at]])')->scalar(),
        ];
    }

    /**
     * @return int|string
     */
    public function getIntegrationBitrix24Time()
    {
        return [
            'name' => 'Bitrix24',
            'time' => $this->getIntegrationBitrix24Query()->select('FROM_UNIXTIME([[created_at]])')->scalar(),
        ];
    }

    /**
     * @return int|string
     */
    public function getIntegrationMonetaRuTime()
    {
        return [
            'name' => 'Moneta.ru',
            'time' => $this->getIntegrationMonetaRuQuery()->select('created_at')->scalar(),
        ];
    }

    /**
     * @return int|string
     */
    public function getIntegrationUKassaTime()
    {
        return [
            'name' => 'ЮKassa',
            'time' => $this->getIntegrationUKassaQuery()->select('created_at')->scalar(),
        ];
    }

    /**
     * @return int|string
     */
    public function getIntegrationZenmoneyTime()
    {
        return [
            'name' => 'Дзен Мани',
            'time' => $this->getIntegrationZenmoneyQuery()->select('created_at')->scalar(),
        ];
    }

    /**
     * @return array
     */
    public function getIntegrationsCountAll()
    {
        return [
            $this->getIntegrationAmocrmCount(),
            $this->getIntegrationBitrix24Count(),
            $this->getIntegrationPlatformaOfdCount(),
            $this->getIntegrationTaxcomOfdCount(),
            $this->getIntegrationTaxcomCount(),
            $this->getIntegrationMonetaRuCount(),
            $this->getIntegrationUKassaCount(),
            $this->getIntegrationZenmoneyCount(),
            $this->getIntegrationApiAndroidCount(),
        ];
    }

    /**
     * @return int|string
     */
    public function getIntegrationPlatformaOfdCount()
    {
        return [
            'name' => 'Платформа ОФД',
            'count' => $this->getIntegrationPlatformaOfdQuery()->count(),
        ];
    }

    /**
     * @return int|string
     */
    public function getIntegrationTaxcomOfdCount()
    {
        return [
            'name' => 'Такском ОФД',
            'count' => $this->getIntegrationTaxcomOfdQuery()->count(),
        ];
    }

    /**
     * @return int|string
     */
    public function getIntegrationTaxcomCount()
    {
        return [
            'name' => 'Такском',
            'count' => $this->getIntegrationTaxcomQuery()->count(),
        ];
    }

    /**
     * @return int|string
     */
    public function getIntegrationAmocrmCount()
    {
        return [
            'name' => 'AmoCRM',
            'count' => $this->getIntegrationAmocrmQuery()->count(),
        ];
    }

    /**
     * @return int|string
     */
    public function getIntegrationBitrix24Count()
    {
        return [
            'name' => 'Bitrix24',
            'count' => $this->getIntegrationBitrix24Query()->count(),
        ];
    }

    /**
     * @return int|string
     */
    public function getIntegrationMonetaRuCount()
    {
        return [
            'name' => 'Moneta.ru',
            'count' => $this->getIntegrationMonetaRuQuery()->count(),
        ];
    }

    /**
     * @return int|string
     */
    public function getIntegrationUKassaCount()
    {
        return [
            'name' => 'ЮKassa',
            'count' => $this->getIntegrationUKassaQuery()->count(),
        ];
    }

    /**
     * @return int|string
     */
    public function getIntegrationZenmoneyCount()
    {
        return [
            'name' => 'Дзен Мани',
            'count' => $this->getIntegrationZenmoneyQuery()->count(),
        ];
    }

    /**
     * @return int|string
     */
    public function getIntegrationApiAndroidCount()
    {
        return [
            'name' => 'Android App',
            'count' => $this->getIntegrationApiAndroidQuery()->count(),
        ];
    }

    /**
     * @return int|string
     */
    public function getIntegrationPlatformaOfdQuery()
    {
        return \common\models\ofd\OfdUser::find()->andWhere([
            'ofd_id' => \common\models\ofd\Ofd::PLATFORMA,
        ])->andFilterWhere([
            'company_id' => $this->company ? $this->company->id : null,
        ])->andFilterWhere([
            '>=',
            'created_at',
            $this->dateFrom ? $this->dateFrom->getTimestamp() : null,
        ])->andFilterWhere([
            '<=',
            'created_at',
            $this->dateTo ? $this->dateTo->getTimestamp() : null,
        ]);
    }

    /**
     * @return int|string
     */
    public function getIntegrationTaxcomOfdQuery()
    {
        return \common\models\ofd\OfdUser::find()->andWhere([
            'ofd_id' => \common\models\ofd\Ofd::TAXCOM,
        ])->andFilterWhere([
            'company_id' => $this->company ? $this->company->id : null,
        ])->andFilterWhere([
            '>=',
            'created_at',
            $this->dateFrom ? $this->dateFrom->getTimestamp() : null,
        ])->andFilterWhere([
            '<=',
            'created_at',
            $this->dateTo ? $this->dateTo->getTimestamp() : null,
        ]);
    }

    /**
     * @return int|string
     */
    public function getIntegrationTaxcomQuery()
    {
        return \frontend\modules\cash\modules\ofd\models\OfdParams::find()->andWhere([
            'ofd_alias' => \frontend\modules\cash\modules\ofd\modules\taxcom\models\OfdModel::ALIAS,
        ])->andFilterWhere([
            'company_id' => $this->company ? $this->company->id : null,
        ])->andFilterWhere([
            '>=',
            'created_at',
            $this->dateFrom ? $this->dateFrom->getTimestamp() : null,
        ])->andFilterWhere([
            '<=',
            'created_at',
            $this->dateTo ? $this->dateTo->getTimestamp() : null,
        ]);
    }

    /**
     * @return int|string
     */
    public function getIntegrationAmocrmQuery()
    {
        return \common\models\amocrm\AmocrmWidget::find()->andFilterWhere([
            'company_id' => $this->company ? $this->company->id : null,
        ])->andFilterWhere([
            '>=',
            'created_at',
            $this->dateFrom ? $this->dateFrom->getTimestamp() : null,
        ])->andFilterWhere([
            '<=',
            'created_at',
            $this->dateTo ? $this->dateTo->getTimestamp() : null,
        ]);
    }

    /**
     * @return int|string
     */
    public function getIntegrationBitrix24Query()
    {
        return \frontend\modules\bitrix24\models\WidgetCompany::find()->andFilterWhere([
            'company_id' => $this->company ? $this->company->id : null,
        ])->andFilterWhere([
            '>=',
            'created_at',
            $this->dateFrom ? $this->dateFrom->getTimestamp() : null,
        ])->andFilterWhere([
            '<=',
            'created_at',
            $this->dateTo ? $this->dateTo->getTimestamp() : null,
        ]);
    }

    /**
     * @return int|string
     */
    public function getIntegrationMonetaRuQuery()
    {
        return \common\modules\acquiring\models\MonetaIdentity::find()->andFilterWhere([
            'company_id' => $this->company ? $this->company->id : null,
        ])->andFilterWhere([
            '>=',
            'created_at',
            $this->dateFrom ? $this->dateFrom->format('Y-m-d H:i:s') : null,
        ])->andFilterWhere([
            '<=',
            'created_at',
            $this->dateTo ? $this->dateTo->format('Y-m-d H:i:s') : null,
        ]);
    }

    /**
     * @return int|string
     */
    public function getIntegrationUKassaQuery()
    {
        return \common\modules\acquiring\models\YookassaIdentity::find()->andFilterWhere([
            'company_id' => $this->company ? $this->company->id : null,
        ])->andFilterWhere([
            '>=',
            'created_at',
            $this->dateFrom ? $this->dateFrom->format('Y-m-d H:i:s') : null,
        ])->andFilterWhere([
            '<=',
            'created_at',
            $this->dateTo ? $this->dateTo->format('Y-m-d H:i:s') : null,
        ]);
    }

    /**
     * @return int|string
     */
    public function getIntegrationZenmoneyQuery()
    {
        return \common\modules\cards\models\ZenmoneyIdentity::find()->andFilterWhere([
            'company_id' => $this->company ? $this->company->id : null,
        ])->andFilterWhere([
            '>=',
            'created_at',
            $this->dateFrom ? $this->dateFrom->format('Y-m-d H:i:s') : null,
        ])->andFilterWhere([
            '<=',
            'created_at',
            $this->dateTo ? $this->dateTo->format('Y-m-d H:i:s') : null,
        ]);
    }

    /**
     * @return int|string
     */
    public function getIntegrationApiAndroidQuery()
    {
        return \common\models\company\ApiFirstVisit::find()->andWhere([
            'api_partners_id' => \Yii::$app->params['api_partners.android.id'] ?? -1,
        ])->andFilterWhere([
            'company_id' => $this->company ? $this->company->id : null,
        ])->andFilterWhere([
            '>=',
            'visit_at',
            $this->dateFrom ? $this->dateFrom->format('Y-m-d H:i:s') : null,
        ])->andFilterWhere([
            '<=',
            'visit_at',
            $this->dateTo ? $this->dateTo->format('Y-m-d H:i:s') : null,
        ]);
    }
}
