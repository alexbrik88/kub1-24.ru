<?php

namespace common\components;

use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\validators\Validator;

/**
 * Поведение для обработки дат created_at и updated_at. Добавляет правила валидации.
 * TimestampBehavior не подходит, т.к. даты приходят из внешних сервисов.
 * Если self::$columns в формате ключ-значение, то ключ воспринимается как имя атрибута,
 * а значение - надо ли устанавливать значение по умолчанию
 *
 */
class IntegrationDateBehavior extends Behavior
{
    public $columns = [
        'created_at',
        'updated_at',
    ];

    public function attach($owner)
    {
        /** @var ActiveRecord $owner */
        parent::attach($owner);
        $default = [];
        $rule = [];
        foreach ($this->columns as $column => $value) {
            if (is_string($column) === true) {
                if ($value) {
                    $default[] = $column;
                }
                $rule[] = $column;
            } else {
                $default[] = $value;
                $rule[] = $value;
            }
        }
        if (count($default) > 0) {
            $owner->validators[] = Validator::createValidator('default', $owner, $default, ['value' => time()]);
        }
        $owner->validators[] = Validator::createValidator('filter', $owner, $rule, [
            'filter' => function ($value) {
            if($value===null) return null;
                if (is_int($value) === false && ctype_digit($value) === false) {
                    $value = strtotime($value);
                }
                return $value;
            },
        ]);
        $owner->validators[] = Validator::createValidator('integer', $owner, $rule);
    }

}