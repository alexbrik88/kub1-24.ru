<?php
namespace common\components;

use Yii;
use yii\base\Model;
use yii\web\Controller;
use yii\web\Response;
use yii\widgets\ActiveForm;

/**
 * Class CommonController
 * @package common\components
 */
class CommonController extends Controller
{
    /**
     * Performs ajax validation
     * @param Model $model
     * @return mixed
     */
    protected function ajaxValidate($model)
    {
        $model->load(Yii::$app->request->post());

        Yii::$app->response->format = Response::FORMAT_JSON;
        return ActiveForm::validate($model);
    }

    /**
     * Ajax validating
     *
     * @example usage:
     * ~~~
     *  if (($errors = $this->_ajaxValidate($model)) !== true) {
     *      return $errors;
     *  }
     * ~~~
     *
     * @param Model $model
     * @return bool
     */
    protected function _ajaxValidate(Model $model)
    {
        if (Yii::$app->request->isAjax) {
            $model->load(
                Yii::$app->request->isPost
                    ? Yii::$app->request->post()
                    : Yii::$app->request->queryParams
            );

            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;

            return \yii\bootstrap\ActiveForm::validate($model);
        }

        return true;
    }
}