<?php

namespace common\components\fileUpload;


use yii\web\ViewAction;

class UploadAction extends ViewAction
{
    /**
     * Runs the action.
     * This method displays the view requested by the user.
     * @throws NotFoundHttpException if the view file cannot be found
     */
    public function run()
    {

    }
}
