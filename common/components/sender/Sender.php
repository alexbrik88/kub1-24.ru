<?php
/**
 * Created by PhpStorm.
 * User: crearsdev
 * Date: 25.02.16
 * Time: 13:12
 */
namespace common\components\sender;

use yii\base\Exception;

/**
 * Class Sender
 * @package common\components\sender
 */
class Sender
{
    /**
     * @param $ruleClassName
     * @return \common\components\sender\rule\BaseRule
     * @throws Exception
     */
    public static function getRule($ruleClassName)
    {
        $object = null;

        if (class_exists($ruleClassName)) {
            $object = new $ruleClassName();
        } else {
            throw new Exception('Правило не найдено');
        }

        return $object;
    }
}