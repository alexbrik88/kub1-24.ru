<?php
/**
 * Created by PhpStorm.
 * User: crearsdev
 * Date: 26.02.16
 * Time: 11:58
 */
namespace common\components\sender\unisender;

use common\components\helpers\ArrayHelper;
use common\components\TextHelper;
use common\models\Company;
use common\models\EmployeeCompany;
use common\models\company\CheckingAccountant;
use common\models\company\CompanyLastVisit;
use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\cash\CashFlowsBase;
use common\models\service\SubscribeHelper;
use frontend\models\Documents;
use frontend\modules\documents\components\InvoiceStatistic;
use Yii;
use yii\base\Exception;
use yii\db\Query;
use yii\helpers\Json;

/**
 * Class UniSender
 * @package common\components\sender\unisender
 */
class UniSender extends unisenderApi
{
    const FIND_INACTIVE_LIMIT = 50;
    const EXPORT_ROWS_LIMIT = 1000;
    const IMPORT_ROWS_LIMIT = 500;
    const DELETE_CONTACT = 1;
    const EMPLOYEE_SELECT_LIMIT = 100;
    const INACTIVE_DAYS_LIMIT = 90;

    const STATUS_ACTIVE = 'active';
    const STATUS_NEW = 'new';
    const STATUS_INACTIVE = 'inactive';
    const STATUS_UNSUBSCRIBED = 'unsubscribed';

    /**
     * Send types list
     *
     * @var array
     */
    public $sendList = [];

    /**
     * Attachments
     * ["file_name" => "binary_content"]
     *
     * @var array
     */
    protected $_attachments = [];

    /**
     * @var array
     */
    protected $_contacts = [];

    /**
     * Send from name
     *
     * @var string
     */
    protected $sendFromName = '';

    /**
     * Send from email
     *
     * @var string
     */
    protected $sendFromEmail = '';

    /**
     * Email subject
     *
     * @var string
     */
    protected $subject = '';

    /**
     * Email template filename
     *
     * @var string
     */
    protected $template = '';

    /**
     * Email templates folder
     *
     * @var string
     */
    protected $emailTemplatesFolder = '@common/mail/unisender';

    /**
     * List id
     *
     * @var int
     */
    protected $listId = 0;

    /**
     * Last error message
     *
     * @var null
     */
    protected $lastError = null;

    /**
     * Last warnings message
     *
     * @var null
     */
    protected $lastWarnings = null;

    /**
     * Last Result
     *
     * @var null
     */
    protected $lastResult = null;

    /**
     * Email template vars
     *
     * @var array
     */
    protected $params = [];

    /**
     * @var integer Unix timestamp
     */
    protected $_inactive_time_limit = null;

    /**
     * @inheritdoc
     */
    public function __construct()
    {
        $uniSenderConfig = \Yii::$app->params['uniSender'];

        parent::__construct($uniSenderConfig['apiKey']);

        // $this->setFromEmail($uniSenderConfig['fromEmail']);
        // $this->setFromName($uniSenderConfig['fromName']);
        // $this->setEmailTemplatePath($uniSenderConfig['templatesFolder']);
        $this->setListId($uniSenderConfig['listId']);

        $this->setParams([
            'baseUrl' => $uniSenderConfig['baseUrl'],
            'lkBaseUrl' => $uniSenderConfig['lkBaseUrl'],
            'domainName' => $uniSenderConfig['domainName'],
            'feedback' => $uniSenderConfig['feedback'],
            'callUs' => $uniSenderConfig['phone'],
            'supportEmail' => \Yii::$app->params['emailList']['support'],
        ]);
    }

    /**
     * Set html template params
     *
     * @param array $params
     * @return $this
     */
    public function setParams($params = [])
    {
        $this->params = ArrayHelper::merge($this->params, $params);

        return $this;
    }

    /**
     * Get html params
     *
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * Get html param
     *
     * @param $name
     * @return null
     */
    public function getParam($name)
    {
        return (isset($this->params[$name]) && !empty($this->params[$name])) ? $this->params[$name] : null;
    }

    /**
     * Get last error message
     *
     * @return string
     */
    public function getLastError()
    {
        return $this->lastError;
    }

    /**
     * Get last error message
     *
     * @return string
     */
    public function getLastWarnings()
    {
        return $this->lastWarnings;
    }

    /**
     * Get last error message
     *
     * @return string
     */
    public function getLastResult()
    {
        return $this->lastResult;
    }

    /**
     * @param $image
     * @return string
     */
    public function getDataURI($image)
    {
        $imageUrl = \Yii::$app->params['uniSender']['imgUrl'] . DIRECTORY_SEPARATOR . $image;
        $imagePath = \Yii::getAlias(\Yii::$app->params['uniSender']['imgPath'] . DIRECTORY_SEPARATOR . $image);

        if (file_exists($imagePath)) {
            return $imageUrl;
        } else {
            return '';
        }
    }

    /**
     * Set list Id
     *
     * @param $listId
     * @return $this
     */
    public function setListId($listId)
    {
        $this->listId = $listId;

        return $this;
    }

    /**
     * Get list Id
     *
     * @return int
     */
    public function getListId()
    {
        return $this->listId;
    }

    /**
     * Set email template name
     *
     * @param $filename
     * @return $this
     */
    public function setEmailTemplateName($filename)
    {
        $this->template = $filename;

        return $this;
    }

    /**
     * Get email template name
     *
     * @return string
     */
    public function getEmailTemplateName()
    {
        return $this->template;
    }

    /**
     * Set email subjects
     *
     * @param $subject
     * @return $this
     */
    public function setSubject($subject)
    {
        $this->subject = $subject;

        return $this;
    }

    /**
     * Get email subject
     *
     * @return string
     */
    public function getSubject()
    {
        return $this->subject;
    }

    /**
     * Set from name param
     *
     * @param $fromName
     * @return $this
     */
    public function setFromName($fromName)
    {
        $this->sendFromName = $fromName;

        return $this;
    }

    /**
     * Get from name param
     *
     * @return string
     */
    public function getFromName()
    {
        return $this->sendFromName;
    }

    /**
     * Get from email param
     *
     * @return string
     */
    public function getFromEmail()
    {
        return $this->sendFromEmail;
    }

    /**
     * Set from email param
     *
     * @param $fromEmail
     * @return $this
     */
    public function setFromEmail($fromEmail)
    {
        $this->sendFromEmail = $fromEmail;

        return $this;
    }

    /**
     * Get templates folder path
     *
     * @return string
     */
    public function getEmailTemplatePath()
    {
        return $this->emailTemplatesFolder;
    }

    /**
     * Set templates folder path
     *
     * @param $path
     * @return $this
     */
    public function setEmailTemplatePath($path)
    {
        $this->emailTemplatesFolder = $path;

        return $this;
    }

    /**
     * Attachments
     *
     * @return $array
     */
    public function getAttachments()
    {
        return $this->_attachments;
    }

    /**
     * Set Attachments
     *
     * @param array $attachments
     * @return $this
     */
    public function setAttachments($attachments)
    {
        $this->_attachments = is_array($attachments) ? $attachments : [];

        return $this;
    }

    /**
     * Add Attachments
     *
     * @param array $attachments
     * @return $this
     */
    public function addAttachments($attachments)
    {
        if (is_array($attachments)) {
            foreach ($attachments as $key => $value) {
                 $this->_attachments[$key] = $value;
            }
        }

        return $this;
    }

    /**
     * Get send type list
     *
     * @return array|mixed
     */
    public function getSendList()
    {
        if (empty($this->sendList)) {
            $temp = json_decode($this->getLists());

            if (isset($temp->result) && sizeof($temp->result) > 0) {
                if (is_array($temp->result)) {
                    foreach ($temp->result as $listRow) {
                        $this->sendList[] = $listRow->id;
                    }
                }
            }
        }

        return $this->sendList;
    }

    /**
     * Set contacts
     *
     * @param $contacts
     * @return $this
     */
    public function setContacts($contacts)
    {
        $this->_contacts = [];
        foreach ((array) $contacts as $contact) {
            if (is_string($contact)) {
                $this->_contacts[] = $contact;
            } elseif (isset($contact['email']) && is_string($contact['email'])) {
                $this->_contacts[] = $contact['email'];
            }
        }

        return $this;
    }

    /**
     * Get contacts
     *
     * @return array
     */
    public function getContacts()
    {
        return (array) $this->_contacts;
    }

    /**
     * @return integer Unix timestamp
     */
    public static function getInactiveTimeLimit()
    {
        return date_create(-self::INACTIVE_DAYS_LIMIT . ' days today')->getTimestamp();
    }

    /**
     * @return integer Unix timestamp
     */
    public function inactiveTimeLimit()
    {
        if ($this->_inactive_time_limit === null) {
            $this->_inactive_time_limit = self::getInactiveTimeLimit();
        }

        return $this->_inactive_time_limit;
    }

    /**
     * Synchronize contact list
     *
     * @return $this
     */
    public function syncContacts($listId)
    {
        Yii::$app->db->createCommand('SET session wait_timeout=28800')->execute();
        Yii::$app->db->createCommand('SET session interactive_timeout=28800')->execute();

        $offset = 0;

        do {
            $query = new Query;
            $query
                ->select([
                    "employee.id",
                    "employee.email",
                    "employee.is_active",
                    "employee.is_deleted",
                    "employee.firstname",
                    "employee.notify_new_features",
                    "last_visit_at" => "MAX({{visit}}.[[time]])",
                    "employee.created_at",
                    "employee.is_mailing_active",
                ])
                ->from([
                    'employee' => Employee::tableName(),
                ])
                ->leftJoin([
                    'visit' => CompanyLastVisit::tableName(),
                ], '{{visit}}.[[employee_id]]={{employee}}.[[id]]')
                ->where(['is_deleted' => false])
                ->orderBy(['id' => SORT_ASC])
                ->groupBy("employee.id")
                ->limit(self::EMPLOYEE_SELECT_LIMIT)
                ->offset($offset++ * self::EMPLOYEE_SELECT_LIMIT);

            $contacts = $query->all();

            if ($contacts) {
                $this->importSubscribers($contacts, $listId);
            }
        } while ($contacts);

        return $this;
    }

    /**
     * @param $name
     * @param $type
     * @return bool
     */
    public function addField($name, $type)
    {
        $result = json_decode($this->createField([
            'name' => $name,
            'type' => $type,
        ]));

        if (isset($result->error)) {
            $this->lastError = $result->error;

            return false;
        }

        return true;
    }

    /**
     * Import contacts data
     *
     * @param $data array
     */
    public function importContacts($data)
    {
        if (isset($data['field_names'], $data['data'])) {
            $i = array_search('email', (array) $data['field_names']);
            if ($i !== false) {
                $denyList = \common\models\employee\EmailDeny::list();
                foreach ((array) $data['data'] as $key => $value) {
                    $email = $value[$i] ?? null;
                    if (empty($email) || !is_string($email) || isset($denyList[strtolower($email)])) {
                        unset($data['data'][$key]);
                    }
                }
            }

            return parent::importContacts($data);
        }

        return parent::importContacts($data);
    }

    /**
     * Import new contact
     *
     * @param $user \common\models\employee\Employee;
     *
     * @return $this
     */
    public function importContact($user)
    {
        $data = [
            'field_names' => [
                'email',
                'email_list_ids',
                'Name',
                'company',
            ],
            'data' => [
                [
                    $user->email,
                    Yii::$app->params['uniSender']['listId'],
                    $user->firstname,
                    $user->company->shortName,
                ],
            ],
        ];

        $this->importContacts($data);

        return $this;
    }

    /**
     * @return $this
     */
    public function importFieldsToDaily()
    {
        $startWeekTime = strtotime('-7 day');
        $startWeek = date('Y-m-d', $startWeekTime);
        $endWeek = date('Y-m-d', strtotime('-1 day'));
        $timeLimit = $this->inactiveTimeLimit();

        foreach ($this->getContacts() as $email) {

            /** @var Employee $employee */
            $employee = Employee::find()->alias('employee')->where([
                'employee.email' => $email,
                'employee.notify_new_features' => true,
                'employee.is_active' => Employee::ACTIVE,
                'employee.is_deleted' => Employee::NOT_DELETED,
                'employee.is_mailing_active' => true,
            ])->leftJoin([
                'visit' => CompanyLastVisit::tableName(),
            ], '{{visit}}.[[employee_id]]={{employee}}.[[id]]')->andWhere([
                '>', 'visit.time', $timeLimit,
            ])->one();

            if ($employee) {
                $dateRange = [
                    'from' => $startWeek,
                    'to' => $endWeek,
                ];

                $companyArray = Company::find()
                                ->leftJoin(['via' => 'employee_company'], 'company.id = via.company_id')
                                ->where([
                                    'company.blocked' => Company::UNBLOCKED,
                                    'company.strict_mode' => Company::OFF_STRICT_MODE,
                                    'via.employee_role_id' => EmployeeRole::ROLE_CHIEF,
                                    'via.employee_id' => $employee->id,
                                ])
                                ->all();

                foreach ($companyArray as $company) {
                    $clientInSum = InvoiceStatistic::getStatisticInfo(
                        Documents::IO_TYPE_OUT,
                        $company->id,
                        InvoiceStatistic::PAID,
                        $dateRange
                    )['sum'];

                    $clientOutSum = InvoiceStatistic::getStatisticInfo(
                        Documents::IO_TYPE_OUT,
                        $company->id,
                        InvoiceStatistic::NOT_PAID,
                        $dateRange
                    )['sum'];

                    $expenses = InvoiceStatistic::getCostsStatistics(
                        Documents::IO_TYPE_IN,
                        $company->id,
                        $dateRange,
                        [InvoiceStatus::STATUS_PAYED,]
                    );

                    $totalPrice = array_sum(ArrayHelper::getColumn($expenses, 'sum'));

                    $data = [
                        'field_names' => [
                            'email',
                            'email_list_ids',
                            'Name',
                            'company',
                            'clientInSum',
                            'clientOutSum',
                            'exhibitInvoiceSum',
                            'expenses',
                        ],
                        'data' => [
                            [
                                $employee->email,
                                Yii::$app->params['uniSender']['listId'],
                                $employee->firstname,
                                $company->shortName,
                                TextHelper::invoiceMoneyFormat($clientInSum, 2),
                                TextHelper::invoiceMoneyFormat($clientOutSum, 2),
                                TextHelper::invoiceMoneyFormat($clientInSum, 2),
                                TextHelper::invoiceMoneyFormat($totalPrice, 2),
                            ],
                        ],
                    ];

                    $this->importContacts($data);
                }
            }
        }

        return $this;
    }

    /**
     * @return $this
     */
    public function importStatisticData($contacts = [])
    {
        $flowsClassArray = [
            \common\models\cash\CashBankFlows::className(),
            \common\models\cash\CashEmoneyFlows::className(),
            \common\models\cash\CashOrderFlows::className(),
        ];
        $startWeekTime = strtotime('-7 day');
        $startWeek = date('Y-m-d', $startWeekTime);
        $endWeek = date('Y-m-d', strtotime('-1 day'));
        $dateRange = [
            'from' => $startWeek,
            'to' => $endWeek,
        ];

        $fieldArray = [];
        $fieldDataArray = json_decode($this->getFields());
        if (!empty($fieldDataArray->result)) {
            foreach ((array) $fieldDataArray->result as $fieldData) {
                if (!empty($fieldData->name)) {
                    $fieldArray[] = $fieldData->name;
                }
            }
        } else {
            return false;
        }

        $this->_contacts = [];
        foreach ($contacts as $employee_id => $company_id_array) {

            $employee = Employee::findOne([
                'id' => $employee_id,
                'notify_new_features' => true,
                'is_mailing_active' => true,
            ]);

            if ($employee) {
                $this->_contacts[] = $employee->email;

                $field_names = [
                    'email',
                    'email_list_ids',
                    'Name',
                ];
                $field_values = [
                    $employee->email,
                    Yii::$app->params['uniSender']['listId'],
                    $employee->firstname,
                ];

                foreach ($company_id_array as $key => $company_id) {
                    if ($key === 0) {
                        $key = '';
                    }

                    $company = Company::findOne($company_id);

                    $clientInSum = 0;
                    foreach ($flowsClassArray as $class) {
                        $clientInSum += (int) $class::find()
                            ->byFlowType(CashFlowsBase::FLOW_TYPE_INCOME)
                            ->byCompany($company_id)
                            ->andWhere(['between', 'date', $startWeek, $endWeek])
                            ->sum('amount');
                    }

                    $clientOutSum = (int) Invoice::find()
                        ->select(['total_amount_with_nds'])
                        ->byCompany($company_id)
                        ->byDeleted(false)
                        ->byIOType(Documents::IO_TYPE_OUT)
                        ->byStatus(InvoiceStatus::getPaymentAllowedTypes(Documents::IO_TYPE_OUT))
                        ->andWhere(['<=', 'document_date', $endWeek])
                        ->sum('total_amount_with_nds');

                    $exhibitInvoiceSum = (int) Invoice::find()
                        ->select(['total_amount_with_nds'])
                        ->byCompany($company_id)
                        ->byDeleted(false)
                        ->byIOType(Documents::IO_TYPE_OUT)
                        ->andWhere(['between', 'document_date', $startWeek, $endWeek])
                        ->andWhere(['not', ['invoice_status_id' => InvoiceStatus::STATUS_REJECTED]])
                        ->sum('total_amount_with_nds');

                    $expenses = InvoiceStatistic::getCostsStatistics(
                        Documents::IO_TYPE_IN,
                        $company->id,
                        $dateRange,
                        [InvoiceStatus::STATUS_PAYED,]
                    );

                    $totalPrice = array_sum(ArrayHelper::getColumn($expenses, 'sum'));

                    $company_field_names = [
                        'company' . $key,
                        'clientInSum' . $key,
                        'clientOutSum' . $key,
                        'exhibitInvoiceSum' . $key,
                        'expenses' . $key,
                    ];
                    foreach ($company_field_names as $value) {
                        if (!in_array($value, $fieldArray)) {
                            $this->createField([
                                'name' => $value,
                                'type' => 'string',
                            ]);
                        }
                        $field_names[] = $value;
                    }
                    $company_field_values = [
                        $company->shortName,
                        TextHelper::invoiceMoneyFormat($clientInSum, 2),
                        TextHelper::invoiceMoneyFormat($clientOutSum, 2),
                        TextHelper::invoiceMoneyFormat($exhibitInvoiceSum, 2),
                        TextHelper::invoiceMoneyFormat($totalPrice, 2),
                    ];
                    foreach ($company_field_values as $value) {
                        $field_values[] = $value;
                    }
                }

                $data = [
                    'field_names' => $field_names,
                    'data' => [$field_values],
                ];

                $this->importContacts($data);
            }
        }

        return $this;
    }

    /**
     * Import contact to UniSender
     *
     * @param array $subscribers
     * @return $this
     */
    protected function importSubscribers($subscribers = [], $listId)
    {
        $inactiveTimeLimit = $this->inactiveTimeLimit();

        $data = [
            'field_names' => [
                'email',
                'delete',
                'email_list_ids',
                'email_status', // unsubscribed ?
                'Name',
                'company',
                'last_visit',
                'NE_zapolnen_profil',
                'Zapolnen_profil_i_NE_i_vystavil_schet',
                'Zapolnen_profil_i_vystavil_schet_NE_otpravil_ego_po_e_mail',
                'dateOfRegistration',
                'tarif',
                'BankName',
                'fs',
                'sno',
            ],
            'data' => [],
        ];

        foreach ($subscribers as $contact) {
            $delete = $contact['is_active'] == Employee::NOT_ACTIVE || $contact['last_visit_at'] < $inactiveTimeLimit;
            $company = $delete ? null : Company::find()->alias('c')->innerJoin([
                'ec' => EmployeeCompany::tableName(),
            ], "{{ec}}.[[company_id]] = {{c}}.[[id]]")->andWHere([
                'c.test' => false,
                'c.blocked' => false,
                'ec.employee_id' => $contact['id'],
                'ec.is_working' => true,
            ])->orderBy([
                'EXISTS(SELECT * FROM {{service_payment}} {{sp}} WHERE {{sp}}.[[company_id]] = {{c}}.[[id]])' => SORT_DESC,
                'ec.created_at' => SORT_ASC,
            ])->one();

            if ($company === null) {
                $delete = true;
            }

            if ($delete) {
                $data['data'][] = [
                    $contact['email'],
                    1,
                    $listId, // implode(',', $this->getSendList()),
                    self::STATUS_INACTIVE,
                    $contact['firstname'],
                    '',
                    '',
                    0,
                    0,
                    0,
                    '',
                    '',
                    '',
                    '',
                    '',
                ];
            } else {
                $tarif = $company->getHasActualSubscription() ?
                        $company->activeSubscribe->getTariffName() :
                        ($company->isFreeTariff ? 'Тариф "Бесплатно"' : 'Нет');
                $activationType = $company->activation_type;

                $data['data'][] = [
                    $contact['email'],
                    (int) $delete,
                    $listId, // implode(',', $this->getSendList()),
                    (bool) $contact['notify_new_features'] ? self::STATUS_ACTIVE : self::STATUS_INACTIVE,
                    $contact['firstname'],
                    $company->shortName,
                    $contact['last_visit_at'] ? date('d.m.Y', $contact['last_visit_at']) : '',
                    $activationType == Company::ACTIVATION_EMPTY_PROFILE ? 1 : 0,
                    $activationType == Company::ACTIVATION_NO_INVOICE ? 1 : 0,
                    $activationType == Company::ACTIVATION_NOT_SEND_INVOICES ? 1 : 0,
                    $contact['created_at'] ? date('d.m.Y', $contact['created_at']) : '',
                    $tarif,
                    ArrayHelper::getValue($company, 'mainCheckingAccountant.bank_name', ''),
                    ArrayHelper::getValue($company, 'companyType.name_short', ''),
                    ArrayHelper::getValue($company, 'companyTaxationType.name', ''),
                ];
            }
        }

        $this->importContacts($data);

        return $this;
    }

    /**
     * @param array $emails
     * @param integer $listId
     * @return $this
     */
    public function clearHtmlContent($emails = [], $listId)
    {
        $offset = 0;
        while ($emailArray = array_slice($emails, $offset++ * self::IMPORT_ROWS_LIMIT, self::IMPORT_ROWS_LIMIT)) {
            $data = [
                'field_names' => [
                    'email',
                    'email_list_ids',
                    'html_content',
                ],
                'data' => [],
            ];

            foreach ($emailArray as $email) {
                $data['data'][] = [
                    is_array($email) ? ($email['email'] ?? '') : strval($email),
                    $listId,
                    '',
                ];
            }

            $this->importContacts($data);
        }
    }

    /**
     * Send email
     *
     * @return bool
     * @throws Exception
     */
    public function send($import = true, $all = false)
    {
        $this->lastError = $this->lastWarnings = $this->lastResult = null;

        if (ArrayHelper::getValue(Yii::$app->params, 'uniSender.reallySend', false)) {
            $contacts = $this->getContacts();

            if (!is_array($contacts) || !sizeof($contacts) > 0) {
                $this->lastError = 'Contact list is empty.';

                return false;
            }

            if (!$messageId = $this->createSendTemplate()) {
                $this->lastError = 'Email template not found';

                return false;
            }


            if ($import) {
                $timeLimit = $this->inactiveTimeLimit();
                foreach ($contacts as $email) {
                    $employee = Employee::find()->alias('employee')->where([
                        'employee.email' => $email,
                        'employee.is_active' => Employee::ACTIVE,
                        'employee.is_deleted' => Employee::NOT_DELETED,
                        'employee.is_mailing_active' => true
                    ])->leftJoin([
                        'visit' => CompanyLastVisit::tableName(),
                    ], '{{visit}}.[[employee_id]]={{employee}}.[[id]]')->andWhere([
                        '>', 'visit.time', $timeLimit,
                    ])->one();

                    $this->importContact($employee);
                }
            }

            if ($all) {
                $this->lastResult = Json::decode($this->createCampaign([
                    'message_id' => $messageId,
                    'defer' => 1,
                    'track_read' => 1,
                    'track_links' => 1,
                ]));
            } else {
                $this->lastResult = Json::decode($this->createCampaign([
                    'message_id' => $messageId,
                    'contacts' => implode(',', $contacts),
                    'defer' => 1,
                    'track_read' => 1,
                    'track_links' => 1,
                ]));
            }

            if (isset($this->lastResult['error'])) {
                $this->lastError = $this->lastResult['error'];

                return false;
            }

            if (isset($this->lastResult['warnings'])) {
                $this->lastWarnings = $this->lastResult['warnings'];
            }

        }

        return true;
    }

    /**
     * @return bool
     */
    public function sendOneEmail()
    {
        $result = json_decode($this->sendEmail([
            'email' => reset($this->getContacts()),
            'sender_name' => $this->getFromName(),
            'sender_email' => $this->getFromEmail(),
            'subject' => $this->getSubject(),
            'body' => $this->getTemplateHtml(),
            'list_id' => $this->getListId(),
            'text_body' => $this->getTemplateText(),
        ]));

        if (isset($result->error)) {
            $this->lastError = $result->error;

            return false;
        }

        return true;
    }

    /**
     * Create email template
     *
     * @return null
     * @throws Exception
     */
    public function createSendTemplate()
    {
        $config = [
            'sender_name' => $this->getFromName(),
            'sender_email' => $this->getFromEmail(),
            'subject' => $this->getSubject(),
            'body' => $this->getTemplateHtml(),
            'list_id' => $this->getListId(),
            'text_body' => $this->getTemplateText(),
        ];

        if (!empty($this->_attachments)) {
            $config['attachments'] = $this->_attachments;
        }

        $result = json_decode($this->createEmailMessage($config));

        if (!isset($result->result) || !empty($result->result)) {
            if (isset($result->message)) {
                throw new Exception($result->message);
            }
        }

        return isset($result->result) && isset($result->result->message_id) ? $result->result->message_id : null;
    }

    /**
     * Get html template
     *
     * @return array
     */
    public function getTemplateHtml()
    {
        $path = $this->getEmailTemplatePath() . '/' . $this->getEmailTemplateName() . '/html.php';

        return \Yii::$app->view->renderFile($path, array_merge($this->params, ['message' => $this]));
    }

    /**
     * Get text template
     *
     * @return string
     */
    public function getTemplateText()
    {
        $path = $this->getEmailTemplatePath() . '/' . $this->getEmailTemplateName() . '/text.php';

        return is_file(\Yii::getAlias($path)) ? \Yii::$app->view->renderFile($path, array_merge($this->params, ['message' => $this])) : false;
    }
}