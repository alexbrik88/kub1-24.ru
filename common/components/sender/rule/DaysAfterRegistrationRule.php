<?php
/**
 * Created by PhpStorm.
 * User: crearsdev
 * Date: 25.02.16
 * Time: 15:38
 */
namespace common\components\sender\rule;

/**
 * Class DaysAfterRegistrationRule
 * @package common\components\sender\rule
 */
class DaysAfterRegistrationRule extends BaseRule
{
    /**
     * Days after registration
     *
     * @var null
     */
    protected $days = null;

    /**
     * Set days
     *
     * @param $days
     * @return $this
     */
    public function setDays($days)
    {
        $this->days = $days;

        return $this;
    }

    /**
     * @inheritdoc
     */
    public function find()
    {
        $this->getQuery()->where('date(from_unixtime(`last_visit_at`)) = (CURDATE() - INTERVAL :day DAY) AND date(from_unixtime(`last_visit_at`)) = date(from_unixtime(`created_at`))', [':day' => $this->getParamByName('days')])
            ->andWhere('notify_new_features = :status', [':status' => 1]);

        return $this;
    }
}