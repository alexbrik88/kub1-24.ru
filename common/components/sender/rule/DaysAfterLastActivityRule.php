<?php
/**
 * Created by PhpStorm.
 * User: crearsdev
 * Date: 25.02.16
 * Time: 15:38
 */
namespace common\components\sender\rule;

/**
 * Class DaysAfterLastActivityRule
 * @package common\components\sender\rule
 */
class DaysAfterLastActivityRule extends BaseRule
{
    /**
     * @inheritdoc
     */
    public function find()
    {
        $this->getQuery()->where('date(from_unixtime(`last_visit_at`)) = (CURDATE() - INTERVAL :day DAY)', [':day' => $this->getParamByName('days')])
            ->andWhere('notify_new_features = :status', [':status' => 1]);

        return $this;
    }
}