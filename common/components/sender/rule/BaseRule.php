<?php
/**
 * Created by PhpStorm.
 * User: crearsdev
 * Date: 25.02.16
 * Time: 13:58
 */
namespace common\components\sender\rule;

use common\models\employee\Employee;
use yii\base\Exception;

/**
 * Class BaseRule
 * @package common\models\sender\rule
 */
abstract class BaseRule
{
    /**
     * Employee Query
     *
     * @var null
     */
    protected $query = null;

    /**
     * Additional Params
     *
     * @var array
     */
    protected $params = [];

    /**
     * Find users by rule
     *
     * @return \common\components\sender\rule\BaseRule
     */
    public abstract function find();

    /**
     * Get called class name
     * @return string
     */
    public static function getClassName()
    {
        return get_called_class();
    }

    /**
     * Set additional params
     *
     * @param array $params
     * @return $this
     */
    public function setParams($params = [])
    {
        $this->params = $params;

        return $this;
    }

    /**
     * Get additional params
     *
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

    /**
     * Get param by name
     *
     * @param $name
     * @return null
     * @throws Exception
     */
    public function getParamByName($name)
    {
        $paramValue = null;

        if (is_array($this->params) && isset($this->params[$name])) {
            $paramValue = $this->params[$name];
        }

        return $paramValue;
    }

    /**
     * Get Employee query
     *
     * @return \common\models\employee\EmployeeQuery
     */
    protected function getQuery()
    {
        if (is_null($this->query)) {
            $this->query = Employee::find()->select('email')->isActual();
        }

        return $this->query;
    }

    /**
     * Get users
     *
     * @return array|\yii\db\ActiveRecord[]
     */
    public function getUsers()
    {
        return $this->getQuery()->asArray()->all();
    }
}