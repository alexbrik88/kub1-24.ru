<?php
/**
 * Created by PhpStorm.
 * User: crearsdev
 * Date: 25.02.16
 * Time: 13:53
 */
namespace common\components\sender\rule;

/**
 * Class AfterRegistrationRule
 * @package common\components\sender\rule
 */
class AfterRegistrationRule extends BaseRule
{
    /**
     * @inheritdoc
     */
    public function find()
    {
        $this->getQuery()->where('date(from_unixtime(`created_at`)) = CURDATE()');

        return $this;
    }
}