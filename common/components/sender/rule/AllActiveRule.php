<?php
/**
 * Created by PhpStorm.
 * User: crearsdev
 * Date: 25.02.16
 * Time: 13:53
 */
namespace common\components\sender\rule;

/**
 * Class AllActiveRule
 * @package common\components\sender\rule
 */
class AllActiveRule extends BaseRule
{
    /**
     * @inheritdoc
     */
    public function find()
    {
        $this->getQuery();

        return $this;
    }
}