<?php
/**
 * Created by PhpStorm.
 * User: crearsdev
 * Date: 25.02.16
 * Time: 15:38
 */
namespace common\components\sender\rule;

/**
 * Class EveryMondayRule
 * @package common\components\sender\rule
 */
class EveryMondayRule extends BaseRule
{
    /**
     * @inheritdoc
     */
    public function find()
    {
        $this->getQuery()->where('date(from_unixtime(`last_visit_at`)) >= (NOW() - INTERVAL :day DAY)', [':day' => $this->getParamByName('days')])
            ->andWhere('notify_nearly_report = :report', [':report' => 1]);

        return $this;
    }
    /**
     * @inheritdoc
     */
    public function andWhere($params = [])
    {
        $this->getQuery()->andWhere($params);

        return $this;
    }
}