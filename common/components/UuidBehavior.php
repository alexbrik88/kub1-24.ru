<?php

namespace common\components;

use yii\base\Behavior;
use yii\db\ActiveRecord;

/**
 * Поведение для хранения UUID в базе данных
 * Сжимает UUID в указанных в $this->$column атрибутах при сохранении записи в БД.
 *
 * @property string|null  $UUID
 * @property ActiveRecord $owner
 */
class UuidBehavior extends Behavior
{

    /** @var string[] Имена полей/столбцов базы данных */
    public $column = ['id'];

    /**
     * Преобразует UUID в binary-строку
     *
     * @param $value
     * @return bool|string|null
     */
    public static function UUID2Binary($value)
    {
        if ($value === null) {
            return null;
        }
        if (strlen($value) === 36) {
            $value = hex2bin(str_replace('-', '', $value));
        }
        return $value;
    }

    public function events()
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'beforeSave',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'beforeSave',
        ];
    }

    /**
     * Преобразует UUID в binary-строку
     *
     * @param string $attribute
     * @return string|null
     */
    public function getBinary(string $attribute = 'id')
    {
        return self::UUID2Binary($this->owner->$attribute);
    }

    /**
     * Восстанавливает UUID из binary
     *
     * @param string $attribute
     * @return string|null
     */
    public function getUUID(string $attribute = 'id')
    {
        $value = $this->owner->$attribute;
        if ($value === null || strlen($value) === 36) {
            return $value;
        }
        $value = unpack("H*", $value);
        $value = strtoupper(preg_replace("/([0-9a-f]{8})([0-9a-f]{4})([0-9a-f]{4})([0-9a-f]{4})([0-9a-f]{12})/", "$1-$2-$3-$4-$5", $value["1"]));
        return $value;
    }

    /**
     * Преобразовать к BINARY(16), если это ещё не сделано
     */
    public function beforeSave()
    {
        foreach ($this->column as $item) {
            $this->owner->$item = $this->getBinary($item);
        }
    }

}