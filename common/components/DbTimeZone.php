<?php

namespace common\components;

use DateTime;
use Yii;

/**
 * Class DbTimeZone
 */
class DbTimeZone extends \yii\base\Component
{
    /**
     * sync db session timezone with php
     */
    public static function sync()
    {
        $date = new DateTime();

        Yii::$app->db->createCommand("
            SET time_zone='{$date->format('P')}';
        ")->execute();
    }
}
