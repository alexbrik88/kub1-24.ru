<?php
namespace common\components;

use common\components\TextHelper;
use common\models\cash\CashBankFlows;
use common\models\cash\CashOrderFlows;
use common\models\cash\CashFlowsBase;
use common\models\Company;
use common\models\company\CompanyFirstEvent;
use common\models\company\CompanyTaxationType;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\document\InvoiceExpenditureGroup;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\PaymentDetails;
use common\models\document\PaymentOrder;
use common\models\document\PaymentType;
use common\models\document\TaxpayersStatus;
use common\models\document\status\PaymentOrderStatus;
use common\models\employee\Employee;
use common\models\reference\FixedPaymentsIp;
use common\models\service\Payment;
use common\models\service\TaxrobotPaymentOrder;
use common\models\service\SubscribeTariffGroup;
use frontend\models\log\LogHelper;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\modules\cash\models\CashContractorType;
use DateTime;
use frontend\modules\tax\models\TaxDeclaration;
use Yii;
use yii\data\ActiveDataProvider;
use yii\data\SqlDataProvider;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/**
 * Class Cropper
 */
class TaxRobotHelper extends \yii\base\Model
{
    const TYPE_BANK = 'bank';
    const TYPE_ORDER = 'order';

    public static $serviceName = 'Бухгалтерия ИП на УСН 6%';
    public static $onePercentOver = 300*1000*100;

    public static $typeList = [
        self::TYPE_BANK => 'Банк',
        self::TYPE_ORDER => 'Касса',
    ];
    public static $modelClassList = [
        self::TYPE_BANK => CashBankFlows::class,
        self::TYPE_ORDER => CashOrderFlows::class,
    ];
    public static $kbkExpense = [
        '18210202140061110160',
        '18210202103081013160',
    ];
    public static $kbkUsn6 = '18210501011011000110';
    public static $kbkOver300 = '18210202140061110160';
    public static $kbkPfr = '18210202140061110160';
    public static $kbkOms = '18210202103081013160';
    public static $titleUsn6 = 'Платеж по налогу УСН Доходы за {period}';
    public static $titleOver300 = '1% от дохода свыше {amount} ₽ за {period}';
    public static $titlePfr = 'Фиксированный взнос на пенсионное страхование за {period}';
    public static $titleOms = 'Фиксированный взнос на медицинское страхование за {period}';
    public static $purposeUsn6 = 'Налог за {period}, взимаемый с налогоплательщиков, выбравших в качестве объекта налогообложения доходы';
    public static $purposeOver300 = 'Страховые взносы с дохода свыше {amount} руб. за {year} год';
    public static $purposePfr = 'Страховые взносы на обязательное пенсионное страхование в фиксированном размере за {year} год';
    public static $purposeOms = 'Страховые взносы на обязательное медицинское страхование за {year} год';
    protected static $q = [1,2,3,4];

    public $payment_type;
    public $contractor_id;
    public $is_taxable;
    public $expenditure_item_id;

    protected $_company;
    protected $_employee;
    protected $_quarter;
    protected $_year;
    protected $_period;
    protected $_defaultPeriod;
    protected $_redirectAction = 'bank';
    protected $_periodArray;

    private $_data = [];

    // Исключаемые контрагенты
    public static $excludeContractorType = [
        'balance' // Баланс начальный
    ];

    // Исключаемые статьи прихода
    public static $excludeItems = [
        2, // Взнос наличными
        3, // Возврат
        4, // Займ
        5, // Кредит
        6, // От учредителя
        9, // Перевод собственных средств
        10, // Возврат займа
        13, // Возврат средств из бюджета
        14, // Обеспечительный платёж
    ];
    // Включаемые статьи расхода (ПФР, ОМС)
    public static $includeExpenditureItems = [
        28, // Страховые взносы за ИП
        46, // Фиксированный платеж в ОМС
        53, // Фиксированный платеж в ПФР
    ];

    /**
     * Constructor.
     * @param Company $company
     * @param array $config name-value pairs that will be used to initialize the object properties
     */
    public function __construct(Company $company, Employee $employee = null, $config = [])
    {
        $this->_company = $company;
        $this->_employee = $employee;
        $this->_redirectAction = [
            'bank',
            'reduce' => Yii::$app->request->get('reduce'),
        ];

        parent::__construct($config);
    }

    /**
     * @return array validation rules
     */
    public function rules()
    {
        return [
            [['payment_type', 'contractor_id', 'is_taxable', 'expenditure_item_id'], 'safe'],
        ];
    }

    /**
     * @return string the form name of this model class.
     */
    public function formName()
    {
        return 'TaxRobot';
    }


    /**
     * @param  integer $payment_type
     * @return string
     */
    public static function typeName($payment_type)
    {
        return ArrayHelper::getValue(self::$typeList, $payment_type);
    }

    /**
     * @inheritdoc
     */
    public function setPeriod($id)
    {
        list($this->_year, $this->_quarter) = explode('_', $id);

        if (isset($this->getPeriodArray()[$id])) {
            $this->_period = (object) $this->getPeriodArray()[$id];
        } else {
            $this->_period = (object) [
                'id' => $id,
                'label' => "",
                'shortLabel' => "",
                'dateFrom' => date_create("{$this->_year}-01-01 00:00:00.000000"),
                'dateTill' => date_create("{$this->_year}-12-31 23:59:59.999999"),
            ];
        }
    }

    /**
     * @return stdClass
     */
    public function getPeriod()
    {
        if ($this->_period === null) {
            $this->_period = (object) $this->getDefaultPeriod();
        }

        return $this->_period;
    }

    /**
     * @return stdClass
     */
    public function getUsn6Period()
    {
        if (!isset($this->_data['getUsn6Period'])) {
            $period = clone $this->getPeriod();
            $period->quarter = $this->getQuarter();
            $q = $period->quarter - 1;
            if ($q > 0) {
                $m = $q * 3;
                $period->dateFrom->modify("+$m month");
            }
            $this->_data['getUsn6Period'] = $period;
        }

        return $this->_data['getUsn6Period'];
    }

    /**
     * @return stdClass
     */
    public function getOver300LastPeriod()
    {
        if (!isset($this->_data['getOver300LastPeriod'])) {
            $year = $this->getYear() - 1;
            $this->_data['getOver300LastPeriod'] = (object) [
                'year' => $year,
                'label' => "{$year}&nbsp;год",
                'dateFrom' => date_create("{$year}-01-01 00:00:00.000000"),
                'dateTill' => date_create("{$year}-12-31 23:59:59.999999"),
            ];
        }

        return $this->_data['getOver300LastPeriod'];
    }

    /**
     * @return stdClass
     */
    public function getOver300ThisPeriod()
    {
        if (!isset($this->_data['getOver300ThisPeriod'])) {
            $year = $this->getYear();
            $this->_data['getOver300ThisPeriod'] = (object) [
                'year' => $year,
                'label' => "{$year}&nbsp;год",
                'dateFrom' => clone $this->getPeriod()->dateFrom,
                'dateTill' => clone $this->getPeriod()->dateTill,
            ];
        }

        return $this->_data['getOver300ThisPeriod'];
    }

    /**
     * @return stdClass
     */
    public function getPfrPeriod()
    {
        if (!isset($this->_data['getPfrPeriod'])) {
            $year = $this->getYear();
            $this->_data['getPfrPeriod'] = (object) [
                'year' => $year,
                'label' => "{$year}&nbsp;год",
                'dateFrom' => date_create("{$year}-01-01 00:00:00.000000"),
                'dateTill' => date_create("{$year}-12-31 23:59:59.999999"),
            ];
        }

        return $this->_data['getPfrPeriod'];
    }

    /**
     * @return stdClass
     */
    public function getOmsPeriod()
    {
        return $this->getPfrPeriod();
    }

    /**
     * @return stdClass
     */
    public function getUsn6PeriodCode($quarter = null)
    {
        $q = intval($quarter ? : $this->getQuarter());

        return "КВ.0{$q}.{$this->getYear()}";
    }

    /**
     * @return stdClass
     */
    public function getOver300LastPeriodCode()
    {
        return 'ГД.00.' . $this->getOver300LastPeriod()->year;
    }

    /**
     * @return stdClass
     */
    public function getOver300ThisPeriodCode()
    {
        return 'ГД.00.' . $this->getOver300ThisPeriod()->year;
    }

    /**
     * @return stdClass
     */
    public function getPfrPeriodCode()
    {
        return 'ГД.00.' . $this->getPfrPeriod()->year;
    }

    /**
     * @return stdClass
     */
    public function getOmsPeriodCode()
    {
        return 'ГД.00.' . $this->getOmsPeriod()->year;
    }

    /**
     * @return Company
     */
    public function getCompany()
    {
        return $this->_company;
    }

    /**
     * @return Company
     */
    public function getEmployee()
    {
        return $this->_employee;
    }

    /**
     * @return integer
     */
    public function getYear()
    {
        if ($this->_year === null) {
            list($this->_year, $this->_quarter) = explode('_', $this->getPeriod()->id);
        }

        return $this->_year;
    }

    /**
     * @return integer
     */
    public function getQuarter()
    {
        if ($this->_quarter === null) {
            list($this->_year, $this->_quarter) = explode('_', $this->getPeriod()->id);
        }

        return $this->_quarter;
    }

    /**
     * @return DateTime
     */
    public function getDateFrom()
    {
        return clone $this->getPeriod()->dateFrom;
    }

    /**
     * @return DateTime
     */
    public function getDateTill()
    {
        return clone $this->getPeriod()->dateTill;
    }

    /**
     * @return integer
     */
    public function getIsPaid()
    {
        if (ArrayHelper::getValue(Yii::$app->params, 'tax-robot-paid', false)) {
            return true;
        }

        return $this->company->getHasActualSubscription(SubscribeTariffGroup::TAX_IP_USN_6) ||
               $this->company->getHasActualSubscription(SubscribeTariffGroup::TAX_DECLAR_IP_USN_6);
    }

    /**
     * @return integer
     */
    public function getIsDeclarationPaid()
    {
        if (ArrayHelper::getValue(Yii::$app->params, 'tax-declaration-paid', false)) {
            return true;
        }

        return $this->company->getHasActualSubscription(SubscribeTariffGroup::TAX_DECLAR_IP_USN_6);
    }

    /**
     * @return integer
     */
    public function actionIsAllowed($action)
    {
        $setFlash = ($action === 'index') ? false : true;
        //$setFlash = true;

        if ($action == 'company') {
            return true;
        }

        $this->company->scenario = Company::SCENARIO_TAX_ROBOT;
        if (!$this->company->validate()) {
            $this->_redirectAction = [
                'company',
                'period' => $this->getUrlPeriodId(),
            ];
            if ($setFlash) {
                Yii::$app->session->setFlash('error', "Необходимо заполнить реквизиты.");
            }
            return false;
        }
        if ($action == 'params') {
            return true;
        }
        if (!$this->company->companyTaxationType->validate()) {
            $this->_redirectAction = [
                'params',
                'period' => $this->getUrlPeriodId(),
            ];
            if ($setFlash) {
                Yii::$app->session->setFlash('error', "Необходимо указать параметры вашего ИП.");
            }
            return false;
        }
        if (!Yii::$app->request->cookies['tax_robot_accepted']) {
            $this->_redirectAction = [
                'params',
                'period' => $this->getUrlPeriodId(),
            ];
            if ($setFlash) {
                Yii::$app->session->setFlash('error', "Необходимо подтвердить данные по вашему ИП.");
            }
            return false;
        }

        if ($action == 'index') {
            return false;
        }
        $freeActions = [
            'error',
            'bank',
            'manual-entry',
            'taxable',
            'delete',
            'create-movement',
            'update-movement',
            'pay',
            'change-declaration-date',
            'add-modal-contractor',
        ];
        if (in_array($action, $freeActions)) {
            return true;
        }
        $isDeclaration = strpos(Yii::$app->controller->getRoute(), 'declaration') !== false;
        // Null declaration
        if ($isDeclaration && Yii::$app->request->get('empty')) {
            return true;
        }

        if ($isDeclaration) {
            if ($this->getIsDeclarationPaid()) {
                return true;
            } else {
                if ($setFlash) {
                    $name = SubscribeTariffGroup::find()->select('name')->where([
                        'id' => SubscribeTariffGroup::TAX_DECLAR_IP_USN_6,
                    ])->scalar();
                    Yii::$app->session->setFlash('error', "Необходимо оплатить услугу \"{$name}\".");
                }
            }
        } else {
            if ($this->getIsPaid()) {
                return true;
            } else {
                if ($setFlash) {
                    $name1 = SubscribeTariffGroup::find()->select('name')->where([
                        'id' => SubscribeTariffGroup::TAX_DECLAR_IP_USN_6,
                    ])->scalar();
                    $name2 = SubscribeTariffGroup::find()->select('name')->where([
                        'id' => SubscribeTariffGroup::TAX_IP_USN_6,
                    ])->scalar();
                    Yii::$app->session->setFlash('error', "Необходимо оплатить услугу \"{$name1}\" или \"{$name2}\".");
                }
            }
        }
        $this->_redirectAction = [
            'bank',
            'period' => $this->getUrlPeriodId(),
        ];

        Yii::$app->session->set('taxrobot_show_pay_popup', true);

        return false;
    }

    /**
     * @return string
     */
    public function getRedirectAction()
    {
        return $this->_redirectAction;
    }

    /**
     * @return integer
     */
    public function getAuthor()
    {
        return $this->_employee ? : (
            Yii::$app->user->identity instanceof Employee ?
            Yii::$app->user->identity :
            $this->company->employeeChief
        );
    }

    public function getUsn6Title($period)
    {
        return Yii::$app->getI18n()->format(self::$titleUsn6, [
            'period' => $period ? : $this->getPeriodLabel(),
        ], Yii::$app->language);
    }

    public function getOver300Title($period)
    {
        return Yii::$app->getI18n()->format(self::$titleOver300, [
            'amount' => number_format(self::$onePercentOver/100, 0, '.', ' '),
            'period' => $period,
        ], Yii::$app->language);
    }

    public function getPfrTitle($period)
    {
        return Yii::$app->getI18n()->format(self::$titlePfr, [
            'period' => $period,
        ], Yii::$app->language);
    }

    public function getOmsTitle($period)
    {
        return Yii::$app->getI18n()->format(self::$titleOms, [
            'period' => $period,
        ], Yii::$app->language);
    }

    public function getUsn6Purpoce($period = null)
    {
        return Yii::$app->getI18n()->format(self::$purposeUsn6, [
            'period' => $period ? : $this->getPeriodLabel(),
        ], Yii::$app->language);
    }

    public function getOver300LastPurpoce($year = null)
    {
        return Yii::$app->getI18n()->format(self::$purposeOver300, [
            'amount' => number_format(self::$onePercentOver/100, 0, '.', ' '),
            'year' => $year ? : $this->getOver300LastPeriod()->year,
        ], Yii::$app->language);
    }

    public function getOver300ThisPurpoce($year = null)
    {
        return Yii::$app->getI18n()->format(self::$purposeOver300, [
            'amount' => number_format(self::$onePercentOver/100, 0, '.', ' '),
            'year' => $year ? : $this->getOver300ThisPeriod()->year,
        ], Yii::$app->language);
    }

    public function getPfrPurpoce($year = null)
    {
        return Yii::$app->getI18n()->format(self::$purposePfr, [
            'year' => $year ? : $this->getPfrPeriod()->year,
        ], Yii::$app->language);
    }

    public function getOmsPurpoce($year = null)
    {
        return Yii::$app->getI18n()->format(self::$purposeOms, [
            'year' => $year ? : $this->getOmsPeriod()->year,
        ], Yii::$app->language);
    }

    public function getLastTaxRobot()
    {
        if (!isset($this->_data['getLastTaxRobot'])) {
            $taxRobot = new TaxRobotHelper($this->_company, $this->_employee);
            $taxRobot->setPeriod(($this->getYear() - 1) . '_4');
            $this->_data['getLastTaxRobot'] = $taxRobot;
        }

        return $this->_data['getLastTaxRobot'];
    }

    public function findFlowsPaymentOrders()
    {
        $year = $this->getYear();
        $quarter = $this->getQuarter();
        $flowsArray = array_merge(
            $this->company->getCashBankFlows()->andWhere([
                'kbk' => [
                    self::$kbkUsn6,
                    self::$kbkOms,
                    self::$kbkPfr,
                ],
                'is_taxable' => true,
            ])->andWhere(['like', 'tax_period_code', $year])->all(),
            $this->company->getCashBankFlows()->andWhere([
                'kbk' => [
                    self::$kbkUsn6,
                    self::$kbkOms,
                    self::$kbkPfr,
                ],
                'is_taxable' => true,
            ])->andWhere(['like', 'tax_period_code', $year-1])->all()
        );
        foreach ($flowsArray as $key => $flow) {
            $flow->findPaymentOrder();
        }
    }

    public function processPeriodPayments()
    {
        $this->findFlowsPaymentOrders();

        if ($this->getQuarter() == 1) {
            $this->getLastTaxRobot()->createUsn6PaymentOrder();
        }
        $this->createUsn6PaymentOrder();
        $this->createOver300LastPaymentOrder();
        $this->createOver300ThisPaymentOrder();
        $this->createPfrPaymentOrder();
        $this->createOmsPaymentOrder();
    }

    /**
     * @return PaymentOrder
     */
    public function getNewBudgetPaymentOrderModel()
    {
        $company = $this->company;
        $ifns = $company->ifns;
        $account = $company->mainCheckingAccountant;

        return new PaymentOrder([
            'company_id' => $company->id,
            'company_name' => $company->companyType->name_short . ' ' . $company->name_full,
            'company_inn' => $company->inn,
            'company_kpp' => strlen($company->kpp) === 9 ? $company->kpp : 0,
            'company_bik' => $account ? $account->bik : null,
            'company_bank_name' => $account ? $account->bank_name : null,
            'company_ks' => $account ? $account->ks : null,
            'company_rs' => $account ? $account->rs : null,
            'document_author_id' => $this->author->id,
            'document_number' => PaymentOrder::getNextDocumentNumber($company),
            'document_date' => date('Y-m-d'),
            'relation_with_in_invoice' => 0,
            'contractor_name' => $ifns ? $ifns->contractorName : '',
            'contractor_inn' => $ifns ? $ifns->g6 : '',
            'contractor_kpp' => $ifns ? $ifns->g7 : '',
            'contractor_bank_name' => $ifns ? $ifns->g8 : '',
            'contractor_bik' => $ifns ? $ifns->g9 : '',
            'contractor_current_account' => $ifns ? $ifns->g11 : '',
            'contractor_corresponding_account' => $ifns ? $ifns->g10 : '',
            'ranking_of_payment' => 5,
            'operation_type_id' => 1,
            'presence_status_budget_payment' => 1,
            'taxpayers_status_id' => $company->getIsLikeIP() ? TaxpayersStatus::DEFAULT_IP : TaxpayersStatus::DEFAULT_OOO,
            'oktmo_code' => $company->oktmo ? : ($company->okato ? : 0),
            'payment_details_id' => PaymentDetails::DETAILS_1,
            'document_number_budget_payment' => "0",
            'document_date_budget_payment' => date('Y-m-d'),
            'payment_type_id' => PaymentType::TYPE_0,
            'uin_code' => "0",
            'payment_limit_date' => '',
            'payment_order_status_id' => 1
        ]);
    }

    /**
     * @return PaymentOrder
     */
    public function createUsn6PaymentOrder($quarter = null)
    {
        $q = intval($quarter ? : $this->getQuarter());
        if ($q > 1) {
            $this->createUsn6PaymentOrder($q-1);
        }

        $period_code = $this->getUsn6PeriodCode($q);

        $existingModels = PaymentOrder::find()->andWhere([
            'company_id' => $this->company->id,
            'kbk' => self::$kbkUsn6,
            'tax_period_code' => $period_code,
        ])->andWhere(['not', [
            'payment_order_status_id' => PaymentOrderStatus::STATUS_PAID,
        ]])->orderBy([
            'document_date' => SORT_ASC,
        ])->all();

        if (($sum = $this->getUsn6RemainingAmount($q)) > 0) {

            $model = array_shift($existingModels);

            $label = self::periodLabelByYQ($this->getYear(), $q);
            if ($model === null) {
                $model = $this->getNewBudgetPaymentOrderModel();
                $model->sum = $sum;
                $model->sum_in_words = TextHelper::mb_ucfirst(trim(TextHelper::amountToWords($sum / 100)));
                $model->purpose_of_payment = $this->getUsn6Purpoce(str_replace("&nbsp;", " ", $label));
                $model->kbk = self::$kbkUsn6;
                $model->tax_period_code = $period_code;
                if (!LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE, function ($model) {
                    return $model->save(false);
                })) {
                    Yii::error(var_export($model->errors, true));
                }
            } elseif ($model->sum != $sum) {
                $model->document_date = date('Y-m-d');
                $model->sum = $sum;
                $model->sum_in_words = TextHelper::mb_ucfirst(trim(TextHelper::amountToWords($sum / 100)));
                $model->purpose_of_payment = $this->getUsn6Purpoce(str_replace("&nbsp;", " ", $label));
                if (!LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE, function ($model) {
                    return $model->save(false);
                })) {
                    Yii::error(var_export($model->errors, true));
                }
            }
        }

        if ($existingModels) {
            foreach ($existingModels as $model) {
                $model->delete();
            }
        }
    }

    /**
     * @return PaymentOrder
     */
    public function createOver300LastPaymentOrder()
    {
        $period_code = 'ГД.00.' . $this->getOver300LastPeriod()->year;

        $existingModels = PaymentOrder::find()->andWhere([
            'company_id' => $this->company->id,
            'kbk' => self::$kbkOver300,
            'tax_period_code' => $period_code,
        ])->andWhere(['not', [
            'payment_order_status_id' => PaymentOrderStatus::STATUS_PAID,
        ]])->andWhere(['like', 'purpose_of_payment', '300'])->orderBy([
            'sum_changed_by_user' => SORT_DESC,
            'document_date' => SORT_ASC,
        ])->all();

        if ($this->getQuarter() == 1 && ($sum = $this->getOver300LastRemainingAmount()) > 0) {
            $model = array_shift($existingModels);

            if ($model === null) {
                $model = $this->getNewBudgetPaymentOrderModel();
                $model->sum = $sum;
                $model->sum_in_words = TextHelper::mb_ucfirst(trim(TextHelper::amountToWords($sum / 100)));
                $model->purpose_of_payment = $this->getOver300LastPurpoce();
                $model->kbk = self::$kbkOver300;
                $model->tax_period_code = $period_code;
                if (!LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE, function ($model) {
                    return $model->save(false);
                })) {
                    Yii::error(var_export($model->errors, true));
                }
            } elseif ($model->sum != $sum) {
                if ($model->sum_changed_by_user && $model->sum < $sum) {
                    $sum -= $model->sum;
                    $model = array_shift($existingModels) ? : $this->getNewBudgetPaymentOrderModel();
                }
                $model->document_date = date('Y-m-d');
                $model->sum = $sum;
                $model->sum_in_words = TextHelper::mb_ucfirst(trim(TextHelper::amountToWords($sum / 100)));
                $model->purpose_of_payment = $this->getOver300LastPurpoce();
                $model->kbk = self::$kbkOver300;
                $model->tax_period_code = $period_code;
                if (!LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE, function ($model) {
                    return $model->save(false);
                })) {
                    Yii::error(var_export($model->errors, true));
                }
            }
        }

        if ($existingModels) {
            foreach ($existingModels as $model) {
                $model->delete();
            }
        }
    }

    /**
     * @return PaymentOrder
     */
    public function createOver300ThisPaymentOrder()
    {
        $period_code = 'ГД.00.' . $this->getOver300ThisPeriod()->year;

        $existingModels = PaymentOrder::find()->andWhere([
            'company_id' => $this->company->id,
            'kbk' => self::$kbkOver300,
            'tax_period_code' => $period_code,
        ])->andWhere(['not', [
            'payment_order_status_id' => PaymentOrderStatus::STATUS_PAID,
        ]])->andWhere(['like', 'purpose_of_payment', '300'])->orderBy([
            'sum_changed_by_user' => SORT_DESC,
            'document_date' => SORT_ASC,
        ])->all();

        if (($sum = $this->getOver300ThisRemainingAmount()) > 0) {
            $model = array_shift($existingModels);

            if ($model === null) {
                $model = $this->getNewBudgetPaymentOrderModel();
                $model->sum = $sum;
                $model->sum_in_words = TextHelper::mb_ucfirst(trim(TextHelper::amountToWords($sum / 100)));
                $model->purpose_of_payment = $this->getOver300ThisPurpoce();
                $model->kbk = self::$kbkOver300;
                $model->tax_period_code = $period_code;
                if (!LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE, function ($model) {
                    return $model->save(false);
                })) {
                    Yii::error(var_export($model->errors, true));
                }
            } elseif ($model->sum != $sum) {
                if ($model->sum_changed_by_user && $model->sum < $sum) {
                    $sum -= $model->sum;
                    $model = array_shift($existingModels) ? : $this->getNewBudgetPaymentOrderModel();
                }
                $model->document_date = date('Y-m-d');
                $model->sum = $sum;
                $model->sum_in_words = TextHelper::mb_ucfirst(trim(TextHelper::amountToWords($sum / 100)));
                $model->purpose_of_payment = $this->getOver300ThisPurpoce();
                $model->kbk = self::$kbkOver300;
                $model->tax_period_code = $period_code;
                if (!LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE, function ($model) {
                    return $model->save(false);
                })) {
                    Yii::error(var_export($model->errors, true));
                }
            }
        }

        if ($existingModels) {
            foreach ($existingModels as $model) {
                $model->delete();
            }
        }
    }

    /**
     * @return PaymentOrder
     */
    public function createPfrPaymentOrder()
    {
        $period_code = 'ГД.00.' . $this->getPfrPeriod()->year;

        $existingModels = PaymentOrder::find()->andWhere([
            'company_id' => $this->company->id,
            'kbk' => self::$kbkPfr,
            'tax_period_code' => $period_code,
        ])->andWhere(['not', [
            'payment_order_status_id' => PaymentOrderStatus::STATUS_PAID,
        ]])->andWhere([
            'not',
            ['like', 'purpose_of_payment', '300'],
        ])->orderBy([
            'sum_changed_by_user' => SORT_DESC,
            'document_date' => SORT_ASC,
        ])->all();

        if (($sum = $this->getPfrRemainingAmount()) > 0) {
            $model = array_shift($existingModels);
            if ($model === null) {
                $model = $this->getNewBudgetPaymentOrderModel();
                $model->sum = $sum;
                $model->sum_in_words = TextHelper::mb_ucfirst(trim(TextHelper::amountToWords($sum / 100)));
                $model->purpose_of_payment = $this->getPfrPurpoce();
                $model->kbk = self::$kbkPfr;
                $model->tax_period_code = $period_code;
                if (!LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE, function ($model) {
                    return $model->save(false);
                })) {
                    Yii::error(var_export($model->errors, true));
                }
            } elseif ($model->sum != $sum) {
                if ($model->sum_changed_by_user && $model->sum < $sum) {
                    $sum -= $model->sum;
                    $model = array_shift($existingModels) ? : $this->getNewBudgetPaymentOrderModel();
                }
                $model->document_date = date('Y-m-d');
                $model->sum = $sum;
                $model->sum_in_words = TextHelper::mb_ucfirst(trim(TextHelper::amountToWords($sum / 100)));
                $model->purpose_of_payment = $this->getPfrPurpoce();
                $model->kbk = self::$kbkPfr;
                $model->tax_period_code = $period_code;
                if (!LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE, function ($model) {
                    return $model->save(false);
                })) {
                    Yii::error(var_export($model->errors, true));
                }
            }
        }

        if ($existingModels) {
            foreach ($existingModels as $model) {
                $model->delete();
            }
        }
    }

    /**
     * @return PaymentOrder
     */
    public function createOmsPaymentOrder()
    {
        $period_code = 'ГД.00.' . $this->getOmsPeriod()->year;

        $existingModels = PaymentOrder::find()->andWhere([
            'company_id' => $this->company->id,
            'kbk' => self::$kbkOms,
            'tax_period_code' => $period_code,
        ])->andWhere(['not', [
            'payment_order_status_id' => PaymentOrderStatus::STATUS_PAID,
        ]])->andWhere([
            'not',
            ['like', 'purpose_of_payment', '300'],
        ])->orderBy([
            'sum_changed_by_user' => SORT_DESC,
            'document_date' => SORT_ASC,
        ])->all();

        if (($sum = $this->getOmsRemainingAmount()) > 0) {
            $model = array_shift($existingModels);

            if ($model === null) {
                $model = $this->getNewBudgetPaymentOrderModel();
                $model->sum = $sum;
                $model->sum_in_words = TextHelper::mb_ucfirst(trim(TextHelper::amountToWords($sum / 100)));
                $model->purpose_of_payment = $this->getOmsPurpoce();
                $model->kbk = self::$kbkOms;
                $model->tax_period_code = $period_code;
                if (!LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE, function ($model) {
                    return $model->save(false);
                })) {
                    Yii::error(var_export($model->errors, true));
                }
            } elseif ($model->sum != $sum) {
                if ($model->sum_changed_by_user && $model->sum < $sum) {
                    $sum -= $model->sum;
                    $model = array_shift($existingModels) ? : $this->getNewBudgetPaymentOrderModel();
                }
                $model->document_date = date('Y-m-d');
                $model->sum = $sum;
                $model->sum_in_words = TextHelper::mb_ucfirst(trim(TextHelper::amountToWords($sum / 100)));
                $model->purpose_of_payment = $this->getOmsPurpoce();
                $model->kbk = self::$kbkOms;
                $model->tax_period_code = $period_code;
                if (!LogHelper::save($model, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE, function ($model) {
                    return $model->save(false);
                })) {
                    Yii::error(var_export($model->errors, true));
                }
            }
        }

        if ($existingModels) {
            foreach ($existingModels as $model) {
                $model->delete();
            }
        }
    }

    public function paymentOrderMaxSum(PaymentOrder $model)
    {
        $year = mb_substr($model->tax_period_code, -4);
        $period = mb_strtoupper(mb_substr($model->tax_period_code, 0, 2));
        $quarter = $period == 'КВ' ? mb_substr($model->tax_period_code, -6, 1) : 4;
        $taxPeriod = $year . '_' . $quarter;
        if (strlen($taxPeriod) == 6) {
            $taxRobot = new TaxRobotHelper($this->_company, $this->_employee);
            $taxRobot->setPeriod($taxPeriod);
            $existSum = 0;
            $remainingSum = null;
            $query = PaymentOrder::find()->andWhere([
                'company_id' => $model->company_id,
                'kbk' => $model->kbk,
                'tax_period_code' => $model->tax_period_code,
            ])->andWhere([
                'not',
                ['id' => $model->id],
            ])->andWhere([
                'not',
                ['payment_order_status_id' => PaymentOrderStatus::STATUS_PAID],
            ]);
            $isOver300 = mb_stripos($model->purpose_of_payment, '300') !== false;

            if ($model->kbk == self::$kbkUsn6) {
                $remainingSum = $taxRobot->getUsn6RemainingAmount();
            } elseif ($model->kbk == self::$kbkOms) {
                $existSum = (int) $query->sum('sum');
                $remainingSum = $taxRobot->getOmsRemainingAmount();
            } elseif ($model->kbk == self::$kbkPfr && !$isOver300) {
                $query->andWhere(['not', ['like', 'purpose_of_payment', '300']]);
                $existSum = (int) $query->sum('sum');
                $remainingSum = $taxRobot->getPfrRemainingAmount();
            } elseif ($model->kbk == self::$kbkOver300 && $isOver300) {
                $query->andWhere(['like', 'purpose_of_payment', '300']);
                $existSum = (int) $query->sum('sum');
                $remainingSum = $taxRobot->getOver300ThisRemainingAmount();
            }

            if ($remainingSum !== null) {
                $model->taxMaxSum = max(0, $remainingSum - $existSum);
            }
        }

        return $model->taxMaxSum;
    }

    public function getPaymentOrder()
    {
        $company = $this->company;

        $model = PaymentOrder::find()->andWhere([
            'company_id' => $company->id,
            'kbk' => self::$kbkUsn6,
            'tax_period_code' => $this->getPeriodString(),
        ])->andWhere(['not', [
            'payment_order_status_id' => PaymentOrderStatus::STATUS_PAID,
        ]])->one();
    }

    public static function periodLabel($periodId)
    {
        $period = explode('_', $periodId);
        if (count($period) === 2) {
            $Y = $period[0];
            $Q = $period[1];
            return self::periodLabelByYQ($Y, $Q);
        }

        return '';
    }

    public static function periodLabelByYQ($year, $q)
    {
        switch ($q) {
            case 1:
                return "1-й&nbsp;квартал {$year}&nbsp;года";
                break;

            case 2:
                return "1-е&nbsp;полугодие {$year}&nbsp;года";
                break;

            case 3:
                return "9-ть&nbsp;месяцев {$year}&nbsp;года";
                break;

            case 4:
            default:
                return "{$year}&nbsp;год";
                break;
        }
    }

    /**
     * @return array
     */
    public function getPeriodArray()
    {
        if ($this->_periodArray === null) {
            $currentYear = (int) date('Y');
            $lastYear = $currentYear -1;
            $prevYear = $currentYear -2;
            $periodArray = [
                "{$prevYear}_4" => [
                    'id' => "{$prevYear}_4",
                    'label' => "{$prevYear}&nbsp;год",
                    'shortLabel' => "{$prevYear}&nbsp;год",
                    'dateFrom' => date_create("{$prevYear}-01-01 00:00:00.000000"),
                    'dateTill' => date_create("{$prevYear}-12-31 23:59:59.999999"),
                ],
                "{$lastYear}_1" => [
                    'id' => "{$lastYear}_1",
                    'label' => "1-й&nbsp;квартал {$lastYear}&nbsp;года",
                    'shortLabel' => "1&nbsp;квартал {$lastYear}&nbsp;года",
                    'dateFrom' => date_create("{$lastYear}-01-01 00:00:00.000000"),
                    'dateTill' => date_create("{$lastYear}-03-31 23:59:59.999999"),
                ],
                "{$lastYear}_2" => [
                    'id' => "{$lastYear}_2",
                    'label' => "1-е&nbsp;полугодие {$lastYear}&nbsp;года",
                    'shortLabel' => "полугодие {$lastYear}&nbsp;года",
                    'dateFrom' => date_create("{$lastYear}-01-01 00:00:00.000000"),
                    'dateTill' => date_create("{$lastYear}-06-30 23:59:59.999999"),
                ],
                "{$lastYear}_3" => [
                    'id' => "{$lastYear}_3",
                    'label' => "9-ть&nbsp;месяцев {$lastYear}&nbsp;года",
                    'shortLabel' => "9&nbsp;месяцев {$lastYear}&nbsp;года",
                    'dateFrom' => date_create("{$lastYear}-01-01 00:00:00.000000"),
                    'dateTill' => date_create("{$lastYear}-09-30 23:59:59.999999"),
                ],
                "{$lastYear}_4" => [
                    'id' => "{$lastYear}_4",
                    'label' => "{$lastYear}&nbsp;год",
                    'shortLabel' => "{$lastYear}&nbsp;год",
                    'dateFrom' => date_create("{$lastYear}-01-01 00:00:00.000000"),
                    'dateTill' => date_create("{$lastYear}-12-31 23:59:59.999999"),
                ],
                "{$currentYear}_1" => [
                    'id' => "{$currentYear}_1",
                    'label' => "1-й&nbsp;квартал {$currentYear}&nbsp;года",
                    'shortLabel' => "1&nbsp;квартал {$currentYear}&nbsp;года",
                    'dateFrom' => date_create("{$currentYear}-01-01 00:00:00.000000"),
                    'dateTill' => date_create("{$currentYear}-03-31 23:59:59.999999"),
                ],
            ];
            $currentMonth = (int) date('n');
            if ($currentMonth > 3) {
                $periodArray["{$currentYear}_2"] = [
                    'id' => "{$currentYear}_2",
                    'label' => "1-е&nbsp;полугодие {$currentYear}&nbsp;года",
                    'shortLabel' => "полугодие {$currentYear}&nbsp;года",
                    'dateFrom' => date_create("{$currentYear}-01-01 00:00:00.000000"),
                    'dateTill' => date_create("{$currentYear}-06-30 23:59:59.999999"),
                ];
                if ($currentMonth > 6) {
                    $periodArray["{$currentYear}_3"] = [
                        'id' => "{$currentYear}_3",
                        'label' => "9-ть&nbsp;месяцев {$currentYear}&nbsp;года",
                        'shortLabel' => "9&nbsp;месяцев {$currentYear}&nbsp;года",
                        'dateFrom' => date_create("{$currentYear}-01-01 00:00:00.000000"),
                        'dateTill' => date_create("{$currentYear}-09-30 23:59:59.999999"),
                    ];
                    if ($currentMonth > 9) {
                        $periodArray["{$currentYear}_4"] = [
                            'id' => "{$currentYear}_4",
                            'label' => "{$currentYear}&nbsp;год",
                            'shortLabel' => "{$currentYear}&nbsp;год",
                            'dateFrom' => date_create("{$currentYear}-01-01 00:00:00.000000"),
                            'dateTill' => date_create("{$currentYear}-12-31 23:59:59.999999"),
                        ];
                    }
                }
            }
            krsort($periodArray);

            $this->_periodArray = $periodArray;
        }

        return $this->_periodArray;
    }

    /**
     * @return stdClass
     */
    public function getDefaultPeriod()
    {
        if ($this->_defaultPeriod === null) {
            $periodArray = $this->getPeriodArray();
            $month = (int) date('n');
            $this->_defaultPeriod = (object) current(array_slice($periodArray, ($month > 3 && $month < 5) ? 2 : 1, 1));
        }

        return $this->_defaultPeriod;
    }

    /**
     * @return string
     */
    public function getUrlPeriodId()
    {
        $id = $this->getPeriod()->id;

        return $this->getDefaultPeriod()->id == $id ? null : $id;
    }

    /**
     * @return array
     */
    public function getPeriodDropdownItems($step = 'bank')
    {
        $items = [];
        foreach ($this->getPeriodArray() as $period) {
            $items[] = [
                'label' => $period['label'],
                'url' => [
                    "/tax/robot/{$step}",
                    'period' => $period['id'] /*$period['id'] == $this->defaultPeriod->id ? null : $period['id']*/,
                ],
                'linkOptions' => ['class' => $period['id'] == $this->period->id ? 'active' : ''],
            ];
        }

        if (Yii::$app->request->get('empty')) {
            foreach ($items as &$item) {
                $item['url']['empty'] = 1;
            }
        }

        return $items;
    }

    /**
     * @return array
     */
    public function getTaxItems()
    {
        if (!isset($this->_data['getTaxItems'])) {
            $this->_data['getTaxItems'] = InvoiceExpenditureItem::find()->andWhere([
                'id' => [
                    InvoiceExpenditureItem::ITEM_TAX_USN_6,
                    InvoiceExpenditureItem::ITEM_INSURANCE_PAYMENT_IP,
                    InvoiceExpenditureItem::ITEM_PFR_FIXED_PAYMENT,
                    InvoiceExpenditureItem::ITEM_OMS_FIXED_PAYMENT,
                ],
                'group_id' => InvoiceExpenditureGroup::TAXES,
            ])->orderBy(['name' => SORT_ASC])->indexBy('id')->all();
        }

        return $this->_data['getTaxItems'];
    }

    /**
     * @return yii\db\ActiveQuery
     */
    public function getBankFlowsQuery($flow_type)
    {
        $query = $this->company->getCashBankFlows()->select([
            'id',
            'flow_type',
            'date',
            'recognition_date',
            'amount',
            'payment_type' => new \yii\db\Expression('"' . self::TYPE_BANK . '"'),
            '[[rs]] AS [[flow_name]]',
            'contractor_id',
            'description',
            'is_taxable',
            'expenditure_item_id',
        ])->andWhere([
            'flow_type' => $flow_type,
        ]);

        return $query;
    }

    /**
     * @return yii\db\ActiveQuery
     */
    public function getOrderFlowsQuery($flow_type)
    {
        $query = $this->company->getCashOrderFlows()->joinWith('cashbox cashbox', false)->select([
            'cash_order_flows.id',
            'flow_type',
            'date',
            'recognition_date',
            'amount',
            'payment_type' => new \yii\db\Expression('"' . self::TYPE_ORDER . '"'),
            '{{cashbox}}.[[name]] AS [[flow_name]]',
            'contractor_id',
            'description',
            'is_taxable',
            'expenditure_item_id',
        ])->andWhere([
            'cash_order_flows.is_accounting' => true,
            'flow_type' => $flow_type,
        ]);

        return $query;
    }

    /**
     * @return yii\db\Query
     */
    public function getIncomeSearchQuery()
    {
        $type = CashBankFlows::FLOW_TYPE_INCOME;
        $dateFrom = $this->dateFrom->format('Y-m-d');
        $dateTill = $this->dateTill->format('Y-m-d');

        $where = [
            'between',
            'recognition_date',
            $dateFrom,
            $dateTill,
        ];

        $whereContractorType = ['not', ['contractor_id' => self::$excludeContractorType]];

        $query = new Query;

        $bankQuery = $this->getBankFlowsQuery($type)->andWhere($where)->andWhere($whereContractorType);

        $orderQuery = $this->getOrderFlowsQuery($type)->andWhere($where)->andWhere($whereContractorType);

        $query->from(['t' => $bankQuery->union($orderQuery, true)]);

        return $query;
    }

    /**
     * @return yii\db\Query
     */
    public function getInsurancePaymentsQuery()
    {
        $dateFrom = $this->period->dateFrom->format('Y-m-d');
        $dateTill = $this->period->dateTill->format('Y-m-d');

        $query = $this->company->getCashBankFlows()->andWhere([
            'flow_type' => CashBankFlows::FLOW_TYPE_EXPENSE,
        ])->andWhere([
            'or',
            ['kbk' => self::$kbkExpense],
            [

                'and',
                [
                    'or',
                    ['kbk' => null],
                    ['kbk' => ''],
                ],
                ['expenditure_item_id' => self::$includeExpenditureItems],
            ]
        ])->andWhere(['between', 'recognition_date', $dateFrom, $dateTill]);

        return $query;
    }

    /**
     * @return array
     */
    public function getTaxAdvanceQuery()
    {
        $query = $this->company->getCashBankFlows()->andWhere([
            'flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE,
            'expenditure_item_id' => InvoiceExpenditureItem::ITEM_TAX_USN_6,
        ])->andWhere([
            'or',
            [
                'tax_period_code' => $this->getTaxPeriodCodeList(),
            ],
            [
                'and',
                [
                    'or',
                    ['tax_period_code' => null],
                    ['tax_period_code' => ''],
                ],
                [
                    'between',
                    'recognition_date',
                    $this->dateFrom->modify('+26 days')->format('Y-m-d'),
                    $this->dateTill->modify('+26 days')->format('Y-m-d'),
                ],
            ],
        ]);

        return $query;
    }

    /**
     * @return yii\db\Query
     */
    public function getExpenseSearchQuery()
    {
        $type = CashBankFlows::FLOW_TYPE_EXPENSE;
        $dateFrom = $this->period->dateFrom->format('Y-m-d');
        $dateTill = $this->period->dateTill->format('Y-m-d');
        $dateFromUsn = $this->dateFrom->modify('+26 days')->format('Y-m-d');
        $dateTillUsn = $dateTill;

        $query = new Query;

        $bankQuery = $this->getBankFlowsQuery($type)->andWhere([
            'or',
            [
                'and',
                [
                    'or',
                    ['kbk' => self::$kbkExpense],
                    [

                        'and',
                        [
                            'or',
                            ['kbk' => null],
                            ['kbk' => ''],
                        ],
                        ['expenditure_item_id' => self::$includeExpenditureItems],
                    ]
                ],
                ['between', 'recognition_date', $dateFrom, $dateTill],
            ],
            [
                'and',
                [
                    'or',
                    ['kbk' => self::$kbkUsn6],
                    [

                        'and',
                        [
                            'or',
                            ['kbk' => null],
                            ['kbk' => ''],
                        ],
                        ['expenditure_item_id' => InvoiceExpenditureItem::ITEM_TAX_USN_6],
                    ]
                ],
                [
                    'or',
                    ['tax_period_code' => $this->getTaxPeriodCodeList()],
                    [
                        'and',
                        [
                            'or',
                            ['tax_period_code' => null],
                            ['tax_period_code' => ''],
                        ],
                        ['between', 'recognition_date', $dateFromUsn, $dateTillUsn],
                    ]
                ],
            ],
        ]);

        $orderQuery = $this->getOrderFlowsQuery($type)->andWhere([
            'or',
            [
                'and',
                ['expenditure_item_id' => self::$includeExpenditureItems],
                ['between', 'recognition_date', $dateFrom, $dateTill],
            ],
            [
                'and',
                ['expenditure_item_id' => InvoiceExpenditureItem::ITEM_TAX_USN_6],
                ['between', 'recognition_date', $dateFromUsn, $dateTillUsn],
            ],
        ]);

        $query->from(['t' => $bankQuery->union($orderQuery, true)]);

        return $query;
    }

    /**
     * @return SqlDataProvider
     */
    public function getDataProvider(Query $query)
    {
        $dataProvider = new SqlDataProvider([
            'sql' => $query->select('*')->createCommand()->rawSql,
            'totalCount' => $query->count(),
            'sort' => [
                'attributes' => [
                    'date',
                    'amount',
                ],
                'defaultOrder' => [
                    'date' => SORT_DESC,
                ]
            ],
        ]);

        return $dataProvider;
    }

    /**
     * @return SqlDataProvider
     */
    public function getIncomeDataProvider($params = [])
    {
        $query = $this->getIncomeSearchQuery();

        if ($params) {
            $query->andFilterWhere([
                'payment_type' => ArrayHelper::getValue($params, 'payment_type'),
                'contractor_id' => ArrayHelper::getValue($params, 'contractor_id'),
                'is_taxable' => ArrayHelper::getValue($params, 'is_taxable'),
                'expenditure_item_id' => ArrayHelper::getValue($params, 'expenditure_item_id'),
            ]);
        }

        return $this->getDataProvider($query);
    }

    /**
     * @return boolean
     */
    public function getHasIncomeInThisYear()
    {
        if (!isset($this->_data['getHasIncomeInThisYear'])) {
            $taxRobot = new TaxRobotHelper($this->_company, $this->_employee);

            $taxRobot->setPeriod($this->getYear().'_4');

            $this->_data['getHasIncomeInThisYear'] = $taxRobot->getIncomeSearchQuery()->andWhere([
                'is_taxable' => true,
            ])->exists();
        }

        return $this->_data['getHasIncomeInThisYear'];
    }

    /**
     * @return SqlDataProvider
     */
    public function getExpenseDataProvider($params = [])
    {
        $query = $this->getExpenseSearchQuery();

        if ($params) {
            $query->andFilterWhere([
                'payment_type' => ArrayHelper::getValue($params, 'payment_type'),
                'contractor_id' => ArrayHelper::getValue($params, 'contractor_id'),
                'is_taxable' => ArrayHelper::getValue($params, 'is_taxable'),
                'expenditure_item_id' => ArrayHelper::getValue($params, 'expenditure_item_id'),
            ]);
        }

        return $this->getDataProvider($query);
    }

    /**
     * @return ActiveQuery
     */
    public function getNeedPaySearchQuery()
    {
        $y = $this->getYear();
        $q = $this->getQuarter();
        $query = $this->company->getPaymentOrders()->andWHere([
            'and',
            ['presence_status_budget_payment' => 1],
            ['not', ['payment_order_status_id' => PaymentOrderStatus::STATUS_PAID]],
        ])->andWhere([
            'kbk' => [
                self::$kbkUsn6,
                self::$kbkPfr,
                self::$kbkOver300,
                self::$kbkOms,
            ]
        ])->andWhere("CONCAT(SUBSTRING([[tax_period_code]], 7, 4), SUBSTRING([[tax_period_code]], 4, 2)) <= :period", [
            ':period' => "{$y}0{$q}",
        ]);

        return $query;
    }

    /**
     * @return array
     */
    public function getIncomeStatistic()
    {
        if (!isset($this->_data['getIncomeStatistic'])) {
            $query = $this->getIncomeSearchQuery(true);

            $query->select([
                'payment_type',
                'flow_name',
                'SUM([[amount]]) AS [[total_amount]]',
                'SUM(IF([[is_taxable]] = true, [[amount]], 0)) AS [[taxable_amount]]',
            ])->groupBy([
                'payment_type',
                'flow_name',
            ]);

            $this->_data['getIncomeStatistic'] = $query->all();
        }

        return $this->_data['getIncomeStatistic'];
    }

    /**
     * @return array
     */
    public function getExpenseStatistic()
    {
        if (!isset($this->_data['getExpenseStatistic'])) {
            $query = $this->getExpenseSearchQuery();

            $query->select([
                'SUM(IF([[is_taxable]] = true, [[amount]], 0)) AS [[taxable_amount]]',
                'expenditure_item_id',
            ])->groupBy([
                'expenditure_item_id',
            ])->indexBy('expenditure_item_id');

            $this->_data['getExpenseStatistic'] = $query->column();
        }

        return $this->_data['getExpenseStatistic'];
    }

    /**
     * @return Query
     */
    public function getTaxableAmountQuery($from, $till)
    {
        $where = [
            'and',
            ['flow_type' => CashFlowsBase::FLOW_TYPE_INCOME],
            ['is_taxable' => true],
            [
                'between',
                'recognition_date',
                $from,
                $till,
            ]
        ];
        $query = new Query;
        $bankQuery = $this->company->getCashBankFlows()->select('amount')->andWhere($where);
        $orderQuery = $this->company->getCashOrderFlows()->select('amount')->andWhere($where)->andWhere([
            'is_accounting' => true,
        ]);
        $query->from(['t' => $bankQuery->union($orderQuery, true)]);

        return $query;
    }

    /**
     * @return integer
     */
    public function getTaxableAmountByPeriod($year, $quarter = null, $summed = true)
    {
        if (!isset($this->_data['getTaxableAmountByPeriod'][$year])) {
            $sum = 0;
            $data = [];
            foreach (self::$q as $q) {
                $m = str_pad((string) $q*3-2, 2, "0", STR_PAD_LEFT);
                $dateFrom = date_create("{$year}-{$m}-01");
                $dateTill = (clone $dateFrom)->modify('last day of +2 month');
                $query = $this->getTaxableAmountQuery($dateFrom->format('Y-m-d'), $dateTill->format('Y-m-d'));
                $val = (int) $query->sum('amount');
                $sum += $val;
                $data[$q]['val'] = $val;
                $data[$q]['sum'] = $sum;
            }
            $this->_data['getTaxableAmountByPeriod'][$year] = $data;
        }

        $q = intval($quarter ? : $this->getQuarter());
        if (isset($this->_data['getTaxableAmountByPeriod'][$year][$q])) {
            return $this->_data['getTaxableAmountByPeriod'][$year][$q][$summed ? 'sum' : 'val'];
        }

        return 0;
    }

    /**
     * @return integer
     */
    public function getTaxableTotalAmount()
    {
        return $this->getTaxableAmountByPeriod($this->getYear(), $this->getQuarter());
    }

    /**
     * @return Query
     */
    public function getDuesAmountQuery($type, $from, $till)
    {
        $items = [
            'Over300' => InvoiceExpenditureItem::ITEM_INSURANCE_PAYMENT_IP,
            'Pfr' => InvoiceExpenditureItem::ITEM_PFR_FIXED_PAYMENT,
            'Oms' => InvoiceExpenditureItem::ITEM_OMS_FIXED_PAYMENT,
        ];

        $bankQuery = $this->company->getCashBankFlows()->select('amount')->andWhere([
            'flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE,
            'is_taxable' => true,
        ])->andWhere([
            'kbk' => self::${"kbk$type"},
            'expenditure_item_id' => $items[$type],
        ])->andWhere([
            'between',
            'recognition_date',
            $from,
            $till,
        ]);
        $orderQuery = $this->company->getCashOrderFlows()->select('amount')->andWhere([
            'flow_type' => CashFlowsBase::FLOW_TYPE_EXPENSE,
            'is_taxable' => true,
            'is_accounting' => true,
            'expenditure_item_id' => $items[$type],
        ])->andWhere([
            'between',
            'recognition_date',
            $from,
            $till,
        ]);

        $query = new Query;
        $query->from(['t' => $bankQuery->union($orderQuery, true)]);

        return $query;
    }

    /**
     * @return integer
     */
    public function getPfrPaidAmountByDatePeriod($quarter = null, $summed = true)
    {
        if (!isset($this->_data['getPfrPaidAmountByDatePeriod'])) {
            $sum = 0;
            $data = [];
            foreach (self::$q as $q) {
                $m = str_pad((string) $q*3-2, 2, "0", STR_PAD_LEFT);
                $dateFrom = date_create("{$this->getYear()}-{$m}-01");
                $dateTill = (clone $dateFrom)->modify('last day of +2 month');
                $query = $this->getDuesAmountQuery('Pfr', $dateFrom->format('Y-m-d'), $dateTill->format('Y-m-d'));
                $val = (int) $query->sum('amount');
                $sum += $val;
                $data[$q]['val'] = $val;
                $data[$q]['sum'] = $sum;
            }
            $this->_data['getPfrPaidAmountByDatePeriod'] = $data;
        }

        $q = intval($quarter ? : $this->getQuarter());
        if (isset($this->_data['getPfrPaidAmountByDatePeriod'][$q])) {
            return $this->_data['getPfrPaidAmountByDatePeriod'][$q][$summed ? 'sum' : 'val'];
        }

        return 0;
    }

    /**
     * @return integer
     */
    public function getOver300PaidAmountByDatePeriod($quarter = null, $summed = true)
    {
        if (!isset($this->_data['getOver300PaidAmountByDatePeriod'])) {
            $sum = 0;
            $data = [];
            foreach (self::$q as $q) {
                $m = str_pad((string) $q*3-2, 2, "0", STR_PAD_LEFT);
                $dateFrom = date_create("{$this->getYear()}-{$m}-01");
                $dateTill = (clone $dateFrom)->modify('last day of +2 month');
                $query = $this->getDuesAmountQuery('Over300', $dateFrom->format('Y-m-d'), $dateTill->format('Y-m-d'));
                $val = (int) $query->sum('amount');
                $sum += $val;
                $data[$q]['val'] = $val;
                $data[$q]['sum'] = $sum;
            }
            $this->_data['getOver300PaidAmountByDatePeriod'] = $data;
        }

        $q = intval($quarter ? : $this->getQuarter());
        if (isset($this->_data['getOver300PaidAmountByDatePeriod'][$q])) {
            return $this->_data['getOver300PaidAmountByDatePeriod'][$q][$summed ? 'sum' : 'val'];
        }

        return 0;
    }

    /**
     * @return integer
     */
    public function getOmsPaidAmountByDatePeriod($quarter = null, $summed = true)
    {
        if (!isset($this->_data['getOmsPaidAmountByDatePeriod'])) {
            $sum = 0;
            $data = [];
            foreach (self::$q as $q) {
                $m = str_pad((string) $q*3-2, 2, "0", STR_PAD_LEFT);
                $dateFrom = date_create("{$this->getYear()}-{$m}-01");
                $dateTill = (clone $dateFrom)->modify('last day of +2 month');
                $query = $this->getDuesAmountQuery('Oms', $dateFrom->format('Y-m-d'), $dateTill->format('Y-m-d'));
                $val = (int) $query->sum('amount');
                $sum += $val;
                $data[$q]['val'] = $val;
                $data[$q]['sum'] = $sum;
            }
            $this->_data['getOmsPaidAmountByDatePeriod'] = $data;
        }

        $q = intval($quarter ? : $this->getQuarter());
        if (isset($this->_data['getOmsPaidAmountByDatePeriod'][$q])) {
            return $this->_data['getOmsPaidAmountByDatePeriod'][$q][$summed ? 'sum' : 'val'];
        }

        return 0;
    }

    /**
     * @return integer
     */
    public function getUsn6TotalAmount($quarter = null)
    {
        if (!isset($this->_data['getUsn6TotalAmount'])) {
            foreach (self::$q as $q) {
                $amount = $this->getTaxableAmountByPeriod($this->getYear(), $q);
                $taxationType = $this->company->companyTaxationType;
                $this->_data['getUsn6TotalAmount'][$q] = (int) round($amount * $taxationType->usn_percent / 100, -2);
            }
        }

        $q = intval($quarter ? : $this->getQuarter());

        return isset($this->_data['getUsn6TotalAmount'][$q]) ? $this->_data['getUsn6TotalAmount'][$q] : 0;
    }

    /**
     * @return integer
     */
    public function getUsn6PreviousTotalAmount($quarter = null)
    {
        $q = intval($quarter !== null ? $quarter : $this->getQuarter());
        if (!isset($this->_data['getUsn6PreviousTotalAmount'][$q])) {
            $amount = 0;
            while (--$q > 0) {
                $amount += $this->getUsn6calculatedAmount($q);
            }
            $this->_data['getUsn6PreviousTotalAmount'][$q] = (int) $amount;
        }

        return $this->_data['getUsn6PreviousTotalAmount'][$q];
    }

    /**
     * @return integer
     */
    public function getUsn6ReductionAmount($quarter = null)
    {
        $q = intval($quarter !== null ? $quarter : $this->getQuarter());
        if (!isset($this->_data['getUsn6ReductionAmount'][$q])) {
            $reductionAmount = $this->getOver300PaidAmountByDatePeriod($q);
            $reductionAmount += $this->getPfrPaidAmountByDatePeriod($q);
            $reductionAmount += $this->getOmsPaidAmountByDatePeriod($q);

            $this->_data['getUsn6ReductionAmount'][$q] = (int) round($reductionAmount, -2);
        }

        if ($quarter = 0) {
            return array_sum($this->_data['getUsn6ReductionAmount']);
        }

        return $this->_data['getUsn6ReductionAmount'][$q];
    }

    /**
     * @return integer
     */
    public function getUsn6previousAmount($q)
    {
        $q--;
        if ($q > 0) {
            return $this->getUsn6calculatedAmount($q) + $this->getUsn6previousAmount($q);
        } else {
            return 0;
        }
    }

    /**
     * @return integer
     */
    public function getUsn6calculatedAmount($quarter = null)
    {
        $q = intval($quarter !== null ? $quarter : $this->getQuarter());
        if ($q) {
            if (!isset($this->_data['getUsn6calculatedAmount'][$q])) {
                $reductionAmount = $q > 1 ? $this->getUsn6PreviousTotalAmount($q) : 0;
                $reductionAmount += $this->getUsn6ReductionAmount($q);

                $this->_data['getUsn6calculatedAmount'][$q] = (int) round(max(0, ($this->getUsn6TotalAmount($q) - $reductionAmount)), -2);
            }

            return $this->_data['getUsn6calculatedAmount'][$q];
        }

        return 0;
    }

    /**
     * @return integer
     */
    public function getUsn6Amount($quarter = null)
    {
        return $this->getUsn6needPayAmount($quarter);
    }

    /**
     * @return integer
     */
    public function getUsn6advanceAmount()
    {
        return $this->getUsn6Paid(0) + $this->getUsn6ReductionAmount(0);
    }

    /**
     * @return integer
     */
    public function getUsn6needPayAmount($quarter = null)
    {
        $q = intval($quarter !== null ? $quarter : $this->getQuarter());

        return (int) round(max(0, ($this->getUsn6TotalAmount($q) - $this->getUsn6advanceAmount())), -2);
    }

    /**
     * @return integer
     */
    public function getUsn6Paid($quarter = null)
    {
        if (!isset($this->_data['getUsn6Paid'])) {
            foreach (range(1, 4) as $q) {
                $this->_data['getUsn6Paid'][$q] = (int) $this->getCompany()->getCashBankFlows()->andWhere([
                    'kbk' => self::$kbkUsn6,
                    'is_taxable' => true,
                ])->andWhere([
                    'or',
                    $this->getUsn6PaidByQ($q),
                    $this->getUsn6PaidByPL($q),
                    $this->getUsn6PaidByGD($q),
                ])->sum('amount');
            }
        }

        if ($quarter === 0) {
            return array_sum($this->_data['getUsn6Paid']);
        }
        if ($quarter === null) {
            return  $this->_data['getUsn6Paid'];
        }
        return $this->_data['getUsn6Paid'][$quarter];
    }

    /**
     * @return array
     */
    public function getUsn6PaidByQ($quarter = null)
    {
        $q = intval($quarter !== null ? $quarter : $this->getQuarter());
        
        return ['tax_period_code' => $this->getUsn6PeriodCode($q)];
    }
    
    /**
     * @return array
     */
    public function getUsn6PaidByPL($quarter = null)
    {
        $q = intval($quarter !== null ? $quarter : $this->getQuarter());
        switch ($q) {
            case 2:
                $pl = "01";
                $from = "{$this->getYear()}-07-01";
                $till = "{$this->getYear()}-09-30";
                break;
            case 4:
                $pl = "02";
                $nextYear = $this->getYear() + 1;
                $from = "{$nextYear}-01-01";
                $till = "{$nextYear}-03-31";
                break;
            default:
                $pl = "00";
                $from = "1901-01-01";
                $till = "1901-01-01";
                break;
        }

        return [
            'and',
            ['between', 'recognition_date', $from, $till],
            ['tax_period_code' => "ПЛ.{$pl}.{$this->getYear()}"],
        ];
    }
    
    /**
     * @return array
     */
    public function getUsn6PaidByGD($quarter = null)
    {
        $q = intval($quarter !== null ? $quarter : $this->getQuarter());
        switch ($q) {
            case 1:
                $from = "{$this->getYear()}-01-01";
                $till = "{$this->getYear()}-06-30";
                break;
            case 2:
                $from = "{$this->getYear()}-07-01";
                $till = "{$this->getYear()}-09-30";
                break;
            case 3:
                $from = "{$this->getYear()}-10-01";
                $till = "{$this->getYear()}-12-31";
                break;
            case 4:
                $year = $this->getYear() + 1;
                $from = "{$year}-01-01";
                $till = "{$year}-03-31";
                break;
            default:
                $from = "1901-01-01";
                $till = "1901-01-01";
                break;
        }

        return [
            'and',
            ['between', 'recognition_date', $from, $till],
            ['tax_period_code' => "ГД.00.{$this->getYear()}"],
        ];
    }

    /**
     * @return integer
     */
    public function getUsn6PaidAmount($quarter = null)
    {
        $q = intval($quarter !== null ? $quarter : $this->getQuarter());

        return ArrayHelper::getValue($this->getUsn6Paid(), $q, 0);
    }

    /**
     * @return integer
     */
    public function getUsn6RemainingAmount($quarter = null)
    {
        $q = intval($quarter !== null ? $quarter : $this->getQuarter());
        $previousAmount = $this->getUsn6previousAmount($q);
        $paidTotal = array_sum((array) $this->getUsn6Paid());
        $paidAmount = max(0, $paidTotal - $previousAmount);

        return max(0, $this->getUsn6calculatedAmount($quarter) - $paidAmount);
    }

    /**
     * @return integer
     */
    public function getOver300LastTaxableAmount()
    {
        if (!isset($this->_data['getOver300LastTaxableAmount'])) {
            $year = $this->getOver300LastPeriod()->year;
            $this->_data['getOver300LastTaxableAmount'] = $this->getTaxableAmountByPeriod($year, 4);
        }

        return $this->_data['getOver300LastTaxableAmount'];
    }

    /**
     * @return integer
     */
    public function getOver300ThisTaxableAmount()
    {
        if (!isset($this->_data['getOver300ThisTaxableAmount'])) {
            $year = $this->getOver300ThisPeriod()->year;
            $quarter = $this->getQuarter();
            $this->_data['getOver300ThisTaxableAmount'] = $this->getTaxableAmountByPeriod($year, $quarter);
        }

        return $this->_data['getOver300ThisTaxableAmount'];
    }

    /**
     * @return integer
     */
    public function getOver300LastUnlimitAmount()
    {
        return (int) round(max(0, ($this->getOver300LastTaxableAmount() - self::$onePercentOver))/100);
    }

    /**
     * @return integer
     */
    public function getOver300ThisUnlimitAmount()
    {
        return (int) round(max(0, ($this->getOver300ThisTaxableAmount() - self::$onePercentOver))/100);
    }

    /**
     * @return integer
     */
    public function getOver300LastLimit()
    {
        return $this->getCompanyFixedPayments($this->getOver300LastPeriod()->year)->limit;
    }

    /**
     * @return integer
     */
    public function getOver300ThisLimit()
    {
        return $this->getCompanyFixedPayments($this->getOver300ThisPeriod()->year)->limit;
    }

    /**
     * @return integer
     */
    public function getOver300LastAmount()
    {
        $limit = $this->getOver300LastLimit();
        $unlimit = $this->getOver300LastUnlimitAmount();

        return min($limit, $unlimit);
    }

    /**
     * @return integer
     */
    public function getOver300ThisAmount()
    {
        $limit = $this->getOver300ThisLimit();
        $unlimit = $this->getOver300ThisUnlimitAmount();

        return min($limit, $unlimit);
    }

    /**
     * @return integer
     */
    public function getOver300LastPaidAmount()
    {
        if (!isset($this->_data['getOver300LastPaidAmount'])) {
            $year = $this->getOver300LastPeriod()->year;
            $this->_data['getOver300LastPaidAmount'] = (int) $this->getPfrPaidQuery($year)->andWhere([
                'expenditure_item_id' => InvoiceExpenditureItem::ITEM_INSURANCE_PAYMENT_IP
            ])->sum('amount');
        }

        return $this->_data['getOver300LastPaidAmount'];
    }

    /**
     * @return integer
     */
    public function getOver300ThisPaidAmount()
    {
        if (!isset($this->_data['getOver300ThisPaidAmount'])) {
            $year = $this->getOver300ThisPeriod()->year;
            $till = $this->getOver300ThisPeriod()->dateTill->format('Y-m-d');
            $this->_data['getOver300ThisPaidAmount'] = (int) $this->getPfrPaidQuery($year)->andWhere([
                'expenditure_item_id' => InvoiceExpenditureItem::ITEM_INSURANCE_PAYMENT_IP
            ])->sum('amount');
        }

        return $this->_data['getOver300ThisPaidAmount'];
    }

    /**
     * @return integer
     */
    public function getOver300LastRemainingAmount()
    {
        return max(0, $this->getOver300LastAmount() - min($this->getOver300LastPaidAmount(), $this->getOver300LastLimit()));
    }

    /**
     * @return integer
     */
    public function getOver300ThisRemainingAmount()
    {
        return max(0, $this->getOver300ThisAmount() - min($this->getOver300ThisPaidAmount(), $this->getOver300ThisLimit()));
    }

    /**
     * @return FixedPaymentsIp
     */
    public function getFixedPayments($year)
    {
        if (!isset($this->_data['getFixedPayments'][$year])) {
            $this->_data['getFixedPayments'][$year] = FixedPaymentsIp::find()->where([
                '<=', 'year', $year
            ])->orderBy([
                'year' => SORT_DESC,
            ])->one() ? : (object) [
                'pfr' => 0,
                'oms' => 0,
                'limit' => 0
            ];
        }

        return $this->_data['getFixedPayments'][$year];
    }

    /**
     * @return stdClass
     */
    public function getCompanyFixedPayments($year)
    {
        if (!isset($this->_data['getCompanyFixedPayments'][$year])) {
            $curYear = $this->getPfrPeriod()->year;
            $regDate = date_create($this->company->tax_authority_registration_date);
            $regYear = $regDate->format('Y');
            $amounts = $this->getFixedPayments($year);
            $pfr = ArrayHelper::getValue($amounts, 'pfr', 0);
            $oms = ArrayHelper::getValue($amounts, 'oms', 0);
            $limit = ArrayHelper::getValue($amounts, 'limit', 0);
            $payments = [
                'pfr' => 0,
                'oms' => 0,
            ];
            if ($regYear < $year) {
                $payments['pfr'] += $pfr;
                $payments['oms'] += $oms;
            } elseif ($regYear == $year) {
                if ($leftMonth = (12 - $regDate->format('n'))) {
                    $payments['pfr'] += ($m1 = (int) round($pfr / 12 * $leftMonth));
                    $payments['oms'] += ($m2 = (int) round($oms / 12 * $leftMonth));
                }
                $days = $regDate->format('t');
                if ($leftDays = ($days - $regDate->format('j') + 1)) {
                    $payments['pfr'] += ($d1 = (int) round($pfr / 12 * $leftDays / $days));
                    $payments['oms'] += ($d2 = (int) round($oms / 12 * $leftDays / $days));
                }
            }
            $payments['limit'] = $limit - $payments['pfr'];

            $this->_data['getCompanyFixedPayments'][$year] = (object) $payments;
        }

        return $this->_data['getCompanyFixedPayments'][$year];
    }

    /**
     * @return integer
     */
    public function getPfrAmount()
    {
        return $this->getCompanyFixedPayments($this->getPfrPeriod()->year)->pfr;
    }

    /**
     * @return integer
     */
    public function getPfrPaidQuery($year)
    {
        return $this->company->getCashBankFlows()->andWhere([
            'kbk' => self::$kbkPfr,
            'tax_period_code' => 'ГД.00.' . $year,
            'is_taxable' => true,
        ]);
    }

    /**
     * @return integer
     */
    public function getPfrExistPaidAmount($year)
    {
        if (!isset($this->_data['getPfrExistPaidAmount'][$year])) {
            $this->_data['getPfrExistPaidAmount'][$year] = (int) $this->getPfrPaidQuery($year)->sum('amount');
        }

        return $this->_data['getPfrExistPaidAmount'][$year];
    }

    /**
     * @return integer
     */
    public function getPfrPaidAmount($year)
    {
        if (!isset($this->_data['getPfrExistPaidAmount'][$year])) {
            $this->_data['getPfrExistPaidAmount'][$year] = (int) $this->getPfrPaidQuery($year)->andWhere([
                'expenditure_item_id' => InvoiceExpenditureItem::ITEM_PFR_FIXED_PAYMENT,
            ])->sum('amount');
        }

        return $this->_data['getPfrExistPaidAmount'][$year];
    }

    /**
     * @return integer
     */
    public function getPfrRemainingAmount()
    {
        return max(0, $this->getPfrAmount() - $this->getPfrPaidAmount($this->getPfrPeriod()->year));
    }

    /**
     * @return integer
     */
    public function getOmsAmount()
    {
        return $this->getCompanyFixedPayments($this->getOmsPeriod()->year)->oms;
    }

    /**
     * @return integer
     */
    public function getOmsPaidQuery()
    {
        return $this->company->getCashBankFlows()->andWhere([
            'kbk' => self::$kbkOms,
            'tax_period_code' => 'ГД.00.' . $this->getOmsPeriod()->year,
            'is_taxable' => true,
        ]);
    }

    /**
     * @return integer
     */
    public function getOmsExistPaidAmount()
    {
        if (!isset($this->_data['getOmsExistPaidAmount'])) {
            $this->_data['getOmsExistPaidAmount'] = (int) $this->getOmsPaidQuery()->sum('amount');
        }

        return $this->_data['getOmsExistPaidAmount'];
    }

    /**
     * @return integer
     */
    public function getOmsRemainingAmount()
    {
        return max(0, $this->getOmsAmount() - $this->getOmsExistPaidAmount());
    }

    /**
     * @return integer
     */
    public function getUsn6ReductionByUsn6($q)
    {
        return $this->getUsn6PaidAmount($q);
    }

    /**
     * @return integer
     */
    public function getUsn6ReductionByPFR()
    {
        return $this->getPfrPaidAmountByDatePeriod();
    }

    /**
     * @return integer
     */
    public function getUsn6ReductionByOver300()
    {
        return $this->getOver300PaidAmountByDatePeriod();
    }

    /**
     * @return integer
     */
    public function getUsn6ReductionByOMS()
    {
        return $this->getOmsPaidAmountByDatePeriod();
    }

    /**
     * @return integer
     */
    public function getTaxPayableAmount()
    {
        return $this->getUsn6Amount();
    }

    /**
     * @return array
     */
    public function getContractorList(Query $query)
    {
        $idArray = $query->select('contractor_id')->distinct()->column();

        $list1 = CashContractorType::find()->select('text')->andWhere([
            'name' => $idArray,
        ])->indexBy('name')->column();
        if (isset($list1[CashContractorType::COMPANY_TEXT])) {
            $list1[CashContractorType::COMPANY_TEXT] = $this->company->getTitle(true);
        }

        $list2 = Contractor::getSorted()->select([
            'CONCAT_WS(" ", {{company_type}}.[[name_short]], {{contractor}}.[[name]])',
            'contractor.id',
        ])->andWhere([
            'contractor.id' => $idArray,
        ])->indexBy('id')->column();

        return $list1 + $list2;
    }

    /**
     * @return array
     */
    public function getTaxPeriodCodeList()
    {
        $taxQuarter = (int) $this->getQuarter();
        $taxYear = (int) $this->getYear();
        $taxPeriodCodeList = [];
        if ($taxQuarter > 1) {
            foreach (range(1, $taxQuarter - 1) as $quarter) {
                $taxPeriodCodeList[] = "КВ.0{$quarter}.{$taxYear}";
            }
        }
        if ($taxQuarter > 2) {
            $taxPeriodCodeList[] = "ПЛ.01.{$taxYear}";
        }
        if ($taxQuarter > 3) {
            $taxPeriodCodeList[] = "ПЛ.02.{$taxYear}";
        }

        $taxPeriodCodeList[] = "ГД.00.{$taxYear}";

        return $taxPeriodCodeList;
    }

    /**
     * @return string
     */
    public function getPeriodLabel()
    {
        return str_replace("&nbsp;", " ", $this->getPeriod()->label);
    }

    /**
     * @return string
     */
    public function getPeriodLabelByCode($code, $html = false)
    {
        $sp = $html ? '&nbsp;': ' ';
        $parts = explode('.', $code);
        $str = mb_strtoupper(ArrayHelper::getValue($parts, 0, ''));
        $num = (int) ArrayHelper::getValue($parts, 1);
        $year = (int) ArrayHelper::getValue($parts, 2);
        if ($str && $year) {
            if ($str == 'ГД') {
                return "{$year}{$sp}год";
            } elseif ($str == 'КВ') {
                switch ($num) {
                    case 1:
                        return "1-й{$sp}квартал {$year}{$sp}года";
                        break;
                    case 2:
                        return "1-е{$sp}полугодие {$year}{$sp}года";
                        break;
                    case 3:
                        return "9-ть{$sp}месяцев {$year}{$sp}года";
                        break;
                    case 4:
                    default:
                        return "{$year}{$sp}год";
                        break;
                }
            }
        }
        if ($year) {
            return "{$year}{$sp}год";
        }

        return "XXXX{$sp}год";
    }

    /**
     * @return string
     */
    public function getPaymentTypeByPaymentOrder(PaymentOrder $model)
    {
        switch ($model->kbk) {
            case self::$kbkUsn6:
                return "Usn6";
                break;
            case self::$kbkOms:
                return "Oms";
                break;
            case self::$kbkPfr:
            case self::$kbkOver300:
                return mb_stripos($model->purpose_of_payment, '300') === false ? "Pfr" : "Over300";
                break;
            default:
                return "";
                break;
        }
    }

    /**
     * @return array
     */
    public static function icon($type, $options = [])
    {
        switch ($type) {
            case self::TYPE_BANK:
                Html::addCssClass($options, ['fa', 'fa-bank']);
                break;
            case self::TYPE_ORDER:
                Html::addCssClass($options, ['fa', 'fa-money']);
                break;
        }

        return Html::tag('i', '', $options);
    }

    /**
     * @param "quarter"|"year" $period
     * @return null|string
     */
    public function getTopLink($period)
    {
        if ($this->isPaid) {
            return null;
        }

        $month = (int)date('m');
        $year  = (int)date('Y');
        $prev_year = $year - 1;
        $message = '';

        if ($period == 'quarter') {
            $declaration = TaxDeclaration::findOne([
                'tax_year' => $year,
                'company_id' => $this->_company->id
            ]);
            if ($month == 4 && (!$declaration || $declaration->downloaded_quarter < 1)) {
                $message = "Рассчитать налоги за 1-й квартал {$year} года";
            }
            if ($month == 7 && (!$declaration || $declaration->downloaded_quarter < 2)) {
                $message = "Рассчитать налоги за 1-е полугодие {$year} года";
            }
            if ($month == 10 && (!$declaration || $declaration->downloaded_quarter < 3)) {
                $message = "Рассчитать налоги за 9-ть месяцев {$year} года";
            }
        }

        if ($period == 'year') {
            $declaration = TaxDeclaration::findOne([
                'tax_year' => $prev_year,
                'company_id' => $this->_company->id
            ]);
            if (in_array($month, [1,2,3,4]) && (!$declaration || $declaration->downloaded_quarter < 4)) {
                $message = "Рассчитать налоги и заполнить декларацию за {$prev_year} год";
            }
        }

        return ($message) ? Html::a($message, '/tax/robot/company') : null;
    }
}
