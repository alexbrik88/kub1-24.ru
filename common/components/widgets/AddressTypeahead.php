<?php
/**
 * Created by konstantin.
 * Date: 23.7.15
 * Time: 11.42
 */

namespace common\components\widgets;


use kartik\typeahead\Typeahead;
use yii\helpers\Html;
use yii\web\JsExpression;

/**
 * Class AddressTypeahead
 * @package common\components\widgets
 */
class AddressTypeahead extends Typeahead
{

    /**
     * Base url for remote access to dictionary.
     * @var string
     */
    public $remoteUrl;

    /**
     * Property that will be used for display in hint list.
     * @var string
     */
    public $display = 'fullname';

    /**
     * Array of selectors of other address fields that needed in search.
     * Representation: [key => value,], where key - parameter in uri, value - selector of input.
     * @var array
     */
    public $related;

    /**
     * @var bool whether the dropdown menu is scrollable
     */
    public $scrollable = true;

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->options = array_merge([
            'maxlength' => true,
        ], $this->options);

        $this->pluginOptions = array_merge([
            'queryWhenFocused' => false,
            'minLength' => 2,
            'highlight' => true,
        ], $this->pluginOptions);

        if ($this->remoteUrl !== null) {
            $related = json_encode((array) $this->related);
            $inputId = Html::getInputId($this->model, $this->attribute);
            $urlReplaceJs = <<<JS
                function(url, query) {
                    url = url.replace('%QUERY', query);
                    url += '?type=' + $('#{$inputId}').data('type');
                    url += '&q=' + encodeURIComponent(query);

                    $.each($related, function (key, selector) {
                        url += '&' + key + '=' + encodeURIComponent($(selector).val());
                    });

                    return url;
                }
JS;

            $this->dataset = array_merge([
                [
                    'display' => $this->display,
                    'limit' => 10,
                    'remote' => [
                        'url' => $this->remoteUrl,
                        'replace' => new JsExpression($urlReplaceJs),
                        'rateLimitWait' => 700,
                    ],
                    'templates' => [
                        'notFound' => '<div class="text-danger" style="padding:0 8px">Адрес не найден. Возможно, его нет в нашей базе адресов.</div>',
                    ],
                ],
            ], $this->dataset);
        }
    }

}