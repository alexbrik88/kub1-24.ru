<?php
/**
 * Created by konstantin.
 * Date: 23.7.15
 * Time: 11.42
 */

namespace common\components\widgets;


use kartik\typeahead\Typeahead;
use yii\web\JsExpression;

/**
 * Class BikTypeahead
 * @package common\components\widgets
 */
class BikTypeahead extends Typeahead
{
    /**
     * Base url for remote access to dictionary.
     * @var string
     */
    public $remoteUrl;

    /**
     * Key-value pairs to fill up inputs after bik was selected.
     * Representation: [key => value,], where key - selector of input, value - attribute of `bik` model.
     * @var array
     */
    public $related;

    /**
     * @var bool whether the dropdown menu is scrollable
     */
    public $scrollable = true;

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->options = array_merge($this->options, [
            'maxlength' => true,
        ]);

        $this->pluginOptions = array_merge($this->pluginOptions, [
            'limit' => 10,
            'highlight' => true,
        ]);

        if (!isset($this->pluginEvents['typeahead:select']) && is_array($this->related)) {
            $related = json_encode($this->related);
            $this->pluginEvents['typeahead:select'] = <<<JS
                function(e, obj) {
                    console.log(obj);
                    $.each($related, function (key, value) {
                        $(key).val(obj[value]);
                    });
                    $(e.currentTarget).trigger('change');
                }
JS;
        }

        if ($this->remoteUrl !== null) {
            $query = parse_url($this->remoteUrl, PHP_URL_QUERY);
            if ($query) {
                $url = $this->remoteUrl.'&q=%QUERY';
            } else {
                $url = $this->remoteUrl.'?q=%QUERY';
            }
            $this->dataset = array_merge($this->dataset, [
                [
                    'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('bik')",
                    'display' => 'bik',
                    'limit' => 20,
                    'remote' => [
                        'url' => $url,
                        'wildcard' => '%QUERY',
                    ],
                    'templates' => [
                        'notFound' => '<div class="text-danger" style="padding:0 8px">БИК не найден. Возможно ваш банк изменил БИК. Проверьте на сайте банка.</div>',
                        'suggestion' => new JsExpression('function (obj) {
                                    return "<span>" + obj.bik + " (" + obj.name + ")" + "</span>";
                                }'),
                    ],
                ],
            ]);
        }
    }
}
