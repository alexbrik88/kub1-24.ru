<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 17.10.2016
 * Time: 6:05
 */

namespace common\components\widgets;


use kartik\typeahead\Typeahead;
use yii\helpers\Url;
use yii\web\JsExpression;

/**
 * Class EmployeeTypeahead
 * @package common\components\widgets
 */
class IfnsTypeahead extends Typeahead
{
    /**
     * Base url for remote access to dictionary.
     * @var string
     */
    public $remoteUrl;

    /**
     * Key-value pairs to fill up inputs after bik was selected.
     * Representation: [key => value,], where key - selector of input, value - attribute of `bik` model.
     * @var array
     */
    public $related;

    /**
     * @var bool whether the dropdown menu is scrollable
     */
    public $scrollable = true;

    /**
     * @var string
     */
    public $kppSelector;

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->options = array_merge($this->options, [
            'maxlength' => true,
        ]);

        $this->pluginOptions = array_merge($this->pluginOptions, [
            'limit' => 10,
            'highlight' => true,
        ]);

        if (is_array($this->related)) {
            $related = json_encode($this->related);
        } else {
            $related = null;
        }

        $ifnsUrl = Url::to(['/dictionary/ifns', 'q' => 'QUERY']);

        if (!isset($this->pluginEvents['typeahead:select']) && $related !== null) {
            $this->pluginEvents['typeahead:select'] = <<<JS
                function(e, obj) {
                    $.each($related, function (key, value) {
                        $(key).val(obj[value]);
                    });
                    $(e.currentTarget).trigger('change');
                }
JS;
        }

        if ($this->kppSelector) {
            $this->view->registerJs(<<<JS
                jQuery(document).on('change', '{$this->kppSelector}', function () {
                    var ifns = this.value.substring(0, 4);
                    if (ifns.length == 4) {
                        jQuery('#{$this->options['id']}').val(ifns).trigger('change');
                        var url = '{$ifnsUrl}';
                        jQuery.get(url.replace('QUERY', ifns), function (data) {
                            var obj = data[0];
                            if (obj) {
                                jQuery.each({$related}, function (key, value) {
                                    jQuery(key).val(obj[value]);
                                });
                            }
                        });
                    }
                });
JS
            );
        }

        $this->dataset = array_merge($this->dataset, [
            [
                'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('ga')",
                'display' => 'ga',
                'limit' => 10,
                'remote' => [
                    'url' => $ifnsUrl,
                    'wildcard' => 'QUERY',
                ],
                'templates' => [
                    'suggestion' => new JsExpression('function (obj) {
                        return "<div>" + obj.ga + " " + obj.gb + "</div>";
                    }'),
                ],
            ],
        ]);
    }
}