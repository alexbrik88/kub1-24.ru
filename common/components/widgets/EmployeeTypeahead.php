<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 17.10.2016
 * Time: 6:05
 */

namespace common\components\widgets;


use kartik\typeahead\Typeahead;
use yii\helpers\Url;
use yii\web\JsExpression;

/**
 * Class EmployeeTypeahead
 * @package common\components\widgets
 */
class EmployeeTypeahead extends Typeahead
{
    /**
     * Base url for remote access to dictionary.
     * @var string
     */
    public $remoteUrl;

    /**
     * @var bool
     */
    public $admin = false;

    /**
     * Key-value pairs to fill up inputs after bik was selected.
     * Representation: [key => value,], where key - selector of input, value - attribute of `bik` model.
     * @var array
     */
    public $related;

    /**
     * @var bool whether the dropdown menu is scrollable
     */
    public $scrollable = true;

    /**
     *
     */
    public function init()
    {
        parent::init();

        $this->options = array_merge($this->options, [
            'maxlength' => true,
        ]);

        $this->pluginOptions = array_merge($this->pluginOptions, [
            'limit' => 10,
            'highlight' => true,
        ]);

        if (!isset($this->pluginEvents['typeahead:select']) && is_array($this->related)) {
            $related = json_encode($this->related);
            $this->pluginEvents['typeahead:select'] = <<<JS
                function(e, obj) {
                    $.each($related, function (key, value) {
                        if ($(key).attr('id') == 'employee-sex') {
                            $(key + ' ' + '[value = ' + obj[value] + ']').click().click();
                        } else {
                            $(key).val(obj[value]).trigger('change');
                        }
                    });
                    $(e.currentTarget).trigger('change');
                }
JS;
        }

        if ($this->remoteUrl !== null) {
            $qOperator = $this->admin ? '&' : '?';
            $this->dataset = array_merge($this->dataset, [
                [
                    'datumTokenizer' => "Bloodhound.tokenizers.obj.whitespace('email')",
                    'display' => 'email',
                    'limit' => 20,
                    'remote' => [
                        'url' => $this->remoteUrl . $qOperator .'q=%QUERY',
                        'wildcard' => '%QUERY',
                    ],
                    'templates' => [
                        'suggestion' => new JsExpression('function (obj) {
                                    return "<span>" + obj.lastname + " " + obj.firstname + " " + obj.patronymic + "</span>";
                                }'),
                    ],
                ],
            ]);
        }
    }
}