<?php

namespace common\components;

use php_rutils\RUtils;

/**
 * Class TextHelper
 * @package common\components
 */
class TextHelper
{
    /**
     * @param $value
     * @param int $decimals
     * @param string $decimalsSeparator
     * @param string $thousandSeparator
     * @return string
     */
    public static function moneyFormat($value, $decimals = 0)
    {
        return \Yii::$app->formatter->asDecimal($value, $decimals);
    }

    /**
     * @param $value
     * @param string $thousandSeparator
     * @return string
     */
    public static function integerFormat($value, $thousandSeparator = ' ')
    {
        return \Yii::$app->formatter->asInteger($value);
    }

    /**
     * @param number $value
     * @param integer $maxDecimal
     * @param string $decimalsSeparator
     * @param string $thousandSeparator
     * @return string
     */
    public static function numberFormat($value, $maxDecimal = 2, $decimalsSeparator = ',', $thousandSeparator = ' ')
    {
        $value = $value + 0;
        $parts = explode('.', (string)$value);
        $decimal = isset($parts[1]) ? strlen($parts[1]) : 0;

        return number_format($value, min($decimal, $maxDecimal), $decimalsSeparator, $thousandSeparator);
    }

    /**
     * @param $value
     * @param int $decimals
     * @param string $decimalsSeparator
     * @param string $thousandSeparator
     * @return string
     */
    public static function invoiceMoneyFormat($value, $decimals = 0, $decimalsSeparator = ',', $thousandSeparator = ' ')
    {
        $value = floatval($value);
        return number_format($value / 100, $decimals, $decimalsSeparator, $thousandSeparator);
    }

    /**
     * @param $value
     * @return float|string
     */
    public static function moneyFormatFromIntToFloat($value, $precision = 2)
    {
        return sprintf("%.{$precision}f", $value / 100);
    }

    /**
     * @param $number
     * @return float
     */
    public static function fixNumber($number)
    {
        $number = str_replace(',', '.', $number);
        $number = str_replace('/[^\d\.]/', '', $number);

        return (float)$number;
    }

    /**
     * @param $amount
     * @param bool|true $requireKopeck
     * @param bool|false $zeroInWords
     * @return string
     */
    public static function amountToWords($amount, $requireKopeck = true, $zeroInWords = false)
    {
        $words = [];

        $amount = bcmul(abs($amount), 1, 2);
        $ruble = floor($amount);
        $kopeck = (bcsub($amount, $ruble, 2) * 100);

        $words[] = RUtils::numeral()->sumString($amount, RUtils::MALE, [
            'рубль', 'рубля', 'рублей',
        ]);

        if ($kopeck > 0 || $requireKopeck) {
            $words[] = $zeroInWords
                ? RUtils::numeral()->sumString($kopeck, RUtils::FEMALE)
                : sprintf('%02d', $kopeck);

            $words[] = RUtils::numeral()->choosePlural($kopeck, ['копейка', 'копейки', 'копеек',]);
        }

        return join(' ', $words);
    }

    /**
     * @param $number
     * @param $integerWords
     * @param null $decimalWords
     * @return string
     */
    public static function getNumberInWords($number, $integerWords, $decimalWords = null)
    {
        if ((!is_string($integerWords) && !is_array($integerWords))
            || ($decimalWords !== null && !is_string($decimalWords) && !is_array($decimalWords))
        ) {
            throw new \BadMethodCallException();
        }

        $integer = (int)$number;
        $decimal = (int)(($number - $integer) * 100);

        $string = '';
        $string .= RUtils::numeral()->getInWords($integer)
            . ' ' . (is_string($integerWords) ? $integerWords : RUtils::numeral()->choosePlural($integer, $integerWords));

        if ($decimal > 0) {
            $string .= RUtils::numeral()->getInWords((int)$decimal)
                . ' ' . (is_string($decimalWords) ? $decimalWords : RUtils::numeral()->choosePlural($decimal, $decimalWords));
        }

        return $string;
    }

    /**
     * @param $string
     * @return string
     */
    public static function mb_ucfirst($string)
    {
        return mb_strtoupper(mb_substr($string, 0, 1)) . mb_strtolower(mb_substr($string, 1));
    }

    /**
     * @param int $length
     * @return string
     */
    public static function randString($length = 10)
    {
        return substr(str_shuffle("0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"), 0, $length);
    }

    /**
     * Takes money from input and modifies it in right float format.
     * @param $string
     * @return mixed
     */
    public static function parseMoneyInput($string)
    {
        $string = str_replace(',', '.', $string);
        $string = str_replace('/[^0-9\.]/', '', $string);

        return (float)$string;
    }

    /**
     * @param $string
     * @return float
     */
    public static function parseMoneyInputWithoutComma($string)
    {
        return (float)preg_replace('/[^0-9]/', '', $string);
    }

    /**
     * Возвращает фамилию и инициалы
     *
     * @param string $string
     * @return string
     */
    public static function nameShort($value)
    {
        if (is_string($value)) {
            $nameItems = preg_split("/[\s.]+/", trim($value), 3);
            if ($nameItems) {
                $name = [array_shift($nameItems)];
                array_walk($nameItems, function (&$v, $k) {
                    $v = mb_substr($v, 0, 1) . '.';
                });
                $name[] = implode('', $nameItems);

                return implode(' ', $name);
            }
        }

        return '';
    }

    /**
     * @param $str
     * @param $maxLen
     * @return string
     */
    public static function explodeStr($str, $maxLen)
    {
        $arr = explode(" ", preg_replace("/\s{2,}/"," ", $str));

        if (!$str || count($arr) <= 1)
            return $str;

        $ret = '';
        $br = 1;
        foreach ($arr as $v) {
            $ret .= $v;
            if (mb_strlen($ret, 'UTF-8') >= $br * $maxLen) {
                $ret .= '<br>';
                $br++;
            } else {
                $ret .= ' ';
            }
        }

        return $ret;
    }
}
