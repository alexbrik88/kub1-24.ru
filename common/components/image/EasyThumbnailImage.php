<?php
/**
 * Created by PhpStorm.
 * User: konstantin
 * Date: 9.6.15
 * Time: 10.54
 */

namespace common\components\image;


use himiklab\thumbnail\FileNotFoundException;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\FileHelper;
use yii\helpers\Html;

class EasyThumbnailImage extends \himiklab\thumbnail\EasyThumbnailImage
{
    /**
     * Creates and caches the image thumbnail and returns <img> tag.
     *
     * @param string $filename
     * @param integer $width
     * @param integer $height
     * @param string $mode
     * @param array $options options similarly with \yii\helpers\Html::img()
     * @return string
     */
    public static function thumbnailImg($filename, $width, $height, $mode = self::THUMBNAIL_OUTBOUND, $options = [],
                                        $quality = null, $checkRemFileMode = self::CHECK_REM_MODE_NONE)
    {
        return Html::img(
            static::thumbnailSrc($filename, $width, $height, $mode, $options, $quality, $checkRemFileMode),
            $options
        );
    }

    public static function thumbnailSrc($filename, $width, $height, $mode = self::THUMBNAIL_OUTBOUND, $options = [])
    {
        $showErrorMessage = ArrayHelper::remove($options, 'showErrorMessage', true);

        $filename = FileHelper::normalizePath(Yii::getAlias($filename));

        try {
            $thumbnailFileUrl = self::thumbnailFileUrl($filename, $width, $height, $mode, 100);
        } catch (FileNotFoundException $e) {
            if ($showErrorMessage) {
                //Yii::warning('Файл не найден.');
            }
        } catch (\Exception $e) {
            if ($showErrorMessage) {
                //Yii::warning("{$e->getCode()}\n{$e->getMessage()}\n{$e->getFile()}");
            }
        }

        if (isset($thumbnailFileUrl)) {
            return $thumbnailFileUrl;
        }

        return '';
    }
}
