<?php
/**
 * Created by:
 * User: konstantin
 * Date: 6/4/15
 * Time: 1:08 AM
 */

namespace common\components\date;


use yii\base\Behavior;
use yii\base\Exception;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\validators\Validator;

/**
 * Class DatePickerFormatBehavior
 * @package common\components\date
 *
 * @example
 * ~~~
 * use common\components\date\DatePickerFormatBehavior;
 *
 * public function rules ()
 * {
 *      return [
 *          ...
 *          [['contract_date'], 'safe'],
 *          ...
 *      ];
 * }
 *
 * public function behaviors()
 * {
 *     return [
 *          ...
 *          [
 *              'class' => DatePickerFormatBehavior::className(),
 *              'attributes' => [
 *                  'contract_date',
 *              ],
 *          ],
 *          ...
 *     ];
 * }
 * ~~~
 */
class DatePickerFormatBehavior extends Behavior
{
    /**
     * Array of attributes that would use datepicker plugin.
     * @var array
     */
    public $attributes;

    /**
     * Default date format after form submit.
     * @var string
     */
    public $formatFrom = DateHelper::FORMAT_USER_DATE;

    /**
     * Default date format into which date will be converted.
     * @var string
     */
    public $formatTo = DateHelper::FORMAT_DATE;

    /**
     * @see getDateFormatParams
     * @var array
     */
    public $dateFormatParams = [];

    /**
     * @see getMinDateFormatParams
     * @var array
     */
    public $minDateFormatParams = [];

    /**
     * Attaches events to the [[owner]]
     * @param \yii\base\Model $owner
     * @throws Exception
     */
    public function attach($owner)
    {
        if (!($owner instanceof Model)) {
            throw new Exception('Behavior\'s owner must be an instance of \yii\base\Model, `' . $owner::className() . '` given');
        }

        parent::attach($owner);

        if (!is_array($this->attributes)) {
            throw new Exception('Property `attributes` must be an array.');
        }

        $this->setValidators();
    }

    /**
     * Declares event handlers for the [[owner]]'s events.
     * @return array
     */
    public function events()
    {
        return [
            Model::EVENT_BEFORE_VALIDATE => 'beforeValidate',
        ];
    }

    /**
     * Convert all dates from datepicker to normal format.
     */
    public function beforeValidate()
    {
        /* @var ActiveRecord $owner */
        $owner = $this->owner;

        foreach ($this->attributes as $key => $value) {
            list($attribute, $params) = $this->parseAttribute($key, $value);

            // If attribute not changed there is empty string but in `oldAttributes` likely would be null
            if (empty($owner->$attribute)) {
                $owner->$attribute = null;
            }

            if (($owner->hasMethod('isAttributeChanged') // for form models
                    && $owner->isAttributeChanged($attribute) // form active record models
                    && $owner->$attribute !== null)
                || (!$owner->hasMethod('isAttributeChanged')
                    && $owner->$attribute !== null)
                // TODO: check input format.
            ) { // changed = comes from form (usually)
                if (!DateHelper::validateDate($owner->$attribute, $this->formatTo)) {
                    $owner->$attribute = DateHelper::format(
                        $owner->$attribute,
                        $this->formatTo,
                        $this->formatFrom
                    );
                }

            }
        }
    }

    /**
     * @param $key
     * @param $value
     * @return array
     */
    private function parseAttribute($key, $value)
    {
        $attribute = is_array($value) ? $key : $value;
        $params = is_array($value) ? $value : [];

        return [
            $attribute,
            $params,
        ];
    }

    /**
     * Set validators to owner model.
     */
    private function setValidators()
    {
        foreach ($this->attributes as $key => $value) {
            list($attribute, $params) = $this->parseAttribute($key, $value);

            $dateValidator = Validator::createValidator(
                'date',
                $this->owner,
                $attribute,
                array_merge($this->getDateFormatParams(), $params)
            );
            $this->owner->validators[] = $dateValidator;

            $minDateValidator = Validator::createValidator(
                'compare',
                $this->owner,
                $attribute,
                array_merge($this->getMinDateFormatParams(), $params)
            );
            $this->owner->validators[] = $minDateValidator;
        }
    }

    /**
     * @return array
     */
    private function getDateFormatParams()
    {
        return array_merge([
            'format' => 'php:' . $this->formatTo,
            'message' => 'Дата указана неверно.',
        ], $this->dateFormatParams);
    }

    /**
     * @return array
     */
    private function getMinDateFormatParams()
    {
        return array_merge([
            'compareValue' => DateHelper::MIN_DATE,
            'operator' => '>=',
            'message' => 'Дата указана неверно..',
            'enableClientValidation' => false,
        ], $this->minDateFormatParams);
    }

}
