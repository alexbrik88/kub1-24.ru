<?php

namespace common\components\date;

use DateInterval;
use DateTime;
use DateTimeInterface;

class DatePeriodCaller
{
    /**
     * @var DateTimeInterface
     */
    private $dateFrom;

    /**
     * @var DateTimeInterface
     */
    private $dateTo;

    /**
     * @var callable
     */
    private $callback;

    /**
     * @param DateTimeInterface $dateFrom
     * @param DateTimeInterface $dateTo
     * @param callable $callback
     */
    public function __construct(DateTimeInterface $dateFrom, DateTimeInterface $dateTo, callable $callback)
    {
        $this->dateFrom = $dateFrom;
        $this->dateTo = $dateTo;
        $this->callback = $callback;
    }

    /**
     * @param DateInterval $interval
     * @return bool
     * @throws
     */
    public function callInterval(DateInterval $interval): bool
    {
        /** @var DateTime $to */
        $to = null;

        while (true) {
            $from = $to ? (clone $to)->add(new DateInterval('P1D')) : clone $this->dateFrom;
            $to = (clone $from)->add($interval);

            if ($from->getTimestamp() > $this->dateTo->getTimestamp()) {
                break;
            }

            if ($to->getTimestamp() > $this->dateTo->getTimestamp()) {
                $to = clone $this->dateTo;
            }

            if (call_user_func($this->callback, clone $from, clone $to) === false) {
                return false;
            }

            if ($to->format(DateHelper::FORMAT_DATE) == $this->dateTo->format(DateHelper::FORMAT_DATE)) {
                break;
            }
        }

        return true;
    }

    /**
     * @return bool
     * @throws
     */
    public function callMonthly(): bool
    {
        return $this->callInterval(new DateInterval('P1M'));
    }
}
