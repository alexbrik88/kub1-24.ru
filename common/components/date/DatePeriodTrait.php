<?php

namespace common\components\date;

use DateTimeInterface;

trait DatePeriodTrait
{
    /**
     * @var DateTimeInterface|null
     */
    private ?DateTimeInterface $dateFrom = null;

    /**
     * @var DateTimeInterface|null
     */
    private ?DateTimeInterface $dateTo = null;

    /**
     * @return DateTimeInterface|null
     */
    public function getDateFrom(): ?DateTimeInterface
    {
        return $this->dateFrom;
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getDateTo(): ?DateTimeInterface
    {
        return $this->dateTo;
    }

    /**
     * @return bool
     */
    public function hasPeriod(): bool
    {
        if ($this->dateTo === null || $this->dateFrom === null) {
            return false;
        }

        return true;
    }

    /**
     * @param DateTimeInterface|null $dateFrom
     */
    public function setDateFrom(?DateTimeInterface $dateFrom): void
    {
        $this->dateFrom = $dateFrom;
    }

    /**
     * @param DateTimeInterface|null $dateTo
     */
    public function setDateTo(?DateTimeInterface $dateTo): void
    {
        $this->dateTo = $dateTo;
    }
}
