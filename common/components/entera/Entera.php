<?php
namespace common\components\entera;

use common\models\file\File;
use common\models\scanRecognize\ScanRecognizeParams;
use common\components\curl\Curl;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\VarDumper;

class Entera {

    public $_debugMode = true;

    public static $auth_hostname = 'id.entera.pro';
    public static $app_hostname = 'app.entera.pro';
    public static $auth_path = '/api/v1/login';
    public static $user_path = '/api/v1/currentUser';
    public static $space_path = '/api/v1/spaces';
    public static $upload_file_path = '/api/v1/recognitionTasks';
    public static $status_path = '/api/v1/recognitionTasks';

    private static $login;
    private static $password;
    private static $jwt_token;

    public static $space_guid = '4ee8578e-7eaa-4e8d-b967-4fdfd7ff7fcf';
    public static $task_guid;

    public function __construct()
    {
        self::$login = ArrayHelper::getValue(Yii::$app->params['entera'], 'login');
        self::$password = ArrayHelper::getValue(Yii::$app->params['entera'], 'password');
        self::$jwt_token = ScanRecognizeParams::getValue('jwt_token');
    }

    public function login()
    {
        $postData = json_encode([
            'login' => self::$login,
            'password' => self::$password
        ]);

        $requestHeader = [
            'Content-Type: application/json',
            'Content-Length: ' . strlen($postData)
        ];

        $curl = new \common\components\curl\Curl();

        $response = $curl->setOptions([
            CURLOPT_HEADERFUNCTION => 'common\components\entera\Entera::curlResponseHeaderCallback',
            CURLOPT_HTTPHEADER => $requestHeader,
            CURLOPT_POSTFIELDS => $postData,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
        ])->post('https://' . self::$auth_hostname . self::$auth_path);

        $this->debugLog($curl, __METHOD__);

        if (!$curl->errorCode) {
            $requestResult = json_decode($response, true);
            if ($requestResult && ArrayHelper::getValue($requestResult, 'result')) {

                ScanRecognizeParams::setValue('jwt_token', self::$jwt_token);

                return true;
            }
        }

        return false;
    }

    public function send(File $file)
    {
        $filepath = $file->getPath();

        $postData = array(
            'file' => new \CURLFile($filepath, mime_content_type($filepath), $file->filename_full),
        );

        $requestHeader = [
            'Cookie: ENTERA_JWT=' . self::$jwt_token,
            'Content-Type: multipart/form-data',
        ];

        $curl = new \common\components\curl\Curl();

        $response = $curl->setOptions([
            CURLOPT_HTTPHEADER => $requestHeader,
            CURLOPT_POSTFIELDS => $postData,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
        ])->post('https://' . self::$app_hostname . self::$upload_file_path . '?spaceId=' . self::$space_guid);

        $this->debugLog($curl, __METHOD__);

        if (!$curl->errorCode) {
            $requestResult = json_decode($response, true);
            if ($requestResult) {
                if (ArrayHelper::getValue($requestResult, 'result')) {
                    $recognitionTask = ArrayHelper::getValue($requestResult, 'recognitionTask');
                    self::$task_guid = ArrayHelper::getValue($recognitionTask, 'id');

                    return self::$task_guid;
                }

                Yii::warning("Entera->send(): " . $response, 'entera');
            }
        }

        return false;
    }

    public function get($taskGuid)
    {
        $requestHeader = [
            'Cookie: ENTERA_JWT=' . self::$jwt_token
        ];

        $curl = new \common\components\curl\Curl();

        $response = $curl->setOptions([
            CURLOPT_HTTPHEADER => $requestHeader,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
        ])->get('https://' . self::$app_hostname . self::$status_path . '/' . $taskGuid);

        $this->debugLog($curl, __METHOD__);

        if (!$curl->errorCode) {
            $requestResult = json_decode($response, true);
            if ($requestResult && ArrayHelper::getValue($requestResult, 'result')) {

                return $requestResult['recognitionTask'];
            }
        }

        return [];
    }

    public function checkApiAccess()
    {
        if (!self::$jwt_token)
            return false;

        $requestHeader = [
            'Cookie: ENTERA_JWT=' . self::$jwt_token
        ];

        $curl = new \common\components\curl\Curl();

        $response = $curl->setOptions([
            CURLOPT_HTTPHEADER => $requestHeader,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
        ])->get('https://' . self::$app_hostname . self::$user_path);

        $this->debugLog($curl, __METHOD__);

        if (!$curl->errorCode) {
            $requestResult = json_decode($response, true);
            if ($requestResult && ArrayHelper::getValue($requestResult, 'result')) {
                return true;
            }
        }

        return false;
    }

    public function isValidToken()
    {
        if ($this->checkApiAccess())
            return true;
        if ($this->login())
            return true;

        return false;
    }

    // CURL CALLBACK

    public static function curlResponseHeaderCallback($ch, $headerLine)
    {
        if (preg_match('/^Set-Cookie:\s*([^;]*)/mi', $headerLine, $cookie) == 1) {
            $c = explode('=', $cookie[1]);
            if (count($c) == 2 && $c[0] == 'ENTERA_JWT')
                self::$jwt_token = $c[1];
        }

        return strlen($headerLine); // Needed by curl
    }

    // LOGS

    /**
     * @return string
     */
    public static function curlLogMsg(Curl $curl)
    {
        if (method_exists($curl, 'getDump')) {
            return $curl->getDump();
        } else {
            $data = [
                'curl_info' => curl_getinfo($curl->curl),
                'curl_postfields' => ArrayHelper::getValue($curl->getOptions(), CURLOPT_POSTFIELDS),
                'response_headers' => $curl->responseHeaders,
                'response' => $curl->response,
            ];
            if ($curl->errorCode || $curl->errorText) {
                $data['error_code'] = $curl->errorCode;
                $data['error_text'] = $curl->errorText;
            }

            return VarDumper::dumpAsString($data);
        }
    }

    /**
     * @inheritdoc
     */
    public function debugLog(Curl $curl, $method)
    {
        if ($this->_debugMode) {
            $logFile = Yii::getAlias('@runtime/logs/debug_entera.log');
            file_put_contents($logFile, $method . "\n" . self::curlLogMsg($curl) . "\n\n", FILE_APPEND);
        }
    }

    /**
     * @inheritdoc
     */
    public function errorLog(Curl $curl = null, $method)
    {
        Yii::warning($method . "\n" . self::curlLogMsg($curl) . "\n", 'banking');
    }
}