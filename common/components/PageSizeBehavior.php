<?php
namespace common\components;

use Yii;
use yii\base\Behavior;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;

/**
 * Class PageSizeBehavior
 */
class PageSizeBehavior extends Behavior
{
    public $pageSizeAttribute = '_page_size';

    /**
     * @return array
     */
    protected function getPageSizeData()
    {
        return (array) (Json::decode($this->owner->{$this->pageSizeAttribute}) ? : []);
    }

    /**
     * @param array $data
     */
    protected function setPageSizeData($data)
    {
        $this->owner->updateAttributes([$this->pageSizeAttribute => Json::encode($data)]);
    }

    /**
     * @return integer
     */
    public function getPageSize(string $route, string $param)
    {
        return ArrayHelper::getValue($this->getPageSizeData(), [$route, $param]);
    }

    /**
     * @param string $route  Route ID
     * @param string $param  Page size GET parameter
     * @param integer $size  Page size value
     */
    public function setPageSize(string $route, string $param, $size)
    {
        $data = $this->getPageSizeData();
        $data[$route][$param] = $size;
        $this->setPageSizeData($data);
    }
}
