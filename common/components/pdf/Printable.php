<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 8.10.15
 * Time: 15.50
 * Email: t.kanstantsin@gmail.com
 */

namespace common\components\pdf;


/**
 * Interface Printable
 * @package common\components\pdf
 */
interface Printable
{

    /**
     * Returns pdf file name.
     * @return string
     */
    public function getPdfFileName();

    /**
     * Returns title for printed into browser sheet.
     * @return string
     */
    public function getPrintTitle();

}