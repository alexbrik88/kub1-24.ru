<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 8.10.15
 * Time: 14.16
 * Email: t.kanstantsin@gmail.com
 */

namespace common\components\pdf;

use Mpdf\Mpdf;
use common\models\document\Act;
use common\models\document\AgentReport;
use common\models\document\Invoice;
use common\models\document\InvoiceFacture;
use common\models\document\PackingList;
use common\models\document\PaymentOrder;
use common\models\document\Upd;
use common\models\Agreement;
use common\models\document\Waybill;
use frontend\assets\PrintAsset;
use frontend\models\Documents;
use Yii;
use yii\base\Component;
use yii\base\InvalidConfigException;
use yii\web\Controller;

/**
 * Class PdfRenderer
 * @package common\components\pdf
 */
class PdfRenderer extends Component
{
    const DISPLAY_MODE_FULLPAGE = 'fullpage';
    const DISPLAY_MODE_FULLWIDTH = 'fullwidth';
    const DISPLAY_MODE_REAL = 'real';
    const DISPLAY_MODE_DEFAULT = 'default';

    const DESTINATION_BROWSER = 'I';
    const DESTINATION_BROWSER_DOWNLOAD = 'D';
    const DESTINATION_FILE = 'F';
    const DESTINATION_STRING = 'S';

    /**
     * Html layout
     * @var string
     */
    public $layout = '@frontend/views/layouts/pdf';
    /**
     * Html view file
     * @var
     */
    public $view;
    /**
     * Paramt for rendering view
     * @var array
     */
    public $params = [];

    /**
     * Destination of pdf (file, browser, string)
     * @var string
     */
    public $destination = self::DESTINATION_BROWSER;
    /**
     * Headers for output (if browser). Not implemented yet.
     * @var
     */
    public $headers;
    /**
     * Title of document (not works)
     * @var
     */
    public $title;
    /**
     * Filename for saved file (on for `save as` in browser)
     * @var
     */
    public $filename;

    /**
     * @var string
     */
    public $displayMode = self::DISPLAY_MODE_FULLPAGE;
    /**
     * Format of pdf document. (e.g. A4-P -> A4 sheet with portrait orientation)
     * @var string
     */
    public $format = 'A4';

    public $mode = '';
    public $fontSize = 0;
    public $font = 'arial';
    public $marginLeft = 0;
    public $marginRight = 0;
    public $marginTop = 0;
    public $marginBottom = 0;
    public $marginHeader = 0;
    public $marginFooter = 0;
    public $orientation = 'P';

    /**
     * Current controller for rendering view.
     * @var Controller
     */
    private $_controller;

    /**
     * @inheritdoc
     */
    public function __construct($config = [])
    {
        defined('_MPDF_TTFONTDATAPATH') or define('_MPDF_TTFONTDATAPATH', \Yii::getAlias('@runtime/ttfontdata/'));
        if (!is_dir(_MPDF_TTFONTDATAPATH)) {
            \yii\helpers\FileHelper::createDirectory(_MPDF_TTFONTDATAPATH, 0775, true);
        }

        parent::__construct($config);
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        if ($this->title === null && $this->filename !== null) {
            $this->title = $this->filename;
        }
        if ($this->title !== null && $this->filename === null) {
            $this->filename = $this->title;
        }

        if ($this->_controller === null) {
            $this->_controller = \Yii::$app->controller;
        }

        if (!($this->_controller instanceof \yii\base\Controller)) {
            throw new InvalidConfigException('Controller must be instance of ' . \yii\base\Controller::className());
        }

        if (isset($this->params['layout'])) {
            $this->layout = $this->params['layout'];
            unset($this->params['layout']);
        }
    }

    /**
     * @param bool $terminate
     * @param int $dpi
     * @return string|\yii\console\Response|\yii\web\Response
     * @throws \MpdfException
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\NotFoundHttpException
     * @throws \yii\web\RangeNotSatisfiableHttpException
     */
    public function output($terminate = true, $dpi = 96)
    {
        $html = $this->render($this->layout, $this->view, $this->params);

        // Scale page
        if (!Yii::$app->request->isConsoleRequest) {
            if (strpos(Yii::$app->request->url, 'documents/upd') !== false
                || strpos(Yii::$app->request->url, 'documents/packing-list') !== false) {
                    $dpi = Yii::$app->request->get('dpi', 105);
            }
        }

        $mpdf = new Mpdf([
            'dpi' => $dpi,
            'img_dpi' => $dpi,
            'mode' => $this->mode,
            'format' => $this->format,
            'default_font_size' => $this->fontSize,
            'default_font' => $this->font,
            'margin_left' => $this->marginLeft,
            'margin_right' => $this->marginRight,
            'margin_top' => $this->marginTop,
            'margin_bottom' => $this->marginBottom,
            'margin_header' => $this->marginHeader,
            'margin_footer' => $this->marginFooter,
            'orientation' => $this->orientation,
        ]);
        $mpdf->SetDisplayMode($this->displayMode);

        if (isset($this->params['multiple'])) {
            $params = $this->params;
            foreach ($params['multiple'] as $id) {
                if ($this->params['model'] instanceof Invoice) {
                    $params['model'] = Invoice::findOne($id);
                } elseif ($this->params['model'] instanceof Act) {
                    if ($model = Act::findOne($id)) {
                        $model->setStatusPrinted();
                        $params['model'] = $model;
                    } else {
                        continue;
                    }
                } elseif ($this->params['model'] instanceof PackingList) {
                    if ($model = PackingList::findOne($id)) {
                        $model->setStatusPrinted();
                        $params['model'] = $model;
                    } else {
                        continue;
                    }
                } elseif ($this->params['model'] instanceof Waybill) {
                    if ($model = Waybill::findOne($id)) {
                        $model->setStatusPrinted();
                        $params['model'] = $model;
                    } else {
                        continue;
                    }
                } elseif ($this->params['model'] instanceof InvoiceFacture) {
                    if ($model = InvoiceFacture::findOne($id)) {
                        $model->setStatusPrinted();
                        $params['model'] = $model;
                    } else {
                        continue;
                    }
                } elseif ($this->params['model'] instanceof Upd) {
                    if ($model = Upd::findOne($id)) {
                        $model->setStatusPrinted();
                        $params['model'] = $model;
                    } else {
                        continue;
                    }
                } elseif ($this->params['model'] instanceof Agreement) {
                    if ($model = Agreement::findOne($id)) {
                        $model->setStatusPrinted();
                        $params['model'] = $model;
                    } else {
                        continue;
                    }
                } elseif ($this->params['model'] instanceof AgentReport) {
                    if ($model = AgentReport::findOne($id)) {
                        $model->setStatusPrinted();
                        $params['model'] = $model;
                    } else {
                        continue;
                    }
                } elseif ($this->params['model'] instanceof PaymentOrder) {
                    $params['model'] = PaymentOrder::findOne($id);
                }
                $mpdf->WriteHTML($this->render($this->layout, $this->view, $params));

                if ($id != end($params['multiple'])) $mpdf->AddPage();
            }
        } else {
            $mpdf->WriteHTML($html);
        }
        $output = $mpdf->Output($this->filename, $this->destination);

        if ($terminate) {
            return Yii::$app->response->sendContentAsFile($output, $this->filename, [
                'mimeType' => 'application/pdf',
                'inline' => true,
            ]);
        } else {
            return $output;
        }
    }

    public function outputTwoOrientations($terminate = true)
    {
        $html = $this->render($this->layout, $this->view, $this->params);
        $html_L = ($this->params['viewL']) ? $this->render($this->layout, $this->params['viewL'], $this->params) : '';

        $mpdf = new Mpdf([
            'mode' => $this->mode,
            'format' => $this->format,
            'default_font_size' => $this->fontSize,
            'default_font' => $this->font,
            'margin_left' => $this->marginLeft,
            'margin_right' => $this->marginRight,
            'margin_top' => $this->marginTop,
            'margin_bottom' => $this->marginBottom,
            'margin_header' => $this->marginHeader,
            'margin_footer' => $this->marginFooter,
            'orientation' => $this->orientation,
        ]);
        $mpdf->SetDisplayMode($this->displayMode);
        $mpdf->AddPage('P');
        $mpdf->WriteHTML($html);
        if ($html_L) {
            $mpdf->setHeader();    // Clear headers before adding page
            $mpdf->AddPage('L');
            $mpdf->WriteHTML($html_L);
        }

        $output = $mpdf->Output($this->filename, $this->destination);

        if ($terminate) {
            return Yii::$app->response->sendContentAsFile($output, $this->filename, [
                'mimeType' => 'application/pdf',
                'inline' => true,
            ]);
        } else {
            return $output;
        }
    }
    public function renderHtmlTwoOrientations($viewL = null)
    {
        $render = $this->render($this->layout, $this->view, $this->params);
        if ($viewL)
            $render .= $this->render($this->layout, $viewL, $this->params);

        return $render;
    }

    /**
     * Renders view into string.
     * @return string
     */
    public function renderHtml()
    {
        return $this->render($this->layout, $this->view, $this->params);
    }

    /**
     * Renders view into string.
     *
     * @param string $layout
     * @param string $view
     * @param array $params
     * @return string
     */
    protected function render($layout, $view, $params)
    {
        $this->_controller->layout = $layout;
        if (empty($this->_controller->view->params['asset'])) {
            $this->_controller->view->params['asset'] = PrintAsset::className();
        }

        return $this->_controller->render($view, $params);
    }
}
