<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 19.12.2018
 * Time: 23:31
 */

namespace common\components\pdf;


class Mpdf extends \mPDF
{
    function _putpatterns()
    {
        for ($i = 1; $i <= count($this->patterns); $i++) {
            $x = $this->patterns[$i]['x'];
            $y = $this->patterns[$i]['y'];
            $w = $this->patterns[$i]['w'];
            $h = $this->patterns[$i]['h'];
            $pgh = $this->patterns[$i]['pgh'];
            $orig_w = $this->patterns[$i]['orig_w'];
            $orig_h = $this->patterns[$i]['orig_h'];
            $image_id = $this->patterns[$i]['image_id'];
            $itype = $this->patterns[$i]['itype'];
            if (isset($this->patterns[$i]['bpa'])) {
                $bpa = $this->patterns[$i]['bpa'];
            } // background positioning area
            else {
                $bpa = array();
            }

            if ($this->patterns[$i]['x_repeat']) {
                $x_repeat = true;
            } else {
                $x_repeat = false;
            }
            if ($this->patterns[$i]['y_repeat']) {
                $y_repeat = true;
            } else {
                $y_repeat = false;
            }
            $x_pos = $this->patterns[$i]['x_pos'];
            if (stristr($x_pos, '%')) {
                $x_pos += 0;
                $x_pos /= 100;
                if (isset($bpa['w']) && $bpa['w'])
                    $x_pos = ($bpa['w'] * $x_pos) - ($orig_w / _MPDFK * $x_pos);
                else
                    $x_pos = ($w * $x_pos) - ($orig_w / _MPDFK * $x_pos);
            }
            $y_pos = $this->patterns[$i]['y_pos'];
            if (stristr($y_pos, '%')) {
                $y_pos += 0;
                $y_pos /= 100;
                if (isset($bpa['h']) && $bpa['h'])
                    $y_pos = ($bpa['h'] * $y_pos) - ($orig_h / _MPDFK * $y_pos);
                else
                    $y_pos = ($h * $y_pos) - ($orig_h / _MPDFK * $y_pos);
            }
            if (isset($bpa['x']) && $bpa['x'])
                $adj_x = ($x_pos + $bpa['x']) * _MPDFK;
            else
                $adj_x = ($x_pos + $x) * _MPDFK;
            if (isset($bpa['y']) && $bpa['y'])
                $adj_y = (($pgh - $y_pos - $bpa['y']) * _MPDFK) - $orig_h;
            else
                $adj_y = (($pgh - $y_pos - $y) * _MPDFK) - $orig_h;
            $img_obj = false;
            if ($itype == 'svg' || $itype == 'wmf') {
                foreach ($this->formobjects AS $fo) {
                    if ($fo['i'] == $image_id) {
                        $img_obj = $fo['n'];
                        $fo_w = $fo['w'];
                        $fo_h = -$fo['h'];
                        $wmf_x = $fo['x'];
                        $wmf_y = $fo['y'];
                        break;
                    }
                }
            } else {
                foreach ($this->images AS $img) {
                    if ($img['i'] == $image_id) {
                        $img_obj = $img['n'];
                        break;
                    }
                }
            }
            if (!$img_obj) {
                echo "Problem: Image object not found for background pattern " . $img['i'];
                exit;
            }

            $this->_newobj();
            $this->_out('<</ProcSet [/PDF /Text /ImageB /ImageC /ImageI]');
            if ($itype == 'svg' || $itype == 'wmf') {
                $this->_out('/XObject <</FO' . $image_id . ' ' . $img_obj . ' 0 R >>');
                // ******* ADD ANY ExtGStates, Shading AND Fonts needed for the FormObject
                // Set in classes/svg array['fo'] = true
                // Required that _putshaders comes before _putpatterns in _putresources
                // This adds any resources associated with any FormObject to every Formobject - overkill but works!
                if (count($this->extgstates)) {
                    $this->_out('/ExtGState <<');
                    foreach ($this->extgstates as $k => $extgstate)
                        if (isset($extgstate['fo']) && $extgstate['fo']) {
                            if (isset($extgstate['trans']))
                                $this->_out('/' . $extgstate['trans'] . ' ' . $extgstate['n'] . ' 0 R');
                            else
                                $this->_out('/GS' . $k . ' ' . $extgstate['n'] . ' 0 R');
                        }
                    $this->_out('>>');
                }
                /* -- BACKGROUNDS -- */
                if (isset($this->gradients) AND ( count($this->gradients) > 0)) {
                    $this->_out('/Shading <<');
                    foreach ($this->gradients as $id => $grad) {
                        if (isset($grad['fo']) && $grad['fo']) {
                            $this->_out('/Sh' . $id . ' ' . $grad['id'] . ' 0 R');
                        }
                    }
                    $this->_out('>>');
                }
                /* -- END BACKGROUNDS -- */
                $this->_out('/Font <<');
                foreach ($this->fonts as $font) {
                    if (!$font['used'] && $font['type'] == 'TTF') {
                        continue;
                    }
                    if (isset($font['fo']) && $font['fo']) {
                        if ($font['type'] == 'TTF' && ($font['sip'] || $font['smp'])) {
                            foreach ($font['n'] AS $k => $fid) {
                                $this->_out('/F' . $font['subsetfontids'][$k] . ' ' . $font['n'][$k] . ' 0 R');
                            }
                        } else {
                            $this->_out('/F' . $font['i'] . ' ' . $font['n'] . ' 0 R');
                        }
                    }
                }
                $this->_out('>>');
            } else {
                $this->_out('/XObject <</I' . $image_id . ' ' . $img_obj . ' 0 R >>');
            }
            $this->_out('>>');
            $this->_out('endobj');

            $this->_newobj();
            $this->patterns[$i]['n'] = $this->n;
            $this->_out('<< /Type /Pattern /PatternType 1 /PaintType 1 /TilingType 2');
            $this->_out('/Resources ' . ($this->n - 1) . ' 0 R');

            $this->_out(sprintf('/BBox [0 0 %.3F %.3F]', $orig_w, $orig_h));
            if ($x_repeat) {
                $this->_out(sprintf('/XStep %.3F', $orig_w));
            } else {
                $this->_out(sprintf('/XStep %d', 1024));
            }
            if ($y_repeat) {
                $this->_out(sprintf('/YStep %.3F', $orig_h));
            } else {
                $this->_out(sprintf('/YStep %d', 1024));
            }

            if ($itype == 'svg' || $itype == 'wmf') {
                $this->_out(sprintf('/Matrix [1 0 0 -1 %.3F %.3F]', $adj_x, ($adj_y + $orig_h)));
                $s = sprintf("q %.3F 0 0 %.3F %.3F %.3F cm /FO%d Do Q", ($orig_w / $fo_w), (-$orig_h / $fo_h), -($orig_w / $fo_w) * $wmf_x, ($orig_w / $fo_w) * $wmf_y, $image_id);
            } else {
                $this->_out(sprintf('/Matrix [1 0 0 1 %.3F %.3F]', $adj_x, $adj_y));
                $s = sprintf("q %.3F 0 0 %.3F 0 0 cm /I%d Do Q", $orig_w, $orig_h, $image_id);
            }

            if ($this->compress) {
                $this->_out('/Filter /FlateDecode');
                $s = gzcompress($s);
            }
            $this->_out('/Length ' . strlen($s) . '>>');
            $this->_putstream($s);
            $this->_out('endobj');
        }
    }
}