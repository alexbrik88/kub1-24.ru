<?php

namespace common\components\moysklad\helper;

use common\components\moysklad\MoyskladImport;
use common\components\moysklad\MoyskladObjectType as ObjectType;
use common\components\moysklad\MoyskladDocumentType as DocumentType;

class MoyskladImportStatistics {

    public static $statStructure = [
        // dict
        ObjectType::CONTRACTOR => 'Контрагенты',
        ObjectType::NOMENCLATURE => 'Товары / Услуги',
        // doc
        DocumentType::IN_INVOICE => 'Вх. Счета',
        DocumentType::IN_UPD => 'Вх. УПД',
        DocumentType::IN_INVOICE_FACTURE => 'Вх. Счета-фактуры',
        DocumentType::OUT_INVOICE => 'Исх. Счета',
        DocumentType::OUT_UPD => 'Исх. УПД',
        DocumentType::OUT_INVOICE_FACTURE => 'Исх. Счета-фактуры',
        // flow as doc
        DocumentType::IN_PAYMENT_ORDER => 'Расх. кассовые ордера',
        DocumentType::OUT_PAYMENT_ORDER => 'Прих. кассовые ордера',
    ];

    public $file_data;
    public $doubles_data;
    public $uploaded_data;

    private $_import;

    public function __construct(MoyskladImport $import)
    {
        $this->_import = $import;
        $this->file_data = ($import->file_data) ? unserialize($import->file_data) : [];
        $this->doubles_data = ($import->doubles_data) ? unserialize($import->doubles_data) : [];
        $this->uploaded_data = ($import->uploaded_data) ? unserialize($import->uploaded_data) : [];
    }

    public function update()
    {
        $this->beforeUpdate();

        $this->_import->updateAttributes([
            'file_data' => serialize($this->file_data),
            'doubles_data' => serialize($this->doubles_data),
            'uploaded_data' => serialize($this->uploaded_data),
        ]);
    }

    public function incrementFile($key, $value = 1)
    {
        if (!isset($this->file_data[$key]))
            $this->file_data[$key] = 0;

        $this->file_data[$key] += $value;
    }

    public function incrementDouble($key, $value = 1)
    {
        if (!isset($this->doubles_data[$key]))
            $this->doubles_data[$key] = 0;

        $this->doubles_data[$key] += $value;
    }

    public function incrementUploaded($key, $value = 1)
    {
        if (!isset($this->uploaded_data[$key]))
            $this->uploaded_data[$key] = 0;

        $this->uploaded_data[$key] += $value;
    }

    private function beforeUpdate()
    {
        ksort($this->file_data);
        ksort($this->doubles_data);
        ksort($this->uploaded_data);
    }
}