<?php

namespace common\components\moysklad\helper;

use common\components\moysklad\MoyskladDocumentValidation;
use common\models\Agreement;
use common\models\Contractor;
use common\models\ContractorAccount;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\InvoiceFacture;
use common\models\document\Order;
use common\models\document\OrderAct;
use common\models\document\OrderPackingList;
use common\models\document\OrderUpd;
use common\models\document\PackingList;
use common\models\document\Upd;
use common\components\moysklad\MoyskladDocument;
use common\components\moysklad\MoyskladImport;
use common\components\moysklad\MoyskladObject;
use common\components\moysklad\MoyskladObjectValidation;

class MoyskladValidationErrors {

    private $_import;

    /**
     * @param MoyskladImport $import
     */
    public function __construct(MoyskladImport $import)
    {
        $this->_import = $import;
    }

    /**
     * @param MoyskladObject $object
     * @param Contractor|ContractorAccount|Agreement $kubObject
     */
    public function addObjectError($object, $kubObject)
    {
        $e = new MoyskladObjectValidation([
            'import_id' => $this->_import->id,
            'object_id' => $object->id,
            'errors' => serialize($kubObject->getErrors())
        ]);

        try {
            $e->save(false);
        } catch (\Throwable $e) {
            // todo: add global error and exit from import
        }
    }

    /**
     * @param MoyskladDocument $document
     * @param PackingList|Act|Upd|Invoice|InvoiceFacture $kubDocument
     * @param null|Order|OrderUpd|OrderAct|OrderPackingList
     */
    public function addDocumentError($document, $kubDocument, $kubOrder = null)
    {
        $e = new MoyskladDocumentValidation([
            'import_id' => $this->_import->id,
            'document_id' => $document->id,
            'errors' => ($kubOrder)
                ? serialize($kubOrder->getErrors())
                : serialize($kubDocument->getErrors())
        ]);
        try {
            $e->save(false);
        } catch (\Throwable $e) {
            // todo: add global error and exit from import
        }
    }
}