<?php

namespace common\components\moysklad;

use common\components\moysklad\MoyskladObjectType as ObjectType;
use common\models\document\query\InvoiceQuery;
use Yii;
use common\models\Company;
use common\models\Contractor;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\OrderDocument;
use common\models\document\PackingList;
use common\models\document\Upd;
use common\models\document\InvoiceFacture;
use common\models\cash\CashOrderFlows;
use common\components\moysklad\MoyskladDocumentType as DocumentType;
use yii\db\ActiveQuery;
use yii\db\Expression;

/**
 * This is the model class for table "moysklad_document".
 *
 * @property int $id
 * @property int $company_id
 * @property int $document_type_id
 * @property int $document_type 1 - in, 2 - out
 * @property string|null $document_date
 * @property string|null $document_number
 * @property string|null $document_amount
 * @property string|null $contractor_inn
 * @property string|null $contractor_kpp
 * @property string|null $contractor_name
 * @property int|null $act_id
 * @property int|null $order_document_id
 * @property int|null $invoice_id
 * @property int|null $packing_list_id
 * @property int|null $upd_id
 * @property int|null $invoice_facture_id
 * @property int|null $cash_order_flows_id
 * @property int|null $is_validated
 * @property int|null $validated_at
 * @property string|null $one_c_object
 *
 * @property Act $act
 * @property Company $company
 * @property Contractor $contractor
 * @property MoyskladDocumentType $documentType
 * @property Invoice $invoice
 * @property OrderDocument $orderDocument
 * @property PackingList $packingList
 * @property Upd $upd
 * @property InvoiceFacture $invoiceFacture
 * @property CashOrderFlows $cashOrderFlows
 */
class MoyskladDocument extends \yii\db\ActiveRecord
{
    const IO_TYPE_IN = 1;
    const IO_TYPE_OUT = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'moysklad_document';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'document_type_id', 'document_type'], 'required'],
            [['company_id', 'document_type_id', 'document_type', 'act_id', 'order_document_id', 'invoice_id', 'packing_list_id', 'upd_id', 'invoice_facture_id', 'is_validated', 'validated_at'], 'integer'],
            [['contractor_inn', 'contractor_kpp', 'contractor_name', 'document_date'], 'safe'],
            [['one_c_object'], 'string'],
            [['document_number'], 'string', 'max' => 64],
            [['document_amount'], 'string', 'max' => 32],
            [['act_id'], 'exist', 'skipOnError' => true, 'targetClass' => Act::class, 'targetAttribute' => ['act_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['company_id' => 'id']],
            [['document_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => MoyskladDocumentType::class, 'targetAttribute' => ['document_type_id' => 'id']],
            [['invoice_id'], 'exist', 'skipOnError' => true, 'targetClass' => Invoice::class, 'targetAttribute' => ['invoice_id' => 'id']],
            [['order_document_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderDocument::class, 'targetAttribute' => ['order_document_id' => 'id']],
            [['packing_list_id'], 'exist', 'skipOnError' => true, 'targetClass' => PackingList::class, 'targetAttribute' => ['packing_list_id' => 'id']],
            [['upd_id'], 'exist', 'skipOnError' => true, 'targetClass' => Upd::class, 'targetAttribute' => ['upd_id' => 'id']],
            [['invoice_facture_id'], 'exist', 'skipOnError' => true, 'targetClass' => InvoiceFacture::class, 'targetAttribute' => ['invoice_facture_id' => 'id']],
            [['cash_order_flows_id'], 'exist', 'skipOnError' => true, 'targetClass' => CashOrderFlows::class, 'targetAttribute' => ['cash_order_flows_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'document_type_id' => 'Document Type ID',
            'document_type' => '1 - in, 2 - out',
            'document_date' => 'Document Date',
            'document_number' => 'Document Number',
            'document_amount' => 'Document Sum',
            'act_id' => 'Act ID',
            'order_document_id' => 'Order Document ID',
            'invoice_id' => 'Invoice ID',
            'packing_list_id' => 'Packing List ID',
            'upd_id' => 'Upd ID',
            'invoice_facture_id' => 'Invoice Facture ID',
            'cash_order_flows_id' => 'Cash Order Flows ID',
            'is_validated' => 'Is Validated',
            'validated_at' => 'Validated At',
            'one_c_object' => 'One C Object',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAct()
    {
        return $this->hasOne(Act::class, ['id' => 'act_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getContractor()
    {
        return $this->hasOne(Contractor::class, ['id' => 'contractor_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDocumentType()
    {
        return $this->hasOne(MoyskladDocumentType::class, ['id' => 'document_type_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoice()
    {
        return $this->hasOne(Invoice::class, ['id' => 'invoice_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getOrderDocument()
    {
        return $this->hasOne(OrderDocument::class, ['id' => 'order_document_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPackingList()
    {
        return $this->hasOne(PackingList::class, ['id' => 'packing_list_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpd()
    {
        return $this->hasOne(Upd::class, ['id' => 'upd_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getInvoiceFacture()
    {
        return $this->hasOne(InvoiceFacture::class, ['id' => 'invoice_facture_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCashOrderFlows()
    {
        return $this->hasOne(CashOrderFlows::class, ['id' => 'cash_order_flows_id']);
    }

    /**
     * @return query\MoyskladDocumentQuery
     */
    public static function find($config = [])
    {
        return new query\MoyskladDocumentQuery(get_called_class(), $config);
    }

    public function setForeignKey($fkID)
    {
        if ($foreignKeyName = $this->getCurrentForeignKeyName()) {
            $this->updateAttributes([
                $foreignKeyName => $fkID,
                'is_validated' => 1,
                'validated_at' => time()
            ]);
        } else {
            $this->updateAttributes([
                'is_validated' => 1,
                'validated_at' => time()
            ]);
        }
    }

    public function setActAndPackingListForeignKeys($fkAct, $fkPackingList)
    {
        $this->updateAttributes([
            'act_id' => $fkAct,
            'packing_list_id' => $fkPackingList,
            'is_validated' => 1,
            'validated_at' => time()
        ]);
    }

    public function setNoForeign()
    {
        $this->updateAttributes([
            'is_validated' => 0,
            'validated_at' => time()
        ]);
    }

    public function setKubInvoiceId($invoiceID)
    {
        $this->updateAttributes([
            'invoice_id' => $invoiceID,
        ]);
    }

    public function getKubInvoiceId()
    {
        return $this->invoice_id;
    }

    public function getForeignKey()
    {
        if ($foreignKeyName = $this->getCurrentForeignKeyName())
            return $this->{$foreignKeyName};

        return null;
    }

    private function getCurrentForeignKeyName()
    {
        switch ($this->document_type_id) {
            case DocumentType::IN_INVOICE:
            case DocumentType::OUT_INVOICE:
                return 'invoice_id';
            case DocumentType::IN_UPD:
            case DocumentType::OUT_UPD:
                return 'upd_id';
            case DocumentType::IN_INVOICE_FACTURE:
            case DocumentType::OUT_INVOICE_FACTURE:
                return 'invoice_facture_id';
            case DocumentType::IN_PAYMENT_ORDER:
            case DocumentType::OUT_PAYMENT_ORDER:
                return 'cash_order_flows_id';
            default:
                return null;
        }
    }

    /**
     * @return array
     */
    public function getData()
    {
        $data = ($this->one_c_object) ? unserialize($this->one_c_object) : [];
        $data['kub_contractor_id'] = $this->getKubContractorId();

        return $data;
    }

    /**
     * @return false|string|null
     */
    public function getKubContractorId()
    {
        $query = MoyskladObject::find()
            ->where([
                'company_id' => $this->company_id,
                'object_type_id' => ObjectType::CONTRACTOR
            ])
            ->select('contractor_id');

        if ($this->contractor_inn) {

            return $query->andWhere([
                'object_inn' => $this->contractor_inn,
                'object_kpp' => $this->contractor_kpp,
            ])->scalar();
        }
        elseif ($this->contractor_name) {

            return $query->andWhere([
                'object_name' => $this->contractor_name
            ])->scalar();
        }

        return null;
    }

    /**
     * @return Contractor|null
     */
    public function getKubContractor()
    {
        $contractorId = $this->getKubContractorId();

        if ($contractorId)
            return Contractor::findOne([
               'id' => $contractorId,
               'company_id' => $this->company_id
            ]);

        return null;
    }
}
