<?php

namespace common\components\moysklad;

use Yii;
use yii\helpers\Html;
use common\models\Company;
use yii\helpers\ArrayHelper;
use common\models\product\Store;
use common\models\employee\Employee;
use common\components\date\DateHelper;
use common\components\moysklad\parser\MoyskladParser;
use common\components\moysklad\converter\MoyskladConverter;
use common\components\moysklad\helper\MoyskladImportStatistics;

/**
 * This is the model class for table "moysklad_import".
 *
 * @property int $id
 * @property int $company_id
 * @property int $employee_id
 * @property int $store_id
 * @property int $document_format_id
 * @property int|null $loading_step
 * @property string|null $file_data
 * @property string|null $doubles_data
 * @property string|null $uploaded_data
 * @property int|null $created_at
 *
 * @property Company $company
 * @property Employee $employee
 * @property Store $store
 */
class MoyskladImport extends \yii\db\ActiveRecord
{
    const STEP_PARSING_FILE = 1;
    const STEP_PREPARING_OBJECTS = 2;
    const STEP_CONTRACTORS = 3;
    const STEP_PRODUCTS = 4;
    const STEP_INVOICES = 5;
    const STEP_UPDS = 6;
    const STEP_FACTURES = 7;
    //
    const STEP_END = 0;
    const STEP_END_WITH_ERROR = -1;

    const DOCUMENT_FORMAT_UPD = 1;
    const DOCUMENT_FORMAT_ACT_WITH_PACKING_LIST = 2;

    /**
     * @var MoyskladParser
     */
    public $parser;

    /**
     * @var MoyskladConverter
     */
    public $converter;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'moysklad_import';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'employee_id', 'store_id', 'document_format_id'], 'required'],
            [['company_id', 'employee_id', 'store_id', 'document_format_id', 'loading_step', 'created_at'], 'integer'],
            [['file_data', 'doubles_data', 'uploaded_data'], 'string'],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['company_id' => 'id']],
            [['employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::class, 'targetAttribute' => ['employee_id' => 'id']],
            [['store_id'], 'exist', 'skipOnError' => true, 'targetClass' => Store::class, 'targetAttribute' => ['store_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'employee_id' => 'Employee ID',
            'document_format_id' => 'Document Format ID',
            'loading_step' => 'Loading Step',
            'file_data' => 'File Data',
            'doubles_data' => 'Doubles Data',
            'uploaded_data' => 'Uploaded Data',
            'created_at' => 'Created At',
        ];
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * Gets query for [[Employee]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::class, ['id' => 'employee_id']);
    }

    /**
     * Gets query for [[Store]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStore()
    {
        return $this->hasOne(Store::class, ['id' => 'employee_id']);
    }

    /**
     * @return bool
     */
    public function asUpd()
    {
        return $this->document_format_id == self::DOCUMENT_FORMAT_UPD;
    }

    /**
     * @return string
     */
    public function getStepDescription()
    {
        switch ($this->loading_step) {
            case self::STEP_PARSING_FILE:
                return 'Парсинг файла...';
            case self::STEP_PREPARING_OBJECTS:
                return 'Подготовка объектов...';
            case self::STEP_CONTRACTORS:
                return 'Импорт контрагентов...';
            case self::STEP_PRODUCTS:
                return 'Импорт товаров...';
            case self::STEP_INVOICES:
                return 'Импорт счетов...';
            case self::STEP_UPDS:
                return 'Импорт документов...';
            case self::STEP_FACTURES:
                return 'Импорт счетов-фактур...';
            case self::STEP_END:
                return 'Импорт успешно завершен.';
            case self::STEP_END_WITH_ERROR:
                return 'Импорт не завершен.';
        }

        return '---';
    }

    /**
     * @param $stepNum
     */
    public function progressStep($stepNum)
    {
        $this->updateAttributes(['loading_step' => (int)$stepNum]);
    }

    public function setRuntimeError(\Throwable $e)
    {
        $this->updateAttributes(['runtime_error' => $this->getRuntimeErrorMessage($e)]);
    }

    public function getRuntimeErrorMessage(\Throwable $e)
    {
        $errorLine = $e->getLine();
        $errorFile = substr($e->getFile(), strrpos($e->getFile(), '\\') + 1);
        $errorMessage = substr($e->getMessage(), 0, 200);

        return $errorMessage.' ('.$errorFile.':'.$errorLine.')';
    }

    public function getHtmlStatistics()
    {
        $fileData = ($this->file_data) ? unserialize($this->file_data) : [];
        $doublesData = ($this->doubles_data) ? unserialize($this->doubles_data) : [];
        $uploadedData = ($this->uploaded_data) ? unserialize($this->uploaded_data) : [];

        $ret = '';
        $ret .= Html::beginTag('table', ['class' => 'table table-style table-count-list table-compact w-100 mt-2 font-14']);
            $ret .= Html::beginTag('thead');
            $ret .= Html::beginTag('tr');
                $ret .= Html::tag('th', 'Тип объекта', ['class' => 'nowrap']);
                $ret .= Html::tag('th', 'Найдено', ['class' => 'nowrap']);
                $ret .= Html::tag('th', 'Дубли загрузки', ['class' => 'nowrap']);
                $ret .= Html::tag('th', 'Загружено', ['class' => 'nowrap']);
            $ret .= Html::endTag('tr');
            $ret .= Html::endTag('thead');
            $ret .= Html::beginTag('tbody');
            foreach (MoyskladImportStatistics::$statStructure as $tagId => $tagName) {
                $ret .= Html::beginTag('tr');
                    $ret .= Html::tag('td', $tagName);
                    $ret .= Html::tag('td', ArrayHelper::getValue($fileData, $tagId, 0));
                    $ret .= Html::tag('td', ArrayHelper::getValue($doublesData, $tagId, 0));
                    $ret .= Html::tag('td', ArrayHelper::getValue($uploadedData, $tagId, 0));
                $ret .= Html::endTag('tr');
            }
            $ret .= Html::endTag('tbody');
        $ret .= Html::endTag('table');

        return $ret;
    }

    public function getHtmlValidationObjects()
    {
        $objectValidation = MoyskladObjectValidation::find()
            ->where(['import_id' => $this->id])
            ->indexBy('object_id')
            ->select(['errors', 'object_id'])
            ->column();

        if (empty($objectValidation)) {
            $ret = '<div class="mt-2 mb-2 font-14" style="padding-left: 10px">Ошибок в справочниках нет</div>';
            return $ret;
        }

        $ret = '<div class="mt-2 mb-2 bold font-14" style="padding-left: 10px">Ошибки в справочниках ('.count($objectValidation).')</div>';
        $ret .= Html::beginTag('table', ['class' => 'table table-style table-count-list table-compact w-100 mt-2 font-14']);
        $ret .= Html::beginTag('thead');
        $ret .= Html::beginTag('tr');
        $ret .= Html::tag('th', 'Тип объекта', ['class' => 'nowrap']);
        $ret .= Html::tag('th', 'Номер / Код', ['class' => 'nowrap']);
        $ret .= Html::tag('th', 'Название / Контрагент', ['class' => 'nowrap']);
        $ret .= Html::tag('th', 'Ошибки', ['class' => 'nowrap']);
        $ret .= Html::endTag('tr');
        $ret .= Html::endTag('thead');
        $ret .= Html::beginTag('tbody');

        foreach ($objectValidation as $objectId => $errors) {

            $object = MoyskladObject::findOne($objectId);
            $errors = (array)unserialize($errors);

            if (!$object || !$errors)
                continue;

            $errorsText = '';
            foreach ($errors as $errorKey => $err) {
                $errorsText .= implode('<br/>', $err);
            }
            $columnsData = [
                ArrayHelper::getValue(MoyskladParser::$SOURCE_OBJECTS, $object->object_type_id),
                $object->object_number ?: ($object->object_code ?: ''),
                $object->object_name ?: '',
                $errorsText
            ];

            $ret .= Html::beginTag('tr');
            $ret .= Html::tag('td', $columnsData[0]);
            $ret .= Html::tag('td', $columnsData[1]);
            $ret .= Html::tag('td', $columnsData[2]);
            $ret .= Html::tag('td', $columnsData[3]);
            $ret .= Html::endTag('tr');
        }
        $ret .= Html::endTag('tbody');
        $ret .= Html::endTag('table');

        return $ret;
    }

    public function getHtmlValidationDocuments()
    {
        $documentValidation = MoyskladDocumentValidation::find()
            ->where(['import_id' => $this->id])
            ->indexBy('document_id')
            ->select(['errors', 'document_id'])
            ->column();

        if (empty($documentValidation)) {
            $ret = '<div class="mt-2 mb-2 font-14" style="padding-left: 10px">Ошибок в документах нет</div>';
            return $ret;
        }

        $ret = '<div class="mt-2 mb-2 bold font-14" style="padding-left: 10px">Ошибки в документах ('.count($documentValidation).')</div>';
        $ret .= Html::beginTag('table', ['class' => 'table table-style table-count-list table-compact w-100 mt-2 font-14']);
        $ret .= Html::beginTag('thead');
        $ret .= Html::beginTag('tr');
        $ret .= Html::tag('th', 'Тип документа', ['class' => 'nowrap']);
        $ret .= Html::tag('th', 'Номер', ['class' => 'nowrap']);
        $ret .= Html::tag('th', 'Дата', ['class' => 'nowrap']);
        $ret .= Html::tag('th', 'Контрагент', ['class' => 'nowrap']);
        $ret .= Html::tag('th', 'Ошибки', ['class' => 'nowrap']);
        $ret .= Html::endTag('tr');
        $ret .= Html::endTag('thead');
        $ret .= Html::beginTag('tbody');

        foreach ($documentValidation as $documentId => $errors) {

            $document = MoyskladDocument::findOne($documentId);
            $errors = (array)unserialize($errors);

            if (!$document || !$errors)
                continue;

            $errorsText = '';
            foreach ($errors as $errorKey => $err) {
                $errorsText .= implode('<br/>', $err);
            }
            $columnsData = [
                ArrayHelper::getValue(MoyskladParser::$SOURCE_DOCUMENTS, $document->document_type_id),
                $document->document_number ?: '',
                ($document->document_date) ? (DateHelper::format($document->document_date, 'd.m.Y', 'Y-m-d')) : '',
                $document->contractor_name ?: '',
                $errorsText
            ];

            $ret .= Html::beginTag('tr');
            $ret .= Html::tag('td', $columnsData[0]);
            $ret .= Html::tag('td', $columnsData[1]);
            $ret .= Html::tag('td', $columnsData[2]);
            $ret .= Html::tag('td', $columnsData[3]);
            $ret .= Html::tag('td', $columnsData[4]);
            $ret .= Html::endTag('tr');
        }
        $ret .= Html::endTag('tbody');
        $ret .= Html::endTag('table');

        return $ret;
    }    
}
