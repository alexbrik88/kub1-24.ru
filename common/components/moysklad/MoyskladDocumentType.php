<?php

namespace common\components\moysklad;

use Yii;

/**
 * This is the model class for table "moysklad_document_type".
 *
 * @property int $id
 * @property string|null $name
 *
 */
class MoyskladDocumentType extends \yii\db\ActiveRecord
{
    const IN_INVOICE = 11;
    const IN_UPD = 12;
    const IN_INVOICE_FACTURE = 13;
    const IN_PAYMENT_ORDER = 14;

    const OUT_INVOICE = 21;
    const OUT_UPD = 22;
    const OUT_INVOICE_FACTURE = 23;
    const OUT_PAYMENT_ORDER = 24;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'moysklad_document_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }
}
