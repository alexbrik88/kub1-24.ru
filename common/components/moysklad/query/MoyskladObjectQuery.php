<?php
namespace common\components\moysklad\query;

use common\components\moysklad\MoyskladDocument;
use common\components\moysklad\MoyskladObject;
use common\models\document\query\IIOStrictQuery;
use common\models\ICompanyStrictQuery;
use yii\db\ActiveQuery;
use yii\db\Expression;

class MoyskladObjectQuery extends ActiveQuery implements ICompanyStrictQuery {

    /**
     * @return MoyskladObjectQuery
     */
    public function byCompany($companyId)
    {
        return $this->andWhere([
            MoyskladObject::tableName().'.company_id' => $companyId
        ]);
    }

    /**
     * @return MoyskladObjectQuery
     */
    public function byObjectType($objectTypeId)
    {
        return $this->andWhere([
            MoyskladObject::tableName().'.object_type_id' => $objectTypeId
        ]);
    }

    /**
     * @param $number
     * @return MoyskladObjectQuery
     */
    public function byNumber($number)
    {
        return $this->andWhere([
            'object_number' => $number,
        ]);
    }

    /**
     * @param $code
     * @param $name
     * @return MoyskladObjectQuery
     */
    public function byCodeOrName($code, $name)
    {

        if(strlen(trim($code))) {
            return $this->andWhere([
                'object_code' => $code
            ]);
        } else {
            return $this->andWhere([
                'object_name' => $name
            ]);
        }
    }

    /**
     * @param $name
     * @return MoyskladObjectQuery
     */
    public function byName($name)
    {
        return $this->andWhere([
            'object_name' => $name,
        ]);
    }

    /**
     * @return MoyskladObjectQuery
     */
    public function byValidated()
    {
        return $this->andWhere([
            MoyskladObject::tableName().'.is_validated' => 1
        ]);
    }

    /**
     * @return MoyskladObjectQuery
     */
    public function byNew()
    {
        return $this->andWhere([
            MoyskladObject::tableName().'.validated_at' => null
        ]);
    }

    /**
     * @return MoyskladObjectQuery
     */
    public function byContractor($params)
    {
        if (isset($params['name']) && isset($params['inn']) && isset($params['kpp']) && count($params) === 3) {
            if (!empty($params['inn'])) {
                return $this->andWhere([
                    MoyskladObject::tableName().'.object_inn' => $params['inn'],
                    MoyskladObject::tableName().'.object_kpp' => $params['kpp']
                ]);
            } else {
                return $this->andWhere([
                    MoyskladObject::tableName().'.object_name' => $params['name']
                ]);
            }
        }

        return $this->andWhere(new Expression('1=0'));
    }
}