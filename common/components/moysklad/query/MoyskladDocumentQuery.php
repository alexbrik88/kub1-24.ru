<?php
namespace common\components\moysklad\query;

use common\components\moysklad\MoyskladDocument;
use common\models\document\query\IIOStrictQuery;
use common\models\ICompanyStrictQuery;
use yii\db\ActiveQuery;
use yii\db\Expression;

class MoyskladDocumentQuery extends ActiveQuery implements ICompanyStrictQuery, IIOStrictQuery {

    /**
     * @return MoyskladDocumentQuery
     */
    public function byCompany($companyId)
    {
        return $this->andWhere([
            MoyskladDocument::tableName().'.company_id' => $companyId
        ]);
    }

    /**
     * @return MoyskladDocumentQuery
     */
    public function byDocumentType($documentTypeId)
    {
        return $this->andWhere([
            MoyskladDocument::tableName().'.document_type_id' => $documentTypeId
        ]);
    }

    /**
     * @return MoyskladDocumentQuery
     */
    public function byIOType($ioType)
    {
        return $this->andWhere([
            MoyskladDocument::tableName().'.document_type' => $ioType
        ]);
    }

    /**
     * @return MoyskladDocumentQuery
     */
    public function byValidated()
    {
        return $this->andWhere([
            MoyskladDocument::tableName().'.is_validated' => 1]);
    }

    /**
     * @return MoyskladDocumentQuery
     */
    public function byNew()
    {
        return $this->andWhere([
            MoyskladDocument::tableName().'.validated_at' => null
        ]);
    }

    /**
     * @return MoyskladDocumentQuery
     */
    public function byContractor($params)
    {
        if (isset($params['name']) && isset($params['inn']) && isset($params['kpp']) && count($params) === 3) {
            if (!empty($params['inn'])) {
                return $this->andWhere([
                    'contractor_inn' => $params['inn'],
                    'contractor_kpp' => $params['kpp']
                ]);
            } else {
                return $this->andWhere([
                    'contractor_name' => $params['name']
                ]);
            }
        }

        return $this->andWhere(new Expression('1=0'));
    }

    /**
     * @return MoyskladDocumentQuery
     */
    public function byDocument($params)
    {
        if (isset($params['type_id']) && isset($params['type']) && isset($params['date']) && isset($params['number']) && count($params) === 4) {

            return $this->andWhere([
                'document_type_id' => $params['type_id'],
                'document_type' => $params['type'],
                'document_date' => $params['date'],
                'document_number' => $params['number'],
            ]);
        }

        return $this->andWhere(new Expression('1=0'));
    }

    /**
     * @return MoyskladDocumentQuery
     */
    public function byDocumentAmount($params)
    {
        if (isset($params['type_id']) && isset($params['type']) && isset($params['date']) && isset($params['amount']) && count($params) === 4) {

            return $this->andWhere([
                'document_type_id' => $params['type_id'],
                'document_type' => $params['type'],
                'document_date' => $params['date'],
                'document_amount' => $params['amount'],
            ]);
        }

        return $this->andWhere(new Expression('1=0'));
    }
}