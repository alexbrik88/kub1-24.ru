<?php

namespace common\components\moysklad\parser\xml;

use common\models\document\DocumentImportType;
use common\models\document\status\AgreementStatus;
use frontend\models\Documents;
use Yii;
use common\components\helpers\ArrayHelper;
use frontend\modules\import\helpers\XmlHelper;
use common\components\moysklad\MoyskladObject;
use common\components\moysklad\MoyskladObjectType as ObjectType;

class AgreementXml extends AbstractXmlObject implements XmlInterface {

    public static function exists($companyId)
    {
        $exists = MoyskladObject::find()
            ->byCompany($companyId)
            ->byObjectType(ObjectType::AGREEMENT)
            ->byContractor([
                'name' => self::$data['contractor_name'],
                'inn' => self::$data['contractor_inn'],
                'kpp' => self::$data['contractor_kpp']
            ])
            ->byValidated()
            ->exists();

        return $exists;
    }

    public static function save($companyId)
    {
        return (bool)Yii::$app->db->createCommand()->insert('moysklad_object', [
            'company_id' => $companyId,
            'object_type_id' => ObjectType::AGREEMENT,
            'object_date' => self::$data['document_date'],
            'object_number' => self::$data['document_number'],
            'object_inn' => self::$data['contractor_inn'],
            'object_kpp' => self::$data['contractor_kpp'],
            'object_name' => self::$data['contractor_name'],
            'one_c_object' => serialize(self::$data)
        ])->execute();
    }

    public static function load(\SimpleXMLElement $xmlObject)
    {
        $contractorObj = XmlHelper::getPropertyByName($xmlObject->{'Ссылка'}, 'Владелец');

        $props = XmlHelper::getProperties($xmlObject);
        $linkProps = XmlHelper::getProperties($xmlObject->{'Ссылка'});
        $contractorProps = ($contractorObj) ? XmlHelper::getProperties($contractorObj->{'Ссылка'}) : [];

        $uid = $linkProps['{УникальныйИдентификатор}'] ?? null;

        $documentDate = $linkProps['Дата'] ?? null;
        $documentNumber = ($linkProps['Номер'] ?? null) ?: 'б/н';
        $documentType = $linkProps['ВидДоговора'] ?? null;

        self::$data = [
            'uid' => $uid,
            'document_date' => ($documentDate) ? substr((string)$documentDate, 0, 10) : null,
            'document_number' => $documentNumber,
            'document_type' => ($documentType) ? ($documentType === 'СПоставщиком' ? Documents::IO_TYPE_IN : Documents::IO_TYPE_OUT) : null,
            'document_name' => $linkProps['Наименование'] ?? null,
            'contractor_inn' => $contractorProps['ИНН'] ?? null,
            'contractor_kpp' => $contractorProps['КПП'] ?? null,
            'contractor_name' => $contractorProps['Наименование'],
        ];

        return true;
    }

    public static function convert2model(&$model, $data)
    {
        ///////////////////////////
        $model->id = null;
        $model->isNewRecord = true;
        $model->one_c_imported = DocumentImportType::TYPE_MOYSKLAD;
        //$model->object_guid = $data['uid'];
        ///////////////////////////

        $model->type = $data['document_type'];
        $model->status_id = AgreementStatus::STATUS_CREATED;
        $model->title_template_id = 1;
        $model->document_type_id = 1;
        $model->is_created = true;
        $model->created_at = time();
        $model->document_date_input = ($data['document_date']) ? date('d.m.Y', strtotime($data['document_date'])) : null;
        $model->document_number = $data['document_number'];
        $model->document_name = $data['document_name'];

        return $model;
    }
}