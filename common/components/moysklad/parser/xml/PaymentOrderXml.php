<?php

namespace common\components\moysklad\parser\xml;

use common\models\cash\CashFlowsBase;
use common\models\cash\CashOrderFlows;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use frontend\rbac\permissions\Cash;
use Yii;
use frontend\models\Documents;
use common\models\currency\Currency;
use common\components\helpers\ArrayHelper;
use frontend\modules\import\helpers\XmlHelper;
use common\components\moysklad\MoyskladObject;
use common\components\moysklad\MoyskladObjectType as ObjectType;
use common\components\moysklad\MoyskladDocument;
use common\components\moysklad\MoyskladDocumentType as DocumentType;

class PaymentOrderXml extends AbstractXmlDocument implements XmlInterface {

    public static function load(\SimpleXMLElement $xmlObject)
    {
        $IO_KEY = self::$ioType;
        $documentTypeId = ($IO_KEY == Documents::IO_TYPE_OUT)
            ? DocumentType::OUT_PAYMENT_ORDER
            : DocumentType::IN_PAYMENT_ORDER;

        $contractorObj = XmlHelper::getPropertyByName($xmlObject, 'Контрагент');
        $agreementObj = XmlHelper::getPropertyByName($xmlObject, 'ДоговорКонтрагента');
        $currencyObj = XmlHelper::getPropertyByName($xmlObject, 'ВалютаДокумента');
        $tableBaseDocObj = XmlHelper::getTagByName($xmlObject, 'ТабличнаяЧасть', 'РасшифровкаПлатежа');

        $props = XmlHelper::getProperties($xmlObject);
        $linkProps = XmlHelper::getProperties($xmlObject->{'Ссылка'});
        $contractorProps = ($contractorObj) ? XmlHelper::getProperties($contractorObj->{'Ссылка'}) : [];
        $agreementProps = ($agreementObj) ? XmlHelper::getProperties($agreementObj->{'Ссылка'}) : [];
        $currencyProps = ($currencyObj) ? XmlHelper::getProperties($currencyObj->{'Ссылка'}) : [];

        $uid = $linkProps['{УникальныйИдентификатор}'] ?? null;
        $documentDate = $linkProps['Дата'] ?? null;
        $documentNumber = $linkProps['Номер'];

        self::$data = [
            'uid' => $uid,
            'document_type_id' => $documentTypeId,
            'document_type' => $IO_KEY,
            'document_date' => substr($documentDate, 0, 10),
            'document_number' => $documentNumber,
            'document_amount' => number_format($props['СуммаДокумента'] ?? 0, 2),
            'contractor_name' => $contractorProps['Наименование'] ?? null,
            'contractor_inn' => $contractorProps['ИНН'] ?? null,
            'contractor_kpp' => $contractorProps['КПП'] ?? null,
            'flow_amount' => floatval($props['СуммаДокумента'] ?? null),
            'flow_type' => ($IO_KEY == Documents::IO_TYPE_OUT) ? CashFlowsBase::FLOW_TYPE_INCOME : CashFlowsBase::FLOW_TYPE_EXPENSE,
            'flow_description' => $props['ВидОперации'] ?? '-'
        ];

        if (!isset($currencyProps['Код']) || $currencyProps['Код'] == '643') {
            $CURRENCY = self::$dbConst['currency']['643'];
        } else {
            $CURRENCY = self::$dbConst['currency'][$currencyProps['Код']] ?? null;
        }

        if ($CURRENCY != 'RUB') {

            if ($tableBaseDocObj && $tableBaseDocObj->children()->count()) {
                for ($i = 0; $i < $tableBaseDocObj->children()->count(); $i++) {
                    if ($recordObj = $tableBaseDocObj->children()->{'Запись'}[$i]) {
                        $recordProps = XmlHelper::getProperties($recordObj);
                        $currencyAmount = $recordProps['КратностьВзаиморасчетов'] ?? Currency::DEFAULT_AMOUNT;
                        $currencyRate = $recordProps['КурсВзаиморасчетов'] ?? Currency::DEFAULT_RATE;
                    }
                }
            }

            self::$data = array_merge(self::$data, [
                'currency_name' => $CURRENCY,
                'currency_amount' => $currencyAmount ?? Currency::DEFAULT_AMOUNT,
                'currency_rate' => $currencyRate ?? Currency::DEFAULT_RATE
            ]);
        }
    }

    public static function convert2model(&$model, $data)
    {
        /** @var CashOrderFlows $model */
        ///////////////////////////
        $model->id = null;
        $model->isNewRecord = true;
        //$model->one_c_imported = 1;
        ///////////////////////////

        $model->date = ($data['document_date']) ? date('d.m.Y', strtotime($data['document_date'])) : null;
        $model->number = $data['document_number'];
        $model->flow_type = $data['flow_type'];
        $model->amount = $data['flow_amount'];
        $model->description = $data['flow_description'];

        if ($contractor = self::_findKubContractor($model->company_id, $data['contractor_inn'], $data['contractor_kpp'], $data['contractor_name'])) {
            $model->contractor_id = $contractor->id;
            $model->is_accounting = ($contractor->not_accounting) ? 0 : 1;
            if ($data['flow_type'] == CashFlowsBase::FLOW_TYPE_INCOME) {
                $model->income_item_id =
                    $contractor->invoice_income_item_id
                        ?: InvoiceIncomeItem::ITEM_PAYMENT_FROM_BUYER;
            } else {
                $model->expenditure_item_id =
                    $contractor->invoice_expenditure_item_id
                        ?: InvoiceExpenditureItem::ITEM_CONTRACTOR_PAYMENT;
            }
        } else {
            $model->contractor_id = null;
            // not null - for prevent many validation errors
            $model->income_item_id = InvoiceIncomeItem::ITEM_PAYMENT_FROM_BUYER;
            $model->expenditure_item_id = InvoiceExpenditureItem::ITEM_CONTRACTOR_PAYMENT;
            $model->is_accounting = 1;
        }

        return $model;
    }
}