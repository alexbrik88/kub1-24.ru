<?php

namespace common\components\moysklad\parser\xml;

use Yii;
use common\components\helpers\ArrayHelper;
use frontend\modules\import\helpers\XmlHelper;
use common\components\moysklad\MoyskladObject;
use common\components\moysklad\MoyskladObjectType as ObjectType;

class BankAccountXml extends AbstractXmlObject implements XmlInterface {

    public static function exists($companyId)
    {
        $exists = MoyskladObject::find()
            ->byCompany($companyId)
            ->byObjectType(ObjectType::BANK_ACCOUNT)
            ->byNumber(self::$data['rs'])
            ->byValidated()
            ->exists();

        return $exists;
    }

    public static function save($companyId)
    {
        return (bool)Yii::$app->db->createCommand()->insert('moysklad_object', [
            'company_id' => $companyId,
            'object_type_id' => ObjectType::BANK_ACCOUNT,
            'object_number' => self::$data['rs'],
            'object_code' => self::$data['bank_bik'],
            'object_name' => self::$data['contractor_name'],
            'object_inn' => self::$data['contractor_inn'],
            'object_kpp' => self::$data['contractor_kpp'],
            'one_c_object' => serialize(self::$data)
        ])->execute();
    }

    public static function load(\SimpleXMLElement $xmlObject)
    {
        $bankObj = XmlHelper::getPropertyByName($xmlObject, 'Банк');
        $contractorObj = XmlHelper::getPropertyByName($xmlObject, 'Владелец');

        $props = XmlHelper::getProperties($xmlObject);
        $linkProps = XmlHelper::getProperties($xmlObject->{'Ссылка'});
        $bankProps = ($bankObj) ? XmlHelper::getProperties($bankObj->{'Ссылка'}) : [];
        $contractorProps = ($contractorObj) ? XmlHelper::getProperties($contractorObj->{'Ссылка'}) : [];
        // todo: $currencyProps

        $uid = $linkProps['{УникальныйИдентификатор}'] ?? null;

        self::$data = [
            'uid' => $uid,
            'bank_bik' => $bankProps['Код'] ?? null,
            'contractor_inn' => $contractorProps['ИНН'] ?? null,
            'contractor_kpp' => $contractorProps['КПП'] ?? null,
            'contractor_name' => $contractorProps['Наименование'] ?? null,
            'rs' => $linkProps['НомерСчета'] ?? null,
        ];

        return true;
    }

    public static function convert2model(&$model, $data)
    {
        ///////////////////////////
        $model->id = null;
        $model->isNewRecord = true;
        ///////////////////////////

        $model->bik = $data['bik'];
        $model->rs = $data['rs'];
        $model->ks = $data['ks'];
        $model->bank_name = $data['bank_name'];
        $model->bank_city = $data['bank_city'];
        $model->bank_address = $data['bank_address'];

        return $model;
    }    
}