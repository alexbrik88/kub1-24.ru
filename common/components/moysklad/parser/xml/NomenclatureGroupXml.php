<?php

namespace common\components\moysklad\parser\xml;

use Yii;
use common\models\product\Product;
use common\components\helpers\ArrayHelper;
use frontend\modules\import\helpers\XmlHelper;
use common\components\moysklad\MoyskladObject;
use common\components\moysklad\MoyskladObjectType as ObjectType;

class NomenclatureGroupXml extends AbstractXmlObject implements XmlInterface {

    public static function exists($companyId)
    {
        return MoyskladObject::find()
            ->byCompany($companyId)
            ->byObjectType(ObjectType::NOMENCLATURE_GROUP)
            ->byName(self::$data['title'])
            ->byValidated()
            ->exists();
    }

    public static function save($companyId)
    {
        return (bool)Yii::$app->db->createCommand()->insert('moysklad_object', [
            'company_id' => $companyId,
            'object_type_id' => ObjectType::NOMENCLATURE_GROUP,
            'object_name' => self::$data['title'],
            'one_c_object' => serialize(self::$data)
        ])->execute();
    }

    public static function load(\SimpleXMLElement $xmlObject)
    {
        $props = XmlHelper::getProperties($xmlObject);
        $linkProps = XmlHelper::getProperties($xmlObject->{'Ссылка'});

        $uid = $linkProps['{УникальныйИдентификатор}'] ?? null;

        self::$data = [
            'uid' => $uid,
            'title' => mb_substr($linkProps['Наименование'] ?? '', 0, 60)
        ];

        return true;
    }

    public static function convert2model(&$model, $data)
    {
        ///////////////////////////
        $model->id = null;
        $model->isNewRecord = true;
        //$model->object_guid = $data['uid'];
        ///////////////////////////

        $model->production_type = Product::PRODUCTION_TYPE_GOODS;
        $model->title = $data['title'];

        return $model;
    }
}