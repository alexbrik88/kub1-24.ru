<?php

namespace common\components\moysklad\parser\xml;

use common\models\company\CompanyType;

class AbstractXmlObject extends AbstractXml {

    public static $data = [];

    public static function _getContractorTypeAndName($xmlFullName)
    {
        $nameArr = explode(' ', str_replace('  ', ' ', trim($xmlFullName)));
        $companyType = array_shift($nameArr);
        switch ($companyType) {
            case 'ИП':
                $typeId = CompanyType::TYPE_IP;
                break;
            case 'ЗАО':
                $typeId = CompanyType::TYPE_ZAO;
                break;
            case 'ПАО':
                $typeId = CompanyType::TYPE_PAO;
                break;
            case 'ОАО':
                $typeId = CompanyType::TYPE_OAO;
                break;
            case 'ООО':
                $typeId = CompanyType::TYPE_OOO;
                break;
            default:
                // not recognized
                $typeId = CompanyType::TYPE_OOO;
                $nameArr = array_merge([$companyType], $nameArr);
                break;
        }

        $name = implode(' ', $nameArr);

        return [$typeId, $name];
    }
}