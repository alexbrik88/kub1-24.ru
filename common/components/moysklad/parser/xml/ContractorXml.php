<?php

namespace common\components\moysklad\parser\xml;

use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\document\DocumentImportType;
use Yii;
use common\components\helpers\ArrayHelper;
use frontend\modules\import\helpers\XmlHelper;
use common\components\moysklad\MoyskladObject;
use common\components\moysklad\MoyskladObjectType as ObjectType;
use yii\base\Exception;

class ContractorXml extends AbstractXmlObject implements XmlInterface {

    public static function exists($companyId)
    {
        $exists = MoyskladObject::find()
            ->byCompany($companyId)
            ->byObjectType(ObjectType::CONTRACTOR)
            ->byContractor([
                'name' => self::$data['name'],
                'inn' => self::$data['ITN'],
                'kpp' => self::$data['PPC']
            ])
            ->byValidated()
            ->exists();

        return $exists;
    }

    public static function save($companyId)
    {
        return (bool)Yii::$app->db->createCommand()->insert('moysklad_object', [
            'company_id' => $companyId,
            'object_type_id' => ObjectType::CONTRACTOR,
            'object_name' => self::$data['name'],
            'object_inn' => self::$data['ITN'],
            'object_kpp' => self::$data['PPC'],
            'one_c_object' => serialize(self::$data)
        ])->execute();
    }

    public static function load(\SimpleXMLElement $xmlObject)
    {
        $props = XmlHelper::getProperties($xmlObject);
        $linkProps = XmlHelper::getProperties($xmlObject->{'Ссылка'});
        $tableInfoObj = XmlHelper::getTagByName($xmlObject, 'ТабличнаяЧасть', 'КонтактнаяИнформация');

        if ($tableInfoObj && $tableInfoObj->children()->count()) {
            for ($i = 0; $i < $tableInfoObj->children()->count(); $i++) {
                if ($recordObj = $tableInfoObj->children()->{'Запись'}[$i]) {
                    $recordProps = XmlHelper::getProperties($recordObj);
                    $key = ArrayHelper::getValue($recordProps, 'Тип');
                    $value = ArrayHelper::getValue($recordProps, 'Представление');

                    switch ($key) {
                        case 'Телефон':
                            $raw = filter_var(str_replace(['+', '-'], '', $value), FILTER_SANITIZE_NUMBER_INT);
                            if ($raw) {
                                $phone = '+'.substr($raw, 0, 1).'('.substr($raw, 1, 3).') '.substr($raw, 4, 3).'-'.substr($raw, 7, 2).'-'.substr($raw, 9, 2);
                            }
                            break;
                        case 'АдресЭлектроннойПочты':
                            $email = filter_var($value, FILTER_SANITIZE_EMAIL);
                            break;
                        case 'Адрес':
                            $addressObj = XmlHelper::getPropertyByName($recordObj, 'Вид');
                            $addressProps = ($addressObj) ? XmlHelper::getProperties($addressObj->{'Ссылка'}) : [];
                            if ($addressProps) {
                                switch ($addressProps['Наименование']) {
                                    case 'Юридический адрес':
                                        $legalAddress = $value;
                                        break;
                                    case 'Фактический адрес':
                                        $actualAddress = $value;
                                        break;
                                    case 'Почтовый адрес':
                                        $postalAddress = $value;
                                        break;
                                }
                            }
                            break;
                        default:
                            break;
                    }
                }
            }
        }

        if (isset($props['ЮридическоеФизическоеЛицо'])) {
            $faceType = ($props['ЮридическоеФизическоеЛицо'] === 'ФизическоеЛицо')
                ? Contractor::TYPE_PHYSICAL_PERSON
                : Contractor::TYPE_LEGAL_PERSON;
        } else {
            // import 1C v.2, needed v.3
            throw new Exception('Неизвестный формат файла');
        }

        $uid = $linkProps['{УникальныйИдентификатор}'] ?? null;

        self::$data = [
            'uid' => $uid,
            'name' => $linkProps['Наименование'] ?? null,
            'ITN' => $linkProps['ИНН'] ?? null,
            'PPC' => $linkProps['КПП'] ?? null,
            'BIN' => $props['РегистрационныйНомер'] ?? null,
            'phone' => $phone ?? null,
            'email' => $email ?? null,
            'legal_address' => $legalAddress ?? null,
            'actual_address' => $actualAddress ?? null,
            'postal_address' => $postalAddress ?? null,
            'face_type' => $faceType
        ];

        return true;
    }
    
    public static function convert2model(&$model, $data)
    {
        ///////////////////////////
        $model->id = null;
        $model->isNewRecord = true;
        $model->type = Contractor::TYPE_UNSET; // set later by doc type
        $model->is_seller = 0;
        $model->is_customer = 0;
        $model->one_c_imported = DocumentImportType::TYPE_MOYSKLAD;
        //$model->object_guid = $data['uid'];
        //$model->document_guid = $data['uid'];
        ///////////////////////////

        $model->status = Contractor::ACTIVE;
        $model->contact_is_director = 1;
        $model->chief_accountant_is_director = 1;
        $model->ITN = $data['ITN'];
        $model->PPC = $data['PPC'];
        $model->BIN = $data['BIN'];
        $model->director_name = null;
        $model->director_phone = $data['phone'];
        $model->director_email = $data['email'];
        $model->legal_address = $data['legal_address'];
        $model->actual_address = $data['actual_address'];
        $model->postal_address = $data['postal_address'];
        $model->face_type = $data['face_type'];

        // todo: load from dadata
        list ($normalizedTypeId, $normalizedName) = self::_getContractorTypeAndName($data['name']);

        if ($model->face_type == Contractor::TYPE_LEGAL_PERSON) {
            $model->company_type_id = $normalizedTypeId;
            $model->name = $normalizedName;
        } else {
            $model->face_type = Contractor::TYPE_PHYSICAL_PERSON;
            $model->company_type_id = null;
            $model->name = $normalizedName;
            $model->physical_fio = $normalizedName;
            $_nameArr = explode(' ', $normalizedName);
            $model->physical_no_patronymic = (count($_nameArr) < 3) ? 1 : 0;
        }

        if ($model->director_phone && !$model->validate(['director_phone']))
            $model->director_phone = null;
        if ($model->director_email && !$model->validate(['director_email']))
            $model->director_email = null;

        return $model;
    }

}