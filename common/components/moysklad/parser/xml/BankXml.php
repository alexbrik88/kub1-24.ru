<?php

namespace common\components\moysklad\parser\xml;

use Yii;
use common\components\helpers\ArrayHelper;
use frontend\modules\import\helpers\XmlHelper;
use common\components\moysklad\MoyskladObject;
use common\components\moysklad\MoyskladObjectType as ObjectType;

class BankXml extends AbstractXmlObject implements XmlInterface {

    /**
     * @param $companyId
     * @return bool
     */
    public static function exists($companyId)
    {
        $exists = MoyskladObject::find()
            ->byCompany($companyId)
            ->byObjectType(ObjectType::BANK)
            ->byNumber(self::$data['bik'])
            ->byValidated()
            ->exists();

        return $exists;
    }

    public static function save($companyId)
    {
        return (bool)Yii::$app->db->createCommand()->insert('moysklad_object', [
            'company_id' => $companyId,
            'object_type_id' => ObjectType::BANK,
            'object_number' => self::$data['bik'],
            'one_c_object' => serialize(self::$data)
        ])->execute();
    }

    public static function load(\SimpleXMLElement $xmlObject)
    {
        $props = XmlHelper::getProperties($xmlObject);
        $linkProps = XmlHelper::getProperties($xmlObject->{'Ссылка'});

        $uid = $linkProps['{УникальныйИдентификатор}'] ?? null;

        self::$data = [
            'uid' => $uid,
            'bik' => $linkProps['Код'] ?? null,
            'bank_name' => $props['Наименование' ?? null],
            'bank_city' => $props['Город'] ?? null,
            'bank_address' => $props['Адрес'] ?? null,
            'ks' => $props['КоррСчет'] ?? null
        ];

        return true;
    }

    public static function convert2model(&$model, $data) {}
}