<?php
namespace common\components\moysklad\parser\xml;

interface XmlInterface {

    /**
     * @param $companyId
     * @return bool
     */
    public static function exists($companyId);
    /**
     * @param $companyId
     * @return bool
     */
    public static function save($companyId);
    /**
     * @param \SimpleXMLElement $xmlObject
     * @return bool
     */
    public static function load(\SimpleXMLElement $xmlObject);

    /**
     * @param $model
     * @param $data
     * @return mixed
     */
    public static function convert2model(&$model, $data);
}