<?php

namespace common\components\moysklad\parser\xml;

use Yii;
use common\components\moysklad\MoyskladDocument;
use common\models\Contractor;
use common\models\product\Product;
use common\components\moysklad\MoyskladObject;
use common\components\moysklad\MoyskladObjectType as ObjectType;

class AbstractXmlDocument extends AbstractXml {

    public static $ioType = null;

    public static $data = [];

    public static function save($companyId)
    {
        return (bool)Yii::$app->db->createCommand()->insert('moysklad_document', [
            'company_id' => $companyId,
            'document_type_id' => self::$data['document_type_id'],
            'document_type' => self::$data['document_type'],
            'document_date' => self::$data['document_date'],
            'document_number' => self::$data['document_number'],
            'document_amount' => self::$data['document_amount'],
            'contractor_inn' => self::$data['contractor_inn'],
            'contractor_kpp' => self::$data['contractor_kpp'],
            'contractor_name' => self::$data['contractor_name'],
            'one_c_object' => serialize(self::$data)
        ])->execute();
    }

    public static function exists($companyId)
    {
        $exists = (bool)MoyskladDocument::find()
            ->byCompany($companyId)
            ->byContractor([
                'inn' => self::$data['contractor_inn'],
                'kpp' => self::$data['contractor_kpp'],
                'name' => self::$data['contractor_name'],
            ])
            ->byDocument([
                'type_id' => self::$data['document_type_id'],
                'type' => self::$data['document_type'],
                'date' => self::$data['document_date'],
                'number' => self::$data['document_number'],
            ])
            ->byValidated()
            ->select('id')
            ->scalar();

        return $exists;
    }

    /**
     * @param $number1C
     * @param $isOut
     * @return array
     */
    protected static function _getDocumentNumberArr($number1C, $isOut)
    {
        if ($isOut) {
            preg_match_all('/[\d]+/', $number1C, $m);
            if (count($m[0]) == 0) {
                $num = $additional_num = null;
            }
            elseif (count($m[0]) == 1) {
                $num = array_pop($m[0]);
                $additional_num = str_replace($num, '', $number1C);
            }
            else {
                $maxLen = 0;
                $num = '';
                foreach ($m[0] as $mm) {
                    if (strlen($mm) > $maxLen) {
                        $num = $mm;
                        $maxLen = strlen($num);
                    }
                }
                $additional_num = str_replace($num, '', $number1C);
            }
            $is_additional_number_before = ($num && strpos($number1C, (string)$num) > 0);
        } else {
            $num = $number1C;
            $additional_num = null;
            $is_additional_number_before = false;
        }

        return [
            'document_number' => $num,
            'document_additional_number' => $additional_num,
            'is_additional_number_before' => $is_additional_number_before
        ];
    }

    /**
     * @param $companyId
     * @param $inn
     * @param $kpp
     * @param $name
     * @return Contractor|null
     */
    protected static function _findKubContractor($companyId, $inn, $kpp, $name)
    {
        $kubId = self::_findKubContractorID($companyId, $inn, $kpp, $name);
        return ($kubId) ? Contractor::findOne(['id' => $kubId, 'company_id' => $companyId]) : null;
    }

    /**
     * @param $companyId
     * @param $code
     * @param $title
     * @return Product|null
     */
    protected static function _findKubProduct($companyId, $code, $title)
    {
        $kubId = self::_findKubProductID($companyId, $code, $title);
        return ($kubId) ? Product::findOne(['id' => $kubId, 'company_id' => $companyId]) : null;
    }

    // todo: add cache
    protected static function _findKubProductID($companyId, $code, $title)
    {
        if ($code) {

            $kubId = MoyskladObject::find()->where([
                'company_id' => $companyId,
                'object_type_id' => ObjectType::NOMENCLATURE,
                'object_code' => $code,
                //'is_validated' => 1,
            ])->select('product_id')->scalar();

        } else {
            $kubId = MoyskladObject::find()->where([
                'company_id' => $companyId,
                'object_type_id' => ObjectType::NOMENCLATURE,
                'object_name' => $title,
                //'is_validated' => 1,
            ])->select('product_id')->scalar();
        }

        return $kubId;
    }

    // todo: add cache
    protected static function _findKubContractorID($companyId, $inn, $kpp, $name)
    {
        if ($inn) {
            $kubId = MoyskladObject::find()->where([
                'company_id' => $companyId,
                'object_type_id' => ObjectType::CONTRACTOR,
                'object_inn' => $inn,
                'object_kpp' => $kpp,
            ])->select('contractor_id')->scalar();

        } else {
            $kubId = MoyskladObject::find()->where([
                'company_id' => $companyId,
                'object_type_id' => ObjectType::CONTRACTOR,
                'object_name' => $name
            ])->select('contractor_id')->scalar();
        }

        return $kubId;
    }
}