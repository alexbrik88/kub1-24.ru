<?php

namespace common\components\moysklad\parser\xml;

use common\models\document\Act;
use common\models\document\DocumentImportType;
use common\models\document\Order;
use common\models\document\OrderAct;
use Yii;
use common\models\document\Invoice;
use common\models\product\Product;
use frontend\models\Documents;
use common\components\moysklad\MoyskladDocumentType as DocumentType;
use common\components\moysklad\MoyskladObjectType as ObjectType;

class ActXml extends AbstractXmlDocument implements XmlInterface {

    public static function load(\SimpleXMLElement $xmlObject)
    {
        // see UpdXml
    }

    public static function convert2model(&$model, $data)
    {
        // see UpdXml
    }

    /**
     * @return Act|null
     */
    public static function convert2dependentModel(Act &$model, Invoice $invoice, $data)
    {
        $IO_KEY = $data['document_type'];

        $numberArray = self::_getDocumentNumberArr($data['document_number'], $IO_KEY == Documents::IO_TYPE_OUT);

        ///////////////////////////
        $model->id = null;
        $model->isNewRecord = true;
        $model->company_id = $invoice->company_id;
        $model->contractor_id = $invoice->contractor_id;
        $model->invoice_id = $invoice->id;
        //$model->document_guid = $uid;
        $model->one_c_imported = DocumentImportType::TYPE_MOYSKLAD;
        ///////////////////////////

        $model->type = $IO_KEY;
        $model->document_date = $data['document_date'];
        $model->document_number = $numberArray['document_number'];
        $model->document_additional_number = $numberArray['document_additional_number'];

        $ownOrderArray = [];
        $ordersSum = 0;
        foreach ($invoice->orders as $order) {
            if ($order->product)
            if ($order->product->production_type == Product::PRODUCTION_TYPE_SERVICE) {
                $ownOrderArray[$order->id] = self::_createOrderAct($order, $order->quantity);
                $ownOrderArray[$order->id]['invoice_id'] = $invoice->id;
                $ordersSum += ($IO_KEY == Documents::IO_TYPE_OUT) ? $order->amount_sales_with_vat : $order->amount_purchase_with_vat;
            }
        }

        if ($ownOrderArray) {

            $model->order_sum = $ordersSum;
            $model->populateRelation('invoice', $invoice);
            $model->populateRelation('orderActs', $ownOrderArray);

            return $model;
        }

        return null;
    }

    private static function _createOrderAct(Order $order, $count)
    {
        $orderAct = new OrderAct([
            'act_id' => null,
            'order_id' => $order->id,
            'product_id' => $order->product_id,
            'quantity' => $count,
        ]);
        $orderAct->populateRelation('invoiceOrder', $order);

        return $orderAct;
    }
}