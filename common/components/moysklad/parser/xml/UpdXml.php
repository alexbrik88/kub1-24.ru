<?php

namespace common\components\moysklad\parser\xml;

use common\components\date\DateHelper;
use common\components\moysklad\MoyskladDocument;
use common\components\moysklad\MoyskladObject;
use common\components\moysklad\MoyskladObjectType;
use common\models\Agreement;
use common\models\AgreementType;
use common\models\Contractor;
use common\models\document\DocumentImportType;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\Order;
use common\models\document\OrderHelper;
use common\models\document\OrderUpd;
use common\models\document\status\InvoiceStatus;
use common\models\document\Upd;
use Yii;
use common\components\helpers\ArrayHelper;
use common\models\currency\Currency;
use common\models\document\Invoice;
use common\models\product\Product;
use common\models\TaxRate;
use frontend\modules\import\helpers\XmlHelper;
use frontend\models\Documents;
use common\components\moysklad\MoyskladDocumentType as DocumentType;
use common\components\moysklad\MoyskladObjectType as ObjectType;

class UpdXml extends AbstractXmlDocument implements XmlInterface {

    public static function load(\SimpleXMLElement $xmlObject)
    {
        $IO_KEY = self::$ioType;
        $documentTypeId = ($IO_KEY == Documents::IO_TYPE_OUT) ? DocumentType::OUT_UPD : DocumentType::IN_UPD;

        $contractorObj = XmlHelper::getPropertyByName($xmlObject, 'Контрагент');
        $agreementObj = XmlHelper::getPropertyByName($xmlObject, 'ДоговорКонтрагента');
        $currencyObj = XmlHelper::getPropertyByName($xmlObject, 'ВалютаДокумента');
        $tableProductObj = XmlHelper::getTagByName($xmlObject, 'ТабличнаяЧасть', 'Товары');
        $tableServiceObj = XmlHelper::getTagByName($xmlObject, 'ТабличнаяЧасть', 'Услуги');
        $storeObj = XmlHelper::getPropertyByName($xmlObject, 'Склад');
        $invoiceObj = ($IO_KEY == Documents::IO_TYPE_OUT) ?
            XmlHelper::getPropertyByName($xmlObject, 'СчетНаОплатуПокупателю') :
            XmlHelper::getPropertyByName($xmlObject, 'СчетНаОплатуПоставщика');

        $props = XmlHelper::getProperties($xmlObject);
        $linkProps = XmlHelper::getProperties($xmlObject->{'Ссылка'});
        $currencyProps = ($currencyObj) ? XmlHelper::getProperties($currencyObj->{'Ссылка'}) : [];
        $contractorProps = ($contractorObj) ? XmlHelper::getProperties($contractorObj->{'Ссылка'}) : [];
        $agreementProps = ($agreementObj) ? XmlHelper::getProperties($agreementObj->{'Ссылка'}) : [];
        $storeProps = ($storeObj) ? XmlHelper::getProperties($storeObj->{'Ссылка'}) : [];
        $invoiceProps = ($invoiceObj) ? XmlHelper::getProperties($invoiceObj->{'Ссылка'}) : [];

        $uid = $linkProps['{УникальныйИдентификатор}'] ?? null;
        $hasNds = false;

        $products = [];
        if ($tableProductObj && $tableProductObj->children()->count()) {
            for ($i = 0; $i < $tableProductObj->children()->count(); $i++) {
                if ($recordObj = $tableProductObj->children()->{'Запись'}[$i]) {
                    $recordProps = XmlHelper::getProperties($recordObj);
                    $nomenclatureObj = XmlHelper::getPropertyByName($recordObj, 'Номенклатура');
                    $nomenclatureProps = ($nomenclatureObj) ? XmlHelper::getProperties($nomenclatureObj->{'Ссылка'}) : [];
                    $products[] = [
                        'uid' => $nomenclatureProps['{УникальныйИдентификатор}'] ?? null,
                        'title' => $nomenclatureProps['Наименование'] ?? null,
                        'code' => $nomenclatureProps['Код'] ?? null,
                        'count' => ($recordProps['Количество'] ?? null) ?: 1,
                        'ndsKey' => $recordProps['СтавкаНДС'] ?? null,
                        'summ' => bcmul(100, $recordProps['Сумма'] ?? 0),
                        'ndsSumm' => bcmul(100, $recordProps['СуммаНДС'] ?? 0),
                        'price' =>  bcmul(100, $recordProps['Цена'] ?? 0),
                        'production_type' => Product::PRODUCTION_TYPE_GOODS
                        //'invoice_date' => $invoiceProps['Дата'] ?? null, // there no $invoiceProps
                        //'invoice_number' => $invoiceProps['Номер'] ?? null, // there no $invoiceProps
                    ];

                    // todo: set products nds id
                    if (($recordProps['СуммаНДС'] ?? 0) > 0) {
                        $hasNds = true;
                    }
                }
            }
        }

        $services = [];
        if ($tableServiceObj && $tableServiceObj->children()->count()) {
            for ($i = 0; $i < $tableServiceObj->children()->count(); $i++) {
                if ($recordObj = $tableServiceObj->children()->{'Запись'}[$i]) {
                    $recordProps = XmlHelper::getProperties($recordObj);
                    $nomenclatureObj = XmlHelper::getPropertyByName($recordObj, 'Номенклатура');
                    $nomenclatureProps = ($nomenclatureObj) ? XmlHelper::getProperties($nomenclatureObj->{'Ссылка'}) : [];
                    $services[] = [
                        'uid' => $nomenclatureProps['{УникальныйИдентификатор}'] ?? null,
                        'title' => $nomenclatureProps['Наименование'] ?? null,
                        'code' => $nomenclatureProps['Код'] ?? null,
                        'count' => ($recordProps['Количество'] ?? null) ?: 1,
                        'ndsKey' => $recordProps['СтавкаНДС'] ?? null,
                        'summ' => bcmul(100, $recordProps['Сумма'] ?? 0),
                        'ndsSumm' => bcmul(100, $recordProps['СуммаНДС'] ?? 0),
                        'price' =>  bcmul(100, $recordProps['Цена'] ?? 0),
                        'production_type' => Product::PRODUCTION_TYPE_SERVICE,
                        //'invoice_date' => $invoiceProps['Дата'] ?? null, // there no $invoiceProps
                        //'invoice_number' => $invoiceProps['Номер'] ?? null, // there no $invoiceProps
                    ];

                    // todo: set products nds id
                    if (($recordProps['СуммаНДС'] ?? 0) > 0) {
                        $hasNds = true;
                    }
                }
            }
        }

        if ($IO_KEY == Documents::IO_TYPE_IN) {
            $documentDate = ($linkProps['ДатаВходящегоДокумента'] ?? null) ?: ($linkProps['Дата'] ?? null);
            $documentNumber = ($linkProps['НомерВходящегоДокумента'] ?? null) ?: (($linkProps['Номер'] ?? null) ?: 'б/н');
        }
        else if ($IO_KEY == Documents::IO_TYPE_OUT) {
            $documentDate = $linkProps['Дата'] ?? null;
            $documentNumber = $linkProps['Номер'] ?? null;
        }
        else {
            $documentDate = $documentNumber = null;
        }

        self::$data = [
            'uid' => $uid,
            'document_type_id' => $documentTypeId,
            'document_type' => $IO_KEY,
            'document_date' => substr($documentDate, 0, 10),
            'document_number' => $documentNumber,
            'document_amount' => number_format($props['СуммаДокумента'] ?? 0, 2),
            'hasNds' => $hasNds,
            'isNdsExcluded' => (isset($props['СуммаВключаетНДС']) && $props['СуммаВключаетНДС'] === "true"),
            'contractor_name' => $contractorProps['Наименование'] ?? null,
            'contractor_inn' => $contractorProps['ИНН'] ?? null,
            'contractor_kpp' => $contractorProps['КПП'] ?? null,
            'agreement_date' => $agreementProps['Дата'] ?? null,
            'agreement_number' => $agreementProps['Номер'] ?? null,
            'products' => $products,
            'services' => $services,
        ];

        if (!isset($currencyProps['Код']) || $currencyProps['Код'] == '643') {
            $CURRENCY = self::$dbConst['currency']['643'];
        } else {
            $CURRENCY = self::$dbConst['currency'][$currencyProps['Код']] ?? null;
        }

        if ($CURRENCY != 'RUB') {
            self::$data = array_merge(self::$data, [
                'currency_name' => $CURRENCY,
                'currency_amount' => $props['КратностьВзаиморасчетов'] ?? Currency::DEFAULT_AMOUNT,
                'currency_rate' => $props['КурсВзаиморасчетов'] ?? Currency::DEFAULT_RATE
            ]);
        }
    }

    public static function convert2model(&$model, $data)
    {
        // not used
    }

    public static function convert2dependentModel(Upd &$model, Invoice $invoice, $data)
    {
        $IO_KEY = $data['document_type'];

        $numberArray = self::_getDocumentNumberArr($data['document_number'], $IO_KEY == Documents::IO_TYPE_OUT);

        ///////////////////////////
        $model->id = null;
        $model->isNewRecord = true;
        $model->company_id = $invoice->company_id;
        $model->contractor_id = $invoice->contractor_id;
        $model->invoice_id = $invoice->id;
        //$model->document_guid = $uid;
        $model->one_c_imported = DocumentImportType::TYPE_MOYSKLAD;
        ///////////////////////////

        $model->type = $IO_KEY;
        $model->documentDate = ($data['document_date']) ? date('d.m.Y', strtotime($data['document_date'])) : null;
        $model->document_date = $data['document_date'];
        $model->document_number = $numberArray['document_number'];
        $model->document_additional_number = $numberArray['document_additional_number'];

        $ownOrderArray = [];
        $ordersSum = 0;
        $orderNds = 0;
        foreach ($invoice->orders as $order) {
            $ownOrderArray[$order->id] = self::_createOrderUpd($order, $order->quantity);
            $ordersSum += ($IO_KEY == Documents::IO_TYPE_OUT) ? $order->amount_sales_with_vat : $order->amount_purchase_with_vat;
            $orderNds += ($IO_KEY == Documents::IO_TYPE_OUT)
                ? ($order->amount_sales_with_vat - $order->amount_sales_no_vat)
                : ($order->amount_purchase_with_vat - $order->amount_purchase_no_vat);
        }

        $model->orders_sum = $ordersSum;

        $model->populateRelation('invoice', $invoice);
        $model->populateRelation('ownOrders', $ownOrderArray);

        return $model;
    }

    private static function _createOrderUpd(Order $order, $count)
    {
        return new OrderUpd([
            'upd_id' => null,
            'order_id' => $order->id,
            'product_id' => $order->product_id,
            'quantity' => $count,
            'country_id' => $order->country_id,
            'custom_declaration_number' => $order->custom_declaration_number,
        ]);
    }
}