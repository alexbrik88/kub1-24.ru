<?php

namespace common\components\moysklad\parser\xml;

use common\components\date\DateHelper;
use common\components\moysklad\MoyskladDocument;
use common\components\moysklad\MoyskladObject;
use common\components\moysklad\MoyskladObjectType;
use common\models\Agreement;
use common\models\AgreementType;
use common\models\Contractor;
use common\models\document\DocumentImportType;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\OrderHelper;
use common\models\document\status\InvoiceStatus;
use Yii;
use common\components\helpers\ArrayHelper;
use common\models\currency\Currency;
use common\models\document\Invoice;
use common\models\product\Product;
use common\models\TaxRate;
use frontend\modules\import\helpers\XmlHelper;
use frontend\models\Documents;
use common\components\moysklad\MoyskladDocumentType as DocumentType;
use common\components\moysklad\MoyskladObjectType as ObjectType;

class InvoiceXml extends AbstractXmlDocument implements XmlInterface {

    public static function load(\SimpleXMLElement $xmlObject)
    {
        $IO_KEY = self::$ioType;
        $documentTypeId = ($IO_KEY == Documents::IO_TYPE_OUT) ? DocumentType::OUT_INVOICE : DocumentType::IN_INVOICE;

        $contractorObj = XmlHelper::getPropertyByName($xmlObject, 'Контрагент');
        $agreementObj = XmlHelper::getPropertyByName($xmlObject, 'ДоговорКонтрагента');
        $currencyObj = XmlHelper::getPropertyByName($xmlObject, 'ВалютаДокумента');
        $tableProductObj = XmlHelper::getTagByName($xmlObject, 'ТабличнаяЧасть', 'Товары');
        $tableServiceObj = XmlHelper::getTagByName($xmlObject, 'ТабличнаяЧасть', 'Услуги');

        $props = XmlHelper::getProperties($xmlObject);
        $linkProps = XmlHelper::getProperties($xmlObject->{'Ссылка'});
        $currencyProps = ($currencyObj) ? XmlHelper::getProperties($currencyObj->{'Ссылка'}) : [];
        $contractorProps = ($contractorObj) ? XmlHelper::getProperties($contractorObj->{'Ссылка'}) : [];
        $agreementProps = ($agreementObj) ? XmlHelper::getProperties($agreementObj->{'Ссылка'}) : [];

        $uid = $linkProps['{УникальныйИдентификатор}'] ?? null;
        $hasNds = false;

        $products = [];
        if ($tableProductObj && $tableProductObj->children()->count()) {
            for ($i = 0; $i < $tableProductObj->children()->count(); $i++) {
                if ($recordObj = $tableProductObj->children()->{'Запись'}[$i]) {
                    $recordProps = XmlHelper::getProperties($recordObj);
                    $nomenclatureObj = XmlHelper::getPropertyByName($recordObj, 'Номенклатура');
                    $nomenclatureProps = ($nomenclatureObj) ? XmlHelper::getProperties($nomenclatureObj->{'Ссылка'}) : [];
                    $products[] = [
                        'uid' => $nomenclatureProps['{УникальныйИдентификатор}'] ?? null,
                        'title' => $nomenclatureProps['Наименование'] ?? null,
                        'code' => $nomenclatureProps['Код'] ?? null,
                        'count' => ($recordProps['Количество'] ?? null) ?: 1,
                        'ndsKey' => $recordProps['СтавкаНДС'] ?? null,
                        'summ' => bcmul(100, $recordProps['Сумма'] ?? 0),
                        'ndsSumm' => bcmul(100, $recordProps['СуммаНДС'] ?? 0),
                        'price' =>  bcmul(100, $recordProps['Цена'] ?? 0),
                        'production_type' => Product::PRODUCTION_TYPE_GOODS
                    ];

                    // todo: set products nds id
                    if (($recordProps['СуммаНДС'] ?? 0) > 0) {
                        $hasNds = true;
                    }
                }
            }
        }

        $services = [];
        if ($tableServiceObj && $tableServiceObj->children()->count()) {
            for ($i = 0; $i < $tableServiceObj->children()->count(); $i++) {
                if ($recordObj = $tableServiceObj->children()->{'Запись'}[$i]) {
                    $recordProps = XmlHelper::getProperties($recordObj);
                    $nomenclatureObj = XmlHelper::getPropertyByName($recordObj, 'Номенклатура');
                    $nomenclatureProps = ($nomenclatureObj) ? XmlHelper::getProperties($nomenclatureObj->{'Ссылка'}) : [];
                    $services[] = [
                        'uid' => $nomenclatureProps['{УникальныйИдентификатор}'] ?? null,
                        'title' => $nomenclatureProps['Наименование'] ?? null,
                        'code' => $nomenclatureProps['Код'] ?? null,
                        'count' => ($recordProps['Количество'] ?? null) ?: 1,
                        'ndsKey' => $recordProps['СтавкаНДС'] ?? null,
                        'summ' => bcmul(100, $recordProps['Сумма'] ?? 0),
                        'ndsSumm' => bcmul(100, $recordProps['СуммаНДС'] ?? 0),
                        'price' =>  bcmul(100, $recordProps['Цена'] ?? 0),
                        'production_type' => Product::PRODUCTION_TYPE_SERVICE
                    ];

                    // todo: set products nds id
                    if (($recordProps['СуммаНДС'] ?? 0) > 0) {
                        $hasNds = true;
                    }
                }
            }
        }

        if ($IO_KEY == Documents::IO_TYPE_IN) {
            $documentDate = ($linkProps['ДатаВходящегоДокумента'] ?? null) ?: ($linkProps['Дата'] ?? null);
            $documentNumber = ($linkProps['НомерВходящегоДокумента'] ?? null) ?: (($linkProps['Номер'] ?? null) ?: 'б/н');
        }
        else if ($IO_KEY == Documents::IO_TYPE_OUT) {
            $documentDate = $linkProps['Дата'] ?? null;
            $documentNumber = $linkProps['Номер'] ?? null;
        }
        else {
            $documentDate = $documentNumber = null;
        }


        self::$data = [
            'uid' => $uid,
            'document_type_id' => $documentTypeId,
            'document_type' => $IO_KEY,
            'document_date' => substr($documentDate, 0, 10),
            'document_number' => $documentNumber,
            'document_amount' => number_format($props['СуммаДокумента'] ?? 0, 2),
            'hasNds' => $hasNds,
            'isNdsExcluded' => (isset($props['СуммаВключаетНДС']) && $props['СуммаВключаетНДС'] === "true"),
            'contractor_name' => $contractorProps['Наименование'] ?? null,
            'contractor_inn' => $contractorProps['ИНН'] ?? null,
            'contractor_kpp' => $contractorProps['КПП'] ?? null,
            'agreement_date' => $agreementProps['Дата'] ?? null,
            'agreement_number' => $agreementProps['Номер'] ?? null,
            'products' => $products,
            'services' => $services,
        ];


        if (!isset($currencyProps['Код']) || $currencyProps['Код'] == '643') {
            $CURRENCY = self::$dbConst['currency']['643'];
        } else {
            $CURRENCY = self::$dbConst['currency'][$currencyProps['Код']] ?? null;
        }

        if ($CURRENCY != 'RUB') {
            self::$data = array_merge(self::$data, [
                'currency_name' => $CURRENCY,
                'currency_amount' => $props['КратностьВзаиморасчетов'] ?? Currency::DEFAULT_AMOUNT,
                'currency_rate' => $props['КурсВзаиморасчетов'] ?? Currency::DEFAULT_RATE
            ]);
        }
    }

    public static function convert2model(&$model, $data)
    {
        $IO_KEY = $data['document_type'];

        $numberArray = self::_getDocumentNumberArr($data['document_number'], $IO_KEY == Documents::IO_TYPE_OUT);

        // todo
        $agreementId = null;

        $productionType = ($data['products'] && $data['services']) ? '0, 1' : ($data['services'] ? '0' : '1');

        ///////////////////////////
        $model->id = null;
        $model->isNewRecord = true;
        //$model->document_guid = $uid;
        $model->one_c_imported = DocumentImportType::TYPE_MOYSKLAD;
        ///////////////////////////
        $createdAt = ($d = date_create($data['document_date'])) ? $d->getTimestamp() : null;

        if ($createdAt) {
            $model->detachBehavior('documentCreatedAtBehavior');
            $model->created_at = $createdAt;
            $model->updated_at = $createdAt;
        }

        $model->type = $IO_KEY;
        $model->invoice_status_id = InvoiceStatus::STATUS_CREATED;
        $model->document_date = $data['document_date'];
        $model->document_number = $numberArray['document_number'];
        $model->document_additional_number = $numberArray['document_additional_number'];
        $model->is_additional_number_before = $numberArray['is_additional_number_before'];
        $model->production_type = $productionType;
        if ($IO_KEY == Documents::IO_TYPE_IN) {
            $model->invoice_expenditure_item_id = InvoiceExpenditureItem::ITEM_CONTRACTOR_PAYMENT;
        }
        $model->payment_limit_date = date(DateHelper::FORMAT_DATE, strtotime('+10 days', strtotime($model->document_date)));
        $model->isAutoinvoice = 0;
        $model->nds_view_type_id = ($data['hasNds']) ? ($data['isNdsExcluded'] ? Invoice::NDS_VIEW_IN : Invoice::NDS_VIEW_OUT) : Invoice::NDS_VIEW_WITHOUT;
        if ($agreementId && ($agreement = Agreement::findOne($agreementId))) {
            // Договор&БП-000100&2020-05-16&1&172
            $model->agreement = implode('&', [
                AgreementType::TYPE_AGREEMENT_NAME,
                $agreement->document_number,
                $agreement->document_date,
                AgreementType::TYPE_AGREEMENT,
                $agreement->id
            ]);
        }

        $model->price_precision = 2;

        // contractor
        if ($contractor = self::_findKubContractor($model->company_id, $data['contractor_inn'], $data['contractor_kpp'], $data['contractor_name'])) {
            $model->contractor_id = $contractor->id;
            $model->contractor_name_short = $contractor->getTitle(true);
            $model->contractor_name_full = $contractor->getTitle(false);
            $model->contractor_address_legal_full = ($contractor->face_type == Contractor::TYPE_LEGAL_PERSON ? $contractor->legal_address : $contractor->physical_address);
            $model->contractor_bank_name = $contractor->bank_name;
            $model->contractor_bank_city = $contractor->bank_city;
            $model->contractor_bik = $contractor->BIC;
            $model->contractor_inn = $contractor->ITN;
            $model->contractor_kpp = $contractor->PPC;
            $model->contractor_ks = $contractor->corresp_account;
            $model->contractor_rs = $contractor->current_account;
        } else {
            $model->contractor_id = null;
        }

        // currency
        if (isset($data['currency_name'])) {
            $model->currency_name = $data['currency_name'];
            $model->currency_rate_type = Currency::RATE_CUSTOM;
            $model->currencyAmountCustom = $data['currency_amount'];
            $model->currencyRateCustom = $data['currency_rate'];
            $model->currency_amount = $data['currency_amount'];
            $model->currency_rate_amount = $data['currency_amount'];
            $model->currency_rate = $data['currency_rate'];
            $model->currency_rate_value = $data['currency_rate'];
        } else {
            $model->currency_name = 'RUB';
            $model->currency_rate_type = Currency::RATE_SERTAIN_DATE;
            $model->currencyAmountCustom = 1;
            $model->currencyRateCustom = 1;
            $model->currency_amount = 1;
            $model->currency_rate_amount = 1;
            $model->currency_rate = 1;
            $model->currency_rate_value = 1;
        }

        // orders
        $orderArray = [];
        foreach ($data['products'] as $arrayProduct) {

            if ($product = self::_findKubProduct($model->company_id, $arrayProduct['code'], $arrayProduct['title'])) {
                // nds
                if ($arrayProduct['ndsKey']) {
                    $taxRateId = ArrayHelper::getValue(self::$dbConst['tax_rate'], $arrayProduct['ndsKey'], TaxRate::RATE_WITHOUT);
                    if ($IO_KEY == Documents::IO_TYPE_OUT) {
                        $product->price_for_sell_nds_id = $taxRateId;
                    } elseif ($IO_KEY == Documents::IO_TYPE_IN) {
                        $product->price_for_buy_nds_id = $taxRateId;
                    }
                }

                $order = OrderHelper::createOrderByProduct($product, $model, $arrayProduct['count'], $arrayProduct['price']);
                $orderArray[] = $order;
            }
        }
        foreach ($data['services'] as $arrayService) {
            if ($service = self::_findKubProduct($model->company_id, $arrayService['code'], $arrayService['title'])) {

                // nds
                if ($arrayService['ndsKey']) {
                    $taxRateId = ArrayHelper::getValue(self::$dbConst['tax_rate'], $arrayService['ndsKey'], TaxRate::RATE_WITHOUT);
                    if ($IO_KEY == Documents::IO_TYPE_OUT) {
                        $service->price_for_sell_nds_id = $taxRateId;
                    } elseif ($IO_KEY == Documents::IO_TYPE_IN) {
                        $service->price_for_buy_nds_id = $taxRateId;
                    }
                }

                $order = OrderHelper::createOrderByProduct($service, $model, $arrayService['count'], $arrayService['price']);
                $orderArray[] = $order;
            }
        }

        $model->populateRelation('orders', $orderArray);
        $model->populateRelation('contractor', $contractor);

        array_walk($orderArray, function ($order, $key) { $order->number = $key + 1; });

        return $model;
    }
}