<?php

namespace common\components\moysklad\parser\xml;

use common\models\document\DocumentImportType;
use Yii;
use common\models\product\Product;
use common\models\product\ProductGroup;
use common\models\product\ProductUnit;
use common\models\TaxRate;
use common\components\helpers\ArrayHelper;
use frontend\modules\import\helpers\XmlHelper;
use common\components\moysklad\MoyskladObject;
use common\components\moysklad\MoyskladObjectType as ObjectType;

class NomenclatureXml extends AbstractXmlObject implements XmlInterface {

    public static $isGroup;

    private static $_okeiArray = [];

    public static function exists($companyId)
    {
        if (self::$isGroup) {
            return NomenclatureGroupXml::exists($companyId);
        }

        return MoyskladObject::find()
            ->byCompany($companyId)
            ->byObjectType(ObjectType::NOMENCLATURE)
            ->byCodeOrName(self::$data['code'], self::$data['title'])
            ->byValidated()
            ->exists();
    }

    public static function save($companyId)
    {
        if (self::$isGroup) {
            return NomenclatureGroupXml::save($companyId);
        }

        return (bool)Yii::$app->db->createCommand()->insert('moysklad_object', [
            'company_id' => $companyId,
            'object_type_id' => ObjectType::NOMENCLATURE,
            'object_name' => self::$data['title'],
            'object_code' => self::$data['code'],
            'one_c_object' => serialize(self::$data)
        ])->execute();
    }

    public static function load(\SimpleXMLElement $xmlObject)
    {
        self::$isGroup = self::_isGroup($xmlObject);

        if (self::$isGroup) {
            return NomenclatureGroupXml::load($xmlObject);
        }

        $unitObj = XmlHelper::getPropertyByName($xmlObject, 'ЕдиницаИзмерения');
        $groupObj = XmlHelper::getPropertyByName($xmlObject->{'Ссылка'}, 'Родитель');

        $props = XmlHelper::getProperties($xmlObject);
        $linkProps = XmlHelper::getProperties($xmlObject->{'Ссылка'});
        $unitProps = ($unitObj) ? XmlHelper::getProperties($unitObj->{'Ссылка'}) : [];
        $groupProps = ($groupObj) ? XmlHelper::getProperties($groupObj->{'Ссылка'}) : [];

        $ndsType = $props['ВидСтавкиНДС'] ?? 'БезНДС';
        $uid = $linkProps['{УникальныйИдентификатор}'] ?? null;

        self::$data = [
            'uid' => $uid,
            'group_name' => $groupProps['Наименование'] ?? null,
            'code' => $linkProps['Код'] ?? null,
            'title' => $linkProps['Наименование'] ?? null,
            'code_okei' => trim($unitProps['Код'] ?? ""),
            'article' => trim($props['Артикул'] ?? ""),
            'nds_id' => self::$dbConst['tax_rate_type'][$ndsType] ?? TaxRate::RATE_WITHOUT,
            'production_type' => (isset($props['Услуга']) && $props['Услуга'] === "true") ?
                Product::PRODUCTION_TYPE_SERVICE : Product::PRODUCTION_TYPE_GOODS,
        ];

        return true;
    }

    public static function convert2model(&$model, $data)
    {
        $groupId = ($data['group_name']) ? self::_findGroupId($model->company_id, $data['group_name']) : null;
        $unitId = self::_getUnitIdByOkei($data['code_okei']);

        ///////////////////////////
        $model->id = null;
        $model->isNewRecord = true;
        //$model->is_import_1c = 1;
        $model->one_c_imported = DocumentImportType::TYPE_MOYSKLAD;
        //$model->object_guid = $data['uid'];
        ///////////////////////////

        $model->production_type = $data['production_type'];
        $model->title = $data['title'];
        $model->code = $data['code'];
        $model->article = $data['article'];
        $model->has_excise = 0;
        $model->group_id = $groupId;
        $model->price_for_sell_nds_id = $data['nds_id'];
        $model->price_for_buy_nds_id = $data['nds_id'];
        $model->product_unit_id = $unitId ?: ProductUnit::UNIT_COUNT;
        $model->country_origin_id = 1; // todo
        // defaults
        $model->not_for_sale = 0;
        //$model->is_alco = 0; // master branch only!
        $model->is_traceable = 0;
        $model->has_excise = 0;
        $model->excise = 'Без акциза';

        return $model;
    }

    private static function _isGroup(\SimpleXMLElement $xmlObject)
    {
        $props = XmlHelper::getProperties($xmlObject);
        return $props['ЭтоГруппа'] === 'true';
    }

    private static function _getUnitIdByOkei($okei)
    {
        if (empty(self::$_okeiArray))
            self::$_okeiArray = ArrayHelper::map(ProductUnit::findAll([]), 'code_okei', 'id');

        return ArrayHelper::getValue($okei, self::$_okeiArray);
    }

    private static function _findGroupId($companyId, $groupName)
    {
        /** @var MoyskladObject $groupXml */
        $groupXml = MoyskladObject::find()->where([
            'company_id' => $companyId,
            'object_type_id' => ObjectType::NOMENCLATURE_GROUP,
            'object_name' => $groupName,
            'is_validated' => 1
        ])->one();

        return ($groupXml) ? $groupXml->getForeignKey() : null;
    }
}