<?php

namespace common\components\moysklad\parser\xml;

use common\models\document\Act;
use common\models\document\DocumentImportType;
use common\models\document\Order;
use common\models\document\OrderAct;
use common\models\document\OrderPackingList;
use common\models\document\PackingList;
use Yii;
use common\models\document\Invoice;
use common\models\product\Product;
use frontend\models\Documents;
use common\components\moysklad\MoyskladDocumentType as DocumentType;
use common\components\moysklad\MoyskladObjectType as ObjectType;

class PackingListXml extends AbstractXmlDocument implements XmlInterface {

    public static function load(\SimpleXMLElement $xmlObject)
    {
        // see UpdXml
    }

    public static function convert2model(&$model, $data)
    {
        // see UpdXml
    }

    /**
     * @return PackingList|null
     */
    public static function convert2dependentModel(PackingList &$model, Invoice $invoice, $data)
    {
        $IO_KEY = $data['document_type'];

        $numberArray = self::_getDocumentNumberArr($data['document_number'], $IO_KEY == Documents::IO_TYPE_OUT);

        ///////////////////////////
        $model->id = null;
        $model->isNewRecord = true;
        $model->company_id = $invoice->company_id;
        $model->contractor_id = $invoice->contractor_id;
        $model->invoice_id = $invoice->id;
        //$model->document_guid = $uid;
        $model->one_c_imported = DocumentImportType::TYPE_MOYSKLAD;
        ///////////////////////////

        $model->type = $IO_KEY;
        $model->document_date = $data['document_date'];
        $model->document_number = $numberArray['document_number'];
        $model->document_additional_number = $numberArray['document_additional_number'];

        $ownOrderArray = [];
        $ordersSum = 0;
        $orderNds = 0;
        foreach ($invoice->orders as $order) {
            if ($order->product)
            if ($order->product->production_type == Product::PRODUCTION_TYPE_GOODS) {
                $ownOrderArray[$order->id] = self::_createOrderPackingList($order, $order->quantity);
                $ordersSum += ($IO_KEY == Documents::IO_TYPE_OUT) ? $order->amount_sales_with_vat : $order->amount_purchase_with_vat;
                $orderNds += ($IO_KEY == Documents::IO_TYPE_OUT)
                    ? ($order->amount_sales_with_vat - $order->amount_sales_no_vat)
                    : ($order->amount_purchase_with_vat - $order->amount_purchase_no_vat);
            }
        }

        if ($ownOrderArray) {

            $model->orders_sum = $ordersSum;
            $model->order_nds = $orderNds;
            $model->populateRelation('invoice', $invoice);
            $model->populateRelation('orderPackingLists', $ownOrderArray);

            return $model;
        }

        return null;
    }

    private static function _createOrderPackingList(Order $order, $count)
    {
        return new OrderPackingList([
            'packing_list_id' => null,
            'order_id' => $order->id,
            'product_id' => $order->product_id,
            'quantity' => $count,
        ]);
    }
}