<?php

namespace common\components\moysklad\parser\xml;

use common\models\TaxRate;

class AbstractXml {

    public static $dbConst = [
        'tax_rate_type' => [
            'Общая' => TaxRate::RATE_20,
            'ОбщаяРасчетная' => TaxRate::RATE_20, // todo: ?? 20/120
            'Пониженная' => TaxRate::RATE_10,
            'ПониженнаяРасчетная' => TaxRate::RATE_10, // todo: ?? 10/110
            'Нулевая' => TaxRate::RATE_0,
            'БезНДС' => TaxRate::RATE_WITHOUT,
        ],
        'tax_rate' => [
            'НДС20' => TaxRate::RATE_20,
            'НДС18' => TaxRate::RATE_18,
            'НДС10' => TaxRate::RATE_10,
            'НДС0' => TaxRate::RATE_0,
            'БезНДС' => TaxRate::RATE_WITHOUT,
            'НДС20_120' => TaxRate::RATE_20, // todo: ?? 20/120
            'НДС18_118' => TaxRate::RATE_18, // todo: ?? 18/118
            'НДС10_110' => TaxRate::RATE_10, // todo: ?? 10/110
        ],
        'currency' => [
            '643' => 'RUB',
            '398' => 'KZT',
            '840' => 'USD',
            '933' => 'BYN',
            '978' => 'EUR',
            '980' => 'UAH'
        ]
    ];

}