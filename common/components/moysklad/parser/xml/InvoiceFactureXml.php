<?php

namespace common\components\moysklad\parser\xml;

use common\models\document\DocumentImportType;
use Yii;
use common\models\document\Invoice;
use common\models\document\InvoiceFacture;
use common\models\document\Order;
use common\models\document\OrderInvoiceFacture;
use common\models\document\Upd;
use frontend\models\Documents;
use common\components\helpers\ArrayHelper;
use frontend\modules\import\helpers\XmlHelper;
use common\components\moysklad\MoyskladObject;
use common\components\moysklad\MoyskladDocument;
use common\components\moysklad\MoyskladObjectType as ObjectType;
use common\components\moysklad\MoyskladDocumentType as DocumentType;

class InvoiceFactureXml extends AbstractXmlDocument implements XmlInterface {

    public static function load(\SimpleXMLElement $xmlObject)
    {
        $IO_KEY = self::$ioType;
        $documentTypeId = ($IO_KEY == Documents::IO_TYPE_OUT) ? DocumentType::OUT_INVOICE_FACTURE : DocumentType::IN_INVOICE_FACTURE;

        $contractorObj = XmlHelper::getPropertyByName($xmlObject, 'Контрагент');
        $agreementObj = XmlHelper::getPropertyByName($xmlObject, 'ДоговорКонтрагента');
        $tableBaseDocObj = XmlHelper::getTagByName($xmlObject, 'ТабличнаяЧасть', 'ДокументыОснования');

        $props = XmlHelper::getProperties($xmlObject);
        $linkProps = XmlHelper::getProperties($xmlObject->{'Ссылка'});
        $contractorProps = ($contractorObj) ? XmlHelper::getProperties($contractorObj->{'Ссылка'}) : [];
        $agreementProps = ($agreementObj) ? XmlHelper::getProperties($agreementObj->{'Ссылка'}) : [];

        $baseDocs = [];
        $basePaymentOrders = [];
        if ($tableBaseDocObj && $tableBaseDocObj->children()->count()) {
            for ($i = 0; $i < $tableBaseDocObj->children()->count(); $i++) {
                if ($recordObj = $tableBaseDocObj->children()->{'Запись'}[$i]) {
                    $recordProps = XmlHelper::getProperties($recordObj);
                    $baseObj = XmlHelper::getPropertyByName($recordObj, 'ДокументОснование');
                    $baseProps = ($baseObj) ? XmlHelper::getProperties($baseObj->{'Ссылка'}) : [];
                    $base = [
                        'document_date' => substr($baseProps['Дата'], 0, 10),
                        'document_number' => $baseProps['Номер'],
                    ];
                    switch ($baseObj->attributes()->{'Тип'}) {
                        case 'ДокументСсылка.ПриходныйКассовыйОрдер':
                        case 'ДокументСсылка.РасходныйКассовыйОрдер':
                            $basePaymentOrders[] = $base;
                            break;
                        case 'ДокументСсылка.ПоступлениеТоваровУслуг':
                        case 'ДокументСсылка.РеализацияТоваровУслуг':
                            $baseDocs[] = $base;
                            break;
                    }
                }
            }
        }

        $uid = $linkProps['{УникальныйИдентификатор}'] ?? null;

        if ($IO_KEY == Documents::IO_TYPE_IN) {
            $documentDate = ($linkProps['ДатаВходящегоДокумента'] ?? null) ?: ($linkProps['Дата'] ?? null);
            $documentNumber = ($linkProps['НомерВходящегоДокумента'] ?? null) ?: (($linkProps['Номер'] ?? null) ?: 'б/н');
        }
        else if ($IO_KEY == Documents::IO_TYPE_OUT) {
            $documentDate = $linkProps['Дата'] ?? null;
            $documentNumber = $linkProps['Номер'] ?? null;
        } else {
            $documentDate = $documentNumber = null;
        }

        self::$data = [
            'uid' => $uid,
            'document_type_id' => $documentTypeId,
            'document_type' => $IO_KEY,
            'document_date' => substr($documentDate, 0, 10),
            'document_number' => $documentNumber,
            'document_amount' => number_format($props['СуммаДокумента'] ?? 0, 2),
            'contractor_name' => $contractorProps['Наименование'] ?? null,
            'contractor_inn' => $contractorProps['ИНН'] ?? null,
            'contractor_kpp' => $contractorProps['КПП'] ?? null,
            'base_documents' => $baseDocs,
            'base_payment_orders' => $basePaymentOrders
        ];
    }

    public static function convert2model(&$model, $data) {
        // not used
    }

    public static function convert2dependentModel(InvoiceFacture &$model, array $invoices, $data)
    {
        $IO_KEY = $data['document_type'];

        $numberArray = self::_getDocumentNumberArr($data['document_number'], $IO_KEY == Documents::IO_TYPE_OUT);

        //////////////////////////////
        $model->id = null;
        $model->isNewRecord = true;
        //$model->object_guid = $uid;
        $model->one_c_imported = DocumentImportType::TYPE_MOYSKLAD;
        //////////////////////////////

        $model->type = ($IO_KEY == Documents::IO_TYPE_IN) ? Documents::IO_TYPE_IN : Documents::IO_TYPE_OUT;
        $model->document_date = $data['document_date'];
        $model->document_number = $numberArray['document_number'];
        $model->document_additional_number = $numberArray['document_additional_number'];
        $model->auto_created = false;

        // orders
        $ownOrderArray = [];
        if ($invoices) {
            foreach ($invoices as $invoice) {
                foreach ($invoice->orders as $order) {
                    $ownOrderArray[] = self::_createOrderInvoiceFacture($order);
                }
            }
        }

        $model->populateRelation('invoice', $invoices[0]);
        $model->populateRelation('invoices', $invoices);
        $model->populateRelation('ownOrders', $ownOrderArray);

        return $model;
    }

    private function _createOrderInvoiceFacture(Order $order)
    {
        return new OrderInvoiceFacture([
            'invoice_facture_id' => null,
            'order_id' => $order->id,
            'quantity' => $order->quantity,
            'country_id' => $order->country_id,
            'custom_declaration_number' => $order->custom_declaration_number,
        ]);
    }
}