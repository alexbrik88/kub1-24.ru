<?php
namespace common\components\moysklad\parser;

use common\components\moysklad\MoyskladDocument;
use common\components\moysklad\MoyskladObject;
use common\components\moysklad\parser\xml\InvoiceFactureXml;
use common\components\moysklad\parser\xml\PaymentOrderXml;
use Yii;
use common\components\moysklad\helper\MoyskladImportStatistics;
use common\components\moysklad\MoyskladImport;
use common\components\moysklad\parser\xml\InvoiceXml;
use common\components\moysklad\parser\xml\StoreXml;
use common\components\moysklad\parser\xml\UpdXml;
use common\models\Company;
use common\models\employee\Employee;
use frontend\models\Documents;
use yii\base\Exception;
use common\components\moysklad\MoyskladObjectType as ObjectType;
use common\components\moysklad\MoyskladDocumentType as DocumentType;
use common\components\moysklad\parser\xml\AgreementXml;
use common\components\moysklad\parser\xml\BankAccountXml;
use common\components\moysklad\parser\xml\BankXml;
use common\components\moysklad\parser\xml\NomenclatureXml;
use common\components\moysklad\parser\xml\ContractorXml;
use yii\db\Transaction;

class MoyskladParser {

    const TRANSACTION_MAX_ROWS = 1000;
    const TRANSACTION_ISOLATION_LEVEL = Transaction::READ_COMMITTED;

    public static $SOURCE_OBJECTS = [
        ObjectType::CONTRACTOR => 'Контрагенты',
        //ObjectType::AGREEMENT => 'ДоговорыКонтрагентов', // todo
        ObjectType::BANK => 'Банки',
        ObjectType::BANK_ACCOUNT => 'БанковскиеСчета',
        ObjectType::NOMENCLATURE => 'Номенклатура',
        ObjectType::STORE => 'Склады',
    ];

    public static $SOURCE_DOCUMENTS = [
        DocumentType::IN_INVOICE => 'СчетНаОплатуПоставщика',
        DocumentType::OUT_INVOICE => 'СчетНаОплатуПокупателю',
        DocumentType::IN_UPD => 'ПоступлениеТоваровУслуг',
        DocumentType::OUT_UPD => 'РеализацияТоваровУслуг',
        DocumentType::IN_INVOICE_FACTURE => 'СчетФактураПолученный',
        DocumentType::OUT_INVOICE_FACTURE => 'СчетФактураВыданный',
        DocumentType::IN_PAYMENT_ORDER => 'РасходныйКассовыйОрдер',
        DocumentType::OUT_PAYMENT_ORDER => 'ПриходныйКассовыйОрдер',
    ];

    public static $GETTER_CLASS = [
        ObjectType::CONTRACTOR => ContractorXml::class,
        ObjectType::AGREEMENT => AgreementXml::class,
        ObjectType::BANK => BankXml::class,
        ObjectType::BANK_ACCOUNT => BankAccountXml::class,
        ObjectType::NOMENCLATURE => NomenclatureXml::class,
        ObjectType::STORE => StoreXml::class,
        DocumentType::IN_INVOICE => InvoiceXml::class,
        DocumentType::OUT_INVOICE => InvoiceXml::class,
        DocumentType::IN_UPD => UpdXml::class,
        DocumentType::OUT_UPD => UpdXml::class,
        DocumentType::IN_INVOICE_FACTURE => InvoiceFactureXml::class,
        DocumentType::OUT_INVOICE_FACTURE => InvoiceFactureXml::class,
        DocumentType::IN_PAYMENT_ORDER => PaymentOrderXml::class,
        DocumentType::OUT_PAYMENT_ORDER => PaymentOrderXml::class,
    ];

    private static $_IN_DOCUMENTS = [
        DocumentType::IN_INVOICE,
        DocumentType::IN_UPD,
        DocumentType::IN_INVOICE_FACTURE,
        DocumentType::IN_PAYMENT_ORDER
    ];
    private static $_OUT_DOCUMENTS = [
        DocumentType::OUT_INVOICE,
        DocumentType::OUT_UPD,
        DocumentType::OUT_INVOICE_FACTURE,
        DocumentType::OUT_PAYMENT_ORDER
    ];

    /**
     * @var MoyskladImportStatistics
     */
    public $stat;

    private $_fileDescriptor;
    private $_fileStringNum = 0;

    private $_import;
    private $_company;
    private $_employee;

    /**
     * @var MoyskladImport $import
     */
    public function __construct(MoyskladImport $import)
    {
        $this->_import = $import;
        $this->_employee = $import->employee;
        $this->_company = $import->company;
    }

    /**
     * @param $fileName
     * @throws Exception
     */
    public function open($fileName)
    {
        if (is_file($fileName))
            $this->_fileDescriptor = fopen($fileName, 'r');
        else
            throw new Exception('File not found');
    }

    /**
     * @throws Exception
     */
    public function close()
    {
        if ($this->_fileDescriptor)
            fclose($this->_fileDescriptor);
        else
            throw new Exception('File not found');
    }

    /**
     * @throws Exception
     */
    public function parse()
    {
        if (!$this->_fileDescriptor) {
            throw new Exception('File not found');
        }

        Yii::$app->db->getTableSchema(MoyskladDocument::tableName());
        Yii::$app->db->getTableSchema(MoyskladObject::tableName());

        // Transaction init ////////////////////////////////////
        $transaction = new Transaction(['db' => Yii::$app->db]);
        $row = 0;
        ////////////////////////////////////////////////////////

        try {

            while ($xmlObject = $this->_getTag('Объект')) {

                // Transaction begin //////////////////////////////////////////////
                if ($row === 0) {
                    $transaction->begin(self::TRANSACTION_ISOLATION_LEVEL);
                    $row++;
                }
                ///////////////////////////////////////////////////////////////////

                $tagName = (string)$xmlObject->attributes()->{'ИмяПравила'};

                if (($tagId = array_search($tagName, self::$SOURCE_OBJECTS)) !== false) {
                    $this->addObject($tagId, $xmlObject);
                    $row++;
                } elseif (($tagId = array_search($tagName, self::$SOURCE_DOCUMENTS)) !== false) {
                    $this->addDocument($tagId, $xmlObject);
                    $row++;
                } else {
                    continue;
                }

                // Transaction end //////////////////////////////////////////////////////
                if ($transaction->isActive && ($row + 1 >= self::TRANSACTION_MAX_ROWS)) {
                    $transaction->commit();
                    $row = 0;
                }
                /////////////////////////////////////////////////////////////////////////
            }

            // Transaction end /////////////////////////////////////////////////
            if ($transaction->isActive && $row > 0) {
                $transaction->commit();
            }
            ///////////////////////////////////////////////////////////////////

        } catch (\Throwable $e) {

            if ($transaction->isActive)
                $transaction->rollBack();

            throw new Exception($e);
        }
    }

    private function addObject($tagId, $xmlObject)
    {
        $this->stat->incrementFile($tagId);

        if ($getterClass = (self::$GETTER_CLASS[$tagId] ?? null)) {

            $isLoaded = call_user_func_array([$getterClass, 'load'], [$xmlObject]);
            $isExists = call_user_func_array([$getterClass, 'exists'], [$this->_company->id]);

            if (!$isExists) {
                try {
                    $isSaved = call_user_func_array([$getterClass, 'save'], [$this->_company->id]);
                } catch (\Exception $e) {
                    $isSaved = false;
                }
            } else {

                $this->stat->incrementDouble($tagId);
            }
        }
    }

    private function addDocument($tagId, $xmlObject)
    {
        $this->stat->incrementFile($tagId);

        if ($getterClass = (self::$GETTER_CLASS[$tagId] ?? null)) {

            //////////////////////////////////////////////////
            $getterClass::$ioType = self::getTagIoType($tagId);
            //////////////////////////////////////////////////

            $isLoaded = call_user_func_array([$getterClass, 'load'], [$xmlObject]);
            $isExists = call_user_func_array([$getterClass, 'exists'], [$this->_company->id]);

            if (!$isExists) {
                try {
                    $isSaved = call_user_func_array([$getterClass, 'save'], [$this->_company->id]);
                } catch (\Exception $e) {
                    $isSaved = false;
                }
            } else {

                $this->stat->incrementDouble($tagId);
            }
        }
    }

    private static function getTagIoType($tagId)
    {
        return (in_array($tagId, self::$_IN_DOCUMENTS)) ? Documents::IO_TYPE_IN : Documents::IO_TYPE_OUT;
    }

    private function _getTag($TAG)
    {
        $tagOpen = $tagClosed = 0;
        $object = '';
        while (($line = fgets($this->_fileDescriptor)) !== false) {

            $this->_fileStringNum++;

            if (strpos($line, "<{$TAG}") !== false) {
                $tagOpen++;
            }
            elseif (strpos($line, "</{$TAG}>") !== false) {
                $tagClosed++;
            }

            if ($tagOpen === 1) {
                $object .= $line;
            }
            if ($tagClosed === 1) {
                if ($xml = simplexml_load_string($object, "\SimpleXMLElement", LIBXML_NOCDATA)) {
                    return $xml;
                } else {
                    throw new Exception('Parsing Error #3 ('.$this->_fileStringNum.')');
                }
            }
            elseif ($tagOpen > 1 || $tagClosed > 1) {
                throw new Exception('Parsing Error #2 ('.$this->_fileStringNum.')');
            }
        }

        if (($tagOpen && !$tagClosed) || (!$tagOpen && $tagClosed)) {
            throw new Exception('Parsing Error #1 ('.$this->_fileStringNum.')');
        }

        return null;
    }
}