<?php

namespace common\components\moysklad;

use Yii;
use common\models\Company;
use common\models\Contractor;
use common\models\Agreement;
use common\models\product\Product;
use common\models\product\ProductGroup;
use common\components\moysklad\MoyskladObjectType as ObjectType;

/**
 * This is the model class for table "moysklad_object".
 *
 * @property int $id
 * @property int $company_id
 * @property int $object_type_id
 * @property string|null $object_name
 * @property string|null $object_code
 * @property string|null $object_date
 * @property string|null $object_number
 * @property string|null $object_inn
 * @property string|null $object_kpp
 * @property int|null $agreement_id
 * @property int|null $contractor_id
 * @property int|null $product_id
 * @property int|null $product_group_id
 * @property int|null $is_validated
 * @property int|null $validated_at
 * @property string|null $one_c_object
 *
 * @property Agreement $agreement
 * @property Company $company
 * @property Contractor $contractor
 * @property MoyskladObjectType $objectType
 * @property Product $product
 * @property ProductGroup $productGroup
 */
class MoyskladObject extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'moysklad_object';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'object_type_id'], 'required'],
            [['company_id', 'object_type_id', 'agreement_id', 'contractor_id', 'product_id', 'product_group_id', 'is_validated', 'validated_at'], 'integer'],
            [['object_date'], 'safe'],
            [['one_c_object'], 'string'],
            [['object_name'], 'string', 'max' => 255],
            [['object_code', 'object_number'], 'string', 'max' => 64],
            [['object_inn', 'object_kpp'], 'string', 'max' => 16],
            [['agreement_id'], 'exist', 'skipOnError' => true, 'targetClass' => Agreement::class, 'targetAttribute' => ['agreement_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['company_id' => 'id']],
            [['contractor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contractor::class, 'targetAttribute' => ['contractor_id' => 'id']],
            [['object_type_id'], 'exist', 'skipOnError' => true, 'targetClass' => MoyskladObjectType::class, 'targetAttribute' => ['object_type_id' => 'id']],
            [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::class, 'targetAttribute' => ['product_id' => 'id']],
            [['product_group_id'], 'exist', 'skipOnError' => true, 'targetClass' => ProductGroup::class, 'targetAttribute' => ['product_group_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'object_type_id' => 'Object Type ID',
            'object_name' => 'Object Name',
            'object_code' => 'Object Code',
            'object_date' => 'Object Date',
            'object_number' => 'Object Number',
            'object_inn' => 'Object Inn',
            'object_kpp' => 'Object Kpp',
            'agreement_id' => 'Agreement ID',
            'contractor_id' => 'Contractor ID',
            'product_id' => 'Product ID',
            'product_group_id' => 'Product Group ID',
            'is_validated' => 'Is Validated',
            'validated_at' => 'Validated At',
            'one_c_object' => 'One C Object',
        ];
    }

    /**
     * @return query\MoyskladObjectQuery
     */
    public static function find($config = [])
    {
        return new query\MoyskladObjectQuery(get_called_class(), $config);
    }

    /**
     * Gets query for [[Agreement]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAgreement()
    {
        return $this->hasOne(Agreement::class, ['id' => 'agreement_id']);
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * Gets query for [[Contractor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getContractor()
    {
        return $this->hasOne(Contractor::class, ['id' => 'contractor_id']);
    }

    /**
     * Gets query for [[ObjectType]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getObjectType()
    {
        return $this->hasOne(MoyskladObjectType::class, ['id' => 'object_type_id']);
    }

    /**
     * Gets query for [[Product]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProduct()
    {
        return $this->hasOne(Product::class, ['id' => 'product_id']);
    }

    /**
     * Gets query for [[ProductGroup]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductGroup()
    {
        return $this->hasOne(ProductGroup::class, ['id' => 'product_group_id']);
    }

    /**
     * @return array
     */
    public function getData()
    {
        return ($this->one_c_object) ? unserialize($this->one_c_object) : [];
    }

    public function setForeignKey($fkID)
    {
        if ($foreignKeyName = $this->getCurrentForeignKeyName()) {
            $this->updateAttributes([
                $foreignKeyName => $fkID,
                'is_validated' => 1,
                'validated_at' => time()
            ]);
        } else {
            $this->updateAttributes([
                'is_validated' => 1,
                'validated_at' => time()
            ]);
        }
    }

    public function setNoForeign()
    {
        $this->updateAttributes([
            'is_validated' => 0,
            'validated_at' => time()
        ]);
    }

    public function getForeignKey()
    {
        if ($foreignKeyName = $this->getCurrentForeignKeyName())
            return $this->{$foreignKeyName};

        return null;
    }

    private function getCurrentForeignKeyName()
    {
        switch ($this->object_type_id) {
            case ObjectType::CONTRACTOR:
                return 'contractor_id';
            case ObjectType::AGREEMENT:
                return 'agreement_id';
            case ObjectType::NOMENCLATURE_GROUP:
                return 'product_group_id';
            case ObjectType::NOMENCLATURE:
                return 'product_id';
            case ObjectType::BANK:
            case ObjectType::BANK_ACCOUNT:
            default:
                return null;
        }
    }
}
