<?php

namespace common\components\moysklad;

use Yii;

/**
 * This is the model class for table "moysklad_document_validation".
 *
 * @property int $import_id
 * @property int $document_id
 * @property string|null $errors
 *
 * @property MoyskladDocument $document
 * @property MoyskladImport $import
 */
class MoyskladDocumentValidation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'moysklad_document_validation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['import_id', 'document_id'], 'required'],
            [['import_id', 'document_id'], 'integer'],
            [['errors'], 'string'],
            [['import_id', 'document_id'], 'unique', 'targetAttribute' => ['import_id', 'document_id']],
            [['document_id'], 'exist', 'skipOnError' => true, 'targetClass' => MoyskladDocument::class, 'targetAttribute' => ['document_id' => 'id']],
            [['import_id'], 'exist', 'skipOnError' => true, 'targetClass' => MoyskladImport::class, 'targetAttribute' => ['import_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'import_id' => 'Import ID',
            'document_id' => 'Document ID',
            'errors' => 'Errors',
        ];
    }

    /**
     * Gets query for [[Document]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDocument()
    {
        return $this->hasOne(MoyskladDocument::class, ['id' => 'document_id']);
    }

    /**
     * Gets query for [[Import]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getImport()
    {
        return $this->hasOne(MoyskladImport::class, ['id' => 'import_id']);
    }
}
