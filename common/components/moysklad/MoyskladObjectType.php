<?php

namespace common\components\moysklad;

use Yii;

/**
 * This is the model class for table "moysklad_object_type".
 *
 * @property int $id
 * @property string|null $name
 */
class MoyskladObjectType extends \yii\db\ActiveRecord
{
    const CONTRACTOR = 1;
    CONST AGREEMENT = 2;
    const BANK = 3;
    const BANK_ACCOUNT = 4;
    const NOMENCLATURE = 5;
    const NOMENCLATURE_GROUP = 6;
    const STORE = 7;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'moysklad_object_type';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
        ];
    }

    /**
     * Gets query for [[MoyskladObjects]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMoyskladObjects()
    {
        return $this->hasMany(MoyskladObject::class, ['object_type_id' => 'id']);
    }
}
