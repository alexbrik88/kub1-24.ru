<?php
namespace common\components\moysklad\converter\kub;

use common\components\moysklad\helper\MoyskladImportStatistics;
use common\components\moysklad\helper\MoyskladValidationErrors;
use Yii;
use yii\db\Connection;
use common\components\moysklad\MoyskladObject;
use common\components\moysklad\MoyskladObjectType as ObjectType;
use common\components\moysklad\parser\xml\NomenclatureXml;
use common\models\product\Product;

trait ProductsTrait {

    public static $productColumns = [
        'company_id',
        'country_origin_id',
        'price_for_buy_nds_id',
        'price_for_sell_nds_id',
        'production_type', 
        'title', 
        'code', 
        'article', 
        'has_excise', 
        'group_id', 
        'product_unit_id', 
        'box_type', 
        'count_in_place', 
        'count_in_package', 
        'place_count', 
        'customs_declaration_number', 
        'mass_gross', 
        'mass_net', 
        'weight',
        'volume', 
        'created_at', 
        'excise', 
        'not_for_sale', 
        //'is_alco', // master branch only!
        'is_traceable',
    ];

    public function insertProducts()
    {
        /** @var MoyskladObject $xmlProduct */
        /** @var Product $kubProduct */
        /** @var MoyskladImportStatistics $stat */
        /** @var MoyskladValidationErrors $validation */

        $stat = $this->stat;
        $validation = $this->validation;
        $model = new Product([
            'company_id' => $this->_company->id,
            'country_origin_id' => 1
        ]);

        $newXmlProductsIds = $this->getNewNomenclatureProducts();

        $productRows = [];
        foreach ($newXmlProductsIds as $xmlProductId) {

            if ($xmlProduct = MoyskladObject::findOne($xmlProductId)) {
                $data = $xmlProduct->getData();

                if ($kubID = self::getExistsProductId($data)) {
                    if (!$xmlProduct->getForeignKey()) {
                        $xmlProduct->setForeignKey($kubID);
                    }
                    continue;
                }
            }

            $kubProduct = NomenclatureXml::convert2model($model, $data);

            if (self::NO_VALIDATE_PRODUCTS || $kubProduct->validate()) {

                $isSaved = true; // see insertProductsToDb()

                //$isSaved = Yii::$app->db->transaction(function (Connection $db) use ($kubProduct, $xmlProduct) {
                //    if ($kubProduct->save(false)) {
                //        $xmlProduct->setForeignKey($kubProduct->id);
                //        return true;
                //    }
                //
                //    $db->transaction->rollBack();
                //    return false;
                //});
                
                $productRows[] = $kubProduct->getAttributes(self::$productColumns);

                if ($isSaved) {
                    $stat->incrementUploaded(ObjectType::NOMENCLATURE);
                } else {
                    $xmlProduct->setNoForeign();
                    $validation->addObjectError($xmlProduct, $kubProduct);
                }

            } else {
                $xmlProduct->setNoForeign();
                $validation->addObjectError($xmlProduct, $kubProduct);
            }
        }

        $this->insertProductsToDb($productRows);
    }

    private function getNewNomenclatureProducts()
    {
        return MoyskladObject::find()
            ->byCompany($this->_company->id)
            ->byObjectType(ObjectType::NOMENCLATURE)
            ->byNew()
            ->select('id')
            ->column();
    }

    private function insertProductsToDb($productRows)
    {
        if ($productRows) {
            try {
                // insert products
                Yii::$app->db->createCommand()
                    ->batchInsert(Product::tableName(), self::$productColumns, $productRows)
                    ->execute();

                // update foreign keys by code
                Yii::$app->db->createCommand('
                UPDATE `moysklad_object` mo
                LEFT JOIN `product` p ON mo.company_id = p.company_id AND mo.object_code = p.code
                SET 
                  mo.product_id = p.id,
                  mo.is_validated = 1,
                  mo.validated_at = UNIX_TIMESTAMP()                    
                WHERE 
                  mo.company_id = ' . $this->_company->id . '
                  AND mo.object_type_id = ' . ObjectType::NOMENCLATURE . '
                  AND mo.product_id IS NULL
                  AND mo.object_code IS NOT NULL
              ')->execute();

                // update foreign keys by title
                Yii::$app->db->createCommand('
                UPDATE `moysklad_object` mo
                LEFT JOIN `product` p ON mo.company_id = p.company_id AND mo.object_name = p.title
                SET 
                  mo.product_id = p.id,
                  mo.is_validated = 1,
                  mo.validated_at = UNIX_TIMESTAMP()  
                WHERE 
                  mo.company_id = ' . $this->_company->id . '
                  AND mo.object_type_id = ' . ObjectType::NOMENCLATURE . '                  
                  AND mo.product_id IS NULL
                  AND mo.object_code IS NULL
              ')->execute();

            } catch (\Throwable $e) {
                return false;
            }
        }

        return true;
    }
}