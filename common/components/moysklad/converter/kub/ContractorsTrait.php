<?php
namespace common\components\moysklad\converter\kub;

use common\components\moysklad\helper\MoyskladImportStatistics;
use common\components\moysklad\helper\MoyskladValidationErrors;
use common\components\moysklad\parser\xml\AbstractXml;
use common\models\Agreement;
use common\models\ContractorAccount;
use frontend\models\Documents;
use Yii;
use yii\base\Exception;
use yii\db\Connection;
use common\components\moysklad\MoyskladObject;
use common\components\moysklad\MoyskladObjectType as ObjectType;
use common\components\moysklad\parser\xml\ContractorXml;
use common\models\Contractor;
use yii\db\Transaction;

trait ContractorsTrait {

    public function insertContractors()
    {
        /** @var MoyskladObject $xmlContractor */
        /** @var Contractor $kubContractor */
        /** @var MoyskladImportStatistics $stat */
        /** @var MoyskladValidationErrors $validation */

        $stat = $this->stat;
        $validation = $this->validation;
        $model = new Contractor([
            'company_id' => $this->_company->id,
            'employee_id' => $this->_employee->id
        ]);
        $model->seller_payment_delay = $this->_company->seller_payment_delay;
        $model->customer_payment_delay = $this->_company->customer_payment_delay;
        $accountModel = new ContractorAccount(['contractor_id' => null]);
        $newXmlContractors = $this->getNewContractors();

        // Transaction init ////////////////////////////////////
        $transaction = new Transaction(['db' => Yii::$app->db]);
        $row = 0;
        ////////////////////////////////////////////////////////

        try {

            foreach ($newXmlContractors as $xmlContractor) {

                // Transaction begin //////////////////////////////////////////////
                if ($row === 0) {
                    $transaction->begin(self::TRANSACTION_ISOLATION_LEVEL);
                    $row++;
                }
                ///////////////////////////////////////////////////////////////////

                $data = $xmlContractor->getData();

                if ($kubID = self::getExistsContractorId($data)) {
                    if (!$xmlContractor->getForeignKey()) {
                        $xmlContractor->setForeignKey($kubID);
                    }
                    continue;
                }

                $kubContractor = ContractorXml::convert2model($model, $data);

                if (self::NO_VALIDATE_CONTRACTORS || $kubContractor->validate()) {

                    $isSaved = $kubContractor->save(false);

                    if ($isSaved) {
                        $xmlContractor->setForeignKey($kubContractor->id);
                        $stat->incrementUploaded(ObjectType::CONTRACTOR);
                        // add Accounts
                        $newXmlAccounts = $this->getNewContractorAccounts($data);
                        $accountModel->contractor_id = $kubContractor->id;
                        $uploadedCount = $this->insertContractorAccounts($accountModel, $newXmlAccounts);
                        $stat->incrementUploaded(ObjectType::BANK_ACCOUNT, $uploadedCount);

                    } else {
                        $xmlContractor->setNoForeign();
                        $validation->addObjectError($xmlContractor, $kubContractor);
                    }

                } else {
                    $xmlContractor->setNoForeign();
                    $validation->addObjectError($xmlContractor, $kubContractor);
                }

                // Transaction end //////////////////////////////////////////////////////
                if ($transaction->isActive && ($row + 1 >= self::TRANSACTION_MAX_ROWS)) {
                    $transaction->commit();
                    $row = 0;
                }
                /////////////////////////////////////////////////////////////////////////
            }

            // Transaction end /////////////////////////////////////////////////
            if ($transaction->isActive && $row > 0) {
                $transaction->commit();
            }
            ///////////////////////////////////////////////////////////////////

        } catch (\Throwable $e) {

            if ($transaction->isActive)
                $transaction->rollBack();

            throw new Exception($e);
        }
    }

    private function getNewContractors()
    {
        return MoyskladObject::find()
            ->byCompany($this->_company->id)
            ->byObjectType(ObjectType::CONTRACTOR)
            ->byNew()
            ->all();
    }
}