<?php
namespace common\components\moysklad\converter\kub;

use common\components\moysklad\helper\MoyskladImportStatistics;
use common\components\moysklad\helper\MoyskladValidationErrors;
use Yii;
use yii\db\Connection;
use common\components\moysklad\MoyskladObject;
use common\components\moysklad\MoyskladObjectType as ObjectType;
use common\components\moysklad\parser\xml\NomenclatureGroupXml;
use common\models\product\ProductGroup;

trait ProductGroupsTrait {

    public function insertProductGroups()
    {
        /** @var MoyskladObject $xmlGroup */
        /** @var ProductGroup $kubGroup */
        /** @var MoyskladImportStatistics $stat */
        /** @var MoyskladValidationErrors $validation */

        $stat = $this->stat;
        $validation = $this->validation;
        $model = new ProductGroup([
            'company_id' => $this->_company->id,
        ]);

        $newXmlGroups = $this->getNewNomenclatureGroups();

        foreach ($newXmlGroups as $xmlGroup) {
            $data = $xmlGroup->getData();

            if ($kubID = self::getExistsProductGroupId($data)) {
                if (!$xmlGroup->getForeignKey()) {
                    $xmlGroup->setForeignKey($kubID);
                }
                continue;
            }

            $kubGroup = NomenclatureGroupXml::convert2model($model, $data);

            if ($kubGroup->validate()) {
                $isSaved = Yii::$app->db->transaction(function (Connection $db) use ($kubGroup, $xmlGroup) {
                    if ($kubGroup->save(false)) {
                        $xmlGroup->setForeignKey($kubGroup->id);
                        return true;
                    }

                    $db->transaction->rollBack();
                    return false;
                });

                if ($isSaved) {
                    $stat->incrementUploaded(ObjectType::NOMENCLATURE_GROUP);
                } else {
                    $xmlGroup->setNoForeign();
                    $validation->addObjectError($xmlGroup, $kubGroup);
                }

            } else {
                $xmlGroup->setNoForeign();
                $validation->addObjectError($xmlGroup, $kubGroup);
            }
        }
    }

    private function getNewNomenclatureGroups()
    {
        return MoyskladObject::find()
            ->byCompany($this->_company->id)
            ->byObjectType(ObjectType::NOMENCLATURE_GROUP)
            ->byNew()
            ->all();
    }
}