<?php
namespace common\components\moysklad\converter\kub;

use common\components\moysklad\helper\MoyskladImportStatistics;
use common\components\moysklad\helper\MoyskladValidationErrors;
use common\components\moysklad\parser\xml\BankAccountXml;
use common\models\ContractorAccount;
use Yii;
use yii\db\Connection;
use common\components\moysklad\MoyskladObject;
use common\components\moysklad\MoyskladObjectType as ObjectType;
use common\components\moysklad\parser\xml\ContractorXml;
use common\models\Contractor;

trait ContractorAccountsTrait
{
    public function insertContractorAccounts(ContractorAccount $account, array $xmlAccounts)
    {
        /** @var MoyskladObject $accountXml */
        /** @var MoyskladObject $bankXml */
        /** @var MoyskladImportStatistics $stat */
        /** @var MoyskladValidationErrors $validation */

        $stat = $this->stat;
        $validation = $this->validation;
        $uploadedCount = 0;
        foreach ($xmlAccounts as $accountXml) {
            $data = $accountXml->getData();
            if ($bankXml = self::getContractorBank($data)) {
                $bankData = $bankXml->getData();
                $accountKub = BankAccountXml::convert2model($account, $data + $bankData);
                if ($accountKub->validate()) {
                    $accountKub->save(false);
                    $uploadedCount++;
                } else {
                    $validation->addObjectError($accountXml, $accountKub);
                }
            }
        }

        return $uploadedCount;
    }

    private function getNewContractorAccounts($data)
    {
        return MoyskladObject::find()
            ->byCompany($this->_company->id)
            ->byObjectType(ObjectType::BANK_ACCOUNT)
            ->byContractor([
                'name' => $data['name'],
                'inn' => $data['ITN'],
                'kpp' => $data['PPC']
            ])
            ->byNew()
            ->all();
    }

    private function getContractorBank($data)
    {
        return MoyskladObject::find()
            ->byCompany($this->_company->id)
            ->byObjectType(ObjectType::BANK)
            ->byNumber($data['bank_bik'])
            ->one();
    }
}