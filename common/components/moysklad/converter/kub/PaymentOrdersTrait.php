<?php
namespace common\components\moysklad\converter\kub;

use common\models\cash\CashOrderFlowToInvoice;
use frontend\models\Documents;
use Yii;
use yii\base\Exception;
use yii\db\Connection;
use common\components\moysklad\MoyskladObject;
use common\components\moysklad\MoyskladDocument;
use common\components\moysklad\MoyskladObjectType as ObjectType;
use common\components\moysklad\MoyskladDocumentType as DocumentType;
use common\components\moysklad\MoyskladDocumentValidation;
use common\components\moysklad\parser\xml\PaymentOrderXml;
use common\models\cash\CashOrderFlows;
use common\components\moysklad\helper\MoyskladImportStatistics;
use common\components\moysklad\helper\MoyskladValidationErrors;
use yii\db\Transaction;

trait PaymentOrdersTrait {

    public function insertPaymentOrders()
    {
        /** @var MoyskladDocument $xmlPayment */
        /** @var CashOrderFlows $kubPayment */
        /** @var MoyskladImportStatistics $stat */
        /** @var MoyskladValidationErrors $validation */

        $stat = $this->stat;
        $validation = $this->validation;
        $model = $this->getPaymentOrderModelTemplate();
        $newXmlPayments = $this->getNewPaymentOrders();

        // Transaction init ////////////////////////////////////
        $transaction = new Transaction(['db' => Yii::$app->db]);
        $row = 0;
        ////////////////////////////////////////////////////////

        try {

            foreach ($newXmlPayments as $xmlPayment) {

                // Transaction begin //////////////////////////////////////////////
                if ($row === 0) {
                    $transaction->begin(self::TRANSACTION_ISOLATION_LEVEL);
                    $row++;
                }
                ///////////////////////////////////////////////////////////////////

                $data = $xmlPayment->getData();

                if ($kubID = self::getExistsPaymentOrderId($data)) {
                    if (!$xmlPayment->getForeignKey()) {
                        $xmlPayment->setForeignKey($kubID);
                    }

                    $stat->incrementDouble($xmlPayment->document_type_id);

                    continue;
                }

                $kubPayment = PaymentOrderXml::convert2model($model, $data);

                if ($kubPayment->validate()) {

                    $isSaved = $kubPayment->save(false);

                    if ($isSaved) {
                        $xmlPayment->setForeignKey($kubPayment->id);
                        $stat->incrementUploaded($xmlPayment->document_type_id);
                    } else {
                        $xmlPayment->setNoForeign();
                        $validation->addDocumentError($xmlPayment, $kubPayment);
                    }

                    if ($isSaved) {
                        $this->linkPayment2Invoice($kubPayment, $xmlPayment);
                    }

                } else {

                    $xmlPayment->setNoForeign();
                    $validation->addDocumentError($xmlPayment, $kubPayment);
                }

                // Transaction end //////////////////////////////////////////////////////
                if ($transaction->isActive && ($row + 1 >= self::TRANSACTION_MAX_ROWS)) {
                    $transaction->commit();
                    $row = 0;
                }
                /////////////////////////////////////////////////////////////////////////
            }

            // Transaction end /////////////////////////////////////////////////
            if ($transaction->isActive && $row > 0) {
                $transaction->commit();
            }
            ///////////////////////////////////////////////////////////////////

        } catch (\Throwable $e) {

            if ($transaction->isActive)
                $transaction->rollBack();

            throw new Exception($e);
        }
    }

    private function linkPayment2Invoice(CashOrderFlows $kubPayment, MoyskladDocument $xmlPayment)
    {
        $possibleBaseDoc = $this->getPossibleBaseDoc($xmlPayment);

        if ($possibleBaseDoc) {
            $relation = new CashOrderFlowToInvoice([
                'flow_id' => $kubPayment->id,
                'invoice_id' => $possibleBaseDoc->invoice_id,
                'amount' => round(100 * $xmlPayment->document_amount)
            ]);
            if ($relation->save()) {
                $xmlPayment->updateAttributes(['invoice_id' => $possibleBaseDoc->invoice_id]);
                return true;
            }
        }

        return false;
    }

    private function getNewPaymentOrders()
    {
        return MoyskladDocument::find()
            ->byCompany($this->_company->id)
            ->byDocumentType([DocumentType::IN_PAYMENT_ORDER, DocumentType::OUT_PAYMENT_ORDER])
            ->byNew()
            ->all();
    }
}