<?php
namespace common\components\moysklad\converter\kub;

use common\components\helpers\ArrayHelper;
use common\components\moysklad\parser\xml\InvoiceFactureXml;
use common\models\document\InvoiceFacture;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use frontend\modules\documents\components\InvoiceHelper;
use Yii;
use yii\base\Exception;
use yii\db\Connection;
use common\components\moysklad\MoyskladDocument;
use common\components\moysklad\MoyskladDocumentType as DocumentType;
use common\components\moysklad\parser\xml\InvoiceXml;
use common\models\document\Invoice;
use common\components\moysklad\helper\MoyskladImportStatistics;
use common\components\moysklad\helper\MoyskladValidationErrors;
use yii\db\Transaction;

trait InvoiceFacturesTrait {

    public static $MESSAGE_INVOICES_NOT_FOUND = 'Счета для СФ № %s не найдены';

    public function insertInvoiceFactures()
    {
        /** @var MoyskladDocument $xmlFacture */
        /** @var InvoiceFacture $kubFacture */
        /** @var MoyskladImportStatistics $stat */
        /** @var MoyskladValidationErrors $validation */

        $stat = $this->stat;
        $validation = $this->validation;
        $modelTemplate = $this->getInvoiceFactureModelTemplate();
        $newXmlFactures = $this->getNewFactures();

        // Transaction init ////////////////////////////////////
        $transaction = new Transaction(['db' => Yii::$app->db]);
        $row = 0;
        ////////////////////////////////////////////////////////

        try {
            foreach ($newXmlFactures as $xmlFacture) {

                // Transaction begin //////////////////////////////////////////////
                if ($row === 0) {
                    $transaction->begin(self::TRANSACTION_ISOLATION_LEVEL);
                    $row++;
                }
                ///////////////////////////////////////////////////////////////////

                $data = $xmlFacture->getData();

                if ($kubID = self::getExistsInvoiceFactureId($data)) {
                    if (!$xmlFacture->getForeignKey()) {
                        $xmlFacture->setForeignKey($kubID);
                    }

                    $stat->incrementDouble($xmlFacture->document_type_id);

                    continue;
                }

                $kubInvoices = array_filter([$this->getBaseDocInvoice($xmlFacture)]);

                if ($kubInvoices) {
                    $kubFacture = InvoiceFactureXml::convert2dependentModel($modelTemplate, $kubInvoices, $data);
                    $kubFacture->invoice_id = $kubInvoices[0]->id;
                } else {
                    $kubFacture = $modelTemplate;
                    $kubFacture->clearErrors();
                    $kubFacture->addError('invoice_id', sprintf(self::$MESSAGE_INVOICES_NOT_FOUND, $data['document_number']));
                    $validation->addDocumentError($xmlFacture, $kubFacture);
                    continue;
                }

                if ($kubFacture->validate()) {

                    $isSaved = LogHelper::save($kubFacture, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE, function($kubFacture) use ($xmlFacture, $validation) {

                        if ($kubFacture->ownOrders) {

                            if ($kubFacture->save(false)) {
                                // LogHelper::log($modelTemplate, $logEntityType, $logEvent)
                                foreach ($kubFacture->ownOrders as $order) {
                                    $order->invoice_facture_id = $kubFacture->primaryKey;
                                    $order->quantity = number_format($order->quantity, 10, '.', '');
                                    if (!$order->save()) {
                                        $validation->addDocumentError($xmlFacture, $kubFacture, $order);

                                        return false;
                                    }
                                }

                                return true;
                            }
                        }

                        return false;
                    });

                    if ($isSaved) {
                        $xmlFacture->setForeignKey($kubFacture->id);
                        $stat->incrementUploaded($xmlFacture->document_type_id);
                        foreach ($kubInvoices as $kubInvoice) {
                            $kubInvoice->updateAttributes([
                                'has_invoice_facture' => true,
                                'can_add_invoice_facture' => false,
                            ]);
                        }
                    } else {
                        $xmlFacture->setNoForeign();
                        $validation->addDocumentError($xmlFacture, $kubFacture);
                    }

                } else {

                    $xmlFacture->setNoForeign();
                    $validation->addDocumentError($xmlFacture, $kubFacture);
                }

                // Transaction end //////////////////////////////////////////////////////
                if ($transaction->isActive && ($row + 1 >= self::TRANSACTION_MAX_ROWS)) {
                    $transaction->commit();
                    $row = 0;
                }
                /////////////////////////////////////////////////////////////////////////
            }

            // Transaction end /////////////////////////////////////////////////
            if ($transaction->isActive && $row > 0) {
                $transaction->commit();
            }
            ///////////////////////////////////////////////////////////////////

        } catch (\Throwable $e) {

            if ($transaction->isActive)
                $transaction->rollBack();

            throw new Exception($e);
        }
    }

    private function getNewFactures()
    {
        return MoyskladDocument::find()
            ->byCompany($this->_company->id)
            ->byDocumentType([DocumentType::IN_INVOICE_FACTURE, DocumentType::OUT_INVOICE_FACTURE])
            ->byNew()
            ->all();
    }

    /**
     * @param MoyskladDocument $xmlFacture
     * @return Invoice|null
     */
    private function getBaseDocInvoice(MoyskladDocument $xmlFacture)
    {
        $possibleBaseDoc = $this->getPossibleBaseDoc($xmlFacture);

        if ($possibleBaseDoc) {
            return Invoice::findOne([
                'id' => $possibleBaseDoc->invoice_id,
                'company_id' => $this->_company->id
            ]);
        }

        return null;
    }

    // Not used (get base documents by table-part in xml object)
    //public function getBaseDocInvoices($data)
    //{
    //    if (empty($data['base_documents']))
    //        return [];
    //
    //    $ret = [];
    //    foreach ($data['base_documents'] as $baseDocData) {
    //        $d = [
    //            'document_type' => $data['document_type'],
    //            'document_date' => $baseDocData['document_date'],
    //            'document_number' => $baseDocData['document_number'],
    //            'contractor_id' => $data['kub_contractor_id']
    //        ];
    //        $kubInvoice = ($kubInvoiceID = self::getExistsInvoiceId($d))
    //            ? Invoice::findOne(['id' => $kubInvoiceID])
    //            : null;
    //
    //        if ($kubInvoice) {
    //            $ret[] = $kubInvoice;
    //        } else {
    //            return []; // return all or nothing
    //        }
    //    }
    //
    //    return $ret;
    //}
}