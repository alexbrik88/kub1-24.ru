<?php
namespace common\components\moysklad\converter\kub;

use common\components\moysklad\helper\MoyskladImportStatistics;
use common\components\moysklad\helper\MoyskladValidationErrors;
use common\components\moysklad\parser\xml\ActXml;
use common\components\moysklad\parser\xml\PackingListXml;
use common\models\document\Act;
use common\models\document\PackingList;
use common\models\product\Product;
use Yii;
use yii\base\Exception;
use yii\db\Connection;
use common\components\moysklad\parser\xml\InvoiceXml;
use common\models\document\Invoice;
use common\components\helpers\ArrayHelper;
use common\components\moysklad\parser\xml\UpdXml;
use common\models\Contractor;
use common\models\document\Upd;
use frontend\models\Documents;
use frontend\models\log\LogEntityType;
use frontend\models\log\LogEvent;
use frontend\models\log\LogHelper;
use common\components\moysklad\MoyskladDocument;
use common\components\moysklad\MoyskladDocumentType as DocumentType;
use frontend\modules\documents\components\InvoiceHelper;
use common\components\moysklad\converter\MoyskladConverter;
use yii\db\Transaction;

trait UpdsTrait {

    public static $MESSAGE_INVOICE_NOT_FOUND = 'Счет для УПД № %s не найден';

    public function insertInvoicesGeneratedFromUpds()
    {
        /** @var MoyskladDocument $xmlInvoiceFromUpd */
        /** @var Invoice $kubInvoice */
        /** @var MoyskladImportStatistics $stat */
        /** @var MoyskladValidationErrors $validation */

        $stat = $this->stat;
        $validation = $this->validation;
        $model = $this->getInvoiceModelTemplate();
        $newXmlInvoices = $this->getNewUpds();

        // Transaction init ////////////////////////////////////
        $transaction = new Transaction(['db' => Yii::$app->db]);
        $row = 0;
        ////////////////////////////////////////////////////////

        try {

            foreach ($newXmlInvoices as $xmlInvoiceFromUpd) {

                // Transaction begin //////////////////////////////////////////////
                if ($row === 0) {
                    $transaction->begin(self::TRANSACTION_ISOLATION_LEVEL);
                    $row++;
                }
                ///////////////////////////////////////////////////////////////////

                $isOut = ($xmlInvoiceFromUpd->document_type == Documents::IO_TYPE_OUT);
                $xmlInvoiceFromUpd->document_type_id = ($isOut) ? DocumentType::OUT_INVOICE : DocumentType::IN_INVOICE;
                $data = $xmlInvoiceFromUpd->getData();

                if ($this->getNoUpdInvoice($xmlInvoiceFromUpd)) {
                    // invoice present in file
                    continue;
                }

                if ($kubID = self::getExistsInvoiceId($data)) {
                    if (!$xmlInvoiceFromUpd->getKubInvoiceId()) {
                        $xmlInvoiceFromUpd->setKubInvoiceId($kubID);
                    }
                    // because it's pseudo-invoice
                    //$stat->incrementDouble($xmlInvoiceFromUpd->document_type_id);
                    continue;
                }

                $kubInvoice = InvoiceXml::convert2model($model, $data);

                if ($kubInvoice->validate()) {

                    $isSaved = InvoiceHelper::save($kubInvoice, false, true);
                    if ($isSaved) {
                        $xmlInvoiceFromUpd->setKubInvoiceId($kubInvoice->id);
                        // because it's pseudo-invoice
                        //$xmlInvoiceFromUpd->setForeignKey($kubInvoice->id);
                        //$stat->incrementUploaded($xmlInvoiceFromUpd->document_type_id);
                    } else {
                        // because it's pseudo-invoice
                        //$xmlInvoiceFromUpd->setNoForeign();
                        //$validation->addDocumentError($xmlInvoiceFromUpd, $kubInvoice);
                    }

                } else {
                    // because it's pseudo-invoice
                    //$xmlInvoiceFromUpd->setNoForeign();
                    //$validation->addDocumentError($xmlInvoiceFromUpd, $kubInvoice);
                }

                // update objects by documents (io_type)
                self::accumulateAfterImportData($kubInvoice);

                // Transaction end //////////////////////////////////////////////////////
                if ($transaction->isActive && ($row + 1 >= self::TRANSACTION_MAX_ROWS)) {
                    $transaction->commit();
                    $row = 0;
                }
                /////////////////////////////////////////////////////////////////////////
            }

            // Transaction end /////////////////////////////////////////////////
            if ($transaction->isActive && $row > 0) {
                $transaction->commit();
            }
            ///////////////////////////////////////////////////////////////////

        } catch (\Throwable $e) {

            if ($transaction->isActive)
                $transaction->rollBack();

            throw new Exception($e);
        }
    }

    public function insertUpds()
    {
        /** @var MoyskladDocument $xmlUpd */
        /** @var Upd $kubUpd */
        /** @var MoyskladImportStatistics $stat */
        /** @var MoyskladValidationErrors $validation */

        $stat = $this->stat;
        $validation = $this->validation;
        $templateUpd = $this->getUpdModelTemplate();
        $templateInvoice = $this->getInvoiceModelTemplate();
        $templateAct = $this->getActModelTemplate();
        $templatePackingList = $this->getPackingListModelTemplate();
        $newXmlUpds = $this->getNewUpds();

        // Transaction init ////////////////////////////////////
        $transaction = new Transaction(['db' => Yii::$app->db]);
        $row = 0;
        ////////////////////////////////////////////////////////

        try {

            foreach ($newXmlUpds as $xmlUpd) {

                // Transaction begin //////////////////////////////////////////////
                if ($row === 0) {
                    $transaction->begin(self::TRANSACTION_ISOLATION_LEVEL);
                    $row++;
                }
                ///////////////////////////////////////////////////////////////////

                $data = $xmlUpd->getData();

                // exists in Kub
                if ($kubID = self::getExistsUpdId($data)) {
                    if (!$xmlUpd->getForeignKey()) {
                        $xmlUpd->setForeignKey($kubID);
                    }
                    $stat->incrementDouble($xmlUpd->document_type_id);
                    continue;
                }

                $kubInvoice = ($kubInvoiceID = self::getExistsInvoiceId($data))
                    ? Invoice::findOne(['id' => $kubInvoiceID])
                    : null;

                if ($kubInvoice) {
                    
                    if ($this->_import->asUpd()) {
                        $kubUpd = UpdXml::convert2dependentModel($templateUpd, $kubInvoice, $data);
                        $kubPackinglist = null;
                        $kubAct = null;
                    } else {
                        $kubPackinglist = PackingListXml::convert2dependentModel($templatePackingList, $kubInvoice, $data);
                        $kubAct = ActXml::convert2dependentModel($templateAct, $kubInvoice, $data);
                        $kubUpd = null;
                    }

                } else {
                    // if invoice not found - validate it's as invoice to get parents errors
                    $kubUpd = InvoiceXml::convert2model($templateInvoice, $data);
                    $kubUpd->validate();
                    $validation->addDocumentError($xmlUpd, $kubUpd);
                    continue;
                }

                if ($kubUpd) {
                    $isSaved =
                        $kubUpd->validate() &&
                        $this->_saveAsUpd($xmlUpd, $kubUpd);
                }
                elseif ($kubAct && $kubPackinglist) {
                    $isSaved =
                        $kubAct->validate() &&
                        $kubPackinglist->validate() &&
                        $this->_saveAsAct($xmlUpd, $kubAct) &&
                        $this->_saveAsPackingList($xmlUpd, $kubPackinglist);
                }
                elseif ($kubAct) {
                    $isSaved =
                        $kubAct->validate() &&
                        $this->_saveAsAct($xmlUpd, $kubAct);
                }
                elseif ($kubPackinglist) {

                    $isSaved =
                        $kubPackinglist->validate() &&
                        $this->_saveAsPackingList($xmlUpd, $kubPackinglist);
                }
                else {
                    $isSaved = false;
                }

                if ($isSaved) {
                    if ($kubUpd) {
                        $xmlUpd->setForeignKey($kubUpd->id);
                    } else {
                        $xmlUpd->setActAndPackingListForeignKeys($kubAct ? $kubAct->id : null, $kubPackinglist ? $kubPackinglist->id : null);
                    }
                    $stat->incrementUploaded($xmlUpd->document_type_id);
                    $kubInvoice->updateAttributes([
                        'has_upd' => !!$kubUpd,
                        'has_act' => !!$kubAct,
                        'has_packing_list' => !!$kubPackinglist,
                        'can_add_upd' => false,
                        'can_add_act' => false,
                        'can_add_packing_list' => false,
                        'can_add_invoice_facture' => $kubInvoice->hasNds,
                        'can_add_waybill' => $kubInvoice->production_type == Product::PRODUCTION_TYPE_GOODS || strpos($kubInvoice->production_type, ',') !== false
                    ]);
                } else {
                    $xmlUpd->setNoForeign();
                    $validation->addDocumentError($xmlUpd, $kubUpd ?: ($kubPackinglist ?: $kubAct));
                }


                // Transaction end //////////////////////////////////////////////////////
                if ($transaction->isActive && ($row + 1 >= self::TRANSACTION_MAX_ROWS)) {
                    $transaction->commit();
                    $row = 0;
                }
                /////////////////////////////////////////////////////////////////////////
            }

            // Transaction end /////////////////////////////////////////////////
            if ($transaction->isActive && $row > 0) {
                $transaction->commit();
            }
            ///////////////////////////////////////////////////////////////////

        } catch (\Throwable $e) {

            if ($transaction->isActive)
                $transaction->rollBack();

            throw new Exception($e);
        }
    }

    private function getNewUpds()
    {
        return MoyskladDocument::find()
            ->byCompany($this->_company->id)
            ->byDocumentType([DocumentType::IN_UPD, DocumentType::OUT_UPD])
            ->byNew()
            ->all();
    }

    /**
     * @param $xmlUpd
     * @param $kubUpd
     * @return bool
     * @throws \Exception
     */
    private function _saveAsUpd($xmlUpd, Upd $kubUpd)
    {
        $validation = $this->validation;

        $isSaved = LogHelper::save($kubUpd, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE, function ($kubUpd) use ($xmlUpd, $validation) {

            /** @var Upd $kubUpd */
            if ($kubUpd->orderUpds) {
                if ($kubUpd->save(false)) {
                    foreach ($kubUpd->orderUpds as $orderUpd) {
                        $orderUpd->upd_id = $kubUpd->primaryKey;
                        $orderUpd->quantity = number_format($orderUpd->quantity, 10, '.', '');
                        if (!$orderUpd->save()) {
                            $validation->addDocumentError($xmlUpd, $kubUpd, $orderUpd);

                            return false;
                        }
                    }

                    return true;
                }
            }

            return false;
        });

        return $isSaved;
    }

    private function _saveAsAct($xmlUpd, Act $kubAct)
    {
        $validation = $this->validation;

        $isSaved = LogHelper::save($kubAct, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE, function ($kubAct) use ($xmlUpd, $validation) {

            /** @var Act $kubAct */
            if ($kubAct->orderActs) {
                if ($kubAct->save(false)) {
                    foreach ($kubAct->orderActs as $orderAct) {
                        $orderAct->act_id = $kubAct->primaryKey;
                        $orderAct->quantity = number_format($orderAct->quantity, 10, '.', '');
                        if (!$orderAct->save()) {
                            $validation->addDocumentError($xmlUpd, $kubAct, $orderAct);

                            return false;
                        }
                    }

                    return true;
                }
            }

            return false;
        });

        return $isSaved;
    }

    private function _saveAsPackingList($xmlUpd, PackingList $kubPackingList)
    {
        $validation = $this->validation;

        $isSaved = LogHelper::save($kubPackingList, LogEntityType::TYPE_DOCUMENT, LogEvent::LOG_EVENT_CREATE, function ($kubPackingList) use ($xmlUpd, $validation) {
            /** @var PackingList $kubPackingList */
            if ($kubPackingList->orderPackingLists) {
                if ($kubPackingList->save(false)) {
                    foreach ($kubPackingList->orderPackingLists as $orderPl) {
                        $orderPl->packing_list_id = $kubPackingList->primaryKey;
                        $orderPl->quantity = number_format($orderPl->quantity, 10, '.', '');
                        if (!$orderPl->save()) {
                            $validation->addDocumentError($xmlUpd, $kubPackingList, $orderPl);

                            return false;
                        }
                    }

                    return true;
                }
            }

            return false;
        });

        return $isSaved;
    }
}