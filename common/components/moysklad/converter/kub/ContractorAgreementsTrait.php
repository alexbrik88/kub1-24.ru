<?php
namespace common\components\moysklad\converter\kub;

use common\components\moysklad\helper\MoyskladImportStatistics;
use common\components\moysklad\helper\MoyskladValidationErrors;
use Yii;
use yii\db\Connection;
use common\models\Agreement;
use common\components\moysklad\MoyskladObject;
use common\components\moysklad\MoyskladObjectType as ObjectType;
use common\components\moysklad\parser\xml\AgreementXml;
use common\components\moysklad\parser\xml\ContractorXml;
use common\models\Contractor;

trait ContractorAgreementsTrait
{
    public function insertContractorAgreements(Agreement $agreement, array $xmlAgreements)
    {
        /** @var MoyskladObject $agreementXml */
        /** @var Agreement $agreementKub */
        /** @var MoyskladImportStatistics $stat */
        /** @var MoyskladValidationErrors $validation */

        $stat = $this->stat;
        $validation = $this->validation;
        $uploaded = 0;
        foreach ($xmlAgreements as $agreementXml) {
            $data = $agreementXml->getData();
            $agreementKub = AgreementXml::convert2model($agreement, $data);
            if ($agreementKub->validate()) {
                $agreementKub->save(false);
                $agreementXml->setForeignKey($agreementKub->id);
                $uploaded++;
            } else {
                $agreementXml->setNoForeign();
                $validation->addObjectError($agreementXml, $agreementKub);
            }
        }

        return $uploaded;
    }

    private function getNewContractorAgreements($data)
    {
        return MoyskladObject::find()
            ->byCompany($this->_company->id)
            ->byObjectType(ObjectType::AGREEMENT)
            ->byContractor([
                'name' => $data['name'],
                'inn' => $data['ITN'],
                'kpp' => $data['PPC']
            ])
            ->byNew()
            ->all();
    }
}