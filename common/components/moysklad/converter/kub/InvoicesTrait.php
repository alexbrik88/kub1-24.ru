<?php
namespace common\components\moysklad\converter\kub;

use frontend\models\log\LogHelper;
use frontend\modules\documents\components\InvoiceHelper;
use Yii;
use yii\base\Exception;
use yii\db\Connection;
use common\components\moysklad\MoyskladDocument;
use common\components\moysklad\MoyskladDocumentType as DocumentType;
use common\components\moysklad\parser\xml\InvoiceXml;
use common\models\document\Invoice;
use common\components\moysklad\helper\MoyskladImportStatistics;
use common\components\moysklad\helper\MoyskladValidationErrors;
use yii\db\Transaction;

trait InvoicesTrait {

    public function insertInvoices()
    {
        /** @var MoyskladImportStatistics $stat */
        /** @var MoyskladValidationErrors $validation */
        /** @var MoyskladDocument $xmlInvoice */
        /** @var Invoice $kubInvoice */

        $stat = $this->stat;
        $validation = $this->validation;
        $model = $this->getInvoiceModelTemplate();
        $newXmlInvoices = $this->getNewInvoices();

        // Transaction init ////////////////////////////////////
        $transaction = new Transaction(['db' => Yii::$app->db]);
        $row = 0;
        ////////////////////////////////////////////////////////

        try {

            foreach ($newXmlInvoices as $xmlInvoice) {

                // Transaction begin //////////////////////////////////////////////
                if ($row === 0) {
                    $transaction->begin(self::TRANSACTION_ISOLATION_LEVEL);
                    $row++;
                }
                ///////////////////////////////////////////////////////////////////

                $data = $xmlInvoice->getData();

                // exists in Kub
                if ($kubID = self::getExistsInvoiceId($data)) {
                    if (!$xmlInvoice->getForeignKey()) {
                        $xmlInvoice->setForeignKey($kubID);
                    }
                    $stat->incrementDouble($xmlInvoice->document_type_id);
                    continue;
                }

                $kubInvoice = InvoiceXml::convert2model($model, $data);

                if ($kubInvoice->validate()) {
                    $isSaved = InvoiceHelper::save($kubInvoice, false, true);
                    if ($isSaved) {
                        $xmlInvoice->setForeignKey($kubInvoice->id);
                        $stat->incrementUploaded($xmlInvoice->document_type_id);
                    } else {
                        $xmlInvoice->setNoForeign();
                        $validation->addDocumentError($xmlInvoice, $kubInvoice);
                    }
                } else {
                    $xmlInvoice->setNoForeign();
                    $validation->addDocumentError($xmlInvoice, $kubInvoice);
                }

                // update objects by documents (io_type)
                self::accumulateAfterImportData($kubInvoice);

                // Transaction end //////////////////////////////////////////////////////
                if ($transaction->isActive && ($row + 1 >= self::TRANSACTION_MAX_ROWS)) {
                    $transaction->commit();
                    $row = 0;
                }
                /////////////////////////////////////////////////////////////////////////
            }

            // Transaction end /////////////////////////////////////////////////
            if ($transaction->isActive && $row > 0) {
                $transaction->commit();
            }
            ///////////////////////////////////////////////////////////////////

        } catch (\Throwable $e) {

            if ($transaction->isActive)
                $transaction->rollBack();

            throw new Exception($e);
        }
    }

    private function getNewInvoices()
    {
        return MoyskladDocument::find()
            ->byCompany($this->_company->id)
            ->byDocumentType([DocumentType::IN_INVOICE, DocumentType::OUT_INVOICE])
            ->byNew()
            ->all();
    }
}