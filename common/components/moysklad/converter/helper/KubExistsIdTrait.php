<?php
namespace common\components\moysklad\converter\helper;

use common\components\moysklad\parser\xml\AbstractXmlObject;
use common\models\cash\CashOrderFlows;
use common\models\Contractor;
use common\models\document\Invoice;
use common\models\document\InvoiceFacture;
use common\models\document\Upd;
use common\models\product\Product;
use common\models\product\ProductGroup;

trait KubExistsIdTrait {

    /**
     * @param $data
     * @return false|string|null
     */
    public function getExistsProductGroupId($data)
    {
        return ProductGroup::find()->where([
            'company_id' => $this->_company->id,
            'title' => $data['title']
        ])
        ->select('id')
        ->scalar();
    }

    /**
     * @param $data
     * @return false|string|null
     */
    public function getExistsProductId($data)
    {
        return Product::find()->where([
            'company_id' => $this->_company->id,
        ])
        ->andWhere(['or',
            ['and', ['code' => $data['code']], ['not', ['code' => [null, '']]]],
            ['and', ['title' => $data['title']], ['code' => [null, '']]]
        ])
        ->select('id')
        ->scalar();
    }

    /**
     * @param $data
     * @return false|string|null
     */
    public function getExistsContractorId($data)
    {
        $query = Contractor::find()
            ->andWhere(['company_id' => $this->_company->id])
            ->andWhere(['is_deleted' => false]);

        if (trim($data['ITN']))  {
            $query
                ->andWhere(['ITN' => $data['ITN']])
                ->andWhere(['PPC' => $data['PPC']]);
        } else {

            list ($normalizedTypeId, $normalizedName) = AbstractXmlObject::_getContractorTypeAndName($data['name']);

            $query
                ->andWhere(['or',
                    ['name' => $normalizedName],
                    ['name' => '"' . $normalizedName . '"'],
                ]);
        }

        return $query->select('id')->scalar();
    }

    /**
     * @param $data
     * @return false|string|null
     */
    public function getExistsInvoiceId($data)
    {
        return Invoice::find()->where([
            'company_id' => $this->_company->id,
            'type' => $data['document_type'],
            'document_date' => $data['document_date'],
            'document_number' => $data['document_number'],
        ])
        ->andWhere([
            'contractor_id' => $data['kub_contractor_id']
        ])
        ->select('id')
        ->scalar();
    }

    /**
     * @param $data
     * @return false|string|null
     */
    public function getExistsUpdId($data)
    {
        return Upd::find()->where([
            'company_id' => $this->_company->id,
            'type' => $data['document_type'],
            'document_date' => $data['document_date'],
            'document_number' => $data['document_number'],
        ])
        ->andWhere([
            'contractor_id' => $data['kub_contractor_id']
        ])
        ->select('id')
        ->scalar();
    }

    /**
     * @param $data
     * @return false|string|null
     */
    public function getExistsInvoiceFactureId($data)
    {
        return InvoiceFacture::find()
            ->joinWith(Invoice::tableName())
            ->where([
                InvoiceFacture::tableName().'.type' => $data['document_type'],
                InvoiceFacture::tableName().'.document_date' => $data['document_date'],
                InvoiceFacture::tableName().'.document_number' => $data['document_number'],
            ])
            ->andWhere([
                Invoice::tableName().'.company_id' => $this->_company->id
            ])
            ->andWhere([
                Invoice::tableName().'.contractor_id' => $data['kub_contractor_id']
            ])
            ->select(InvoiceFacture::tableName().'.id')
            ->scalar();
    }

    /**
     * @param $data
     * @return false|string|null
     */
    public function getExistsPaymentOrderId($data)
    {
        return CashOrderFlows::find()->where([
            'company_id' => $this->_company->id,
            'flow_type' => $data['flow_type'],
            'date' => $data['document_date'],
            'number' => $data['document_number'],
        ])
        ->andWhere([
            'contractor_id' => $data['kub_contractor_id']
        ])
        ->select('id')
        ->scalar();
    }
}