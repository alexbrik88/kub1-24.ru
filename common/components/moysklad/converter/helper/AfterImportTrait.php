<?php
namespace common\components\moysklad\converter\helper;

use common\models\document\Invoice;
use Yii;
use common\models\Contractor;
use frontend\models\Documents;
use yii\db\Transaction;

trait AfterImportTrait {

    public function updateContractorsTypes()
    {
        if (empty(self::$UPDATE_BY_DOCS[self::KUB_CONTRACTOR_TYPE]))
            return;

        $transaction = new Transaction(['db' => Yii::$app->db]);
        $transaction->begin(self::TRANSACTION_ISOLATION_LEVEL);

        foreach (self::$UPDATE_BY_DOCS[self::KUB_CONTRACTOR_TYPE] as $kubContractorId => $typesArray)
        {
            $sql = [];
            if (in_array(Documents::IO_TYPE_OUT, $typesArray)) {
                $sql[] = '`type` = 2';
                $sql[] = '`is_customer` = 1';
            }
            if (in_array(Documents::IO_TYPE_IN, $typesArray)) {
                $sql[] = '`type` = 1';
                $sql[] = '`is_seller` = 1';
            }

            if ($sql) {
                Yii::$app->db->createCommand("UPDATE `contractor` SET " . implode(',', $sql) . " WHERE `id` = " . (int)$kubContractorId . " AND `company_id`=" . $this->_company->id)->execute();
            }
        }

        $transaction->commit();
    }

    public function updateProductsPrices()
    {
        if (empty(self::$UPDATE_BY_DOCS[self::KUB_PRODUCT_PRICE_FOR_SELL]) && empty(self::$UPDATE_BY_DOCS[self::KUB_PRODUCT_PRICE_FOR_BUY]))
            return;

        $transaction = new Transaction(['db' => Yii::$app->db]);
        $transaction->begin(self::TRANSACTION_ISOLATION_LEVEL);

        foreach (self::$UPDATE_BY_DOCS[self::KUB_PRODUCT_PRICE_FOR_BUY] as $kubProductId => $pricesArray)
        {
            if (($lastPrice = array_pop($pricesArray)) > 0) {
                Yii::$app->db->createCommand("UPDATE `product` SET `price_for_buy_with_nds` = " . round($lastPrice, 2) . " WHERE `id` = " . (int)$kubProductId . " AND `company_id`=" . $this->_company->id)->execute();
            }
        }

        foreach (self::$UPDATE_BY_DOCS[self::KUB_PRODUCT_PRICE_FOR_SELL] as $kubProductId => $pricesArray)
        {
            if (($lastPrice = array_pop($pricesArray)) > 0) {
                Yii::$app->db->createCommand("UPDATE `product` SET `price_for_sell_with_nds` = " . round($lastPrice, 2) . " WHERE `id` = " . (int)$kubProductId . " AND `company_id`=" . $this->_company->id)->execute();
            }
        }

        $transaction->commit();
    }

    public static function accumulateAfterImportData(Invoice $kubInvoice)
    {
        if ($kubInvoice->contractor_id && $kubInvoice->type) {
            if (!isset(self::$UPDATE_BY_DOCS[self::KUB_CONTRACTOR_TYPE][$kubInvoice->contractor_id])) {
                self::$UPDATE_BY_DOCS[self::KUB_CONTRACTOR_TYPE][$kubInvoice->contractor_id] = [];
            }
            self::$UPDATE_BY_DOCS[self::KUB_CONTRACTOR_TYPE][$kubInvoice->contractor_id][] = $kubInvoice->type;
        }

        if ($kubInvoice->orders && $kubInvoice->type) {
            foreach ($kubInvoice->orders as $order) {
                if ($kubInvoice->type == Documents::IO_TYPE_OUT) {
                    if (!isset(self::$UPDATE_BY_DOCS[self::KUB_PRODUCT_PRICE_FOR_SELL][$order->product_id])) {
                        self::$UPDATE_BY_DOCS[self::KUB_PRODUCT_PRICE_FOR_SELL][$order->product_id] = [];
                    }
                    self::$UPDATE_BY_DOCS[self::KUB_PRODUCT_PRICE_FOR_SELL][$order->product_id][] = $order->selling_price_with_vat;
                } elseif ($kubInvoice->type == Documents::IO_TYPE_IN) {
                    if (!isset(self::$UPDATE_BY_DOCS[self::KUB_PRODUCT_PRICE_FOR_BUY][$order->product_id])) {
                        self::$UPDATE_BY_DOCS[self::KUB_PRODUCT_PRICE_FOR_BUY][$order->product_id] = [];
                    }
                    self::$UPDATE_BY_DOCS[self::KUB_PRODUCT_PRICE_FOR_BUY][$order->product_id][] = $order->purchase_price_with_vat;
                }
            }
        }
    }
}