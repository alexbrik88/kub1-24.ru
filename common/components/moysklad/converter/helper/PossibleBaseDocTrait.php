<?php
namespace common\components\moysklad\converter\helper;

use common\components\moysklad\MoyskladDocument;
use common\components\moysklad\MoyskladDocumentType as DocumentType;
use frontend\models\Documents;

trait PossibleBaseDocTrait {

    /**
     * @param MoyskladDocument $xmlDocument
     * @return MoyskladDocument|null
     */
    public function getPossibleBaseDoc(MoyskladDocument $xmlDocument)
    {
        $isOut = ($xmlDocument->document_type == Documents::IO_TYPE_OUT);

        /** @var MoyskladDocument $possibleBaseDoc */
        $possibleBaseDoc = MoyskladDocument::find()
            ->byCompany($this->_company->id)
            ->byContractor([
                'inn' => $xmlDocument->contractor_inn,
                'kpp' => $xmlDocument->contractor_kpp,
                'name' => $xmlDocument->contractor_name,
            ])
            ->byDocumentAmount([
                'type_id' => ($isOut)
                    ? [DocumentType::OUT_INVOICE, DocumentType::OUT_UPD]
                    : [DocumentType::IN_INVOICE, DocumentType::IN_UPD],
                'type' => $xmlDocument->document_type,
                'date' => $xmlDocument->document_date,
                'amount' => $xmlDocument->document_amount,
            ])
            ->andWhere(['not', ['invoice_id' => null]])
            ->byValidated()
            ->one();

        return $possibleBaseDoc;
    }

    /**
     * @param MoyskladDocument $xmlUpd
     * @return MoyskladDocument|null
     */
    public function getNoUpdInvoice(MoyskladDocument $xmlUpd)
    {
        $isOut = ($xmlUpd->document_type == Documents::IO_TYPE_OUT);

        /** @var MoyskladDocument $possibleBaseDoc */
        $possibleBaseDoc = MoyskladDocument::find()
            ->byCompany($this->_company->id)
            ->byContractor([
                'inn' => $xmlUpd->contractor_inn,
                'kpp' => $xmlUpd->contractor_kpp,
                'name' => $xmlUpd->contractor_name,
            ])
            ->byDocument([
                'type_id' => ($isOut) ? [DocumentType::OUT_INVOICE] : [DocumentType::IN_INVOICE],
                'type' => $xmlUpd->document_type,
                'date' => $xmlUpd->document_date,
                'number' => $xmlUpd->document_number,
            ])
            ->one();

        return $possibleBaseDoc;
    }
}
