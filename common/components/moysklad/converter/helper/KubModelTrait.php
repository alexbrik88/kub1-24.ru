<?php
namespace common\components\moysklad\converter\helper;

use common\models\cash\Cashbox;
use common\models\cash\CashOrderFlows;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\InvoiceFacture;
use common\models\document\PackingList;
use common\models\document\Upd;

trait KubModelTrait {

    /**
     * @return Invoice
     */
    public function getInvoiceModelTemplate()
    {
        return new Invoice([
            'id' => null,
            // fk company + employee + store
            'company_id' => $this->_company->id,
            'store_id' => $this->_import->store_id,
            'invoice_status_author_id' => $this->_employee->id,
            'document_author_id' => $this->_employee->id,
            'company_checking_accountant_id' => $this->_company->mainCheckingAccountant->id,
            // company data
            'company_inn' => $this->_company->inn,
            'company_kpp' => $this->_company->kpp,
            'company_egrip' => $this->_company->egrip,
            'company_okpo' => $this->_company->okpo,
            'company_name_full' => $this->_company->getTitle(),
            'company_name_short' => $this->_company->getTitle(true, true),
            'company_address_legal_full' => $this->_company->getAddressLegalFull(),
            'company_phone' => $this->_company->phone,
            'company_chief_post_name' => $this->_company->chief_post_name,
            'company_chief_lastname' => $this->_company->chief_lastname,
            'company_chief_firstname_initials' => $this->_company->chief_firstname_initials,
            'company_chief_patronymic_initials' => $this->_company->chief_patronymic_initials,
            'company_chief_accountant_lastname' => $this->_company->chief_accountant_lastname,
            'company_chief_accountant_firstname_initials' => $this->_company->chief_accountant_firstname_initials,
            'company_chief_accountant_patronymic_initials' => $this->_company->chief_accountant_patronymic_initials,
            'company_print_filename' => $this->_company->print_link,
            'company_chief_signature_filename' => $this->_company->chief_signature_link,
        ]);
    }

    /**
     * @return Upd
     */
    public function getUpdModelTemplate()
    {
        return new Upd([
            'id' => null,
            'company_id' => $this->_company->id,
            'document_author_id' => $this->_employee->id,
            'status_out_author_id' => $this->_employee->id,
            'status_out_updated_at' => time(),
        ]);
    }

    /**
     * @return InvoiceFacture
     */
    public function getInvoiceFactureModelTemplate()
    {
        return new InvoiceFacture([
            'id' => null,
            //'company_id' => $this->_company->id,
            'document_author_id' => $this->_employee->id,
            'status_out_author_id' => $this->_employee->id,
            'status_out_updated_at' => time(),
        ]);
    }

    /**
     * @return CashOrderFlows
     */
    public function getPaymentOrderModelTemplate()
    {
        return new CashOrderFlows([
            'id' => null,
            'company_id' => $this->_company->id,
            'author_id' => $this->_employee->id,
            'cashbox_id' => $this->_cashbox->id,
        ]);
    }

    public function getActModelTemplate()
    {
        return new Act([
            'id' => null,
            'company_id' => $this->_company->id,
            'document_author_id' => $this->_employee->id,
        ]);
    }

    public function getPackingListModelTemplate()
    {
        return new PackingList([
            'id' => null,
            'company_id' => $this->_company->id,
            'document_author_id' => $this->_employee->id,
        ]);
    }
}