<?php
namespace common\components\moysklad\converter;

use common\models\Agreement;
use common\models\cash\Cashbox;
use common\models\cash\CashOrderFlows;
use common\models\Contractor;
use common\models\document\Act;
use common\models\document\Invoice;
use common\models\document\InvoiceFacture;
use common\models\document\Order;
use common\models\document\PackingList;
use common\models\document\Upd;
use common\models\product\Product;
use common\models\product\ProductGroup;
use common\models\product\ProductUnit;
use frontend\models\log\LogHelper;
use Yii;
use common\components\moysklad\MoyskladImport;
use common\components\moysklad\helper\MoyskladImportStatistics;
use common\components\moysklad\helper\MoyskladValidationErrors;
use yii\db\Transaction;
use yii\queue\Queue;

class MoyskladConverter {

    use kub\ContractorsTrait;
    use kub\ContractorAccountsTrait;
    use kub\ContractorAgreementsTrait;
    use kub\ProductGroupsTrait;
    use kub\ProductsTrait;
    use kub\InvoicesTrait;
    use kub\UpdsTrait;
    use kub\PaymentOrdersTrait;
    use kub\InvoiceFacturesTrait;

    use helper\AfterImportTrait;
    use helper\KubExistsIdTrait;
    use helper\KubModelTrait;
    use helper\PossibleBaseDocTrait;

    const TRANSACTION_MAX_ROWS = 100;
    const TRANSACTION_ISOLATION_LEVEL = Transaction::READ_COMMITTED;

    const NO_VALIDATE_CONTRACTORS = 1;
    const NO_VALIDATE_PRODUCTS = 0;

    const KUB_CONTRACTOR_TYPE = 'contractor';
    const KUB_PRODUCT_PRICE_FOR_BUY = 'product_price_for_buy';
    const KUB_PRODUCT_PRICE_FOR_SELL = 'product_price_for_sell';

    public static $UPDATE_BY_DOCS = [
        self::KUB_CONTRACTOR_TYPE => [],
        self::KUB_PRODUCT_PRICE_FOR_BUY => [],
        self::KUB_PRODUCT_PRICE_FOR_SELL => []
    ];

    /**
     * @var MoyskladImportStatistics
     */
    public $stat;

    /**
     * @var MoyskladValidationErrors
     */
    public $validation;

    /**
     * @var MoyskladImport $_import;
     */
    private $_import;

    private $_company;
    private $_employee;
    private $_cashbox;

    /**
     * @var MoyskladImport $import
     * @var MoyskladImportStatistics $stat
     */
    public function __construct(MoyskladImport $import)
    {
        $this->_import = $import;
        $this->_employee = $import->employee;
        $this->_company = $import->company;
        $this->_cashbox = $this->getCompanyMainCashbox();

        $this->_initObjectsTableSchema();
        $this->_initDocumentsTableSchema();
        $this->_initOthersTableSchema();

        $this->_setLogHelperTransaction(false);
    }

    public function __destruct()
    {
        $this->_setLogHelperTransaction(true);
    }

    /**
     * @return Cashbox
     */
    public function getCompanyMainCashbox()
    {
        $cashbox = $this->_company->getRubleCashboxes()
            ->where(['is_main' => 1])
            ->select('id')
            ->one();

        return $cashbox ?: new Cashbox(['company_id' => $this->_company->id, 'is_main' => 1]);
    }

    private function _initObjectsTableSchema()
    {
        $this->_initTableSchema([
            Contractor::tableName(),
            Product::tableName(),
            ProductGroup::tableName(),
            ProductUnit::tableName()
        ]);
    }

    private function _initDocumentsTableSchema()
    {
        $this->_initTableSchema([
            Invoice::tableName(),
            InvoiceFacture::tableName(),
            Upd::tableName(),
            Act::tableName(),
            PackingList::tableName(),
            CashOrderFlows::tableName(),
            Agreement::tableName(),
        ]);
    }

    private function _initTableSchema($tables)
    {
        foreach ($tables as $table) {
            Yii::$app->db->getTableSchema($table);
        }
    }

    private function _initOthersTableSchema()
    {
        $this->_initTableSchema([
            'queue',
            'tax_rate',
            'checking_accountant',
            'company_details_file',
            'order',
            'order_act',
            'order_packing_list',
            'order_upd'
        ]);
    }
    
    private function _setLogHelperTransaction($val)
    {
        LogHelper::$useTransaction = $val;
    }
}