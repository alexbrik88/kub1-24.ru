<?php
namespace common\components\moysklad\uploader;

use common\models\Company;
use common\models\employee\Employee;
use yii\helpers\FileHelper;

class MoyskladUploader {

    private $company;
    private $employee;

    /**
     * @param Company $company
     * @param Employee $employee
     */
    public function __construct(Company $company, Employee $employee)
    {
        $this->company = $company;
        $this->employee = $employee;
    }

    /**
     * @param $path
     * @return bool
     */
    public function saveTmpFile($path) {
        if (move_uploaded_file($path, $this->getUploadsDir() . DIRECTORY_SEPARATOR . $this->getTmpFilename()))
            return true;

        return false;
    }

    /**
     * @return bool
     */
    public function removeTmpFile() {
        @unlink($this->getUploadsDir() . DIRECTORY_SEPARATOR . $this->getTmpFilename());

        return true;
    }

    /**
     * @return string
     */
    public function getUploadsDir()
    {
        $uploadsDir = \Yii::getAlias('@common/uploads') . DIRECTORY_SEPARATOR . 'moysklad_import';
        if (!is_dir($uploadsDir))
            FileHelper::createDirectory($uploadsDir);

        return $uploadsDir;
    }

    /**
     * @return string
     */
    public function getTmpFilename()
    {
        return "import_c{$this->company->id}e{$this->employee->id}.xml";
    }
}