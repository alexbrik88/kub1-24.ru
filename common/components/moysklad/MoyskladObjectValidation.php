<?php

namespace common\components\moysklad;

use Yii;

/**
 * This is the model class for table "moysklad_object_validation".
 *
 * @property int $import_id
 * @property int $object_id
 * @property string|null $errors
 *
 * @property MoyskladImport $import
 * @property MoyskladObject $object
 */
class MoyskladObjectValidation extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'moysklad_object_validation';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['import_id', 'object_id'], 'required'],
            [['import_id', 'object_id'], 'integer'],
            [['errors'], 'string'],
            [['import_id', 'object_id'], 'unique', 'targetAttribute' => ['import_id', 'object_id']],
            [['import_id'], 'exist', 'skipOnError' => true, 'targetClass' => MoyskladImport::class, 'targetAttribute' => ['import_id' => 'id']],
            [['object_id'], 'exist', 'skipOnError' => true, 'targetClass' => MoyskladObject::class, 'targetAttribute' => ['object_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'import_id' => 'Import ID',
            'object_id' => 'Object ID',
            'errors' => 'Errors',
        ];
    }

    /**
     * Gets query for [[Import]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getImport()
    {
        return $this->hasOne(MoyskladImport::class, ['id' => 'import_id']);
    }

    /**
     * Gets query for [[Object]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getObject()
    {
        return $this->hasOne(MoyskladObject::class, ['id' => 'object_id']);
    }
}
