<?php

namespace common\components\data;

use RangeException;

class DataChunkCaller
{
    /** @var int */
    private const DEFAULT_BLOCK_SIZE = 1000;

    /**
     * @var callable
     */
    private $callback;

    /**
     * @var int
     */
    private $blockSize;

    /**
     * @param callable $callback
     * @param int $blockSize
     */
    public function __construct(callable $callback, int $blockSize = self::DEFAULT_BLOCK_SIZE)
    {
        $this->callback = $callback;
        $this->blockSize = $blockSize;

        if ($this->blockSize < 1) {
            throw new RangeException();
        }
    }

    /**
     * @inheritDoc
     */
    public function callChunk(array $data): int
    {
        $count = 0;

        foreach (array_chunk($data, $this->blockSize) as $part) {
            $result = call_user_func($this->callback, $part);

            if (is_int($result)) {
                $count += $result;
            }
        }

        return $count;
    }
}
