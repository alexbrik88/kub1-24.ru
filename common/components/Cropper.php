<?php
namespace common\components;

/**
 * Class Cropper
 */
class Cropper extends \yii\base\Component
{
    /**
     * @var integer JPEG image quality
     */
    public $jpeg_quality = 100;
    /**
     * @var integer PNG compression level (0 = no compression).
     */
    public $png_compression = 5;
    /**
     * @var string The path of cropping image file
     */
    public $imagePath;
    /**
     * @var string The path for saving thumbnails
     */
    public $destPath;

    public $width;

    /**
     * Get the cropping coordinates from post.
     *
     * @param type $attribute The model attribute name used.
     * @return array Cropping coordinates indexed by : x, y, h, w
     */
    public function getCoordsFromPost($attribute)
    {
        $coords = array('x' => null, 'y' => null, 'h' => null, 'w' => null);
        foreach ($coords as $key => $value) {
            $coords[$key] = round($_POST[$attribute . '_' . $key]);
        }
        return $coords;
    }

    /**
     * Crop an image and save the thumbnail.
     *
     * @param array $coords Cropping coordinates indexed by : x, y, h, w
     * @return string path of cropped image.
     */
    public function crop($attribute)
    {
        if (!$this->destPath) {
            throw new \Exception(__CLASS__ . ' : destPath is not specified.');
        }
        $coords = $this->getCoordsFromPost($attribute);
        foreach ($coords as $key => $value) {
            if ($value === null) {
                return false;
            }
        }
        $file_type = strtolower(pathinfo($this->imagePath, PATHINFO_EXTENSION));

        if ($file_type == 'jpg' || $file_type == 'jpeg') {
            $img = imagecreatefromjpeg($this->imagePath);
        } elseif ($file_type == 'png') {
            $img = imagecreatefrompng($this->imagePath);
        } else {
            return false;
        }
        if (!$coords['w'] || !$coords['h']) {
            return false;
        }
        $outImage = imagecreatetruecolor($coords['w'], $coords['h']);
        $colourBlack = imagecolorallocate($outImage, 0, 0, 0);
        imagecolortransparent($outImage, $colourBlack);
        imagealphablending($outImage, false);
        imagesavealpha($outImage, true);
        if (!imagecopyresampled($outImage, $img, 0, 0, $coords['x'], $coords['y'], $coords['w'], $coords['h'], $coords['w'], $coords['h'])) {
            return false;
        }

        // save only png or jpeg pictures
        if ($file_type == 'jpg' || $file_type == 'jpeg') {
            imagejpeg($outImage, $this->destPath, $this->jpeg_quality);
        } elseif ($file_type == 'png') {
            imagepng($outImage, $this->destPath, $this->png_compression);
        }

        return $this->destPath;
    }

}