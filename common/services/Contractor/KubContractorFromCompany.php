<?php

namespace common\services\Contractor;

use Yii;
use common\models\Company;
use common\models\Contractor;
use common\models\company\CompanyType;
use common\models\company\RegistrationPageType;
use frontend\modules\export\models\one_c\OneCExport;

/**
 * Создание контрагента для компании КУБ из компании, зарегистрированной в сервисе
 */
class KubContractorFromCompany
{
    private Company $kubCompany;
    private Company $company;

    public function __construct(Company $company)
    {
        $this->kubCompany = \Yii::$app->kubCompany;
        $this->company = $company;
    }

    private function getExistingContractor() : ?Contractor
    {
        $contractor = $this->kubCompany->getContractors()->andWhere([
            'created_from_company_id' => $this->company->id,
        ])->one();

        return $contractor;
    }

    private function getNewContractor() : Contractor
    {
        $contractor = new Contractor([
            'company_id' => $this->kubCompany->id,
            'type' => Contractor::TYPE_CUSTOMER,
            'chief_accountant_is_director' => true,
            'contact_is_director' => true,
            'object_guid' => OneCExport::generateGUID(),
            'created_from_company_id' => $this->company->id,
        ]);

        return $contractor;
    }

    public function create(bool $save = true) : ?Contractor
    {
        $this->errors = [];
        if ($this->company->id == $this->kubCompany->id) {
            return null;
        }

        $company = $this->company;
        $account = $company->mainCheckingAccountant;
        $contractor = $this->getExistingContractor() ?: $this->getNewContractor();

        $contractor->is_customer = 1;
        $contractor->is_deleted = false;
        $contractor->face_type = $company->self_employed ? Contractor::TYPE_PHYSICAL_PERSON : Contractor::TYPE_LEGAL_PERSON;
        $contractor->status = Contractor::ACTIVE;
        $contractor->company_type_id = $company->company_type_id;
        $contractor->name = $company->name_full ?: $company->name_short;
        $contractor->director_name = $company->getChiefFio();
        $contractor->director_email = $company->email;
        $contractor->director_phone = $company->phone;
        $contractor->legal_address = $company->getAddressLegalFull();
        $contractor->actual_address = $company->getAddressActualFull();
        $contractor->ITN = $company->inn;
        $contractor->PPC = $company->kpp;
        $contractor->BIN = ($company->company_type_id == CompanyType::TYPE_IP) ? ($company->egrip ? $company->egrip : '') : $company->ogrn;
        $contractor->current_account = $account ? $account->rs : null;
        $contractor->bank_name = $account ? $account->bank_name : null;
        $contractor->corresp_account = $account ? $account->ks : null;
        $contractor->BIC = $account ? $account->bik : null;
        $contractor->taxation_system = $company->companyTaxationType->osno ? Contractor::WITH_NDS : Contractor::WITHOUT_NDS;
        $contractor->campaign_id = $this->getCompaignId($company);
        $contractor->physical_firstname = $company->self_employed ? $company->ip_firstname : null;
        $contractor->physical_lastname = $company->self_employed ? $company->ip_lastname : null;
        $contractor->physical_patronymic = $company->self_employed ? $company->ip_patronymic : null;

        if ($save && !$contractor->save(false)) {
            \common\components\helpers\ModelHelper::logErrors($contractor, __METHOD__);

            return null;
        }

        return $contractor;
    }

    private function getCompaignId(Company $company)
    {
        if (in_array($company->registration_page_type_id, RegistrationPageType::$analyticsPages)) {
            return Yii::$app->params['kub_contractor_campaign_ids'][$company->utm_source] ?? null;
        }

        return Yii::$app->params['kub_contractor_campaign_ids']['default'] ?? null;
    }
}
