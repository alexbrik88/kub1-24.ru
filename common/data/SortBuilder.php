<?php

namespace common\data;

use yii\data\Sort;

class SortBuilder
{
    /**
     * @var array
     */
    private $params = [];

    /**
     * @param string $attribute
     * @param string $column
     * @param string|null $additional
     */
    public function setAttribute(string $attribute, string $column, string $additional = null): void
    {
        $asc = $additional ? [$column => SORT_ASC, $additional => SORT_ASC] : [$column => SORT_ASC];
        $desc = $additional ? [$column => SORT_DESC, $additional => SORT_DESC] : [$column => SORT_DESC];
        $this->params['attributes'][$attribute] = ['asc' => $asc, 'desc' => $desc];
    }

    /**
     * @param string[] $attributes
     * @param string|null $additional
     */
    public function setAttributes(array $attributes, string $additional = null): void
    {
        array_walk($attributes, function (string $column, string $attribute) use ($additional): void {
            $this->setAttribute($attribute, $column, $additional);
        });
    }

    /**
     * @param string $attribute
     * @param string|null $column
     * @param int $order
     * @param string|null $additional
     */
    public function setDefaultOrder(
        string $attribute,
        string $column = null,
        int $order = SORT_ASC,
        string $additional = null
    ): void {
        if ($column === null) {
            $column = $attribute;
        }

        $this->params['defaultOrder'] = $additional
            ? [$attribute => [$column => $order, $additional => $order]]
            : [$attribute => [$column => $order]];
    }

    /**
     * @return Sort
     */
    public function buildSort(): Sort
    {
        return new Sort($this->params);
    }
}
