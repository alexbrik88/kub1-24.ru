<?php
return [
    'serviceSite' => 'https://kub-24.ru/',
    'serviceSiteLk' => 'https://lk.kub-24.ru/',
    'serviceSiteStore' => 'https://store.kub-24.ru/',

    'siteModules' => [
        'invoices' => 'https://kub-24.ru/',
        'price-lists' => 'https://kub-24.ru/price-list-1/',
        'b2b' => 'https://kub-24.ru/modul-b2b-sales/',
        'bookkeeping' => 'https://robot.kub-24.ru/',
        'analysis' => 'https://kub-24.ru/finance/',
    ],

    'user.passwordResetTokenExpire' => 3600,

    'image.placeholder' => '/img/placeholder_200x200.gif',

    'api_partners.android.id' => 1,

    'emailList' => [
        'docs' => 'docs@domain',
        'admin' => 'admin@domain',
        'info' => 'info@domain',
        'support' => 'support@domain',
    ],

    'passwordValidator' => [
        'minLength' => 8,
        'minConditions' => 3,
    ],

    // form
    /** for itself */
    'formDefaultConfig' => [
        'layout' => 'horizontal',
        'fieldConfig' => [
            'labelOptions' => [
                'class' => 'col-md-4 control-label bold-text',
            ],
            'wrapperOptions' => [
                'class' => 'col-md-6 inp_one_line-product',
            ],
            'hintOptions' => [
                'tag' => 'small',
                'class' => 'text-muted',
            ],
            'horizontalCssClasses' => [
                'offset' => 'col-md-offset-4',
                'hint' => 'col-md-8',
            ],
            'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
        ],
    ],

    'requiredOptions' => [
        'options' => [
            'class' => 'form-group required',
        ],
    ],

    'formDatePickerTemplate' =>
        "{label}\n{beginWrapper}\n"
        . '<div class="input-icon"><i class="fa fa-calendar"></i>'
        . "{input}"
        . '</div>'
        . "\n{error}\n{hint}\n{endWrapper}",

    'numberPattern' => '/^\s*[-+]?[0-9]*[\.,]?[0-9]+([eE][-+]?[0-9]+)?\s*$/',

    'uploadDir' => '@common/uploads',
    'unisenderApiKey' => '5ui9tajabarz87mochxqdswaidf5m3im63jnntpa',

    'paymentUser' => null,

    'emailFromName' => 'КУБ24',

    //Google Analytics tracking ID
    'GATID' => 'UA-85802138-1',

    'free_tariff' => [
        'upload_limit' => 1024, //MB
        'invoice_limit' => 5,
    ],

    'trial_tariff' => [
        'upload_limit' => 2048, //MB
        'invoice_limit' => 10,
    ],
    'free_store_cabinets_count' => 5,

    'maxCashSum' => 999 * 1000 * 1000, // without kopeck

    // bank integration module params
    'banking' => [
        // sberbank
        'sberbank' => [
            'ssl_cert' => '',
            'ssl_key' => '',
            'ssl_key_pass' => '',
            'ca_info' => '',
            'client_id' => '8220',
            'client_secret' => 'jItxT491',
            'debugMode' => true,
        ],
        // tinkoff bank
        'tinkoff' => [
            'ssl_key' => '',
            'ssl_cert' => '',
            'ssl_key_pass' => '',
            'auth_basic' => '',
            'client_id' => '',
            'securityKey' => '',
            'partnerId' => '',
        ],
        // tochka
        'tochka' => [
            'client_id' => '',
            'client_secret' => '',
        ],
        // otkrytiye
        'otkrytiye' => [
            'service_name' => 'cube',
            'service_name.invoice' => 'cube',
            'service_name.taxrobot' => 'cube_accountant',
            'client_id' => 'cube',
            'client_id_2' => 'cube_m2m',
            'client_secret' => 'password',
            'client_secret_2' => 'password',
        ],
        // modulbank
        'modulbank' => [
            'sandbox' => true,
            'clientId' => 'sandboxapp',
            'clientSecret' => 'sandboxappsecret',
            'token' => 'sandboxtoken',
        ],
    ],

    // OFD integration modules params
    'ofd' => [
        'taxcom' => [
            'debugMode' => true,
            'integratorId' => '341ABB11-ED3F-4436-9869-42DA62A0CD43',
            'client_id' => 'prodcdkub',
            'client_secret' => 'qwerty'
        ],
        'platforma' => [
            'debugMode' => true,
            'app_token' => 'evotor_token',
            'base_url' => 'https://api.evotor.ru/',
        ],
    ],

    // OFD import documents integration modules params
    'ofd_import' => [
        'taxcom' => [
            'debugMode' => true,
            'client_id' => 'sboskub',
            'client_secret' => 'qwerty',
        ],
    ],

    // VESTA
    'vesta' => [
        'hostname' => '',
        'username' => '',
        'password' => '',
        'mail_domain' => 'inbox.kub-24.ru',
        'one_s_mail_domain' => 'lc.kub-24.ru',
        'mail_user' => '',
        'imap_host' => 'kub-24.ru',
        'imap_port' => '143',
    ],

    // ENTERA
    'entera' => [
        'login' => 'support@kub-24.ru',
        'password' => 'TPf1123581321',
    ],

    // GET RESPONSE API
    'get_response' => [
        'api_key' => ''
    ],

    /**
     * DaData API token
     * https://dadata.ru/api/
     */
    'dadata_token' => '78497656dfc90c2b00308d616feb9df60c503f51',

    // 1-WEB, 2-API, 3-B2B
    'create_type_id' => 1,
    'create_api_id' => null,

    // 1-PC, 2-Mobile, 3-E-mail
    'upload_type_id' => 1,

    'moneta' => [
        'encrypt_password' => 'SnzvG36G94muK6ZfAEEM',
    ],
    'zachestniybiznes_api_key' => 'oVLstLd4Um5eEgdVGiLpXCAybxn_JCls',

    /**
     * Data of banks that are not in the BikDictionary
     *
     * See console\controllers\BikController::$attributes
     */
    'static_bik_dictionary' => [
        [
            'bik' => '048142711',
            'ks' => '30101810500000000711',
            'name' => 'БУРЯТСКИЙ ФИЛИАЛ ПАО АКБ "СВЯЗЬ-БАНК"',
            'namemini' => 'СВЯЗЬ-БАНК',
            'index' => '670034',
            'city' => 'Улан-Удэ',
            'address' => 'пр-т 50 ЛЕТ ОКТЯБРЯ, 26',
            'phone',
            'okato',
            'okpo',
            'regnum',
            'srok',
            'dateadd',
            'datechange',
        ],
    ],
    'google' => [
        'analyticsOauthRedirectUri' => YII_ENV_PROD
            ? 'https://lk.kub-24.ru/analytics/marketing/default/analytics-token'
            : 'https://dev.kub-24.ru/analytics/marketing/default/analytics-token',
    ],

    'telegram' => [
        'bot_token' => '1177454576:AAEworCg-dpndHaAJuyMA2Ymx6eqC8nRioo',
        'bot_link' => 'https://t.me/kub24_bot',
        'web_hook_secret' => 'PdnGX33jx9NxVto4NoElyJL6WSLiQPzmVDYjaUmUI7lPQodpTV0EaAcOFb0P1mNh',
    ],
    'crm' => [
        // Список идентификаторов компаний, имеющих доступ к модулю (пустой массив - все компании)
        'allowCompanies' => [],
    ],
    'kub_contractor_campaign_ids' => [
        'default' => null,
        'facebook' => null,
        'google' => null,
        'startpack' => null,
        'yandex' => null,
    ],
];
