<?php

return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm' => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'language' => 'ru-RU',
    'sourceLanguage' => 'en-US',
    'bootstrap' => [
        'thumbnail',
    ],
    'container' => require __DIR__ . '/container.php',
    'components' => [
        'kubCompany' => [
            'class' => 'common\components\KubCompany',
        ],
        'formatter' => [
            'class' => 'common\components\Formatter',
            'timeFormat' => 'HH:mm',
            'dateFormat' => 'dd.MM.Y',
            'datetimeFormat' => 'dd.MM.Y HH:mm',
            'decimalSeparator' => ',',
            'thousandSeparator' => ' ',
            'nullDisplay' => '',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'thumbnail' => [
            'class' => 'himiklab\thumbnail\EasyThumbnail',
            'cacheAlias' => 'assets/gallery_thumbnails',
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                'thumbs/<path:.*>' => 'site/thumb',
            ],
        ],
        'assetManager' => [
            'bundles' => [
                'yii\bootstrap\BootstrapPluginAsset' => [
                    'js' => [],
                ],
                'yii\web\JqueryAsset' => [
                    'jsOptions' => ['position' => \yii\web\View::POS_HEAD],
                ],
                'yii2mod\slider\IonSliderAsset' => [
                    'css' => [
                        'css/normalize.css',
                        'css/ion.rangeSlider.css',
                        'css/ion.rangeSlider.skinFlat.css'
                    ]
                ],
            ],
            'appendTimestamp' => true,
        ],
        'i18n' => [
            'translations' => [
                'file-input*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'sourceLanguage' => 'en-US',
                    'basePath' => '@vendor/2amigos/yii2-file-input-widget/src/messages/',
                ],
            ],
        ],
        'queue' => 'yii\queue\Queue',
    ],
];
