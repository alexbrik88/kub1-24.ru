<?php

return [
    'singletons' => [
        'yii\queue\db\Queue' => [
            'class' => 'yii\queue\db\Queue',
            'ttr' => 60 * 20,
            'mutex' => 'yii\mutex\MysqlMutex',
            'as log' => 'yii\queue\LogBehavior',
            'on afterError' => function (\yii\queue\ExecEvent $event) {
                $text = date('c')." ".$event->error->__toString()."\n\n";
                $file = Yii::getAlias('@runtime/logs/queue_job_error.log');
                file_put_contents($file, $text, FILE_APPEND);
            },
        ],
        'yii\queue\Queue' => 'yii\queue\db\Queue', // Alias
        'common\modules\import\components\ImportCommandManager' => [
            'class' => 'common\modules\import\components\ImportCommandManager',
            'commands' => [
                2   => 'common\modules\acquiring\commands\MonetaImportCommand',
                1   => 'common\tasks\ExportUnisender',
                9   => 'common\modules\marketing\commands\FacebookImportCommand',
                7   => 'common\modules\marketing\commands\GoogleAdsImportCommand',
                8   => 'common\modules\marketing\commands\VkAdsImportCommand',
                6   => 'common\modules\marketing\commands\YandexDirectImportCommand',
                10  => 'common\modules\acquiring\commands\YookassaImportCommand',
                11  => 'common\modules\cards\commands\ZenmoneyImportCommand',
            ],
        ],
        'common\modules\marketing\components\YandexDirectComponent' => [
            'class' => 'common\modules\marketing\components\YandexDirectComponent',
            'clientId' => '9af4066f2637495aa20dbeafd1407e8f',
            'secret' => 'ccbc018487964eb7a09acc2147a96996',
            'redirectUri' => YII_ENV_PROD
                ? 'https://lk.kub-24.ru/integration/yandex-direct/token'
                : 'https://dev.kub-24.ru/integration/yandex-direct/token',
        ],
        'common\modules\marketing\components\GoogleAdsComponent' => [
            'class' => 'common\modules\marketing\components\GoogleAdsComponent',
            'clientId' => '1034436667027-rqmk46uvvj41j9qvb5vu40co3m02b8sb.apps.googleusercontent.com',
            'clientSecret' => 'r00ajZlJuTB2WgUuzk6rAG7v',
            'redirectUri' => YII_ENV_PROD
                ? 'https://lk.kub-24.ru/analytics/marketing/google-words/token'
                : 'https://dev.kub-24.ru/analytics/marketing/google-words/token',
        ],
        'common\modules\marketing\components\GoogleAnalyticsComponent' => [
            'class' => 'common\modules\marketing\components\GoogleAnalyticsComponent',
            'clientId' => '1034436667027-rqmk46uvvj41j9qvb5vu40co3m02b8sb.apps.googleusercontent.com',
            'clientSecret' => 'r00ajZlJuTB2WgUuzk6rAG7v',
            'redirectUri' => YII_ENV_PROD
                ? 'https://lk.kub-24.ru/analytics/marketing/default/analytics-token'
                : 'https://dev.kub-24.ru/analytics/marketing/default/analytics-token',
        ],
        'common\modules\marketing\components\VkAdsComponent' => [
            'class' => 'common\modules\marketing\components\VkAdsComponent',
            'clientId' => 7153606,
            'clientSecret' => 'Oo3eqG3sxb45qvrbUFlm',
            'redirectUri' => YII_ENV_PROD
                ? 'https://lk.kub-24.ru/analytics/marketing/vk-ads/token'
                : 'http://dev.kub-24.ru/analytics/marketing/vk-ads/token',
        ],
        'common\modules\marketing\components\FacebookComponent' => [
            'class' => 'common\modules\marketing\components\FacebookComponent',
            'clientId' => '426911014625985',
            'clientSecret' => 'a11ffb966b8106bb6667622f3cd80297',
            'redirectUri' => YII_ENV_PROD
                ? 'https://lk.kub-24.ru/analytics/marketing/facebook/token'
                : 'https://dev.kub-24.ru/analytics/marketing/facebook/token',
        ],
        'common\modules\acquiring\components\YookassaComponent' => [
            'class' => 'common\modules\acquiring\components\YookassaComponent',
            'clientId' => YII_ENV_PROD
                ? 'o96vbbvpbj6ngaps98e76j7e2umiek5u'
                : 'i97f9ekh0nh96h5ijq5t7m0k6eptsact',
            'clientSecret' => YII_ENV_PROD
                ? 'ha72gnQum_cKSjy0RnXlZVW64KFS8oBRKKyBhDVEcJN4eNVG8ex8s_iInvyQfpMR'
                : 'TzdDx_mxCh8XPtV68NXjl1o5LMT-rc7jl5jnMTObTucdsWZUHRVjnYHgtKAkhxKB',
        ],
        'common\modules\cards\components\ZenmoneyComponent' => [
            'class' => 'common\modules\cards\components\ZenmoneyComponent',
            'clientId' => YII_ENV_PROD
                ? 'gc297b70b8455fc898ff0025687748'
                : 'g4c1642746a2a82177062b8d02b378',
            'clientSecret' => YII_ENV_PROD
                ? '00d0e56e0c'
                : '87e5876b0e',
            'redirectUri' => YII_ENV_PROD
                ? 'https://lk.kub-24.ru/cards/zenmoney/token'
                : 'https://dev.kub-24.ru/cards/zenmoney/token',
        ],
    ],
];
