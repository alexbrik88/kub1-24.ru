<?php

namespace common\modules\analytics;

use common\models\Company;
use frontend\modules\analytics\models\AnalyticsArticleForm;
use frontend\modules\analytics\models\AnalyticsSimpleSearch;
use frontend\modules\analytics\models\ExpensesSearch;
use frontend\modules\analytics\models\IncomeSearch;
use frontend\modules\analytics\models\OddsSearch;
use frontend\modules\analytics\models\ProfitAndLossSearchModel;
use frontend\modules\analytics\models\BalanceSearch2;

class AnalyticsManager {

    private static ?ProfitAndLossSearchModel $palModel = null;
    private static array $balanceModel = []; // BalanceSearch2[]

    public static function getPalModel(array $userOptions = []): ProfitAndLossSearchModel
    {
        if (empty(self::$palModel)) {
            self::$palModel = new ProfitAndLossSearchModel();
            self::$palModel->disableMultiCompanyMode();
            self::$palModel->setUserOption('revenueRuleGroup', AnalyticsArticleForm::REVENUE_RULE_GROUP_UNSET);
            self::$palModel->setUserOption('revenueRule', AnalyticsArticleForm::REVENUE_RULE_UNSET);
            self::$palModel->setUserOption('monthGroupBy', AnalyticsArticleForm::MONTH_GROUP_UNSET);
            foreach ($userOptions as $option => $value) {
                self::$palModel->setUserOption($option, $value);
            }
            self::$palModel->handleItems();
        }

        return self::$palModel;
    }

    public static function getIncomeSearchModel(): IncomeSearch
    {
        return (new IncomeSearch());
    }

    public static function getExpensesSearchModel(): ExpensesSearch
    {
        return (new ExpensesSearch());
    }

    public static function getOddsModel(): OddsSearch
    {
        return (new OddsSearch());
    }

    public static function getBalanceModel(int $balanceYear): BalanceSearch2
    {
        if (empty(self::$balanceModel[$balanceYear])) {
            self::$balanceModel[$balanceYear] = new BalanceSearch2();
            self::$balanceModel[$balanceYear]->year = $balanceYear;
            self::$balanceModel[$balanceYear]->search();
        }

        return self::$balanceModel[$balanceYear];
    }

    public static function getAnalyticsSimpleSearchModel(Company $company): AnalyticsSimpleSearch
    {
        return new AnalyticsSimpleSearch($company);
    }
}