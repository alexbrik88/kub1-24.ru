<?php

namespace common\modules\cards\behaviors;

use common\modules\cards\models\CardAccount;
use common\modules\cards\models\CardAccountRepository;
use common\modules\cards\models\CardIdentityInterface;
use yii\base\Behavior;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;

/**
 * @property-read CardAccount $cardAccount
 * @property-read CardIdentityInterface $owner
 */
class CardIdentityBehavior extends Behavior
{
    /**
     * @var CardAccount|null
     */
    private ?CardAccount $_cardAccount = null;

    /**
     * @inheritDoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'enableAccount',
            ActiveRecord::EVENT_AFTER_UPDATE => 'enableAccount',
            ActiveRecord::EVENT_AFTER_DELETE => 'disableAccount',
        ];
    }

    /**
     * @return void
     * @throws InvalidConfigException
     */
    public function enableAccount(): void
    {
        if (!($this->owner instanceof CardIdentityInterface)) {
            throw new InvalidConfigException();
        }

        $this->cardAccount->is_active = true;
        $this->cardAccount->save();
    }

    /**
     * @return void
     * @throws InvalidConfigException
     */
    public function disableAccount(): void
    {
        if (!($this->owner instanceof CardIdentityInterface)) {
            throw new InvalidConfigException();
        }

        $this->cardAccount->is_active = false;
        $this->cardAccount->save();
    }

    /**
     * @return CardAccount
     */
    public function getCardAccount(): CardAccount
    {
        if ($this->_cardAccount === null) {
            $this->_cardAccount = CardAccountRepository::findAccount($this->owner);
        }

        if ($this->_cardAccount === null) {
            $this->_cardAccount = CardAccountRepository::createAccount($this->owner);
        }

        return $this->_cardAccount;
    }
}
