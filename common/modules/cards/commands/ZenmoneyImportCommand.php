<?php

namespace common\modules\cards\commands;

use common\components\data\DataChunkCaller;
use common\components\date\DateHelper;
use common\models\Contractor;
use common\models\contractor\ContractorRepository;
use common\models\currency\Currency;
use common\models\document\ExpenditureItemRepository;
use common\models\document\IncomeItemRepository;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\modules\cards\components\ZenmoneyException;
use common\modules\cards\components\ZenmoneyIntegration;
use common\modules\cards\models\CardBill;
use common\modules\cards\models\CardBillRepository;
use common\modules\cards\models\CardOperation;
use common\modules\cards\models\ZenmoneyIdentity;
use common\modules\cards\models\ZenmoneyIdentityRepository;
use common\modules\import\commands\ImportCommandInterface;
use common\modules\import\commands\ImportCommandTrait;
use common\modules\import\components\ImportParamsHelper;
use common\modules\import\components\ImportUnauthorizedException;
use common\modules\import\models\ImportJobData;
use DateTimeInterface;
use yii\db\Exception;

final class ZenmoneyImportCommand implements ImportCommandInterface
{
    use ImportCommandTrait;

    /**
     * @var DateTimeInterface
     */
    private DateTimeInterface $dateFrom;

    /**
     * @var DateTimeInterface
     */
    private DateTimeInterface $dateTo;

    /**
     * @var ZenmoneyIdentity
     */
    private ZenmoneyIdentity $identity;

    /**
     * @var ZenmoneyIntegration
     */
    private ZenmoneyIntegration $integration;

    /**
     * @var Contractor[]
     */
    private array $_contractors = [];

    /**
     * @var InvoiceExpenditureItem[]
     */
    private array $_expenditureItems = [];

    /**
     * @var InvoiceIncomeItem[]
     */
    private array $_incomeItems = [];

    /**
     * @var CardBill[]
     */
    private array $_bills = [];

    /**
     * @param ZenmoneyIdentity $identity
     * @param DateTimeInterface $dateFrom
     * @param DateTimeInterface $dateTo
     */
    public function __construct(ZenmoneyIdentity $identity, DateTimeInterface $dateFrom, DateTimeInterface $dateTo)
    {
        $this->identity = $identity;
        $this->integration = new ZenmoneyIntegration($identity->access_token);
        $this->dateFrom = $dateFrom;
        $this->dateTo = $dateTo;
    }

    /**
     * @inheritDoc
     */
    public static function createImportCommand(ImportJobData $jobData): ImportCommandInterface
    {
        $helper = new ImportParamsHelper($jobData);
        $identity = ZenmoneyIdentityRepository::findByAccountId($helper->getIdentifier(), $jobData->company);

        if ($identity === null) {
            throw new ImportUnauthorizedException();
        }

        return new self($identity, $helper->getDateFrom(), $helper->getDateTo());
    }

    /**
     * @inheritDoc
     */
    public static function getImportCommandName(): string
    {
        return 'Выгрузка операций из Дзен мани';
    }

    /**
     * @inheritDoc
     */
    public static function getImportCommandLabel(): string
    {
        return 'Дзен мани';
    }

    /**
     * @inheritDoc
     * @throws ZenmoneyException
     */
    public function run(): void
    {
        $chunkCaller = new DataChunkCaller([$this, 'saveTransactions']);
        $this->savedCount = $chunkCaller->callChunk($this->integration->getTransactions($this->dateFrom));
    }

    /**
     * @param array[] $transactions
     * @return int
     * @throws Exception
     */
    public function saveTransactions(array $transactions): int
    {
        //$rows = array_map([$this, 'createRowData'], $transactions);
        $currencyArray = $this->integration->getData()['instrument'];
        $rows = [];
        foreach ($transactions as $transaction) {
            if (!$transaction['deleted'])
                $rows[] = $this->createRowData($transaction, $currencyArray);
        }
        if (empty($rows))
            return 0;

        $columns = array_keys($rows[0]);
        $db = CardOperation::getDb();
        $sql = $db
                ->createCommand()
                ->batchInsert(CardOperation::tableName(), $columns, $rows)
                ->getSql() . 'ON DUPLICATE KEY UPDATE
                flow_type = VALUES(flow_type),
                is_deleted = VALUES(is_deleted),
                amount = VALUES(amount),
                amount_rub = VALUES(amount_rub),
                date = VALUES(date),
                time = VALUES(time),
                bill_id = VALUES(bill_id),
                recognition_date = VALUES(recognition_date),
                expenditure_item_id = VALUES(expenditure_item_id),
                income_item_id = VALUES(income_item_id),
                contractor_id = VALUES(contractor_id),
                is_accounting = VALUES(is_accounting)';

        return $db->createCommand($sql)->execute();
    }

    /**
     * @param array $transaction
     * @return array
     * @throws ZenmoneyException
     */
    private function createRowData(array $transaction, array $currencyArray): array
    {
        $reason = $this->fetchReason($transaction);
        $isIncome = ($transaction['income'] > 0);
        $bill = $this->getBill($transaction[$isIncome ? 'incomeAccount' : 'outcomeAccount']);
        $flowType = $isIncome ? CardOperation::FLOW_TYPE_INCOME : CardOperation::FLOW_TYPE_EXPENSE;
        $contractor_id = $transaction['payee'] ? $this->getContractor($transaction['payee'], $flowType)->id : null;
        $expenditure_item_id = ($reason && !$isIncome) ? $this->getExpenditureItem($reason)->id : null;
        $income_item_id = ($reason && $isIncome) ? $this->getIncomeItem($reason)->id : null;
        $amount = $isIncome ? $transaction['income'] : $transaction['outcome'];
        $currency = $isIncome ? ($transaction['opIncomeInstrument'] ?? null) : ($transaction['opOutcomeInstrument'] ?? null);
        $currencyId = $currencyName = null;
        if ($currency) {
            $currencyName = ArrayHelper::getValue($currencyArray, $currency);
            $currencyId = array_search($currencyName, Currency::$currencyNameById) ?: null;
        }
        if ($currencyId) {
            $currencyAmount = $isIncome ? $transaction['opIncome'] : $transaction['opOutcome'];
        } else {
            $currencyAmount = $amount;
            $currencyId = Currency::DEFAULT_ID;
            $currencyName = Currency::DEFAULT_NAME;
        }

        return [
            'account_id' => $this->identity->cardAccount->id,
            'bill_id' => $bill->id,
            'company_id' => $this->identity->company->id,
            'contractor_id' => $contractor_id,
            'expenditure_item_id' => $expenditure_item_id,
            'income_item_id' => $income_item_id,
            'currency_id' => $currencyId,
            'currency_name' => $currencyName,
            'flow_type' => $flowType,
            'is_deleted' => $transaction['deleted'],
            'amount' => $currencyAmount * 100,
            'amount_rub' => $amount * 100,
            'uuid' => $transaction['id'],
            'description' => (string) $transaction['comment'],
            'recognition_date' => $transaction['date'],
            'date' => $transaction['date'],
            'time' => date(DateHelper::FORMAT_TIME, $transaction['created']),
            'is_accounting' => (int)$bill->is_accounting
        ];
    }

    /**
     * @param array $transaction
     * @return string|null
     * @throws ZenmoneyException
     */
    private function fetchReason(array $transaction): ?string
    {
        if (!isset($transaction['tag'][0])) {
            return null;
        }

        return $this->integration->getTagTitle($transaction['tag'][0]);
    }

    /**
     * @param string $name
     * @param int $flowType
     * @return Contractor
     */
    private function getContractor(string $name, int $flowType): Contractor
    {
        $contractor = $this->_contractors[$name] ?? null;

        if ($contractor === null) {
            $contractor = ContractorRepository::findByName($name, $this->identity->company);
        }

        if ($contractor === null) {
            $contractor = ContractorRepository::createContractor(
                $name,
                ($flowType == CardOperation::FLOW_TYPE_EXPENSE) ? Contractor::TYPE_SELLER : Contractor::TYPE_CUSTOMER,
                Contractor::TYPE_LEGAL_PERSON,
                $this->identity->company
            );
        }

        if ($flowType == CardOperation::FLOW_TYPE_EXPENSE && !$contractor->is_seller) {
            $contractor->is_seller = true;
            $contractor->save(false);
        }

        if ($flowType == CardOperation::FLOW_TYPE_INCOME && !$contractor->is_customer) {
            $contractor->is_customer = true;
            $contractor->save(false);
        }

        $this->_contractors[$name] = $contractor;

        return $contractor;
    }

    /**
     * @param string $uuid
     * @return CardBill
     * @throws ZenmoneyException
     */
    private function getBill(string $uuid): CardBill
    {
        $bill = $this->_bills[$uuid] ?? null;
        $number = $this->integration->getAccountNumbers($uuid)[0] ?? '';

        if ($bill === null) {
            $bill = CardBillRepository::findByUuid($uuid, $this->identity->cardAccount);
        }

        if ($bill === null) {
            $bill = CardBillRepository::createCardBill(
                $this->identity->cardAccount,
                $uuid,
                $this->integration->getAccountTitle($uuid),
                $number
            );
        }

        $bill->setAttribute('number', $number);

        if ($bill->isNewRecord || $bill->isAttributeChanged('number')) {
            $bill->save();
        }

        $this->_bills[$uuid] = $bill;

        return $bill;
    }

    /**
     * @param string $reason
     * @return InvoiceExpenditureItem
     */
    private function getExpenditureItem(string $reason): InvoiceExpenditureItem
    {
        $item = $this->_expenditureItems[$reason] ?? null;

        if ($item === null) {
            $item = ExpenditureItemRepository::findByReason($reason, $this->identity->company);
        }

        if ($item === null) {
            $item = ExpenditureItemRepository::createExpenditureItem($reason, $this->identity->company);
            $item->save();
        }

        $this->_expenditureItems[$reason] = $item;

        return $item;
    }

    /**
     * @param string $reason
     * @return InvoiceIncomeItem
     */
    private function getIncomeItem(string $reason): InvoiceIncomeItem
    {
        $item = $this->_incomeItems[$reason] ?? null;

        if ($item === null) {
            $item = IncomeItemRepository::findByReason($reason, $this->identity->company);
        }

        if ($item === null) {
            $item = IncomeItemRepository::createIncomeItem($reason, $this->identity->company);
            $item->save();
        }

        $this->_incomeItems[$reason] = $item;

        return $item;
    }
}
