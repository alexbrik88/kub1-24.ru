<?php

namespace common\modules\cards\models;

use common\models\Company;
use common\models\ListBuilder;
use common\models\ListInterface;

class CardBillList implements ListInterface
{
    /**
     * @var Company
     */
    private Company $company;

    /**
     * @var CardAccount|null
     */
    private ?CardAccount $account;

    /**
     * @var string[]|null
     */
    private ?array $_items = null;

    /**
     * @param Company $company
     * @param CardAccount|null $account
     */
    public function __construct(Company $company, ?CardAccount $account)
    {
        $this->company = $company;
        $this->account = $account;
    }

    /**
     * @inheritDoc
     */
    public function getItems(): array
    {
        if ($this->_items === null) {
            $repository = new CardBillRepository([
                'company' => $this->company,
                'account' => $this->account,
            ]);
            $builder = new ListBuilder();
            $builder->setSort($repository->getSort());
            $builder->setIndexBy('name');
            $this->_items = $builder->buildItems($repository->getDataProvider(), 'name');
        }

        return $this->_items;
    }
}
