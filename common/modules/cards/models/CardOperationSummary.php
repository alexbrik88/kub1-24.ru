<?php

namespace common\modules\cards\models;

class CardOperationSummary
{
    /**
     * @var int
     */
    private int $startBalance;

    /**
     * @var int
     */
    private int $endBalance;

    /**
     * @var int
     */
    private int $totalIncome;

    /**
     * @var int
     */
    private int $totalExpense;

    /**
     * @var int
     */
    private int $countIncome;

    /**
     * @var int
     */
    private int $countExpense;

    /**
     * @param int $startBalance
     * @param int $endBalance
     * @param int $totalIncome
     * @param int $totalExpense
     * @param int $countIncome
     * @param int $countExpense
     */
    public function __construct(
        int $startBalance,
        int $endBalance,
        int $totalIncome,
        int $totalExpense,
        int $countIncome,
        int $countExpense
    ) {
        $this->startBalance = $startBalance;
        $this->endBalance = $endBalance;
        $this->totalIncome = $totalIncome;
        $this->totalExpense = $totalExpense;
        $this->countIncome = $countIncome;
        $this->countExpense = $countExpense;
    }

    /**
     * @return int
     */
    public function getStartBalance(): int
    {
        return $this->startBalance;
    }

    /**
     * @return int
     */
    public function getEndBalance(): int
    {
        return $this->endBalance;
    }

    /**
     * @return int
     */
    public function getTotalIncome(): int
    {
        return $this->totalIncome;
    }

    /**
     * @return int
     */
    public function getTotalExpense(): int
    {
        return $this->totalExpense;
    }

    /**
     * @return int
     */
    public function getCountIncome(): int
    {
        return $this->countIncome;
    }

    /**
     * @return int
     */
    public function getCountExpense(): int
    {
        return $this->countExpense;
    }
}
