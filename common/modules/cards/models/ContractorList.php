<?php

namespace common\modules\cards\models;

use common\models\Company;
use common\models\ListInterface;
use frontend\modules\cash\models\CashContractorType;
use yii\db\ActiveRecordInterface;

class ContractorList implements ListInterface
{
    /**
     * @var array|null
     */
    private ?array $_items = null;

    /**
     * @var CardOperationRepository
     */
    private CardOperationRepository $repository;

    /**
     * @param Company|null $company
     * @param CardAccount|null $account
     */
    public function __construct(?Company $company, ?CardAccount $account)
    {
        $this->repository = new CardOperationRepository([
            'company' => $company,
            'account' => $account,
        ]);
    }

    /**
     * @inheritDoc
     */
    public function getItems(): array
    {
        if ($this->_items === null) {
            $this->_items = [];

            foreach ($this->getOperations() as $operation) {
                if ($operation->contractor) {
                    $this->_items[$operation->contractor->id] = $operation->contractor->name;
                } elseif ($operation->contractor_id == CashContractorType::BALANCE_TEXT) {
                    $this->_items[$operation->contractor_id] = 'Баланс начальный';
                }
            }
        }

        return $this->_items;
    }

    /**
     * @return ActiveRecordInterface[]|CardOperation[]
     */
    private function getOperations(): array
    {
        return $this->repository
            ->getQuery()
            ->andWhere(['not', ['contractor_id' => null]])
            ->groupBy(['contractor_id'])
            ->all();
    }
}
