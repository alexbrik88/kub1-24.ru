<?php

namespace common\modules\cards\models;

use common\components\date\DateHelper;
use common\components\date\DatePeriodTrait;
use common\db\SortBuilder;
use common\models\FilterInterface;
use common\models\Company;
use common\models\Contractor;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\RepositoryCompanyTrait;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\DataProviderInterface;
use yii\data\Sort;
use yii\db\ActiveQuery;
use yii\db\ActiveQueryInterface;
use yii\db\ActiveRecordInterface;

/**
 * @property-write CardAccount|null $account
 */
class CardOperationRepository extends Model implements FilterInterface
{
    use RepositoryCompanyTrait;
    use DatePeriodTrait;

    /**
     * @var string
     */
    public string $contractor_id = '';

    /**
     * @var string
     */
    public string $bill_name = '';

    /**
     * @var string
     */
    public string $flow_type = '';

    /**
     * @var string
     */
    public string $reason = '';

    /**
     * @var string
     */
    public string $search = '';

    /**
     * @var string
     */
    public string $is_deleted = '0';

    /**
     * @var string
     */
    public string $project_id = '';

    /**
     * @var string
     */
    public string $sale_point_id = '';

    /**
     * @var CardAccount|null
     */
    private ?CardAccount $account = null;

    /**
     * @var ContractorList|null
     */
    private ?ContractorList $_contractorList = null;

    /**
     * @var ReasonList|null
     */
    private ?ReasonList $_reasonList = null;

    /**
     * @param int $operationId
     * @param Company|null $company
     * @return ActiveRecordInterface|CardOperation|null
     */
    public static function findOperationById(int $operationId, ?Company $company): ?CardOperation
    {
        $repository = new static([
            'company' => $company,
        ]);

        return $repository->getQuery()->andWhere(['operation.id' => $operationId])->one();
    }

    /**
     * @param int[] $id
     * @param Company|null $company
     * @return int
     */
    public static function deleteOperation(array $id, ?Company $company): int
    {
        $condition = ['id' => $id];

        if ($company) {
            $condition['company_id'] = $company->id;
        }

        return CardOperation::deleteAll($condition);
    }

    /**
     * @param array $id
     * @param int $expenditureItemId
     * @param Company|null $company
     * @return int
     */
    public static function updateExpenditureItem(array $id, int $expenditureItemId, ?Company $company): int
    {
        $condition = [
            'id' => $id,
            'flow_type' => CardOperation::FLOW_TYPE_EXPENSE,
        ];

        if ($company) {
            $condition['company_id'] = $company->id;
        }

        return CardOperation::updateAll(['expenditure_item_id' => $expenditureItemId], $condition);
    }

    /**
     * @param array $id
     * @param int $incomeItemId
     * @param Company|null $company
     * @return int
     */
    public static function updateIncomeItem(array $id, int $incomeItemId, ?Company $company): int
    {
        $condition = [
            'id' => $id,
            'flow_type' => CardOperation::FLOW_TYPE_INCOME,
        ];

        if ($company) {
            $condition['company_id'] = $company->id;
        }

        return CardOperation::updateAll(['income_item_id' => $incomeItemId], $condition);
    }

    /**
     * @return DataProviderInterface
     */
    public function getDataProvider(): DataProviderInterface
    {
        return new ActiveDataProvider([
            'query' => $this->getQuery(),
            'sort' => $this->getSort(),
        ]);
    }

    /**
     * @return ActiveQueryInterface|ActiveQuery
     */
    public function getQuery(): ActiveQueryInterface
    {
        $query = CardOperation::find()
            ->alias('operation')
            ->joinWith([
                'billRelation',
                'expenditureItemRelation',
                'incomeItemRelation',
                'contractorRelation',
            ])
            ->andFilterWhere([
                'operation.company_id' => $this->company ? $this->company->id : null,
                'operation.account_id' => $this->account ? $this->account->id : null,
                'operation.flow_type' => $this->flow_type,
                'operation.contractor_id' => $this->contractor_id,
                'operation.project_id' => $this->project_id,
                'operation.sale_point_id' => $this->sale_point_id,
                'operation.is_deleted' => $this->is_deleted,
            ])->andFilterWhere([
                CardBill::tableName() . '.name' => $this->bill_name,
            ]);

        if ($this->reason) {
            $query->andWhere(['or',
                [
                    'operation.flow_type' => CardOperation::FLOW_TYPE_INCOME,
                    InvoiceIncomeItem::tableName() . '.name' => $this->reason,
                ],
                [
                    'operation.flow_type' => CardOperation::FLOW_TYPE_EXPENSE,
                    InvoiceExpenditureItem::tableName() . '.name' => $this->reason,
                ],
            ]);
        }

        if (!empty($this->search)) {
            $query->andWhere(['like', Contractor::tableName() . '.name', $this->search]);
        }

        if ($this->hasPeriod()) {
            $query->andWhere([
                'between',
                'operation.date',
                $this->getDateFrom()->format(DateHelper::FORMAT_DATETIME),
                $this->getDateTo()->format(DateHelper::FORMAT_DATETIME),
            ]);
        }

        return $query;
    }

    /**
     * @inheritDoc
     */
    public function getSort(): Sort
    {
//        $builder = new SortBuilder();
//        $builder->setAttributes('operation', 'id', [
//            'id' => 'id',
//            'date' => 'date',
//            'description' => 'description',
//            'amount_income' => 'amount',
//            'amount_expense' => 'amount',
//        ]);
//        $builder->setDefaultSort('date', 'operation');
//
//        return $builder->buildSort();

        return new Sort([
            'attributes' => [
                'date',
                'amount_income_expense' => [
                    'asc' => [
                        'flow_type' => SORT_ASC,
                        'amount' => SORT_ASC,
                    ],
                    'desc' => [
                        'flow_type' => SORT_DESC,
                        'amount' => SORT_DESC,
                    ],
                ],
                'amount_income' => [
                    'asc' => [
                        'flow_type' => SORT_DESC, // Приход имеет flow_type = 1, поэтому сортируем по DESC
                        'amount' => SORT_ASC,
                    ],
                    'desc' => [
                        'flow_type' => SORT_DESC, // Приход имеет flow_type = 1, поэтому сортируем по DESC
                        'amount' => SORT_DESC,
                    ],
                ],
                'amount_expense' => [
                    'asc' => [
                        'flow_type' => SORT_ASC, // Расход имеет flow_type = 0, поэтому сортируем по ASC
                        'amount' => SORT_ASC,
                    ],
                    'desc' => [
                        'flow_type' => SORT_ASC, // Расход имеет flow_type = 0, поэтому сортируем по ASC
                        'amount' => SORT_DESC,
                    ],
                ],
                'contractor_id',
                'description',
                'created_at',
            ],
            'defaultOrder' => [
                'date' => SORT_DESC,
                'created_at' => SORT_DESC,
            ],
        ]);

    }

    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return [
            [['contractor_id'], 'string'],
            [['project_id'], 'string'],
            [['flow_type'], 'integer'],
            [['bill_name'], 'string'],
            [['reason'], 'string'],
            [['search'], 'string', 'max' => 45],
            [['is_deleted'], 'boolean'],
        ];
    }

    /**
     * @param ?CardAccount $account
     */
    public function setAccount(?CardAccount $account): void
    {
        $this->account = $account;
    }

    /**
     * @return CardOperationSummary
     */
    public function getSummary(): CardOperationSummary
    {
        $startBalance = $this->getStartBalance();
        $totalIncome = $this->getQuery()
            ->andWhere(['operation.flow_type' => CardOperation::FLOW_TYPE_INCOME])
            ->sum('operation.amount') ?: 0;
        $totalExpense = $this->getQuery()
            ->andWhere(['operation.flow_type' => CardOperation::FLOW_TYPE_EXPENSE])
            ->sum('operation.amount') ?: 0;
        $endBalance = $startBalance + $totalIncome - $totalExpense;

        return new CardOperationSummary(
            $startBalance,
            $endBalance,
            $totalIncome,
            $totalExpense,
            $this->getQuery()
                ->andWhere(['operation.flow_type' => CardOperation::FLOW_TYPE_INCOME])
                ->count('operation.id') ?: 0,
            $this->getQuery()
                ->andWhere(['operation.flow_type' => CardOperation::FLOW_TYPE_EXPENSE])
                ->count('operation.id') ?: 0
        );
    }

    /**
     * @return int
     */
    private function getStartBalance(): int
    {
        if (!$this->hasPeriod()) {
            return 0;
        }

        $repository = clone $this;
        $repository->setDateFrom(null);
        $repository->setDateTo(null);
        $totalIncome = $repository
            ->getQuery()
            ->andWhere(['<', 'date', $this->getDateFrom()->format(DateHelper::FORMAT_DATETIME)])
            ->andWhere(['operation.flow_type' => CardOperation::FLOW_TYPE_INCOME])
            ->sum('operation.amount') ?: 0;
        $totalExpense = $repository
            ->getQuery()
            ->andWhere(['<', 'date', $this->getDateFrom()->format(DateHelper::FORMAT_DATETIME)])
            ->andWhere(['operation.flow_type' => CardOperation::FLOW_TYPE_EXPENSE])
            ->sum('operation.amount') ?: 0;

        return ($totalIncome - $totalExpense);
    }
}
