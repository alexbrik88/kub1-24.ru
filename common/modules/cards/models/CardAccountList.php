<?php

namespace common\modules\cards\models;

use common\models\Company;
use common\models\ListBuilder;
use common\models\ListInterface;

class CardAccountList implements ListInterface
{
    /**
     * @var Company
     */
    private Company $company;

    /**
     * @var string[]|null
     */
    private ?array $_items = null;

    /**
     * @param Company $company
     */
    public function __construct(Company $company)
    {
        $this->company = $company;
    }

    /**
     * @inheritDoc
     */
    public function getItems(): array
    {
        if ($this->_items === null) {
            $repository = new CardAccountRepository(['company' => $this->company]);
            $builder = new ListBuilder();
            $builder->setIndexBy('id');
            $builder->setSort($repository->getSort());
            $this->_items = $builder->buildItems($repository->getDataProvider(), 'fullName');
        }

        return $this->_items;
    }
}
