<?php

namespace common\modules\cards\models;

use Yii;
use common\components\TextHelper;
use common\models\currency\Currency;
use common\components\date\DateHelper;
use common\models\employee\EmployeeRole;
use common\models\document\InvoiceIncomeItem;
use frontend\modules\cash\models\CashContractorType;

class CardBillForm extends CardBill
{
    public $saved = false;
    public $createStartBalance = false;
    public $startBalanceAmount;
    public $startBalanceDate;

    /**
     * roles that all Cards can see
     * @var array
     */
    public static $rolesViewAll = [
        EmployeeRole::ROLE_CHIEF,
        EmployeeRole::ROLE_FOUNDER,
        EmployeeRole::ROLE_FINANCE_DIRECTOR
    ];

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'trim'],
            [['currency_id'], 'required'],
            [['accessible',], 'safe'],
            [['startBalanceDate'], 'date', 'format' => 'd.M.yyyy'],
            [
                [
                    'startBalanceAmount',
                    'startBalanceDate',
                ],
                'required',
                'when' => function (CardBillForm $model) {
                    return $model->createStartBalance;
                },
                'message' => 'Необходимо заполнить',
            ],
            [['is_accounting'], 'filter', 'filter' => function($value) {
                return (int) $value;
            }],
            [['startBalanceAmount'],
                'number',
                'numberPattern' => Yii::$app->params['numberPattern'],
                'max' => Yii::$app->params['maxCashSum'] * 100, // with kopeck
                'tooBig' => 'Значение «{attribute}» не должно превышать ' . TextHelper::moneyFormat(Yii::$app->params['maxCashSum'], 2) . '.',
                'whenClient' => 'function(){}', // because of `123,00` - not valid float number (with comma) and there doesn't exist any methods to parse it on client.
            ],
            [['is_accounting'], 'integer'],
            [['createStartBalance'], 'safe'],
            [['name'], 'string', 'max' => 50],
            [
                ['currency_id'], 'exist',
                'targetClass' => \common\models\currency\Currency::class,
                'targetAttribute' => 'id',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'is_accounting' => 'Для учета в бухгалтерии',
            'name' => 'Карта (название)',
            'accessible' => 'Доступна',
            'createStartBalance' => 'Указать начальный остаток',
            'startBalanceAmount' => 'Сумма начального остатка',
            'startBalanceDate' => 'на дату',
            'currency_id' => 'Валюта',
            'currency_name' => 'Валюта',
        ];
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (parent::beforeSave($insert)) {
            if ($this->isAttributeChanged('currency_id')) {
                $this->currency_name = Currency::find()->select('name')->where([
                    'id' => $this->currency_id,
                ])->scalar();
            }
        }
    }

    /**
     * @param bool $insert
     * @param array $changedAttributes
     */
    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);

        $this->saved = true;

        if (isset($changedAttributes['is_accounting'])) {
            $condition = [
                'and',
                ['bill_id' => $this->id],
                ['company_id' => $this->account->company_id]

            ];
            CardOperation::updateAll(['is_accounting' => $this->is_accounting], $condition);
        }

        $startBalanceOperation = CardOperation::find()
            ->where(['bill_id' => $this->id, 'contractor_id' => CashContractorType::BALANCE_TEXT])
            ->one();

        if ($this->createStartBalance) {
            $model = $startBalanceOperation ?: (new CardOperation());
            $model->company_id = $this->account->company_id;
            $model->account_id = $this->account_id;
            $model->bill_id = $this->id;
            $model->date = DateHelper::format($this->startBalanceDate, 'Y-m-d', 'd.m.Y');
            $model->recognition_date = $model->date;
            $model->amount = round(100 * $this->startBalanceAmount);
            $model->flow_type = CardOperation::FLOW_TYPE_INCOME;
            $model->is_accounting = $this->is_accounting;
            $model->contractor_id = CashContractorType::BALANCE_TEXT;
            $model->income_item_id = InvoiceIncomeItem::ITEM_STARTING_BALANCE;
            $model->description = 'Баланс начальный';
            $model->uuid = 'balance-'.substr(md5(mt_rand()), 0, 16);
            $model->time = '00:00:00';
            if (!$model->save()) {
                var_dump($model->getErrors());
                exit;
                // \common\components\helpers\ModelHelper::logErrors($model, __METHOD__);
            }
        } elseif ($startBalanceOperation) {
            $startBalanceOperation->delete();
        }
    }

    public function setAccessible($value)
    {
        // todo
    }

    public function getAccessible()
    {
        return 'all'; // todo
    }
}
