<?php

namespace common\modules\cards\models;

use Exception;

class AccountAlreadyUsedException extends Exception
{
    public function __construct()
    {
        parent::__construct('Аккаунт уже подключен к другой компании.');
    }
}
