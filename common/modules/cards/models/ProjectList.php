<?php

namespace common\modules\cards\models;

use common\models\Company;
use common\models\ListInterface;
use frontend\modules\cash\models\CashContractorType;
use yii\db\ActiveRecordInterface;

class ProjectList implements ListInterface
{
    /**
     * @var array|null
     */
    private ?array $_items = null;

    /**
     * @var CardOperationRepository
     */
    private CardOperationRepository $repository;

    /**
     * @param Company|null $company
     * @param CardAccount|null $account
     */
    public function __construct(?Company $company, ?CardAccount $account)
    {
        $this->repository = new CardOperationRepository([
            'company' => $company,
            'account' => $account,
        ]);
    }

    /**
     * @inheritDoc
     */
    public function getItems(): array
    {
        if ($this->_items === null) {
            $this->_items = [];

            foreach ($this->getOperations() as $operation) {
                if ($operation->project) {
                    $this->_items[$operation->project_id] = $operation->project->name;
                }
            }
        }

        return $this->_items;
    }

    /**
     * @return ActiveRecordInterface[]|CardOperation[]
     */
    private function getOperations(): array
    {
        return $this->repository
            ->getQuery()
            ->andWhere(['not', ['project_id' => null]])
            ->groupBy(['project_id'])
            ->all();
    }
}
