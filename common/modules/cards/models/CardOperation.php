<?php

namespace common\modules\cards\models;
use common\components\cash\behaviors\CalculationBehavior;
use common\components\date\DateHelper;
use common\components\TextHelper;
use common\db\ExpressionFactory;
use common\models\ContractorRelationTrait;
use common\models\company\CompanyRelationTrait;
use common\models\currency\Currency;
use common\models\currency\CurrencyRate;
use common\models\document\ExpenditureItemRelationTrait;
use common\models\document\IncomeItemRelationTrait;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\models\document\InvoiceReasonInterface;
use common\models\project\Project;
use common\modules\cards\query\CardOperationQuery;
use frontend\components\StatisticPeriod;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * @property-read int $id
 * @property int $account_id
 * @property int $bill_id
 * @property int $company_id
 * @property string $contractor_id
 * @property int|null $expenditure_item_id
 * @property int|null $income_item_id
 * @property int $flow_type
 * @property int $is_deleted
 * @property int $amount
 * @property string $operation_uuid
 * @property string $description
 * @property string|null $recognition_date
 * @property string $date
 * @property string $time
 * @property int $is_accounting
 * @property int|null $project_id
 * @property-read string $created_at
 * @property-read string $updated_at
 *
 * @property CalculationBehavior $Calculation
 * @mixin CalculationBehavior
 */
class CardOperation extends ActiveRecord
{
    use CompanyRelationTrait;
    use ContractorRelationTrait;
    use ExpenditureItemRelationTrait;
    use IncomeItemRelationTrait;

    /** @var int */
    public const FLOW_TYPE_EXPENSE = 0;

    /** @var int */
    public const FLOW_TYPE_INCOME = 1;

    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'value' => (new ExpressionFactory)->createNowExpression(),
            ],
            'Calculation' => [
                'class' => CalculationBehavior::class,
                'beginAt' => \DateTime::createFromFormat(DateHelper::FORMAT_DATE, StatisticPeriod::getSessionPeriod()['from']),
                'endAt' => \DateTime::createFromFormat(DateHelper::FORMAT_DATE, StatisticPeriod::getSessionPeriod()['to']),
            ],
        ];
    }

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['project_id'], 'filter', 'filter' => function ($value) {
                return (int)$value ?: null;
            }],
        ];
    }

    /**
     * @return string
     */
    public function getAmountText(): string
    {
        return TextHelper::invoiceMoneyFormat($this->amount, 2);
    }

    /**
     * @return int|string
     */
    public function getAmountIncome()
    {
        return $this->flow_type == self::FLOW_TYPE_INCOME ? $this->amount : '-';
    }

    /**
     * @return int|string
     */
    public function getAmountExpense()
    {
        return $this->flow_type == self::FLOW_TYPE_EXPENSE ? $this->amount : '-';
    }

    /**
     * @return ActiveQuery
     */
    public function getBillRelation(): ActiveQuery
    {
        return $this->hasOne(CardBill::class, ['id' => 'bill_id']);
    }

    /**
     * @return CardBill
     */
    public function getBill(): CardBill
    {
        return $this->__get('billRelation');
    }

    /**
     * @return ActiveQuery
     */
    public function getProject(): ActiveQuery
    {
        return $this->hasOne(Project::class, ['id' => 'project_id']);
    }

    /**
     * @return float|null
     */
    public function getIncomeAmount(): ?float
    {
        if (($this->flow_type == self::FLOW_TYPE_INCOME)) {
            return $this->amount / 100;
        }

        return null;
    }

    /**
     * @return float|null
     */
    public function getExpenseAmount(): ?float
    {
        if (($this->flow_type == self::FLOW_TYPE_EXPENSE)) {
            return $this->amount / 100;
        }

        return null;
    }

    /**
     * @return int|null
     */
    public function getReasonId(): ?int
    {
        if ($this->flow_type == self::FLOW_TYPE_INCOME && $this->hasIncomeItem()) {
            return $this->incomeItem->id;
        }

        if ($this->flow_type == self::FLOW_TYPE_EXPENSE && $this->hasExpenditureItem()) {
            return $this->expenditureItem->id;
        }

        return null;
    }

    /**
     * @return InvoiceReasonInterface|null
     */
    public function getReasonItem(): ?InvoiceReasonInterface
    {
        if ($this->flow_type == self::FLOW_TYPE_INCOME && $this->hasIncomeItem()) {
            return $this->incomeItem;
        }

        if ($this->flow_type == self::FLOW_TYPE_EXPENSE && $this->hasExpenditureItem()) {
            return $this->expenditureItem;
        }

        return null;
    }

    /**
     * @inheritDoc
     */
    public static function tableName(): string
    {
        return 'card_operation';
    }

    /**
     * Gets query for [[ExpenditureItem]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getExpenditureItem()
    {
        return $this->hasOne(InvoiceExpenditureItem::class, ['id' => 'expenditure_item_id']);
    }

    /**
     * Gets query for [[IncomeItem]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIncomeItem()
    {
        return $this->hasOne(InvoiceIncomeItem::class, ['id' => 'income_item_id']);
    }

    public function getStatisticQuery(): CardOperationQuery
    {
        $query = new CardOperationQuery(static::class);
        $query->andFilterWhere([
            'company_id' => $this->company_id,
            'bill_id' => $this->bill_id,
        ]);

        return $query;
    }

    /**
     * @param bool $insert
     * @return bool
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        if ($this->currency_name == Currency::DEFAULT_NAME) {
            $this->amount_rub = $this->amount;
        } else {
            if ($this->isAttributeChanged('amount', false) ||
                $this->isAttributeChanged('date') ||
                $this->isAttributeChanged('currency_name')
            ) {
                if (($date = date_create($this->date)) && ($rate = CurrencyRate::getRateOnDate($date)[$this->currency_name] ?? null)) {
                    $this->amount_rub = round($this->amount/$rate['amount']*$rate['value']);
                } else {
                    $this->amount_rub = 0;
                }
            }
        }

        return true;
    }
}
