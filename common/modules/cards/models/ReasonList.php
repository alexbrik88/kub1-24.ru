<?php

namespace common\modules\cards\models;

use common\models\Company;
use common\models\ListInterface;
use yii\db\ActiveRecordInterface;

class ReasonList implements ListInterface
{
    /**
     * @var array|null
     */
    private ?array $_items = null;

    /**
     * @var CardOperationRepository
     */
    private CardOperationRepository $repository;

    /**
     * @param Company|null $company
     * @param CardAccount|null $account
     */
    public function __construct(?Company $company, ?CardAccount $account)
    {
        $this->repository = new CardOperationRepository([
            'company' => $company,
            'account' => $account,
        ]);
    }

    /**
     * @inheritDoc
     */
    public function getItems(): array
    {
        if ($this->_items === null) {
            $this->_items = [];

            foreach ($this->getOperations() as $operation) {
                $item = $operation->getReasonItem();

                if ($item) {
                    $this->_items[$item->getReasonName()] = $item->getReasonName();
                }
            }
        }

        return $this->_items;
    }

    /**
     * @return ActiveRecordInterface[]|CardOperation[]
     */
    private function getOperations(): array
    {
        return $this->repository
            ->getQuery()
            ->groupBy(['expenditure_item_id', 'income_item_id'])
            ->all();
    }
}
