<?php

namespace common\modules\cards\models;

use common\db\ExpressionFactory;
use common\models\FilterInterface;
use common\models\Company;
use common\models\RepositoryCompanyTrait;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\DataProviderInterface;
use yii\data\Sort;
use yii\db\ActiveQueryInterface;
use yii\db\ActiveRecordInterface;

class ZenmoneyIdentityRepository extends Model implements FilterInterface
{
    use RepositoryCompanyTrait;

    /**
     * @var int|null
     */
    public $account_id;

    /**
     * @param int $accountId
     * @param Company|null $company
     * @return ActiveRecordInterface|ZenmoneyIdentity|null
     */
    public static function findByAccountId(int $accountId, Company $company = null): ?ZenmoneyIdentity
    {
        $repository = new static(['company' => $company, 'account_id' => $accountId]);

        return $repository->getQuery()->one();
    }

    /**
     * @param int $accountId
     * @param Company $company
     * @return bool
     * @throws
     */
    public static function removeByAccountId(int $accountId, Company $company): bool
    {
        $model = static::findByAccountId($accountId, $company);

        if ($model && !$model->delete()) {
            return false;
        }

        return true;
    }

    /**
     * @param Company $company
     * @param int $accountId
     * @param string $accessToken
     * @param int $expiresIn
     * @return ZenmoneyIdentity
     * @throws AccountAlreadyUsedException
     */
    public static function createZenmoneyIdentity(
        Company $company,
        int $accountId,
        string $accessToken,
        int $expiresIn
    ): ZenmoneyIdentity {
        $identity = static::findByAccountId($accountId);

        if ($identity && $identity->company_id !== $company->id) {
            throw new AccountAlreadyUsedException();
        }

        if ($identity === null) {
            $identity = new ZenmoneyIdentity();
        }

        $expiresIn /= 10; // Это костыль
        $identity->setAttributes([
            'company_id' => $company->id,
            'account_id' => $accountId,
            'access_token' => $accessToken,
            'expired_at' => (new ExpressionFactory)->createExpiredAtExpression($expiresIn),
        ], false);

        return $identity;
    }

    /**
     * @inheritDoc
     */
    public function getDataProvider(): DataProviderInterface
    {
        return new ActiveDataProvider([
            'query' => $this->getQuery(),
            'sort' => $this->getSort(),
            'pagination' => false,
        ]);
    }

    /**
     * @return ActiveQueryInterface
     */
    public function getQuery(): ActiveQueryInterface
    {
        return ZenmoneyIdentity::find()->with(['companyRelation'])->andFilterWhere([
            'company_id' => $this->company ? $this->company->id : null,
            'account_id' => $this->account_id,
        ]);
    }

    /**
     * @inheritDoc
     */
    public function getSort(): Sort
    {
        return new Sort(['defaultOrder' => ['account_id' => SORT_ASC]]);
    }
}
