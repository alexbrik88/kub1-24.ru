<?php

namespace common\modules\cards\models;

use common\models\currency\Currency;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * @property-read int $id
 * @property string $currency_id
 * @property string $currency_name
 * @property int $account_id
 * @property string $uuid
 * @property string $name
 * @property string $number
 * @property integer $is_accounting
 *
 * @property CardAccount $account
 * @property CardOperation[] $operations
 */
class CardBill extends ActiveRecord
{
    const DEFAULT_CURRENCY_ID = '643';
    const DEFAULT_CURRENCY_NAME = 'RUB';

    /**
     * @return CardAccount
     */
    public function getAccount(): CardAccount
    {
        return $this->__get('accountRelation');
    }

    /**
     * @return ActiveQuery
     */
    public function getAccountRelation(): ActiveQuery
    {
        return $this->hasOne(CardAccount::class, ['id' => 'account_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getCurrency(): ActiveQuery
    {
        return $this->hasOne(Currency::class, ['id' => 'currency_id']);
    }

    /**
     * @return ActiveQuery
     */
    public function getOperations(): ActiveQuery
    {
        return $this->hasMany(CardOperation::class, ['bill_id' => 'id']);
    }

        /**
     * @return string
     */
    public function getFullName(): string
    {
        if (empty($this->number)) {
            return $this->name;
        }

        return sprintf('%s (%s)', $this->name, $this->number);
    }

    /**
     * @inheritDoc
     */
    public static function tableName(): string
    {
        return '{{%card_bill}}';
    }

    /**
     * @return bool
     */
    public function hasMovement()
    {
        return $this->getOperations()->exists();
    }
}
