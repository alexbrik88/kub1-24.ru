<?php

namespace common\modules\cards\models;

use common\models\FilterInterface;
use common\models\Company;
use common\models\RepositoryCompanyTrait;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\DataProviderInterface;
use yii\data\Sort;
use yii\db\ActiveQueryInterface;
use yii\db\ActiveRecordInterface;

class CardAccountRepository extends Model implements FilterInterface
{
    use RepositoryCompanyTrait;

    /**
     * @var int|null
     */
    public $identifier;

    /**
     * @var string|null
     */
    public $account_type;

    /**
     * @var bool|null
     */
    public $is_active;

    /**
     * @param CardIdentityInterface $identity
     * @return CardAccount
     */
    public static function createAccount(CardIdentityInterface $identity): CardAccount
    {
        return new CardAccount([
            'company_id' => $identity->getCompany()->id,
            'identifier' => $identity->getCardIdentifier(),
            'account_type' => $identity->getCardAccountType(),
            'is_active' => true,
        ]);
    }

    /**
     * @param CardIdentityInterface $identity
     * @return ActiveRecordInterface|CardAccount|null
     */
    public static function findAccount(CardIdentityInterface $identity): ?CardAccount
    {
        $repository = new static([
            'company' => $identity->getCompany(),
            'account_type' => $identity->getCardAccountType(),
            'identifier' => $identity->getCardIdentifier(),
        ]);

        return $repository->getQuery()->one();
    }

    /**
     * @param Company $company
     * @return CardAccount[]
     */
    public static function findAccounts(Company $company): array
    {
        $repository = new static(['company' => $company]);

        return $repository->getQuery()->all();
    }

    /**
     * @param Company $company
     * @param int $accountType
     * @return CardAccount[]
     */
    public static function findActiveAccounts(Company $company, int $accountType): array
    {
        $repository = new static([
            'company' => $company,
            'account_type' => $accountType,
            'is_active' => true,
        ]);

        return $repository->getQuery()->all();
    }

    /**
     * @param int $accountId
     * @param Company|null $company
     * @return ActiveRecordInterface|CardAccount|null
     */
    public static function findAccountById(int $accountId, ?Company $company): ?CardAccount
    {
        $repository = new static(['company' => $company]);

        return $repository->getQuery()->andWhere(['id' => $accountId])->one();
    }

    /**
     * @inheritDoc
     */
    public function getDataProvider(): DataProviderInterface
    {
        return new ActiveDataProvider([
            'query' => $this->getQuery(),
            'sort' => $this->getSort(),
            'pagination' => false,
        ]);
    }

    /**
     * @return ActiveQueryInterface
     */
    public function getQuery(): ActiveQueryInterface
    {
        return CardAccount::find()
            ->with(['companyRelation'])
            ->andFilterWhere([
                'company_id' => $this->company ? $this->company->id : null,
                'account_type' => $this->account_type,
                'identifier' => $this->identifier,
                'is_active' => $this->is_active,
            ]);
    }

    /**
     * @inheritDoc
     */
    public function getSort(): Sort
    {
        return new Sort(['defaultOrder' => ['id' => SORT_ASC]]);
    }
}
