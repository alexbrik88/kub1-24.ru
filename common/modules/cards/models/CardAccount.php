<?php

namespace common\modules\cards\models;

use common\db\ExpressionFactory;
use common\models\company\CompanyRelationTrait;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * @property-read int $id
 * @property int $company_id
 * @property int $account_type
 * @property string $identifier
 * @property bool $is_active
 * @property-read $created_at
 * @property-read $updated_at

 * @property string $fullName
 */
class CardAccount extends ActiveRecord
{
    use CompanyRelationTrait;

    /** @var int */
    public const ACCOUNT_TYPE_ZENMONEY = 1;

    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'value' => (new ExpressionFactory)->createNowExpression(),
            ],
        ];
    }

    /**
     * @return string[]
     */
    public static function getAccountTypes(): array
    {
        return [
            self::ACCOUNT_TYPE_ZENMONEY => 'Дзен мани',
        ];
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return sprintf('%s (%s)', static::getAccountTypes()[$this->account_type], $this->identifier);
    }

    /**
     * @inheritDoc
     */
    public static function tableName(): string
    {
        return '{{%card_account}}';
    }
}
