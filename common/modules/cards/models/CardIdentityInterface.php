<?php

namespace common\modules\cards\models;

use common\models\Company;
use yii\db\ActiveRecordInterface;

interface CardIdentityInterface extends ActiveRecordInterface
{
    /**
     * @return string
     */
    public function getCardIdentifier(): string;

    /**
     * @return Company
     */
    public function getCompany(): Company;

    /**
     * @return int
     */
    public function getCardAccountType(): int;
}
