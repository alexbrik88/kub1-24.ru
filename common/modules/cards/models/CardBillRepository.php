<?php

namespace common\modules\cards\models;

use common\models\FilterInterface;
use common\models\RepositoryCompanyTrait;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\DataProviderInterface;
use yii\data\Sort;
use yii\db\ActiveQueryInterface;
use yii\db\ActiveRecordInterface;

class CardBillRepository extends Model implements FilterInterface
{
    use RepositoryCompanyTrait;

    /**
     * @var CardAccount|null
     */
    private ?CardAccount $account = null;

    /**
     * @param CardAccount $account
     * @param string $uuid
     * @param string $name
     * @param string $number
     * @return CardBill
     */
    public static function createCardBill(CardAccount $account, string $uuid, string $name, string $number): CardBill
    {
        return new CardBill([
            'account_id' => $account->id,
            'uuid' => $uuid,
            'name' => $name,
            'number' => $number,
            'currency_id' => CardBill::DEFAULT_CURRENCY_ID,
            'currency_name' => CardBill::DEFAULT_CURRENCY_NAME
        ]);
    }

    /**
     * @param string $uuid
     * @param CardAccount $account
     * @return ActiveRecordInterface|CardBill|null
     */
    public static function findByUuid(string $uuid, CardAccount $account): ?CardBill
    {
        $repository = new static([
            'account' => $account,
        ]);

        return $repository->getQuery()->andWhere(['uuid' => $uuid])->one();
    }

    /**
     * @inheritDoc
     */
    public function getDataProvider(): DataProviderInterface
    {
        return new ActiveDataProvider([
            'pagination' => false,
            'query' => $this->getQuery(),
            'sort' => $this->getSort(),
        ]);
    }

    /**
     * @return ActiveQueryInterface
     */
    public function getQuery(): ActiveQueryInterface
    {
        return CardBill::find()
            ->alias('bill')
            ->joinWith(['accountRelation'])
            ->andFilterWhere([
                CardAccount::tableName() . '.id' => $this->account ? $this->account->id : null,
                CardAccount::tableName() . '.company_id' => $this->company ? $this->company->id : null,
            ]);
    }

    /**
     * @inheritDoc
     */
    public function getSort(): Sort
    {
        return new Sort(['defaultOrder' => ['name' => SORT_ASC]]);
    }

    /**
     * @param CardAccount|null $account
     * @return void
     */
    public function setAccount(?CardAccount $account): void
    {
        $this->account = $account;
    }
}
