<?php

namespace common\modules\cards\models;

use common\models\ListInterface;

class FlowTypeList implements ListInterface
{
    /**
     * @inheritDoc
     */
    public function getItems(): array
    {
        return [
            CardOperation::FLOW_TYPE_INCOME => 'Приход',
            CardOperation::FLOW_TYPE_EXPENSE => 'Расход',
        ];
    }
}
