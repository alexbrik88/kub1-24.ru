<?php

namespace common\modules\cards\models;

use common\models\Company;
use common\models\company\CompanyRelationTrait;
use common\modules\cards\behaviors\CardIdentityBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * @property-read int $account_id Идентификатор аккаунта
 * @property-read int $company_id Идентификатор компании
 * @property-read string $access_token Токен доступа
 * @property-read string $created_at Дата создания
 * @property-read string $updated_at Дата обновления
 * @property-read CardAccount $cardAccount
 */
class ZenmoneyIdentity extends ActiveRecord implements CardIdentityInterface
{
    use CompanyRelationTrait;

    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
            ],
            'cardIdentity' => [
                'class' => CardIdentityBehavior::class,
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public function getCardAccountType(): int
    {
        return CardAccount::ACCOUNT_TYPE_ZENMONEY;
    }

    /**
     * @inheritDoc
     */
    public function getCardIdentifier(): string
    {
        return $this->account_id;
    }

    /**
     * @inheritDoc
     */
    public static function tableName(): string
    {
        return '{{%zenmoney_identity}}';
    }
}
