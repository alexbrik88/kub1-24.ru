<?php

namespace common\modules\cards\models;

use common\models\Company;
use common\models\ListBuilder;
use common\models\ListInterface;

class CardIdentifierList implements ListInterface
{
    /**
     * @var Company
     */
    private Company $company;

    /**
     * @var int
     */
    private int $accountType;

    /**
     * @param Company $company
     * @param int $accountType
     */
    public function __construct(Company $company, int $accountType)
    {
        $this->company = $company;
        $this->accountType = $accountType;
    }

    /**
     * @inheritDoc
     */
    public function getItems(): array
    {
        $repository = new CardAccountRepository([
            'company' => $this->company,
            'account_type' => $this->accountType,
        ]);
        $builder = new ListBuilder();
        $builder->setIndexBy('identifier');
        $builder->setSort($repository->getSort());

        return $builder->buildItems($repository->getDataProvider(), 'identifier');
    }
}
