<?php

namespace common\modules\cards\components;

use common\components\ObjectFactoryTrait;
use common\modules\import\components\AuthComponentInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;
use yii\base\Component;
use yii\base\InvalidConfigException;

final class ZenmoneyComponent extends Component implements AuthComponentInterface
{
    use ObjectFactoryTrait;

    /** @var string */
    private const OAUTH_URL = 'https://api.zenmoney.ru/oauth2/authorize/';

    /** @var string */
    private const TOKEN_URL = 'https://api.zenmoney.ru/oauth2/token/';

    /**
     * @var string
     */
    public string $clientId = '';

    /**
     * @var string
     */
    public string $clientSecret = '';

    /**
     * @var string
     */
    public string $redirectUri = '';

    /**
     * @var Client
     */
    private Client $client;

    /**
     * @param array $config
     */
    public function __construct($config = [])
    {
        $this->client = new Client();

        parent::__construct($config);
    }

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init(): void
    {
        if (!isset($this->clientId) || !isset($this->clientSecret) || !isset($this->redirectUri)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @return string
     */
    public function getAuthorizationUrl(): string
    {
        $query = http_build_query([
            'response_type' => 'code',
            'client_id' => $this->clientId,
            'redirect_uri' => $this->redirectUri,
        ]);

        return sprintf('%s?%s', self::OAUTH_URL, $query);
    }

    /**
     * @param string $code
     * @return string[]
     * @throws ZenmoneyException
     */
    public function getCredentials(string $code): array
    {
        try {
            $response = $this->client->request('POST', self::TOKEN_URL, [
                'form_params' => [
                    'grant_type' => 'authorization_code',
                    'client_id' => $this->clientId,
                    'client_secret' => $this->clientSecret,
                    'code' => $code,
                    'redirect_uri' => $this->redirectUri,
                ],
            ]);

            return json_decode($response->getBody(), true);
        } catch (GuzzleException $exception) {
            throw new ZenmoneyException($exception->getMessage(), $exception->getCode(), $exception);
        }
    }
}
