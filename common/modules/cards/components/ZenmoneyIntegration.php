<?php

namespace common\modules\cards\components;

use common\components\helpers\ArrayHelper;
use DateTimeInterface;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

final class ZenmoneyIntegration
{
    /** @var string */
    private const API_URL = 'https://api.zenmoney.ru';

    /**
     * @var Client
     */
    private Client $client;

    /**
     * @var array|null
     */
    private ?array $_data = null;

    /**
     * @param string $token
     */
    public function __construct(string $token)
    {
        $this->client = new Client([
            'headers' => [
                'Authorization' => sprintf('Bearer %s',  $token),
            ],
        ]);
    }

    /**
     * @param string $uuid
     * @return string
     * @throws ZenmoneyException
     */
    public function getTagTitle(string $uuid): string
    {
        return $this->getData()['tags'][$uuid];
    }

    /**
     * @param string $uuid
     * @return string
     * @throws ZenmoneyException
     */
    public function getAccountTitle(string $uuid): string
    {
        return $this->getData()['accounts'][$uuid];
    }

    /**
     * @param string $uuid
     * @return string[]|null
     * @throws ZenmoneyException
     */
    public function getAccountNumbers(string $uuid): ?array
    {
        return $this->getData()['numbers'][$uuid];
    }

    /**
     * @param DateTimeInterface $dateFrom
     * @return array
     * @throws ZenmoneyException
     */
    public function getTransactions(DateTimeInterface $dateFrom): array
    {
        return $this->getDiff($dateFrom->getTimestamp())['transaction'] ?? [];
    }

    /**
     * @return array
     * @throws ZenmoneyException
     */
    public function getData(): array
    {
        if ($this->_data === null) {
            $diff = $this->getDiff();
            $this->_data = [
                'instrument' => ArrayHelper::map($diff['instrument'] ?? [], 'id', 'shortTitle'),
                'tags' => ArrayHelper::map($diff['tag'] ?? [], 'id', 'title'),
                'accounts' => ArrayHelper::map($diff['account'] ?? [], 'id', 'title'),
                'numbers' => ArrayHelper::map($diff['account'] ?? [], 'id', 'syncID'),
            ];
        }

        return $this->_data;
    }

    /**
     * @param int $timestamp
     * @return array
     * @throws ZenmoneyException
     */
    public function getDiff(int $timestamp = 0): array
    {
        try {
            $response = $this->client->request('POST', sprintf('%s%s', self::API_URL, '/v8/diff/'), [
                'json' => [
                    'currentClientTimestamp' => time(),
                    'serverTimestamp' => $timestamp,
                ],
            ]);

            return json_decode($response->getBody()->getContents(), true);
        } catch (GuzzleException $exception) {
            throw new ZenmoneyException($exception->getMessage(), $exception->getCode(), $exception);
        }
    }
}
