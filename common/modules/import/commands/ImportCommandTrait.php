<?php

namespace common\modules\import\commands;

trait ImportCommandTrait
{
    /**
     * @var int
     */
    protected $savedCount = 0;

    /**
     * @inheritDoc
     * @see ImportCommandInterface::getSavedCount()
     */
    public function getSavedCount(): int
    {
        return $this->savedCount;
    }
}
