<?php

namespace common\modules\import\commands;

use common\commands\CommandInterface;
use common\components\date\DateHelper;
use common\modules\import\components\ImportJobHandler;
use common\modules\import\factories\DateTimeFactory;
use common\modules\import\factories\ImportJobFactory;
use common\modules\import\models\AutoImportSettings;

class AutoImportCommand implements CommandInterface
{
    /**
     * @var DateTimeFactory
     */
    private DateTimeFactory $dateTimeFactory;

    /**
     *
     */
    public function __construct()
    {
        $this->dateTimeFactory = new DateTimeFactory();
    }

    /**
     * @inheritDoc
     */
    public function run(): void
    {
        $autoImportSettings = AutoImportSettings::find()->all();

        array_walk($autoImportSettings, [$this, 'createJob']);
    }

    /**
     * @param AutoImportSettings $autoImportSettings
     * @return void
     */
    private function createJob(AutoImportSettings $autoImportSettings): void
    {
        if (!$this->canCreate($autoImportSettings)) {
            return;
        }

        $importJob = (new ImportJobFactory)->createJob(
            $autoImportSettings->company->getEmployeeChief(),
            $autoImportSettings->command_type,
            $this->createJobParams($autoImportSettings),
            true
        );

        ImportJobHandler::getInstance()->dispatchJob($importJob);
    }

    /**
     * @param AutoImportSettings $autoImportSettings
     * @return bool
     * @throws
     */
    private function canCreate(AutoImportSettings $autoImportSettings): bool
    {
        if ($autoImportSettings->period_type == AutoImportSettings::PERIOD_TYPE_NEVER) {
            return false;
        }

        if ($autoImportSettings->period_type == AutoImportSettings::PERIOD_TYPE_EVERY_DAY) {
            return true;
        }

        $dayOfTheWeek = $this->dateTimeFactory->createDateTime()->format('N');

        if ($autoImportSettings->period_type == AutoImportSettings::PERIOD_TYPE_EVERY_WORKING_DAYS) {
            if (in_array($dayOfTheWeek, range(1, 5))) {
                return true;
            }
        }

        if ($autoImportSettings->period_type == AutoImportSettings::PERIOD_TYPE_EVERY_MONDAY_WEDNESDAY_FRIDAY) {
            if (in_array($dayOfTheWeek, [1, 3, 5])) {
                return true;
            }
        }

        return false;
    }

    /**
     * @param AutoImportSettings $autoImportSettings
     * @return string[]
     * @throws
     */
    private function createJobParams(AutoImportSettings $autoImportSettings): array
    {
        return [
            'from' => $this->dateTimeFactory->createDateFrom()->format(DateHelper::FORMAT_DATETIME),
            'to' => $this->dateTimeFactory->createDateTo()->format(DateHelper::FORMAT_DATETIME),
            'identifier' => $autoImportSettings->identifier,
        ];
    }
}
