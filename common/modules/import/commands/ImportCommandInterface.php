<?php

namespace common\modules\import\commands;

use common\commands\CommandInterface;
use common\modules\import\models\ImportJobData;

interface ImportCommandInterface extends CommandInterface
{
    /**
     * @param ImportJobData $jobData
     * @return static
     */
    public static function createImportCommand(ImportJobData $jobData): ImportCommandInterface;

    /**
     * @return string
     */
    public static function getImportCommandName(): string;

    /**
     * @return string
     */
    public static function getImportCommandLabel(): string;

    /**
     * @return int
     */
    public function getSavedCount(): int;
}
