<?php

namespace common\modules\import\models;

use common\models\employee\Employee;
use common\models\ListInterface;
use common\modules\import\components\ImportCommandType;

abstract class AbstractImportForm2 extends AbstractImportForm
{
    /**
     * @var string
     */
    public $identifier;

    /**
     * @var string[]
     */
    public $identifierList;

    /**
     * @inheritDoc
     */
    public function attributeLabels(): array
    {
        return [
            'identifier' => 'Лицевой счет',
            'dateFrom' => 'Начало периода',
            'dateTo' => 'Конец периода',
        ];
    }

    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return array_merge(parent::rules(), [
            [['identifier'], 'required'],
            [['identifier'], 'string'],
            [['identifier'], 'in', 'range' => array_keys($this->identifierList)],
        ]);
    }

    /**
     * @param Employee $employee
     * @param ImportCommandType $commandType
     * @param ListInterface $identifierList,
     * @param string $identifier
     */
    public function __construct(
        Employee $employee,
        ImportCommandType $commandType,
        ListInterface $identifierList,
        string $identifier
    ) {
        $this->identifier = $identifier;
        $this->identifierList = $identifierList->getItems();

        parent::__construct($employee, $commandType);
    }

    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    /**
     * @inheritDoc
     */
    protected function createJobParams(): array
    {
        $params = parent::createJobParams();
        $params['identifier'] = $this->identifier;

        return $params;
    }

    /**
     * @return AutoImportFormInterface
     */
    abstract public function createAutoImportForm(): AutoImportFormInterface;
}
