<?php

namespace common\modules\import\models;

use common\models\Company;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\Model;

abstract class AbstractTokenForm extends Model implements TokenFormInterface
{
    /**
     * @var string
     */
    public $code;

    /**
     * @var Company
     */
    private $company;

    /**
     * @param Company $company
     * @param array $config
     */
    public function __construct(Company $company, array $config = [])
    {
        $this->company = $company;

        parent::__construct($config);
    }

    /**
     * @inheritDoc
     * @return object|TokenFormInterface
     * @throws InvalidConfigException
     */
    public static function createTokenForm(Company $company): TokenFormInterface
    {
        return Yii::createObject(static::class, [$company]);
    }

    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return [
            [['code'], 'required'],
            [['code'], 'string'],
        ];
    }

    /**
     * @return Company
     */
    public function getCompany(): Company
    {
        return $this->company;
    }
}
