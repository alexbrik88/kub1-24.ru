<?php

namespace common\modules\import\models;

use common\models\FilterInterface;
use common\models\Company;
use common\modules\import\components\ImportCommandType;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\DataProviderInterface;
use yii\data\Sort;
use yii\db\ActiveQueryInterface;

class AutoImportSettingsRepository extends Model implements FilterInterface
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $identifier;

    /**
     * @var $period_type
     */
    public $period_type;

    /**
     * @var int
     */
    private $command_type;

    /**
     * @var int
     */
    private $company_id;

    /**
     * @param string $identifier
     * @param Company $company
     * @param ImportCommandType $commandType
     * @return AutoImportSettings|null
     */
    public static function findByIdentifier(
        string $identifier,
        Company $company,
        ImportCommandType $commandType
    ): ?AutoImportSettings {
        $repository = new static([
            'company' => $company,
            'commandType' => $commandType,
            'identifier' => $identifier,
        ]);

        /** @var AutoImportSettings $model */
        $model = $repository->getQuery()->one();

        return $model;
    }

    /**
     * @param string $identifier
     * @param Company $company
     * @param ImportCommandType $commandType
     * @param int $periodType
     * @return AutoImportSettings
     */
    public static function createAutoImportSettings(
        string $identifier,
        Company $company,
        ImportCommandType $commandType,
        int $periodType = AutoImportSettings::PERIOD_TYPE_NEVER
    ): AutoImportSettings {
        $model = static::findByIdentifier($identifier, $company, $commandType);

        if ($model === null) {
            $model = new AutoImportSettings([
                'company_id' => $company->id,
                'command_type' => $commandType->getCommandId(),
                'identifier' => $identifier,
                'period_type' => $periodType,
            ]);
        }

        return $model;
    }

    /**
     * @param string $identifier
     * @param Company $company
     * @param ImportCommandType $commandType
     * @return bool
     * @throws
     */
    public static function removeByIdentifier(
        string $identifier,
        Company $company,
        ImportCommandType $commandType
    ): bool {
        $autoImportSettings = static::findByIdentifier($identifier, $company, $commandType);

        if ($autoImportSettings && !$autoImportSettings->delete()) {
            return false;
        }

        return true;
    }

    /**
     * @inheritDoc
     */
    public function getDataProvider(): DataProviderInterface
    {
        return new ActiveDataProvider([
            'query' => $this->getQuery(),
            'sort' => $this->getSort(),
            'pagination' => false,
        ]);
    }

    /**
     * @return ActiveQueryInterface
     */
    public function getQuery(): ActiveQueryInterface
    {
        return AutoImportSettings::find()->andFilterWhere([
            'id' => $this->id,
            'company_id' => $this->company_id,
            'command_type' => $this->command_type,
            'identifier' => $this->identifier,
            'period_type' => $this->period_type,
        ]);
    }

    /**
     * @inheritDoc
     */
    public function getSort(): Sort
    {
        return new Sort(['defaultOrder' => ['id' => SORT_ASC]]);
    }

    /**
     * @param Company|null $company
     */
    public function setCompany(?Company $company): void
    {
        $this->company_id = $company ? $company->id : null;
    }

    /**
     * @param ImportCommandType|null $commandType
     * @return void
     */
    public function setCommandType(?ImportCommandType $commandType): void
    {
        $this->command_type = $commandType ? $commandType->getCommandId() : null;
    }
}
