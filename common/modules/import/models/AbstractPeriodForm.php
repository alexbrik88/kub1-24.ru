<?php

namespace common\modules\import\models;

use common\components\date\DateHelper;
use common\models\FormInterface;
use common\modules\import\factories\DateTimeFactory;
use yii\base\Model;

abstract class AbstractPeriodForm extends Model implements FormInterface
{
    /**
     * @var string
     */
    public string $dateFrom;

    /**
     * @var string
     */
    public string $dateTo;

    /**
     * @var DateTimeFactory
     */
    private $factory;

    /**
     *
     */
    public function __construct()
    {
        $this->factory = new DateTimeFactory();
        $this->dateFrom = $this->factory->createDateFrom()->format(DateHelper::FORMAT_USER_DATE);
        $this->dateTo = $this->factory->createDateTo()->format(DateHelper::FORMAT_USER_DATE);

        parent::__construct();
    }

    /**
     * @inheritDoc
     */
    public function attributeLabels(): array
    {
        return [
            'dateFrom' => 'Начало периода',
            'dateTo' => 'Конец периода',
        ];
    }

    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return [
            [['dateFrom', 'dateTo'], 'required', 'message' => 'Необходимо заполнить.'],
            [
                ['dateFrom'],
                'date',
                'format' => DateHelper::FORMAT_USER_DATE,
                'max' => $this->factory->createDateTime()->format(DateHelper::FORMAT_USER_DATE),
                'tooBig' => 'Дата не должна быть больше текущей даты.'
            ],
            [
                ['dateTo'],
                'date',
                'format' => DateHelper::FORMAT_USER_DATE,
                'max' => $this->dateFrom,
                'tooBig' => 'Дата окончания должна быть больше даты начала.',
            ],
        ];
    }
}
