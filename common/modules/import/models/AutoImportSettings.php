<?php

namespace common\modules\import\models;

use common\db\ExpressionFactory;
use common\models\company\CompanyRelationTrait;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

/**
 * @property-read int $id
 * @property int $company_id
 * @property int $command_type
 * @property int $period_type
 * @property string $identifier
 * @property-read string $created_at
 * @property-read string $updated_at
 */
class AutoImportSettings extends ActiveRecord
{
    use CompanyRelationTrait;

    public const PERIOD_TYPE_EVERY_DAY = 0;
    public const PERIOD_TYPE_EVERY_WORKING_DAYS = 1;
    public const PERIOD_TYPE_EVERY_MONDAY_WEDNESDAY_FRIDAY = 2;
    public const PERIOD_TYPE_NEVER = 3;

    /** @var string[] */
    public const PERIOD_TYPE_MAP = [
        self::PERIOD_TYPE_EVERY_WORKING_DAYS => 'Каждый пн, вт, ср, чт, пт',
        self::PERIOD_TYPE_EVERY_DAY => 'Каждый день',
        self::PERIOD_TYPE_EVERY_MONDAY_WEDNESDAY_FRIDAY => 'Каждый пн, ср, пт',
        self::PERIOD_TYPE_NEVER => 'Нет',
    ];

    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'value' => (new ExpressionFactory)->createNowExpression(),
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public static function tableName(): string
    {
        return '{{%auto_import_settings}}';
    }
}
