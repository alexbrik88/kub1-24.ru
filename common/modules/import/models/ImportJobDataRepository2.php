<?php

namespace common\modules\import\models;

use common\models\Company;
use common\models\employee\Employee;
use common\modules\import\components\ImportCommandType;
use yii\data\Sort;
use yii\db\ActiveQueryInterface;
use yii\db\ActiveRecordInterface;

class ImportJobDataRepository2
{
    /** @var int */
    private const AUTO_JOBS_LIMIT = 5;

    /** @var array */
    private const DEFAULT_ORDER = ['id' => SORT_DESC];

    /**
     * @var Company
     */
    private Company $company;

    /**
     * @param Company $company
     */
    public function __construct(Company $company)
    {
        $this->company = $company;
    }

    /**
     * @param int $id
     * @return ImportJobData|null
     */
    public function findById(int $id): ?ImportJobData
    {
        return ImportJobData::findOne(['id' => $id]);
    }

    /**
     * @inheritDoc
     */
    public function getQuery(array $params = []): ActiveQueryInterface
    {
        $query = ImportJobData::find()
            ->andWhere(['company_id' => $this->company->id])
            ->andFilterWhere([
                'employee_id' => $params['employee_id'] ?? null,
                'type' => $params['type'] ?? null,
                'is_auto' => $params['is_auto'] ?? null,
                'is_viewed' => $params['is_viewed'] ?? null,
            ]);

        if (array_key_exists('isFinished', $params)) {
            if ($params['isFinished'] > 0) {
                $query->andWhere(['NOT', ['finished_at' => null]]);
            } else {
                $query->andWhere(['finished_at' => null]);
            }
        }

        return $query;
    }

    /**
     * @inheritDoc
     */
    public function getSort(): Sort
    {
        return new Sort(['defaultOrder' => self::DEFAULT_ORDER]);
    }

    /**
     * @param ImportCommandType $commandType
     * @param bool|null $isAuto
     * @param bool|null $isFinished
     * @return ActiveRecordInterface|ImportJobData|null
     */
    public function findLastJobData(
        ImportCommandType $commandType,
        ?bool $isAuto = null,
        ?bool $isFinished = null
    ): ?ImportJobData {
        $params = [
            'type' => $commandType->getCommandId(),
            'is_auto' => $isAuto,
            'isFinished' => $isFinished,
        ];

        return $this->getQuery($params)->orderBy(self::DEFAULT_ORDER)->one();
    }

    /**
     * @param ImportCommandType $commandType
     * @param int $limit
     * @return ActiveRecordInterface[]|ImportJobData[]
     */
    public function findAutoJobsData(ImportCommandType $commandType, int $limit = self::AUTO_JOBS_LIMIT): array
    {
        $params = [
            'type' => $commandType->getCommandId(),
            'is_auto' => true,
            'isFinished' => true,
        ];

        return $this->getQuery($params)->orderBy(self::DEFAULT_ORDER)->limit($limit)->all();
    }

    /**
     * @param Employee $employee
     * @return ActiveRecordInterface[]|ImportJobData[]
     */
    public function findFinishedUnviewedJobsData(Employee $employee): array
    {
        $params = [
            'employee_id' => $employee->id,
            'isFinished' => true,
            'is_viewed' => false,
            'is_auto' => false,
        ];

        return $this->getQuery($params)->orderBy(self::DEFAULT_ORDER)->all();
    }

    /**
     * @param Employee $employee
     * @return bool
     */
    public function hasUnfinishedJobs(Employee $employee): bool
    {
        $params = [
            'employee_id' => $employee->id,
            'isFinished' => false,
            'is_auto' => false,
        ];

        return $this->getQuery($params)->orderBy(self::DEFAULT_ORDER)->count();
    }

    /**
     * @return ActiveRecordInterface|ImportJobData|null
     */
    public function findFinishedJobData(): ?ImportJobData
    {
        return $this->getQuery(['isFinished' => true])->orderBy(['id' => SORT_DESC])->one();
    }
}
