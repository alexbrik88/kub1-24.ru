<?php

namespace common\modules\import\models;

use common\models\Company;
use common\models\FormInterface;

interface TokenFormInterface extends FormInterface
{
    /**
     * @param Company $company
     * @return static
     */
    public static function createTokenForm(Company $company): self;

    /**
     * @return bool
     */
    public function updateToken(): bool;
}
