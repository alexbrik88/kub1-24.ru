<?php

namespace common\modules\import\models;

use common\behaviors\JsonAttributesBehavior;
use common\models\company\CompanyRelationTrait;
use common\models\employee\EmployeeRelationTrait;
use DateTimeImmutable;
use DateTimeInterface;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use RuntimeException;

/**
 * @property-read int $id
 * @property int $type
 * @property int $created_at
 * @property int|null $finished_at
 * @property int $count
 * @property bool $is_auto
 * @property bool $is_viewed
 * @property string $result
 * @property string $error
 */
class ImportJobData extends ActiveRecord
{
    use CompanyRelationTrait;
    use EmployeeRelationTrait;

    /** @var string */
    public const DEFAULT_RESULT_TEXT = 'Задача выполняется...';

    /** @var string */
    public const STATUS_JOB_PROCESSING = 'Происходит запрос данных...';

    /** @var string */
    public const STATUS_JOB_ERROR = 'Произошла ошибка при запросе данных';

    /** @var string */
    public const STATUS_JOB_SUCCESS = 'Данные загружены %s';

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'attributes' => [
                    self::EVENT_BEFORE_INSERT => ['created_at'],
                ],
            ],
            'jsonAttributes' => [
                'class' => JsonAttributesBehavior::class,
                'attributes' => [
                    'params',
                ],
            ],
        ];
    }

    /**
     * @return DateTimeInterface|null
     * @throws
     */
    public function getCreatedAt(): ?DateTimeInterface
    {
        if ($this->created_at === null) {
            return null;
        }

        return (new DateTimeImmutable)->setTimestamp($this->created_at);
    }

    /**
     * @return DateTimeInterface|null
     */
    public function getFinishedAt(): ?DateTimeInterface
    {
        if ($this->finished_at === null) {
            return null;
        }

        return (new DateTimeImmutable)->setTimestamp($this->finished_at);
    }

    /**
     * @return string|null
     */
    public function getJobStatus(): ?string
    {
        if ($this->error) {
            return self::STATUS_JOB_ERROR;
        }

        if (!$this->finished_at) {
            return self::STATUS_JOB_PROCESSING;
        }

        if ($this->finished_at) {
            return sprintf(self::STATUS_JOB_SUCCESS, date('d.m.Y в H:i', $this->finished_at));
        }

        return null;
    }

    /**
     * @return bool
     */
    public function isFinished(): bool
    {
        if ($this->finished_at === null) {
            return false;
        }

        return true;
    }

    /**
     * @param bool $runValidation
     * @param string[]|null $attributeNames
     * @return void
     */
    public function saveOrThrown(bool $runValidation = true, $attributeNames = null): void
    {
        if (!parent::save($runValidation, $attributeNames)) {
            throw new RuntimeException('Unable to save job data');
        }
    }

    /**
     * @param string $key
     * @return bool
     */
    public function hasParam(string $key): bool
    {
        $params = $this->getAttribute('params');

        if ($params !== null && array_key_exists($key, $params)) {
            return true;
        }

        return false;
    }

    /**
     * @param string $key
     * @param mixed $default
     * @return mixed|null
     */
    public function getParam(string $key, $default = null)
    {
        $params = $this->getAttribute('params');

        if ($this->hasParam($key)) {
            return $params[$key];
        }

        return $default;
    }

    /**
     * @param string $key
     * @param mixed|null $value
     * @return void
     */
    public function setParam(string $key, $value): void
    {
        $params = $this->getAttribute('params');

        if ($params === null) {
            $params = [];
        }

        if ($value === null) {
            unset($params[$key]);
        } else {
            $params[$key] = $value;
        }

        if (empty($params)) {
            $params = null;
        }

        $this->setAttribute('params', $params);
    }

    /**
     * @return string
     */
    public static function tableName()
    {
        return '{{%import_job_data}}';
    }
}
