<?php

namespace common\modules\import\models;

use common\models\FilterInterface;
use common\models\Company;
use common\models\employee\Employee;
use common\models\RepositoryCompanyTrait;
use common\modules\import\components\ImportCommandType;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\Sort;
use yii\db\ActiveQueryInterface;
use yii\db\ActiveRecordInterface;

class ImportJobDataRepository extends Model implements FilterInterface
{
    use RepositoryCompanyTrait;

    /** @var int */
    private const AUTO_JOBS_LIMIT = 5;

    /**
     * @var int
     */
    public $type;

    /**
     * @var bool
     */
    public $is_auto;

    /**
     * @var bool
     */
    public $isFinished;

    /**
     * @var
     */
    public $isViewed;

    /**
     * @param Company $company
     * @param array $config
     */
    public function __construct(Company $company, array $config = [])
    {
        $this->setCompany($company);

        parent::__construct($config);
    }

    /**
     * @param int $id
     * @return ImportJobData|null
     */
    public static function findById(int $id): ?ImportJobData
    {
        return ImportJobData::findOne(['id' => $id]);
    }

    /**
     * @inheritDoc
     */
    public function getDataProvider(): ActiveDataProvider
    {
        return new ActiveDataProvider([
            'pagination' => false,
            'query' => $this->getQuery(),
            'sort' => $this->getSort(),
        ]);
    }

    /**
     * @return ActiveQueryInterface
     */
    public function getQuery(): ActiveQueryInterface
    {
        $query = ImportJobData::find()->andFilterWhere([
            'company_id' => $this->company->id,
            'type' => $this->type,
        ]);

        if ($this->is_auto !== null) {
            $query->andWhere(['is_auto' => $this->is_auto]);
        }

        if ($this->isFinished === true) {
            $query->andWhere(['NOT', ['finished_at' => null]]);
        }

        if ($this->isFinished === false) {
            $query->andWhere(['finished_at' => null]);
        }

        if ($this->isViewed === true) {
            $query->andWhere(['is_viewed' => 1]);
        }

        if ($this->isViewed === false) {
            $query->andWhere(['is_viewed' => 0]);
        }

        return $query;
    }

    /**
     * @inheritDoc
     */
    public function getSort(): Sort
    {
        return new Sort(['defaultOrder' => ['id' => SORT_DESC]]);
    }

    /**
     * @param ImportCommandType $commandType
     * @param bool|null $isAuto
     * @param bool|null $isFinished
     * @return ActiveRecordInterface|ImportJobData|null
     */
    public function getLastJobData(
        ImportCommandType $commandType,
        ?bool $isAuto = null,
        ?bool $isFinished = null
    ): ?ImportJobData {
        $repository = new static($this->company, [
            'type' => $commandType->getCommandId(),
            'is_auto' => $isAuto,
            'isFinished' => $isFinished,
        ]);

        return $repository->getQuery()->orderBy(['id' => SORT_DESC])->one();
    }

    /**
     * @param ImportCommandType $commandType
     * @param int $limit
     * @return ActiveRecordInterface[]|ImportJobData[]
     */
    public function getAutoJobsData(ImportCommandType $commandType, int $limit = self::AUTO_JOBS_LIMIT): array
    {
        $repository = new static($this->company, [
            'type' => $commandType->getCommandId(),
            'is_auto' => true,
            'isFinished' => true,
        ]);

        return $repository->getQuery()->orderBy(['id' => SORT_DESC])->limit($limit)->all();
    }

    /**
     * @param Employee $employee
     * @return ActiveRecordInterface[]|ImportJobData[]
     */
    public function getFinishedUnviewedJobsData(Employee $employee): array
    {
        $repository = new static($this->company, [
            'isFinished' => true,
            'isViewed' => false,
            'is_auto' => false
        ]);

        return $repository
            ->getQuery()
            ->andWhere(['employee_id' => $employee->id])
            ->orderBy(['id' => SORT_DESC])
            ->all();
    }

    /**
     * @param Employee $employee
     * @return int
     */
    public function hasUnfinishedJobs(Employee $employee): int
    {
        $repository = new static($this->company, [
            'isFinished' => false,
            'is_auto' => false
        ]);

        return $repository
            ->getQuery()
            ->andWhere(['employee_id' => $employee->id])
            ->orderBy(['id' => SORT_DESC])
            ->count();
    }
}
