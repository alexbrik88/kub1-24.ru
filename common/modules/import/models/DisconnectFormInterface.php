<?php

namespace common\modules\import\models;

use common\models\Company;
use common\models\FormInterface;
use common\modules\import\components\ImportCommandType;

interface DisconnectFormInterface extends FormInterface
{
    /**
     * @return bool
     * @throws
     */
    public function removeIntegration(): bool;

    /**
     * @param Company $company
     * @param ImportCommandType $commandType,
     * @param string $identifier
     * @return static
     */
    public static function createDisconnectForm(
        Company $company,
        ImportCommandType $commandType,
        string $identifier
    ): self;
}
