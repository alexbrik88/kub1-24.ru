<?php

namespace common\modules\import\models;

use common\models\Company;
use common\models\ListInterface;
use common\modules\import\components\ImportCommandType;
use yii\base\Model;

/**
 * @property-read AutoImportSettings $autoImportSettings
 */
abstract class AbstractAutoImportForm extends Model implements AutoImportFormInterface
{
    /**
     * @var string
     */
    public $identifier;

    /**
     * @var int
     */
    public $period_type;

    /**
     * @var string[]
     */
    public $identifierList;

    /**
     * @var ImportCommandType
     */
    private $commandType;

    /**
     * @var Company
     */
    private $company;

    /**
     * @var AutoImportSettings|null
     */
    private $_autoImportSettings;

    /**
     * AbstractAutoImportForm constructor.
     * @param Company $company
     * @param ImportCommandType $commandType
     * @param ListInterface $identifierList
     * @param string $identifier
     * @param array $config
     */
    public function __construct(
        Company $company,
        ImportCommandType $commandType,
        ListInterface $identifierList,
        string $identifier,
        array $config = []
    ) {
        $this->company = $company;
        $this->commandType = $commandType;
        $this->identifierList = $identifierList->getItems();
        $this->identifier = $identifier;

        parent::__construct($config);
    }

    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return [
            [['period_type'], 'required'],
            [['period_type'], 'in', 'range' => array_keys(AutoImportSettings::PERIOD_TYPE_MAP)],
            [['identifier'], 'required'],
            [['identifier'], 'string'],
            [['identifier'], 'in', 'range' => array_keys($this->identifierList)],
        ];
    }

    /**
     * @inheritDoc
     */
    public function setAutoImport(): bool
    {
        $this->autoImportSettings->period_type = $this->period_type;

        if ($this->autoImportSettings->period_type == AutoImportSettings::PERIOD_TYPE_NEVER) {
            return AutoImportSettingsRepository::removeByIdentifier(
                $this->identifier,
                $this->company,
                $this->commandType,
            );
        }

        if ($this->autoImportSettings->save()) {
            return true;
        }

        return false;
    }

    /**
     * @inheritDoc
     */
    public function getAutoImportSettings(): AutoImportSettings
    {
        if ($this->_autoImportSettings === null) {
            $this->_autoImportSettings = AutoImportSettingsRepository::createAutoImportSettings(
                $this->identifier,
                $this->company,
                $this->commandType,
            );
        }

        return $this->_autoImportSettings;
    }

    /**
     * @return Company
     */
    public function getCompany(): Company
    {
        return $this->company;
    }
}
