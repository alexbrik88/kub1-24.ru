<?php

namespace common\modules\import\models;

use common\models\Company;
use common\models\ListInterface;
use common\modules\import\components\ImportCommandType;
use yii\base\Model;

abstract class AbstractDisconnectForm extends Model implements DisconnectFormInterface
{
    /**
     * @var string
     */
    public $identifier;

    /**
     * @var string[]
     */
    public $identifierList;

    /**
     * @var Company
     */
    private $company;

    /**
     * @var ImportCommandType
     */
    private $commandType;

    /**
     * @param Company $company
     * @param ImportCommandType $commandType
     * @param ListInterface $identifierList
     * @param string $identifier
     */
    public function __construct(
        Company $company,
        ImportCommandType $commandType,
        ListInterface $identifierList,
        string $identifier
    ) {
        $this->company = $company;
        $this->commandType = $commandType;
        $this->identifierList = $identifierList->getItems();
        $this->identifier = $identifier;

        parent::__construct();
    }

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['identifier'], 'required'],
            [['identifier'], 'string'],
            [['identifier'], 'in', 'range' => array_keys($this->identifierList)],
        ];
    }

    /**
     * @inheritDoc
     */
    public function removeIntegration(): bool
    {
        $remove = AutoImportSettingsRepository::removeByIdentifier(
            $this->identifier,
            $this->company,
            $this->commandType
        );

        if ($remove && $this->removeAccount()) {
            return true;
        }

        return false;
    }

    /**
     * @inheritDoc
     */
    public function getCompany(): Company
    {
        return $this->company;
    }

    /**
     * @return bool
     */
    abstract protected function removeAccount(): bool;
}
