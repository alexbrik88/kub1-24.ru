<?php

namespace common\modules\import\models;

use common\models\FormInterface;

interface AutoImportFormInterface extends FormInterface
{
    /**
     * @return AutoImportSettings
     */
    public function getAutoImportSettings(): AutoImportSettings;

    /**
     * @return bool
     */
    public function setAutoImport(): bool;
}
