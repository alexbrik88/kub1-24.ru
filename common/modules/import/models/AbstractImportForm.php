<?php

namespace common\modules\import\models;

use common\components\date\DateHelper;
use common\models\employee\Employee;
use common\models\FormInterface;
use common\modules\import\components\ImportCommandType;
use common\modules\import\components\ImportJobHandler;
use common\modules\import\factories\ImportJobFactory;
use DateTimeImmutable;

abstract class AbstractImportForm extends AbstractPeriodForm implements FormInterface
{
    /**
     * @var ImportCommandType
     */
    private $commandType;

    /**
     * @var Employee
     */
    private $employee;

    /**
     * @param Employee $employee
     * @param ImportCommandType $commandType
     */
    public function __construct(Employee $employee, ImportCommandType $commandType)
    {
        $this->employee = $employee;
        $this->commandType = $commandType;

        parent::__construct();
    }

    /**
     * @return bool
     */
    public function createJob(): bool
    {
        $params = $this->createJobParams();
        $jobData = ImportJobData::findOne([
            'company_id' => $this->employee->company->id,
            'type' => $this->commandType->getCommandId(),
            'is_auto' => false,
            'finished_at' => null,
            'params' => json_encode($params, JSON_UNESCAPED_UNICODE),
        ]);

        if ($jobData) {
            return true;
        }

        $job = (new ImportJobFactory)->createJob($this->employee, $this->commandType->getCommandId(), $params, false);

        if ($job && ImportJobHandler::getInstance()->dispatchJob($job)) {

            $this->employee->needCheckFinishedUnviewedJobs(true);

            return true;
        }

        return false;
    }

    /**
     * @return string[]
     */
    protected function createJobParams(): array
    {
        return [
            'from' => DateTimeImmutable::createFromFormat(DateHelper::FORMAT_USER_DATE, $this->dateFrom)
                ->setTime(0, 0, 0, 0)
                ->format(DateHelper::FORMAT_DATETIME),
            'to' => DateTimeImmutable::createFromFormat(DateHelper::FORMAT_USER_DATE, $this->dateTo)
                ->setTime(23, 59, 59, 999999)
                ->format(DateHelper::FORMAT_DATETIME),
        ];
    }

    /**
     * @return ImportCommandType
     */
    public function getCommandType(): ImportCommandType
    {
        return $this->commandType;
    }

    /**
     * @return Employee
     */
    public function getEmployee(): Employee
    {
        return $this->employee;
    }
}
