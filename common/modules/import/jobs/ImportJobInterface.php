<?php

namespace common\modules\import\jobs;

use yii\base\Configurable;
use yii\queue\JobInterface;

interface ImportJobInterface extends Configurable, JobInterface
{
    /**
     * @return int
     */
    public function getJobId(): int;
}
