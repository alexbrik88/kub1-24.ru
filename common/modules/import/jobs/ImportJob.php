<?php

namespace common\modules\import\jobs;

use common\modules\import\components\ImportJobHandler;
use Throwable;
use Yii;

final class ImportJob implements ImportJobInterface
{
    /**
     * @var int
     */
    private $jobId;

    /**
     * @param int $jobId
     */
    public function __construct(int $jobId)
    {
        $this->jobId = $jobId;
    }

    /**
     * @return int
     */
    public function getJobId(): int
    {
        return $this->jobId;
    }

    /**
     * @inheritDoc
     * @throws Throwable
     */
    public function execute($queue)
    {
        /** @var ImportJobHandler $handler */
        $handler = Yii::createObject(ImportJobHandler::class);
        $handler->handleJob($this);
    }
}
