<?php

namespace common\modules\import\components;

use common\components\date\DateHelper;
use common\modules\import\models\ImportJobData;
use DateTimeImmutable;
use DateTimeInterface;

final class ImportParamsHelper
{
    /**
     * @var ImportJobData $jobData
     */
    private $jobData;

    /**
     * @param ImportJobData $jobData
     */
    public function __construct(ImportJobData $jobData)
    {
        $this->jobData = $jobData;
    }

    /**
     * @return DateTimeInterface
     */
    public function getDateFrom(): DateTimeInterface
    {
        return DateTimeImmutable::createFromFormat(DateHelper::FORMAT_DATETIME, $this->jobData->getParam('from'));
    }

    /**
     * @return DateTimeInterface
     */
    public function getDateTo(): DateTimeInterface
    {
        return DateTimeImmutable::createFromFormat(DateHelper::FORMAT_DATETIME, $this->jobData->getParam('to'));
    }

    /**
     * @return string
     */
    public function getIdentifier(): string
    {
        return $this->jobData->getParam('identifier', '');
    }

    /**
     * @return bool
     */
    public function hasDateFrom(): bool
    {
        return $this->jobData->hasParam('from');
    }

    /**
     * @return bool
     */
    public function hasDateTo(): bool
    {
        return $this->jobData->hasParam('to');
    }

    /**
     * @return bool
     */
    public function hasIdentifier(): bool
    {
        return $this->jobData->hasParam('identifier');
    }

    /**
     * @param DateTimeInterface $dateFrom
     * @return void
     */
    public function setDateFrom(DateTimeInterface $dateFrom): void
    {
        $this->jobData->setParam('from', $dateFrom->format(DateHelper::FORMAT_DATETIME));
    }

    /**
     * @param DateTimeInterface $dateTo
     * @return void
     */
    public function setDateTo(DateTimeInterface $dateTo): void
    {
        $this->jobData->setParam('to', $dateTo->format(DateHelper::FORMAT_DATETIME));
    }

    /**
     * @param string $identifier
     * @return void
     */
    public function setIdentifier(string $identifier): void
    {
        $this->jobData->setParam('identifier', $identifier);
    }
}
