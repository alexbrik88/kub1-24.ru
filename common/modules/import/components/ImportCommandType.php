<?php

namespace common\modules\import\components;

use common\modules\import\commands\ImportCommandInterface;
use RuntimeException;

final class ImportCommandType
{
    /**
     * @var int
     */
    private $commandId;

    /**
     * @var string|ImportCommandInterface
     */
    private $commandClass;

    /**
     * @param string $commandClass
     * @param int $commandId
     */
    public function __construct(int $commandId, string $commandClass)
    {
        if (!is_a($commandClass, ImportCommandInterface::class, true)) {
            $format = 'Class %s does not realise of interface %s';

            throw new RuntimeException(sprintf($format, $commandClass, ImportCommandInterface::class));
        }

        $this->commandId = $commandId;
        $this->commandClass = $commandClass;
    }

    /**
     * @return int
     */
    public function getCommandId(): int
    {
        return $this->commandId;
    }

    /**
     * @return string|ImportCommandInterface
     */
    public function getCommandClass(): string
    {
        return $this->commandClass;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->getCommandClass();
    }
}
