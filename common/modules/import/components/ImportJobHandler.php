<?php

namespace common\modules\import\components;

use common\components\ObjectFactoryTrait;
use common\modules\import\commands\ImportCommandInterface;
use common\modules\import\jobs\ImportJobInterface;
use common\modules\import\models\ImportJobData;
use common\modules\import\models\ImportJobDataRepository;
use Throwable;
use yii\base\BootstrapInterface;
use yii\queue\ExecEvent;
use yii\queue\Queue;

final class ImportJobHandler implements BootstrapInterface
{
    use ObjectFactoryTrait;

    /**
     * @var ImportCommandManager
     */
    private $commandManager;

    /**
     * @var Queue
     */
    private $queue;

    /**
     * @param ImportCommandManager $commandManager
     * @param Queue $queue
     */
    public function __construct(ImportCommandManager $commandManager, Queue $queue)
    {
        $this->commandManager = $commandManager;
        $this->queue = $queue;
    }

    /**
     * @inheritDoc
     */
    public function bootstrap($app)
    {
        $this->queue->on(Queue::EVENT_AFTER_ERROR, [$this, 'afterError']);
    }

    /**
     * @param ImportJobInterface $job
     * @return bool
     */
    public function dispatchJob(ImportJobInterface $job): bool
    {
        if ($this->queue->push($job)) {
            return true;
        }

        return false;
    }

    /**
     * @param ExecEvent $event
     * @return void
     */
    public function afterError(ExecEvent $event): void
    {
        if ($event->job instanceof ImportJobInterface) {
            $jobData = ImportJobDataRepository::findById($event->job->getJobId());

            if ($jobData) {
                $this->handleError($event->error, $jobData);
                $jobData->saveOrThrown();
            }
        }
    }

    /**
     * @param ImportJobInterface $job
     * @throws Throwable
     */
    public function handleJob(ImportJobInterface $job): void
    {
        $jobData = ImportJobDataRepository::findById($job->getJobId());

        if (!$jobData) {
            return;
        }

        try {
            $command = $this->createCommand($jobData);
            $this->handleCommand($command, $jobData);
        } catch (ImportUnauthorizedException $exception) {
            $this->handleUnauthorized($exception->getMessage(), $jobData);
        }

        $jobData->saveOrThrown();
    }

    /**
     * @param ImportCommandInterface $command
     * @param ImportJobData $jobData
     * @return void
     */
    private function handleCommand(ImportCommandInterface $command, ImportJobData $jobData): void
    {
        if ($jobData->isFinished()) {
            return;
        }

        $command->run();
        $jobData->refresh();

        if ($jobData->isFinished()) {
            return;
        }

        $jobData->count = $command->getSavedCount();
        $jobData->result = ($jobData->count === 0)
            ? 'За выбранный период нет данных.'
            : sprintf('Выгрузка успешно завершена. Количество: %d.', $jobData->count);

        $jobData->finished_at = time();
    }

    /**
     * @param string|null $error
     * @param ImportJobData $jobData
     * @return void
     */
    private function handleError(?string $error, ImportJobData $jobData): void
    {
        if (!$jobData->isFinished()) {
            $jobData->finished_at = time();
            $jobData->result = ImportJobData::STATUS_JOB_ERROR;
            $jobData->error = $error;
        }
    }

    /**
     * @param string $message
     * @param ImportJobData $jobData
     */
    private function handleUnauthorized(string $message, ImportJobData $jobData): void
    {
        if (!$jobData->isFinished()) {
            $jobData->finished_at = time();
            $jobData->result = $message;
        }
    }

    /**
     * @param ImportJobData $jobData
     * @return ImportCommandInterface
     */
    private function createCommand(ImportJobData $jobData): ImportCommandInterface
    {
        $commandType = $this->commandManager->getTypeById($jobData->type);
        $commandClass = $commandType->getCommandClass();

        return $commandClass::createImportCommand($jobData);
    }
}
