<?php

namespace common\modules\import\components;

use common\components\ObjectFactoryTrait;
use yii\base\Component;
use yii\base\InvalidConfigException;

final class ImportCommandManager extends Component
{
    use ObjectFactoryTrait;

    /**
     * @var string[]
     */
    public $commands = [];

    /**
     * @var ImportCommandType[]
     */
    private $types;

    /**
     * @throws InvalidConfigException
     */
    public function init(): void
    {
        if (empty($this->commands)) {
            throw new InvalidConfigException();
        }

        array_walk($this->commands, function (string $commandClass, int $commandId) {
            $this->types[$commandId] = new ImportCommandType($commandId, $commandClass);
        });
    }

    /**
     * @param string $commandClass
     * @return ImportCommandType
     */
    public function getTypeByClass(string $commandClass): ImportCommandType
    {
        $classes = array_flip($this->commands);
        $commandId = $classes[$commandClass];

        return $this->types[$commandId];
    }

    /**
     * @param int $commandId
     * @return ImportCommandType
     */
    public function getTypeById(int $commandId): ImportCommandType
    {
        return $this->types[$commandId];
    }
}
