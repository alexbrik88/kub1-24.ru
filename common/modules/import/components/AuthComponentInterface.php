<?php

namespace common\modules\import\components;

interface AuthComponentInterface
{
    /**
     * @return string
     */
    public function getAuthorizationUrl(): string;
}
