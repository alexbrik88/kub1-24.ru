<?php

namespace common\modules\import\components;

use RuntimeException;

class ImportUnauthorizedException extends RuntimeException
{
}
