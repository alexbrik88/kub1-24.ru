<?php

namespace common\modules\import\factories;

use DateInterval;
use DateTimeImmutable;
use DateTimeInterface;
use DateTimeZone;

class DateTimeFactory
{
    /**
     * @return DateTimeInterface
     */
    public function createDateFrom(): DateTimeInterface
    {
        return $this->createDateTime()->sub(new DateInterval('P1D'))->setTime(0, 0, 0, 0);
    }

    /**
     * @return DateTimeInterface
     */
    public function createDateTo(): DateTimeInterface
    {
        return $this->createDateTime()->sub(new DateInterval('P1D'))->setTime(23, 59, 59, 999999);
    }

    /**
     * @return DateTimeInterface
     * @throws
     */
    public function createDateTime(): DateTimeInterface
    {
        return new DateTimeImmutable('now', $this->createTimeZone());
    }

    /**
     * @return DateTimeZone
     */
    public function createTimeZone(): DateTimeZone
    {
        return new DateTimeZone('Europe/Moscow');
    }
}
