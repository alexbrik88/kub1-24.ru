<?php

namespace common\modules\import\factories;

use common\models\employee\Employee;
use common\modules\import\jobs\ImportJob;
use common\modules\import\jobs\ImportJobInterface;
use common\modules\import\models\ImportJobData;

class ImportJobFactory
{
    /**
     * @param Employee $employee
     * @param int $type
     * @param bool $isAuto
     * @param array $params
     * @return ImportJobInterface
     */
    public function createJob(
        Employee $employee,
        int $type,
        array $params = [],
        bool $isAuto = false
    ): ImportJobInterface {
        $jobData = new ImportJobData([
            'company_id' => $employee->company->id,
            'employee_id' => $employee->id,
            'type' => $type,
            'is_auto' => $isAuto,
            'params' => $params,
            'result' => ImportJobData::DEFAULT_RESULT_TEXT,
        ]);
        $jobData->saveOrThrown();

        return new ImportJob($jobData->id);
    }
}
