<?php

namespace common\modules\marketing\models;

use common\models\company\CompanyRelationTrait;
use common\models\Company;

/**
 * This is the model class for table "marketing_notification_message".
 *
 * @property int $id
 * @property int $company_id
 * @property int $channel_id
 * @property int $group_id
 * @property string|null $message
 * @property int $status
 * @property int|null $created_at
 *
 * @property MarketingChannel $channel
 * @property MarketingNotificationGroup $group
 */
class MarketingNotificationMessage extends \yii\db\ActiveRecord
{
    use CompanyRelationTrait;

    const STATUS_ACTIVE = 1;
    const STATUS_INACTIVE = 2;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'marketing_notification_message';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'channel_id', 'group_id'], 'required'],
            [['company_id', 'channel_id', 'group_id', 'status', 'created_at'], 'integer'],
            [['message'], 'string'],
            [['channel_id'], 'exist', 'skipOnError' => true, 'targetClass' => MarketingChannel::className(), 'targetAttribute' => ['channel_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => MarketingNotificationGroup::className(), 'targetAttribute' => ['group_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'channel_id' => 'Channel ID',
            'group_id' => 'Group ID',
            'message' => 'Message',
            'status' => 'Status',
            'created_at' => 'Created At',
        ];
    }

    /**
     * Gets query for [[Channel]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getChannel()
    {
        return $this->hasOne(MarketingChannel::className(), ['id' => 'channel_id']);
    }

    /**
     * Gets query for [[Group]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(MarketingNotificationGroup::className(), ['id' => 'group_id']);
    }
}
