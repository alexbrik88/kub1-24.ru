<?php namespace common\modules\marketing\models\channel;

use common\models\yandex\YandexDirectCampaignReport;
use yii\db\Expression;

class YandexNotificationHelper
{
    /**
     * @param $company_id
     * @return int
     * @throws \Exception
     */
    public static function getClicksCount($company_id)
    {
        $date = (new \DateTime())->modify('-1 day')->format('Y-m-d');
        return (int)YandexDirectCampaignReport::find()
            ->where(['AND', ['company_id' => $company_id], ['date' => $date]])
            ->sum('clicks_count');
    }

    /**
     * @param $company_id
     * @return int
     * @throws \Exception
     */
    public static function getClicksCosts($company_id)
    {
        $date = (new \DateTime())->modify('-1 day')->format('Y-m-d');
        return (float)YandexDirectCampaignReport::find()
            ->where(['AND', ['company_id' => $company_id], ['date' => $date]])
            ->select(new Expression('SUM(avg_cpc) / COUNT(avg_cpc)'))
            ->scalar();
    }

    /**
     * @param $company_id
     * @return int
     * @throws \Exception
     */
    public static function getLeadsCount($company_id)
    {
        $date = (new \DateTime())->modify('-1 day')->format('Y-m-d');
        return (int)YandexDirectCampaignReport::find()
            ->where(['AND', ['company_id' => $company_id], ['date' => $date]])
            ->sum('conversions_count');
    }

    /**
     * @param $company_id
     * @return int
     * @throws \Exception
     */
    public static function getLeadsCosts($company_id)
    {
        $date = (new \DateTime())->modify('-1 day')->format('Y-m-d');
        return (float)YandexDirectCampaignReport::find()
            ->where(['AND', ['company_id' => $company_id], ['date' => $date]])
            ->select(new Expression('SUM(cost / conversions_count)'))
            ->scalar();
    }

    /**
     * @param $company_id
     * @return int
     * @throws \Exception
     */
    public static function getAdCosts($company_id)
    {
        $date = (new \DateTime())->modify('-1 day')->format('Y-m-d');
        return (int)YandexDirectCampaignReport::find()
            ->where(['AND', ['company_id' => $company_id], ['date' => $date]])
            ->sum('cost');
    }

    /**
     * @param $company_id
     * @param $balance
     * @return float|int|mixed
     * @throws \Exception
     */
    public static function calcBalanceLeftDays($company_id, $balance)
    {
        $date = new \DateTime();

        $costD1 = (int)YandexDirectCampaignReport::find()
                ->where([
                    'AND',
                    ['company_id' => $company_id],
                    ['between', 'date', (clone $date)->modify('-1 day')->format('Y-m-d'), (clone $date)->modify('-1 day')->format('Y-m-d')]
                ])->sum('cost') / 1;
        $costD3 = (int)YandexDirectCampaignReport::find()
                ->where([
                    'AND',
                    ['company_id' => $company_id],
                    ['between', 'date', (clone $date)->modify('-4 day')->format('Y-m-d'), (clone $date)->modify('-1 day')->format('Y-m-d')]
                ])->sum('cost') / 3;
        $costD7 = (int)YandexDirectCampaignReport::find()
                ->where([
                    'AND',
                    ['company_id' => $company_id],
                    ['between', 'date', (clone $date)->modify('-8 day')->format('Y-m-d'), (clone $date)->modify('-1 day')->format('Y-m-d')]
                ])->sum('cost') / 7;

        if ($costD7 > 0 && $costD3 > 0 && $costD1 > 0) {
            $ret = min((int)$balance/$costD7, (int)$balance/$costD3, (int)$balance/$costD1);
        }
        else if ($costD7 > 0 && $costD3 > 0) {
            $ret = min((int)$balance/$costD7, (int)$balance/$costD3);
        } else if ($costD7 > 0) {
            $ret = (int)$balance/$costD7;
        } else if ($costD3 > 0) {
            $ret = (int)$balance / $costD3;
        } else if ($costD1 > 0) {
            $ret = (int)$balance / $costD1;
        } else {
            $ret = 9E9;
        }

        return $ret;
    }

    /**
     * @param $company_id
     * @param $attr
     * @return float|int
     * @throws \Exception
     */
    public static function calcDeviation($company_id, $attr)
    {
        $date = new \DateTime();

        $sumD1 = (float)YandexDirectCampaignReport::find()
                ->where([
                    'AND',
                    ['company_id' => $company_id],
                    ['between', 'date', (clone $date)->modify('-1 day')->format('Y-m-d'), (clone $date)->modify('-1 day')->format('Y-m-d')]
                ])->select(new Expression("SUM({$attr})"))->scalar() / 1;
        $sumD7 = (float)YandexDirectCampaignReport::find()
                ->where([
                    'AND',
                    ['company_id' => $company_id],
                    ['between', 'date', (clone $date)->modify('-8 day')->format('Y-m-d'), (clone $date)->modify('-1 day')->format('Y-m-d')]
                ])->select(new Expression("SUM({$attr})"))->scalar() / 7;

        if ($sumD7 > 0)
            return (($sumD1 - $sumD7) / $sumD7) * 100;

        return 0;
    }
}