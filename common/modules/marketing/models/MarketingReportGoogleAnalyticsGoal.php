<?php

namespace common\modules\marketing\models;

use yii\db\ActiveRecord;

/**
 * @property-read int $id
 * @property int $company_id
 * @property int $source_type
 * @property string $date
 * @property string $campaign_name
 * @property string $utm_source
 * @property string $utm_medium
 * @property string $utm_campaign
 * @property int $goal_id
 * @property int $total
 */
class MarketingReportGoogleAnalyticsGoal extends ActiveRecord
{
    public $conversion_rate;
    public $roi;

    /**
     * @inheritDoc
     */
    public static function tableName()
    {
        return '{{%marketing_report_google_analytics_goal}}';
    }
}
