<?php

namespace common\modules\marketing\models;

use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * @property int $id
 * @property string|null $name
 * @property int|null $sort
 *
 * @property MarketingNotification[] $marketingNotifications
 * @property-read string $controllerId
 */
class MarketingChannel extends ActiveRecord
{
    public const ALL = 0;
    public const YANDEX_AD = 1;
    public const GOOGLE_AD = 2;
    public const FACEBOOK  = 3;
    public const INSTAGRAM = 4;
    public const VKONTAKTE = 5;
    public const GOOGLE_ANALYTICS = 6;
    public const DASHBOARD_TOTALS = 0;

    /** @var string[] */
    public const CONTROLLER_LIST = [
        self::YANDEX_AD => 'yandex-direct',
        self::GOOGLE_AD => 'google-words',
        self::FACEBOOK => 'facebook',
        self::VKONTAKTE => 'vk-ads',
    ];

    /**
     * @inheritDoc
     */
    public static function tableName(): string
    {
        return '{{%marketing_channel}}';
    }

    /**
     * @inheritDoc
     */
    public function rules(): array
    {
        return [
            [['sort'], 'integer'],
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getMarketingNotifications(): ActiveQuery
    {
        return $this->hasMany(MarketingNotification::class, ['channel_id' => 'id']);
    }

    /**
     * @return string
     */
    public function getControllerId(): string
    {
        return self::CONTROLLER_LIST[$this->id];
    }

    public static function getUrl($channelId) // TODO: remove
    {
        switch ($channelId) {
            case self::YANDEX_AD:
                return '/marketing/reports/yandex-direct';
            case self::GOOGLE_AD:
            case self::FACEBOOK:
            case self::INSTAGRAM:
            case self::VKONTAKTE:
                return '#'; // todo
        }

        return '#';
    }
}
