<?php

namespace common\modules\marketing\models;

use common\models\employee\Employee;
use yii\base\Model;
use common\models\Company;
use common\modules\marketing\models\channel\YandexNotificationHelper;

class MarketingNotificationSender extends Model
{
    /**
     * @var Company
     */
    private $_company;

    /**
     * @var int
     */
    private $_channel_id;

    /**
     * @param $newBalance
     * @throws \Exception
     */
    public function triggerBalanceEvent($newBalance)
    {
        $params = $this->_company->integration(Employee::INTEGRATION_YANDEX_DIRECT);
        $oldBalance = $params['balance_amount'] ?? 0;

        /** @var $notificationByGroup MarketingNotification[] */
        $notificationByGroup = MarketingNotification::find()
            ->where([
                'company_id' => $this->_company->id,
                'channel_id' => $this->_channel_id
            ])
            ->indexBy('group_id')
            ->all();

        // zero balance always notified
        if (!array_key_exists(MarketingNotificationGroup::BALANCE_BY_ZERO, $notificationByGroup)) {
            $notificationByGroup[MarketingNotificationGroup::BALANCE_BY_ZERO] = new MarketingNotification([
                'company_id' => $this->_company->id,
                'channel_id' => $this->_channel_id,
                'group_id' => MarketingNotificationGroup::BALANCE_BY_ZERO,
                'last_date' => date('Y-m-d', 0),
                'notify_by_informer' => true,
                'threshold' => 0
            ]);
        }

        foreach ($notificationByGroup as $groupId => $notification)
        {
            switch ($groupId) {
                case MarketingNotificationGroup::BALANCE_BY_AMOUNT:
                    if ($newBalance < $notification->threshold) {
                        $this->send($notification, $newBalance);
                    }
                    break;
                case MarketingNotificationGroup::BALANCE_BY_DAYS:
                    $leftDays = YandexNotificationHelper::calcBalanceLeftDays($this->_company->id, $newBalance);
                    if ($leftDays < $notification->threshold) {
                        $this->send($notification, $leftDays);
                    }
                    break;
                case MarketingNotificationGroup::BALANCE_BY_ZERO:
                    if ($newBalance <= 0 && $newBalance < $oldBalance) {
                        $this->send($notification);
                    }
                    break;
            }
        }
    }

    public function triggerUpdateEvent()
    {
        /** @var $notificationByGroup MarketingNotification[] */
        $notificationByGroup = MarketingNotification::find()
            ->where([
                'company_id' => $this->_company->id,
                'channel_id' => $this->_channel_id
            ])
            ->indexBy('group_id')
            ->all();

        foreach ($notificationByGroup as $groupId => $notification)
        {
            switch ($groupId) {
                case MarketingNotificationGroup::CLICKS_BY_COUNT:
                    $count = YandexNotificationHelper::getClicksCount($this->_company->id);
                    if ($count < $notification->threshold) {
                        $this->send($notification, $count);
                    }
                    break;
                case MarketingNotificationGroup::CLICKS_BY_COSTS:
                    $costs = YandexNotificationHelper::getClicksCosts($this->_company->id);
                    if ($costs > $notification->threshold) {
                        $this->send($notification, $costs);
                    }
                    break;
                case MarketingNotificationGroup::LEADS_BY_COUNT:
                    $count = YandexNotificationHelper::getLeadsCount($this->_company->id);
                    if ($count < $notification->threshold) {
                        $this->send($notification, $count);
                    }
                    break;
                case MarketingNotificationGroup::LEADS_BY_COSTS:
                    $costs = YandexNotificationHelper::getLeadsCosts($this->_company->id);
                    if ($costs > $notification->threshold) {
                        $this->send($notification, $costs);
                    }
                    break;
                case MarketingNotificationGroup::VISITS_BY_COUNT:
                    // todo
                    break;
                case MarketingNotificationGroup::AD_COST:
                    $costs = YandexNotificationHelper::getAdCosts($this->_company->id);
                    if ($costs > $notification->threshold) {
                        $this->send($notification, $costs);
                    }
                    break;
                case MarketingNotificationGroup::LEADS_BY_COUNT_DEVIATION:
                    $deviationCount = YandexNotificationHelper::calcDeviation($this->_company->id, 'conversions_count');
                    if ($deviationCount <= 0 && abs($deviationCount) < $notification->threshold) {
                        $this->send($notification, $deviationCount);
                    }
                    break;
                case MarketingNotificationGroup::LEADS_BY_COSTS_DEVIATION:
                    $deviationCosts = YandexNotificationHelper::calcDeviation($this->_company->id, 'cost / conversions_count');
                    if ($deviationCosts > $notification->threshold) {
                        $this->send($notification, $deviationCosts);
                    }
                    break;
                case MarketingNotificationGroup::AD_COST_DEVIATION:
                    $adCosts = YandexNotificationHelper::calcDeviation($this->_company->id, 'cost');
                    if ($adCosts > $notification->threshold) {
                        $this->send($notification, $adCosts);
                    }
                    break;

            }
        }
    }

    /**
     * @param MarketingNotification $notification
     * @param int $newValue
     * @return bool
     */
    public function send(MarketingNotification $notification, $newValue = 0)
    {
        if ($notification->isNotified())
            return true;

        $groupId = $notification->group_id;
        $thresholdValue = $notification->threshold;

        if ($notification->notify_by_informer) {

            $msg = MarketingNotificationMessage::find()
                ->where([
                    'company_id' => $this->_company->id,
                    'channel_id' => $this->_channel_id,
                    'group_id' => $groupId
                ])->one();

            if (!$msg) {
                $msg = new MarketingNotificationMessage();
                $msg->company_id = $this->_company->id;
                $msg->channel_id = $this->_channel_id;
                $msg->group_id = $groupId;
            }

            $msg->message = MarketingNotificationGroup::getMessage($groupId, $thresholdValue, $newValue);
            $msg->status = MarketingNotificationMessage::STATUS_ACTIVE;
            $msg->created_at = time();

            if ($msg->save()) {
                $notification->updateAttributes(['last_date' => date('Y-m-d')]);
            } else {
                \Yii::error(implode(', ', [
                    'Cannot save marketing notification',
                    'company:'.$this->_company->id,
                    'channel:'.$this->_channel_id,
                    'group:'.$this->$groupId,
                    json_encode($msg->getErrors())
                ]));
            }
        }
        if ($notification->notify_by_email) {
            // todo
            return true;
        }
        if ($notification->notify_by_whatsapp) {
            // todo
            return true;
        }
        if ($notification->notify_by_telegram) {
            // todo
            return true;
        }

        return true;
    }

    /**
     * @param Company $company
     */
    public function setCompany(Company $company)
    {
        $this->_company = $company;
    }

    /**
     * @param int $channel
     */
    public function setChannel(int $channel)
    {
        $this->_channel_id = $channel;
    }
}