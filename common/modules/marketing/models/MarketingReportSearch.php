<?php

namespace common\modules\marketing\models;

use Yii;
use common\models\Company;
use DateTime;
use frontend\components\StatisticPeriod;
use yii\data\ActiveDataProvider;
use yii\db\Expression;
use yii\db\Query;

class MarketingReportSearch extends MarketingReport
{
    public static $SHOW_GROUPS = 1;
    public static $GROUP_BY_CUSTOM_GROUPS = 1;
    public static $GROUP_BY_CHANNELS = 0;

    /**
     * @var string
     */
    public $campaignName;

    private $groupBy;
    private $defaultSorting;

    private $_query = null;
    private $_sortAttributes = null;

    private static $_keyIndicatorCache;
    private static $_keyIndicatorCacheMonth;
    private static $_maxChartDate;

    public function __construct($groupBy = [], $defaultSorting = 'clicks', $config = [])
    {
        parent::__construct($config);
        $this->groupBy = $groupBy;
        $this->defaultSorting = $defaultSorting;
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['campaignName'], 'safe'],
        ];
    }

    public function search(int $channelType, Company $company, $params = [], $period = null): ActiveDataProvider
    {
        if ($period === null || !is_array($period)) {
            $period = StatisticPeriod::getSessionPeriod();
        }
        if ($channelType === MarketingChannel::ALL) {
            $channelType = null;
        }

        $googleAnalyticsGoals = $company->googleAnalyticsProfile->goals ?? [];

        $commonSelectColumns = [
            'channel' => 't.source_type',
            'i.group_id',
            'utm_campaign_group' => 'IF(i.group_id IS NULL, t.utm_campaign, CONCAT("g_", i.group_id))',
            't.date',
            't.campaign_name',
            't.utm_campaign',
            'cost' => 'SUM(t.cost)',
            'clicks' => 'SUM(t.clicks)',
            'impressions' => 'SUM(t.impressions)',
            'ctr' => 'IFNULL((SUM(t.clicks) / SUM(t.impressions)) * 100, 0)',
            'avg_cpc' => 'IFNULL(SUM(t.cost) / SUM(t.clicks), 0)',
        ];

        $googleAnalyticsGoalSelectColumns = [];

        foreach ($googleAnalyticsGoals as $goal) {

            $totalColumn = "goal_{$goal->external_id}_total";
            $conversionRateColumn = "goal_{$goal->external_id}_conversion_rate";
            $roiColumn = "goal_{$goal->external_id}_roi";

            $googleAnalyticsGoalSelectColumns[$totalColumn] =
                 MarketingReportGoogleAnalyticsGoal::find()
                    ->alias($totalColumn)
                    ->select(["SUM($totalColumn.total)"])
                    ->where([
                        'AND',
                        ["$totalColumn.company_id" => $company->id],
                        ["$totalColumn.goal_id" => $goal->external_id],
                        ($channelType)
                            ? ["$totalColumn.source_type" => $channelType]
                            : "1=1",
                        "t.utm_campaign = $totalColumn.utm_campaign",
                        ['between', "$totalColumn.date", $period['from'], $period['to']],
                    ]);
            $googleAnalyticsGoalSelectColumns[$conversionRateColumn] =
                MarketingReportGoogleAnalyticsGoal::find()
                    ->alias($conversionRateColumn)
                    ->select(["IFNULL((SUM($conversionRateColumn.total) / SUM(t.clicks)), 0) * 100"])->where([
                        'AND',
                        ["$conversionRateColumn.company_id" => $company->id],
                        ["$conversionRateColumn.goal_id" => $goal->external_id],
                        ($channelType)
                            ? ["$conversionRateColumn.source_type" => $channelType]
                            : "1=1",
                        "t.utm_campaign = $conversionRateColumn.utm_campaign",
                        ['between', "$conversionRateColumn.date", $period['from'], $period['to']],
                    ]);
            $googleAnalyticsGoalSelectColumns[$roiColumn] =
                MarketingReportGoogleAnalyticsGoal::find()
                    ->alias($roiColumn)
                    ->select(["IFNULL(SUM(t.cost) / (SUM($roiColumn.total)), 0)"])->where([
                        'AND',
                        ["$roiColumn.company_id" => $company->id],
                        ["$roiColumn.goal_id" => $goal->external_id],
                        ($channelType)
                            ? ["$roiColumn.source_type" => $channelType]
                            : "1=1",

                        "t.utm_campaign = $roiColumn.utm_campaign",
                        ['between', "$roiColumn.date", $period['from'], $period['to']],
                    ]);
        }

        $selectColumns = array_merge(
            $commonSelectColumns,
            $googleAnalyticsGoalSelectColumns
        );

        $query = MarketingReport::find()
            ->alias('t')
            ->leftJoin(['i' => MarketingReportGroupItem::tableName()], 't.company_id = i.company_id AND t.source_type = i.source_type AND t.utm_campaign = i.utm_campaign')
            ->select($selectColumns)->andWhere([
                'AND',
                ['t.company_id' => $company->id],
                ['between', 't.date', $period['from'], $period['to']],
            ])->andFilterWhere([
                't.source_type' => $channelType
            ]);

        $this->load($params);

        if ($this->campaignName) {
            $query->andWhere(['like', 't.campaign_name', $this->campaignName]);
        }

        if ($this->group_id) {
            $query->andWhere(['group_id' => (int)$this->group_id]);
        }

        $query->groupBy($this->groupBy);

        if (self::$SHOW_GROUPS) {

            $commonSelectColumns2 = [
                't2.channel',
                't2.group_id',
                't2.utm_campaign_group',
                't2.date',
                'campaign_name' => (self::$GROUP_BY_CHANNELS) ? new Expression('null') : 'IF(t2.group_id IS NULL, t2.campaign_name, g.title)',
                't2.utm_campaign',
                'cost' => 'SUM(t2.cost)',
                'clicks' => 'SUM(t2.clicks)',
                'impressions' => 'SUM(t2.impressions)',
                'ctr' => 'IFNULL((SUM(t2.clicks) / SUM(t2.impressions)) * 100, 0)',
                'avg_cpc' => 'IFNULL(SUM(t2.cost) / SUM(t2.clicks), 0)'
            ];

            $googleAnalyticsGoalSelectColumns2 = [];

            foreach ($googleAnalyticsGoals as $goal) {
                $totalColumn = "goal_{$goal->external_id}_total";
                $conversionRateColumn = "goal_{$goal->external_id}_conversion_rate";
                $roiColumn = "goal_{$goal->external_id}_roi";
                $googleAnalyticsGoalSelectColumns2[$totalColumn] = "SUM(t2.{$totalColumn})";
                $googleAnalyticsGoalSelectColumns2[$conversionRateColumn] = "(SUM(t2.{$totalColumn}) / SUM(t2.clicks) * 100)";
                $googleAnalyticsGoalSelectColumns2[$roiColumn] = "(SUM(t2.cost) / SUM(t2.{$totalColumn}))";
            }

            $selectColumns2 = array_merge(
                $commonSelectColumns2,
                $googleAnalyticsGoalSelectColumns2
            );

            // wrap main query
            if (self::$GROUP_BY_CHANNELS) {
                $query = (new Query)
                    ->from(['t2' => $query])
                    ->select($selectColumns2)
                    ->groupBy('channel');
            }
            elseif (self::$GROUP_BY_CUSTOM_GROUPS) {
                $query = (new Query)
                    ->from(['t2' => $query])
                    ->leftJoin(['g' => MarketingReportGroup::tableName()], 't2.group_id = g.id')
                    ->select($selectColumns2)
                    ->groupBy('utm_campaign_group');
            }
        }

        $commonAttributes = [
            'campaign_name',
            'utm_campaign',
            'cost',
            'clicks',
            'impressions',
            'ctr',
            'avg_cpc',
        ];

        $googleAnalyticsGoalAttributes = [];

        foreach ($googleAnalyticsGoals as $goal) {
            $googleAnalyticsGoalAttributes[] = "goal_{$goal->external_id}_total";
            $googleAnalyticsGoalAttributes[] = "goal_{$goal->external_id}_conversion_rate";
            $googleAnalyticsGoalAttributes[] = "goal_{$goal->external_id}_roi";
        }

        $sortAttributes = array_merge(
            $commonAttributes,
            $googleAnalyticsGoalAttributes
        );

        $this->_query = $query;
        $this->_sortAttributes = $sortAttributes;

        $dataProvider = new ActiveDataProvider([
            'sort' => [
                'attributes' => $sortAttributes,
                'defaultOrder' => [
                    // Сортировка по первой выбранной цели — по количеству достижений цели
                    $this->defaultSorting => SORT_DESC,
                ],
            ],
            'query' => $query,
        ]);

        return $dataProvider;
    }

    public function getQuery(): \yii\db\Query
    {
        return $this->_query;
    }

    public function getSortAttributes()
    {
        return $this->_sortAttributes;
    }

    public static function getKeyIndicatorData($companyId, $channelType, $tableAttr, $xDays, $filters = [], $goalId = null)
    {
        list($table, $attrToSum) = explode('.', $tableAttr);
        $_cacheKey = md5($companyId . $table . $xDays . serialize($filters) . $goalId);
        $maxChartDate = self::getMaxChartDate($companyId);

        if (!isset(self::$_keyIndicatorCache[$_cacheKey])) {

            $xOffset = $filters['offset'] ?? 0;
            $campaign = $filters['campaign'] ?? null;

            if ($table === MarketingReport::tableName()) {
                $sumExpression = '
                IFNULL((SUM(t.clicks) / SUM(t.impressions)) * 100, 0) ctr,
                IFNULL(SUM(t.cost) / SUM(t.clicks), 0) avg_cpc,
                IFNULL(SUM(t.clicks), 0) clicks,
                IFNULL(SUM(t.cost), 0) cost';
            } else {
                // goal table
                $sumExpression = 'IFNULL(SUM(t.total), 0) total';
            }

            $xDays--;
            $startDate = (new DateTime())->modify("-{$xDays} days")->modify("+{$xOffset} days")->format('Y-m-d');
            $interval = "('{$startDate}' + INTERVAL (seq) DAY)";
            $seq = "seq_0_to_{$xDays}";

            $byGoalFilter = ($goalId) ? ('t.goal_id = ' . (int)$goalId) : '1=1';
            $byCampaignFilter = ($campaign) ? ('t.utm_campaign = ' . Yii::$app->db->quoteValue($campaign)) : '1=1';
            $byChannel = ($channelType) ? "(t.source_type = {$channelType})" : '1=1';

            $query = \Yii::$app->db2->createCommand("
                SELECT 
                DATE_FORMAT({$interval}, '%Y-%m-%d') AS date,
                DATE_FORMAT({$interval}, '%d.%m.%Y') AS x,
                {$sumExpression}
                FROM {$seq}
                LEFT JOIN {$table} t
                    ON  (t.date = {$interval})
                    AND (t.company_id = {$companyId})
                    AND {$byChannel}
                    AND {$byGoalFilter}
                    AND {$byCampaignFilter}
                GROUP BY ({$interval})
           ");

            self::$_keyIndicatorCache[$_cacheKey] = $query->queryAll();
        }

        $data = &self::$_keyIndicatorCache[$_cacheKey];

        $xy = [];
        foreach ($data as $v) {
            $xy[] = [
                'x' => $v['x'],
                'y' => ($v['date'] <= $maxChartDate)
                    ? (float)$v[$attrToSum]
                    : null
            ];
        }

        return $xy;
    }

    public static function getKeyIndicatorDataMonth($companyId, $channelType, $tableAttr, $xMonths, $filters = [], $goalId = null)
    {
        list($table, $attrToSum) = explode('.', $tableAttr);
        $_cacheKey = md5($companyId . $table . $xMonths . serialize($filters) . $goalId);
        $maxChartDate = self::getMaxChartDate($companyId);

        if (!isset(self::$_keyIndicatorCacheMonth[$_cacheKey])) {

            $xOffset = $filters['offset'] ?? 0;
            $campaign = $filters['campaign'] ?? null;

            if ($table === MarketingReport::tableName()) {
                $sumExpression = '
                IFNULL((SUM(t.clicks) / SUM(t.impressions)) * 100, 0) ctr,
                IFNULL(SUM(t.cost) / SUM(t.clicks), 0) avg_cpc,
                IFNULL(SUM(t.clicks), 0) clicks,
                IFNULL(SUM(t.cost), 0) cost';
            } else {
                // goal table
                $sumExpression = 'IFNULL(SUM(t.total), 0) total';
            }

            $xMonths--;
            $startDate = (new DateTime())->modify("-{$xMonths} months")->modify("+{$xOffset} months")->format('Y-m-01');
            $intervalFrom = "('{$startDate}' + INTERVAL (seq) MONTH)";
            $intervalTo = "('{$startDate}' + INTERVAL (seq+1) MONTH)";
            $seq = "seq_0_to_{$xMonths}";

            $byGoalFilter = ($goalId) ? ('t.goal_id = ' . (int)$goalId) : '1=1';
            $byCampaignFilter = ($campaign) ? ('t.utm_campaign = ' . Yii::$app->db->quoteValue($campaign)) : '1=1';
            $byChannel = ($channelType) ? "(t.source_type = {$channelType})" : '1=1';

            $query = \Yii::$app->db2->createCommand("
                SELECT 
                DATE_FORMAT({$intervalFrom}, '%Y-%m-01') AS date,
                DATE_FORMAT({$intervalFrom}, '%d.%m.%Y') AS x,
                {$sumExpression}
                FROM {$seq}
                LEFT JOIN {$table} t
                    ON  (t.date >= {$intervalFrom} AND t.date < {$intervalTo})
                    AND (t.company_id = {$companyId})
                    AND {$byChannel}
                    AND {$byGoalFilter}
                    AND {$byCampaignFilter}          
                GROUP BY ({$intervalFrom})
           ");

            self::$_keyIndicatorCacheMonth[$_cacheKey] = $query->queryAll();
        }

        $data = &self::$_keyIndicatorCacheMonth[$_cacheKey];

        $xy = [];
        foreach ($data as $v) {
            $xy[] = [
                'x' => $v['x'],
                'y' => ($v['date'] <= $maxChartDate)
                    ? (float)$v[$attrToSum]
                    : null
            ];
        }

        return $xy;
    }

    public static function calcKeyIndicatorData(array $arr1, array $arr2, $mathOperation = 'divide', $isPercent = false)
    {
        $ret = [];
        if ($mathOperation === 'divide') {
            foreach ($arr1 as $k => $v) {
                $a = $v['y'];
                $b = ($arr2[$k]['y'] ?? 0) ?: 9E99;
                $ret[] = [
                    'x' => $v['x'],
                    'y' => ($v['y'] === null || $arr2[$k]['y'] === null)
                        ? null
                        : round(($isPercent ? 100 : 1) * $a / $b, 1)
                ];
            }
        } else {
            foreach ($arr1 as $k => $v) {
                $ret[] = [
                    'x' => $v['x'],
                    'y' => null
                ];
            }
        }

        return $ret;
    }

    public static function _getIndicatorPopularChannel2($name, $subname, $unitname, $arr, $precision = 0)
    {
        $today = $arr[count($arr)-1]['y'] ?? 0;
        $yesterday = $arr[count($arr)-2]['y'] ?? 0;

        return [
            'name' => $name,
            'subname' => $subname,
            'unit_name' => $unitname,
            'today' => [
                'value' => number_format(round($today, 1), $precision),
                'percent' => round(($yesterday > 0 && $today > 0) ? (($today - $yesterday) / $today * 100) : ($today > 0 ? 100 : 0), 1)
            ],
            'yesterday' => ['value' => round($yesterday, 1)],
            'average' => round(array_sum(array_column(array_slice($arr, -7), 'y')) / 7, $precision),
            'y' => array_column(array_slice($arr, -8), 'y')
        ];
    }

    public static function getChartDataByDays($companyId, $sourceType, $periods, $prevPeriods, $campaign = null)
    {
        $res = [
            'curr' => [],
            'prev' => [],
        ];

        $maxChartDate = self::getMaxChartDate($companyId);
        foreach ($periods as $k => $p) {
            $res['curr'][$k] =
            $res['prev'][$k] = ($p['from'] < $maxChartDate) ? 0 : null;
        }

        $result = [
            'ctr' => $res,
            'clicks' => $res,
            'leads' => $res,
            'cost' => $res,
            'cpc' => $res,
            'conversion' => $res,
            'cpl' => $res,
        ];

        $dateFrom = $prevPeriods[0]['from'];
        $dateTill = $periods[count($periods)-1]['to'];

        $query = MarketingReport::find()
            ->alias('t')
            ->select(new Expression('
                t.date,
                IFNULL((SUM(t.clicks) / SUM(t.impressions)) * 100, 0) ctr,
                IFNULL(SUM(t.cost) / SUM(t.clicks), 0) cpc,
                IFNULL(SUM(t.clicks), 0) clicks,
                IFNULL(SUM(t.google_analytics_leads), 0) leads,
                IFNULL(SUM(t.cost), 0) cost
            '))
            ->where(['company_id' => $companyId])
            ->andWhere(['between', 'date', $dateFrom, $dateTill])
            ->andFilterWhere(['utm_campaign'  => $campaign])
            ->andFilterWhere(['source_type' => $sourceType ?: null])
            ->groupBy('date');

        foreach ($query->asArray()->all(\Yii::$app->db2) as $r)
        {
            if ($r['date'] < $periods[0]['from']) {
                foreach ($prevPeriods as $i => $p) {
                    if ($r['date'] == $p['from']) {
                        $result['ctr']['prev'][$i] += $r['ctr'];
                        $result['clicks']['prev'][$i] += $r['clicks'];
                        $result['cost']['prev'][$i] += $r['cost'];
                        $result['cpc']['prev'][$i] += $r['cpc'];
                        continue 2;
                    }
                }
            } else {
                foreach ($periods as $i => $p) {
                    if ($r['date'] == $p['from']) {
                        $result['ctr']['curr'][$i] += $r['ctr'];
                        $result['clicks']['curr'][$i] += $r['clicks'];
                        $result['cost']['curr'][$i] += $r['cost'];
                        $result['cpc']['curr'][$i] += $r['cpc'];
                        continue 2;
                    }
                }
            }
        }

        $query2 = MarketingReportGoogleAnalyticsGoal::find()
            ->alias('t')
            ->select(new Expression('
                t.date,
                IFNULL(SUM(t.total), 0) leads
            '))
            ->where(['company_id' => $companyId])
            ->andWhere(['between', 'date', $dateFrom, $dateTill])
            ->andFilterWhere(['utm_campaign'  => $campaign])
            ->andFilterWhere(['source_type' => $sourceType ?: null])
            ->groupBy('date');

        foreach ($query2->asArray()->all(\Yii::$app->db2) as $r)
        {
            if ($r['date'] < $periods[0]['from']) {
                foreach ($prevPeriods as $i => $p) {
                    if ($r['date'] == $p['from']) {
                        $result['leads']['prev'][$i] += $r['leads'];
                        continue 2;
                    }
                }
            } else {
                foreach ($periods as $i => $p) {
                    if ($r['date'] == $p['from']) {
                        $result['leads']['curr'][$i] += $r['leads'];
                        continue 2;
                    }
                }
            }
        }

        foreach ($result['leads'] as $k2 => $v2) {
            foreach ($v2 as $k => $v) {
                $a = $v;
                $b = ($result['clicks'][$k2][$k] ?? 0) ?: 9E99;
                $result['conversion'][$k2][$k] = ($result['clicks'][$k2][$k] === null)
                    ? null
                    : round(100 * $a / $b, 1);
            }
        }

        foreach ($result['cost'] as $k2 => $v2) {
            foreach ($v2 as $k => $v) {
                $a = $v;
                $b = ($result['leads'][$k2][$k] ?? 0) ?: 9E99;
                $result['cpl'][$k2][$k] = ($result['leads'][$k2][$k] === null)
                    ? null
                    : round(1 * $a / $b, 1);
            }
        }

        return $result;
    }

    public static function getChannelList()
    {
        return [
            MarketingChannel::DASHBOARD_TOTALS => 'Все',
            MarketingChannel::YANDEX_AD => 'Яндекс.Директ',
            MarketingChannel::GOOGLE_AD => 'Google.Ads',
            MarketingChannel::FACEBOOK => 'Facebook',
            MarketingChannel::VKONTAKTE => 'Вконтакте'
        ];

    }

    public static function getCampaignList($company, $channelType, $period = null)
    {
        if (!$period)
            $period = StatisticPeriod::getSessionPeriod();

        return MarketingReport::find()
            ->alias('t')
            ->andWhere([
                'AND',
                ['t.company_id' => $company->id],
                ['between', 'date', $period['from'], $period['to']],
                ['t.source_type' => $channelType],
            ])
            ->select(['campaign_name', 'utm_campaign'])
            ->distinct()
            ->indexBy('utm_campaign')
            ->column();
    }

    public static function getMaxChartDate($companyId, $channelType = null)
    {
        if (self::$_maxChartDate === null) {
            self::$_maxChartDate = MarketingReport::find()
                ->alias('t')
                ->andWhere(['t.company_id' => $companyId])
                ->andFilterWhere(['t.source_type' => $channelType])
                ->max('date');
        }
        
        return self::$_maxChartDate ?: '1970-01-01';
    }

    public static function getGoalColumns()
    {
        return [
            'total' => [
                'name' => 'Кол-во',
                'toggle' => false,
                'sort' => true,
                'calculateValue' => function ($reports, $attributePrefix) {
                    return MarketingReport::getTotal($reports, "{$attributePrefix}_total");
                },
            ],
            'conversion_rate' => [
                'name' => 'Конверсия, %',
                'toggle' => false,
                'sort' => true,
                'calculateValue' => function ($reports, $attributePrefix) {
                    return MarketingReport::getTotalConversionRate($reports, "{$attributePrefix}_total", 'clicks');
                },
            ],
            'roi' => [
                'name' => 'Цена цели, ₽',
                'toggle' => true,
                'sort' => false,
                'calculateValue' => function ($reports, $attributePrefix) {
                    return MarketingReport::getTotalRoi($reports, 'cost', "{$attributePrefix}_total");
                },
            ]
        ];
    }
}