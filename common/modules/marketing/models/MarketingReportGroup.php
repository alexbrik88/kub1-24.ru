<?php

namespace common\modules\marketing\models;

use Yii;
use common\models\Company;

/**
 * This is the model class for table "marketing_report_group".
 *
 * @property int $id
 * @property int $company_id
 * @property int $source_type
 * @property string $title
 *
 * @property Company $company
 * @property MarketingReportGroupItem[] $items
 */
class MarketingReportGroup extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'marketing_report_group';
    }

    public $itemsErrors;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'source_type', 'title'], 'required'],
            [['company_id', 'source_type'], 'integer'],
            [['title'], 'string', 'max' => 255],
            [['title'], 'unique', 'filter' => ['company_id' => $this->company_id],
                'message' => 'Такая группа уже существует',
            ],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['company_id' => 'id']],
            [['source_type'], 'in', 'range' => [
                MarketingChannel::YANDEX_AD,
                MarketingChannel::GOOGLE_AD,
                MarketingChannel::FACEBOOK,
                MarketingChannel::VKONTAKTE,
            ]],
            [['itemsErrors'], 'safe']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Компания',
            'source_type' => 'Канал',
            'title' => 'Название группы',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Company::class, ['id' => 'company_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getItems()
    {
        return $this->hasMany(MarketingReportGroupItem::class, ['group_id' => 'id']);
    }
}
