<?php

namespace common\modules\marketing\models;

use Yii;
use common\models\Company;

/**
 * This is the model class for table "marketing_report_group_item".
 *
 * @property int $company_id
 * @property int $group_id
 * @property int $source_type
 * @property string $utm_campaign
 * @property string $campaign_name
 *
 * @property MarketingReportGroup $group
 */
class MarketingReportGroupItem extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'marketing_report_group_item';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'group_id', 'source_type', 'utm_campaign'], 'required'],
            [['utm_campaign'], 'unique', 'targetAttribute' => ['company_id', 'group_id', 'utm_campaign']],
            [['company_id', 'source_type', 'group_id'], 'integer'],
            [['utm_campaign', 'campaign_name'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['company_id' => 'id']],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => MarketingReportGroup::class, 'targetAttribute' => ['group_id' => 'id']],
            [['utm_campaign'], 'exist', 'skipOnError' => true, 'targetClass' => MarketingReport::class, 'targetAttribute' => ['utm_campaign' => 'utm_campaign']],
            [['source_type'], 'in', 'range' => [
                MarketingChannel::YANDEX_AD,
                MarketingChannel::GOOGLE_AD,
                MarketingChannel::FACEBOOK,
                MarketingChannel::VKONTAKTE,
            ]],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'group_id' => 'Group ID',
            'utm_campaign' => 'Utm Campaign',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(MarketingReportGroup::class, ['id' => 'group_id']);
    }
}
