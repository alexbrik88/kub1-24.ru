<?php

namespace common\modules\marketing\models;

use common\models\Company;
use common\models\company\CompanyRelationTrait;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * @property int $id
 * @property int $company_id
 * @property string $user_name
 * @property string|null $avatar
 * @property string $access_token
 * @property string $expires_at
 * @property string $refresh_token
 * @property int $analytics_account_id
 * @property string $analytics_account_name
 * @property string $analytics_property_id
 * @property string $analytics_property_name
 * @property int $analytics_view_id
 * @property string $analytics_view_name
 * @property int $analytics_goal_id
 * @property string $analytics_goal_name
 *
 * @property-read GoogleAnalyticsProfileGoal[] $goals
 */
class GoogleAnalyticsProfile extends ActiveRecord
{
    use CompanyRelationTrait;

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['company_id', 'user_name', 'access_token', 'expires_at', 'refresh_token'], 'required'],
            [['company_id', 'expires_at', 'analytics_account_id', 'analytics_view_id',
                'analytics_goal_id'], 'integer'],
            [['access_token', 'user_name', 'refresh_token', 'avatar', 'analytics_account_name', 'analytics_property_name',
                'analytics_view_name', 'analytics_goal_name', 'analytics_property_id'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * @return ActiveQuery
     */
    public function getGoals()
    {
        return $this->hasMany(GoogleAnalyticsProfileGoal::class, ['profile_id' => 'id', 'company_id' => 'company_id']);
    }

    /**
     * @inheritDoc
     */
    public static function tableName()
    {
        return '{{%google_analytics_profile}}';
    }

    /**
     * @param Company $company
     * @return static|null
     */
    public static function findByCompany(Company $company): ?self
    {
        return static::findOne(['company_id' => $company->id]);
    }
}
