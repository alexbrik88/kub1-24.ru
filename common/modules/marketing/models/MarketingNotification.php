<?php

namespace common\modules\marketing\models;

use common\models\company\CompanyRelationTrait;
use Yii;
use common\models\Company;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "marketing_notification".
 *
 * @property int $id
 * @property int $company_id
 * @property int $channel_id
 * @property int $group_id
 * @property int $threshold
 * @property int $notify_by_informer
 * @property int $notify_by_email
 * @property int $notify_by_whatsapp
 * @property int $notify_by_telegram
 * @property string|null $last_date
 * @property int $skip
 *
 * @property MarketingChannel $channel
 * @property MarketingNotificationGroup $group
 */
class MarketingNotification extends \yii\db\ActiveRecord
{
    use CompanyRelationTrait;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'marketing_notification';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['company_id', 'channel_id', 'group_id'], 'required'],
            [['company_id', 'channel_id', 'group_id', 'notify_by_informer', 'notify_by_email', 'notify_by_whatsapp', 'notify_by_telegram', 'skip'], 'integer'],
            [['threshold'], 'number'],
            [['threshold'], 'filter', 'filter' => function ($value) {
                return str_replace(",", ".", $value);
            }],
            [['last_date'], 'safe'],
            [['channel_id'], 'exist', 'skipOnError' => true, 'targetClass' => MarketingChannel::className(), 'targetAttribute' => ['channel_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::className(), 'targetAttribute' => ['company_id' => 'id']],
            [['group_id'], 'exist', 'skipOnError' => true, 'targetClass' => MarketingNotificationGroup::className(), 'targetAttribute' => ['group_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'channel_id' => 'Channel ID',
            'group_id' => 'Group ID',
            'threshold' => 'Threshold',
            'notify_by_informer' => 'Notify By Informer',
            'notify_by_email' => 'Notify By Email',
            'notify_by_whatsapp' => 'Notify By Whatsapp',
            'notify_by_telegram' => 'Notify By Telegram',
            'last_date' => 'Last Date',
            'skip' => 'Skip',
        ];
    }

    /**
     * Gets query for [[Channel]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getChannel()
    {
        return $this->hasOne(MarketingChannel::className(), ['id' => 'channel_id']);
    }

    /**
     * Gets query for [[Group]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getGroup()
    {
        return $this->hasOne(MarketingNotificationGroup::className(), ['id' => 'group_id']);
    }

    /**
     * @param $key
     * @param $channelId
     * @param $groupId
     * @param $items
     * @return mixed
     */
    public static function getValue($key, $channelId, $groupId, $items)
    {
        if ($groupId == MarketingNotificationGroup::BALANCE_BY_ZERO) {
            switch ($key) {
                case 'threshold':
                    return 0;
                case 'notify_by_informer':
                    return true;
                default:
                    break;
            }
        }

        return ArrayHelper::getValue(ArrayHelper::getValue(ArrayHelper::getValue($items, $channelId), $groupId), $key);
    }

    public static function getItems()
    {
        $items = self::find()->where(['company_id' => Yii::$app->user->identity->company->id])->asArray()->all();
        $ret = [];
        foreach ($items as $item)
            $ret[$item['channel_id']] = [];
        foreach ($items as $item)
            $ret[$item['channel_id']][$item['group_id']] = $item;

        return $ret;
    }

    /**
     * @param $arr
     * @param $company
     * @return bool
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public static function _save($arr, $company)
    {
        foreach ($arr as $cid => $groups) {
            foreach ($groups as $gid => $values) {

                $model = self::findOne([
                    'company_id' => $company->id,
                    'channel_id' => $cid,
                    'group_id' => $gid,
                ]);

                $presentInDB = !empty($model);

                if (!$model) {
                    $model = new self;
                }

                $model->company_id = (int)$company->id;
                $model->channel_id = (int)$cid;
                $model->group_id = (int)$gid;
                $model->threshold = (float)ArrayHelper::getValue($values, 'threshold', 0);
                $model->notify_by_informer = (int)ArrayHelper::getValue($values, 'notify_by_informer', 0);
                $model->notify_by_email = (int)ArrayHelper::getValue($values, 'notify_by_email', 0);
                $model->notify_by_whatsapp = (int)ArrayHelper::getValue($values, 'notify_by_whatsapp', 0);
                $model->notify_by_telegram = (int)ArrayHelper::getValue($values, 'notify_by_telegram', 0);

                if (self::_needSave($model)) {
                    if (!$model->save()) {
                        var_dump($model->getErrors());exit;
                        return false;
                    }
                } elseif ($presentInDB) {
                    $model->delete();
                }
            }
        }

        return true;
    }

    /**
     * @param $model
     * @return bool
     */
    public static function _needSave($model)
    {
        return (bool)($model->notify_by_informer || $model->notify_by_email || $model->notify_by_whatsapp || $model->notify_by_telegram);
    }

    public function isNotified()
    {
        return ($this->last_date == date('Y-m-d'));
    }
}
