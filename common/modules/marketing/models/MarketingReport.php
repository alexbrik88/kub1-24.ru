<?php

namespace common\modules\marketing\models;

use common\models\company\CompanyRelationTrait;
use frontend\components\StatisticPeriod;
use yii\db\ActiveRecord;

/**
 * @property-read int $id
 * @property int $company_id
 * @property int $source_type
 * @property string $date
 * @property string $campaign_name
 * @property string $utm_source
 * @property string $utm_medium
 * @property string $utm_campaign
 * @property float $cost
 * @property int $clicks
 * @property int $impressions
 * @property int $google_analytics_leads
 */
class MarketingReport extends ActiveRecord
{
    use CompanyRelationTrait;

    public $ctr;
    public $avg_cpc;

    public $minDate;
    public $maxDate;

    public $group_id;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'marketing_report';
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'google_analytics_leads' => 'Лиды',
        ];
    }

    public function rules()
    {
        return [
//            [['goal_2_total'], 'safe'],
        ];
    }

    public function init()
    {
        parent::init();

        foreach (range(1, 20) as $index) {
            foreach (['total', 'conversion_rate', 'roi'] as $goalAttribute) {
                $goalAttributeName = "goal_{$index}_{$goalAttribute}";
                $this->{$goalAttributeName} = null;
            }
        }
    }

    public function __set($name, $value)
    {
        if (strpos($name, 'goal_') !== false) {
            $this->{$name} = $value;
        } else {
            parent::__set($name, $value);
        }
    }

    public function getMaxDate()
    {
        return date('Y-m-d');
    }

    public function getStatisticPeriodName()
    {
        $name = StatisticPeriod::getSessionName();
        if ($this->minDate && $this->maxDate) {
            $statisticsPeriod = StatisticPeriod::getSessionPeriod();
            $searchPeriod = ['from' => strtotime($statisticsPeriod['from']), 'to' => strtotime($statisticsPeriod['to'])];
            $dataPeriod = ['from' => strtotime($this->minDate), 'to' => strtotime($this->maxDate)];

            if ($searchPeriod['from'] < $dataPeriod['from'] || $searchPeriod['to'] > $dataPeriod['to']) {
                $name = date('d.m.Y', $dataPeriod['from']) . ' - ' . date('d.m.Y', $dataPeriod['to']);
            }
        }

        return $name;
    }

    /**
     * @param array $provider
     * @param string $fieldName
     * @return int
     */
    public static function getTotal($provider, $fieldName)
    {
        $total = 0;

        foreach ($provider as $item) {
            $total += $item[$fieldName];
        }

        return $total;
    }

    /**
     * @param array $provider
     * @param string $clicksFieldName
     * @param string $impressionsFieldName
     * @return float|int
     */
    public static function getTotalCtr($provider, $clicksFieldName, $impressionsFieldName)
    {
        $clicks = self::getTotal($provider, $clicksFieldName);
        $impressions = self::getTotal($provider, $impressionsFieldName);

        return $impressions ? ($clicks / $impressions * 100) : 0;
    }

    /**
     * @param array $provider
     * @param string $costFieldName
     * @param string $clicksFieldName
     * @return float|int
     */
    public static function getTotalCpc($provider, $costFieldName, $clicksFieldName)
    {
        $cost = self::getTotal($provider, $costFieldName);
        $clicks = self::getTotal($provider, $clicksFieldName);

        return $clicks ? ($cost / $clicks) : 0;
    }

    /**
     * @param array $provider
     * @param string $totalFieldName
     * @param string $clicksFieldName
     * @return float|int
     */
    public static function getTotalConversionRate($provider, $totalFieldName, $clicksFieldName)
    {
        $total = self::getTotal($provider, $totalFieldName);
        $clicks = self::getTotal($provider, $clicksFieldName);

        return $clicks ? ($total / $clicks * 100) : 0;
    }

    public static function getTotalRoi($provider, $costFieldName, $totalFieldName)
    {
        $cost = self::getTotal($provider, $costFieldName);
        $total = self::getTotal($provider, $totalFieldName);

        return $total ? ($cost / $total) : 0;
    }
}
