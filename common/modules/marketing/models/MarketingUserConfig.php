<?php

namespace common\modules\marketing\models;

use Yii;
use common\models\employee\Employee;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "marketing_user_config".
 *
 * @property int $employee_id
 * @property int $statistics_chart_type_1
 * @property int $statistics_chart_type_2
 * @property int $statistics_chart_type_3
 * @property int $dynamics_chart_type
 * @property int $dynamics_chart_period
 *
 * @property Employee $employee
 */
class MarketingUserConfig extends \yii\db\ActiveRecord
{
    const CHART_TYPE_CTR = 1;
    const CHART_TYPE_CLICKS = 2;
    const CHART_TYPE_CPC = 3;
    const CHART_TYPE_CONVERSION = 4;
    const CHART_TYPE_LEADS = 5;
    const CHART_TYPE_CPL = 6;
    const CHART_TYPE_COST = 7;
    const CHART_TYPE_UNKNOWN = 0;

    const CHART_PERIOD_DAYS = 1;
    const CHART_PERIOD_MONTHS = 2;

    public static $chartTypeName = [
        self::CHART_TYPE_CTR => 'CTR',
        self::CHART_TYPE_CLICKS => 'Клики',
        self::CHART_TYPE_CPC => 'CPC',
        self::CHART_TYPE_CONVERSION => 'Конверсия',
        self::CHART_TYPE_LEADS => 'Лиды',
        self::CHART_TYPE_CPL => 'CPL',
        self::CHART_TYPE_COST => 'Расходы',
    ];

    public static $chartTypeId = [
        self::CHART_TYPE_CTR => 'ctr',
        self::CHART_TYPE_CLICKS => 'clicks',
        self::CHART_TYPE_CPC => 'cpc',
        self::CHART_TYPE_CONVERSION => 'conversion',
        self::CHART_TYPE_LEADS => 'leads',
        self::CHART_TYPE_CPL => 'cpl',
        self::CHART_TYPE_COST => 'cost',
        self::CHART_TYPE_UNKNOWN => 'unknown'
    ];

    public static $chartTypeTitle = [
        self::CHART_TYPE_CTR => 'CTR',
        self::CHART_TYPE_CLICKS => 'КЛИКИ',
        self::CHART_TYPE_CPC => 'CPC',
        self::CHART_TYPE_CONVERSION => 'КОНВЕРСИЯ',
        self::CHART_TYPE_LEADS => 'ЛИДЫ',
        self::CHART_TYPE_CPL => 'CPL',
        self::CHART_TYPE_COST => 'РАСХОДЫ',
        self::CHART_TYPE_UNKNOWN => '---'
    ];

    public static $chartPeriodName = [
        self::CHART_PERIOD_DAYS => 'День',
        self::CHART_PERIOD_MONTHS => 'Месяц',
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'marketing_user_config';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['statistics_chart_type_1'], 'default', 'value' => self::CHART_TYPE_CLICKS],
            [['statistics_chart_type_2'], 'default', 'value' => self::CHART_TYPE_LEADS],
            [['statistics_chart_type_3'], 'default', 'value' => self::CHART_TYPE_COST],
            [['dynamics_chart_type'], 'default', 'value' => self::CHART_TYPE_CTR],
            [['dynamics_chart_period'], 'default', 'value' => self::CHART_PERIOD_DAYS],
            [[
                'statistics_chart_type_1',
                'statistics_chart_type_2',
                'statistics_chart_type_3',
                'dynamics_chart_type',
                'dynamics_chart_period'
            ], 'filter', 'filter' => function ($value) {return (int)$value;} ],
            [[
                'statistics_chart_type_1',
                'statistics_chart_type_2',
                'statistics_chart_type_3',
                'dynamics_chart_type',
                'dynamics_chart_period'
            ], 'integer'],
            [['employee_id'], 'exist', 'skipOnError' => true, 'targetClass' => Employee::class, 'targetAttribute' => ['employee_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'employee_id' => 'Employee ID',
            'statistics_chart_type_1' => 'График 1',
            'statistics_chart_type_2' => 'График 2',
            'statistics_chart_type_3' => 'График 3',
            'dynamics_chart_type' => 'График (динамика показателей)',
            'dynamics_chart_period' => 'Период (динамика показателей)',
        ];
    }

    /**
     *
     */
    public function setDefaults()
    {
        $this->statistics_chart_type_1 = self::CHART_TYPE_CLICKS;
        $this->statistics_chart_type_2 = self::CHART_TYPE_LEADS;
        $this->statistics_chart_type_3 = self::CHART_TYPE_COST;
        $this->dynamics_chart_type = self::CHART_TYPE_CTR;
        $this->dynamics_chart_period = self::CHART_PERIOD_DAYS;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEmployee()
    {
        return $this->hasOne(Employee::class, ['id' => 'employee_id']);
    }

    /**
     * @return array
     */
    public function getTopChartViews()
    {
        $path = 'top_chart_factory/chart_';

        return [
            1 => $path . (self::$chartTypeId[$this->statistics_chart_type_1] ?? self::$chartTypeId[self::CHART_TYPE_UNKNOWN]),
            2 => $path . (self::$chartTypeId[$this->statistics_chart_type_2] ?? self::$chartTypeId[self::CHART_TYPE_UNKNOWN]),
            3 => $path . (self::$chartTypeId[$this->statistics_chart_type_3] ?? self::$chartTypeId[self::CHART_TYPE_UNKNOWN]),
        ];
    }

    public function getTopChartOptions($xAxis, $yAxis)
    {
        return [
            1 => [
                'id' => 'chart_11',
                'jsModel' => 'Chart123.chart_11',
                'xAxis' => $xAxis,
                'title' => '<b>'.(self::$chartTypeTitle[$this->statistics_chart_type_1] ?? self::$chartTypeTitle[self::CHART_TYPE_UNKNOWN]).'</b> по дням',
                'yAxis' => ArrayHelper::getValue($yAxis, (self::$chartTypeId[$this->statistics_chart_type_1] ?? '??'), [null]),
            ],
            2 => [
                'id' => 'chart_12',
                'jsModel' => 'Chart123.chart_12',
                'title' => '<b>'.(self::$chartTypeTitle[$this->statistics_chart_type_2] ?? self::$chartTypeTitle[self::CHART_TYPE_UNKNOWN]).'</b> по дням',
                'xAxis' => $xAxis,
                'yAxis' => ArrayHelper::getValue($yAxis, (self::$chartTypeId[$this->statistics_chart_type_2] ?? '??'), [null]),
            ],
            3 => [
                'id' => 'chart_13',
                'jsModel' => 'Chart123.chart_13',
                'title' => '<b>'.(self::$chartTypeTitle[$this->statistics_chart_type_3] ?? self::$chartTypeTitle[self::CHART_TYPE_UNKNOWN]).'</b> по дням',
                'xAxis' => $xAxis,
                'yAxis' => ArrayHelper::getValue($yAxis, (self::$chartTypeId[$this->statistics_chart_type_3] ?? '??'), [null]),
                'isAjax' => $isAjax ?? null
            ],
        ];
    }
}
