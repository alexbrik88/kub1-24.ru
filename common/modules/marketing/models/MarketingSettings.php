<?php

namespace common\modules\marketing\models;

use common\models\company\CompanyRelationTrait;
use yii\db\ActiveRecord;

/**
 * @property-read int $id
 * @property int $company_id
 * @property int $command_type
 * @property int $auto_import_type
 */
class MarketingSettings extends ActiveRecord
{
    use CompanyRelationTrait;

    /** @var int */
    public const AUTO_IMPORT_TYPE_DISABLED = 0;

    /** @var int */
    public const AUTO_IMPORT_TYPE_EVERY_DAY = 1;

    /** @var string[] */
    public const AUTO_IMPORT_TYPE_MAP = [
        self::AUTO_IMPORT_TYPE_DISABLED => 'Нет',
        self::AUTO_IMPORT_TYPE_EVERY_DAY => 'Каждый день',
    ];

    /**
     * @inheritDoc
     */
    public static function tableName()
    {
        return '{{%marketing_settings}}';
    }
}
