<?php

namespace common\modules\marketing\models;

use yii\db\ActiveRecord;

/**
 * @property int $company_id
 * @property int $profile_id
 * @property int $external_id
 * @property string $name
 */
class GoogleAnalyticsProfileGoal extends ActiveRecord
{
    /**
     * @inheritDoc
     */
    public static function tableName()
    {
        return '{{%google_analytics_profile_goal}}';
    }
}
