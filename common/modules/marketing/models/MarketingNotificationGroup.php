<?php

namespace common\modules\marketing\models;

use common\components\helpers\ArrayHelper;
use common\components\TextHelper;
use Yii;

/**
 * This is the model class for table "marketing_notification_group".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $rule
 * @property string|null $description
 * @property int|null $threshold_unit
 * @property int|null $sort
 *
 * @property MarketingNotification[] $marketingNotifications
 */
class MarketingNotificationGroup extends \yii\db\ActiveRecord
{
    const BALANCE_BY_AMOUNT = 1;
    const BALANCE_BY_DAYS = 2;
    const BALANCE_BY_ZERO = 3;
    const CLICKS_BY_COUNT = 4;
    const CLICKS_BY_COSTS = 5;
    const LEADS_BY_COUNT = 6;
    const LEADS_BY_COSTS = 7;
    const VISITS_BY_COUNT = 8;
    const AD_COST = 9;
    const LEADS_BY_COUNT_DEVIATION = 11;
    const LEADS_BY_COSTS_DEVIATION = 12;
    const AD_COST_DEVIATION = 13;

    /**
     *
     */
    const THRESHOLD_NAME = [
        0 => '',
        1 => '₽',
        2 => 'дн.',
        3 => '%'
    ];

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'marketing_notification_group';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['threshold_unit', 'sort'], 'integer'],
            [['name', 'rule', 'description'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'rule' => 'Rule',
            'description' => 'Description',
            'threshold_unit' => 'Threshold Unit',
            'sort' => 'Sort',
        ];
    }

    /**
     * Gets query for [[MarketingNotifications]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMarketingNotifications()
    {
        return $this->hasMany(MarketingNotification::className(), ['group_id' => 'id']);
    }

    /**
     * @param $unitId
     * @return mixed
     */
    public static function getThresholdUnitName($unitId)
    {
        return ArrayHelper::getValue(self::THRESHOLD_NAME, $unitId);
    }

    /**
     * @param $groupId
     * @param $thresholdValue
     * @param $value
     * @return string
     */
    public static function getMessage($groupId, $thresholdValue, $value)
    {
        $msg = "";
        switch ($groupId) {
            case self::BALANCE_BY_AMOUNT:
                $msg = "Баланс ниже суммы " . TextHelper::moneyFormat($thresholdValue, 2) . " ₽ и равен " . TextHelper::moneyFormat($value, 2) . " ₽";
                break;
            case self::BALANCE_BY_DAYS:
                $msg = "Баланса осталось на " . TextHelper::numberFormat($value, 2) . " дн.";
                break;
            case self::BALANCE_BY_ZERO:
                $msg = "Баланс равен " . TextHelper::moneyFormat($value, 2) . " ₽";
                break;
            case self::CLICKS_BY_COUNT:
                $msg = "Кол-во кликов за вчера ниже " . TextHelper::numberFormat($thresholdValue) . " и равно " . TextHelper::numberFormat($value);
                break;
            case self::CLICKS_BY_COSTS:
                $msg = "Стоим-ть клика за вчера выше " . TextHelper::moneyFormat($thresholdValue, 2) . " ₽ и равна " . TextHelper::moneyFormat($value, 2) . " ₽";
                break;
            case self::LEADS_BY_COUNT:
                $msg = "Кол-во лидов за вчера ниже " . TextHelper::numberFormat($thresholdValue) . " и равно " . TextHelper::numberFormat($value);
                break;
            case self::LEADS_BY_COSTS:
                $msg = "Стоим-ть лида за вчера выше " . TextHelper::moneyFormat($thresholdValue, 2) . " ₽ и равна " . TextHelper::moneyFormat($value, 2) . " ₽";
                break;
            case self::VISITS_BY_COUNT:
                $msg = "Кол-во визитов на сайт за вчера ниже " . TextHelper::numberFormat($thresholdValue) . " и равно " . TextHelper::numberFormat($value);
                break;
            case self::AD_COST:
                $msg = "Расход на рекламу за вчера выше " . TextHelper::moneyFormat($thresholdValue, 2) . " ₽ и равен " . TextHelper::moneyFormat($value, 2) . " ₽";
                break;
            case self::LEADS_BY_COUNT_DEVIATION:
                $msg = "Отклонение кол-ва лидов за вчера в меньшую сторону от среднего за неделю на " . TextHelper::numberFormat($value) . " %";
                break;
            case self::LEADS_BY_COSTS_DEVIATION:
                $msg = "Отклонение стоим-ти лида за вчера в большую сторону от среднего за неделю на " . TextHelper::numberFormat($value) . " %";
                break;
            case self::AD_COST_DEVIATION:
                $msg = "Отклонение расхода на рекламу за вчера больше среднего расхода за неделю на " . TextHelper::numberFormat($value) . " %";
                break;
            default:
                $msg = "---";
                break;
        }

        return $msg;
    }
}
