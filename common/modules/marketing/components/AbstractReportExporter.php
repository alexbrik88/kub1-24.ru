<?php

namespace common\modules\marketing\components;

use common\models\Company;
use yii\base\BaseObject;

abstract class AbstractReportExporter extends BaseObject implements ReportExporterInterface
{
    /** @var string */
    protected const DEFAULT_MEDIUM = 'cpc';

    /**
     * @var Company
     */
    protected $company;

    /**
     * @param Company $company
     */
    public function __construct(Company $company)
    {
        $this->company = $company;

        parent::__construct();
    }

    /**
     * @param Company $company
     * @return static
     */
    public static function createReportExporter(Company $company): ReportExporterInterface
    {
        return new static($company);
    }

    /**
     * @param array $stats
     * @return array
     */
    protected function groupReport(array $stats): array
    {
        $groupedStats = [];

        foreach ($stats as $row) {
            $key = implode('_', [$row['date'], $row['utm_source'], $row['utm_medium'], $row['utm_campaign']]);

            if (!isset($groupedStats[$key])) {
                $groupedStats[$key] = [
                    'date' => $row['date'],
                    'campaign_name' => $row['campaign_name'],
                    'utm_medium' => $row['utm_medium'],
                    'utm_source' => $row['utm_source'],
                    'utm_campaign' => $row['utm_campaign'],
                    'cost' => (float) $row['cost'],
                    'clicks' => (int) $row['clicks'],
                    'impressions' => (int) $row['impressions'],
                ];
            } else {
                $groupedStats[$key]['cost'] += (float) $row['cost'];
                $groupedStats[$key]['clicks'] += (int) $row['clicks'];
                $groupedStats[$key]['impressions'] += (int) $row['impressions'];
            }
        }

        return $groupedStats;
    }
}
