<?php

namespace common\modules\marketing\components;

use backend\modules\employee\models\Employee;
use common\modules\import\components\ImportUnauthorizedException;
use DateTimeInterface;
use FacebookAds\Object\Ad;
use FacebookAds\Object\AdCreative;
use FacebookAds\Object\AdsInsights;
use FacebookAds\Object\Fields\AdCreativeFields;
use FacebookAds\Object\Fields\AdFields;
use FacebookAds\Object\Fields\AdsInsightsFields;
use yii\helpers\ArrayHelper;

class FacebookReportExporter extends AbstractReportExporter
{
    /** @var string */
    protected const DEFAULT_SOURCE = 'facebook';

    /**
     * @var FacebookIntegration
     */
    private $integration;

    /**
     * @var string
     */
    private $accessToken = '';

    /**
     * @var int
     */
    private $accountId = 0;

    /**
     * @var Ad[]|null
     */
    private $_ads;

    /**
     * @var AdsInsights[]|null
     */
    private $_adCreatives;

    /**
     * @inheritDoc
     */
    public function init(): void
    {
        $integrationData = $this->company->getIntegrationData(Employee::INTEGRATION_FACEBOOK);
        $this->accessToken = $integrationData->getAccessToken();
        $this->accountId = $integrationData->getAccountId();

        if (!$this->accessToken || !$this->accountId) {
            throw new ImportUnauthorizedException('Произошла ошибка при экспорте аналитики. Необходимо авторизироваться.');
        }

        $this->integration = new FacebookIntegration($this->accessToken);

        if (!$this->integration->isTokenValid()) {
            $integrationData->cleanConfig();
            $integrationData->saveConfig();
            throw new ImportUnauthorizedException('Произошла ошибка при экспорте аналитики. Необходимо авторизироваться.');
        }
    }

    /**
     * @inheritDoc
     */
    public function getBalance(): float
    {
        $balance = $this->integration->getBalance($this->accountId);
        $integrationData = $this->company->getIntegrationData(Employee::INTEGRATION_FACEBOOK);
        $integrationData->setBalance($balance);
        $integrationData->saveConfig();

        return $integrationData->getBalance();
    }

    /**
     * @inheritDoc
     */
    public function getReport(DateTimeInterface $from, DateTimeInterface $to): array
    {
        sleep(1);

        $stats = [];
        $adInsights = $this->integration->getAdInsights($this->accountId, $from, $to);

        foreach ($adInsights as $adInsight) {
            $queryParts = $this->getQueryParts($adInsight->{AdsInsightsFields::AD_ID});
            $stats[] = [
                'date' => $adInsight->{AdsInsightsFields::DATE_START},
                'campaign_name' => $adInsight->{AdsInsightsFields::CAMPAIGN_NAME},
                'cost' => $adInsight->{AdsInsightsFields::SPEND},
                'clicks' => $adInsight->{AdsInsightsFields::CLICKS},
                'impressions' => $adInsight->{AdsInsightsFields::IMPRESSIONS},
                'utm_source' => $queryParts['utm_source'] ?? self::DEFAULT_SOURCE,
                'utm_medium' => $queryParts['utm_medium'] ?? self::DEFAULT_MEDIUM,
                'utm_campaign' => $queryParts['utm_campaign'] ?? $adInsight->{AdsInsightsFields::CAMPAIGN_NAME},
            ];
        }

        return $this->groupReport($stats);
    }

    /**
     * @param int $id
     * @return Ad|null
     */
    private function getAdById(int $id): ?Ad
    {
        if ($this->_ads === null) {
            $this->_ads = ArrayHelper::index($this->integration->getAds($this->accountId), AdFields::ID);
        }

        return $this->_ads[$id] ?? null;
    }

    /**
     * @param int $id
     * @return AdCreative
     */
    private function getAdCreativeById(int $id): AdCreative
    {
        if ($this->_adCreatives === null) {
            $this->_adCreatives = ArrayHelper::index(
                $this->integration->getAdCreatives($this->accountId),
                AdCreativeFields::ID
            );
        }

        return $this->_adCreatives[$id];
    }

    /**
     * @param int $id
     * @return string[]
     * @throws
     */
    private function getQueryParts(int $id): ?array
    {
        // $linkUrl = '';
        $queryParts = [];
        $ad = $this->getAdById($id);

        if ($ad) {
            $adCreative = $this->getAdCreativeById($ad->{AdFields::CREATIVE}[AdCreativeFields::ID]);
            $linkUrl = $adCreative->{AdCreativeFields::OBJECT_STORY_SPEC}['link_data']['link'] ?? '';
            $urlParts = parse_url($linkUrl);

            if (isset($urlParts['query'])) {
                parse_str($urlParts['query'], $queryParts);
            }
        }

        return $queryParts;
    }
}
