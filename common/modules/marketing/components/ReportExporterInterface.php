<?php

namespace common\modules\marketing\components;

use common\models\Company;
use DateTimeInterface;
use yii\base\Configurable;
use Throwable;

interface ReportExporterInterface extends Configurable
{
    /**
     * @param Company $company
     * @return static
     */
    public static function createReportExporter(Company $company): self;

    /**
     * @param DateTimeInterface $from
     * @param DateTimeInterface $to
     * @return array
     * @throws Throwable
     */
    public function getReport(DateTimeInterface $from, DateTimeInterface $to): array;

    /**
     * @return float
     * @throws Throwable
     */
    public function getBalance(): float;
}
