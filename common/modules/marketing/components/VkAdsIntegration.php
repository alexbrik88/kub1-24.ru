<?php

namespace common\modules\marketing\components;

use DateTimeInterface;

final class VkAdsIntegration
{
    /**
     * @var string
     */
    private $accountId;

    /**
     * @var string
     */
    private $accessToken;

    /**
     * @param string $accessToken
     * @param string $accountId
     */
    public function __construct(string $accessToken, string $accountId)
    {
        $this->accountId = $accountId;
        $this->accessToken = $accessToken;
    }

    /**
     * @param bool $includeDeleted
     * @return array[]
     * @throws VkAdsException
     */
    public function getAdsLayout(bool $includeDeleted): array
    {
        return (array) $this->callMethod('ads.getAdsLayout', [
            'include_deleted' => (int) $includeDeleted,
        ]);
    }

    /**
     * @return float
     * @throws VkAdsException
     */
    public function getBudget(): float
    {
        return (float) $this->callMethod('ads.getBudget');
    }

    /**
     * @param bool $includeDeleted
     * @return array[]
     * @throws VkAdsException
     */
    public function getCampaigns(bool $includeDeleted): array
    {
        return (array) $this->callMethod('ads.getCampaigns', [
            'include_deleted' => (int) $includeDeleted,
        ]);
    }

    /**
     * @param int[] $adIds
     * @param DateTimeInterface $dateFrom
     * @param DateTimeInterface $dateTo
     * @return array[]
     * @throws VkAdsException
     */
    public function getStatistics(array $adIds, DateTimeInterface $dateFrom, DateTimeInterface $dateTo): array
    {
        return (array) $this->callMethod('ads.getStatistics', [
            'ids_type' => 'ad',
            'ids' => implode(',', $adIds),
            'period' => 'day',
            'date_from' => $dateFrom->format('Y-m-d'),
            'date_to' => $dateTo->format('Y-m-d'),
        ]);
    }

    /**
     * @param int[] $postIds
     * @return array[]
     * @throws VkAdsException
     */
    public function getWallById(array $postIds): array
    {
        return (array) $this->callMethod('wall.getById', [
            'posts' => implode(',', $postIds),
        ]);
    }

    /**
     * @param string $method
     * @param array $data
     * @return mixed
     * @throws VkAdsException
     */
    private function callMethod(string $method, array $data = [])
    {
        $data['access_token'] = $this->accessToken;
        $data['account_id'] = $this->accountId;

        return VkAdsComponent::getInstance()->callMethod($method, $data);
    }
}
