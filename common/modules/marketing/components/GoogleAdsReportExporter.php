<?php

namespace common\modules\marketing\components;

use common\models\employee\Employee;
use common\modules\import\components\ImportUnauthorizedException;
use DateTimeInterface;

class GoogleAdsReportExporter extends AbstractReportExporter
{
    /**
     * @var string
     */
    private $clientCustomerId;

    /**
     * @var string
     */
    private $refreshToken;

    /**
     * @var GoogleIntegration
     */
    private $integration;

    /**
     * @inheritDoc
     */
    public function init(): void
    {
        $integrationData = $this->company->getIntegrationData(Employee::INTEGRATION_GOOGLE_ADS);
        $this->refreshToken = $integrationData->getAccessToken();
        $this->clientCustomerId = $integrationData->getAccountId();

        if (!$this->refreshToken || !$this->clientCustomerId) {
            throw new ImportUnauthorizedException('Произошла ошибка при экспорте аналитики. Необходимо авторизироваться.');
        }

        $this->integration = new GoogleIntegration($this->refreshToken, $this->clientCustomerId);
    }

    /**
     * @inheritDoc
     */
    public function getBalance(): float
    {
        return 0.0; // TODO: getBalance
    }

    /**
     * @inheritDoc
     */
    public function getReport(DateTimeInterface $from, DateTimeInterface $to): array
    {
        $stats = [];
        $landingPageReport = $this->integration->generateLandingPageReport($from, $to);
        $landingPageReportData = $this->parseTsv($landingPageReport);

        foreach ($landingPageReportData as $row) {
            $urlParts = parse_url($row['Expanded landing page']);
            $queryParts = [];

            if (isset($urlParts['query'])) {
                parse_str($urlParts['query'], $queryParts);
            }

            $utmSource = $this->replaceUrlTags($queryParts['utm_source'] ?? 'google', $row);
            $utmMedium = $this->replaceUrlTags($queryParts['utm_medium'] ?? 'cpc', $row);
            $utmCampaign = $this->replaceUrlTags($queryParts['utm_campaign'] ?? $row['Campaign'], $row);

            if ($utmSource !== 'google' && $utmMedium !== 'cpc') {
                continue;
            }

            $stats[] = [
                'date' => str_replace('-', '', $row['Day']),
                'cost' => (float)$row['Cost'] / 1000000,
                'clicks' => (int)$row['Clicks'],
                'impressions' => (int)$row['Impressions'],
                'utm_medium' => $utmMedium,
                'utm_source' => $utmSource,
                'utm_campaign' => $utmCampaign,
                'campaign_name' => $row['Campaign'],
            ];
        }

        return $this->groupReport($stats);
    }

    private function parseTsv($string)
    {
        $rows = explode("\n", $string);

        array_shift($rows);
        array_pop($rows);
        array_pop($rows);

        $header = explode("\t", array_shift($rows));
        $data = [];

        foreach ($header as $key => $column) {
            foreach ($rows as $rowNumber => $row) {
                $body = explode("\t", $row);
                $data[$rowNumber][$column] = $body[$key];
            }
        }

        return $data;
    }

    private function replaceUrlTags($string, $data)
    {

        return $string;

//        preg_match_all('/(\{.*?\})/is', $string, $matches);
//
//        if (count($matches) == 0) {
//            return $string;
//        }
//
//        $tags = $matches[1];
//
//        $resultString = $string;
//
//        foreach ($tags as $tag) {
//
//            switch ($tag) {
//
//                case '{campaignid}':
//
//                    if (!isset($data['Campaign ID'])) {
//                        throw new \Exception('Replace URL tags: cannot find "Campaign ID"');
//                    }
//
//                    $resultString = str_replace($tag, $data['Campaign ID'], $resultString);
//
//                    break;
//
//                case '{keyword}':
//
//                    if (!isset($data['Keyword / Placement'])) {
//                        throw new \Exception('Replace URL tags: cannot find "Keyword / Placement"');
//                    }
//
//                    $resultString = str_replace($tag, $data['Keyword / Placement'], $resultString);
//
//                    break;
//
//                case '{network}':
//
//                    if (!isset($data['Network (with search partners)'])) {
//                        throw new \Exception('Replace URL tags: cannot find "Network (with search partners)"');
//                    }
//
//                    $network = $data['Network (with search partners)'];
//
//                    if (in_array($network, ['Google search', 'Search partners', 'Display Network'])) {
//                        if ($network === 'Google search') {
//                            $value = 'g';
//                        } elseif ($network === 'Search partners') {
//                            $value = 's';
//                        } elseif ($network === 'Display Network') {
//                            $value = 'd';
//                        }
//
//                        $resultString = str_replace($tag, $value, $resultString);
//                    }
//
//                    break;
//
//                default:
//                    break;
//
//            }
//
//        }
//
//        return $resultString;

    }
}
