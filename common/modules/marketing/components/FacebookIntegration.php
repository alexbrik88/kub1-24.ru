<?php

namespace common\modules\marketing\components;

use common\components\date\DateHelper;
use Facebook\Facebook;
use Facebook\GraphNodes\GraphUser;
use FacebookAds\Api;
use FacebookAds\Cursor;
use FacebookAds\Object\Ad;
use FacebookAds\Object\AdAccount;
use FacebookAds\Object\AdCreative;
use FacebookAds\Object\AdsInsights;
use FacebookAds\Object\Fields\AdCreativeFields;
use FacebookAds\Object\Fields\AdFields;
use FacebookAds\Object\Fields\AdsInsightsFields;
use DateTimeInterface;
use yii\helpers\ArrayHelper;

final class FacebookIntegration
{
    /** @var string[] */
    private const ADS_INSIGHTS_FIELDS = [
        AdsInsightsFields::AD_ID,
        AdsInsightsFields::CAMPAIGN_NAME,
        AdsInsightsFields::CLICKS,
        AdsInsightsFields::SPEND,
        AdsInsightsFields::IMPRESSIONS,
        AdsInsightsFields::OBJECTIVE,
    ];

    /** @var string[] */
    private const AD_CREATIVE_FIELDS = [
        AdCreativeFields::ID,
        AdCreativeFields::URL_TAGS,
        AdCreativeFields::OBJECT_STORY_SPEC,
    ];

    /** @var string[] */
    private const AD_FIELDS = [
        AdFields::ID,
        AdFields::CREATIVE,
    ];

    /**
     * @var string
     */
    private $token;

    /**
     * @var GraphUser
     */
    private $_user;

    /**
     * @var array
     */
    private $_adAccounts;

    /**
     * @var Api
     */
    private $_api;

    /**
     * @param string $token
     */
    public function __construct(string $token)
    {
        $this->token = $token;

        Cursor::setDefaultUseImplicitFetch(true);
    }

    public function isTokenValid(): bool
    {
        return $this->getFacebook()->getOAuth2Client()->debugToken($this->token)->getIsValid();
    }

    /**
     * @return GraphUser
     */
    public function getUser(): GraphUser
    {
        if ($this->_user === null) {
            $this->_user = $this->getFacebook()->get('/me?fields=adaccounts', $this->token)->getGraphUser();
        }

        return $this->_user;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->getUser()->getId();
    }

    /**
     * @return array
     * @throws
     */
    public function getAdAccounts(): array
    {
        $userId = $this->getUserId();

        if ($this->_adAccounts === null) {
            $this->_adAccounts = $this
                ->getFacebook()
                ->get("/{$userId}/adaccounts?fields=account_id,name,balance", $this->token)
                ->getGraphEdge()
                ->asArray();
        }

        return $this->_adAccounts;
    }

    /**
     * @param int $adAccountId
     * @param DateTimeInterface $dateFrom
     * @param DateTimeInterface $dateTo
     * @return AdsInsights[]
     */
    public function getAdInsights(int $adAccountId, DateTimeInterface $dateFrom, DateTimeInterface $dateTo): array
    {
        $params = [
            'level' => 'ad',
            'filtering' => [],
            'breakdowns' => [],
            'time_increment' => 1,
            'time_range' => [
                'since' => $dateFrom->format(DateHelper::FORMAT_DATE),
                'until' => $dateTo->format(DateHelper::FORMAT_DATE),
            ],
        ];

        $adAccount = new AdAccount('act_' . $adAccountId, null, $this->getApi());
        $cursor = $adAccount->getInsights(self::ADS_INSIGHTS_FIELDS, $params);

        return iterator_to_array($cursor);
    }

    /**
     * @param int $adAccountId
     * @return Ad[]
     */
    public function getAds(int $adAccountId): array
    {
        $cursor = $this->getAdAccount($adAccountId)->getAds(self::AD_FIELDS);

        return iterator_to_array($cursor);
    }

    /**
     * @param int $adAccountId
     * @return float
     */
    public function getBalance(int $adAccountId): float
    {
        $adAccounts = ArrayHelper::index($this->getAdAccounts(), 'account_id');

        return $adAccounts[$adAccountId]['balance'] / -100;
    }

    /**
     * @param int $adAccountId
     * @return AdCreative[]
     */
    public function getAdCreatives(int $adAccountId): array
    {
        $cursor = $this->getAdAccount($adAccountId)->getAdCreatives(self::AD_CREATIVE_FIELDS);

        return iterator_to_array($cursor);
    }

    /**
     * @return Facebook
     */
    protected function getFacebook(): Facebook
    {
        return FacebookComponent::getInstance()->facebook;
    }

    /**
     * @return Api
     */
    protected function getApi(): Api
    {
        if ($this->_api === null) {
            $this->_api = Api::init(
                $this->getFacebook()->getApp()->getId(),
                $this->getFacebook()->getApp()->getSecret(),
                $this->token
            );
        }

        return $this->_api;
    }

    /**
     * @param int $adAccountId
     * @return AdAccount
     */
    private function getAdAccount(int $adAccountId): AdAccount
    {
        return new AdAccount('act_' . $adAccountId, null, $this->getApi());
    }
}
