<?php

namespace common\modules\marketing\components;

use common\components\ObjectFactoryTrait;
use common\modules\import\components\AuthComponentInterface;
use GuzzleHttp\Client;
use yii\base\Component;
use yii\base\InvalidConfigException;

final class VkAdsComponent extends Component implements AuthComponentInterface
{
    use ObjectFactoryTrait;

    /** @var string */
    private const OAUTH_URL = 'https://oauth.vk.com';

    /** @var string */
    private const API_URL = 'https://api.vk.com';

    /** @var string */
    private const VERSION = '5.101';

    /** @var int */
    private const SCOPE_ADS = 32768;

    /** @var int */
    private const SCOPE_OFFLINE = 65536;

    /** @var int[] */
    private const SCOPES = [
        self::SCOPE_ADS,
        self::SCOPE_OFFLINE,
    ];

    /**
     * @var string
     */
    public $clientId;

    /**
     * @var string
     */
    public $clientSecret;

    /**
     * @var string
     */
    public $redirectUri;

    /**
     * @var Client
     */
    private $client;

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if (!isset($this->clientId, $this->clientSecret, $this->redirectUri)) {
            throw new InvalidConfigException();
        }

        $this->client = new Client();
    }

    /**
     * @param string $method
     * @param array $data
     * @return mixed
     * @throws VkAdsException
     */
    public function callMethod(string $method, array $data = [])
    {
        $url = sprintf("%s/method/%s", self::API_URL, $method);
        $query = ['client_id' => $this->clientId, 'v' => self::VERSION];
        $response = $this->client->get($url, ['query' => $data + $query]);
        $result = json_decode($response->getBody()->getContents(), true);

        if (isset($result['error'])) {
            throw new VkAdsException($result['error']['error_msg'], $result['error']['error_code']);
        }

        return $result['response'];
    }

    /**
     * @return string
     */
    public function getAuthorizationUrl(): string
    {
        $query = [
            'client_id' => $this->clientId,
            'redirect_uri' => $this->redirectUri,
            'scope' => array_sum(self::SCOPES),
            'v' => self::VERSION,
            'response_type' => 'code',
            'display' => 'page',
        ];

        return sprintf('%s/authorize?%s', self::OAUTH_URL, http_build_query($query));
    }

    /**
     * @param string $accessToken
     * @return array[]
     * @throws VkAdsException
     */
    public function getAccounts(string $accessToken): array
    {
        return (array) $this->callMethod('ads.getAccounts', [
            'access_token' => $accessToken,
            'client_secret' => $this->clientSecret,
        ]);
    }

    /**
     * @param string $code
     * @return string
     * @throws VkAdsException
     */
    public function getAccessToken(string $code): string
    {
        $url = sprintf('%s/%s', self::OAUTH_URL, 'access_token');
        $response = $this->client->get($url, [
            'query' => [
                'client_id' => $this->clientId,
                'client_secret' => $this->clientSecret,
                'redirect_uri' => $this->redirectUri,
                'code' => $code,
            ],
        ]);

        $response = json_decode($response->getBody()->getContents(), true);

        if (isset($response['error_description'])) {
            throw new VkAdsException($response['error_description'], 0);
        }

        return $response['access_token'];
    }
}
