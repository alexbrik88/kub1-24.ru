<?php

namespace common\modules\marketing\components;

use common\components\date\DateHelper;
use common\components\ObjectFactoryTrait;
use common\modules\import\components\AuthComponentInterface;
use DateTimeInterface;
use GuzzleHttp\Client;
use Yii;
use yii\base\Component;
use yii\base\InvalidArgumentException;
use yii\base\InvalidConfigException;

final class YandexDirectComponent extends Component implements AuthComponentInterface
{
    use ObjectFactoryTrait;

    const API_BASE_URL = 'https://api.direct.yandex.com/json/v5/';

    /** @var string */
    private const OAUTH_URL = 'https://oauth.yandex.ru';

    public $clientId;

    public $secret;

    public $redirectUri;

    private $accessToken;

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init(): void
    {
        if (empty($this->clientId) || empty($this->secret) || empty($this->redirectUri)) {
            throw new InvalidConfigException('clientId, secret and redirectUri is required');
        }
    }

    public function getAccessToken($code)
    {
        $context = stream_context_create([
            'http' => [
                'method' => 'POST',
                'header' => 'Content-type: application/x-www-form-urlencoded',
                'content' => $this->buildQuery($code),
            ],
        ]);

        $result = file_get_contents($this->getTokenUrl(), false, $context);
        $result = json_decode($result);

        return $result->access_token;
    }

    /**
     * @inheritDoc
     */
    public function getAuthorizationUrl(): string
    {
        return sprintf('%s/authorize?%s', self::OAUTH_URL, http_build_query([
            'force_confirm' => 'yes',
            'response_type' => 'code',
            'client_id' => $this->clientId,
            'redirect_uri' => $this->redirectUri,
        ]));
    }

    public function getTokenUrl()
    {
        return self::OAUTH_URL . '/token';
    }

    /**
     * @param DateTimeInterface $dateFrom
     * @param DateTimeInterface $dateTo
     * @return bool
     * @throws YandexDirectException
     * @throws \yii\base\Exception
     */
    public function getCampaignReportByPeriod(DateTimeInterface $dateFrom, DateTimeInterface $dateTo){
        if ($this->accessToken === null) {
            throw new InvalidArgumentException('Yandex access token can not be blank.');
        }

        $curl = $this->buildCurl('reports', json_encode([
            'params' => [
                'SelectionCriteria' => [
                    'DateFrom' => $dateFrom->format(DateHelper::FORMAT_DATE),
                    'DateTo' => $dateTo->format(DateHelper::FORMAT_DATE),
                ],
                'FieldNames' => [
                    'Date',
                    'CampaignId',
                    'CampaignName',
                    'Impressions',
                    'Clicks',
                    'Ctr',
                    'Cost',
                    'AvgCpc',
                    'AvgPageviews',
                    'ConversionRate',
                    'Revenue',
                    'CostPerConversion',
                    'Conversions',
                    'GoalsRoi',
                    'Profit',
                ],
                'ReportName' => Yii::$app->security->generateRandomString(8) . time(),
                'ReportType' => 'CAMPAIGN_PERFORMANCE_REPORT',
                'DateRangeType' => 'CUSTOM_DATE',
                'Format' => 'TSV',
                'IncludeVAT' => 'NO',
                'IncludeDiscount' => 'NO',
            ]
        ]));

        $response = $this->sendReportRequest($curl);
        if ($response === false) {
            throw new InvalidArgumentException('Cannot send request.');
        }

        return $response;
    }

    public function getCampaignPerformanceReport(DateTimeInterface $dateFrom, DateTimeInterface $dateTo)
    {
        if ($this->accessToken === null) {
            throw new InvalidArgumentException('Yandex access token can not be blank.');
        }

        $curl = $this->buildCurl('reports', json_encode([
            'params' => [
                'SelectionCriteria' => [
                    'DateFrom' => $dateFrom->format(DateHelper::FORMAT_DATE),
                    'DateTo' => $dateTo->format(DateHelper::FORMAT_DATE),
                ],
                'FieldNames' => [
                    'CampaignId',
                    'CampaignType',
                    'CampaignName',
                ],
                'ReportName' => Yii::$app->security->generateRandomString(8) . time(),
                'ReportType' => 'CAMPAIGN_PERFORMANCE_REPORT',
                'DateRangeType' => 'CUSTOM_DATE',
                'Format' => 'TSV',
                'IncludeVAT' => 'NO',
                'IncludeDiscount' => 'NO',
            ]
        ]));

        $response = $this->sendReportRequest($curl);
        if ($response === false) {
            throw new InvalidArgumentException('Cannot send request.');
        }

        return $response;
    }

    public function getCustomReport(DateTimeInterface $dateFrom, DateTimeInterface $dateTo, $campaignIds)
    {
        if ($this->accessToken === null) {
            throw new InvalidArgumentException('Yandex access token can not be blank.');
        }

        $params = [
            'params' => [
                'SelectionCriteria' => [
                    'DateFrom' => $dateFrom->format(DateHelper::FORMAT_DATE),
                    'DateTo' => $dateTo->format(DateHelper::FORMAT_DATE),
                    'Filter' => [
                        [
                            'Field'    => 'CampaignId',
                            'Operator' => 'IN',
                            'Values'   => $campaignIds,
                        ]
                    ]
                ],
                'FieldNames' => [
                    'Date',
                    'AdGroupId',
                    'AdId',
                    'Clicks',
                    'Impressions',
                    'Cost',
                    'CampaignId',
                    'CampaignType',
                    'CampaignName',
                    'AdNetworkType',
                    'Conversions',
                ],
                'ReportName' => Yii::$app->security->generateRandomString(8) . time(),
                'ReportType' => 'CUSTOM_REPORT',
                'DateRangeType' => 'CUSTOM_DATE',
                'Format' => 'TSV',
                'IncludeVAT' => 'NO',
                'IncludeDiscount' => 'NO',
            ]
        ];

        $curl = $this->buildCurl('reports', json_encode($params));

        $response = $this->sendReportRequest($curl);
        if ($response === false) {
            throw new InvalidArgumentException('Cannot send request.');
        }

        return $response;
    }

    public function getAdsNew($ids)
    {
        $curl = $this->buildCurl('ads', json_encode([
            'method' => 'get',
            'params' => [
                'SelectionCriteria' => [
                    'Ids' => $ids,
                ],
                'FieldNames' => [
                    'Id',
                    'AdGroupId',
                    'CampaignId',
                    'Type',
                    'Subtype'
                ],
                'TextAdFieldNames' => ['Href'],
                'TextImageAdFieldNames' => ['Href'],
                'TextAdBuilderAdFieldNames' => ['Href'],
                'CpmBannerAdBuilderAdFieldNames' => ['Href'],
                'CpcVideoAdBuilderAdFieldNames' => ['Href'],
                'CpmVideoAdBuilderAdFieldNames' => ['Href'],
            ],
        ]));
        $result = curl_exec($curl);
        $responseHeadersSize = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
        $responseBody = substr($result, $responseHeadersSize);

        return json_decode($responseBody, true)['result']['Ads'];
    }

    public function getAdGroupsNew($ids)
    {
        $curl = $this->buildCurl('adgroups', json_encode([
            'method' => 'get',
            'params' => [
                'SelectionCriteria' => [
                    'Ids' => $ids,
                ],
                'FieldNames' => [
                    'Id',
                    'CampaignId',
                    'Name',
                    'Type',
                    'TrackingParams'
                ],
            ],
        ]));
        $result = curl_exec($curl);
        $responseHeadersSize = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
        $responseBody = substr($result, $responseHeadersSize);

        return json_decode($responseBody, true)['result']['AdGroups'];
    }

    /**
     * @param DateTimeInterface $dateFrom
     * @param DateTimeInterface $dateTo
     * @return bool
     * @throws YandexDirectException
     * @throws \yii\base\Exception
     */
    public function getAdGroupReportByPeriod(DateTimeInterface $dateFrom, DateTimeInterface $dateTo)
    {
        if ($this->accessToken === null) {
            throw new InvalidArgumentException('Yandex access token can not be blank.');
        }

        $curl = $this->buildCurl('reports', json_encode([
            'params' => [
                'SelectionCriteria' => [
                    'DateFrom' => $dateFrom->format(DateHelper::FORMAT_DATE),
                    'DateTo' => $dateTo->format(DateHelper::FORMAT_DATE),
                ],
                'FieldNames' => [
                    'Date',
                    'AdGroupId',
                    'CampaignId',
                    'AdGroupName',
                    'Impressions',
                    'Clicks',
                    'Ctr',
                    'Cost',
                    'AvgCpc',
                    'AvgPageviews',
                    'ConversionRate',
                    'Revenue',
                    'CostPerConversion',
                    'Conversions',
                    'GoalsRoi',
                    'Profit',
                    'BounceRate',
                ],
                'ReportName' => Yii::$app->security->generateRandomString(8) . time(),
                'ReportType' => 'ADGROUP_PERFORMANCE_REPORT',
                'DateRangeType' => 'CUSTOM_DATE',
                'Format' => 'TSV',
                'IncludeVAT' => 'NO',
                'IncludeDiscount' => 'NO',
            ]
        ]));

        $response = $this->sendReportRequest($curl);
        if ($response === false) {
            throw new InvalidArgumentException('Cannot send request.');
        }

        return $response;
    }

    /**
     * @param DateTimeInterface $dateFrom
     * @param DateTimeInterface $dateTo
     * @return bool
     * @throws YandexDirectException
     * @throws \yii\base\Exception
     */
    public function getAdReportByPeriod(DateTimeInterface $dateFrom, DateTimeInterface $dateTo)
    {
        if ($this->accessToken === null) {
            throw new InvalidArgumentException('Yandex access token can not be blank.');
        }

        $curl = $this->buildCurl('reports', json_encode([
            'params' => [
                'SelectionCriteria' => [
                    'DateFrom' => $dateFrom->format(DateHelper::FORMAT_DATE),
                    'DateTo' => $dateTo->format(DateHelper::FORMAT_DATE),
                ],
                'FieldNames' => [
                    'Date',
                    'AdId',
                    'AdGroupId',
                    'CampaignId',
                    'Impressions',
                    'Clicks',
                    'Ctr',
                    'Cost',
                    'AvgCpc',
                    'AvgPageviews',
                    'ConversionRate',
                    'Revenue',
                    'CostPerConversion',
                    'Conversions',
                    'GoalsRoi',
                    'Profit',
                    'BounceRate',
                ],
                'ReportName' => Yii::$app->security->generateRandomString(8) . time(),
                'ReportType' => 'AD_PERFORMANCE_REPORT',
                'DateRangeType' => 'CUSTOM_DATE',
                'Format' => 'TSV',
                'IncludeVAT' => 'NO',
                'IncludeDiscount' => 'NO',
            ]
        ]));

        $response = $this->sendReportRequest($curl);
        if ($response === false) {
            throw new InvalidArgumentException('Cannot send request.');
        }

        return $response;
    }

    /**
     * @param array $campaignsIds
     * @return mixed
     */
    public function getCampaigns($campaignsIds)
    {
        $curl = $this->buildCurl('campaigns', json_encode([
            'method' => 'get',
            'params' => [
                'SelectionCriteria' => [
                    'Ids' => $campaignsIds,
                ],
                'FieldNames' => [
                    'Id',
                    'Type',
                    'Status',
                    'StatusClarification',
                    'DailyBudget',
                ],
                'TextCampaignFieldNames' => ['BiddingStrategy'],
                'MobileAppCampaignFieldNames' => ['BiddingStrategy'],
                'DynamicTextCampaignFieldNames' => ['BiddingStrategy'],
                'CpmBannerCampaignFieldNames' => ['BiddingStrategy'],
            ],
        ]));
        $result = curl_exec($curl);
        $responseHeadersSize = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
        $responseBody = substr($result, $responseHeadersSize);

        return json_decode($responseBody, true);
    }

    /**
     * @param array $adGroupsIds
     * @return mixed
     */
    public function getAdGroups($adGroupsIds)
    {
        $curl = $this->buildCurl('adgroups', json_encode([
            'method' => 'get',
            'params' => [
                'SelectionCriteria' => [
                    'Ids' => $adGroupsIds,
                ],
                'FieldNames' => [
                    'Id',
                    'Status',
                ],
            ],
        ]));
        $result = curl_exec($curl);
        $responseHeadersSize = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
        $responseBody = substr($result, $responseHeadersSize);

        return json_decode($responseBody, true);
    }

    public function getAds($adsIds)
    {
        $curl = $this->buildCurl('ads', json_encode([
            'method' => 'get',
            'params' => [
                'SelectionCriteria' => [
                    'Ids' => $adsIds,
                ],
                'FieldNames' => [
                    'Id',
                    'Type',
                    'Subtype',
                    'StatusClarification',
                ],
                'TextAdFieldNames' => ['Title'],
                'MobileAppAdFieldNames' => ['Title'],
                'DynamicTextAdFieldNames' => ['Text'],
                'TextImageAdFieldNames' => ['AdImageHash'],
                'MobileAppImageAdFieldNames' => ['AdImageHash'],
                'TextAdBuilderAdFieldNames' => ['Creative'],
                'MobileAppAdBuilderAdFieldNames' => ['Creative'],
                'CpcVideoAdBuilderAdFieldNames' => ['Creative'],
                'CpmBannerAdBuilderAdFieldNames' => ['Creative'],
                'CpmVideoAdBuilderAdFieldNames' => ['Creative'],
            ],
        ]));
        $result = curl_exec($curl);
        $responseHeadersSize = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
        $responseBody = substr($result, $responseHeadersSize);

        return json_decode($responseBody, true);
    }

    public function accountInfo()
    {
        $client = new Client([
            'headers' => [
                'Content-type' => 'application/json;charset=UTF-8',
            ],
        ]);
        $response = $client->post('https://api.direct.yandex.ru/live/v4/json/', [
            'json' => [
                'token' => $this->accessToken,
                'method' => 'AccountManagement',
                'param' => [
                    'Action' => 'Get',
                    'SelectionCriteria' => [
                        'Logins' => null,
                    ],
                ],
            ],
        ]);

        return json_decode($response->getBody()->getContents(), true);
    }

    public function setAccessToken($accessToken)
    {
        $this->accessToken = $accessToken;
    }

    private function getHeaders()
    {
        return [
            "Authorization: Bearer {$this->accessToken}",
            "Accept-Language: ru",
            "processingMode: auto",
        ];
    }

    private function buildQuery($code)
    {
        return http_build_query([
            'grant_type' => 'authorization_code',
            'code' => $code,
            'client_id' => $this->clientId,
            'client_secret' => $this->secret
        ]);
    }

    private function buildCurl($service, $body)
    {
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, self::API_BASE_URL . $service);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $body);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HEADER, true);
        curl_setopt($curl, CURLINFO_HEADER_OUT, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $this->getHeaders());

        return $curl;
    }

    private function sendReportRequest($curl)
    {
        while (true) { // TODO: избавиться от этого
            $result = curl_exec($curl);

            if ($result === false) {
                throw new YandexDirectException('Ошибка cURL: ' . curl_errno($curl) . ' - ' . curl_error($curl));
            }

            $responseHeadersSize = curl_getinfo($curl, CURLINFO_HEADER_SIZE);
            $responseHeaders = substr($result, 0, $responseHeadersSize);
            $responseBody = substr($result, $responseHeadersSize);
            $httpCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            $retryIn = preg_match('/retryIn: (\d+)/', $responseHeaders, $arr) ? $arr[1] : 60;

            switch ($httpCode) {
                case 200:
                    return $this->parseResponse($responseBody);
                case 201:
                case 202:
                    sleep($retryIn);
                    break;
                case 400:
                    $error = json_decode($responseBody, true)['error'];

                    throw new YandexDirectException((string) $error['error_string'], (int) $error['error_code']);
                case 500:
                    throw new YandexDirectException('При формировании отчета произошла ошибка. Пожалуйста, попробуйте повторить запрос позднее.');
                case 502:
                    throw new YandexDirectException('Время формирования отчета превысило серверное ограничение.');
                default:
                    throw new YandexDirectException('Произошла непредвиденная ошибка.');
            }
        }

        curl_close($curl);

        return false;
    }

    private function parseResponse($responseBody)
    {
        $result = [];

        $rows = explode("\n", $responseBody);

        array_shift($rows);
        array_pop($rows);
        array_pop($rows);

        $header = explode("\t", array_shift($rows));

        foreach ($header as $key => $column) {
            foreach ($rows as $rowNumber => $row) {
                $body = explode("\t", $row);
                $result[$rowNumber][$column] = $body[$key];
            }

        }

        return $result;
    }
}
