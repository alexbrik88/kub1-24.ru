<?php

namespace common\modules\marketing\components;

use common\components\ObjectFactoryTrait;
use common\modules\import\components\AuthComponentInterface;
use Google\Auth\OAuth2;
use Google_Client;
use Google_Service_Oauth2;
use Google_Service_Oauth2_Userinfo;
use yii\base\Component;
use yii\base\InvalidConfigException;

abstract class AbstractGoogleComponent extends Component implements AuthComponentInterface
{
    use ObjectFactoryTrait;

    /** @var string */
    private const AUTH_URL = 'https://accounts.google.com/o/oauth2/v2/auth';

    /** @var string */
    private const TOKEN_URL = 'https://www.googleapis.com/oauth2/v4/token';

    /**
     * @var string
     */
    public $clientId;

    /**
     * @var string
     */
    public $clientSecret;

    /**
     * @var string
     */
    public $redirectUri;

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init(): void
    {
        if (!isset($this->clientId, $this->clientSecret, $this->redirectUri)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @return string
     */
    public function getAuthorizationUrl(): string
    {
        return $this->createOAuth2()->buildFullAuthorizationUri(['prompt' => 'consent']);
    }

    /**
     * @param string $code
     * @return string[]
     */
    public function getCredentials(string $code): array
    {
        $oauth2 = $this->createOAuth2();
        $oauth2->setCode($code);

        return $oauth2->fetchAuthToken();
    }

    /**
     * @param string $accessToken
     * @return Google_Service_Oauth2_Userinfo
     */
    public function getUserInfo(string $accessToken): Google_Service_Oauth2_Userinfo
    {
        $client = $this->createClient($accessToken);
        $service = new Google_Service_Oauth2($client);

        return $service->userinfo->get();
    }

    /**
     * @param string $refreshToken
     * @return Google_Client
     */
    public function createClient(string $refreshToken): Google_Client
    {
        $client = new Google_Client([
            'client_id' => $this->clientId,
            'client_secret' => $this->clientSecret,
        ]);

        $client->setScopes($this->getScopes());
        $client->refreshToken($refreshToken);

        return $client;
    }

    /**
     * @return string[]
     */
    abstract protected function getScopes(): array;

    /**
     * @return OAuth2
     */
    private function createOAuth2(): OAuth2
    {
        return new OAuth2([
            'authorizationUri' => self::AUTH_URL,
            'tokenCredentialUri' => self::TOKEN_URL,
            'redirectUri' => $this->redirectUri,
            'clientId' => $this->clientId,
            'clientSecret' => $this->clientSecret,
            'scope' => $this->getScopes(),
            'state' => null,
        ]);
    }
}
