<?php

namespace common\modules\marketing\components;

use common\components\date\DateHelper;
use common\models\Company;
use common\modules\marketing\models\MarketingReport;
use yii\db\Exception;

class ReportImporter implements ReportImporterInterface
{
    /** @var int */
    public const INSERT_LIMIT = 1000;

    /**
     * @var Company
     */
    private $company;

    /**
     * @var int
     */
    private $sourceType;

    /**
     * @param Company $company
     * @param int $sourceType
     */
    public function __construct(Company $company, int $sourceType)
    {
        $this->company = $company;
        $this->sourceType = $sourceType;
    }

    /**
     * @inheritDoc
     */
    public static function createReportImporter(Company $company, int $sourceType): ReportImporterInterface
    {
        return new static($company, $sourceType);
    }

    /**
     * @inheritDoc
     */
    public function importReport(array $report): int
    {
        $saved = 0;

        foreach (array_chunk($report, self::INSERT_LIMIT) as $part) {
            $rows = array_map([$this, 'createRowData'], $part);
            $saved += $this->insertRows($rows);
        }

        return $saved;
    }

    /**
     * @param array[] $rows
     * @return int
     * @throws Exception
     */
    private function insertRows(array $rows): int
    {
        $columns = array_keys($rows[0]);
        $db = MarketingReport::getDb();
        $sql = $db
            ->createCommand()
            ->batchInsert(MarketingReport::tableName(), $columns, $rows)
            ->getSql() . 'ON DUPLICATE KEY UPDATE
                campaign_name = VALUES(campaign_name),
                cost = VALUES(cost),
                clicks = VALUES(clicks),
                impressions = VALUES(impressions)';

        return $db->createCommand($sql)->execute();
    }

    /**
     * @param array $data
     * @return array
     */
    private function createRowData(array $data): array
    {
        return [
            'company_id' => $this->company->id,
            'source_type' => $this->sourceType,
            'date' => date(DateHelper::FORMAT_DATE, strtotime($data['date'])),
            'campaign_name' => $data['campaign_name'],
            'utm_source' => $data['utm_source'],
            'utm_medium' => $data['utm_medium'],
            'utm_campaign' => $data['utm_campaign'],
            'cost' => $data['cost'],
            'clicks' => $data['clicks'],
            'impressions' => $data['impressions'],
        ];
    }
}
