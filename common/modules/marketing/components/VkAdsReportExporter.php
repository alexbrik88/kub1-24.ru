<?php

namespace common\modules\marketing\components;

use Carbon\Carbon;
use common\models\employee\Employee;
use common\modules\import\components\ImportUnauthorizedException;
use DateTimeInterface;
use yii\helpers\ArrayHelper;

class VkAdsReportExporter extends AbstractReportExporter
{
    /** @var string */
    private const DEFAULT_SOURCE = 'vk';

    /** @var int */
    private const REQUEST_FREQUENCY = 1;

    /** @var int */
    private const LAYOUTS_LIMIT = 50;

    /**
     * @var VkAdsIntegration
     */
    private $integration;

    /**
     * @var string[][]
     */
    private $_campaigns;

    /**
     * @var string[][]
     */
    private $_adsLayouts;

    /**
     * @var string[][]
     */
    private $_queryParts = [];

    /**
     * @inheritDoc
     */
    public function init(): void
    {
        $integrationData = $this->company->getIntegrationData(Employee::INTEGRATION_VK);
        $accessToken = $integrationData->getAccessToken();
        $accountId = $integrationData->getAccountId();

        if (!isset($accessToken, $accountId)) {
            throw new ImportUnauthorizedException('Произошла ошибка при экспорте аналитики. Необходимо авторизироваться.');
        }

        $this->integration = new VkAdsIntegration($accessToken, $accountId);
    }

    /**
     * @inheritDoc
     */
    public function getBalance(): float
    {
        $balance = $this->integration->getBudget();
        $integrationData = $this->company->getIntegrationData(Employee::INTEGRATION_VK);
        $integrationData->setBalance($balance);
        $integrationData->saveConfig();

        return $integrationData->getBalance();
    }

    /**
     * @inheritDoc
     */
    public function getReport(DateTimeInterface $from, DateTimeInterface $to): array
    {
        $stats = [];

        foreach (array_chunk($this->getAdsLayouts(), self::LAYOUTS_LIMIT, true) as $adsLayouts) {
            $adIds = array_keys($adsLayouts);
            $this->beforeRequest();
            $this->log("Get statistics by ads...");
            $apiStatistics = $this->integration->getStatistics($adIds, $from, $to);

            foreach ($apiStatistics as $row) {
                if (count($row['stats']) > 0) {
                    $this->log("Collecting stats...");
                    $adLayout = $this->getAdsLayoutById($row['id']);
                    $campaign = $this->getCampaignById($adLayout['campaign_id']);
                    $queryParts = $this->getQueryParts($row['id']);

                    // if (!in_array($adLayout['ad_format'], [1, 9, 11, 12])) {
                    //     throw new Exception('Unsupported ad format: ' . $adLayout['ad_format']);
                    // }

                    foreach ($row['stats'] as $stat) {
                        $stats[] = [
                            'date' => Carbon::parse($stat['day'])->format('Y-m-d'),
                            'campaign_name' => $campaign['name'],
                            'cost' => $stat['spent'] ?? 0,
                            'clicks' => $stat['clicks'] ?? 0,
                            'impressions' => $stat['impressions'] ?? 0,
                            'utm_source' => $queryParts['utm_source'] ?? self::DEFAULT_SOURCE,
                            'utm_medium' => $queryParts['utm_medium'] ?? self::DEFAULT_MEDIUM,
                            'utm_campaign' => $queryParts['utm_campaign'] ?? $campaign['name'],
                        ];
                    }
                }
            }
        }

        if (count($stats) === 0) {
            $this->log("No stats");
        }

        return $this->groupReport($stats);
    }

    private function log($message)
    {
        $dt = date('Y-m-d H:i:s');
        echo "[$dt] $message\n";
    }

    /**
     * @param int $id
     * @return string[]|null
     * @throws
     */
    private function getCampaignById(int $id): array
    {
        if ($this->_campaigns === null) {
            $this->beforeRequest();
            $this->log("Get campaigns...");
            $campaigns = $this->integration->getCampaigns(true);
            $this->_campaigns = ArrayHelper::index($campaigns, 'id');
        }

        return $this->_campaigns[$id];
    }

    /**
     * @param int $id
     * @return string[]|null
     * @throws
     */
    private function getAdsLayoutById(int $id): array
    {
        $adsLayouts = $this->getAdsLayouts();

        return $adsLayouts[$id];
    }

    /**
     * @return string[]|null
     * @throws
     */
    private function getAdsLayouts(): ?array
    {
        if ($this->_adsLayouts === null) {
            $this->beforeRequest();
            $this->log("Get ads layouts...");
            $this->_adsLayouts = [];
            $adsLayouts = $this->integration->getAdsLayout(true);

            if ($adsLayouts) {
                $this->_adsLayouts = ArrayHelper::index($adsLayouts, 'id');
            }
        }

        return $this->_adsLayouts;
    }

    /**
     * @param int $id
     * @return string[]
     */
    private function getQueryParts(int $id): array
    {
        if (!array_key_exists($id, $this->_queryParts)) {
            $urlParts = parse_url($this->getLinkUrl($id));
            $this->_queryParts[$id] = [];

            if (isset($urlParts['query'])) {
                parse_str($urlParts['query'], $this->_queryParts[$id]);
            }
        }

        return $this->_queryParts[$id];
    }

    /**
     * @param int $id
     * @return string
     * @throws
     */
    private function getLinkUrl(int $id): string
    {
        $adLayout = $this->getAdsLayoutById($id);

        // 1 - изображение и текст
        // 11 - адаптивный формат
        if (in_array($adLayout['ad_format'], [1, 11])) {
            return $adLayout['link_url'];
        }

        // 9 - запись в сообществе
        if ($adLayout['ad_format'] == 9) {
            $this->beforeRequest();
            $this->log("Get wall post...");

            $wallId = '-' . explode('-', $adLayout['link_url'])[1];
            $posts = $this->integration->getWallById([$wallId]);
            $post = $posts[0];

            if (isset($post['attachments']) && is_array($post['attachments'])) {
                foreach ($post['attachments'] as $attachment) {
                    if ($attachment['type'] == 'link') {
                        return $attachment['link']['url'];
                    } elseif ($attachment['type'] == 'pretty_cards') {
                        return $attachment['pretty_cards']['cards'][0]['link_url'];
                    }
                }
            }
        }

        // 12 - stories
        if ($adLayout['ad_format'] == 12) {
            // TODO:
        }

        return '';
    }

    /**
     * @return void
     */
    private function beforeRequest(): void
    {
        sleep(self::REQUEST_FREQUENCY);
    }
}
