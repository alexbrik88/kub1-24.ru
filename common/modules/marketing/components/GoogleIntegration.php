<?php

namespace common\modules\marketing\components;

use DateTimeInterface;
use Google\AdsApi\AdWords\AdWordsSession;
use Google\AdsApi\AdWords\AdWordsSessionBuilder;
use Google\AdsApi\AdWords\Reporting\ReportDownloadResult;
use Google\AdsApi\AdWords\Reporting\v201809\DownloadFormat;
use Google\AdsApi\AdWords\Reporting\v201809\ReportDownloader;
use Google\AdsApi\AdWords\Query\v201809\ReportQuery;
use Google\AdsApi\AdWords\Query\v201809\ReportQueryBuilder;
use Google\AdsApi\AdWords\v201809\cm\ReportDefinitionReportType;

class GoogleIntegration
{
    /** @var string */
    public const DEVELOPER_TOKEN = 'oaklMkuZER3cmdG9DsRsBg'; // TODO

    /**
     * @var string
     */
    private $refreshToken;

    /**
     * @var string
     */
    private $clientCustomerId;

    /**
     * @var AdWordsSession
     */
    private $_session;

    /**
     * @param string $refreshToken
     * @param string $clientCustomerId
     */
    public function __construct(string $refreshToken, string $clientCustomerId)
    {
        $this->refreshToken = $refreshToken;
        $this->clientCustomerId = $clientCustomerId;
    }

    /**
     * @param DateTimeInterface $dateFrom
     * @param DateTimeInterface $dateTo
     * @return string
     */
    public function generateLandingPageReport(DateTimeInterface $dateFrom, DateTimeInterface $dateTo): string
    {
        $query = (new ReportQueryBuilder)
            ->select([
                'Date',
                'AdGroupId',
                'CampaignId',
                'CampaignName',
                'Impressions',
                'Clicks',
                'Cost',
                'AdNetworkType2',
                'ExpandedFinalUrlString',
                'Conversions',
            ])
            ->from(ReportDefinitionReportType::LANDING_PAGE_REPORT)
            ->during($dateFrom->format('Ymd'), $dateTo->format('Ymd'))
            ->build();

        return $this->getDownloadResult($query, DownloadFormat::TSV)->getAsString();
    }

    /**
     * @return AdWordsSession
     */
    private function getSession(): AdWordsSession
    {
        if ($this->_session === null) {
            $client = GoogleAdsComponent::getInstance()->createClient($this->refreshToken);
            $this->_session = (new AdWordsSessionBuilder)
                ->withOAuth2Credential($client->getOAuth2Service())
                ->withDeveloperToken(self::DEVELOPER_TOKEN)
                ->withClientCustomerId($this->clientCustomerId)
                ->build();
        }

        return $this->_session;
    }

    /**
     * @param ReportQuery $query
     * @param string $format
     * @return ReportDownloadResult
     * @throws
     */
    private function getDownloadResult(ReportQuery $query, string $format = DownloadFormat::CSV): ReportDownloadResult
    {
        $session = $this->getSession();
        $downloader = new ReportDownloader($session);

        return $downloader->downloadReportWithAwql(sprintf('%s', $query), $format);
    }
}
