<?php

namespace common\modules\marketing\components;

use common\models\Company;
use yii\base\Configurable;
use Throwable;

interface ReportImporterInterface extends Configurable
{
    /**
     * @param Company $company
     * @param int $sourceType
     * @return static
     */
    public static function createReportImporter(Company $company, int $sourceType): self;

    /**
     * @param array[] $report
     * @return int
     * @throws Throwable
     */
    public function importReport(array $report): int;
}
