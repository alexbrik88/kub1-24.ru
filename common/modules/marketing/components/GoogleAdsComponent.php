<?php

namespace common\modules\marketing\components;

use Google_Service_AdSense;

final class GoogleAdsComponent extends AbstractGoogleComponent
{
    /** @var string */
    public const SCOPE_ADWORDS = 'https://www.googleapis.com/auth/adwords';

    /** @var string[] */
    public const SCOPES = [
        self::SCOPE_ADWORDS,
        Google_Service_AdSense::ADSENSE_READONLY
    ];

    /**
     * @inheritDoc
     */
    protected function getScopes(): array
    {
        return self::SCOPES;
    }
}
