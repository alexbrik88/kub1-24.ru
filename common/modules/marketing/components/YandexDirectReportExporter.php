<?php

namespace common\modules\marketing\components;

use common\models\employee\Employee;
use common\modules\import\components\ImportUnauthorizedException;
use DateTimeInterface;

class YandexDirectReportExporter extends AbstractReportExporter
{
    /** @var string[] https://yandex.ru/dev/direct/doc/dg/objects/campaign.html/ */
    private const CAMPAIGN_LIST = [
        'TEXT_CAMPAIGN',
        'SMART_CAMPAIGN',
        'DYNAMIC_TEXT_CAMPAIGN',
        'MOBILE_APP_CAMPAIGN',
        'MCBANNER_CAMPAIGN',
        'CPM_BANNER_CAMPAIGN',
        'CPM_DEALS_CAMPAIGN',
        'CPM_FRONTPAGE_CAMPAIGN',
    ];

    /**
     * @var YandexDirectComponent
     */
    protected $component;

    /**
     * @var string
     */
    private $accessToken;

    /**
     * @inheritDoc
     */
    public function init(): void
    {
        $integrationData = $this->company->getIntegrationData(Employee::INTEGRATION_YANDEX_DIRECT);
        $this->accessToken = $integrationData->getAccessToken();

        if (!isset($this->accessToken)) {
            throw new ImportUnauthorizedException('Произошла ошибка при экспорте аналитики. Необходимо авторизироваться.');
        }

        $this->component = YandexDirectComponent::getInstance();
        $this->component->setAccessToken($this->accessToken);
    }

    /**
     * @inheritDoc
     */
    public function getBalance(): float
    {
        $balance = $this->component->accountInfo()['data']['Accounts'][0]['Amount'];
        $integrationData = $this->company->getIntegrationData(Employee::INTEGRATION_YANDEX_DIRECT);
        $integrationData->setBalance($balance);
        $integrationData->setAccountId('0'); // TODO: это костыль
        $integrationData->saveConfig();

        return $integrationData->getBalance();
    }

    /**
     * @inheritDoc
     */
    public function getReport(DateTimeInterface $from, DateTimeInterface $to): array
    {
        $stats = [];

        $campaignPerformanceReport = $this->component->getCampaignPerformanceReport($from, $to);

        $campaignIds = array_map(function ($item) {
            return $item['CampaignId'];
        }, $campaignPerformanceReport);

        if (count($campaignIds) === 0) {
            return [];
        }

        $customReport = $this->component->getCustomReport($from, $to, $campaignIds);

        $adIds = [];
        $groupIds = [];

        foreach ($customReport as $row) {
            $adIds[] = $row['AdId'];
            $groupIds[] = $row['AdGroupId'];
        }

        $adIds = array_values(array_unique($adIds));
        $groupIds = array_values(array_unique($groupIds));

        $tmpAds = $this->component->getAdsNew($adIds);

        $ads = [];

        foreach ($tmpAds as $ad) {
            $ads[$ad['Id']] = $ad;
        }

        $tmpGroups = $this->component->getAdGroupsNew($groupIds);

        $groups = [];

        foreach ($tmpGroups as $group) {
            $groups[$group['Id']] = $group;
        }

        foreach ($customReport as $row) {
            if (!in_array($row['CampaignType'], self::CAMPAIGN_LIST)) {
//                $log['unsupported_campaigns'][$row['CampaignType']][$row['CampaignId']] = $row['CampaignName'];
                throw new \LogicException('Unsupported campaign type');
//                continue;
            }

            $href = '';

            if ($row['CampaignType'] === 'DYNAMIC_TEXT_CAMPAIGN') {

                $group = $groups[$row['AdGroupId']];

                if (!isset($group['Type']) || $group['Type'] != 'DYNAMIC_TEXT_AD_GROUP') {
                    continue;
                }

                $href = $this->getHrefFromAdGroup($group);
            } elseif (!in_array($row['CampaignType'], ['MCBANNER_CAMPAIGN', 'MOBILE_APP_CAMPAIGN'])) {
                // Проверка $ads[$row['AdId']] для временного фикса проблемы с аудиообъявлениями
                // https://pinbonus.avaza.com/project/view/24633#!tab=task-pane&groupby=ProjectSection&view=vertical&task=188551&fileview=grid
                $href = isset($ads[$row['AdId']]) ? $this->getHrefFromAd($ads[$row['AdId']]) : '';
            }

            $urlParts = $this->parseUrl($href);

            $queryParts = [];

            if (isset($urlParts['query'])) {
                parse_str($urlParts['query'], $queryParts);
            }

            if (
                !isset($queryParts['utm_medium']) ||
                !isset($queryParts['utm_source']) ||
                !isset($queryParts['utm_campaign'])
            ) {
                $missingUtmList = [];

                if (!isset($queryParts['utm_medium'])) {
                    $missingUtmList[] = 'utm_medium';
                }

                if (!isset($queryParts['utm_source'])) {
                    $missingUtmList[] = 'utm_source';
                }

                if (!isset($queryParts['utm_campaign'])) {
                    $missingUtmList[] = 'utm_campaign';
                }

//                $log['has_missing_utm'] = true;
//
//                if (array_search($row['CampaignId'], array_column($log['missing_utm'], 'campaign_id')) === false) {
//                    $log['missing_utm'][] = [
//                        'campaign_id' => $row['CampaignId'],
//                        'campaign_type' => $row['CampaignType'],
//                        'campaign_name' => $row['CampaignName'],
//                        'utm' => $missingUtmList,
//                    ];
//                }
            }

            $medium = $this->replaceUrlTags(
                ($queryParts['utm_medium'] ?? 'cpc'),
                $row
            );

            $source = $this->replaceUrlTags(
                ($queryParts['utm_source'] ?? 'yandex'),
                $row
            );

            $campaign = $this->replaceUrlTags(
                ($queryParts['utm_campaign'] ?? 'default_kub_campaign'),
                $row
            );

            if ($source !== 'yandex' && $medium !== 'cpc') {
                continue;
            }

            $stats[] = [
                'date' => str_replace('-', '', $row['Date']),
                'campaign_name' => $row['CampaignName'],
                'cost' => (float) $row['Cost'] / 1000000,
                'clicks' => (int) $row['Clicks'],
                'impressions' => (int) $row['Impressions'],
                'utm_source' => $source,
                'utm_medium' => $medium,
                'utm_campaign' => $campaign,
            ];

        }

        // Additional group
        $report = $this->groupReport($stats);

        return $report;
    }

    private function getHrefFromAd($ad)
    {
        if ($ad['Type'] == 'TEXT_AD') {

            $href = $ad['TextAd']['Href'];

        } elseif ($ad['Type'] == 'IMAGE_AD') {

            if ($ad['Subtype'] == 'TEXT_IMAGE_AD') {
                $href = $ad['TextImageAd']['Href'];
            } elseif ($ad['Subtype'] == 'TEXT_AD_BUILDER_AD') {
                $href = $ad['TextAdBuilderAd']['Href'];
            } else {
                throw new \Exception(sprintf('Unsupported ad subtype: %s / %s', $ad['Type'], $ad['Subtype']));
            }

        } elseif ($ad['Type'] == 'CPM_BANNER_AD') {

            $href = $ad['CpmBannerAdBuilderAd']['Href'];

        } elseif ($ad['Type'] == 'CPC_VIDEO_AD') {

            $href = $ad['CpcVideoAdBuilderAd']['Href'];

        } elseif ($ad['Type'] == 'CPM_VIDEO_AD') {

            $href = $ad['CpmVideoAdBuilderAd']['Href'];

        } else {

            throw new \Exception(sprintf('Unsupported ad type: %s', $ad['Type']));

        }

        return $href;
    }

    private function getHrefFromAdGroup($group)
    {
        // if (!isset($group['Type']) || $group['Type'] != 'DYNAMIC_TEXT_AD_GROUP') {
        //     throw new Exception(sprintf('Unsupported ad group type: %s', $group['Type']));
        // }

        if (strpos($group['TrackingParams'], '?') === false) {
            return '?' . $group['TrackingParams'];
        } else {
            return $group['TrackingParams'];
        }
    }

    private function parseUrl($url)
    {
        $result = false;

        // Build arrays of values we need to decode before parsing
        $entities = array('%21', '%2A', '%27', '%28', '%29', '%3B', '%3A', '%40', '%26', '%3D', '%24', '%2C', '%2F', '%3F', '%23', '%5B', '%5D');
        $replacements = array('!', '*', "'", "(", ")", ";", ":", "@", "&", "=", "$", ",", "/", "?", "#", "[", "]");

        // Create encoded URL with special URL characters decoded so it can be parsed
        // All other characters will be encoded
        $encodedURL = str_replace($entities, $replacements, urlencode($url));

        // Parse the encoded URL
        $encodedParts = parse_url($encodedURL);

        // Now, decode each value of the resulting array
        if ($encodedParts)
        {
            foreach ($encodedParts as $key => $value)
            {
                $result[$key] = urldecode(str_replace($replacements, $entities, $value));
            }
        }
        return $result;
    }

    private function replaceUrlTags($string, $data)
    {
        preg_match_all('/(\{.*?\})/is', $string, $matches);

        if (count($matches) == 0) {
            return $string;
        }

        $tags = $matches[1];

        $resultString = $string;

        foreach ($tags as $tag) {

            switch ($tag) {

                case '{campaign_id}':

                    if (!isset($data['CampaignId'])) {
                        throw new \Exception('Replace URL tags: cannot find "CampaignId"');
                    }

                    $resultString = str_replace($tag, $data['CampaignId'], $resultString);

                    break;


                case '{campaign_type}':

                    if (!isset($data['CampaignType'])) {
                        throw new \Exception('Replace URL tags ({campaign_type}): cannot find "CampaignType"');
                    }

                    $campaignType = null;

                    if ($data['CampaignType'] === 'TEXT_CAMPAIGN') {
                        $campaignType = 'type1';
                    } elseif ($data['CampaignType'] === 'MOBILE_APP_CAMPAIGN') {
                        $campaignType = 'type2';
                    } elseif ($data['CampaignType'] === 'DYNAMIC_TEXT_CAMPAIGN') {
                        $campaignType = 'type3';
                    } elseif ($data['CampaignType'] === 'SMART_CAMPAIGN') {
                        $campaignType = 'type4';
                    }

                    if ($campaignType) {
                        $resultString = str_replace($tag, $campaignType, $resultString);
                    }

                    break;

                case '{campaign_name}':

                    if (!isset($data['CampaignName'])) {
                        throw new \Exception('Replace URL tags: cannot find "CampaignName"');
                    }

                    $resultString = str_replace($tag, $data['CampaignName'], $resultString);

                    break;

                default:
                    break;

            }

        }

        return $resultString;
    }
}
