<?php

namespace common\modules\marketing\components;

use common\modules\import\components\ImportUnauthorizedException;
use DateTimeInterface;
use common\modules\marketing\models\GoogleAnalyticsProfile;
use common\modules\marketing\models\GoogleAnalyticsProfileGoal;
use Google_Client;
use Google_Service_AnalyticsReporting;
use Google_Service_AnalyticsReporting_DateRange;
use Google_Service_AnalyticsReporting_Dimension;
use Google_Service_AnalyticsReporting_GetReportsRequest;
use Google_Service_AnalyticsReporting_Metric;
use Google_Service_AnalyticsReporting_Report;
use Google_Service_AnalyticsReporting_ReportRequest;
use UnexpectedValueException;

class GoogleAnalyticsReportExporter extends AbstractReportExporter
{
    /**
     * @var GoogleAnalyticsProfileGoal
     */
    public $goal;

    /**
     * @var Google_Client
     */
    private $client;

    /**
     * @var GoogleAnalyticsProfile
     */
    private $profile;

    /**
     * @inheritDoc
     */
    public function init(): void
    {
        $this->profile = GoogleAnalyticsProfile::findByCompany($this->company);

        if (!($this->profile instanceof GoogleAnalyticsProfile)) {
            $message = 'Произошла ошибка при экспорте аналитики. Необходимо авторизироваться в Google Analytics.';

            throw new ImportUnauthorizedException($message);
        }

        $this->client = GoogleAnalyticsComponent::getInstance()->createClient($this->profile->refresh_token);
    }

    /**
     * @inheritDoc
     */
    public function getBalance(): float
    {
        return 0.0;
    }

    /**
     * @inheritDoc
     */
    public function getReport(DateTimeInterface $from, DateTimeInterface $to): array
    {
        if ($this->goal === null) {
            throw new UnexpectedValueException();
        }

        $dateRange = new Google_Service_AnalyticsReporting_DateRange();
        $dateRange->setStartDate($from->format('Y-m-d'));
        $dateRange->setEndDate($to->format('Y-m-d'));

        $date = new Google_Service_AnalyticsReporting_Dimension();
        $date->setName('ga:date');

        $source = new Google_Service_AnalyticsReporting_Dimension();
        $source->setName('ga:source');

        $medium = new Google_Service_AnalyticsReporting_Dimension();
        $medium->setName('ga:medium');

        $campaign = new Google_Service_AnalyticsReporting_Dimension();
        $campaign->setName('ga:campaign');

        $leads = new Google_Service_AnalyticsReporting_Metric();
        $leads->setExpression(sprintf('ga:goal%sCompletions', $this->goal->external_id));
        $leads->setAlias('total');

        $request = new Google_Service_AnalyticsReporting_ReportRequest();
        $request->setViewId((string)$this->profile->analytics_view_id);
        $request->setDateRanges($dateRange);
        $request->setDimensions([$date, $source, $medium, $campaign]);
        $request->setMetrics([$leads]);
//        $request->setOrderBys();

        $body = new Google_Service_AnalyticsReporting_GetReportsRequest();
        $body->setReportRequests([$request]);

        $analytics = new Google_Service_AnalyticsReporting($this->client);
        $result = $analytics->reports->batchGet($body, []);
        /** @var Google_Service_AnalyticsReporting_Report[] $reports */
        $reports = $result->getReports();
        $result = [];

        for ($reportIndex = 0; $reportIndex < count($reports); $reportIndex++) {
            $report = $reports[ $reportIndex ];
            $header = $report->getColumnHeader();
            $dimensionHeaders = $header->getDimensions();
            $metricHeaders = $header->getMetricHeader()->getMetricHeaderEntries();
            $rows = $report->getData()->getRows();

            for ($rowIndex = 0; $rowIndex < count($rows); $rowIndex++) {
                $row = $rows[$rowIndex];
                $dimensions = $row->getDimensions();
                $metrics = $row->getMetrics();
                $data = [];

                for ($i = 0; $i < count($dimensionHeaders) && $i < count($dimensions); $i++) {
                    $data[$dimensionHeaders[$i]] = $dimensions[$i];
                }

                for ($j = 0; $j < count($metrics); $j++) {
                    $values = $metrics[$j]->getValues();
                    for ($k = 0; $k < count($values); $k++) {
                        $entry = $metricHeaders[$k];
                        $data[$entry->getName()] = $values[$k];
                    }
                }

                $result[] = $data;
            }
        }

        return $result;
    }

    /**
     * @param GoogleAnalyticsProfileGoal $goal
     * @return void
     */
    public function setGoal(GoogleAnalyticsProfileGoal $goal): void
    {
        $this->goal = $goal;
    }
}
