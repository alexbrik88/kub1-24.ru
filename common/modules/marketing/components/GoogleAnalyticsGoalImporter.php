<?php

namespace common\modules\marketing\components;

use common\components\date\DateHelper;
use common\models\Company;
use common\modules\marketing\models\GoogleAnalyticsProfileGoal;
use common\modules\marketing\models\MarketingReportGoogleAnalyticsGoal;
use UnexpectedValueException;
use yii\db\Exception;

final class GoogleAnalyticsGoalImporter implements ReportImporterInterface
{
    /** @var int */
    private const INSERT_LIMIT = 1000;

    /** @var string */
    private const DATE_FORMAT = 'Ymd';

    /**
     * @var Company
     */
    private $company;

    /**
     * @var GoogleAnalyticsProfileGoal
     */
    private $goal;

    /**
     * @var int
     */
    private $sourceType;

    /**
     * @param Company $company
     * @param int $sourceType
     */
    public function __construct(Company $company, int $sourceType)
    {
        $this->company = $company;
        $this->sourceType = $sourceType;
    }

    /**
     * @inheritDoc
     */
    public static function createReportImporter(Company $company, int $sourceType): ReportImporterInterface
    {
        return new static($company, $sourceType);
    }

    /**
     * @inheritDoc
     */
    public function importReport(array $report): int
    {
        if ($this->goal === null) {
            throw new UnexpectedValueException();
        }

        $saved = 0;

        if ($report) {
            foreach (array_chunk($report, self::INSERT_LIMIT) as $part) {
                $rows = array_map([$this, 'createRowData'], $part);
                $saved += $this->insertRows($rows);
            }
        }

        return $saved;
    }

    /**
     * @param GoogleAnalyticsProfileGoal $goal
     * @return void
     */
    public function setGoal(GoogleAnalyticsProfileGoal $goal): void
    {
        $this->goal = $goal;
    }

    /**
     * @param array[] $rows
     * @return int
     * @throws Exception
     */
    private function insertRows(array $rows): int
    {
        $columns = array_keys($rows[0]);
        $db = MarketingReportGoogleAnalyticsGoal::getDb();
        $sql = $db
            ->createCommand()
            ->batchInsert(MarketingReportGoogleAnalyticsGoal::tableName(), $columns, $rows)
            ->getSql() . 'ON DUPLICATE KEY UPDATE total = VALUES(total)';

        return $db->createCommand($sql)->execute();
    }

    /**
     * @param array $data
     * @return array
     */
    private function createRowData(array $data): array
    {
        return [
            'company_id' => $this->company->id,
            'source_type' => $this->sourceType,
            'date' => DateHelper::format($data['ga:date'], DateHelper::FORMAT_DATE, self::DATE_FORMAT),
            'campaign_name' => null,
            'utm_source' => $data['ga:source'],
            'utm_medium' => $data['ga:medium'],
            'utm_campaign' => $data['ga:campaign'],
            'goal_id' => $this->goal->external_id,
            'total' => $data['total'],
        ];
    }
}
