<?php

namespace common\modules\marketing\components;

use Google_Service_Analytics;
use Google_Service_Oauth2;

final class GoogleAnalyticsComponent extends AbstractGoogleComponent
{
    /** @var string[] */
    private const SCOPES = [
        Google_Service_Analytics::ANALYTICS,
        Google_Service_Analytics::ANALYTICS_EDIT,
        Google_Service_Analytics::ANALYTICS_READONLY,
        Google_Service_Oauth2::USERINFO_PROFILE,
    ];

    /**
     * @inheritDoc
     */
    protected function getScopes(): array
    {
        return self::SCOPES;
    }
}
