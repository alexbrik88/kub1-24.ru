<?php

namespace common\modules\marketing\components;

use Exception;

class VkAdsException extends Exception
{
    /**
     * @param string $message
     * @param int $code
     */
    public function __construct(string $message, int $code)
    {
        parent::__construct($message, $code);
    }
}
