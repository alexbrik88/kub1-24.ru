<?php

namespace common\modules\marketing\components;

use common\components\ObjectFactoryTrait;
use common\modules\import\components\AuthComponentInterface;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;
use yii\base\Component;
use yii\base\InvalidConfigException;

final class FacebookComponent extends Component implements AuthComponentInterface
{
    use ObjectFactoryTrait;

    /** @var string */
    private const DEFAULT_GRAPH_VERSION = 'v12.0';

    /** @var string[] */
    private const AUTHORIZATION_SCOPE = ['ads_management'];

    /**
     * @var string
     */
    public $clientId;

    /**
     * @var string
     */
    public $clientSecret;

    /**
     * @var string
     */
    public $redirectUri;

    /**
     * @var Facebook
     */
    public $facebook;

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     * @throws FacebookSDKException
     */
    public function init()
    {
        if (empty($this->clientId) || empty($this->clientSecret) || empty($this->redirectUri)) {
            throw new InvalidConfigException();
        }

        $this->facebook = new Facebook([
            'app_id' => $this->clientId,
            'app_secret' => $this->clientSecret,
            'default_graph_version' => self::DEFAULT_GRAPH_VERSION,
        ]);
    }

    /**
     * @param string $code
     * @return string
     * @throws FacebookSDKException
     * @throws FacebookResponseException
     */
    public function getAccessToken(string $code): string
    {
        return $this->facebook->getOAuth2Client()->getAccessTokenFromCode($code, $this->redirectUri);
    }

    /**
     * @return string
     */
    public function getAuthorizationUrl(): string
    {
        $fb = $this->facebook;

        return $fb->getOAuth2Client()->getAuthorizationUrl($this->redirectUri, '', self::AUTHORIZATION_SCOPE);
    }
}
