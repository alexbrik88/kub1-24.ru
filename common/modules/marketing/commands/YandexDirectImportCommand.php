<?php

namespace common\modules\marketing\commands;

use common\modules\marketing\components\YandexDirectReportExporter;
use common\modules\marketing\models\MarketingChannel;

final class YandexDirectImportCommand extends AbstractImportCommand
{
    /**
     * @inheritDoc
     */
    public static function getImportCommandName(): string
    {
        return 'Импорт отчета из Яндекс.Директ';
    }

    /**
     * @inheritDoc
     */
    public static function getImportCommandLabel(): string
    {
        return 'Яндекс.Директ';
    }

    /**
     * @inheritDoc
     */
    protected function getChannelType(): int
    {
        return MarketingChannel::YANDEX_AD;
    }

    /**
     * @inheritDoc
     */
    protected function getSource(): string
    {
        return 'yandex';
    }

    /**
     * @inheritDoc
     */
    protected function getExporterClass(): string
    {
        return YandexDirectReportExporter::class;
    }
}
