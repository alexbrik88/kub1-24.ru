<?php

namespace common\modules\marketing\commands;

use common\modules\marketing\components\VkAdsReportExporter;
use common\modules\marketing\models\MarketingChannel;

final class VkAdsImportCommand extends AbstractImportCommand
{
    /**
     * @inheritDoc
     */
    public static function getImportCommandName(): string
    {
        return 'Импорт отчета из Vkontakte';
    }

    /**
     * @inheritDoc
     */
    public static function getImportCommandLabel(): string
    {
        return 'Вконтакте';
    }

    /**
     * @inheritDoc
     */
    protected function getChannelType(): int
    {
        return MarketingChannel::VKONTAKTE;
    }

    /**
     * @inheritDoc
     */
    protected function getSource(): string
    {
        return 'vk';
    }

    /**
     * @return string
     */
    protected function getExporterClass(): string
    {
        return VkAdsReportExporter::class;
    }
}
