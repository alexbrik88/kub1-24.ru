<?php

namespace common\modules\marketing\commands;

use common\commands\CommandInterface;
use common\components\date\DateHelper;
use common\modules\import\components\ImportJobHandler;
use common\modules\import\factories\DateTimeFactory;
use common\modules\import\factories\ImportJobFactory;
use common\modules\marketing\models\MarketingSettings;

class MarketingAutoImportCommand implements CommandInterface
{
    /**
     * @var DateTimeFactory
     */
    private DateTimeFactory $dateTimeFactory;

    /**
     *
     */
    public function __construct()
    {
        $this->dateTimeFactory = new DateTimeFactory();
    }

    /**
     * @inheritDoc
     */
    public function run(): void
    {
        $marketingSettings = MarketingSettings::find()->where([
            'auto_import_type' => MarketingSettings::AUTO_IMPORT_TYPE_EVERY_DAY
        ])->all();

        array_walk($marketingSettings, [$this, 'createJob']);
    }

    /**
     * @param MarketingSettings $marketingSettings
     * @return void
     */
    private function createJob(MarketingSettings $marketingSettings): void
    {
        $importJob = (new ImportJobFactory)->createJob(
            $marketingSettings->company->getEmployeeChief(),
            $marketingSettings->command_type,
            $this->createJobParams(),
            true
        );

        ImportJobHandler::getInstance()->dispatchJob($importJob);
    }

    /**
     * @return string[]
     * @throws
     */
    private function createJobParams(): array
    {
        return [
            'from' => $this->dateTimeFactory->createDateFrom()->format(DateHelper::FORMAT_DATETIME),
            'to' => $this->dateTimeFactory->createDateTo()->format(DateHelper::FORMAT_DATETIME),
        ];
    }
}
