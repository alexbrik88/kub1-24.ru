<?php

namespace common\modules\marketing\commands;

use common\components\date\DatePeriodCaller;
use common\models\Company;
use common\modules\import\components\ImportParamsHelper;
use common\modules\marketing\models\MarketingNotificationSender;
use common\modules\import\commands\ImportCommandInterface;
use common\modules\import\commands\ImportCommandTrait;
use common\modules\import\models\ImportJobData;
use DateTimeInterface;
use common\modules\marketing\components\GoogleAnalyticsGoalImporter;
use common\modules\marketing\components\GoogleAnalyticsReportExporter;
use common\modules\marketing\components\ReportExporterInterface;
use common\modules\marketing\components\ReportImporter;
use common\modules\marketing\components\ReportImporterInterface;

abstract class AbstractImportCommand implements ImportCommandInterface
{
    use ImportCommandTrait;

    /**
     * @var Company
     */
    private $company;

    /**
     * @var DateTimeInterface
     */
    private $dateFrom;

    /**
     * @var DateTimeInterface
     */
    private $dateTo;

    /**
     * @var ReportExporterInterface
     */
    private $_reportExporter;

    /**
     * @var ReportImporterInterface
     */
    private $_reportImporter;

    /**
     * @var DatePeriodCaller
     */
    private $periodCaller;

    /**@param ImportJobData $jobData
     */
    public function __construct(ImportJobData $jobData) {
        $helper = new ImportParamsHelper($jobData);
        $this->dateFrom = $helper->getDateFrom();
        $this->dateTo = $helper->getDateTo();
        $this->company = $jobData->company;
        $this->periodCaller = new DatePeriodCaller($this->dateFrom, $this->dateTo, [$this, 'importPeriod']);
    }

    /**
     * @inheritDoc
     */
    public static function createImportCommand(ImportJobData $jobData): ImportCommandInterface
    {
        return new static($jobData);
    }

    /**
     * @inheritDoc
     */
    public function run(): void
    {
        $this->periodCaller->callMonthly();
        $this->updateBalance();
    }

    /**
     * @param DateTimeInterface $from
     * @param DateTimeInterface $to
     * @return bool
     * @throws
     */
    public function importPeriod(DateTimeInterface $from, DateTimeInterface $to): bool
    {
        $exporter = $this->getExporter();
        $report = $exporter->getReport($from, $to);
        $importer = $this->getImporter();
        $this->savedCount += $importer->importReport($report);
        $this->importGoals($from, $to);

        return true;
    }

    /**
     * @param DateTimeInterface $from
     * @param DateTimeInterface $to
     * @return void
     * @throws
     */
    protected function importGoals(DateTimeInterface $from, DateTimeInterface $to): void
    {
        // Получение статы из ГА по целям
        $profile = $this->company->googleAnalyticsProfile;

        if ($profile === null) {
            return;
        }

        $exporter = GoogleAnalyticsReportExporter::createReportExporter($this->company);
        $importer = GoogleAnalyticsGoalImporter::createReportImporter($this->company, $this->getChannelType());

        if ($profile && count($profile->goals) > 0) {
            foreach ($profile->goals as $goal) {
                $exporter->setGoal($goal);
                $importer->setGoal($goal);
                $gaReport = $exporter->getReport($from, $to);
                $importer->importReport(array_filter($gaReport, function (array $row): bool {
                    if ($row['ga:source'] !== $this->getSource() || $row['ga:medium'] !== 'cpc') {
                        return false;
                    }

                    return true;
                }));
            }
        }
    }

    /**
     * @return void
     * @throws
     */
    protected function updateBalance(): void
    {
        $balance = $this->getExporter()->getBalance();
        $notification = new MarketingNotificationSender();
        $notification->setCompany($this->company);
        $notification->setChannel($this->getChannelType());
        $notification->triggerBalanceEvent($balance);
    }

    /**
     * @return int
     */
    abstract protected function getChannelType();

    /**
     * @return string
     */
    abstract protected function getSource();

    /**
     * @return string|ReportExporterInterface
     */
    abstract protected function getExporterClass(): string;

    /**
     * @return ReportExporterInterface
     */
    private function getExporter(): ReportExporterInterface
    {
        if ($this->_reportExporter === null) {
            $class = $this->getExporterClass();
            $this->_reportExporter = $class::createReportExporter($this->company);
        }

        return $this->_reportExporter;
    }

    /**
     * @return ReportImporterInterface
     */
    private function getImporter(): ReportImporterInterface
    {
        if ($this->_reportImporter === null) {
            $this->_reportImporter = ReportImporter::createReportImporter($this->company, $this->getChannelType());
        }

        return $this->_reportImporter;
    }
}
