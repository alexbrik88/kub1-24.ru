<?php

namespace common\modules\marketing\commands;

use common\modules\marketing\components\GoogleAdsReportExporter;
use common\modules\marketing\models\MarketingChannel;

final class GoogleAdsImportCommand extends AbstractImportCommand
{
    /**
     * @inheritDoc
     */
    public static function getImportCommandName(): string
    {
        return 'Импорт отчета из GoogleAds';
    }

    /**
     * @inheritDoc
     */
    public static function getImportCommandLabel(): string
    {
        return 'GoogleAds';
    }

    /**
     * @inheritDoc
     */
    protected function getChannelType(): int
    {
        return MarketingChannel::GOOGLE_AD;
    }

    /**
     * @inheritDoc
     */
    protected function getSource(): string
    {
        return 'google';
    }

    /**
     * @inheritDoc
     */
    protected function getExporterClass(): string
    {
        return GoogleAdsReportExporter::class;
    }
}
