<?php

namespace common\modules\marketing\commands;

use common\modules\marketing\components\FacebookReportExporter;
use common\modules\marketing\models\MarketingChannel;

final class FacebookImportCommand extends AbstractImportCommand
{
    /**
     * @inheritDoc
     */
    public static function getImportCommandName(): string
    {
        return 'Импорт отчета из Facebook';
    }

    /**
     * @inheritDoc
     */
    public static function getImportCommandLabel(): string
    {
        return 'Facebook';
    }

    /**
     * @inheritDoc
     */
    protected function getChannelType(): int
    {
        return MarketingChannel::FACEBOOK;
    }

    /**
     * @inheritDoc
     */
    protected function getSource(): string
    {
        return 'facebook';
    }

    /**
     * @inheritDoc
     */
    protected function getExporterClass(): string
    {
        return FacebookReportExporter::class;
    }
}
