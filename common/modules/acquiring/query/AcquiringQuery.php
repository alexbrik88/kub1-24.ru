<?php

namespace common\modules\acquiring\query;

use common\modules\acquiring\models\AcquiringOperation;
use yii\db\ActiveQuery;

class AcquiringQuery extends ActiveQuery
{
    public function byFlowType($flowType)
    {
        return $this->andWhere([
            'flow_type' => $flowType,
        ]);
    }

    public function byCompany($companyId)
    {
        return $this->andWhere([
            'company_id' => $companyId,
        ]);
    }

    public function selectSum($as = 'sum')
    {
        return $this->addSelect('SUM(IF(flow_type = ' . AcquiringOperation::FLOW_TYPE_INCOME . ', `amount`, -`amount`))' . ($as !== null ? ' AS ' . $as : ''));
    }

    public function getTableAlias()
    {
        return $this->getTableNameAndAlias()[1];
    }
}
