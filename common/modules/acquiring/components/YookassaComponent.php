<?php

namespace common\modules\acquiring\components;

use common\components\ObjectFactoryTrait;
use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\GuzzleException;
use yii\base\Component;
use yii\base\InvalidConfigException;

final class YookassaComponent extends Component
{
    use ObjectFactoryTrait;

    /** @var string */
    private const OAUTH_URL = 'https://yookassa.ru/oauth/v2/authorize';

    /** @var string */
    private const TOKEN_URL = 'https://yookassa.ru/oauth/v2/token';

    /**
     * @var string
     */
    public $clientId;

    /**
     * @var string
     */
    public $clientSecret;

    /**
     * @inheritDoc
     * @throws InvalidConfigException
     */
    public function init(): void
    {
        if (!isset($this->clientId, $this->clientSecret)) {
            throw new InvalidConfigException();
        }
    }

    /**
     * @return string
     */
    public function getAuthorizationUrl(): string
    {
        return sprintf('%s?%s', self::OAUTH_URL, http_build_query([
            'client_id' => $this->clientId,
            'response_type' => 'code',
            'state' => uniqid(),
        ]));
    }

    /**
     * @param string $code
     * @return array
     * @throws YookassaException
     */
    public function getCredentials(string $code): array
    {
        try {
            $client = new GuzzleClient();
            $response = $client->request('POST', self::TOKEN_URL, [
                'auth' => [$this->clientId, $this->clientSecret],
                'form_params' => ['grant_type' => 'authorization_code', 'code' => $code],
            ]);

            if ($response->getStatusCode() !== 200) {
                throw new YookassaException();
            }

            return json_decode($response->getBody()->getContents(), true);
        } catch (GuzzleException $exception) {
            throw new YookassaException($exception->getMessage(), $exception->getCode(), $exception);
        }
    }
}
