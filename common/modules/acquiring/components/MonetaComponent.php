<?php

namespace common\modules\acquiring\components;

use common\components\ObjectFactoryTrait;
use DateTimeInterface;
use GuzzleHttp\Client;
use InvalidArgumentException;
use yii\base\Component;

final class MonetaComponent extends Component
{
    use ObjectFactoryTrait;

    const API_URL = 'https://service.moneta.ru/services';

    const COMPANY_INN = '1215192632';
    const COMPANY_KPP = '121501001';

    const STATUS_SUCCEED = 'SUCCEED';

    const CATEGORY_BUSINESS = 'BUSINESS';
    const CATEGORY_WITHDRAWAL = 'WITHDRAWAL';

    private $client;

    private $username;

    private $password;

    public function findOperationsList(DateTimeInterface $dateFrom, DateTimeInterface $dateTo, $pageNumber = null)
    {
        $response = $this->getClient()->post(self::API_URL, [
            'json' => [
                'Envelope' => [
                    'Header' => $this->getHeader(),
                    'Body' => [
                        'FindOperationsListRequest' => [
                            'filter' => [
                                'dateFrom' => $dateFrom->format('Y-m-d\TH:i:s.vP'),
                                'dateTo' => $dateTo->format('Y-m-d\TH:i:s.vP'),
                            ],
                            'pager' => [
                                'pageNumber' => $pageNumber ?? 1,
                                'pageSize' => 100,
                            ],
                        ],
                    ],
                ],
            ],
        ]);

        return json_decode($response->getBody()->getContents(), true);
    }

    public function getProfileInfo()
    {
        $response = $this->getClient()->post(self::API_URL, [
            'json' => [
                'Envelope' => [
                    'Header' => $this->getHeader(),
                    'Body' => [
                        'GetProfileInfoRequest' => [
                            'version' => 'VERSION_2',
                        ],
                    ],
                ],
            ],
        ]);

        return json_decode($response->getBody()->getContents(), true);
    }

    public function GetOperationDetailsById($operationId)
    {
        $response = $this->getClient()->post(self::API_URL, [
            'json' => [
                'Envelope' => [
                    'Header' => $this->getHeader(),
                    'Body' => [
                        'GetOperationDetailsByIdRequest' => $operationId,
                    ],
                ],
            ],
        ]);

        return json_decode($response->getBody()->getContents(), true);
    }

    public function hasCreds()
    {
        return $this->username !== null && $this->password !== null;
    }

    public function setUsername($value)
    {
        $this->username = $value;
    }

    public function setPassword($value)
    {
        $this->password = $value;
    }

    private function getHeader()
    {
        if (!$this->hasCreds()) {
            throw new InvalidArgumentException('username and password params should be specified.');
        }

        return [
            'Security' => [
                'UsernameToken' => [
                    'Username' => $this->username,
                    'Password' => $this->password,
                ],
            ],
        ];
    }

    private function getClient()
    {
        if ($this->client === null) {
            $this->client = new Client([
                'headers' => [
                    'Content-type' => 'application/json;charset=UTF-8',
                ],
            ]);
        }

        return $this->client;
    }
}
