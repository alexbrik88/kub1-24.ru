<?php

namespace common\modules\acquiring\commands;

use common\components\data\DataChunkCaller;
use common\components\date\DateHelper;
use common\components\date\DatePeriodCaller;
use common\models\document\InvoiceIncomeItem;
use common\modules\import\commands\ImportCommandInterface;
use common\modules\import\commands\ImportCommandTrait;
use common\modules\import\components\ImportParamsHelper;
use common\modules\import\models\ImportJobData;
use common\modules\acquiring\models\YookassaIdentity;
use DateTime;
use DateTimeInterface;
use common\modules\acquiring\models\Acquiring;
use common\modules\acquiring\models\AcquiringOperation;
use yii\db\Exception;
use YooKassa\Client;
use YooKassa\Common\Exceptions\ApiException;
use YooKassa\Common\Exceptions\ExtensionNotFoundException;
use YooKassa\Model\PaymentStatus;
use YooKassa\Request\Payments\PaymentsRequest;
use YooKassa\Model\Payment;

final class YookassaImportCommand implements ImportCommandInterface
{
    use ImportCommandTrait;

    /**
     * @var YookassaIdentity|null
     */
    private $identity;

    /**
     * @var DateTimeInterface
     */
    private $dateFrom;

    /**
     * @var DateTimeInterface
     */
    private $dateTo;

    /**
     * @var Client
     */
    private $client;

    /**
     * @var DatePeriodCaller
     */
    private $periodCaller;

    /**
     * @var DataChunkCaller
     */
    private $chunkCaller;

    /**
     * @param ImportJobData $jobData
     */
    public function __construct(ImportJobData $jobData)
    {
        $helper = new ImportParamsHelper($jobData);
        $this->dateFrom = $helper->getDateFrom();
        $this->dateTo = $helper->getDateTo();
        $this->client = new Client();
        $this->periodCaller = new DatePeriodCaller($this->dateFrom, $this->dateTo, [$this, 'importPeriod']);
        $this->chunkCaller = new DataChunkCaller([$this, 'savePayments']);
        $this->identity = YookassaIdentity::findOne([
            'company_id' => $jobData->company->id,
            'account_id' => $helper->getIdentifier(),
        ]);
    }

    /**
     * @inheritDoc
     */
    public static function createImportCommand(ImportJobData $jobData): ImportCommandInterface
    {
        return new static($jobData);
    }

    /**
     * @inheritDoc
     */
    public static function getImportCommandName(): string
    {
        return 'Выгрузка операций ЮKassa';
    }

    /**
     * @inheritDoc
     */
    public static function getImportCommandLabel(): string
    {
        return 'ЮKassa';
    }

    /**
     * @inheritDoc
     */
    public function run(): void
    {
        if ($this->identity instanceof YookassaIdentity) {
            $this->client->setAuthToken($this->identity->access_token);
            $this->periodCaller->callMonthly();
        }
    }

    /**
     * @param DateTimeInterface $from
     * @param DateTimeInterface $to
     * @return bool
     * @throws ApiException
     * @throws ExtensionNotFoundException
     */
    public function importPeriod(DateTimeInterface $from, DateTimeInterface $to): bool
    {
        $this->savedCount += $this->chunkCaller->callChunk($this->fetchPayments($from, $to));

        return true;
    }

    /**
     * @param Payment[] $payments
     * @return int
     * @throws Exception
     */
    public function savePayments(array $payments): int
    {
        $rows = array_map([$this, 'createRowData'], $payments);
        $columns = array_keys($rows[0]);
        $db = AcquiringOperation::getDb();
        $sql = $db
            ->createCommand()
            ->batchInsert(AcquiringOperation::tableName(), $columns, $rows)
            ->getSql() . 'ON DUPLICATE KEY UPDATE
                flow_type = VALUES(flow_type),
                amount = VALUES(amount),
                date = VALUES(date),
                datetime = VALUES(datetime),
                recognition_date = VALUES(recognition_date),
                expenditure_item_id = NULL';

        return $db->createCommand($sql)->execute();
    }

    /**
     * @param Payment $payment
     * @return array
     */
    private function createRowData(Payment $payment): array
    {
        return [
            'acquiring_id' => $this->identity->acquiring->id,
            'company_id' => $this->identity->company->id,
            'flow_type' => AcquiringOperation::FLOW_TYPE_INCOME,
            'acquiring_identifier' => $this->identity->account_id,
            'date' => $payment->getCapturedAt()->format(DateHelper::FORMAT_DATE),
            'datetime' => $payment->getCapturedAt()->format(DateHelper::FORMAT_DATETIME),
            'recognition_date' => $payment->getCapturedAt()->format(DateHelper::FORMAT_DATE),
            'operation_id' => $payment->getId(),
            'amount' => $payment->getAmount()->getValue() * 100,
            'description' => (string) $payment->getDescription(),
            'expenditure_item_id' => null,
            'income_item_id' => InvoiceIncomeItem::ITEM_PAYMENT_FROM_BUYER,
            'acquiring_type' => Acquiring::TYPE_YOOKASSA,
        ];
    }

    /**
     * @param DateTimeInterface $from
     * @param DateTimeInterface $to
     * @return Payment[]
     * @throws ApiException
     * @throws ExtensionNotFoundException
     */
    private function fetchPayments(DateTimeInterface $from, DateTimeInterface $to): array
    {
        $payments = [];
        $cursor = null;

        while (true) {
            $request = $this->createRequest($from, $to, $cursor);
            $response = $this->client->getPayments($request);
            $payments = array_merge($payments, $response->getItems());

            if (!$response->hasNextCursor()) {
                break;
            }

            $cursor = $response->getNextCursor();
        }

        return $payments;
    }

    /**
     * @param DateTimeInterface $from
     * @param DateTimeInterface $to
     * @param string $cursor
     * @return PaymentsRequest
     * @throws
     */
    private function createRequest(
        DateTimeInterface $from,
        DateTimeInterface $to,
        string $cursor = null
    ): PaymentsRequest {
        $beginDate = new DateTime();
        $beginDate->setTimestamp($from->getTimestamp());
        $beginDate->setTime(0, 0, 0, 0);

        $endDate = new DateTime();
        $endDate->setTimestamp($to->getTimestamp());
        $endDate->setTime(23, 59, 59, 999999);

        $request = new PaymentsRequest();
        $request->setStatus(PaymentStatus::SUCCEEDED);
        $request->setCreatedAtGte($beginDate);
        $request->setCreatedAtLte($endDate);
        $request->setLimit(PaymentsRequest::MAX_LIMIT_VALUE);

        if ($cursor) {
            $request->setCursor($cursor);
        }

        return $request;
    }
}
