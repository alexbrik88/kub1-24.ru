<?php

namespace common\modules\acquiring\commands;

use common\commands\CommandInterface;
use common\modules\acquiring\models\YookassaIdentity;
use YooKassa\Client;
use YooKassa\Common\Exceptions\ApiException;
use YooKassa\Common\Exceptions\ExtensionNotFoundException;

final class YookassaRemoveWebHookCommand implements CommandInterface
{
    /**
     * @inheritDoc
     */
    public function run(): void
    {
        /** @var YookassaIdentity[] $identities */
        $identities = YookassaIdentity::find()->all();

        array_walk($identities, [static::class, 'removeWebHooks']);
    }

    /**
     * @param YookassaIdentity $identity
     * @throws ApiException
     * @throws ExtensionNotFoundException
     */
    private static function removeWebHooks(YookassaIdentity $identity): void
    {
        $client = new Client();
        $client->setAuthToken($identity->access_token);

        foreach ($client->getWebhooks()->getItems() as $webHook) {
            $client->removeWebhook($webHook->getId());
        }
    }
}
