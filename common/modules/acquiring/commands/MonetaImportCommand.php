<?php

namespace common\modules\acquiring\commands;

use common\components\DadataClient;
use common\components\date\DateHelper;
use common\modules\acquiring\components\MonetaComponent;
use common\models\Contractor;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use common\modules\import\commands\ImportCommandInterface;
use common\modules\import\commands\ImportCommandTrait;
use common\modules\import\components\ImportParamsHelper;
use common\modules\import\models\ImportJobData;
use common\modules\acquiring\models\MonetaIdentity;
use DateTime;
use DateTimeInterface;
use Exception;
use RuntimeException;
use common\modules\acquiring\models\Acquiring;
use common\modules\acquiring\models\AcquiringOperation;
use GuzzleHttp\Exception\ClientException;
use yii\base\Model;

final class MonetaImportCommand implements ImportCommandInterface
{
    use ImportCommandTrait;

    /**
     * @var MonetaIdentity
     */
    public $identity;

    /**
     * @var DateTimeInterface
     */
    public $dateFrom;

    /**
     * @var DateTimeInterface
     */
    public $dateTo;

    /**
     * @var MonetaComponent
     */
    private $moneta;

    /**
     * @var Contractor[]
     */
    private $contractorCache = [];

    /**
     * @param ImportJobData $jobData
     */
    public function __construct(ImportJobData $jobData)
    {
        $helper = new ImportParamsHelper($jobData);
        $this->moneta = MonetaComponent::getInstance();
        $this->dateFrom = $helper->getDateFrom();
        $this->dateTo = $helper->getDateTo();
        $this->identity = MonetaIdentity::findOne([
            'company_id' => $jobData->company->id,
            'account_id' => $helper->getIdentifier(),
        ]);
    }

    /**
     * @inheritDoc
     */
    public static function createImportCommand(ImportJobData $jobData): ImportCommandInterface
    {
        return new static($jobData);
    }

    /**
     * @inheritDoc
     */
    public static function getImportCommandName(): string
    {
        return 'Выгрузка операций из Монета';
    }

    /**
     * @return string
     */
    public static function getImportCommandLabel(): string
    {
        return 'Монета';
    }

    /**
     * @inheritDoc
     */
    public function run(): void
    {
        if ($this->identity instanceof MonetaIdentity) {
            $this->moneta->setUsername($this->identity->username);
            $this->moneta->setPassword($this->identity->password);
            $this->pullMonetaOperations();
        }
    }

    /**
     * @param int|null $pageNumber
     * @throws Exception
     */
    private function pullMonetaOperations(int $pageNumber = null): void
    {
        try {
            $response = $this->moneta->findOperationsList($this->dateFrom, $this->dateTo, $pageNumber);

            $this->parseResponse($response);
        } catch (ClientException $e) {
            $errorResponse = json_decode($e->getResponse()->getBody(), true);
            $errorCode = isset($errorResponse['Envelope']['Body']['fault']['detail']['faultDetail']) ?? null;
            $message = 'Произошла ошибка при экспорте операций.';

            if ($errorCode === '300.1' || $errorCode == 200) {
                $message .= ' Неверный логин или пароль.';
            }

            throw new Exception($message);
        }
    }

    /**
     * @param $response
     * @throws Exception
     */
    private function parseResponse(array $response): void
    {
        $operations = $response['Envelope']['Body']['FindOperationsListResponse'];
        $pageNumber = $operations['pageNumber'];
        $pagesCount = $operations['pagesCount'];
        $monetaSeller = $this->getContractor(
            Contractor::TYPE_SELLER,
            $this->moneta::COMPANY_INN,
            $this->moneta::COMPANY_KPP
        );

        if ($operations['size'] == 0) {
            return;
        }

        foreach ($operations['operation'] as $operation) {
            $parsedAttributes = $this->parseOperationAttributes($operation);

            if ($parsedAttributes['statusid'] !== $this->moneta::STATUS_SUCCEED) {
                continue;
            }

            $date = new DateTime($parsedAttributes['modified']);
            $baseAttributes = [
                'operationId' => $parsedAttributes['id'],
                'date' => $date->format(DateHelper::FORMAT_USER_DATE),
                'dateTime' => $date->format('H:i:s'),
            ];

            switch ($parsedAttributes['category']) {
                case $this->moneta::CATEGORY_BUSINESS:
                    preg_match('/(?<=№)[0-9]+/', $parsedAttributes['description'], $orderNumber);
                    $orderNumber = current($orderNumber);
                    $sourceAmount = abs((float)$parsedAttributes['sourceamount']);
                    if (isset($parsedAttributes['isrefund'])) {
                        if ($this->hasOperation($parsedAttributes['id'], AcquiringOperation::FLOW_TYPE_EXPENSE)) {
                            continue 2;
                        }

                        $originalOperationAttributes = $this->getMonetaOperation($parsedAttributes['parentid']);
                        $operationSeller = $this->getOperationContractor(
                            Contractor::TYPE_SELLER,
                            $originalOperationAttributes['customfield:customer'] ?? null,
                            $this->getSiteFromDescription($originalOperationAttributes['description']),
                            $originalOperationAttributes['cardnumber'] ?? null
                        );

                        $this->createOperation(array_merge($baseAttributes, [
                            'flowType' => AcquiringOperation::FLOW_TYPE_EXPENSE,
                            'contractorId' => $operationSeller->id,
                            'itemId' => InvoiceExpenditureItem::ITEM_RETURN_FOUNDER,
                            'amount' => $sourceAmount,
                            'description' => "Возврат по \"{$parsedAttributes['description']}\"",
                            'recognitionDate' => $date->format(DateHelper::FORMAT_DATE),
                        ]));

                        $this->savedCount++;
                    } else {
                        $sourceAmountFee = abs((float)$parsedAttributes['sourceamountfee']);
                        if (!$this->hasOperation($parsedAttributes['id'], AcquiringOperation::FLOW_TYPE_INCOME)) {
                            $operationCustomer = $this->getOperationContractor(
                                Contractor::TYPE_CUSTOMER,
                                $parsedAttributes['customfield:customer'] ?? null,
                                $this->getSiteFromDescription($parsedAttributes['description']),
                                $parsedAttributes['cardnumber'] ?? null
                            );

                            $this->createOperation(array_merge($baseAttributes, [
                                'flowType' => AcquiringOperation::FLOW_TYPE_INCOME,
                                'contractorId' => $operationCustomer->id,
                                'itemId' => InvoiceIncomeItem::ITEM_PAYMENT_FROM_BUYER,
                                'amount' => $sourceAmount,
                                'description' => $parsedAttributes['description'],
                                'recognitionDate' => $date->format(DateHelper::FORMAT_DATE),
                            ]));

                            $this->savedCount++;
                        }

                        if ($this->hasOperation($parsedAttributes['id'], AcquiringOperation::FLOW_TYPE_EXPENSE)) {
                            continue 2;
                        }

                        $this->createOperation(array_merge($baseAttributes, [
                            'flowType' => AcquiringOperation::FLOW_TYPE_EXPENSE,
                            'contractorId' => $monetaSeller->id,
                            'itemId' => InvoiceIncomeItem::ITEM_OTHER,
                            'amount' => $sourceAmountFee,
                            'description' => "Коммисия по заказу №{$orderNumber} на сумму {$sourceAmount}р",
                            'recognitionDate' => $date->format(DateHelper::FORMAT_DATE),
                        ]));

                        $this->savedCount++;
                    }
                    break;
                case $this->moneta::CATEGORY_WITHDRAWAL:
                    if ($this->hasOperation($parsedAttributes['id'], AcquiringOperation::FLOW_TYPE_EXPENSE)) {
                        continue 2;
                    }

                    $recognitionDate = new DateTime($parsedAttributes['wiretransferorderdate']);
                    $withdrawalSeller = $this->getContractor(
                        Contractor::TYPE_SELLER,
                        $parsedAttributes['wireuserinn']
                    );

                    $this->createOperation(array_merge($baseAttributes, [
                        'flowType' => AcquiringOperation::FLOW_TYPE_EXPENSE,
                        'contractorId' => $withdrawalSeller->id,
                        'itemId' => InvoiceExpenditureItem::ITEM_OWN_FOUNDS,
                        'amount' => abs((float)$parsedAttributes['targetamount']),
                        'description' => $parsedAttributes['wirepaymentpurpose'],
                        'recognitionDate' => $recognitionDate->format(DateHelper::FORMAT_DATE),
                    ]));

                    $this->savedCount++;
                    break;
                default:
                    continue 2;
            }
        }

        if ($pagesCount > $pageNumber) {
            $this->pullMonetaOperations(++$pageNumber);
        }
    }

    private function parseOperationAttributes($operation)
    {
        $parsedAttributes['id'] = $operation['id'];
        foreach ($operation['attribute'] as $attribute) {
            $parsedAttributes[$attribute['key']] = $attribute['value'];
        }

        return $parsedAttributes;
    }

    /**
     * @param array $attributes
     * @return AcquiringOperation
     */
    private function createOperation(array $attributes): AcquiringOperation
    {
        $operation = AcquiringOperation::findOne([
            'acquiring_id' => $this->identity->acquiring->id,
            'company_id' => $this->identity->company->id,
            'operation_id' => (string) $attributes['operationId'],
            'acquiring_identifier' => $this->identity->account_id,
            'acquiring_type' => Acquiring::TYPE_MONETA,
        ]) ?: new AcquiringOperation();

        $operation->company_id = $this->identity->company->id;
        $operation->flow_type = $attributes['flowType'];
        $operation->acquiring_type = Acquiring::TYPE_MONETA;
        $operation->acquiring_identifier = (string) $this->identity->account_id;
        $operation->date = $attributes['date'];
        $operation->datetime = $attributes['dateTime'];
        $operation->recognition_date = $attributes['recognitionDate'];
        $operation->operation_id = (string) $attributes['operationId'];
        $operation->contractor_id = $attributes['contractorId'];
        $operation->amount = (string) $attributes['amount'];
        $operation->description = $attributes['description'];

        if ($operation->flow_type === AcquiringOperation::FLOW_TYPE_EXPENSE) {
            $operation->expenditure_item_id = $attributes['itemId'];
        } else {
            $operation->income_item_id = $attributes['itemId'];
        }

        if (!$operation->save()) {
            $this->throwModelException($operation);
        }

        return $operation;
    }

    private function getContractor($type, $inn, $kpp = null): Contractor
    {
        if (!isset($this->contractorCache["{$type}-{$inn}-{$kpp}"])) {
            $seller = Contractor::find()
                ->byCompany($this->identity->company->id)
                ->byContractor($type)
                ->byDeleted()
                ->andWhere(['ITN' => $inn]);
            if ($kpp !== null) {
                $seller->andWhere(['PPC' => $kpp]);
            }

            $seller = $seller->one();
            if ($seller === null) {
                $attributes = DadataClient::getContractorAttributes($inn, $kpp);
                $seller = new Contractor();
                $seller->type = $type;
                $seller->company_id = $this->identity->company->id;
                $seller->status = Contractor::ACTIVE;
                $seller->chief_accountant_is_director = true;
                $seller->contact_is_director = true;
                $seller->setAttributes($attributes);

                if (!$seller->save()) {
                    $this->throwModelException($seller);
                }
            }

            $this->contractorCache["{$type}-{$inn}-{$kpp}"] = $seller;
        }

        return $this->contractorCache["{$type}-{$inn}-{$kpp}"];
    }

    private function getOperationContractor($type, $email, $site, $cardNumber): Contractor
    {
        $email = $email ?? '(Нет пользовательских данных)';
        $customer = Contractor::find()
            ->byCompany($this->identity->company->id)
            ->byContractor($type)
            ->byDeleted()
            ->andWhere([
                'AND',
                ['face_type' => Contractor::TYPE_PHYSICAL_PERSON],
                ['physical_lastname' => $email],
            ])->one();

        if ($customer === null) {
            $customer = new Contractor();
            $customer->type = $type;
            $customer->face_type = Contractor::TYPE_PHYSICAL_PERSON;
            $customer->company_id = $this->identity->company->id;
            $customer->status = Contractor::ACTIVE;
            $customer->chief_accountant_is_director = true;
            $customer->contact_is_director = true;
            $customer->physical_lastname = $email;
            $customer->physical_firstname = $site;
            $customer->isMonetaCustomer = true;

            if ($cardNumber === null) {
                $customer->physical_no_patronymic = true;
            } else {
                $customer->physical_patronymic = $cardNumber;
            }

            if (!$customer->save()) {
                $this->throwModelException($customer);
            }
        }

        return $customer;
    }

    private function hasOperation($id, $flowType): bool
    {
        return AcquiringOperation::find()
            ->andWhere(['operation_id' => $id])
            ->andWhere(['company_id' => $this->identity->company->id])
            ->andWhere(['flow_type' => $flowType])
            ->exists();
    }

    /**
     * @param int $id
     * @return array
     */
    private function getMonetaOperation(int $id): array
    {
        $response = $this->moneta->GetOperationDetailsById($id);
        $operation = $response['Envelope']['Body']['GetOperationDetailsByIdResponse']['operation'];
        $attributes = $this->parseOperationAttributes($operation);

        return $attributes;
    }

    private function getSiteFromDescription($description)
    {
        return mb_substr(mb_strrchr($description, 'на сайте '), 9);
    }

    /**
     * @param Model $model
     * @return void
     */
    private function throwModelException(Model $model): void
    {
        throw new RuntimeException($this->formatErrors($model->getErrors()));
    }

    /**
     * @param array $errors
     * @return string
     */
    private function formatErrors(array $errors): string
    {
        $formattedErrors = array_map(function ($key, $val) {
            return "{$key}: {$val[0]}";
        }, array_keys($errors), $errors);

        return implode(' ', $formattedErrors);
    }
}
