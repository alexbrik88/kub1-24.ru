<?php

namespace common\modules\acquiring\models;

use common\models\Company;
use common\models\company\CompanyRelationTrait;
use common\modules\acquiring\behaviors\AcquiringIdentityBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * @property-read int $company_id Идентификатор компании
 * @property-read int $account_id Идентификатор аккаунта
 * @property-read string $access_token Токен доступа
 * @property-read string $expired_at Дата истечения
 * @property-read string $created_at Дата создания
 * @property-read string $updated_at Дата обновления
 * @property-read Company $company Компания
 * @property-read Acquiring|null $acquiring
 */
class YookassaIdentity extends ActiveRecord implements AcquiringIdentityInterface
{
    use CompanyRelationTrait;

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
            ],
            'acquiringIdentity' => [
                'class' => AcquiringIdentityBehavior::class,
            ]
        ];
    }

    /**
     * @param int $accountId
     * @return static|null
     */
    public static function findByAccountId(int $accountId): ?self
    {
        return static::findOne(['account_id' => $accountId]);
    }

    /**
     * @inheritDoc
     */
    public function getAcquiringIdentifier(): string
    {
        return $this->account_id;
    }

    /**
     * @inheritDoc
     */
    public function getAcquiringType(): int
    {
        return Acquiring::TYPE_YOOKASSA;
    }

    /**
     * @inheritDoc
     */
    public function getCompanyId(): int
    {
        return $this->company_id;
    }

    /**
     * @inheritDoc
     */
    public static function tableName()
    {
        return '{{%yookassa_identity}}';
    }
}
