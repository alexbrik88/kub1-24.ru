<?php

namespace common\modules\acquiring\models;

use yii\db\ActiveRecordInterface;

interface AcquiringIdentityInterface extends ActiveRecordInterface
{
    /**
     * @return string
     */
    public function getAcquiringIdentifier(): string;

    /**
     * @return int
     */
    public function getCompanyId(): int;

    /**
     * @return int
     */
    public function getAcquiringType(): int;
}
