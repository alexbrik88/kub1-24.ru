<?php

namespace common\modules\acquiring\models;

use common\models\FilterInterface;
use common\models\Company;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\data\DataProviderInterface;
use yii\data\Sort;
use yii\db\ActiveQueryInterface;

/**
 * @property-write Company $company
 */
class AcquiringRepository extends Model implements FilterInterface
{
    /**
     * @var int
     */
    public $type;

    /**
     * @var bool
     */
    public $is_active;

    /**
     * @var Company
     */
    private $company;

    /**
     * @param Company $company
     * @param int $acquiringType
     * @return Acquiring[]
     */
    public static function findActiveAcquiring(Company $company, int $acquiringType): array
    {
        $repository = new static(['company' => $company, 'type' => $acquiringType, 'is_active' => true]);
        $models = $repository->getQuery()->all();

        return $models;
    }

    /**
     * @inheritDoc
     */
    public function getDataProvider(): DataProviderInterface
    {
        return new ActiveDataProvider([
            'query' => $this->getQuery(),
            'sort' => $this->getSort(),
            'pagination' => false,
        ]);
    }

    /**
     * @return ActiveQueryInterface
     */
    public function getQuery(): ActiveQueryInterface
    {
        return Acquiring::find()->andFilterWhere([
            'company_id' => $this->company ? $this->company->id : null,
            'type' => $this->type,
            'is_active' => $this->is_active,
        ]);
    }

    /**
     * @inheritDoc
     */
    public function getSort(): Sort
    {
        return new Sort(['defaultOrder' => ['id' => SORT_ASC]]);
    }

    /**
     * @param Company|null $company
     */
    public function setCompany(?Company $company): void
    {
        $this->company = $company;
    }
}
