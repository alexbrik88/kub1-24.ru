<?php

namespace common\modules\acquiring\models;

use common\components\cash\behaviors\CalculationBehavior;
use common\components\date\DateHelper;
use common\components\helpers\ArrayHelper;
use common\components\TextHelper;
use common\models\Company;
use common\models\company\CompanyRelationTrait;
use common\models\Contractor;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use DateTime;
use frontend\components\StatisticPeriod;
use common\modules\acquiring\query\AcquiringQuery;
use Yii;
use yii\db\ActiveRecord;

/**
 * This is the model class for table "moneta_operation".
 *
 * @property int $id
 * @property int $acquiring_id
 * @property int $company_id
 * @property int $acquiring_type
 * @property int $flow_type
 * @property string $acquiring_identifier
 * @property string $date
 * @property string|null $datetime
 * @property string|null $recognition_date
 * @property string $operation_id
 * @property int $contractor_id
 * @property int $amount
 * @property string $description
 * @property int|null $expenditure_item_id
 * @property int|null $income_item_id
 * @property int $is_accounting
 *
 * @property Contractor $contractor
 * @property InvoiceExpenditureItem $expenditureItem
 * @property InvoiceIncomeItem $incomeItem
 *
 * @property CalculationBehavior $Calculation
 * @mixin CalculationBehavior
 */
class AcquiringOperation extends ActiveRecord
{
    use CompanyRelationTrait;

    public const FLOW_TYPE_EXPENSE = 0;
    public const FLOW_TYPE_INCOME = 1;

    protected $_statisticQuery;

    public static function tableName(): string
    {
        return 'acquiring_operation';
    }

    public static function find(): AcquiringQuery
    {
        return new AcquiringQuery(get_called_class());
    }

    public function behaviors(): array
    {
        return [
            'Calculation' => [
                'class' => CalculationBehavior::class,
                'beginAt' => DateTime::createFromFormat(DateHelper::FORMAT_DATE, StatisticPeriod::getSessionPeriod()['from']),
                'endAt' => DateTime::createFromFormat(DateHelper::FORMAT_DATE, StatisticPeriod::getSessionPeriod()['to']),
            ],
        ];
    }

    public function rules(): array
    {
        return [
            [['operation_id'], 'default', 'value' => ''],
            [['operation_id'], 'string', 'max' => 255],
            [['acquiring_id', 'company_id', 'flow_type', 'acquiring_identifier', 'date', 'amount', 'description', 'acquiring_type'], 'required'],
            [['acquiring_id', 'company_id', 'flow_type', 'contractor_id', 'amount', 'expenditure_item_id',
                'income_item_id', 'acquiring_type'], 'integer'],
            [['date', 'datetime', 'recognition_date'], 'safe'],
            [['date'], 'date', 'format' => 'd.M.yyyy'],
            [['datetime'], 'date', 'format' => 'php:H:i:s'],
            [['acquiring_identifier', 'description'], 'string', 'max' => 255],
            [['amount'],
                'number',
                'numberPattern' => Yii::$app->params['numberPattern'],
                'max' => Yii::$app->params['maxCashSum'] * 100, // with kopeck
                'tooBig' => 'Значение «{attribute}» не должно превышать ' . TextHelper::moneyFormat(Yii::$app->params['maxCashSum'], 2) . '.',
                'whenClient' => 'function(){}', // because of `123,00` - not valid float number (with comma) and there doesn't exist any methods to parse it on client.
            ],
            [['amount'], 'compare', 'operator' => '>', 'compareValue' => 0.01,
                'whenClient' => 'function(){}', // because of `123,00` - not valid float number (with comma) and there doesn't exist any methods to parse it on client.
            ],
            [['acquiring_id'], 'exist', 'skipOnError' => true, 'targetClass' => Acquiring::class, 'targetAttribute' => ['acquiring_id' => 'id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['company_id' => 'id']],
            [['contractor_id'], 'exist', 'skipOnError' => true, 'targetClass' => Contractor::class, 'targetAttribute' => ['contractor_id' => 'id']],
            [['expenditure_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => InvoiceExpenditureItem::class, 'targetAttribute' => ['expenditure_item_id' => 'id']],
            [['income_item_id'], 'exist', 'skipOnError' => true, 'targetClass' => InvoiceIncomeItem::class, 'targetAttribute' => ['income_item_id' => 'id']],
            [['is_accounting'], 'boolean'],
        ];
    }

    public function attributeLabels(): array
    {
        return [
            'id' => 'ID',
            'company_id' => 'Company ID',
            'flow_type' => 'Тип',
            'acquiring_identifier' => 'Лицевой счет',
            'date' => 'Дата',
            'datetime' => 'Время',
            'recognition_date' => 'Дата признания',
            'operation_id' => 'Operation ID',
            'contractor_id' => $this->getContractorLabel(),
            'amount' => 'Сумма',
            'description' => 'Назначение',
            'expenditure_item_id' => 'Статья расхода',
            'income_item_id' => 'Статья прихода',
        ];
    }

    /**
     * @param boolean $insert
     * @return boolean
     */
    public function beforeSave($insert)
    {
        if (!parent::beforeSave($insert)) {
            return false;
        }

        $this->date = DateHelper::format($this->date, DateHelper::FORMAT_DATE, DateHelper::FORMAT_USER_DATE);
        $this->datetime = $this->date . ' ' . $this->datetime;

        return true;
    }

    /**
     * @return bool
     */
    public function beforeValidate()
    {
        $this->amount = TextHelper::parseMoneyInput($this->amount);

        if ($this->isAttributeChanged('amount')) {
            $this->amount = round($this->amount * 100);
        }
        if (count(explode(' ', $this->datetime === 2))) {
            @list($date, $this->datetime) = explode(' ', $this->datetime);
        }

        return parent::beforeValidate();
    }

    public function afterFind()
    {
        parent::afterFind();

        if (strlen($this->date) > 0) {
            $this->date = DateHelper::format($this->date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
        }
    }

    /**
     * @return array
     */
    public static function getFlowTypes()
    {
        return [
            self::FLOW_TYPE_INCOME => 'Приход',
            self::FLOW_TYPE_EXPENSE => 'Расход',
        ];
    }

    /**
     * @return AcquiringQuery
     */
    public function getStatisticQuery(): AcquiringQuery
    {
        $query = new AcquiringQuery(static::class);
        $query->andFilterWhere([
            'company_id' => $this->company_id,
            'acquiring_type' => $this->acquiring_type,
            'acquiring_identifier' => $this->acquiring_identifier,
        ]);

        return $query;
    }

    public function getContractorLabel(): string
    {
        return ArrayHelper::getValue([
            self::FLOW_TYPE_INCOME => 'Покупатель',
            self::FLOW_TYPE_EXPENSE => 'Поставщик',
        ], $this->flow_type, 'Контрагент');
    }

    public function getIsForeign()
    {
        return false;
    }

    /**
     * @return int|string
     */
    public function getAmountIncome()
    {
        return $this->flow_type == self::FLOW_TYPE_INCOME ? $this->amount : '-';
    }

    /**
     * @return int|string
     */
    public function getAmountExpense()
    {
        return $this->flow_type == self::FLOW_TYPE_EXPENSE ? $this->amount : '-';
    }

    /**
     * Gets query for [[Contractor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getContractor()
    {
        return $this->hasOne(Contractor::class, ['id' => 'contractor_id']);
    }

    /**
     * Gets query for [[ExpenditureItem]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getExpenditureItem()
    {
        return $this->hasOne(InvoiceExpenditureItem::class, ['id' => 'expenditure_item_id']);
    }

    /**
     * Gets query for [[IncomeItem]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIncomeItem()
    {
        return $this->hasOne(InvoiceIncomeItem::class, ['id' => 'income_item_id']);
    }

}
