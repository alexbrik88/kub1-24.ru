<?php

namespace common\modules\acquiring\models;

use common\models\Company;
use common\models\company\CompanyRelationTrait;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * @property-read int $id
 * @property int $company_id
 * @property int $type
 * @property string $identifier
 * @property string $username
 * @property string $password
 * @property bool $is_active
 * @property-read string $created_at
 * @property-read string $updated_at
 */
class Acquiring extends ActiveRecord
{
    use CompanyRelationTrait;

    /** @var int */
    public const TYPE_MONETA = 0;

    /** @var int */
    public const TYPE_YOOKASSA = 1;

    /** @var array  */
    public static $typeMap = [
        self::TYPE_MONETA => 'Монета',
        self::TYPE_YOOKASSA => 'ЮKassa',
    ];

    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * @inheritDoc
     */
    public static function tableName()
    {
        return '{{%acquiring}}';
    }

    /**
     * @inheritDoc
     */
    public function rules()
    {
        return [
            [['is_active'], 'default', 'value' => true],
            [['is_active'], 'boolean'],
            [['company_id', 'type', 'identifier', 'username', 'password'], 'required'],
            [['company_id', 'type'], 'integer'],
            [['password'], 'string'],
            [['identifier', 'username'], 'string', 'max' => 255],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Company::class, 'targetAttribute' => ['company_id' => 'id']],
        ];
    }

    /**
     * @return string
     */
    public function getFullName(): string
    {
        return sprintf('%s (%s)', self::$typeMap[$this->type], $this->identifier);
    }
}
