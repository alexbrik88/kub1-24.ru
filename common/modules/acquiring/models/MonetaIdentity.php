<?php

namespace common\modules\acquiring\models;

use common\behaviors\PasswordAttributeBehavior;
use common\models\company\CompanyRelationTrait;
use common\modules\acquiring\behaviors\AcquiringIdentityBehavior;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * @property-read int $company_id Идентификатор компании
 * @property-read int $account_id Идентификатор аккаунта
 * @property string $username
 * @property string $password
 * @property-read string $created_at Дата создания
 * @property-read string $updated_at Дата обновления
 * @property-read Acquiring|null $acquiring
 */
class MonetaIdentity extends ActiveRecord implements AcquiringIdentityInterface
{
    use CompanyRelationTrait;

    /**
     * @inheritDoc
     */
    public function behaviors(): array
    {
        return [
            'timestamp' => [
                'class' => TimestampBehavior::class,
                'value' => new Expression('NOW()'),
            ],
            'passwordAttribute' => [
                'class' => PasswordAttributeBehavior::class,
                'attribute' => 'password',
                'encryptPassword' => Yii::$app->params['moneta']['encrypt_password'],
            ],
            'acquiringIdentity' => [
                'class' => AcquiringIdentityBehavior::class,
            ],
        ];
    }

    /**
     * @param int $accountId
     * @return static|null
     */
    public static function findByAccountId(int $accountId): ?self
    {
        return static::findOne(['account_id' => $accountId]);
    }

    /**
     * @inheritDoc
     */
    public function getAcquiringIdentifier(): string
    {
        return $this->account_id;
    }

    /**
     * @inheritDoc
     */
    public function getAcquiringType(): int
    {
        return Acquiring::TYPE_MONETA;
    }

    /**
     * @inheritDoc
     */
    public function getCompanyId(): int
    {
        return $this->company_id;
    }

    /**
     * @inheritDoc
     */
    public static function tableName(): string
    {
        return '{{%moneta_identity}}';
    }
}
