<?php

namespace common\modules\acquiring\models;

use common\models\cash\CashFlowsBase;
use common\models\employee\Employee;
use common\models\FormInterface;
use frontend\components\StatisticPeriod;
use common\modules\acquiring\query\AcquiringQuery;
use frontend\modules\cash\models\query\CashPlanFactQuery;
use yii\data\SqlDataProvider;
use frontend\modules\cash\models\cashSearch\FilterItemsTrait;
use frontend\modules\cash\models\cashSearch\FilterNamesTrait;
use frontend\modules\cash\models\cashSearch\WalletNamesTrait;
use yii\db\Expression;

class AcquiringOperationsSearch extends AcquiringOperation implements FormInterface
{
    use FilterItemsTrait;
    use FilterNamesTrait;
    use WalletNamesTrait;

    const FILTER_AMOUNT_TYPE_EQUAL = 0;
    const FILTER_AMOUNT_TYPE_MORE_THAN = 1;
    const FILTER_AMOUNT_TYPE_LESS_THAN = 2;
    const FILTER_FLOW_TYPE_ALL = -1;
    const FILTER_FLOW_TYPE_INCOME = 1;
    const FILTER_FLOW_TYPE_EXPENSE = 0;

    public $foreign;
    public $currency_name;
    public $currency_id;

    public $incomeItemIdManyItem;
    public $expenditureItemIdManyItem;
    public $wallet_id;

    private $_filterQuery;
    private $_userConfigShowPlan;

    public function rules(): array
    {
        return [
            [['contractor_id', 'flow_type'], 'safe'],
            [['project_id', 'wallet_ids', 'contractor_ids', 'reason_ids', 'contractor_name', 'operation_type'], 'safe'],
            [['sale_point_id', 'industry_id'], 'safe'],
            [[
                'contractor_name_2',
                'description_2',
                'amount_type_2',
                'amount_2',
                'flow_type_2',
            ], 'safe']
        ];
    }

    /**
     *
     */
    public function init()
    {
        $this->initWalletNames($this->company_id, CashFlowsBase::WALLET_ACQUIRING);
    }

    /**
     *
     */
    public function load($params, $formName = null)
    {
        parent::load($params);
        $this->contractor_ids = array_filter((array)$this->contractor_ids);
        $this->reason_ids = array_filter((array)$this->reason_ids);
        $this->wallet_ids = array_filter((array)$this->wallet_ids);
        $this->project_id = $this->project_id ?: null;
        $this->sale_point_id = $this->sale_point_id ?: null;
        $this->industry_id = $this->industry_id ?: null;
    }

    public function search(?Acquiring $acquiring, $params = []) {
        $period = StatisticPeriod::getSessionPeriod();

        $this->load($params);

        $query = (new CashPlanFactQuery())
            ->alias('acq')
            ->from(['acq' => $this->getPlanFactQuery()])
            ->leftJoin('contractor', 'contractor.id = acq.contractor_id')
            ->select([
                'acq.id',
                'acq.company_id',
                'acq.tb',
                'acq.acquiring_identifier',
                'acq.created_at',
                'acq.date',
                'acq.flow_type',
                'acq.amount',
                'acq.contractor_id',
                'acq.description',
                'acq.expenditure_item_id',
                'acq.income_item_id',
                'acq.wallet_id',
                'acq.is_internal_transfer',
                //
                'acq.amountExpense',
                'acq.amountIncome',
                'acq.transfer_key',
                'acq.project_id',
                'acq.sale_point_id',
                'acq.industry_id'
            ]);

        $this->setFilters($query, 'acq')
            ->andWhere(['between', 'acq.date', $period['from'], $period['to']])
            ->andFilterWhere([
                'acq.acquiring_identifier' => ($this->wallet_ids)
                    ? array_map(function($v) { return str_replace(CashFlowsBase::WALLET_ACQUIRING.'_', '', $v); }, $this->wallet_ids)
                    : $this->acquiring_identifier
            ]);

        $this->_filterQuery = clone $query;
        $this->_statisticQuery = (clone $query)->andWhere(['not', ['wallet_id' => 'plan']]);

        $dataProvider = new SqlDataProvider([
            'sql' => $query->createCommand()->rawSql,
            'sort' => [
                'attributes' => [
                    'date',
                    'amountIncomeExpense' => [
                        'asc' => [
                            'flow_type' => SORT_ASC,
                            'amount' => SORT_ASC,
                        ],
                        'desc' => [
                            'flow_type' => SORT_DESC,
                            'amount' => SORT_DESC,
                        ],
                    ],
                    'amountIncome' => [
                        'asc' => [
                            'flow_type' => SORT_DESC, // Приход имеет flow_type = 1, поэтому сортируем по DESC
                            'amount' => SORT_ASC,
                        ],
                        'desc' => [
                            'flow_type' => SORT_DESC, // Приход имеет flow_type = 1, поэтому сортируем по DESC
                            'amount' => SORT_DESC,
                        ],
                    ],
                    'amountExpense' => [
                        'asc' => [
                            'flow_type' => SORT_ASC, // Расход имеет flow_type = 0, поэтому сортируем по ASC
                            'amount' => SORT_ASC,
                        ],
                        'desc' => [
                            'flow_type' => SORT_ASC, // Расход имеет flow_type = 0, поэтому сортируем по ASC
                            'amount' => SORT_DESC,
                        ],
                    ],
                    'contractor_id',
                    'description',
                    'created_at',
                ],
                'defaultOrder' => [
                    'date' => SORT_DESC,
                    'created_at' => SORT_DESC,
                ],
            ],
        ]);

        return $dataProvider;
    }

    public function getPlanFactQuery()
    {
        $classFact = AcquiringOperation::class;
        $classPlan = null;

        $factQuery = $classFact::find()
            ->alias('f')
            ->andWhere(['f.company_id' => $this->company_id])
            ->groupBy('f.id');

        $factQuery->addSelect([
            'f.id',
            'f.company_id',
            '"'.$classFact::tableName().'" AS {{tb}}',
            'f.acquiring_identifier',
            'null AS [[created_at]]',
            'f.date',
            'f.flow_type',
            'f.amount',
            'f.contractor_id',
            'f.description',
            'f.expenditure_item_id',
            'f.income_item_id',
            'CONCAT("4_", f.acquiring_identifier) AS [[wallet_id]]',
            'null AS [[is_internal_transfer]]',
            //
            "IF({{f}}.[[flow_type]] = 0, {{f}}.[[amount]], 0) AS amountExpense",
            "IF({{f}}.[[flow_type]] = 1, {{f}}.[[amount]], 0) AS amountIncome",
            new Expression('IF (flow_type = 0, CONCAT("ACQUIRING_", id, "_C"), CONCAT("ACQUIRING_", "C_", id)) AS transfer_key'),
            'f.project_id',
            'f.sale_point_id',
            'f.industry_id'
        ]);

        return $factQuery;
    }

}