<?php

namespace common\modules\acquiring\models;

use common\models\Company;
use common\models\ListBuilder;
use common\models\ListInterface;

class AcquiringIdentifierList implements ListInterface
{
    /**
     * @var Company
     */
    private $company;

    /**
     * @var int
     */
    private $acquiringType;

    /**
     * @param Company $company
     * @param int $acquiringType
     */
    public function __construct(Company $company, int $acquiringType)
    {
        $this->company = $company;
        $this->acquiringType = $acquiringType;
    }

    /**
     * @inheritDoc
     */
    public function getItems(): array
    {
        $repository = new AcquiringRepository([
            'company' => $this->company,
            'type' => $this->acquiringType,
            'is_active' => true,
        ]);
        $builder = new ListBuilder();
        $builder->setIndexBy('identifier');
        $builder->setSort($repository->getSort());

        return $builder->buildItems($repository->getDataProvider(), 'identifier');
    }
}
