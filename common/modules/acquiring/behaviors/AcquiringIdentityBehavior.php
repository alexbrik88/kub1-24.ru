<?php

namespace common\modules\acquiring\behaviors;

use common\modules\acquiring\models\AcquiringIdentityInterface;
use common\modules\acquiring\models\Acquiring;
use yii\base\Behavior;
use yii\base\InvalidConfigException;
use yii\db\ActiveRecord;

/**
 * @property-read AcquiringIdentityInterface $owner
 * @property-read Acquiring|null $acquiring
 */
class AcquiringIdentityBehavior extends Behavior
{
    /**
     * @inheritDoc
     */
    public function events()
    {
        return [
            ActiveRecord::EVENT_AFTER_INSERT => 'enableAcquiring',
            ActiveRecord::EVENT_AFTER_UPDATE => 'enableAcquiring',
            ActiveRecord::EVENT_AFTER_DELETE => 'disableAcquiring',
        ];
    }

    /**
     * @return void
     * @throws InvalidConfigException
     */
    public function enableAcquiring(): void
    {
        $acquiring = $this->getAcquiring();

        if ($acquiring) {
            $acquiring->is_active = true;
            $acquiring->save(false, ['is_active']);
        } else {
            $acquiring = new Acquiring([
                'company_id' => $this->owner->getCompanyId(),
                'identifier' => $this->owner->getAcquiringIdentifier(),
                'type' => $this->owner->getAcquiringType(),
                'is_active' => true,
            ]);

            $acquiring->save(false);
        }
    }

    /**
     * @return void
     * @throws InvalidConfigException
     */
    public function disableAcquiring(): void
    {
        $acquiring = $this->getAcquiring();

        if ($acquiring) {
            $acquiring->is_active = false;
            $acquiring->save(false, ['is_active']);
        }
    }

    /**
     * @return Acquiring|null
     * @throws InvalidConfigException
     */
    public function getAcquiring(): ?Acquiring
    {
        if (!($this->owner instanceof AcquiringIdentityInterface)) {
            throw new InvalidConfigException();
        }

        return Acquiring::findOne([
            'company_id' => $this->owner->getCompanyId(),
            'identifier' => $this->owner->getAcquiringIdentifier(),
            'type' => $this->owner->getAcquiringType(),
        ]);
    }
}
