<?php

namespace common\assets;

use yii\web\AssetBundle;

/**
 * Class MomentTimezoneAsset
 * @package common\assets
 */
class MomentAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@bower/moment';

    /**
     * @var array
     */
    public $js = [
        'moment.js',
    ];
}