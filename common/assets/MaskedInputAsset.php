<?php

namespace common\assets;

use yii\web\AssetBundle;

/**
 * The asset bundle for the [[MaskedInput]] widget.
 */
class MaskedInputAsset extends AssetBundle
{
    public $sourcePath = '@bower/inputmask/dist';
    public $js = [
        'inputmask/bindings/inputmask.binding.js'
    ];
    public $depends = [
        'yii\widgets\MaskedInputAsset'
    ];
}
