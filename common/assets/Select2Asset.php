<?php

namespace common\assets;

use yii\web\AssetBundle;


/**
 * Class Select2Asset
 * @package common\assets
 */
class Select2Asset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@vendor/select2/select2/dist';

    /**
     * @var array
     */
    public $css = [
        'css/select2.min.css',
    ];
    /**
     * @var array
     */
    public $js = [
        'js/select2.full.min.js',
        'js/i18n/ru.js',
    ];
    /**
     * @var array
     */
    public $depends = [
        'yii\web\YiiAsset',
    ];
}
