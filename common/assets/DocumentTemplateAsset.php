<?php

namespace common\assets;

use yii\web\AssetBundle;

class DocumentTemplateAsset extends AssetBundle
{
    public $sourcePath = '@common/assets/web';
    public $css = [
        'css/document-template.css',
        //'css/document-template.min.css',
    ];
    public $js = [];
    public $depends = [
        'common\assets\FontsAsset',
    ];
}
