<?php
namespace common\assets;

use yii\web\AssetBundle;

/**
 *  Font Awesome 4.7.0 by @davegandy - http://fontawesome.io - @fontawesome
 *  License - http://fontawesome.io/license (Font: SIL OFL 1.1, CSS: MIT License)
 */
class Fontawesome4Asset extends AssetBundle
{
    public $sourcePath = '@common/assets/web/css/font-awesome';

    public $css = [
        'css/font-awesome.min.css',
    ];
}
