<?php

namespace common\assets;

use yii\web\AssetBundle;

/**
 * Class JquerySerializeObjectAsset
 */
class JquerySerializeObjectAsset extends AssetBundle
{
    public $sourcePath = '@bower/jquery-serialize-object/dist';

    public $js = array(
        'jquery.serialize-object.min.js'
    );

    public $depends = array(
        'yii\web\JqueryAsset',
    );
}
