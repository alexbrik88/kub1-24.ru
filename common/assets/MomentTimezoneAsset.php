<?php

namespace common\assets;

use yii\web\AssetBundle;

/**
 * Class MomentTimezoneAsset
 * @package common\assets
 */
class MomentTimezoneAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@bower/moment-timezone';

    /**
     * @var array
     */
    public $js = [
        'builds/moment-timezone-with-data.js',
    ];
}