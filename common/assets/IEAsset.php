<?php

namespace common\assets;

use yii\web\AssetBundle;

// TODO: move all possible libraries to bower.

/**
 * Class CommonAsset
 * @package common\assets
 */
class IEAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@common/assets/web';



    /**
     * @var array
     */
    public $css = [
    ];

    /**
     * @var array
     */
    public $js = [
        'scripts/ie.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
