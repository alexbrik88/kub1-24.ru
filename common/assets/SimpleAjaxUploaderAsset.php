<?php

namespace common\assets;

use yii\web\AssetBundle;

/**
 * Class HistoryApiAsset
 * @package common\assets
 */
class SimpleAjaxUploaderAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@bower/simple-ajax-uploader';

    /**
     * @var array
     */
    public $js = [
        'SimpleAjaxUploader.js',
    ];
}
