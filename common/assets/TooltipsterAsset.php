<?php

namespace common\assets;

use philippfrenzel\yii2tooltipster\yii2tooltipsterAsset;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class TooltipsterAsset extends yii2tooltipsterAsset
{
    public $sourcePath = '@bower/tooltipster/dist';

    public $css = array(
        'css/tooltipster.bundle.min.css',
    );

    public $js = array(
        'js/tooltipster.bundle.min.js'
    );

    public $depends = array(
        'yii\web\JqueryAsset',
    );
}
