<?php
namespace common\assets;

use yii\web\AssetBundle;

class JCropAsset extends AssetBundle
{
    public $sourcePath = '@common/assets/jcrop';

    public $css = [
        'css/jcrop.custom.css',
    ];
    public $js = [
        'js/jquery.color.js',
        'js/jcrop.custom.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
    ];
    
    public function registerAssetFiles($view)
    {
        $this->css[] = 'css/jquery.Jcrop' . (!YII_DEBUG ? '.min' : '') . '.css';
        $this->js[] = 'js/jquery.Jcrop' . (!YII_DEBUG ? '.min' : '') . '.js';
        parent::registerAssetFiles($view);
    }
}
