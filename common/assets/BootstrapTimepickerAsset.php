<?php

namespace common\assets;

use yii\web\AssetBundle;

class BootstrapTimepickerAsset extends AssetBundle
{
    public $sourcePath = '@common/assets/web/plugins/bootstrap-timepicker';
    public $css = [
        'css/bootstrap-timepicker.min.css',
    ];
    public $js = [
        'js/bootstrap-timepicker.min.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
