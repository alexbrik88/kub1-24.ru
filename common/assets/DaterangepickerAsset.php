<?php

namespace common\assets;

use yii\web\AssetBundle;

/**
 * DaterangepickerAsset
 */
class DaterangepickerAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@bower/jquery-date-range-picker/dist';

    /**
     * @var array
     */
    public $css = [
        'daterangepicker.min.css',
    ];
    /**
     * @var array
     */
    public $js = [
        'jquery.daterangepicker.min.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
