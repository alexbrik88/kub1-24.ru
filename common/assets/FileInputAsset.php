<?php

namespace common\assets;

use yii\web\AssetBundle;

class FileInputAsset extends AssetBundle
{
    public $sourcePath = '@vendor/jasny/bootstrap/dist';
    
    public $css = [
        'css/jasny-bootstrap.css',
    ];

    public $js = [
        'js/jasny-bootstrap.js',
    ];

    //public $depends = [
    //    'yii\bootstrap4\BootstrapPluginAsset',
    //];
}