<?php

namespace common\assets;

use yii\web\AssetBundle;

/**
 * Class MaterialIconsAsset
 *
 * https://google.github.io/material-design-icons/
 *
 * https://material.io/resources/icons/?style=baseline
 *
 * Using:
 *
 *  <i class="material-icons">face</i>
 *
 * where "face" icon name
 *
 * or us numeric character:
 *
 * <i class="material-icons">&#xE87C;</i>
 *
 * Find both the icon names and codepoints on the codepoints file
 */
class MaterialIconsAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@bower/material-design-icons/iconfont';

    /**
     * @var array
     */
    public $css = [
        'material-icons.css',
    ];
}
