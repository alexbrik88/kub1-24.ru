<?php
namespace common\assets;

use yii\web\AssetBundle;

/**
 * Class MorrisChartAsset
 * @package common\assets
 */
class MorrisChartAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@common/assets/web/plugins/morris.js/0.5.1';

    /**
     * @var array
     */
    public $css = [
        'morris.css',
    ];

    /**
     * @var array
     */
    public $js = [
        'morris.js',
        'morris.min.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        'yii\web\JqueryAsset',
        'common\assets\RaphaelAsset',
    ];

    /**
     * Registers the CSS and JS files with the given view.
     * @param \yii\web\View $view the view that the asset files are to be registered with.
     */
    public function registerAssetFiles($view)
    {
        $this->js[] = 'morris' . (!YII_DEBUG ? '.min' : '') . '.js';
        parent::registerAssetFiles($view);
    }
}
