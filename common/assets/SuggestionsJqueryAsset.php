<?php

namespace common\assets;

use yii\web\AssetBundle;

class SuggestionsJqueryAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@vendor/hflabs/suggestions-jquery';

    /**
     * @var array
     */
    public $js = [
        'dist/js/jquery.suggestions.min.js',
    ];
}