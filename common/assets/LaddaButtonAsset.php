<?php

namespace common\assets;

use yii\web\AssetBundle;

/**
 * LaddaButtonAsset
 * https://github.com/uvicate/Ladda
 */
class LaddaButtonAsset extends AssetBundle
{
    public $sourcePath = '@vendor/uvicate/ladda/dist';

    public $css = array(
        'ladda-themeless.min.css',
    );

    public $js = [
        'spin.min.js',
        'ladda.min.js',
    ];
}
