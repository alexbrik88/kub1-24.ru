<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 31.05.2017
 * Time: 11:33
 */

namespace common\assets;


use yii\web\AssetBundle;

class SlickAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@bower/slick-carousel';

    /**
     * @var array
     */
    public $js = [
        'slick/slick.js',
    ];

    public $css = [
        'slick/slick.css',
    ];
}