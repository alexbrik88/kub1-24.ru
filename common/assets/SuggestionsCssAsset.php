<?php

namespace common\assets;

use yii\web\AssetBundle;

class SuggestionsCssAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@vendor/hflabs/suggestions-jquery';

    /**
     * @var array
     */
    public $css = [
        'dist/css/suggestions.css',
    ];
}