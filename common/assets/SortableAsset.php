<?php

namespace common\assets;

use yii\web\AssetBundle;

/**
 * Class SortableAsset
 *
 * http://johnny.github.com/jquery-sortable/
 *
 * @package common\assets
 */
class SortableAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@common/assets/web/plugins/sortable';

    /**
     * @var array
     */
    public $css = [
        'sortable.css',
    ];
    /**
     * @var array
     */
    public $js = [];

    /**
     * @var array
     */
    public $depends = [
        'yii\web\JqueryAsset',
        'yii\jui\JuiAsset',
    ];

    /**
     * Registers the CSS and JS files with the given view.
     * @param \yii\web\View $view the view that the asset files are to be registered with.
     */
    public function registerAssetFiles($view)
    {
        $this->js[] = 'jquery-sortable' . (YII_DEBUG ? '' : '.min') . '.js';
        parent::registerAssetFiles($view);
    }
}
