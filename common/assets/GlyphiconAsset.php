<?php

namespace common\assets;


use yii\web\AssetBundle;

class GlyphiconAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@common/assets/web/plugins/glyphicon';

    /**
     * @var array
     */
    public $css = [
        'css/glyphicon.css',
    ];
}
