
(function ($, undef) {
    "use strict";

    $.uniform = {
        defaults: {
        },
        elements: []
    };

    $.fn.uniform = function (options) {
        return this;
    };

    $.uniform.restore = $.fn.uniform.restore = function (elem) {
        return;
    };

    $.uniform.update = $.fn.uniform.update = function (elem) {
        return;
    };
}(jQuery));
