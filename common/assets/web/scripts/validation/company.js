/**
 * Help methods to improve client validating.
 */

function addressLegalIsNotActual(attribute, value) {
    return !$('#address_legal_is_actual_input').is(':checked');
}

function chiefIsChiefAccountant(attribute, value) {
    return companyIsOoo(attribute, value)
        && !$('#chief_is_chief_accountant_input').is(':checked');
}

function companyIsOoo() {
    if ($('#create-company').hasClass('in')) {
        return $('#create-company-modal').find('#company-company_type_id :checked').val() != 1
    } else if ($('#add-new').hasClass('in')) {
        return $('#new-company-invoice-form').find('#company-company_type_id').val() != 1
    } else if ($('#form-update-company').length) {
        return $('#form-update-company').find('#company-company_type_id').val() != 1;
    }
    return $('#company-company_type_id').val() != 1;
}

function companyIsIp() {
    if ($('#new-company-invoice-form').length) {
        return $('#new-company-invoice-form').find('#company-company_type_id').val() == 1;
    } else if ($('#create-company-modal').length) {
        return $('#create-company-modal').find('#company-company_type_id :checked').val() == 1;
    } else if ($('#form-update-company').length) {
        return $('#form-update-company').find('#company-company_type_id').val() == 1;
    }
    return $('#company-company_type_id').val() == 1;
}

function companyIsSelfEmployed() {
    return $('#company-self_employed').val() == 1;
}