/**
 * Help methods to improve client validating.
 */

function productIsGoods() {
    return $('#production_type_input').val() == '1';
}

function productIsService() {
    return $('#production_type_input').val() == '0';
}