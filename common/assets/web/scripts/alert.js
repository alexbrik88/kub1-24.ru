(function ($) {
    $.alert_ext = {
        defaults: {
            type: 'danger',
            speed: 'normal',
            isOnly: false,
            isScroll: true,
            topOffset: 0,
            onShow: function () {
            },
        },

        // шаблон
        tmpl: '<div class="${State} alert fade in"><span class="close" data-dismiss="alert" aria-hidden="true">×</span>${Content}</div>',

        init: function (containerSelector, msg, options) {
            this.container = $(containerSelector);
            this.options = $.extend({}, this.defaults, options);

            this.create(msg);

            this.bind_event();

            return this.alertDiv;
        },

        template: function (tmpl, data) {
            $.each(data, function (k, v) {
                tmpl = tmpl.replace('${' + k + '}', v);
            });
            return $(tmpl);
        },

        create: function (msg) {
            this.alertDiv = this.template(this.tmpl, {
                State: 'alert-' + this.options.type,
                Content: msg
            });
            if (!this.options.title) {
                $('h4', this.alertDiv).remove();
                $('p', this.alertDiv).css('margin-right', '15px');
            }
            if (this.options.isOnly) {
                $('body > .alert').remove();
            }
            this.alertDiv.appendTo(this.container);
        },

        bind_event: function () {
            this.bind_show();
            if (this.options.isScroll) {
                this.bind_scroll();
            }
        },

        bind_show: function () {
            var ops = this.options;
            ops.onShow($(this));
        },

        bind_scroll: function () {
            $('html, body').animate({
                scrollTop: this.alertDiv.offset().top - this.options.topOffset
            }, this.options.speed);
        },
    };

    $.alert = function (container, msg, arg) {
        if (!$.trim(msg).length) {
            return false;
        }
        if (arg && arg.type == 'error') {
            arg.type = 'danger';
        }
        return $.alert_ext.init(container, msg, arg);
    };
})(jQuery);