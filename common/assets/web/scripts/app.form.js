/**
 * FORM
 *
 * @namespace APP
 * @example APP.form;
 */
;
(function (APP, window, document, undefined) {


    /**
     * Прикрепляю к проекту
     *
     * @method form
     * @namespace APP.form
     * @return {Object} открытые методы
     */
    APP.form = (function () {


        /**
         * Приватные свойства
         * @private
         * @type {Variable}
         */
        var _formAjaxSubmit = '.js_form_ajax_submit'
            ;


        /**
         * Иницилиализация модуля
         *
         * @function init
         */
        function init() {
            setBind()
        }

        /**
         * Прослушивание событий
         *
         * @function setBind
         * @private
         */
        function setBind() {
            var that = this;

            setAjaxSubmitForm();
        }

        function setAjaxSubmitForm() {
            $(document)
                .on('beforeSubmit', _formAjaxSubmit, function (e) {
                    var form = this;
                    var $form = $(this);

                    if ($form.find('.has-error').length) {
                        return false;
                    }

                    $.ajax({
                        url: form.action,
                        type: 'post',
                        data: $form.serialize(),
                        success: function (data) {
                            var callback = $form.data('callback');

                            if (callback === false) {
                                return;
                            } else if ($form.data('callback') != undefined) {
                                eval(callback, [data, form]);
                            } else {
                                alert(data);
                            }
                        }
                    });

                    return false;
                })
                .on('submit', _formAjaxSubmit, function (e) {
                    e.preventDefault();
                });
        }


        /**
         * @method get получить ответ
         * @public
         */
        return {
            init: init
        };

    })();


    $(function () {
        APP.form.init();
    });


})(window.APP = window.APP || {}, window, document);

