(function($, window, document) {
    'use strict';

    var TogleViewButton = {
        processCurrentState: function (button) {
            let btn = $(button);
            let active = btn.hasClass('active');
            let activeBtnClass = btn.data('on-class');
            let inactiveBtnClass = btn.data('off-class');
            let hideTarget = btn.data('hide-targer');
            let showTarget = btn.data('show-target');
            let disableTarget = btn.data('disable-target');
            let enableTarget = btn.data('enable-target');
            let container = btn.data('container-selector') || document;
            if (activeBtnClass) {
                btn.toggleClass(activeBtnClass, active);
            }
            if (inactiveBtnClass) {
                btn.toggleClass(inactiveBtnClass, !active);
            }
            if (hideTarget) {
                $(hideTarget, container).each(function() {
                    $(this).toggleClass('hidden', active);
                });
            }
            if (showTarget) {
                $(showTarget, container).each(function() {
                    $(this).toggleClass('hidden', !active);
                });
            }
            if (disableTarget) {
                $(disableTarget, container).each(function() {
                    $(this).prop('disabled', active).toggleClass('disabled', active);
                });
            }
            if (enableTarget) {
                $(enableTarget, container).each(function() {
                    $(this).prop('disabled', !active).toggleClass('disabled', !active);
                });
            }
            btn.trigger('toggleView.change');
        }
    };

    $.fn.togleViewButton = function() {
        return this.each(function() {
            TogleViewButton.processCurrentState(this);
        });
    };

    $(document).on('click', '.togle-view-button', function (e) {
        $(this).toggleClass('active');
        TogleViewButton.processCurrentState(this);
    });
})(jQuery, window, document);
