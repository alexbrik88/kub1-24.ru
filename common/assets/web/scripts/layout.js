/*!
 * JavaScript Cookie v2.1.3
 * https://github.com/js-cookie/js-cookie
 *
 * Copyright 2006, 2015 Klaus Hartl & Fagner Brack
 * Released under the MIT license
 */
;
(function (factory) {
    var registeredInModuleLoader = false;
    if (typeof define === 'function' && define.amd) {
        define(factory);
        registeredInModuleLoader = true;
    }
    if (typeof exports === 'object') {
        module.exports = factory();
        registeredInModuleLoader = true;
    }
    if (!registeredInModuleLoader) {
        var OldCookies = window.Cookies;
        var api = window.Cookies = factory();
        api.noConflict = function () {
            window.Cookies = OldCookies;
            return api;
        };
    }
}(function () {
    function extend() {
        var i = 0;
        var result = {};
        for (; i < arguments.length; i++) {
            var attributes = arguments[i];
            for (var key in attributes) {
                result[key] = attributes[key];
            }
        }
        return result;
    }

    function init(converter) {
        function api(key, value, attributes) {
            var result;
            if (typeof document === 'undefined') {
                return;
            }

            // Write

            if (arguments.length > 1) {
                attributes = extend({
                    path: '/'
                }, api.defaults, attributes);

                if (typeof attributes.expires === 'number') {
                    var expires = new Date();
                    expires.setMilliseconds(expires.getMilliseconds() + attributes.expires * 864e+5);
                    attributes.expires = expires;
                }

                try {
                    result = JSON.stringify(value);
                    if (/^[\{\[]/.test(result)) {
                        value = result;
                    }
                } catch (e) {
                }

                if (!converter.write) {
                    value = encodeURIComponent(String(value))
                        .replace(/%(23|24|26|2B|3A|3C|3E|3D|2F|3F|40|5B|5D|5E|60|7B|7D|7C)/g, decodeURIComponent);
                } else {
                    value = converter.write(value, key);
                }

                key = encodeURIComponent(String(key));
                key = key.replace(/%(23|24|26|2B|5E|60|7C)/g, decodeURIComponent);
                key = key.replace(/[\(\)]/g, escape);

                return (document.cookie = [
                    key, '=', value,
                    attributes.expires ? '; expires=' + attributes.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
                    attributes.path ? '; path=' + attributes.path : '',
                    attributes.domain ? '; domain=' + attributes.domain : '',
                    attributes.secure ? '; secure' : ''
                ].join(''));
            }

            // Read

            if (!key) {
                result = {};
            }

            // To prevent the for loop in the first place assign an empty array
            // in case there are no cookies at all. Also prevents odd result when
            // calling "get()"
            var cookies = document.cookie ? document.cookie.split('; ') : [];
            var rdecode = /(%[0-9A-Z]{2})+/g;
            var i = 0;

            for (; i < cookies.length; i++) {
                var parts = cookies[i].split('=');
                var cookie = parts.slice(1).join('=');

                if (cookie.charAt(0) === '"') {
                    cookie = cookie.slice(1, -1);
                }

                try {
                    var name = parts[0].replace(rdecode, decodeURIComponent);
                    cookie = converter.read ?
                        converter.read(cookie, name) : converter(cookie, name) ||
                    cookie.replace(rdecode, decodeURIComponent);

                    if (this.json) {
                        try {
                            cookie = JSON.parse(cookie);
                        } catch (e) {
                        }
                    }

                    if (key === name) {
                        result = cookie;
                        break;
                    }

                    if (!key) {
                        result[name] = cookie;
                    }
                } catch (e) {
                }
            }

            return result;
        }

        api.set = api;
        api.get = function (key) {
            return api.call(api, key);
        };
        api.getJSON = function () {
            return api.apply({
                json: true
            }, [].slice.call(arguments));
        };
        api.defaults = {};

        api.remove = function (key, attributes) {
            api(key, '', extend(attributes, {
                expires: -1
            }));
        };

        api.withConverter = init;

        return api;
    }

    return init(function () {
    });
}));


/**
 Core script to handle the entire theme and core functions
 **/
var Layout = function () {
    var layoutImgPath = 'admin/layout/img/';

    var layoutCssPath = 'admin/layout/css/';

    //* BEGIN:CORE HANDLERS *//
    // this function handles responsive layout on screen size resize or mobile device rotate.

    // Set proper height for sidebar and content. The content and sidebar height must be synced always.
    var handleSidebarAndContentHeight = function () {
        var content = $('.page-content');
        var sidebar = $('.page-sidebar');
        var body = $('body');
        var height;

        if (body.hasClass("page-footer-fixed") === true && body.hasClass("page-sidebar-fixed") === false) {
            var available_height = Metronic.getViewPort().height - $('.page-footer').outerHeight() - $('.page-header').outerHeight();
            if (content.height() < available_height) {
                content.attr('style', 'min-height:' + available_height + 'px');
            }
        } else {
            if (body.hasClass('page-sidebar-fixed')) {
                height = _calculateFixedSidebarViewportHeight();
                if (body.hasClass('page-footer-fixed') === false) {
                    height = height - $('.page-footer').outerHeight();
                }
            } else {
                var headerHeight = $('.page-header').outerHeight();
                var footerHeight = $('.page-footer').outerHeight();

                if (Metronic.getViewPort().width < 992) {
                    height = Metronic.getViewPort().height - headerHeight - footerHeight;

                } else {
                    height = sidebar.height() + 30;
                }

                if ((height + headerHeight + footerHeight) <= Metronic.getViewPort().height) {
                    height = Metronic.getViewPort().height - headerHeight - footerHeight;
                }
            }

            if (sidebar.hasClass('page-scan-preview')) {
                $('.issuu-embed-container').css({'padding-bottom': (Metronic.getViewPort().height - 46 - 45) + 'px'});
                $('.issuu-img-container').css({'min-height': (Metronic.getViewPort().height - 46 - 45) + 'px'});
            }

            content.attr('style', 'min-height:' + height + 'px');
        }
    };

    // Handle sidebar menu links
    var handleSidebarMenuActiveLink = function (mode, el) {
        var url = location.hash.toLowerCase();

        var menu = $('.page-sidebar-menu');

        if (mode === 'click' || mode === 'set') {
            el = $(el);
        } else if (mode === 'match') {
            menu.find("li > a").each(function () {
                var path = $(this).attr("href").toLowerCase();
                // url match condition
                if (path.length > 1 && url.substr(1, path.length - 1) == path.substr(1)) {
                    el = $(this);
                    return;
                }
            });
        }

        if (!el || el.length == 0) {
            return;
        }

        if (el.attr('href').toLowerCase() === 'javascript:;' || el.attr('href').toLowerCase() === '#') {
            return;
        }

        var slideSpeed = parseInt(menu.data("slide-speed"));
        var keepExpand = menu.data("keep-expanded");

        // disable active states
        menu.find('li.active').removeClass('active');
        menu.find('li > a > .selected').remove();

        if (menu.hasClass('page-sidebar-menu-hover-submenu') === false) {
            menu.find('li.open').each(function () {
                if ($(this).children('.sub-menu').length === 0) {
                    $(this).removeClass('open');
                    $(this).find('> a > .arrow.open').removeClass('open');
                }
            });
        } else {
            menu.find('li.open').removeClass('open');
        }

        el.parents('li').each(function () {
            $(this).addClass('active');
            $(this).find('> a > span.arrow').addClass('open');

            if ($(this).parent('ul.page-sidebar-menu').length === 1) {
                $(this).find('> a').append('<span class="selected"></span>');
            }

            if ($(this).children('ul.sub-menu').length === 1) {
                $(this).addClass('open');
            }
        });

        if (mode === 'click') {
            if (Metronic.getViewPort().width < 992 && $('.page-sidebar').hasClass("in")) { // close the menu on mobile view while laoding a page
                $('.page-header .responsive-toggler').click();
            }
        }
    };

    $('.nav-item_accordeon').removeClass('active');
    $('.nav-item_accordeon').removeClass('open');
    $('.nav-item_accordeon').click(function () {
        var arrOpen = $(this);
        var aaa = $('.nav-link.nav-toggle');
        if ($('.page-sidebar.navbar-collapse.collapse').width() < 230) {
            /*aaa.unbind('click').removeClass('open').removeClass('active').removeClass('active_menu');*/
            console.log('привет');
        }
        $('.nav-item_accordeon:not(.active_menu)').removeClass('open');

        if ($('.nav-item_accordeon').hasClass('active_menu')) {
            arrOpen.addClass('open');
        } else {
            arrOpen.removeClass('open');
        }
    });
    $('.nav-item_accordeon:not(.my-companies) ul li.active').parent().parent().css('background', '#3c6c95').addClass('active_menu').addClass('open');

    $('.nav-item_accordeon').removeClass('active');
    $('.nav-item_accordeon').removeClass('open');
    $('.nav-item_accordeon').click(function () {
        var arrOpen = $(this);
        var aaa = $('.nav-link.nav-toggle');
        if ($('.page-sidebar.navbar-collapse.collapse').width() < 230) {
            /*aaa.unbind('click').removeClass('open').removeClass('active').removeClass('active_menu');*/
            console.log('привет');
        }
        $('.nav-item_accordeon:not(.active_menu)').removeClass('open');

        if ($('.nav-item_accordeon').hasClass('active_menu')) {
            arrOpen.addClass('open');
        } else {
            arrOpen.removeClass('open');
        }
    });
    $('.nav-item_accordeon ul li.active').parent().parent().css('background', '#3c6c95').addClass('active_menu').addClass('open');

    $('.nav-item_accordeon.my-companies ul li.active').parent().parent().removeClass('active_menu').removeClass('open');

    /*        $('.nav-item_accordeon').click(function(){
     var arrOpen = $(this);
     $('.nav-item_accordeon').removeClass('open');
     if($('.nav-item_accordeon').hasClass('active')) {
     arrOpen.removeClass('active_menu');
     arrOpen.removeClass('active');
     arrOpen.removeClass('open');
     } else {
     console.log('привет');
     arrOpen.addClass('active');
     }
     if ($('.nav-item_accordeon').hasClass('active_menu')) {
     arrOpen.addClass('open');
     } else {
     arrOpen.removeClass('open');
     }
     }); */

    // Handle sidebar menu
    // var handleSidebarMenu = function () {
    //     // handle sidebar link click
    //     jQuery('.page-sidebar').on('click', 'li > a', function (e) {
    //         var hasSubMenu = $(this).next().hasClass('sub-menu');

    //         if (Metronic.getViewPort().width >= 992 && $(this).parents('.page-sidebar-menu-hover-submenu').length === 1) { // exit of hover sidebar menu
    //             return;
    //         }

    //         if (hasSubMenu === false) {
    //             if (Metronic.getViewPort().width < 992 && $('.page-sidebar').hasClass("in")) { // close the menu on mobile view while laoding a page
    //                 $('.page-header .responsive-toggler').click();
    //             }
    //             return;
    //         }

    //         if ($(this).next().hasClass('sub-menu always-open')) {
    //             return;
    //         }

    //         var parent = $(this).parent().parent();
    //         var the = $(this);
    //         var menu = $('.page-sidebar-menu');
    //         var sub = jQuery(this).next();

    //         var autoScroll = menu.data("auto-scroll");
    //         var slideSpeed = parseInt(menu.data("slide-speed"));
    //         var keepExpand = menu.data("keep-expanded");

    //         if (keepExpand !== true) {
    //             parent.children('li.open').children('a').children('.arrow').removeClass('open');
    //             parent.children('li.open').children('.sub-menu:not(.always-open)').slideUp(slideSpeed);
    //             parent.children('li.open').removeClass('open');
    //         }

    //         var slideOffeset = -200;

    //         if (sub.is(":visible")) {
    //             jQuery('.arrow', jQuery(this)).removeClass("open");
    //             jQuery(this).parent().removeClass("open");
    //             sub.slideUp(slideSpeed, function () {
    //                 if (autoScroll === true && $('body').hasClass('page-sidebar-closed') === false) {
    //                     if ($('body').hasClass('page-sidebar-fixed')) {
    //                         menu.slimScroll({
    //                             'scrollTo': (the.position()).top
    //                         });
    //                     } else {
    //                         Metronic.scrollTo(the, slideOffeset);
    //                     }
    //                 }
    //                 handleSidebarAndContentHeight();
    //             });
    //         } else if (hasSubMenu) {
    //             jQuery('.arrow', jQuery(this)).addClass("open");
    //             jQuery(this).parent().addClass("open");
    //             sub.slideDown(slideSpeed, function () {
    //                 if (autoScroll === true && $('body').hasClass('page-sidebar-closed') === false) {
    //                     if ($('body').hasClass('page-sidebar-fixed')) {
    //                         menu.slimScroll({
    //                             'scrollTo': (the.position()).top
    //                         });
    //                     } else {
    //                         Metronic.scrollTo(the, slideOffeset);
    //                     }
    //                 }
    //                 handleSidebarAndContentHeight();
    //             });
    //         }

    //         e.preventDefault();
    //     });

    //     // handle ajax links within sidebar menu
    //     jQuery('.page-sidebar').on('click', ' li > a.ajaxify', function (e) {
    //         e.preventDefault();
    //         Metronic.scrollTop();

    //         var url = $(this).attr("href");
    //         var menuContainer = jQuery('.page-sidebar ul');
    //         var pageContent = $('.page-content');
    //         var pageContentBody = $('.page-content .page-content-body');

    //         menuContainer.children('li.active').removeClass('active');
    //         menuContainer.children('arrow.open').removeClass('open');

    //         $(this).parents('li').each(function () {
    //             $(this).addClass('active');
    //             $(this).children('a > span.arrow').addClass('open');
    //         });
    //         $(this).parents('li').addClass('active');

    //         if (Metronic.getViewPort().width < 992 && $('.page-sidebar').hasClass("in")) { // close the menu on mobile view while laoding a page
    //             $('.page-header .responsive-toggler').click();
    //         }

    //         Metronic.startPageLoading();

    //         var the = $(this);

    //         $.ajax({
    //             type: "GET",
    //             cache: false,
    //             url: url,
    //             dataType: "html",
    //             success: function (res) {
    //                 if (the.parents('li.open').length === 0) {
    //                     $('.page-sidebar-menu > li.open > a').click();
    //                 }

    //                 Metronic.stopPageLoading();
    //                 pageContentBody.html(res);
    //                 Layout.fixContentHeight(); // fix content height
    //                 Metronic.initAjax(); // initialize core stuff
    //             },
    //             error: function (xhr, ajaxOptions, thrownError) {
    //                 Metronic.stopPageLoading();
    //                 pageContentBody.html('<h4>Could not load the requested content.</h4>');
    //             }
    //         });
    //     });

    //     // handle ajax link within main content
    //     jQuery('.page-content').on('click', '.ajaxify', function (e) {
    //         e.preventDefault();
    //         Metronic.scrollTop();

    //         var url = $(this).attr("href");
    //         var pageContent = $('.page-content');
    //         var pageContentBody = $('.page-content .page-content-body');

    //         Metronic.startPageLoading();

    //         if (Metronic.getViewPort().width < 992 && $('.page-sidebar').hasClass("in")) { // close the menu on mobile view while laoding a page
    //             $('.page-header .responsive-toggler').click();
    //         }

    //         $.ajax({
    //             type: "GET",
    //             cache: false,
    //             url: url,
    //             dataType: "html",
    //             success: function (res) {
    //                 Metronic.stopPageLoading();
    //                 pageContentBody.html(res);
    //                 Layout.fixContentHeight(); // fix content height
    //                 Metronic.initAjax(); // initialize core stuff
    //             },
    //             error: function (xhr, ajaxOptions, thrownError) {
    //                 pageContentBody.html('<h4>Could not load the requested content.</h4>');
    //                 Metronic.stopPageLoading();
    //             }
    //         });
    //     });

    //     // handle sidebar hover effect
    //     handleFixedSidebarHoverEffect();

    //     // handle the search bar close
    //     $('.page-sidebar').on('click', '.sidebar-search .remove', function (e) {
    //         e.preventDefault();
    //         $('.sidebar-search').removeClass("open");
    //     });

    //     // handle the search query submit on enter press
    //     $('.page-sidebar .sidebar-search').on('keypress', 'input.form-control', function (e) {
    //         if (e.which == 13) {
    //             $('.sidebar-search').submit();
    //             return false; //<---- Add this line
    //         }
    //     });

    //     // handle the search submit(for sidebar search and responsive mode of the header search)
    //     $('.sidebar-search .submit').on('click', function (e) {
    //         e.preventDefault();
    //         if ($('body').hasClass("page-sidebar-closed")) {
    //             if ($('.sidebar-search').hasClass('open') === false) {
    //                 if ($('.page-sidebar-fixed').length === 1) {
    //                     $('.page-sidebar .sidebar-toggler').click(); //trigger sidebar toggle button
    //                 }
    //                 $('.sidebar-search').addClass("open");
    //             } else {
    //                 $('.sidebar-search').submit();
    //             }
    //         } else {
    //             $('.sidebar-search').submit();
    //         }
    //     });

    //     // handle close on body click
    //     if ($('.sidebar-search').length !== 0) {
    //         $('.sidebar-search .input-group').on('click', function(e){
    //             e.stopPropagation();
    //         });

    //         $('body').on('click', function() {
    //             if ($('.sidebar-search').hasClass('open')) {
    //                 $('.sidebar-search').removeClass("open");
    //             }
    //         });
    //     }
    // };

    // Helper function to calculate sidebar height for fixed sidebar layout.
    var _calculateFixedSidebarViewportHeight = function () {
        var sidebarHeight = Metronic.getViewPort().height - $('.page-header').outerHeight();
        if ($('body').hasClass("page-footer-fixed")) {
            sidebarHeight = sidebarHeight - $('.page-footer').outerHeight();
        }

        return sidebarHeight;
    };

    // Handles fixed sidebar
    var handleFixedSidebar = function () {
        var menu = $('.page-sidebar-menu');

        Metronic.destroySlimScroll(menu);

        if ($('.page-sidebar-fixed').length === 0) {
            handleSidebarAndContentHeight();
            return;
        }

        if (Metronic.getViewPort().width >= 992) {
            menu.attr("data-height", _calculateFixedSidebarViewportHeight());
            Metronic.initSlimScroll(menu);
            handleSidebarAndContentHeight();
        }
    };

    // Handles sidebar toggler to close/hide the sidebar.
    var handleFixedSidebarHoverEffect = function () {
        var body = $('body');
        if (body.hasClass('page-sidebar-fixed')) {
            $('.page-sidebar').on('mouseenter', function () {
                if (body.hasClass('page-sidebar-closed')) {
                    $(this).find('.page-sidebar-menu').removeClass('page-sidebar-menu-closed');
                }
            }).on('mouseleave', function () {
                if (body.hasClass('page-sidebar-closed')) {
                    $(this).find('.page-sidebar-menu').addClass('page-sidebar-menu-closed');
                }
            });
        }
    };

    // Hanles sidebar toggler
    var handleSidebarToggler = function () {
        var body = $('body');
       // if (Cookies.get('sidebar_closed') === '1' && Metronic.getViewPort().width >= 992) {
       //     $('body').addClass('page-sidebar-closed');
       //     $('.page-sidebar-menu').addClass('page-sidebar-menu-closed');
       //     $('.logo-default').css({'display': 'none'});
       //     console.log('hidden-menu');
       // }
       // else {
//
       //     console.log('visible-menu');
       // }

        // handle sidebar show/hide
        $('body').on('click', '.sidebar-toggler', function (e) {
            var sidebar = $('.page-sidebar');
            var sidebarMenu = $('.page-sidebar-menu');
            var logoDefault = $('.logo-default');
            var summaryInfo = $('.summary-info');
            var $status = 0;
            $(".sidebar-search", sidebar).removeClass("open");

            if (body.hasClass("page-sidebar-closed")) {
                body.removeClass("page-sidebar-closed");
                sidebarMenu.removeClass("page-sidebar-menu-closed");
                logoDefault.css({'display': 'inline'});
                summaryInfo.css({'width': 'calc(100% - 275px)'});

                //Cookies.set('sidebar_closed', '0');

            } else {
                body.addClass("page-sidebar-closed");
                sidebarMenu.addClass("page-sidebar-menu-closed");
                logoDefault.css({'display': 'none'});
                if (body.hasClass("page-sidebar-fixed")) {
                    sidebarMenu.trigger("mouseleave");
                }
                summaryInfo.css({'width': 'calc(100% - 85px)'});
                $status = 1;
                //Cookies.set('sidebar_closed', '1');
            }

            if ($(this).hasClass('admin')) {
                $.post('/admin/site/sidebar-status', {
                    status: $status
                }, function (data) {

                });
            } else {
                $.post('/site/sidebar-status', {
                    minimize_side_menu: $status
                }, function (data) {

                });
            }
            $(window).trigger('resize');
        });
    };

    // Handles the horizontal menu
    var handleHorizontalMenu = function () {
        //handle tab click
        $('.page-header').on('click', '.hor-menu a[data-toggle="tab"]', function (e) {
            e.preventDefault();
            var nav = $(".hor-menu .nav");
            var active_link = nav.find('li.current');
            $('li.active', active_link).removeClass("active");
            $('.selected', active_link).remove();
            var new_link = $(this).parents('li').last();
            new_link.addClass("current");
            new_link.find("a:first").append('<span class="selected"></span>');
        });

        // handle search box expand/collapse
        $('.page-header').on('click', '.search-form', function (e) {
            $(this).addClass("open");
            $(this).find('.form-control').focus();

            $('.page-header .search-form .form-control').on('blur', function (e) {
                $(this).closest('.search-form').removeClass("open");
                $(this).unbind("blur");
            });
        });

        // handle hor menu search form on enter press
        $('.page-header').on('keypress', '.hor-menu .search-form .form-control', function (e) {
            if (e.which == 13) {
                $(this).closest('.search-form').submit();
                return false;
            }
        });

        // handle header search button click
        $('.page-header').on('mousedown', '.search-form.open .submit', function (e) {
            e.preventDefault();
            e.stopPropagation();
            $(this).closest('.search-form').submit();
        });

        // handle hover dropdown menu for desktop devices only
        $('[data-hover="megamenu-dropdown"]').not('.hover-initialized').each(function () {
            $(this).dropdownHover();
            $(this).addClass('hover-initialized');
        });

        $(document).on('click', '.mega-menu-dropdown .dropdown-menu', function (e) {
            e.stopPropagation();
        });
    };

    // Handles Bootstrap Tabs.
    var handleTabs = function () {
        // fix content height on tab click
        $('body').on('shown.bs.tab', 'a[data-toggle="tab"]', function () {
            handleSidebarAndContentHeight();
        });
    };

    // Handles the go to top button at the footer
    var handleGoTop = function () {
        var offset = 300;
        var duration = 500;

        if (navigator.userAgent.match(/iPhone|iPad|iPod/i)) {  // ios supported
            $(window).bind("touchend touchcancel touchleave", function (e) {
                if ($(this).scrollTop() > offset) {
                    $('.scroll-to-top').fadeIn(duration);
                } else {
                    $('.scroll-to-top').fadeOut(duration);
                }
            });
        } else {  // general
            $(window).scroll(function () {
                if ($(this).scrollTop() > offset) {
                    $('.scroll-to-top').fadeIn(duration);
                } else {
                    $('.scroll-to-top').fadeOut(duration);
                }
            });
        }

        $('.scroll-to-top').click(function (e) {
            e.preventDefault();
            $('html, body').animate({scrollTop: 0}, duration);
            return false;
        });
    };

    // Hanlde 100% height elements(block, portlet, etc)
    var handle100HeightContent = function () {

        var target = $('.full-height-content');
        var height;

        height = Metronic.getViewPort().height -
            $('.page-header').outerHeight(true) -
            $('.page-footer').outerHeight(true) -
            $('.page-title').outerHeight(true) -
            $('.page-bar').outerHeight(true);

        if (target.hasClass('portlet')) {
            var portletBody = target.find('.portlet-body');

            if (Metronic.getViewPort().width < 992) {
                Metronic.destroySlimScroll(portletBody.find('.full-height-content-body')); // destroy slimscroll
                return;
            }

            height = height -
                target.find('.portlet-title').outerHeight(true) -
                parseInt(target.find('.portlet-body').css('padding-top')) -
                parseInt(target.find('.portlet-body').css('padding-bottom')) - 2;

            if (target.hasClass("full-height-content-scrollable")) {
                height = height - 35;
                portletBody.find('.full-height-content-body').css('height', height);
                Metronic.initSlimScroll(portletBody.find('.full-height-content-body'));
            } else {
                portletBody.css('min-height', height);
            }
        } else {
            if (Metronic.getViewPort().width < 992) {
                Metronic.destroySlimScroll(target.find('.full-height-content-body')); // destroy slimscroll
                return;
            }

            if (target.hasClass("full-height-content-scrollable")) {
                height = height - 35;
                target.find('.full-height-content-body').css('height', height);
                Metronic.initSlimScroll(target.find('.full-height-content-body'));
            } else {
                target.css('min-height', height);
            }
        }
    };
    //* END:CORE HANDLERS *//

    return {
        // Main init methods to initialize the layout
        //IMPORTANT!!!: Do not modify the core handlers call order.

        initHeader: function () {
            handleHorizontalMenu(); // handles horizontal menu
        },

        setSidebarMenuActiveLink: function (mode, el) {
            handleSidebarMenuActiveLink(mode, el);
        },

        initSidebar: function () {
            //layout handlers
            handleFixedSidebar(); // handles fixed sidebar menu
            // handleSidebarMenu(); // handles main menu
            handleSidebarToggler(); // handles sidebar hide/show

            if (Metronic.isAngularJsApp()) {
                handleSidebarMenuActiveLink('match'); // init sidebar active links
            }

            Metronic.addResizeHandler(handleFixedSidebar); // reinitialize fixed sidebar on window resize
        },

        initContent: function () {
            handle100HeightContent(); // handles 100% height elements(block, portlet, etc)
            handleTabs(); // handle bootstrah tabs

            Metronic.addResizeHandler(handleSidebarAndContentHeight); // recalculate sidebar & content height on window resize
            Metronic.addResizeHandler(handle100HeightContent); // reinitialize content height on window resize
        },

        initFooter: function () {
            handleGoTop(); //handles scroll to top functionality in the footer
        },

        init: function () {
            this.initHeader();
            this.initSidebar();
            this.initContent();
            this.initFooter();
        },

        //public function to fix the sidebar and content height accordingly
        fixContentHeight: function () {
            handleSidebarAndContentHeight();
        },

        initFixedSidebarHoverEffect: function () {
            handleFixedSidebarHoverEffect();
        },

        initFixedSidebar: function () {
            handleFixedSidebar();
        },

        getLayoutImgPath: function () {
            return Metronic.getAssetsPath() + layoutImgPath;
        },

        getLayoutCssPath: function () {
            return Metronic.getAssetsPath() + layoutCssPath;
        }
    };
}();


$(document).on('ready', function () {
    if ($("select").is(".nds-select")) {

        $(".nds-select").select2({
            minimumResultsForSearch: Infinity
        });

    }

    if ($("select").is("#contractor-company_type_id")) {

        $("#contractor-company_type_id").select2({
            minimumResultsForSearch: Infinity
        });

    }
    $('#create-company').on('shown.bs.modal', function (e) {
        console.log('21123213')
        if ($("select").is(".company-create-select")) {

            $(".company-create-select").select2({
                minimumResultsForSearch: Infinity
            });

        }
    });
    if ($("select").is(".company-create-select")) {

        $(".company-create-select").select2({
            minimumResultsForSearch: Infinity
        });

    }
    $(document).mouseup(function (e) {
        var container = $(".vidimus-type-item");
        if (container.has(e.target).length === 0) {
            container.hide();
        }
    });
});


$(document).on('ready', function () {
    function menufixed() {
        var menuh = $('#side-menu').outerHeight() + 60;
        var winh = $(window).height();
        if (menuh < winh) {
            $('#side-menu').addClass('fixed');
        }
        else {
            $('#side-menu').removeClass('fixed');
        }
    }
    function menudropdown() {

     //   if (window.location.pathname === "/site/index" || window.location.pathname === "/") {
     //       $('.page-header').on('mouseenter', function() {
     //           $('.dropdown-notification-menu').addClass('open');
     //       });
     //       $('.page-sidebar').on('mouseenter', function() {
     //           $('.dropdown-notification-menu').addClass('open');
     //       });
     //   }
    }

    $(window).on('load', function () {
        menufixed();
        menudropdown();

        var timerId = setInterval(function () {
            menufixed()
        }, 500);

        if (window.location.pathname === '/site/index') {
            $('.current-company a').addClass('active-company');
        }

    });
    $(window).on('resize', function () {
        menufixed();
    });

});

$(document).ready(function() {
    if ($('body').hasClass("page-sidebar-closed")) {
        $('.summary-info').css({'width': 'calc(100% - 85px)'});
    }
});