
var taxrobotPayPanelOpen = function(group) {
    $(".taxrobot-pay-panel-trigger").addClass("active");
    $(".page-shading-panel").removeClass("hidden");
    $(".taxrobot-pay-panel").show("fast");
    $("#visible-right-menu-message-panel").show();
    $("html").attr("style", "overflow: hidden;");
    $(".taxrobot-pay-panel .main-block").scrollTop(0);
}

var taxrobotPayPanelClose = function() {
    $(".taxrobot-pay-panel-trigger").removeClass("active");
    $(".taxrobot-pay-panel").hide("fast", function() {
        console.log(this);
        $(".page-shading-panel").addClass("hidden");
        $(".additional-content", this).toggleClass("hidden", true);
        $(".main-content", this).toggleClass("hidden", false);
    });
    $("#visible-right-menu-message-panel").hide();
    $("html").removeAttr("style");
}

$(document).on("click", ".taxrobot-pay-panel-trigger", function (e) {
    e.preventDefault();
    taxrobotPayPanelOpen();
});

$(document).on("click", ".taxrobot-pay-panel .tariff-block-button", function (e) {
    var panel = $(this).closest('.taxrobot-pay-panel');
    var group = $(this).data('group');
    $(".additional-content.group_"+group, panel).toggleClass("hidden", false);
    $(".main-content", panel).toggleClass("hidden", true);
});

$(document).on("click", ".taxrobot-pay-panel .tariff-select-cancel", function (e) {
    var panel = $(this).closest('.taxrobot-pay-panel');
    $(".additional-content", panel).toggleClass("hidden", true);
    $(".main-content", panel).toggleClass("hidden", false);
});

$(document).on("click", ".taxrobot-pay-panel-close", function () {
    taxrobotPayPanelClose();
});

$(document).on("click", ".modal-order-load-statement", function(e) {
    $('.update-order-movement-link').click();
});

$(document).on("click", ".modal-bank-load-statement-1c", function(e) {
    $('.banking-module-open-link').click();
});

$(document).on("click", ".modal-bank-load-statement", function(e) {
    $('.banking-module-open-link').click();
});

$(document).on("click", ".modal-ofd-load-statement", function(e) {
    $('.ofd-module-open-link').click();
});

$(document).on("click", ".modal-bank-add-statement", function(e) {
    $('.update-bank-movement-link').click();
});

$(document).on("click", ".tariff-group-payment-form .choose-tariff", function(e) {
    e.preventDefault();
    var tariff = $(this);
    var form = tariff.closest('form');
    var tariffId = tariff.data('tariff-id');
    $('#paymentform-tariffid', form).val(tariffId);
    tariff.closest('.dropdown').removeClass('open').find('.dropdown-toggle').html(tariff.html());
    $('.tariff-price', form).addClass('hidden');
    $('.tariff-price-'+tariffId, form).removeClass('hidden');

    var per_month = $('.tariff-price-per-month');
    $('.tariff-price', per_month).addClass('hidden');
    $('.tariff-price-'+tariffId, per_month).removeClass('hidden');
});

$(document).on('click', '.tariff-group-payment-form .submit', function (e) {
    e.preventDefault();
    var button = this;
    var form = $(this).closest('form');
    var formData = form.serializeArray();
    formData.push({ name: button.name, value: button.value });
    $(button).addClass('ladda-button');
    var l = Ladda.create(button);
    l.start();
    $.post($(form).attr('action'), formData, function(data) {
        Ladda.stopAll();
        $(button).removeClass('ladda-button');
        if (data.content) {
            $('.form-submit-result', form).html(data.content);
        }
        if (data.alert && typeof data.alert === 'object') {
            for (alertType in data.alert) {
                var offset = $('.navbar-fixed-top').outerHeight(true) +20;
                $.alert('#js-form-alert', data.alert[alertType], {type: alertType, topOffset: offset});
            }
        }
        if (data.done) {
            if (data.link) {
                var link=document.createElement("a");
                link.id = 'document-print-link';
                link.href = data.link;
                link.target = '_blank';
                $('.form-submit-result', form).html(link);
                document.getElementById('document-print-link').click();
            }
        }
    });
});