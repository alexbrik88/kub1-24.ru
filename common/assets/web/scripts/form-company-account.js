
/* Company accounts */
!function( $ ) {
    var checkCompanyAccountFormFields = function (form) {
        let isRub = $(form).data('ruble') == '1';
        let isFor = $(form).data('foreign') == '1';
        $("input#checkingaccountant-rs", form).inputmask({mask: isFor ? '*{0,34}' : (isRub ? '9{5}8109{12}' : '9{20}')});
        $('.foreign_currency_show', form).toggleClass('hidden', isRub && !isFor);
        $(".ruble_hidden", form).toggleClass("hidden", isRub);
        $(".currency_hidden", form).toggleClass("hidden", !isRub);
        $(".ruble_disabled", form).prop('disabled', isRub);
        $(".currency_disabled", form).prop('disabled', !isRub);
        $(".russian_hidden", form).toggleClass("hidden", !isFor);
        $(".foreign_hidden", form).toggleClass("hidden", isFor);
        $(".russian_disabled", form).prop('disabled', !isFor);
        $(".foreign_disabled", form).prop('disabled', isFor);
    };
    $(document).on("change", "input.account_start_balance_checkbox", function (e) {
        let form = this.form;
        $(".start-balance-block", form).toggleClass("hidden", !this.checked);
    });
    $(document).on("change", "select.select-accountant-currency_id", function (e) {
        let form = this.form;
        let isRub = this.value == '643';
        let value = this.value;
        let currency = $(this).data('currency');
        $(form).data('ruble', isRub ? 1 : 0);
        $('.currency_name_tag', form).text(currency[value] || '');
        checkCompanyAccountFormFields(form);
    });
    $(document).on("change", "input#checkingaccountant-is_foreign_bank", function (e) {
        let form = this.form;
        $(form).data('foreign', this.checked ? 1 : 0);
        checkCompanyAccountFormFields(form);
    });
    $(document).on("change input", "input#checkingaccountant-bik", function (e) {
        $('input#checkingaccountant-is_foreign_bank', this.form).prop('readonly', this.value != '').uniform('refresh');
    });
    $(document).on('afterValidate', 'form.form-checking-accountant', function (event, messages, errorAttributes) {
        let form = this;
        if ($('#accountDopColumns .is-invalid', form).length) {
            $('#accountDopColumns', form).collapse('show');
        }
    });
    $(document).ready(function () {
        $('form.form-checking-accountant').each(function () {
            checkCompanyAccountFormFields(this);1
        });
    });
}( window.jQuery );

/* Contractor accounts */
!function( $ ) {
    $(document).on("change", "select#contractoraccount-currency_id", function (e) {
        let form = this.form;
        let isRub = this.value == '643';
        let isForeign = $("input#contractoraccount-is_foreign_bank", form).is(':checked');
        $(".currency_ruble", form).toggleClass("hidden", !isRub);
        $(".currency_foreign", form).toggleClass("hidden", isRub);
        $(".currency_ruble input", form).prop('disabled', !isRub);
        $(".currency_foreign input", form).prop('disabled', isRub);
        $(".currency_ruble .validating, .currency_foreign .validating", form).removeClass("validating");
        $(".currency_ruble .is-invalid, .currency_foreign .is-invalid", form).removeClass("is-invalid");
        $(".currency_ruble .is-valid, .currency_foreign .is-valid", form).removeClass("is-valid");
        $(".currency_ruble .invalid-feedback, .currency_foreign .invalid-feedback", form).remove();
        $(".no_mask_wrap", form).toggleClass("hidden", !isRub || isForeign);
    });
    $(document).on("change", "input#contractoraccount-is_foreign_bank", function (e) {
        let form = this.form;
        let isForeign = this.checked;
        let isRub = $("select#contractoraccount-currency_id", form).val() == '643';
        $(".russian_bank", form).toggleClass("hidden", isForeign);
        $(".foreign_bank", form).toggleClass("hidden", !isForeign);
        $(".russian_bank input", form).prop('disabled', isForeign);
        $(".foreign_bank input", form).prop('disabled', !isForeign);
        $(".russian_bank .validating, .foreign_bank .validating", form).removeClass("validating");
        $(".russian_bank .is-invalid, .foreign_bank .is-invalid", form).removeClass("is-invalid");
        $(".russian_bank .is-valid, .foreign_bank .is-valid", form).removeClass("is-valid");
        $(".russian_bank .invalid-feedback, .foreign_bank .invalid-feedback", form).remove();
        $(".no_mask_wrap", form).toggleClass("hidden", !isRub || isForeign);
    });
    $(document).on("change", "input#contractoraccount-no_mask", function (e) {
        var input = $("#contractoraccount-rs");
        if (this.checked) {
            input.inputmask({"mask":"9{20}"});
        } else {
            input.inputmask({"mask":"9{5}8109{12}"});
        }
    });
}( window.jQuery );
