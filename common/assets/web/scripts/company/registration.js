$(function () {

    $(document)
        .on('change keyup', '.username-bind', function (e) {
            $(this.form).find('.username-bind').not(this).val(this.value);
        })
        .on('init change', '.company-type-chooser input', function (e) {
            if (!this.checked) {
                return true;
            }

            var blockClass = this.value == 1 /*ip*/ ? 'company-type-block-ip' : 'company-type-block-non-ip';

            $(this).parents('.company-type-chooser')
                .siblings().hide()
                .filter('.' + blockClass).show();
        });

    $(document)
        .find('.company-type-chooser input:checked')
        .trigger('init');

});
