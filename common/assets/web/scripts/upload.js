$(document).ready(function () {
    // todo: create plugin.

    var _uploadAreaSelector = '.upload-area';
    var _fileListSelector = '.file-list';
    var _uploadButtonSelector = '.upload-button';

    var _$uploadArea = $(_uploadAreaSelector);
    var _$fileLIst = _$uploadArea.find(_fileListSelector);
    var _$uploadButton = _$uploadArea.find(_uploadButtonSelector);
    var _$uploadInput = _$uploadArea.find('input[type=file]');
    
    var _uploadUrl = _$uploadArea.data('upload-url');
    var _deleteUrl = _$uploadArea.data('delete-url');
    var _listUrl = _$uploadArea.data('list-url');
    var _csrfParameter = _$uploadArea.data('csrf-parameter');
    var _csrfToken = _$uploadArea.data('csrf-token');

    if (_$uploadArea.length === 0) {
        return;
    }

    // upload file


    // delete file
    $(document).on('click', '.delete-file', function (e) {
        $("#file-ajax-loading").show();
        e.preventDefault();
        var $this = $(this);
        var fileId = $this.data('file');
        var $file = $('#file-' + fileId);

        deleteFile($file);
    });

    function deleteFile($file) {
        $("#file-ajax-loading").show();
        $.ajax(_deleteUrl, {
            data: {
                'file-id': $file.data('id')
            },
            method: 'POST',
            success: function (data, textStatus, jqXHR) {
                updateFileList();
                $("#file-ajax-loading").hide();
            },
            error: function (jqXHR, textStatus, errorThrown) {
                updateFileList();
                $("#file-ajax-loading").hide();
                alert(jqXHR.responseText);
            }
        });
    }

    // update file list
    function updateFileList() {
        $("#file-ajax-loading").show();
        _$fileLIst.load(_listUrl + ' ' + _fileListSelector, function () {
            toggleUploadButton();
            $("#file-ajax-loading").hide();
        });
    }

    // shows or hides upload button
    function toggleUploadButton() {
        if ($('.file-list .file').length < 3 ) {
        _$uploadButton.toggleClass('hide', false);
        } else {
            _$uploadButton.toggleClass('hide', true);
        }
    }

    // init
    updateFileList();

    var uploadData = {};
    uploadData[_csrfParameter] = _csrfToken;

    var uploader = new ss.SimpleUpload({
        button: _$uploadButton[0], // HTML element used as upload button
        url: _uploadUrl, // URL of server-side upload handler
        data: uploadData,
        multipart: true,    
        multiple: true,
        encodeCustomHeaders: true,
        responseType: 'json',
        name: 'file', // Parameter name of the uploaded file
        onSubmit: function() {
            $("#file-ajax-loading").show();
        },
        onComplete: function (filename, response) {
            updateFileList();
            $("#file-ajax-loading").hide();
            if (response['success'] === false) {
                alert(response['msg']);
            }
        }
    });
});
