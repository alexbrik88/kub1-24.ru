
!function( $ ) {
    var internalTransferFormNeedRate = function (form)  {
        let from = $('#internaltransferform-amount_from', form);
        let to = $('#internaltransferform-amount_to', form);

        return from.data('currency').length && to.data('currency').length && from.data('currency') != to.data('currency');
    };
    var internalTransferFormCheckAmount = function (form) {
        let from = $('#internaltransferform-amount_from', form);
        let to = $('#internaltransferform-amount_to', form);
        console.log(from.data('currency'));
        console.log(to.data('currency'));
        if (from.data('currency') == '' || to.data('currency') == '' || from.data('currency') == to.data('currency')) {
            if (from.val()) {
                to.val(from.val());
            } else if (to.val()) {
                from.val(to.val());
            }
            to.prop('readonly', false);
        } else {
            to.prop('readonly', false);
        }

        let rate = $('#internaltransferform-rate_value', form);
        if (internalTransferFormNeedRate(form)) {
            if (to.data('currency') == 'RUB') {
                $('.rate_curr_1_name', form).text(to.data('currency'));
                $('.rate_curr_2_name', form).text(from.data('currency'));
            } else {
                $('.rate_curr_1_name', form).text(from.data('currency'));
                $('.rate_curr_2_name', form).text(to.data('currency'));
            }
            rate.closest('.rate_value_vrap').toggleClass('hidden', false);
        } else {
            rate.closest('.rate_value_vrap').toggleClass('hidden', true);
        }
    };
    var internalTransferFormRateByAmount = function (form) {
        let from = $('#internaltransferform-amount_from', form);
        let to = $('#internaltransferform-amount_to', form);
        let rate = $('#internaltransferform-rate_value', form);
        if (rate.is(':visible')) {
            let fromVal = parseFloat(from.val());
            let toVal = parseFloat(to.val());
            if (fromVal && toVal && !Number.isNaN(fromVal) && !Number.isNaN(toVal)) {
                let rateValue = to.data('currency') == 'RUB' ? toVal / fromVal : fromVal / toVal;
                rateVal = +rateValue.toFixed(4) || '';
                rate.val(+rateValue.toFixed(4) || '');
            } else {
                rate.val('');
            }
        } else {
            rate.val('');
        }
    };
    var internalTransferFormAmountByRate = function (form) {
        let from = $('#internaltransferform-amount_from', form);
        let to = $('#internaltransferform-amount_to', form);
        let rate = $('#internaltransferform-rate_value', form);
        let rateInputVal = parseFloat(rate.val().replace(/,/g, '.'));
        let rateVal = Number.isNaN(rateInputVal) ? '' : +rateInputVal.toFixed(4);
        if (rateVal == 0 || rateVal == '') {
            to.val('');
            return;
        }
        if (rateVal != rateInputVal) {
            rate.val(rateVal);
        }
        let fromVal = parseFloat(from.val());
        if (Number.isNaN(fromVal)) {
            to.val('');
            return;
        }
        let toVal = to.data('currency') == 'RUB' ? rateVal * fromVal : fromVal / rateVal;
        to.val(toVal.toFixed(2));
    };
    $(document).on('change', '#internaltransferform-account_from', function () {
        let form = this.form;
        let selected = $(':selected', this);
        $('#internaltransferform-amount_from', form).data('currency', selected.data('currency'));
        $('.amount_from_suffix', form).html(selected.data('label-suffix'));
        internalTransferFormCheckAmount(form);
        internalTransferFormAmountByRate(form);
    });
    $(document).on('change', '#internaltransferform-account_to', function () {
        let form = this.form;
        let selected = $(':selected', this);
        $('#internaltransferform-amount_to', form).data('currency', selected.data('currency'));
        $('.amount_to_suffix', form).html(selected.data('label-suffix'));
        internalTransferFormCheckAmount(form);
        internalTransferFormRateByAmount(form);
    });
    $(document).on('change input', '#internaltransferform-amount_from', function () {
        internalTransferFormCheckAmount(this.form);
        if (internalTransferFormNeedRate(this.form)) {
            internalTransferFormAmountByRate(this.form);
        }
    });
    $(document).on('change input', '#internaltransferform-amount_to', function () {
        internalTransferFormCheckAmount(this.form);
        if (internalTransferFormNeedRate(this.form)) {
            internalTransferFormRateByAmount(this.form);
        }
    });
    $(document).on('change input', '#internaltransferform-rate_value', function () {
        internalTransferFormAmountByRate(this.form);
    });
    $(document).on('change', '#internaltransferform-date_from', function () {
        let form = this.form;
        $('#internaltransferform-date_to', form).val(this.value);
    });
    $(document).on('change', '#cashbox-is_main', function(e) {
        let form = this.form;
        if (this.checked) {
            $('#cashbox-is_closed', form).prop('checked', false).prop('disabled', true).uniform('refresh');
        } else {
            $('#cashbox-is_closed', form).prop('disabled', false).uniform('refresh');
        }
    });
    $(document).on('change', '#cashbox-is_closed', function(e) {
        let form = this.form;
        if (this.checked) {
            $('#cashbox-is_main', form).prop('checked', false).prop('disabled', true).uniform('refresh');
        } else {
            $('#cashbox-is_main', form).prop('disabled', false).uniform('refresh');
        }
    });
    $(document).on('change', '.flow_form_rs_select', function(e) {
        let form = this.form;
        let item = $(':selected', this);
        let foreign = $(this).data('foreign');
        if (item.data('foreign') != foreign) {
            $('.flow_form_rs_toggle_link', form).attr("href", item.data('url')).trigger('click');
        }
        $('.currency_name_tag', form).text(item.data('currency'));
    });
}( window.jQuery );
