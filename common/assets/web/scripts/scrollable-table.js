
(function( $ ) {
    var selector1 = ".th.fixed-column input";
    selector1 += ", .th.fixed-column select";
    selector1 += ", .th.fixed-column textarea";
    selector1 += ", .td.fixed-column input";
    selector1 += ", .td.fixed-column select";
    selector1 += ", .td.fixed-column textarea";
    var selector2 = ".th:not(.fixed-column) input";
    selector2 += ", .th:not(.fixed-column) select";
    selector2 += ", .th:not(.fixed-column) textarea";
    selector2 += ", .td:not(.fixed-column) input";
    selector2 += ", .td:not(.fixed-column) select";
    selector2 += ", .td:not(.fixed-column) textarea";
    var wrap = "<div class='scrollable-table-container'>";
    wrap += "\n<div class='scrollable-table-wrap'>";
    wrap += "\n<div class='scrollable-table-content'>";
    wrap += "\n</div>\n</div>\n</div>";
    var scrollingTable = function() {
        $(".scrollable-table:not(.scrolling-done)").each(function (i, table) {
            let $table = $(table);
            $table.wrap(wrap);
            if ($('.fixed-column', $table).length > 0) {
                let wrapper = $table.closest('.scrollable-table-wrap');
                let container = $table.closest('.scrollable-table-container');
                let $clone = $table.clone(true);
                $(selector1, $table).prop("disabled", true);
                $(selector2, $clone).prop("disabled", true);
                $clone.appendTo(wrapper).addClass('scrollable-table-clone').removeClass('scrollable-table');
                if ($table.hasClass("double-scrollbar-top")) {
                    let width = $clone.outerWidth();
                    let $bar = $("<div class='scrollable-table-bar'><div style='width:"+width+"px;height:1px;'></div></div>");
                    $bar.prependTo(container);
                    $(function(){
                        $(".scrollable-table-content", container).scroll(function(){
                            $(".scrollable-table-bar", container).scrollLeft($(".scrollable-table-content", container).scrollLeft());
                        });
                        $(".scrollable-table-bar", container).scroll(function(){
                            $(".scrollable-table-content", container).scrollLeft($(".scrollable-table-bar", container).scrollLeft());
                        });
                    });
                }
            }
            $table.addClass('scrolling-done');
        });
    };

    $(document).ready(function() {
        scrollingTable();
    });

    $(document).ajaxComplete(function(event, request, settings) {
        scrollingTable();
    });
})(jQuery);
