if (window.navigator.userAgent.indexOf("MSIE ") != -1) {
    $('button[form]').on('click', function (e) {
        var $button = $(this);
        var $form = $('#' + $button.attr('form'));

        $form.trigger('submit');
    });

    $('label').on('click', function () {
        $('#' + this.for).click();
    });
}