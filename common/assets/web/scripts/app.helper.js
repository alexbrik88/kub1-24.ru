/**
 * HELPER
 *
 * @namespace APP
 * @example APP.helper;
 */
;
(function (APP, window, document, undefined) {


    /**
     * Прикрепляю к проекту
     *
     * @method helper
     * @namespace APP.helper
     * @return {Object} открытые методы
     */
    APP.helper = (function () {


        /**
         * Приватные свойства
         * @private
         * @type {Variable}
         */
        var _inputToMoneyFormatSelector = '.js_input_to_money_format'
            ;


        /**
         * Иницилиализация модуля
         *
         * @function init
         */
        function init() {
            setBind()
        }

        /**
         * Прослушивание событий
         *
         * @function setBind
         * @private
         */
        function setBind() {

            // Takes money from input and modifies it in right float format.
            $(document).on('paste change', _inputToMoneyFormatSelector, function (e) { // delete all non-number symbols
                e.preventDefault();
                var value = e.type === 'paste'
                    ? (e.originalEvent || e).clipboardData.getData('text/plain')
                    : this.value;
                this.value = _inputToMoneyFormat(value);
            });

        }

        var _inputToMoneyFormat = function (input) {
            return input.replace(',', '.').replace(/[^0-9\.]/g, '');
        };

        /**
         * @method get получить ответ
         * @public
         */
        return {
            init: init,
            inputToMoneyFormat: _inputToMoneyFormat,
        };

    })();


    $(function () {
        APP.helper.init();
    });


})(window.APP = window.APP || {}, window, document);
