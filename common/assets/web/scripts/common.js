/**
 * Created by Наталья on 18.11.2014.
 */

$.pjax.defaults.timeout = 20000;

var kubDatepickerConfig = {
    keyboardNavigation: false,
    forceParse: false,
    language: 'ru',
    autoclose: true
};

/**
 * Getting number precision
 * @return integer
 */
Number.prototype.precision = function () {
    if(Math.floor(this.valueOf()) === this.valueOf()) return 0;
    return this.toString().split(".")[1].length || 0;
};

$.fn.serializeFormAsObject = function() {
  var data = {};

  function buildInputObject(arr, val) {
    if (arr.length < 1)
      return val;
    var objkey = arr[0];
    if (objkey.slice(-1) == "]") {
      objkey = objkey.slice(0,-1);
    }
    var result = {};
    if (arr.length == 1){
      result[objkey] = val;
    } else {
      arr.shift();
      var nestedVal = buildInputObject(arr,val);
      result[objkey] = nestedVal;
    }
    return result;
  }

  $.each(this.serializeArray(), function() {
    var val = this.value;
    var c = this.name.split("[");
    var a = buildInputObject(c, val);
    $.extend(true, data, a);
  });

  return data;
};

$(document).ready(function () {

    $('#side-menu').metisMenu();

    $(function () {
        // $('.knob').knob({});
        $('.knob').parent('div').css('position', 'absolute');
    });

    var Search = {
        //main function to initiate the module
        init: function () {
            if (jQuery().datepicker && $.fn.datepicker.dates) {
                $.fn.datepicker.dates['ru'] = {
                    days: ["Воскресенье", "Понедельник", "Вторник", "Среда", "Четверг", "Пятница", "Суббота"],
                    daysShort: ["Вск", "Пнд", "Втр", "Срд", "Чтв", "Птн", "Суб"],
                    daysMin: ["Вс", "Пн", "Вт", "Ср", "Чт", "Пт", "Сб"],
                    months: ["Январь", "Февраль", "Март", "Апрель", "Май", "Июнь", "Июль", "Август", "Сентябрь", "Октябрь", "Ноябрь", "Декабрь"],
                    monthsShort: ["Янв", "Фев", "Мар", "Апр", "Май", "Июн", "Июл", "Авг", "Сен", "Окт", "Ноя", "Дек"],
                    today: "Сегодня",
                    clear: "Очистить",
                    format: "dd.mm.yyyy",
                    weekStart: 1
                };

                $('.date-picker').datepicker(kubDatepickerConfig).on("change.dp", Search.dateChanged);
            }
            Metronic.initFancybox();
        },

        dateChanged: function (ev) {
            var $input = $(ev.currentTarget);
            if (ev.bubbles == undefined) {
                if (ev.currentTarget.value == "") {
                    if ($input.data("last-value") == null) {
                        $input.data("last-value", ev.currentTarget.defaultValue);
                    }
                    var $lastDate = $input.data("last-value");
                    $input.datepicker("setDate", $lastDate);
                } else {
                    $input.data("last-value", ev.currentTarget.value);
                }
            }
            if ($input.hasClass('input-sm')) {
                $input.toggleClass('edited', $input.val() != "");
            }
        }
    };

    Search.init();

    $.fn.fixedButtons = function (elementObj) {
        $(this).each(function () {
            var container = $(this);
            var contentIn = container.find('.page-content-in');
            var element = $(elementObj);

            function minHeightCheck() {
                var minHeight = $(window).height() - $('.page-header').outerHeight() - $('.page-footer').outerHeight();
                minHeight = minHeight - element.outerHeight() - 10;

                if ($('.table-price-list').length) {
                    var tableHeight = $('.table-price-list').height();
                    var headerHeight = $('.header-price-list').height();
                    minHeight = Math.round(100 + headerHeight + tableHeight);
                }

                contentIn.css('min-height', minHeight);
            }

            setTimeout(minHeightCheck, 300);

            $(window).resize(function () {
                minHeightCheck();
            });
        });
    };
    $('.page-content').fixedButtons('.action-buttons');

    $.fn.init_checkAll = function () {
        var page = $(this);
        var $checkAll = page.find('#check-all');

        $checkAll.click(function () {
            var $checkAllSlide = $('.checkall-slide');
            var $portletBodyCheckAll = $('.portlet-body-checkall');

            if ($checkAll.hasClass('active')) {
                $checkAll.removeClass('active');
                $checkAllSlide
                    .addClass('collapse')
                    .removeClass('expand');
                $portletBodyCheckAll.css({display: 'block'});
                $('html, body').animate({scrollTop: 0}, 600);
                $('.feeds').removeClass('open');
            } else {
                $checkAll.addClass('active');
                $checkAllSlide
                    .removeClass('collapse')
                    .addClass('expand');
                $portletBodyCheckAll.css({display: 'none'});
                $('html, body').delay(100).animate({
                    scrollTop: $('.widget-latest-actions').offset().top
                }, 600, 'swing');
                $(this).html($(this).html() == 'Посмотреть все' ? 'Посмотреть последние' : 'Посмотреть все');
                $('.feeds').addClass('open');
            }
        });

    };
    $('.home-page').init_checkAll();

    $('.js_filetype , .js_select').each(function () {
        var buttonText = $(this).data('button-text') !== undefined ? $(this).data('button-text') : 'Добавить документ';
        $(this).styler({
            'fileBrowse': buttonText
        });
    });

    var UIExtendedModals = function () {
        return {
            //main function to initiate the module
            init: function () {

                //dynamic demo:
                $('.dynamic .demo').click(function () {
                    var tmpl = [
                        // tabindex is required for focus
                        '<div class="modal hide fade" tabindex="-1">',
                        '<div class="modal-header">',
                        '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">Ã—</button>',
                        '<h4 class="modal-title">Modal header</h4>',
                        '</div>',
                        '<div class="modal-body">',
                        '<p>Test</p>',
                        '</div>',
                        '<div class="modal-footer">',
                        '<a href="#" data-dismiss="modal" class="btn btn-default">Close</a>',
                        '<a href="#" class="btn btn-primary">Save changes</a>',
                        '</div>',
                        '</div>'
                    ].join('');

                    $(tmpl).modal();
                });

                //ajax demo:
                var $modal = $('#ajax-modal');

                $('#ajax-demo').on('click', function () {
                    // create the backdrop and wait for next modal to be triggered
                    $('body').modalmanager('loading');

                    setTimeout(function () {
                        $modal.load('ui_extended_modals_ajax_sample.html', '', function () {
                            $modal.modal();
                        });
                    }, 1000);
                });

                $modal.on('click', '.update', function () {
                    $modal.modal('loading');
                    setTimeout(function () {
                        $modal
                            .modal('loading')
                            .find('.modal-body')
                            .prepend('<div class="alert alert-info fade in">' +
                            'Updated!<button type="button" class="close" data-dismiss="alert">&times;</button>' +
                            '</div>');
                    }, 1000);
                });
            }

        };


    }();
    UIExtendedModals.init();
    // Demo.init();
    /*

     var $edit = $('.edit');
     var $editIn = $('.edit-in');

     var $btnCancel = $('.btn-cancel');
     var $btnSave = $('.btn-save');

     var $editableField = $('.editable-field');
     var $editableFieldInput = $('.input-editable-field');

     var $infoButton = $('.info-button');

     var $uploadedFile = $('.remove-table');

     var $controlPanelEdit = $('.control-panel-edit');
     var $controlPanelPreEdit = $('.control-panel-pre-edit');


     $edit.on('click', function () {
     $btnSave.removeClass('hide');
     $btnCancel.removeClass('hide');

     $edit.addClass('hide');
     $('.view-action-buttons').addClass('hide');
     $('.form-action-buttons').removeClass('hide');
     $('.editable-field').addClass('hide');
     $('.input-editable-field').removeClass('hide');

     $controlPanelPreEdit.addClass('hide');

     $infoButton.addClass('hide');
     });
     */

    $(document).on('click', '.edit', function () {
        $('.btn-save').removeClass('hide');
        $('.btn-cancel').removeClass('hide');

        $('.edit').addClass('hide');
        $('.view-action-buttons').addClass('hide');
        $('.form-action-buttons').removeClass('hide');
        $('.editable-field').addClass('hide');
        $('.input-editable-field').removeClass('hide');

        $('.control-panel-pre-edit').addClass('hide');

        $('.info-button').addClass('hide');
    });


    $(document).on('click', '.edit-in', function () {
        $('.q-span').addClass('hide');
        $('.q-input').removeClass('hide');
        $('.edit-in').addClass('hide');

        $('.control-panel-edit').removeClass('hide');
        $('.control-panel-edit').removeClass('hide');

        $('.customer_info_wrapper')
            .addClass('col-md-12')
            .removeClass('col-md-7');
        $('.remove-table').addClass('hide');
    });

    $(document).on('click', '.btn-cancel', function () {
        console.log($('.input-editable-field').length);
        $('.btn-save').addClass('hide');
        $('.btn-cancel').addClass('hide');
        $('.q-span').removeClass('hide');
        $('.q-input').addClass('hide');
        $('.edit').removeClass('hide');
        $('.view-action-buttons').removeClass('hide');
        $('.form-action-buttons').addClass('hide');
        $('.editable-field').removeClass('hide');
        $('.input-editable-field').addClass('hide');

        $('.info-button').removeClass('hide');

        $('.control-panel-edit').addClass('hide');
        $('.control-panel-pre-edit').removeClass('hide');


        $('.customer_info_wrapper')
            .addClass('col-md-7')
            .removeClass('col-md-12');

        $('.edit-in').removeClass('hide');
        $('.customer-info-cash').removeClass('hide');
        $('#cash-order').addClass('hide');
        $('#edit-cash-order').addClass('hide');

        $('.remove-table').removeClass('hide');
    });

    /*    $btnCancel.on('click', function () {
     $btnSave.addClass('hide');
     $btnCancel.addClass('hide');
     $('.q-span').removeClass('hide');
     $('.q-input').addClass('hide');
     $edit.removeClass('hide');
     $('.view-action-buttons').removeClass('hide');
     $('.form-action-buttons').addClass('hide');
     $editableField.removeClass('hide');
     $editableFieldInput.addClass('hide');

     $infoButton.removeClass('hide');

     $controlPanelEdit.addClass('hide');
     $controlPanelPreEdit.removeClass('hide');


     $('.customer_info_wrapper')
     .addClass('col-md-7')
     .removeClass('col-md-12');

     $editIn.removeClass('hide');
     $('.customer-info-cash').removeClass('hide');
     $('#cash-order').addClass('hide');
     $('#edit-cash-order').addClass('hide');

     $uploadedFile.removeClass('hide');
     });*/
    /*
     $editIn.on('click', function () {
     $('.q-span').addClass('hide');
     $('.q-input').removeClass('hide');
     $editIn.addClass('hide');

     $controlPanelEdit.removeClass('hide');

     $('.customer_info_wrapper')
     .addClass('col-md-12')
     .removeClass('col-md-7');

     $uploadedFile.addClass('hide');
     });*/

    // cash
    $('.edit-cash-order').on('click', function () {
        $('.customer-info-cash').addClass('hide');
        $('#cash-order').removeClass('hide');
    });

    // employee
    $('#edit-employee').click(function () {
        $('.customer-info-cash').addClass('hide');
        $('.edit-employee-form').removeClass('hide');
    });

    Layout.init();

    $('#add-invoice-to-payment-order').parent().removeClass('col-sm-4');

    //Change bootstrap tooltip
    $('.tools').find('a').tooltip({
        title: 'Закрыть/Открыть'
    });


    // $(window).load вместо $('.logo_cont').find('img), т.к.
    // 'в некоторых случаях, если картинка содержится в кеше браузера, событие load может не произойти'
    $(window).on('load', function () {
        var locoCont = $('.logo_cont');
        centering(locoCont);
        $(document).resize(function () {
            centering(locoCont);
        });
    });
});

$(document).ready(function () {
    $('table.fix-thead').each(function () {
        var table = $(this);
        if (table.children('thead').length && table.children('tbody tr').length > 2 && !table.hasClass('upload-documents-order-table')) {
            table.DataTable({
                ordering: false,
                orderMulti: false,
                orderClasses: false,
                searching: false,
                paging: false,
                filter: false,
                info: false,
                dom: "t",
                classes: {sNoFooter: ''},
                fixedHeader: {
                    headerOffset: $('.page-header.navbar.navbar-fixed-top').outerHeight()
                }
            });
        }
    });
});


// Centering images
function centering(imgCont) {
    imgCont.find('img').css({
        'left': -(imgCont.find('img').width() - imgCont.width()) / 2 + 'px',
        'top': -(imgCont.find('img').height() - imgCont.height()) / 2 + 'px'
    });
}


/* FILE UPLOADING */
$(function () {
    $(document).on('change', '.fileUpload', function () {
        var $this = $(this);
        var $title = $this.find('.title');
        var filename = $this.find('input[type=file]').val().replace(/.*(\/|\\)/, '');

        $title.text(filename ? filename : $title.data('default-title'));
    });

    $(document).on('click', '.fileUploadRefresh', function (e) {
        e.preventDefault();
        var $fileUpload = $(this).siblings('.fileUpload');

        $fileUpload.find('input').val('');
        $fileUpload.trigger('change');
    });
});


$(function () {
    // Forms
    $(document).on('change', '.form-autosubmit', function () {
        $(this.form).trigger('submit');
    });

    $('[data-toggle=popover]').popover();


    // Modal
    $(".auto-send-modal").on("show", function (e) {
        var $this = $(this);
        $this.find(".modal-body").html("").load($this.data("href"));
    });
});

$(document).on("pjax:complete", function (e) {
    $('input[type="radio"]:not(.md-radiobtn), input[type="checkbox"]:not(.active-store-account, .md-check, .switch)', e.target).uniform();
});

$(document).on('pjax:timeout, pjax:error', function(event) {
    // Prevent default redirection behavior
    event.preventDefault();
    console.log(event.type);
});

/*
 * возвращает слово из списка в зависимости от количества
 * пример:
 * declOfNum(3, ['компанию', 'компании', 'компаний'])
 *
 * @param integer number
 * @param array titles
 */
function declOfNum(number, titles) {
    cases = [2, 0, 1, 1, 1, 2];
    return titles[(number % 100 > 4 && number % 100 < 20) ? 2 : cases[(number % 10 < 5) ? number % 10 : 5]];
}

/*
 * аналог функции PHP - empty()
 *
 * @param mixed mixed_var
 */
function empty(mixed_var) {
    return (
        mixed_var === '' ||
        mixed_var === 0 ||
        mixed_var === '0' ||
        mixed_var === null ||
        mixed_var === false ||
        mixed_var === 'false' ||
        mixed_var === undefined
    );
}

$(document).on('change', 'select[data-tax-kbk]', function () {
    var kbk = $(this).data('tax-kbk')[this.value];
    if (kbk) {
        $('#cashbankflowsform-kbk, #cashbankflows-kbk', this.form).val(kbk);
    }
});

$(document).on("click", "[data-toggle=hidden]", function (e) {
    $($(this).data("target")).toggleClass("hidden");
});