<?php

namespace common\assets;

use yii\web\AssetBundle;

/**
 * Class RaphaelAsset
 * @package common\assets
 */
class RaphaelAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@common/assets/web/plugins/raphael/2.2.0';

    /**
     * @var array
     */
    public $js = [
        'raphael.js',
        'raphael.min.js',
    ];

    /**
     * Registers the CSS and JS files with the given view.
     * @param \yii\web\View $view the view that the asset files are to be registered with.
     */
    public function registerAssetFiles($view)
    {
        $this->js[] = 'raphael' . (!YII_DEBUG ? '.min' : '') . '.js';
        parent::registerAssetFiles($view);
    }
}
