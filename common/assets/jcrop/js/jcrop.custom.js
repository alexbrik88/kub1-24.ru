function pjaxImgForm(pjaxUrl, companyId, attr, action, apply) {
    apply = apply || 0;
    jQuery.pjax({
        url: pjaxUrl,
        data: {'id': companyId, 'attr': attr, 'action': action, 'apply': apply},
        container: '#' + attr + '-pjax-container',
        timeout: 5000,
        push: false,
        scrollTo: false
    });

    if (action == 'view') {
        $(document).on('pjax:complete', function () {
            var edit = $('#' + attr + '-image-edit');
            $('#' + attr + '-image-result').html(edit.html()).removeClass('hidden');
            if (attr == 'chiefAccountantSignatureImage') {
                if (edit.find('.company_img_dummy').lenght) {
                    $('#chiefAccountantSignatureImage-image').addClass('hidden');
                } else {
                    $('#chiefAccountantSignatureImage-image').removeClass('hidden');
                }
            }
        });
    }
}

function jcropImageDelete(pjaxUrl, formUrl, companyId, attr) {
    jQuery.post(pjaxUrl, {'id': companyId, 'attr': attr}, function (data) {
        pjaxImgForm(formUrl, companyId, attr, 'view');
    });
}

function jcrop_ajaxRequest(pjaxUrl, formUrl, companyId, attr, el) {
    var hasApply = el !== undefined;
    var tab = hasApply ? $(el).closest('.jcrop-image-container') : null;
    var apply = tab !== null ? $('input.apply-image-radio:checked', tab).val() : '0';
    var ajaxData = {};
    ajaxData[attr + '_x'] = $('#' + attr + '_x').val();
    ajaxData[attr + '_x2'] = $('#' + attr + '_x2').val();
    ajaxData[attr + '_y'] = $('#' + attr + '_y').val();
    ajaxData[attr + '_y2'] = $('#' + attr + '_y2').val();
    ajaxData[attr + '_h'] = $('#' + attr + '_h').val();
    ajaxData[attr + '_w'] = $('#' + attr + '_w').val();
    ajaxData['id'] = companyId;
    ajaxData['attr'] = attr;
    ajaxData['width'] = $('#' + attr).width();
    ajaxData['apply'] = apply;
    if ($('#company-tmpid').lenght) {
        ajaxData['tmpId'] = $('#company-tmpid').val();
    }
    jQuery.ajax({
        type: 'post',
        url: pjaxUrl,
        data: ajaxData,
        success: function (data) {
            pjaxImgForm(formUrl, companyId, attr, 'view', apply);
        }
    });
}

function jcrop_updatePreview(c, api) {
    if (parseInt(c.w) > 0 && parseInt(c.h) > 0) {
        var id = '#' + api.attr;
        var imgWidth = api.w;
        var imgHeight = api.h;
        var $pCont = $(id + '-preview-container');
        var $pInner = $(id + '-preview-inner');
        var $pImg = $(id + '-preview-inner img');
        var pWidth = $pCont.innerWidth();
        var pHeight = $pCont.innerHeight();
        var pRatio = pWidth / pHeight;
        var cRatio = c.w / c.h;
        var k = 1;
        var wDif = 0;
        var hDif = 0;
        if (pRatio >= cRatio) {
            k = pHeight / c.h;
            wDif = Math.round(pWidth - c.w * k) / 2;
        } else {
            k = pWidth / c.w;
            hDif = Math.round(pHeight - c.h * k) / 2;
        }
        $pInner.css({
            left: wDif + 'px',
            right: wDif + 'px',
            top: hDif + 'px',
            bottom: hDif + 'px',
        });
        $pImg.css({
            width: Math.round(k * imgWidth) + 'px',
            height: Math.round(k * imgHeight) + 'px',
            marginLeft: '-' + Math.round(k * c.x) + 'px',
            marginTop: '-' + Math.round(k * c.y) + 'px'
        });
    }
}

function jcrop_setCoords(c, api) {
    var id = '#' + api.attr;
    $(id + '_x').val(c.x);
    $(id + '_y').val(c.y);
    $(id + '_x2').val(c.x2);
    $(id + '_y2').val(c.y2);
    $(id + '_w').val(c.w);
    $(id + '_h').val(c.h);
}

function jcrop_processCoords(c) {
    jcrop_updatePreview(c, this);
    jcrop_setCoords(c, this);
}

function jcrop_initByAttr(attr) {
    var jcrop = {};
    var id = '#' + attr;
    var img = document.getElementById(attr),
        new_img = new Image();
    new_img.onload = function () {
        w = this.width;
        h = this.height;

        if (!jcrop.id) {
            jcrop.id = $(id).Jcrop({
                trueSize: [w, h],
                setSelection: [10, 10, 200, 200],
                onChange: jcrop_processCoords,
                onSelect: jcrop_processCoords,
                bgColor: '',
            }, function () {
                this.attr = attr;
                this.w = w;
                this.h = h;
                var dim = this.getBounds();
                this.animateTo([w * 0.2, h * 0.2, w * 0.8, h * 0.8]);
            });
        }
    }
    new_img.src = img.src;
}
