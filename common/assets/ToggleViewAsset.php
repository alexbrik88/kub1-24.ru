<?php

namespace common\assets;

use yii\web\AssetBundle;

/**
 * Class ToggleViewAsset
 * @package common\assets
 */
class ToggleViewAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@common/assets/web/scripts';

    /**
     * @var array
     */
    public $js = [
        'toggle-view.js',
    ];

    /**
     * @var array
     */
    public $depends = [
        'yii\web\JqueryAsset',
    ];
}
