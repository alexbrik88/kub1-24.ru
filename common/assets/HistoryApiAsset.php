<?php

namespace common\assets;

use yii\web\AssetBundle;

/**
 * Class HistoryApiAsset
 * @package common\assets
 */
class HistoryApiAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@bower/html5-history-api';

    /**
     * @var array
     */
    public $js = [
        'history.min.js',
    ];
}
