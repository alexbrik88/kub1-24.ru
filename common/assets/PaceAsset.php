<?php

namespace common\assets;

use yii\web\AssetBundle;

/**
 * Class PaceAsset
 * @package common\assets
 */
class PaceAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@bower/pace';

    /**
     * @var array
     */
    public $css = [
        'themes/blue/pace-theme-minimal.css',
    ];

    /**
     * @var array
     */
    public $js = [
        'pace.min.js',
    ];

    /**
     * @var array
     */
    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD,
    ];
}
