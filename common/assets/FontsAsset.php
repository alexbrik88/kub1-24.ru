<?php

namespace common\assets;


use yii\web\AssetBundle;

class FontsAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $sourcePath = '@common/assets/web/fonts';

    /**
     * @var array
     */
    public $css = [
        'fonts.css',
    ];
}
