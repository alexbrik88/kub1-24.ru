<?php

namespace common\tasks;

use common\components\date\DateHelper;
use common\components\sender\unisender\unisenderApi;
use common\models\employee\Employee;
use common\models\unisender\UnisenderMessage;
use common\modules\import\commands\ImportCommandInterface;
use common\modules\import\commands\ImportCommandTrait;
use common\modules\import\components\ImportParamsHelper;
use common\modules\import\models\ImportJobData;
use DateTime;
use Exception;
use Yii;
use yii\db\Transaction;

final class ExportUnisender implements ImportCommandInterface
{
    use ImportCommandTrait;

    /**
     * @var ImportJobData
     */
    private $jobData;

    /** @var unisenderApi */
    private $unisenderApi;

    /**
     * @param ImportJobData $jobData
     */
    public function __construct(ImportJobData $jobData)
    {
        $this->jobData = $jobData;
    }

    /**
     * @inheritDoc
     */
    public static function createImportCommand(ImportJobData $jobData): ImportCommandInterface
    {
        return new static($jobData);
    }

    /**
     * @inheritDoc
     */
    public static function getImportCommandName(): string
    {
        return 'Выгрузка писем из Unisender';
    }

    /**
     * @return string
     */
    public static function getImportCommandLabel(): string
    {
        return 'Unisender';
    }

    /**
     * @inheritDoc
     */
    public function run(): void
    {
        $transaction = Yii::$app->db->beginTransaction(Transaction::READ_COMMITTED);
        $result = $this->getCampaigns();
        if (!$result) {
            $transaction->rollBack();
        } else {
            $transaction->commit();
        }

        $job = $this->jobData;
        $job->count = $this->savedCount;

        if ($job->finished_at === null) {
            $job->finished_at = time();
            if ($result) {
                if ($this->savedCount === 0) {
                    $job->result = 'За выбранный период не найдено писем.';
                } else {
                    $job->result = "Выгрузка успешно завершена. Количество: {$this->savedCount}.";
                }
            } else {
                $job->result = 'Произошла ошибка при выгрузке.';
            }
        }

        if (!$job->save()) {
            $this->log([
                'Job updating failed.',
                "Job ID: {$job->id}",
                'Errors:',
                $this->formatErrors($job->getErrors()),
            ]);
        }
    }

    private function getCampaigns($offset = 0)
    {
        $job = $this->jobData;
        $helper = new ImportParamsHelper($this->jobData);
        $response = json_decode($this->getUnisenderApi()->getCampaigns([
            'from' => $helper->getDateFrom()->format(DateHelper::FORMAT_DATETIME),
            'to' => $helper->getDateTo()->format(DateHelper::FORMAT_DATETIME),
            'limit' => 100,
            'offset' => $offset,
        ]));
        if (isset($response->error)) {
            if ($response->code === 'invalid_api_key') {
                $job->finished_at = time();
                $job->result = 'Произошла ошибка при экспорте писем. Неверный ключ доступа к API.';
                if (!$job->save()) {
                    $this->log([
                        'Job updating failed.',
                        "Job ID: {$job->id}",
                        'Errors:',
                        $this->formatErrors($job->getErrors()),
                    ]);
                }
            }

            $this->log("Unisender api error. Error: {$response->error}. Code {$response->code}.");

            return false;
        }

        return $this->parseResponse($response, $offset);
    }

    private function parseResponse($response, $offset)
    {
        if (!isset($response->result)) {
            $this->log('Empty response.');
            return false;
        }

        $messagesCount = count($response->result);
        if ($messagesCount === 0) {
            return true;
        }

        foreach ($response->result as $message) {
            $existsMessage = $this->getMessage($message->id);
            if ($existsMessage !== null) {
                if ($message->status !== $existsMessage->status) {
                    $existsMessage->status = $message->status;
                    if (!$existsMessage->save()) {
                        $this->log([
                            'Message status updating failed.',
                            "Message ID: {$existsMessage->id}",
                            'Errors:',
                            $this->formatErrors($existsMessage->getErrors()),
                        ]);

                        return false;
                    }
                }

                continue;
            }

            $stats = $this->getMessageStats($message->id);
            if ($stats === null) {
                $this->log([
                    'Failed getting message stats.',
                    "Message ID: {$existsMessage->message_id}",
                    "Unisender message ID: {$message->id}",
                ]);

                return false;
            }

            $unisenderMessage = new UnisenderMessage();
            $unisenderMessage->company_id = $this->jobData->company_id;
            $unisenderMessage->employee_id = $this->jobData->employee_id;
            $unisenderMessage->unisender_id = $message->id;
            $date = new DateTime($message->start_time);
            $date->modify('+3 hours');
            $unisenderMessage->start_date = $date->getTimestamp();
            $unisenderMessage->status = $message->status;
            $unisenderMessage->message_id = $message->message_id;
            $unisenderMessage->list_id = $message->list_id;
            $unisenderMessage->subject = $message->subject;
            $unisenderMessage->sender_name = $message->sender_name;
            $unisenderMessage->sender_email = $message->sender_email;
            $unisenderMessage->total_contacts_count = $stats->total ?? 0;
            $unisenderMessage->sent_messages_count = $stats->sent ?? 0;
            $unisenderMessage->delivered_messages_count = $stats->delivered ?? 0;
            $unisenderMessage->read_unique_count = $stats->read_unique ?? 0;
            $unisenderMessage->read_count = $stats->read_all ?? 0;
            $unisenderMessage->clicked_unique_count = $stats->clicked_unique ?? 0;
            $unisenderMessage->clicked_count = $stats->clicked_all ?? 0;
            $unisenderMessage->unsubscribed_contacts_count = $stats->unsubscribed ?? 0;
            $unisenderMessage->spam_contacts_count = $stats->spam ?? 0;
            if (!$unisenderMessage->save()) {
                $this->log([
                    'Cannot save message.',
                    "Unisender message ID: {$message->id}",
                    'Errors:',
                    $this->formatErrors($unisenderMessage->getErrors()),
                ]);

                return false;
            }

            $this->savedCount++;
        }

        if ($messagesCount === 100) {
            return $this->getCampaigns($offset + 100);
        }

        return true;
    }

    private function getMessageStats($id)
    {
        $response = json_decode($this->getUnisenderApi()->getCampaignCommonStats(['campaign_id' => $id]));
        if (isset($response->error) || !isset($response->result)) {
            return null;
        }

        return $response->result;
    }

    /**
     * @param $id
     * @return UnisenderMessage|null
     */
    private function getMessage($id)
    {
        return UnisenderMessage::findOne(['unisender_id' => $id]);
    }

    /**
     * @return unisenderApi
     */
    private function getUnisenderApi()
    {
        if ($this->unisenderApi === null) {
            $unisenderParams = $this->jobData->company->integration(Employee::INTEGRATION_UNISENDER);
            $this->unisenderApi = new unisenderApi($unisenderParams['apiKey'] ?? null);
        }

        return $this->unisenderApi;
    }

    private function log($message)
    {
        if (is_array($message)) {
            $message = implode(' ', $message);
        }

        Yii::error(new Exception($message));
    }

    private function formatErrors($errors)
    {
        if (!is_array($errors)) {
            return null;
        }

        $formattedErrors = array_map(function ($key, $val) {
            return "{$key}: {$val[0]}";
        }, array_keys($errors), $errors);

        return implode(' ', $formattedErrors);
    }
}
