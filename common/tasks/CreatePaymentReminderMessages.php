<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 03.01.2020
 * Time: 22:07
 */

namespace common\tasks;

use common\models\Company;
use common\models\paymentReminder\PaymentReminderMessage;
use common\models\paymentReminder\PaymentReminderMessageCategory;
use InvalidArgumentException;
use yii\queue\JobInterface;

class CreatePaymentReminderMessages implements JobInterface
{
    public $companyId;

    public static $messages = [
        [
            'number' => 1,
            'categoryId' => PaymentReminderMessageCategory::CATEGORY_DEBT,
            'eventHeader' => 'Счет не оплачен.',
            'eventBody' => 'До окончания срока оплаты счета осталось "Х" дней.
            <br>
            Будет отправлено письмо Покупателю с напоминание об оплате.',
            'templateSubject' => 'Не забудьте оплатить счет',
            'templateBody' => '[Имя покупателя], добрый день!
            <br>
            <br>
            Согласно нашему Договору [№ и дата договора],
            вы должны оплатить наш счет в течение [Период оплаты] дней от даты счета.
            Убедительная просьба не нарушать данный пункт договора и оплатить сегодня
            счет № [№ счета] от [Дата счета] на сумму [Сумма по счету].
            <br>
            <br>
            Спасибо за понимание.',
        ],
        [
            'number' => 2,
            'categoryId' => PaymentReminderMessageCategory::CATEGORY_DEBT,
            'eventHeader' => 'Счет не оплачен.',
            'eventBody' => 'Оплата счета просрочена на "Х" дней.
            <br>
            Будет отправлено письмо Покупателю с напоминание о просрочке оплаты счета.',
            'templateSubject' => 'Вы не оплатили счет',
            'templateBody' => '[Имя покупателя], добрый день!
            <br>
            <br>
            Мы ценим наши взаимоотношения и дорожим вами как клиентом, но Вы просрочили оплату счета на [Просрочено дней] дней.
            <br>
            Согласно нашему Договору [№ и дата договора],
            вы должны были оплатить счет в течении [Период оплаты] дней от даты счета.
            <br>
            <br>
            Просьба оплатить долг сегодня.
            <br>
            В противном случае будем вынуждены воспользоваться п. 3 и п. 4 Договора и начислить пени за просрочку оплаты,
            а так же приостановить работу до полного погашения долга.',
        ],
        [
            'number' => 3,
            'categoryId' => PaymentReminderMessageCategory::CATEGORY_DEBT,
            'eventHeader' => 'Счет не оплачен.',
            'eventBody' => 'Оплата счета просрочена на "Х" дней.
            <br>
            Будет отправлено письмо Покупателю с напоминанием об оплате и со ссылкой на пункт договора о штрафных санкциях',
            'templateSubject' => 'Будем вынуждены приостановить работу',
            'templateBody' => '[Имя покупателя], добрый день!
            <br>
            <br>
            Вы просрочили оплату счета на [Просрочено дней] дней.
            <br>
            Согласно нашему Договору [№ и дата договора],
            вы должны были оплатить счет в течение [Период оплаты] дней от даты счета.
            <br>
            <br>
            Убедительная просьба оплатить долг сегодня.
            <br>
            В противном случае уведомляем вас о том, что согласно пункту 3.1.1.
            с завтрашнего дня начинаются начисляться пени за просрочку оплаты в размере 0,1% за каждый день просрочки.
            <br>
            <br>
            Так же согласно п. 3.3.3. Договора о ______ мы приостанавливаем работу и не несем ответственность за не
            подготовку отчетности и не сдачу ее в налоговую и фонды.',
        ],
        [
            'number' => 4,
            'categoryId' => PaymentReminderMessageCategory::CATEGORY_DEBT,
            'eventHeader' => 'Счет не оплачен.',
            'eventBody' => 'Оплата счета просрочена на "Х" дней.
            <br>
            Будет отправлено письмо Покупателю с напоминание об оплате и предупреждением о том, что готовим претензию.',
            'templateSubject' => 'Готовим претензию',
            'templateBody' => '[Имя покупателя], добрый день!
            <br>
            <br>
            В виду не оплаты услуг/товара, уведомляем вас, что наши юристы начали подготовку претензии,
            которую отправим вам в ближайшее время. Далее мы обратимся в суд для взыскания задолженности.
            <br>
            <br>
            Информируем вас, что согласно закону ФЗ №____ от ____ в случае не оплаты долга вашей компанией
            по решению суда, мы имеем право взыскать долг с генерального директора и учредителей, а так же с
            аффилированных с ними лиц, включая супругов.
            <br>
            <br>
            В случае не оплаты долга, наши юристы воспользуются данным правом согласно ФЗ №____ от ____.',
        ],
        [
            'number' => 5,
            'categoryId' => PaymentReminderMessageCategory::CATEGORY_DEBT,
            'eventHeader' => 'Счет не оплачен.',
            'eventBody' => 'Оплата счета просрочена на "Х" дней.
            <br>
            Будет отправлено письмо Сотруднику вашей компании с указаниями, что делать по данному Покупателю.
            (Например юристу подготовить претензию)',
            'templateSubject' => 'Приостановка работы по клиенту',
            'templateBody' => '[Имя покупателя], добрый день!
            <br>
            <br>
            В связи с образовавшейся задолженностью прекратить работу по компании [Название компании покупателя].
            <br>
            В связи с образовавшейся задолженностью прекратить отгрузку по компании [Название компании покупателя].
            <br>
            В связи с образовавшейся задолженностью, подготовить претензию для отправки компании [Название компании покупателя].',
        ],
        [
            'number' => 6,
            'categoryId' => PaymentReminderMessageCategory::CATEGORY_DEBT,
            'eventHeader' => 'Сумма просроченного долга больше суммы N.',
            'eventBody' => 'В день превышения суммы долга.
            <br>
            Будет отправлено письмо Покупателю с напоминание об оплате.',
            'templateSubject' => 'Сумма долга превысила допустимый лимит',
            'templateBody' => '[Имя покупателя], добрый день!
            <br>
            <br>
            Мы ценим наши взаимоотношения и дорожим вами как клиентом, но сумма Вашего долга составляет [Сумма по счету].
            <br>
            Согласно нашему Договору [№ и дата договора],
             вы должны были оплатить счета в течение [Просрочено дней] дней от даты счета.
            <br>
            <br>
            Просьба оплатить долг сегодня.
            <br>
            В противном случае будем вынуждены воспользоваться п. 3 и п. 4 Договора и начислить пени за
            просрочку оплаты, а так же приостановить работу до полного погашения долга.',
        ],
        [
            'number' => 7,
            'categoryId' => PaymentReminderMessageCategory::CATEGORY_DEBT,
            'eventHeader' => 'Сумма просроченного долга больше суммы N.',
            'eventBody' => 'Через "Х" дней после превышения долга.
            <br>
            Будет отправлено письмо Покупателю с напоминанием о сумме долга и со ссылкой на пункт договора о штрафных санкциях.',
            'templateSubject' => 'Вынуждены приостановить работу',
            'templateBody' => '[Имя покупателя], добрый день!
            <br>
            <br>
            Сумма Вашего долга составляет [Сумма по счету].
            <br>
            <br>
            Согласно нашему Договору [№ и дата договора],
            вы должны были оплатить счета в течение [Просрочено дней] дней от даты счета.
            <br>
            <br>
            Убедительная просьба оплатить долг сегодня.
            <br>
            В противном случае уведомляем вас о том, что согласно пункту 3.1.1. с завтрашнего дня начинаются
            начисляться пени за просрочку оплаты в размере 0,1% за каждый день просрочки.
            <br>
            <br>
            Так же согласно п. 3.3.3. Договора о ……. мы приостанавливаем работу с вашей компанией.',
        ],
        [
            'number' => 8,
            'categoryId' => PaymentReminderMessageCategory::CATEGORY_DEBT,
            'eventHeader' => 'Сумма просроченного долга больше суммы N.',
            'eventBody' => 'Через "Х" дней после превышения долга.
            <br>
            Будет отправлено письмо Покупателю с напоминание об оплате и предупреждением о том, что готовим претензию.',
            'templateSubject' => 'Готовим претензию',
            'templateBody' => '[Имя покупателя], добрый день!
            <br>
            <br>
            В виду образовавшегося долга Вашей компании перед нами в размере [Сумма по счету],
             за оказанные услуги (работы или отгруженные товары), уведомляем вас, что наши юристы начали подготовку
             претензии, которую отправим вам в ближайшее время. Далее мы обратимся в суд для взыскания задолженности.
            <br>
            <br>
            Информируем вас, что согласно закону ФЗ №____ от ____ в случае не оплаты долга вашей компанией
            по решению суда, мы имеем право взыскать долг с генерального директора и учредителей, а так же с
            аффилированных с ними лиц, включая супругов.
            <br>
            В случае не оплаты долга, наши юристы воспользуются данным правом согласно ФЗ №____ от ____.',
        ],
        [
            'number' => 9,
            'categoryId' => PaymentReminderMessageCategory::CATEGORY_DEBT,
            'eventHeader' => 'Сумма просроченного долга больше суммы N.',
            'eventBody' => 'Будет отправлено письмо Сотруднику вашей компании с указаниями, что
            делать по данному Покупателю. (Например юристу подготовить претензию)',
            'templateSubject' => 'Приостановка работы по клиенту',
            'templateBody' => '[Имя покупателя], добрый день!
            <br>
            <br>
            В связи с образовавшейся задолженностью прекратить работу по компании [Название компании покупателя].
            <br>
            В связи с образовавшейся задолженностью прекратить отгрузку по компании [Название компании покупателя].
            <br>
            В связи с образовавшейся задолженностью, подготовить претензию для отправки компании [Название компании покупателя].',
        ],
        [
            'number' => 10,
            'categoryId' => PaymentReminderMessageCategory::CATEGORY_PAID,
            'eventHeader' => 'Счет оплачен вовремя (до истечения срока «Оплатить до:»).',
            'eventBody' => 'При смене статуса счета на «Оплачен»,
            <br>
            отправляется письмо Покупателю с благодарность об оплате.',
            'templateSubject' => 'Спасибо, ваша оплата получена',
            'templateBody' => '[Имя покупателя], добрый день!
            <br>
            <br>
            Спасибо за своевременную оплату счета № [№ счета] от [Дата счета] на сумму [Сумма по счету].',
        ],
    ];

    public static function create($companyId)
    {
        $company = Company::findOne(['id' => $companyId]);
        if ($company === null) {
            throw new InvalidArgumentException("Company with id {$companyId} not found.");
        }

        $result = new static();
        $result->companyId = $companyId;

        return $result;
    }

    public function execute($queue)
    {
        foreach (self::$messages as $message) {
            $paymentReminderMessage = new PaymentReminderMessage();
            $paymentReminderMessage->number = $message['number'];
            $paymentReminderMessage->category_id = $message['categoryId'];
            $paymentReminderMessage->company_id = $this->companyId;
            $paymentReminderMessage->event_header = $message['eventHeader'];
            $paymentReminderMessage->event_body = $message['eventBody'];
            $paymentReminderMessage->message_template_subject = $message['templateSubject'];
            $paymentReminderMessage->message_template_body = $message['templateBody'];
            $paymentReminderMessage->status = PaymentReminderMessage::STATUS_INACTIVE;
            $paymentReminderMessage->save();
        }
    }
}