<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 07.01.2020
 * Time: 4:18
 */

namespace common\tasks;

use common\models\Contractor;
use common\models\paymentReminder\PaymentReminderMessageContractor;
use InvalidArgumentException;
use Webmozart\Assert\Assert;
use yii\queue\JobInterface;

class CreatePaymentReminderMessagesContractor implements JobInterface
{
    public $contractorId;

    public static function create($contractorId)
    {
        $contractor = Contractor::findOne(['id' => $contractorId]);
        if ($contractor === null) {
            throw new InvalidArgumentException("Contractor with id {$contractorId} not found.");
        }

        $result = new static();
        $result->contractorId = $contractorId;

        return $result;
    }
    
    public function execute($queue)
    {
        $contractor = Contractor::findOne($this->contractorId);
        $paymentReminderMessageContractor = new PaymentReminderMessageContractor();
        $paymentReminderMessageContractor->company_id = $contractor->company_id;
        $paymentReminderMessageContractor->contractor_id = $contractor->id;
        foreach (range(1, 10) as $number) {
            $attribute = "message_{$number}";
            $paymentReminderMessageContractor->$attribute = 0;
            if (PaymentReminderMessageContractor::find()
                    ->andWhere(['company_id' => $contractor->company_id])
                    ->exists() &&
                !PaymentReminderMessageContractor::find()
                    ->andWhere(['company_id' => $contractor->company_id])
                    ->andWhere([$attribute => false])
                    ->exists()
            ) {
                $paymentReminderMessageContractor->$attribute = 1;
            }
        }

        $paymentReminderMessageContractor->save();
    }
}