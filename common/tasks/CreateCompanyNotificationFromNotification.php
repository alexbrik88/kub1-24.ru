<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 24.12.2019
 * Time: 1:35
 */

namespace common\tasks;

use common\models\Company;
use common\models\company\CompanyNotification;
use common\models\notification\Notification;
use InvalidArgumentException;
use yii\queue\JobInterface;

class CreateCompanyNotificationFromNotification implements JobInterface
{
    /**
     * @var Notification
     */
    public $notification;

    public static function create($notificationId) {
        $notification = Notification::findOne(['id' => $notificationId]);
        if ($notification === null) {
            throw new InvalidArgumentException("Notification with id {$notificationId} not found.");
        }

        $result = new static();
        $result->notification = $notification;

        return $result;
    }

    public function execute($queue)
    {
        foreach (Company::find()->isBlocked(Company::UNBLOCKED)->batch() as $batchCompany) {
            /** @var Company $company */
            foreach ($batchCompany as $company) {
                if (!CompanyNotification::find()
                    ->andWhere(['company_id' => $company->id])
                    ->andWhere(['notification_id' => $this->notification->id])
                    ->exists()) {
                    $companyNotification = new CompanyNotification();
                    $companyNotification->company_id = $company->id;
                    $companyNotification->notification_id = $this->notification->id;
                    $companyNotification->status = Notification::STATUS_NOT_PASSED;
                    $companyNotification->save();
                }
            }
        }
    }
}