<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 26.12.2019
 * Time: 21:42
 */

namespace common\tasks;

use common\models\Company;
use common\models\company\CompanyNotification;
use common\models\notification\Notification;
use InvalidArgumentException;
use yii\queue\JobInterface;

class CreateCompanyNotificationsFromCompany implements JobInterface
{
    public $companyId;

    public static function create($companyId)
    {
        $company = Company::findOne(['id' => $companyId]);
        if ($company === null) {
            throw new InvalidArgumentException("Company with id {$companyId} not found.");
        }

        $result = new static();
        $result->companyId = $companyId;

        return $result;
    }

    public function execute($queue)
    {
        $company = Company::findOne($this->companyId);
        foreach (Notification::find()->batch() as $notificationBatch) {
            /** @var Notification $notification */
            foreach ($notificationBatch as $notification) {
                if (!CompanyNotification::find()
                    ->andWhere(['company_id' => $company->id])
                    ->andWhere(['notification_id' => $notification->id])
                    ->exists()) {
                    $companyNotification = new CompanyNotification();
                    $companyNotification->company_id = $company->id;
                    $companyNotification->notification_id = $notification->id;
                    $companyNotification->status = Notification::STATUS_NOT_PASSED;
                    $companyNotification->save();
                }
            }
        }
    }
}