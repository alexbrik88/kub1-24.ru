<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-retail',
    'name' => 'КУБ.РозничнаяТочка',
    'basePath' => dirname(__DIR__),
    'homeUrl' => '/',
    'bootstrap' => ['log'],
    'controllerNamespace' => 'retail\controllers',
    'components' => [
        'assetManager' => [
            'appendTimestamp' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'request' => [
            'csrfParam' => '_csrf-retail',
            'csrfCookie' => [
                'httpOnly' => true,
                'secure' => YII_ENV_PROD
            ],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the retail
            'name' => 'advanced-retail',
            'cookieParams' => [
                'httpOnly' => true,
                'secure' => YII_ENV_PROD,
            ],
        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/' => '/site/index',
            ],
        ],
        'user' => [
            'identityClass' => 'common\models\employee\Employee',
            'identityCookie' => [
                'name' => '_identity-retail',
                'httpOnly' => true,
                'secure' => YII_ENV_PROD
            ],
            'enableAutoLogin' => false,
        ],
    ],
    'params' => $params,
];
