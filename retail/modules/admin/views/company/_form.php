<?php

use common\widgets\DatePicker;
use common\models\company\CompanyType;
use common\components\widgets\BikTypeahead;
use dosamigos\datetimepicker\DateTimePicker;
use kartik\select2\Select2;
use retail\assets\SuggestionsAsset;
use retail\assets\DevbridgeAutocompleteAsset;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Company */
/* @var $form yii\widgets\ActiveForm */

SuggestionsAsset::register($this);
DevbridgeAutocompleteAsset::register($this);

$companyTypes = ['' => ''] + CompanyType::find()->select('name_short')->andWhere([
    'in_company' => true,
])->indexBy('id')->column();
$typeIds = CompanyType::find()->select(['id'])->andWhere([
    'in_company' => true,
])->indexBy('name_short')->column();
$isIP = $model->company_type_id == CompanyType::TYPE_IP;
$isOOO = $model->company_type_id && $model->company_type_id != CompanyType::TYPE_IP;
$account = $model->checkingAccountant;
$tax = $model->companyTaxationType;
$errorModelArray = [
    $model,
    $account,
    $tax,
];

$ndsViewOsno = ArrayHelper::map(\common\models\NdsOsno::find()->all(), 'id', 'name');
?>

<?php $form = ActiveForm::begin([
    'id' => 'company-form',
    'enableAjaxValidation' => true,
    'enableClientValidation' => false,
    'options' => [
        'class' => 'form-retail',
    ],
]); ?>

    <?= $form->errorSummary($errorModelArray); ?>

    <div class="row">
        <div class="col-sm-5">
            <?= $form->field($model, 'inn')->textInput([
                'maxlength' => true,
                'autocomplete' => 'off',
                'data-types' => Json::encode($typeIds),
            ])->label('<span class="is-empty">Автозаполнение по </span>ИНН') ?>
        </div>
        <div class="col-sm-5">
            <?= $form->field($model, 'kpp')->textInput([
                'maxlength' => true,
            ]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-2">
            <?= $form->field($model, 'company_type_id')->widget(Select2::classname(), [
                'data' => $companyTypes,
                'options' => [
                    'placeholder' => '',
                ],
                'pluginOptions' => [
                    //'allowClear' => true,
                    'dropdownCssClass' => 'form-retail',
                    'minimumResultsForSearch' => 10,
                ],
            ])->label('Форма'); ?>
        </div>
        <div class="col-sm-10">
            <?= Html::beginTag('div', [
                'class' => 'ip-filds' . ($isIP ? '' : ' hidden'),
            ]);  ?>
                <?= $form->field($model, 'chief_fio')->textInput([
                    'maxlength' => true,
                    'disabled' => !$isIP,
                ])->label('ФИО') ?>
            <?= Html::endTag('div') ?>
            <?= Html::beginTag('div', [
                'class' => 'ooo-filds' . ($isOOO ? '' : ' hidden'),
            ]);  ?>
                <?= $form->field($model, 'name_short')->textInput([
                    'maxlength' => true,
                    'disabled' => !$isOOO,
                ]) ?>
            <?= Html::endTag('div') ?>
        </div>
    </div>

    <?= Html::beginTag('div', [
        'class' => 'ooo-filds' . ($isOOO ? '' : ' hidden'),
    ]);  ?>
        <?= $form->field($model, 'name_full')->textInput([
            'maxlength' => true,
            'disabled' => !$isOOO,
        ]) ?>
    <?= Html::endTag('div') ?>

    <?= $form->field($tax, 'osno', [
        'options' => [
            'class' => 'form-group required',
        ],
        'parts' => [
            '{input}' => Html::beginTag('div', [
                    'class' => 'field-width inp_one_line_company',
                ]) . "\n" .
                Html::activeCheckbox($tax, 'osno', [
                    'class' => 'uniform',
                    'labelOptions' => [
                        'class' => 'm-r-15',
                        'style' => 'cursor: pointer;',
                    ],
                    'disabled' => (boolean) $tax->usn,
                ]) . "\n" .
                Html::activeCheckbox($tax, 'usn', [
                    'class' => 'uniform',
                    'labelOptions' => [
                        'class' => 'm-r-15',
                        'style' => 'cursor: pointer;',
                    ],
                    'disabled' => (boolean) $tax->osno,
                ]) . "\n" .
                Html::activeCheckbox($tax, 'envd', [
                    'class' => 'uniform',
                    'labelOptions' => [
                        'class' => 'm-r-15',
                        'style' => 'cursor: pointer;',
                    ],
                ]) . "\n" .
                Html::activeCheckbox($tax, 'psn', [
                    'class' => 'uniform',
                    'labelOptions' => [
                        'style' => 'cursor: pointer;',
                    ],
                    'disabled' => $model->company_type_id != CompanyType::TYPE_IP,
                ]) . "\n" .

                Html::beginTag('div', ['class' => 'row nds-view-osno collapse' . ($tax->osno ? ' in' : '')]) . "\n" .
                $form->field($model, 'nds', [
                    'options' => [
                        'class' => 'col-lg-12 m-b-5',
                    ],
                ])->radioList($ndsViewOsno, [
                    'class' => 'inp_one_line_company',
                    'item' => function ($index, $label, $name, $checked, $value) {
                        $item = Html::beginTag('div', ['class' => 'm-b-5']);
                        $item .= Html::tag('label', Html::radio($name, $checked, [
                            'value' => $value,
                            'class' => 'uniform',
                        ]) . $label, [
                            'class' => 'm-b-5',
                        ]);
                        $item .= Html::endTag('div');

                        return $item;
                    },
                ])->label('В счетах цену за товар/услугу указывать:') . "\n" .
                Html::endTag('div') . "\n" .

                Html::beginTag('div', ['class' => 'row tax-usn-config collapse' . ($tax->usn ? ' in' : '')]) . "\n" .
                $form->field($tax, 'usn_type', [
                    'options' => [
                        'class' => 'col-lg-6 ',
                        'style' => 'margin-bottom: 5px; max-width: 250px',
                    ],
                ])->radioList(\common\models\company\CompanyTaxationType::$usnType, [
                    'unselect' => null,
                    'class' => '',
                    'style' => 'margin-top: 10px;',
                    'item' => function ($index, $label, $name, $checked, $value) use ($tax) {
                        $item = Html::beginTag('div', ['class' => 'm-b-5']);
                        $item .= Html::tag('label', Html::radio($name, $checked, [
                            'value' => $value,
                            'class' => 'uniform',
                        ]) . $label, [
                            'style' => 'display: inline-block; width: 130px; margin-right: 20px;',
                        ]);
                        $item .= Html::activeTextInput($tax, 'usn_percent', [
                            'class' => 'form-control input-sm usn-percent usn-percent-' . $value,
                            'style' => 'display: inline-block; width: 50px;',
                            'disabled' => !$checked,
                            'value' => $checked ? $tax->usn_percent : $tax->defaultUsnPercent($value),
                            'data-default' => $tax->defaultUsnPercent($value),
                        ]) . ' %';
                        $item .= Html::endTag('div');

                        return $item;
                    },
                ])->label(false) . "\n" .

                Html::beginTag('div', ['class' => 'col-lg-6 tax-patent-config collapse' . ($tax->psn ? ' in' : '')]) . "\n" .

                Html::beginTag('div', ['class' => 'row', 'style' => 'margin-bottom:4px']) .
                Html::label('Субъект РФ, в котором получен патент', 'ip_patent_city', [
                    'class'=>'col-md-12 label-width',
                    'style' => 'padding-top:5px;']) .
                Html::beginTag('div', ['style' => 'position:relative', 'class' => 'col-md-12']) .
                Html::activeTextInput($model, 'ip_patent_city', [
                    'class' => 'form-control',
                ]) .
                Html::endTag('div') .
                Html::endTag('div') . "\n" .

                Html::beginTag('div', ['class' => 'row', 'style' => 'margin-bottom:4px']) .
                Html::label('Дата начала действия патента', 'patentDate', [
                    'class'=>'col-md-12 label-width',
                    'style' => 'padding-top:5px']) .
                Html::beginTag('div', ['style' => 'max-width: 170px;', 'class' => 'col-md-12']) .
                DatePicker::widget([
                    'model' => $model,
                    'attribute' => 'patentDate',
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'dd.mm.yyyy',
                    ]
                ]) .
                Html::endTag('div') .
                Html::endTag('div') . "\n" .

                Html::beginTag('div', ['class' => 'row', 'style' => 'margin-bottom:14px']) .
                Html::label('Дата окончания действия патента', 'patentDateEnd', [
                    'class'=>'col-md-12 label-width p-t-5',
                ]) .
                Html::beginTag('div', ['style' => 'max-width: 170px;', 'class' => 'col-md-12']) .
                DatePicker::widget([
                    'model' => $model,
                    'attribute' => 'patentDateEnd',
                    'language' => 'ru',
                    'clientOptions' => [
                        'autoclose' => true,
                        'format' => 'dd.mm.yyyy',
                        'language' => 'ru',
                    ]
                ]) .
                Html::endTag('div') .
                Html::endTag('div') .
                Html::endTag('div') .
                Html::endTag('div') . "\n" .



                Html::endTag('div'),
        ],
    ])->label('Система налогообложения'); ?>

    <?= Html::beginTag('div', [
        'class' => 'row ooo-filds' . ($isOOO ? '' : ' hidden'),
    ]);  ?>
        <div class="col-sm-5">
            <?= $form->field($model, 'chief_post_name')->textInput([
                'maxlength' => true,
                'disabled' => !$isOOO,
            ]) ?>
        </div>
        <div class="col-sm-7">
            <?= $form->field($model, 'chief_fio')->textInput([
                'maxlength' => true,
                'disabled' => !$isOOO,
            ]) ?>
        </div>
    <?= Html::endTag('div') ?>

    <?= $form->field($model, 'address_legal')->textInput([
        'maxlength' => true,
    ]) ?>

    <?= $form->field($model, 'phone')->textInput()->widget(\yii\widgets\MaskedInput::className(), [
        'mask' => '+7(9{3}) 9{3}-9{2}-9{2}',
        'options' => [
            'class' => 'form-control' . ($model->phone ? ' edited' : ''),
        ],
        'clientOptions' => [
            'showMaskOnHover' => false,
        ]
    ]) ?>

    <div class="row">
        <div class="col-sm-7">
            <?= $form->field($account, 'rs')->textInput([
                'maxlength' => true,
            ])->label('Расчётный счёт') ?>
        </div>
        <div class="col-sm-5">
            <?= $form->field($account, 'bik')->textInput([
                'maxlength' => true,
                'class' => 'form-control dictionary-bik' . ($account->bik ? ' edited' : ''),
                'data' => [
                    'url' => Url::to(['/dictionary/bik']),
                    'target-name' => '#' . Html::getInputId($account, 'bank_name'),
                    'target-city' => '#' . Html::getInputId($account, 'bank_city'),
                    'target-ks' => '#' . Html::getInputId($account, 'ks'),
                    'target-collapse' => '#company-bank-block',
                ]
            ])->label('БИК банка') ?>
        </div>
    </div>

    <div id="company-bank-block" class="collapse<?= $account->bik ? ' in' : ''; ?>">
        <div class="row">
            <div class="col-sm-7">
                <?= $form->field($account, 'bank_name')->textInput([
                    'maxlength' => true,
                    'readonly' => true,
                ]) ?>
            </div>
            <div class="col-sm-5">
                <?= $form->field($account, 'bank_city')->textInput([
                    'maxlength' => true,
                    'readonly' => true,
                ]) ?>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-7">
                <?= $form->field($account, 'ks')->textInput([
                    'maxlength' => true,
                    'readonly' => true,
                ]) ?>
            </div>
        </div>
    </div>

    <div class="ip-filds">
        <?= Html::activeHiddenInput($model, 'egrip', ['disabled' => !$isIP]); ?>
    </div>
    <div class="ooo-filds">
        <?= Html::activeHiddenInput($model, 'ogrn', ['disabled' => !$isOOO]); ?>
    </div>

    <div class="form-group">
        <?= Html::submitButton('Сохранить', ['class' => 'btn btn-yes text-bold text-upp']) ?>
        <?php if (Yii::$app->user->identity->getCompaniesByChief()->exists()) : ?>
            <?= Html::a('Отменить', ['retail-point/index'], [
                'class' => 'btn btn-no text-bold text-upp pull-right',
            ]) ?>
        <?php endif ?>
    </div>
<?php ActiveForm::end(); ?>


<?php
$this->registerJs(<<<JS
var toggleCompanyType = function(form, type) {
    $('.ooo-filds', form).toggleClass('hidden', type == '1');
    $('.ip-filds', form).toggleClass('hidden', type != '1');
    $('.ooo-filds input', form).prop('disabled', type == '1');
    $('.ip-filds input', form).prop('disabled', type != '1');
}
$('#company-form #company-inn').suggestions({
    serviceUrl: 'https://dadata.ru/api/v2',
    token: '78497656dfc90c2b00308d616feb9df60c503f51',
    type: 'PARTY',
    count: 10,

    onSelect: function(suggestion) {
        console.log(suggestion);
        var form = this.form;
        var typeList = $(this).data('types');
        var typeName = suggestion.data.opf.short;
        var typeId = typeList[typeName] | '';
        var address = '';
        if (suggestion.data.address.data && suggestion.data.address.data.postal_code) {
            if (suggestion.data.address.value.indexOf(suggestion.data.address.data.postal_code) == -1) {
                address += suggestion.data.address.data.postal_code + ', ';
            }
        }
        address += suggestion.data.address.value;
        $('#company-inn', form).val(suggestion.data.inn);
        $('#company-company_type_id', form).val(typeId);
        $('#company-okpo', form).val(suggestion.data.okpo);
        $('#company-okved', form).val(suggestion.data.okved);
        $('#company-address_legal', form).val(address);
        $('#company-egrip', form).val(suggestion.data.ogrn);
        $('#company-kpp', form).val(suggestion.data.kpp);
        $('#company-name_short', form).val(suggestion.data.name.short);
        if (typeId == '1') {
            $('.ip-filds #company-chief_fio', form).val(suggestion.data.name.full);
            $('.ip-filds #company-egrip', form).val(suggestion.data.ogrn);
        } else {
            $('.ooo-filds #company-name_full', form).val(suggestion.data.name.full);
            $('.ooo-filds #company-chief_fio', form).val(suggestion.data.management.name);
            $('.ooo-filds #company-chief_post_name', form).val(suggestion.data.management.post);
            $('.ooo-filds #company-ogrn', form).val(suggestion.data.ogrn);
        }
        toggleCompanyType(form, typeId);
    }
});
$(document).on("change", "#company-form #company-company_type_id", function() {
    var form = this.form;
    if (this.value == '1') {
        $("#companytaxationtype-psn", form).prop("disabled", false).uniform("refresh");
    } else {
        $("#companytaxationtype-psn", form).prop("checked", false).prop("disabled", true).uniform("refresh");
    }
    toggleCompanyType(form, this.value);
});
$(document).on("change", "#company-form #companytaxationtype-osno", function() {
    var form = this.form;
    if (this.checked) {
        $("#companytaxationtype-usn", form).prop("checked", false).prop("disabled", true).uniform("refresh");
        $(".nds-view-osno", form).collapse("show");
    } else {
        $("#companytaxationtype-usn", form).prop("disabled", false).uniform("refresh");
        $(".nds-view-osno", form).collapse("hide");
        $(".nds-view-osno .has-error", form).removeClass("has-error");
        $(".nds-view-osno input[type=radio]", form).prop("checked", false).uniform("refresh");
    }
});
$(document).on("change", "#company-form #companytaxationtype-usn", function() {
    var form = this.form;
    if (this.checked) {
        $("#companytaxationtype-osno", form).prop("checked", false).prop("disabled", true).uniform("refresh");
        $(".tax-usn-config", form).collapse("show");
    } else {
        $("#companytaxationtype-osno", form).prop("disabled", false).uniform("refresh");
        $(".tax-usn-config", form).collapse("hide");
        $("#companytaxationtype-usn_type input[type=radio][value=0]", form).prop("checked", true).uniform("refresh");
        $("#companytaxationtype-usn_type input[type=radio][value=1]", form).prop("checked", false).uniform("refresh");
        $("#companytaxationtype-usn_percent", form).val("");
    }
});
$(document).on("change", "#company-form #companytaxationtype-psn", function() {
    var form = this.form;
    if (this.checked) {
        $("#companytaxationtype-envd", form).prop("checked", false).prop("disabled", true).uniform("refresh");
        $(".tax-patent-config", form).collapse("show");
    } else {
        $("#companytaxationtype-envd", form).prop("disabled", false).uniform("refresh");
        $(".tax-patent-config", form).collapse("hide");
    }
});
$(document).on("change", "#company-form #companytaxationtype-usn_type input[type=radio]", function() {
    var form = this.form;
    var value = $('#companytaxationtype-usn_type input[type=radio]:checked', form).val();
    $('#companytaxationtype-usn_type input.usn-percent', form).each(function (i, item) {
        $(item).prop('disabled', true).val($(item).data('default'));
    });
    $('#companytaxationtype-usn_type input.usn-percent-' + value, form).prop('disabled', false);
});
$('#company-form .dictionary-bik').devbridgeAutocomplete({
    serviceUrl: function() {
        return $(this).data('url');
    },
    minLength: 1,
    paramName: 'q',
    dataType: 'json',
    showNoSuggestionNotice: true,
    noSuggestionNotice: 'БИК не найден. Проверьте введённые данные.',
    transformResult: function (response) {
        return {
            suggestions: $.map(response, function (item, key) {
                item.value = item.name;
                return item;
            }),
        };
    },
    onSelect: function (suggestion) {
        $(this).val(suggestion.bik);
        $($(this).data('target-name')).val(suggestion.name);
        $($(this).data('target-city')).val(suggestion.city);
        $($(this).data('target-ks')).val(suggestion.ks);
        $($(this).data('target-collapse')).collapse('show');

        $('#first-invoice-form .invoice-form-company .form-control').each(function(){
            $(this).toggleClass('edited', $(this).val().length > 0);
        });
    }
});
JS
);
