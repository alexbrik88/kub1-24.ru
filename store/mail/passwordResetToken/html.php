<?php
use yii\helpers\Html;

$this->params['utm_source'] = 'unisender';
$this->params['utm_medium'] = 'email';
$this->params['utm_campaign'] = 'store';
$this->params['utm_term'] = \yii\helpers\Inflector::slug($subject, '_');

$resetLink = Yii::$app->urlManager->createAbsoluteUrl([
    'site/reset-password',
    'token' => $user->password_reset_token,
]);
?>
<table cellpadding="20" cellspacing="0" style="width:100%;">
    <tr>
        <td>
            <p style="font-size: 24pt; line-height: 21px; margin-bottom: 20px; color: #000;">
                Восстановить пароль
            </p>
            <p style="margin: 0; font-size: 12pt; margin-bottom: 20px;">
                Здравствуйте, <?= Html::encode($user->firstname) ?>!
            </p>
            <p style="margin: 0; font-size: 12pt; margin-bottom: 20px;">
                Для восстановления Вашего пароля на сайте <?= Yii::$app->request->serverName ?>,
                Вам надо пройти по следующей ссылке и следовать дальнейшим инструкциям:
            </p>
            <p style="margin: 0; font-size: 12pt; margin-bottom: 20px;">
                Пройдите со ссылке:
                <br>
                <?= Html::a(Html::encode($resetLink), $resetLink, [
                    'target' => '_blank',
                    'style' => 'color: #2E41BA;'
                ]) ?>
            </p>
            <p style="margin: 0; font-size: 12pt; margin-bottom: 20px;">
                Если ссылка не работает, скопируйте и вставьте URL в адресную строку в новом окне браузера.
            </p>
            <p style="margin: 0; font-size: 12pt; margin-bottom: 20px;">
                Если Вы получили это письмо по ошибке, вероятно другой пользователь случайно указал Ваш адрес при изменении пароля.
                В таком случае проигнорируйте это сообщение.
            </p>
            <p style="margin: 0; font-size: 12pt; margin-bottom: 20px;">
                По всем вопросам обращайтесь в службу поддержки
                <a href="mailto:<?= $supportEmail ?>"><?= $supportEmail ?></a>
            </p>
        </td>
    </tr>
    <tr>
        <td>
            <p style="margin: 0; font-size: 12pt; font-weight: bold;">
                С уважением,<br> Команда КУБ
            </p>
        </td>
    </tr>
</table>

<?= $this->render('@common/mail/layouts/parts/_contacts') ?>
