<?php

namespace store\components;

use common\components\CommonController;
use common\components\DbTimeZone;
use Yii;
use yii\helpers\Url;

/**
 * Class FrontendController
 * @package store\components
 */
abstract class StoreController extends CommonController
{
    /**
     * @var string
     */
    public $layoutWrapperCssClass = '';

    /**
     * Init
     */
    public function init()
    {
        parent::init();

        if (!Yii::$app->user->isGuest) {
            $tz = Yii::$app->user->identity->timeZone ?
                Yii::$app->user->identity->timeZone->time_zone :
                'Europe/Moscow';

            date_default_timezone_set($tz);

            DbTimeZone::sync();
        }
    }

    /**
     * @param \yii\base\Action $action
     * @return bool|\yii\web\Response
     * @throws ForbiddenHttpException
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        if (parent::beforeAction($action)) {
            if (!Yii::$app->user->isGuest) {
                if (Yii::$app->user->identity->getCurrentStoreCompany() === null
                    && !in_array($this->id, ['site', 'profile'])
                    && Yii::$app->user->identity->company_id == null
                ) {
                    $this->redirect(['/site/blocked'])->send();

                    return false;
                }
            }

            return true;
        }

        return false;
    }

    /**
     * @param \yii\base\Action $action
     * @param mixed $result
     * @return mixed
     */
    public function afterAction($action, $result)
    {
        $result = parent::afterAction($action, $result);

        if (!Yii::$app->request->isAjax && !in_array($action->id, ['create', 'update'])) {
            Url::remember('', 'lastPage');
        }

        return $result;
    }
}
