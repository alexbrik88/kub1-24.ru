<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 17.04.2018
 * Time: 15:32
 */

namespace store\assets;


use yii\web\AssetBundle;

/**
 * Class DocumentPrintAsset
 * @package store\assets
 */
class DocumentPrintAsset extends AssetBundle
{
    /**
     * @var string
     */
    public $basePath = '@webroot';

    /**
     * @var array
     */
    public $css = [
        'css/print/documents.css',
    ];
    /**
     * @var array
     */
    public $depends = [
        'store\assets\PrintAsset',
    ];
}