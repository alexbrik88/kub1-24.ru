<?php

namespace store\assets;

use yii\web\AssetBundle;

/**
 * Main store application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/custom.css',
    ];
    public $js = [
        'js/custom.js',
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\jui\JuiAsset',
        'yii\widgets\PjaxAsset',
        'common\assets\CommonAsset',
    ];
}
