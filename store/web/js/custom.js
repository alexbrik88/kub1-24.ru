(function ($) {
    $(document).on("click", ".new-request-form-btn", function (e) {
        e.preventDefault();
        var formData = $(this.form).serialize() + '&' + this.name + '=' + this.value;
        $.post(this.form.action, formData, function (data) {
            if (typeof data.content !== 'undefined') {
                $("#new-company-request-content").html(data.content);
            }
            if (typeof data.newCount !== 'undefined') {
                $("#new-company-request-count").html(data.newCount);
            }
        });
    });

    var selectedProdType = null;
    window.ORDERDOCUMENT = {
        productSeletedToOrderDocument: [],
        $table: $('#product-table-order-document'),
        $addToOrderDocumentButton: $('#add-to-invoice-button'),
        OrderOut: 2,
        OrderIn: 1,
        getItems: function () {
            return this.$table.find('tbody > tr:not(.template):not(.button-add-line)');
        },
        getFloatPrice: function (price) {
            return (price / 100).toFixed(2);
        },
        tultipProduct: function (selector) {
            if ($.isFunction($().tooltipster)) {
                $(selector).tooltipster({
                    "multiple": true,
                    "theme": ['tooltipster-kub'],
                    "functionBefore": function (instance, helper) {
                        instance.content(helper.origin.value);
                    }
                });
            }
        },
        removeProductFromOrderDocumentId: {},
        removeProductFromOrderDocument: function () {
            var btn = ORDERDOCUMENT.removeProductFromOrderDocumentId;

            if ($(btn).hasClass('from-new')) {
                $('#from-new-add-row').hide();
            } else {
                $(btn).parents('tr:first').remove();
                this.recalculateOrderDocumentTable();
            }
            this.recalculateOrderDocumentTable();
        },
        recalculateOrderDocumentTable: function () {
            var $discountFromAmount = $('input.discount-from-amount:hidden');
            var $discountPercent = $('input.discount-percent');
            var subTotal = 0;
            var $totalWeight = 0;
            var $sumForDiscount = $discountFromAmount.length > 0 ? $discountFromAmount.val() : -1;
            var $discount = $discountPercent.length > 0 ? $discountPercent.val() : 0;
            var totalDiscount = 0;

            this.getItems().each(function () {
                var $this = $(this);
                var $countInput = $this.find('input.product-count');
                if (!$this.hasClass('from-new-add')) {
                    var productCount = Math.max(0, parseFloat($countInput.val() || 0));
                    var priceWithNds = APP.helper.inputToMoneyFormat($this.find('.price-one-with-nds').text());
                    var $weight = parseFloat($this.find('.product-weight').text());
                    var productSubTotal = priceWithNds * productCount;
                    if (isNaN($weight)) {
                        $weight = 0;
                    }
                    subTotal += productSubTotal;
                    $totalWeight += $weight * productCount;
                    if ($countInput.val() != productCount) {
                        $countInput.val(productCount);
                    }
                }
            });
            if (subTotal >= $sumForDiscount && $discount > 0) {
                subTotal = 0;
                this.getItems().each(function () {
                    var $this = $(this);
                    var $countInput = $this.find('input.product-count');
                    if (!$this.hasClass('from-new-add')) {
                        ORDERDOCUMENT.recalculateItemPrice($this, $discount);
                        var priceWithNds = APP.helper.inputToMoneyFormat($this.find('.price-one-with-nds').text());
                        var productCount = Math.max(0, parseFloat($countInput.val() || 0));
                        subTotal += (priceWithNds * productCount);
                        if ($countInput.val() != productCount) {
                            $countInput.val(productCount);
                        }
                        productDiscount = (APP.helper.inputToMoneyFormat($this.find('.price-one').text()) * ($discount / 100)) * productCount;
                        totalDiscount += productDiscount;
                    }
                });
            } else {
                $('.discount_column').addClass('hidden');
            }
            $('#discount_sum').text(parseFloat(totalDiscount).toFixed(2));
            $('#total_price').text(parseFloat(subTotal).toFixed(2));
            $('#total_mass').text($totalWeight.toFixed(2));
        },
        recalculateItemPrice: function ($input, $discount) {
            $discount = typeof $discount !== 'undefined' ? $discount : 0;
            var $row = $input.closest('tr');
            var $priceInput = $row.find('.price-one');
            var $priceInputWithDiscount = $row.find('.discount_column span.price-one-with-nds-and-discount');
            var $discountInput = $row.find('.discount_column span.discount-percent');
            var count = $row.find('.product-count').val() || 0;
            var price = Math.round(($priceInput.data('price') + "").trim() * 100) / 100 || 0;
            var discount = Math.min(99.9999, Math.round($discount * 10000) / 10000 || 0);
            var priceOne = price - Math.round((price * discount / 100) * 100) / 100;
            var $availableCount = $row.find('.product-available').text() * 1 > 0 ? $row.find('.product-available').text() * 1 : 1;
            if (+count > $availableCount) {
                $row.find('.product-count').val($availableCount);
                count = $availableCount;
            }
            if (discount > 0) {
                $discountInput.text($discount);
                $priceInputWithDiscount.text(parseFloat(priceOne).toFixed(2));
                $('.discount_column').removeClass('hidden');
            } else {
                $discountInput.text(0);
                $priceInputWithDiscount.text(0);
                $('.discount_column').addClass('hidden');
            }
            $priceInput.text(parseFloat(price).toFixed(2));
            $row.find('.price-one-with-nds').text(parseFloat(priceOne).toFixed(2));
            $row.find('.price-one-with-nds-input').val(priceOne);
            $row.find('.price-with-nds').text(parseFloat(priceOne * count).toFixed(2));
        },
        addProductToTable: function (items) {
            if (items) {
                var $template = ORDERDOCUMENT.$table.find('.template').clone();
                $template.removeClass('template').addClass('product-row');
                $template.find('input').attr('disabled', false);
                var $itemArray = [];
                var itemNumber = ORDERDOCUMENT.getItems().length;

                for (var i = 0; i < items.length; i++) {
                    var item = items[i];
                    var unitName = item.productUnit ? item.productUnit.name : '---';
                    $item = $template.clone();
                    $item.attr('id', 'model_' + item.id);
                    $item.find('.product-delete').find('.remove-product-from-order-document').data('id', item.id);
                    $item.find('.product-title')
                        .attr('name', 'orderArray[' + itemNumber + '][title]')
                        .val(item.title);
                    $item.find('.product-article').text(item.article ? item.article : '---');
                    $item.find('.tax-rate').val(item.priceForSellNds.rate);
                    $item.find('.product-id')
                        .attr('name', 'orderArray[' + itemNumber + '][id]')
                        .val(item.id);
                    $item.find('.product-model')
                        .attr('name', 'orderArray[' + itemNumber + '][model]');
                    $item.find('.product-unit-name').text(unitName);
                    $item.find('.product-quantity').text(item.totalQuantity ? item.totalQuantity * 1 : '---');
                    $item.find('.product-reserve').text(item.reserve);
                    $item.find('.product-available').text(item.available);
                    $item.find('.product-weight').text(item.weight ? item.weight * 1 : '---');
                    $item.find('.product-volume').text(item.volume ? item.volume * 1 : '---');
                    $item.find('.product-count')
                        .attr('id', 'item-count' + item.id)
                        .attr('name', 'orderArray[' + itemNumber + '][count]')
                        .val(1);
                    if (unitName == '---') {
                        $item.find('.product-count').attr('type', 'hidden');
                        $item.find('.product-no-count').removeClass('hidden');
                    }
                    $item.find('.price-input')
                        .attr('name', 'orderArray[' + itemNumber + '][price]');
                    $item.find('.discount-input')
                        .attr('name', 'orderArray[' + itemNumber + '][discount]');
                    $item.find('.price-one-with-nds-input')
                        .attr('name', 'orderArray[' + itemNumber + '][priceOneWithVat]');
                    $item.find('.price-for-sell-nds-name').text(item.priceForSellNds.name);
                    var viewType = parseInt($('.view-type').val());
                    if (item.price_for_sell_with_nds == null) {
                        item.price_for_sell_with_nds = 0;
                    }
                    if (item.price_for_buy_with_nds == null) {
                        item.price_for_buy_with_nds = 0;
                    }
                    var priceForSell = viewType == 2 ? Math.round(item.price_for_sell_with_nds / (Number(item.priceForSellNds.rate) + 1)) : item.price_for_sell_with_nds;

                    $item.find('.price-input').val(ORDERDOCUMENT.getFloatPrice(parseInt(priceForSell)));
                    $item.find('.price-one-with-nds').text(ORDERDOCUMENT.getFloatPrice(parseInt(priceForSell)));
                    $item.find('.price-one-with-nds-input').val(ORDERDOCUMENT.getFloatPrice(parseInt(priceForSell)));
                    $item.find('.price-with-nds').text(ORDERDOCUMENT.getFloatPrice((priceForSell)));
                    $item.find('.price-one').text(ORDERDOCUMENT.getFloatPrice(parseInt(priceForSell))).data('price', ORDERDOCUMENT.getFloatPrice(parseInt(priceForSell)));

                    $itemArray.push($item);
                    itemNumber++;
                }
                for (i in $itemArray) {
                    $itemArray[i].show().insertBefore('#from-new-add-row');
                }
                this.recalculateOrderDocumentTable();

                return true;
            }

            return false;
        },
        searchProduct: function (type, doctype) {
            var exists = $('input.product-id').map(function (idx, elem) {
                return $(elem).val();
            }).get();
            $.pjax({
                url: '/product/get-products',
                container: '#pjax-product-grid',
                data: {documentType: doctype, productType: type, exists: exists},
                push: false,
                timeout: 10000,
            });
            $('#add-from-exists').modal();
        },
        addNewProduct: function (url, form) {
            $.post(
                url,
                form,
                function (data) {
                    $('#add-new').modal();
                    if (data) {
                        if (data.header !== undefined) {
                            $('#add-new .modal-header').html(data.header);
                            $('#block-modal-new-product-form').html(data.body);
                            $('.col-sm-4.col-md-5.control-label.bold-text').next().addClass('inp_one_line-product');
                            var urlunits = $('#get-product-units-url').val();
                            $("#product-production_type").on("change", "input", function () {
                                $('#new-product-order-document-form input, #new-product-order-document-form select').attr('disabled', false);
                                $('#new-product-order-document-form input:radio:not(.md-radiobtn), #new-product-order-document-form input:checkbox:not(.md-check)').uniform("refresh");
                                $('#new-product-order-document-form input[type="text"]').val('');
                                $('.error-summary').remove();
                                if ($(this).val() == '0') {
                                    $('.forProduct').addClass('selectedDopColumns');
                                    $('.field-product-product_unit_id').removeClass('required');
                                } else {
                                    $('.forProduct').removeClass('selectedDopColumns');
                                    $('.field-product-product_unit_id').addClass('required');
                                }
                                if ($('#is_first_invoice').val() == '1') {
                                    if ($(this).val() == '0') {
                                        $('#product-title').attr('placeholder', 'Например: Доставка товара');
                                        $('#product-price_for_sell_with_nds').attr('placeholder', 'Например: 500 (указывать только цифры)');
                                    }
                                    if ($(this).val() == '1') {
                                        $('#product-title').attr('placeholder', 'Например: Доска обрезная');
                                        $('#product-price_for_sell_with_nds').attr('placeholder', 'Например: 1000 (указывать только цифры)');
                                    }
                                }

                                $.post(urlunits, {
                                    productType: $(this).val()
                                }).done(function (data) {
                                    if (data.html) {
                                        $('#product-product_unit_id').html($(data.html).html());
                                    }
                                });
                            });
                        } else if (data.items !== undefined) {
                            var prodCount = data.services + data.goods;

                            $('.order-add-static-items .service-count-value').text(data.services);
                            if (data.services > 0) {
                                $('li.select2-results__option.add-modal-services').removeClass('hidden');
                            }
                            $('.order-add-static-items .product-count-value').text(data.goods);
                            if (data.goods > 0) {
                                $('li.select2-results__option.add-modal-products').removeClass('hidden');
                            }
                            if ($('.add-exists-product.hidden').length && prodCount > 0) {
                                $('.add-first-product-button').addClass('hidden');
                                $('.add-exists-product.hidden').removeClass('hidden');
                            }
                            ORDERDOCUMENT.addProductToTable(data.items);
                            $('#from-new-add-row').hide();

                            $('#add-new').modal('hide');
                        } else {
                            $('#add-new').modal('hide');
                        }
                    }
                }
            );
        },
    };

    window.PREORDER = {
        selectedData: {},
        params: {},
        recalculate: function () {
            this.params = {};
            var items = 0;
            var price = 0;
            var totalCount = 0;
            var weight = 0;
            for (var key in this.selectedData) {
                items += 1;
                var prod = this.selectedData[key];
                var count = prod.count;
                this.params['count[' + key + ']'] = count;
                if (count === "") continue;
                count = parseFloat(count);
                totalCount += count;
                price += (count * prod.price);
                weight += (count * prod.weight);
            }
            console.log(items);
            weight = Math.round(weight * 1000) / 1000;
            $("#order_summary .total_price").text(price.toFixed(2).replace(/(\d)(?=(\d{3})+(\.|$))/g, "$1 "));
            $("#order_summary .total_count").text(totalCount.toString().replace(/(\d)(?=(\d{3})+(\.|$))/g, "$1 "));
            $("#order_summary .total_weight").text(weight.toString().replace(/(\d)(?=(\d{3})+(\.|$))/g, "$1 "));
            $("#order_summary .total_items").text(items);
            if (items > 0) {
                $(".company-pricelist .add-to-order").attr("disabled", false);
                $(".company-pricelist .add-to-order").tooltipster("disable");
            } else {
                $(".company-pricelist .add-to-order").attr("disabled", true);
                $(".company-pricelist .add-to-order").tooltipster("enable");
            }
            this.updateState();
        },
        updateState: function () {
            if (typeof history.pushState != 'undefined') {
                var url = new Url();
                var query = this.updateParams(url.query);
                url.clearQuery();
                for (var k in query) {
                    url.query[k] = query[k];
                }
                history.pushState(null, '', url.toString());
            } else {
                console.log('"history.pushState" not supported');
            }
        },
        updateParams: function (query) {
            Object.keys(query).filter(function (key) {
                if (key.indexOf('count') === 0) {
                    delete query[key];
                }
            });

            return $.extend({}, query, this.params);
        },
        reloadPjax: function (u) {
            var url = new Url(u);
            var query = this.updateParams(url.query);
            url.clearQuery();
            for (var k in query) {
                url.query[k] = query[k];
            }
            $.pjax({url: url.toString(), container: '#pricelist-pjax-container', timeout: 10000});
        },
        submit: function () {
            if (!$.isEmptyObject(this.params)) {
                $("#product_checker_form .inputs-selected").html("");
                for (var key in this.params) {
                    $('<input />').attr('type', 'hidden')
                        .attr('name', key)
                        .attr('value', this.params[key])
                        .appendTo("#product_checker_form .inputs-selected");
                }
                $("#product_checker_form").submit();
            }
        },
        search: function () {
            if (!$.isEmptyObject(this.params)) {
                $("#product-search-form .inputs-selected").html("");
                for (var key in this.params) {
                    $('<input />').attr('type', 'hidden')
                        .attr('name', key)
                        .attr('value', this.params[key])
                        .appendTo("#product-search-form .inputs-selected");
                }
            }
            return true;
        },
    };

    $(document).ready(function () {
        ORDERDOCUMENT.recalculateOrderDocumentTable();
        if ($(".company-pricelist").length) {
            $(".preview-product-photo").tooltipster({
                "theme": ["tooltipster-kub"],
                "trigger": "hover",
                "side": "right",
                "contentAsHTML": true,
            });
            $(".product-comment-box").tooltipster({
                "theme": ["tooltipster-kub"],
                "trigger": "hover",
                "contentAsHTML": true
            });
            $(".add-to-order").tooltipster({
                "theme": ["tooltipster-kub"],
                "trigger": "hover",
                "contentAsHTML": true
            });
        }
    });

    $(document).on('pjax:success', '#pricelist-pjax-container', function () {
        $(".preview-product-photo").tooltipster({
            "theme": ["tooltipster-kub"],
            "trigger": "hover",
            "side": "right",
            "contentAsHTML": true,
        });
        $(".product-comment-box").tooltipster({
            "theme": ["tooltipster-kub"],
            "trigger": "hover",
            "contentAsHTML": true
        });
    });

    $(document).on("submit", "#product-search-form", function (e) {
        return PREORDER.search();
    });
    $(document).on("click", "#pricelist-pjax-container a", function (e) {
        e.preventDefault();
        PREORDER.reloadPjax(this.href);
    });
    $(document).on("change", ".company-pricelist input.select-on-check-all", function (e) {
        $("input.product_checker").prop("checked", this.checked).uniform("refresh").trigger("change");
    });
    $(document).on("change", ".company-pricelist input.product_checker", function (e) {
        var id = this.value;
        var $input = $(this).closest("tr").find("input.product-count");
        delete PREORDER.selectedData[id];
        if ($(this).prop("checked")) {
            PREORDER.selectedData[id] = $input.data();
        } else {
            $input.val("");
            $input.data("count", "");
        }
        PREORDER.recalculate();
    });
    $(document).on("change", ".company-pricelist input.product-count", function () {
        var val = this.value;
        var $input = $(this);
        if (val !== "") {
            val = Math.max(0, Math.min($input.attr("max"), val));
            if (val != this.value) {
                this.value = val;
            }
        }
        $input.data("count", val);
        $input.closest("tr").find("input.product_checker").prop("checked", true).uniform("refresh").trigger("change");
    });
    $(document).on("click", ".company-pricelist .add-to-order", function (e) {
        e.preventDefault();
        PREORDER.submit();
    });
    $(document).on('click', '.add-modal-product-new-order-document', function () {
        var ioType = 2;
        var form = {
            documentType: ioType,
            Product: {production_type: null, flag: 1}
        };
        $('#order-add-select, #product-add-select').select2('close');
        ORDERDOCUMENT.addNewProduct('/documents/order-document/add-modal-product', form);
    });

    $(document).on('click', '.add-modal-products', function () {
        $('#order-add-select, #product-add-select').select2('close');
        ORDERDOCUMENT.searchProduct('1', $('#order-add-select').data('doctype'));
    });

    $(document).on('click', '.add-modal-services', function () {
        $('#order-add-select, #product-add-select').select2('close');
        ORDERDOCUMENT.searchProduct('0', $('#order-add-select').data('doctype'));
    });

    $(document).on('change', 'select', (function () {
        var value = $(this).val();
        var form = {
            documentType: ORDERDOCUMENT.OrderOut,
            Product: {
                production_type: 1,
                flag: 1
            }
        };
        if (value == 'add-modal-product') {
            $(this).val(null).trigger("change");
            form.Product.production_type = selectedProdType = null;
            ORDERDOCUMENT.addNewProduct(value, form);
        }

        if (value == 'add-modal-products') {
            $(this).val(null).trigger("change");
            ORDERDOCUMENT.searchProduct('1', $(this).data('doctype'));
        }

        if (value == 'add-modal-services') {
            $(this).val(null).trigger("change");
            ORDERDOCUMENT.searchProduct('0', $(this).data('doctype'));
        }

        if ($(this).attr('id') == 'order-add-select' && parseInt(value)) {
            $(this).val(null).trigger("change");
            $.post('/product/get-products-table?toOrderDocument=1', {in_order: [value]}, function (data) {
                ORDERDOCUMENT.addProductToTable(data);
                $('#from-new-add-row').hide();
            });
        }
    }));
    $(document).on('submit', '#new-product-order-document-form', function (event) {
        event.preventDefault();
        var url = '/documents/order-document/add-modal-product';
        var form = $('#new-product-order-document-form').serialize();
        ORDERDOCUMENT.addNewProduct(url, form);
    });
    $('#add-from-exists').on('shown.bs.modal', function () {
        ORDERDOCUMENT.productSeletedToOrderDocument = [];
    });
    $(document).on('click', '#pjax-product-grid a', function (e) {
        e.preventDefault();
        var $product = ORDERDOCUMENT.productSeletedToOrderDocument;
        $.pjax.reload('#pjax-product-grid', {
            url: $(this).attr('href'),
            type: 'post',
            data: {product: $product},
            push: false,
            replace: false,
            timeout: 5000
        });
    });
    $(document).on('change', '#products_in_order .product_selected', function () {
        var prodId = $(this).val();
        if (this.checked) {
            ORDERDOCUMENT.productSeletedToOrderDocument.push(prodId);
        } else {
            ORDERDOCUMENT.productSeletedToOrderDocument = jQuery.grep(ORDERDOCUMENT.productSeletedToOrderDocument, function (value) {
                return value != prodId;
            });
        }
    });
    $(document).on('click', '#add-to-invoice-button, #add-all-to-invoice-button', function () {
        if ($('#order-document-form').length) {
            $.post('/product/get-products-table?toOrderDocument=1', {in_order: ORDERDOCUMENT.productSeletedToOrderDocument}, function (data) {
                ORDERDOCUMENT.addProductToTable(data);
                ORDERDOCUMENT.productSeletedToOrderDocument = [];
                $('#from-new-add-row').hide();
            });
        }
    });
    $(document).on('hidden.bs.modal', '#add-from-exists', function () {
        ORDERDOCUMENT.productSeletedToOrderDocument = [];
    });
    $(document).on('change', '#orderdocument-has_discount', function () {
        if ($(this).prop('checked')) {
            $('.discount_column').removeClass('hidden');
        } else {
            $('.discount_column').addClass('hidden');
            $('.discount-input').val(0);
            $('#discount_sum').text('0.00');
            $('tr input.price-input:enabled').each(function () {
                ORDERDOCUMENT.recalculateItemPrice($(this));
            });
        }
        ORDERDOCUMENT.recalculateOrderDocumentTable();
    });
    ORDERDOCUMENT.$table.on('click', '.remove-product-from-order-document', function () {
        $('#modal-remove-one-product').modal("show");
        ORDERDOCUMENT.removeProductFromOrderDocumentId = this;
    });
    $(document).on('click', '#modal-remove-one-product .yes', function (e) {
        ORDERDOCUMENT.removeProductFromOrderDocument();
    });
    ORDERDOCUMENT.$table.on('keyup change', '.price-input, .discount-input', function (e) {
        ORDERDOCUMENT.recalculateItemPrice($(this));
        ORDERDOCUMENT.recalculateOrderDocumentTable();
    });
    ORDERDOCUMENT.$table.on('keyup change', '.product-count', function () {
        ORDERDOCUMENT.recalculateItemPrice($(this));
        ORDERDOCUMENT.recalculateOrderDocumentTable();
    });
    $(document).on('change', '#orderdocument-nds_view_type_id', function () {
        ORDERDOCUMENT.recalculateOrderDocumentTable();
    });
    $('.report-item').click(function () {
        var id = $(this).data('id');

        $('#report_' + id).hide();
        $('#report_edit_' + id).hide();
        $('#report_form_' + id).removeClass('hide');
    });
    $('.cancel-edit-report').click(function () {
        var id = $(this).data('id');

        $('#report_' + id).show();
        $('#report_edit_' + id).show();
        $('#report_form_' + id).addClass('hide');
    });
    $(document).on('click', 'span.btn-add-line-table', function () {
        $('#from-new-add-row').show();
    });
    $('.change-order-document .previous, .change-order-document .next').click(function () {
        location.href = $(this).data('url');
    });

    //date range statistic button
    $(document).on('apply.daterangepicker', '#reportrange, .statistic-period-btn', function (ev, picker) {
        var button = $(this);

        $.post(
            '/site/statistic-range',
            {
                name: picker.chosenLabel,
                date_from: picker.startDate.format('YYYY-MM-DD'),
                date_to: picker.endDate.format('YYYY-MM-DD')
            },
            function (data) {
                if (button.hasClass('ajax')) {
                    $('#search-invoice-form').submit();
                } else if (button.data("pjax")) {
                    $.pjax.reload(button.data("pjax"), {timeout: 10000});
                } else if (data) {
                    location.href = location.href;
                }
            }
        );
    });
})(jQuery);
