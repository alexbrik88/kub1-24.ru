<?php

namespace store\widgets;

use common\models\Company;
use common\models\Contractor;
use common\models\product\Product;
use common\models\store\StoreUser;
use frontend\models\Documents;
use Yii;
use yii\base\Widget;

/**
 * Class SideMenuWidget
 * @package frontend\widgets
 */
class SideMenuWidget extends Widget
{
    /**
     * @return string
     */
    public function run()
    {
        return $this->render('sideMenuWidget', [
            'items' => $this->getItems(),
        ]);
    }

    /**
     * @return array
     */
    public function getItems()
    {
        $controller = Yii::$app->controller->id;
        $action = Yii::$app->controller->action->id;
        $module = Yii::$app->controller->module->id;
        $providerItems = [];
        $canProduct = false;
        $currentCompanyName = '';
        /* @var $user StoreUser */
        if ($user = Yii::$app->user->identity) {
            if ($user->company_id) {
                $currentCompanyName = $user->company->getTitle(true);
                $providerItems[0] = [
                    'label' => $user->company->getTitle(true),
                    'encode' => false,
                    'url' => Yii::$app->defaultRoute,
                    'active' => false,
                    'linkOptions' => [
                        'class' => '',
                        'style' => 'background: #3c6c95',
                    ],
                ];
            } else {
                if ($storeCompany = $user->currentStoreCompany) {
                    if ($currentKubContractor = $user->currentKubContractor) {
                        $currentCompanyName = $currentKubContractor->company->getTitle(true);
                        $providerItems[0] = [
                            'label' => $currentKubContractor->company->getTitle(true),
                            'encode' => false,
                            'url' => Yii::$app->defaultRoute,
                            'active' => false,
                            'linkOptions' => [
                                'class' => '',
                                'style' => 'background: #3c6c95',
                            ],
                        ];
                        $canProduct = $user->isContractorActive &&
                                      $currentKubContractor->company->createInvoiceAllowed(Documents::IO_TYPE_OUT);
                    }

                    $contractorQuery = $storeCompany->getContractors()->andWhere([
                        'status' => Contractor::ACTIVE,
                        'is_deleted' => false,
                    ]);

                    foreach ($contractorQuery->all() as $contractor) {
                        if ($currentKubContractor && $contractor->id == $currentKubContractor->id) {
                            continue;
                        }
                        $providerItems[$contractor->id] = [
                            'label' => $contractor->company->getTitle(true),
                            'encode' => false,
                            'url' => ['/site/provider', 'id' => $contractor->id],
                            'active' => false,
                            'linkOptions' => [
                                'class' => 'hidden-companies',
                            ],
                        ];
                    }
                }
            }
            $isCompany = (bool)$user->company_id;
        }

        return [
            [
                'label' => '<i class="icon-list"></i>Мои поставщики<span class="arrow"></span>',
                'encode' => false,
                'url' => 'javascript:;',
                'visible' => !Yii::$app->user->isGuest,
                'active' => false,
                'items' => $providerItems,
                'linkOptions' => [
                    'class' => 'nav-link nav-toggle',
                ],
                'options' => [
                    'class' => 'nav-item_accordeon my-companies',
                ],
            ],
            [
                'label' => '<i class="icon-home"></i><span class="title username">' . $currentCompanyName . '</span>',
                'encode' => false,
                'url' => ['/order/index'],
                'active' => true,
                'visible' => !empty($currentCompanyName),
                'linkOptions' => [
                    'style' => 'padding: 8px 15px 8px 15px;display: block;margin: 0;
                                text-decoration: none;font-size: 14px;font-weight: 300;
                                 border: none;',
                ],
                'options' => [
                    'class' => 'current-company nav-item',
                    'style' => 'background: #467eaf;margin-top: 0.8px !important;',
                ],
            ],
            [
                'label' => '<i class="fa fa-cubes"></i><span class="title">Товары</span>',
                'encode' => false,
                'url' => ['/product/index', 'productionType' => Product::PRODUCTION_TYPE_GOODS],
                'active' => $controller == 'product',
                'linkOptions' => [
                    'class' => 'nav-link nav-toggle',
                ],
                'visible' => !Yii::$app->user->isGuest && ($canProduct || $isCompany),
                'options' => [
                    'class' => 'nav-item',
                ],
            ],
            [
                'label' => '<i class="icon-basket"></i><span class="title">Заказы</span>',
                'encode' => false,
                'url' => ['/order/index'],
                'active' => $controller == 'order',
                'linkOptions' => [
                    'class' => 'nav-link nav-toggle',
                ],
                'visible' => !Yii::$app->user->isGuest,
                'options' => [
                    'class' => 'nav-item',
                ],
            ],
            [
                'label' => '<i class="icon-user"></i><span class="title">Профиль</span>',
                'encode' => false,
                'url' => ['/profile/index'],
                'active' => $controller == 'user',
                'linkOptions' => [
                    'class' => 'nav-link nav-toggle',
                ],
                'visible' => !Yii::$app->user->isGuest && !$isCompany,
                'options' => [
                    'class' => 'nav-item',
                ],
            ],
            [
                'label' => '<i class="icon-login"></i><span class="title">Вход</span>',
                'encode' => false,
                'url' => ['/site/login'],
                'linkOptions' => [
                    'class' => 'nav-link nav-toggle',
                ],
                'visible' => Yii::$app->user->isGuest,
                'options' => [
                    'class' => 'nav-item',
                ],
            ],
            [
                'label' => '<i class="icon-note"></i><span class="title">Регистрация</span>',
                'encode' => false,
                'url' => ['/site/signup'],
                'linkOptions' => [
                    'class' => 'nav-link nav-toggle',
                ],
                'visible' => false, // Yii::$app->user->isGuest,
                'options' => [
                    'class' => 'nav-item',
                ],
            ],
            [
                'label' => '<i class="icon-logout"></i><span class="title">Выход</span>',
                'encode' => false,
                'url' => ['/site/logout'],
                'linkOptions' => [
                    'class' => 'nav-link nav-toggle',
                    'data-method' => 'post',
                ],
                'visible' => !Yii::$app->user->isGuest && !$isCompany,
                'options' => [
                    'class' => 'nav-item',
                ],
            ],
        ];
    }
}
