<?php

use common\widgets\SideMenuNav;

/** @var $this \yii\web\View */
/** @var $items array */

?>

<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <?= \common\widgets\SideMenuNav::widget([
            'items' => $items,
            'options' => [
                'class' => 'page-sidebar-menu page-sidebar-menu-light metismenu',
                'id' => 'side-menu',
                'data' => [
                    'keep-expanded' => 'false',
                    'auto-scroll' => 'true',
                    'slide-speed' => '200',
                ],
                'style' => 'padding-top: 20px',
            ],
        ]); ?>
    </div>
</div>