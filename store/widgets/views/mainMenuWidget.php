<?php
// TODO: set phpdoc.
use common\models\Chat;
use common\models\Contractor;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\product\Product;
use common\models\store\StoreCompanyContractor;
use frontend\models\Documents;
use php_rutils\RUtils;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\store\StoreUser;

/* @var $notifications common\models\notification\Notification[]
 * @var $newMessages Chat[]
 * @var $newMessagesIDs
 */

$userName = '';
$newProviders = [];

/* @var $user StoreUser */
if ($user = Yii::$app->user->identity) {
    $userName = $user->firstname;
    if ($storeCompany = $user->currentStoreCompany) {
        $userName = $storeCompany->getTitle(true);
        $newLinkArray = $storeCompany->getStoreCompanyContractors()->andWhere([
            'status' => StoreCompanyContractor::STATUS_ACTIVE,
            'notified' => false,
        ])->all();

        foreach ($newLinkArray as $link) {
            $link->updateAttributes(['notified' => true]);
            $newProviders[] = $link->contractor->company->getTitle(true);
        }
    }
    if ($user->company_id) {
        $userName = 'ООО "Ваш покупатель"';
    }
}
?>

<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="/">
                <img class="logo-default m-t-8" alt="logo"  src="/img/logo_pravka.png">
            </a>

            <div class="menu-toggler sidebar-toggler">
                <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
            </div>
        </div>
        <!-- END LOGO -->

        <div class="page-header-date">
            <?= \frontend\widgets\TimePickerWidget::widget(); ?>

            <div class="hidden-xs hidden-sm hidden-md" style="display: inline-block;">
                <?= RUtils::dt()->ruStrFTime([
                    'format' => ' d F Y, ',
                    'monthInflected' => true,
                ]); ?>
                <?= mb_strtoupper(RUtils::dt()->ruStrFTime(['format' => 'D',]), 'UTF8'); ?>
            </div>
        </div>

        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a data-target=".navbar-collapse" data-toggle="collapse"
           class="menu-toggler responsive-toggler" href="javascript:;"></a>
        <!-- END RESPONSIVE MENU TOGGLER -->

        <!-- BEGIN TOP NAVIGATION MENU -->
        <div class="top-menu">
            <ul class="nav navbar-nav pull-right">
                <?php if (!Yii::$app->user->isGuest) : ?>
                    <li class="dropdown dropdown-user">
                        <a data-close-others="true" data-hover="dropdown"
                           data-toggle="dropdown" class="dropdown-toggle"
                           href="#">
                            <span
                                class="username username-hide-on-mobile partially_hide_username"
                                title="<?= $userName ?>">
                                <?= $userName ?>
                            </span>
                            <i class="fa fa-angle-down" style="margin-right: -10px;"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <li>
                                <?= Html::a('<i class="icon-lock"></i> Ваш профиль', ['/profile/index']); ?>
                            </li>
                            <li>
                                <?= Html::a('<i class="icon-key"></i> Выход', ['/site/logout'], [
                                    'title' => 'Logout ' . (Yii::$app->user->identity ? ' (' . $userName . ')' : ''),
                                    'data-method' => 'post',
                                ]); ?>
                            </li>
                        </ul>
                    </li>

                    <li class="dropdown dropdown-extended dropdown-notification">
                        <?php echo yii\helpers\Html::a('<i class="icon-logout"></i>', Yii::$app->urlManager->createUrl('site/logout'), [
                            'class' => 'dropdown-toggle logout-btn',
                            'title' => 'Logout' . (Yii::$app->user->identity ? ' (' . $userName . ')' : ''),
                            'data-method' => 'post',
                        ]); ?>
                    </li>

                <?php else: ?>

                    <li class="dropdown dropdown-extended dropdown-notification">
                        <?php echo yii\helpers\Html::a('<i class="icon-login"></i>', ['/site/login'], [
                            'class' => 'dropdown-toggle logout-btn',
                            'title' => 'Login',
                        ]); ?>
                    </li>

                <?php endif; ?>
            </ul>
        </div>
        <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END HEADER INNER -->
</div>

<?= Html::hiddenInput(null, $newMessagesIDs, [
    'id' => 'exists-messages',
]); ?>

<div class="notification-message-template" style="display: none;">
    <a href="">
        <span class="details">
            <span class="time">
            </span>
            <span class="details">
                <span class="label label-sm label-icon label-info">
                    <i class="fa fa-bullhorn"></i>
                </span>
                <span class="details-title">
                </span>
            </span>
        </span>
    </a>
</div>

<?php if (!Yii::$app->user->isGuest) {
    $userId = Yii::$app->user->id;
    $this->registerJS(<<<JS
$(function () {
    $(document).on('mouseenter', '#header_notification_bar', function () {
    var notificationMenu = $(this);
        if (notificationMenu.find('.badge').length > 0) {
            $.post('/employee/view-notification', { id : $userId }, function() {
                notificationMenu.find(".badge").remove();
            });
        }
    });
    $(document).on('click', '#header_chat_bar > a', function () {
        location.href = $(this).attr('href');
    });
    $(document).on('mouseenter', '#header_chat_bar', function () {
    var notificationMenu = $(this);
        if (notificationMenu.find('.badge').length > 0) {
            $.post('/employee/view-messages', { id : $userId }, function() {
                notificationMenu.find(".badge").remove();
            });
        }
    });
});
JS
    );
} ?>

<?php if ($newProviders) : ?>
    <?php Modal::begin([
        'id' => 'new-providers-modal',
        'header' => '<h1>Новые поставщики</h1>',
    ]); ?>

        <?= implode('<br>', $newProviders) ?>

        <div style="margin-top: 20px; text-align: center;">
            <button class="btn darkblue text-white" data-dismiss="modal">Ok</button>
        </div>

    <?php Modal::end(); ?>

    <?php $this->registerJs('
        $(document).ready(function(){
            $("#new-providers-modal").modal("show");
        });
    '); ?>
<?php endif ?>