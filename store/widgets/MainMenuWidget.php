<?php

namespace store\widgets;

use common\models\Chat;
use frontend\components\NotificationHelper;
use yii\base\Widget;
use common\models\notification\Notification;
use Yii;
use common\models\employee\Employee;

class MainMenuWidget extends Widget
{
    public function run()
    {
        $notifications = [];
        $newMessages = [];
        $newMessagesIDs = null;

        return $this->render('mainMenuWidget', [
            'notifications' => $notifications,
            'newMessages' => $newMessages,
            'newMessagesIDs' => $newMessagesIDs,
        ]);
    }
}
