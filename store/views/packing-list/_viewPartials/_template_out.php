<?php

use common\components\TextHelper;
use common\models\product\Product;

/** @var \common\models\document\OrderPackingList $order */
/** @var [] $product */

$plus = 0;

?>
<?php if (isset($order)): ?>
    <?php
    if ($order->packingList->invoice->hasNds) {
        $priceNoNds = $order->priceNoNds;
        $amountNoNds = $order->amountNoNds;
        $amountWithNds = $order->amountWithNds;
        $TaxRateName = $order->order->saleTaxRate->name;
        $ndsAmount = TextHelper::invoiceMoneyFormat(($amountWithNds - $amountNoNds), $precision);
    } else {
        $priceNoNds = $order->priceWithNds;
        $amountNoNds = $order->amountWithNds;
        $amountWithNds = $order->amountWithNds;
        $TaxRateName = 'Без НДС';
        $ndsAmount = Product::DEFAULT_VALUE;
    }
    ?>
    <tr role="row" class="odd order">
        <td><span class="editable-field"><?= $key + 1; ?></span><span
                    class="icon-close input-editable-field delete-row hide"></span>
        </td>
        <td><?= $order->order ? $order->order->product_title : \common\components\helpers\Html::dropDownList('test', null, $result, ['class' => 'dropdownlist form-control', 'prompt' => '']); ?></td>
        <td><?= $order->product->productUnit ? $order->product->productUnit->name : \common\models\product\Product::DEFAULT_VALUE; ?></td>
        <td><?= $order->product->code; ?></td>
        <td><?= $order->product->productUnit ? $order->product->productUnit->code_okei : \common\models\product\Product::DEFAULT_VALUE; ?></td>
        <td><?= $order->order->box_type; ?></td>
        <td><?= $order->order->count_in_place; ?></td>
        <td><?= $order->order->place_count; ?></td>
        <td><?= $order->order->mass_gross; ?></td>
        <td><span
                    class="editable-field"><?= $order->quantity; ?></span><input
                    class="input-editable-field quantity hide  form-control" type="number"
                    value="<?= $order->quantity ?>" min="1"
                    max="<?= $order ? $order->getAvailableQuantity() : 1 ?>"
                    id="<?= $order->order_id ?>"
                    name="<?= $order ? 'OrderPackingList[' . $order->order_id . '][quantity]' : '' ?>"
                    style="padding: 6px 6px;">
        </td>
        <input class="status" type="hidden"
               name="<?= 'OrderPackingList[' . $order->order_id . '][status]' ?>"
               value="active">
        <td><?= TextHelper::invoiceMoneyFormat($priceNoNds, $precision); ?></td>
        <td><?= TextHelper::invoiceMoneyFormat($amountNoNds, $precision); ?></td>
        <td>
            <?= $TaxRateName ?>
        </td>
        <td>
            <?= $ndsAmount ?>
        </td>
        <td><?= TextHelper::invoiceMoneyFormat($amountWithNds, $precision); ?></td>
    </tr>
<?php else : ?>
    <tr role="row" class="odd order">
        <td><span class="editable-field"><?= $key + 1; ?></span><span
                    class="icon-close input-editable-field delete-row hide"></span>
        </td>
        <td><?=  \common\components\helpers\Html::dropDownList('test', null, $result, ['class' => 'dropdownlist form-control', 'prompt' => '']); ?></td>
        <td><?= Product::DEFAULT_VALUE; ?></td>
        <td></td>
        <td><?= Product::DEFAULT_VALUE; ?></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td> </td>
        <input class="status" type="hidden"
               name=""
               value="active">
        <td></td>
        <td</td>
        <td>
        </td>
        <td>
        </td>
        <td></td>
        <td class="text-right"></td>
    </tr>
<?php endif; ?>
