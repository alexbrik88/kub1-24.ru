<?php

use yii\helpers\Html;
use common\models\document\status;

/** @var \common\models\document\PackingList $model */
/* @var $useContractor boolean */
/* @var $contractorId integer|null */

?>
<div class="row action-buttons view-action-buttons">
    <div class="button-bottom-page-lg col-sm-1 col-xs-1" title="Отправить по e-mail">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?= Html::a('PDF', ['document-print', 'actionType' => 'pdf', 'id' => $model->id, 'filename' => $model->getPdfFileName(),], [
            'target' => '_blank',
            'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
        ]); ?>
        <?= Html::a('<i class="fa fa-file-pdf-o fa-2x"></i>', ['document-print', 'actionType' => 'pdf', 'id' => $model->id, 'filename' => $model->getPdfFileName(),], [
            'target' => '_blank',
            'class' => 'btn darkblue widthe-100 hidden-lg',
        ]); ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
</div>