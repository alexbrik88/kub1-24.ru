<?php
use common\components\date\DateHelper;

/** @var \common\models\document\PackingList $model */
/* @var $useContractor boolean */
/* @var $contractorId integer|null */

?>

<div class="col-lg-5 col-w-lg-4 col-md-5 control-panel margin991" style="float: right;">
    <div class="status-panel btn-group pull-right">
        <div class="btn darkblue no-margin-l box-status-act" title="Дата изменения статуса">
            <?= date("d.m.Y", $model->status_out_updated_at); ?>
        </div>
        <div class="btn btn-status darkblue" title="Статус">
            <i class="icon pull-left <?= $model->statusOut->getIcon(); ?>"></i>
            <?= $model->statusOut->name; ?>
        </div>
    </div>
    <div class="document-panel widthe-i box-width-100">
            <a href="<?= \yii\helpers\Url::toRoute(['invoice/view', 'type' => $model->type, 'id' => $model->invoice->id, 'contractorId' => $contractorId,]); ?>"
               class="btn btn-account yellow pull-right width179">
                <i class="icon pull-left <?= $model->invoice->invoiceStatus->getIcon(); ?>"></i>
                СЧЕТ
            </a>
    </div>
</div>
