<?php
use common\components\date\DateHelper;
use common\models\document\PackingList;
use common\models\product\Product;
use frontend\modules\documents\components\Message;
use kartik\select2\Select2;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\Pjax;

/* @var \yii\web\View $this */
/* @var PackingList $model */
/* @var $message Message */
/* @var string $dateFormatted */

$waybillDateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->waybill_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);
$proxyDateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->proxy_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);
$basisDocumentDate = \php_rutils\RUtils::dt()->ruStrFTime([
    'date' => $model->basis_document_date,
    'format' => 'd F Y г.',
    'monthInflected' => true,
]);
$productionType = explode(', ', $model->invoice->production_type);
$agreementArray = $model->invoice->contractor->getAgreements()->orderBy(['document_date' => SORT_DESC])->all();
$invoiceItemValue = "Счет&{$model->invoice->fullNumber}&{$model->invoice->document_date}&";
$invoiceItemName = 'Счет № ' . $model->invoice->fullNumber . ' от ' .
    DateHelper::format($model->invoice->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE);
$agreementDropDownList = ['add-modal-agreement' => '[ + Добавить договор ]'] +
    [$invoiceItemValue => $invoiceItemName] + ArrayHelper::map($agreementArray, 'listItemValue', 'listItemName');
$jsUrl = Url::to(['add-stamp', 'id' => $model->id]);
$this->registerJs('
    $(document).on("change", "#packinglist-add_stamp", function() {
        $.post("' . $jsUrl . '", $(this).serialize(), function(data) {
            console.log(data);
        });
    });
');
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-hover',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized'],
        'trigger' => 'hover',
        'contentAsHTML' => true,
    ],
]);
?>

<div class="portlet customer-info">
    <div class="portlet-title">
        <div class="col-md-9 caption">
            <?= $message->get(Message::TITLE_SHORT_SINGLE); ?>
            № <span class="editable-field"><?= $model->fullNumber; ?></span>

            <?= Html::activeTextInput($model, 'document_number', [
                'id' => 'account-number',
                'data-required' => 1,
                'class' => 'form-control input-editable-field hide',
                'style' => 'max-width: 60px; display:inline-block;',
                'value' => $model->getDocumentNumber(),
            ]); ?>

            <?= Html::activeTextInput($model, 'document_additional_number', [
                'maxlength' => true,
                'class' => 'form-control  input-editable-field hide',
                'placeholder' => 'доп. номер',
                'style' => 'max-width: 110px; display:inline-block;',
            ]); ?>

            от <span class="editable-field"><?= $dateFormatted; ?></span>

            <div class="input-icon input-calendar input-editable-field hide margin-top-15">
                <i class="fa fa-calendar"></i>
                <?= Html::activeTextInput($model, 'document_date', [
                    'class' => 'form-control date-picker',
                    'data-date-viewmode' => 'years',
                    'value' => DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                ]); ?>
            </div>
        </div>
    </div>
    <div class="portlet-body no_mrg_bottom">
        <table class="table no_mrg_bottom">
            <tr class="box-tr-pac-list">
                <td style="line-height: 32px;">
                    <span class="box-pac-list-span">Транспортная накладная №</span>
                    <span
                        class="editable-field"><?= $model->waybill_number ? $model->waybill_number : '---'; ?></span>
                    <?= Html::activeTextInput($model, 'waybill_number', [
                        'maxlength' => true,
                        'id' => 'account-number',
                        'data-required' => 1,
                        'class' => 'form-control  input-editable-field hide',
                        'placeholder' => 'номер',
                        'style' => 'width:200px',
                    ]); ?>
                    от
                    <span class="editable-field"><?= $model->waybill_date ? $waybillDateFormatted : '---'; ?></span>

                    <div
                        class="input-icon input-calendar input-editable-field hide">
                        <i class="fa fa-calendar"></i>
                        <?= Html::activeTextInput($model, 'waybill_date', [
                            'id' => 'under-date',
                            'class' => 'form-control date-picker',
                            'size' => 16,
                            'data-date-viewmode' => 'years',
                            'value' => DateHelper::format($model->waybill_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                        ]); ?>
                    </div>
                </td>
            </tr>
        </table>
        <table class="table no_mrg_bottom" style="max-width: 548px;">
            <tr>
                <td style="line-height: 32px; width: 1%;">
                    Основание:
                </td>
                <td style="line-height: 32px; padding-left: 0;">
                    <div class="editable-field">
                        <span class="editable-field">
                            <?= $model->agreementType ? $model->agreementType->name : ($model->basis_name == 'Счет' ? 'Счет' : '---') ?>
                        </span>
                        №
                        <span class="editable-field"><?= $model->basis_document_number ? $model->basis_document_number : '---'; ?></span>
                        от
                        <span class="editable-field"><?= $model->basis_document_date ? $basisDocumentDate : '---'; ?></span>
                    </div>
                    <div class="input-editable-field hide" style="max-width: 450px;">
                        <?php Pjax::begin([
                            'id' => 'agreement-pjax-container',
                            'enablePushState' => false,
                            'linkSelector' => false,
                        ]);
                        echo Select2::widget([
                            'model' => $model,
                            'attribute' => 'agreement',
                            'data' => $agreementDropDownList,
                            'pluginOptions' => [
                                'allowClear' => false,
                                'placeholder' => '',
                            ],
                        ]);
                        Pjax::end(); ?>
                    </div>
                </td>
            </tr>
        </table>

        <table class="table no_mrg_bottom" style="max-width: 548px;">
            <tr>
                <td style="line-height: 32px; width: 1%;">
                    Грузоотправитель:
                </td>
                <td style="line-height: 32px; padding-left: 0;">
                    <div class="editable-field">
                        <?= $model->consignor ? $model->consignor->getTitle(true) : 'Он же'; ?>
                    </div>
                    <div class="input-editable-field hide" style="max-width: 450px;">
                        <?= Select2::widget([
                            'model' => $model,
                            'attribute' => 'consignor_id',
                            'data' => $model->consignorArray,
                        ]); ?>
                    </div>
                </td>
            </tr>
        </table>

        <table class="table no_mrg_bottom">
            <tr class="box-tr-pac-list">
                <td style="line-height: 32px;">
                    <span class="box-pac-list-span">Доверенность: №</span>
                    <span class="editable-field"><?= $model->proxy_number ? $model->proxy_number : '---'; ?></span>
                    <?= Html::activeTextInput($model, 'proxy_number', [
                        'maxlength' => true,
                        'class' => 'form-control  input-editable-field hide',
                        'placeholder' => 'номер',
                        'style' => 'width:200px; display: inline-block;',
                    ]); ?>
                    от
                    <span class="editable-field"><?= $model->proxy_date ? $proxyDateFormatted : '---'; ?></span>

                    <div
                        class="input-icon input-calendar input-editable-field hide">
                        <i class="fa fa-calendar"></i>
                        <?= Html::activeTextInput($model, 'proxy_date', [
                            'id' => 'under-date',
                            'class' => 'form-control date-picker',
                            'size' => 16,
                            'data-date-viewmode' => 'years',
                            'value' => DateHelper::format($model->proxy_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                        ]); ?>
                    </div>
                </td>
            </tr>
        </table>

        <table class="table no_mrg_bottom">
            <tr>
                <td style="line-height: 32px;">
                    <span
                        class="customer-characteristic"><?= $message->get(Message::CONTRACTOR); ?>
                        :</span>
                    <span><?= $model->invoice->contractor_name_short; ?></span>
                </td>
            </tr>
        </table>

        <table class="table no_mrg_bottom" style="max-width: 548px;">
            <tr>
                <td style="line-height: 32px; width: 1%;">
                    Грузополучатель:
                </td>
                <td style="line-height: 32px; padding-left: 0;">
                    <div class="editable-field">
                        <?= $model->consignee ? $model->consignee->getTitle(true) : 'Он же'; ?>
                    </div>
                    <div class="input-editable-field hide" style="max-width: 450px;">
                        <?= Select2::widget([
                            'model' => $model,
                            'attribute' => 'consignee_id',
                            'data' => $model->consignorArray,
                        ]); ?>
                    </div>
                </td>
            </tr>
        </table>
    </div>
</div>
<div class="tooltip_templates" style="display: none;">
    <span id="tooltip_add_stamp" style="display: inline-block; text-align: center;">
        Добавить в ТН печать и подпись
        <br>
        при отправке по e-mail и
        <br>
        при скачивании в PDF
    </span>
</div>