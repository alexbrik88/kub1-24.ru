<?php
use common\components\date\DateHelper;
use common\models\document\PackingList;
use frontend\modules\documents\components\Message;
use yii\bootstrap\Html;
use yii\helpers\Url;
use common\models\product\Product;
/* @var \yii\web\View $this */
/* @var PackingList $model */
/* @var $message Message */
/* @var string $dateFormatted */

$productionType = explode(', ', $model->invoice->production_type);
?>

<div class="portlet customer-info">
    <div class="portlet-title">
        <div class="col-md-9 caption">
            <?= $message->get(Message::TITLE_SHORT_SINGLE); ?>
            №

            <span class="editable-field"><?= $model->getDocumentNumber(); ?></span>
            <?= Html::activeTextInput($model, 'document_number', [
                'maxlength' => true,
                'class' => 'form-control  input-editable-field hide',
                'placeholder' => 'доп. номер',
                'style' => 'max-width: 110px; display:inline-block;',
            ]); ?>

            от <span class="editable-field"><?= $dateFormatted; ?></span>
            <div class="input-icon input-calendar input-editable-field hide">
                <i class="fa fa-calendar"></i>
                <?= Html::activeTextInput($model, 'document_date', [
                    'class' => 'form-control date-picker',
                    'data-date-viewmode' => 'years',
                    'value' => DateHelper::format($model->document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                ]); ?>
            </div>
        </div>

        <div class="actions">
            <?= \frontend\modules\documents\widgets\CreatedByWidget::widget([
                'createdAt' => DateHelper::format($model->created_at, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATETIME),
                'author' => $model->documentAuthor->fio,
            ]); ?>

            <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::UPDATE, ['model' => $model,])) : ?>
                <button type="submit" class="btn-save btn darkblue btn-sm hide" style="color: #FFF;"
                   title="Сохранить">
                    <span class="ico-Save-smart-pls"></span></button>
                <a href="#" class="btn-cancel btn darkblue btn-sm hide" style="color: #FFF;"
                   title="Отменить">
                    <span class="ico-Cancel-smart-pls"></span></a>
                <a href="#" class="edit edit-in btn darkblue btn-sm" title="Редактировать"><i class="icon-pencil"></i></a>
            <?php endif; ?>
        </div>
    </div>
    <div class="portlet-body no_mrg_bottom">
        <table class="table no_mrg_bottom">
            <tr>
                <td>
                    <span class="customer-characteristic"><?= $message->get(Message::CONTRACTOR); ?>:</span>
                    <span><?= Html::a($model->invoice->contractor_name_short, [
                        '/contractor/view',
                        'type' => $model->invoice->contractor->type,
                        'id' => $model->invoice->contractor->id,
                    ]) ?></span>
                </td>
            </tr>

            <tr>
                <td>
                    <?= \common\models\file\widgets\FileUpload::widget([
                        'uploadUrl' => Url::to(['file-upload', 'type' => $model->type, 'id' => $model->id,]),
                        'deleteUrl' => Url::to(['file-delete', 'type' => $model->type, 'id' => $model->id,]),
                        'listUrl' => Url::to(['file-list', 'type' => $model->type, 'id' => $model->id,]),
                    ]); ?>
                </td>
            </tr>
        </table>
    </div>
</div>
