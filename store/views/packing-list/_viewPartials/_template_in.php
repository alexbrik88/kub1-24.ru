<?php
/**
 * Created by PhpStorm.
 * User: hp-m6
 * Date: 20.01.2017
 * Time: 3:12
 */
use common\components\TextHelper;
use common\models\document\Invoice;
use common\models\product\Product;

/* @var \common\models\document\PackingList $model */

$plus = 0;
?>
<?php if (isset($order)): ?>
<tr role="row" class="odd order">
    <td><span class="editable-field"><?= $key + 1; ?></span><span
            class="icon-close input-editable-field hide delete-row"></span>
    </td>
    <td><?= $order->order ? $order->order->product_title : \common\components\helpers\Html::dropDownList('test',null,$result, ['class'=>'dropdownlist form-control', 'prompt'=>'']); ?></td>
    <td><span
            class="editable-field"><?= $order->quantity; ?></span><input
            class="input-editable-field quantity hide form-control" type="number"
            value="<?= $order->quantity ?>" min="1"
            max="<?= $order ? $order->getAvailableQuantity() : 1?>"
            id="<?= $order->order_id ?>"
            name="<?= $order ? 'OrderPackingList[' . $order->order_id . '][quantity]' : ''?>"
            style="padding: 6px 6px;">
    </td>
    <td><?= $order->product->productUnit ? $order->product->productUnit->name : Product::DEFAULT_VALUE; ?></td>
    <?php if ($order->packingList->invoice->nds_view_type_id != Invoice::NDS_VIEW_WITHOUT) : ?>
        <td><?= $order->order->purchaseTaxRate->name; ?></td>
    <?php endif; ?>
    <input class="status" type="hidden" name="<?= 'OrderPackingList['. $order->order_id  .'][status]' ?>" value="active">
    <?php if ($order->packingList->invoice->nds_view_type_id == Invoice::NDS_VIEW_OUT) : ?>
        <td class="text-right"><?= TextHelper::invoiceMoneyFormat($order->priceNoNds, $precision); ?></td>
        <td class="text-right"><?= TextHelper::invoiceMoneyFormat($order->amountNoNds, $precision); ?></td>
    <?php else : ?>
        <td class="text-right"><?= TextHelper::invoiceMoneyFormat($order->priceWithNds, $precision); ?></td>
        <td class="text-right"><?= TextHelper::invoiceMoneyFormat($order->amountWithNds, $precision); ?></td>
    <?php endif; ?>
</tr>
<?php else : ?>
<tr role="row" class="odd order">
    <td><span class="editable-field"><?= $key + 1; ?></span><span
                class="icon-close input-editable-field hide delete-row"></span>
    </td>
    <td><?= \common\components\helpers\Html::dropDownList('test',null,$result, ['class'=>'dropdownlist form-control', 'prompt'=>'']); ?></td>
    <td></td>
    <td></td>
    <?php if (isset($invoice) && $invoice->nds_view_type_id != Invoice::NDS_VIEW_WITHOUT) : ?>
        <td width="5%"></td>
    <?php endif; ?>
    <input class="status" type="hidden" name="" value="active">
    <td class="text-right"></td>
    <td class="text-right"></td>
</tr>
<?php endif; ?>