<?php
use yii\helpers\Html;
use yii\helpers\Url;

/** @var \common\models\document\PackingList $model */
/* @var $useContractor boolean */
/* @var $contractorId integer|null */
?>

<div class="row action-buttons form-action-buttons hide">
    <div class="spinner-button col-sm-1 col-xs-1">
        <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
            'class' => 'form-action-button btn darkblue btn-save darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
            'form' => 'edit-packing-list',
            'data-style' => 'expand-right',
        ]); ?>
        <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
            'class' => 'form-action-button btn darkblue btn-save darkblue widthe-100 hidden-lg',
            'title' => 'Сохранить',
            'form' => 'edit-packing-list',
        ]); ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <button type="button" class="form-action-button btn darkblue btn-cancel darkblue widthe-100 hidden-md hidden-sm hidden-xs">Отменить</button>
        <button type="button" class="form-action-button btn darkblue btn-cancel darkblue widthe-100 hidden-lg" title="Отменить"><i class="fa fa-reply fa-2x"></i></button>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
</div>

<div class="row action-buttons buttons-fixed view-action-buttons" id="buttons-fixed">
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <a href="<?= Url::to(['original', 'id' => $model->id, 'type' => $model->type, 'val' => 0]) ?>" class="is-original-button btn darkblue widthe-100 hidden-md hidden-sm hidden-xs">Копия</a>
        <a href="<?= Url::to(['original', 'id' => $model->id, 'type' => $model->type, 'val' => 0]) ?>" class="is-original-button btn darkblue widthe-100 hidden-lg" title="Копия"><span class="icon icon-doc"></span></a>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <a href="<?= Url::to(['original', 'id' => $model->id, 'type' => $model->type, 'val' => 1]) ?>" class="is-original-button btn darkblue widthe-100 hidden-md hidden-sm hidden-xs">Оригинал</a>
        <a href="<?= Url::to(['original', 'id' => $model->id, 'type' => $model->type, 'val' => 1]) ?>" class="is-original-button btn darkblue widthe-100 hidden-lg" title="Оригинал"><span class="icon icon-doc"></span></a>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Document::DELETE)): ?>
            <button type="button" class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs" data-toggle="modal" href="#delete-confirm">Удалить</button>
            <button type="button" class="btn darkblue widthe-100 hidden-lg" data-toggle="modal" href="#delete-confirm" title="Удалить"><i class="fa fa-trash-o fa-2x"></i></button>
        <?php endif; ?>
    </div>
</div>
