<?php

use common\components\grid\GridView;
use common\components\grid\DropDownSearchDataColumn;
use common\components\TextHelper;
use common\models\product\Product;
use common\models\product\ProductGroup;
use common\models\product\ProductUnit;
use frontend\rbac\permissions;
use yii\bootstrap\Dropdown;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use common\models\document\status\OrderDocumentStatus;
use common\models\product\Store;
use common\models\store\StoreUser;
use common\models\Company;
use common\components\ImageHelper;

/* @var $this yii\web\View */
/* @var $company \common\models\Company */
/* @var $dataProvider yii\data\ActiveDataProvider */
/* @var $searchModel \common\models\product\ProductSearch */
/* @var $productionType int */
/* @var $user StoreUser */

//$this->title = Product::$productionTypes[$productionType] . ' ' . $company->getTitle(true) . ' для продажи';
$this->title = 'Прайс-лист';
$tableHeader = [
    Product::PRODUCTION_TYPE_GOODS => 'Список товаров',
    Product::PRODUCTION_TYPE_SERVICE => 'Список услуг',
];
$this->context->layoutWrapperCssClass = 'page-good';
if ($productionType == 1) {
    $emptyMessage = 'Нет доступных товаров';
} elseif ($productionType == 0) {
    $emptyMessage = 'Нет доступных услуг';
} else {
    $emptyMessage = 'Ничего не найдено';
}
$user = isset($user) ? $user : null;
$productUnits = ProductUnit::find()->select(['name', 'id'])->andWhere(['goods' => true])->orderBy([
    'IF([[id]] = 1, 0, 1)' => SORT_ASC,
    'name' => SORT_ASC,
])->indexBy('id')->column();

$selectedProduct = [];
if (!Yii::$app->request->isAjax && ($productCount = Yii::$app->request->get('count'))) {
    $result = $searchModel->baseQuery()->select([
        'prod.id',
        'prod.price_for_sell_with_nds',
        'prod.weight',
        'quantity' => 'GREATEST(IFNULL({{s}}.[[total]], 0) - IFNULL({{r}}.[[total]], 0), 0)',
    ])->andWhere(['prod.id' => array_keys($productCount)])->asArray()->all();
    foreach ($result as $product) {
        $count = $productCount[$product['id']];
        $max = $product['quantity'] * 1;
        $selectedProduct[$product['id']] = [
            'id' => $product['id'],
            'count' => $count === '' ? $count : max(0, min($count, $max)),
            'price' => $product['price_for_sell_with_nds'] / 100,
            'weight' => $product['weight'] * 1,
            'max' => $max,
        ];
    }
    $selectedData = json_encode($selectedProduct);
    $this->registerJs("
        PREORDER.selectedData = {$selectedData};
        PREORDER.recalculate();
    ");
}
?>
<div class="company-pricelist">
    <div class="portlet box">
        <div class="btn-group pull-right title-buttons">
            <?= Html::beginForm(['order/create'], 'post', ['id' => 'product_checker_form']); ?>
            <div class="inputs-selected"></div>
            <?= Html::endForm(); ?>
            <?= Html::a('СОЗДАТЬ ЗАКАЗ', null, [
                'class' => 'btn yellow add-to-order',
                'data-tooltip-content' => '#add-to-order-tooltip',
                'disabled' => true,
                'style' => 'margin-left: 15px;width: 150px;',
            ]); ?>
        </div>
        <h3 class="page-title">
            <?= Html::encode($this->title); ?>
        </h3>
    </div>
    <div class="clearfix"></div>
    <div>
        <div class="pull-right" style="display: inline-block;width: 72%;">
            <?php if ($company->store_show_novelty_button): ?>
                <div class="col-md-12 p-r-0">
                    <?= Html::a($searchModel->new ? 'Весь товар' : 'Новинки', Url::current([
                        $searchModel->formName() => ['new' => $searchModel->new ? null : 1]
                    ]), [
                        'class' => 'btn text-white',
                        'style' => 'margin: 0 0 15px 15px; width: 150px; background-color: #e35b5a; float:right;',
                        'title' => $searchModel->new ? 'Показать все товары' : 'Выбрать недавно добавленные'
                    ]); ?>
                </div>
            <?php endif; ?>
            <div style="display: none;">
                <span id="add-to-order-tooltip">
                    Выберите
                    <?= $productionType ? 'товар' : 'услугу'; ?>
                    для создания заказа
                </span>
            </div>
            <?php if ($company->store_has_discount): ?>
                <div class="discount-block-product col-md-9"
                     style="width: auto;<?= $company->store_show_novelty_button ? 'margin-top: 22px;' : null; ?>">
                    <div class="mt-element-ribbon bg-grey-steel">
                        <div class="ribbon ribbon-border-hor ribbon-clip ribbon-color-danger uppercase">
                            <div class="ribbon-sub ribbon-clip"></div>
                            Акция
                        </div>
                        <p class="ribbon-content">
                            При заказе на сумму больше <?= $company->store_discount_from_amount; ?> рублей,
                            <?= $company->store_discount_type == Company::DISCOUNT_TYPE_ANOTHER ?
                                $company->store_discount_another :
                                'скидка ' . intval($company->store_discount) . '%'; ?>
                        </p>
                    </div>
                </div>
            <?php endif; ?>
        </div>
        <table id="order_summary" class="table table-striped table-bordered" style="width: auto;">
            <thead>
            <tr class="heading">
                <th>Информация по заказу</th>
                <th style="width: 100px; text-align: center;">Всего</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>Сумма, <i class="fa fa-rub"></i></td>
                <td class="total_price" style="text-align: right;">0.00</td>
            </tr>
            <tr>
                <td>Наименований</td>
                <td class="total_items" style="text-align: right;">0</td>
            </tr>
            <tr>
                <td>Штук (в россыпи)</td>
                <td class="total_count" style="text-align: right;">0</td>
            </tr>
            <tr>
                <td>Общим весом, кг.</td>
                <td class="total_weight" style="text-align: right;">0</td>
            </tr>
            </tbody>
        </table>
    </div>
    <?php \yii\widgets\Pjax::begin([
        'id' => 'pricelist-pjax-container',
        'linkSelector' => false,
        'timeout' => 10000,
    ]); ?>
    <div class="portlet box darkblue">
        <div class="portlet-title">
            <div class="caption caption_for_input">
                <?= $tableHeader[$productionType] . ': ' . $dataProvider->totalCount; ?>
            </div>
            <div class="tools search-tools tools_button col-sm-6">
                <div class="form-body">
                    <?php $form = ActiveForm::begin([
                        'id' => 'product-search-form',
                        'method' => 'GET',
                        'action' => Url::current([$searchModel->formName() => null]),
                        'fieldConfig' => [
                            'template' => "{input}\n{error}",
                            'options' => [
                                'class' => '',
                            ],
                        ],
                    ]); ?>
                    <div class="inputs-selected"></div>
                    <div class="search_cont">
                        <div class="wimax_input ">
                            <?= $form->field($searchModel, 'title')->textInput([
                                'placeholder' => 'Поиск по наименованию и группе товара',
                            ]); ?>
                        </div>
                        <div class="wimax_button">
                            <?= Html::submitButton('НАЙТИ', [
                                'class' => 'btn btn__ins btn-sm default btn_marg_down green-haze',
                            ]) ?>
                        </div>
                    </div>
                    <?php $form->end(); ?>
                </div>
            </div>
        </div>
        <div class="portlet-body accounts-list">
            <div class="table-container products-table clearfix" style="">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'emptyText' => $emptyMessage,
                    'formatter' => ['class' => 'yii\i18n\Formatter', 'nullDisplay' => '---'],
                    'tableOptions' => [
                        'class' => 'table table-striped table-bordered table-hover dataTable customers_table fix-thead',
                        'id' => 'datatable_ajax',
                        'aria-describedby' => 'datatable_ajax_info',
                        'role' => 'grid',
                    ],
                    'headerRowOptions' => [
                        'class' => 'heading',
                    ],
                    'options' => [
                        'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                    ],
                    'rowOptions' => [
                        'role' => 'row',
                    ],
                    'pager' => [
                        'options' => [
                            'class' => 'pagination pull-right',
                        ],
                    ],
                    'layout' => $this->render('@frontend/views/layouts/grid/layout', ['totalCount' => $dataProvider->totalCount]),
                    'columns' => [
                        [
                            'class' => 'yii\grid\CheckboxColumn',
                            'cssClass' => 'product_checker',
                            'checkboxOptions' => function ($model, $key, $index, $column) {
                                return ['value' => $model->id, 'checked' => isset($_GET['count'][$model->id])];
                            },
                            'headerOptions' => [
                                'style' => 'width: 20px;',
                            ],
                            'contentOptions' => [
                                'style' => 'vertical-align: middle;',
                            ],
                        ],
                        [
                            'label' => 'Заказ (шт)',
                            'headerOptions' => [
                                'style' => 'width: 10px;',
                            ],
                            'contentOptions' => [
                                'style' => 'vertical-align: middle;',
                            ],
                            'format' => 'raw',
                            'value' => function ($data) {
                                $value = isset($_GET['count'][$data->id]) ? $_GET['count'][$data->id] : '';

                                return Html::input('number', "count[{$data->id}]", $value, [
                                    'class' => 'product-count form-control',
                                    'style' => 'width: 60px; padding: 5px 0 5px 10px;',
                                    'data' => [
                                        'id' => $data->id,
                                        'count' => $value,
                                        'price' => $data->price_for_sell_with_nds / 100,
                                        'weight' => $data->weight * 1,
                                    ],
                                    'min' => 0,
                                    'max' => $data->quantity * 1,
                                    'step' => 'any',
                                ]);
                            }
                        ],
                        [
                            'attribute' => 'title',
                            'headerOptions' => [
                                'class' => 'sorting',
                                'width' => '25%',
                            ],
                            'contentOptions' => [
                                'style' => 'vertical-align: middle;',
                            ],
                            'format' => 'raw',
                            'value' => function (Product $data) use ($productionType) {
                                return Html::a($data->title, Url::toRoute(['view', 'productionType' => $productionType, 'id' => $data->id]));
                            },
                        ],
                        [
                            'attribute' => 'group_id',
                            'class' => DropDownSearchDataColumn::className(),
                            'label' => 'Группа товара',
                            'headerOptions' => [
                                'class' => 'dropdown-filter',
                                'width' => '10%',
                            ],
                            'contentOptions' => [
                                'style' => 'vertical-align: middle;',
                            ],
                            'filter' => ArrayHelper::merge([null => 'Все'], ArrayHelper::map(ProductGroup::getGroups(), 'id', 'title')),
                            'format' => 'raw',
                            'value' => 'group.title',
                            'visible' => $productionType == Product::PRODUCTION_TYPE_GOODS,
                        ],
                        [
                            'label' => 'Фото',
                            'headerOptions' => [
                                'width' => '30px',
                            ],
                            'contentOptions' => [
                                'style' => 'vertical-align: middle;',
                            ],
                            'format' => 'raw',
                            'visible' => $productionType == Product::PRODUCTION_TYPE_GOODS,
                            'value' => function (Product $data) {
                                $content = '';
                                if ($thumb = $data->getImageThumb(200, 300)) {
                                    $tooltipId = 'tooltip-product-image-' . $data->id;
                                    $content = Html::tag('span', '', [
                                        'class' => 'preview-product-photo pull-center icon icon-paper-clip',
                                        'data-tooltip-content' => '#' . $tooltipId,
                                        'style' => 'cursor: pointer;',
                                    ]);
                                    $content .= Html::beginTag('div', ['class' => 'hidden', 'style' => 'display: inline-block;']);
                                    $content .= Html::beginTag('div', ['id' => $tooltipId]);
                                    $content .= Html::img($thumb, ['alt' => '']);
                                    $content .= Html::endTag('div');
                                    $content .= Html::endTag('div');
                                }

                                return $content;
                            },
                        ],
                        [
                            'attribute' => 'comment_photo',
                            'label' => 'Описание',
                            'headerOptions' => [
                                'width' => '25%',
                            ],
                            'contentOptions' => [
                                'style' => 'vertical-align: middle;',
                            ],
                            'format' => 'raw',
                            'visible' => $productionType == Product::PRODUCTION_TYPE_GOODS,
                            'value' => function ($data) {
                                $content = '';
                                if ($data->comment_photo) {
                                    $tooltipId = 'tooltip-product-comment-' . $data->id;
                                    $content .= Html::tag('div', Html::tag('div', Html::encode($data->comment_photo)), [
                                        'class' => 'product-comment-box',
                                        'data-tooltip-content' => '#' . $tooltipId,
                                        'style' => 'cursor: pointer;'
                                    ]);
                                    $content .= Html::beginTag('div', ['class' => 'hidden', 'style' => 'display: inline-block;']);
                                    $content .= Html::beginTag('div', ['id' => $tooltipId, 'style' => 'max-width: 300px;']);
                                    $content .= Html::encode($data->comment_photo);
                                    $content .= Html::endTag('div');
                                    $content .= Html::endTag('div');
                                }

                                return $content;
                            },
                        ],
                        [
                            'class' => DropDownSearchDataColumn::className(),
                            'attribute' => 'product_unit_id',
                            'label' => 'Ед. измерения',
                            'filter' => [null => 'Все'] + $productUnits,
                            'headerOptions' => [
                                'width' => '10%',
                            ],
                            'contentOptions' => [
                                'style' => 'vertical-align: middle;',
                            ],
                            'format' => 'raw',
                            'value' => 'productUnit.name',
                        ],
                        [
                            'attribute' => 'quantity',
                            'label' => 'Доступно',
                            'headerOptions' => [
                                'class' => 'sorting',
                                'width' => '10%',
                            ],
                            'contentOptions' => [
                                'style' => 'vertical-align: middle;',
                            ],
                            'format' => 'raw',
                            'value' => function ($data) use ($user, $company) {
                                $quantity = $data->quantity * 1;
                                if ($quantity == 0 && $user instanceof StoreUser &&
                                    $company->store_where_empty_products_type == Company::WHERE_EMPTY_PRODUCTS_TYPE_TEXT &&
                                    $company->store_where_empty_products_text_type !== null
                                ) {
                                    return Company::$whereEmptyProductsTextType[$company->store_where_empty_products_text_type];
                                }

                                return $data->quantity * 1;
                            },
                            'visible' => $productionType == Product::PRODUCTION_TYPE_GOODS,
                        ],
                        [
                            'attribute' => 'price_for_sell_with_nds',
                            'label' => 'Цена продажи',
                            'headerOptions' => [
                                'class' => 'sorting',
                                'width' => '10%',
                            ],
                            'contentOptions' => [
                                'style' => 'vertical-align: middle;',
                            ],
                            'format' => 'raw',
                            'value' => function ($data) {
                                return TextHelper::invoiceMoneyFormat($data->price_for_sell_with_nds, 2);
                            },
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
    <?php \yii\widgets\Pjax::end(); ?>
</div>
