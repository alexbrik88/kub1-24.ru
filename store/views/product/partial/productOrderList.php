<?php
use common\components\TextHelper;
use common\models\product\Product;
use yii\grid\GridView;
use yii\helpers\Html;
use yii\widgets\Pjax;

$productIdArray = Yii::$app->request->post('product', []);
?>

<?php Pjax::begin([
    'id' => 'pjax-product-grid',
    'linkSelector' => false,
    'timeout' => 10000,
    'enablePushState' => false,
    'enableReplaceState' => false,
]); ?>

<div class="modal-header" id="for-product-type">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
    <?php if ($productType == 1 ) : ?>
        <?php $emptyText = 'Все товары уже добавлены'; ?>
        <h1>Выбрать товар из списка</h1>
    <?php else : ?>
        <?php $emptyText = 'Все услуги уже добавлены'; ?>
        <h1>Выбрать услугу из списка</h1>
    <?php endif; ?>
</div>
<div class="modal-body">

    <?= Html::beginForm(['/product/get-products'], 'get', [
        'id' => 'products_in_order',
        'class' => 'add-to-invoice',
        'data' => [
            'pjax' => true,
        ]
    ]); ?>

    <?= Html::hiddenInput('documentType', $documentType, [
        'id' => 'documentTypeHidden',
    ]); ?>

    <?=  Html::hiddenInput('productType', $productType, [
        'id' => 'productTypeHidden',
    ]); ?>

    <?=  Html::hiddenInput('searchTitle', $searchModel->title, [
        'id' => 'searchTitleHidden',
    ]); ?>

    <div class="form-body">
        <div class="portlet m-b">
            <div class="row search-form-default">
                <div class="col-md-12">
                    <div class="input-group">
                        <div class="input-cont">
                            <?= Html::textInput('title', $searchModel->title, [
                                'placeholder' => 'Поиск...',
                                'class' => 'form-control',
                                'id' => 'product-title-search',
                            ]) ?>
                        </div>
                        <span class="input-group-btn">
                            <?= Html::submitButton('Найти &nbsp;<i class="m-icon-swapright m-icon-white"></i>', [
                                'class' => 'btn green-haze',
                            ]); ?>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="portlet m-b add-to-invoice-table" id="add-to-invoice-tbody">
            <div class="portlet-body accounts-list">
                <div class="table-container" style="">
                    <?= GridView::widget([
                        'dataProvider' => $dataProvider,
                        'tableOptions' => [
                            'class' => 'table table-striped table-bordered table-hover dataTable customers_table overfl_text_hid',
                            'id' => 'datatable_ajax',
                            'aria-describedby' => 'datatable_ajax_info',
                            'role' => 'grid',
                        ],
                        'headerRowOptions' => [
                            'class' => 'heading',
                        ],
                        'options' => [
                            'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                        ],
                        'rowOptions' => [
                            'role' => 'row',
                        ],
                        'pager' => [
                            'options' => [
                                'class' => 'pagination pull-right m-b-0',
                            ],
                        ],
                        'layout' => "{items}\n{pager}",
                        'emptyText' => $searchModel->exclude ? $emptyText : 'Ничего не найдено.',
                        'columns' => [
                            [
                                'format' => 'raw',
                                'value' => function (Product $model) use ($productIdArray) {
                                    return Html::checkbox('in_order[]', in_array($model->id, $productIdArray), [
                                        'id' => 'product-' . $model->id,
                                        'value' => $model->id,
                                        'class' =>'product_selected',
                                    ]);
                                },
                                'headerOptions' => [
                                    'class' => 'w30pr',
                                ],
                                'header' => Html::checkbox('in_order_all', false, [
                                    'id' => 'product_selected-all',
                                ]),
                            ],
                            [
                                'attribute' => 'title',
                                'label' => 'Продукция',
                                'headerOptions' => [
                                    'class' => 'sorting w115px',
                                ],
                                'format' => 'text',
                            ],
                            //Группа
                            [
                                'attribute' => 'group_id',
                                'label' => 'Группа',
                                'headerOptions' => [
                                    'class' => 'sorting w105px',
                                ],
                                'format' => 'raw',
                                'value' => 'group.title',
                                'visible' => ($productType == Product::PRODUCTION_TYPE_SERVICE ? false : true),
                            ],
                            //Количество на складе
                            [
                                'attribute' => 'quantity',
                                'label' => 'Количество на складе',
                                'headerOptions' => [
                                    'class' => 'sorting w115px',
                                ],
                                'format' => 'raw',
                                'value' => function ($data) {
                                    return $data->quantity * 1;
                                },
                                'visible' => ($productType == Product::PRODUCTION_TYPE_SERVICE ? false : true),
                            ],
                            [
                                'attribute' => 'price_for_sell_with_nds',
                                'label' => 'Цена продажи',
                                'format' => 'raw',
                                'headerOptions' => [
                                    'class' => 'sorting w105px',
                                ],
                                'value' => function (Product $model) {
                                    return TextHelper::invoiceMoneyFormat($model->price_for_sell_with_nds);
                                },
                                'visible' => $documentType == \frontend\models\Documents::IO_TYPE_OUT,
                            ],
                            [
                                'attribute' => 'price_for_buy_with_nds',
                                'label' => 'Цена покупки',
                                'format' => 'raw',
                                'headerOptions' => [
                                    'class' => 'sorting w105px',
                                ],
                                'value' => function (Product $model) {
                                    return TextHelper::invoiceMoneyFormat($model->price_for_buy_with_nds);
                                },
                                'visible' => $documentType == \frontend\models\Documents::IO_TYPE_IN,
                            ],
                        ],
                    ]); ?>
                </div>
            </div>
        </div>
    </div>
    <?= Html::endForm(); ?>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-12">
                <?= Html::button('Добавить отмеченные', [
                    'class' => 'btn darkblue pull-right btn-w button-add-marked',
                    'data-dismiss' => 'modal',
                    'id' => 'add-to-invoice-button',
                    'style' => 'margin-left: 15px;'
                ]); ?>
                <?= Html::button('Добавить все', [
                    'class' => 'btn darkblue pull-right btn-w button-add-all',
                    'data-dismiss' => 'modal',
                    'id' => 'add-all-to-invoice-button',
                ]); ?>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
$(document).on("change", "#product_selected-all", function() {
    $("input.product_selected").prop("checked", $(this).prop("checked")).trigger("change");
    $("input.product_selected:not(.md-check, .md-radiobtn)").uniform("refresh");
});
</script>

<?php Pjax::end(); ?>