<?php

use common\models\product\Product;
use yii\bootstrap\Tabs;
use yii\helpers\Url;
use yii\helpers\Html;
use common\components\TextHelper;

/* @var $this yii\web\View */
/* @var $model common\models\product\Product */

?>

<div class="portlet-body goods-info">
    <div class="row">
        <div class="col-sm-4" style="max-width: 500px">
            <table class="table table-resume" style="width: auto; max-width: 500px; margin-left: -15px;">
                <colgroup>
                    <col width="180" align="right">
                </colgroup>
                <tbody>
                <tr class="even">
                    <td class="bold-text">Группа:</td>
                    <td><?= $model->group ? $model->group->title : Product::DEFAULT_VALUE; ?></td>
                </tr>
                <tr class="odd">
                    <td class="bold-text">Код:</td>
                    <td><?= $model->code ?: Product::DEFAULT_VALUE; ?></td>
                </tr>
                <tr class="even">
                    <td class="bold-text">Артикул:</td>
                    <td><?= $model->article ?: Product::DEFAULT_VALUE; ?></td>
                </tr>
                <tr class="odd">
                    <td class="bold-text">Единица измерения:</td>
                    <td><?= $model->productUnit ? $model->productUnit->name : Product::DEFAULT_VALUE; ?></td>
                </tr>
                <tr class="even">
                    <td class="bold-text">Вес:</td>
                    <td><?= $model->weight ? $model->weight . ' кг' : Product::DEFAULT_VALUE; ?> </td>
                </tr>
                <tr class="odd">
                    <td class="bold-text">Объем:</td>
                    <td><?= $model->volume ? $model->volume . ' м3' : Product::DEFAULT_VALUE; ?> </td>
                </tr>
                <tr class="odd">
                    <td class="bold-text">Цена продажи:</td>
                    <td><?= TextHelper::invoiceMoneyFormat($model->price_for_sell_with_nds, 2); ?> </td>
                </tr>
                <tr class="even">
                    <td class="bold-text">Вид упаковки:</td>
                    <td><?= $model->box_type ? : Product::DEFAULT_VALUE; ?></td>
                </tr>
                <tr class="odd">
                    <td class="bold-text">Количество в одном месте:</td>
                    <td><?= $model->count_in_place ? : Product::DEFAULT_VALUE; ?></td>
                </tr>
                <tr class="even">
                    <td class="bold-text">Количество мест, штук:</td>
                    <td><?= $model->place_count ? : Product::DEFAULT_VALUE; ?></td>
                </tr>
                <tr class="odd">
                    <td class="bold-text">Масса брутто:</td>
                    <td><?= $model->mass_gross ? : Product::DEFAULT_VALUE; ?></td>
                </tr>
                <tr class="even">
                    <td class="bold-text">Страна происхождения товара:</td>
                    <td><?= $model->countryOrigin ? $model->countryOrigin->name_short : Product::DEFAULT_VALUE; ?></td>
                </tr>
                <tr class="odd">
                    <td class="bold-text">Номер таможенной декларации:</td>
                    <td><?= $model->customs_declaration_number ? : Product::DEFAULT_VALUE; ?></td>
                </tr>
                <tr class="even">
                    <td class="bold-text"><?= $model->getAttributeLabel('item_type_code') ?>:</td>
                    <td><?= $model->item_type_code ? : Product::DEFAULT_VALUE; ?></td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="col-sm-4" style="max-width: 250px; padding-top: 10px;">
            <span style="display: block;font-weight: bold;padding-bottom: 10px;">Фото</span>
            <?= ($src = $model->getImageThumb()) ? Html::img($src, ['alt' => '', 'style' => 'max-width: 100%;']) : null; ?>
        </div>
        <div class="col-sm-4" style="padding-top: 10px;">
            <span style="display: block;font-weight: bold;padding-bottom: 10px;">Описание</span>
            <?= $model->comment_photo; ?>
        </div>
    </div>
</div>
