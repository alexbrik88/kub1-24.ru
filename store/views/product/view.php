<?php

use common\models\product\Product;
use frontend\rbac\permissions;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\product\Product */

$this->title = $model->title;
$this->context->layoutWrapperCssClass = 'out-sf out-document out-act cash-order';
$isMoveable = $model->production_type == Product::PRODUCTION_TYPE_GOODS && max($model->quantity) > 0;
switch ($model->status) {
    case Product::DELETED:
        $statusText = $model->production_type == Product::PRODUCTION_TYPE_GOODS ? 'Товар удалён' : 'Услуга удалена';
        break;
    case Product::ARCHIVE:
        $statusText = 'Находится в архиве';
        break;
    default:
        $statusText = '';
        break;
}
$backUrl = Url::previous('product_view_back');
?>
<div class="out-sf out-document out-act cash-order">
    <style type="text/css">
        .product-view .table.table-resume td {
            padding: 10px 15px;
            text-align: left !important;
        }
    </style>
    <div class="product-view page-content-in">
        <?= Html::a('Назад к списку', $backUrl ? : [
            'index',
            'productionType' => $model->production_type,
        ], [
            'class' => 'back-to-customers',
        ]) ?>
        <?php if ($statusText) : ?>
            <h3 class="text-warning"><?= $statusText; ?></h3>
        <?php endif; ?>
        <div class="row">
            <div class="col-md-12">
            </div>
        </div>
        <div class="portlet">
            <div class="portlet-title">
                <div class="caption" style="font-size: 23px;font-weight: bold;">
                    <?= $model->title; ?>
                </div>
                <div class="actions">
                    <?= Html::a('<i class="ico-Cancel-smart-pls fs" style="color:white"></i>', $backUrl ? : [
                        'index',
                        'productionType' => $model->production_type,
                    ], [
                        'class' => 'btn darkblue btn-sm info-button',
                        'style' => 'width:33px; height: 27px;',
                    ]); ?>
                </div>
            </div>
            <?php if ($model->production_type == Product::PRODUCTION_TYPE_GOODS): ?>
                <?= $this->render('view/_goods', ['model' => $model]) ?>
            <?php elseif ($model->production_type == Product::PRODUCTION_TYPE_SERVICE): ?>
                <?= $this->render('view/_service', ['model' => $model]) ?>
            <?php endif; ?>
        </div>
    </div>
    <div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <div class="created-by">
                        создан
                        <span><?= date("d.m.Y", $model->created_at); ?></span>
                        <span><?= $model->creator->fio; ?></span>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
</div>
