<?php

use common\models\product\Product;
use yii\helpers\Url;
use common\models\document\OrderDocument;
use common\models\document\status\OrderDocumentStatus;

/* @var $this yii\web\View */
/* @var $model OrderDocument */

$status = $model->status;

$productionTypeArray = explode(', ', $model->production_type);
$hasInvoice = $model->getInvoice()->exists();
if ($hasInvoice) {
    $productCountPL = $model->invoice->getPackingLists()->joinWith('orderPackingLists')->select('SUM({{order_packing_list}}.[[quantity]])')->scalar();
    $productCountInvoice = $model->invoice->getOrders()->joinWith('product')->andWhere([
        'product.production_type' => Product::PRODUCTION_TYPE_GOODS,
    ])->select('SUM({{order}}.[[quantity]])')->scalar();
}
?>
<div class="control-panel col-xs-12 pad0 pull-right">
    <div class="col-xs-12 pad0">
        <div class="col-xs-12 col-sm-3 pad3">
            <div class="btn full_w marg order-document_status_date"
                 style="padding-left:0; padding-right:0;text-align: center;background-color: <?= $model->status->color; ?>"
                 title="Дата изменения статуса">
                <?= date("d.m.Y", $model->status_updated_at); ?>
            </div>
        </div>
        <div class="col-xs-12 col-sm-9 pad3" style="padding-right: 0;">
            <div class="btn full_w" style="background-color: <?= $model->status->color; ?>">
                <?= $model->status->name ?>
            </div>
        </div>
    </div>
    <div class="col-xs-12 pad0" style="margin-bottom: 5px !important;">
        <div class="col-xs-3"> &nbsp; </div>
        <div class="col-xs-9 pad3" style="padding-top: 0px !important; margin-top: -3px; ">
            <div class="document-panel btn-group tooltip2" style="width: 99.9% !important; margin-bottom: 0px;">
                <?php if (!$model->invoice): ?>
                    <?= $this->render('status_rows/_first_row', [
                        'document' => 'invoice',
                        'title' => 'Счет',
                        'model' => $model,
                        'action' => 'create-from-order',
                    ]); ?>
                <?php else: ?>
                    <?= $this->render('status_rows/_first_row', [
                        'document' => 'invoice',
                        'title' => 'Счет',
                        'model' => $model,
                        'action' => 'view',
                        'id' => $model->invoice->id,
                    ]); ?>
                <?php endif; ?>
                <?php if (in_array(Product::PRODUCTION_TYPE_GOODS, $productionTypeArray) && $hasInvoice): ?>
                    <?php if (!$model->invoice->packingLists): ?>
                        <?= $this->render('status_rows/_first_row', [
                            'document' => 'packing-list',
                            'title' => 'тов. накладная',
                            'model' => $model,
                            'action' => 'create',
                            'id' => 0,
                        ]);
                        ?>
                    <?php elseif (count($model->invoice->packingLists) == 1 && $productCountPL == $productCountInvoice): ?>
                        <?= $this->render('status_rows/_first_row', [
                            'document' => 'packing-list',
                            'title' => 'тов. накладная',
                            'model' => $model,
                            'action' => 'view',
                            'id' => $model->invoice->packingLists[0]->id,
                        ]);
                        ?>
                    <?php else: ?>
                        <?= $this->render('status_rows/_many_rows', [
                            'document' => 'packing-list',
                            'title' => 'тов. накладная',
                            'model' => $model,
                            'addAvailable' => $productCountPL != $productCountInvoice,
                        ]);
                        ?>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
