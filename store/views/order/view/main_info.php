<?php

use common\models\document\OrderDocument;
use yii\bootstrap\Html;
use yii\helpers\Url;
use frontend\modules\documents\widgets\CreatedByWidget;
use common\models\Contractor;
use common\components\date\DateHelper;
use common\models\company\CompanyType;
use common\components\TextHelper;
use common\models\employee\Employee;

/* @var $this yii\web\View
 * @var $model OrderDocument
 * @var $dateFormatted string
 * @var $user Employee
 */

$company = $model->company;
?>
<style type="text/css">
    .pre-view-table .bord-dark tr, .bord-dark td, .bord-dark th {
        padding: 2px 4px;
        font-size: 9pt;
        border: 1px solid #000000;
    }

    .pre-view-table h3 {
        text-align: center;
        margin: 6px 0 5px;
        font-weight: bold;
    }

    .m-size-div div {
        font-size: 12px;
    }

    .m-size-div div span {
        font-size: 9px;
    }

    .no_min_h {
        min-height: 0 !important;
    }
</style>
<div class="page-content-in m-size-div container-first-account-table pad0 no_min_h"
     style="min-width: 520px; max-width:735px; border:1px solid #4276a4; margin-top:3px;">
    <div class="col-xs-12 pad5 pre-view-table">
        <div class="col-xs-12 pad3" style="height: 80px;">
            <div class="col-xs-6 pad0 font-bold" style="height: inherit">
                <div class="col-xs-12 pad0 actions" style="margin-top: -8px;">
                    <?= CreatedByWidget::widget([
                        'createdAt' => date("d.m.Y", $model->created_at),
                        'author' => $model->author ? $model->author->fio : $model->contractor_name_short,
                        'statusUpdatedAt' => ($model->status_updated_at > $model->created_at) ? date("d.m.Y", $model->status_updated_at) : null,
                        'statusAuthor' => $model->statusAuthor ? $model->statusAuthor->fio : $model->contractor_name_short,
                        'statusAuthorId' => $model->status_author_id ? $model->status_author_id : $model->status_store_author_id,
                        'model' => $model,
                    ]); ?>
                    <?= Html::a('<i class="icon-pencil"></i>', Url::to(['update', 'id' => $model->id]), [
                        'class' => 'btn darkblue btn-sm',
                        'title' => 'Редактировать',
                        'style' => 'padding-bottom: 4px !important;',
                    ]); ?>
                </div>
                <div class="col-xs-12 pad0" style="padding-top: 22px !important;">
                    <p style="font-size: 17px"> <?= $model->contractor->getTitle(true); ?> </p>
                </div>
            </div>
        </div>
        <div class="col-xs-12 pad0" style="padding-top: 5px !important; border-bottom: 2px solid #000000">
            <h3 style="marg">Заказ № <?= $model->fullNumber; ?> от <?= $dateFormatted; ?></h3>
        </div>
        <div class="col-xs-12 pad3" style="padding-top: 15px !important;">
            <div class="col-xs-12 pad0">
                <div class="col-xs-2 pad0"> Поставщик <br/> (Исполнитель):</div>
                <div class="col-xs-10 pad0" style="padding-left:3px;font-weight: bold;">
                    <?= $model->company->getTitle(true); ?>,
                    ИНН <?= $model->company->inn; ?><?= !empty($model->company->kpp) ? ', КПП' . $model->company->kpp : ''; ?>
                    , <?= $model->company->getAddressLegalFull(); ?>
                </div>
            </div>
            <div class="col-xs-12 pad0" style="padding-top: 5px !important;">
                <div class="col-xs-2 pad0"> Покупатель<br/>(Заказчик):</div>
                <div class="col-xs-10 pad0" style="padding-left:3px;font-weight: bold;">
                    <?= $model->contractor_name_short; ?>,
                    <?= ($model->contractor->face_type == Contractor::TYPE_LEGAL_PERSON) ? 'ИНН ' . $model->contractor_inn . ',' : '' ?>
                    <?= ($model->contractor->face_type == Contractor::TYPE_LEGAL_PERSON && !empty($model->contractor_kpp)) ? 'КПП ' . $model->contractor_kpp . ',' : '' ?>
                    <?= $model->contractor_address_legal_full; ?>
                </div>
            </div>
            <?php if ($model->basis_document_name && $model->basis_document_number && $model->basis_document_date) : ?>
                <div class="col-xs-12 pad0"
                     style="padding-top: 5px !important;">
                    <div class="col-xs-2 pad0"> Основание:</div>
                    <div class="col-xs-10 pad3" style="font-weight: bold;">
                        <?= $model->agreementType ? $model->agreementType->name : 'Договор' ?>
                        № <?= Html::encode($model->basis_document_number) ?>
                        от <?= DateHelper::format($model->basis_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                    </div>
                </div>
            <?php endif ?>
        </div>
        <div class="col-xs-12 pad3">
            <?= $this->render('orders_table', [
                'model' => $model,
                'user' => $user,
            ]); ?>
        </div>
        <div class="col-xs-12 pad3">
            <div class="col-xs-12 pad0 m-t-n" style="border-bottom: 2px solid #000000;">
                <div class="col-xs-12 pad3">
                    <div class="txt-9">
                        Всего наименований <?= count($model->orderDocumentProducts) ?>, на сумму
                        <?= TextHelper::invoiceMoneyFormat($model->total_amount, 2); ?>
                        руб.
                    </div>
                    <div class="txt-9-b">
                        Всего к
                        оплате: <?= TextHelper::mb_ucfirst(TextHelper::amountToWords($model->total_amount / 100)); ?>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 pad0" style="padding-top: 10px !important; z-index: 2;height: 220px;">
                <div class="col-xs-12 pad0" style="height: 65px; text-align: center">
                    <div class="col-xs-3 pad3 v_bottom" style="font-weight: bold; text-align: left">
                        <div> Руководитель:</div>
                    </div>
                    <div class="col-xs-3 pad3 v_bottom">
                        <div> <?= $model->contractor_director_post_name; ?> </div>
                    </div>
                    <div class="col-xs-3 pad0 v_bottom"></div>
                    <div class="col-xs-3 pad3 v_bottom">
                        <div>
                            <?= $model->contractor_director_name; ?>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 pad0"
                     style="padding-top: 3px; text-align: center; font-size: 9px !important;">
                    <div class="col-xs-3 pad3"> &nbsp; </div>
                    <div class="col-xs-3 pad3">
                        <div class="bord-dark-t"><span> должность </span></div>
                    </div>
                    <div class="col-xs-3 pad3">
                        <div class="bord-dark-t"><span> подпись </span></div>
                    </div>
                    <div class="col-xs-3 pad3">
                        <div class="bord-dark-t"><span> расшифровка подписи </span></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>