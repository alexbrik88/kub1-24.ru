<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\ActiveForm;
use common\models\document\Invoice;
use common\models\document\Act;
use common\models\document\PackingList;
use common\models\document\InvoiceFacture;
use common\models\document\Upd;
use frontend\modules\documents\forms\InvoiceSendForm;
use common\models\document\OrderDocument;
use frontend\modules\documents\forms\OrderDocumentSendForm;

/* @var OrderDocument $model */
/* @var $useContractor string */

$sendForm = new OrderDocumentSendForm(Yii::$app->user->identity->currentEmployeeCompany);
$modalHeader = '';
$contractor = $model->contractor;
$modalHeader = 'заказ';
$sendForm->textRequired = true;
$sendForm->emailText = $model->emailText;
$labelWidth = (!$contractor->chief_accountant_is_director && !empty($contractor->chief_accountant_email)) ? 159 : 123.9;
$company = $model->company;
?>
<?php Modal::begin([
    'header' => Html::tag('h1', Html::tag('span', '', [
            'class' => 'ico-Send-smart-pls fs pull-left',
            'style' => 'font-size:inherit;line-height:inherit;',
        ]) . ' Выберите, кому отправить ' . $modalHeader),
    'headerOptions' => ['style' => 'background-color: #00b7af; color: #fff;'],
    'options' => [
        'id' => 'send',
        'class' => 'fade',
    ],
    'toggleButton' => false,
]); ?>
<?php $form = ActiveForm::begin([
    'id' => 'send-document-form',
    'action' => Url::to(['send', 'id' => $model->id,]),
    'options' => [
        'class' => 'add-to-order-document',
    ],
    'enableAjaxValidation' => true,
]); ?>
<div class="form-body">
    <div class="form-group row"
         style="<?= empty($contractor->director_email) ? 'margin-bottom: 0px;' : ''; ?>">
        <div class="container-who-send-label mar-top-8">
            <?= $form->field($sendForm, 'sendToChief', ['template' => "{label}\n{input}"])->checkbox([
                'label' => 'Руководитель:',
                'labelOptions' => [
                    'style' => 'width: ' . $labelWidth . 'px;',
                ],
                'class' => 'add-chief-email',
            ]); ?>
        </div>
        <div class="container-who-send-input chief-email"
             style="margin-bottom: -8px;">

            <?php if (empty($contractor->director_email)) { ?>
                <?php if (!empty($contractor->director_name)) { ?>
                    <span class="text-muted inline-block col-lg-6 col-md-6 col-xs-6 mar-top-8 pad-left-none"
                          style="padding-right: 15px;"><?= $contractor->director_name; ?> </span>
                <?php } ?>
                <?= $form->field($sendForm, 'chiefEmail', ['options' => ['class' => 'col-lg-6 col-md-6 col-xs-6 pad-right-none pad-left-none']])->label(false)->textInput([
                    'placeHolder' => 'Укажите e-mail',
                    'class' => 'form-control width100',
                ]); ?>
            <?php } else { ?>
                <span
                    class="text-muted mar-top-8 inline-block col-lg-6 col-md-6 col-xs-6 pad-left-none"><?= $contractor->director_name; ?> </span>
                <span
                    class="text-muted mar-top-8 inline-block col-lg-6 col-md-6 col-xs-6 pad-right-none pad-left-none"><?= $contractor->director_email; ?></span>
            <?php } ?>
        </div>
        <div class="col-sm-12">
            <?= $form->field($sendForm, 'sendToChief', ['template' => "{error}"])->checkbox(); ?>
        </div>
    </div>

    <?php if (!$contractor->chief_accountant_is_director): ?>
        <div class="form-group row"
             style="<?= empty($contractor->chief_accountant_email) ? 'margin-bottom: 0px;' : ''; ?>">
            <div class="container-who-send-label mar-top-8">
                <?= $form->field($sendForm, 'sendToChiefAccountant')->checkbox([
                    'label' => 'Главный бухгалтер:',
                ]); ?>
            </div>
            <div class="container-who-send-input chief-email"
                 style="margin-bottom: -8px;">

                <?php if (empty($contractor->chief_accountant_email)) { ?>
                    <?php if (!empty($contractor->chief_accountant_name)) { ?>
                        <span class="text-muted inline-block col-lg-6 col-md-6 mar-top-8 pad-left-none"
                              style="padding-right: 15px;"><?= $contractor->chief_accountant_name; ?> </span>
                    <?php } ?>
                    <?= $form->field($sendForm, 'chiefAccountantEmail', ['options' => ['class' => 'col-lg-6 col-md-6 pad-right-none pad-left-none']])->label(false)->textInput([
                        'placeHolder' => 'Укажите e-mail',
                        'class' => 'form-control width100',
                    ]); ?>
                <?php } else { ?>
                    <span
                        class="text-muted mar-top-8 inline-block col-lg-6 col-md-6 col-xs-6 pad-left-none"><?= $contractor->chief_accountant_name; ?> </span>
                    <span
                        class="text-muted mar-top-8 inline-block col-lg-6 col-md-6 col-xs-6 pad-right-none pad-left-none"><?= $contractor->chief_accountant_email; ?></span>
                <?php } ?>
            </div>
        </div>
    <?php endif; ?>

    <?php if (!$contractor->contact_is_director): ?>
        <div class="form-group row" style="margin-bottom: 0px;">
            <div class="container-who-send-label mar-top-8">
                <?= $form->field($sendForm, 'sendToContact')->checkbox([
                    'label' => 'Контакт:',
                    'labelOptions' => [
                        'style' => 'width: ' . $labelWidth . 'px;',
                    ],
                ]); ?>
            </div>
            <div class="container-who-send-input chief-email"
                 style="margin-bottom: -8px;">

                <?php if (empty($contractor->contact_email)) { ?>
                    <?php if (!empty($contractor->contact_name)) { ?>
                        <span
                            class="text-muted inline-block col-lg-6 col-md-6 mar-top-8 pad-left-none"><?= $contractor->contact_name; ?> </span>
                    <?php } ?>
                    <?= $form->field($sendForm, 'contactAccountantEmail', ['options' => ['class' => 'col-lg-6 col-md-6 pad-right-none pad-left-none']])->label(false)->textInput([
                        'placeHolder' => 'Укажите e-mail',
                        'class' => 'form-control width100',
                    ]); ?>
                <?php } else { ?>
                    <span class="text-muted"
                          style="padding-right: 15px;"><?= $contractor->contact_name; ?> </span>
                    <span class="text-muted"
                          style="padding-left: 15px;"><?= $contractor->contact_email; ?></span>
                <?php } ?>
            </div>
        </div>
    <?php endif; ?>
    <div class="form-group row">
        <div class="container-who-send-label mar-top-8">
            <?= $form->field($sendForm, 'sendToOther')->checkbox([
                'label' => 'Еще:',
                'labelOptions' => [
                    'style' => 'width: ' . $labelWidth . 'px;',
                ],
            ]); ?>
        </div>
        <div class="container-who-send-input">
            <?= $form->field($sendForm, 'otherEmail')->label(false)->textInput([
                'placeHolder' => 'Укажите e-mail',
                'class' => 'form-control width100',
            ]); ?>
        </div>
    </div>
    <?php if ($sendForm->textRequired): ?>
        <div class="form-group row">
            <div class="col-sm-12">
                <label id="invoicesendform-emailtext-label" class="control-label"
                       style="cursor: pointer; border-bottom: 1px dashed #e5e5e5; margin-bottom: 0;">
                    Текст письма <span class="caret"></span>
                </label>

                <div class="email_text_input hidden" style="margin-top: -1px;">
                    <?= $form->field($sendForm, 'emailText')->textArea([
                        'rows' => 4,
                        'onkeyup' => 'emailTextResize()',
                        'style' => 'padding: 10px; width: 100%; border-bottom: 0; resize: none; overflow: hidden; border-color: #e5e5e5 !important;',
                    ])->hint($this->render('email_text_hint'))->label(false); ?>
                </div>
            </div>
        </div>
        <?php $this->registerJs('
                function emailTextResize() {
                    var inputArea = document.getElementById("orderdocumentsendform-emailtext")
                    $(inputArea).height(1);
                    $(inputArea).height(inputArea.scrollHeight);
                }
                $("#send").on("shown.bs.modal", function() {
                    emailTextResize();
                })
                $(document).on("click", "#orderdocumentsendform-emailtext-label", function() {
                    $(".email_text_input").toggleClass("hidden");
                    emailTextResize();
                });
            '); ?>
    <?php endif ?>
</div>
<div class="form-group">
    <div class="row">
        <div class="col-sm-12">
            Ответное письмо клиента на данное письмо с заказом,
            <br>
            придет на вашу почту – <span><?= Yii::$app->user->identity->email; ?></span>
        </div>
    </div>
</div>

<div class="form-actions">
    <div class="row action-buttons">
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            <?= Html::submitButton('Отправить', [
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs'
            ]) ?>
            <?= Html::submitButton('<i class="glyphicon glyphicon-envelope"></i>', [
                'class' => 'btn darkblue widthe-100 hidden-lg',
                'title' => 'Отправить',
            ]) ?>
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
            <a href="#"
               class="btn darkblue widthe-100 hidden-md hidden-sm hidden-xs" data-dismiss="modal" aria-hidden="true">Отменить</a>
            <a href="#" class="btn darkblue widthe-100 hidden-lg"
               title="Отменить" data-dismiss="modal" aria-hidden="true"><i class="fa fa-reply fa-2x"></i></a>
        </div>
    </div>
</div>

<?php $form->end(); ?>

<div id="send-document-loading" class="ajax-loader-wrapper" style="display: none;">
    <img src="/img/loading.gif">
</div>

<?php Modal::end(); ?>

<?php $this->registerJs('
$(document).on("submit", "#send-document-form", function() {
    $("#send-document-loading").show();
});
$(document).on("hidden.bs.modal", "#send", function () {
    $("#send-document-form")[0].reset();
})
'); ?>
