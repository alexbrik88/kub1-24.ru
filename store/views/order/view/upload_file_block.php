<?php

use yii\bootstrap\Html;
use common\models\document\OrderDocument;
use common\components\date\DateHelper;
use frontend\rbac\permissions;

/* @var $this yii\web\View */
/* @var $model OrderDocument */

$canUpdate = Yii::$app->user->can(permissions\document\Document::UPDATE, ['model' => $model]);
?>
<div class="col-xs-12 col-lg-9 pull-right pad3" style="max-width: 720px;">
    <div class="portlet">
        <div class="customer-info" style="border: 1px solid #4276a4; min-height: 50px;">
            <div class="portlet-body no_mrg_bottom main_inf_no-bord">
                <table class="table no_mrg_bottom">
                    <tr>
                        <td>
                            <span class="customer-characteristic">
                                Поставщик:
                            </span>
                            <span>
                                <?= $model->company->getTitle(true); ?>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span
                                class="customer-characteristic">Отгрузить до:</span>
                            <span><?= DateHelper::format($model->ship_up_to_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?></span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <?php if ($model->basis_document_name && $model->basis_document_number && $model->basis_document_date) : ?>
                                <span class="customer-characteristic">
                                        <?= $model->agreementType ? $model->agreementType->name : 'Договор'; ?>:
                                </span>
                                № <?= Html::encode($model->basis_document_number); ?>
                                от <?= DateHelper::format($model->basis_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                            <?php endif ?>
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
</div>