<?php

use common\components\TextHelper;
use common\models\document\status\ActStatus;
use common\models\document\status\PackingListStatus;
use common\models\document\status\InvoiceFactureStatus;
use yii\bootstrap\Html;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use common\models\document\OrderDocument;

/**
 * @var $model OrderDocument
 * @var $document string
 * @var $title string
 * @var $addAvailable boolean
 */

$iconClass = 'icon-doc';
if ($document == 'act') {
    $docQuery = $model->invoice->getAct();
    $iconClass = ActStatus::getStatusIcon($model->invoice->getAct()->min('status_out_id'));
} elseif ($document == 'packing-list') {
    $docQuery = $model->invoice->getPackingList();
    $iconClass = PackingListStatus::getStatusIcon($model->invoice->getPackingList()->min('status_out_id'));
} elseif ($document == 'invoice-facture') {
    $docQuery = $model->invoice->getInvoiceFactures();
    $iconClass = InvoiceFactureStatus::getStatusIcon($model->invoice->getInvoiceFactures()->min('status_out_id'));
} elseif ($document == 'upd') {
    $docQuery = $model->invoice->getUpds();
}
$docArray = !empty($docQuery) ? $docQuery->orderBy('document_number DESC')->all() : [];
$sum = $docArray ? array_sum(ArrayHelper::getColumn($docArray, 'totalAmountWithNds')) : 0;
?>
<div class="clearfix" style="display: block; position: relative;">
    <div class="col-xs-7 pad0" style="padding-right: 3px !important;">
        <a data-toggle="dropdown"
           class="btn yellow dropdown-toggle tooltipstered<?= $model->isRejected ? ' disabled' : ''; ?>"
            <?= $model->isRejected ? ' disabled="disabled"' : ''; ?>
           style="text-transform: uppercase;width: 100%; height: 34px;margin-left: 0;padding-right: 14px; padding-left: 14px;<?= $model->isRejected ? ' background-color: #a2a2a2;' : ''; ?>"
           title="<?= mb_convert_case(($addAvailable ? 'Добавить ' : '') . $title, MB_CASE_TITLE) ?>">
            <i class="pull-left icon <?= $iconClass; ?>"></i>
            <?= $title ?>
        </a>
        <ul class="dropdown-menu" style="width: 317px;">
            <?php if ($addAvailable): ?>
                <li>
                    <?= Html::a(
                        '[+Добавить ' . $title . ']',
                        [$document . '/create', 'type' => $model->type, 'invoiceId' => $model->invoice->id],
                        ['options' => ['class' => 'dropdown-new']]
                    ); ?>
                </li>
            <?php endif; ?>
            <?php foreach ($docArray as $list_item): ?>
                <li>
                    <a class="dropdown-item" style="position: relative;"
                       href="<?= Url::to([$document . '/view', 'type' => $model->type, 'id' => $list_item->id]) ?>">
                    <span class="dropdown-span-left">
                        № <?= $list_item->document_number ?>
                        от <?= Yii::$app->formatter->asDate($list_item->created_at) ?>
                    </span>
                        <?php if ($document == 'act'): ?>
                            <span class="dropdown-span-right"><i
                                    class="fa fa-rub"></i><?= TextHelper::invoiceMoneyFormat($list_item->totalAmountWithNds, 2) ?></span>
                        <?php else: ?>
                            <span class="dropdown-span-right"><i
                                    class="fa fa-rub"></i><?= TextHelper::invoiceMoneyFormat($list_item->totalAmountWithNds, 2) ?></span>
                        <?php endif; ?>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div>
    <div class="col-xs-5 pad0" style="padding-left: 3px !important;">
        <a class="btn yellow" style="width: 100%; <?= $model->isRejected ? ' background-color: #a2a2a2;' : ''; ?>"
            <?= $model->isRejected ? ' disabled="disabled"' : ''; ?>
           title="Сумма текущих <?= $title ?>">
            <i class="fa fa-rub"></i> <?= TextHelper::invoiceMoneyFormat($sum, 2); ?>
        </a>
    </div>
</div>
