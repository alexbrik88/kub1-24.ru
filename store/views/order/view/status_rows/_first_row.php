<?php

use frontend\models\Documents;
use yii\helpers\Url;
use common\models\document\OrderDocument;
use common\models\document\status\PackingListStatus;
use common\models\document\status\ActStatus;
use common\models\document\status\InvoiceFactureStatus;

/**
 * @var $model OrderDocument
 * @var $id integer
 * @var $document string
 * @var $title string
 * @var $action string
 */

if ($action == 'create') {
    $url = Url::to([$document . '/' . $action, 'type' => Documents::IO_TYPE_OUT, 'invoiceId' => $model->invoice->id]);
} elseif ($action == 'view') {
    $url = Url::to([$document . '/' . $action, 'type' => Documents::IO_TYPE_OUT, 'id' => $id]);
} elseif ($action == 'create-from-order') {
    $url = Url::to([$document . '/' . $action, 'type' => Documents::IO_TYPE_OUT, 'orderDocumentID' => $model->id]);
} else {
    return null;
}
$hasInvoice = $model->getInvoice()->exists();
$iconClass = 'fa fa-plus-circle';

if ($document == 'invoice') {
    if ($hasInvoice) {
        $iconClass = 'icon-doc';
    }
} elseif ($document == 'packing-list') {
    if ($model->invoice->getPackingList()->exists()) {
        $iconClass = PackingListStatus::getStatusIcon($model->invoice->getPackingList()->min('status_out_id'));
    }
} elseif ($document == 'act') {
    if ($model->invoice->getAct()->exists()) {
        $iconClass = ActStatus::getStatusIcon($model->invoice->getAct()->min('status_out_id'));
    }
} elseif ($document == 'invoice-facture') {
    if ($model->invoice->getInvoiceFactures()->exists()) {
        $iconClass = InvoiceFactureStatus::getStatusIcon($model->invoice->getInvoiceFactures()->min('status_out_id'));
    }
} elseif ($document == 'upd') {
    if ($model->invoice->getUpds()->exists()) {
        $iconClass = 'icon-doc';
    }
}
?>
<div class="clearfix">
    <a href="<?= $url ?>" class="btn yellow full_w <?= $model->isRejected ? ' disabled' : ''; ?>"
        <?= $model->isRejected ? ' disabled="disabled"' : ''; ?>
       style="height: 34px;margin-left: 0; text-transform: uppercase;<?= $model->isRejected ? ' background-color: #a2a2a2;' : ''; ?>"
       title="<?= mb_convert_case(($action == 'create' ? 'Добавить ' : '') . $title, MB_CASE_TITLE) ?>">
        <i class="pull-left icon <?= $iconClass; ?>"></i>
        <?= $title; ?>
    </a>
</div>