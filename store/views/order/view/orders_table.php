<?php

use common\components\TextHelper;
use common\models\product\Product;
use common\models\document\OrderDocument;
use common\models\employee\Employee;

/* @var $this yii\web\View */
/* @var $model OrderDocument */
/* @var $user Employee */

$isNdsExclude = $model->has_nds && $model->company->isNdsExclude;
$sum = $model->total_amount;
$discount = $model->getDiscountSum();
?>
<style>
    .bord-dark tr, .bord-dark td, .bord-dark th {
        border: 1px solid #000000;
    }
</style>
<div class="portlet">
    <table class="bord-dark" style="width: 99.9%; border:2px solid #000000">
        <thead>
        <tr style="text-align: center;">
            <th> №</th>
            <th> Наименование работ, услуг</th>
            <th> Артикул</th>
            <th> Кол-во</th>
            <th> Ед.</th>
            <th> Цена</th>
            <?php if ($model->has_discount) : ?>
                <th> Скидка %</th>
                <th> Цена со скидкой</th>
            <?php endif ?>
            <th> Сумма</th>
        </tr>
        </thead>
        <tbody class="bord-dark">
        <?php foreach ($model->orderDocumentProducts as $order): ?>
            <?php if ($isNdsExclude) {
                $priceBase = $order->base_price_no_vat;
                $priceOne = $order->selling_price_no_vat;
                $amount = $order->amount_sales_no_vat;
            } else {
                $priceBase = $order->base_price_with_vat;
                $priceOne = $order->selling_price_with_vat;
                $amount = $order->amount_sales_with_vat;
            }
            $unitName = $order->unit ? $order->unit->name : Product::DEFAULT_VALUE; ?>
            <tr>
                <td style="text-align: center; width: 5%"><?= $order->number; ?></td>
                <td style=" width: 0">
                    <?= $order->product_title; ?>
                </td>
                <td style="width: 10%;text-align: center">
                    <?= $order->product->article ? $order->product->article : '---'; ?>
                </td>
                <td style="text-align: right; width: 10%">
                    <?= $unitName == Product::DEFAULT_VALUE ? $unitName : str_replace('.', ',', $order->quantity); ?>
                </td>
                <td style="text-align: right; width: 7%"><?= $unitName ?></td>
                <?php if ($model->has_discount) : ?>
                    <td style="text-align: right; width: 13%">
                        <?= TextHelper::invoiceMoneyFormat($priceBase, 2); ?>
                    </td>
                    <td style="text-align: right; width: 13%">
                        <?= strtr($order->discount, ['.' => ',']) ?>
                    </td>
                <?php endif ?>
                <td style="text-align: right; width: 13%">
                    <?= TextHelper::invoiceMoneyFormat($priceOne, 2); ?>
                </td>
                <td style="text-align: right; width: 15%">
                    <?= TextHelper::invoiceMoneyFormat($amount, 2); ?>
                </td>
            </tr>
        <?php endforeach; ?>
        <tr>
            <td style="text-align: center; width: 5%;border-right: none;"></td>
            <td colspan="2" style="width: 0;border-left: none;">
                <b>Итого</b>
            </td>
            <td style="text-align: right; width: 10%">
                <?= str_replace('.', ',', $model->getOrderDocumentProducts()->sum('quantity') * 1); ?>
            </td>
            <td colspan="2" style="text-align: right; width: 7%"></td>
            <td style="text-align: right; width: 10%">
                <?= TextHelper::invoiceMoneyFormat($model->total_amount, 2); ?>
            </td>
        </tr>
        </tbody>
    </table>
    <table class="it-b" style="width: 99.9%;">
        <?php if ($model->has_discount) : ?>
            <tr>
                <td width="430px" style="border: none"></td>
                <td class="txt-b2" style="text-align: right; border: none; width: 150px;">
                    Сумма скидки:
                </td>
                <td class="txt-b2" style="text-align: right; border: none; width: 124px;">
                    <b><?= TextHelper::invoiceMoneyFormat($discount, 2); ?></b>
                </td>
            </tr>
        <?php endif ?>
    </table>
</div>
