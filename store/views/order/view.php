<?php

use common\models\document\OrderDocument;
use yii\bootstrap\Html;
use yii\helpers\Url;
use php_rutils\RUtils;
use common\models\store\StoreUser;

/**
 * @var $this yii\web\View
 * @var $model OrderDocument
 * @var $user StoreUser
 */

$dateFormatted = RUtils::dt()->ruStrFTime([
    'format' => 'd F Y г.',
    'monthInflected' => true,
    'date' => $model->document_date,
]);
$this->context->layoutWrapperCssClass = 'order-document-view out-document';

$this->title = 'Заказ №' . $model->fullNumber . ' от ' . $dateFormatted;
$contractorID = $user->currentStoreCompany->currentStoreCompanyContractor->contractor_id;
$previousOrderDocument = $model->getPreviousOrderDocument($contractorID);
$nextOrderDocument = $model->getNextOrderDocument($contractorID);
?>
<div class="page-content-in" style="padding-bottom: 30px;">
    <div style="margin-top: -13px;">
        <?= Html::a('Назад к списку', Url::to(['index']), [
            'class' => 'back-to-customers',
            'style' => 'display: inline-block;',
        ]); ?>
        <div class="change-order-document">
            <?php if ($previousOrderDocument !== null): ?>
                <span class="fa fa-long-arrow-left previous"
                      data-url="<?= Url::to(['view', 'id' => $previousOrderDocument->id]); ?>"></span>
            <?php endif; ?>
            <?php if ($nextOrderDocument !== null): ?>
                <span class="fa fa-long-arrow-right next"
                      data-url="<?= Url::to(['view', 'id' => $nextOrderDocument->id]); ?>"></span>
            <?php endif; ?>
        </div>
    </div>
    <?php if ($model->is_deleted): ?>
        <h1 class="text-warning">Заказ удалён</h1>
    <?php endif; ?>
    <div class="col-xs-12 pad0">
        <div class="col-xs-12 col-lg-7 pad0">
            <?= $this->render('view/main_info', [
                'model' => $model,
                'dateFormatted' => $dateFormatted,
                'user' => $user,
            ]); ?>
        </div>
        <?php if (!$model->is_deleted) : ?>
            <div class="col-xs-12 col-lg-5 pad0 pull-right" style="padding-bottom: 5px !important; max-width: 480px;">
                <div class="col-xs-12" style="padding-right:0px !important;">
                    <?= $this->render('view/status_block', [
                        'model' => $model,
                    ]); ?>
                    <?= $this->render('view/upload_file_block', [
                        'model' => $model,
                        'dateFormatted' => $dateFormatted,
                    ]); ?>
                </div>
            </div>
        <?php endif ?>
    </div>
    <?php if (!$model->is_deleted) : ?>
        <div class="col-xs-12 col-sm-12 col-lg-12 pad0" id="buttons-container" style="padding-top: 10px !important;">
            <div class="col-xs-12 col-sm-12 col-lg-12 pad0" id="buttons-scroll-fixed">
                <div class="buttons-container-white">
                    <?= $this->render('view/action_buttons', [
                        'model' => $model,
                    ]); ?>
                </div>
            </div>
            <style>
                #buttons-scroll-fixed {
                    position: fixed;
                    bottom: 0;
                    z-index: 9999;
                    background-color: white;
                    /*left: 255px;*/
                }

                .buttons-container-white {
                    background-color: white;
                    padding: 10px 20px;
                    margin-left: -20px;
                    margin-right: -20px;
                }

                .footer-for-invoice {
                    text-align: left !important;
                }
            </style>
            <script>
                var buttonsScroll = (function () {
                    var forMobileOffset = 60;
                    var forDesktopOffset = 28;
                    var startPosition = 10;
                    var currentPosition = 10;
                    var offsetHeightMonitor = 25;
                    var offset = startPosition + forDesktopOffset;
                    var idTimer = [];

                    var init = function () {
                        $('#page-footer').css({'display': 'none'});
                        $('#footer-for-invoice').removeClass('hide');
                        calculate();
                    };

                    var addStyle = function (forPix, toPix) {
                        var increment = forPix <= toPix ? true : false;
                        var current = forPix;
                        var iteration = Math.abs(forPix - toPix);

                        $.each(idTimer, function (def, id) {
                            clearTimeout(id);
                        });

                        idTimer[0] = setTimeout(function run() {
                            if (increment) {
                                current++;
                            } else {
                                current--;
                            }
                            $('#buttons-scroll-fixed').css({'bottom': current + 'px'});

                            if (iteration < 1) {
                                currentPosition = current;
                                $.each(idTimer, function (def, id) {
                                    clearTimeout(id);
                                });
                            } else {
                                idTimer[2] = setTimeout(run, 1);
                            }

                            iteration--;

                        }, 200);
                    };

                    var calculate = function () {
                        $('#buttons-scroll-fixed').css({'width': $('#buttons-container').width() + 'px'});
                        if ($(window).width() < 992) {
                            /*$('#buttons-scroll-fixed').css({'left': '0px'});
                             $('#buttons-scroll-fixed').css({'width': '100%'});*/
                            //$('#buttons-scroll-fixed').removeClass('col-md-12');

                            if ($('.buttons-container-white').length > 0) {
                                $('.page-footer').hide();
                            }

                            //$('.page-container').css({'padding-bottom:': '50px !important'});
                        } else {
                            //$('#buttons-scroll-fixed').addClass('col-md-12');
                            /*$('#buttons-scroll-fixed').css({'left': '255px'});*/
                            /*$('#buttons-scroll-fixed').css({'margin-right:': '-17px'});*/
                            if ($('.buttons-container-white').lenth > 0) {
                                $('.page-footer').show()
                            }
                        }

                        if ($(window).width() < 425) {
                            offset = startPosition + forMobileOffset + 24;
                            offsetHeightMonitor + 38;
                        } else {
                            if ($(window).width() < 992) {
                                offset = startPosition + forMobileOffset;
                            } else {
                                offset = startPosition + forDesktopOffset;
                            }
                        }
                    };

                    window.onscroll = function () {
                        calculate();
                        /*var positionScrollBottom = $(window).scrollTop() + $(window).height();
                         if ($(document).height() - positionScrollBottom < offsetHeightMonitor) {
                         addStyle(currentPosition, offset);
                         } else {
                         addStyle(currentPosition, startPosition);
                         }*/
                    };

                    return {init: init};
                })
                ();
                $(document).ready(function () {
                    buttonsScroll.init();
                });
            </script>
        </div>
    <?php endif ?>
</div>
