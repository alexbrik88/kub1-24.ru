<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 10.02.2018
 * Time: 17:59
 */

use common\models\document\OrderDocument;
use yii\bootstrap\ActiveForm;
use common\models\Company;
use common\models\store\StoreUser;

/* @var $this yii\web\View
 * @var $model OrderDocument
 * @var $company Company
 * @var $user StoreUser
 */

$this->title = 'Создать заказ';
$this->context->layoutWrapperCssClass = 'create-order-document';
$errorModelArray = [$model];
?>
    <div class="create-order-document">
        <?php $form = ActiveForm::begin(array_merge(Yii::$app->params['formDefaultConfig'], [
            'id' => 'order-document-form',
            'options' => [
                'enctype' => 'multipart/form-data',
                'class' => 'add-avtoschet',
            ],
            'enableClientValidation' => false,
        ])); ?>
        <div class="form-body form-body_sml form-body-order-document">
            <?= $form->errorSummary($errorModelArray); ?>
            <div class="portlet">
                <?= $this->render('form/header', [
                    'model' => $model,
                    'form' => $form,
                ]); ?>
                <div class="portlet-body">
                    <?= $this->render('form/body', [
                        'model' => $model,
                        'form' => $form,
                        'company' => $company,
                    ]); ?>
                    <div class="col-xs-12 pad0" style="margin-top: 5px !important">
                        <?= $this->render('form/product_table', [
                            'model' => $model,
                            'company' => $company,
                            'user' => $user,
                        ]); ?>
                    </div>
                    <div class="row">
                        <div class="col-md-4 col-md-push-8">
                            <?= $this->render('form/total_block', [
                                'model' => $model,
                                'user' => $user,
                            ]); ?>
                        </div>
                    </div>
                </div>
            </div>
            <?= $this->render('form/buttons', [
                'model' => $model,
                'company' => $company,
                'user' => $user,
            ]); ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>

<?= $this->render('form/product_new_modal'); ?>

<?= $this->render('form/product_existed_modal', [
    'model' => $model,
]); ?>

<?= $this->render('form/remove_product_modal'); ?>

<?php $this->registerJs(<<<JS
    $(document).on('blur', '.product-count', function(){
        if ($(this).val() == 0 || $(this).val() == ''){
            $(this).val($(this).attr('data-value'));
            ORDERDOCUMENT.recalculateOrderDocumentTable();
        } else {
           $(this).attr('data-value', $(this).val());
        }
    });
    $(document).on('blur','.product-title', function(){
        var name = $(this).val().replace(/\s+/g, '').length;
        if (name == 0) {
            $(this).closest('tr').remove();
            $('#from-new-add-row').show();
            ORDERDOCUMENT.recalculateOrderDocumentTable();
        }

    });
    $(document).on('click', '.product-title-clear', function(){
        $(this).closest('td').find('.product-title').val('');
        $(this).closest('td').find('.product-title').focus();
    });
JS
    , $this::POS_READY);
