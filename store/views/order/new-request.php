<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use lavrentiev\widgets\toastr\NotificationFlash;

/* @var $this yii\web\View */
/* @var $newRequests common\models\store\StoreCompanyContractor[] */

?>
<?= NotificationFlash::widget([
    'options' => [
        'closeButton' => true,
        'showDuration' => 1000,
        'hideDuration' => 1000,
        'timeOut' => 5000,
        'extendedTimeOut' => 1000,
        'positionClass' => NotificationFlash::POSITION_TOP_RIGHT,
    ],
]); ?>
<?php if ($newRequests) : ?>
    <?php foreach ($newRequests as $key => $link) : ?>
        <?= $key ? '<hr/>' : ''; ?>
        <?php $form = ActiveForm::begin([
            'action' => ['/order/new-request'],
            'options' => ['class' => 'new-request-form'],
        ]); ?>
            <h3><?= Html::encode($link->contractor->company->getTitle(true)) ?></h3>
            <div style="margin: 20px 0;">
                <?= Html::submitButton('Принять', [
                    'class' => 'new-request-form-btn btn darkblue text-white',
                    'name' => "link[{$link->contractor_id}]",
                    'value' => '1',
                ]) ?>
                <?= Html::submitButton('Отклонить', [
                    'class' => 'new-request-form-btn btn darkblue text-white pull-right',
                    'name' => "link[{$link->contractor_id}]",
                    'value' => '0',
                ]) ?>
            </div>
        <?php ActiveForm::end(); ?>
    <?php endforeach ?>
<?php else : ?>
    <h4>Нет новых предложений</h4>
    <div style="margin-top: 30px; text-align: center;">
        <button type="button" class="btn darkblue text-white" data-dismiss="modal" aria-hidden="true">Ok</button>
    </div>
<?php endif ?>