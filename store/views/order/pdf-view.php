<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 16.04.15
 * Time: 16:42
 */

use common\components\date\DateHelper;
use common\components\image\EasyThumbnailImage;
use common\components\TextHelper;
use common\models\Contractor;
use yii\helpers\Html;
use common\models\document\OrderDocument;
use common\models\company\CompanyType;
use common\models\currency\Currency;

/* @var $this yii\web\View */
/* @var $model OrderDocument */
/* @var $multiple [] */

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'format' => 'd F Y г.',
    'monthInflected' => true,
    'date' => $model->document_date,
]);

$this->title = $model->getPrintTitle();
$this->context->layoutWrapperCssClass = 'out-invoice out-document';

$printLink = (empty($addStamp) || !$model->company->print_link) ? null :
    EasyThumbnailImage::thumbnailSrc($model->company->getImage('printImage'), 200, 200, EasyThumbnailImage::THUMBNAIL_INSET);

$logoLink = !$model->company->logo_link ? null :
    EasyThumbnailImage::thumbnailSrc($model->company->getImage('logoImage'), 150, 50, EasyThumbnailImage::THUMBNAIL_INSET);
?>
    <style type="text/css">
        <?php if (is_file($file = \Yii::getAlias('@frontend/web/css/print/common.css'))) {
            echo $this->renderFile($file);
        }
        if (is_file($file = \Yii::getAlias('@frontend/web/css/print/documents-invoice.css'))) {
            echo $this->renderFile($file);
        } ?>
        .auto_tpl {
            font-style: italic;
            font-weight: normal;
        }
    </style>

    <div class="page-content-in p-center pad-pdf-p" style="box-sizing: content-box;">
        <table class="no-b m-t" style="width: 100%; margin-top: 0;">
            <tr>
                <td class="font-bold print-title"
                    style="padding-left: 0;padding-bottom: 12px;"><?= $model->contractor_name_short; ?>
                </td>
                <td style="width: 325px; text-align: right">
                    <?php if ($logoLink) : ?>
                        <img class="pull-right" src="<?= $logoLink ?>" alt="">
                    <?php endif; ?>
                </td>
            </tr>
        </table>

        <table class="t-p m-t-m" style="width: 100%; border: none">
            <tr>
                <td colspan="2"
                    style="border-bottom: none"><?= $model->company->mainCheckingAccountant->bank_name; ?></td>
                <td style="text-align: left; padding-left: 2px; width: 8%; vertical-align: top;">
                    БИК
                </td>
                <td style="border-bottom: none; text-align: left; padding: 2px 0 0 2px; vertical-align: top;">
                    <?= $model->company->mainCheckingAccountant->bik; ?><br/>
                </td>
            </tr>

            <tr>
                <td colspan="2" style="border-top: none"><br/>Банк получателя</td>
                <td style="text-align: left; padding-left: 2px; vertical-align: top;">
                    Сч. №
                </td>
                <td style="border-top: 0;text-align: left; padding: 2px 0 0 2px; vertical-align: top;"><?= $model->company->mainCheckingAccountant->ks; ?></td>
            </tr>

            <tr>
                <td style="width: 170px;">ИНН <?= $model->company->inn; ?></td>
                <td style="width: 189px;">КПП <?= $model->company->kpp; ?></td>
                <td rowspan="2"
                    style="text-align: left; padding-left: 2px; vertical-align: top;">
                    Сч. №
                </td>
                <td rowspan="2"
                    style="text-align: left; padding: 2px 0 0 2px; vertical-align: top;"><?= $model->company->mainCheckingAccountant->rs; ?></td>
            </tr>
            <tr>
                <td colspan="2"><?= $model->company->getTitle(true); ?><br/><br/>Получатель
                </td>
            </tr>
        </table>

        <h3>
            Заказ № <?= $model->fullNumber; ?> от <?= $dateFormatted; ?>
        </h3>

        <div style="border-bottom: 2px solid #000000; padding: 2px 4px;"></div>

        <table class="no-b" style="width: 100%; margin-bottom: 5px;">
            <tr>
                <td class="txt-t">Поставщик<br/>(Исполнитель):</td>
                <td class="txt-b">
                    <?= $model->company->getTitle(true); ?>,
                    ИНН <?= $model->company->inn; ?><?= !empty($model->company->kpp) ? ', КПП' . $model->company->kpp : ''; ?>
                    , <?= $model->company->getAddressLegalFull(); ?>
                </td>
            </tr>
            <tr>
                <td class="txt-t">Покупатель<br/>(Заказчик):</td>
                <td class="txt-b">
                    <?= $model->contractor_name_short; ?>,
                    <?= ($model->contractor->face_type == Contractor::TYPE_LEGAL_PERSON) ? 'ИНН ' . $model->contractor_inn . ',' : '' ?>
                    <?= ($model->contractor->face_type == Contractor::TYPE_LEGAL_PERSON && !empty($model->contractor_kpp)) ? 'КПП ' . $model->contractor_kpp . ',' : '' ?>
                    <?= $model->contractor_address_legal_full; ?>
                </td>
            </tr>
            <?php if ($model->basis_document_name && $model->basis_document_number && $model->basis_document_date) : ?>
                <tr>
                    <td class="txt-t">Основание:</td>
                    <td class="txt-b">
                        <?= $model->agreementType ? $model->agreementType->name : 'Договор' ?>
                        № <?= Html::encode($model->basis_document_number) ?>
                        от <?= DateHelper::format($model->basis_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                    </td>
                </tr>
            <?php endif ?>
        </table>

        <?= $this->render('view/pdf_orders_table', [
            'model' => $model,
        ]); ?>

        <?php if ($model->contractor->order_currency == 0) : ?>
            <div class="txt-9-b">Всего
                наименований <?= count($model->orderDocumentProducts); ?>, на
                сумму <?= TextHelper::invoiceMoneyFormat($model->total_amount, 2); ?> руб.
            </div>

            <div class="txt-9-b">
                Всего к оплате: <?= TextHelper::mb_ucfirst(TextHelper::amountToWords($model->total_amount / 100)); ?>
            </div>
        <?php else : ?>
            <?php $exchange_value = Currency::find()
                ->where(['id' => $model->contractor->order_currency])
                ->one()->current_value;
            $order_value = round(($model->total_amount / $exchange_value), 2); ?>
            <div class="txt-9">Всего
                наименований <?= count($model->orderDocumentProducts); ?>, на
                сумму <?= TextHelper::invoiceMoneyFormat($order_value, 2); ?>
                <?= Contractor::$CURRENCY_CAPTION[$model->contractor->order_currency] ?>
            </div>
        <?php endif; ?>

        <div style="border-bottom: 2px solid #000000; padding: 2px 4px;"></div>

        <div class="bg-seal"
             style="background: url('<?= $printLink ?>'); background-repeat: no-repeat; background-position: right center; height: 230px; width: 675px; -webkit-print-color-adjust: exact;">
            <div class="row">
                <div class="col-md-12">
                    <br/>
                    <br/>
                    <table class="podp-r va-bottom message"
                           style="width: 675px; table-layout: fixed; -webkit-print-color-adjust: exact;">
                        <col width="13%"/>
                        <col width="3%"/>
                        <col width="24%"/>
                        <col width="3%"/>
                        <col width="24%"/>
                        <col width="3%"/>
                        <col width="30%"/>
                        <tr>
                            <td class="txt-9-b"
                                style="width: 13%; border: none; padding: 0;">
                                Руководитель:
                            </td>
                            <td style="width: 3%; border: none;"></td>
                            <td class="pad-line"
                                style="width: 24%; border: none; border-bottom: 1px solid #000000;text-align: center; margin: 0 0 0 20px;"><?= $model->contractor_director_post_name; ?></td>
                            <td style="width: 3%; border: none;">
                            </td>
                            <td class="pad-line"
                                style="width: 24%; border: none; border-bottom: 1px solid #000000;text-align: center; position: relative; height: 40px!important;"></td>
                            <td style="width: 3%; border: none;"></td>
                            <td class="pad-line"
                                style="width: 30%; border: none; border-bottom: 1px solid #000000;text-align: center; padding-top: 30px!important;">
                                <?= $model->contractor_director_name; ?>
                            </td>
                        </tr>
                        <tr class="small-txt va-top">
                            <td style="width: 13%; border: none; padding: 0;width: 15%"></td>
                            <td style="width: 3%; border: none;"></td>
                            <td style="width: 24%; border: none; text-align: center; padding-top: 0;margin-top: 0; vertical-align: top;">
                                должность
                            </td>
                            <td style="width: 3%; border: none;"></td>
                            <td style="width: 24%%; border: none; text-align: center; vertical-align: top;">подпись
                            </td>
                            <td style="width: 3%; border: none;"></td>
                            <td style="width: 30%; border: none; text-align: center; vertical-align: top;">
                                расшифровка
                                подписи
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>

<?php if (isset($multiple) && (int)array_pop($multiple) !== $model->id): ?>
    <pagebreak/>
<?php endif; ?>