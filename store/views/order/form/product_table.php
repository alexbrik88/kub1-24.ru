<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 11.02.2018
 * Time: 18:03
 */

use common\models\document\OrderDocument;
use common\models\product\Product;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;
use common\models\Company;
use frontend\widgets\TableConfigWidget;
use common\models\employee\Employee;

/* @var $this yii\web\View */
/* @var $model OrderDocument */
/* @var $company Company */
/* @var $user Employee */
?>
<?php if ($company->store_has_discount || $company->store_discount_type == Company::DISCOUNT_TYPE_PERCENT): ?>
    <?= Html::hiddenInput(null, intval($company->store_discount_from_amount), [
        'class' => 'discount-from-amount',
    ]); ?>
    <?= Html::hiddenInput(null, intval($company->store_discount), [
        'class' => 'discount-percent',
    ]); ?>
<?php endif; ?>
<?php Pjax::begin([
    'linkSelector' => false,
    'enablePushState' => false,
    'timeout' => 5000,
    'options' => [
        'id' => 'product-table-order-document',
        'class' => 'portlet',
        'data-url' => Url::to(['contractor-product']),
    ],
]); ?>
    <table id="table-for-order-document"
           class="table table-striped table-bordered table-hover account_table last-line-table">
        <thead>
        <tr class="heading" role="row">
            <th width="5%" tabindex="0" rowspan="1" colspan="1">
            </th>
            <th width="25%" tabindex="0" rowspan="1" colspan="1">
                Наименование
            </th>
            <th width="15%" class="" tabindex="0" rowspan="1" colspan="1">
                Артикул
            </th>
            <th width="10%" class="" tabindex="0" rowspan="1" colspan="1">
                Количество
            </th>
            <th width="5%" class="" tabindex="0" rowspan="1" colspan="1">
                Ед.измерения
            </th>
            <th width="5%" class="" tabindex="0" rowspan="1" colspan="1">
                Доступно
            </th>
            <th width="5%" class="" tabindex="0" rowspan="1" colspan="1">
                Вес (кг)
            </th>
            <th width="15%" class="" tabindex="0" rowspan="1" colspan="1">
                Цена
            </th>
            <th width="15%" class="discount_column<?= $model->has_discount ? '' : ' hidden'; ?>" tabindex="0"
                rowspan="1" colspan="1">
                Скидка %
            </th>
            <th width="15%" class="discount_column<?= $model->has_discount ? '' : ' hidden'; ?>" tabindex="0"
                rowspan="1" colspan="1">
                Цена со скидкой
            </th>
            <th width="15%" class="" tabindex="0" rowspan="1" colspan="1">
                Сумма
            </th>
        </tr>
        </thead>
        <tbody id="table-product-list-body">
        <?php foreach ($model->orderDocumentProducts as $key => $orderDocumentProduct): ?>
            <?= $this->render('order_document_product_row', [
                'orderDocumentProduct' => $orderDocumentProduct,
                'number' => $key,
                'model' => $model,
                'company' => $company,
                'user' => $user,
            ]); ?>
        <?php endforeach; ?>
        <?= $this->render('add_order_row', [
            'hasOrders' => (boolean)$model->orderDocumentProducts,
            'hasDiscount' => (boolean)$model->has_discount,
            'company' => $company,
            'user' => $user,
        ]) ?>
        <tr class="template disabled-row" role="row">
            <td class="product-delete">
                <span class="icon-close remove-product-from-order-document"></span>
            </td>
            <td>
                <input type="text" class="product-title form-control tooltip-product" name="orderArray[][title]"
                       style="width: 100%;">
            </td>
            <td class="product-article"></td>
            <td>
                <input disabled="disabled" type="hidden" class="tax-rate"
                       value="0"/>
                <input disabled="disabled" type="hidden" class="product-model"
                       name="orderArray[][model]"
                       value="<?= Product::tableName(); ?>"/>
                <input disabled="disabled" type="hidden" class="product-id"
                       name="orderArray[][id]" value="0"/>
                <input disabled="disabled" type="number" min="0" step="any"
                       class="form-control product-count"
                       name="orderArray[][count]" value="1" data-value="1"/>
                <span class="price-one-with-nds hidden">0</span>
                <?= Html::hiddenInput('orderArray[][priceOneWithVat]', 0, [
                    'class' => 'form-control price-one-with-nds-input hidden',
                    'disabled' => 'disabled',
                ]); ?>
                <span class="product-no-count hidden"><?= Product::DEFAULT_VALUE ?></span>
            </td>
            <td class="product-unit-name"></td>
            <td class="product-available"></td>
            <td class="product-weight"></td>
            <td class="price-one">0</td>
            <td class="discount discount_column<?= $model->has_discount ? '' : ' hidden'; ?>">
                <span class="discount-percent">0</span>
            </td>
            <td class="discount_column<?= $model->has_discount ? '' : ' hidden'; ?>">
                <span class="price-one-with-nds-and-discount">0</span>
            </td>
            <td class="price-with-nds" style="text-align: right;border-bottom: 1px solid #ddd;">0</td>
        </tr>
        <tr class="button-add-line" role="row">
            <td class="" colspan="" style="border-right: none;">
                <div class="portlet pull-left control-panel button-width-table">
                    <div class="btn-group pull-right" style="display: inline-block; padding-top: 7px">
                        <span class="btn yellow btn-add-line-table">
                            <i class="pull-left fa icon fa-plus-circle"></i>
                        </span>
                    </div>
                </div>
            </td>
            <td class="" colspan="11" style="padding-top: 12px;">
            </td>
        </tr>
        </tbody>
    </table>
<?php Pjax::end(); ?>