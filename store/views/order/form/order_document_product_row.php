<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 12.02.2018
 * Time: 11:32
 */

use common\components\TextHelper;
use common\models\product\Product;
use frontend\models\Documents;
use yii\helpers\Html;
use common\models\document\OrderDocumentProduct;
use common\models\document\OrderDocument;
use common\models\Company;
use common\models\employee\Employee;

/* @var OrderDocumentProduct $orderDocumentProduct */
/* @var integer $number */
/* @var OrderDocument $model */
/* @var $company Company */
/* @var $user Employee */

$baseName = 'orderArray[' . $number . ']';
$id = $orderDocumentProduct->isNewRecord ? $orderDocumentProduct->product_id : $orderDocumentProduct->id;
$prodType = $orderDocumentProduct->productionType;
if ($company->hasNds() && $company->isNdsExclude) {
    $price = $orderDocumentProduct->base_price_no_vat;
    $priceOne = $orderDocumentProduct->selling_price_no_vat;
    $amount = $orderDocumentProduct->amount_sales_no_vat;
} else {
    $price = $orderDocumentProduct->base_price_with_vat;
    $priceOne = $orderDocumentProduct->selling_price_with_vat;
    $amount = $orderDocumentProduct->amount_sales_with_vat;
}
?>

<tr id="model_<?= $orderDocumentProduct->product_id; ?>" class="product-row" role="row">
    <td class="product-delete">
        <span class="icon-close remove-product-from-order-document"
              data-id="<?= $orderDocumentProduct->product_id; ?>"></span>
    </td>
    <td style="position: relative;">
        <input type="text" class="product-title form-control tooltip-product"
               style="padding-right: 25px; width: 100%;"
               name="<?= $baseName; ?>[title]"
               data-value="<?= $orderDocumentProduct->productTitle; ?>"
               value="<?= $orderDocumentProduct->productTitle; ?>">
        <span class="product-title-clear">×</span>
    </td>
    <td class="product-article">
        <?= $orderDocumentProduct->product->article ? $orderDocumentProduct->product->article : '---'; ?>
    </td>
    <td>
        <input type="hidden" class="tax-rate" disabled="disabled"
               value="<?= $orderDocumentProduct->saleTaxRate->rate; ?>"/>

        <input type="hidden" class="product-model"
               name="<?= $baseName; ?>[model]"
               value="<?= $orderDocumentProduct->isNewRecord ? Product::tableName() : $orderDocumentProduct->tableName(); ?>"/>
        <input type="hidden" class="product-id"
               name="<?= $baseName; ?>[id]" value="<?= $id; ?>"/>
        <span class="price-one-with-nds hidden"><?= TextHelper::moneyFormatFromIntToFloat($priceOne); ?></span>
        <?= Html::hiddenInput($baseName . '[priceOneWithVat]', TextHelper::moneyFormatFromIntToFloat($priceOne), [
            'class' => 'form-control price-one-with-nds-input hidden',
            'data-price' => $priceOne,
            'min' => 0,
        ]); ?>
        <?php if (!$orderDocumentProduct->unit || $orderDocumentProduct->unit->name == Product::DEFAULT_VALUE) : ?>
            <?= Html::hiddenInput($baseName . '[count]', 1, [
                'class' => 'product-count',
                'data-value' => 1,
            ]) ?>
            <span><?= Product::DEFAULT_VALUE ?></span>
        <?php else : ?>
            <input type="number" min="0" step="any" name="<?= $baseName; ?>[count]"
                   value="<?= $orderDocumentProduct->quantity; ?>"
                   data-value="<?= $orderDocumentProduct->quantity ?>"
                   class="product-count form-control"/>
        <?php endif ?>
    </td>
    <td class="product-unit-name"><?= $orderDocumentProduct->unit ? $orderDocumentProduct->unit->name : Product::DEFAULT_VALUE; ?></td>
    <td class="product-available"><?= $orderDocumentProduct->getProductAvailable($model->id); ?></td>
    <td class="product-weight">
        <?= $orderDocumentProduct->product->weight ? $orderDocumentProduct->product->weight : '---'; ?>
    </td>
    <td class="price-one" data-price="<?= TextHelper::moneyFormatFromIntToFloat($price) ?>">
        <?= TextHelper::moneyFormatFromIntToFloat($price) ?>
    </td>
    <td class="discount discount_column<?= $model->has_discount ? '' : ' hidden'; ?>">
        <span class="discount-percent"><?= $orderDocumentProduct->discount; ?></span>
    </td>
    <td class="discount_column<?= $model->has_discount ? '' : ' hidden'; ?>">
        <span class="price-one-with-nds-and-discount"><?= TextHelper::moneyFormatFromIntToFloat($priceOne) ?></span>
    </td>
    <td class="price-with-nds" style="text-align: right;">
        <?= TextHelper::moneyFormatFromIntToFloat($amount); ?>
    </td>
</tr>