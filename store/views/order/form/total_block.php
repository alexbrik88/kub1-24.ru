<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 13.02.2018
 * Time: 17:46
 */

use common\components\TextHelper;
use common\models\document\OrderDocument;
use common\models\employee\Employee;

/* @var $this yii\web\View */
/* @var $model OrderDocument */
/* @var $user Employee */

?>
<div class="portlet pull-right" id="invoice-sum">
    <table class="table table-resume">
        <tbody>
        <tr role="row" class="discount_column<?= $model->has_discount ? '' : ' hidden'; ?>">
            <td><b>Сумма скидки:</b></td>
            <td id="discount_sum"><?= TextHelper::moneyFormatFromIntToFloat($model->getDiscountSum()); ?></td>
        </tr>
        <tr role="row">
            <td><b>Итого:</b></td>
            <td id="total_price"><?= TextHelper::moneyFormatFromIntToFloat($model->total_amount); ?></td>
        </tr>
        <tr class="" role="row">
            <td><b>Вес (кг):</b></td>
            <td id="total_mass">
                <?= $model->getTotalWeight(); ?>
            </td>
        </tr>
        </tbody>
    </table>
</div>