<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 11.02.2018
 * Time: 17:22
 */

use common\models\document\OrderDocument;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use kartik\select2\Select2;
use common\models\Contractor;
use frontend\widgets\ConfirmModalWidget;
use yii\helpers\Url;
use common\components\date\DateHelper;
use common\models\Company;

/* @var $this yii\web\View
 * @var $model OrderDocument
 * @var $form ActiveForm
 * @var $company Company
 */
?>
<?= Html::activeHiddenInput($model, 'production_type', [
    'class' => 'order_document-production-type-hidden',
    'value' => '',
]); ?>
<div class="row">
    <div class="col-xs-12 pad0 col-lg-7">
        <div class="col-xs-12 pad0" id="tooltip_requisites_block" style="width: 500px;">
            <div class="col-lg-12" style="max-width: 590px">
                <div class="form-group row">
                    <div class="col-sm-4" style="max-width: 175px">
                        <label class="control-label">
                            Поставщик
                            <span class="required" aria-required="true">*</span>:
                        </label>
                    </div>
                    <div id="select-existing-contractor" class="col-sm-8"
                         style="max-width: 500px;">
                        <?= $form->field($model, 'company_id', ['template' => "{input}", 'options' => [
                            'class' => '',
                        ]])->widget(Select2::classname(), [
                            'data' => [$company->id => $company->getTitle(true)],
                            'options' => [
                                'class' => 'form-control contractor-select',
                                'disabled' => true,
                                'prompt' => '',
                            ],
                        ]); ?>
                    </div>
                </div>
            </div>
            <div class="col-xs-12" style="max-width: 590px">
                <div class="form-group required row">
                    <div class="col-xs-12 col-sm-4"
                         style="max-width: 175px">
                        <label class="control-label">
                            Основание
                        </label>
                    </div>
                    <div class="col-xs-12 col-sm-8"
                         style="max-width: 500px;">
                        <?= $this->render('basis_document', [
                            'model' => $model,
                        ]); ?>
                    </div>
                </div>
            </div>
            <div class="col-xs-12" style="max-width: 590px">
                <!-- start -->
                <div class="row">
                    <div class="col-xs-12 pad0" style="margin-bottom: 15px; max-width: 328px">
                        <div class="col-xs-12" style="max-width: 175px;">
                            <label for="invoice-payment_limit_date" class="control-label">
                                Отгрузить до
                                <span class="required" aria-required="true">*</span>:
                            </label>
                        </div>
                        <div class="col-xs-12" style="max-width: 153px;padding-left: 7px;">
                            <div class="input-icon">
                                <i class="fa fa-calendar"></i>
                                <?= Html::activeTextInput($model, 'ship_up_to_date', [
                                    'class' => 'form-control date-picker',
                                    'data-date-viewmode' => 'years',
                                    'value' => DateHelper::format($model->ship_up_to_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE),
                                ]); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>