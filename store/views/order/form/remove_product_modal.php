<div id="modal-remove-one-product" class="fade modal" role="dialog" tabindex="-1" aria-hidden="true"
     style="display: none; margin-top: -45px;">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-body" style="margin-bottom: 15px;text-align: center;font-size: 16px;">
                    <div class="row">Вы уверены, что хотите удалить эту позицию из заказа?</div>
                </div>
                <div class="form-actions row">
                    <div class="col-xs-6">
                        <button type="button" data-dismiss="modal" class="btn darkblue pull-right yes"
                                style="width: 80px;color: white;">ДА
                        </button>
                    </div>
                    <div class="col-xs-6">
                        <button type="button" data-dismiss="modal" class="btn darkblue"
                                style="width: 80px;color: white;">НЕТ
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>