<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 12.02.2018
 * Time: 19:00
 */

use common\models\document\OrderDocument;
use yii\widgets\Pjax;

/* @var $model OrderDocument */
?>

<div class="modal fade" id="add-from-exists" tabindex="-1" role="modal" aria-hidden="true" style="margin-top: 21%;">
    <div class="modal-dialog modal-dialog-middle">
        <div class="modal-content to-invoice-content">
            <?php Pjax::begin([
                'id' => 'pjax-product-grid',
                'linkSelector' => false,
                'timeout' => 10000,
                'enablePushState' => false,
                'enableReplaceState' => false,
            ]); ?>

            <?php Pjax::end(); ?>
        </div>
    </div>
</div>