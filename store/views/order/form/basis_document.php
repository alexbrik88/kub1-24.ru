<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 11.02.2018
 * Time: 17:51
 */

use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\widgets\Pjax;

$agreementDropDownList = ['' => '---'];
$outnds = 0;
if ($model->contractor) {
    $agreementArray = $model->contractor->getAgreements()->joinWith('agreementType')->orderBy([
        'agreement_type.name' => SORT_ASC,
        'agreement.document_date' => SORT_DESC,
    ])->all();
    $agreementDropDownList += ArrayHelper::map($agreementArray, 'listItemValue', 'listItemName');
    if ($model->contractor->isOutInvoiceHasNds) {
        $outnds = 1;
    }
}

Pjax::begin([
    'id' => 'agreement-pjax-container',
    'enablePushState' => false,
    'linkSelector' => false,
    'options' => [
        'data' => [
            'url' => Url::to(['basis-document']),
        ]
    ]
]);

echo Select2::widget([
    'id' => 'invoice-agreement',
    'model' => $model,
    'attribute' => 'agreement',
    'data' => $agreementDropDownList,
    'options' => [
        'data' => [
            'delay' => !empty($delay) ? $delay : 10,
            'refresh' => empty($refresh) ? 'false' : 'true',
            'outnds' => $outnds,
        ]
    ]
]);

Pjax::end();