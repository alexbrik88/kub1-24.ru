<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 14.02.2018
 * Time: 7:04
 */

use common\models\Company;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\document\OrderDocument;
use common\models\store\StoreUser;

/* @var $this yii\web\View */
/* @var $model OrderDocument */
/* @var $company Company */
/* @var $user StoreUser */

$user = Yii::$app->user->identity;
?>

<div class="form-actions">
    <div class="row action-buttons">
        <div class="spinner-button col-sm-1 col-xs-1">
            <?php if ($user->company): ?>
                <?= Html::button('Сохранить', [
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                    'data-toggle' => 'modal',
                    'href' => '#save-modal',
                ]); ?>
                <?= Html::button('<span class="ico-Save-smart-pls fs"></span>', [
                    'class' => 'btn darkblue widthe-100 hidden-lg',
                    'data-toggle' => 'modal',
                    'href' => '#save-modal',
                    'title' => 'Сохранить',
                ]); ?>
                <div id="save-modal" class="confirm-modal fade modal in" role="dialog" tabindex="-1"
                     aria-hidden="false">
                    <div class="modal-dialog ">
                        <div class="modal-content">
                            <div class="modal-body">
                                <div class="form-body">
                                    <div class="row">
                                        Вы находитесь в просмотровом кабинете.<br>
                                        Поэтому тут сохранить заказ нельзя.
                                    </div>
                                </div>
                                <div class="form-actions row">
                                    <div class="col-xs-12 text-center">
                                        <button type="button" class="btn darkblue" data-dismiss="modal">ОК</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            <?php else: ?>
                <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs create-invoice mt-ladda-btn ladda-button'
                        . (($company->strict_mode == Company::ON_STRICT_MODE) ? ' company-in-strict-mode' : ''),
                    'data-style' => 'expand-right',
                ]); ?>
                <?= Html::submitButton('<span class="ico-Save-smart-pls fs"></span>', [
                    'class' => 'btn darkblue widthe-100 hidden-lg create-invoice'
                        . (($company->strict_mode == Company::ON_STRICT_MODE) ? ' company-in-strict-mode' : ''),
                    'title' => 'Сохранить',
                ]); ?>
            <?php endif; ?>
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
        <div class="spinner-button col-sm-1 col-xs-1">
            <?php $cancelUrl = Url::previous('order_form_back') ?: Url::previous('lastPage');
            echo Html::a('Отменить', $cancelUrl, [
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs mt-ladda-btn ladda-button',
                'data-style' => 'expand-right',
            ]);
            echo Html::a('<span class="ico-Cancel-smart-pls fs"></span>', $cancelUrl, [
                'class' => 'btn darkblue widthe-100 hidden-lg',
                'title' => 'Отменить',
            ]); ?>
        </div>
        <div class="button-bottom-page-lg col-sm-1 col-xs-1">
        </div>
    </div>
</div>