<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 10.02.2018
 * Time: 18:07
 */

use yii\bootstrap\Html;
use common\components\date\DateHelper;
use common\models\document\OrderDocument;
use yii\bootstrap\ActiveForm;
use common\models\document\status\OrderDocumentStatus;

/* @var $this yii\web\View
 * @var $model OrderDocument
 * @var $form ActiveForm
 */
?>
<div class="portlet-title">
    <div class="caption" style="width: 100%">
        <table cellpadding="0" cellspacing="0" style="width: 100%;">
            <tr>
                <td valign="middle" style="white-space:nowrap;">
                    <span>
                        <?= Html::hiddenInput('', (int)$model->isNewRecord, [
                            'id' => 'isNewRecord',
                        ]) ?>

                        <?php if ($model->isNewRecord) : ?>
                            Новый заказ
                        <?php else : ?>
                            Заказ
                            № <?= $model->fullNumber ?>
                            от <?= DateHelper::format($model->document_date,
                                   DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) ?>
                        <?php endif ?>

                        <br class="box-br">
                    </span>
                </td>
                <td style="width: 1%;">
                    <?= Html::tag('div', $model->status->name, [
                        'title' => 'Статус заказа',
                        'style' => 'width: 150px; height: 34px; padding: 6px 12px; font-size: 14px; font-weight: normal; color: #ffffff;line-height: 22px;' .
                                   'background-color: ' . $model->status->color,
                    ]) ?>
                </td>
            </tr>
        </table>
    </div>
</div>
<?php $this->registerJs('
    $(document).ready(function() {
        var $orderDocumentStatus = $(".order-document-status");
        $orderDocumentStatus.select2({
            templateResult: formatState,
            minimumResultsForSearch: -1,
            escapeMarkup: function(markup) {
                return markup;
            }
        });
        var $select2container = $("#select2-account-number-container");
        $select2container.css("background-color", $(".order-document-status option[value=" + $orderDocumentStatus.val() + "]").data("color"));

        function formatState (state) {
            if (!state.id) {
              return state.text;
            }
            var $orderDocumentStatusOption = $(".order-document-status option[value=" + state.id + "]");
            var $state = "<span class=\"order-document-status-option\" style=\"background-color: " + $orderDocumentStatusOption.data("color") + "\"></span>" + state.text

            return $state;
        };

        $orderDocumentStatus.on("select2:select", function (e) {
            $select2container.css("background-color", $(".order-document-status option[value=" + e.params.data.id + "]").data("color"));
        });
    });
'); ?>
