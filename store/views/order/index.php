<?php

use frontend\models\Documents;
use yii\bootstrap\Html;
use yii\helpers\Url;
use frontend\widgets\RangeButtonWidget;
use common\components\TextHelper;
use yii\widgets\ActiveForm;
use frontend\modules\documents\models\OrderDocumentSearch;
use common\models\document\OrderDocument;
use common\components\grid\GridView;
use common\components\date\DateHelper;
use common\components\grid\DropDownSearchDataColumn;
use frontend\modules\documents\components\FilterHelper;
use yii\bootstrap\Modal;
use common\models\store\StoreUser;

/**
 * @var $this yii\web\View
 * @var $searchModel OrderDocumentSearch
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var $dashBoardData []
 * @var $storeUser StoreUser
 */

$company = $storeUser->company ? $storeUser->company : $storeUser->getCurrentKubCompany();
$this->title = 'Заказы у ' . $company->getTitle(true);

$newRequests = Yii::$app->user->identity->getNewRequests()->all();
$newRequestsCount = Html::tag('span', count($newRequests), [
    'id' => 'new-company-request-count',
]);
?>
<div class="portlet box">
    <?php if ($storeUser->isContractorActive || $storeUser->company_id !== null):?>
    <div class="btn-group pull-right title-buttons">
        <?= Html::a('<i class="fa fa-plus"></i> Добавить', Url::to(['create']), [
            'class' => 'btn yellow',
        ]); ?>
    </div>
    <?php endif; ?>
    <h3 class="page-title"><?= Html::encode($this->title); ?></h3>
</div>
<div class="row statistic-block">
    <div class="col-sm-3">
        <div class="statistic-cell" style="background-color: #dfba49; height: 120px;">
            <i class="fa fa-comments"></i>
            <div class="stat-value">
                <?= TextHelper::invoiceMoneyFormat($dashBoardData[0]['amount'], 2); ?>
                <i class="fa fa-rub"></i>
            </div>
            <div class="stat-label">
                <?= $dashBoardData[0]['name']; ?>
            </div>
            <div class="stat-footer" style="background-color: #d4af46;">
                Количество заказов: <?= $dashBoardData[0]['count']; ?>
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="statistic-cell" style="background-color: #f3565d; height: 120px;">
            <i class="fa fa-comments"></i>
            <div class="stat-value">
                <?= TextHelper::invoiceMoneyFormat($dashBoardData[1]['amount'], 2); ?>
                <i class="fa fa-rub"></i>
            </div>
            <div class="stat-label">
                <?= $dashBoardData[1]['name']; ?>
            </div>
            <div class="stat-footer" style="background-color: #e25157;">
                Количество заказов: <?= $dashBoardData[1]['count']; ?>
            </div>
        </div>
    </div>
    <div class="col-sm-3">
        <div class="statistic-cell" style="background-color: #45b6af; height: 120px;">
            <i class="fa fa-comments"></i>
            <div class="stat-value">
                <?= TextHelper::invoiceMoneyFormat($dashBoardData[2]['amount'], 2); ?>
                <i class="fa fa-rub"></i>
            </div>
            <div class="stat-label">
                <?= $dashBoardData[2]['name']; ?>
            </div>
            <div class="stat-footer" style="background-color: #43a9a2;">
                Количество заказов: <?= $dashBoardData[2]['count']; ?>
            </div>
        </div>
    </div>

    <div class="col-sm-3 col-sm-3">
        <div class="dashboard-stat" style="position: relative;">
            <?= RangeButtonWidget::widget(['cssClass' => 'doc-gray-button btn_select_days btn_row',]); ?>
        </div>
    </div>
</div>

<div class="portlet box darkblue" id="invoices-table">
    <div class="portlet-title">
        <div class="caption">
            Список заказов
        </div>
        <div class="tools search-tools tools_button col-md-4 col-sm-4">
            <div class="form-body">
                <?php $form = ActiveForm::begin([
                    'method' => 'GET',
                    'fieldConfig' => [
                        'template' => "{input}\n{error}",
                        'options' => [
                            'class' => '',
                        ],
                    ],
                ]); ?>
                <div class="search_cont">
                    <div class="wimax_input ">
                        <?= $form->field($searchModel, 'byNumber')->textInput([
                            'placeholder' => 'Номер заказа',
                        ]); ?>
                    </div>
                    <div class="wimax_button">
                        <?= Html::submitButton('НАЙТИ', [
                            'class' => 'btn btn__ins btn-sm default btn_marg_down green-haze',
                        ]) ?>
                    </div>
                </div>
                <?php $form->end(); ?>
            </div>
        </div>
    </div>
    <div class="portlet-body accounts-list">
        <div class="table-container" style="">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'filterModel' => $searchModel,
                'tableOptions' => [
                    'class' => 'table table-bordered table-hover dataTable documents_table status_nowrap invoice-table fix-thead',
                    'aria-describedby' => 'datatable_ajax_info',
                    'role' => 'grid',
                ],
                'headerRowOptions' => [
                    'class' => 'heading',
                ],
                'options' => [
                    'class' => 'dataTables_wrapper dataTables_extended_wrapper',
                ],
                'pager' => [
                    'options' => [
                        'class' => 'pagination pull-right',
                    ],
                ],
                'layout' => $this->render('@frontend/views/layouts/grid/layout', [
                    'totalCount' => $dataProvider->totalCount,
                    'scroll' => false,
                ]),
                'columns' => [
                    [
                        'header' => Html::checkbox('', false, [
                            'class' => 'joint-operation-main-checkbox',
                        ]),
                        'headerOptions' => [
                            'class' => 'text-center pad0',
                            'width' => '5%',
                        ],
                        'contentOptions' => [
                            'class' => 'text-center pad0-l pad0-r',
                        ],
                        'format' => 'raw',
                        'value' => function ($model) {
                            return Html::checkbox('OrderDocument[' . $model->id . '][checked]', false, [
                                'class' => 'joint-operation-checkbox',
                            ]);
                        },
                    ],
                    [
                        'attribute' => 'document_number',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '5%',
                        ],
                        'format' => 'html',
                        'value' => function ($model) {
                            return Html::a($model->fullNumber, ['/order/view', 'id' => $model->id,]);
                        },
                    ],
                    [
                        'attribute' => 'document_date',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '14%',
                        ],
                        'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
                    ],
                    [
                        'attribute' => 'company_id',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '14%',
                        ],
                        'value' => function ($model) {
                            return $model->company->getTitle(true);
                        },
                    ],
                    [
                        'attribute' => 'total_amount',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '14%',
                        ],
                        'contentOptions' => [
                            'style' => 'white-space: initial;',
                        ],
                        'format' => 'raw',
                        'value' => function ($model) {
                            return TextHelper::invoiceMoneyFormat($model->total_amount, 2);
                        },
                    ],
                    [
                        'attribute' => 'paymentSum',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '10%',
                        ],
                        'contentOptions' => [
                            'style' => 'white-space: initial;',
                        ],
                        'format' => 'raw',
                        'value' => function ($model) {
                            return $model->invoice ?
                                TextHelper::invoiceMoneyFormat($model->invoice->getPaidAmount(), 2) : '---';
                        },
                    ],
                    [
                        'attribute' => 'status_id',
                        'class' => DropDownSearchDataColumn::className(),
                        'headerOptions' => [
                            'class' => 'dropdown-filter',
                            'width' => '10%',
                        ],
                        'contentOptions' => [
                            'style' => 'white-space: initial;',
                        ],
                        'filter' => $searchModel->getStatusFilter(),
                        'format' => 'raw',
                        'value' => function ($model) {
                            return $model->status->name;
                        },
                    ],
                    [
                        'attribute' => 'ship_up_to_date',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '14%',
                        ],
                        'format' => ['date', 'php:' . DateHelper::FORMAT_USER_DATE],
                    ],
                    [
                        'attribute' => 'invoice_id',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '10%',
                        ],
                        'value' => function ($model) {
                            return $model->invoice ? '№ ' . $model->invoice->fullNumber : '';
                        }
                    ],
                    [
                        'label' => 'Акт',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '10%',
                        ],
                        'format' => 'html',
                        'value' => function ($model) {
                            $content = [];
                            if ($model->invoice) {
                                if ($docs = $model->invoice->acts) {
                                    foreach ($docs as $doc) {
                                        $content[] = '№ ' . $doc->fullNumber;
                                    }
                                }

                            }
                            return implode('<br>', $content);
                        }
                    ],
                    [
                        'label' => 'Товарная накладная',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '10%',
                        ],
                        'format' => 'html',
                        'value' => function ($model) {
                            $content = [];
                            if ($model->invoice) {
                                if ($docs = $model->invoice->packingLists) {
                                    foreach ($docs as $doc) {
                                        $content[] = '№ ' . $doc->fullNumber;
                                    }
                                }

                            }
                            return implode('<br>', $content);
                        }
                    ],
                    [
                        'label' => 'Счет фактура',
                        'headerOptions' => [
                            'class' => 'sorting',
                            'width' => '10%',
                        ],
                        'format' => 'html',
                        'value' => function ($model) {
                            $content = [];
                            if ($model->invoice) {
                                if ($docs = $model->invoice->invoiceFactures) {
                                    foreach ($docs as $doc) {
                                        $content[] = '№ ' . $doc->fullNumber;
                                    }
                                }

                            }
                            return implode('<br>', $content);
                        }
                    ],
                ],
            ]); ?>
        </div>
    </div>
</div>
