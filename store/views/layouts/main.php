<?php

use common\widgets\AjaxModalWidget;
use store\assets\AppAsset;
use store\widgets\MainMenuWidget;
use store\widgets\SideMenuWidget;
use yii\helpers\Html;
use lavrentiev\widgets\toastr\NotificationFlash;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
$wrapperCssClass = empty($this->params['layoutWrapperCssClass']) ? '' :
                   $this->params['layoutWrapperCssClass'];
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="/favicon.ico?i=1" type="image/x-icon">
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,700&subset=latin,cyrillic'
          rel='stylesheet' type='text/css'>
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:regular,italic,bold,bolditalic"
          rel="stylesheet" type="text/css"/>
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="page-header-fixed page-quick-sidebar-over-content home-page">
<?php $this->beginBody() ?>

<div class="black-screen"></div>

<?= MainMenuWidget::widget(); ?>

<div class="page-container">

    <?= SideMenuWidget::widget(); ?>

    <div class="page-content-wrapper">
        <div class="page-content <?= $wrapperCssClass ?>">
            <?= NotificationFlash::widget([
                'options' => [
                    'closeButton' => true,
                    'showDuration' => 1000,
                    'hideDuration' => 1000,
                    'timeOut' => 5000,
                    'extendedTimeOut' => 1000,
                    'positionClass' => NotificationFlash::POSITION_TOP_RIGHT,
                ],
            ]); ?>
            <div id="js-alert"></div>
            <div class="vitala"></div>
            <?php echo $content ?>
        </div>
    </div>
</div>

<div class="page-footer" id="page-footer">
    <div class="page-footer-inner">
        &copy; ООО "КУБ", <?= date('Y'); ?>
    </div>
    <div class="box-tech-support-tel">
        Техподдержка, тел: 8-800-500-54-36, e-mail: support@kub-24.ru
    </div>
    <div class="page-footer-inner pull-right box-l-t-c">
        <a href="http://kub-24.ru/TermsOfUseAll/TermsOfUseAll.pdf"
           target="_blank">Лицензионное соглашение</a>
        <a href="http://kub-24.ru/pomoshh" target="_blank" class="dis-hidden-700">Техническая
            поддержка</a>
        <a href="http://kub-24.ru/about" target="_blank" class="dis-hidden-700">Контакты</a>
    </div>
</div>

<?= AjaxModalWidget::widget(); ?>

<?php $this->endBody(); ?>
<script>
    jQuery(document).ready(function () {
        Metronic.init(); // init metronic core components
    });
</script>
</body>
</html>
<?php $this->endPage(); ?>
