<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use lavrentiev\widgets\toastr\NotificationFlash;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Войти';
$this->params['layoutWrapperCssClass'] = 'out-sf out-document out-act cash-order login-page';
?>
<?= NotificationFlash::widget([
    'options' => [
        'closeButton' => true,
        'showDuration' => 1000,
        'hideDuration' => 1000,
        'timeOut' => 5000,
        'extendedTimeOut' => 1000,
        'positionClass' => NotificationFlash::POSITION_TOP_RIGHT,
    ],
]); ?>

<h1><?= Html::encode($this->title) ?></h1>

<div class="row">
    <div class="col-lg-5">
        <?php $form = ActiveForm::begin([
            'id' => 'store-login-form',
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
        ]); ?>
        <?= $form->field($model, 'email') ?>
        <?= $form->field($model, 'password')->passwordInput() ?>
        <?= $form->field($model, 'rememberMe')->checkbox() ?>
        <div style="color:#999;margin:1em 0">
            Если Вы забыли пароль, то Вы можете <?= Html::a('сбросить его', ['site/request-password-reset']) ?>.
        </div>
        <div class="form-group">
            <?= Html::submitButton('Войти', ['class' => 'btn darkblue text-white', 'name' => 'login-button']) ?>
        </div>
        <?php ActiveForm::end(); ?>
    </div>
</div>
