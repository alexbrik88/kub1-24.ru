<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'У вас нет активных поставщимков.';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
</div>
