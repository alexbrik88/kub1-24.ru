<?php

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */
$cssFile = Yii::getAlias('@store/web/css/form-cors.css');
$protocol = Yii::$app->request->isSecureConnection ? 'https' : 'http';
$actionUrl = Url::toRoute(['/site/login'], $protocol);
$loaderSrc = Url::toRoute(['/img/ajax-loader.gif'], $protocol);
$styleUrl = Url::toRoute(['/css/form-cors.css', 'v' => filemtime($cssFile)], $protocol);
$resetUrl = Url::toRoute(['/site/request-password-reset'], $protocol);
?>

<link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=PT+Sans:regular,italic,bold,bolditalic"/>
<link rel="stylesheet" type="text/css" href="<?= $styleUrl ?>"/>

<div id="store-form-container">
    <div style="max-width: 270px; border: 1px solid #0076A4; margin: 0 auto;">
        <div style="padding: 12px; background-color: #0076A4; color: #fff; text-align: center;">
            Личный кабинет покупателя
        </div>
        <div style="padding: 10px;">
            <form id="store-login-form" method="post" action="<?= $actionUrl ?>">
                <?= Html::hiddenInput(Yii::$app->request->csrfParam, Yii::$app->request->getCsrfToken()); ?>
                <div class="field-loginform-email">
                    <input type="text" id="loginform-email" name="LoginForm[email]" placeholder="Ваш логин">
                    <div class="help-block help-block-error"></div>
                </div>
                <div class="field-loginform-password" style="margin: 15px 0;">
                    <input type="password" id="loginform-password" name="LoginForm[password]" placeholder="Ваш пароль">
                    <div class="help-block help-block-error"></div>
                </div>
                <div style="margin: 15px 0; text-align: right;">
                    <a href="<?= Url::toRoute(['/site/request-password-reset'], true) ?>">
                        Забыли пароль?
                    </a>
                </div>
                <button type="submit">
                    Войти
                    <span id="ajax-loader-gif" style="display: none;">
                        <img src="<?= $loaderSrc ?>" alt="" style="height: 14px; width: 14px;">
                    </span>
                </button>
            </form>
        </div>
    </div>
</div>
