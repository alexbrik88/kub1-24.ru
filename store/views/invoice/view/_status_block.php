<?php
use common\components\TextHelper;
use common\models\document\Invoice;
use common\models\document\PackingList;
use common\models\document\status\InvoiceStatus;
use common\models\product\Product;
use frontend\models\Documents;
use yii\bootstrap\Modal;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model Invoice */
/* @var $ioType integer */
/* @var $invoiceFlowCalc common\models\document\InvoiceFlowCalculator */
/* @var $useContractor string */


$status = $model->invoiceStatus;
$styleClass = $model->isOverdue() ? 'red' : $status->getStyleClass();
?>

<!-- _status_block -->
<div class="control-panel col-xs-12 pad0 pull-right">
    <!--    <div class="status-panel pull-right"> -->
    <div class="status-panel col-xs-12 pad0">
        <div class="col-xs-12 col-sm-3 pad3">
            <div class="btn full_w marg <?= $styleClass; ?>"
                 style="padding-left:0px; padding-right:0px;text-align: center; "
                 title="Дата изменения статуса">
                <?= date("d.m.Y", $model->invoice_status_updated_at); ?>
            </div>
        </div>
        <div class="col-xs-12 col-sm-9 pad0">
            <div class="col-xs-7 pad3">
                <div class="btn full_w marg <?= $styleClass; ?>"
                     title="Статус">
                    <?php if ($model->getIsFullyPaid()) : ?>
                        <span class="pull-left"><?= $model->getPaymentIcon() ?></span>
                    <?php else : ?>
                        <span class="icon pull-left <?= $status->getIcon(); ?>"></span>
                    <?php endif; ?>
                    <?= $status->name; ?>
                </div>
            </div>
            <div class="col-xs-5 pad3">
                <div class="btn full_w marg <?= $styleClass; ?>"
                     style="padding-left:0px; padding-right:0px;text-align: center; "
                     title="Сумма оплаты">
                    <i class="fa fa-rub"></i> <?= TextHelper::invoiceMoneyFormat($model->getPaidAmount(), 2); ?>
                </div>
            </div>
        </div>
    </div>
</div>
