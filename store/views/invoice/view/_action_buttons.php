<?php
use common\models\document\status\InvoiceStatus;
use frontend\models\Documents;
use frontend\widgets\ConfirmModalWidget;
use yii\bootstrap\Dropdown;
use yii\helpers\Html;
use yii\helpers\Url;

/** @var $this View */
/** @var $model \common\models\document\Invoice */

?>

<div class="row action-buttons buttons-fixed" id="buttons-fixed">
    <div class="button-bottom-page-lg col-sm-1 col-xs-1"></div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1"></div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1" style="width: 160px;">
        <?= Html::a('Печать', ['print', 'id' => $model->id], [
            'target' => '_blank',
            'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
        ]); ?>
        <?= Html::a('<i class="fa fa-print fa-2x"></i>', ['print', 'id' => $model->id], [
            'target' => '_blank',
            'title' => 'Печать',
            'class' => 'btn darkblue widthe-100 hidden-lg',
        ]); ?>
    </div>
    <div class="button-bottom-page-lg col-sm-1 col-xs-1" style="width: 160px;">
        <style>
            .dropdown-menu-mini {
                width: 100%;
                min-width: 98px;
                border-color: #4276a4 !important;
            }

            .dropdown-menu-mini a {
                padding: 7px 0px;
                text-align: center;
            }
        </style>
        <span class="dropup">
            <?= Html::a('Скачать', '#', [
                'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs dropdown-toggle',
                'data-toggle' => 'dropdown'
            ]); ?>
            <?= Html::a('<i class="glyphicon glyphicon-download" style="font-size: 17px;"></i>', '#', [
                'class' => 'btn darkblue widthe-100 hidden-lg dropdown-toggle',
                'data-toggle' => 'dropdown'
            ]); ?>
            <?= Dropdown::widget([
                'options' => [
                    'style' => 'left: 15px; width: 130px !important; min-width: auto; border: 1px solid rgb(66, 118, 164);',
                ],
                'items' => [
                    [
                        'label' => '<span style="display: inline-block;">PDF</span> файл',
                        'encode' => false,
                        'url' => ['pdf', 'id' => $model->id],
                        'linkOptions' => [
                            'target' => '_blank',
                        ]
                    ],
                    [
                        'label' => '<span style="display: inline-block;">Word</span> файл',
                        'encode' => false,
                        'url' => ['docx', 'id' => $model->id],
                        'linkOptions' => [
                            'target' => '_blank',
                        ]
                    ],
                ],
            ]); ?>
        </span>
    </div>
    <div class="col-sm-2">
        <?php if (!$model->is_deleted && $model->invoiceStatus->rejectAllowed() && false): ?>
            <?= ConfirmModalWidget::widget([
                'toggleButton' => [
                    'label' => 'Отменён',
                    'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs',
                ],
                'confirmUrl' => Url::to(['update-status',
                    'type' => $model->type, 'id' => $model->id, 'contractorId' => $contractorId,]),
                'message' => 'Вы уверены, что хотите отклонить этот счёт?',
                'confirmParams' => [
                    'status' => InvoiceStatus::STATUS_REJECTED,
                ],
            ]);
            ?>
            <?= ConfirmModalWidget::widget([
                'toggleButton' => [
                    'label' => '<span class="ico-Denied-smart-pls fs"></span>',
                    'title' => 'Отменён',
                    'class' => 'btn darkblue widthe-100 hidden-lg',
                ],
                'confirmUrl' => Url::to(['update-status',
                    'type' => $model->type, 'id' => $model->id, 'contractorId' => $contractorId,]),
                'message' => 'Вы уверены, что хотите отклонить этот счёт?',
                'confirmParams' => [
                    'status' => InvoiceStatus::STATUS_REJECTED,
                ],
            ]);
            ?>
        <?php endif; ?>
    </div>
</div>
