<?php
use common\components\date\DateHelper;
use common\models\document\Invoice;
use common\models\product\Product;
use frontend\models\Documents;
use frontend\modules\documents\components\Message;
use frontend\rbac\permissions;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model Invoice */
/* @var $message Message */
/* @var $ioType integer */
/* @var $dateFormatted string */
/* @var $useContractor string *
/* @var $newCompanyTemplate bool*/

$cashFlowData = [];
if ($model->cashBankFlows) {
    $dateArray = ArrayHelper::getColumn($model->cashBankFlows, 'date');
    $idArray = ArrayHelper::getColumn($model->cashBankFlows, 'id');
    $data = Html::beginForm(['/cash/bank/index'], 'post', ['style' => 'display: inline-block;']);
    foreach ($idArray as $flow_id) {
        $data .= Html::hiddenInput('flow_id[]', $flow_id);
    }
    $data .= Html::submitButton(implode(', ', $dateArray) . ' по Банку', ['class' => 'btn-as-link']);
    $data .= Html::endForm();
    $cashFlowData[] = $data;
}
if ($model->cashOrderFlows) {
    $dateArray = ArrayHelper::getColumn($model->cashOrderFlows, 'date');
    $idArray = ArrayHelper::getColumn($model->cashOrderFlows, 'id');
    $data = Html::beginForm(['/cash/order/index'], 'post', ['style' => 'display: inline-block;']);
    foreach ($idArray as $flow_id) {
        $data .= Html::hiddenInput('flow_id[]', $flow_id);
    }
    $data .= Html::submitButton(implode(', ', $dateArray) . ' по Кассе', ['class' => 'btn-as-link']);
    $data .= Html::endForm();
    $cashFlowData[] = $data;
}
if ($model->cashEmoneyFlows) {
    $dateArray = ArrayHelper::getColumn($model->cashEmoneyFlows, 'date');
    $idArray = ArrayHelper::getColumn($model->cashEmoneyFlows, 'id');
    $data = Html::beginForm(['/cash/e-money/index'], 'post', ['style' => 'display: inline-block;']);
    foreach ($idArray as $flow_id) {
        $data .= Html::hiddenInput('flow_id[]', $flow_id);
    }
    $data .= Html::submitButton(implode(', ', $dateArray) . ' по E-money', ['class' => 'btn-as-link']);
    $data .= Html::endForm();
    $cashFlowData[] = $data;
}
$canUpdate = false;
?>

<style>
    .main_inf_no-bord td {
        border: none !important;
    }
</style>

<!-- _main_info -->
<!--col-md-5 block-left-->
<div class="col-xs-12 col-lg-9 pull-right pad3" style="max-width: 720px;">
    <div class="portlet">
        <div class="customer-info" style="min-height:auto !important">
            <div class="portlet-body" style="border: 1px solid #ccc; padding: 5px;">
                <table class="table no_mrg_bottom">
                    <tr>
                        <td>
                            <span class="customer-characteristic">Поставщик:</span>
                            <span>
                                <?= Html::a($model->company_name_short) ?>
                            </span>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <span
                                class="customer-characteristic">Оплатить до:</span>
                            <span><?= DateHelper::format($model->payment_limit_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?></span>
                        </td>
                    </tr>

                    <?php if ($ioType == Documents::IO_TYPE_IN) : ?>
                        <tr>
                            <td>
                                <span class="customer-characteristic">Статья расходов:</span>
                                <span><?= $model->invoiceExpenditureItem !== null ? $model->invoiceExpenditureItem->name : 'не указано'; ?></span>
                            </td>
                        </tr>
                    <?php else : ?>
                        <tr>
                            <td>
                                <?php if ($model->basis_document_name && $model->basis_document_number && $model->basis_document_date) : ?>
                                    <span class="customer-characteristic">
                                        <?= $model->agreementType ? $model->agreementType->name : 'Договор' ?>
                                        :
                                    </span>
                                    № <?= Html::encode($model->basis_document_number) ?>
                                    от <?= DateHelper::format($model->basis_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                                <?php endif ?>
                            </td>
                        </tr>
                    <?php endif; ?>

                    <?php if ($cashFlowData) : ?>
                        <tr>
                            <td>
                                <span class="customer-characteristic">Оплата:</span>
                                <span><?= implode(', ', $cashFlowData) ?></span>
                            </td>
                        </tr>
                    <?php endif ?>
                </table>
            </div>
        </div>
    </div>
</div>
