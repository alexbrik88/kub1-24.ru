<?php
use common\components\TextHelper;
use common\models\product\Product;
use frontend\models\Documents;
use common\models\document\Invoice;

/* @var $this yii\web\View */
/* @var $model common\models\document\Invoice */
/* @var $ioType integer */

$hasNds = $model->hasNds;
$precision = $model->price_precision;
?>

<style>
    .bord-dark tr, .bord-dark td, .bord-dark th {
        border: 1px solid #000000;
    }
</style>

<div class="portlet">
    <table class="bord-dark" style="width: 99.9%; border:2px solid #000000">
        <thead>
        <tr style="text-align: center;">
            <th> №</th>
            <th> Наименование работ, услуг</th>
            <th> Кол-во</th>
            <th> Ед.</th>
            <th> Цена</th>
            <?php if ($model->has_discount) : ?>
                <th> Скидка %</th>
                <th> Цена со скидкой</th>
            <?php endif ?>
            <th> Сумма</th>
        </tr>
        </thead>
        <tbody class="bord-dark">
        <?php foreach ($model->orders as $order): ?>
            <?php
            $unitName = $order->unit ? $order->unit->name : Product::DEFAULT_VALUE;
            ?>
            <tr>
                <td style="text-align: center; width: 5%"><?= $order->number; ?></td>
                <td style=" width: 0">
                    <?= $order->product_title; ?>
                </td>
                <td style="text-align: right; width: 10%">
                    <?= $unitName == Product::DEFAULT_VALUE ? $unitName : str_replace('.', ',', $order->quantity); ?>
                </td>
                <td style="text-align: right; width: 7%"><?= $unitName ?></td>
                <?php if ($model->has_discount) : ?>
                    <td style="text-align: right; width: 13%">
                        <?= TextHelper::invoiceMoneyFormat($order->view_price_base, $precision); ?>
                    </td>
                    <td style="text-align: right; width: 13%">
                        <?= strtr($order->discount, ['.' => ',']) ?>
                    </td>
                <?php endif ?>
                <td style="text-align: right; width: 13%">
                    <?= TextHelper::invoiceMoneyFormat($order->view_price_one, $precision); ?>
                </td>
                <td style="text-align: right; width: 15%">
                    <?= TextHelper::invoiceMoneyFormat($order->view_total_amount, $precision); ?>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>
    <table class="it-b" style="width: 99.9%;">
        <?php if ($model->has_discount) : ?>
            <tr>
                <td width="430px" style="border: none"></td>
                <td class="txt-b2" style="text-align: right; border: none; width: 150px;">
                    Сумма скидки:
                </td>
                <td class="txt-b2" style="text-align: right; border: none; width: 124px;">
                    <b><?= TextHelper::invoiceMoneyFormat($model->view_total_discount, 2); ?></b>
                </td>
            </tr>
        <?php endif ?>
        <tr>
            <td width="430px" style="border: none"></td>
            <td class="txt-b2" style="text-align: right; border: none; width: 150px;">
                Итого:
            </td>
            <td class="txt-b2" style="text-align: right; border: none; width: 124px;">
                <b><?= TextHelper::invoiceMoneyFormat($model->view_total_amount, 2); ?></b>
            </td>
        </tr>
        <tr>
            <td width="430px" style="border: none"></td>
            <td class="txt-b2" style="text-align: right; border: none">
                <?= $model->ndsViewType->title; ?>
            </td>
            <td class="txt-b2" style="text-align: right; border: none;">
                <b><?= $hasNds ? TextHelper::invoiceMoneyFormat($model->view_total_nds, 2) : '-'; ?></b>
            </td>
        </tr>
        <tr>
            <td width="430px" style="border: none"></td>
            <td class="txt-b2" style="text-align: right; border: none">
                Всего к оплате:
            </td>
            <td class="txt-b2" style="text-align: right; border: none;">
                <b><?= TextHelper::invoiceMoneyFormat($model->view_total_with_nds, 2); ?></b>
            </td>
        </tr>
    </table>
</div>
