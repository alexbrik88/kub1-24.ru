<?php

use common\components\date\DateHelper;
use common\components\image\EasyThumbnailImage;
use common\components\TextHelper;
use common\models\Company;
use common\models\company\CompanyType;
use common\models\currency\Currency;
use common\models\document\Invoice;
use common\models\document\status\InvoiceStatus;
use common\widgets\Modal;
use frontend\models\Documents;
use frontend\modules\documents\assets\TooltipAsset;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\document\Invoice */
/* @var $orders common\models\document\Order[] */
/* @var $message Message */
/* @var $ioType integer */
/* @var $multiple [] */
/* @var $useContractor string */

$company = $model->company;

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'format' => 'd F Y г.',
    'monthInflected' => true,
    'date' => $model->document_date,
]);

$this->title = $model->printTitle;

$signatureLink = null;
$accountantSignatureLink = '';
if ($model->signed_by_name) {
    $accountantSignatureLink = $signatureLink = (!$model->employeeSignature) ? null :
        EasyThumbnailImage::thumbnailSrc($model->employeeSignature->file, 165, 50, EasyThumbnailImage::THUMBNAIL_INSET);
} else {
    $signatureLink = (!$company->chief_signature_link) ? null :
        EasyThumbnailImage::thumbnailSrc($company->getImage('chiefSignatureImage'), 165, 50, EasyThumbnailImage::THUMBNAIL_INSET);

    if (!$company->chief_is_chief_accountant) {
        $accountantSignatureLink = (!$company->chief_accountant_signature_link) ? null :
            EasyThumbnailImage::thumbnailSrc($company->getImage('chiefAccountantSignatureImage'), 165, 50, EasyThumbnailImage::THUMBNAIL_INSET);
    } else {
        $accountantSignatureLink = $signatureLink;
    }
}

$printLink = (!$company->print_link) ? null :
    EasyThumbnailImage::thumbnailSrc($company->getImage('printImage'), 100, 100, EasyThumbnailImage::THUMBNAIL_INSET);

$logoLink = !$company->logo_link ? null :
    EasyThumbnailImage::thumbnailSrc($company->getImage('logoImage'), 150, 90, EasyThumbnailImage::THUMBNAIL_INSET);

/* add logo */
$images['logo'] = ['tab' => 0, 'link' => $logoLink, 'name' => '+ Добавить логотип'];
$images['print'] = ['tab' => 1, 'link' => $printLink, 'name' => '+ Добавить печать'];
$images['chief'] = ['tab' => 2, 'link' => $signatureLink, 'name' => '+ Добавить подпись'];
?>

    <style type="text/css">
        <?php
        if (is_file($file = \Yii::getAlias('@frontend/web/css/print/common.css'))) {
            //echo $this->renderFile($file);

        }
        if (is_file($file = \Yii::getAlias('@frontend/web/css/print/documents-invoice.css'))) {
            //echo $this->renderFile($file);
        }
        ?>

        .pre-view-table .bord-dark tr, .bord-dark td, .bord-dark th {
            padding: 2px 4px;
            font-size: 9pt;
            border: 1px solid #000000;
        }

        .pre-view-table h3 {
            text-align: center;
            margin: 6px 0 5px;
            font-weight: bold;
        }

        .auto_tpl {
            font-style: italic;
            font-weight: normal;
        }

        .m-size-div div {
            font-size: 12px;
        }

        .m-size-div div span {
            font-size: 9px;
        }

        .file_upload_block {
            text-align: center;
            border: 2px dashed #ffb848;
            color: #ffb848;
            padding: 12px;
        }

        .no_min_h {
            min-height: 0px !important;
        }
    </style>

    <div class="page-content-in m-size-div container-first-account-table no_min_h pad0"
         style="min-width: 520px; max-width:735px; border:1px solid #4276a4; margin-top:3px;">


        <div class="col-xs-12 pad5 pre-view-table">
            <div class="col-xs-12 pad3" style="height: 80px;">
                <div class="col-xs-6 pad0 font-bold" style="height: inherit">
                    <div class="col-xs-12 pad0" style="padding-top: 22px !important;">
                        <p style="font-size: 17px"> <?= $model->company_name_short; ?> </p>
                    </div>
                </div>

                <div class="col-xs-6 pad3 v_middle" style="height: inherit; text-align: right;">
                    <div>
                        <?php if ($logoLink) : ?>
                            <img style="max-height: 70px;max-width: 170px;" src="<?= $logoLink ?>" alt="">
                        <?php endif ?>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 pad3" style="padding-top: 10px !important;">
                <div class="col-xs-12 pad0 bord-dark"
                     style="height: 60px; border-bottom: 0px">
                    <div class="col-xs-6 pad0">
                        <div class="col-xs-12 pad1" style="height: 40px;">
                            <div class="col-xs-12 pad1 v_middle" style="height: inherit">
                                <?= $model->company_bank_name; ?>
                            </div>
                        </div>


                        <div class="col-xs-12 pad1"> Банк получателя</div>
                    </div>
                    <div class="col-xs-2 pad0 bord-dark-r bord-dark-l"
                         style="height: inherit;">
                        <div class="col-xs-12 pad1"> БИК</div>
                        <div class="col-xs-12 pad1 bord-dark-t"
                             style="padding-top: 5px"> Сч. №
                        </div>
                    </div>
                    <div class="col-xs-4 pad0">
                        <div
                            class="col-xs-12 pad1">  <?= $model->company_bik; ?> </div>
                        <div class="col-xs-12 pad1"
                             style="padding-top: 5px">  <?= $model->company_ks; ?> </div>
                    </div>
                </div>

                <div class="col-xs-12 pad0 bord-dark" style="height: 82px;">
                    <div class="col-xs-6 pad0 full_h">
                        <div class="col-xs-12 pad0">
                            <div class="col-xs-6 pad1 bord-dark-r">
                                ИНН <?= $model->company_inn; ?> </div>
                            <div class="col-xs-6 pad1">
                                КПП <?= $model->company_kpp; ?> </div>
                        </div>

                        <div class="col-xs-12 pad0 bord-dark-t">
                            <div
                                class="col-xs-12 pad1"> <?= $model->company_name_short; ?> </div>
                            <div class="col-xs-12 pad0"> &nbsp; </div>
                            <div class="col-xs-12 pad1" style="padding-top: 5px">
                                Получатель
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-2 pad0 bord-dark-r bord-dark-l full_h">
                        Сч. №
                    </div>
                    <div class="col-xs-4 pad0 full_h">
                        <div
                            class="col-xs-12 pad1">  <?= $model->company_rs; ?> </div>
                    </div>
                </div>
            </div>

            <div class="col-xs-12 pad0"
                 style="padding-top: 5px !important; border-bottom: 2px solid #000000">
                <h3 style="marg"> Счет
                    № <?= $model->fullNumber; ?>
                    от <?= $dateFormatted; ?>
                </h3>
            </div>

            <div class="col-xs-12 pad3" style="padding-top: 15px !important;">
                <div class="col-xs-12 pad0">
                    <div class="col-xs-2 pad0"> Поставщик <br/> (Исполнитель):</div>
                    <div class="col-xs-10 pad0" style="padding-left:3px;font-weight: bold;">
                        <?= $model->company_name_short; ?>,
                        ИНН <?= $model->company_inn; ?><?= !empty($model->company_kpp) ? ', КПП' . $model->company_kpp : ''; ?>
                        , <?= $model->company_address_legal_full; ?>
                    </div>
                </div>
                <div class="col-xs-12 pad0" style="padding-top: 5px !important;">
                    <div class="col-xs-2 pad0"> Покупатель<br/>(Заказчик):</div>
                    <div class="col-xs-10 pad0" style="padding-left:3px;font-weight: bold;">
                        <?= $model->contractor_name_short; ?>,
                        <?= ($model->contractor->face_type == \common\models\Contractor::TYPE_LEGAL_PERSON) ? 'ИНН ' . $model->contractor_inn . ',' : '' ?>
                        <?= ($model->contractor->face_type == \common\models\Contractor::TYPE_LEGAL_PERSON && !empty($model->contractor_kpp)) ? 'КПП ' . $model->contractor_kpp . ',' : '' ?>
                        <?= $model->contractor_address_legal_full; ?>
                    </div>
                </div>

                <?php if ($model->basis_document_name && $model->basis_document_number && $model->basis_document_date) : ?>
                    <div class="col-xs-12 pad0"
                         style="padding-top: 5px !important;">
                        <div class="col-xs-2 pad0"> Основание:</div>
                        <div class="col-xs-10 pad3" style="font-weight: bold;">
                            <?= $model->agreementType ? $model->agreementType->name : 'Договор' ?>
                            № <?= Html::encode($model->basis_document_number) ?>
                            от <?= DateHelper::format($model->basis_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                        </div>
                    </div>
                <?php endif ?>
            </div>

            <div class="col-xs-12 pad3">
                <?= $this->render('view/_pre_view_orders_table', [
                    'model' => $model,
                    'ioType' => $ioType,
                ]); ?>
            </div>

            <div class="col-xs-12 pad3">
                <div class="txt-9">
                    Всего наименований <?= count($model->orders) ?>, на сумму
                    <?= TextHelper::invoiceMoneyFormat($model->view_total_with_nds, 2); ?>
                    <?= $model->currency_name == Currency::DEFAULT_NAME ? 'руб' : $model->currency_name ?>.
                </div>
                <div class="txt-9-b">
                    <?= TextHelper::mb_ucfirst(Currency::textPrice($model->view_total_with_nds / 100, $model->currency_name)); ?>
                </div>
                <?php if ($model->comment) : ?>
                    <div class="txt-9" style="padding-top: 10px;">
                        <?= nl2br(Html::encode($model->comment)) ?>
                    </div>
                <?php endif; ?>

                <div style="border-bottom: 2px solid #000000; padding: 2px 4px;"></div>

                <?php if ($company->company_type_id != CompanyType::TYPE_IP) : ?>
                    <!-- Руководитель -->
                    <div class="col-xs-12 pad0" style="padding-top: 10px !important; z-index: 2;">
                        <div class="col-xs-12 pad0" style="height: 65px; text-align: center">
                            <div class="col-xs-3 pad3 v_bottom" style="font-weight: bold; text-align: left">
                                <div> Руководитель:</div>
                            </div>
                            <div class="col-xs-3 pad3 v_bottom">
                                <div> <?= $model->company_chief_post_name; ?> </div>
                            </div>
                            <div class="col-xs-3 pad0 v_bottom">
                                <div>
                                    <?php if ($signatureLink) { ?>
                                        <img style="max-width: 130px" src="<?= $signatureLink ?>" alt="">
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="col-xs-3 pad3 v_bottom">
                                <div>
                                    <?= $model->signed_by_name ? $model->signed_by_name : $model->getCompanyChiefFio(true); ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 pad0"
                             style="padding-top: 3px; text-align: center; font-size: 9px !important;">
                            <div class="col-xs-3 pad3"> &nbsp; </div>
                            <div class="col-xs-3 pad3">
                                <div class="bord-dark-t"><span> должность </span></div>
                            </div>
                            <div class="col-xs-3 pad3">
                                <div class="bord-dark-t"><span> подпись </span></div>
                            </div>
                            <div class="col-xs-3 pad3">
                                <div class="bord-dark-t"><span> расшифровка подписи </span></div>
                            </div>
                        </div>
                    </div>
                    <!-- Руководитель.end -->
                    <!-- Печать -->
                    <div class="col-xs-12 pad3">
                        <div style="text-align:center;position:absolute; right: 15px; z-index:1; top:-71px;">
                            <?php if ($printLink) { ?>
                                <img style="width: 150px;" src="<?= $printLink; ?>" alt="">
                            <?php } ?>
                        </div>
                    </div>
                    <!-- Печать.end -->

                    <!-- Главбух -->
                    <div class="col-xs-12 pad0" style="padding-top: 0px !important;">
                        <div class="col-xs-12 pad0" style="height: 52px;text-align: center;">
                            <div class="col-xs-5 pad3 v_bottom" style="font-weight: bold; text-align: left">
                                <div> Главный (старший) бухгалтер:</div>
                            </div>
                            <div class="col-xs-3 pad3 v_bottom">
                                <div>
                                    <?php if (isset($accountantSignatureLink)) { ?>
                                        <img style="max-width: 130px" src="<?= $accountantSignatureLink ?>" alt="">
                                    <?php } ?>
                                    &nbsp;
                                </div>
                            </div>
                            <div class="col-xs-4 pad3 v_bottom" style="z-index: 2">
                                <div>
                                    <?= $model->signed_by_name ? $model->signed_by_name : $model->getCompanyChiefAccountantFio(true); ?>
                                    <?php if ($model->signed_by_employee_id) : ?>
                                        <br>по <?= mb_strtolower($model->signBasisDocument->name2) . ' №' . $model->sign_document_number ?>
                                        <br><?= 'от ' . DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . 'г.'; ?>
                                    <?php endif ?>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 pad0"
                             style="z-index: 2;padding-top: 3px; text-align: center; font-size: 9px !important;">
                            <div class="col-xs-5 pad3"> &nbsp; </div>
                            <div class="col-xs-3 pad3">
                                <div class="bord-dark-t"><span> подпись </span></div>
                            </div>
                            <div class="col-xs-4 pad3">
                                <div class="bord-dark-t"><span> расшифровка подписи </span></div>
                            </div>
                        </div>
                    </div>
                    <!-- Главбух.end -->
                <?php else: ?>
                    <div class="m-t-xl"
                         style="background: url('<?= $printLink ?>'); background-repeat: no-repeat; background-position: right center; height: 200px; -webkit-print-color-adjust: exact;">
                        <div class="col-xs-12 pad0">
                            <br/>
                            <table class="podp-r"
                                   style="background: url('<?= $signatureLink ?>'); background-repeat: no-repeat; background-position: 175px 5px; margin-top: -20px; width: 675px; -webkit-print-color-adjust: exact;">
                                <tr>
                                    <td class="txt-9-b"
                                        style="padding: 0 0 0 0;width: 20%; vertical-align: bottom;">
                                        Предприниматель
                                    </td>
                                    <td style="width: 4%"></td>
                                    <td class="pad-line"
                                        style="border-bottom: 1px solid #000000;text-align: center; margin: 0 0 0 0; width: 35%; padding-top: 48px!important;"></td>
                                    <td style="width: 4%"></td>
                                    <td class="pad-line"
                                        style="border-bottom: 1px solid #000000;text-align: center; vertical-align: bottom; margin: 0 0 0 0; width: 41%;"><?= $model->getCompanyChiefFio(true); ?></td>
                                </tr>
                                <tr class="small-txt">
                                    <td style="padding: 0 0 0 0;width: 20%"></td>
                                    <td style="width: 4%"></td>
                                    <td style="text-align: center; margin: 0 0 0 0; width: 42%;">
                                        подпись
                                    </td>
                                    <td style="width: 4%"></td>
                                    <td style="text-align: center; margin: 0 0 0 0; width: 42%;">
                                        расшифровка подписи
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php if (isset($multiple) && (int)array_pop($multiple) !== $model->id): ?>
    <pagebreak/>
<?php endif; ?>