<?php
use yii\helpers\Html;

$employee = Yii::$app->user->identity;
$employeeCompany = $employee->currentEmployeeCompany;
?>
<div style="padding: 0 10px 10px; border: 1px solid #e5e5e5; border-top: 0;">
    <div style="font-size: 14px; margin-bottom: 25px;">
        <span style="display: inline-block; padding: 8px 16px; color: #fff; background-color: #0077A7;">
            Ссылка на счет
        </span>
    </div>
    <div style="font-size: 14px;">
        С уважением,<br>
        <?= $employeeCompany->getFio(true); ?><br>
        <?= $employeeCompany->position; ?>
    </div>
</div>