<?php
/**
 * Created by PhpStorm.
 * User: dmitry
 * Date: 16.04.15
 * Time: 16:42
 */

use common\components\date\DateHelper;
use common\components\image\EasyThumbnailImage;
use common\components\ImageHelper;
use common\components\TextHelper;
use common\models\currency\Currency;
use common\models\document\status\InvoiceStatus;
use frontend\modules\documents\components\Message;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\document\Invoice */
/* @var $orders common\models\document\Order[] */
/* @var $message Message */
/* @var $ioType integer */
/* @var $multiple[] */

//$addStamp = 1;

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'format' => 'd F Y г.',
    'monthInflected' => true,
    'date' => $model->document_date,
]);

$this->title = $model->printTitle;
$this->context->layoutWrapperCssClass = 'out-invoice out-document';

if ($model->signed_by_name) {
    $accountantSignatureLink = $signatureLink = (empty($addStamp) || !$model->employeeSignature) ? null:
        EasyThumbnailImage::thumbnailSrc($model->employeeSignature->file, 165, 50, EasyThumbnailImage::THUMBNAIL_INSET);
} else {
    $signatureLink = (empty($addStamp) || !$model->company->chief_signature_link) ? null:
        EasyThumbnailImage::thumbnailSrc($model->company->getImage('chiefSignatureImage'), 165, 50, EasyThumbnailImage::THUMBNAIL_INSET);

    if (!$model->company->chief_is_chief_accountant) {
        $accountantSignatureLink = (empty($addStamp) || !$model->company->chief_accountant_signature_link) ? null:
            EasyThumbnailImage::thumbnailSrc($model->company->getImage('chiefAccountantSignatureImage'), 165, 50, EasyThumbnailImage::THUMBNAIL_INSET);
    } else {
        $accountantSignatureLink = $signatureLink;
    }
}

$printLink = (empty($addStamp) || !$model->company->print_link) ? null:
    EasyThumbnailImage::thumbnailSrc($model->company->getImage('printImage'), 200, 200, EasyThumbnailImage::THUMBNAIL_INSET);

$logoLink = !$model->company->logo_link? null:
    EasyThumbnailImage::thumbnailSrc($model->company->getImage('logoImage'), 150, 50, EasyThumbnailImage::THUMBNAIL_INSET);

$isAuto = $model->invoice_status_id == InvoiceStatus::STATUS_AUTOINVOICE;
?>

<style type="text/css">
<?php
if (is_file($file = \Yii::getAlias('@frontend/web/css/print/common.css'))) {
    echo $this->renderFile($file);
}
if (is_file($file = \Yii::getAlias('@frontend/web/css/print/documents-invoice.css'))) {
    echo $this->renderFile($file);
}
?>
.auto_tpl {
    font-style: italic;
    font-weight: normal;
}
</style>

<div class="page-content-in p-center pad-pdf-p" style="box-sizing: content-box;">
    <table class="no-b m-t" style="width: 100%; margin-top: 0;">
        <tr>
            <td class="font-bold print-title"
                style="padding-left: 0;padding-bottom: 12px;"><?= $model->company_name_short; ?>
            </td>
            <td style="width: 325px; text-align: right">
                <?php if ($logoLink) : ?>
                    <img class="pull-right" src="<?= $logoLink ?>" alt="">
                <?php endif; ?>
            </td>
        </tr>
    </table>

    <table class="t-p m-t-m" style="width: 100%; border: none">

        <tr>
            <td colspan="2"
                style="border-bottom: none"><?= $model->company_bank_name; ?></td>
            <td style="text-align: left; padding-left: 2px; width: 8%; vertical-align: top;">
                БИК
            </td>
            <td style="border-bottom: none; text-align: left; padding: 2px 0 0 2px; vertical-align: top;">
                <?= $model->company_bik; ?><br/>
            </td>
        </tr>

        <tr>
            <td colspan="2" style="border-top: none"><br/>Банк получателя</td>
            <td style="text-align: left; padding-left: 2px; vertical-align: top;">
                Сч. №
            </td>
            <td style="border-top: 0;text-align: left; padding: 2px 0 0 2px; vertical-align: top;"><?= $model->company_ks; ?></td>
        </tr>

        <tr>
            <td style="width: 170px;">ИНН <?= $model->company_inn; ?></td>
            <td style="width: 189px;">КПП <?= $model->company_kpp; ?></td>
            <td rowspan="2"
                style="text-align: left; padding-left: 2px; vertical-align: top;">
                Сч. №
            </td>
            <td rowspan="2"
                style="text-align: left; padding: 2px 0 0 2px; vertical-align: top;"><?= $model->company_rs; ?></td>
        </tr>
        <tr>
            <td colspan="2"><?= $model->company_name_short; ?><br/><br/>Получатель
            </td>
        </tr>
    </table>

    <h3>
        Счет №
        <?= $isAuto ? '<i class="auto_tpl">##</i>' : $model->fullNumber; ?>
        от
        <?= $isAuto ? '<i class="auto_tpl">## ###</i> 20<i class="auto_tpl">##</i> г.' : $dateFormatted; ?>
    </h3>

    <div style="border-bottom: 2px solid #000000; padding: 2px 4px;"></div>

    <table class="no-b" style="width: 100%; margin-bottom: 5px;">
        <tr>
            <td class="txt-t">Поставщик<br/>(Исполнитель):</td>
            <td class="txt-b">
                <?= $model->company_name_short; ?>,
                ИНН <?= $model->company_inn; ?><?= !empty($model->company_kpp)? ', КПП' . $model->company_kpp: ''; ?>
                , <?= $model->company_address_legal_full; ?>
            </td>
        </tr>
        <tr>
            <td class="txt-t">Покупатель<br/>(Заказчик):</td>
            <td class="txt-b">
                <?= $model->contractor_name_short; ?>,
                <?= ($model->contractor->face_type == \common\models\Contractor::TYPE_LEGAL_PERSON) ? 'ИНН ' . $model->contractor_inn . ',' : '' ?>
                <?= ($model->contractor->face_type == \common\models\Contractor::TYPE_LEGAL_PERSON && !empty($model->contractor_kpp)) ? 'КПП ' . $model->contractor_kpp . ',' : '' ?>
                <?= $model->contractor_address_legal_full; ?>
            </td>
        </tr>
        <?php if ($model->basis_document_name && $model->basis_document_number && $model->basis_document_date) : ?>
            <tr>
                <td class="txt-t">Основание:</td>
                <td class="txt-b">
                        <?= $model->agreementType ? $model->agreementType->name : 'Договор' ?>
                        № <?= Html::encode($model->basis_document_number) ?>
                        от <?= DateHelper::format($model->basis_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE); ?>
                </td>
            </tr>
        <?php endif ?>
    </table>

    <?= $this->render('view/_pdf_orders_table', [
        'model' => $model,
        'ioType' => $ioType,
        'isAuto' => $isAuto,
    ]); ?>

    <div class="txt-9">
        Всего наименований <?= count($model->orders) ?>, на сумму
        <?= TextHelper::invoiceMoneyFormat($model->view_total_with_nds, 2); ?>
        <?= $model->currency_name == Currency::DEFAULT_NAME ? 'руб' : $model->currency_name ?>.
    </div>
    <div class="txt-9-b">
        <?= TextHelper::mb_ucfirst(Currency::textPrice($model->view_total_with_nds / 100, $model->currency_name)); ?>
    </div>
    <?php if ($model->comment) : ?>
        <div class="txt-9" style="padding-top: 10px;">
            <?= nl2br(Html::encode($model->comment)) ?>
        </div>
    <?php endif; ?>

    <div style="border-bottom: 2px solid #000000; padding: 2px 4px;"></div>

    <?php if ($model->company->company_type_id != \common\models\company\CompanyType::TYPE_IP) : ?>
        <div class="bg-seal" style="background: url('<?= $printLink ?>'); background-repeat: no-repeat; background-position: right center; height: 230px; width: 675px; -webkit-print-color-adjust: exact;">
            <div class="row">
                <div class="col-md-12">
                    <br/>
                    <br/>
                    <table class="podp-r va-bottom message" style="background: url('<?= $signatureLink ?>') no-repeat 300px top; width: 675px; table-layout: fixed; -webkit-print-color-adjust: exact;">
                        <col width="13%" />
                        <col width="3%" />
                        <col width="24%" />
                        <col width="3%" />
                        <col width="24%" />
                        <col width="3%" />
                        <col width="30%" />
                        <tr>
                            <td class="txt-9-b"
                                style="width: 13%; border: none; padding: 0;">
                                Руководитель:
                            </td>
                            <td style="width: 3%; border: none;"></td>
                            <td class="pad-line"
                                style="width: 24%; border: none; border-bottom: 1px solid #000000;text-align: center; margin: 0 0 0 20px;"><?= $model->company_chief_post_name; ?></td>
                            <td style="width: 3%; border: none;">
                            </td>
                            <td class="pad-line"
                                style="width: 24%; border: none; border-bottom: 1px solid #000000;text-align: center; position: relative; height: 40px!important;"></td>
                            <td style="width: 3%; border: none;"></td>
                            <td class="pad-line"
                                style="width: 30%; border: none; border-bottom: 1px solid #000000;text-align: center; padding-top: 30px!important;">
                                <?= $model->signed_by_name ? $model->signed_by_name : $model->getCompanyChiefFio(true); ?>
                                <?php if ($model->signed_by_employee_id) : ?>
                                    <br>по <?= mb_strtolower($model->signBasisDocument->name2) . ' №' . $model->sign_document_number ?>
                                    <br><?= 'от ' . DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . 'г.'; ?>
                                <?php endif ?>
                            </td>
                        </tr>
                        <tr class="small-txt va-top">
                            <td style="width: 13%; border: none; padding: 0;width: 15%"></td>
                            <td style="width: 3%; border: none;"></td>
                            <td style="width: 24%; border: none; text-align: center; padding-top: 0;margin-top: 0; vertical-align: top;">
                                должность
                            </td>
                            <td style="width: 3%; border: none;"></td>
                            <td style="width: 24%%; border: none; text-align: center; vertical-align: top;">подпись
                            </td>
                            <td style="width: 3%; border: none;"></td>
                            <td style="width: 30%; border: none; text-align: center; vertical-align: top;">
                                расшифровка
                                подписи
                            </td>
                        </tr>
                    </table>
                </div>
            </div>

            <div style="margin-top: -35px;">
                <br/>
                <br/>
                <br/>
                <table class="podp-r va-bottom" style="background: url('<?= $accountantSignatureLink ?>') no-repeat  255px 2px;  width: 675px; -webkit-print-color-adjust: exact;">
                    <tr>
                        <td class="txt-9-b"
                            style="padding: 0 0 0 0;width: 25%;border: none;">
                            Главный
                            (старший)
                            бухгалтер:
                        </td>
                        <td style="width: 4%;border: none;"></td>
                        <td class="pad-line"
                            style="border: none;border-bottom: 1px solid #000000;text-align: center; margin: 0 0 0 0; width: 27%; height: 40px!important;"></td>
                        <td style="border: none;width: 4%"></td>
                        <td class="pad-line"
                            style="border: none;border-bottom: 1px solid #000000;text-align: center; margin: 0 0 0 0; width: 27%; padding-top: 30px;">
                            <?= $model->signed_by_name ? $model->signed_by_name : $model->getCompanyChiefAccountantFio(true); ?>
                            <?php if ($model->signed_by_employee_id) : ?>
                                <br>по <?= mb_strtolower($model->signBasisDocument->name2) . ' №' . $model->sign_document_number ?>
                                <br><?= 'от ' . DateHelper::format($model->sign_document_date, DateHelper::FORMAT_USER_DATE, DateHelper::FORMAT_DATE) . 'г.'; ?>
                            <?php endif ?>
                        </td>
                    </tr>
                    <tr class="small-txt">
                        <td style="border: none;padding: 0 0 0 0;width: 25%"></td>
                        <td style="border: none;width: 4%"></td>
                        <td style="border: none;text-align: center; margin: 0 0 0 0; width: 27%; vertical-align: top;">
                            подпись
                        </td>
                        <td style="border: none;width: 4%"></td>
                        <td style="border: none;text-align: center; margin: 0 0 0 0; width: 27%; vertical-align: top;">
                            расшифровка подписи
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    <?php else: ?>
        <div class="row m-t-xl"
             style="background: url('<?= $printLink ?>'); background-repeat: no-repeat; background-position: right center; height: 200px; -webkit-print-color-adjust: exact;">
            <div class="col-md-12">
                <br/>
                <table class="podp-r"
                       style="background: url('<?= $signatureLink ?>'); background-repeat: no-repeat; background-position: 175px 5px; margin-top: -20px; width: 675px; -webkit-print-color-adjust: exact;">
                    <tr>
                        <td class="txt-9-b"
                            style="padding: 0 0 0 0;width: 20%; vertical-align: bottom;">
                            Предприниматель
                        </td>
                        <td style="width: 4%"></td>
                        <td class="pad-line"
                            style="border-bottom: 1px solid #000000;text-align: center; margin: 0 0 0 0; width: 35%; padding-top: 48px!important;"></td>
                        <td style="width: 4%"></td>
                        <td class="pad-line"
                            style="border-bottom: 1px solid #000000;text-align: center; vertical-align: bottom; margin: 0 0 0 0; width: 41%;"><?= $model->getCompanyChiefFio(true); ?></td>
                    </tr>
                    <tr class="small-txt">
                        <td style="padding: 0 0 0 0;width: 20%"></td>
                        <td style="width: 4%"></td>
                        <td style="text-align: center; margin: 0 0 0 0; width: 42%;">
                            подпись
                        </td>
                        <td style="width: 4%"></td>
                        <td style="text-align: center; margin: 0 0 0 0; width: 42%;">
                            расшифровка подписи
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    <?php endif; ?>
</div>

<?php if (isset($multiple) && (int)array_pop($multiple) !== $model->id): ?>
<pagebreak />
<?php endif; ?>