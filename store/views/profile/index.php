<?php
/** @var $this yii\web\View */

use yii\bootstrap\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use common\models\TimeZone;
use lavrentiev\widgets\toastr\NotificationFlash;

/** @var $employee \common\models\employee\Employee */

$changePasswordModel = new \frontend\models\ChangePasswordForm();
$changeEmailModel = new \frontend\models\ChangeEmailForm(Yii::$app->user->identity);
$changeTimeZone = new \frontend\models\ChangeTimeZoneForm();

$changeTimeZone->newTimeZone = TimeZone::DEFAULT_TIME_ZONE;

$this->title = 'Настройки профиля';

$this->params['layoutWrapperCssClass'] = 'profile-setup';
?>

<h3 class="page-title">Профиль: <?= Yii::$app->user->identity->nameFull; ?></h3>

<?php $flash = Yii::$app->session->getFlash('change_profile'); ?>
<?php if (!empty($flash)) {
    echo NotificationFlash::widget([
        'options' => [
            'closeButton' => true,
            'showDuration' => 1000,
            'hideDuration' => 1000,
            'timeOut' => 5000,
            'extendedTimeOut' => 1000,
            'positionClass' => NotificationFlash::POSITION_TOP_RIGHT,
            'message' => $flash
        ],
    ]);
} ?>

<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">
            Изменение пароля
        </div>
    </div>
    <div class="portlet-body">
        <?php $form = ActiveForm::begin([
            'id' => 'changepassword-form',
            'action' => ['changepassword'],
            'class' => 'form-horizontal',
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
        ]); ?>
        <div class="form-body form-horizontal">
            <?= $form->field($changePasswordModel, 'oldPassword')->label(null, [
                'class' => 'control-label col-md-4 label-width',
            ])->passwordInput([
                'class' => 'form-control field-width inp_one_line_prof',
            ]); ?>

            <?= $form->field($changePasswordModel, 'newPassword')->label(null, [
                'class' => 'control-label col-md-4 label-width',
            ])->passwordInput([
                'class' => 'form-control field-width inp_one_line_prof',
            ]); ?>

            <?= $form->field($changePasswordModel, 'newPasswordRepeat')->label(null, [
                'class' => 'control-label col-md-4 label-width',
            ])->passwordInput([
                'class' => 'form-control field-width inp_one_line_prof',
            ]); ?>

            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo Html::submitButton('Изменить', [
                            'class' => 'btn darkblue text-white',
                            'form' => 'changepassword-form',
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
        <?php $form->end(); ?>
    </div>
</div>

<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">
            Изменение часового пояса
        </div>
    </div>
    <div class="portlet-body">
        <div class="form-body form-horizontal">
            <div class="form-group">
                <label for="name" class="control-label col-md-4 label-width">Текущий
                    часовой пояс:</label>

                <div class="col-md-5 pad-e-b inp_one_line_prof">
                    <p class="form-control-static pad-e-p"><?php $timeZone = TimeZone::findOne($employee->time_zone_id);
                        echo $timeZone->out_time_zone ?></p>
                </div>
            </div>
        </div>
        <?php $form = ActiveForm::begin([
            'id' => 'changetimezone-form',
            'action' => ['changetimezone'],
            'class' => 'form-horizontal',
        ]); ?>

        <?= $form->field($changeTimeZone, 'newTimeZone')->label(null, [
            'class' => 'control-label contr_lab_left',
        ])->dropDownList(ArrayHelper::map(TimeZone::getList(), 'id', 'out_time_zone')); ?>

        <div class="form-actions">
            <div class="row">
                <div class="col-md-12">
                    <?php echo Html::submitButton('Изменить', [
                        'class' => 'btn darkblue text-white',
                        'form' => 'changetimezone-form',
                    ]); ?>
                </div>
            </div>
        </div>

        <?php $form->end(); ?>
    </div>
</div>

<div class="portlet box darkblue">
    <div class="portlet-title">
        <div class="caption">
            Изменение адреса электронной почты
        </div>
    </div>
    <div class="portlet-body">
        <?php $form = ActiveForm::begin([
            'id' => 'changeemail-form',
            'action' => ['changeemail'],
            'class' => 'form-horizontal',
            'enableAjaxValidation' => true,
            'enableClientValidation' => false,
        ]); ?>

        <div class="form-body form-horizontal">

            <div class="form-group">
                <label for="name" class="control-label col-md-4 label-width">Текущая
                    электронная почта:</label>

                <div class="col-md-5 pad-e-b inp_one_line_prof">
                    <p class="form-control-static pad-e-p"><?php echo $employee->email; ?></p>
                </div>
            </div>

            <?= $form->field($changeEmailModel, 'email')->label(null, [
                'class' => 'control-label col-md-4 label-width',
            ])->textInput([
                'class' => 'form-control field-width inp_one_line_prof',
            ]); ?>

            <?= $form->field($changeEmailModel, 'password')->label(null, [
                'class' => 'control-label col-md-4 label-width',
            ])->passwordInput([
                'class' => 'form-control field-width inp_one_line_prof',
            ]); ?>

            <div class="form-actions">
                <div class="row">
                    <div class="col-md-12">
                        <?php echo Html::submitButton('Изменить', [
                            'class' => 'btn darkblue text-white',
                            'form' => 'changeemail-form',
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>

        <?php $form->end(); ?>
    </div>
</div>
