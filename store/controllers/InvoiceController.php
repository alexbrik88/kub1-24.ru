<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 28.02.2018
 * Time: 5:04
 */

namespace store\controllers;


use common\components\filters\AccessControl;
use common\components\pdf\PdfRenderer;
use common\components\pdf\Printable;
use common\components\phpWord\PhpWordInvoice;
use common\models\document\Invoice;
use common\models\document\OrderDocument;
use common\models\store\StoreUser;
use frontend\models\Documents;
use store\assets\PrintAsset;
use store\components\StoreController;
use Yii;
use yii\helpers\ArrayHelper;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;

/**
 * Class InvoiceController
 * @package store\controllers
 */
class InvoiceController extends StoreController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Displays a single Invoice model.
     *
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($id)
    {
        /** @var Invoice $model */
        $model = $this->findModel($id, true);

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * @param $id
     * @return string|void
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function actionPrint($id)
    {
        $model = $this->findModel($id);

        return $this->_documentPrint($model, PrintAsset::className(), 'print', 'pdf-view', [
            'addStamp' => true,
        ]);
    }

    /**
     * @param $id
     * @return string|void
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function actionPdf($id)
    {
        $model = $this->findModel($id);

        return $this->_documentPrint($model, PrintAsset::className(), 'pdf', 'pdf-view', [
            'addStamp' => true,
        ]);
    }

    /**
     * @param $id
     * @return string|void
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function actionDocx($id)
    {
        $model = $this->findModel($id);

        $word = new PhpWordInvoice($model);

        return $word->getFile();
    }

    /**
     * @param $model
     * @param $asset
     * @param $actionType
     * @param $view
     * @param array $params
     * @return string|void
     * @throws Exception
     */
    protected function _documentPrint($model, $asset, $actionType, $view, $params = [])
    {
        if (!($model instanceof Printable)) {
            throw new Exception('Model must be instance of Printable.');
        }

        /** @var AbstractDocument $model */

        $renderer = new PdfRenderer([
            'view' => $view,
            'params' => array_merge([
                'model' => $model,
                'message' => null,
                'ioType' => ($model->hasAttribute('type') ? $model->type : null),
            ], $params),

            'destination' => PdfRenderer::DESTINATION_BROWSER,
            'filename' => $model->printTitle . '.pdf',
            'displayMode' => ArrayHelper::getValue($params, 'displayMode', PdfRenderer::DISPLAY_MODE_FULLPAGE),
            'format' => ArrayHelper::getValue($params, 'documentFormat', 'A4-P'),
        ]);

        $this->view->params['asset'] = $asset;
        switch ($actionType) {
            case 'pdf':
                return $renderer->output();
            case 'print':
            default:
                if ($this->action->id != 'out-view') {
                    Yii::$app->view->registerJs('window.print();');
                }

                return $renderer->renderHtml();
        }
    }

    /**
     * @param $orderDocumentID
     * @return \yii\web\Response
     */
    public function actionCreateFromOrder($orderDocumentID)
    {
        if (($company = Yii::$app->user->identity->getCurrentKubCompany()) &&
            !$company->createInvoiceAllowed(Documents::IO_TYPE_OUT)
        ) {
            $message = 'Количество заказов у поставщика достигло лимита.';
            $message .= "\r\n";
            $message .= 'За подробной информацией обратитесь к ';
            $message .= $company->getTitle(true) . '.';

            throw new ForbiddenHttpException($message);
        }
        $orderDocument = OrderDocument::findOne($orderDocumentID);
        if ($orderDocument && $orderDocument->createInvoice()) {
            Yii::$app->session->setFlash('success', 'Счет создан.');
        };

        return $this->redirect(['/order/view', 'id' => $orderDocumentID]);
    }

    /**
     * Finds the Invoice model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     *
     * @param bool $allowDeleted
     *
     * @return Invoice the loaded model
     * @throws NotFoundHttpException
     */
    protected function findModel($id, $allowDeleted = false)
    {
        /* @var $user StoreUser */
        $user = Yii::$app->user->identity;
        $contractor = $user->getCurrentKubContractor();
        $company = $user->getCurrentKubCompany();

        /* @var Invoice $model */
        $model = Invoice::findOne([
            'id' => $id,
            'company_id' => $company->id,
            'contractor_id' => $contractor->id,
        ]);

        if ($model !== null && (!$model->is_deleted || $allowDeleted)) {
            return $model;
        } else {
            throw new NotFoundHttpException('Страница не найдена');
        }
    }
}
