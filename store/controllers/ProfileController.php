<?php
namespace store\controllers;

use common\models\employee\Employee;
use frontend\models\ChangeEmailForm;
use frontend\models\ChangeNotifyForm;
use frontend\models\ChangePasswordForm;
use frontend\models\ChangeTimeZoneForm;
use store\components\StoreController;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\Controller;

/**
 * Profile controller
 */
class ProfileController extends StoreController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'changepassword', 'changeemail', 'changenotify', 'changetimezone'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ]);
    }

    /**
     * View user profile.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index', [
            'employee' => Yii::$app->user->identity,
        ]);
    }

    /**
     * Changing user password
     *
     * @return mixed
     */
    public function actionChangepassword()
    {
        $model = new ChangePasswordForm();

        if (Yii::$app->request->isAjax) {
            return $this->ajaxValidate($model);
        }

        if (($p = $model->load(Yii::$app->request->post())) && ($s = $model->save())) {
            Yii::$app->getSession()->setFlash('success', 'Новый пароль сохранён.');
        } else {
            Yii::$app->getSession()->setFlash('error', 'Ошибка при изменении пароля.');
        }

        return $this->redirect(['profile/index']);
    }

    /**
     * Changing user email
     *
     * @return mixed
     */
    public function actionChangeemail()
    {
        $model = new ChangeEmailForm(Yii::$app->user->identity);

        if (Yii::$app->request->isAjax) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->save()) {
            Yii::$app->getSession()->setFlash('success', 'Адрес электронной почты изменён.');
        } else {
            Yii::$app->getSession()->setFlash('error', 'Ошибка при изменении адреса электронной почты.');
        }

        return $this->redirect(['profile/index']);
    }

    /**
     * Changing user notify
     *
     * @return mixed
     */
    public function actionChangenotify()
    {
        $model = new ChangeNotifyForm();

        if (Yii::$app->request->isAjax) {
            return $this->ajaxValidate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->saveNotify()) {
            Yii::$app->getSession()->setFlash('success', 'Настройки уведомлений изменены.');
        } else {
            Yii::$app->getSession()->setFlash('error', 'Ошибка при изменении настроек уведомления.');
        }

        return $this->redirect(['profile/index']);
    }

    /**
     * Changing user timezone
     *
     * @return mixed
     */
    public function actionChangetimezone()
    {
        $model = new ChangeTimeZoneForm();

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->save()) {
            Yii::$app->getSession()->setFlash('success', 'Часовой пояс изменен.');
        } else {
            Yii::$app->getSession()->setFlash('error', 'Ошибка при изменении часового пояса.');
        }

        return $this->redirect(['profile/index']);
    }

}
