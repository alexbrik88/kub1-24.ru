<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 28.02.2018
 * Time: 5:09
 */

namespace store\controllers;


use common\components\helpers\ArrayHelper;
use common\components\pdf\PdfRenderer;
use common\components\pdf\Printable;
use common\models\document\AbstractDocument;
use common\models\document\Invoice;
use common\models\document\PackingList;
use common\models\document\status\OrderDocumentStatus;
use frontend\models\Documents;
use store\assets\DocumentPrintAsset;
use frontend\modules\documents\components\Message;
use store\components\StoreController;
use Yii;
use common\components\filters\AccessControl;
use yii\base\Exception;
use yii\web\NotFoundHttpException;

/**
 * Class PackingListController
 * @package store\controllers
 */
class PackingListController extends StoreController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
        ];
    }

    /**
     * @param $type
     * @param $invoiceId
     * @return \yii\web\Response
     * @throws \yii\web\ForbiddenHttpException
     * @throws \yii\web\NotFoundHttpException
     */
    public function actionCreate($type, $invoiceId)
    {
        /* @var Invoice $invoice */
        $invoice = Documents::loadModel($invoiceId, Documents::DOCUMENT_INVOICE, $type, false);
        if ($invoice->getCanAddPackingList()) {
            if ($invoice->createPackingList()) {
                $orderDocument = $invoice->orderDocument;
                if ($orderDocument) {
                    $orderDocument->status_id = OrderDocumentStatus::STATUS_ASSEMBLED;
                    $orderDocument->save(true, ['status_id']);
                }
                Yii::$app->session->setFlash('success', 'ТН создана.');
            } else {
                Yii::$app->session->setFlash('error', 'При создании ТН возникли ошибки.');
            }
        } else {
            Yii::$app->session->setFlash('error', 'Нельзя создать ТН.');
        }

        return $this->redirect(['order/view', 'id' => $invoice->orderDocument->id,]);
    }

    /**
     * @param $id
     * @param $type
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionView($id, $type)
    {
        /** @var PackingList $model */
        $model = $this->findModel($id);

        return $this->render('view', [
            'model' => $model,
            'message' => new Message(Documents::DOCUMENT_PACKING_LIST, $model->type),
            'ioType' => $type,
            'useContractor' => true,
            'contractorId' => $model->invoice->contractor_id,
        ]);
    }

    /**
     * @param $actionType
     * @param $id
     * @param $filename
     * @return string|void
     * @throws NotFoundHttpException
     * @throws \yii\base\Exception
     */
    public function actionDocumentPrint($actionType, $id, $filename)
    {
        $model = $this->findModel($id);
        return $this->_documentPrint($model, DocumentPrintAsset::className(), $actionType, 'pdf-view', [
            'documentFormat' => 'A4-L',
            'packing_list_id' => $id,
        ]);
    }

    /**
     * @param $model
     * @param $asset
     * @param $actionType
     * @param $view
     * @param array $params
     * @return string|void
     * @throws Exception
     */
    protected function _documentPrint($model, $asset, $actionType, $view, $params = [])
    {
        if (!($model instanceof Printable)) {
            throw new Exception('Model must be instance of Printable.');
        }
        /** @var AbstractDocument $model */
        $renderer = new PdfRenderer([
            'view' => $view,
            'params' => array_merge([
                'model' => $model,
                'message' => (isset(Message::$message[Documents::DOCUMENT_PACKING_LIST]) ? new Message(Documents::DOCUMENT_PACKING_LIST,
                    $model->type) : null),
                'ioType' => ($model->hasAttribute('type') ? $model->type : null),
            ], $params),

            'destination' => PdfRenderer::DESTINATION_BROWSER,
            'filename' => $this->getPdfFileName($model),
            'displayMode' => ArrayHelper::getValue($params, 'displayMode', PdfRenderer::DISPLAY_MODE_FULLPAGE),
            'format' => ArrayHelper::getValue($params, 'documentFormat', 'A4-P'),
        ]);

        $this->view->params['asset'] = $asset;
        switch ($actionType) {
            case 'pdf':
                return $renderer->output();
            case 'print':
            default:
                if ($this->action->id != 'out-view') {
                    Yii::$app->view->registerJs('window.print();');
                }

                return $renderer->renderHtml();
        }
    }

    /**
     * @param $model
     * @return string
     */
    protected function getPdfFileName($model)
    {
        return $model->printTitle . '.pdf';
    }

    /**
     * Finds the PackingList model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param integer $id
     **
     * @return PackingList the loaded model
     * @throws NotFoundHttpException
     */
    protected function findModel($id)
    {
        /* @var PackingList $model */
        $model = PackingList::find()
            ->andWhere([PackingList::tableName() . '.id' => $id,])
            ->byCompany(Yii::$app->user->identity->currentKubCompany->id)
            ->one();
        if ($model !== null) {
            return $model;
        }
        throw new NotFoundHttpException('Страница не найдена');

    }
}