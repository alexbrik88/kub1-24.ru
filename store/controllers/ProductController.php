<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 26.02.2018
 * Time: 11:09
 */

namespace store\controllers;

use common\components\filters\AccessControl;
use common\components\filters\AjaxFilter;
use common\models\document\OrderDocument;
use common\models\document\OrderDocumentProduct;
use common\models\document\status\OrderDocumentStatus;
use common\models\product\Product;
use common\models\product\ProductStore;
use common\models\prompt\PageType;
use common\models\store\StoreUser;
use frontend\components\PageSize;
use frontend\models\Documents;
use store\components\StoreController;
use store\models\ProductSearch;
use Yii;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;

/**
 * Class ProductController
 * @package store\controllers
 */
class ProductController extends StoreController
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
         //   'ajax' => [
         //       'class' => AjaxFilter::className(),
         //       'only' => ['get-products', 'get-products-table'],
         //   ],
        ];
    }

    /**
     * @param $productionType
     * @return string
     * @throws NotFoundHttpException
     */
    public function actionIndex($productionType = null)
    {
        if ($productionType == null) {
            $productionType = Product::PRODUCTION_TYPE_GOODS;
        }
        $this->_checkProductionType($productionType);

        /* @var $user StoreUser */
        $user = Yii::$app->user->identity;
        $company = $user->company_id ? $user->company : $user->getCurrentKubCompany();
        $productionType = Product::PRODUCTION_TYPE_GOODS;
        $searchModel = new ProductSearch([
            'company_id' => $company->id,
            'production_type' => $productionType,
        ]);
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams, true);
        $dataProvider->pagination->pageSize = PageSize::get();

        return $this->render('index', [
            'user' => $user,
            'company' => $company,
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'productionType' => $productionType,
        ]);
    }

    /**
     * Displays a single Product model.
     *
     * @param $productionType
     * @param integer $id
     *
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionView($productionType, $id)
    {
        $model = $this->findModel($productionType, $id, true);

        $referrer = Yii::$app->request->referrer ? : '';
        if (strpos($referrer, 'product/view')) {
            $referrer = '';
        }
        if ($referrer) {
            Url::remember($referrer, 'product_view_back');
        }

        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * @return string
     */
    public function actionGetProducts()
    {
        /* @var $user StoreUser */
        $user = Yii::$app->user->identity;
        $company = $user->company_id ? $user->company : $user->getCurrentKubCompany();
        $documentType = Yii::$app->request->get('documentType', Documents::IO_TYPE_OUT);
        $productType = Yii::$app->request->get('productType', -1);
        $searchModel = new ProductSearch([
            'company_id' => $company->id,
            'production_type' => $productType,
            'exclude' => Yii::$app->request->get('exists'),
        ]);

        $dataProvider = $searchModel->search();
        $dataProvider->pagination->pageSize = 10;

        $this->layout = 'empty';

        return $this->render('partial/productOrderList', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'documentType' => $documentType,
            'productType' => $productType,
        ]);
    }

    /**
     * @param null $q
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionSearch($q = null)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;

        $productArray = [];
        if ($q) {
            /* @var $user StoreUser */
            $user = Yii::$app->user->identity;
            $contractor = $user->getCurrentKubContractor();
            $companyID = $user->company ? $user->company->id : $contractor->company->id;
            $searchModel = new ProductSearch([
                'company_id' => $companyID,
                'production_type' => Product::PRODUCTION_TYPE_GOODS,
            ]);
            $dataProvider = $searchModel->search([], true);
            $query = $dataProvider->query->andWhere(['like', 'prod.title', $q . '%', false]);

            $productArray = $query->limit(5)->indexBy('id')->asArray()->all();
        }

        return $productArray;
    }

    /**
     * @param bool|false $toOrderDocument
     * @return array|\yii\db\ActiveRecord[]
     */
    public function actionGetProductsTable($toOrderDocument = false)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        /* @var $user StoreUser */
        $user = Yii::$app->user->identity;
        $company = $user->company_id ? $user->company : $user->getCurrentKubCompany();
        $inOrder = Yii::$app->request->post('in_order');
        $product = new Product();

        return $product->getProductsByIds($inOrder, $company->id, $toOrderDocument);
    }

    /**
     * @param int $productionType
     *
     * @throws NotFoundHttpException
     */
    private function _checkProductionType($productionType)
    {
        if (!isset(Product::$productionTypes[$productionType])) {
            throw new NotFoundHttpException('Не указан тип товара.');
        }
    }

    /**
     * Finds the Product model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     *
     * @param $productionType
     * @param integer $id
     *
     * @param bool $allowDeleted
     *
     * @return Product the loaded model
     * @throws NotFoundHttpException
     */
    protected function findModel($productionType, $id)
    {
        /* @var $user StoreUser */
        $user = Yii::$app->user->identity;
        $contractor = $user->getCurrentKubContractor();
        $companyID = $user->company ? $user->company->id : $contractor->company->id;

        /* @var Product $model */
        $query = Product::find()->andWhere([
                'id' => $id,
                'company_id' => $companyID,
                'is_deleted' => false,
                'not_for_sale' => false,
            ]);

        if ($productionType !== null) {
            $query->byProductionType($productionType);
        }

        $model = $query->one();

        if ($model !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('Страница не найдена');
        }
    }

    /**
     * @param \yii\base\Action $action
     * @return bool|\yii\web\Response
     * @throws ForbiddenHttpException
     * @throws \yii\web\BadRequestHttpException
     */
    public function beforeAction($action)
    {
        /* @var $user StoreUser */
        $user = Yii::$app->user->identity;
        if (parent::beforeAction($action)) {
            if (!Yii::$app->user->identity->isContractorActive && $user->company_id == null) {
                $this->redirect(['/order/index'])->send();
            }

            if (($company = $user->getCurrentKubCompany()) && !$company->createInvoiceAllowed(Documents::IO_TYPE_OUT)) {
                $message = 'Количество заказов у поставщика достигло лимита.';
                $message .= "\r\n";
                $message .= 'За подробной информацией обратитесь к ';
                $message .= $company->getTitle(true) . '.';

                throw new ForbiddenHttpException($message);
            }

            return true;
        }

        return false;
    }
}
