<?php
namespace store\controllers;

use common\models\company\CompanySite;
use common\models\Contractor;
use common\models\store\StoreCompanyContractor;
use common\models\store\StoreUser;
use store\components\StoreController;
use store\models\LoginForm;
use store\models\PasswordResetRequestForm;
use store\models\ResetPasswordForm;
use store\models\SignupForm;
use store\models\ContactForm;
use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\widgets\ActiveForm;
use yii\filters\AccessControl;
use yii\filters\Cors;
use yii\filters\VerbFilter;
use yii\helpers\Url;

/**
 * Site controller
 */
class SiteController extends StoreController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'corsFilter' => [
                'class' => Cors::className(),
                'only' => ['login', 'login-js'],
                'cors' => [
                    'Origin' => CompanySite::find()->select('url')->column(),
                    'Access-Control-Allow-Credentials' => true,
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'logout', 'blocked', 'provider', 'no-provider', 'statistic-range'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions' => ['error', 'signup', 'login', 'login-js', 'request-password-reset', 'reset-password'],
                        'allow' => true,
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionBlocked()
    {
        //if (Yii::$app->user->identity->currentStoreCompany !== null) {
        //    return $this->redirect(Yii::$app->defaultRoute);
        //}

        return $this->render('blocked');
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionNoProvider()
    {
        if (Yii::$app->user->identity->currentKubContractor !== null) {
            return $this->redirect(Yii::$app->defaultRoute);
        }

        return $this->render('no-provider');
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionProvider($id)
    {
        $storeCompany = Yii::$app->user->identity->currentStoreCompany;
        $contractor = $storeCompany->getContractors()->andWhere(['id' => $id])->one();

        if ($contractor !== null) {
            Yii::$app->user->identity->updateAttributes([
                'contractor_id' => $contractor->id,
            ]);

            return $this->redirect(Yii::$app->defaultRoute);
        } else {
            throw new NotFoundHttpException('Страница не найдена');
        }
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin($form = null)
    {
        if ($form == 'cors') {
            return $this->renderPartial('login_cors');
        }
        $key = Yii::$app->request->get('key');

        $model = new LoginForm();

        if (Yii::$app->request->post('ajax')) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $model->load(Yii::$app->request->post());

            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            if (Yii::$app->user->identity->isContractorActive) {
                return $this->redirect(['/product/index']);
            }

            return $this->goBack();
        } else {
            /* @var $storeUser StoreUser */
            if ($key && ($storeUser = StoreUser::find()->where(['login_key' => $key])->one()) !== null) {
                $model->email = $storeUser->email;
                $model->password = $storeUser->login_password;
                $model->rememberMe = false;
                if ($model->login()) {
                    return $this->redirect(['/product/index']);
                }
            } else {
                $model->password = '';

                return $this->render('login', [
                    'model' => $model,
                ]);
            }
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLoginJs()
    {
        $loginUrl = Url::toRoute(['/site/login'], Yii::$app->request->isSecureConnection ? 'https' : 'http');
        $content = $this->renderPartial('login-js/login.min.js');
        $content = strtr($content, ['[[LOGIN_URL]]' => $loginUrl]);
        $response = Yii::$app->getResponse();
        $response->headers->set('Content-Type', 'application/javascript');
        $response->format = Response::FORMAT_RAW;
        $response->content = $content;

        return $response->send();
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                if (Yii::$app->getUser()->login($user)) {
                    return $this->goHome();
                }
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     *
     */
    public function actionStatisticRange()
    {
        $post = Yii::$app->request->post();
        if (!empty($post)) {
            Yii::$app->user->identity->setStatisticRangeDates($post['date_from'], $post['date_to']);
            Yii::$app->user->identity->setStatisticRangeName($post['name'], $post['date_from'], $post['date_to']);
            echo true;
        } else {
            echo false;
        }
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'На Ваш e-mail: ' . $model->email . ' были высланы дальнейшие инструкции.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Извините, мы не можем сбросить пароль для указанного адреса электронной почты.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'Новый пароль сохранён.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
}
