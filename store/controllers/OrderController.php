<?php

namespace store\controllers;

use common\components\date\DateHelper;
use common\components\filters\AjaxFilter;
use common\components\helpers\ArrayHelper;
use common\components\pdf\PdfRenderer;
use common\components\pdf\Printable;
use common\components\phpWord\PhpWordOrderDocument;
use common\models\Company;
use common\models\document\OrderDocument;
use common\models\document\OrderDocumentProduct;
use common\models\document\status\OrderDocumentStatus;
use common\models\product\Product;
use common\models\store\StoreUser;
use frontend\models\Documents;
use frontend\modules\documents\assets\DocumentPrintAsset;
use frontend\modules\documents\components\Message;
use frontend\modules\documents\components\OrderDocumentHelper;
use frontend\modules\documents\components\OrderDocumentProductHelper;
use store\components\StoreController;
use store\models\OrderDocumentSearch;
use store\models\StoreCompanyContractor;
use Yii;
use yii\base\Exception;
use yii\db\Connection;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\helpers\Url;
use yii\web\ForbiddenHttpException;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use common\models\file;

/**
 * Class OrderController
 * @package store\controllers
 */
class OrderController extends StoreController
{
    /**
     * @var int
     */
    public $typeDocument = Documents::DOCUMENT_ORDER;

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        /* @var $user StoreUser */
        $user = Yii::$app->user->identity;
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'view', 'new-request', 'document-print', 'docx', 'copy',
                            'update-status'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) use ($user) {
                            return $user->getCurrentKubContractor() !== null || $user->company_id !== null;
                        },
                    ],
                    [
                        'actions' => ['create', 'update'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) use ($user) {
                            return $user->isContractorActive || $user->company_id !== null;
                        },
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'ajax' => [
                'class' => AjaxFilter::className(),
                'only' => ['new-request'],
            ],
        ];
    }

    /**
     * @return mixed
     */
    public function actionIndex()
    {
        /* @var $user StoreUser */
        $user = Yii::$app->user->identity;
        $contractor = $user->getCurrentKubContractor();
        $company = $user->company ? $user->company : $contractor->company;
        $searchModel = new OrderDocumentSearch([
            'company_id' => $company->id,
            'contractor_id' => $contractor ? $contractor->id : null,
        ]);
        $dataProvider = $searchModel->search(Yii::$app->request->get());
        $dashBoardData = OrderDocument::getStoreDashBoardData($user);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'storeUser' => $user,
            'dashBoardData' => $dashBoardData,
        ]);
    }

    /**
     * @return string|Response
     * @throws \Exception
     * @throws \Throwable
     */
    public function actionCreate()
    {
        /* @var $user StoreUser */
        $user = Yii::$app->user->identity;
        if (($company = $user->getCurrentKubCompany()) && !$company->createInvoiceAllowed(Documents::IO_TYPE_OUT)) {
            $message = 'Количество заказов у поставщика достигло лимита.';
            $message .= "\r\n";
            $message .= 'За подробной информацией обратитесь к ';
            $message .= $company->getTitle(true) . '.';

            throw new ForbiddenHttpException($message);
        }
        $contractor = $user->getCurrentKubContractor();

        $company = $user->company ? $user->company : $contractor->company;

        $model = new OrderDocument();
        $model->company_id = $company->id;
        $model->store_author_id = $user->id;
        $model->document_date = date(DateHelper::FORMAT_DATE);
        $model->contractor_id = $contractor ? $contractor->id : null;
        $model->store_id = $company->mainStore->id;
        $model->has_nds = $company->companyTaxationType->osno ?
            OrderDocument::HAS_NDS :
            OrderDocument::HAS_NO_NDS;
        $model->status_id = OrderDocumentStatus::STATUS_NEW;
        $model->document_number = (string)OrderDocument::getNextDocumentNumber($company, null, $model->document_date);
        $model->from_store = true;

        $model->agreement = $contractor ? ($contractor->last_basis_document ?:
            ($contractor->randomAgreement ? $contractor->randomAgreement->listItemValue : null)) : null;

        $model->populateRelation('company', $company);

        $orderDocumentSaved = Yii::$app->db->transaction(function (Connection $db) use ($model, $company) {
            if (OrderDocumentHelper::load($model, Yii::$app->request->post())) {
                if (OrderDocumentHelper::save($model, true, null)) {
                    return true;
                }
            }
            $db->transaction->rollBack();

            return false;
        });

        if ($orderDocumentSaved) {
            Yii::$app->session->setFlash('success', 'Заказ создан.');

            return $this->redirect(['view', 'id' => $model->id]);
        }

        $addProduct = Yii::$app->request->post('count');
        if ($addProduct) {
            $orderArray = [];
            foreach ($addProduct as $pid => $count) {
                if ($product = Product::getById($model->company_id, $pid)) {
                    $order = OrderDocumentProductHelper::createOrderByProduct($product, $model, $count);
                    $orderArray[] = $order;
                }
            }
            $model->populateRelation('orderDocumentProducts', $orderArray);
            $model->beforeValidate();
        }

        $referrer = Yii::$app->request->referrer ?: '';
        if (strpos($referrer, 'order/create')) {
            $referrer = '';
        }
        if ($referrer) {
            Url::remember($referrer, 'order_form_back');
        }

        return $this->render('create', [
            'model' => $model,
            'company' => $company,
            'user' => $user,
        ]);
    }

    /**
     * @param $id
     * @return string|Response
     * @throws NotFoundHttpException
     */
    public function actionUpdate($id)
    {
        /* @var $user StoreUser */
        $user = Yii::$app->user->identity;
        $company = $user->company ? $user->company : $user->getCurrentKubCompany();
        $model = $this->findModel($id);

        if (OrderDocumentHelper::load($model, Yii::$app->request->post()) && OrderDocumentHelper::save($model)) {
            Yii::$app->session->setFlash('success', 'Заказ изменен');

            return $this->redirect(['view',
                'id' => $id
            ]);
        }

        $referrer = Yii::$app->request->referrer ?: '';
        if (strpos($referrer, 'order/update')) {
            $referrer = '';
        }
        if ($referrer) {
            Url::remember($referrer, 'order_form_back');
        }

        return $this->render('create', [
            'model' => $model,
            'company' => $company,
            'user' => $user,
        ]);
    }

    /**
     * @param $id
     * @return mixed
     */
    public function actionView($id)
    {
        /* @var $user StoreUser */
        $user = Yii::$app->user->identity;

        return $this->render('view', [
            'model' => $this->findModel($id, true),
            'user' => $user,
        ]);
    }

    /**
     * @return array
     */
    public function actionNewRequest()
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        $user = Yii::$app->user->identity;
        $newRequests = $user->getNewRequests()->indexBy('contractor_id')->all();
        $link = YII::$app->request->post('link');

        foreach ($link as $id => $value) {
            if (isset($newRequests[$id])) {
                $newRequests[$id]->accept($value);
            }
        }

        $newRequests = $user->getNewRequests()->all();

        return [
            'content' => $this->renderPartial('new-request', [
                'newRequests' => $newRequests,
            ]),
            'newCount' => count($newRequests),
        ];
    }

    /**
     * @param $actionType
     * @param $id
     * @return string|void
     * @throws Exception
     * @throws NotFoundHttpException
     */
    public function actionDocumentPrint($actionType, $id)
    {
        $model = $this->findModel($id);

        return $this->_documentPrint($model, DocumentPrintAsset::className(), $actionType, 'pdf-view', [
            'addStamp' => 0,
        ]);
    }

    /**
     * @param $model
     * @param $asset
     * @param $actionType
     * @param $view
     * @param array $params
     * @return string|void
     * @throws Exception
     */
    protected function _documentPrint(OrderDocument $model, $asset, $actionType, $view, $params = [])
    {
        if (!($model instanceof Printable)) {
            throw new Exception('Model must be instance of Printable.');
        }
        $renderer = new PdfRenderer([
            'view' => $view,
            'params' => array_merge([
                'model' => $model,
                'message' => isset(Message::$message[$this->typeDocument]) ?
                    new Message($this->typeDocument, Documents::IO_TYPE_OUT) :
                    null,
                'ioType' => null,
            ], $params),

            'destination' => PdfRenderer::DESTINATION_BROWSER,
            'filename' => $model->getPrintTitle() . '.pdf',
            'displayMode' => ArrayHelper::getValue($params, 'displayMode', PdfRenderer::DISPLAY_MODE_FULLPAGE),
            'format' => ArrayHelper::getValue($params, 'documentFormat', 'A4-P'),
        ]);
        $this->view->params['asset'] = $asset;
        switch ($actionType) {
            case 'pdf':
                return $renderer->output();
            case 'print':
            default:
                if ($this->action->id != 'out-view') {
                    Yii::$app->view->registerJs('window.print();');
                }

                return $renderer->renderHtml();
        }
    }

    /**
     * @param $id
     * @return mixed
     * @throws NotFoundHttpException
     */
    public function actionDocx($id)
    {
        $model = $this->findModel($id);
        $word = new PhpWordOrderDocument($model);

        return $word->getFile();
    }

    /**
     * @param $id
     * @return Response
     * @throws NotFoundHttpException
     */
    public function actionCopy($id)
    {
        $cloneModel = $this->cloneOrderDocument($id);
        $cloneModel->status_id = OrderDocumentStatus::STATUS_NEW;
        $cloneModel->status_store_author_id = $cloneModel->store_author_id = Yii::$app->user->identity->id;
        if (OrderDocumentHelper::save($cloneModel, false)) {
            Yii::$app->session->setFlash('success', 'Заказ скопирован');

            return $this->redirect(['view', 'id' => $cloneModel->id,]);
        } else {
            return $this->redirect(['view', 'id' => $id,]);
        }
    }

    /**
     * @param $id
     * @return OrderDocument
     * @throws NotFoundHttpException
     */
    public function cloneOrderDocument($id)
    {
        $model = $this->findModel($id);
        /* @var $user StoreUser */
        $user = Yii::$app->user->identity;
        $company = $user->company ? $user->company : $user->getCurrentKubCompany();

        $clone = clone $model;
        unset($clone->id);
        $clone->isNewRecord = true;
        $clone->invoice_id = null;
        $clone->document_date = date(DateHelper::FORMAT_DATE);
        $clone->document_number = $model->getNextDocumentNumber($company, null, $clone->document_date);
        $cloneOrders = array_map([$this, 'cloneOrderDocumentProducts'], $model->orderDocumentProducts);
        $clone->populateRelation('orderDocumentProducts', $cloneOrders);

        return $clone;
    }

    /**
     * @param OrderDocumentProduct $order
     * @return OrderDocumentProduct
     */
    public function cloneOrderDocumentProducts(OrderDocumentProduct $order)
    {
        $cloneOrder = clone $order;
        unset($cloneOrder->id);
        $cloneOrder->isNewRecord = true;
        $cloneOrder->order_document_id = null;

        return $cloneOrder;
    }

    /**
     * @param $id
     * @return array
     * @throws NotFoundHttpException
     */
    public function actionUpdateStatus($id)
    {
        $statusID = Yii::$app->request->post('status_id');
        $statusArray = [
            OrderDocumentStatus::STATUS_CANCELED,
        ];

        if (!in_array($statusID, $statusArray)) {
            throw new NotFoundHttpException('Запрошенная страница не существует.');
        }
        $model = $this->findModel($id);
        if ($model->status_id != $statusID) {
            $model->status_id = $statusID;
            if ($model->save()) {
                Yii::$app->session->setFlash('success', 'Заказ отменён');
            } else {
                Yii::$app->session->setFlash('error', 'Заказ нельзя отменить');
            }
        }

        return $this->redirect(['view', 'id' => $model->id,]);
    }

    /**
     * @param $id
     * @return OrderDocument
     * @throws NotFoundHttpException
     */
    protected function findModel($id = null, $canDeleted = false)
    {
        /* @var $user StoreUser */
        $user = Yii::$app->user->identity;
        $contractor = $user->getCurrentKubContractor();
        $company = $user->company ? $user->company : $contractor->company;
        if ($id === null) {
            $id = Yii::$app->request->get('id');
        }
        $query = OrderDocument::find()
            ->andWhere(['id' => $id]);
        if ($contractor) {
            $query->andFilterWhere(['contractor_id' => $contractor->id]);
        } else {
            $query->andWhere(['company_id' => $company->id]);
        }
        $model = $query->one();
        if ($model !== null && (!$model->is_deleted || $canDeleted)) {
            return $model;
        }
        throw new NotFoundHttpException('Запрошенная страница не существует.');
    }
}
