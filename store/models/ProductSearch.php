<?php

namespace store\models;

use common\models\Company;
use common\models\document\Invoice;
use common\models\document\Order;
use common\models\document\OrderDocument;
use common\models\document\OrderDocumentProduct;
use common\models\document\status\InvoiceStatus;
use common\models\document\status\OrderDocumentStatus;
use common\models\employee\Employee;
use common\models\product\Product;
use common\models\product\ProductStore;
use common\models\store\StoreUser;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;
use yii\db\Expression;
use yii\db\Query;

/**
 * ProductSearch
 */
class ProductSearch extends Product
{
    public $exclude;
    public $exclOd;
    public $quantity;
    public $new;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'group_id', 'product_unit_id', 'exclOd', 'new'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return Model::scenarios();
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'quantity' => 'Количество на складе',
        ]);
    }

    /**
     * @return ActiveQuery
     */
    public function baseQuery()
    {
        $query = ProductSearch::find()->alias('prod')
            ->leftJoin(['s' => $this->storeQuery()], '{{prod}}.[[id]] = {{s}}.[[product_id]]')
            ->leftJoin(['r' => $this->reserveQueryNew()], '{{prod}}.[[id]] = {{r}}.[[product_id]]')
            ->byCompany($this->company_id)
            ->notForSale(false)
            ->byStatus(Product::ACTIVE)
            ->andWhere(['prod.production_type' => $this->production_type])
            ->andWhere(['prod.is_deleted' => false])
            ->andWhere([
                'or',
                ['prod.production_type' => Product::PRODUCTION_TYPE_SERVICE],
                ['>', 'IFNULL({{s}}.[[total]], 0)', new Expression('IFNULL({{r}}.[[total]], 0)')],
            ]);

        return $query;
    }

    /**
     * @return Query
     */
    public function reserveQueryNew()
    {
        $query = new Query;
        $subQuery1 = OrderDocumentProduct::find()->alias('odp')
            ->leftJoin(['od' => OrderDocument::tableName()], '{{odp}}.[[order_document_id]] = {{od}}.[[id]]')
            ->leftJoin(['invoice' => Invoice::tableName()], '{{od}}.[[invoice_id]] = {{invoice}}.[[id]]')
            ->select(['odp.product_id', 'count' => 'odp.quantity'])
            ->andWhere(['od.company_id' => $this->company_id])
            ->andWhere(['od.is_deleted' => false])
            ->andWhere(['not', ['od.status_id' => [OrderDocumentStatus::STATUS_RETURN, OrderDocumentStatus::STATUS_CANCELED]]])
            ->andWhere([
                'or',
                ['invoice.id' => null],
                ['invoice.is_deleted' => true],
                ['invoice.invoice_status_id' => InvoiceStatus::STATUS_REJECTED],
            ]);
        $subQuery2 = Order::find()->alias('or')
            ->select(['or.product_id', 'count' => 'or.reserve'])
            ->joinWith(['product pr'], false)
            ->andWhere(['pr.company_id' => $this->company_id])
            ->andWhere(['pr.production_type' => Product::PRODUCTION_TYPE_GOODS]);
        $query->select(['t.product_id', 'total' => 'SUM({{t}}.[[count]])'])
            ->from(['t' => $subQuery1->union($subQuery2, true)])
            ->groupBy('product_id');

        return $query;
    }

    /**
     * @return ActiveQuery
     */
    public function storeQuery()
    {
        $query = ProductStore::find()->alias('ps')
            ->joinWith('store store', false)
            ->select(['ps.product_id', 'total' => 'SUM({{ps}}.[[quantity]])'])
            ->andWhere(['store.company_id' => $this->company_id])
            ->andWhere(['store.is_closed' => false])
            ->groupBy('ps.product_id');

        return $query;
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params = [])
    {
        /* @var Employee|StoreUser $user */
        $user = Yii::$app->user->identity;
        $this->load($params);

        $query = $this->baseQuery()
            ->joinWith('group group')
            ->select([
                //'prod.*',
                'prod.id',
                'prod.title',
                'prod.article',
                'prod.company_id',
                'prod.group_id',
                'prod.product_unit_id',
                'prod.comment_photo',
                'prod.price_for_sell_with_nds',
                'prod.weight',
                'quantity' => 'GREATEST(IFNULL({{s}}.[[total]], 0) - IFNULL({{r}}.[[total]], 0), 0)',
            ]);

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'title',
                    'article',
                    'quantity',
                    'price_for_sell_with_nds',
                ],
                'defaultOrder' => [
                    'title' => SORT_ASC,
                ],
            ],
        ]);

        if (!empty($this->exclude)) {
            $query->andWhere(['not', ['prod.id' => $this->exclude]]);
        }

        $query->andFilterWhere([
            'prod.group_id' => $this->group_id,
            'prod.product_unit_id' => $this->product_unit_id,
        ]);

        $query->andFilterWhere([
            'or',
            ['like', 'prod.title', $this->title],
            ['like', 'group.title', $this->title],
        ]);

        /* @var $company Company */
        if ($this->new && $user instanceof StoreUser) {
            $company = $user->company ? $user->company : $user->getCurrentKubContractor()->company;
            if ($company->store_show_novelty_button) {
                $days = intval($company->store_novelty_product_by_days_count);
                $query->andWhere(['>', 'prod.created_at', date_create('today -' . $days . ' days')->getTimestamp()]);
            }
        }
        if ($user instanceof StoreUser && ($company = $user->company) !== null) {
            if ($company->store_where_empty_products_type === Company::WHERE_EMPTY_PRODUCTS_TYPE_NOT_SHOW) {
                $query->andHaving(['>', 'quantity', 0]);
            }
        }

        return $dataProvider;
    }
}
