<?php

namespace store\models;

use Yii;

/**
 * This is the model class for table "store_company_contractor".
 *
 * @property integer $store_company_id
 * @property integer $contractor_id
 * @property integer $status
 * @property integer $created_at
 *
 * @property Contractor $contractor
 * @property StoreCompany $storeCompany
 */
class StoreCompanyContractor extends \common\models\store\StoreCompanyContractor
{
}
