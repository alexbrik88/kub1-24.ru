<?php
namespace store\models;

use yii\base\Model;
use common\models\store\StoreUser;
use common\models\TimeZone;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $email;
    public $phone;
    public $password;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['email', 'username', 'phone'], 'trim'],
            [['email', 'username', 'phone'], 'required'],
            [['email', 'username', 'phone'], 'string', 'max' => 255],

            ['email', 'email'],
            ['email', 'unique', 'targetClass' => StoreUser::class, 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Имя',
            'email' => 'Email',
            'phone' => 'Телефон',
            'password' => 'Пароль',
        ];
    }

    /**
     * Signs user up.
     *
     * @return StoreUser|null the saved model or null if saving fails
     */
    public function signup()
    {
        if (!$this->validate()) {
            return null;
        }

        $user = new StoreUser();
        $user->email = $this->email;
        $user->username = $this->username;
        $user->phone = $this->phone;
        $user->time_zone_id = TimeZone::DEFAULT_TIME_ZONE;
        $user->setPassword($this->password);
        $user->generateAuthKey();

        return $user->save() ? $user : null;
    }
}
