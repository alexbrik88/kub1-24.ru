<?php
namespace store\models;

use Yii;
use yii\base\Model;
use common\models\store\StoreUser;

/**
 * Password reset request form
 */
class PasswordResetRequestForm extends Model
{
    public $email;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'exist',
                'targetClass' => '\common\models\store\StoreUser',
                'filter' => ['status' => StoreUser::STATUS_ACTIVE],
                'message' => 'Пользователь с таким e-mail не найден.'
            ],
        ];
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return bool whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user StoreUser */
        $user = StoreUser::findOne([
            'status' => StoreUser::STATUS_ACTIVE,
            'email' => $this->email,
        ]);

        if (!$user) {
            return false;
        }

        if (!StoreUser::isPasswordResetTokenValid($user->password_reset_token)) {
            $user->generatePasswordResetToken();
            if (!$user->save()) {
                return false;
            }
        }

        return Yii::$app
            ->mailer
            ->compose([
                'html' => 'passwordResetToken/html',
                'text' => 'passwordResetToken/text',
            ], [
                'user' => $user,
                'subject' => 'Восстановление пароля',
                'supportEmail' => Yii::$app->params['emailList']['support'],
            ])
            ->setFrom(['support@kub-24.ru' => Yii::$app->params['emailFromName']])
            ->setTo($this->email)
            ->setSubject('Восстановление пароля')
            ->send();
    }
}
