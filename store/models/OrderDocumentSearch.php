<?php

namespace store\models;


use common\components\helpers\ArrayHelper;
use common\models\cash\CashBankFlowToInvoice;
use common\models\cash\CashEmoneyFlowToInvoice;
use common\models\cash\CashOrderFlowToInvoice;
use common\models\Company;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\document\Invoice;
use common\models\document\OrderDocument;
use frontend\components\StatisticPeriod;
use yii\base\Model;
use Yii;
use yii\data\ActiveDataProvider;
use yii\db\ActiveQuery;

/**
 * Class OrderDocumentSearch
 * @package store\models
 */
class OrderDocumentSearch extends OrderDocument
{
    /**
     * @var
     */
    public $byNumber;
    public $paymentSum;

    /**
     * @return array
     */
    public function rules()
    {
        return [
            [['status_id', 'author_id'], 'integer'],
            [['byNumber'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'document_number' => '№',
            'document_date' => 'Дата',
            'company_id' => 'Контрагент',
            'total_amount' => 'Сумма',
            'paymentSum' => 'Оплачено',
            'status_id' => 'Статус',
            'ship_up_to_date' => 'План. дата отгрузки',
            'invoice_id' => 'Счет',
        ]);
    }

    /**
     * @return array
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * @param $params
     * @param bool|false $forExport
     * @return ActiveDataProvider
     */
    public function search($params, $forExport = false)
    {
        $query = $this->getBaseQuery()
            ->select([
                self::tableName() . '.*',
                'paymentSum' => '
                    IFNULL(SUM(`bankFlow`.`amount`), 0) +
                    IFNULL(SUM(`orderFlow`.`amount`), 0) +
                    IFNULL(SUM(`emoneyFlow`.`amount`), 0)
                ',
            ])
            ->joinWith([
                'contractor',
                'invoice',
            ])
            ->leftJoin(['bankFlow' => CashBankFlowToInvoice::tableName()], 'bankFlow.invoice_id = invoice.id')
            ->leftJoin(['orderFlow' => CashOrderFlowToInvoice::tableName()], 'orderFlow.invoice_id = invoice.id')
            ->leftJoin(['emoneyFlow' => CashEmoneyFlowToInvoice::tableName()], 'emoneyFlow.invoice_id = invoice.id')
            ->groupBy(OrderDocument::tableName() . '.id');

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => [
                'attributes' => [
                    'paymentSum',
                    'document_date',
                    'total_amount',
                    'ship_up_to_date',
                    'document_number' => [
                        'asc' => [
                            self::tableName() . '.`document_number` * 1' => SORT_ASC,
                            self::tableName() . '.`document_additional_number` * 1' => SORT_ASC,
                        ],
                        'desc' => [
                            self::tableName() . '.`document_number` * 1' => SORT_DESC,
                            self::tableName() . '.`document_additional_number` * 1' => SORT_DESC,
                        ],
                        'default' => SORT_DESC,
                    ],
                ],
                'defaultOrder' => ['document_number' => SORT_DESC],
            ],
        ]);

        $this->load($params);

        if (!empty($this->byNumber)) {
            $paramArray = explode(' ', preg_replace('/\s{2,}/', ' ', trim($this->byNumber)));
            foreach ($paramArray as $param) {

                if ($companyTypeId = CompanyType::find()->select('id')->andWhere(['name_short' => $param])->scalar()) {
                    $query->andWhere([Contractor::tableName() . '.company_type_id' => $companyTypeId]);
                    continue;
                }

                $query->andFilterWhere([
                    'or',

                    ['like', self::tableName() . '.document_number', $param],
                    ['like', self::tableName() . '.document_additional_number', $param],
                    ['like', Contractor::tableName() . '.name', $param]
                ]);
            }
        }

        $query->andFilterWhere([
            self::tableName() . '.status_id' => $this->status_id,
            self::tableName() . '.company_id' => $this->company_id,
        ]);

        return $dataProvider;
    }

    /**
     * @return array
     */
    public function getStatusFilter()
    {
        return ArrayHelper::merge([null => 'Все'], ArrayHelper::map($this->getBaseQuery()
            ->joinWith('status')
            ->groupBy('status_id')
            ->all(), 'status_id', 'status.name'));
    }

    /**
     * @return ActiveQuery
     */
    public function getBaseQuery()
    {
        $dateRange = StatisticPeriod::getSessionPeriod();

        return self::find()->andWhere(['and',
            [self::tableName() . '.is_deleted' => false],
            ['between', self::tableName() . '.document_date', $dateRange['from'], $dateRange['to']],
            [self::tableName() . '.company_id' => $this->company_id],
        ])->andFilterWhere([self::tableName() . '.contractor_id' => $this->contractor_id]);
    }
}