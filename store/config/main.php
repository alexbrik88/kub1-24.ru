<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

$config = [
    'id' => 'app-store',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],
    'controllerNamespace' => 'store\controllers',
    'defaultRoute' => '/order/index',
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-store',
        ],
        'user' => [
            'identityClass' => 'common\models\store\StoreUser',
            'enableAutoLogin' => true,
        ],
        'session' => [
            // this is the name of the session cookie used for login on the store
            'name' => 'advanced-store',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => '/site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            'viewPath' => '@store/mail',
        ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '/js/login-form.min.js' => '/site/login-js',
            ],
        ],

    ],
    'params' => $params,
];

if (YII_ENV_PROD) {
    $config['components']['log']['targets'][] = [
        'class' => 'notamedia\sentry\SentryTarget',
        'dsn' => 'https://1b9e1edd68f64b4d9689f018496b7057:8a51bfb5b5c64af5aae315ec044ca562@sentry.io/1219607',
        'levels' => ['error', 'warning'],
        'except' => ['yii\web\HttpException:403', 'yii\web\HttpException:404'],
        'context' => true
    ];
}
return $config;
