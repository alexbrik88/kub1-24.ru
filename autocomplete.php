<?php

/**
 * Yii bootstrap file.
 * Used for enhanced IDE code autocompletion.
 * Note: To avoid "Multiple Implementations" PHPStorm warning and make autocomplete faster
 * exclude or "Mark as Plain Text" vendor/yiisoft/yii2/Yii.php file
 */
class Yii extends yii\BaseYii
{
    /**
     * @var yii\console\Application|yii\web\Application|BaseApplication|WebApplication|ConsoleApplication the application instance
     */
    public static $app;
}

/**
 * Class BaseApplication
 * Used for properties that are identical for both WebApplication and ConsoleApplication
 *
 * @property-read common\components\KubCompany $kubCompany
 * @property-read common\components\Formatter $formatter
 * @property-read yii\queue\db\Queue $queue
 */
abstract class BaseApplication extends yii\base\Application
{}

/**
 * Class WebApplication
 * Include only Web application related components here
 *
 * @property-read yii\web\User|frontend\components\WebUser $user
 * @property-read yii\web\Session|yii\web\DbSession $session
 */
abstract class WebApplication extends yii\web\Application
{}

/**
 * Class ConsoleApplication
 * Include only Console application related components here
 */
abstract class ConsoleApplication extends yii\console\Application
{}
