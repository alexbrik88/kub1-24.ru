<?php

namespace frontend\widgets;

use common\models\EmployeeCompany;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\JsExpression;
use yii\web\View;
use yii\widgets\ActiveForm;

class NewResponsibleEmployeeWidget extends Widget
{
    public ActiveForm $form;
    public EmployeeCompany $model;

    /**
     * EmployeeCompany update form ID
     * @var string
     */
    public string $formId;

    /**
     * EmployeeCompany.date_dismissal input ID
     * @var string
     */
    public string $dateDismissalId;

    /**
     * New responsible Employee input name
     * @var string
     */
    public string $inputName;

    /**
     * Modal visible CSS class
     * 'in' - fore BS3
     * 'show' - fore BS4
     * @var string
     */
    public string $modalVisibleClass;

    protected string $modalId;

    /**
     * Executes the widget.
     * @return string the result of widget execution to be outputted.
     */
    public function run()
    {
        if ($this->formId &&
            $this->dateDismissalId &&
            $this->modalVisibleClass &&
            $this->model->is_working
        ) {
            $this->inputName = Html::getInputName($this->model, 'new_responsible_employee_id');
            $query = EmployeeCompany::find()->where([
                'and',
                ['is_working' => true],
                ['company_id' => $this->model->company_id],
                ['not', ['employee_id' => $this->model->employee_id]],
            ]);

            $data = ArrayHelper::map($query->all(), 'employee_id', 'shortFio');

            if ($data) {
                $this->modalId = $this->formId.'_new_responsible_employee';

                $this->registerJs();

                return $this->render('newResponsibleEmployee', [
                    'form' => $this->form,
                    'model' => $this->model,
                    'data' => $data,
                    'modalId' => $this->modalId,
                    'inputName' => $this->inputName,
                    'modalTitle' => 'Изменить ответственного',
                    'modalText' => "Перевести контрагентов и неоплаченные счета, закрепленные за {$this->model->getShortFio()} на:",
                ]);
            }
        }
    }

    /**
     * @return
     */
    protected function registerJs()
    {
        $js = <<<JS
            $(document).on('submit', '#{$this->formId}', function (e) {
                var form = this;
                var dateDismissal = $('#{$this->dateDismissalId}', form).val();
                if (dateDismissal.length > 0 && !$('#{$this->modalId}', form).hasClass('{$this->modalVisibleClass}')) {
                    e.preventDefault();
                    Ladda.stopAll();
                    $('#{$this->modalId}', form).modal('show');

                    return false;
                }
            });
JS;

        $this->getView()->registerJs($js);
    }
}
