<?php

namespace frontend\widgets;

use common\components\date\DateHelper;
use common\models\Company;
use common\models\DiscountType;
use common\models\service\SubscribeHelper;
use Yii;
use yii\base\InvalidConfigException;
use yii\bootstrap\Widget;

/**
 * Class ReminderWidget
 * @package frontend\modules\subscribe\widgets
 */
class SubscribeReminderWidget extends Widget
{
    /**
     * @var Company
     */
    public $company;

    /**
     * @var
     */
    public $expireDate;

    /**
     * @var
     */
    public $expireDays;

    /**
     * Days before subscription ends to show reminder
     * @var int
     */
    public $remindStart = 8;
    /**
     * Days before subscription ends to show __colorized__ reminder
     * @var int
     */
    public $remindWarningStart = 5;

    /**
     * Key in session to check, if reminder was already shown
     * @var string
     */
    public $sessionKey = 'subscribe-reminder';

    protected $discount;

    /**
     * @inheritdoc
     * @throws InvalidConfigException
     */
    public function init()
    {
        if ($this->company === null) {
            throw new InvalidConfigException('Company must be defined.');
        }

        if (!$this->company->subscribe_payment_key) {
            $this->company->updateAttributes(['subscribe_payment_key' => uniqid()]);
        }

        $subscribeArray = SubscribeHelper::getPayedSubscriptions($this->company->id);
        $this->expireDate = SubscribeHelper::getExpireDate($subscribeArray);
        $this->expireDays = SubscribeHelper::getExpireLeftDays($this->expireDate);

        $this->discount = $this->company->getDiscounts()->andWhere([
            'type_id' => DiscountType::TYPE_1,
            'is_active' => true,
            'is_known' => false,
        ])->andWhere(['<', 'active_from', time()])->andWhere(['>', 'active_to', time()])->one();
    }

    /**
     * @inheritdoc
     * @throws \Exception
     */
    public function run()
    {
        $remind = $discount = '';
        if (
            !(Yii::$app->controller->module && Yii::$app->controller->module->id === 'subscribe') &&
            $this->expireDate && $this->expireDays <= $this->remindStart && $this->checkSession()
        ) {
            $remind = $this->render('subscribeReminderWidget', [
                'company' => $this->company,
                'expireDate' => date(DateHelper::FORMAT_USER_DATE, $this->expireDate),
                'expireDays' => $this->expireDays,
            ]);

            $this->view->registerJs('
                $("#service-reminder-trial.modal").modal("show");
                $.post("/site/view-remaining-modal");
            ');
        }
        if ($this->discount) {
            //$discount = $this->render('subscribeReminderWidgetDiscount', [
            //    'company' => $this->company,
            //]);

            if ($remind) {
                $this->view->registerJs('
                    $("#service-reminder-trial.modal").on("hidden.bs.modal", function () {
                        $("#service-stock.modal").modal("show");
                    });
                ');
            } else {
                $this->view->registerJs('$("#service-stock.modal").modal("show");');
            }

            $this->view->registerJs('
                $("#service-stock.modal").on("shown.bs.modal", function () {
                    $.post("/site/view-discount-modal");
                });
                $(document).on("change", "#discount-switch", function() {
                    $.post("/site/check-discount", $(this).serialize(), function(data) {
                        if (typeof data.is_active !== "undefined") {
                            $(this).prop("checked", data.is_active).trigger("change");
                        } else {
                            $(this).prop("checked", !this.checked).trigger("change");
                        }
                    });
                });
            ');
        }

        return $remind . "\n" . $discount;
    }

    /**
     * Check if reminder was shown
     * @return bool
     */
    private function checkSession()
    {
        $session = Yii::$app->session;
        $newValue = $this->expireDays;
        $oldValue = $session->get($this->sessionKey);

        if ($newValue === $oldValue) {
            return false;
        } else {
            $session->set($this->sessionKey, $newValue);

            return true;
        }
    }
}
