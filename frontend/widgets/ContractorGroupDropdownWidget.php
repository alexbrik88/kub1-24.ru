<?php

namespace frontend\widgets;

use common\models\contractor\ContractorGroup;
use Yii;
use frontend\assets\ContractorGroupDropdownWidgetAsset;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;

/**
 * Class ProductGroupDropdownWidget
 * @package frontend\widgets
 */
class ContractorGroupDropdownWidget extends Select2
{
    /**
     * @var int
     */
    public $type = ContractorGroup::TYPE_WITHOUT;
    public $minimumResultsForSearch = 11;
    public $loadAssets = true;
    public $addDeleteModal = true;

    /**
     * @inheritdoc
     */
    private function _setPluginOptions()
    {
        $this->pluginOptions = array_merge($this->pluginOptions, [
            'width' => '100%',
        ]);
    }
    public function run()
    {
        static $id_palgin = 1;
        $this->options['id'] = $this->options['id']."_".$id_palgin;
        $this->_setPluginOptions();
        if ($this->loadAssets) {
            ContractorGroupDropdownWidgetAsset::register($this->getView());
        }
        $this->processData();
        parent::run();
        echo $this->render('ContractorGroupDropdownWidget', [
            'inputId' => $this->options['id'],
            'type' => $this->type,
            'addDeleteModal' => $this->addDeleteModal,
        ]);
        //$this->pluginName = $this->pluginName.$id_palgin;
        $id_palgin+=1;

    }

    /**
     * Registers script for widget.
     */
    public function processData()
    {
        $groupOptions = [];


        $groupArray = ContractorGroup::getListGroupExistContactor($this->type,Yii::$app->user->identity->company->id);
        foreach ($groupArray as &$group) {
            $groupOptions[$group->id] = [
                'data-editable' => $group->company_id == Yii::$app->user->identity->company->id ? 1 : 0,
                'data-deletable' =>($group->exist_contractor != 0)?0:1,
                'data-type' => $group->type
            ];
        }
        $this->data = ArrayHelper::map($groupArray, 'id', function ($model) {return Html::encode($model->title);});
        $this->options['options'] = $groupOptions;
        $this->options['data-delurl'] = Url::to(['/contractor/group-delete']);
        $this->options['data-editurl'] = Url::to(['/contractor/group-edit']);
        $this->pluginOptions['minimumResultsForSearch'] = $this->minimumResultsForSearch;
        $this->pluginOptions['templateResult'] = new JsExpression('ContractorGroupDropdownItemsTemplate');

        $js = <<<JS
       
        $(document).on("select2:selecting", "#{$this->options['id']}", function(e) {
            var target = e.params.args.originalEvent.target;
            if ($(target).closest("li.select2-results__option").hasClass("editable-expenditure-item")) {
                e.preventDefault();
                
                if (target.classList.contains("edit-exp-item-cancel")) {
                    editContractorGroupCancel();
                }
                if (target.classList.contains("edit-exp-item-apply")) {
                    editContractorGroupApply(this, target);
                }
            }
            if (target.classList.contains("del-exp-item")) {
                e.preventDefault();
                deleteContractorGroup(this, target);
            }
            if (target.classList.contains("edit-exp-item")) {
                e.preventDefault();
                editContractorGroup(this, target);
            }
        });
JS;

        $view = $this->getView();
        $view->registerJs($js, View::POS_HEAD);
    }
}
