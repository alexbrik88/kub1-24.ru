<?php

namespace frontend\widgets;

use common\models\product\CustomPriceGroup;
use common\models\product\Product;
use Yii;
use common\models\product\ProductGroup;
use frontend\assets\ProductPriceDropdownWidgetAsset;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;

/**
 * Class ProductPriceDropdownWidget
 * @package frontend\widgets
 */
class ProductPriceDropdownWidget extends Select2
{
    /**
     * @var int
     */
    public $productionType = Product::PRODUCTION_TYPE_GOODS;
    public $minimumResultsForSearch = 11;

    /**
     * @inheritdoc
     */
    public function run()
    {
        ProductPriceDropdownWidgetAsset::register($this->getView());
        $this->processData();
        parent::run();
        echo $this->render('ProductPriceDropdownWidget', [
            'inputId' => $this->options['id'],
            'productionType' => $this->productionType,
        ]);
    }

    /**
     * Registers script for widget.
     */
    public function processData()
    {
        $groupOptions = [];
        /*
        $groupArray = ProductGroup::find()
            ->andWhere(['and',
                [
                    'or',
                    ['production_type' => $this->productionType],
                    ['production_type' => null],
                ],
                [
                    'or',
                    ['id' => ProductGroup::WITHOUT],
                    ['company_id' => Yii::$app->user->identity->company->id],
                ],
            ])
            ->orderBy([
                'company_id' => SORT_ASC,
                'title' => SORT_ASC,
            ])
            ->all(); */

        $groupArray = CustomPriceGroup::find()->where(['company_id' => Yii::$app->user->identity->company->id])->all();
        foreach ($groupArray as $group) {
            $groupOptions[$group->id] = [
                'data-editable' => $group->company_id == Yii::$app->user->identity->company->id ? 1 : 0,
                'data-deletable' => 1,
            ];
        }
        $this->data = ArrayHelper::map($groupArray, 'id', 'name');
        $this->options['options'] = $groupOptions;
        $this->options['data-delurl'] = Url::to(['/product/price-delete']);
        $this->options['data-editurl'] = Url::to(['/product/price-edit']);
        $this->pluginOptions['minimumResultsForSearch'] = $this->minimumResultsForSearch;
        $this->pluginOptions['templateResult'] = new JsExpression('productPriceDropdownItemsTemplate');

        $js = <<<JS
        $(document).on("select2:selecting", "#{$this->options['id']}", function(e) {
            var target = e.params.args.originalEvent.target;
            if ($(target).closest("li.select2-results__option").hasClass("editable-expenditure-item")) {
                e.preventDefault();
                if (target.classList.contains("edit-exp-item-cancel")) {
                    editProductPriceGroupCancel();
                }
                if (target.classList.contains("edit-exp-item-apply")) {
                    editProductPriceGroupApply(this, target);
                }
            }
            if (target.classList.contains("del-exp-item")) {
                e.preventDefault();
                dleteProductPriceGroup(this, target);
            }
            if (target.classList.contains("edit-exp-item")) {
                e.preventDefault();
                editProductPriceGroup(this, target);
            }
        });
JS;

        $view = $this->getView();
        $view->registerJs($js, View::POS_HEAD);
    }
}
