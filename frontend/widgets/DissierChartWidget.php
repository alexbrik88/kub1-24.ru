<?php

namespace frontend\widgets;

use common\components\zchb\FinancialStatement;
use Exception;
use miloschuman\highcharts\Highcharts;

/**
 * Виджет построения графика динамики выручки, прибыли и стоимости ОС
 */
class DissierChartWidget extends Highcharts
{

    /**
     * @param FinancialStatement $fs
     * @param string $indicator
     * @return string|void
     */
    public static function widgetFS(FinancialStatement $fs, string $indicator)
    {
        $chartData = self::chartData($fs, $indicator);
        if (count($chartData['range']) < 2) {
            return null;
        }
        try {
            return static::widget([
                'id' => 'chart-' . $indicator,
                'options' => [
                    'title' => false,
                    'credits' => [
                        'enabled' => false
                    ],
                    'xAxis' => [
                        'categories' => $chartData['range'],
                        'labels' => [
                            'style' => [
                                'color' => '#aab',
                            ],
                        ],
                    ],
                    'yAxis' => [
                        'title' => false,
                        'labels' => [
                            'style' => [
                                'color' => '#aab',
                            ],
                            'format' => $chartData['format'],
                        ],
                    ],
                    'legend' => false,
                    'series' => [
                        [
                            'color' => '#356df1',
                            'negativeColor' => 'red',
                            'lineWidth' => 3,
                            'shadow' => [
                                'offsetX' => 0,
                                'offsetY' => 0,
                                'width' => 7,
                                'color' => '#356df1',
                            ],
                            'marker' => [
                                'lineWidth' => 2,
                                'lineColor' => 'white',
                                'radius' => 5,
                            ],
                            'name' => self::title($indicator),
                            'data' => $chartData['data'],
                        ],
                    ],
                ],
            ]);
        } catch (Exception $e) {
            return null;
        }
    }

    public static function chartData(FinancialStatement $fs, string $indicator): array
    {
        $avg = 0;
        $data = array_reverse(array_values($fs->get($indicator)));
        foreach ($data as $item) {
            $avg += abs($item);

        }
        if (count($data) > 0) {
            $avg = $avg / count($data);
            if ($avg > 800000) {
                $avg = 1000000;
            } elseif ($avg > 800) {
                $avg = 1000;
            } else {
                $avg = 1;
            }
            foreach ($data as $i => $item) {
                $data[$i] = round($item / $avg, 1);
            }
            if ($avg === 1000000) {
                $avg = ' млн';
            } elseif ($avg === 1000) {
                $avg = ' тыс';
            } else {
                $avg = '';
            }
            $period = $fs->yearList(false, $indicator, false);
        } else {
            $data = [];
            $avg = '';
            $period = [];
        }
        return [
            'data' => $data,
            'format' => '{value}' . $avg,
            'range' => $period,
        ];
    }

    public static function title(string $indicator): string
    {
        switch ($indicator) {
            default:
            case FinancialStatement::INDICATOR_REVENUE:
                return 'Выручка';
            case FinancialStatement::INDICATOR_PROFIT:
                return 'Прибыль';
            case FinancialStatement::INDICATOR_FIXED_ASSETS:
                return 'Сумма';
        }
    }
}