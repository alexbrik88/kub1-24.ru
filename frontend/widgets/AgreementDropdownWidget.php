<?php

namespace frontend\widgets;

use common\models\Agreement;
use common\models\document\Invoice;
use frontend\themes\kub\helpers\Icon;
use kartik\select2\Select2;
use yii\web\JsExpression;

class AgreementDropdownWidget extends Select2 {

    /**
     * @var int
     */

    public $contractorId;
    public $invoiceId;
    public $documentType;

    public $canAddAgreement = true;
    public $canEmptyAgreement = true;

    public $minimumResultsForSearch = 11;
    public $data = [];
    public $options = [];
    public $pluginOptions = [];

    /**
     * @inheritdoc
     */
    public function run()
    {
        $agreements = $this->_getAgreements();

        $this->_setPluginOptions($agreements);
        $this->_setOptions($agreements);
        $this->_setData($agreements);
        $this->_setUpdateUrl();
        $this->_registerJs();

        parent::run();
    }

    private function _getAgreements()
    {
        return Agreement::find()
            ->where(['contractor_id' => $this->contractorId ?: -1])
            ->orderBy(['document_date' => SORT_DESC])
            ->all();
    }

    private function _setData($agreements)
    {
        if ($this->canEmptyAgreement) {
            $this->data[''] = '   ';
        }
        if ($this->canAddAgreement) {
            $this->data['add-modal-agreement'] = Icon::PLUS . ' Добавить договор';
        }
        /** @var Invoice $invoice */
        if ($this->invoiceId && ($invoice = Invoice::findOne($this->invoiceId))) {
            $this->data[$invoice->getListItemValue()] = $invoice->getListItemName();
        }
        /** @var Agreement $agreement */
        foreach ($agreements as $agreement) {
            $this->data[$agreement->getListItemValue(false)] = $agreement->getListItemName();
        }
    }

    private function _setOptions($agreements)
    {
        $this->options['options'] = [];
        /** @var Agreement $a */
        foreach ($agreements as $a) {
            $id = $a->getListItemValue(false);
            $templateId = $a->agreement_template_id;
            $this->options['options'][$id] = [
                'data-id' => $a->id,
                'data-editable' => $templateId ? 0 : 1,
            ];
        }


    }

    private function _setPluginOptions($agreements)
    {
        $this->pluginOptions = array_merge($this->pluginOptions, [
            'width' => '100%',
            'escapeMarkup' => new JsExpression($this->_escapeMarkup()),
            'templateResult' => new JsExpression($this->_templateResult())
        ]);
    }

    private function _setUpdateUrl()
    {
        $id = '%%ID';
        $type = $this->documentType;
        $cid = $this->contractorId;
        $this->options['data-url'] = "/documents/agreement/update?id={$id}&type={$type}&contractor_id={$cid}&old_record=1";
    }

    private function _templateResult()
    {
        return new JsExpression(<<<JS
            function(data, container) {
                const option = $(data.element);
                const icon = '<svg class="svg-icon" style="pointer-events: none!important;" viewBox="0 0 16 16"> <path d="M10.486 1.966L0 12.452V16h3.548L16 3.548 12.452 0l-1.966 1.966zm-7.48 12.723H1.31v-1.694l.327-.328 1.695 1.695-.328.327zm1.254-1.254L2.565 11.74l7.92-7.92 1.695 1.695-7.92 7.92zm8.847-8.848l-1.694-1.694 1.039-1.039 1.694 1.695-1.039 1.038z"></path> </svg>';
                if (option.data('editable') === 1) {
                    container.innerHTML = 
                        '<div>' +
                            '<i class="edit-agreements-item link pull-right" data-id="'+option.data('id')+'" title="Изменить">' + icon + '</i>' +
                            '<div class="item-name">'+data.text+'</div>' +
                        '</div>';
                } else {
                    container.innerHTML = data.text;
                }
                return container;
            }
        JS);
    }

    private function _escapeMarkup()
    {
        return new JsExpression('function(text) { return text; }');
    }

    private function _registerJs()
    {
        $this->view->registerJs(<<<JS
            window._s2val_agreement = null;
            $(document).on("select2:opening", "#{$this->options['id']}", function(e) {
                window._s2val_agreement = this.value;
                this.value = null;
            });
            
            $(document).on("select2:closing", "#{$this->options['id']}", function(e) {
                this.value = _s2val_agreement;
                window._s2val_agreement = null;
            });
            
            $(document).on("select2:selecting", "#{$this->options['id']}", function(e) {
                const target = e.params.args.originalEvent.target;
                let url;
                
                window._s2val_agreement = e.params.args.data.id;
                
                if (target.classList.contains("edit-agreements-item")) {
                    e.preventDefault();
                    url = $(this).data('url').replace('%%ID', $(target).data('id'));
                    $.pjax({url: url, container: "#agreement-form-container", push: false, scrollTo: false});
                    $("#{$this->options['id']}").select2('close');
                    $("#agreement-modal-container").modal("show");
                }
            });
        JS);
    }
}