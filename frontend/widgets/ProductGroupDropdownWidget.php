<?php

namespace frontend\widgets;

use common\models\product\Product;
use Yii;
use common\models\product\ProductGroup;
use frontend\assets\ProductGroupDropdownWidgetAsset;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;

/**
 * Class ProductGroupDropdownWidget
 * @package frontend\widgets
 */
class ProductGroupDropdownWidget extends Select2
{
    /**
     * @var int
     */
    public $productionType = Product::PRODUCTION_TYPE_GOODS;
    public $minimumResultsForSearch = 11;
    public $loadAssets = true;
    public $addDeleteModal = true;

    /**
     * @inheritdoc
     */
    public function run()
    {
        if ($this->loadAssets) {
            ProductGroupDropdownWidgetAsset::register($this->getView());
        }

        $this->processData();
        parent::run();
        echo $this->render('ProductGroupDropdownWidget', [
            'inputId' => $this->options['id'],
            'productionType' => $this->productionType,
            'addDeleteModal' => $this->addDeleteModal,
        ]);
    }

    /**
     * Registers script for widget.
     */
    public function processData()
    {
        $groupOptions = [];
        $groupArray = ProductGroup::find()
            ->andWhere(['and',
                [
                    'or',
                    ['production_type' => $this->productionType],
                    ['production_type' => null],
                ],
                [
                    'or',
                    ['id' => ProductGroup::WITHOUT],
                    ['company_id' => Yii::$app->user->identity->company->id],
                ],
            ])
            ->orderBy([
                'company_id' => SORT_ASC,
                'title' => SORT_ASC,
            ])
            ->all();
        foreach ($groupArray as $group) {
            $groupOptions[$group->id] = [
                'data-editable' => $group->company_id == Yii::$app->user->identity->company->id ? 1 : 0,
                'data-deletable' => $group->getProducts()
                    ->andWhere(['is_deleted' => false])
                    ->exists() ? 0 : 1,
            ];
        }
        $this->data = ArrayHelper::map($groupArray, 'id', function ($model) {return Html::encode($model->title);});
        $this->options['options'] = $groupOptions;
        $this->options['data-delurl'] = Url::to(['/product/group-delete']);
        $this->options['data-editurl'] = Url::to(['/product/group-edit']);
        $this->pluginOptions['minimumResultsForSearch'] = $this->minimumResultsForSearch;
        $this->pluginOptions['templateResult'] = new JsExpression('productGroupDropdownItemsTemplate');

        $js = <<<JS
        $(document).on("select2:selecting", "#{$this->options['id']}", function(e) {
            var target = e.params.args.originalEvent.target;
            if ($(target).closest("li.select2-results__option").hasClass("editable-expenditure-item")) {
                e.preventDefault();
                if (target.classList.contains("edit-exp-item-cancel")) {
                    editProductGroupCancel();
                }
                if (target.classList.contains("edit-exp-item-apply")) {
                    editProductGroupApply(this, target);
                }
            }
            if (target.classList.contains("del-exp-item")) {
                e.preventDefault();
                dleteProductGroup(this, target);
            }
            if (target.classList.contains("edit-exp-item")) {
                e.preventDefault();
                editProductGroup(this, target);
            }
        });
JS;

        $view = $this->getView();
        $view->registerJs($js, View::POS_HEAD);
    }
}
