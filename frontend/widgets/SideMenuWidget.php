<?php

namespace frontend\widgets;

use common\models\Company;
use common\models\company\CompanyTaxationType;
use common\models\company\CompanyType;
use common\models\Contractor;
use common\models\document\InvoiceFacture;
use common\models\employee\Employee;
use common\models\EmployeeCompany;
use common\models\product\Product;
use common\models\product\ProductSearch;
use common\models\service\SubscribeTariffGroup;
use frontend\models\Documents;
use frontend\modules\documents\assets\TooltipAsset;
use frontend\modules\reference\models\ArticlesSearch;
use frontend\modules\tax\models\Kudir;
use frontend\modules\tax\models\TaxDeclaration;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use Yii;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use kartik\checkbox\CheckboxX;

/**
 * Class SideMenuWidget
 * @package frontend\widgets
 */
class SideMenuWidget extends Widget
{
    /**
     * @return string
     * @throws \yii\web\NotFoundHttpException
     */
    public function run()
    {
        $session = Yii::$app->session;
        $sideBarStatus = !empty($session['sidebar']) ? $session['sidebar']['status'] : 0;

        return $this->render('sideMenuWidget', [
            'items' => $this->getItems(),
            'newCompany' => new Company([
                'company_type_id' => CompanyType::TYPE_OOO,
                'scenario' => Company::SCENARIO_CREATE_COMPANY,
            ]),
            'sideBarStatus' => $sideBarStatus,
            'companyTaxation' => new CompanyTaxationType(),
        ]);
    }

    /**
     * @return array
     * @throws \yii\web\NotFoundHttpException
     */
    public function getItems()
    {
        $controller = Yii::$app->controller->id;
        $action = Yii::$app->controller->action->id;
        $module = Yii::$app->controller->module->id;
        $isIp = false;
        $canTaxModule = false;

        /* @var Employee $user */
        $user = Yii::$app->user->identity;

        $paramType = Yii::$app->request->getQueryParam('type');
        $companies = [];
        if (!Yii::$app->user->isGuest) {
            $company = $user->company;
            $visibleOutInvoiceFacture = false;
            $visibleInInvoiceFacture = false;
            if ($company) {
                $visibleByTaxationType = $company->companyTaxationType->osno;
                $visibleOutInvoiceFacture = $visibleByTaxationType ||
                    InvoiceFacture::find()
                        ->alias('if')
                        ->joinWith('invoice invoice')
                        ->where([
                            'invoice.company_id' => $company->id,
                            'invoice.is_deleted' => false,
                            'if.type' => Documents::IO_TYPE_OUT
                        ])
                        ->exists();
                $visibleInInvoiceFacture = $visibleByTaxationType ||
                    InvoiceFacture::find()
                        ->alias('if')
                        ->joinWith('invoice invoice')
                        ->where([
                            'invoice.company_id' => $company->id,
                            'invoice.is_deleted' => false,
                            'if.type' => Documents::IO_TYPE_IN
                        ])
                        ->exists();
                $isIp = $company->company_type_id == CompanyType::TYPE_IP;
                $canTaxModule = $isIp || $company->canOOOTaxModule;

                $companyName = Html::encode($company->getTitle(true));
                $companies[$user->company_id] = [
                    'label' => !empty($companyName) ? $companyName : '(не задано)',
                    'encode' => false,
                    'url' => ['/'],
                    'active' => false,
                ];
            }

            $employeeCompanyArray = $user->getEmployeeCompany()
                ->joinWith('company.companyType')
                ->andWhere([
                    'employee_company.is_working' => true,
                    'company.blocked' => false,
                ])->orderBy([
                    new \yii\db\Expression("ISNULL({{company_type}}.[[name_short]])"),
                    "company_type.name_short" => SORT_ASC,
                    "company.name_short" => SORT_ASC,
                ])->all();
            /* @var EmployeeCompany $employeeCompany */
            foreach ($employeeCompanyArray as $employeeCompany) {
                if ($user->company_id !== $employeeCompany->company->id) {
                    $companyName = Html::encode($employeeCompany->company->getTitle(true));
                    $companies[$employeeCompany->company->id] = [
                        'label' => !empty($companyName) ? $companyName : '(не задано)',
                        'encode' => false,
                        'url' => ['/site/change-company', 'id' => $employeeCompany->company->id],
                        'active' => false,
                        'linkOptions' => [
                            'class' => 'hidden-companies',
                        ],
                    ];
                }
            }

            if ($company && (!trim($company->name_short) || !trim($company->inn))) {
                $companies[] = [
                    'label' => '<i class="glyphicon glyphicon-plus-sign"></i>Добавить',
                    'encode' => false,
                    'url' => ['/company/update'],
                ];
            } else {
                $companies[] = [
                    'label' => '<i class="glyphicon glyphicon-plus-sign"></i>Добавить',
                    'encode' => false,
                    'url' => '#create-company',
                    'linkOptions' => [
                        'class' => 'nav-link nav-toggle hidden-companies',
                        'data-toggle' => 'modal',
                    ],
                ];
            }
            $companyName = $company ? $company->getTitle(true) : null;
            $currentCompanyMenuItem = !empty($companyName) ? $companyName : '(не задано)';

            $companyId = ArrayHelper::getValue(Yii::$app->user, ['identity', 'company', 'id']);
            $allowedIds = ArrayHelper::getValue(Yii::$app->params, ['prjectSectionAllowedIds'], []);
            $prjectSectionAllowed = $companyId && $allowedIds && in_array($companyId, $allowedIds);

            $myCompanies = [
                'label' => '<i class="fa fa-gear multiuser-icon" style="opacity: 1;"></i>Мои компании<span class="arrow"></span>',
                'encode' => false,
                'url' => 'javascript:;',
                'linkOptions' => [
                    'class' => 'nav-link nav-toggle',
                ],
                'active' => false,
                'items' => $companies,
                'options' => [
                    'class' => 'nav-item_accordeon my-companies',
                ],
            ];

            // echo var_dump($companies); die();
            if ($company === null) {
                return [$myCompanies];
            }

            if (!Yii::$app->user->can(permissions\Service::HOME_PAGE)) {
                return [];
            }

            return [
                $myCompanies,
                [
                    'label' => Html::tag('i', '', [
                            'class' => 'icon-home',
                        ]) . Html::tag('span', $currentCompanyMenuItem, [
                            'class' => 'title username'
                        ]),
                    'encode' => false,
                    'url' => ['/site/index'],
                    'active' => true,
                    'linkOptions' => [
                        'style' => 'height: auto;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;',
                        'title' => $currentCompanyMenuItem,
                    ],
                    'options' => [
                        'class' => 'current-company nav-item',
                        'id' => $user->company_id,
                        'style' => 'background: #467eaf;margin-top: 0.8px !important;',
                    ],
                ],
                [
                    'label' => '<i class="flaticon-growth"></i><span class="title">Модуль В2В</span>',
                    'encode' => false,
                    'url' => ['/b2b/index'],
                    'active' => $controller == 'b2b',
                    'linkOptions' => [
                        'class' => 'nav-link nav-toggle',
                    ],
                    'options' => [
                        'class' => 'nav-item menu-none b2b-trigger_menu-item' .
                        (ArrayHelper::getValue($user, 'menuItem.b2b_item') ? '' : ' hidden'),
                    ],
                    'visible' => Yii::$app->user->can(UserRole::ROLE_CHIEF),
                ],
                [
                    'label' => '<i class="icon-basket"></i><span class="title">Покупатели</span>',
                    'encode' => false,
                    'url' => ['/contractor/index', 'type' => Contractor::TYPE_CUSTOMER],
                    'active' => $controller == 'contractor' && $paramType == Contractor::TYPE_CUSTOMER,
                    'linkOptions' => [
                        'class' => 'nav-link nav-toggle',
                    ],
                    'options' => [
                        'class' => 'nav-item menu-none invoice-trigger_menu-item logistics-trigger_menu-item' . (
                            ArrayHelper::getValue($user, 'menuItem.invoice_item') ||
                            ArrayHelper::getValue($user, 'menuItem.logistics_item') ? '' : ' hidden'
                        ),
                    ],
                    'visible' => Yii::$app->user->can(permissions\Contractor::INDEX, [
                        'type' => Contractor::TYPE_CUSTOMER,
                    ]),
                ],
                [
                    'label' => '<i class="flaticon-trolley"></i><span class="title">Поставщики</span>',
                    'encode' => false,
                    'url' => ['/contractor/index', 'type' => Contractor::TYPE_SELLER],
                    'active' => $controller === 'contractor' && $paramType == Contractor::TYPE_SELLER,
                    'linkOptions' => [
                        'class' => 'nav-link nav-toggle',
                    ],
                    'options' => [
                        'class' => 'nav-item menu-none invoice-trigger_menu-item logistics-trigger_menu-item ' . (
                            ArrayHelper::getValue($user, 'menuItem.invoice_item') ||
                            ArrayHelper::getValue($user, 'menuItem.logistics_item') ? '' : ' hidden'
                        ),
                    ],
                    'visible' => Yii::$app->user->can(permissions\Contractor::INDEX, [
                        'type' => Contractor::TYPE_SELLER,
                    ]),
                ],
                [
                    'label' => '<i class="flaticon-new"></i>Продажи<span class="arrow"></span>',
                    'encode' => false,
                    'url' => 'javascript:;',
                    'linkOptions' => [
                        'class' => 'nav-link nav-toggle',
                    ],
                    'options' => [
                        'class' => 'nav-item_accordeon invoice-trigger_menu-item logistics-trigger_menu-item ' . (
                            ArrayHelper::getValue($user, 'menuItem.invoice_item') ||
                            ArrayHelper::getValue($user, 'menuItem.logistics_item') ? '' : ' hidden'
                        ),
                    ],
                    'active' => $module == 'documents' && $paramType != Documents::IO_TYPE_IN
                        || $controller == 'payment-order',
                    'visible' => Yii::$app->user->can(permissions\document\Document::INDEX, [
                        'ioType' => Documents::IO_TYPE_OUT,
                    ]),
                    'items' => [
                        [
                            'label' => 'Счета',
                            'encode' => false,
                            'url' => ['/documents/invoice/index', 'type' => Documents::IO_TYPE_OUT],
                            'active' => $controller === 'invoice' && $paramType != Documents::IO_TYPE_IN,
                        ],
                        [
                            'label' => 'Акты',
                            'encode' => false,
                            'url' => ['/documents/act/index', 'type' => Documents::IO_TYPE_OUT],
                            'active' => $controller === 'act' && $paramType == Documents::IO_TYPE_OUT,
                        ],
                        [
                            'label' => 'Товарные накладные',
                            'encode' => false,
                            'url' => ['/documents/packing-list/index', 'type' => Documents::IO_TYPE_OUT],
                            'active' => $controller === 'packing-list' && $paramType == Documents::IO_TYPE_OUT,
                        ],
                        [
                            'label' => 'Товарно-транспортные накладные',
                            'encode' => false,
                            'url' => ['/documents/waybill/index', 'type' => Documents::IO_TYPE_OUT],
                            'active' => $controller === 'waybill' && $paramType == Documents::IO_TYPE_OUT,
                        ],
                        [
                            'label' => 'Счета-фактуры',
                            'encode' => false,
                            'url' => ['/documents/invoice-facture/index', 'type' => Documents::IO_TYPE_OUT],
                            'active' => $controller === 'invoice-facture' && $paramType == Documents::IO_TYPE_OUT,
                            'visible' => $visibleOutInvoiceFacture,
                        ],
                        [
                            'label' => 'УПД',
                            'encode' => false,
                            'url' => ['/documents/upd/index', 'type' => Documents::IO_TYPE_OUT],
                            'active' => $controller === 'upd' && $paramType == Documents::IO_TYPE_OUT,
                        ],
                        [
                            'label' => 'Заказы',
                            'encode' => false,
                            'url' => ['/documents/order-document/index'],
                            'active' => $controller === 'order-document',
                        ],
                        [
                            'label' => 'Договоры',
                            'encode' => false,
                            'url' => ['/documents/agreement/index', 'type' => Documents::IO_TYPE_OUT],
                            'active' => ($controller === 'agreement' || $controller === 'agreement-template') && $paramType == Documents::IO_TYPE_OUT,
                            'visible' => Yii::$app->user->can(permissions\Contractor::CREATE, [
                                'type' => Contractor::TYPE_CUSTOMER,
                            ]),
                        ],
                        /*[
                            'label' => 'Розничные продажи',
                            'encode' => false,
                            'url' => ['/documents/retail/index'],
                            'active' => $controller==='retail',
                        ],*/
                    ],
                ],
                [
                    'label' => '<i class="flaticon-bottom"></i>Покупки<span class="arrow"></span>',
                    'encode' => false,
                    'url' => 'javascript:;',
                    'linkOptions' => [
                        'class' => 'nav-link nav-toggle',
                    ],
                    'options' => [
                        'class' => 'nav-item_accordeon invoice-trigger_menu-item logistics-trigger_menu-item ' . (
                            ArrayHelper::getValue($user, 'menuItem.invoice_item') ||
                            ArrayHelper::getValue($user, 'menuItem.logistics_item') ? '' : ' hidden'
                        ),
                    ],
                    'active' => $module == 'documents' && $paramType == Documents::IO_TYPE_IN,
                    'visible' => Yii::$app->user->can(permissions\document\Document::INDEX, [
                        'ioType' => Documents::IO_TYPE_IN,
                    ]),
                    'items' => [
                        [
                            'label' => 'Счета',
                            'encode' => false,
                            'url' => ['/documents/invoice/index', 'type' => Documents::IO_TYPE_IN],
                            'active' => $controller === 'invoice' && $paramType == Documents::IO_TYPE_IN,
                        ],
                        [
                            'label' => 'Акты',
                            'encode' => false,
                            'url' => ['/documents/act/index', 'type' => Documents::IO_TYPE_IN],
                            'active' => $controller === 'act' && $paramType == Documents::IO_TYPE_IN,
                        ],
                        [
                            'label' => 'Товарные накладные',
                            'encode' => false,
                            'url' => ['/documents/packing-list/index', 'type' => Documents::IO_TYPE_IN],
                            'active' => $controller === 'packing-list' && $paramType == Documents::IO_TYPE_IN,
                        ],
                        [
                            'label' => 'Счета-фактуры',
                            'encode' => false,
                            'url' => ['/documents/invoice-facture/index', 'type' => Documents::IO_TYPE_IN],
                            'active' => $controller === 'invoice-facture' && $paramType == Documents::IO_TYPE_IN,
                            'visible' => $visibleInInvoiceFacture,
                        ],
                        [
                            'label' => 'УПД',
                            'encode' => false,
                            'url' => ['/documents/upd/index', 'type' => Documents::IO_TYPE_IN],
                            'active' => $controller === 'upd' && $paramType == Documents::IO_TYPE_IN,
                        ],
                        [
                            'label' => 'Платёжные поручения',
                            'encode' => false,
                            'url' => ['/documents/payment-order/index'],
                            'active' => $controller === 'payment-order',
                            'visible' => Yii::$app->user->can(permissions\document\PaymentOrder::INDEX),
                        ],
                        [
                            'label' => 'Распознавание сканов',
                            'encode' => false,
                            'url' => ['/documents/upload-documents/index'],
                            'active' => $controller === 'upload-documents',
                            'visible' => false,
                        ],
                        [
                            'label' => 'Договоры',
                            'encode' => false,
                            'url' => ['/documents/agreement/index', 'type' => Documents::IO_TYPE_IN],
                            'active' => ($controller === 'agreement' || $controller === 'agreement-template') && $paramType == Documents::IO_TYPE_IN,
                            'visible' => Yii::$app->user->can(permissions\Contractor::CREATE, [
                                'type' => Contractor::TYPE_SELLER,
                            ])
                        ],
                        [
                            'label' => 'Доверенности',
                            'encode' => false,
                            'url' => ['/documents/proxy/index', 'type' => Documents::IO_TYPE_IN],
                            'active' => $controller === 'proxy' && $paramType == Documents::IO_TYPE_IN,
                        ],
                    ],
                ],
                [
                    'label' => '<i class="fa fa-rub"></i><span class="title">Деньги</span><span class="arrow"></span>',
                    'encode' => false,
                    'url' => 'javascript:;',
                    'linkOptions' => [
                        'class' => 'nav-link nav-toggle',
                    ],
                    'options' => [
                        'class' => 'nav-item_accordeon invoice-trigger_menu-item logistics-trigger_menu-item accountant-trigger_menu-item ' . (
                            ArrayHelper::getValue($user, 'menuItem.invoice_item') ||
                            ArrayHelper::getValue($user, 'menuItem.analytics_item') ||
                            ArrayHelper::getValue($user, 'menuItem.accountant_item') ||
                            ArrayHelper::getValue($user, 'menuItem.logistics_item') ? '' : ' hidden'
                        ),
                    ],
                    'active' => $module == 'cash',
                    'visible' => Yii::$app->user->can(permissions\Cash::INDEX) ||
                        Yii::$app->user->can(permissions\CashOrder::INDEX),
                    'items' => [
                        [
                            'label' => '<i class="fa fa-bank m-r-sm"></i>Банк',
                            'encode' => false,
                            'url' => ['/cash/bank'],
                            'active' => $module == 'cash' && $controller == 'bank',
                            'visible' => Yii::$app->user->can(permissions\Cash::INDEX),
                        ],
                        [
                            'label' => '<i class="fa fa-money m-r-sm"></i>Касса',
                            'encode' => false,
                            'url' => '/cash/order',
                            'active' => $module == 'cash' && $controller == 'order',
                            'visible' => Yii::$app->user->can(permissions\CashOrder::INDEX),
                        ],
                        [
                            'label' => '<i class="flaticon-wallet31 m-r-sm m-l-n-xs"></i>E-money',
                            'encode' => false,
                            'url' => '/cash/e-money',
                            'active' => $module == 'cash' && $controller == 'e-money',
                            'visible' => Yii::$app->user->can(permissions\Cash::INDEX),
                        ],
                        [
                            'label' => '<i class="fa fa-refresh m-r-sm"></i>Итого',
                            'encode' => false,
                            'url' => '/cash',
                            'active' => $module == 'cash' && $controller == 'default',
                            'visible' => Yii::$app->user->can(permissions\Cash::INDEX),
                        ],
                    ],
                ],
                [
                    'label' => '<i class="fa fa-pencil"></i><i class="fa fa-wrench fa-pencil-wrench"></i><span class="title">' . Product::$productionTypes[Product::PRODUCTION_TYPE_SERVICE] . '</span>',
                    'encode' => false,
                    'url' => ['/product/index', 'productionType' => Product::PRODUCTION_TYPE_SERVICE, 'ProductSearch' => ['filterStatus' => ProductSearch::IN_WORK]],
                    'active' => $controller == 'product' && Yii::$app->request->getQueryParam('productionType') == Product::PRODUCTION_TYPE_SERVICE,
                    'linkOptions' => [
                        'class' => 'nav-link nav-toggle',
                    ],
                    'options' => [
                        'class' => 'nav-item menu-none invoice-trigger_menu-item logistics-trigger_menu-item ' . (
                            ArrayHelper::getValue($user, 'menuItem.invoice_item') ||
                            ArrayHelper::getValue($user, 'menuItem.logistics_item') ? '' : ' hidden'
                        ),
                    ],
                    'visible' => Yii::$app->user->can(permissions\Product::INDEX),
                ],
                [
                    'label' => '<i class="fa fa-cubes"></i>Товары<span class="arrow"></span>',
                    'encode' => false,
                    'url' => 'javascript:;',
                    'linkOptions' => [
                        'class' => 'nav-link nav-toggle',
                    ],
                    'options' => [
                        'class' => 'nav-item_accordeon menu-none invoice-trigger_menu-item logistics-trigger_menu-item ' . (
                            ArrayHelper::getValue($user, 'menuItem.invoice_item') ||
                            ArrayHelper::getValue($user, 'menuItem.logistics_item') ? '' : ' hidden'
                        ),
                    ],
                    'active' => $controller == 'product' && Yii::$app->request->getQueryParam('productionType') == Product::PRODUCTION_TYPE_GOODS,
                    'visible' => Yii::$app->user->can(permissions\Product::INDEX),
                    'items' => [
                        [
                            'label' => '<i class="fa fa-cubes m-r-sm"></i><span class="title">Склад</span>',//'Товар',
                            'encode' => false,
                            'url' => ['/product/index', 'productionType' => Product::PRODUCTION_TYPE_GOODS, 'ProductSearch' => ['filterStatus' => ProductSearch::IN_WORK]],
                            'active' => $controller == 'product' && Yii::$app->request->getQueryParam('productionType') == Product::PRODUCTION_TYPE_GOODS
                                && ($action == 'index' || $action == 'view' || $action == 'create' || $action == 'update'),
                        ],
                        [
                            'label' => '<img class="m-r-sm" src="/img/icons/price-list.png" style="width: 18px;"><span class="title">Прайс-листы</span>',
                            'encode' => false,
                            'url' => ['/price-list', 'per-page' => 20],
                            'active' => $controller == 'price-list',
                        ],
                        [
                            'label' => '<i class="fa fa-refresh m-r-sm"></i><span class="title">Оборот товара</span>',
                            'encode' => false,
                            'url' => ['/product/turnover', 'per-page' => 20, 'productionType' => Product::PRODUCTION_TYPE_GOODS, 'turnoverType' => ProductSearch::TURNOVER_BY_COUNT],
                            'active' => $controller == 'product' && Yii::$app->request->getQueryParam('productionType') == Product::PRODUCTION_TYPE_GOODS
                                && $action == 'turnover' && Yii::$app->request->getQueryParam('turnoverType') == ProductSearch::TURNOVER_BY_COUNT,
                        ],
                        [
                            'label' => '<i class="fa fa-refresh m-r-sm"></i><span class="title">Оборот</span><i class="fa fa-rub"></i>',
                            'encode' => false,
                            'url' => ['/product/turnover', 'per-page' => 20, 'productionType' => Product::PRODUCTION_TYPE_GOODS, 'turnoverType' => ProductSearch::TURNOVER_BY_AMOUNT],
                            'active' => $controller == 'product' && Yii::$app->request->getQueryParam('productionType') == Product::PRODUCTION_TYPE_GOODS
                                && $action == 'turnover' && Yii::$app->request->getQueryParam('turnoverType') == ProductSearch::TURNOVER_BY_AMOUNT,
                            'visible' => Yii::$app->user->can(permissions\document\Document::INDEX, [
                                'ioType' => Documents::IO_TYPE_IN,
                            ]),
                        ],
                        [
                            'label' => '<span class="title">XYZ Анализ</span>',
                            'encode' => false,
                            'url' => ['/product/xyz'],
                            'active' => $controller == 'product' && $action == 'xyz',
                            'visible' => (YII_ENV_DEV || $user->company_id == 486),// <-Удалить условие, когда задача будет принята
                        ],
                    ],
                ],
                [
                    'label' => '<i class="flaticon-truck-1"></i>Логистика<span class="arrow"></span>',
                    'encode' => false,
                    'url' => 'javascript:;',
                    'linkOptions' => [
                        'class' => 'nav-link nav-toggle',
                    ],
                    'options' => [
                        'class' => 'nav-item_accordeon menu-none logistics-trigger_menu-item ' . (
                            ArrayHelper::getValue($user, 'menuItem.logistics_item') ? '' : ' hidden'
                        ),
                    ],
                    'active' => $controller == 'product' && Yii::$app->request->getQueryParam('productionType') == Product::PRODUCTION_TYPE_GOODS,
                    'visible' => YII_ENV_DEV,
                    'items' => [
                        [
                            'label' => '<span class="title">Заявки</span>',
                            'encode' => false,
                            'url' => ['/logistics/request/index'],
                            'active' => $module == 'logistics' && $controller == 'request' && ($action == 'index' || $action == 'view' || $action == 'create' || $action == 'update'),
                        ],
                        [
                            'label' => '<span class="title">ТТН</span>',
                            'encode' => false,
                            'url' => 'javascript:;',
                        ],
                        [
                            'label' => '<span class="title">Путевые листы</span>',
                            'encode' => false,
                            'url' => 'javascript:;',
                        ],
                        [
                            'label' => '<span class="title">Отчеты</span>',
                            'encode' => false,
                            'url' => 'javascript:;',
                        ],
                        [
                            'label' => '<span class="title">Справочники</span>',
                            'encode' => false,
                            'url' => ['/logistics/driver/index'],
                            'active' => $module == 'logistics' && ($controller == 'driver' || $controller == 'vehicle' || $controller == 'address') &&
                                ($action == 'index' || $action == 'view' || $action == 'create' || $action == 'update'),
                        ],
                    ],
                ],
                [
                    'label' => '<i class="flaticon-truck"></i>Логистика<span class="arrow"></span>',
                    'encode' => false,
                    'url' => 'javascript:;',
                    'linkOptions' => [
                        'class' => 'nav-link nav-toggle',
                    ],
                    'active' => $controller == 'product' && Yii::$app->request->getQueryParam('productionType') == Product::PRODUCTION_TYPE_GOODS,
                    'items' => [
                        [
                            'label' => '<span class="title">Заявки</span>',
                            'encode' => false,
                            'url' => ['/logistics/request/index'],
                            'active' => $module == 'logistics' && $controller == 'request' && ($action == 'index' || $action == 'view' || $action == 'create' || $action == 'update'),
                        ],
                        [
                            'label' => '<span class="title">ТТН</span>',
                            'encode' => false,
                            'url' => 'javascript:;',
                        ],
                        [
                            'label' => '<span class="title">Путевые листы</span>',
                            'encode' => false,
                            'url' => 'javascript:;',
                        ],
                        [
                            'label' => '<span class="title">Отчеты</span>',
                            'encode' => false,
                            'url' => 'javascript:;',
                        ],
                        [
                            'label' => '<span class="title">Справочники</span>',
                            'encode' => false,
                            'url' => ['/logistics/driver/index'],
                            'active' => $module == 'logistics' && ($controller == 'driver' || $controller == 'vehicle' || $controller == 'address') &&
                                ($action == 'index' || $action == 'view' || $action == 'create' || $action == 'update'),
                        ],
                    ],
                    'options' => [
                        'class' => 'nav-item_accordeon menu-none',
                    ],
                ],
                [
                    'label' => '<i class="fa fa-line-chart"></i><span class="title">Отчеты</span><span class="arrow">',
                    'encode' => false,
                    'url' => 'javascript:;',
                    'linkOptions' => [
                        'class' => 'nav-link nav-toggle',
                    ],
                    'options' => [
                        'class' => 'nav-item_accordeon invoice-trigger_menu-item ' . (
                            ArrayHelper::getValue($user, 'menuItem.invoice_item') ? '' : ' hidden'
                            ),
                    ],
                    'visible' => Yii::$app->user->can(permissions\Reports::VIEW),
                    'items' => [
                        [
                            'label' => '<span class="title">Финансы</span>',
                            'encode' => false,
                            'url' => ['/reports/finance/odds'],
                            'visible' => Yii::$app->user->can(permissions\Reports::FINANCE),
                            'active' => ($this->params['is_finance_reports_module'] ?? false) && !in_array($action, ['profit-and-loss', 'balance', 'debtor', 'breakeven-point'])
                                || $module == 'reference' && $controller == 'articles',
                        ],
                        [
                            'label' => '<span class="title">Отчеты</span>',
                            'encode' => false,
                            'visible' => Yii::$app->user->can(permissions\Reports::REPORTS),
                            'url' => Yii::$app->user->can(UserRole::ROLE_CHIEF) ?
                                ['/reports/analysis/index'] :
                                ['/reports/debt-report/debtor'],
                            'active' => $module == 'reports' && in_array($controller, [
                                'debtor',
                                'debt-report',
                                'debt-report-seller',
                                'analysis',
                                'discipline',
                                'employees',
                                'invoice-report',
                                'selling-report',
                            ]),
                        ],
                    ],
                ],
                [
                    'label' => '<i class="flaticon-calculator"></i><span class="title">Бухгалтерия</span><span class="arrow">',
                    'encode' => false,
                    'url' => 'javascript:;',
                    'linkOptions' => [
                        'class' => 'nav-link nav-toggle',
                    ],
                    'options' => [
                        'class' => 'nav-item_accordeon accountant-trigger_menu-item ' . (
                            ArrayHelper::getValue($user, 'menuItem.accountant_item') ? '' : ' hidden'
                        ),
                    ],
                    'visible' => $company &&
                        ($company->getCanTaxModule() || $company->getCanOOOTaxModule()) &&
                        Yii::$app->user->can(UserRole::ROLE_CHIEF),
                    'items' => [
                        [
                            'label' => '<span class="title">Бухгалтерия для ИП</span>',
                            'encode' => false,
                            'url' => ['/tax/robot/index'],
                            'active' => \Yii::$app->controller->id == 'robot',
                            'visible' => $company && $company->getCanTaxModule() &&
                                Yii::$app->user->can(UserRole::ROLE_CHIEF),
                        ],
                        [
                            'label' => '<span class="title">Бухгалтерия для ООО</span>',
                            'encode' => false,
                            'url' => ['/tax/declaration-osno/index'],
                            'active' => \Yii::$app->controller->id == 'declaration-osno' && \Yii::$app->controller->action->id != 'index-list',
                            'visible' => $company && $company->getCanOOOTaxModule() &&
                                Yii::$app->user->can(UserRole::ROLE_CHIEF),
                        ],
                        [
                            'label' => '<span class="title">Налоговый календарь</span>',
                            'encode' => false,
                            'url' => ['/notification/index', 'month' => date('m'), 'year' => date('Y')],
                            'active' => \Yii::$app->controller->id == 'notification',
                            'visible' => $company && ($company->getCanTaxModule() || $company->getCanOOOTaxModule()) &&
                                Yii::$app->user->can(UserRole::ROLE_CHIEF),
                        ],
                        [
                            'label' => '<span class="title">Отчетность</span>',
                            'encode' => false,
                            'url' => [($isIp) ? '/tax/declaration/index' : '/tax/declaration-osno/index-list'],
                            'active' => Yii::$app->controller->id == 'declaration' || Yii::$app->controller->id == 'kudir' ||
                                (Yii::$app->controller->id == 'declaration-osno' && \Yii::$app->controller->action->id == 'index-list'),
                            'visible' => $company && ($company->getCanTaxModule() || $company->getCanOOOTaxModule()) &&
                                Yii::$app->user->can(UserRole::ROLE_CHIEF) && (
                                    $company->getHasActualSubscription(SubscribeTariffGroup::TAX_DECLAR_IP_USN_6) ||
                                    $company->getHasActualSubscription(SubscribeTariffGroup::TAX_IP_USN_6) ||
                                    $company->getHasActualSubscription(SubscribeTariffGroup::OOO_OSNO_NULL_REPORTING)
                                ),
                        ],
                    ],
                ],
                /*[
                    'label' => '<i class="fa fa-cubes"></i><span class="title">' . Product::$productionTypes[Product::PRODUCTION_TYPE_GOODS] . '</span>',
                    'encode' => false,
                    'url' => ['/product/index', 'productionType' => Product::PRODUCTION_TYPE_GOODS],
                    'active' => $controller == 'product' && Yii::$app->request->getQueryParam('productionType') == Product::PRODUCTION_TYPE_GOODS,
                    'linkOptions' => [
                        'class' => 'nav-link nav-toggle',
                    ],
                    'visible' => Yii::$app->user->can(permissions\Product::INDEX),
                    'options' => [
                        'class' => 'nav-item',
                    ],
                ],*/
                [
                    'label' => '<i class="fa fa-rocket"></i><span class="title">Проекты</span>',
                    'encode' => false,
                    'url' => ['/project/index'],
                    'active' => $controller == 'project',
                    'linkOptions' => [
                        'class' => 'nav-link nav-toggle',
                    ],
                    'options' => [
                        'class' => 'nav-item menu-none project-trigger_menu-item ' .
                            (ArrayHelper::getValue($user, 'menuItem.project_item') ? '' : 'hidden'),
                    ],
                    'visible' => Yii::$app->user->can(permissions\Project::INDEX) && (YII_ENV_DEV || $prjectSectionAllowed),
                ],
                [
                    'label' => '<i class="fa fa-envelope-o"></i>Почта',
                    'encode' => false,
                    'url' => ['/integration/email'],
                    'visible' => $company && $user->integration(Employee::INTEGRATION_EMAIL) !== null,
                ],
                [
                    'label' => '<i class="fa fa-gear"></i>Настройки<span class="arrow"></span>',
                    'encode' => false,
                    'url' => 'javascript:;',
                    'linkOptions' => [
                        'class' => 'nav-link nav-toggle',
                    ],
                    'items' => [
                        [
                            'label' => '<span class="title">Профиль компании</span>',
                            'encode' => false,
                            'url' => ['/company/index'],
                            'active' => $controller == 'company',
                            'linkOptions' => [
                                'class' => 'nav-link nav-toggle',
                            ],
                            'visible' => Yii::$app->user->can(permissions\Company::PROFILE),
                        ],
                        [
                            'label' => '<span class="title">Сотрудники</span>',
                            'encode' => false,
                            'url' => ['/employee/index'],
                            'active' => $controller == 'employee',
                            'linkOptions' => [
                                'class' => 'nav-link nav-toggle',
                            ],
                            'visible' => Yii::$app->user->can(permissions\Employee::INDEX),
                        ],
                        [
                            'label' => '<span class="title">Оплатить КУБ</span>',
                            'encode' => false,
                            'url' => ['/subscribe'],
                            'active' => $module == 'subscribe' && $controller == 'default',
                            'linkOptions' => [
                                'class' => 'nav-link nav-toggle',
                            ],
                            'visible' => Yii::$app->user->can(permissions\Subscribe::INDEX),
                        ],
                        [
                            'label' => '<span class="title">Автосбор долгов</span>',
                            'encode' => false,
                            'url' => ['/payment-reminder/index'],
                            'active' => $controller == 'payment-reminder',
                            'options' => [
                                'class' => 'invoice-trigger_menu-item' . (
                                    ArrayHelper::getValue($user, 'menuItem.invoice_item') ? '' : ' hidden'
                                ),
                            ],
                            'visible' => Yii::$app->user->can(permissions\PaymentReminder::INDEX),
                        ],
                        [
                            'label' => '<span class="title">Интеграции</span>',
                            'encode' => false,
                            'url' => ['/integration'],
                            'active' => $module === 'integration' && $controller === 'default',
                            'visible' => YII_ENV_DEV || in_array($user->company_id, [1]), // <-Удалить условие, когда задача будет принята
                        ],
                        [
                            'label' => '<span class="title">Фоновые задачи</span>',
                            'encode' => false,
                            'url' => ['/jobs/index'],
                            'active' => $controller === 'jobs',
                            'visible' => (YII_ENV_DEV ||
                                (
                                    in_array($user->company_id, [486, 23083, 1, 1119]) &&
                                    Yii::$app->user->can(UserRole::ROLE_CHIEF)
                                )
                            ),// <-Удалить условие, когда задача будет принята
                        ],
                    ],
                    'options' => [
                        'class' => 'nav-item_accordeon menu-none',
                    ],
                ],
                [
                    'label' => '<i class="glyphicon glyphicon-cloud-download"></i><span class="title">Загрузка / Выгрузка</span><span class="arrow">',
                    'encode' => false,
                    'url' => 'javascript:;',
                    'linkOptions' => [
                        'class' => 'nav-link nav-toggle',
                    ],
                    'items' => [
                        [
                            'label' => '<span class="title">Мои документы</span>',
                            'encode' => false,
                            'url' => ['/documents/specific-document/index'],
                            'active' => $module == 'documents' && ($controller == 'specific-document' || $controller == 'scan-document'),
                        ],
                        [
                            'label' => '<span class="title">Шаблоны</span>',
                            'encode' => false,
                            'url' => ['/template/index'],
                            'active' => $controller == 'template',
                        ],
                        [
                            'label' => '<span class="title">Отчётность</span>',
                            'encode' => false,
                            'url' => ['/report/index'],
                            'active' => $controller == 'report',
                            'visible' => Yii::$app->user->can(permissions\Report::INDEX),
                        ],
                        [
                            'label' => '<span class="title">Выгрузка в 1С</span>',
                            'encode' => false,
                            'url' => ['/export/one-s/index'],
                            'active' => $module == 'export' && $controller == 'one-s',
                        ],
                        [
                            'label' => '<span class="title">Выгрузка документов</span>',
                            'encode' => false,
                            'url' => '/export/files/index',
                            'active' => $module == 'export' && $controller == 'files',
                        ],
                    ],
                    'options' => [
                        'class' => 'nav-item_accordeon menu-none',
                    ],
                ],
                [
                    'label' => '<i class="flaticon-addition"></i><span class="title">Еще</span><span class="arrow">',
                    'encode' => false,
                    'url' => 'javascript:;',
                    'linkOptions' => [
                        'class' => 'nav-link nav-toggle',
                    ],
                    'items' => [
                        [
                            'label' => '<i class="flaticon-new"></i>КУБ.Счета' . CheckboxX::widget([
                                    'id' => 'invoice_item',
                                    'name' => 'invoice-trigger_menu',
                                    'value' => ArrayHelper::getValue($user, 'menuItem.invoice_item'),
                                    'options' => [
                                        'class' => 'invoice-trigger_menu trigger-menu_checkbox',
                                        'data-menu_item' => '.invoice-trigger_menu-item',
                                        'disabled' => !Yii::$app->user->can(permissions\document\Document::SOME_ACCESS),
                                    ],
                                    'pluginOptions' => [
                                        'size' => 'xs',
                                        'threeState' => false,
                                        'inline' => false,
                                        'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                        'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                                    ],
                                ]),
                            'encode' => false,
                            'url' => 'javascript:;',
                            'options' => [
                                'class' => !Yii::$app->user->can(UserRole::ROLE_CHIEF) ? 'no-rules' : null,
                            ],
                        ],
                        [
                            'label' => '<i class="flaticon-growth"></i>КУБ.Модуль В2В' . CheckboxX::widget([
                                    'id' => 'b2b_item',
                                    'name' => 'b2b-trigger_menu',
                                    'value' => ArrayHelper::getValue($user, 'menuItem.b2b_item'),
                                    'options' => [
                                        'class' => 'b2b-trigger_menu trigger-menu_checkbox',
                                        'data-menu_item' => '.b2b-trigger_menu-item',
                                        'disabled' => !Yii::$app->user->can(UserRole::ROLE_CHIEF),
                                    ],
                                    'pluginOptions' => [
                                        'size' => 'xs',
                                        'threeState' => false,
                                        'inline' => false,
                                        'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                        'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                                    ],
                                ]),
                            'encode' => false,
                            'url' => 'javascript:;',
                            'options' => [
                                'class' => !Yii::$app->user->can(UserRole::ROLE_CHIEF) ? 'no-rules' : null,
                            ],
                        ],
                        [
                            'label' => '<i class="flaticon-truck-1"></i>КУБ.Логистика' . CheckboxX::widget([
                                    'id' => 'logistics_item',
                                    'name' => 'logistics-trigger_menu',
                                    'value' => ArrayHelper::getValue($user, 'menuItem.logistics_item'),
                                    'options' => [
                                        'class' => 'logistics-trigger_menu trigger-menu_checkbox',
                                        'data-menu_item' => '.logistics-trigger_menu-item',
                                        'disabled' => !Yii::$app->user->can(UserRole::ROLE_CHIEF),
                                    ],
                                    'pluginOptions' => [
                                        'size' => 'xs',
                                        'threeState' => false,
                                        'inline' => false,
                                        'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                        'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                                    ],
                                ]),
                            'encode' => false,
                            'url' => 'javascript:;',
                            'options' => [
                                'class' => !Yii::$app->user->can(UserRole::ROLE_CHIEF) ? 'no-rules' : null,
                            ],
                            'visible' => YII_ENV_DEV,
                        ],
                        [
                            'label' => '<i class="flaticon-calculator"></i>КУБ.Бухгалтерия' . CheckboxX::widget([
                                    'id' => 'accountant_item',
                                    'name' => 'accountant-trigger_menu',
                                    'value' => ArrayHelper::getValue($user, 'menuItem.accountant_item'),
                                    'options' => [
                                        'class' => 'accountant-trigger_menu trigger-menu_checkbox',
                                        'data-menu_item' => '.accountant-trigger_menu-item',
                                        'disabled' => !(Yii::$app->user->can(UserRole::ROLE_CHIEF) && $canTaxModule),
                                    ],
                                    'pluginOptions' => [
                                        'size' => 'xs',
                                        'threeState' => false,
                                        'inline' => false,
                                        'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                        'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                                    ],
                                ]),
                            'encode' => false,
                            'url' => 'javascript:;',
                            'options' => [
                                'class' => (!Yii::$app->user->can(UserRole::ROLE_CHIEF) ? 'no-rules' : null) .
                                    ($canTaxModule ? null : ' tooltip-side_menu'),
                                'data-tooltip-content' => $canTaxModule ? null : '#accountant_menu-tooltip',
                            ],
                        ],
                        [
                            'label' => '<i class="fa fa-rocket"></i>КУБ.Проекты' . CheckboxX::widget([
                                    'id' => 'project_item',
                                    'name' => 'project-trigger_menu',
                                    'value' => ArrayHelper::getValue($user, 'menuItem.project_item'),
                                    'options' => [
                                        'class' => 'project-trigger_menu trigger-menu_checkbox',
                                        'data-menu_item' => '',
                                        'disabled' => !Yii::$app->user->can(permissions\Project::INDEX),
                                    ],
                                    'pluginOptions' => [
                                        'size' => 'xs',
                                        'threeState' => false,
                                        'inline' => false,
                                        'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                        'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                                    ],
                                ]),
                            'encode' => false,
                            'url' => 'javascript:;',
                            'options' => [
                                'class' => !Yii::$app->user->can(permissions\Project::INDEX) ? 'no-rules' : null,
                            ],
                            'visible' => Yii::$app->user->can(permissions\Project::INDEX) && (YII_ENV_DEV || $prjectSectionAllowed),
                        ],
                        [
                            'label' => '<i class="fa fa-cubes"></i>КУБ.Склад' . CheckboxX::widget([
                                    'id' => 'product_item',
                                    'name' => 'product-trigger_menu',
                                    'value' => ArrayHelper::getValue($user, 'menuItem.product_item'),
                                    'options' => [
                                        'class' => 'product-trigger_menu trigger-menu_checkbox',
                                        'data-menu_item' => '',
                                        'disabled' => !Yii::$app->user->can(UserRole::ROLE_CHIEF),
                                    ],
                                    'pluginOptions' => [
                                        'size' => 'xs',
                                        'threeState' => false,
                                        'inline' => false,
                                        'iconChecked' => '<i class="glyphicon glyphicon-plus"></i>',
                                        'iconUnchecked' => '<i class="glyphicon glyphicon-minus"></i>',
                                    ],
                                ]),
                            'encode' => false,
                            'url' => 'javascript:;',
                            'options' => [
                                'class' => !Yii::$app->user->can(UserRole::ROLE_CHIEF) ? 'no-rules' : null,
                            ],
                            'visible' => YII_ENV_DEV,
                        ],
                    ],
                    'options' => [
                        'class' => 'nav-item_accordeon menu-none',
                    ],
                ],
            ];
        }
        return [];
    }
}
