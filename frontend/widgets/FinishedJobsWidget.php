<?php

namespace frontend\widgets;

use common\models\employee\Employee;
use common\modules\import\components\ImportCommandManager;
use common\modules\import\models\ImportJobData;
use common\modules\import\models\ImportJobDataRepository;
use common\modules\acquiring\commands\MonetaImportCommand;
use frontend\components\WebUser;
use yii\base\Widget;
use yii\web\User;

class FinishedJobsWidget extends Widget
{
    /**
     * @var ImportCommandManager
     */
    private $commandManager;

    /**
     * @var WebUser
     */
    private $user;

    /**
     * @param User $user
     * @param ImportCommandManager $commandManager
     * @param mixed[] $config
     */
    public function __construct(User $user, ImportCommandManager $commandManager, array $config = [])
    {
        $this->user = $user;
        $this->commandManager = $commandManager;

        parent::__construct($config);
    }

    /**
     * @inheritDoc
     */
    public function run(): void
    {
        if (!($this->user->identity instanceof Employee)) {
            return;
        }

        $jobDataRepository = new ImportJobDataRepository($this->user->identity->company);
        $commandType = $this->commandManager->getTypeByClass(MonetaImportCommand::class);
        $jobData = $jobDataRepository->getLastJobData($commandType, false, true);

        if ($jobData === null) {
            return;
        }

        $jobData->is_viewed = true;
        $jobData->save(true, ['is_viewed']);

        $this->getView()->registerJs($this->getJs($jobData));
    }

    /**
     * @param ImportJobData $jobData
     * @return string
     */
    private function getJs(ImportJobData $jobData): string
    {
        return <<<JS
            $(document).ready(function () {
                var jobId = parseInt('{$jobData->id}');
                var type = parseInt('{$jobData->type}');
                var count = parseInt('{$jobData->count}');

                window.showFinishedJobModal(jobId, type, count);
            });
JS;
    }
}
