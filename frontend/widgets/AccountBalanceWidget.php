<?php

namespace frontend\widgets;

use common\models\cash\CashBankFlows;
use common\models\cash\Cashbox;
use common\models\cash\CashFlowsBase;
use common\models\cash\Emoney;
use common\models\Company;
use common\models\company\CheckingAccountant;
use common\models\employee\Employee;
use Yii;
use yii\base\Widget;
use yii\db\Expression;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;
use yii\web\View;
use frontend\modules\cash\models\WidgetConfigAccountBalance as WidgetConfig;

/**
 * Class AccountBalanceWidget
 * @package frontend\widgets
 */
class AccountBalanceWidget extends Widget
{
    protected $_company;
    protected $_employee;

    /**
     * @return string
     */
    public function run()
    {
        Yii::$app->view->registerJs($this->getJs(), View::POS_READY);

        if ($company = $this->getCompany()) {

            /** @var WidgetConfig $config */
            $config = WidgetConfig::findOne([
                'company_id' => ($this->_company) ? $this->_company->id : null,
                'employee_id' => ($this->_employee) ? $this->_employee->id : null,
            ]) ?: new WidgetConfig();

            /** @var $accounts CheckingAccountant[] */
            $accounts = $company->getRubleAccounts()
                ->active()
                ->orderBy(['name' => SORT_ASC])
                ->indexBy('rs')
                ->all();

            /** @var $cashboxes Cashbox[] */
            $cashboxes = $company->getRubleCashboxes()
                ->orderBy(['name' => SORT_ASC])
                ->indexBy('id')
                ->all();

            /** @var $emoneys Emoney[] */
            $emoneys = $company->getRubleEmoneys()
                ->orderBy(['name' => SORT_ASC])
                ->indexBy('id')
                ->all();

            $rsList = array_keys($accounts);
            $cbList = array_keys($cashboxes);
            $emList = array_keys($emoneys);

            $statisticBank =
                ($rsList ? $company->getCashBankFlows()->select([
                    'rs',
                    'flow_type',
                    'balance' => 'SUM([[amount]])',
                ])->andWhere([
                    'rs' => $rsList,
                ])->groupBy([
                    'flow_type',
                    'rs',
                ])->asArray()->all() : []);

            $statisticCashbox =
                ($cbList ? $company->getCashOrderFlows()->select([
                    'cashbox_id',
                    'flow_type',
                    'balance' => 'SUM([[amount]])',
                ])->andWhere([
                    'cashbox_id' => $cbList,
                ])->groupBy([
                    'flow_type',
                    'id',
                ])->asArray()->all() : []);

            $statisticEmoney =
                ($emList ? $company->getCashEmoneyFlows()->select([
                    'emoney_id',
                    'flow_type',
                    'balance' => 'SUM([[amount]])',
                ])->andWhere([
                    'emoney_id' => $emList,
                ])->groupBy([
                    'flow_type',
                    'id',
                ])->asArray()->all() : []);

            $balance = [];
            $balanceTotal = 0;
            foreach ($accounts as $rs => $account)
            foreach ($statisticBank as $key => $value) {
                if ($rs == $value['rs']) {
                    $key = CashFlowsBase::WALLET_BANK . '_' . $value['rs'];
                    if (!isset($balance[$key])) {
                        $balance[$key] = [
                            'wallet_type' => CashFlowsBase::WALLET_BANK,
                            'rs' => $value['rs'],
                            'bik' => ArrayHelper::getValue($accounts, "{$value['rs']}.bik"),
                            'name' => ArrayHelper::getValue($accounts, "{$value['rs']}.name"),
                            'balance' => 0,
                            'url' => Url::to(['/cash/bank/index', 'rs' => $value['rs']]),
                        ];
                    }

                    if (in_array($value['rs'], $config->exceptRs)) {
                        $balance[$key]['checked'] = false;
                        if ($value['flow_type'] == CashBankFlows::FLOW_TYPE_INCOME) {
                            $balance[$key]['balance'] += $value['balance'];
                        } else {
                            $balance[$key]['balance'] -= $value['balance'];
                        }
                    } else {
                        $balance[$key]['checked'] = true;
                        if ($value['flow_type'] == CashBankFlows::FLOW_TYPE_INCOME) {
                            $balance[$key]['balance'] += $value['balance'];
                            $balanceTotal += $value['balance'];
                        } else {
                            $balance[$key]['balance'] -= $value['balance'];
                            $balanceTotal -= $value['balance'];
                        }
                    }
                }
            }

            // new theme only
            if ($this->_employee) {

                foreach ($cashboxes as $id => $cashbox)
                    foreach ($statisticCashbox as $key => $value) {
                        if ($id == $value['cashbox_id']) {
                            $key = CashFlowsBase::WALLET_CASHBOX . '_' . $value['cashbox_id'];
                            if (!isset($balance[$key])) {
                                $balance[$key] = [
                                    'wallet_type' => CashFlowsBase::WALLET_CASHBOX,
                                    'cashbox_id' => $value['cashbox_id'],
                                    'name' => ArrayHelper::getValue($cashboxes, "{$value['cashbox_id']}.name"),
                                    'balance' => 0,
                                    'url' => Url::to(['/cash/order/index', 'cashbox' => $value['cashbox_id']])
                                ];
                            }

                            if (in_array($value['cashbox_id'], $config->exceptCashboxes)) {
                                $balance[$key]['checked'] = false;
                                if ($value['flow_type'] == CashBankFlows::FLOW_TYPE_INCOME) {
                                    $balance[$key]['balance'] += $value['balance'];
                                } else {
                                    $balance[$key]['balance'] -= $value['balance'];
                                }
                            } else {
                                $balance[$key]['checked'] = true;
                                if ($value['flow_type'] == CashBankFlows::FLOW_TYPE_INCOME) {
                                    $balance[$key]['balance'] += $value['balance'];
                                    $balanceTotal += $value['balance'];
                                } else {
                                    $balance[$key]['balance'] -= $value['balance'];
                                    $balanceTotal -= $value['balance'];
                                }
                            }
                        }
                    }
                foreach ($emoneys as $id => $emoney)
                    foreach ($statisticEmoney as $key => $value) {
                        if ($id == $value['emoney_id']) {
                            $key = CashFlowsBase::WALLET_EMONEY . '_' . $value['emoney_id'];
                            if (!isset($balance[$key])) {
                                $balance[$key] = [
                                    'wallet_type' => CashFlowsBase::WALLET_EMONEY,
                                    'emoney_id' => $value['emoney_id'],
                                    'name' => ArrayHelper::getValue($emoneys, "{$value['emoney_id']}.name"),
                                    'balance' => 0,
                                    'url' => Url::to(['/cash/e-money/index', 'emoney' => $value['emoney_id']])
                                ];
                            }

                            if (in_array($value['emoney_id'], $config->exceptEmoneys)) {
                                $balance[$key]['checked'] = false;
                                if ($value['flow_type'] == CashBankFlows::FLOW_TYPE_INCOME) {
                                    $balance[$key]['balance'] += $value['balance'];
                                } else {
                                    $balance[$key]['balance'] -= $value['balance'];
                                }
                            } else {
                                $balance[$key]['checked'] = true;
                                if ($value['flow_type'] == CashBankFlows::FLOW_TYPE_INCOME) {
                                    $balance[$key]['balance'] += $value['balance'];
                                    $balanceTotal += $value['balance'];
                                } else {
                                    $balance[$key]['balance'] -= $value['balance'];
                                    $balanceTotal -= $value['balance'];
                                }
                            }
                        }
                    }
            }

            return $this->render('accountBalanceWidget', [
                'company' => $company,
                'accounts' => $accounts,
                'balance' => $balance,
                'balanceTotal' => $balanceTotal
            ]);
        }

        return null;
    }

    private function getJs()
    {
        return "
            $('#dropdownBankingBody .dropdown-banking-label').on('click', function(e) {
                e.stopPropagation();
            });
            $('#dropdownBankingBody .dropdown-banking-checkbox').on('change', function(e) {
                e.stopPropagation();
                const divTotalAmount = $('#dropdownBankingBody').find('.banking-dropdown-total');
                const divTotalAmountTop = $('#dropdownBanking').find('.banking-dropdown-total');
                let totalAmount = 0;
                let exceptData = [];
                $('#dropdownBankingBody').find('.dropdown-banking-checkbox').each(function(i, input) {
                    if ($(input).prop('checked')) {
                        totalAmount += Number($(input).val());
                        $(input).closest('li').find('a').removeClass('text-grey');
                    } else {
                        $(input).closest('li').find('a').addClass('text-grey');
                        exceptData.push($(input).attr('name'));
                    }
                });
                if (totalAmount < 0) {
                    divTotalAmount.addClass('text-red');
                } else {
                    divTotalAmount.removeClass('text-red');
                }
                divTotalAmount.html(number_format(totalAmount, 2, ',', ' ') + ' ₽');
                divTotalAmountTop.html(number_format(totalAmount, 2, ',', ' ') + ' ₽');
                $.post('/site/save-widget-config', {exceptData:exceptData}, function(data) {
                    console.log(data);
                });                
            });";
    }

    public function setCompany(Company $model) : void
    {
        $this->_company = $model;
    }

    public function getCompany() : Company
    {
        return $this->_company;
    }

    public function setEmployee(Employee $model) : void
    {
        $this->_employee = $model;
    }

    public function getEmployee() : Employee
    {
        return $this->_employee;
    }
}