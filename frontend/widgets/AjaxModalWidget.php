<?php

namespace frontend\widgets;

use Yii;
use yii\bootstrap4\Modal;
use yii\bootstrap4\Widget;
use yii\helpers\Html;

/**
 * Class AjaxModalWidget
 * @package frontend\widgets

 */
class AjaxModalWidget extends Widget
{
    /**
     * @var string
     */
    public $id = 'ajax-modal-box';
    /**
     * @var string
     */
    public $linkSelector = '.ajax-modal-btn';
    /**
     * @var
     */
    public $options;
    /**
     * @var
     */
    public $closeButton = [];

    /**
     *
     */
    public function init()
    {
        Modal::begin([
            'id' => $this->id,
            'headerOptions' => ['style' => 'display:none'],
            'options' => $this->options,
            'closeButton' => false,
            'toggleButton' => false,
        ]); ?>
            <h4 class="modal-title ajax-modal-title"></h4>
            <button type="button" class="modal-close close ajax-modal-hide" aria-label="Close">
                <svg class="svg-icon">
                    <use xlink:href="/images/svg-sprite/svgSprite.svg#close"></use>
                </svg>
            </button>
            <div class="ajax-modal-content"></div>

        <?php Modal::end();

        $js = <<<JS
        $(function(){
            $(document).on('click', '{$this->linkSelector}', function(e){
                e.preventDefault();
                var ajaxModal = $('#{$this->id}');
                let url = $(this).data('url') || $(this).attr('href');
                if (url) {
                    $('.ajax-modal-content', ajaxModal).load(url, {}, function() {
                        ajaxModal.trigger("ajax-modal-box-loaded");
                    });
                    let title = $(this).data('title') || $(this).attr('title');
                    if (title) {
                        $('.ajax-modal-title', ajaxModal).html(title).show();
                    } else {
                        $('.ajax-modal-title', ajaxModal).hide();
                    }
                    ajaxModal.modal('show');
                }
            });
            $(document).on('hidden.bs.modal', '#{$this->id}', function(e) {
                $('.ajax-modal-title', e.target).html('');
                $('.ajax-modal-content', e.target).html('');
                $('.modal-dialog', e.target).attr('class', 'modal-dialog');
            });
            $(document).on('click', '#{$this->id} *:not(.modal) .ajax-modal-hide', function() {
                $(this).closest('.modal').modal('hide');
            });
        });
JS;

        $view = $this->getView();
        $view->registerJs($js);
    }
}
