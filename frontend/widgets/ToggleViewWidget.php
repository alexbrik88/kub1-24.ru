<?php

namespace frontend\widgets;

use common\assets\ToggleViewAsset;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\base\Widget;

/**
 * Class ToggleViewWidget
 * @package frontend\widgets
 */
class ToggleViewWidget extends Widget
{
    public array $options = [];
    public bool $isActive = false;
    public ?string $activeBtnClass = null;
    public ?string $inactiveBtnClass = null;
    public ?string $hideTarget = null;
    public ?string $showTarget = null;
    public ?string $disableTarget = null;
    public ?string $enableTarget = null;
    public ?string $containerSelector = null;

    /**
     * @return string
     */
    public function run()
    {
        $this->registerJs();

        $options = $this->options;
        $tag = ArrayHelper::remove($options, 'tag', 'button');
        $label = ArrayHelper::remove($options, 'label', '</>');

        if ($this->isActive) {
            Html::addCssClass($options, 'active');
            if ($this->activeBtnClass) {
                Html::addCssClass($options, $this->activeBtnClass);
            }
            if ($this->inactiveBtnClass) {
                Html::removeCssClass($options, $this->inactiveBtnClass);
            }
        } else {
            Html::removeCssClass($options, 'active');
            if ($this->activeBtnClass) {
                Html::removeCssClass($options, $this->activeBtnClass);
            }
            if ($this->inactiveBtnClass) {
                Html::addCssClass($options, $this->inactiveBtnClass);
            }
        }

        Html::addCssClass($options, ['widget' => 'togle-view-button']);

        $options = ArrayHelper::merge($options, [
            'data-on-class' => $this->activeBtnClass ?: null,
            'data-off-class' => $this->inactiveBtnClass ?: null,
            'data-hide-targer' => $this->hideTarget ?: null,
            'data-show-target' => $this->showTarget ?: null,
            'data-disable-target' => $this->disableTarget ?: null,
            'data-enable-target' => $this->enableTarget ?: null,
            'data-container-selector' => $this->containerSelector ?: null,
        ]);

        echo Html::tag($tag, $label, $options);
    }

    public function registerJs()
    {
        ToggleViewAsset::register($this->view);
    }
}
