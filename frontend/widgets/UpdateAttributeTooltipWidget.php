<?php

namespace frontend\widgets;

use philippfrenzel\yii2tooltipster\yii2tooltipster;
use philippfrenzel\yii2tooltipster\yii2tooltipsterAsset;
use Yii;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\View;

class UpdateAttributeTooltipWidget extends Widget
{
    public $input;
    public $action = '';
    public $method = 'post';
    public $options = ['style' => 'width: 100px;'];
    public $formOptions = [];
    public $mainCssClass = 'update-attribute-tooltip';
    public $toggleButton = [
        'tag' => 'span',
        'label' => '<i class="fa fa-pencil" aria-hidden="true"></i>',
        'class' => 'cursor-pointer',
    ];

    /**
     * Initializes the widget.
     */
    public function init()
    {
        parent::init();

        if (!isset($this->options['id'])) {
            $this->options['id'] = $this->getId();
        }

        Html::addCssClass($this->options, $this->mainCssClass . '-content');
        Html::addCssClass($this->formOptions, $this->mainCssClass . '-form');
    }


    /**
     * Executes the widget.
     * @return string the result of widget execution to be outputted.
     */
    public function run()
    {
        echo $this->renderToggleButton();

        echo "\n" . Html::beginTag('div', ['class' => 'hidden']);
        echo "\n" . Html::beginTag('div', $this->options);
        echo "\n" . Html::beginForm($this->action, $this->method, $this->formOptions);
        echo "\n" . Html::tag('div', $this->input, [
            'style' => 'margin: 6px 0 10px;',
        ]);
        //echo "\n" . Html::button('<i class="fa fa-reply"></i>', [
        echo "\n" . Html::button('<i class="fa fa-times"></i>', [
            //'class' => 'btn btn-sm darkblue text-white cursor-pointer pull-right cancel',
            'class' => 'pull-right cancel',
            'style' => 'border: none; background: none; padding: 0; font-size: 24px;',
        ]);
        //echo "\n" . Html::submitButton('<i class="fa fa-floppy-o"></i>', [
        echo "\n" . Html::submitButton('<i class="fa fa-check-square-o"></i>', [
            //'class' => 'btn btn-sm darkblue text-white cursor-pointer',
            'style' => 'border: none; background: none; padding: 0; font-size: 24px;',
        ]);
        echo "\n" . Html::endForm();
        echo "\n" . Html::endTag('div'); // update-attribute-tooltip-content
        echo "\n" . Html::endTag('div'); // hidden

        $this->registerPlugin();
    }

    /**
     * Renders the toggle button.
     * @return string the rendering result
     */
    protected function renderToggleButton()
    {
        if (($toggleButton = $this->toggleButton) !== false) {
            $tag = ArrayHelper::remove($toggleButton, 'tag', 'button');
            $label = ArrayHelper::remove($toggleButton, 'label', 'Edit');
            if ($tag === 'button' && !isset($toggleButton['type'])) {
                $toggleButton['type'] = 'button';
            }
            Html::addCssClass($toggleButton, $this->mainCssClass);
            $toggleButton['data']['tooltip-content'] = '#' . $this->options['id'];

            return Html::tag($tag, $label, $toggleButton);
        } else {
            return null;
        }
    }

    /**
     * @return
     */
    protected function registerPlugin()
    {
        //get the displayed view and register the needed assets
        $view = $this->getView();
        yii2tooltipsterAsset::register($view);

        $js = [];

        $options = Json::encode([
            'theme' => ['tooltipster-kub'],
            'trigger' => 'custom',
            'triggerOpen' => ['click' => true],
            'contentAsHTML' => true,
        ]);
        $js[] = "$('.{$this->mainCssClass}').tooltipster($options);";

        $view->registerJs(implode("\n", $js), View::POS_READY);
    }
}
