<?php

namespace frontend\widgets;


use common\models\company\CompanyIndustry;
use Yii;
use yii\base\Widget;
use yii\helpers\ArrayHelper;

class MassChangeItemWidget extends Widget
{
    /**
     * @var string the value of the "id" attribute of the change industry modal
     */
    public $widgetId = 'mass_change_invoice_item';

    /**
     * @var string
     */
    public $modalTitle;

    /**
     * @var string
     */
    public $formAction;

    /**
     * @var string
     */
    public $formTitle;

    /**
     * @var string the value of the "name" attribute of the change industry input
     */
    public $inputName = 'item_id';

    /**
     * @var string selected items input "name"
     */
    public $itemsInputName = 'selected';

    /**
     * @var array
     */
    public $data;

    /**
     * @var array the options for rendering the toggle button tag.
     *
     * The following special options are supported:
     *
     * - tag: string, the tag name of the button. Defaults to 'button'.
     * - label: string, the label of the button. Defaults to 'Show'.
     *
     * The rest of the options will be rendered as the HTML attributes of the button tag.
     */
    public $toggleButton = false;

    /**
     * @inheritdoc
     */
    public function run()
    {
        $this->registerJs();

        return $this->render('changeItemsWidget', [
            'widgetId' => $this->widgetId,
            'modalTitle' => $this->modalTitle,
            'formAction' => $this->formAction,
            'formTitle' => $this->formTitle,
            'inputName' => $this->inputName,
            'inputValue' => null,
            'toggleButton' => $this->toggleButton,
            'data' => $this->data,
        ]);
    }

    /**
     * @inheritdoc
     */
    public function registerJs()
    {
        $script = <<<JS
            $(document).on("show.bs.modal", "#{$this->widgetId}_modal", function () {
                $(".joint-operation-checkbox:checked").each(function (i) {
                    let itemId = $(this).data("invoice-id");
                    $("<input>").attr({
                        type: "hidden",
                        id: "{$this->itemsInputName}"+itemId,
                        name: "{$this->itemsInputName}[]",
                        value: itemId,
                    }).appendTo("#{$this->widgetId}_form .additional_inputs");
                });
            });
            $(document).on("hidden.bs.modal", "#{$this->widgetId}_modal", function () {
                $('#{$this->widgetId}_form .additional_inputs').html('');
                $('#{$this->widgetId}_form select[name={$this->inputName}]').val('').trigger('change');
            });
        JS;

        $this->view->registerJs($script);
    }
}
