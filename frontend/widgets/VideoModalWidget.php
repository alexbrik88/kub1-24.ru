<?php

namespace frontend\widgets;

use yii\base\Widget;

class VideoModalWidget extends Widget
{
    public $modalOptiions = [];
    public $videoUrl;

    public function run()
    {
        return $this->render('videoModalWidget', [
            'modalOptiions' => $this->modalOptiions,
            'videoUrl' => $this->videoUrl,
        ]);
    }
}
