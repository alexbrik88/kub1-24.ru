<?php

namespace frontend\widgets;

use Yii;
use yii\bootstrap\Widget;
use common\models\employee\Employee;

/**
 * Class QueueNotificationsWidget
 * @package frontend\widgets
 */
class QueueNotificationsWidget extends Widget
{
    public $urlCheck = '/queue-notification/check-unviewed-jobs';
    public $urlMark = '/queue-notification/set-job-is-viewed';

    /**
     * @return string
     */
    public function run()
    {
        if (Yii::$app->user->isGuest)
            return "";

        /** @var Employee $employee */
        $employee = Yii::$app->user->identity;

        $this->view->registerJs($this->getJs());

        if ($employee->has_finished_unviewed_jobs) {

            $this->view->registerJs('
                $(document).ready(function() {
                   QueueNotifications.checkUnviewedJobs();
                });
            ');
        }

        return "";
    }

    private function getJs()
    {
        $urlCheck = $this->urlCheck;
        $urlMark = $this->urlMark;

        return <<<JS
        
            QueueNotifications = {
                interval: null,
                _start: function() {
                    this.interval = window.setInterval(function() { QueueNotifications._check() }, 5 * 1000);
                },
                _stop: function() {
                    if (this.interval) {
                        window.clearInterval(this.interval);
                    }
                    this.interval = null;
                },                
                _check: function() {
                    $.ajax('{$urlCheck}')
                        .done(function(data) {
                            if (data.result) {
                                QueueNotifications._stop();
                                if (data.message) {
                                    window.toastr.success(data.message, "", {
                                        "closeButton": true,
                                        "showDuration": 1000,
                                        "hideDuration": 1000,
                                        "timeOut": 10000,
                                        "extendedTimeOut": 1000,
                                        "escapeHtml": false
                                    });
                                    
                                    QueueNotifications._markAsRead(data.id);
                                }
                                
                                if (data.is_last) {
                                    QueueNotifications._stop();
                                }
                            }
                        })
                        .fail(function() {
                            QueueNotifications._stop();
                        });
                },
                _markAsRead: function(jobId)
                {
                    $.ajax({url:'{$urlMark}', data:{id:jobId}});
                },
                checkUnviewedJobs: function() {
                    QueueNotifications._stop();
                    QueueNotifications._start();
                }
            };
         
        JS;
    }
}
