<?php

namespace frontend\widgets;


use Yii;
use yii\bootstrap\Modal;
use yii\bootstrap\Widget;
use yii\helpers\Html;

/**
 * Class ConfirmModalWidget
 * @package frontend\widgets
 *
 * @example usage:
 * With target button :
 * <?= \frontend\widgets\ConfirmModalWidget::widget([
 *      'toggleButton' => [
 *          'label' => 'Удалить',
 *          'class' => 'btn darkblue',
 *      ],
 *      'confirmUrl' => Url::to(['delete', 'id' => $model->id,]),
 *      'confirmParams' => [],
 *      'message' => 'Вы уверены, что хотите удалить этот элемент?',
 * ]); ?>
 *
 * Without target button:
 * widget:
 * <?= \frontend\widgets\ConfirmModalWidget::widget([
 *      'options' => [
 *          'id' => 'delete-confirm',
 *      ],
 *      'toggleButton' => false,
 *      'confirmUrl' => Url::toRoute(['delete', 'type' => $ioType, 'id' => $model->id,]),
 *      'confirmParams' => [],
 *      'message' => 'Вы уверены, что хотите удалить исходящий счёт?',
 * ]);
 * button:
 * <button type="button" class="btn darkblue" data-toggle="modal" href="#delete-confirm">Удалить</button>
 *
 */
class ConfirmModalWidget extends Widget
{
    /**
     * @var string
     */
    public $message = 'Are you sure?';
    /**
     * @var string
     */
    public $confirmUrl = '#';
    /**
     * @var array
     */
    public $confirmParams = [];

    /**
     * @var
     */
    public $options;
    /**
     * @var
     */
    public $toggleButton;

    /**
     *
     */
    public function init()
    {
        Html::addCssClass($this->options, 'confirm-modal');
        Html::addCssClass($this->options, 'fade');

        Modal::begin([
            'options' => $this->options,
            'closeButton' => false,
            'toggleButton' => $this->toggleButton !== null ? $this->toggleButton : [
                'label' => 'Confirm'
            ],
        ]); ?>
        <div class="form-body">
            <div class="row"><?= $this->message; ?></div>
        </div>
        <div class="form-actions row">
            <div class="col-xs-6">
                <?= Html::a('<span class="ladda-label">ДА</span><span class="ladda-spinner"></span>', $this->confirmUrl, [
                    'class' => 'btn darkblue pull-right mt-ladda-btn ladda-button',
                    'data-style' => 'expand-right',
                    'data' => [
                        'method' => 'post',
                        'params' => array_merge($this->confirmParams, [
                            '_csrf' => Yii::$app->request->csrfToken,
                        ]),
                    ],
                ]); ?>
            </div>
            <div class="col-xs-6">
                <button type="button" class="btn darkblue" data-dismiss="modal">НЕТ</button>
            </div>
        </div>
        <?php Modal::end();
    }
}
