<?php

namespace frontend\widgets;

use yii\base\Widget;

/**
 * Class StatisticWidget
 * @package frontend\modules\documents\widgets
 */
class RangeButtonWidget extends Widget
{
    /**
     * @var $string
     */
    public $cssClass;

    /**
     * @var $string
     */
    public $pjaxSelector;

    /**
     * @var $int
     */
    public $zIndex = 1002;

    public $byMonths = false;

    /**
     * @return mixed
     */
    public function run()
    {

        return $this->render($this->getViewFile(), [
            'cssClass' => $this->cssClass,
            'pjaxSelector' => $this->pjaxSelector,
            'zIndex' => $this->zIndex,
        ]);
    }

    private function getViewFile()
    {
        return ($this->byMonths)
            ? 'rangeButtonWidgetMonths'
            : 'rangeButtonWidget';
    }
}
