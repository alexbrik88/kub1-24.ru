<?php
/**
 * Created by PhpStorm.
 * User: valik
 * Date: 27.01.2016
 * Time: 17:02
 */

namespace frontend\widgets;

use yii\base\Widget;
use Yii;

class TimePickerWidget extends Widget
{
    /**
     * @return mixed
     */
    public function run()
    {
        return $this->render('timePickerWidget');
    }
}