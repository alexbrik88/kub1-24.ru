<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 28.06.2018
 * Time: 13:10
 */

namespace frontend\widgets;

use common\models\service\PaymentType;
use common\models\service\ServiceInvoiceTariff;
use common\models\service\SubscribeTariff;
use common\components\date\DateHelper;
use frontend\models\PaymentInvoiceForm;
use yii\base\Widget;
use yii\helpers\Html;
use Yii;

/**
 * Class InvoicePaymentModal
 * @package frontend\widgets
 */
class InvoicePaymentModal extends Widget
{
    /**
     * @return string
     */
    public function run()
    {
        if (!Yii::$app->user->isGuest) {
            $controller = Yii::$app->controller->id;
            /* @var $company \common\models\Company */
            $company = Yii::$app->user->identity->company;


            if ($company->getInvoiceLeft() == 0 &&
                (date('Y-m-d', $company->show_invoice_payment_popup_date) !== date('Y-m-d') || $controller == 'invoice')
            ) {
                if (!$company->subscribe_payment_key) {
                    $company->subscribe_payment_key = uniqid();
                }
                $company->show_invoice_payment_popup_date = time();
                $company->save(false, [
                    'show_invoice_payment_popup_date',
                    'subscribe_payment_key',
                ]);

                $this->view->registerJs('$(document).ready(function () {
                    $("#modal-invoice-payment").modal();
                });');

                $model = new PaymentInvoiceForm($company);
                $oneInvoiceTariff = ServiceInvoiceTariff::findOne(ServiceInvoiceTariff::ONE_INVOICE);
                $threeInvoiceTariff = ServiceInvoiceTariff::findOne(ServiceInvoiceTariff::THREE_INVOICES);
                $subscribeTariff = SubscribeTariff::findOne(SubscribeTariff::TARIFF_1);

                $data = [
                    [
                        'count' => $oneInvoiceTariff->invoice_count,
                        'price' => $oneInvoiceTariff->total_amount,
                        'action' => ['/invoice-payment/online-payment'],
                        'method' => 'post',
                        'input' => Html::activeHiddenInput($model, 'tariffId', ['value' => $oneInvoiceTariff->id]),
                    ],
                    [
                        'count' => $threeInvoiceTariff->invoice_count,
                        'price' => $threeInvoiceTariff->total_amount,
                        'action' => ['/invoice-payment/online-payment'],
                        'method' => 'post',
                        'input' => Html::activeHiddenInput($model, 'tariffId', ['value' => $threeInvoiceTariff->id]),
                    ],
                    [
                        'count' => $subscribeTariff->tariff_limit,
                        'price' => $subscribeTariff->price,
                        'action' => [
                            '/subscribe/default/create',
                            'id' => $company->id,
                            'key' => $company->subscribe_payment_key,
                            'tariff' => $subscribeTariff->id,
                            'type' => PaymentType::TYPE_ONLINE
                        ],
                        'method' => 'get',
                        'input' => null,
                    ],
                ];

                return $this->render('invoicePaymentModal', [
                    'data' => $data,
                ]);
            }
        }

        return null;
    }
}
