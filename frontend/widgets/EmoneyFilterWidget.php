<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 05.09.2018
 * Time: 16:27
 */

namespace frontend\widgets;


use common\components\helpers\Html;
use yii\bootstrap\Dropdown;
use yii\helpers\Url;
use yii\base\Widget;
use Yii;

/**
 * Class EmoneyFilterWidget
 * @package frontend\widgets
 */
class EmoneyFilterWidget extends Widget
{
    /**
     * @var
     */
    public $pageTitle;
    /**
     * @var
     */
    public $emoney;
    /**
     * @var \common\models\Company
     */
    public $company;

    protected $_list = false;
    protected $_items = false;
    /**
     * @return null|string
     * @throws \Exception
     */
    public function run()
    {
        Yii::$app->view->registerJs('
            $(document).on("submit", "#emoney-form", function () {
                $this = $(this);
                $modal = $this.closest(".modal");
                $(this).append("<input type=\"hidden\" name=\"emoney\" value=\"' . $this->emoney . '\">");
                $action = $this.data("isnewrecord") == 1 ? "/cash/e-money/create-emoney" : "/cash/e-money/update-emoney?id=" + $this.data("modelid");
                $.post($action, $(this).serialize(), function (data) {
                    if (data.result == true) {
                        $modal.modal("hide");
                        $("ul#user-bank-dropdown").replaceWith(data.html);
                        if (data.label) {
                            $("span.emoney-label").text(data.label + " ");
                        }
                        Ladda.stopAll();
                    } else {
                        $this.html($(data.html).find("form").html());
                        $("#emoney-form input:checkbox:not(.md-check)").uniform();
                    }
                });

                return false;
            });
        ');

        return $this->getContent();
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getList()
    {
        if ($this->_list === false) {
            $this->_list = $this->company->getEmoneys()
                ->select(['name'])
                ->orderBy([
                    'is_closed' => SORT_ASC,
                    'is_main' => SORT_DESC,
                    'name' => SORT_ASC,
                ])->indexBy('id')->column();
        }

        return $this->_list;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getItems()
    {
        $list = $this->getList();

        $items = [];
        if ($list) {
            if (count($list) > 1) {
                $list = $list + ['all' => 'Все E-money'];
            }
            foreach ($list as $key => $name) {
                $items[] = [
                    'label' => '<span class="item-label">' . $name . '</span>' .
                        ($key !== 'all' ? '<span class="glyphicon glyphicon-pencil update-emoney ajax-modal-btn"
                        title="Обновить" aria-label="Обновить"
                        data-url="' . Url::to(['/cash/e-money/update-emoney', 'id' => $key]) . '"></span>' : null),
                    'url' => ['index', 'emoney' => $key],
                    'linkOptions' => [
                        'class' => $this->emoney == $key ? 'active' : '',
                    ],
                ];
            }
        }

        $items[] = [
            'label' => '[ + ДОБАВИТЬ E-MONEY ]',
            'url' => Url::to(['/cash/e-money/create-emoney']),
            'linkOptions' => [
                'data-title' => 'Добавить E-money',
                'class' => 'ajax-modal-btn btn yellow',
                'style' => 'text-align:left'
            ],
        ];

        return $items;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getContent()
    {
        $emoneyList = $this->getList();
        $emoneyItems = $this->getItems();
        $currentName = isset($emoneyList[$this->emoney]) ? $emoneyList[$this->emoney] : (count($emoneyItems) > 1 ? 'Все E-money' : $this->pageTitle);

        if ($emoneyItems) {
            if (count($emoneyItems) > 1) {
                $list = '<div style="display: inline-block;"><div class="dropdown" style="border-bottom: 1px dashed #666;">' .
                    Html::a('<span class="emoney-label">' . Html::encode($currentName) . ' </span><b class="caret"></b>', '#', [
                        'data-toggle' => 'dropdown',
                        'class' => 'dropdown-toggle',
                        'style' => 'text-decoration: none;text-transform: uppercase;font-weight: bold;font-size: 19px;line-height: 20px;color: #555;',
                    ]) .
                    Dropdown::widget([
                        'id' => 'user-bank-dropdown',
                        'encodeLabels' => false,
                        'items' => $emoneyItems,
                    ]) .
                    '</div></div>';

                return $list;
            }
        }

        return Html::encode($this->pageTitle);
    }
}
