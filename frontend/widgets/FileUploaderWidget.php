<?php
/**
 * Created by PhpStorm.
 * User: konstantin
 * Date: 10.6.15
 * Time: 17.32
 */

namespace frontend\widgets;

use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\InputWidget;

/**
 * Class FileUploaderWidget
 * @package frontend\modules\documents\widgets
 */
class FileUploaderWidget extends InputWidget
{

    /**
     * Html options of widget wrapper.
     * @var array
     */
    public $wrapperOptions = [];

    /**
     * @var string the template that is used to arrange the label, the input field, the error message and the hint text.
     * The following tokens will be replaced: `{beginWrapper}`, `{existedFile}`, `{input}` and `{endWrapper}`.
     * @todo: {error} and {hint} tokens.
     */
    public $template = "{beginWrapper}\n{existedFile}\n{input}\n{endWrapper}";

    /**
     * Parts of template
     * @var array
     */
    public $parts = [];

    /**
     * Existed file `button`. If null button is hidden.
     * @var string
     */
    public $existedFileName;
    /**
     * Url to view existed file. If null - <span> will be instead of <a>
     * @var string
     */
    public $existedFileUrl;

    /**
     * Html options for existed file button.
     * @var array
     */
    public $existedFileOptions = [];
    /**
     * Html options for input wrapper.
     * Note: options for input itself are in [[options]] field.
     * Note: you should set `default-title` to define Html input button text.
     * @var array
     */
    public $inputWrapperOptions = [];

    /**
     * @throws \yii\base\InvalidConfigException
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        $this
            ->setBeginWrapper()
            ->setExistedFile()
            ->setInput()
            ->setEndWrapper();

        echo strtr($this->template, $this->parts); // replaces {keys} to content.
    }

    /**
     * Sets begin of whole wrapper.
     *
     * @return FileUploaderWidget
     */
    protected function setBeginWrapper()
    {
        if (!isset($this->parts['{beginWrapper}'])) {
            Html::addCssClass($this->wrapperOptions, 'btn-group');

            $this->parts['{beginWrapper}'] = Html::beginTag('div', $this->wrapperOptions);
        }

        return $this;
    }

    /**
     * Sets existed file button.
     * If file url defined - opens it in new tab.
     * Otherwise renders <span> with filename.
     *
     * @return FileUploaderWidget
     */
    protected function setExistedFile()
    {
        if (empty($this->existedFileName)) {
            $this->parts['{existedFile}'] = '';

            return $this;
        }

        if (!isset($this->parts['{existedFile}']) && !empty($this->existedFileName)) {
            $tagName = 'span';
            if (!empty($this->existedFileUrl)) {
                $tagName = 'a';
                $this->existedFileOptions['target'] = '_blank';
                $this->existedFileOptions['href'] = $this->existedFileUrl;
            }

            Html::addCssClass($this->existedFileOptions, 'fileUploadExisted');
            Html::addCssClass($this->existedFileOptions, 'btn');
            Html::addCssClass($this->existedFileOptions, 'btn-primary');

            $this->parts['{existedFile}'] = Html::tag(
                $tagName,
                $this->existedFileName,
                $this->existedFileOptions
            );
        }

        return $this;
    }

    /**
     * @return FileUploaderWidget
     */
    protected function setInput()
    {
        if (!isset($this->parts['{input}'])) {
            Html::addCssClass($this->options, 'upload');

            if ($this->hasModel()) {
                $input = Html::activeFileInput($this->model, $this->attribute, $this->options);
            } else {
                $input = Html::fileInput($this->name, $this->value, $this->options);
            }

            $defaultTitle = ArrayHelper::remove($this->options, 'default-title', 'Загрузить документ');

            $input = join('', [
                Html::tag('span', '', [
                    'class' => 'upload-button glyphicon glyphicon-download-alt padding-right-5',
                    'aria-hidden' => 'true',
                ]),
                Html::tag('span', $defaultTitle, [
                    'class' => 'title',
                    'data' => [
                        'default-title' => $defaultTitle,
                    ],
                ]),
                $input,
            ]);

            Html::addCssClass($this->inputWrapperOptions, 'fileUpload');
            Html::addCssClass($this->inputWrapperOptions, 'btn');
            Html::addCssClass($this->inputWrapperOptions, 'btn-primary');
            $this->inputWrapperOptions['for'] = $this->id;

            $this->parts['{input}'] = Html::tag('label', $input, $this->inputWrapperOptions);
        }

        return $this;
    }

    /**
     * Sets end of wrapper
     *
     * @return FileUploaderWidget
     */
    protected function setEndWrapper()
    {
        if (!isset($this->parts['{endWrapper}'])) {
            $this->parts['{endWrapper}'] = Html::endTag('div');
        }

        return $this;
    }


}