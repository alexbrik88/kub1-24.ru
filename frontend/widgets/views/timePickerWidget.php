<?php

use common\models\TimeZone;

if (Yii::$app->user->identity === null) {
    $timeZone = TimeZone::TEXT_DEFAULT_TIME_ZONE;
} else {
    $timeZone = TimeZone::findOne(Yii::$app->user->identity->time_zone_id)->time_zone;
}
$this->registerJs(<<<JS
$(function () {
    function startTime() {
        var a = moment();
        var b = a.tz('$timeZone').format('HH:mm');
        $(".page-header-date b").text(b);
        var t = setTimeout(function () {
            startTime()
        }, 1000);
    }
    startTime();
});
JS
);
?>

<b></b>
