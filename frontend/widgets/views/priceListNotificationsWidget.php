<?php
use yii\bootstrap\Modal;
use yii\helpers\Html;

/** @var $notification \common\models\product\PriceListNotification */

if ($notification->contractor) {
    $viewer = Html::a($notification->contractor->getShortName(),
            ['/contractor/view', 'type' => 2, 'id' => $notification->contractor_id]) . ' ' . ' ' . $notification->contractor->getRealContactName();
    $viewerPhone = $notification->contractor->getRealContactPhone();
    if ($viewerPhone)
        $viewerPhone .= '<br/>Позвоните клиенту, возможно у него есть вопросы.';
    $viewerEmail = $notification->contractor->getRealContactEmail()
        . '<br/>Напишите клиенту, возможно у него есть вопросы.';
} else {
    $viewer = 'клиент';
    $viewerPhone = '';
    $viewerEmail = $notification->contractor_email
        . '<br/>Напишите клиенту, возможно у него есть вопросы.';
}
?>

<?php
Modal::begin([
    'options' => [
        'class' => 'confirm-modal modal-price-list-notification',
        'data-notification-id' => $notification->id,
    ],
    'closeButton' => false,
    'toggleButton' => false
]); ?>

    <div class="form-body">
        <div class="row">
            <div class="col-xs-12">
                Ваш прайс-лист
                <b><?= Html::a($notification->priceList->name, ['/price-list/view-card', 'id' => $notification->priceList->id], ['target' => '_blank']) ?></b>
                сейчас смотрит
                <?= $viewer ?>. <br/>
                <?= $viewerPhone ? ('Его телефон: ' . $viewerPhone) : ('Его e-mail: ' . $viewerEmail) ?>
            </div>
        </div>
    </div>
    <div class="form-actions row">
        <div class="col-xs-12 text-center">
            <button type="button" class="btn darkblue" data-dismiss="modal">ОК</button>
        </div>
    </div>

<?php Modal::end();