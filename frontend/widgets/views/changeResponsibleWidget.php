<?php

use kartik\select2\Select2;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Modal;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $modalId string */
/* @var $modalTitle string */
/* @var $formAction string */
/* @var $formTitle string */
/* @var $inputName string */
/* @var $inputValue string */
/* @var $toggleButton array */
/* @var $data array */

?>

<?php Modal::begin([
    'id' => $modalId,
    'header' => $modalTitle ?: '<h1>Изменить ответственного</h1>',
    'toggleButton' => $toggleButton,
]) ?>

    <?php $form = ActiveForm::begin([
        'id' => $modalId.'_form',
        'action' => $formAction,
    ]); ?>

    <div class="hidden additional_inputs"></div>

    <?php if ($formTitle) : ?>
        <div class="form-group">
            <?= $formTitle ?>
        </div>
    <?php endif ?>

    <div class="row form-horizontal">
        <label class="col-md-3 control-label width-label font-bold">
            Ответственный
        </label>
        <div class="col-md-9">
            <?= Select2::widget([
                'name' => $inputName,
                'value' => $inputValue,
                'data' => $data,
                'options' => [
                    'multiple' => false,
                    'placeholder' => empty($inputValue) ? '' : null,
                ],
            ]); ?>
        </div>
    </div>

    <div style="margin-top: 35px;">
        <?= Html::submitButton('Сохранить', [
            'class' => 'btn darkblue text-white ladda-button',
            'data-style' => 'expand-right',
        ]) ?>
        <?= Html::button('Отменить', [
            'class' => 'btn darkblue text-white pull-right',
            'data-dismiss' => 'modal',
        ]) ?>
    </div>

    <?php ActiveForm::end(); ?>

<?php Modal::end() ?>
