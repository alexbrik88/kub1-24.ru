<?php

/* @var $label string
 * @var $items \common\models\news\News[]
 * @var $company \common\models\Company
 * @var $buttonClass string|null
 */

use Carbon\Carbon;
use common\components\date\DateHelper;
use common\components\helpers\Html;
use common\models\news\NewsReaction;
use yii\helpers\Url;

?>

<?= Html::button($label, [
    'class' => "news-button button-regular button-hover-transparent toggle-news-panel {$buttonClass}",
    'style' => 'padding: 5px;height: 30px;margin-top: 8px;margin-right: 15px;',
    'data-target' => 'news',
    'data-toggle' => 'toggleVisible',
]) ?>

<div class="page-shading-panel hidden"></div>
<div class="news-panel">
    <div class="main-block news-wrap">
        <p class="bold" style="font-size:30px;margin-bottom: 35px;">
            <?= $label ?>
        </p>

        <div class="news-block">
            <?php foreach ($items as $item): ?>
                <div class="news-block-wrapper" id="<?= $item->id ?>">
                    <?php if ($item->date): ?>
                        <p class="news-date">
                            <?= Carbon::createFromFormat(DateHelper::FORMAT_DATE, $item->date)->locale('ru')->isoFormat('D MMMM Y г.') ?>
                        </p>
                    <?php endif; ?>
                    <p class="news-header">
                        <?= $item->name ?>
                    </p>
                    <div class="news-body">
                        <?= str_replace(['&lt;', '&gt;'], ['<', '>'], $item->text) ?>
                        <div class="news-buttons-wrapper pull-left">
                            <div class="news-button-block" style="margin-right: 10px;">
                                <span class="glyphicon glyphicon-thumbs-up news-buttons toggle-news-reaction <?= $item->hasCompanyReaction($company, NewsReaction::LIKE_REACTION) ? 'active' : null ?>"
                                      data-url="<?= Url::to(['/site/toggle-news-reaction', 'newsId' => $item->id, 'reaction' => NewsReaction::LIKE_REACTION]) ?>"></span>
                                <span class="news-buttons-text likes-count">
                                    <?= $item->getLikesCount() ?>
                                </span>
                            </div>
                            <div class="news-button-block">
                                <span class="glyphicon glyphicon-thumbs-down news-buttons toggle-news-reaction <?= $item->hasCompanyReaction($company, NewsReaction::DISLIKE_REACTION) ? 'active' : null ?>"
                                      data-url="<?= Url::to(['/site/toggle-news-reaction', 'newsId' => $item->id, 'reaction' => NewsReaction::DISLIKE_REACTION]) ?>"></span>
                                <span class="news-buttons-text dislikes-count">
                                    <?= $item->getDislikesCount() ?>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="line"></div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <span class="side-panel-close" title="Закрыть">
        <span class="side-panel-close-inner"></span>
    </span>
</div>

<?php $this->registerJs('
    $(document).on("click", ".toggle-news-panel", function (e) {
        e.preventDefault();

        $(".toggle-news-panel").addClass("active");
        $(".page-shading-panel").removeClass("hidden");
        $(".news-panel").show("fast");
        $("#visible-right-menu-message-panel").show();
        $("html").attr("style", "overflow: hidden;");
        $(".news-panel .main-block").scrollTop(0);
    });

    $(document).on("click", ".news-panel .side-panel-close, .news-panel .side-panel-close-button", function () {
        $(".toggle-news-panel").removeClass("active");
        $(".news-panel").hide("fast", function() {
            $(".page-shading-panel").addClass("hidden");
        });
        $("#visible-right-menu-message-panel").hide();
        $("html").removeAttr("style");
    });

    $(document).ready(function() {
        $(".news-wrap p:has(img, iframe)").addClass("text-center");
        $(document).on("click", ".toggle-news-reaction", function() {
            let buttonsWrapper = $(this).closest(".news-buttons-wrapper");
            $.post($(this).data("url"), {}, function(data) {
                if (data.result) {
                    buttonsWrapper.find(".likes-count").text(data.likesCount);
                    buttonsWrapper.find(".dislikes-count").text(data.dislikesCount);
                    buttonsWrapper.find(".toggle-news-reaction").toggleClass("active");
                }
            });
        });
    });
    
    $(document).on("click", ".news-button", function() {
        $.post("/site/view-news");
    });
    
    $(document).on("click", ".news-button.active", function() {
        $(this).removeClass("active");
    });
    
    $(document).on("click", ".news-header", function() {
        let newsBlockWrapper = $(this).closest(".news-block-wrapper");
        let activeNewsBlockWrapper = $(this).closest(".news-block").find(".news-block-wrapper.active");
    
        newsBlockWrapper.toggleClass("active");
        if (newsBlockWrapper.attr("id") !== activeNewsBlockWrapper.attr("id")) {
            activeNewsBlockWrapper.removeClass("active");
        }
    });
'); ?>