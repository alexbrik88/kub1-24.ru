<?php

use common\models\bank\BankingParams;
use common\models\cash\CashBankStatementUpload;
use common\models\company\CheckingAccountant;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\modules\cash\modules\banking\widgets\BankingModalWidget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $company common\models\Company */
/* @var $accounts common\models\company\CheckingAccountant[] */
/* @var $balance array */
/* @var $balanceTotal integer */

$p = Banking::currentRouteEncode();
$banking = [];
$integratedBanking = [];
foreach ($balance as $data) {
    if ($class = Banking::classByBik($data['bik'])) {
        $accountId = ArrayHelper::getValue($accounts, "{$data['rs']}.id");
        $params = [
            'class' => $class,
            'alias' => $class::ALIAS,
            'accountId' => $accountId,
        ];
        $banking[] = $params;
        $hasUpload = CashBankStatementUpload::find()->where([
            'rs' => $data['rs'],
            'company_id' => $company->id,
            'source' => [CashBankStatementUpload::SOURCE_BANK_AUTO, CashBankStatementUpload::SOURCE_BANK],
        ])->orderBy(['created_at' => SORT_DESC])->exists();
        if ($hasUpload || BankingParams::getValue($company, $class::ALIAS, 'access_token') !== null) {
            $integratedBanking[] = $params;
        }
    }
}
if ($integratedBanking) {
    $data = reset($integratedBanking);
    $url = [
        "/cash/banking/{$data['alias']}/default/index",
        'account_id' => $data['accountId'],
        'start_load_urgently' => true,
        'p' => $p,
    ];
} elseif ($banking) {
    $data = reset($banking);
    $url = [
        "/cash/banking/{$data['alias']}/default/index",
        'account_id' => $data['accountId'],
        'p' => $p,
    ];
} else {
    $url = [
        "/cash/banking/default/index",
        'p' => $p,
    ];
}
?>

<style type="text/css">
.banking-balance-container {
    position: relative;
    display: inline-block;
}
.banking-balance-container .refresh-banking-statements {
    margin: 6px 5px 0 0;
    padding: 0;
}
.banking-balance-container .banking-balance-view {
    display: flex;
    padding-top: 4px;
    height: 42px;
    line-height: 1;
    color: #fff;
}
.banking-balance-container .banking-balance-view .current-time b {
    margin: 0;
    font-size: 14px;
    vertical-align: baseline;
}
.banking-balance-container .dropdown .dropdown-menu a {
    color: #555;
    display: flex; font-size: 12px;
    justify-content: space-between;
}
.banking-balance-container .dropdown .dropdown-toggle {
    cursor: pointer;
}
</style>

<div class="banking-balance-container">
    <div class="banking-balance-view">
        <div>
            <?= Html::a('<i class="icon-refresh" style="font-size:20px;"></i>', $url, [
                'class' => 'refresh-banking-statements banking-module-open-link',
            ]) ?>
        </div>
        <div class="dropdown dropdown-user">
            <div data-close-others="true" data-toggle="dropdown" class="dropdown-toggle">
                <div style="font-size: 14px; text-align: right;">
                    <?= Yii::$app->formatter->asDecimal(bcdiv($balanceTotal, 100, 2), 2) ?> ₽
                </div>
                <div style="line-height: 20px; font-weight: 300; text-align: right;">
                    на рублевых счетах
                </div>
            </div>
            <ul class="dropdown-menu dropdown-menu-default">
                <?php foreach ($balance as $data) : ?>
                    <li>
                        <a href="<?= Url::to(['/cash/bank/index', 'rs' => $data['rs']]) ?>">
                            <div style="padding-right: 10px;">
                                <?= $data['name'] ?>
                            </div>
                            <div style="text-align: right;">
                                <?= Yii::$app->formatter->asDecimal(bcdiv($data['balance'], 100, 2), 2) ?>
                            </div>
                        </a>
                    </li>
                <?php endforeach ?>
            </ul>
        </div>
    </div>
</div>

<?= BankingModalWidget::widget() ?>