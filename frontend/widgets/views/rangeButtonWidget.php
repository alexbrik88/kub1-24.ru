<?php
/**
 * @var $cssClass string
 */
use frontend\components\StatisticPeriod;
use common\models\employee\Employee;
use common\models\TimeZone;

?>
<div
    class="btn default pull-right btn-calendar auto-width p-t-7 p-b-7 portlet mrg_bottom <?= $cssClass; ?>"
    <?= $pjaxSelector ? "data-pjax=\"{$pjaxSelector}\"" : null; ?>
    id="reportrange">

    <?php
    $this->registerJs("
    $('#reportrange').daterangepicker({
        format: 'DD.MM.YYYY',
        ranges: {" . StatisticPeriod::getRangesJs() . "},
        startDate: '" . StatisticPeriod::getStartEndDateJs('from') . "',
        endDate: '" . StatisticPeriod::getStartEndDateJs('to') . "',
        locale: {
            daysOfWeek: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
            monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'],
            firstDay: 1
        }
    });
    "); ?>

    <?= StatisticPeriod::getSessionName(); ?>
    <b class="fa fa-angle-down"></b>
</div>


