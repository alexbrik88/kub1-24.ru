<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;

/** @var $this yii\web\View */
/** @var $modalId string */
/** @var $modalOptions array */
/** @var $text string */

$options = array_merge([
    'closeButton' => false,
    'toggleButton' => false,
], $modalOptions, [
    'id' => $modalId,
]);

?>

<?php Modal::begin($options); ?>

    <div class="text-center" style="font-size: 16px;">
        <?= $text ?>
    </div>
    <div class="text-center" style="margin-top: 20px; font-size: 16px;">
        Перейти к оплате?
    </div>
    <div class="form-actions row">
        <div class="col-xs-6">
            <?= Html::a('ДА', ['/subscribe/default/index'], [
                'class' => 'btn darkblue text-white pull-right',
                'style' => 'width: 80px;',
            ]) ?>
        </div>
        <div class="col-xs-6">
            <?= Html::button('НЕТ', [
                'class' => 'btn darkblue text-white',
                'style' => 'width: 80px;',
                'data-dismiss' => 'modal',
            ]) ?>
        </div>
    </div>

<?php Modal::end();
