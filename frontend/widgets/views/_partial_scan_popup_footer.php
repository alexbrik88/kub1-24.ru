<?php
use yii\widgets\MaskedInput;
use yii\helpers\Html;
?>
<span class="enter-you-phone text-center">
    Документы присылайте сюда <br /> <a style="color:white; font-size:14px" href="mail:support@kub-24.ru">support@kub-24.ru</a> <br /><br />
</span>
<div class="send-phone-block text-center">
    <span class="enter-you-phone">
        Не забудьте
    </span>
    <span style="color: white;display: block;font-size: 14px;">
        указать ваш ИНН и название вашей компании
    </span>
</div>
