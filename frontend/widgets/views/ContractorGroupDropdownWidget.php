<?php
use frontend\rbac\UserRole;
use yii\helpers\Html;

/* @var \yii\web\View $this */
/* @var string $inputId */
/* @var integer $productionType */
?>

<?php if (Yii::$app->user->can(UserRole::ROLE_PRODUCT_ADMIN)) : ?>

<div class="add-contracter-block" style="display: none;">
    <span class="add-new-contracter-group-form"
        style="display: inline-block; position: relative; width: 100%; padding: 15px 52px 15px 15px; border-top: 1px solid #aaa;">
        <button class="btn yellow new-contracter-group-submit" style="position: absolute; right: 10px; top: 27px; padding: 0 7px;border: 0px">
            <svg class="add-button-icon svg-icon" style="font-size: 22px;" viewBox="0 0 16 16">
                 <path d="M8 16A8 8 0 1 1 8 0a8 8 0 0 1 0 16zm0-1.486c3.866 0 6.505-3.295 6.505-6.514 0-3.219-2.639-6.534-6.505-6.534s-6.534 3.2-6.534 6.524c0 3.323 2.668 6.524 6.534 6.524zm.667-9.847v2.666h2.666v1.334H8.667v2.666H7.333V8.667H4.667V7.333h2.666V4.667h1.334z" fill-rule="evenodd"></path>
            </svg>
        </button>
        <?= Html::textInput('title', null, [
            'class' => 'form-control',
            'maxlength' => 45,
            'placeholder' => 'Добавить новую группу',
            'style' => 'display: inline-block; width: 100%;',
        ]); ?>
        <?= Html::hiddenInput('type', $type); ?>
    </span>
</div>
<script type="text/javascript">
    $('#<?= $inputId ?>').on('select2:open', function (evt) {
        if (!$('#select2-<?= $inputId ?>-results').parent().children('.add-new-contracter-group-form').length) {
            var $newContracterForm = $('select#<?= $inputId; ?>').siblings('.add-contracter-block').find('.add-new-contracter-group-form').clone();
            $newContracterForm.find('.new-contracter-group-submit').attr('id', 'new-contracter-group-submit-<?= $inputId ?>');
            $newContracterForm.clone().insertAfter('#select2-<?= $inputId ?>-results');
        }
    });
    $(document).on('click', '#new-contracter-group-submit-<?= $inputId ?>', function() {
        var input = $(this).parent().children('input');
        $.post('/contractor/add-group', input.serialize(), function(data) {
            if (data.itemId && data.itemName) {
                input.find('[name!=userType]').val('');
                var newOption = new Option(data.itemName, data.itemId, false, true);
                newOption.dataset.editable = 1;
                newOption.dataset.deletable = 1;
                $("#<?= $inputId ?>").select2("close");
                $("#<?= $inputId ?>").append(newOption).trigger('change');
            }
        });
    });
</script>

        <div id="<?= $inputId ?>-del-modal" class="confirm-modal fade modal" role="dialog" tabindex="-1" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <h4 class="modal-title text-center mb-4">
                        Вы уверены, что хотите удалить группу
                    </h4>
                    <div class="text-center">
                        <?= Html::a('<span class="ladda-label">Да</span><span class="ladda-spinner"></span>', 'javascript:;', [
                            'class' => 'button-clr button-regular button-hover-transparent button-width-medium mr-2 js-item-delete ladda-button',
                            'data-style' => 'expand-right',
                        ]) ?>
                        <?= Html::button('Нет', [
                            'class' => 'button-clr button-regular button-hover-transparent button-width-medium ml-1',
                            'data-dismiss' => 'modal',
                        ]) ?>
                    </div>
                </div>
            </div>
        </div>

    <script>
        $('.js-item-delete, .js-item-delete-cancel').on('click', function() {
            $(this).closest('.modal').modal('hide');
        });
    </script>

<?php endif ?>

