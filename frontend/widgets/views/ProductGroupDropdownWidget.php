<?php

use frontend\rbac\UserRole;
use yii\helpers\Html;

/* @var \yii\web\View $this */
/* @var string $inputId */
/* @var integer $productionType */
?>

<?php if (Yii::$app->user->can(UserRole::ROLE_PRODUCT_ADMIN)) : ?>

<div class="add-product-block" style="display: none;">
    <span class="add-new-product-group-form"
        style="display: inline-block; position: relative; width: 100%; padding: 5px 52px 0 5px; border-top: 1px solid #aaa;">
        <button class="btn yellow new-product-group-submit" style="position: absolute; right: 12px; top: 5px; padding: 0 7px; font-size: 24px;">
            <span class="fa icon fa-plus-circle"></span>
        </button>
        <?= Html::textInput('title', null, [
            'class' => 'form-control',
            'maxlength' => 45,
            'placeholder' => 'Добавить новую группу товара',
            'style' => 'display: inline-block; width: 100%;',
        ]); ?>
        <?= Html::hiddenInput('productionType', $productionType); ?>
    </span>
</div>
<script type="text/javascript">
    $('#<?= $inputId ?>').on('select2:open', function (evt) {
        if (!$('#select2-<?= $inputId ?>-results').parent().children('.add-new-product-group-form').length) {
            var $newProductForm = $('select#<?= $inputId; ?>').siblings('.add-product-block').find('.add-new-product-group-form').clone();
            $newProductForm.find('.new-product-group-submit').attr('id', 'new-product-group-submit-<?= $inputId ?>');
            $newProductForm.clone().insertAfter('#select2-<?= $inputId ?>-results');
        }
    });
    $(document).on('click', '#new-product-group-submit-<?= $inputId ?>', function() {
        var input = $(this).parent().children('input');
        $.post('/product/add-group', input.serialize(), function(data) {
            if (data.itemId && data.itemName) {
                input.val('');
                var newOption = new Option(data.itemName, data.itemId, false, true);
                newOption.dataset.editable = 1;
                newOption.dataset.deletable = 1;
                $("#<?= $inputId ?>").select2("close");
                $("#<?= $inputId ?>").append(newOption).trigger('change');
            }
        });
    });
</script>

<div id="<?= $inputId ?>-del-modal" class="confirm-modal fade modal"
     role="dialog" tabindex="-1" aria-hidden="true"
     style="display: none; margin-top: -51.5px;">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-body">
                    <div class="row">
                        Вы уверены, что хотите удалить группу товара
                        <br>
                        "<span class="item-name"></span>"?
                    </div>
                </div>
                <div class="form-actions row">
                    <div class="col-xs-6">
                        <button type="button" class="btn darkblue pull-right js-item-delete" data-dismiss="modal">ДА</button>
                    </div>
                    <div class="col-xs-6">
                        <button type="button" class="btn darkblue" data-dismiss="modal">НЕТ</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php endif ?>
