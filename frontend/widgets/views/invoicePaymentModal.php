<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 28.06.2018
 * Time: 13:30
 */

use yii\bootstrap\Modal;
use php_rutils\RUtils;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $data array */

$data = $data ?? [];

Modal::begin([
    'header' => '<h1 style="text-align: center;margin: 0;text-transform: uppercase;">Добавить счет</h1>',
    'id' => 'modal-invoice-payment',
]); ?>

    <div class="form-body">
        <div class="row blocks">
            <?php foreach ($data as $item) : ?>
                <div class="col-md-4">
                    <div class="col-md-4 one-block" style="width: 100%; margin: 0;">
                        <span class="tariff-name">
                            <?= $item['count'] ?>
                            <?= RUtils::numeral()->choosePlural($item['count'], [
                                'счет', //1
                                'счета', //2
                                'счетов' //5
                            ]); ?> за <br> <?= $item['price']; ?> руб.<br><br><br>
                        </span>
                        <span class="tariff-info">
                            <span class="fa fa-info"></span>
                            Вместе со счетом вы сможете создать акт, накладную и счет-фактуру
                        </span>
                    </div>
                </div>
            <?php endforeach ?>
            <?php foreach ($data as $item) : ?>
                <div class="col-md-4" style="margin-top: 10px;">
                    <?= Html::beginForm($item['action'], $item['method'], ['target' => '_blank',]) ?>
                        <?= $item['input'] ?>
                        <?= Html::submitButton('Оплатить', [
                            'class' => 'btn darkblue widthe-100',
                        ]); ?>
                    <?= Html::endForm() ?>
                </div>
            <?php endforeach ?>
        </div>
    </div>

<?php Modal::end(); ?>
