<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 05.12.2018
 * Time: 15:02
 */

/* @var $inputId string */
?>

<div id="<?= $inputId ?>-del-modal" class="confirm-modal fade modal"
     role="dialog" tabindex="-1" aria-hidden="true"
     style="display: none; margin-top: -51.5px;">
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-body">
                <div class="form-body">
                    <div class="row">
                        Вы уверены, что хотите удалить доп. расход
                        <br>
                        "<span class="item-name"></span>"?
                    </div>
                </div>
                <div class="form-actions row">
                    <div class="col-xs-6">
                        <button type="button" class="btn darkblue pull-right js-item-delete" data-dismiss="modal">ДА</button>
                    </div>
                    <div class="col-xs-6">
                        <button type="button" class="btn darkblue" data-dismiss="modal">НЕТ</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
