<?php

use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Html;

/* @var $model yii\db\ActiveRecord */
/* @var $buttonClass string */
/* @var $attribute string */
/* @var $tableSelector string */

$tooltipClass = "tooltip-view-widget";
$tooltipContentId = "tooltip-text-view-widget";
$tooltipContent = ($model->{$attribute}) ? 'Обычный вид' : 'Компактный вид';
$icon = ($model->{$attribute}) ? 'tv_list2.png' : 'tv_list3.png';
if (empty($buttonClass)) {
    $buttonClass = 'button-list button-hover-transparent button-clr mr-auto';
}

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.change-table-view-mode',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'contentAsHTML' => true
    ],
]);

?>

<?= Html::button(
    '<img width="16" height="16" class="tv-icon" src="/img/icons/'.$icon.'"/>',
    [
        'class' => implode(' ', ['change-table-view-mode', $buttonClass, $tooltipClass]),
        'data-tooltip-content' => '.'.$tooltipContentId,
        'data-attribute' => $attribute,
        'data-target' => $tableSelector,
        'data-pjax' => 0,
        'title' => $tooltipContent,
    ]
); ?>

<div style="display: none">
    <img src="/img/icons/tv_list2.png"/>
    <img src="/img/icons/tv_list3.png"/>
</div>
