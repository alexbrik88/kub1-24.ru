<?php

use yii\helpers\Html;

/* @var $model yii\db\ActiveRecord */
/* @var $items array */
/* @var $sortingItems array */

?>
<div class="pull-right" title="Настройка вида" style="display: inline-block;">
    <?= Html::a('<i class="glyphicon glyphicon-th icon-th" style="font-size: 14px; line-height: 14px; margin: 0;"></i>', '#', [
        'class' => 'dropdown-toggle',
        'data-toggle' => 'dropdown',
        'aria-expanded' => 'false',
    ]) ?>
    <ul id="config_items_box" class="dropdown-menu" role="menu" style="padding: 0 5px;">
        <li>
            <label>Столбцы</label>
        </li>
        <?php foreach ($items as $item) : ?>
            <?php if (!empty($item['attribute']) && (!array_key_exists('visible', $item) || $item['visible'])) : ?>
                <?php $type = $item['type'] ?? null; ?>
                <li>
                    <?php if ($type === null): ?>
                        <?= Html::activeCheckbox($model, $item['attribute'], [
                            'data-target' => 'col_' . $item['attribute'],
                            'label' => isset($item['label']) ? $item['label'] : $model->getAttributeLabel($item['attribute']),
                        ]); ?>
                    <?php elseif ($type === 'radio'): ?>
                        <?= Html::activeRadio($model, $item['attribute'], [
                            'data-target' => 'col_' . $item['attribute'],
                            'label' => isset($item['label']) ? $item['label'] : $model->getAttributeLabel($item['attribute']),
                        ]); ?>
                    <?php endif; ?>
                </li>
            <?php endif ?>
        <?php endforeach ?>
        <?php if (!empty($sortingItems)): ?>
            <li class="bold" style="border-top: 1px solid #ddd;">
                Сортировка по умолчанию <br>
                по столбцу:
            </li>
        <?php endif; ?>
        <?php foreach ($sortingItems as $sortingItem): ?>
            <?php if (!empty($sortingItem['attribute']) && (!array_key_exists('visible', $sortingItem) || $sortingItem['visible'])) : ?>
                <li>
                    <?= Html::radio($sortingItem['attribute'], $sortingItem['checked'], [
                        'class' => 'sorting-table-config-item',
                        'id' => $sortingItem['attribute'],
                        'data-target' => 'col_' . $sortingItem['attribute'],
                        'label' => isset($sortingItem['label']) ? $sortingItem['label'] : $sortingItem['attribute'],
                    ]); ?>
                </li>
            <?php endif; ?>
        <?php endforeach; ?>
    </ul>
</div>