<?php

use common\widgets\Modal;
use common\components\ImageHelper;


Modal::begin([
    'header' => '
        <h1 style="text-align: center;margin: 0;text-transform: uppercase;">ЗАГРУЗИМ В КУБ ВАШИ ДОКУМЕНТЫ <br /> БЕСПЛАТНО!</h1>
        ',
    'footer' => $this->render('_partial_scan_popup_footer'),
    'footerOptions' => [
        'style' => 'background-color:#4276a4'
    ],
    'id' => 'modal-scan',
]); ?>
<div class="form-body">
    <div class="row">
        <h4 style="text-align: center;font-size: 17px;color: #00618e;">
           Что бы у вас все данные были в одном месте и их было удобно анализировать, а так же контролировать должников, пришлите нам ваши счета и акты, товарные накладные и счета-фактуры<br />
            Мы их загрузим в КУБ абсолютно бесплатно!
        </h4>
    </div>
    <div class="col-md-12 steps">
        <div class="col-md-4" style="width: 5.5%;border:  0;"></div>
        <div class="col-md-4 next-modal expose-invoice"
             style="background-position: center;background-repeat: no-repeat;
                 background-image: url(<?= ImageHelper::getThumbSrc('img/modal_registration/invoice.png', [160, 160]); ?>)">
            <div class="name">
                Все документы в одном месте и удобно отображаются
            </div>
        </div>
        <div class="col-md-4 next-modal" data-type="2"
             style="background-position: center;background-repeat: no-repeat;
                 background-image: url(<?= ImageHelper::getThumbSrc('img/modal_registration/business_analyse.png', [160, 160]); ?>)">
            <div class="name">
                Просто копировать и создавать новые счета и другие документы
            </div>
        </div>
        <div class="col-md-4 next-modal" data-type="3"
             style="background-position: center;background-repeat: no-repeat;
                 background-image: url(<?= ImageHelper::getThumbSrc('img/modal_registration/expose_other_documents.png', [160, 160]); ?>)">
            <div class="name">
                Контроль оплат и контроль должников
            </div>
        </div>
        <div class="col-md-4" style="width: 5.5%;border:  0;"></div>
        <div class="col-md-4" style="width: 5.5%;border:  0;"></div>
        <div class="col-md-4 next-modal" data-type="4"
             style="background-position: center;background-repeat: no-repeat;
                 background-image: url(<?= ImageHelper::getThumbSrc('img/modal_registration/employee_plan.png', [160, 160]); ?>)">
            <div class="name">
                Наглядные графики по месяцам: выручка, затраты, долги, прибыль
            </div>
        </div>
        <div class="col-md-4 next-modal" data-type="5"
             style="background-position: center;background-repeat: no-repeat;
                 background-image: url(<?= ImageHelper::getThumbSrc('img/modal_registration/stock_control.png', [160, 160]); ?>)">
            <div class="name">
                Удобная аналитика: движение денег, продажи, остаток денег, средний чек, затраты
            </div>
        </div>
        <div class="col-md-4 next-modal" data-type="6"
             style="background-position: center;background-repeat: no-repeat;
                 background-image: url(<?= ImageHelper::getThumbSrc('img/modal_registration/bill.png', [160, 160]); ?>)">
            <div class="name">
                Источники прибыли: маржа по товарам и услугам, доход на клиента, доход на сотрудника
            </div>
        </div>
        <div class="col-md-4" style="width: 5.5%;border:  0;"></div>
    </div>
</div>
<?php Modal::end(); ?>

<?php $this->registerJs('
        $(document).ready(function () {
            var timer = parseInt(localStorage["scan_t"]);
            if (timer >= 15) {
                $("#modal-scan").modal();
                $.post("' . \yii\helpers\Url::to(['/documents/invoice/hide-scan-popup']) . '", {"showed_scan":1});
                return;
            }
            if (!timer) {
                timer = localStorage["scan_t"] = 1;
            }
            var intervalId = setInterval(function() {
                console.log(timer);
                if (timer >= 15) {
                    $("#modal-scan").modal();
                    clearInterval(intervalId);
                    $.post("' . \yii\helpers\Url::to(['/documents/invoice/hide-scan-popup']) . '", {"showed_scan":1});
                    return;
                }
                timer = parseInt(localStorage["scan_t"]);
                localStorage["scan_t"] = timer + 1;
            }, 1000);
        });
');?>