<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 14.01.2019
 * Time: 22:48
 */

use common\widgets\Modal;

?>
<?php Modal::begin([
    'id' => 'menu-no-rules',
    'header' => null,
]); ?>
    <div class="col-md-12">
        <div class="text">
            У вас недостаточно прав для использования данного раздела
        </div>
        <div class="text" style="margin-top: 20px;">
            Обратитесь к вашему Руководителю для добавления дополнительных возможностей сервиса КУБ
        </div>
    </div>
    <div class="col-md-12 text-center" style="margin-top: 30px;">
        <button type="button" class="btn darkblue text-white" data-dismiss="modal" style="width: 65px;">OK</button>
    </div>
<?php Modal::end(); ?>
