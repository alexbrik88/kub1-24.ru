<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 06.10.2016
 * Time: 6:53
 */

use common\components\helpers\ArrayHelper;
use common\models\Company;
use common\models\company\CompanyType;
use common\models\TaxationType;
use frontend\models\RegistrationForm;
use frontend\rbac\permissions;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Html;
use yii\bootstrap\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var Company $newCompany */
/* @var $companyTaxation common\models\company\CompanyTaxationType */

$config = [
    'options' => [
        'class' => 'form-group',
    ],
    'labelOptions' => [
        'class' => 'col-md-4 control-label bold-text',
    ],
    'wrapperOptions' => [
        'class' => 'col-md-8',
    ],
    'template' => "{label}\n{beginWrapper}\n{input}\n{error}\n{hint}\n{endWrapper}",
];

$this->registerJs('
    $(document).on("submit", "#create-company-modal", function() {
        $("#create-company-modal-loading").show();
    })
');

$typeArray = CompanyType::find()->select('name_short')->andWhere([
    'id' => RegistrationForm::$typeIds,
])->indexBy('id')->column();
$typeItems = [];
foreach (RegistrationForm::$typeIds as $typeId) {
    $typeItems[$typeId] = $typeArray[$typeId] ? : 'Самозанятый';
}
?>

<?php Modal::begin([
    'id' => 'create-company',
    'header' => '<h1>Добавление новой компании</h1>'
]) ?>
    <?php if (1 || Yii::$app->user->can(permissions\Company::CREATE)) : ?>
        <?php $form = ActiveForm::begin([
            'action' => Url::to(['/company/create']),
            'options' => [
                'class' => 'form-horizontal form-checking-accountant',
                'id' => 'create-company-modal',
            ],
        ]); ?>

        <div class="form-body">
            <div>
                <?= $form->field($newCompany, 'company_type_id', array_merge($config, [
                    'options' => [
                        'class' => 'form-group company-type-chooser',
                    ],
                ]))->radioList($typeItems)
                    ->label('Форма собственности'); ?>
            </div>
            <div
                class="taxation-type field-registrationform-taxationtypeosno required">
                <label class="control-label bold-text">Система
                    налогобложения</label>

                <div class="fields required" style="display: inline-block;">
                    <?= $form->field($companyTaxation, 'osno', [
                        'options' => [
                            'style' => 'margin-left: 38px; display: inline-block;',
                        ],
                    ])->checkbox()->error(false); ?>
                    <?= $form->field($companyTaxation, 'usn', [
                        'options' => [
                            'style' => 'margin-left: 20px; display: inline-block;',
                        ],
                    ])->checkbox(); ?>
                    <?= $form->field($companyTaxation, 'psn', [
                        'options' => [
                            'style' => 'margin-left: 20px; display: inline-block;',
                        ],
                    ])->checkbox([
                        'disabled' => true,
                    ]); ?>
                    <div class="help-block"></div>
                </div>
            </div>
            <div class="form-actions">
                <div class="row action-buttons" id="buttons-fixed">
                    <div
                        class="spinner-button col-sm-1 col-xs-1">
                        <?= Html::submitButton('<span class="ladda-label">Сохранить</span><span class="ladda-spinner"></span>', [
                            'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-md hidden-sm hidden-xs  mt-ladda-btn ladda-button',
                            'data-style' => 'expand-right',
                            'style' => 'width: 130px!important;',
                        ]); ?>
                        <?= Html::submitButton('<i class="fa fa-floppy-o fa-2x"></i>', [
                            'class' => 'btn darkblue btn-save darkblue widthe-100 hidden-lg',
                            'title' => 'Сохранить',
                        ]); ?>
                    </div>
                    <div
                        class="button-bottom-page-lg col-sm-1 col-xs-1">
                    </div>
                    <div
                        class="button-bottom-page-lg col-sm-1 col-xs-1">
                    </div>
                    <div
                        class="button-bottom-page-lg col-sm-1 col-xs-1">
                    </div>
                    <div
                        class="button-bottom-page-lg col-sm-1 col-xs-1">
                    </div>
                    <div
                        class="spinner-button col-sm-2 col-xs-1">
                        <?= Html::button('Отменить', [
                            'class' => 'btn darkblue widthe-100 hidden-md hidden-sm hidden-xs back',
                            'style' => 'width: 130px!important;',
                        ]); ?>
                        <?= Html::button('<i class="fa fa-reply fa-2x"></i></button>', [
                            'class' => 'btn darkblue widthe-100 hidden-lg back',
                            'title' => 'Отменить',
                        ]); ?>
                    </div>
                </div>
            </div>
        </div>
        <?php $form->end(); ?>
    <?php else : ?>
        Ваш аккаунт имеет компании на «Пробном» или «Бесплатном» тарифе. Чтобы добавить еще одну компанию, нужно
        <?= Html::a('оплатить сервис', '/subscribe/default/index') ?>.
    <?php endif ?>
<?php Modal::end() ?>
