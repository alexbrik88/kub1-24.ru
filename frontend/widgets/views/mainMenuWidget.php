<?php
// TODO: set phpdoc.

use common\models\Contractor;
use common\models\document\ScanDocument;
use common\models\product\Product;
use frontend\models\Documents;
use frontend\modules\cash\modules\banking\components\Banking;
use frontend\widgets\AccountBalanceWidget;
use frontend\widgets\CompanyLimitsWidget;
use frontend\widgets\NewsWidget;
use frontend\rbac\UserRole;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\employee\Employee;
use common\models\employee\EmployeeRole;
use common\models\service;
use yii\bootstrap\Modal;
use php_rutils\RUtils;
use common\models\Chat;
use frontend\rbac\permissions;
use common\components\ImageHelper;
use yii\widgets\Pjax;
use common\models\company\RegistrationPageType;
use \common\models\file\File;

/* @var $notifications common\models\notification\Notification[]
 * @var $newMessages Chat[]
 * @var $newMessagesIDs
 */

$count = 0;
$company = ArrayHelper::getValue(Yii::$app->user, 'identity.company');
$companyId = ArrayHelper::getValue($company, 'id', '');
$companyName = ArrayHelper::getValue($company, 'shortTitle', '');

$notifications = empty($notifications) ? [] : $notifications;

$newFilesCount = $company ? ScanDocument::getNewScansCount($company) : 0;
$subscribeUrl = Yii::$app->user->isMtsUser ? ['/mts/subscribe/index'] : ['/subscribe'];

$canThemeSwitch = true; /* YII_ENV_DEV || (
    $company && in_array($company->id, (array) ArrayHelper::getValue(Yii::$app->params, 'canThemeSwitch'))
); */
?>
<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner" style="display: flex; align-items: center;">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="/">
                <img class="logo-default m-t-8" src="/img/logo-standart-w24.png" style="height: 30px;" alt="logo">
                <!-- <img class="logo-default m-t-8" alt="logo"
                     src="/img/logo_pravka.png"> -->
            </a>

            <div class="menu-toggler sidebar-toggler">
                <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
            </div>
        </div>
        <!-- END LOGO -->
        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a data-target=".navbar-collapse" data-toggle="collapse"
           class="menu-toggler responsive-toggler" href="javascript:;"></a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <!-- BEGIN TOP NAVIGATION MENU -->
        <div class="top-menu" style="display: flex; align-items: center; flex: 1; justify-content: space-between;">
            <div style="display: flex; align-items: center; flex: 1;">
                <div class="hidden-xs hidden-sm" style="
                    display: flex;
                    flex: 1;
                    align-items: center;
                    justify-content: space-between;
                    max-width: 330px;
                    height: 46px;
                ">
                    <div class="hidden-md" style="margin-left: 15px;">
                        <?php if ($company && Yii::$app->user->can(UserRole::ROLE_CHIEF)) : ?>
                            <?= AccountBalanceWidget::widget(['company' => $company]) ?>
                        <?php else : ?>
                            <?= \frontend\widgets\TimePickerWidget::widget() ?>
                            <div class="hidden-xs hidden-sm hidden-md" style="display: inline-block;">
                                <?php
                                echo \php_rutils\RUtils::dt()->ruStrFTime([
                                    'format' => ' d F Y, ',
                                    'monthInflected' => true,
                                ]);
                                echo mb_strtoupper(\php_rutils\RUtils::dt()->ruStrFTime(['format' => 'D',]), 'UTF8');
                                ?>
                            </div>
                        <?php endif ?>
                    </div>
                    <?php if ($canThemeSwitch) : ?>
                        <?= Html::beginForm(['/site/change-theme'], 'get', [
                            'id' => 'theme-switcher-form',
                        ]) ?>
                            <?= Html::hiddenInput('theme', 'new') ?>
                            <div class="switch">
                                <?= Html::checkbox('theme', false, [
                                    'id' => 'theme-switcher-checkbox',
                                    'class' => 'md-check switch-input input-hidden hidden',
                                ]) ?>
                                <label class="switch-label" for="theme-switcher-checkbox">
                                    <span class="switch-pseudo">&nbsp;</span>
                                    <span class="switch-label-text">
                                        Перейти в
                                        <br>
                                        новый дизайн
                                    </span>
                                </label>
                            </div>
                        <?= Html::endForm() ?>
                        <script type="text/javascript">
                            (function( $ ) {
                                $("#theme-switcher-checkbox").on("change", function (e) {
                                    this.form.submit();
                                });
                            })(jQuery);
                        </script>
                    <?php endif ?>
                </div>
            </div>
            <ul class="nav navbar-nav pull-right" style="margin-left: auto;">
                <?php if (!Yii::$app->user->isGuest) : ?>
                    <?php

                    $linkText = '';
                    /* @var $user Employee */
                    $user = Yii::$app->user->identity;
                    $company = $user->company;
                    $canCreateCash = Yii::$app->user->can(frontend\rbac\permissions\Cash::CREATE);
                    $canCreateProduct = Yii::$app->user->can(frontend\rbac\permissions\Product::CREATE);
                    $actualSubscribes = $company ? service\SubscribeHelper::getPayedSubscriptions($company->id) : [];
                    $expireDate = service\SubscribeHelper::getExpireDate($actualSubscribes);
                    $expireDays = service\SubscribeHelper::getExpireLeftDays($expireDate);
                    $isTrial = (count($actualSubscribes) === 1 && $actualSubscribes[0]->tariff_id == service\SubscribeTariff::TARIFF_TRIAL);
                    $canViewAutoAct = $canViewAutoInvoice = Yii::$app->user->can(permissions\document\Document::INDEX, [
                        'ioType' => Yii::$app->request->get('type', null),
                    ]);
                    $canViewPaymentReminder = in_array($user->employee_role_id, [EmployeeRole::ROLE_CHIEF,
                        EmployeeRole::ROLE_MANAGER, EmployeeRole::ROLE_SUPERVISOR, EmployeeRole::ROLE_SUPERVISOR_VIEWER,]);
                    $canViewSaleIncrease = Yii::$app->user->can(permissions\Contractor::INDEX, [
                        'type' => Contractor::TYPE_CUSTOMER,
                    ]);
                    $canActivate = $canViewAutoAct || $canViewAutoInvoice || $canViewPaymentReminder || $canViewSaleIncrease;
                    $canCreateDocument = Yii::$app->user->can(permissions\document\Document::CREATE);

                    echo Html::hiddenInput(null, Yii::$app->user->identity->id, [
                        'class' => 'identity_employee_id',
                    ]);
                    echo Html::hiddenInput(null, YII_ENV_DEV ? 8085 : 8086, [
                        'class' => 'chat-port',
                    ]);

                    ?>

                    <?php if ($company && $user->employee_role_id === EmployeeRole::ROLE_CHIEF) : ?>
                        <?php if ($actualSubscribes === null || $isTrial || $expireDays <= 10) :
                            $invoiseLeft = $company->getInvoiceLeft();
                            if ($company->isFreeTariff || $company->getIsTrialNotPaid()) {
                                $linkText .= 'Вы можете создать еще ' . RUtils::numeral()->getPlural($invoiseLeft ? : 0, ['счет', 'счета', 'счетов']);
                            } else {
                                if ($isTrial) {
                                    $linkText .= 'Осталось ';
                                    $linkText .= RUtils::numeral()->getPlural($expireDays, ['день', 'дня', 'дней']);
                                    $linkText .= '. ';
                                }
                                $linkText .= 'Оплатить КУБ';
                            } ?>
                            <li class="dropdown dropdown-extended dropdown-notification">
                                <?= Html::a('<span class="pay-service">' . $linkText . '</span>', $subscribeUrl, [
                                    'class' => 'dropdown-toggle pay-service-desct',
                                ]); ?>
                                <?= $company !== null && $company->isFreeTariff ?
                                    Html::a(
                                        $this->render('main_menu_parts/_ico_free'),
                                        $subscribeUrl,
                                        ['class' => 'dropdown-toggle pay-service-mob']
                                    ) :
                                    Html::a(
                                        '<i class="fa fa-rub"></i>',
                                        $subscribeUrl,
                                        ['class' => 'dropdown-toggle pay-service-mob-is-free']
                                    );
                                ?>
                            </li>
                        <?php endif ?>
                    <?php endif ?>

                    <?= $company ? NewsWidget::widget(['employee' => $user]) : null; ?>

                    <li class="dropdown dropdown-extended dropdown-notification dropdown-notification-menu hidden-fa-plus-circle">
                        <a data-close-others="true" data-hover="dropdown"
                           data-toggle="dropdown" class="dropdown-toggle"
                           href="#" aria-expanded="true" style="padding: 5px 5px;">
                            <?= ImageHelper::getThumb(Yii::getAlias('@webroot/img/ico-plus.png'), [35, 35], [
                                'class' => 'fa fa-plus-circle',
                                'style' => 'width: 35px;',
                            ]); ?>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default with_pads">
                            <li class="title">
                                Добавить
                            </li>
                            <li>
                                <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Invoice::CREATE, [
                                    'ioType' => Documents::IO_TYPE_OUT,
                                ])
                                ): ?>
                                    <?= Html::a('Счет покупателю', Url::to(['/documents/invoice/create', 'type' => Documents::IO_TYPE_OUT]), [
                                        'class' => $company->createInvoiceAllowed(Documents::IO_TYPE_OUT) ?
                                            'btn' :
                                            'btn action-is-limited',
                                        'style' => 'background-color: #45b6af; color: #fff !important;',
                                    ]); ?>
                                <?php endif; ?>
                            </li>
                            <li>
                                <?php if (Yii::$app->user->can(frontend\rbac\permissions\Contractor::CREATE, ['type' => Contractor::TYPE_CUSTOMER])): ?>
                                    <?= Html::a('Покупателя', Url::to(['/contractor/create', 'type' => Contractor::TYPE_CUSTOMER]), [
                                        'class' => 'btn',
                                    ]); ?>
                                <?php endif; ?>
                            </li>
                            <li>
                                <?php if (Yii::$app->user->can(frontend\rbac\permissions\document\Invoice::CREATE, [
                                    'ioType' => Documents::IO_TYPE_IN,
                                ])
                                ): ?>
                                    <?= Html::a('Счет от поставщика', Url::to(['/documents/invoice/create', 'type' => Documents::IO_TYPE_IN]), [
                                        'class' => $company->createInvoiceAllowed(Documents::IO_TYPE_IN) ?
                                            'btn' :
                                            'btn action-is-limited',
                                    ]); ?>
                                <?php endif; ?>
                            </li>
                            <li>
                                <?php if (Yii::$app->user->can(frontend\rbac\permissions\Contractor::CREATE, ['type' => Contractor::TYPE_SELLER])): ?>
                                    <?= Html::a('Поставщика', Url::to(['/contractor/create', 'type' => Contractor::TYPE_SELLER]), [
                                        'class' => 'btn',
                                    ]); ?>
                                <?php endif; ?>
                            </li>
                            <li>
                                <?php if ($canCreateProduct): ?>
                                    <?= Html::a('Товар', Url::to(['/product/create', 'productionType' => Product::PRODUCTION_TYPE_GOODS]), [
                                        'class' => 'btn',
                                    ]); ?>
                                <?php endif; ?>
                            </li>
                            <li>
                                <?php if (Yii::$app->user->can(frontend\rbac\permissions\Employee::CREATE)): ?>
                                    <?= Html::a('Сотрудника', Url::to(['/employee/create']), [
                                        'class' => 'btn',
                                    ]); ?>
                                <?php endif; ?>
                            </li>
                            <li>
                                <?php if ($canCreateProduct): ?>
                                    <?= Html::a('Услугу', Url::to(['/product/create', 'productionType' => Product::PRODUCTION_TYPE_SERVICE]), [
                                        'class' => 'btn',
                                    ]); ?>
                                <?php endif; ?>
                            </li>
                            <li>
                                <?php if (Yii::$app->user->can(frontend\rbac\permissions\Contractor::CREATE)): ?>
                                    <?= Html::a('Договор',
                                        Url::to(['/documents/agreement/index', 'type' => Documents::IO_TYPE_OUT, 'modal' => 1]
                                        ), [
                                            'class' => 'btn',
                                            'data-url' => '/documents/agreement/index?modal=1'
                                        ]); ?>
                                <?php endif; ?>
                            </li>
                            <?php if ($canCreateCash || $canCreateProduct) : ?>
                                <li class="title title-pad2">
                                    Загрузить
                                </li>
                                <?php if ($canCreateCash): ?>
                                    <li class="btn-line">
                                        <?= Html::a('Выписку из банка', [
                                            '/cash/banking/default/index',
                                            'p' => Banking::routeEncode(['/cash/bank/index']),
                                        ], [
                                            'class' => 'btn',
                                        ]); ?>
                                    </li>
                                <?php endif; ?>
                                <?php if ($canCreateProduct) : ?>
                                    <li class="btn-line">
                                        <?= Html::a('Товары из таблицы Excel', Url::to(['/product/index', 'productionType' => 1, 'modal' => true]), [
                                            'class' => 'btn',
                                        ]); ?>
                                    </li>
                                    <li class="btn-line">
                                        <?= Html::a('Услуги из таблицы Excel', Url::to(['/product/index', 'productionType' => 0, 'modal' => true]), [
                                            'class' => 'btn',
                                        ]); ?>
                                    </li>
                                <?php endif ?>
                            <?php endif ?>
                            <?php if ($canActivate): ?>
                                <li class="title title-pad2">
                                    Включить
                                </li>
                                <?php if ($canViewAutoInvoice): ?>
                                    <li class="btn-line">
                                        <?= Html::a('Рассылку АвтоСчетов', Url::to(['/documents/invoice/index-auto',]), [
                                            'class' => 'btn',
                                        ]); ?>
                                    </li>
                                <?php endif; ?>
                                <?php if ($canViewAutoAct): ?>
                                    <li class="btn-line">
                                        <?= Html::a('Выставление АвтоАктов', Url::to(['/documents/act/index', 'type' => Documents::IO_TYPE_OUT, 'autoActModal' => true]), [
                                            'class' => 'btn',
                                        ]); ?>
                                    </li>
                                <?php endif; ?>
                                <?php if ($canViewPaymentReminder && false): ?>
                                    <li class="btn-line">
                                        <?= Html::a('Рассылку писем должникам', Url::to(['/payment-reminder/index',]), [
                                            'class' => 'btn',
                                        ]); ?>
                                    </li>
                                <?php endif; ?>
                                <?php if ($canViewSaleIncrease): ?>
                                    <li class="btn-line">
                                        <?= Html::a('Увеличение онлайн продаж', Url::to(['/contractor/sale-increase',]), [
                                            'class' => 'btn',
                                        ]); ?>
                                    </li>
                                <?php endif; ?>
                            <?php endif; ?>
                        </ul>
                    </li>
                    <!-- BEGIN NOTIFICATION DROPDOWN -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->

                    <?php foreach ($notifications as $notification) {
                    if (date('Y-m-d H:i:s', $notification->updated_at) > Yii::$app->user->identity->view_notification_date) {
                        $count++;
                    }
                } ?>
                    <li id="header_notification_bar"
                        class="dropdown dropdown-extended dropdown-notification dropdown-notification-list-head hide413"
                        style="display: <?= empty($notifications) ? 'none' : 'inline-block'; ?>;">
                        <a data-close-others="true"
                           data-hover="dropdown"
                           data-toggle="dropdown"
                           class="dropdown-toggle"
                           href="#">
                            <i class="icon-bell"></i>
                            <?php if ($count > 0 && Yii::$app->controller->action->id != 'first-create') : ?>
                                <span class="badge badge-default">
                                        <?= $count; ?>
                                    </span>
                            <?php endif; ?>
                        </a>
                        <ul class="dropdown-menu">
                            <li style="width:100%!important;">
                                <div class="slimScrollDiv"
                                     style="position: relative; overflow: hidden; width: auto; height: 270px;">
                                    <ul data-handle-color="#637283"
                                        style="height: 270px; overflow: hidden; width: auto;"
                                        class="dropdown-menu-list scroller"
                                        data-initialized="1">
                                        <?php foreach ($notifications as $notification) : ?>
                                            <li style="width:100%!important;">
                                                <a href="<?= Url::to(['/notification/index', 'month' => date('m'), 'year' => date('Y')]); ?>">
                                                        <span class="details">
                                                            <span class="time">
                                                                <?= \php_rutils\RUtils::dt()->ruStrFTime([
                                                                    'format' => 'd F',
                                                                    'monthInflected' => true,
                                                                ]); ?>
                                                            </span>
                                                            <span
                                                                    class="details">
                                                                <span
                                                                        class="label label-sm label-icon label-info">
                                                                    <i class="fa fa-bullhorn"></i>
                                                                </span>
                                                                <span
                                                                        class="details-title">
                                                                    <?= $notification->title; ?>
                                                                </span>
                                                            </span>
                                                        </span>
                                                </a>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                    <div class="slimScrollBar"
                                         style="background: none repeat scroll 0% 0% rgb(99, 114, 131); width: 7px; position: absolute; top: 0px; opacity: 0.4; display: block; border-radius: 7px; z-index: 99; right: 1px;"></div>
                                    <div class="slimScrollRail"
                                         style="width: 7px; height: 100%; position: absolute; top: 0px; display: none; border-radius: 7px; background: none repeat scroll 0% 0% rgb(234, 234, 234); opacity: 0.2; z-index: 90; right: 1px;"></div>
                                </div>
                            </li>
                        </ul>
                    </li>

                    <?php if ($company) : ?>
                        <?= CompanyLimitsWidget::widget(['company' => $company]) ?>
                    <?php endif; ?>

                    <?php /*
                    <li class="dropdown dropdown-extended dropdown-notification">
                        <?php $isUploadManagerActive = strpos(Yii::$app->request->url, 'upload-manager'); ?>
                        <?php echo yii\helpers\Html::a('<img width="20" src="/img/documents/main-folder-2.png"/>' .
                            ($newFilesCount ? ('<span class="badge badge-blue" style="right:0">'.$newFilesCount.'</span>') : ''),
                            ($isUploadManagerActive) ?
                                    Url::previous('UploadManagerReturn') :
                                    Yii::$app->urlManager->createUrl('documents/upload-manager/index'), [
                                'class' => 'dropdown-toggle ' . ($isUploadManagerActive ? 'active':''),
                                'title' => 'Документы',
                                'style' => 'padding-top:11px'
                            ]); ?>
                    </li>
                    */ ?>
                    <?php /*
                    <li id="header_chat_bar" class="dropdown dropdown-extended dropdown-notification hide413">
                        <a href="<?= Url::to(['/chat/index']); ?>"
                           class="dropdown-toggle"
                           data-close-others="true"
                           data-hover="dropdown"
                           data-toggle="dropdown">
                            <i class="fa fa-comments-o" aria-hidden="true"></i>
                            <!-- <i class="new-icons icon-speech-bubble"
                               style="font-size: 16px !important; height: 20px;"></i> -->
                            <?php if (count($newMessages)): ?>
                                <span class="badge badge-default">
                                    <?= count($newMessages); ?>
                                    </span>
                            <?php endif; ?>
                        </a>
                        <ul class="dropdown-menu" style="display: <?= count($newMessages) ? '' : 'none'; ?>;">
                            <li style="width:100%!important;">
                                <ul data-handle-color="#637283"
                                    style="height: 270px; overflow: hidden; width: auto;"
                                    class="dropdown-menu-list scroller"
                                    data-initialized="1">
                                    <?php foreach ($newMessages as $newMessage): ?>
                                        <li class="chat-notification" style="width:100%!important;">
                                            <a href="<?= Url::to(['/chat/index', 'userID' => $newMessage->from_employee_id]); ?>">
                                                        <span class="details">
                                                            <span class="time">
                                                                <?= \php_rutils\RUtils::dt()->ruStrFTime([
                                                                    'date' => $newMessage->created_at,
                                                                    'format' => 'd F',
                                                                    'monthInflected' => true,
                                                                ]); ?>
                                                            </span>
                                                            <span
                                                                    class="details">
                                                                <span
                                                                        class="label label-sm label-icon label-info">
                                                                    <i class="fa fa-bullhorn"></i>
                                                                </span>
                                                                <span
                                                                    class="details-title">
                                                                    <?= $newMessage->message; ?>
                                                                </span>
                                                            </span>
                                                        </span>
                                            </a>
                                        </li>
                                    <?php endforeach; ?>
                                </ul>
                            </li>
                        </ul>
                    </li>
                    */ ?>
                    <!-- END NOTIFICATION DROPDOWN -->
                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <!--<li>-->
                    <!--    <i class="fa fa-plus-circle"></i>-->
                    <!--</li>-->
                    <!--<li>-->
                    <!--    <i class="fa fa-question-circle"></i>-->
                    <!--</li>-->
                    <li class="dropdown dropdown-user">
                        <a data-close-others="true" data-hover="dropdown"
                           data-toggle="dropdown" class="dropdown-toggle"
                           style="white-space: nowrap;"
                           href="#">
                            <span
                                    class="username username-hide-on-mobile partially_hide_username"
                                    title="<?= $companyName ?>">
                                <?= $companyName ?>
                            </span>
                            <i class="fa fa-angle-down" style="margin-right: -10px;margin-bottom: 4px;"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <?php if (Yii::$app->user->can(frontend\rbac\permissions\Company::PROFILE)): ?>
                                <li>
                                    <?= Html::a('<i class="fa fa-gear"></i> Профиль компании', ['/company/index']); ?>
                                </li>
                            <?php endif; ?>
                            <?php if (Yii::$app->user->can(frontend\rbac\permissions\Company::VISIT_CARD)): ?>
                                <li>
                                    <?= Html::a('<i class="flaticon-id-card"></i> Визитка компании', ['/company/visit-card']); ?>
                                </li>
                            <?php endif; ?>
                            <?php if (Yii::$app->user->can(frontend\rbac\permissions\User::PROFILE_EDIT)): ?>
                                <li>
                                    <?= Html::a('<i class="icon-user"></i> Ваш профиль', ['/profile/index']); ?>
                                </li>
                            <?php endif; ?>
                            <?php if (Yii::$app->user->can(frontend\rbac\permissions\Subscribe::INDEX)): ?>
                                <li>
                                    <?= Html::a('<i class="fa fa-rub" style="color: #fff;"></i> Оплатить КУБ', $subscribeUrl, [
                                        'style' => 'background-color: #45b6af; color: #fff;',
                                    ]); ?>
                                </li>
                            <?php endif; ?>
                            <?php if (Yii::$app->user->can(frontend\rbac\permissions\User::LOGOUT)): ?>
                                <li>
                                    <?= Html::a('<i class="icon-key"></i> Выход', ['/site/logout'], [
                                        'title' => 'Logout ' . (Yii::$app->user->identity ? ' (' . Yii::$app->user->identity->username . ')' : ''),
                                        'data-method' => 'post',
                                    ]); ?>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </li>
                    <!-- END USER LOGIN DROPDOWN -->
                    <li class="dropdown dropdown-user menu-settings">
                        <a data-close-others="true" data-hover="dropdown"
                           data-toggle="modal" class="dropdown-toggle" data-target="#modal-kub-do-it"
                           data-pulsate="<?= (int)($company && $company->show_popup_kub_do_it); ?>"
                           data-first_show="<?= (int)($company && $company->show_popup_kub_do_it); ?>"
                           data-url="<?= Url::to(['/site/view-popup-kub-do-it']); ?>"
                           href="#">
                            <i class="fa fa-question-circle font-size-20"></i>
                        </a>
                    </li>
                    <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <li class="dropdown dropdown-extended dropdown-notification">
                        <?php echo yii\helpers\Html::a('<i class="icon-logout"></i>', Yii::$app->urlManager->createUrl('site/logout'), [
                            'class' => 'dropdown-toggle logout-btn',
                            'title' => 'Выход',
                            'data-method' => 'post',
                            'style' => '-moz-transform: rotate(180deg);
                                            -ms-transform: rotate(180deg);
                                            -webkit-transform: rotate(180deg);
                                            -o-transform: rotate(180deg);
                                            transform: rotate(180deg);',
                        ]); ?>
                    </li>
                    <!-- END QUICK SIDEBAR TOGGLER -->
                <?php else: ?>
                    <?php if (Yii::$app->user->can(frontend\rbac\permissions\User::LOGIN)): ?>
                        <li class="dropdown dropdown-extended dropdown-notification">
                            <?php echo yii\helpers\Html::a('<i class="icon-login"></i>', ['/site/login'], [
                                'class' => 'dropdown-toggle logout-btn',
                                'title' => 'Login',
                            ]); ?>
                        </li>
                    <?php endif; ?>
                <?php endif; ?>
            </ul>
        </div>
        <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END HEADER INNER -->
</div>
<?= Html::hiddenInput(null, $newMessagesIDs, [
    'id' => 'exists-messages',
]); ?>
<div class="notification-message-template" style="display: none;">
    <a href="">
            <span class="details">
                <span class="time">
                </span>
                <span class="details">
                    <span class="label label-sm label-icon label-info">
                        <i class="fa fa-bullhorn"></i>
                    </span>
                    <span class="details-title">
                    </span>
                </span>
            </span>
    </a>
</div>

<?php
if (!Yii::$app->user->isGuest && $company) {
    if ($company->isFreeTariff) {
        Modal::begin([
            'id' => 'freeTariffNotify',
            'header' => $company->freeTariffNotified ?
                '<h3>Текущая подписка - БЕСПЛАТНО</h3>' :
                '<h3>Ваш аккаунт переведен на тариф БЕСПЛАТНО</h3>',
            'headerOptions' => ['style' => 'background-color: #00b7af; color: #fff;'],
            'toggleButton' => false,
            'options' => ['style' => 'max-width: 580px;']
        ]);

        echo $this->render('_free_tariff_notify');

        Modal::end();

        if (Yii::$app->user->identity->employee_role_id === EmployeeRole::ROLE_CHIEF) {
            if (!$company->freeTariffNotified) {
                $company->freeTariffNotified = true;
                $this->registerJs('$("#freeTariffNotify").modal("show");');
            }
            $this->registerJs('
                $(document).on("click", ".action-is-limited", function(e) {
                    e.preventDefault();
                    $("#freeTariffNotify").modal("show");
                });
            ');
        }
    }

    $userId = Yii::$app->user->id;
    $this->registerJS(<<<JS
$(function () {
    $(document).on('mouseenter', '#header_notification_bar', function () {
    var notificationMenu = $(this);
        if (notificationMenu.find('.badge').length > 0) {
            $.post('/employee/view-notification', { id : $userId }, function() {
                notificationMenu.find(".badge").remove();
            });
        }
    });
});
JS
    );
} ?>
