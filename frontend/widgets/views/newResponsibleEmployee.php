<?php

use common\models\EmployeeCompany;
use kartik\select2\Select2;
use yii\bootstrap\Modal;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $form yii\widgets\ActiveForm */
/* @var $model common\models\EmployeeCompany */
/* @var $data array */
/* @var $modalId string */
/* @var $modalTitle string */
/* @var $modalText string */

?>

<?php Modal::begin([
    'id' => $modalId,
    'header' => Html::tag('h1', $modalTitle),
    'toggleButton' => false,
    'closeButton' => false,
]) ?>

    <div class="row">
        <div class="col-md-12 mb-3">
            <strong>
                <?= $modalText ?>
            </strong>
        </div>
        <div class="col-md-12">
            <?= $form->field($model, 'new_responsible_employee_id')->label('Ответственный')->widget(Select2::class, [
                'data' => $data,
                'hideSearch' => true,
                'options' => [
                    'prompt' => '',
                ],
                'pluginOptions' => [
                    'width' => '100%'
                ]
            ]); ?>
        </div>
    </div>

    <div class="mt-3 d-flex justify-content-between action-buttons">
        <?= Html::submitButton('Сохранить', [
            'class' => 'btn darkblue button-width ladda-button',
            'data-style' => 'expand-right',
        ]) ?>
        <?= Html::button('Отменить', [
            'class' => 'btn darkblue button-width',
            'data-dismiss' => 'modal',
        ]) ?>
    </div>

<?php Modal::end() ?>
