<?php

use yii\bootstrap\Modal;
use yii\helpers\Html;

/** @var $modalOptiions array */
/** @var $videoUrl string */

?>

<?php Modal::begin($modalOptiions); ?>

    <?=  Html::tag('iframe', '', [
        'src' => $videoUrl,
        'width' => '100%',
        'height' => '100%',
        'frameborder' => '0',
        'allow' => 'accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture',
        'allowfullscreen' => 'allowfullscreen',
    ]) ?>

<?php Modal::end();