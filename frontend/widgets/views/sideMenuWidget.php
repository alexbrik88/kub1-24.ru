<?php

use frontend\rbac\permissions;
use common\models\Company;
use philippfrenzel\yii2tooltipster\yii2tooltipster;

/** @var \yii\web\View $this */
/** @var array $items */
/** @var Company $newCompany */
/** @var integer $sideBarStatus */
/** @var  $companyTaxation common\models\company\CompanyTaxationType */

$status = $sideBarStatus ? 'page-sidebar-menu-closed' : '';

echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-side_menu',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-noir', 'tooltipster-noir-customized', 'tooltipster-side-menu-right'],
        'trigger' => 'click',
        'zIndex' => 10000,
        'position' => 'right',
    ],
]);
?>

<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <?= \common\widgets\SideMenuNav::widget([
            'items' => $items,
            'options' => [
                'class' => 'page-sidebar-menu page-sidebar-menu-light metismenu ' . $status,
                'id' => 'side-menu',
                'data' => [
                    'keep-expanded' => 'false',
                    'auto-scroll' => 'true',
                    'slide-speed' => '200',
                ],
                'style' => 'padding-top: 20px',
            ],
        ]) ?>
        <?= $this->render('modal/_create_company', [
            'newCompany' => $newCompany,
            'companyTaxation' => $companyTaxation,
        ]); ?>
        <?= $this->render('modal/menu_no_rules'); ?>
    </div>
</div>
<div class="tooltip_templates" style="display: none;">
    <span id="accountant_menu-tooltip"
          style="display: inline-block;">
        Данный блок только для<br>
        1) ИП на УСН Доходы,<br>
        2) ООО на ОСНО с нулевой отчетностью.
    </span>
</div>