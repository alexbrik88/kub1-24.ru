<?php

use common\components\helpers\Html;
use common\components\ImageHelper;
use common\models\service\SubscribeTariff;
use common\models\service\PaymentType;
use php_rutils\RUtils;
use yii\bootstrap\Modal;
use yii\helpers\Url;

/* @var \yii\web\View $this */
/* @var string $expireDate */
/* @var int $expireDays */
/* @var common\models\Company $company */

$route = [
    '/subscribe/default/create',
    'id' => $company->id,
    'key' => $company->subscribe_payment_key,
];
$url_1_invoice = Url::to(array_merge($route, ['tariff' => SubscribeTariff::TARIFF_1, 'type' => PaymentType::TYPE_INVOICE]));
$url_1_online = Url::to(array_merge($route, ['tariff' => SubscribeTariff::TARIFF_1, 'type' => PaymentType::TYPE_ONLINE]));
$url_2_invoice = Url::to(array_merge($route, ['tariff' => SubscribeTariff::TARIFF_2, 'type' => PaymentType::TYPE_INVOICE]));
$url_2_online = Url::to(array_merge($route, ['tariff' => SubscribeTariff::TARIFF_2, 'type' => PaymentType::TYPE_ONLINE]));
$url_3_invoice = Url::to(array_merge($route, ['tariff' => SubscribeTariff::TARIFF_3, 'type' => PaymentType::TYPE_INVOICE]));
$url_3_online = Url::to(array_merge($route, ['tariff' => SubscribeTariff::TARIFF_3, 'type' => PaymentType::TYPE_ONLINE]));

$variants = ['день', 'дня', 'дней'];

?>

<?php Modal::begin([
    'id' => 'service-reminder-trial',
    'header' => '<h1>НАПОМИНАНИЕ</h1>',
]); ?>

<div class="row text">
    Через <span class="days-left"><?= RUtils::numeral()->getPlural($expireDays, $variants) ?></span>
    <?php if ($company->isTrialNotPaid) : ?>
        ваш пробный период
    <?php else : ?>
        ваша подписка
    <?php endif?>
    закончится.
    <br>
    Для продолжения работы без ограничений,<br>
    сделайте оплату сегодня
</div>
<div class="col-md-12 tariff-preview-img">
    <div class="col-md-4">
        <?= ImageHelper::getThumb(Yii::getAlias('@frontend/web/img/reminder') . DIRECTORY_SEPARATOR . 'reminder_1_month.png', [
            245, 145,
        ]); ?>
    </div>
    <div class="col-md-4">
        <?= ImageHelper::getThumb(Yii::getAlias('@frontend/web/img/reminder') . DIRECTORY_SEPARATOR . 'reminder_4_month.png', [
            245, 145,
        ]); ?>
    </div>
    <div class="col-md-4">
        <?= ImageHelper::getThumb(Yii::getAlias('@frontend/web/img/reminder') . DIRECTORY_SEPARATOR . 'reminder_12_month.png', [
            245, 145,
        ]); ?>
    </div>
</div>
<div class="col-md-12 bill">
    <div class="col-md-4">
        <?= Html::a('Выставить СЧЕТ', $url_1_invoice, [
            'class' => 'btn darkblue',
            'title' => 'Для оплаты со счета вашей компании',
            'target' => '_blank',
        ]); ?>
    </div>
    <div class="col-md-4">
        <?= Html::a('Выставить СЧЕТ', $url_2_invoice, [
            'class' => 'btn darkblue',
            'title' => 'Для оплаты со счета вашей компании',
            'target' => '_blank',
        ]); ?>
    </div>
    <div class="col-md-4">
        <?= Html::a('Выставить СЧЕТ', $url_3_invoice, [
            'class' => 'btn darkblue',
            'title' => 'Для оплаты со счета вашей компании',
            'target' => '_blank',
        ]); ?>
    </div>
</div>
<div class="col-md-12 pay-card">
    <div class="col-md-4">
        <?= Html::a('Оплатить КАРТОЙ', $url_1_online, [
            'class' => 'btn',
            'title' => 'Онлайн платеж картой через систему электронных платежей Robokassa',
            'target' => '_blank',
        ]); ?>
    </div>
    <div class="col-md-4">
        <?= Html::a('Оплатить КАРТОЙ', $url_2_online, [
            'class' => 'btn',
            'title' => 'Онлайн платеж картой через систему электронных платежей Robokassa',
            'target' => '_blank',
        ]); ?>
    </div>
    <div class="col-md-4">
        <?= Html::a('Оплатить КАРТОЙ', $url_3_online, [
            'class' => 'btn',
            'title' => 'Онлайн платеж картой через систему электронных платежей Robokassa',
            'target' => '_blank',
        ]); ?>
    </div>
</div>

<?php Modal::end(); ?>
