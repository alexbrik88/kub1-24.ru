<?php

namespace frontend\widgets;

use common\components\helpers\Html;
use common\models\Company;
use common\models\service\SubscribeHelper;
use common\models\service\SubscribeStatus;
use common\models\service\SubscribeTariffGroup;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use Yii;

/**
 * Class CompanyLimitsWidget
 * @package frontend\widgets
 */
class CompanyLimitsWidget extends Widget
{
    /**
     * @var \common\models\Company
     */
    public $company;

    /**
     * @var boolean
     */
    public $mark = false;

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();

        if (!$this->company instanceof Company) {
            throw new InvalidConfigException('Company must be defined.');
        }
    }

    /**
     * Executes the widget.
     * @return string the result of widget execution to be outputted.
     */
    public function run()
    {
        return $this->render('companyLimitsWidget', [
            'data' => $this->getData(),
            'mark' => $this->mark,
        ]);
    }

    /**
     * @return array.
     */
    public function getData()
    {
        return [
            $this->getDaysData(),
            $this->getInvoiceData(),
            // $this->getCheksData(),
            $this->getPricelistData(),
            // $this->getRecognitionData(),
            // $this->getTransactionData(),
        ];
    }

    /**
     * @return array.
     */
    public function getInvoiceData()
    {
        $left = $this->company->getInvoiceLeft();

        if ($left < 6) {
            $this->mark = true;
        }

        $title = 'Осталось выставить счетов в месяц';
        if ($subscribeStd = $this->company->getActualSubscription(SubscribeTariffGroup::STANDART)) {
            if ($subscribeStd->getMonthsLeft() > 0) {
                $title .= '<br/>';
                $title .= 'Дата начисления: ';
                $title .= date('d.m.Y', $subscribeStd->getLimitNextTime());
            }
        }

        return [
            'label' => 'Счетов',
            'title' => $title,
            'left' => $this->company->getInvoiceLeft(),
            'icon' => 'list-1',
        ];
    }

    /**
     * @return array.
     */
    public function getDaysData()
    {
        $data = [
            'label' => 'Дней',
            'icon' => 'pay-calendar',
            'title' => 'Нет активных подписок',
            'left' => 0,
            'pending' => 0,
        ];

        $maxSubscribe = null;
        foreach ($this->company->activeSubscribes as $subscribe) {
            if ($subscribe->expired_at > ($maxSubscribe ? $maxSubscribe->expired_at : 0)) {
                $maxSubscribe = $subscribe;
            }
        }

        if ($maxSubscribe) {
            $name = [
                ArrayHelper::getValue($maxSubscribe, ['tariffGroup', 'name']),
                ArrayHelper::getValue($maxSubscribe, 'tariffName'),
            ];
            $data['title'] = Html::encode('Осталось дней до окончания "'.implode(' ', $name).'"');
            $data['left'] = SubscribeHelper::getExpireLeftDays($maxSubscribe->expired_at);

            $pendingSubscriptions = $this->company->getSubscribes()->andWhere([
                'tariff_group_id' => $maxSubscribe->tariff_group_id,
                'status_id' => SubscribeStatus::STATUS_PAYED,
            ])->all();


            foreach ($pendingSubscriptions as $subscribe) {
                $data['pending'] += ($subscribe->duration_month * 30 + $subscribe->duration_day);
            }
        }

        if (!empty($this->company->activeSubscribes) && $data['left'] < 6) {
            $this->mark = true;
        }

        return $data;
    }

    /**
     * @return array.
     */
    public function getCheksData()
    {
        $data = [
            'label' => 'Проверок',
            'icon' => 'find',
            'title' => 'Осталось проверок контрагентов',
            'left' => 0,
        ];

        if ($subscribe = $this->company->getActualSubscription(SubscribeTariffGroup::CHECK_CONTRACTOR)) {
            $data['left'] = $subscribe->getLimitLeft();
        }

        return  $data;
    }

    /**
     * @return array.
     */
    public function getPricelistData()
    {
        $data = [
            'label' => 'Прайс-листов',
            'icon' => 'price',
            'title' => 'Осталось созданий прайс-листов',
            'left' => 0,
        ];

        if ($subscribe = $this->company->getActualSubscription(SubscribeTariffGroup::PRICE_LIST)) {
            $data['left'] = $subscribe->getLimitLeft();
        }

        return  $data;
    }

    /**
     * @return array.
     */
    public function getRecognitionData()
    {
        $data = [
            'label' => 'Распознаваний',
            'icon' => 'recognize',
            'title' => 'Осталось распознаваний документов',
            'left' => 0,
        ];

        if ($subscribe = $this->company->getActualSubscription(SubscribeTariffGroup::DOCUMENT_RECOGNITION)) {
            $data['left'] = $subscribe->getLimitLeft();
        }

        return  $data;
    }

    /**
     * @return array.
     */
    public function getTransactionData()
    {
        $data = [
            'label' => 'Транзакций',
            'icon' => 'transactions',
            'title' => 'Осталось загрузить транзакций',
            'left' => 0,
        ];

        if ($subscribe = $this->company->getHasActualSubscription(0)) {
            $data['left'] = $subscribe->getLimitLeft();
        }

        return  $data;
    }
}
