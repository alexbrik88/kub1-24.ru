<?php

namespace frontend\widgets;

use yii\base\Widget;
use Yii;

class TableViewWidget extends Widget
{
    public $attribute;
    public $buttonClass;
    public $tableSelector;

    public $css = '';
    public $js = '
$(".change-table-view-mode").on("click", function(e) {
    var that = this;
    var url  = "/site/change-table-view-mode";
    var target = $(that).data("target") ||"";
    var table = target.length ? $(target) : $(".table.table-count-list").not(".compact-disallow");
    $.post(url, {attribute: $(that).data("attribute")}, function(data) {
        if (data.result) {
            table.toggleClass("table-compact");
            if (table.hasClass("table-compact")) {
                $(that).tooltipster("content", "Обычный вид");
                $(".tv-icon", that).attr("src", "/img/icons/tv_list2.png");
            } else {
                $(that).tooltipster("content", "Компактный вид");
                $(".tv-icon", that).attr("src", "/img/icons/tv_list3.png");
            }
        }
    })
});';

    /**
     * @return mixed
     */
    public function run()
    {
        $model = Yii::$app->user->identity ? Yii::$app->user->identity->config : null;

        if ($model !== null) {
            $this->view->registerCss($this->css);
            $this->view->registerJs($this->js);

            echo $this->render('tableViewWidget', [
                'model' => $model,
                'attribute' => $this->attribute,
                'buttonClass' => $this->buttonClass,
                'tableSelector' => $this->tableSelector,
            ]);
        }
    }
}
