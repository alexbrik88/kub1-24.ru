<?php

namespace frontend\widgets;

use common\models\Contractor;
use frontend\components\WebUser;
use yii\helpers\ArrayHelper;
use yii\helpers\Json;
use yii\helpers\Url;
use yii\web\JsExpression;

/**
 * ContractorDropdown widget
 */
class ContractorDropdown extends \kartik\select2\Select2
{
    /**
     * @var array|string
     */
    public $dataUrl = ['/dictionary/contractor-dropdown'];

    /**
     * @var Company
     */
    public $company;

    /**
     * Contractor::type
     * @var integer
     */
    public $contractorType;

    /**
     * Contractor::face_type
     * @var integer
     */
    public $contractorFaceType;

    /**
     * Items before found
     * @var array
     */
    public $staticData = [];

    /**
     * $this->staticData html data attributes
     * @var array
     */
    public $staticDataOptions = [];

    /**
     * @var array|string
     */
    public $excludeIds;

    /**
     * @var array|string
     */
    public $addRequestParams = [];

    /**
     * Html data attributes of `option` tags
     * Array $key => $value
     * Where
     *     $key - "data-$key" - <option> html attribute
     *     $value - Contractor::$value - Contractor model attribute
     *
     * @var array
     */
    public $dataAttributes = [
        'inn' => 'ITN',
        'kpp' => 'PPC',
        'verified' => 'verified',
        'face_type' => 'face_type',
        'industry' => 'industry_id',
        'sale_point' => 'sale_point_id',
    ];

    /**
     * @inheritDoc
     * @param WebUser $user
     * @param array $config
     */
    public function __construct(WebUser $user, array $config = [])
    {
        $this->company = $user->identity->company;

        parent::__construct($config);
    }

    public function init()
    {
        $contractor = null;
        $selectedText = null;
        $selectedOptions = [];
        if ($selectedId = $this->getSelectedId()) {
            if (is_numeric($selectedId) && ($contractor = $this->getContractor($selectedId))) {
                $selectedText = $contractor->nameWithType;
            } elseif (isset($this->staticData[$selectedId])) {
                $selectedText = $this->staticData[$selectedId];
            }
        }

        $staticDataString = '';
        if ($this->staticData) {
            $staticData = Json::encode($this->staticData);
            $staticDataString = "params.staticData = {$staticData};";
        }

        $optionDataString = '';
        if ($this->dataAttributes) {
            $optionDataString .= '$(data.element)';
            foreach ($this->dataAttributes as $key => $value) {
                $optionDataString .= ".attr('data-{$key}', data.{$value})";
            }
            $optionDataString .= ';';
            // defaults
            if ($contractor instanceof Contractor) {
                $dataAttributes = [];
                foreach ($this->dataAttributes as $key => $value) {
                    $dataAttributes["data-{$key}"] = $contractor->{$value} ?? null;
                }
                $dataAttributes["data-industry"] = ($this->contractorType === Contractor::TYPE_CUSTOMER)
                    ? $contractor->customer_industry_id : $contractor->seller_industry_id;
                $dataAttributes["data-sale_point"] = ($this->contractorType === Contractor::TYPE_CUSTOMER)
                    ? $contractor->customer_sale_point_id : $contractor->seller_sale_point_id;
                $this->options['options'][$contractor->id] = $dataAttributes;
            }
        }

        if (isset($selectedId, $selectedText)) {
            $this->data = (array) $this->data;
            $this->data[$selectedId] = $selectedText;
        }

        $staticKeys = Json::encode(array_keys($this->staticData));

        $excludeCashbox = "$('#cashorderflows-cashbox_id').length ? $('#cashorderflows-cashbox_id').val() : null";
        $excludeIds = implode(',', (array) $this->excludeIds);

        $addRequestParams = '';
        foreach ($this->addRequestParams as $key => $value) {
            $addRequestParams .= sprintf('params["%s"] = "%s";', $key, $value).PHP_EOL;
        }

        $pluginOptions = [
            'allowClear' => true,
            'minimumInputLength' => 0,
            'ajax' => [
                'url' => Url::to($this->dataUrl),
                'dataType' => 'json',
                'delay' => 250,
                'data' => new JsExpression(<<<JS
                    function(params) {
                        params.type = '{$this->contractorType}';
                        params.faceType = '{$this->contractorFaceType}';
                        params.excludeCashbox = {$excludeCashbox};
                        params.excludeIds = "{$excludeIds}";
                        {$staticDataString};
                        {$addRequestParams}

                        return params;
                    }
JS
                ),
            ],
            'escapeMarkup' => new JsExpression(<<<JS
                function(text) {
                    return text;
                    let escape = document.createElement('textarea');
                    escape.textContent = text;
                    return escape.innerHTML;
                }
JS
            ),
            'templateSelection' => new JsExpression(<<<JS
                function(data, container) { {$optionDataString} return data.text; }
JS
            ),
            'templateResult' => new JsExpression(<<<JS
                function(data, container) {
                    const max_name_length = 30;
                    if (data.showPPC) {
                        $(container).attr('title', data.showPPC);
                        $(container).addClass('opt-tooltip-4');
                        $(container).attr('data-placement', 'right');
                        $(container).parents('.select2-dropdown').css('overflow', 'visible');
                        if (data.text.length > max_name_length) {
                            container.innerHTML = data.text;
                        } else {
                            container.innerHTML =
                                '<div class="opt-contractor">' +
                                '<div class="name">' + data.text + '</div>' +
                                '<div class="kpp">' + data.showPPC.substr(0, 8) + '</div>' +
                                '</div>';
                        }
                    } else {
                        container.innerHTML = data.text;
                    }

                    return container;
                }
JS
            ),
            'width' => '100%'
        ];

        $this->pluginOptions = ArrayHelper::merge($pluginOptions, $this->pluginOptions);

        parent::init();
    }

    /**
     * @return array
     */
    protected function getSelectedId()
    {
        if ($this->hasModel()) {
            $contractorId = $this->model->{$this->attribute};
        } else {
            $contractorId = $this->value;
        }

        return $contractorId;
    }

    /**
     * @return Contractor
     */
    protected function getContractor($id)
    {
        return Contractor::find()->where([
            'id' => $id,
        ])->andWhere([
            'or',
            ['company_id' => null],
            ['company_id' => $this->company->id],
        ])->one();
    }
}
