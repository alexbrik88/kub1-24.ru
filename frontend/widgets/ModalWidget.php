<?php

namespace frontend\widgets;

use common\components\helpers\ArrayHelper;
use yii\bootstrap\Modal;
use yii\bootstrap\Widget;
use yii\helpers\Html;

/**
 * Class ConfirmModalWidget
 * @package frontend\widgets
 *
 * @example usage:
 *  echo \frontend\widgets\ModalWidget::widget([
 *      'toggleButton' => [
 *      'label' => 'Удалить',
 *      'class' => 'btn darkblue',
 *  ],
 * 'message' => 'Вы не можете удалить этого контрагента, потому что у него имеются активные счета!',
 * 'options' => [
 * 'textCssClass' => 'red-alert',
 * ],
 * ]);
 *
 * button:
 * <button type="button" class="btn darkblue" data-toggle="modal" href="#delete-confirm">Удалить</button> *
 */
class ModalWidget extends Widget{
    /**
     * @var string
     */
    public $message = 'Are you sure?';
    /**
     * @var array
     */
    public $options;
    /**
     * @var
     */
    public $toggleButton;

    public function init()
    {
        Html::addCssClass($this->options, 'fade');

        Modal::begin([
            'options' => $this->options,
            'closeButton' => false,
            'toggleButton' => $this->toggleButton !== null ? $this->toggleButton : [
                'label' => 'Confirm'
            ],
        ]); ?>
        <span class="<?= ArrayHelper::getValue($this->options, 'textCssClass'); ?>"><?= $this->message; ?></span>
        <?php Modal::end();
    }
}