<?php

namespace frontend\widgets;


use common\components\date\DateHelper;
use common\models\product\PriceListNotification;
use Yii;
use yii\bootstrap\Modal;
use yii\bootstrap\Widget;
use yii\helpers\Html;

/**
 * Class PriceListNotificationsWidget
 * @package frontend\widgets
 */
class PriceListNotificationsWidget extends Widget
{
    /**
     * @var
     */
    public $options;

    /**
     * @return string
     */
    public function run()
    {
        if (!Yii::$app->user->isGuest) {

            /* @var $company \common\models\Company */
            $company = Yii::$app->user->identity->company;

            if ($company->has_price_list_notifications) {

                /** @var PriceListNotification $notification */
                $notifications = PriceListNotification::find()->where([
                    'company_id' => $company->id,
                    'is_viewed' => 1,
                    'is_notification_viewed' => 0
                ])->all();

                if ($notifications) {

                    $modalsCount = 0;

                    foreach ($notifications as $notification) {

                        if ($notification->priceList->use_notifications) {

                            echo $this->render('priceListNotificationsWidget', [
                                'notification' => $notification
                            ]);

                            $modalsCount++;

                        } else {

                            $notification->updateAttributes([
                                'is_notification_viewed' => 1,
                                'notification_viewed_at' => time()
                            ]);
                        }

                    }

                    if ($modalsCount) {

                        $this->view->registerJs('
                            $(document).ready(function () {
                                $(".modal-price-list-notification").modal();                            
                            });
                            $(document).on("hide.bs.modal", ".modal-price-list-notification", function() {
                                $.get("/price-list/notification-viewed", {"id": $(this).attr("data-notification-id")}, function(data) {});
                            });
                        ');

                    } else {

                        $company->updateAttributes([
                            'has_price_list_notifications' => 0
                        ]);
                    }
                }
            }
        }

        return null;
    }
}
