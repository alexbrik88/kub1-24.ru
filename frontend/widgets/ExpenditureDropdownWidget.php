<?php
/**
 * Created by PhpStorm.
 * User: valik
 * Date: 27.01.2016
 * Time: 17:02
 */

namespace frontend\widgets;

use frontend\rbac\UserRole;
use Yii;
use common\models\document\InvoiceExpenditureItem;
use common\models\document\InvoiceIncomeItem;
use frontend\assets\ExpenditureDropdownWidgetAsset;
use kartik\select2\Select2;
use yii\helpers\Url;
use yii\web\JsExpression;
use yii\web\View;

class ExpenditureDropdownWidget extends Select2
{
    public $company;
    public $income = false;
    public $exclude = [];
    public $loadAssets = true;

    /**
     * @inheritdoc
     */
    public function run()
    {
        if ($this->loadAssets)
            ExpenditureDropdownWidgetAsset::register($this->getView());

        $this->processData();
        parent::run();
        echo $this->render('ExpenditureDropdownWidget', [
            'income' => $this->income,
            'inputId' => $this->options['id'],
        ]);
    }

    /**
     * Registers script for widget.
     */
    public function processData()
    {
        if ($this->income) {
            $expenditureItems = InvoiceIncomeItem::getSelect2Data(
                Yii::$app->user->identity->company->id,
                $this->exclude,
                Yii::$app->user->can(UserRole::ROLE_CHIEF)
            );
            $this->options['data-delurl'] = Url::to(['/documents/default/income-delete']);
            $this->options['data-editurl'] = Url::to(['/documents/default/income-edit']);
        } else {
            $expenditureItems = InvoiceExpenditureItem::getSelect2Data(
                Yii::$app->user->identity->company->id,
                $this->exclude,
                Yii::$app->user->can(UserRole::ROLE_CHIEF)
            );
            $this->options['data-delurl'] = Url::to(['/documents/default/expenditure-delete']);
            $this->options['data-editurl'] = Url::to(['/documents/default/expenditure-edit']);
        }
        $this->data = $expenditureItems['list'];
        $this->options['options'] = $expenditureItems['options'];
        $this->pluginOptions['templateResult'] = new JsExpression('expenditureDropdownItemsTemplate');
        $this->pluginOptions['templateSelection'] = new JsExpression('expenditureDropdownItemsTemplateSelection');
        $this->pluginOptions['containerCssClass'] = 'select2-cash-article-container';
        $this->pluginOptions['dropdownCssClass'] = 'select2-cash-article-dropdown';

        $js = <<<JS
        $(document).on("select2:selecting", "#{$this->options['id']}", function(e) {
            var target = e.params.args.originalEvent.target;
            if ($(target).closest("li.select2-results__option").hasClass("editable-expenditure-item")) {
                e.preventDefault();
                if (target.classList.contains("edit-exp-item-cancel")) {
                    editExpenditureItemCancel();
                }
                if (target.classList.contains("edit-exp-item-apply")) {
                    editExpenditureItemApply(this, target);
                }
            }
            if (target.classList.contains("del-exp-item")) {
                e.preventDefault();
                dleteExpenditureItem(this, target);
            }
            if (target.classList.contains("edit-exp-item")) {
                e.preventDefault();
                editExpenditureItem(this, target);
            }
        });
JS;

        $view = $this->getView();
        $view->registerJs($js, View::POS_HEAD);
    }
}
