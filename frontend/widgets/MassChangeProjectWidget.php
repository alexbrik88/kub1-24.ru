<?php

namespace frontend\widgets;


use common\models\project\Project;
use Yii;
use yii\base\Widget;
use yii\helpers\ArrayHelper;

class MassChangeProjectWidget extends Widget
{
    /**
     * @var string the value of the "id" attribute of the change project modal
     */
    public $modalId = 'mass_change_project';

    /**
     * @var string
     */
    public $modalTitle;

    /**
     * @var string
     */
    public $formAction;

    /**
     * @var string
     */
    public $formTitle;

    /**
     * @var string the value of the "name" attribute of the change project input
     */
    public $inputName = 'project_id';

    /**
     * @var string selected items input "name"
     */
    public $itemsInputName = 'selected';

    /**
     * @var array the options for rendering the toggle button tag.
     *
     * The following special options are supported:
     *
     * - tag: string, the tag name of the button. Defaults to 'button'.
     * - label: string, the label of the button. Defaults to 'Show'.
     *
     * The rest of the options will be rendered as the HTML attributes of the button tag.
     */
    public $toggleButton;

    /**
     * @inheritdoc
     */
    public function run()
    {
        $this->registerJs();

        if (Yii::$app->user->can(\frontend\rbac\UserRole::ROLE_CHIEF)) {

            $toggleButton = $this->toggleButton === false ? false : [
                'tag' => 'span',
                'label' => 'Поменять направление',
            ];
            if (is_array($this->toggleButton)) {
                $toggleButton = array_merge($toggleButton, $this->toggleButton);
            }

            return $this->render('changeProjectWidget', [
                'modalId' => $this->modalId,
                'modalTitle' => $this->modalTitle,
                'formAction' => $this->formAction,
                'formTitle' => $this->formTitle,
                'inputName' => $this->inputName,
                'inputValue' => null,
                'toggleButton' => $toggleButton,
                'data' => Project::getSelect2Data(),
            ]);
        }

        return '';
    }

    /**
     * @inheritdoc
     */
    public function registerJs()
    {
        $script = <<<JS
            $(document).on("show.bs.modal", "#{$this->modalId}", function () {
                $(".joint-operation-checkbox:checked").each(function (i) {
                    let itemId = $(this).data("invoice-id");
                    $("<input>").attr({
                        type: "hidden",
                        id: "{$this->itemsInputName}"+itemId,
                        name: "{$this->itemsInputName}[]",
                        value: itemId,
                    }).appendTo("#{$this->modalId}_form .additional_inputs");
                });
            });
            $(document).on("hidden.bs.modal", "#{$this->modalId}", function () {
                $('#{$this->modalId}_form .additional_inputs').html('');
                $('#{$this->modalId}_form select[name={$this->inputName}]').val('').trigger('change');
            });
        JS;

        $this->view->registerJs($script);
    }
}
