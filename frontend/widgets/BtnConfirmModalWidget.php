<?php

namespace frontend\widgets;

use Yii;
use yii\bootstrap\Modal;
use yii\bootstrap\Widget;
use yii\helpers\Html;

class BtnConfirmModalWidget extends ConfirmModalWidget
{
    /**
     * @var integer
     */
    public $modelId;

    /**
     * @var string|null
     */
    public $markAsDeleteClass;

    /**
     *
     */
    public function init()
    {
        Html::addCssClass($this->options, 'confirm-modal');
        Html::addCssClass($this->options, 'fade');

        Modal::begin([
            'id' => sha1($this->confirmUrl . serialize($this->confirmParams)),
            'options' => $this->options,
            'closeButton' => false,
            'toggleButton' => $this->toggleButton !== null ? $this->toggleButton : [
                'label' => 'Confirm'
            ],
        ]); ?>
        <div class="form-body">
            <div class="row"><?= $this->message; ?></div>
        </div>
        <div class="form-actions row">
            <div class="col-md-6">
                <?= Html::button('ДА', [
                    'class' => 'btn darkblue  pull-right ' . ($this->markAsDeleteClass ?? 'btn-confirm-yes'),
                    'data' => [
                        'url' => $this->confirmUrl,
                        'type' => 'post',
                        'dismiss' => 'modal',
                        'params' => array_merge($this->confirmParams, [
                            '_csrf' => Yii::$app->request->csrfToken,
                        ]),
                        'model_id' => $this->modelId,
                    ],
                ]); ?>
            </div>
            <div class="col-md-6">
                <button type="button" class="btn darkblue" data-dismiss="modal">НЕТ</button>
            </div>
        </div>
        <?php Modal::end();
    }
}
