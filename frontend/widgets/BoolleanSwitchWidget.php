<?php

namespace frontend\widgets;

use philippfrenzel\yii2tooltipster\yii2tooltipsterAsset;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\web\JsExpression;
use yii\web\View;

class BoolleanSwitchWidget extends Widget
{
    public $label;
    public $labelOptions = ['class' => 'mar-b-5 text-bold text-center'];
    public $inputName;
    public $inputValue;
    public $action = '';
    public $method = 'post';
    public $options = ['style' => 'width: 100px;'];
    public $formOptions = [];
    public $mainCssClass = 'update-attribute-tooltip';
    public $toggleButton = [
        'tag' => 'span',
        'label' => '<i class="fa fa-pencil" aria-hidden="true"></i> Изменить',
        'class' => 'color-link cursor-pointer',
    ];
    public $buttonBoxOptions = [
        'class' => 'btn-group',
        'style' => 'border: 1px solid #ddd;',
    ];
    public $buttonOptions = [
        'class' => 'btn',
        'style' => 'width: 49px;',
    ];
    public $trueValue = 1;
    public $falseValue = 0;
    public $trueLabel = 'Да';
    public $falseLabel = 'Нет';
    public $closeByFalseLabel = false;
    public $trueCssClass;
    public $falseCssClass;
    public $activeCssClass = 'active';
    public $addFalseForm = true;

    private $trueId;
    private $falseId;

    public $activeStyle;
    public $inactiveStyle;

    /**
     * Initializes the widget.
     */
    public function init()
    {
        parent::init();

        if (!isset($this->options['id'])) {
            $this->options['id'] = $this->getId();
        }

        $this->trueId = $this->options['id'] . '-true';
        $this->falseId = $this->options['id'] . '-false';

        Html::addCssClass($this->options, $this->mainCssClass . '-content');
        Html::addCssClass($this->formOptions, $this->mainCssClass . '-form');
    }


    /**
     * Executes the widget.
     * @return string the result of widget execution to be outputted.
     */
    public function run()
    {
        echo $this->renderToggleButton();

        echo $this->renderContent();

        $this->registerPlugin();
    }

    /**
     * Renders the toggle button.
     * @return string
     */
    protected function renderToggleButton()
    {
        if (($toggleButton = $this->toggleButton) !== false) {
            $tag = ArrayHelper::remove($toggleButton, 'tag', 'button');
            $label = ArrayHelper::remove($toggleButton, 'label', 'Edit');
            if ($tag === 'button' && !isset($toggleButton['type'])) {
                $toggleButton['type'] = 'button';
            }
            Html::addCssClass($toggleButton, $this->mainCssClass);
            $toggleButton['data']['tooltip-content'] = '#' . $this->options['id'];

            return Html::tag($tag, $label, $toggleButton);
        } else {
            return null;
        }
    }

    /**
     * Renders the widget content.
     * @return string
     */
    protected function renderContent()
    {
        $html = $this->renderLabel();
        $html .= $this->renderTrueForm();
        if ($this->addFalseForm) {
            $html .= $this->renderFalseForm();
        }
        $html .= $this->renderButtons();

        return Html::tag('div', Html::tag('div', $html, $this->options), [
            'class' => 'hidden'
        ]);
    }

    /**
     * @return string
     */
    protected function renderLabel()
    {
        return $this->label ? Html::tag('div', $this->label, $this->labelOptions) : null;
    }

    /**
     * @return string
     */
    protected function renderTrueForm()
    {
        $html = Html::beginForm($this->action, $this->method, array_merge($this->formOptions, [
            'id' => $this->trueId,
        ]));
        $html .= Html::hiddenInput($this->inputName, $this->trueValue);
        $html .= Html::endForm();

        return $html;
    }

    /**
     * @return string
     */
    protected function renderFalseForm()
    {
        $html = Html::beginForm($this->action, $this->method, array_merge($this->formOptions, [
            'id' => $this->falseId,
        ]));
        $html .= Html::hiddenInput($this->inputName, $this->falseValue);
        $html .= Html::endForm();

        return $html;
    }

    /**
     * @return string
     */
    protected function renderButtons()
    {
        $trueOptions = $this->buttonOptions;
        $falseOptions = $this->buttonOptions;
        $trueOptions['form'] = $this->trueId;
        $falseOptions['form'] = $this->falseId;
        if ($this->trueCssClass) {
            Html::addCssClass($trueOptions, $this->trueCssClass);
        }
        if ($this->falseCssClass) {
            Html::addCssClass($falseOptions, $this->falseCssClass);
        }
        if ($this->inputValue == $this->trueValue) {
            Html::addCssClass($trueOptions, $this->activeCssClass);
        }
        if ($this->inputValue == $this->falseValue) {
            Html::addCssClass($falseOptions, $this->activeCssClass);
        }
        if ($this->closeByFalseLabel) {
            $falseOptions['onclick'] =
                new JsExpression('
                    var instances = $.tooltipster.instances();
                    $.each(instances, function(i, instance){
                        instance.close();
                    });
                ');
        }
        $html = Html::submitButton($this->trueLabel, $trueOptions);
        $html .= Html::submitButton($this->falseLabel, $falseOptions);

        return Html::tag('div', $html, $this->buttonBoxOptions);
    }

    /**
     * @return
     */
    protected function registerPlugin()
    {
        //get the displayed view and register the needed assets
        $view = $this->getView();
        yii2tooltipsterAsset::register($view);

        $js = [];

        $options = Json::encode([
            'theme' => ['tooltipster-kub', 'tooltipster-switch-widget'],
            'trigger' => 'click',
            'contentAsHTML' => true,
            'interactive' => true,
        ]);
        $js[] = "$('.{$this->mainCssClass}').tooltipster($options);";

        $view->registerJs(implode("\n", $js), View::POS_READY);
    }
}
