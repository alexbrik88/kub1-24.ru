<?php

namespace frontend\widgets;


use Yii;
use yii\base\Widget;
use yii\helpers\ArrayHelper;

class ChangeResponsibleWidget extends Widget
{
    /**
     * @var string the prefix to the automatically generated widget IDs.
     * @see getId()
     */
    public static $autoIdPrefix = 'change_responsible_';

    /**
     * @var string the value of the "id" attribute of the change responsible modal
     */
    public $modalId;

    /**
     * @var string
     */
    public $modalTitle;

    /**
     * @var string
     */
    public $formAction;

    /**
     * @var string
     */
    public $formTitle;

    /**
     * @var string|array the value of the "action" attribute of the change responsible form
     */
    public $action;

    /**
     * @var string the value of the "name" attribute of the change responsible input
     */
    public $inputName = 'responsible_employee_id';

    /**
     * @var string|integer the value of the "value" attribute of the change responsible input
     */
    public $inputValue;

    /**
     * @var array the options for rendering the toggle button tag.
     *
     * The following special options are supported:
     *
     * - tag: string, the tag name of the button. Defaults to 'button'.
     * - label: string, the label of the button. Defaults to 'Show'.
     *
     * The rest of the options will be rendered as the HTML attributes of the button tag.
     */
    public $toggleButton;

    /**
     * @inheritdoc
     */
    public function run()
    {
        if (Yii::$app->user->can(\frontend\rbac\UserRole::ROLE_CHIEF)) {
            $employeeCompanyArray = Yii::$app->user->identity->company->getEmployeeCompanies()->andWhere([
                'is_working' => true,
            ])->orderBy([
                'lastname' => SORT_ASC,
                'firstname' => SORT_ASC,
                'patronymic' => SORT_ASC,
            ])->all();

            $data = ArrayHelper::map($employeeCompanyArray, 'employee_id', 'fio');

            $toggleButton = $this->toggleButton === false ? false : [
                'tag' => 'span',
                'label' => 'Поменять ответственного',
            ];
            if (is_array($this->toggleButton)) {
                $toggleButton = array_merge($toggleButton, $this->toggleButton);
            }

            return $this->render('changeResponsibleWidget', [
                'modalId' => $this->modalId ?: $this->id,
                'modalTitle' => $this->modalTitle,
                'formAction' => $this->formAction,
                'formTitle' => $this->formTitle,
                'inputName' => $this->inputName,
                'inputValue' => $this->inputValue,
                'toggleButton' => $toggleButton,
                'data' => $data,
            ]);
        }
    }
}
