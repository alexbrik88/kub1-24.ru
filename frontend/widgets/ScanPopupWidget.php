<?php

namespace frontend\widgets;
use common\models\employee\Employee;
use yii\base\Widget;

class ScanPopupWidget extends Widget
{
    public function run()
    {
        if (!\Yii::$app->session->get(Employee::SHOW_SCAN_POPUP_SESSION_KEY)) {
            return false;
        }
        return $this->render('scan_popup');
    }
}