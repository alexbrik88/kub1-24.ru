<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 05.09.2018
 * Time: 16:27
 */

namespace frontend\widgets;


use common\components\helpers\Html;
use yii\bootstrap\Dropdown;
use yii\helpers\Url;
use yii\base\Widget;
use Yii;

/**
 * Class CashBoxFilterWidget
 * @package frontend\widgets
 */
class CashBoxFilterWidget extends Widget
{
    /**
     * @var
     */
    public $pageTitle;
    /**
     * @var
     */
    public $cashbox;
    /**
     * @var \common\models\Company
     */
    public $company;

    /**
     *
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @return null|string
     * @throws \Exception
     */
    public function run()
    {
        Yii::$app->view->registerJs('
            $(document).on("submit", "#cashbox-form", function () {
                $this = $(this);
                $modal = $this.closest(".modal");
                $(this).append("<input type=\"hidden\" name=\"cashbox\" value=\"' . $this->cashbox . '\">");
                $action = $this.data("isnewrecord") == 1 ? "/cash/order/create-cashbox" : "/cash/order/update-cashbox?id=" + $this.data("modelid");
                $.post($action, $(this).serialize(), function (data) {
                    if (data.result == true) {
                        $modal.modal("hide");
                        $("ul#user-bank-dropdown").replaceWith(data.html);
                        if (data.label) {
                            $("span.cashbox-label").text(data.label + " ");
                        }
                        Ladda.stopAll();
                    } else {
                        $this.html($(data.html).find("form").html());
                        $("#cashbox-form input:checkbox:not(.md-check)").uniform();
                        $("#cashbox-startbalancedate").datepicker({
                            language: "ru",
                            autoclose: true,
                        }).on("change.dp", dateChanged);
                        
                        function dateChanged(ev) {
                            if (ev.bubbles == undefined) {
                                var $input = $("#" + ev.currentTarget.id);
                                if (ev.currentTarget.value == "") {
                                    if ($input.data("last-value") == null) {
                                        $input.data("last-value", ev.currentTarget.defaultValue);
                                    }
                                    var $lastDate = $input.data("last-value");
                                    $input.datepicker("setDate", $lastDate);
                                } else {
                                    $input.data("last-value", ev.currentTarget.value);
                                }
                            }
                        }
                        
                        $("#cashbox-createstartbalance").on("change", function (e) {
                            $(".start-balance-block").toggleClass("hidden");
                        });
                    }
                });
                
                return false;
            });
        ');

        return $this->getCheckboxesList();
    }

    /**
     * @return string
     * @throws \Exception
     */
    private function getCheckboxesList()
    {
        /* @var $user \common\models\employee\Employee */
        $user = Yii::$app->user->identity;
        $cashboxList = $user->getCashboxes()
            ->select(['name', 'id'])
            ->orderBy([
                'is_main' => SORT_DESC,
                'name' => SORT_ASC,
            ])->indexBy('id')->column();
        $cashboxItems = [];
        if ($cashboxList) {
            if (count($cashboxList) > 1) {
                $cashboxList = $cashboxList + ['all' => 'Все кассы'];
            }
            foreach ($cashboxList as $key => $name) {
                $cashboxItems[] = [
                    'label' => '<span class="item-label">' . $name . '</span>' .
                        ($key !== 'all' ? '<span class="glyphicon glyphicon-pencil update-cashbox ajax-modal-btn" 
                        title="Обновить" aria-label="Обновить" 
                        data-url="' . Url::to(['/cashbox/update', 'id' => $key]) . '"></span>' : null),
                    'url' => Url::to(['index', 'cashbox' => $key]),
                    'linkOptions' => [
                        'class' => $this->cashbox == $key ? 'active' : '',
                    ],
                ];
            }
        }
        $currentName = isset($cashboxList[$this->cashbox]) ? $cashboxList[$this->cashbox] : $this->pageTitle;

        $cashboxItems[] = [
            'label' => '[ + ДОБАВИТЬ КАССУ ]',
            'options' => [
                'class' => 'add-cashbox ajax-modal-btn',
                'data-title' => 'Добавить кассу',
                'data-url' => Url::to(['/cashbox/create']),
            ],
        ];

        if ($cashboxItems) {
            if (count($cashboxItems) > 1) {
                $list = '<div style="display: inline-block;"><div class="dropdown" style="border-bottom: 1px dashed #666;">' .
                    Html::a('<span class="cashbox-label">' . Html::encode($currentName) . ' </span><b class="caret"></b>', '#', [
                        'data-toggle' => 'dropdown',
                        'class' => 'dropdown-toggle',
                        'style' => 'text-decoration: none;text-transform: uppercase;font-weight: bold;font-size: 19px;line-height: 20px;color: #555;',
                    ]) .
                    Dropdown::widget([
                        'id' => 'user-bank-dropdown',
                        'encodeLabels' => false,
                        'items' => $cashboxItems,
                    ]) .
                    '</div></div>';

                return $list;
            }
        }

        return Html::encode($this->pageTitle);
    }
}