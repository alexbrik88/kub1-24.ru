<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 18.09.2018
 * Time: 12:40
 */

namespace frontend\widgets;


use common\components\helpers\Html;
use common\models\product\Product;
use common\models\product\ProductSearch;
use yii\base\Widget;
use Yii;
use yii\bootstrap\Dropdown;
use yii\helpers\Url;

/**
 * Class StoreFilterWidget
 * @package frontend\widgets
 */
class StoreFilterWidget extends Widget
{
    /**
     * @var
     */
    public $pageTitle;
    /**
     * @var
     */
    public $store;
    /**
     * @var
     */
    public $productionType;
    /**
     * @var
     */
    public $priceList = null;
    /**
     * @var
     */
    public $help;
    /**
     * @var \common\models\Company
     */
    public $company;

    /**
     * @return string
     * @throws \Exception
     */
    public function run()
    {
        $this->priceList = Yii::$app->request->get('priceList');

        Yii::$app->view->registerJs('
            $(document).on("submit", "#store-form", function () {
                $this = $(this);
                $modal = $this.closest(".modal");
                $(this).append("<input type=\"hidden\" name=\"store\" value=\"' . $this->store . '\">");
                $action = $this.data("isnewrecord") == 1 ? "/product/create-store?productionType=' . $this->productionType . '&priceList=' . $this->priceList . '" : "/product/update-store?id=" + $this.data("modelid") + "&productionType=' . $this->productionType . '&priceList=' . $this->priceList . '";
                $.post($action, $(this).serialize(), function (data) {
                    if (data.result == true) {
                        $modal.modal("hide");
                        $("ul#user-bank-dropdown").replaceWith(data.html);
                        if (data.label) {
                            $("span.store-label").text(data.label + " ");
                        }
                        Ladda.stopAll();
                    } else {
                        $this.html($(data.html).find("form").html());
                    }
                });

                return false;
            });
        ');

        return $this->getStoreList();
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getStoreList()
    {
        /* @var $user \common\models\employee\Employee */
        $user = Yii::$app->user->identity;
        $storeSelectList = $user->getStores()
            ->select('name')
            ->orderBy(['is_main' => SORT_DESC])
            ->indexBy('id')
            ->column();
        $hasArchive = $this->company->getProducts()->andWhere([
            'production_type' => $this->productionType,
            'status' => Product::ARCHIVE,
        ])->exists();
        $storeItems = [];
        if ($storeSelectList) {
            $this->pageTitle = 'Склад';
            if (count($storeSelectList) > 1) {
                $storeSelectList += ['all' => 'Все склады'];
            }
        }
        if ($hasArchive) {
            if (count($storeSelectList) === 0) {
                $storeSelectList[''] = $this->pageTitle;
            }
            $storeSelectList += ['archive' => 'Архив'];
        }
        foreach ($storeSelectList as $key => $name) {
            $storeItems[] = [
                'label' => $name .
                    (!in_array($key, ['all', 'archive']) ? '<span class="glyphicon glyphicon-pencil update-store ajax-modal-btn"
                        title="Обновить" aria-label="Обновить"
                        data-url="' . Url::to(['/store/update', 'id' => $key]) . '"></span>' : null),
                'url' => [
                    $this->priceList ? 'add-to-price-list' : 'index',
                    'productionType' => $this->productionType,
                    'store' => $key ? : null,
                    'priceList' => $this->priceList,
                ],
                'linkOptions' => [
                    'class' => $this->store == $key ? 'active' : '',
                ],
            ];
        }
        $currentName = isset($storeSelectList[$this->store]) ? $storeSelectList[$this->store] : $this->pageTitle;

        $storeItems[] = [
            'label' => '[ + ДОБАВИТЬ СКЛАД ]',
            'options' => [
                'class' => 'add-store ajax-modal-btn',
                'data-title' => 'Добавить склад',
                'data-url' => Url::to(['/store/create']),
            ],
        ];

        if ($storeItems) {
            if (count($storeItems) > 1) {
                $list = '<div style="display: inline-block;"><div class="dropdown" style="border-bottom: 1px dashed #666;">' .
                    Html::a('<span class="store-label">' . Html::encode($currentName) . ' </span><b class="caret"></b>', '#', [
                        'data-toggle' => 'dropdown',
                        'class' => 'dropdown-toggle',
                        'style' => 'text-decoration: none; color: #666;'
                    ]) . Dropdown::widget([
                        'id' => 'user-bank-dropdown',
                        'encodeLabels' => false,
                        'items' => $storeItems,
                    ]) .
                    '</div></div>';

                return $list;
            }
        }

        return Html::encode($currentName) . $this->help;
    }
}