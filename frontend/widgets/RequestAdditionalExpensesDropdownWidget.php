<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 05.12.2018
 * Time: 14:41
 */

namespace frontend\widgets;


use common\components\helpers\ArrayHelper;
use common\models\logisticsRequest\LogisticsRequestAdditionalExpenses;
use frontend\assets\RequestAdditionalExpensesDropdownWidgetAsset;
use kartik\select2\Select2;
use yii\helpers\Url;
use Yii;
use yii\web\JsExpression;
use yii\web\View;

class RequestAdditionalExpensesDropdownWidget extends Select2
{
    public $company;
    public $income = false;

    /**
     * @return string|void
     * @throws \ReflectionException
     * @throws \yii\base\InvalidConfigException
     */
    public function run()
    {
        RequestAdditionalExpensesDropdownWidgetAsset::register($this->getView());
        $this->processData();
        parent::run();
        echo $this->render('requestAdditionalExpensesDropdownWidget', [
            'income' => $this->income,
            'inputId' => $this->options['id'],
        ]);
    }

    /**
     * Registers script for widget.
     */
    public function processData()
    {
        $items = LogisticsRequestAdditionalExpenses::getSelect2Data(Yii::$app->user->identity->company->id);
        $this->options['data-delurl'] = Url::to(['/logistics/request/expenses-delete']);
        $this->options['data-editurl'] = Url::to(['/logistics/request/expenses-edit']);

        $this->data = ArrayHelper::map($items['list'], 'id', 'name');
        $this->options['options'] = $items['options'];
        $this->pluginOptions['templateResult'] = new JsExpression('expenditureDropdownItemsTemplate');

        $js = <<<JS
        $(document).on("select2:selecting", "#{$this->options['id']}", function(e) {
            var target = e.params.args.originalEvent.target;
            if ($(target).closest("li.select2-results__option").hasClass("editable-expenditure-item")) {
                e.preventDefault();
                if (target.classList.contains("edit-exp-item-cancel")) {
                    editExpenditureItemCancel();
                }
                if (target.classList.contains("edit-exp-item-apply")) {
                    editExpenditureItemApply(this, target);
                }
            }
            if (target.classList.contains("del-exp-item")) {
                e.preventDefault();
                dleteExpenditureItem(this, target);
            }
            if (target.classList.contains("edit-exp-item")) {
                e.preventDefault();
                editExpenditureItem(this, target);
            }
        });
JS;

        $view = $this->getView();
        $view->registerJs($js, View::POS_HEAD);
    }
}