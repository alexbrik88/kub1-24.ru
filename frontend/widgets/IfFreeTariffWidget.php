<?php

namespace frontend\widgets;

use Yii;
use yii\base\Widget;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\web\View;

/**
 * Class IfFreeTariffWidget
 * @package frontend\widgets

 */
class IfFreeTariffWidget extends Widget
{
    /**
     * @var string
     */
    public $modalId;

    /**
     * @var array
     */
    public $modalOptions = [];

    /**
     * @var string
     */
    public $linkSelector;

    /**
     * @var string
     */
    public $text;

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();

        if (empty($this->modalId) || !is_string($this->modalId)) {
            throw new InvalidConfigException('Invalid parameter "modalId".');
        }
        if (empty($this->linkSelector) || !is_string($this->linkSelector)) {
            throw new InvalidConfigException('Invalid parameter "linkSelector".');
        }
        if (empty($this->text) || !is_string($this->text)) {
            throw new InvalidConfigException('Invalid parameter "text".');
        }
    }

    /**
     * @inheritdoc
     */
    public function run()
    {
        $company = ArrayHelper::getValue(Yii::$app, ['user', 'identity', 'company']);

        if ($company !== null && !$company->getHasPaidActualSubscription()) {
            $this->registerScript();
            $this->renderContent();
        }
    }

    /**
     * @inheritdoc
     */
    public function registerScript()
    {
        $js = <<<JS
        $(document).on('click', '{$this->linkSelector}', function (e) {
            e.preventDefault();
            $('#{$this->modalId}').modal('show');
            $(this).off('click');
        });
JS;

        $view = $this->getView();
        $view->registerJs($js, View::POS_HEAD);
    }

    /**
     * @inheritdoc
     */
    public function renderContent()
    {
        echo $this->render('ifFreeTariffWidget', [
            'modalId' => $this->modalId,
            'modalOptions' => (array) $this->modalOptions,
            'text' => $this->text,
        ]);
    }
}
