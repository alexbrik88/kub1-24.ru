<?php

namespace frontend\widgets;

use common\models\Company;
use common\models\service\Subscribe;
use yii\base\InvalidConfigException;
use yii\base\Widget;
use Yii;

/**
 * Class CompanySubscribesWidget
 * @package frontend\widgets
 */
class CompanySubscribesWidget extends Widget
{
    const SHOW_MARK_MIN_DAYS = 5;
    /**
     * @var \common\models\Company
     */
    public $company;

    /**
     * @var boolean
     */
    public $mark = false;

    /**
     * @throws InvalidConfigException
     */
    public function init()
    {
        parent::init();

        if (!$this->company instanceof Company) {
            throw new InvalidConfigException('Company must be defined.');
        }
    }

    /**
     * Executes the widget.
     * @return string the result of widget execution to be outputted.
     */
    public function run()
    {
        /** @var Subscribe[] $subscribes */
        $subscribes = $this->company->activeSubscribes;
        $data = [];
        foreach ($subscribes as $subscribe)
        {
            $daysLeft = ceil(($subscribe->expired_at - time()) / (24 * 3600));

            if ($daysLeft <= self::SHOW_MARK_MIN_DAYS) {
                $this->mark = true;
            }

            $data[] = [
                'name' => $subscribe->tariffGroup->name,
                'expired_date' => date('d.m.Y', $subscribe->expired_at),
                'days_left' => $daysLeft,
                'icon' => $subscribe->tariffGroup->icon,
                'mark_row' => $daysLeft <= self::SHOW_MARK_MIN_DAYS
            ];
        }

        return $this->render('companySubscribesWidget', [
            'data' => $data,
            'mark' => $this->mark,
        ]);
    }
}
