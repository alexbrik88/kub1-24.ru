<?php

namespace frontend\widgets;

use yii\base\Widget;
use Yii;

class TableConfigWidget extends Widget
{
    public $items = [];
    public $sortingItems = [];
    public $showItems = [];
    public $hideItems = [];
    public $customPriceItems = [];
    public $buttonClass;
    public $mainTitle = 'Столбцы';
    public $sortingItemsTitle = 'Сортировка по умолчанию <br> по столбцу';
    public $showColorSelector;
    public $help;

    public $css = <<<STYLE
.theme-kub #config_items_box label {
    font-weight: bold;
}
STYLE;

    public $js = <<<JS
$(".theme-kub #config_items_box label").on("click", function (e) { e.stopPropagation() });
$("#config_items_box input").on("change", function(e) {
    var input = this;
    var target = "." + $(input).data("target");
    var targetInvert = "." + ($(input).data("target-invert") || "");
    var attr = input.id.replace("config-", "");
    var url  = (input.hasAttribute('data-url')) ? input.getAttribute('data-url') : "/site/config";
    var need_pay = $(input).data('need-pay');
    var inputs = {};

    if (need_pay && $(input).prop('checked')) {
        $($(input).data('need-pay-modal')).modal('show');
        $(input).prop('checked', false).uniform('refresh');
        return false;
    }
    
    $("#config_items_box input[type=checkbox]").each(function(i,v) {
        let name = $(v).attr('name');
        let checked = Number($(v).prop('checked'));
        inputs[name] = checked;
    });
    
    $.post(url, inputs, function(data) {
        console.log($(input).data('refresh-page'));
        if (typeof data[attr] !== "undefined" && $(target).length) {
            if (data[attr]) {
                $(target).removeClass("hidden");
                if (targetInvert.length > 1 && $(targetInvert).length) { 
                    $(targetInvert).addClass("hidden"); 
                }
            } else {
                $(target).addClass("hidden");
                if (targetInvert.length > 1 && $(targetInvert).length) { 
                    $(targetInvert).removeClass("hidden"); 
                }
            }
            $(target).closest("table").trigger("tcw.change");
        }
        if ($(input).data('refresh-page'))
            window.location.reload();
        
        if ($(input).data('callback-object') && $(input).data('callback-method')) {
            const object = $(input).data('callback-object');
            const method = $(input).data('callback-method');
            if (typeof window[object][method] === "function")
                window[object][method]();
        }
    })
});
$(".sorting-table-config-item").change(function (e) {
    $(".sorting-table-config-item:checked:not(#" + $(this).attr("id") + ")").prop("checked", false).uniform();
    insertParam('defaultSorting', $(this).attr('id'));
});
function insertParam(key, value) {
    key = escape(key); value = escape(value);
    var kvp = document.location.search.substr(1).split('&');
    if (kvp == '') {
        document.location.search = '?' + key + '=' + value;
    }
    else {
        var i = kvp.length; var x; while (i--) {
            x = kvp[i].split('=');
            if (x[0] == 'sort') {
               delete kvp[i];
            }
            if (x[0] == key) {
                x[1] = value;
                kvp[i] = x.join('=');
                break;
            }
        }
        if (i < 0) { kvp[kvp.length] = [key, value].join('='); }
        document.location.search = kvp.join('&');
    }
}
JS;

    public $jsColorDefault = <<<JS
window.defaultLinksColor = "%DEFAULT_LINKS_COLOR%";

JS;
    public $jsColorSelector = <<<JS
window.linksColorSelector = 
".table-style:not(.table-bleak) td a:not(.link-bleak), " +
 ".table-style:not(.table-bleak) td .link:not(.link-bleak), " +
  ".table-style:not(.table-bleak) td button:not(.link-bleak), " +
   ".table-style:not(.table-bleak).table-compact td a:not(.link-bleak), " +
    ".table-style:not(.table-bleak).table-compact td .link:not(.link-bleak), " +
     ".table-style:not(.table-bleak).table-compact td button:not(.link-bleak)";
window.lastSelectedLinksColor = "";
window.setLinksColor = function(color) {
    $('#tooltip_color_selector_style').html("<style>" + window.linksColorSelector + " { color: " + color + "!important; }</style>");
    window.lastSelectedLinksColor = color;    
}

$('.color-selector-item').on('mouseover', function() {
    const selectedColor = $(this).data('color');
    if (window.lastSelectedLinksColor !== selectedColor) {
        $('#tooltip_color_selector_style').html("<style>" + window.linksColorSelector + " { color: " + selectedColor + "!important; }</style>");
        window.setLinksColor(selectedColor);
    }
});
$('.color-selector-item').on('mouseout', function() {
    const selectedColor = window.defaultLinksColor;
    if (window.lastSelectedLinksColor !== selectedColor) {
        window.setLinksColor(selectedColor);
    }
});
$('input[name="color-selector-item"]').on('change', function() {
    const selectedColor = $(this).data('color');
    const selectedColorIndex = $(this).data('color-index');
    window.defaultLinksColor = selectedColor;
    $.ajax({
        'type': 'post',
        'url': '/site/change-theme-color',
        'data': {'element': 'table-links', 'color_index': selectedColorIndex},            
        'success': function(data) {
            if (data.success) {
                window.setLinksColor(selectedColor);                                
                window.toastr.success('Цвет изменен', "", {
                    "closeButton": true,
                    "showDuration": 1000,
                    "hideDuration": 1000,
                    "timeOut": 3000,
                    "extendedTimeOut": 1000,
                    "escapeHtml": false
                });
            }
        },
    });
});

JS;


    /**
     * @return mixed
     */
    public function run()
    {
        $employee = Yii::$app->user->identity;
        $model = $employee ? $employee->config : null;

        if ($model !== null) {
            $this->view->registerCss($this->css);
            $this->view->registerJs($this->js);
            if ($this->showColorSelector) {
                $this->view->registerJs(str_replace('%DEFAULT_LINKS_COLOR%', $employee->getLinksColor(), $this->jsColorDefault));
                $this->view->registerJs($this->jsColorSelector);
            }

            echo $this->render('tableConfigWidget', [
                'model' => $model,
                'items' => $this->items,
                'mainTitle' => $this->mainTitle,
                'sortingItemsTitle' => $this->sortingItemsTitle,
                'sortingItems' => $this->sortingItems,
                'showItems' => $this->showItems,
                'hideItems' => $this->hideItems,
                'customPriceItems' => $this->customPriceItems,
                'buttonClass' => $this->buttonClass,
                'showColorSelector' => $this->showColorSelector,
                'help' => $this->help
            ]);
        }
    }
}
