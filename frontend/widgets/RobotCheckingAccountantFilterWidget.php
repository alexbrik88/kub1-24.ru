<?php
namespace frontend\widgets;


use backend\models\Bank;
use common\components\helpers\Html;
use common\components\ImageHelper;
use common\models\bank\BankingParams;
use common\models\company\CheckingAccountant;
use common\models\employee\Employee;
use frontend\modules\cash\modules\banking\components\Banking;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\base\Widget;
use yii\bootstrap\Dropdown;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * Class RobotCheckingAccountantFilterWidget
 * @package frontend\widgets
 */
class RobotCheckingAccountantFilterWidget extends Widget
{
    /**
     * @var
     */
    public $pageTitle;
    /**
     * @var
     */
    public $bik;
    /* @var \common\models\Company */
    public $company;

    /**
     *
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function run()
    {
        $result = null;
        $result .= $this->getAccountantList();
        $result .= $this->getAccountantModals();
        $result .= yii2tooltipster::widget([
            'options' => [
                'class' => '.tooltip_rs',
            ],
            'clientOptions' => [
                'theme' => ['tooltipster-kub'],
                'trigger' => 'hover',
                'position' => 'right',
            ],
        ]);

        Yii::$app->view->registerJs('
            $(document).on("click", ".add-checking-accountant", function () {
                $(".modal.new-company-rs").modal();
            });
            $(document).on("beforeSubmit", "form.form-checking-accountant", function () {
                $this = $(this);
                $modal = $(this).closest(".modal");
                $(this).append("<input type=\"hidden\" name=\"bik\" value=\"' . $this->bik . '\">");
                $.post($(this).attr("action"), $(this).serialize(), function (data) {
                    if (data.result == true) {
                        $modal.modal("hide");
                        if ($this.attr("is_new_record") == 1) {
                            $this.find("input").val("");
                        }
                        $("#company_rs_update_form_list").remove();
                        $(".page-content").append("<div id=\"company_rs_update_form_list\"></div>");
                        $("#company_rs_update_form_list").html($(data.updateModals).html());
                        $("ul#user-bank-dropdown").replaceWith(data.html);
                        $(".tooltip_rs").tooltipster({
                            "theme":["tooltipster-kub"],
                            "trigger":"hover",
                            "position":"right"
                        });
                        $("#company_rs_update_form_list input[type=\"checkbox\"]").uniform();
                        Ladda.stopAll();
                    } else {
                        $this.html($(data.html).find("form").html());
                    }
                });
                return false;
            });
        ');

        return $result;
    }

    /**
     * @return null|string
     * @throws \Exception
     */
    private function getAccountantList()
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $list = null;
        $activeAccount = null;
        $bankItems = [];
        /* @var $bankArray CheckingAccountant[] */
        $bankArray = $this->company->getCheckingAccountants()
            ->groupBy('bik')
            ->indexBy('bik')
            ->orderBy('type')
            ->all();
        if ($bankArray) {
            foreach ($bankArray as $account) {
                $bankItems[] = [
                    'label' => Html::tag('span', $account->bank_name, [
                            'class' => 'tooltip_rs',
                            'title' => "р/с: {$account->rs}",
                        ]),
                        //]) .
                        //'<span class="glyphicon glyphicon-pencil update-account" data-toggle="modal" title="Обновить"
                        //aria-label="Обновить" data-target="#update-company-rs-' . $account->id . '"></span>',
                    'url' => ['/tax/robot/bank', 'bik' => $account->bik],
                    'linkOptions' => [
                        'class' => $this->bik == $account->bik ? 'active' : '',
                    ],
                ];
                if ($this->bik == $account->bik) {
                    $activeAccount = $account;
                }
            }
            $banking = '';
            if ($activeAccount !== null && ($bankingClass = Banking::classByBik($activeAccount->bik)) !== null) {
                if (($bank = Bank::findOne(['bik' => $bankingClass::BIK, 'is_blocked' => false])) !== null && $bank->little_logo_link) {
                    $banking .= ImageHelper::getThumb($bank->getUploadDirectory() . $bank->little_logo_link, [32, 32], [
                        'class' => 'little_logo_bank',
                        'style' => 'display: inline-block;margin-left: 20px;margin-right: 10px;',
                    ]);
                }
                if ($activeAccount->autoload_mode_id === null && $bankingClass::$hasAutoload) {
                    $route = ['/cash/bank/index'];
                    if (Yii::$app->getRequest()->get('bik') == $activeAccount->bik) {
                        $route['bik'] = $activeAccount->bik;
                    }
                    $banking .= Html::a("Подгружать выписку автоматически", [
                        "/cash/banking/{$bankingClass::$alias}/default/index",
                        'account_id' => $activeAccount->id,
                        'p' => Banking::routeEncode($route),
                    ], [
                        'class' => 'banking-module-open-link',
                        'style' => 'font-size: 13px;font-weight: 400;letter-spacing: normal;',
                    ]);
                }
            }
            $bankCount = count($bankItems);
            if ($bankCount > 0) {
                $currentBankName = $this->bik == 'all' ? ' Все счета' : Html::tag('span', $bankArray[$this->bik]->bank_name, [
                    'class' => 'tooltip_rs',
                    'title' => "р/с: {$bankArray[$this->bik]->rs}",
                ]);
                if ($bankCount > 1) {
                    $pageRoute['bik'] = $this->bik;
                    $bankItems[] = [
                        'label' => 'Все счета',
                        'url' => ['index', 'bik' => 'all'],
                        'linkOptions' => [
                            'class' => $this->bik == 'all' ? 'active' : '',
                        ],
                    ];
                }
            }
            //$bankItems[] = [
            //    'label' => '[ + ДОБАВИТЬ расч / счет ]',
            //    'options' => [
            //        'class' => 'add-checking-accountant',
            //    ],
            //];

            if ($bankCount > 0) {
                $list = '<div style="display: inline-block;width: 85%;"><div class="dropdown">' .
                    Html::a($currentBankName . ' <b class="caret"></b>', '#', [
                        'data-toggle' => 'dropdown',
                        'class' => 'dropdown-toggle',
                        'style' => 'text-decoration: none;border-bottom: 1px dashed #666;text-transform: uppercase;
                        font-weight: bold;font-size: 19px;line-height: 20px;color: #555;',
                    ]) .
                    Dropdown::widget([
                        'id' => 'user-bank-dropdown',
                        'encodeLabels' => false,
                        'items' => $bankItems,
                    ]) . $banking .
                    '</div></div>';

                return $list;
            }
        }

        return Html::encode($this->pageTitle);
    }

    /**
     * @return string
     */
    private function getAccountantModals()
    {
        $checkingAccountant = new CheckingAccountant();

        $modals = $this->render('@frontend/views/company/form/modal_rs/_create', [
            'checkingAccountant' => $checkingAccountant,
            'company' => null,
        ]);
        $modals .= '<div id="company_rs_update_form_list">' .
            $this->render('@frontend/views/company/form/_rs_update_form_list', [
                'model' => $this->company,
            ]) .
            '</div>';

        return $modals;
    }
}