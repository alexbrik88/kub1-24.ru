<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 05.09.2018
 * Time: 14:43
 */

namespace frontend\widgets;


use backend\models\Bank;
use common\components\helpers\Html;
use common\components\ImageHelper;
use common\models\bank\BankingParams;
use common\models\cash\CashBankStatementUpload;
use common\models\company\CheckingAccountant;
use common\models\employee\Employee;
use frontend\modules\cash\modules\banking\components\Banking;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\base\Widget;
use yii\bootstrap\Dropdown;
use Yii;
use yii\helpers\ArrayHelper;
use yii\helpers\Url;

/**
 * Class CheckingAccountantFilterWidget
 * @package frontend\widgets
 */
class CheckingAccountantFilterWidget extends Widget
{
    /**
     * @var
     */
    public $pageTitle;
    /**
     * @var
     */
    public $bik;
    public $rs;
    /* @var \common\models\Company */
    public $company;

    /**
     *
     */
    public function init()
    {
        parent::init();
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function run()
    {
        $result = null;
        $result .= $this->getAccountantList();
        $result .= $this->getAccountantModals();
        $result .= yii2tooltipster::widget([
            'options' => [
                'class' => '.tooltip_rs',
            ],
            'clientOptions' => [
                'theme' => ['tooltipster-kub'],
                'trigger' => 'hover',
                'position' => 'right',
            ],
        ]);

        Yii::$app->view->registerJs('
            $(document).on("click", ".add-checking-accountant", function () {
                $(".modal.new-company-rs").modal();
            });
            $(document).on("beforeSubmit", "form.form-checking-accountant", function () {
                $this = $(this);
                $modal = $(this).closest(".modal");
                $(this).append("<input type=\"hidden\" name=\"bik\" value=\"' . $this->bik . '\">");
                $.post($(this).attr("action"), $(this).serialize(), function (data) {
                    if (data.result == true) {
                        $modal.modal("hide");
                        if ($this.attr("is_new_record") == 1) {
                            $this.find("input").val("");
                        }
                        $("#company_rs_update_form_list").remove();
                        $(".page-content").append("<div id=\"company_rs_update_form_list\"></div>");
                        $("#company_rs_update_form_list").html($(data.updateModals).html());
                        $("ul#user-bank-dropdown").replaceWith(data.html);
                        $(".tooltip_rs").tooltipster({
                            "theme":["tooltipster-kub"],
                            "trigger":"hover",
                            "position":"right"
                        });
                        $("#company_rs_update_form_list input[type=\"checkbox\"]").uniform();
                        Ladda.stopAll();
                    } else {
                        $this.html($(data.html).find("form").html());
                    }
                });
                return false;
            });
        ');

        return $result;
    }

    /**
     * @return null|string
     * @throws \Exception
     */
    private function getAccountantList()
    {
        /* @var $user Employee */
        $user = Yii::$app->user->identity;
        $list = null;
        $activeAccount = null;
        $items = [];
        /* @var $bankArray CheckingAccountant[] */
        $rsArray = $this->company->getCheckingAccountants()
            ->orderBy('type')
            ->all();
        if ($rsArray) {
            foreach ($rsArray as $account) {
                if ($this->rs == $account->rs) {
                    $activeAccount = $account;
                    $linkCss = 'active';
                } else {
                    $linkCss = '';
                }
                $title = "р/с: {$account->rs}";
                if ($account->isClosed) {
                    $title .= ' - закрыт';
                }
                $items[] = [
                    'label' => Html::tag('span', $account->name, [
                            'class' => 'tooltip_rs',
                            'title' => $title,
                        ]) .
                        '<span class="glyphicon glyphicon-pencil update-account" data-toggle="modal" title="Обновить"
                        aria-label="Обновить" data-target="#update-company-rs-' . $account->id . '"></span>',
                    'url' => ['index', 'rs' => $account->rs],
                    'linkOptions' => [
                        'class' => $linkCss,
                    ],
                ];
            }
            $banking = '';
            if ($activeAccount !== null && ($bankingClass = Banking::classByBik($activeAccount->bik)) !== null) {
                if (($bank = Bank::findOne(['bik' => $bankingClass::BIK, 'is_blocked' => false])) !== null && $bank->little_logo_link) {
                    $banking .= ImageHelper::getThumb($bank->getUploadDirectory() . $bank->little_logo_link, [32, 32], [
                        'class' => 'little_logo_bank',
                        'style' => 'display: inline-block;margin-left: 20px;margin-right: 10px;',
                    ]);
                }
                if ($activeAccount->autoload_mode_id === null && $bankingClass::$hasAutoload) {
                    $route = ['/cash/bank/index'];
                    if ($this->rs == $activeAccount->rs) {
                        $route['rs'] = $activeAccount->rs;
                    }
                    $banking .= Html::a("Подгружать выписку автоматически", [
                        "/cash/banking/{$bankingClass::$alias}/default/index",
                        'account_id' => $activeAccount->id,
                        'p' => Banking::routeEncode($route),
                    ], [
                        'class' => 'banking-module-open-link',
                        'style' => 'font-size: 13px;font-weight: 400;letter-spacing: normal;',
                    ]);
                }
                if ($lastStatementUpload = CashBankStatementUpload::find()->where([
                        'rs' => $activeAccount->rs,
                        'company_id' => $this->company->id,
                        'source' => [CashBankStatementUpload::SOURCE_BANK_AUTO, CashBankStatementUpload::SOURCE_BANK],
                    ])->orderBy(['created_at' => SORT_DESC])->one()
                ) {
                    $banking .= Html::tag('span', 'Выписка загружена ' . date_timestamp_set(new \DateTime, $lastStatementUpload->created_at)->format('d.m.Y в H:i'), [
                        'class' => 'tooltip3',
                        'title' => 'Последняя ' . ($lastStatementUpload->source == CashBankStatementUpload::SOURCE_BANK ? 'выгрузка' : 'автовыгрузка') . ' выписки из Вашего клиент-банка',
                        'style' => 'display:inline-block; margin-left: 10px; font-size: 13px;font-weight: 400;letter-spacing: normal; cursor: pointer;',
                    ]);
                }
                $hasIntegration = $lastStatementUpload || BankingParams::getValue($user->company, $bankingClass::ALIAS, 'access_token');
                if ($activeAccount !== null && $hasIntegration) {
                    $route = ['/cash/bank/index'];
                    if ($this->rs == $activeAccount->rs) {
                        $route['rs'] = $activeAccount->rs;
                        $banking .= Html::a('<i class="icon-refresh" style="font-size:20px;"></i>', [
                            "/cash/banking/{$bankingClass::$alias}/default/index",
                            'account_id' => $activeAccount->id,
                            'start_load_urgently' => true,
                            'p' => Banking::routeEncode($route),
                        ], [
                            'class' => 'refresh-banking-statements banking-module-open-link',
                        ]);

                        $banking .= '<a href="javascript:;" title="Обновить" class="refresh-banking-statements"></a>';
                    }
                }
            }
            $itemsCount = count($items);
            if ($itemsCount > 0) {
                $currentBankName = $activeAccount ? Html::tag('span', $activeAccount->name, [
                    'class' => 'tooltip_rs',
                    'title' => "р/с: {$activeAccount->rs}",
                ]) : ' Все счета';
                if ($itemsCount > 1) {
                    $pageRoute['rs'] = $this->rs;
                    $items[] = [
                        'label' => 'Все счета',
                        'url' => ['index', 'rs' => 'all'],
                        'linkOptions' => [
                            'class' => $this->rs == 'all' ? 'active' : '',
                        ],
                    ];
                }
            }
            $items[] = [
                'label' => '[ + ДОБАВИТЬ расч / счет ]',
                'options' => [
                    'class' => 'add-checking-accountant',
                ],
            ];

            if ($itemsCount > 0) {
                $list = '<div style="display: inline-block;width: 85%;"><div class="dropdown">' .
                    Html::a($currentBankName . ' <b class="caret"></b>', '#', [
                        'data-toggle' => 'dropdown',
                        'class' => 'dropdown-toggle',
                        'style' => 'text-decoration: none;border-bottom: 1px dashed #666;text-transform: uppercase;
                        font-weight: bold;font-size: 19px;line-height: 20px;color: #555;',
                    ]) .
                    Dropdown::widget([
                        'id' => 'user-bank-dropdown',
                        'encodeLabels' => false,
                        'items' => $items,
                    ]) . $banking .
                    '</div></div>';

                return $list;
            }
        }

        return Html::encode($this->pageTitle);
    }

    /**
     * @return string
     */
    private function getAccountantModals()
    {
        $checkingAccountant = new CheckingAccountant();

        $modals = $this->render('@frontend/views/company/form/modal_rs/_create', [
            'checkingAccountant' => $checkingAccountant,
            'company' => null,
        ]);
        $modals .= '<div id="company_rs_update_form_list">' .
            $this->render('@frontend/views/company/form/_rs_update_form_list', [
                'model' => $this->company,
            ]) .
            '</div>';

        return $modals;
    }
}