<?php

namespace frontend\widgets;

use backend\models\Prompt;
use PhpOffice\PhpWord\Shared\Html;
use yii\base\Widget;

/**
 * Class PromptWidget
 * @package frontend\widgets
 */
class PromptWidget extends Widget
{


    /**
     * @var Prompt
     */
    public $prompt;

    /**
     * @return string
     */
    public function init()
    {
        if ($this->prompt !== null) : ?>
            <div class="portlet box darkblue">
                <div class="portlet-title row-fluid">
                    <div
                        class="caption list_recip col-md-12 col-sm-12 no-margin" style="width: 100%!important;">
                        <?= $this->prompt->title; ?>
                    </div>
                </div>
                <div class="portlet-body prompt">
                    <div class="wrapper">
                        <div class="row">
                            <div
                                class="col-md-5 col-sm-5"><?= $this->prompt->text; ?></div>
                            <div class="col-md-7 col-sm-7">
                                <div class="video-container">
                                    <iframe width="480" height="270"
                                            src="<?= $this->prompt->video_link; ?>"
                                            frameborder="0" allowfullscreen>
                                    </iframe>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif;
    }
}