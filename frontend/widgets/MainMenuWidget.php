<?php

namespace frontend\widgets;

use common\models\Chat;
use frontend\components\NotificationHelper;
use yii\base\Widget;
use common\models\notification\Notification;
use Yii;
use common\models\employee\Employee;

class MainMenuWidget extends Widget
{
    public function run()
    {
        $notifications = null;
        $newMessages = null;
        $newMessagesIDs = null;
        if (!\Yii::$app->user->isGuest) {
            /* @var $user Employee */
            $user = Yii::$app->user->identity;
            if ($user->company !== null) {
                $notifications = (new NotificationHelper([
                    'notification_type' => Notification::NOTIFICATION_TYPE_TAX,
                ]))->getNotifications($user->company);
            }
            $newMessages = Chat::find()->andWhere(['and',
                ['to_employee_id' => $user->id],
                ['is_new' => true],
            ])->orderBy('created_at')->all();
            $newMessagesIDs = serialize(Chat::find()->andWhere(['and',
                ['to_employee_id' => $user->id],
                ['is_new' => true],
            ])->orderBy('created_at')->column());
        }

        return $this->render('mainMenuWidget', [
            'notifications' => $notifications,
            'newMessages' => $newMessages,
            'newMessagesIDs' => $newMessagesIDs,
        ]);
    }
}
