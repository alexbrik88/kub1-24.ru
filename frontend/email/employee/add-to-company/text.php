<?php
/**
 * @var $message \common\components\sender\unisender\UniSender
 * @var $data array
 * @var $user \common\models\employee\Employee
 */
?>
Регистрация в сервисе КУБ

Вы зарегистрированы в качестве нового сотрудника компании <?= $company->companyType->name_short; ?> "<?= $company->name_short; ?>".

Ваши данные для входа в систему стались прежними:
Логин: <?= $user->email; ?>

Подключенные компании в вашем профиле:
<?php foreach ($user->employeeCompany as $key => $employeeCompany): ?>
    <?= ++$key . '. ' . $employeeCompany->company->companyType->name_short . ' "' . $employeeCompany->company->name_short . '"'; ?>
<?php endforeach; ?>


ВОЙТИ В КУБ <?= $data['baseUrl']; ?>/login

Нужна помощь?

Позвоните нам <?php echo $data['callUs']; ?>

Напишите нам <?php echo $data['supportEmail']; ?>

Оставить сообщение на сайте <?php echo $data['feedback']; ?>


Вы получили это письмо, потому что подписывались на новости сервиса КУБ на сайте <?php echo $data['baseUrl']; ?>.
У Вас есть возможность отписаться от рассылки, для этого необходимо перейти в личный кабинет <?php echo $data['baseUrl']; ?>/login, в раздел настройки и убрать галочки в блоке «Получение уведомлений».
