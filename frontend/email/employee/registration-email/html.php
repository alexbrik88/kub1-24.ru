<?php

use common\components\helpers\Html;

/**
 * @var $message \common\components\sender\unisender\UniSender
 * @var $data array
 */

$imageUrl = \Yii::$app->params['uniSender']['imgUrl'] . DIRECTORY_SEPARATOR . 'logo.png';
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <title></title></head>
<body>
<table cellpadding="0" class="preview" cellspacing="0" width="100%" border="0"
       style="min-width:768px;">
    <tr>
        <td valign="top" align="center">
            <table width="768" border="0" cellspacing="0" cellpadding="0"
                   class="campaign"
                   style="background-color: #fff;border-bottom-width:4px;border-color:#0077A7;border-style:solid; border-left-width: 0px; border-right-width: 0px; border-top-width: 0px;border-collapse: initial;">
                <tr>
                    <td width="768" height="12" class="campaign"></td>
                </tr>
                <tr>
                    <td width="768" align="left" valign="middle"
                        style="padding-left:20px;font-family:Arial;font-size: 11px;color:#000000;font-family:Arial, Helvetica, sans-serif;font-size:11px;"
                        class="campaign">
                        <?= Html::a(Html::img($message->embed(Yii::getAlias(Yii::$app->params['uniSender']['imgPath'] . DIRECTORY_SEPARATOR . 'logo.png')), [
                            'style' => 'style="border-bottom-width:0px; border-left-width: 0px; border-right-width: 0px; border-top-width: 0px;',
                        ]), Yii::$app->params['serviceSite'] . Yii::$app->params['uniSender']['utm']['logo'], [
                            'target' => '_blank',
                        ]); ?>
                    </td>
                    <td width="768" align="right" valign="middle"
                        style="padding-right:20px;font-family:Arial;font-size: 11px;color:#000000;font-family:Arial, Helvetica, sans-serif;font-size:11px;"
                        class="campaign"><a
                            href="<?php echo $data['baseUrl'] . Yii::$app->params['uniSender']['utm']['entrance']; ?>"
                            target="_blank"
                            style="font-size: 11px;color:#4D7ACC;font-family:Arial, Helvetica, sans-serif;font-size:14pt;"
                            id="watch_offline">Вход</a></td>
                </tr>
                <tr>
                    <td class="campaign" width="768" height="12"></td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" width="768" border="0"
                   style="" bgcolor="#F6F6F6" class="campaign">
                <tr>
                    <td align="center" width="768">
                        <table border="0" cellspacing="0" cellpadding="0"
                               width="768" style="background-color:#f6f6f6;">
                            <tr>
                                <td align="left" width="768" class="headerimage"
                                    style="border-bottom-width: 0; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #f6f6f6; border-top-width: 20px; margin-bottom: -3px;">
                                    <p style="font-size: 24pt; padding-left: 30px; margin-top: 20px; line-height: 21px; margin-bottom: 20px; color: #000;">
                                        Регистрация в сервисе КУБ</p></td>
                            </tr>
                            <tr>
                                <td width="768" class="spacer" align="left"
                                    style="border-bottom-width: 0; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #f6f6f6; border-top-width: 0px; margin-bottom: -3px;">
                                    <p style="margin: 0; font-size: 12pt; padding-left: 30px; margin-bottom: 20px;">
                                        Вы зарегистрированы в качестве нового
                                        сотрудника
                                        компании <?php echo $data['companyTypeShort']; ?>
                                        "<?php echo $data['companyName']; ?>
                                        ".</p></td>
                            </tr>
                            <tr>
                                <td width="768" class="spacer" align="left"
                                    style="border-bottom-width: 0; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #f6f6f6; border-top-width: 0px; margin-bottom: -3px;">
                                    <p style="margin: 0; font-size: 12pt; padding-left: 30px;">
                                        Ваши данные для входа в систему:</p>

                                    <p style="margin: 0; font-size: 12pt; padding-left: 30px;">
                                        Логин:
                                        <span><?php echo $data['login']; ?></span>
                                    </p>

                                    <p style="margin: 0; font-size: 12pt; padding-left: 30px; margin-bottom: 20px">
                                        Пароль:
                                        <span><?php echo $data['password']; ?></span>
                                    </p></td>
                            </tr>
                            <tr>
                                <td width="768" class="spacer" align="left"
                                    style="border-bottom-width: 0; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #f6f6f6; border-top-width: 0px; margin-bottom: -3px;">
                                    <p style="margin: 0; font-size: 12pt; padding-left: 30px; margin-bottom: 30px; font-weight: bold;">
                                        С уважением,<br> Команда КУБ</p></td>
                            </tr>
                        </table>
                        <table width="768" border="0" cellspacing="0"
                               cellpadding="0" class="table"
                               style="background-color:#f6f6f6;">
                            <tr>
                                <td align="left" width="768" class="headerimage"
                                    style="border-bottom-width: 0; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #f6f6f6; border-top-width: 0px; text-align: center;">
                                    <a href="<?php echo $data['baseUrl'] . Yii::$app->params['uniSender']['utm']['entranceKub']; ?>"
                                       target="_blank"
                                       style="color: #000; font-weight: bold; font-size: 14pt;">ВОЙТИ
                                        В КУБ</a></td>
                            </tr>
                            <tr>
                                <td align="left" width="768" class="headerimage"
                                    style="border-bottom-width: 0; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #f6f6f6; border-top-width: 0px; border-bottom-width: 0;">
                                    <p style="padding-top: 20px; display: block; text-align: center; color: #000; font-size: 12pt; margin-top: -3px; margin-bottom: 0;">
                                        Нужна помощь?</p></td>
                            </tr>
                        </table>
                        <table width="768" border="0" cellspacing="0"
                               cellpadding="0" class="table"
                               style="background-color:#f6f6f6;">
                            <tr>
                                <td width="520" align="center" class="cell">
                                    <table width="768" border="0"
                                           cellspacing="0" cellpadding="0"
                                           class="table16452476">
                                        <tr>
                                            <td align="center" width="246"
                                                class="headerimage"
                                                style="width: 246px; background-color: #fff; border-left-width: 20px; border-style: solid; border-color: #f6f6f6; border-top-width: 0px; border-bottom-width: 0; padding-top: 20px; border-right-color: #fff;">
                                                <?= Html::img($message->embed(Yii::getAlias(Yii::$app->params['uniSender']['imgPath'] . DIRECTORY_SEPARATOR . 'phone.png'))) ?>
                                            </td>
                                            <td align="center" width="246"
                                                class="headerimage"
                                                style="width: 246px; background-color: #fff; border-bottom-width: 0; padding-top: 20px;">
                                                <?= Html::img($message->embed(Yii::getAlias(Yii::$app->params['uniSender']['imgPath'] . DIRECTORY_SEPARATOR . 'mail.png'))) ?>
                                            </td>
                                            <td align="center" width="246"
                                                class="headerimage"
                                                style="width: 246px; background-color: #fff; border-style: solid; border-right-width: 20px; border-color: #f6f6f6; border-top-width: 0px; border-bottom-width: 0; padding-top: 20px; border-left-color: #fff;">
                                                <?= Html::img($message->embed(Yii::getAlias(Yii::$app->params['uniSender']['imgPath'] . DIRECTORY_SEPARATOR . 'web.png'))) ?>
                                            </td>
                                        </tr>
                                    </table>
                                    <table width="768" border="0"
                                           cellspacing="0" cellpadding="0"
                                           class="table16452476">
                                        <tr>
                                            <td align="center" width="246"
                                                class="headerimage"
                                                style="width: 246px; background-color: #fff; border-left-width: 20px; border-style: solid; border-color: #f6f6f6; border-top-width: 0px; border-bottom-width: 0; border-right-color: #fff;">
                                                <p style="margin-bottom: 30px;">
                                                    Позвоните
                                                    нам<br><?php echo $data['callUs']; ?>
                                                </p></td>
                                            <td align="center" width="246"
                                                class="headerimage"
                                                style="width: 246px; background-color: #fff; border-bottom-width: 0;">
                                                <p style="margin-bottom: 30px;">
                                                    Напишите нам<br><a
                                                        href="mailto:<?php echo $data['supportEmail']; ?>"><?php echo $data['supportEmail']; ?></a>
                                                </p></td>
                                            <td align="center" width="246"
                                                class="headerimage"
                                                style="width: 246px; background-color: #fff; border-style: solid; border-right-width: 20px; border-color: #f6f6f6; border-top-width: 0px; border-bottom-width: 0; border-left-color: #fff; padding-bottom: 17px;">
                                                <a href="<?= $data['feedback'] . Yii::$app->params['uniSender']['utm']['leaveMessage']; ?>"
                                                   style="color: #000;">Оставить
                                                    сообщение<br>на сайте</a>
                                            </td>
                                        </tr>
                                    </table>
                                    <table width="768" border="0"
                                           cellspacing="0" cellpadding="0"
                                           class="table16452476">
                                        <tr>
                                            <td align="left" width="768"
                                                class="headerimage"
                                                style="display: block; width: 728px; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #f6f6f6; border-top-width: 0px; height: 10px; border-bottom-width: 0;"></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <table width="768" border="0" cellspacing="0"
                               cellpadding="0" class="table" style="">
                            <tr>
                                <td width="20"></td>
                                <td width="768" height="7" class="spacer"></td>
                                <td width="20"></td>
                            </tr>
                            <tr>
                                <td width="50"></td>
                                <td width="768" align="left" valign="top"
                                    style="color:#000000;font-family:Arial, Helvetica, sans-serif;line-height:15px;font-size:8pt;"
                                    class="cell">
                                    <p style=" margin-bottom: 10px;">Вы получили
                                        это письмо, потому что подписывались на
                                        новости сервиса КУБ на сайте <a
                                            href="<?php echo $data['baseUrl']; ?>"
                                            target="_blank"
                                            style="color: #055EC3;"><?php echo $data['domainName']; ?></a>.<br>
                                        У Вас есть возможность отписаться от
                                        рассылки, для этого необходимо перейти в
                                        <a href="<?php echo $data['baseUrl']; ?>/site/login"
                                           target="_blank"
                                           style="color: #055EC3;">личный
                                            кабинет</a>, в раздел настройки и
                                        убрать галочки в блоке «Получение уведомлений».
                                    </p>
                                </td>
                                <td width="20"></td>
                            </tr>
                            <tr>
                                <td width="20"></td>
                                <td width="768" height="7" class="spacer"></td>
                                <td width="20"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<?= \frontend\email\MailTracking::getTag() ?>

</body>
</html>
