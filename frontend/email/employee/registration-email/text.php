<?php
/**
 * @var $message \common\components\sender\unisender\UniSender
 * @var $data array
 */
?>
Регистрация в сервисе КУБ

Вы зарегистрированы в качестве нового сотрудника компании <?php echo $data['companyTypeShort']; ?> "<?php echo $data['companyName']; ?>".

Ваши данные для входа в систему:
Логин: <?php echo $data['login']; ?>

Пароль: <?php echo $data['password']; ?>


С уважением, Команда КУБ

ВОЙТИ В КУБ <?php echo $data['baseUrl']; ?>/login

Нужна помощь?

Позвоните нам <?php echo $data['callUs']; ?>

Напишите нам <?php echo $data['supportEmail']; ?>

Оставить сообщение на сайте <?php echo $data['feedback']; ?>


Вы получили это письмо, потому что подписывались на новости сервиса КУБ на сайте <?php echo $data['baseUrl']; ?>.
У Вас есть возможность отписаться от рассылки, для этого необходимо перейти в личный кабинет <?php echo $data['baseUrl']; ?>/login, в раздел настройки и убрать галочки в блоке «Получение уведомлений».
