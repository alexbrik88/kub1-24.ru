<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 1.9.15
 * Time: 14.27
 * Email: t.kanstantsin@gmail.com
 */

/* @var \yii\web\View $this */
/* @var common\models\employee\Employee $model */
/* @var string $confirmUrl */

$greeting = 'Здравствуйте, ' . $model->firstname . '!';
?>
<?= $greeting; ?>
<?= str_repeat('=', mb_strlen($greeting)); ?>


Подтвердите ваш e-mail и Ваши покупатели будут видеть его при получении счетов из сервиса КУБ.

Для подтверждения перейдите по ссылке: <?= $confirmUrl; ?>

