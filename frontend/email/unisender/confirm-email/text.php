<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 1.9.15
 * Time: 14.27
 * Email: t.kanstantsin@gmail.com
 */

/* @var \yii\web\View $this */
/* @var common\models\employee\Employee $model */
/* @var string $confirmUrl */

?>
Здравствуйте, {{Name}}!


Подтвердите ваш e-mail и Ваши покупатели будут видеть его при получении счетов из сервиса КУБ.

Для подтверждения перейдите по ссылке: <?= $message->getParam('confirmUrl'); ?>

