<?php
/**
 * @var $message \common\components\sender\unisender\UniSender
 */
?>
Здравствуйте, <?= $employee->firstname; ?>!

Для восстановления Вашего пароля на сайте <?php echo $uniSender->getParam('domainName'); ?>, Вам надо пройти по следующей ссылке и следовать дальнейшим инструкциям:

Пройдите со ссылке: <?php echo $passwordRecoverUrl; ?>

Эта ссылка будет действительна в течение одного часа.

Если Вы получили это письмо по ошибке, вероятно, другой пользователь случайно указал Ваш адрес при изменении пароля. В таком случае проигнорируйте это сообщение.

По всем вопросам обращайтесь в службу поддержки <?php echo $supportEmail; ?>


С уважением, Команда КУБ

Вы получили это письмо, потому что подписывались на новости сервиса КУБ на сайте <?php echo $uniSender->getParam('baseUrl'); ?>.
У Вас есть возможность отписаться от рассылки, для этого необходимо перейти в личный кабинет <?php echo $uniSender->getParam('baseUrl'); ?>/login, в раздел настройки и убрать галочки в блоке «Получение уведомлений».
