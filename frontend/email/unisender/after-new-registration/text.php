<?php
/**
 * @var $message \common\components\sender\unisender\UniSender
 */
?>
Спасибо, что вы с нами!

Ваши данные для входа в систему.
Логин: <?php echo $message->getParam('login'); ?>

Пароль: <?php echo $message->getParam('password'); ?>


Что вы можете сделать в КУБе сегодня:
- Заполнить данные по Вашей компании.
	Добавьте логотип вашей компании, чтобы счет был красивым.
	Добавьте скан печати и подписи, чтобы высылать счет уже подписанным.
- Добавьте Ваших покупателей.
	Это просто - достаточно ввести ИНН вашего покупателя и все данные автоматически заполнятся. Далее вводите e-mail руководителя и/или контактного лица - того, кому будете отправлять счета.
	Если у вас есть расчетный счет покупателя, то заполните это поле и поле БИК, по нему заполняются остальные поля по банку. Расчетный счет и БИК можно заполнить позже - они понадобятся для Актов и Товарных накладных. А для счета они не нужны.
-	Выставить первый счет покупателю.
Готовя счет, вы добавляете услуги или товары. Они сохранятся и в следующий раз вы их выберите из списка.
-	Добавить Ваших поставщиков.
	Все делаем, так же как и по покупателям.


ВОЙТИ В КУБ <?php echo $message->getParam('baseUrl'); ?>/login

Нужна помощь?

Позвоните нам <?php echo $message->getParam('callUs'); ?>

Напишите нам <?php echo $message->getParam('supportEmail'); ?>

Оставить сообщение на сайте <?php echo $message->getParam('feedback'); ?>


Вы получили это письмо, потому что подписывались на новости сервиса КУБ на сайте <?php echo $message->getParam('baseUrl'); ?>.
У Вас есть возможность отписаться от рассылки, для этого необходимо перейти в личный кабинет <?php echo $message->getParam('baseUrl'); ?>, в раздел настройки и убрать галочки в блоке «Получение уведомлений».