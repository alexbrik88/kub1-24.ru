<?php
/**
 * @var $message \common\components\sender\unisender\UniSender
 */
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <title></title></head>
<body>
<table cellpadding="0" class="preview" cellspacing="0" width="100%" border="0"
       style="min-width:768px;">
    <tr>
        <td valign="top" align="center">
            <table width="808" border="0" cellspacing="0" cellpadding="0"
                   class="campaign"
                   style="background-color: #fff; border-width: 0; border-top-width: 0; border-left-width: 0; border-right-width: 0;  border-bottom: solid 4px #0077A7;">
                <tr>
                    <td width="768" height="12" class="campaign"></td>
                </tr>
                <tr>
                    <td width="768" align="left" valign="middle"
                        style="padding-left:20px;font-family:Arial;font-size: 11px;color:#000000;font-family:Arial, Helvetica, sans-serif;font-size:11px; border: 0;"
                        class="campaign">
                        <a href="<?php echo $message->getParam('baseUrl') . Yii::$app->params['uniSender']['utm']['logo']; ?>">
                            <img src="<?= $message->getDataURI('logo.png'); ?>" style="border-bottom-width:0px; border-left-width: 0px; border-right-width: 0px; border-top-width: 0px;">
                        </a>
                    </td>
                    <td width="768" align="right" valign="middle"
                        style="padding-right:20px;font-family:Arial;font-size: 11px;color:#000000;font-family:Arial, Helvetica, sans-serif;font-size:11px; border: 0;"
                        class="campaign"><a
                            href="<?php echo $message->getParam('baseUrl') . Yii::$app->params['uniSender']['utm']['entrance']; ?>"
                            target="_blank"
                            style="font-size: 11px;color:#4D7ACC;font-family:Arial, Helvetica, sans-serif;font-size:14pt;"
                            id="watch_offline">Вход</a></td>
                </tr>
                <tr>
                    <td class="campaign" width="768" height="12"></td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" width="768" border="0"
                   style="" bgcolor="#F6F6F6" class="campaign">
                <tr>
                    <td align="center" width="768">
                        <table border="0" cellspacing="0" cellpadding="0"
                               width="768" style="background-color:#f6f6f6; border: 20px solid #f6f6f6; border-bottom: 0;">
                            <tr>
                                <td align="left" width="768" class="headerimage"
                                    style="border-bottom-width:0px; width: 728px; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #ffffff; border-top-width: 20px; margin-bottom: -3px;">
                                    <p style="font-size: 24pt; margin-top: 20px; line-height: 21px; margin-bottom: 20px; color: #000;">
                                        Спасибо, что вы с нами!</p></td>
                            </tr>
                            <tr>
                                <td align="left" width="768" class="spacer"
                                    style="border-bottom-width:0px; width: 728px; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #ffffff; border-top-width: 0px; margin-bottom: -3px;">
                                    <p style="margin: 0; font-size: 12pt;  margin-bottom: 20px;">
                                        Ваши данные для входа в систему.</p>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="768" class="spacer"
                                    style="border-bottom-width:0px; width: 728px; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #ffffff; border-top-width: 0px; margin-bottom: -3px;">
                                    <p style="margin: 0; font-size: 12pt;">
                                        Логин:
                                        <span><?php echo $message->getParam('login'); ?></span>
                                    </p>

                                    <p style="margin: 0; font-size: 12pt;">
                                        Пароль:
                                        <span><?php echo $message->getParam('password'); ?></span>
                                    </p></td>
                            </tr>
                            <tr>
                                <td align="left" width="768" class="spacer"
                                    style="border-bottom-width:0px; width: 728px; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #ffffff; border-top-width: 0px; margin-bottom: -3px; font-weight: bold;">
                                    <p style="margin: 0; font-size: 12pt; padding-left: 30px; padding-top: 30px;">
                                        Что вы можете сделать в КУБе
                                        сегодня.</p></td>
                            </tr>
                            <tr>
                                <td align="left" width="768" class="spacer"
                                    style="border-bottom-width: 0; padding-right: 0; background-color: #fff; border-left-width: 20px; border-style: solid; border-color: #ffffff; border-top-width: 0px; margin-bottom: -3px; border-right-width: 20px;">
                                    <table border="0" cellspacing="0"
                                           cellpadding="0" width="728">
                                        <tr>
                                            <td style="width: 20px; vertical-align: top; padding-top: 11px; padding-left: 30px;">
                                                <img
                                                    src="<?php echo $message->getDataURI('checkbox-ico.png'); ?>">
                                            </td>
                                            <td width="768" class="spacer"
                                                style="border-bottom-width: 0; background-color: #ffffff; border-left-width: 0px; border-style: solid; border-right-width: 0px; border-color: #ffffff; border-top-width: 0px; margin-bottom: -3px;">
                                                <p style="margin: 0; font-size: 12pt; padding-left: 10px; padding-top: 13px;">
                                                    <span
                                                        style="font-weight: bold;">Заполнить данные по Вашей компании.</span>  <br>
                                                    Добавьте логотип вашей компании, чтобы счет был красивым.
Добавьте скан печати и подписи, чтобы высылать счет уже подписанным.
</p></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="768" class="spacer"
                                    style="border-bottom-width: 0; width: 728px; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #ffffff; border-top-width: 0px; margin-bottom: -3px; border-right-width: 20px;">
                                    <table border="0" cellspacing="0"
                                           cellpadding="0" width="728">
                                        <tr>
                                            <td style="width: 20px; vertical-align: top; padding-top: 11px; padding-left: 30px;">
                                                <img
                                                    src="<?php echo $message->getDataURI('checkbox-ico.png'); ?>">
                                            </td>
                                            <td width="768" class="spacer"
                                                style="border-bottom-width: 0; background-color: #fff; border-left-width: 0px; border-style: solid; border-right-width: 0px; border-color: #ffffff; border-top-width: 0px; margin-bottom: -3px;">
                                                <p style="margin: 0; font-size: 12pt; padding-left: 10px; padding-top: 13px;">
                                                    <span
                                                        style="font-weight: bold;">Добавьте Ваших покупателей.</span><br>
                                                        Это просто - достаточно ввести ИНН вашего покупателя и все данные автоматически заполнятся. Далее вводите e-mail руководителя и/или контактного лица - того, кому будете отправлять счета. <br>
Если у вас есть расчетный счет покупателя, то заполните это поле и поле БИК, по нему заполняются остальные поля по банку. Расчетный счет и БИК можно заполнить позже - они понадобятся для Актов и Товарных накладных. А для счета они не нужны.

                                                        </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="768" class="spacer"
                                    style="border-bottom-width: 0; width: 728px; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #ffffff; border-top-width: 0px; margin-bottom: -3px; border-right-width: 20px;">
                                    <table border="0" cellspacing="0"
                                           cellpadding="0" width="728">
                                        <tr>
                                            <td style="width: 20px; vertical-align: top; padding-top: 11px; padding-left: 30px;">
                                                <img
                                                    src="<?php echo $message->getDataURI('checkbox-ico.png'); ?>">
                                            </td>
                                            <td width="768" class="spacer"
                                                style="border-bottom-width: 0; width: 682px; background-color: #fff; border-left-width: 0px; border-style: solid; border-right-width: 0px; border-color: #ffffff; border-top-width: 0px; margin-bottom: -3px;">
                                                <p style="margin: 0; font-size: 12pt; padding-left: 10px; padding-top: 13px;">
                                                    <span
                                                        style="font-weight: bold;">Выставить первый счет покупателю.</span>
                                                        <br>
                                                        Готовя счет, вы добавляете услуги или товары. Они сохранятся и в следующий раз вы их выберите из списка.
                                                </p></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="768" class="spacer"
                                    style="border-bottom-width: 0; width: 728px; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #ffffff; border-top-width: 0px; margin-bottom: -3px; border-right-width: 20px;">
                                    <table border="0" cellspacing="0"
                                           cellpadding="0" width="728">
                                        <tr>
                                            <td style="width: 20px; vertical-align: top; padding-top: 11px; padding-left: 30px;">
                                                <img
                                                    src="<?php echo $message->getDataURI('checkbox-ico.png'); ?>">
                                            </td>
                                            <td width="768" class="spacer"
                                                style="border-bottom-width: 0; width: 682px; background-color: #fff; border-left-width: 0px; border-style: solid; border-right-width: 0px; border-color: #ffffff; border-top-width: 0px; margin-bottom: -3px;">
                                                <p style="margin: 0; font-size: 12pt; padding-left: 10px; padding-top: 13px;">
                                                    <span
                                                        style="font-weight: bold;">Добавить Ваших поставщиков.</span><br>Все делаем, так же как и по покупателям.</p></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="768" class="spacer"
                                    style="border-bottom-width: 0; width: 728px; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #ffffff; border-top-width: 0px; margin-bottom: -3px;">
                                    <p style="margin: 0; font-size: 12pt; padding-top: 13px;">
                                        После этого ваш бухгалтер сможет сам
                                        смотреть ваши документы и все, что нужно
                                        выгружать в 1С.</p></td>
                            </tr>
                        </table>
                        <table width="768" border="0" cellspacing="0"
                               cellpadding="0" class="table"
                               style="background-color:#f6f6f6;">
                            <tr>
                                <td align="left" width="768" class="headerimage"
                                    style="border-bottom-width: 0; width: 728px; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #ffffff; border-top-width: 0px; height: 125px; border-bottom-width: 0;"></td>
                            </tr>
                            <tr>
                                <td align="left" width="768" class="headerimage"
                                    style="border-bottom-width: 0; width: 728px; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #ffffff; border-top-width: 0px; text-align: center;">
                                    <a href="<?php echo $message->getParam('baseUrl'); ?>/login"
                                       style="color: #000; font-weight: bold; font-size: 14pt;">ВОЙТИ
                                        В КУБ</a></td>
                            </tr>
                            <tr>
                                <td align="left" width="768" class="headerimage"
                                    style="border-bottom-width: 0; width: 728px; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #ffffff; border-top-width: 0px; border-bottom-width: 0;">
                                    <p style="padding-top: 20px; display: block; text-align: center; color: #000; font-size: 12pt; margin-top: -3px; margin-bottom: 0;">
                                        Нужна помощь?</p></td>
                            </tr>
                        </table>
                        <table width="768" border="0" cellspacing="0"
                               cellpadding="0" class="table"
                               style="background-color:#f6f6f6;">
                            <tr>
                                <td width="520" align="center" class="cell">
                                    <table width="768" border="0"
                                           cellspacing="0" cellpadding="0"
                                           class="table16452476">
                                        <tr>
                                            <td align="center" width="246"
                                                class="headerimage"
                                                style="width: 246px; background-color: #fff; border-left-width: 20px; border-style: solid; border-color: #ffffff; border-top-width: 0px; border-bottom-width: 0; padding-top: 20px; border-right-color: #fff;">
                                                <img
                                                    src="<?php echo $message->getDataURI('phone.png'); ?>">
                                            </td>
                                            <td align="center" width="246"
                                                class="headerimage"
                                                style="width: 246px; background-color: #fff; border-bottom-width: 0; padding-top: 20px;">
                                                <img
                                                    src="<?php echo $message->getDataURI('mail.png'); ?>">
                                            </td>
                                            <td align="center" width="246"
                                                class="headerimage"
                                                style="width: 246px; background-color: #fff; border-style: solid; border-right-width: 20px; border-color: #ffffff; border-top-width: 0px; border-bottom-width: 0; padding-top: 20px; border-left-color: #fff;">
                                                <img
                                                    src="<?php echo $message->getDataURI('web.png'); ?>">
                                            </td>
                                        </tr>
                                    </table>
                                    <table width="768" border="0"
                                           cellspacing="0" cellpadding="0"
                                           class="table16452476">
                                        <tr>
                                            <td align="center" width="246"
                                                class="headerimage"
                                                style="width: 246px; background-color: #fff; border-left-width: 20px; border-style: solid; border-color: #ffffff; border-top-width: 0px; border-bottom-width: 0; border-right-color: #fff;">
                                                <p style="margin-bottom: 30px;">
                                                    Позвоните
                                                    нам<br><?php echo $message->getParam('callUs'); ?>
                                                </p></td>
                                            <td align="center" width="246"
                                                class="headerimage"
                                                style="width: 246px; background-color: #fff; border-bottom-width: 0;">
                                                <p style="margin-bottom: 30px;">
                                                    Напишите
                                                    нам<br><?php echo $message->getParam('supportEmail'); ?>
                                                </p></td>
                                            <td align="center" width="246"
                                                class="headerimage"
                                                style="width: 246px; background-color: #fff; border-style: solid; border-right-width: 20px; border-color: #ffffff; border-top-width: 0px; border-bottom-width: 0; border-left-color: #fff; padding-bottom: 17px;">
                                                <a href="<?= $message->getParam('feedback') . Yii::$app->params['uniSender']['utm']['leaveMessage']; ?>"
                                                   style="color: #000;">Оставить
                                                    сообщение<br>на сайте</a>
                                            </td>
                                        </tr>
                                    </table>
                                    <table width="768" border="0"
                                           cellspacing="0" cellpadding="0"
                                           class="table16452476">
                                        <tr>
                                            <td align="left" width="768"
                                                class="headerimage"
                                                style="display: block; width: 728px; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #ffffff; border-top-width: 0px; height: 110px; border-bottom-width: 0;"></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <table width="768" border="0" cellspacing="0"
                               cellpadding="0" class="table" style="">
                            <tr>
                                <td width="20"></td>
                                <td width="768" height="7" class="spacer"></td>
                                <td width="20"></td>
                            </tr>
                            <tr>
                                <td width="50"></td>
                                <td width="768" align="left" valign="top"
                                    style="color:#000000;font-family:Arial, Helvetica, sans-serif;line-height:15px;font-size:8pt;"
                                    class="cell">
                                    <p style=" margin-bottom: 10px;">Вы получили
                                        это письмо, потому что подписывались на
                                        новости сервиса КУБ на сайте <a
                                            href="<?php echo $message->getParam('baseUrl'); ?>"
                                            style="color: #055EC3;"><?php echo $message->getParam('domainName'); ?></a>.<br>
                                        У Вас есть возможность отписаться от
                                        рассылки, для этого необходимо перейти в
                                        <a href="<?php echo $message->getParam('baseUrl'); ?>/site/login"
                                           style="color: #055EC3;">личный
                                            кабинет</a>, в раздел настройки и
                                        убрать галочки в блоке «Получение
                                        уведомлений».
                                    </p>
                                </td>
                                <td width="20"></td>
                            </tr>
                            <tr>
                                <td width="20"></td>
                                <td width="768" height="7" class="spacer"></td>
                                <td width="20"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<?= \frontend\email\MailTracking::getTag() ?>

</body>
</html>
