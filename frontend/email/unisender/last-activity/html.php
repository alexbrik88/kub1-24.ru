<?php
/**
 * @var $message \common\components\sender\unisender\UniSender
 */
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <title></title></head>
<body>
<table cellpadding="0" class="preview" cellspacing="0" width="100%" border="0"
       style="min-width:768px;">
    <tr>
        <td valign="top" align="center">
            <table width="768" border="0" cellspacing="0" cellpadding="0"
                   class="campaign"
                   style="background-color: #fff;border-bottom-width:4px;border-color:#0077A7;border-style:solid;border-left-width: 0px;border-right-width: 0px;border-top-width: 0px;border-collapse: initial;">
                <tr>
                    <td width="768" height="12" class="campaign"></td>
                </tr>
                <tr>
                    <td width="768" align="left" valign="middle"
                        style="padding-left:20px;font-family:Arial;font-size: 11px;color:#000000;font-family:Arial, Helvetica, sans-serif;font-size:11px;"
                        class="campaign">
                        <a href="<?php echo $message->getParam('baseUrl') . Yii::$app->params['uniSender']['utm']['logo']; ?>">
                            <img src="<?= $message->getDataURI('logo.png'); ?>" style="border-bottom-width:0px; border-left-width: 0px; border-right-width: 0px; border-top-width: 0px;">
                        </a>
                    </td>
                    <td width="768" align="right" valign="middle"
                        style="padding-right:20px;font-family:Arial;font-size: 11px;color:#000000;font-family:Arial, Helvetica, sans-serif;font-size:11px;"
                        class="campaign"><a
                            href="<?php echo $message->getParam('baseUrl') . Yii::$app->params['uniSender']['utm']['entrance']; ?>"
                            target="_blank"
                            style="font-size: 11px;color:#4D7ACC;font-family:Arial, Helvetica, sans-serif;font-size:14pt;"
                            id="watch_offline">Вход</a></td>
                </tr>
                <tr>
                    <td class="campaign" width="768" height="12"></td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" width="768" border="0"
                   style="" bgcolor="#F6F6F6" class="campaign">
                <tr>
                    <td align="center" width="768">
                        <table border="0" cellspacing="0" cellpadding="0"
                               width="768" style="background-color:#f6f6f6;">
                            <tr>
                                <td align="left" width="768" class="headerimage"
                                    style="border-bottom-width:0px; width: 728px; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #f6f6f6; border-top-width: 20px; margin-bottom: -3px;">
                                    <p style="font-size: 24pt; padding-left: 30px; margin-top: 20px; line-height: 21px; margin-bottom: 20px; color: #000;">
                                        Привет, {{Name}} </p>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="768" class="spacer"
                                    style="border-bottom-width:0px; width: 728px; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #f6f6f6; border-top-width: 0px; margin-bottom: -3px;">
                                    <p style="margin: 0; font-size: 12pt; padding-left: 30px; margin-bottom: 20px;">
                                        Меня зовут Алексей.<br>
                                        Вы зарегистрировались в сервисе КУБ и больше не заходили в него. Я знаю, что сложно найти время, что бы изучить, что-то новое, но поверьте мне -  это стоит того. Автоматизировав выставление счетов и других документов, вы упростите свою работу и будете уверены, что в документах нет ошибок. У вас будет больше свободного времени, что бы заниматься вашим любимым делом.
                                    </p></td>
                            </tr>
                            <tr>
                                <td align="left" width="768" class="spacer"
                                    style="border-bottom-width:0px; width: 728px; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #f6f6f6; border-top-width: 0px; margin-bottom: -3px;">
                                    <p style="margin: 0; font-size: 12pt; padding-left: 30px;">
                                        Потратьте несколько минут, что бы посмотреть, как КУБ упростит вашу работу.</p></td>
                            </tr>
                            <tr>
                                <td width="768" class="spacer"
                                    style="border-bottom-width:0px; width: 728px; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #f6f6f6; border-top-width: 0px; margin-bottom: -3px;">
                                    <table border="0" cellspacing="0"
                                           cellpadding="0"
                                           style="border-bottom: 1px dashed #DEDEDE; margin: 0 95px 0 115px; padding-top: 30px;">
                                        <tr>
                                            <td width="768" class="spacer"
                                                style="border-bottom-width:0px; width: 320px; background-color: #fff; border-left-width: 0px; border-style: solid; border-right-width: 0px; border-color: #f6f6f6; border-top-width: 0px; border-bottom-width: 0;">
                                                <p style="margin: 0; font-size: 12pt; width: 280px;">
                                                    <span
                                                        style="font-weight: bold;">1. Заполнение данных по Вашей компании.</span>
                                                </p>

                                                <p style="margin: 0; font-size: 12pt; width: 280px; padding-bottom: 20px;">
                                                   Вводите ваш ИНН, добавляете логотип, скан печати и подписи.</p></td>
                                            <td style="padding-bottom: 25px;">
                                                <img
                                                    src="<?php echo $message->getDataURI('video_play.png'); ?>">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td width="768" class="spacer"
                                    style="border-bottom-width:0px; width: 728px; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #f6f6f6; border-top-width: 0px; margin-bottom: -3px;">
                                    <table border="0" cellspacing="0"
                                           cellpadding="0"
                                           style="border-bottom: 1px dashed #DEDEDE; margin: 0 95px 0 115px; padding-top: 30px;">
                                        <tr>
                                            <td width="768" class="spacer"
                                                style="border-bottom-width:0px; width: 320px; background-color: #fff; border-left-width: 0px; border-style: solid; border-right-width: 0px; border-color: #f6f6f6; border-top-width: 0px; border-bottom-width: 0;">
                                                <p style="margin: 0; font-size: 12pt; width: 280px;">
                                                    <span
                                                        style="font-weight: bold;">2. Добавление покупателя.</span>
                                                </p>

                                                <p style="margin: 0; font-size: 12pt; width: 280px; padding-bottom: 20px;">
                                                    Вводите ИНН покупателя, БИК его банк и расчетный счет.</p></td>
                                            <td style="padding-bottom: 25px; padding-left: 30px; padding-right: 20px;">
                                                <img
                                                    src="<?php echo $message->getDataURI('macbook_ipad.png'); ?>">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td width="768" class="spacer"
                                    style="border-bottom-width:0px; width: 728px; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #f6f6f6; border-top-width: 0px; margin-bottom: -3px;">
                                    <table border="0" cellspacing="0"
                                           cellpadding="0"
                                           style="border-bottom: 1px dashed #DEDEDE; margin: 0 95px 0 115px; padding-top: 30px;">
                                        <tr>
                                            <td width="768" class="spacer"
                                                style="border-bottom-width:0px; width: 320px; background-color: #fff; border-left-width: 0px; border-style: solid; border-right-width: 0px; border-color: #f6f6f6; border-top-width: 0px; border-bottom-width: 0;">
                                                <p style="margin: 0; font-size: 12pt; width: 280px;">
                                                    <span
                                                        style="font-weight: bold;">3. Выставление счета. </span>
                                                </p>

                                                <p style="margin: 0; font-size: 12pt; width: 280px; padding-bottom: 20px;">
                                                    Выставляйте счета в несколько кликов и отправляйте их на e-mail покупателя.
</p></td>
                                            <td style="padding-bottom: 25px;">
                                                <img
                                                    src="<?php echo $message->getDataURI('video_play.png'); ?>">
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <table width="768" border="0" cellspacing="0"
                               cellpadding="0" class="table"
                               style="background-color:#f6f6f6;">
                            <tr>
                                <td align="left" width="768" class="headerimage"
                                    style="border-bottom-width:0px; width: 728px; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #f6f6f6; border-top-width: 0px; height: 125px; border-bottom-width: 0;"></td>
                            </tr>
                            <tr>
                                <td align="left" width="768" class="headerimage"
                                    style="border-bottom-width: 0; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #f6f6f6; border-top-width: 0px; text-align: center;">
                                    <a href="<?php echo $message->getParam('baseUrl') . Yii::$app->params['uniSender']['utm']['entranceKub']; ?>"
                                       target="_blank"
                                       style="color: #000; font-weight: bold; font-size: 14pt;">ВОЙТИ
                                        В КУБ</a></td>
                            </tr>
                            <tr>
                                <td align="left" width="768" class="headerimage"
                                    style="border-bottom-width:0px; width: 728px; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #f6f6f6; border-top-width: 0px; border-bottom-width: 0;">
                                    <p style="padding-top: 20px; display: block; text-align: center; color: #000; font-size: 12pt; margin-top: -3px; margin-bottom: 0;">
                                        Нужна помощь?</p></td>
                            </tr>
                        </table>
                        <table width="768" border="0" cellspacing="0"
                               cellpadding="0" class="table"
                               style="background-color:#f6f6f6;">
                            <tr>
                                <td width="520" align="center" class="cell">
                                    <table width="768" border="0"
                                           cellspacing="0" cellpadding="0"
                                           class="table16452476">
                                        <tr>
                                            <td align="center" width="246"
                                                class="headerimage"
                                                style="width: 246px; background-color: #fff; border-left-width: 20px; border-style: solid; border-color: #f6f6f6; border-top-width: 0px; border-bottom-width: 0; padding-top: 20px; border-right-color: #fff;">
                                                <img
                                                    src="<?php echo $message->getDataURI('phone.png'); ?>">
                                            </td>
                                            <td align="center" width="246"
                                                class="headerimage"
                                                style="width: 246px; background-color: #fff; border-bottom-width: 0; padding-top: 20px;">
                                                <img
                                                    src="<?php echo $message->getDataURI('mail.png'); ?>">
                                            </td>
                                            <td align="center" width="246"
                                                class="headerimage"
                                                style="width: 246px; background-color: #fff; border-style: solid; border-right-width: 20px; border-color: #f6f6f6; border-top-width: 0px; border-bottom-width: 0; padding-top: 20px; border-left-color: #fff;">
                                                <img
                                                    src="<?php echo $message->getDataURI('web.png'); ?>">
                                            </td>
                                        </tr>
                                    </table>
                                    <table width="768" border="0"
                                           cellspacing="0" cellpadding="0"
                                           class="table16452476">
                                        <tr>
                                            <td align="center" width="246"
                                                class="headerimage"
                                                style="width: 246px; background-color: #fff; border-left-width: 20px; border-style: solid; border-color: #f6f6f6; border-top-width: 0px; border-bottom-width: 0; border-right-color: #fff;">
                                                <p style="margin-bottom: 30px;">
                                                    Позвоните
                                                    нам<br><?php echo $message->getParam('callUs'); ?>
                                                </p></td>
                                            <td align="center" width="246"
                                                class="headerimage"
                                                style="width: 246px; background-color: #fff; border-bottom-width: 0;">
                                                <p style="margin-bottom: 30px;">
                                                    Напишите нам<br><a
                                                        href="mailto:<?php echo $message->getParam('supportEmail'); ?>"><?php echo $message->getParam('supportEmail'); ?></a>
                                                </p></td>
                                            <td align="center" width="246"
                                                class="headerimage"
                                                style="width: 246px; background-color: #fff; border-style: solid; border-right-width: 20px; border-color: #f6f6f6; border-top-width: 0px; border-bottom-width: 0; border-left-color: #fff; padding-bottom: 17px;">
                                                <a href="<?= $message->getParam('feedback') . Yii::$app->params['uniSender']['utm']['leaveMessage']; ?>"
                                                   style="color: #000;">Оставить
                                                    сообщение<br>на сайте</a>
                                            </td>
                                        </tr>
                                    </table>
                                    <table width="768" border="0"
                                           cellspacing="0" cellpadding="0"
                                           class="table16452476">
                                        <tr>
                                            <td align="center" width="246"
                                                class="headerimage"
                                                style="width: 728px; background-color: #fff; border-left-width: 20px; border-right-width: 20px; border-style: solid; border-color: #f6f6f6; border-top-width: 0px; border-bottom-width: 0;">
                                                <a target="_blank"
                                                   href="https://www.facebook.com/sharer/sharer.php?u=http://kub-24.ru"
                                                   style="margin-right: 20px;"><img
                                                        src="<?php echo $message->getDataURI('facebook.png'); ?>"
                                                        style="border-bottom-width:0px; border-left-width: 0px; border-right-width: 0px; border-top-width: 0px;"></a>
                                                <a target="_blank"
                                                   href="http://vk.com/share.php?url=http://kub-24.ru&title=КУБ&image=https://kub-24.ru/wp-content/themes/flat-bootstrap-child/img/logo2.png&noparse=true"
                                                   style="margin-right: 20px;"><img
                                                        src="<?php echo $message->getDataURI('vk.png'); ?>"
                                                        style="border-bottom-width:0px; border-left-width: 0px; border-right-width: 0px; border-top-width: 0px;"></a>
                                                <a target="_blank"
                                                   href="https://vimeo.com/user49227704"
                                                    style="margin-right: 20px;"><img
                                                        src="<?php echo $message->getDataURI('vimeo.png'); ?>"
                                                        style="border-bottom-width:0px; border-left-width: 0px; border-right-width: 0px; border-top-width: 0px;"></a>
                                                <a target="_blank"
                                                   href="https://twitter.com/share?url=http://kub-24.ru&text=КУБ"><img
                                                        src="<?php echo $message->getDataURI('twitter.png'); ?>"
                                                        style="border-bottom-width:0px; border-left-width: 0px; border-right-width: 0px; border-top-width: 0px;"></a>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" width="768"
                                                class="headerimage"
                                                style="width: 728px; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #f6f6f6; border-top-width: 0px; height: 20px; border-bottom-width: 0;"></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <table width="768" border="0" cellspacing="0"
                               cellpadding="0" class="table" style="">
                            <tr>
                                <td width="20"></td>
                                <td width="768" height="7" class="spacer"></td>
                                <td width="20"></td>
                            </tr>
                            <tr>
                                <td width="50"></td>
                                <td width="768" align="left" valign="top"
                                    style="color:#000000;font-family:Arial, Helvetica, sans-serif;line-height:15px;font-size:8pt;"
                                    class="cell">
                                    <p style=" margin-bottom: 10px;">Вы получили
                                        это письмо, потому что подписывались на
                                        новости сервиса КУБ на сайте <a
                                            href="<?php echo $message->getParam('baseUrl'); ?>"
                                            style="color: #055EC3;"><?php echo $message->getParam('domainName'); ?></a>.<br>
                                        У Вас есть возможность отписаться от
                                        рассылки, для этого необходимо перейти в
                                        <a href="<?php echo $message->getParam('baseUrl'); ?>/site/login"
                                           style="color: #055EC3;">личный
                                            кабинет</a>, в раздел настройки и
                                        убрать галочки в блоке «Получение уведомлений».
                                    </p>
                                </td>
                                <td width="20"></td>
                            </tr>
                            <tr>
                                <td width="20"></td>
                                <td width="768" height="7" class="spacer"></td>
                                <td width="20"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<?= \frontend\email\MailTracking::getTag() ?>

</body>
</html>

