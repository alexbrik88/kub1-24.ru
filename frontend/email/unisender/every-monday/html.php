<?php
/**
 * @var $message \common\components\sender\unisender\UniSender
 */
$startWeekTime = strtotime('-7 day');
$startWeek = date('d.m.Y', $startWeekTime);
$endWeek = date('d.m.Y', strtotime('-1 day'));
?>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <title></title></head>
<body>
<table cellpadding="0" class="preview" cellspacing="0" width="100%" border="0"
       style="min-width:768px;">
    <tr>
        <td valign="top" align="center">
            <table width="768" border="0" cellspacing="0" cellpadding="0"
                   class="campaign"
                   style="background-color: #fff;border-bottom-width:4px;border-color:#0077A7;border-style:solid; border-left-width: 0px; border-right-width: 0px; border-top-width: 0px;border-collapse: initial;">
                <tr>
                    <td width="768" height="12" class="campaign"></td>
                </tr>
                <tr>
                    <td width="768" align="left" valign="middle"
                        style="padding-left:20px;font-family:Arial;font-size: 11px;color:#000000;font-family:Arial, Helvetica, sans-serif;font-size:11px;"
                        class="campaign">
                        <a href="<?php echo $message->getParam('baseUrl') . Yii::$app->params['uniSender']['utm']['logo']; ?>">
                            <img src="<?= $message->getDataURI('logo.png'); ?>" style="border-bottom-width:0px; border-left-width: 0px; border-right-width: 0px; border-top-width: 0px;">
                        </a>
                    </td>
                    <td width="768" align="right" valign="middle"
                        style="padding-right:20px;font-family:Arial;font-size: 11px;color:#000000;font-family:Arial, Helvetica, sans-serif;font-size:11px;"
                        class="campaign"><a
                            href="<?php echo $message->getParam('baseUrl') . Yii::$app->params['uniSender']['utm']['entrance']; ?>"
                            target="_blank"
                            style="font-size: 11px;color:#4D7ACC;font-family:Arial, Helvetica, sans-serif;font-size:14pt;"
                            id="watch_offline">Вход</a></td>
                </tr>
                <tr>
                    <td class="campaign" width="768" height="12"></td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" width="768" border="0"
                   style="" bgcolor="#F6F6F6" class="campaign">
                <tr>
                    <td align="center" width="768">
                        <table border="0" cellspacing="0" cellpadding="0"
                               width="768" style="background-color:#f6f6f6;">
                            <tr>
                                <td align="left" width="768" class="headerimage"
                                    style="border-bottom-width:0px; width: 728px; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #f6f6f6; border-top-width: 20px; margin-bottom: -3px;">
                                    <p style="font-size: 24px; padding-left: 30px; margin-top: 20px; line-height: 21px; margin-bottom: 20px; color: #000;">
                                        Ваши недельные результаты!</p></td>
                            </tr>
                            <tr>
                                <td align="left" width="768" class="spacer"
                                    style="border-bottom-width:0px; width: 728px; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #f6f6f6; border-top-width: 0px; margin-bottom: -3px;">
                                    <p style="margin: 0; font-size: 12pt; padding-left: 30px; margin-bottom: 20px;">
                                        Привет, {{Name}}!</p></td>
                            </tr>
                            <tr>
                                <td align="left" width="768" class="spacer"
                                    style="border-bottom-width:0px; width: 728px; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #f6f6f6; border-top-width: 0px; margin-bottom: -3px;">
                                    <p style="margin: 0; font-size: 12pt; padding-left: 30px;">
                                        Итоги вашего бизнеса ({{company}}) за прошедшую неделю:
                                        <i><?= $startWeek . ' - ' . $endWeek; ?></i>
                                    </p></td>
                            </tr>
                            <tr>
                                <td width="768" class="spacer"
                                    style="border-bottom-width:0px; width: 728px; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #f6f6f6; border-top-width: 0px; margin-bottom: -3px;">
                                    <table border="0" cellspacing="0"
                                           cellpadding="0"
                                           style="border-bottom: 0px dashed #DEDEDE; margin-top: 0; margin-left: 55px; margin-bottom: 0; margin-right: 55px;">
                                        <tbody>
                                        <tr>
                                            <td width="768" height="30"
                                                class="spacer"
                                                style="border-bottom-width: 0px;width: 275px;background-color: #ffffff;border-left-width: 0px;border-style: solid;border-right-width: 62px;border-color: #FFFFFF;border-top-width: 0px;height: 30px;"></td>

                                            <td height="30"
                                                style="border-bottom-width: 0px;width: 275px;background-color: #ffffff;border-left-width: 0px;border-style: solid;border-right-width: 0;border-color: #7D7D7D;border-top-width: 0px;height: 30px;"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table border="0" cellspacing="0"
                                           cellpadding="0"
                                           style="border-bottom: 0px dashed #DEDEDE; margin-top: 0; margin-left: 55px; margin-bottom: 0; margin-right: 55px;">
                                        <tr>
                                            <td width="768" class="spacer"
                                                style="border-bottom-width: 0px;width: 275px;background-color: #E0F3FE;border-left-width: 0px;border-style: solid;border-right-width: 62px;border-color: #FFFFFF;border-top-width: 0px;">
                                                <table style="width: 100%;">
                                                    <tbody>
                                                    <tr>
                                                        <td style="border-bottom: 1px;padding-top: 5px;padding-bottom: 5px;font-size: 14px;padding-left: 10px;border-style: solid;border-top: 0;border-left: 0;border-right: 0;border-color: #C3E7FA;">
                                                            <i>Клиенты вам
                                                                оплатили:</i>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <table style="width: 100%;">
                                                    <tbody>
                                                    <tr>
                                                        <td style="border-bottom: 1px;padding-top: 15px;padding-bottom: 25px;font-size: 30px;padding-left: 10px;text-align: center;">
                                                            {{clientInSum}} ₽
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td style="border-bottom-width: 0px;width: 275px;background-color: #E0F3FE;border-left-width: 0px;border-style: solid;border-right-width: 0;border-color: #7D7D7D;border-top-width: 0px;">
                                                <table style="width: 100%;">
                                                    <tbody>
                                                    <tr>
                                                        <td style="border-bottom: 1px;padding-top: 5px;padding-bottom: 5px;font-size: 14px;padding-left: 10px;border-style: solid;border-top: 0;border-left: 0;border-right: 0;border-color: #C3E7FA;">
                                                            <i>Клиенты вам
                                                                должны:</i></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <table style="width: 100%;">
                                                    <tbody>
                                                    <tr>
                                                        <td style="border-bottom: 1px;padding-top: 15px;padding-bottom: 25px;font-size: 30px;padding-left: 10px;text-align: center;">
                                                            {{clientOutSum}} ₽
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <table border="0" cellspacing="0"
                                           cellpadding="0"
                                           style="border-bottom: 1px dashed #DEDEDE; margin-top: 0; margin-left: 55px; margin-bottom: 0; margin-right: 55px;">
                                        <tr>
                                            <td width="768" height="30"
                                                class="spacer"
                                                style="border-bottom-width: 0px;background-color: #ffffff;border-left-width: 0px;border-style: solid;border-right-width: 0;border-color: #FFFFFF;border-top-width: 0px;height: 30px;"></td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td width="768" class="spacer"
                                    style="border-bottom-width:0px; width: 728px; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #f6f6f6; border-top-width: 0px; margin-bottom: -3px;">
                                    <table border="0" cellspacing="0"
                                           cellpadding="0"
                                           style="border-bottom-width: 0px; margin-top: 0; margin-left: 55px; margin-bottom: 0; margin-right: 55px;">
                                        <tbody>
                                        <tr>
                                            <td width="768" height="30"
                                                class="spacer"
                                                style="border-bottom-width: 0px;width: 275px;background-color: #ffffff;border-left-width: 0px;border-style: solid;border-right-width: 62px;border-color: #FFFFFF;border-top-width: 0px;height: 30px;"></td>

                                            <td height="30"
                                                style="border-bottom-width: 0px;width: 275px;background-color: #ffffff;border-left-width: 0px;border-style: solid;border-right-width: 0;border-color: #7D7D7D;border-top-width: 0px;height: 30px;"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table border="0" cellspacing="0"
                                           cellpadding="0"
                                           style="border-bottom: 0px dashed #DEDEDE; margin-top: 0; margin-left: 55px; margin-bottom: 0; margin-right: 55px;">
                                        <tr>
                                            <td width="768" class="spacer"
                                                style="border-bottom-width: 0px;width: 275px;background-color: #ffffff;border-left-width: 0px;border-style: solid;border-right-width: 62px;border-color: #FFFFFF;border-top-width: 0px;">
                                                <table
                                                    style="width: 100%; background-color: #C7E9FF;">
                                                    <tbody>
                                                    <tr>
                                                        <td style="border-bottom: 0;padding-top: 3px;padding-bottom: 3px;font-size: 14px;padding-left: 10px;border-style: solid;border-top: 0;border-left: 0;border-right: 0;border-color: #ffffff;">
                                                            <i>Вы выставили
                                                                счетов на:</i>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <table style="width: 100%;">
                                                    <tbody>
                                                    <tr>
                                                        <td style="border-bottom: 1px;padding-top: 15px;padding-bottom: 30px;font-size: 30px;padding-left: 10px;text-align: center;">
                                                            {{exhibitInvoiceSum}}
                                                            ₽
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td style="border-bottom-width: 0px;width: 275px;background-color: #ffffff;border-left-width: 0px;border-style: solid;border-right-width: 0;border-color: #7D7D7D;border-top-width: 0px;">
                                                <table style="width: 100%;">
                                                    <tbody>
                                                    <tr>
                                                        <td style="border-bottom: 1px;padding-top: 0;padding-bottom: 13px;font-size: 14px;padding-left: 10px;">
                                                            <i>Откладывали
                                                                выставления
                                                                счетов? Это
                                                                легко исправить.
                                                                Начните
                                                                выставлять
                                                                счета, быстрая
                                                                отправка счетов
                                                                - это самый
                                                                быстрый способ
                                                                получить
                                                                деньги.</i></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <table style="width: 100%;">
                                                    <tbody>
                                                    <tr>
                                                        <td style="border-bottom: 1px;padding-top: 0;padding-bottom: 0;font-size: 13px;padding-left: 10px;text-align: left;">
                                                            <a href="<?= $message->getParam('lkBaseUrl') . Yii::$app->params['uniSender']['utm']['billInvoice']; ?>"
                                                               style="text-decoration: none;color: #ffffff;padding-top: 8px;background-color: #0077A7;padding-bottom: 8px;padding-left: 16px;padding-right: 16px;">Выставить
                                                                счет</a></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <table border="0" cellspacing="0"
                                           cellpadding="0"
                                           style="border-bottom: 1px dashed #DEDEDE; margin-top: 0; margin-left: 55px; margin-bottom: 0; margin-right: 55px;">
                                        <tbody>
                                        <tr>
                                            <td width="768" height="30"
                                                class="spacer"
                                                style="border-bottom-width: 0px;background-color: #ffffff;border-left-width: 0px;border-style: solid;border-right-width: 0px;border-color: #FFFFFF;border-top-width: 0px;height: 30px;"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td width="768" class="spacer"
                                    style="border-bottom-width:0px; width: 728px; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #f6f6f6; border-top-width: 0px; margin-bottom: -3px;">
                                    <table border="0" cellspacing="0"
                                           cellpadding="0"
                                           style="border-bottom: 0px dashed #DEDEDE; margin-top: 0; margin-left: 55px; margin-bottom: 0; margin-right: 55px;">
                                        <tbody>
                                        <tr>
                                            <td width="768" height="30"
                                                class="spacer"
                                                style="border-bottom-width: 0px;width: 275px;background-color: #ffffff;border-left-width: 0px;border-style: solid;border-right-width: 62px;border-color: #FFFFFF;border-top-width: 0px;height: 30px;"></td>

                                            <td height="30"
                                                style="border-bottom-width: 0px;width: 275px;background-color: #ffffff;border-left-width: 0px;border-style: solid;border-right-width: 0;border-color: #7D7D7D;border-top-width: 0px;height: 30px;"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                    <table border="0" cellspacing="0"
                                           cellpadding="0"
                                           style="margin-top: 0; margin-left: 55px; margin-bottom: 0; margin-right: 55px;">
                                        <tr>
                                            <td width="768" class="spacer"
                                                style="border-bottom-width: 0px;width: 275px;border-left-width: 0px;border-style: solid;border-right-width: 62px;border-color: #FFFFFF;border-top-width: 0px;">
                                                <table
                                                    style="width: 100%; background-color: #C7E9FF;">
                                                    <tbody>
                                                    <tr>
                                                        <td style="border-bottom: 0;padding-top: 3px;padding-bottom: 3px;font-size: 14px;padding-left: 10px;border-style: solid;border-top: 0;border-left: 0;border-right: 0;border-color: #ffffff;">
                                                            <i>Ваши расходы:</i>
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <table style="width: 100%;">
                                                    <tbody>
                                                    <tr>
                                                        <td style="border-bottom: 1px;padding-top: 35px;padding-bottom: 25px;font-size: 30px;padding-left: 10px;text-align: center;">
                                                            {{expenses}} ₽
                                                        </td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td style="border-bottom-width: 0px;width: 275px;background-color: #ffffff;border-left-width: 0px;border-style: solid;border-right-width: 0;border-color: #7D7D7D;border-top-width: 0px;">
                                                <table style="width: 100%;">
                                                    <tbody>
                                                    <tr>
                                                        <td style="border-bottom: 1px;padding-top: 0;padding-bottom: 13px;font-size: 14px;padding-left: 10px;">
                                                            <i>Вы уверены, что
                                                                ничего не
                                                                покупали на этой
                                                                неделе? Если вы
                                                                пропустили и не
                                                                внесли расходные
                                                                документы, то
                                                                сделайте это
                                                                сейчас. Когда
                                                                нужно будет
                                                                сдавать
                                                                отчетность, они
                                                                вам обязательно
                                                                понадобятся, что
                                                                бы уменьшить
                                                                налоги.</i></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                                <table style="width: 100%;">
                                                    <tbody>
                                                    <tr>
                                                        <td style="border-bottom: 1px;padding-top: 0;padding-bottom: 0;font-size: 13px;padding-left: 10px;text-align: left;">
                                                            <a href="<?= $message->getParam('lkBaseUrl') . Yii::$app->params['uniSender']['utm']['addCosts']; ?>"
                                                               style="text-decoration: none;color: #ffffff;padding-top: 8px;background-color: #0077A7;padding-bottom: 8px;padding-left: 16px;padding-right: 16px;">Внести
                                                                расходы</a></td>
                                                    </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    <table border="0" cellspacing="0"
                                           cellpadding="0"
                                           style="border-bottom: 0px dashed #DEDEDE; margin-top: 0; margin-left: 55px; margin-bottom: 0; margin-right: 55px;">
                                        <tbody>
                                        <tr>
                                            <td width="768" height="30"
                                                class="spacer"
                                                style="border-bottom-width: 0px;background-color: #ffffff;border-left-width: 0px;border-style: solid;border-right-width: 0px;border-color: #FFFFFF;border-top-width: 0px;height: 30px;"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </table>
                        <table width="768" border="0" cellspacing="0"
                               cellpadding="0" class="table"
                               style="background-color:#f6f6f6;">
                            <tr>
                                <td align="left" width="768" class="headerimage"
                                    style="border-bottom-width:0px; width: 728px; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #f6f6f6; border-top-width: 0px; border-bottom-width: 0;">
                                    <p style="padding-top: 20px; display: block; text-align: left; color: #000; font-size: 12pt; margin-top: -3px; margin-bottom: 0;padding-left: 30px;">
                                        По всем вопросам обращайтесь в службу
                                        поддержки <a
                                            href="mailto:<?= $message->getParam('supportEmail'); ?>"><?= $message->getParam('supportEmail'); ?></a>
                                    </p>
                                </td>
                            </tr>
                        </table>
                        <table width="768" border="0" cellspacing="0"
                               cellpadding="0" class="table"
                               style="background-color:#f6f6f6;">
                            <tr>
                                <td align="left" width="768" class="headerimage"
                                    style="border-bottom-width:0px; width: 728px; height: 30px; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #f6f6f6; border-top-width: 0px; border-bottom-width: 0;">
                                    <p></p></td>
                            </tr>
                        </table>
                        <table width="768" border="0" cellspacing="0"
                               cellpadding="0" class="table" style="">
                            <tr>
                                <td width="20"></td>
                                <td width="768" height="7" class="spacer"></td>
                                <td width="20"></td>
                            </tr>
                            <tr>
                                <td width="50"></td>
                                <td width="768" align="left" valign="top"
                                    style="color:#000000;font-family:Arial, Helvetica, sans-serif;line-height:15px;font-size:8pt;"
                                    class="cell">
                                    <p style=" margin-bottom: 10px;">Вы получили
                                        это письмо, потому что подписывались на
                                        новости сервиса КУБ на сайте <a
                                            href="<?= $message->getParam('baseUrl'); ?>"
                                            style="color: #055EC3;"><?= $message->getParam('domainName'); ?></a>.<br>
                                        У Вас есть возможность отписаться от
                                        рассылки, для этого необходимо перейти в
                                        <a href="<?= $message->getParam('baseUrl'); ?>/site/login"
                                           style="color: #055EC3;">личный
                                            кабинет</a>, в раздел настройки и
                                        убрать галочки в блоке «Получение
                                        уведомлений».
                                    </p>
                                </td>
                                <td width="20"></td>
                            </tr>
                            <tr>
                                <td width="20"></td>
                                <td width="768" height="7" class="spacer"></td>
                                <td width="20"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<?= \frontend\email\MailTracking::getTag() ?>

</body>
</html>
