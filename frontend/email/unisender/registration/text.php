<?php
/**
 * @var $message \common\components\sender\unisender\UniSender
 */
?>
Регистрация в сервисе КУБ

Вы зарегистрированы в качестве нового сотрудника компании <?php echo $message->getParam('companyTypeShort'); ?> "<?php echo $message->getParam('companyName'); ?>".

Ваши данные для входа в систему:
Логин: <?php echo $message->getParam('login'); ?>

Пароль: <?php echo $message->getParam('password'); ?>


С уважением, Команда КУБ

ВОЙТИ В КУБ <?php echo $message->getParam('baseUrl'); ?>/login

Нужна помощь?

Позвоните нам <?php echo $message->getParam('callUs'); ?>

Напишите нам <?php echo $message->getParam('supportEmail'); ?>

Оставить сообщение на сайте <?php echo $message->getParam('feedback'); ?>


Вы получили это письмо, потому что подписывались на новости сервиса КУБ на сайте <?php echo $message->getParam('baseUrl'); ?>.
У Вас есть возможность отписаться от рассылки, для этого необходимо перейти в личный кабинет <?php echo $message->getParam('baseUrl'); ?>/login, в раздел настройки и убрать галочки в блоке «Получение уведомлений».
