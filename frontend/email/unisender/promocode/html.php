<?php
/**
 * @var $message \common\components\sender\unisender\UniSender
 */
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <title></title></head>
<body>
<table cellpadding="0" class="preview" cellspacing="0" width="100%" border="0"
       style="min-width:768px;">
    <tr>
        <td valign="top" align="center">
            <table width="768" border="0" cellspacing="0" cellpadding="0"
                   class="campaign"
                   style="background-color: #fff;border-bottom-width:4px;border-color:#0077A7;border-style:solid; border-left-width: 0px; border-right-width: 0px; border-top-width: 0px;border-collapse: initial;">
                <tr>
                    <td width="768" height="12" class="campaign"></td>
                </tr>
                <tr>
                    <td width="768" align="left" valign="middle"
                        style="padding-left:20px;font-family:Arial;font-size: 11px;color:#000000;font-family:Arial, Helvetica, sans-serif;font-size:11px;"
                        class="campaign">
                        <a href="<?php echo $message->getParam('baseUrl') . Yii::$app->params['uniSender']['utm']['logo']; ?>">
                            <img src="<?= $message->getDataURI('logo.png'); ?>" style="border-bottom-width:0px; border-left-width: 0px; border-right-width: 0px; border-top-width: 0px;">
                        </a>
                    </td>
                    <td width="768" align="right" valign="middle"
                        style="padding-right:20px;font-family:Arial;font-size: 11px;color:#000000;font-family:Arial, Helvetica, sans-serif;font-size:11px;"
                        class="campaign"><a
                            href="<?php echo $message->getParam('baseUrl') . Yii::$app->params['uniSender']['utm']['entrance']; ?>"
                            target="_blank"
                            style="font-size: 11px;color:#4D7ACC;font-family:Arial, Helvetica, sans-serif;font-size:14pt;"
                            id="watch_offline">Вход</a></td>
                </tr>
                <tr>
                    <td class="campaign" width="768" height="12"></td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" width="768" border="0"
                   style="" bgcolor="#F6F6F6" class="campaign">
                <tr>
                    <td align="center" width="768">
                        <table border="0" cellspacing="0" cellpadding="0"
                               width="768" style="background-color:#f6f6f6;">
                            <tr>
                                <td align="left" width="768" class="headerimage"
                                    style="border-bottom-width: 0; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #f6f6f6; border-top-width: 20px; margin-bottom: -3px;">
                                    <p style="font-size: 24pt; padding-left: 30px; margin-top: 20px; line-height: 21px; margin-bottom: 20px; color: #000;">
                                        Вам назначен промокод</p></td>
                            </tr>
                            <tr>
                                <td align="left" width="768" class="spacer"
                                    style="border-bottom-width: 0; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #f6f6f6; border-top-width: 0px; margin-bottom: -3px;">
                                    <p style="margin: 0; font-size: 12pt; padding-left: 30px; margin-bottom: 20px;">
                                        Здравствуйте, {{Name}}!</p></td>
                            </tr>
                            <tr>
                                <td align="left" width="768" class="spacer"
                                    style="border-bottom-width: 0; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #f6f6f6; border-top-width: 0px; margin-bottom: -3px;">
                                    <p style="margin: 0; font-size: 12pt; padding-left: 30px; margin-bottom: 20px;">
                                        Вам назначен промокод: <span
                                            style="font-weight: bold;"><?php echo $message->getParam('promoCode')->code; ?></span>
                                    </p></td>
                            </tr>
                            <tr>
                                <td align="left" width="768" class="spacer"
                                    style="border-bottom-width: 0; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #f6f6f6; border-top-width: 0px; margin-bottom: -3px;">
                                    <p style="margin: 0; font-size: 12pt; padding-left: 30px;">
                                        Пройдите по ссылке, что бы ввести
                                        промокод: <a
                                            href="<?php echo $message->getParam('baseUrl'); ?>/subscribe"
                                            target="_blank"
                                            style="color: #2E41BA">
                                            <?php echo $message->getParam('baseUrl'); ?>/subscribe</a></p></td>
                            </tr>
                            <tr>
                                <td align="left" width="768" class="spacer"
                                    style="border-bottom-width: 0; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #f6f6f6; border-top-width: 0px; margin-bottom: -3px;">
                                    <p style="margin: 0; font-size: 12pt; padding-left: 30px; margin-bottom: 20px;">
                                        Если ссылка не работает, скопируйте и
                                        вставьте URL в адресную строку в новом
                                        окне браузера.</p></td>
                            </tr>
                            <tr>
                                <td align="left" width="768" class="spacer"
                                    style="border-bottom-width: 0; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #f6f6f6; border-top-width: 0px; margin-bottom: -3px;">
                                    <p style="margin: 0; font-size: 12pt; padding-left: 30px; margin-bottom: 40px;">
                                        По всем вопросам обращайтесь в службу
                                        поддержки <?php echo $message->getParam('supportEmail'); ?></p>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="768" class="spacer"
                                    style="border-bottom-width: 0; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #f6f6f6; border-top-width: 0px; margin-bottom: -3px;">
                                    <p style="margin: 0; font-size: 12pt; padding-left: 30px; margin-bottom: 140px; font-weight: bold;">
                                        С уважением,<br> Команда КУБ</p></td>
                            </tr>
                        </table>
                        <table width="768" border="0" cellspacing="0"
                               cellpadding="0" class="table" style="">
                            <tr>
                                <td width="20"></td>
                                <td width="768" height="7" class="spacer"></td>
                                <td width="20"></td>
                            </tr>
                            <tr>
                                <td width="50"></td>
                                <td width="768" align="left" valign="top"
                                    style="color:#000000;font-family:Arial, Helvetica, sans-serif;line-height:15px;font-size:8pt;"
                                    class="cell">
                                    <p style=" margin-bottom: 10px;">Вы получили
                                        это письмо, потому что подписывались на
                                        новости сервиса КУБ на сайте <a
                                            href="<?php echo $message->getParam('baseUrl'); ?>"
                                            target="_blank"
                                            style="color: #055EC3;"><?php echo $message->getParam('domainName'); ?></a>.<br>
                                        У Вас есть возможность отписаться от
                                        рассылки, для этого необходимо перейти в
                                        <a href="<?php echo $message->getParam('baseUrl'); ?>/site/login"
                                           target="_blank"
                                           style="color: #055EC3;">личный
                                            кабинет</a>, в раздел настройки и
                                        убрать галочки в блоке «Получение уведомлений».
                                    </p>
                                </td>
                                <td width="20"></td>
                            </tr>
                            <tr>
                                <td width="20"></td>
                                <td width="768" height="7" class="spacer"></td>
                                <td width="20"></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<?= \frontend\email\MailTracking::getTag() ?>

</body>
</html>
