<?php
/**
 * @var $message \common\components\sender\unisender\UniSender
 */
?>
Здравствуйте, {{Name}}!

Вам назначен промокод: <?php echo $message->getParam('promoCode')->code; ?>

Пройдите по ссылке, что бы ввести промокод: <?php echo $message->getParam('lkBaseUrl'); ?>/subscribe

По всем вопросам обращайтесь в службу поддержки <?php echo $message->getParam('supportEmail'); ?>

С уважением, Команда КУБ

Вы получили это письмо, потому что подписывались на новости сервиса КУБ на сайте <?php echo $message->getParam('baseUrl'); ?>.
У Вас есть возможность отписаться от рассылки, для этого необходимо перейти в личный кабинет <?php echo $message->getParam('baseUrl'); ?>/login, в раздел настройки и убрать галочки в блоке «Получение уведомлений».