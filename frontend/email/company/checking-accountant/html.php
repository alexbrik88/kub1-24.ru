<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 04.08.2016
 * Time: 7:55
 */

use yii\helpers\Html;
use common\models\company\ApplicationToBank;

/* @var \yii\web\View $this */
/* @var \yii\mail\BaseMessage $message */
/* @var $application ApplicationToBank */
?>
<html>
<head>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <title></title>
</head>
<body style="margin: 0px">
<table cellpadding="0" class="preview" cellspacing="0" width="100%" border="0" style="min-width:768px; font-family: sans-serif;">
    <tr>
        <td valign="top" align="center">
            <table width="768" border="0" cellspacing="0" cellpadding="0" class="campaign" style="background-color: #ffffff; border-width: 0; border-top-width: 0; border-left-width: 0; border-right-width: 0;  border-bottom: solid 4px #0077A7;">
                <tr>
                    <td width="768" align="left" valign="middle" style="padding-left:20px; font-family:Arial; font-size: 11px; color:#000000; font-family:Arial, Helvetica, sans-serif; font-size:11px; padding-left: .8cm;" class="campaign">

                            <p style="font-size: 24px; padding-left: 0px; margin-top: 15px; line-height: 21px; margin-bottom: 5px; color: #000;"><?= $application->company_name; ?></p>
                    </td>
                </tr>
                <tr>
                    <td class="campaign" width="768" height="12"></td>
                </tr>
            </table>
            <table cellspacing="0" cellpadding="0" width="768" border="0" style="" bgcolor="#F6F6F6" class="campaign">
                <tr>
                    <td align="center" width="768">
                        <table cellspacing="0" cellpadding="0" width="768" style="background-color: #f6f6f6; border: 20px solid #f6f6f6;">
                            <tr>
                                <td colspan="2" align="left"  class="headerimage" style="width: 100%; border-bottom-width: 0; background-color: #ffffff; border-style: solid; border-right-width: 20px; border-color: #ffffff;">
                                    <p style="font-size: 24px; margin-top: 20px; line-height: 21px; margin-bottom: 20px; color: #000; margin-left: 0.6cm">Заявка на открытие расчетного счета</p>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="50%" style="background-color: #ffffff; border: 0;">
                                    <p style="margin: 0; font-size: 14px; margin-bottom: 20px; margin-left: 0.6cm;">Название компании:</p>
                                </td>
                                <td align="left" width="50%" style="border-right: 20px solid #ffffff; background-color: #ffffff;">
                                    <p style="margin: 0; margin-left: 3px; font-size: 14px; margin-bottom: 20px; "><b><?= $application->company_name; ?></b></p>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="50%" style="background-color: #ffffff; border: 0;">
                                    <p style="margin: 0; font-size: 14px; margin-bottom: 20px; margin-left: 0.6cm;">ИНН:</p>
                                </td>
                                <td align="left" width="50%" style="border-right: 20px solid #ffffff; background-color: #ffffff;">
                                    <p style="margin: 0; margin-left: 3px; font-size: 14px; margin-bottom: 20px; "><b><?= $application->inn; ?></b></p>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="50%" style="background-color: #ffffff; border: 0;">
                                    <p style="margin: 0; font-size: 14px; margin-bottom: 20px; margin-left: 0.6cm;">Юр.Адрес:</p>
                                </td>
                                <td align="left" width="50%" style="border-right: 20px solid #ffffff; background-color: #ffffff;">
                                    <p style="margin: 0; margin-left: 3px; font-size: 14px; margin-bottom: 20px; "><b><?= $application->legal_address; ?></b></p>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="50%" style="background-color: #ffffff; border: 0;">
                                    <p style="margin: 0; font-size: 14px; margin-bottom: 20px; margin-left: 0.6cm;">ФИО:</p>
                                </td>
                                <td align="left" width="50%" style="border-right: 20px solid #ffffff; background-color: #ffffff;">
                                    <p style="margin: 0; margin-left: 3px; font-size: 14px; margin-bottom: 20px; "><b><?= $application->fio; ?></b></p>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="50%" style="background-color: #ffffff; border: 0;">
                                    <p style="margin: 0; font-size: 14px; margin-bottom: 20px; margin-left: 0.6cm;">Контактный телефон:</p>
                                </td>
                                <td align="left" width="50%" style="border-right: 20px solid #ffffff; background-color: #ffffff;">
                                    <p style="margin: 0; margin-left: 3px; font-size: 14px; margin-bottom: 20px; "><b><?= $application->contact_phone; ?></b></p>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" width="50%" style="background-color: #ffffff; border: 0;">
                                    <p style="margin: 0; font-size: 14px; margin-bottom: 20px; margin-left: 0.6cm;">Банк:</p>
                                </td>
                                <td align="left" width="50%" style="border-right: 20px solid #ffffff; background-color: #ffffff;">
                                    <p style="margin: 0; margin-left: 3px; font-size: 14px; margin-bottom: 20px; "><b><?= $application->bank->bank_name; ?></b></p>
                                </td>
                            </tr>

                            <tr>
                                <td colspan="2" align="left"  height="100" class="spacer" style="border-bottom-width: 0; background-color: #fff; border-left-width: 20px; border-style: solid; border-right-width: 20px; border-color: #ffffff; border-top-width: 0px; margin-bottom: -3px;"></td>
                            </tr>
                            <tr>
                                <td colspan="2" align="left" style="width: 100%; border-bottom-width: 0; background-color: #ffffff; border-style: solid; border-right-width: 20px; border-color: #ffffff; border-top-width: 0px;">
                                    <p style="margin: 0; font-size: 14px; margin-bottom: 25px; font-weight: bold; margin-left: 0.6cm;">С уважением,<br> Команда КУБ</p>
                                </td>
                            </tr>
                        </table>

                        <table width="768" border="0" cellspacing="0" cellpadding="0" class="table" style="">

                            <tr>
                                <td height="20"></td>
                                <td></td>
                            </tr>
                            <tr>
                                <td width="80%" align="left" valign="top" style="color:#000000;font-family:Arial, Helvetica, sans-serif;line-height:15px;font-size:12px;" class="cell">
                                    <p style="margin-left: 0.6cm; margin-bottom: 10px;">Это письмо было отправлено через он-лайн сервис для предпринимателей КУБ<br>
                                        <?= Html::a('Попробовать бесплатно', Yii::$app->params['serviceSite'] . '?utm_source=letter&utm_medium=email&utm_campaign=vizitka', [
                                            'target' => '_blank',
                                            'style' => 'color: #055EC3;',
                                        ]); ?>
                                    </p>
                                </td>
                                <td width="20%" align="right" valign="middle" style="border-right: 20px solid #f6f6f6; font-family:Arial;font-size: 11px;color:#000000;font-family:Arial, Helvetica, sans-serif;font-size:11px;" class="campaign"><?= Html::a(Html::img($message->embed(Yii::getAlias('@frontend/email/company/checking-accountant/assets/logo.png')), [
                                        'style' => 'style="border-bottom-width:0px; border-left-width: 0px; border-right-width: 0px; border-top-width: 0px;',
                                    ]), Yii::$app->params['serviceSite'], [
                                        'target' => '_blank',
                                    ]); ?></td>
                            </tr>
                            <tr>
                                <td height="7"></td>
                                <td></td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<?= \frontend\email\MailTracking::getTag() ?>

</body>
</html>
