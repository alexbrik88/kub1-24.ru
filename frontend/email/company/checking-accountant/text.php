<?php
/**
 * Created by PhpStorm.
 * User: Валик
 * Date: 04.08.2016
 * Time: 8:15
 */

use common\models\company\ApplicationToBank;

/* @var \yii\web\View $this */
/* @var \yii\mail\BaseMessage $message */
/* @var $application ApplicationToBank */
?>
<?= $application->company_name; ?>
Заявка на открытие расчетного счета
Название компании:

<?= $application->company_name; ?>

ИНН: <?= $application->inn; ?>

Юр.Адрес: <?= $application->legal_address; ?>

ФИО: <?= $application->fio; ?>

Контактный телефон: <?= $application->contact_phone; ?>

Банк: <?= $application->bank->bank_name; ?>



С уважением,
Команда КУБ

Это письмо было отправлено через он-лайн сервис для предпринимателей КУБ
Попробовать бесплатно

