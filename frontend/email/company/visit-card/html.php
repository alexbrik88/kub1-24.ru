<?php
/**
 * Created by Konstantin Timoshenko
 * Date: 13.10.15
 * Time: 12.27
 * Email: t.kanstantsin@gmail.com
 */
use yii\helpers\Html;
use common\models\company\CheckingAccountant;

/* @var \yii\web\View $this */
/* @var \yii\mail\BaseMessage $message */
/* @var \common\models\Company $company */
/* @var common\models\employee\Employee $model */

$checkingAccountant = new CheckingAccountant;
$mainCheckingAccountant = $company->mainCheckingAccountant;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title></title>
</head>
<body>
<div
    style="font-family: Arial; margin-left: auto; margin-right: auto; display: block; width: 1000px;">
    <table style="width: 100%;">
        <tbody>
        <tr>
            <td colspan="2">
                <table
                    style="color: #ffffff; background: #45b6af;vertical-align:middle;width:100%;">
                    <tbody>
                    <tr>
                        <td style="text-align: right;">
                            Это письмо было отправлено при помощи онлайн сервиса
                        </td>
                        <td style="width: 100px; text-align: center;">
                            <?= Html::img($message->embed(Yii::getAlias(Yii::$app->params['email']['logoWhite']))); ?>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table style="margin: 15px;">
                    <tbody>
                    <tr>
                        <td colspan="2">
                            <h3>
                                <b><?php echo $company->getTitle(true); ?></b>
                            </h3>
                        </td>
                    </tr>
                    <tr>
                        <th>Система налогообложения:</th>
                        <td><?= $company->companyTaxationType->name; ?></td>
                    </tr>

                    <tr>
                        <th>&nbsp;</th>
                    </tr>

                    <tr>
                        <th style="text-align: left">Юридический адрес:</th>
                        <td><?= $company->getAddressLegalFull(); ?></td>
                    </tr>
                    <tr>
                        <th style="text-align: left">Фактический адрес:</th>
                        <td><?= $company->getAddressActualFull(); ?></td>
                    </tr>
                    </tbody>

                    <tr>
                        <th>&nbsp;</th>
                    </tr>

                    <?php if ($company->company_type_id == \common\models\company\CompanyType::TYPE_IP): ?>
                        <tr>
                            <th style="text-align: left"><?= $company->getAttributeLabel('egrip'); ?>
                                :
                            </th>
                            <td><?= $company->egrip; ?></td>
                        </tr>
                        <tr>
                            <th style="text-align: left"><?= $company->getAttributeLabel('inn'); ?>
                                :
                            </th>
                            <td><?= $company->inn; ?></td>
                        </tr>
                        <tr>
                            <th style="text-align: left"><?= $company->getAttributeLabel('okved'); ?>
                                :
                            </th>
                            <td><?= $company->okved; ?></td>
                        </tr>
                    <?php else: ?>
                        <tr>
                            <th style="text-align: left"><?= $company->getAttributeLabel('ogrn'); ?>
                                :
                            </th>
                            <td><?= $company->ogrn; ?></td>
                        </tr>
                        <tr>
                            <th style="text-align: left"><?= $company->getAttributeLabel('inn'); ?>
                                :
                            </th>
                            <td><?= $company->inn; ?></td>
                        </tr>
                        <tr>
                            <th style="text-align: left"><?= $company->getAttributeLabel('kpp'); ?>
                                :
                            </th>
                            <td><?= $company->kpp; ?></td>
                        </tr>
                        <tr>
                            <th style="text-align: left"><?= $company->getAttributeLabel('okved'); ?>
                                :
                            </th>
                            <td><?= $company->okved; ?></td>
                        </tr>
                    <?php endif; ?>

                    <tr>
                        <th>&nbsp;</th>
                    </tr>

                    <tr>
                        <th style="text-align: left"><?= $checkingAccountant->getAttributeLabel('rs'); ?>
                            :
                        </th>
                        <td><?= $mainCheckingAccountant? $mainCheckingAccountant->rs: ''; ?></td>
                    </tr>
                    <tr>
                        <th style="text-align: left"><?= $checkingAccountant->getAttributeLabel('bank_name'); ?>
                            :
                        </th>
                        <td><?= $mainCheckingAccountant? $mainCheckingAccountant->bank_name: ''; ?></td>
                    </tr>
                    <tr>
                        <th style="text-align: left"><?= $checkingAccountant->getAttributeLabel('ks'); ?>
                            :
                        </th>
                        <td><?= $mainCheckingAccountant? $mainCheckingAccountant->ks: ''; ?></td>
                    </tr>
                    <tr>
                        <th style="text-align: left"><?= $checkingAccountant->getAttributeLabel('bik'); ?>
                            :
                        </th>
                        <td><?= $mainCheckingAccountant? $mainCheckingAccountant->bik: ''; ?></td>
                    </tr>

                    <?php if ($company->company_type_id != \common\models\company\CompanyType::TYPE_IP): ?>
                        <tr>
                            <th>&nbsp;</th>
                        </tr>

                        <?php if ($company->chief_post_name) : ?>
                            <tr>
                                <th style="text-align: left"><?= mb_convert_case($company->chief_post_name, MB_CASE_TITLE, 'UTF-8'); ?>
                                    :
                                </th>
                                <td><?= $company->getChiefFio(); ?></td>
                            </tr>
                        <?php endif; ?>
                        <tr>
                            <th style="text-align: left">Главный бухгалтер:</th>
                            <td><?= $company->getChiefAccountantFio(); ?></td>
                        </tr>
                    <?php endif; ?>

                    <tr>
                        <th>&nbsp;</th>
                    </tr>

                    <tr>
                        <th style="text-align: left"><?= $company->getAttributeLabel('phone'); ?>
                            :
                        </th>
                        <td><?= $company->phone; ?></td>
                    </tr>
                </table>
            </td>
            <td style="width: 241px;">

                <div style="background: #45b6af;width: 241px;padding: 10px 10px 10px 18px;">
                    <table
                        style="color: #ffffff;vertical-align: top;">
                        <tbody>
                        <tr>
                            <td colspan="2" style="text-align: center;padding-top: 10px;padding-bottom: 8px">
                                <h2 style="margin: 0;">Что такое КУБ?</h2>
                            </td>
                        </tr>
                        <?php foreach ([
                                           ['img/invoice.png', 'Подготовка счетов в 2 клика'],
                                           ['img/mailing.png', 'Отправка счетов по e-mail'],
                                           ['img/invoice-check.png', 'Изменение статуса счета'],
                                           ['img/archive.png', 'Упорядоченное хранение документов'],
                                           ['img/download.png', 'Выгрузка документов бухгалтеру'],
                                       ] as $item): ?>
                            <tr>
                                <td style="padding-bottom: 8px"><?= Html::img($message->embed(Yii::getAlias('@frontend/email/company/visit-card/assets/' . $item[0]))); ?></td>
                                <td style="padding-left: 10px; padding-bottom: 8px"><?= $item[1]; ?></td>
                            </tr>
                        <?php endforeach; ?>
                        <tr>
                            <td colspan="2" style="text-align: center;padding-top: 4px;padding-bottom: 10px">
                                <?= Html::a('Попробовать бесплатно', Yii::$app->params['serviceSite'] . '?utm_source=letter&utm_medium=email&utm_campaign=vizitka', [
                                    'target' => '_blank',
                                    'style' => 'display:block; text-decoration: none; color: #ffffff; background-color: #ffb848; padding: 7px;',
                                ]); ?>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>

            </td>
        </tr>
        </tbody>
    </table>
</div>

<?= \frontend\email\MailTracking::getTag() ?>

</body>
</html>