<?php

use yii\helpers\Html;

/* @var $model yii\db\ActiveRecord */
/* @var $items array */
/* @var $sortingItems array */
?>
    <?= Html::a('', '#', [
        'class' => 'small-btn-mts dot',
        'data-toggle' => 'dropdown',
        'aria-expanded' => 'false',
    ]) ?>
    <ul id="config_items_box" class="dropdown-menu" role="menu">
        <li>
            <label>Столбцы</label>
        </li>
        <?php foreach ($items as $item) : ?>
            <?php if (!empty($item['attribute']) && (!array_key_exists('visible', $item) || $item['visible'])) : ?>
                <li>
                    <?= Html::activeCheckbox($model, $item['attribute'], [
                        'data-target' => 'col_' . $item['attribute'],
                        'label' => isset($item['label']) ? $item['label'] : $model->getAttributeLabel($item['attribute']),
                        'labelOptions' => ['class' => 'red']
                    ]); ?>
                </li>
            <?php endif ?>
        <?php endforeach ?>
        <?php if (!empty($sortingItems)): ?>
            <li>
                Сортировка по умолчанию <br>
                по столбцу:
            </li>
        <?php endif; ?>
        <?php foreach ($sortingItems as $sortingItem): ?>
            <?php if (!empty($sortingItem['attribute']) && (!array_key_exists('visible', $sortingItem) || $sortingItem['visible'])) : ?>
                <li>
                    <?= Html::radio($sortingItem['attribute'], $sortingItem['checked'], [
                        'class' => 'sorting-table-config-item',
                        'id' => $sortingItem['attribute'],
                        'data-target' => 'col_' . $sortingItem['attribute'],
                        'label' => isset($sortingItem['label']) ? $sortingItem['label'] : $sortingItem['attribute'],
                    ]); ?>
                </li>
            <?php endif; ?>
        <?php endforeach; ?>
    </ul>
