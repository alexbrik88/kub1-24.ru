<?php
use common\models\document\status\InvoiceStatus;
use frontend\modules\documents\components\InvoiceStatistic;
use yii\helpers\Url;

/* @var $ioType int */
/* @var $contractorId int */
/* @var $contractorStatus int */
/* @var $outerClass string */
?>

<?= \frontend\modules\documents\widgets\Statistic::widget([
    'items' => [
        InvoiceStatistic::NOT_PAID => [
            'outerClass' => $outerClass,
            'innerClass' => '_yellow',
            'text' => 'Не оплачено ВСЕГО',
            'url' => !$clickable ? null : Url::to([
                '/documents/invoice/index',
                'type' => $ioType,
                'InvoiceSearch[invoice_status_id]' => implode(',', InvoiceStatistic::$invoiceStatuses[InvoiceStatistic::NOT_PAID][$ioType])
            ]),
            'num' => '1',
        ],

        InvoiceStatistic::NOT_PAID_IN_TIME => [
            'outerClass' => $outerClass,
            'innerClass' => '_red',
            'text' => 'Не оплачено в срок',
            'url' => !$clickable ? null : Url::to([
                '/documents/invoice/index',
                'type' => $ioType,
                'InvoiceSearch[invoice_status_id]' => implode(',', InvoiceStatistic::$invoiceStatuses[InvoiceStatistic::NOT_PAID_IN_TIME][$ioType]),
            ]),
            'num' => '2',
        ],

        InvoiceStatistic::PAID => [
            'outerClass' => $outerClass,
            'innerClass' => '_green',
            'text' => 'Оплачено',
            'url' => !$clickable ? null : Url::to([
                '/documents/invoice/index',
                'type' => $ioType,
                'InvoiceSearch[invoice_status_id]' => implode(',', InvoiceStatistic::$invoiceStatuses[InvoiceStatistic::PAID][$ioType]),
            ]),
            'num' => '3',
        ],
    ],

    'type' => $ioType,
    'contractorId' => $contractorId,
    'contractorStatus' => $contractorStatus,
    'invoiceStatusId' => $invoiceStatusId,
    'searchModel' => $searchModel,
]); ?>
