<?php
use common\components\TextHelper;

$num = ($innerClass == '_yellow' ? 1 : ($innerClass == '_red' ? 2 : 3));

/* @var $outerClass string */
/* @var $innerClass string */
/* @var $text string */
/* @var $sum float */
/* @var $count int */
/* @var $num int */
?>

<div class="<?= $outerClass ?> <?= $innerClass ?>">
    <div class="sum" data-value="<?= $sum ?>"><?= TextHelper::invoiceMoneyFormat($sum, 2); ?>₽</div>
    <div class="text"><?= $text; ?></div>
    <div class="text_bot">Количество счетов: <div style="float:right"><?= $count; ?></div></div>
    <?php /* if (!empty($url)) : ?>
        <a href="<?= $url ?>" style="display: inline-block; position: absolute; top: 0; bottom: 0; left: 0; right: 0;"></a>
    <?php endif; */ ?>
</div>

