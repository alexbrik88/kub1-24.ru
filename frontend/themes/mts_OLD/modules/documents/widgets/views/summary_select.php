<?php

use yii\bootstrap\Dropdown;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $widget frontend\modules\documents\widgets\SummarySelectWidget */

?>

<div id="bottom_but" class="bottom_but hidden">
    <div class="checker-wrap">
        <input type="checkbox" class="joint-operation-main-checkbox" name="" value="1">
        <label for="option"></label>
    </div>
    <div class="total-count-select">Выбрано: <span>0</span></div>
    <div class="total-amount-select">Сумма: <span>0</span></div>
    <div class="actions-many-items">
        <?php foreach ($widget->buttons as $key => $button) : ?>
            <?= $button ?>
        <?php endforeach; ?>
    </div>
</div>

<script>
    $(function(){
        $('.joint-operation-checkbox').on('change', function(){
            var countChecked = 0;
            var summary = 0;
            $('.joint-operation-checkbox:checked').each(function(){
                var price = $(this).closest('tr').find('.price').data('price');
                price = price.replace(",",".");
                summary += parseFloat(price);
                countChecked++;
            });
            if (countChecked > 0) {
                $('#bottom_but').removeClass('hidden');
                $('#bottom_but .total-count-select span').text(countChecked);
                $('#bottom_but .total-amount-select span').text(number_format(summary, 2, ',', ' '));
            } else {
                $('#bottom_but').addClass('hidden');
            }
        });
    });
</script>