<?php

use frontend\models\Documents;
use frontend\modules\documents\assets\TooltipAsset;
use frontend\modules\documents\components\Message;
use frontend\rbac\permissions;
use frontend\widgets\ConfirmModalWidget;
use philippfrenzel\yii2tooltipster\yii2tooltipster;
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\document\Invoice */
/* @var $message Message */
/* @var $ioType integer */
/* @var $useContractor string */
/* @var $invoiceContractorSignature \common\models\document\InvoiceContractorSignature */

$dateFormatted = \php_rutils\RUtils::dt()->ruStrFTime([
    'format' => 'd F Y г.',
    'monthInflected' => true,
    'date' => $model->document_date,
]);

$this->registerAssetBundle(TooltipAsset::className(), $this::POS_END);
$this->title = $message->get(Message::TITLE_SINGLE) . ' №' . $model->fullNumber . ' от ' . $dateFormatted;
$this->context->layoutWrapperCssClass = 'out-invoice out-document';

$backUrl = null;
if ($useContractor && Yii::$app->user->can(permissions\Contractor::VIEW)) {
    if (!isset($contractorTab)) $contractorTab = null;
    $backUrl = ['/contractor/view', 'type' => $model->type, 'id' => $model->contractor_id, 'tab' => $contractorTab];
} elseif (Yii::$app->user->can(permissions\document\Document::INDEX, ['ioType' => $ioType])) {
    $backUrl = (\Yii::$app->request->referrer && strpos(\Yii::$app->request->referrer, 'documents/invoice/index')) ?
        \Yii::$app->request->referrer :
        ['index', 'type' => $model->type,];
}
$showSendPopup = Yii::$app->session->remove('show_send_popup');
?>

<?= Yii::$app->session->getFlash('invoiceFactureError'); ?>

<div class="invoice-view" style="margin-bottom: 110px;">
    <?php if ($backUrl !== null) : ?>
        <div>
            <?= Html::a('Назад к списку', $backUrl); ?>
        </div>
    <?php endif; ?>

    <?php if ($model->is_deleted): ?>
        <p class="text-warning">Счёт удалён</p>
    <?php endif; ?>

    <div class="light-box" style="margin-top: 15px;">
        <div style="position: relative;">
            <div style="margin-right: 250px;">
                <?= $this->render('/invoice/pre-view-' . Documents::$ioTypeToUrl[$ioType], [
                    'model' => $model,
                    'ioType' => $model->type,
                    'orders' => $model->orders,
                    'useContractor' => $useContractor,
                    'invoiceContractorSignature' => $invoiceContractorSignature,
                ]); ?>
            </div>
            <div style="width: 235px; position: absolute; top: 0; right: 0;">
                <?= $this->render('/invoice/view/_status_block', [
                    'model' => $model,
                    'ioType' => $model->type,
                    'useContractor' => $useContractor,
                ]); ?>

                <?= $this->render('/invoice/view/_main_info', [
                    'model' => $model,
                    'ioType' => $model->type,
                    'message' => $message,
                    'dateFormatted' => $dateFormatted,
                    'useContractor' => $useContractor,
                    'newCompanyTemplate' => true,
                    'invoiceContractorSignature' => $invoiceContractorSignature,
                ]); ?>
            </div>
        </div>
    </div>

    <div id="buttons-bar-fixed">
        <div class="mts_container">
            <div class="main_part">
                <?= $this->render('/invoice/_action_buttons_' . Documents::$ioTypeToUrl[$ioType], [
                    'model' => $model,
                    'useContractor' => $useContractor,
                    'showSendPopup' => $showSendPopup,
                ]); ?>
            </div>
        </div>d
    </div>
</div>

<?php if ($model->invoiceStatus->sendAllowed()): ?>
    <?= $this->render('/invoice/view/_send_message', [
        'model' => $model,
        'useContractor' => $useContractor,
        'showSendPopup' => $showSendPopup,
    ]); ?>
<?php endif; ?>
<?php
if (Yii::$app->user->can(permissions\document\Document::DELETE)) {
    echo ConfirmModalWidget::widget([
        'options' => [
            'id' => 'delete-confirm',
        ],
        'toggleButton' => false,
        'confirmUrl' => Url::toRoute(['delete', 'type' => $model->type, 'id' => $model->id]),
        'confirmParams' => [],
        'message' => "Вы уверены, что хотите удалить исходящий счёт?",
    ]);
}

echo $this->render('/invoice-facture/_form_create', [
    'invoice' => $model,
    'useContractor' => $useContractor,
]);

if ($model->invoiceStatus->paymentAllowed($model->type)
    && Yii::$app->user->can(permissions\document\Invoice::ADD_CASH_FLOW)
) {
    echo $this->render('/invoice/_modal_add_flow', [
        'model' => $model,
        'useContractor' => $useContractor,
    ]);
}

echo ConfirmModalWidget::widget([
    'options' => [
        'id' => 'payment-order',
    ],
    'toggleButton' => false,
    'confirmUrl' => Url::toRoute(['create-payment-order', 'type' => $model->type, 'id' => $model->id]),
    'confirmParams' => [],
    'message' => "Вы уверены, что хотите подготовить платежку в банк?",
]); ?>

<?php echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip3',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'custom',
        'side' => 'right',
        'zIndex' => 10000,
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-bottom',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'custom',
        'side' => 'bottom',
        'zIndex' => 10000,
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-right',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'custom',
        'side' => 'right',
        'zIndex' => 10000,
    ],
]);
echo \philippfrenzel\yii2tooltipster\yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip2-left',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'side' => 'left',
        'zIndex' => 10000,
    ],
]);
echo yii2tooltipster::widget([
    'options' => [
        'class' => '.tooltip-hover',
    ],
    'clientOptions' => [
        'theme' => ['tooltipster-kub'],
        'trigger' => 'hover',
        'zIndex' => 10000,
    ],
]);
$urlActivateScanPopup = \yii\helpers\Url::to(['/documents/invoice/run-scan-popup']);
$canShowScan = Yii::$app->user->identity->show_scan_popup;
$this->registerJs(<<<JS
setTimeout(function() {
    $('.tooltipstered-main').tooltipster('open');
}, 1000);
$('#send').on('shown.bs.modal', function() {
    $('.tooltip-self-send').tooltipster({
        "theme":["tooltipster-noir","tooltipster-noir-customized"],
        "trigger":"custom",
        "side":"right",
        "zIndex":$.topZIndex('.modal-scrollable'),
    });
    setTimeout(function(){
        $('.tooltip-self-send').tooltipster('open');
    }, 500);
});
$('#send').on('hide.bs.modal', function() {
    if ($canShowScan) {
        $.post("{$urlActivateScanPopup}", function(){
            location.reload();
        });
    }
    $('.tooltip-self-send').tooltipster('destroy');
});
$(document).on('click','.btn-close-tooltip-templates',function() {
    var name = $(this).closest('.box-tooltip-templates').attr('id') + "_{$model->company_id}";
    document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    $(this).closest('.tooltipster-base').hide();
});
JS
);
