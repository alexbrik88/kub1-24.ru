<?php
use frontend\models\Documents;
use yii\helpers\Html;
?>

<?php if (\Yii::$app->controller->action->id == 'index-auto') : ?>
    <?= Html::a('Счета', ['index', 'type' => Documents::IO_TYPE_OUT], ['class' => 'btn-mts btn-mts-white width100']) ?>
<?php else : ?>
    <?= Html::a('АвтоСчета', ['index-auto'], ['class' => 'btn-mts btn-mts-white width100']) ?>
<?php endif ?>
