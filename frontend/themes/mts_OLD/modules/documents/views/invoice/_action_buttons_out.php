<?php

use common\models\document\status\InvoiceStatus;
use frontend\rbac\permissions;
use frontend\rbac\UserRole;
use frontend\widgets\ConfirmModalWidget;
use yii\bootstrap\Dropdown;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\web\View;
use frontend\models\Documents;

/** @var $this View */
/** @var \common\models\document\Invoice $model */
/* @var $useContractor boolean */
/* @var $showSendPopup integer */

$contractorId = $useContractor ? $model->contractor_id : null;
$canUpdate = Yii::$app->user->can(permissions\document\Document::UPDATE_STATUS, [
    'model' => $model,
]);
$canSend = $canUpdate && !$model->is_deleted && $model->invoiceStatus->sendAllowed();
$printUrl = [
    'document-print',
    'actionType' => 'print',
    'id' => $model->id,
    'type' => $model->type,
    'filename' => $model->getPrintTitle(),
];
?>
<style type="text/css">
    .action-buttons-item {
        width: 13.5%;
    }
    .action-buttons-item .btn-mts {
        white-space: nowrap;
        padding: 10px 5px;
    }
    .dropdown-menu-mini {
        width: 100%;
        min-width: 98px;
        border-color: #4276a4 !important;
    }

    .dropdown-menu-mini a {
        padding: 7px 0px;
        text-align: center;
    }
</style>
<div class="action-buttons-box">
    <div class="action-buttons-item"<?= $canSend ? ' title="Отправить по e-mail"' : '' ?>>
        <?php if ($canSend): ?>
            <span id="tooltip-send-button" <?= isset($_COOKIE["tooltip_send_{$model->company_id}"]) && !$showSendPopup ?
                'class="tooltip2 tooltipstered-main" data-tooltip-content="#tooltip_send"' :
                null; ?> >
                <button class="btn-mts btn-mts-white width100 send-message-panel-trigger" id="send-btn">
                    <i class="ico-send"></i>
                    Отправить
                </button>
            </span>
        <?php endif; ?>
    </div>
    <div class="action-buttons-item">
        <?= Html::a('<i class="ico-print"></i> Печать', $printUrl, [
            'target' => '_blank',
            'class' => 'btn-mts btn-mts-white width100',
        ]); ?>
    </div>
    <div class="action-buttons-item">
        <span <?= isset($_COOKIE["tooltip_pdf_{$model->company_id}"]) ?
            'class="dropup tooltip2 tooltipstered-main" data-tooltip-content="#tooltip_pdf_hide"' : 'class="dropup"'; ?>>
            <?= Html::a('<i class="ico-download"></i> Скачать', '#', [
                'class' => 'btn-mts btn-mts-white width100 dropdown-toggle dropdown-linkjs',
                'data-toggle' => 'dropdown',
            ]); ?>
            <?= Dropdown::widget([
                'options' => [
                    'style' => '',
                    'class' => 'dropdown-menu-mini min-w-190 dropdown-centerjs',
                ],
                'items' => [
                    [
                        'label' => '<span style="display: inline-block;">PDF</span> файл',
                        'encode' => false,
                        'url' => ['document-print', 'actionType' => 'pdf', 'id' => $model->id, 'type' => $model->type, 'filename' => $model->getPdfFileName()],
                        'linkOptions' => [
                            'target' => '_blank',
                        ]
                    ],
                    [
                        'label' => '<span style="display: inline-block;">Word</span> файл',
                        'encode' => false,
                        'url' => ['docx', 'id' => $model->id, 'type' => $model->type],
                        'linkOptions' => [
                            'target' => '_blank',
                            'class' => 'get-word-link',
                        ]
                    ],
                    [
                        'label' => 'Отправить на мой e-mail',
                        'url' => 'javascript:;',
                        'linkOptions' => [
                            'class' => 'send-message-panel-trigger send-to-me',
                        ]
                    ],
                ],
            ]); ?>
        </span>
    </div>
    <div class="action-buttons-item">
        <?php if (Yii::$app->user->can(permissions\document\Invoice::CREATE)): ?>
            <?php if (\Yii::$app->user->identity->company->createInvoiceAllowed($model->type)) : ?>
                <?= ConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => '<i class="ico-copy"></i> Копировать',
                        'class' => 'btn-mts btn-mts-white width100',
                    ],
                    'confirmUrl' => Url::to(['create',
                        'type' => $model->type, 'clone' => $model->id, 'contractorId' => $contractorId,]),
                    'message' => 'Вы уверены, что хотите скопировать этот счёт?',
                ]);
                ?>
            <?php else : ?>
                <span class="btn-mts btn-mts-white width100 action-is-limited">
                    <i class="ico-copy"></i>
                    Копировать
                </span>
            <?php endif ?>
        <?php endif; ?>
    </div>

    <div class="action-buttons-item">
        <?php if ($model->isFullyPaid) : ?>
            <?php if (Yii::$app->user->can(UserRole::ROLE_CHIEF)): ?>
                <?= ConfirmModalWidget::widget([
                    'toggleButton' => [
                        'label' => '<span class="ico-Paid-smart-pls fs"></span> Снять оплату',
                        'class' => 'btn-mts btn-mts-white width100',
                    ],
                    'confirmUrl' => Url::to(['unpaid',
                        'type' => $model->type, 'id' => $model->id, 'contractorId' => $contractorId,]),
                    'message' => 'Вы уверены, что нужно снять оплату со счета?',
                ]);
                ?>
            <?php endif; ?>
        <?php else : ?>
            <?php if (!$model->is_deleted
                && $model->invoiceStatus->paymentAllowed($model->type)
                && Yii::$app->user->can(permissions\document\Invoice::ADD_CASH_FLOW)
            ): ?>
                <span id="tooltip-pay-button" <?= isset($_COOKIE["tooltip_paid_{$model->company_id}"]) && !$showSendPopup ?
                    'class="tooltip2 tooltipstered-main" data-tooltip-content = "#tooltip_paid"' :
                    null; ?>>
                    <button href="#paid" class="btn-mts btn-mts-white width100" data-toggle="modal">
                        <i class="ico-check_2"></i>
                        Оплачен
                    </button>
                </span>
            <?php endif; ?>
        <?php endif; ?>
    </div>

    <div class="action-buttons-item">
        <?php if (!$model->is_deleted && $model->invoiceStatus->rejectAllowed() && $canUpdate): ?>
            <?= ConfirmModalWidget::widget([
                'toggleButton' => [
                    'label' => '<i class="ico-cancel"></i> Отменён',
                    'class' => 'btn-mts btn-mts-white width100',
                ],
                'confirmUrl' => Url::to(['update-status',
                    'type' => $model->type, 'id' => $model->id, 'contractorId' => $contractorId,]),
                'message' => 'Вы уверены, что хотите отклонить этот счёт?',
                'confirmParams' => [
                    'status' => InvoiceStatus::STATUS_REJECTED,
                ],
            ]);
            ?>
        <?php endif; ?>
    </div>

    <div class="action-buttons-item">
        <?php if (!$model->is_deleted && Yii::$app->user->can(permissions\document\Document::DELETE)
            && in_array($model->invoice_status_id, InvoiceStatus::$deleteAllowed)): ?>
            <button class="btn-mts btn-mts-white width100" data-toggle="modal" href="#delete-confirm">
                    <i class="ico-trash"></i>
                    Удалить
            </button>
        <?php endif; ?>
    </div>
</div>
<div class="hidden">
    <div id="tooltip_send" class="box-tooltip-templates">
        <div class="box-my-account">
            2
        </div>
        Нажмите на кнопку, и выберете, кому отправить счет
        <div class="btn-close-tooltip-templates fa fa-times"></div>
    </div>

    <div id="tooltip_pdf" class="box-tooltip-templates">
        <div class="box-my-account">
            3
        </div>
        Нажмите на кнопку, что бы скачать в формате PDF
        <div class="btn-close-tooltip-templates fa fa-times"></div>
    </div>


    <div id="tooltip_paid" class="box-tooltip-templates">
        <div class="box-my-account">
            3
        </div>
        Если счет оплачен, нажмите на кнопку и выберите тип оплаты
        <div class="btn-close-tooltip-templates fa fa-times"></div>
    </div>
</div>
