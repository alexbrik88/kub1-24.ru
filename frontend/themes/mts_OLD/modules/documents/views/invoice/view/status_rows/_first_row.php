<?php

use common\models\document\status\ActStatus;
use common\models\document\status\PackingListStatus;
use common\models\document\status\WaybillStatus;
use common\models\document\status\InvoiceFactureStatus;
use frontend\models\Documents;
use yii\helpers\Html;
use yii\helpers\Url;
use common\models\document\status\ProxyStatus;

/**
 * @var $model \common\models\document\Invoice
 * @var $id integer
 * @var $document string
 * @var $title string
 * @var $action string
 */

$url = $action == 'create' ?
    [$document . '/' . $action, 'type' => $model->type, 'invoiceId' => $model->id] :
    [$document . '/' . $action, 'type' => $model->type, 'id' => $id];

$iconClass = 'plus-circle';

$isCreateProxy = ($document == 'proxy' && $action == 'create');

if ($document == 'waybill') {
    if ($model->getWaybill()->exists()) {
        $iconClass = $model->type == Documents::IO_TYPE_IN ? 'icon-doc' :
            WaybillStatus::getStatusIcon($model->getWaybill()->min('status_out_id'));
    }
} elseif ($document == 'packing-list') {
    if ($model->getPackingList()->exists()) {
        $iconClass = $model->type == Documents::IO_TYPE_IN ? 'icon-doc' :
            PackingListStatus::getStatusIcon($model->getPackingList()->min('status_out_id'));
    }
} elseif ($document == 'proxy') {
    if ($model->getProxies()->exists()) {
        $iconClass = $model->type == Documents::IO_TYPE_IN ? 'icon-doc' :
            ProxyStatus::getStatusIcon($model->getProxy()->min('status_out_id'));
    }
} elseif ($document == 'act') {
    if ($model->getAct()->exists()) {
        $iconClass = $model->type == Documents::IO_TYPE_IN ? 'icon-doc' :
            ActStatus::getStatusIcon($model->getAct()->min('status_out_id'));
    }
} elseif ($document == 'invoice-facture') {
    if ($model->getInvoiceFactures()->exists()) {
        $iconClass = $model->type == Documents::IO_TYPE_IN ? 'icon-doc' :
            InvoiceFactureStatus::getStatusIcon($model->getInvoiceFactures()->min('status_out_id'));
    }
} elseif ($document == 'upd') {
    if ($model->getUpds()->exists()) {
        $iconClass = 'icon-doc';
    }
} elseif ($document == 'order-document') {
    $iconClass = 'icon-doc';
} elseif ($document == 'agent-report') {
    $iconClass = 'icon-doc';
    $url = [$document . '/' . $action, 'type' => $model->agentReport->type, 'id' => $id];
}
?>

<div style="margin-top: 8px;">
    <?= Html::a(Html::tag('i', '', ['class' => "icon $iconClass"]) . $title, $url, [
        'class' => 'btn-mts btn-mts-white width100'.
            ($model->isRejected ? ' disabled' : '').
            ($isCreateProxy ? ' add-proxy' : ''),
        'title' => mb_convert_case(($action == 'create' ? 'Добавить ' : '') . $title, MB_CASE_TITLE)
    ]) ?>
</div>

<?php if ($isCreateProxy): ?>
    <?= $this->render('@frontend/modules/documents/views/proxy/_viewPartials/_invoices_modal', ['fromInvoice' => $model->id]) ?>
<?php endif; ?>